(define-module (crates-io ck b- ckb-dao-utils) #:use-module (crates-io))

(define-public crate-ckb-dao-utils-0.37.0-pre (c (n "ckb-dao-utils") (v "0.37.0-pre") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0in1801pyhgf8m946swy4l2z6cxyi4xhr50m0q94bcs6gl8aa91w")))

(define-public crate-ckb-dao-utils-0.37.0 (c (n "ckb-dao-utils") (v "0.37.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1c3bjdwxv352fg1by9dvp6zrzklchg6vb84g5fnzi4aii73y12x1")))

(define-public crate-ckb-dao-utils-0.38.0 (c (n "ckb-dao-utils") (v "0.38.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.38.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "057hi3s20psn8phi7kyimb7knpz04dfqnll5x9lbwx2sxkysgpap")))

(define-public crate-ckb-dao-utils-0.39.0 (c (n "ckb-dao-utils") (v "0.39.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.0") (d #t) (k 0)))) (h "0dsdan3pjbhbm8wzdbnib08hh2hjb1x66dvhcqzq2qyrwxgw6kwg")))

(define-public crate-ckb-dao-utils-0.39.1 (c (n "ckb-dao-utils") (v "0.39.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.1") (d #t) (k 0)))) (h "1spmlf2icpla5jf56mb0kxzcgxk5vxcgmd3cxp71fgfich64sqv6")))

(define-public crate-ckb-dao-utils-0.40.0 (c (n "ckb-dao-utils") (v "0.40.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.40.0") (d #t) (k 0)))) (h "1nfxsc6xd8ivmbyplf1xrn9b31nnl7cn4x3p5vq2wpn2qyw817vd")))

(define-public crate-ckb-dao-utils-0.42.0 (c (n "ckb-dao-utils") (v "0.42.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.42.0") (d #t) (k 0)))) (h "0aam9pbvldzrjab047nafss3z6h5i00gxagbygxyrhjrgc0l9495")))

(define-public crate-ckb-dao-utils-0.43.0 (c (n "ckb-dao-utils") (v "0.43.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.43.0") (d #t) (k 0)))) (h "07rk04an8p5brbnwp108sxzpg7igdhm303bwvbw2gjwl6a1nwsp0")))

(define-public crate-ckb-dao-utils-0.100.0-rc2 (c (n "ckb-dao-utils") (v "0.100.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "02pim1rkvxx2zcidj6vsdhap9i8nha68pc8lfzj193zf3n0bnnwi")))

(define-public crate-ckb-dao-utils-0.43.2 (c (n "ckb-dao-utils") (v "0.43.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.43.2") (d #t) (k 0)))) (h "0w3f8dgqz0f8hhck0n7f3vxc8ggvvc3fbw8pijll1ngxrq51aqxw")))

(define-public crate-ckb-dao-utils-0.100.0-rc4 (c (n "ckb-dao-utils") (v "0.100.0-rc4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "0snd6ldbiiwhn5yk3hk7qgpzv53v98rggmpai5947g16bdgc77sp")))

(define-public crate-ckb-dao-utils-0.100.0-rc5 (c (n "ckb-dao-utils") (v "0.100.0-rc5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "0ngp3ikf41rz25migcr61nbbxb3xy3k3149cki0hvlzmpfzhmj5p")))

(define-public crate-ckb-dao-utils-0.100.0 (c (n "ckb-dao-utils") (v "0.100.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0") (d #t) (k 0)))) (h "0qzddh4ma9q1xn03cy9s6j5wiawjbbhb2lr68cby58xcr9ma811n")))

(define-public crate-ckb-dao-utils-0.101.0 (c (n "ckb-dao-utils") (v "0.101.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.0") (d #t) (k 0)))) (h "15sc0hnmh0fijp6qvj50qw6pgjx43xzc3f83dk23ia3nh7vw9xik")))

(define-public crate-ckb-dao-utils-0.101.1 (c (n "ckb-dao-utils") (v "0.101.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.1") (d #t) (k 0)))) (h "0fs8ppngjsa9xzis10wxrpc07zcj4zgw50dqg48dalpijmhl1ihz")))

(define-public crate-ckb-dao-utils-0.101.2-testnet1 (c (n "ckb-dao-utils") (v "0.101.2-testnet1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "196gmz2fxdqw76jj4j1x0kjijrymczcfq2zjk5m0w5hawq5rdgir")))

(define-public crate-ckb-dao-utils-0.101.2 (c (n "ckb-dao-utils") (v "0.101.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.2") (d #t) (k 0)))) (h "0234mysg1p8q56vhhy2bfhhcyx12mlf3m5b6lg6mzvs85na047c6")))

(define-public crate-ckb-dao-utils-0.101.3 (c (n "ckb-dao-utils") (v "0.101.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.3") (d #t) (k 0)))) (h "1a7r2l8pfqcvqb1cj2ykf8x38lh3hx9ab191cibav1z5fsy5md5d")))

(define-public crate-ckb-dao-utils-0.101.4 (c (n "ckb-dao-utils") (v "0.101.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.4") (d #t) (k 0)))) (h "1ngcfs7wxq4mv3zvcmqlva7mpm3shb5d6x5g9jcaz0c4kkgj3fva")))

(define-public crate-ckb-dao-utils-0.101.5 (c (n "ckb-dao-utils") (v "0.101.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.5") (d #t) (k 0)))) (h "06c65831ah71s7017ai87r6hy83a157ahpjcgdk67yc7dq9dlggn") (y #t)))

(define-public crate-ckb-dao-utils-0.101.6 (c (n "ckb-dao-utils") (v "0.101.6") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.6") (d #t) (k 0)))) (h "0fw037ad96la9wn77c88ncc0rvpvasy0m98w6vd0fhdqg238a8px")))

(define-public crate-ckb-dao-utils-0.101.7 (c (n "ckb-dao-utils") (v "0.101.7") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.7") (d #t) (k 0)))) (h "1bxcx962f5jwhraqry4g06vsnliji4i585mi24i70qld53bf4hfs")))

(define-public crate-ckb-dao-utils-0.101.8 (c (n "ckb-dao-utils") (v "0.101.8") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.8") (d #t) (k 0)))) (h "0kv87bi9k31bwncj8vgns6ps2zl7i2i3vr92xi38skcwjpyrch4v")))

(define-public crate-ckb-dao-utils-0.102.0 (c (n "ckb-dao-utils") (v "0.102.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.102.0") (d #t) (k 0)))) (h "1d2l55ymfxpf4k50aypwphys5nnw363i1gdwl6qws86b2am1yg8l") (y #t)))

(define-public crate-ckb-dao-utils-0.103.0 (c (n "ckb-dao-utils") (v "0.103.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.103.0") (d #t) (k 0)))) (h "1ax0vd1970gr5whnkpjh34q6ghjhl34ripcmfl0hnxh13rb9a93d")))

(define-public crate-ckb-dao-utils-0.104.0 (c (n "ckb-dao-utils") (v "0.104.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.104.0") (d #t) (k 0)))) (h "0xf76lh7sh69bxnv3an5ck1xsvhy1qhb908w0d8ba688jlbyx2xc")))

(define-public crate-ckb-dao-utils-0.104.1 (c (n "ckb-dao-utils") (v "0.104.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.104.1") (d #t) (k 0)))) (h "1fxf4a8h4vzi1x7p7lvx30mgl0krj2v69hlyq5jpld3qf6wk973h")))

(define-public crate-ckb-dao-utils-0.105.0 (c (n "ckb-dao-utils") (v "0.105.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.105.0") (d #t) (k 0)))) (h "1fddpsg16y23fc0iim5233l9lc1g64f04xjqbpcdn0b2wdiv1czr") (y #t)))

(define-public crate-ckb-dao-utils-0.105.1 (c (n "ckb-dao-utils") (v "0.105.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.105.1") (d #t) (k 0)))) (h "1smm2lkr0n66xd1jz03cmpy6hbch269p3rdlkq6905f30rpc955j") (y #t)))

(define-public crate-ckb-dao-utils-0.106.0 (c (n "ckb-dao-utils") (v "0.106.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.106.0") (d #t) (k 0)))) (h "07mrj6426z5iwxrpwyg8w83zqzf9c60bf85ha4h0gr5zsbn7424p")))

(define-public crate-ckb-dao-utils-0.107.0 (c (n "ckb-dao-utils") (v "0.107.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.107.0") (d #t) (k 0)))) (h "1a5ycx8vib8q2kwy5rdrqvc3bgk49g7p59k6y22d5bs8c4f1b25h")))

(define-public crate-ckb-dao-utils-0.108.0 (c (n "ckb-dao-utils") (v "0.108.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.108.0") (d #t) (k 0)))) (h "0g6b26p6wj2lg8q21l0nf2kxv82sb97xfq8k2li1vzi3g5icja8r")))

(define-public crate-ckb-dao-utils-0.108.1 (c (n "ckb-dao-utils") (v "0.108.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.108.1") (d #t) (k 0)))) (h "1v34fqfj4xqh4i69276kd0vhbibai5lxfd00ly74ggc40c4wmz8i")))

(define-public crate-ckb-dao-utils-0.109.0 (c (n "ckb-dao-utils") (v "0.109.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.109.0") (d #t) (k 0)))) (h "0nf76l248b6kpg692bqb2fpyyiv2hfi15153zcqxwxvp11c5hdsx")))

(define-public crate-ckb-dao-utils-0.110.0 (c (n "ckb-dao-utils") (v "0.110.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.0") (d #t) (k 0)))) (h "0f7spsqjxh0mp3yqj9vlkkak8zyy4r007yhplg1vb5hzwl7agsl4")))

(define-public crate-ckb-dao-utils-0.110.0-rc1 (c (n "ckb-dao-utils") (v "0.110.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "1bygzmrfmpmvwjpn3vhzj6804dmczilmmxwhzrxwlpa511a89x48")))

(define-public crate-ckb-dao-utils-0.111.0-rc2 (c (n "ckb-dao-utils") (v "0.111.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "0wpfdyfmh9dyq6h7jlzqwp3zxsj405zyi4kw8j940w0qw355zywn")))

(define-public crate-ckb-dao-utils-0.111.0-rc3 (c (n "ckb-dao-utils") (v "0.111.0-rc3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "0bc7rr5rq03yhh4im8chd26id1ghqx2xx4nza3ndbn45ffqmlz1p")))

(define-public crate-ckb-dao-utils-0.111.0-rc4 (c (n "ckb-dao-utils") (v "0.111.0-rc4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "1305lpqhqyj7k2c4rdgbqv0qdyrh3vwjhlkz568zfqysmdhicay7")))

(define-public crate-ckb-dao-utils-0.111.0-rc5 (c (n "ckb-dao-utils") (v "0.111.0-rc5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "056lc51m27bmz5l1f2viarbfbh4dyzjcg6ia2bsmd1nwrmzz0s5z")))

(define-public crate-ckb-dao-utils-0.111.0-rc6 (c (n "ckb-dao-utils") (v "0.111.0-rc6") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "1xfs8kzh2psm6br5h48hy9rwb4knn9dvz0k1lhab13z2p1w0sfp0")))

(define-public crate-ckb-dao-utils-0.111.0-rc7 (c (n "ckb-dao-utils") (v "0.111.0-rc7") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "07a1cpp0m66ik2ham7zagay82945v5fb9ib4bimds0nbhw5xmpda")))

(define-public crate-ckb-dao-utils-0.111.0-rc8 (c (n "ckb-dao-utils") (v "0.111.0-rc8") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "1w9rhamrdmnbj5s30756j1v5q11lr8ddivnr1d9n1bmbjhl1s6lk")))

(define-public crate-ckb-dao-utils-0.110.1-rc1 (c (n "ckb-dao-utils") (v "0.110.1-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "03lydglmf1mn6hm9pl3ly4nx1spigxyd8y57al8r0x4srl1ajw92")))

(define-public crate-ckb-dao-utils-0.110.1-rc2 (c (n "ckb-dao-utils") (v "0.110.1-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "0w69qs8byyk6b6fqdc3wpjd74h1ba52mrxj4i7sy2qqkj812qyrb")))

(define-public crate-ckb-dao-utils-0.111.0-rc9 (c (n "ckb-dao-utils") (v "0.111.0-rc9") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "0c81q6923qgl2h2g9rkf9i85llzb9anqimrgj6xpcjdwc4i3ifa8")))

(define-public crate-ckb-dao-utils-0.111.0-rc10 (c (n "ckb-dao-utils") (v "0.111.0-rc10") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "0igz3f07kz9nh67aq669g75nr8jgwc9w1y7mrcbhmvhq60pagx2a")))

(define-public crate-ckb-dao-utils-0.110.1 (c (n "ckb-dao-utils") (v "0.110.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1") (d #t) (k 0)))) (h "0isz6gdjmjnsldw1k5yj4wl7k7jipzb4ngm5mm073yx3bcc4rap3")))

(define-public crate-ckb-dao-utils-0.110.2-rc1 (c (n "ckb-dao-utils") (v "0.110.2-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "026vgw94vdpkp0zkaq81jgwzgnyl2yk93lbfcc74hxm3n9sg9f7a")))

(define-public crate-ckb-dao-utils-0.111.0-rc11 (c (n "ckb-dao-utils") (v "0.111.0-rc11") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "0z0qrammczy5zbavz1r76sn75pm71c2dc4n3dnn2qmyn4dds2r4i")))

(define-public crate-ckb-dao-utils-0.110.2-rc2 (c (n "ckb-dao-utils") (v "0.110.2-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "0pf4sxj61dx2sgcvri3xd0551ir3pv3g5v0qzv5vyfyd2s1jgf18")))

(define-public crate-ckb-dao-utils-0.111.0-rc12 (c (n "ckb-dao-utils") (v "0.111.0-rc12") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "157a4dq89gg7phpdqrni2zgywamfvzvya1cnlkf48pwi6r17a9xf")))

(define-public crate-ckb-dao-utils-0.110.2 (c (n "ckb-dao-utils") (v "0.110.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2") (d #t) (k 0)))) (h "0qfk20d603cl5n75ki5gwwpyadn9iy3czjad7qp7scxbq1qs45rm")))

(define-public crate-ckb-dao-utils-0.111.0 (c (n "ckb-dao-utils") (v "0.111.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0") (d #t) (k 0)))) (h "00x4j94a42x8mshcqwv2kii2qmvl4hrqgvlxip8grlg9lkjqq5ay")))

(define-public crate-ckb-dao-utils-0.112.0-rc1 (c (n "ckb-dao-utils") (v "0.112.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "0axayqw9x5ay0f4fw1xmkgr0lwhl26zrxksviprg6rb0vrgw99fi")))

(define-public crate-ckb-dao-utils-0.112.0-rc2 (c (n "ckb-dao-utils") (v "0.112.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "0ifqmc01p4xbqyyhdy0n4wjk5iz52xrdh89fg84avpa4lm4qxg5z")))

(define-public crate-ckb-dao-utils-0.112.0 (c (n "ckb-dao-utils") (v "0.112.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0") (d #t) (k 0)))) (h "057dnxban6c6fiix8i9f2ijsg6d1536gihhgcxz22nsagjj3nypn")))

(define-public crate-ckb-dao-utils-0.112.0-rc3 (c (n "ckb-dao-utils") (v "0.112.0-rc3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "009giz62xnzvf6ny3i539x4ckvr96lvml8z58c4mzav84alvlc5n")))

(define-public crate-ckb-dao-utils-0.112.1 (c (n "ckb-dao-utils") (v "0.112.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.1") (d #t) (k 0)))) (h "0w0fflf92n3k73c83g5njaq4n6hd4gkcjmdpc50cjhmqcd8lhmgz")))

(define-public crate-ckb-dao-utils-0.113.0-rc1 (c (n "ckb-dao-utils") (v "0.113.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "0cx6ywmgx56n84gdd1ssx1dxq0ngrvixyqpwicf0j7czk758d296")))

(define-public crate-ckb-dao-utils-0.113.0-rc2 (c (n "ckb-dao-utils") (v "0.113.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "1vshiqcygcmpmkzsf61pj6m77anfzqqr6l8i3c0nrrjlzlkb8gjg")))

(define-public crate-ckb-dao-utils-0.113.0-rc3 (c (n "ckb-dao-utils") (v "0.113.0-rc3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "08j3rfw2akvm830q8fx9xak10d1pcwkvajv52z9npm6qz8znhbsn")))

(define-public crate-ckb-dao-utils-0.113.0 (c (n "ckb-dao-utils") (v "0.113.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0") (d #t) (k 0)))) (h "0y97gh3bkw4g0d4fm4hlysk6skvhr573gvd6dibnxwqzkc7k1yda")))

(define-public crate-ckb-dao-utils-0.113.1-rc1 (c (n "ckb-dao-utils") (v "0.113.1-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "12i07x7mpzp0d4a7v7894q5amp0ds11h1l1sipbncmvflzxdbxqb")))

(define-public crate-ckb-dao-utils-0.114.0-rc1 (c (n "ckb-dao-utils") (v "0.114.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "0n40l0jrpg5f1052w2qcx8wcxw2bdxwgf1h9nni1d1gc9l85rg8a")))

(define-public crate-ckb-dao-utils-0.113.1-rc2 (c (n "ckb-dao-utils") (v "0.113.1-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "1rq3pil2vyy00fsq9m4xqxn349aapg05j862sl44ch6z42b1wmjz")))

(define-public crate-ckb-dao-utils-0.113.1 (c (n "ckb-dao-utils") (v "0.113.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1") (d #t) (k 0)))) (h "1m58skqndlzrb02s6lagnv5dqmk8n53ynvnk7w0d6s16adhj1my5")))

(define-public crate-ckb-dao-utils-0.114.0-rc2 (c (n "ckb-dao-utils") (v "0.114.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "1pf8k8l8jiz4n6gmqi25zrn9ias21w59wklcv3y7y1a8zl3m3s81")))

(define-public crate-ckb-dao-utils-0.114.0-rc3 (c (n "ckb-dao-utils") (v "0.114.0-rc3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "11p64c19z5wpqvvwz0518qvgmny68190afji7d1i9ks2wakkkf4j")))

(define-public crate-ckb-dao-utils-0.114.0 (c (n "ckb-dao-utils") (v "0.114.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0") (d #t) (k 0)))) (h "1iy0xz2dv3j9d08h95mcmy56x4dzh1fqmprv69j778lhpgnjl25f")))

(define-public crate-ckb-dao-utils-0.115.0-pre (c (n "ckb-dao-utils") (v "0.115.0-pre") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-pre") (d #t) (k 0)))) (h "1pbvncdsg7pa77rwrfx319scsl5x7xcgxpnik8d49qh9snnqhzda")))

(define-public crate-ckb-dao-utils-0.115.0-rc1 (c (n "ckb-dao-utils") (v "0.115.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "1hsv90mxl1yyrjqyhls2wrc12nzaf9zbgl4y6yiya9ha4h417y73")))

(define-public crate-ckb-dao-utils-0.115.0-rc2 (c (n "ckb-dao-utils") (v "0.115.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "1lmq85n6cwqk94jg6rgky03shqg6fl8hnp5h2s4phbbxb4jpzdq2")))

(define-public crate-ckb-dao-utils-0.115.0 (c (n "ckb-dao-utils") (v "0.115.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0") (d #t) (k 0)))) (h "15lccccn99vncjz6vvkr5vn8830jjwfawhm98vcvprqh092yb7yf")))

(define-public crate-ckb-dao-utils-0.116.0-rc1 (c (n "ckb-dao-utils") (v "0.116.0-rc1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0rdk187yzyg0l5v5mr0vqssrvhvaf9nddcz5vsqa4skijfzqby2m")))

(define-public crate-ckb-dao-utils-0.116.0-rc2 (c (n "ckb-dao-utils") (v "0.116.0-rc2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "0w3ljaham5g22ybv3yq5628wami1wdgygdiv92xy9yyap9nh555i")))

(define-public crate-ckb-dao-utils-0.116.0 (c (n "ckb-dao-utils") (v "0.116.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0") (d #t) (k 0)))) (h "16gjspvv2yibyzla6m4ydz9ygpsgyij94gw8g7qmwp4kl1mpgm6v") (y #t)))

(define-public crate-ckb-dao-utils-0.116.1 (c (n "ckb-dao-utils") (v "0.116.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.1") (d #t) (k 0)))) (h "11c0a0407kiir3mx99p1vzjlyza2ssvy9zbv66c40hiac1n61cvf")))

