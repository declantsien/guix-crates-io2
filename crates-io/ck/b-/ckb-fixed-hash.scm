(define-module (crates-io ck b- ckb-fixed-hash) #:use-module (crates-io))

(define-public crate-ckb-fixed-hash-0.37.0-pre (c (n "ckb-fixed-hash") (v "0.37.0-pre") (d (list (d (n "ckb-fixed-hash-core") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.37.0-pre") (d #t) (k 0)))) (h "09bki50xwszn36sx85j46w1q0bi1yfqiy5lwslm0hvrmqfwii4gk")))

(define-public crate-ckb-fixed-hash-0.37.0 (c (n "ckb-fixed-hash") (v "0.37.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.37.0") (d #t) (k 0)))) (h "1ka1x7pxmnpjbjmqk1i69crj3qf6cp2sfnjqf2mwh1wl36j2kxmq")))

(define-public crate-ckb-fixed-hash-0.38.0 (c (n "ckb-fixed-hash") (v "0.38.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.38.0") (d #t) (k 0)))) (h "1014vcjkjy6j0nhc4s9wfv7fi8xbr8i2a95701jfl7rb67f4fpl1")))

(define-public crate-ckb-fixed-hash-0.39.0 (c (n "ckb-fixed-hash") (v "0.39.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.39.0") (d #t) (k 0)))) (h "0iqk16kvgcag2kz7yi5jcmg75g8z9yr5xa0qp9b2vwp95racmf62")))

(define-public crate-ckb-fixed-hash-0.39.1 (c (n "ckb-fixed-hash") (v "0.39.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.39.1") (d #t) (k 0)))) (h "1ikd9g1ngvi2navamrwxqihdrm2dpd02y5jh8bkzq8qzcb41lwr0")))

(define-public crate-ckb-fixed-hash-0.40.0 (c (n "ckb-fixed-hash") (v "0.40.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.40.0") (d #t) (k 0)))) (h "00l6xfxwzy6sjvwvbmsr594h08f5hq55sbfdl0rykm0khbzp7jdz")))

(define-public crate-ckb-fixed-hash-0.42.0 (c (n "ckb-fixed-hash") (v "0.42.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.42.0") (d #t) (k 0)))) (h "0sqz4j8j6dwl9d655kxwg31xmkc590j1mj541f8b3847yikfnrqp")))

(define-public crate-ckb-fixed-hash-0.43.0 (c (n "ckb-fixed-hash") (v "0.43.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.43.0") (d #t) (k 0)))) (h "1cyj38fn6fvq7nw1xsfljh8kh7slf5d8jh15b7g5k7kylrk34pdz")))

(define-public crate-ckb-fixed-hash-0.100.0-rc2 (c (n "ckb-fixed-hash") (v "0.100.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "1livyxy9s9fxkrixlb6qq5imzd43jnjb7mydj54jgrmmgb64zry1")))

(define-public crate-ckb-fixed-hash-0.43.2 (c (n "ckb-fixed-hash") (v "0.43.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.43.2") (d #t) (k 0)))) (h "1w3rlb4xllvkrpr47i4cr0cay3f5cvk9byzyqm77mbb2jwbzphsj")))

(define-public crate-ckb-fixed-hash-0.100.0-rc4 (c (n "ckb-fixed-hash") (v "0.100.0-rc4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "0acd60wkrjyl5f2dn9q7hm58yj3qzw4s8j0pq0hbjy67vfmn24ny")))

(define-public crate-ckb-fixed-hash-0.100.0-rc5 (c (n "ckb-fixed-hash") (v "0.100.0-rc5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "1nm42072x4fr1xbxdc01207v8wd9rbrlpzicc73rgcwahw0mfilz")))

(define-public crate-ckb-fixed-hash-0.100.0 (c (n "ckb-fixed-hash") (v "0.100.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.100.0") (d #t) (k 0)))) (h "0lzi6a2bj2zawv0w7ldjzl5habi2in2y93dp3f1h43lp21a8r8iv")))

(define-public crate-ckb-fixed-hash-0.101.0 (c (n "ckb-fixed-hash") (v "0.101.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.0") (d #t) (k 0)))) (h "1896s8x8gpwi0ka8m7snq8fkzr9nj2ff9jr68gfw42dyr80r93pl")))

(define-public crate-ckb-fixed-hash-0.101.1 (c (n "ckb-fixed-hash") (v "0.101.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.1") (d #t) (k 0)))) (h "0lbpfc1vayp0vgfyr9rl9xgijp528918lzhlxrqfjaz4x0xb2aad")))

(define-public crate-ckb-fixed-hash-0.101.2-testnet1 (c (n "ckb-fixed-hash") (v "0.101.2-testnet1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "116cvslwnkdqg84803dvaxynndxmcii64phn66nxnkc6wqn7yn3x")))

(define-public crate-ckb-fixed-hash-0.101.2 (c (n "ckb-fixed-hash") (v "0.101.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.2") (d #t) (k 0)))) (h "1dd0v6vyhg2mq8lsi4i40jcnw3d57ln8x7jrlpsfy9977kj0g77i")))

(define-public crate-ckb-fixed-hash-0.101.3 (c (n "ckb-fixed-hash") (v "0.101.3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.3") (d #t) (k 0)))) (h "1bx3ga1lwgqrhj96amlwxk58vkjnpna5gf1scr74qwnzqx0x5rws")))

(define-public crate-ckb-fixed-hash-0.101.4 (c (n "ckb-fixed-hash") (v "0.101.4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.4") (d #t) (k 0)))) (h "15m5qk5l4nfn9mg2d9lcg7xm8z207dx528f3mpyx6wb1xdhqq47s")))

(define-public crate-ckb-fixed-hash-0.101.5 (c (n "ckb-fixed-hash") (v "0.101.5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.5") (d #t) (k 0)))) (h "0ivxhr3ck2zcswhsa29gmv27pd8r0rbj0942iqpd6m1fyg525y6j") (y #t)))

(define-public crate-ckb-fixed-hash-0.101.6 (c (n "ckb-fixed-hash") (v "0.101.6") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.6") (d #t) (k 0)))) (h "0dagk27zgi4v0hxvqggkz24vkvyf6020r2rigjvmamc788v8hqhv")))

(define-public crate-ckb-fixed-hash-0.101.7 (c (n "ckb-fixed-hash") (v "0.101.7") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.7") (d #t) (k 0)))) (h "07yjwvcv3s3iqzc8ax470v1m6xznwyr3spw6jkp15zzifrv35qkp")))

(define-public crate-ckb-fixed-hash-0.101.8 (c (n "ckb-fixed-hash") (v "0.101.8") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.101.8") (d #t) (k 0)))) (h "0vrm1vd0zw1jiizw5rablyj8yl2llkfq067m7ax0vdkifdlwar0p")))

(define-public crate-ckb-fixed-hash-0.102.0 (c (n "ckb-fixed-hash") (v "0.102.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.102.0") (d #t) (k 0)))) (h "0h26yxv1bfwig9bmgjmvcy037arfm8ayp7bvkdx6sm7m0lca3h68") (y #t)))

(define-public crate-ckb-fixed-hash-0.103.0 (c (n "ckb-fixed-hash") (v "0.103.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.103.0") (d #t) (k 0)))) (h "0grnqck0zsa14cb979psj75vypfrcyq8yk8m42ccxdkks0k6rqc7")))

(define-public crate-ckb-fixed-hash-0.104.0 (c (n "ckb-fixed-hash") (v "0.104.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.104.0") (d #t) (k 0)))) (h "1lwfik9h0pl1chn9fxrbkr8i3wh1npkxvls2n6zkmzqgzl9fv0w1")))

(define-public crate-ckb-fixed-hash-0.104.1 (c (n "ckb-fixed-hash") (v "0.104.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.104.1") (d #t) (k 0)))) (h "10f2981dr2z4fc8d9qj9yilay4r7gc0hwfrh9splw46c4sj5ngx1")))

(define-public crate-ckb-fixed-hash-0.105.0 (c (n "ckb-fixed-hash") (v "0.105.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.105.0") (d #t) (k 0)))) (h "0xc50hgpfgzdkhf1xncqz1n8c10fsi6s8dacm834zs512hlajm9x") (y #t)))

(define-public crate-ckb-fixed-hash-0.105.1 (c (n "ckb-fixed-hash") (v "0.105.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.105.1") (d #t) (k 0)))) (h "1qgf0fbkjxigjsig2y96chxsw0xpgpbavi37ivgz53xv7l0nbmmk") (y #t)))

(define-public crate-ckb-fixed-hash-0.106.0 (c (n "ckb-fixed-hash") (v "0.106.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.106.0") (d #t) (k 0)))) (h "1jlfzaca4n01kn32m5h25fdrvl3amy4jq08jajad0fm60rq8zflf")))

(define-public crate-ckb-fixed-hash-0.107.0 (c (n "ckb-fixed-hash") (v "0.107.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.107.0") (d #t) (k 0)))) (h "1458ca8lc1f322qvbdnb8c78yh9z43g1n0sq5hz4x7lgya6vxk1a")))

(define-public crate-ckb-fixed-hash-0.108.0 (c (n "ckb-fixed-hash") (v "0.108.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.108.0") (d #t) (k 0)))) (h "0yf8nr7jvd7mzn9p1wapl3zmc3g30f5645kdw0r8nx13bd2vrjq0")))

(define-public crate-ckb-fixed-hash-0.108.1 (c (n "ckb-fixed-hash") (v "0.108.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.108.1") (d #t) (k 0)))) (h "1dm9xhyc0nll3wcz078xsv3mfgqn3f8kz7agi2yspnsgb3lznw85")))

(define-public crate-ckb-fixed-hash-0.109.0 (c (n "ckb-fixed-hash") (v "0.109.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.109.0") (d #t) (k 0)))) (h "1kq8blwc73ydcngzq6shvypfv6adr618rzfhlqjvvrmr7r11jv68")))

(define-public crate-ckb-fixed-hash-0.110.0 (c (n "ckb-fixed-hash") (v "0.110.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.0") (d #t) (k 0)))) (h "0idn683pzxpj9z9v70s4avqn2y8257k86vsq7hnzdbav8pcc54g4")))

(define-public crate-ckb-fixed-hash-0.110.0-rc1 (c (n "ckb-fixed-hash") (v "0.110.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "0gvvjpd2ncxr8vg48km756pm0c67vcjnlfii4797j9ipfka7pvqb")))

(define-public crate-ckb-fixed-hash-0.111.0-rc2 (c (n "ckb-fixed-hash") (v "0.111.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "0a01zials5lg28vsrhfhvz6385mccr20sy16scb7ldvky0i9n76m")))

(define-public crate-ckb-fixed-hash-0.111.0-rc3 (c (n "ckb-fixed-hash") (v "0.111.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "0kn1y0wyf2ak2pnmacyza51hx73qbpml5xqpakbd86apwdhk147b")))

(define-public crate-ckb-fixed-hash-0.111.0-rc4 (c (n "ckb-fixed-hash") (v "0.111.0-rc4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "0jwxrvpwqcml3anakxyhs7i6qxykch7sggiwq9rz6bc9ngdg6p8c")))

(define-public crate-ckb-fixed-hash-0.111.0-rc5 (c (n "ckb-fixed-hash") (v "0.111.0-rc5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "1717g8bs2dsjay83ir1rs515k0s23mszmflhl3l3j3kayvna3crz")))

(define-public crate-ckb-fixed-hash-0.111.0-rc6 (c (n "ckb-fixed-hash") (v "0.111.0-rc6") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "17i2vvk0g1vkw4lr1qnz1hs3bcn6zm3vxx8sb9nrbppjci0y5kzk")))

(define-public crate-ckb-fixed-hash-0.111.0-rc7 (c (n "ckb-fixed-hash") (v "0.111.0-rc7") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "1xc5v406pr0vaa417c6qfq7xgkf24fk6w2f7wcvhdfk8p9v08p49")))

(define-public crate-ckb-fixed-hash-0.111.0-rc8 (c (n "ckb-fixed-hash") (v "0.111.0-rc8") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "0hv7fq67m36zkmkqg2y455ib3c127ghslk83zd9xwjiwi806i5cr")))

(define-public crate-ckb-fixed-hash-0.110.1-rc1 (c (n "ckb-fixed-hash") (v "0.110.1-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "1z7zsi1p43ff1ghl7098c9hq9j4bq6r7xn0nxxfd7apic7qr1sq2")))

(define-public crate-ckb-fixed-hash-0.110.1-rc2 (c (n "ckb-fixed-hash") (v "0.110.1-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "0h33ijfk6yzhkhqz19iyn1dxx0afb78akqd0z7z1bkndx991xgq0")))

(define-public crate-ckb-fixed-hash-0.111.0-rc9 (c (n "ckb-fixed-hash") (v "0.111.0-rc9") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "0b9fbrh758p8238b5kyd1nf3ly8xc12drnr34q90iv6jrfj64kzs")))

(define-public crate-ckb-fixed-hash-0.111.0-rc10 (c (n "ckb-fixed-hash") (v "0.111.0-rc10") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "0rdw8y680sbkp9vdjimffcmdq21vry2msailpyw3qqzdw0p8brp8")))

(define-public crate-ckb-fixed-hash-0.110.1 (c (n "ckb-fixed-hash") (v "0.110.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.1") (d #t) (k 0)))) (h "14kf4m8gr98icnxs9g8d5skq8pz8ms0l12fjch7hsawnjya2n1zm")))

(define-public crate-ckb-fixed-hash-0.110.2-rc1 (c (n "ckb-fixed-hash") (v "0.110.2-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "099yxx8zfmjc62052p0ml4pnj5hfix04qp3jvq3plwlwv2qn02gh")))

(define-public crate-ckb-fixed-hash-0.111.0-rc11 (c (n "ckb-fixed-hash") (v "0.111.0-rc11") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "09hzy0zipl6zghzcg6vyfs4vhqcc5q4ijf7arx2rrbz4xfznwbd1")))

(define-public crate-ckb-fixed-hash-0.110.2-rc2 (c (n "ckb-fixed-hash") (v "0.110.2-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "165klk2wv6p96nwww6bw2pmgn5bqqam9ahmrld0z75wjas21m4ag")))

(define-public crate-ckb-fixed-hash-0.111.0-rc12 (c (n "ckb-fixed-hash") (v "0.111.0-rc12") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "0cgqvkw0c9caxq8rm7j17268xz792000ik7k01zm3xa0qrsh260m")))

(define-public crate-ckb-fixed-hash-0.110.2 (c (n "ckb-fixed-hash") (v "0.110.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.110.2") (d #t) (k 0)))) (h "16jchrd9lhgdjlzhh47987nf3034i7zkgqnbc97ln6pbnjdmn3vj")))

(define-public crate-ckb-fixed-hash-0.111.0 (c (n "ckb-fixed-hash") (v "0.111.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.111.0") (d #t) (k 0)))) (h "1l1zqaa9qws4jm5xq1xqpwdclcwqy3hp63b8x7533ypz8nqgl79v")))

(define-public crate-ckb-fixed-hash-0.112.0-rc1 (c (n "ckb-fixed-hash") (v "0.112.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "0bay8812gchf99v5wr5ccpidjfwc955hxh0bw4mds7syrlm3an6p")))

(define-public crate-ckb-fixed-hash-0.112.0-rc2 (c (n "ckb-fixed-hash") (v "0.112.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "02j2wjyj8hkpcixz2j01vxvv1dwcpabgn118p0mvgl66bq138sgv")))

(define-public crate-ckb-fixed-hash-0.112.0 (c (n "ckb-fixed-hash") (v "0.112.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.112.0") (d #t) (k 0)))) (h "0nx14gmys7vp2wgj219lrh8pm9aap1wxf9xk41xhcgx4wlr3gvba")))

(define-public crate-ckb-fixed-hash-0.112.0-rc3 (c (n "ckb-fixed-hash") (v "0.112.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "1pqbj6rmhx9jrn33nc45zgznn59vvhrn62zvisjiax1ynr9r77gz")))

(define-public crate-ckb-fixed-hash-0.112.1 (c (n "ckb-fixed-hash") (v "0.112.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.112.1") (d #t) (k 0)))) (h "0gpiql7386wjrzy4h8nly2zsj00cj9dlr7hy622vr6jfm3yslm0j")))

(define-public crate-ckb-fixed-hash-0.113.0-rc1 (c (n "ckb-fixed-hash") (v "0.113.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "147bs2syz5866n9mcqga57zk80y2c65qssvq2bwxjzy8iik1wr4r")))

(define-public crate-ckb-fixed-hash-0.113.0-rc2 (c (n "ckb-fixed-hash") (v "0.113.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "1963sbmlyivm076yjmpk3682l3siy1f8jbmf5adldknfh6al5wrq")))

(define-public crate-ckb-fixed-hash-0.113.0-rc3 (c (n "ckb-fixed-hash") (v "0.113.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "0g89svzp0lzgxhx92g3q515n5lz62hs5kpx57jyqik3i8wbkhd2x")))

(define-public crate-ckb-fixed-hash-0.113.0 (c (n "ckb-fixed-hash") (v "0.113.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.0") (d #t) (k 0)))) (h "1357fg30l7zz6wk2w6yg19ra1hwidb1xmvsw0azmnz0j0lz9ha86")))

(define-public crate-ckb-fixed-hash-0.113.1-rc1 (c (n "ckb-fixed-hash") (v "0.113.1-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "0h5rz8j6m19mbdpcvnnarf3p18x4hkjp2ki6s06hlxxm3jslc7b9")))

(define-public crate-ckb-fixed-hash-0.114.0-rc1 (c (n "ckb-fixed-hash") (v "0.114.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "0j8j72j03jzbgig5fr5j5jyv42azmvcbsjma042jbbpy263hdr0c")))

(define-public crate-ckb-fixed-hash-0.113.1-rc2 (c (n "ckb-fixed-hash") (v "0.113.1-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "0mk3zvwyms5kcxl659fsbfqjh86wz1vkfk48i7mayz2lr6bh7lf6")))

(define-public crate-ckb-fixed-hash-0.113.1 (c (n "ckb-fixed-hash") (v "0.113.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.113.1") (d #t) (k 0)))) (h "1ywv7rxml1isimz828f4mka6y9xdsnrwhh2cd55kkg9gqxp66rwm")))

(define-public crate-ckb-fixed-hash-0.114.0-rc2 (c (n "ckb-fixed-hash") (v "0.114.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "0sj86xajlsw5m79315n6892qk8gnpl4mznvps47iqikhf2xg3ni7")))

(define-public crate-ckb-fixed-hash-0.114.0-rc3 (c (n "ckb-fixed-hash") (v "0.114.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "190rsv80a5yvgizrksvqjq01247wjdd2s3hvn2bwmr7f2diaaqb4")))

(define-public crate-ckb-fixed-hash-0.114.0 (c (n "ckb-fixed-hash") (v "0.114.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.114.0") (d #t) (k 0)))) (h "01y9kl3blqxc8nqq09lvz7in9jkr5p58zl22s970dh8v4zhpvchs")))

(define-public crate-ckb-fixed-hash-0.115.0-pre (c (n "ckb-fixed-hash") (v "0.115.0-pre") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.115.0-pre") (d #t) (k 0)))) (h "1cqb6irr3c029arlpz5cjflcql0nzkxkakzd82i07bgbxlqhn0di")))

(define-public crate-ckb-fixed-hash-0.115.0-rc1 (c (n "ckb-fixed-hash") (v "0.115.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "1dc727a1a899qqqqibqc573q0820pqr2wl9qq3yjn6l679n3a0s9")))

(define-public crate-ckb-fixed-hash-0.115.0-rc2 (c (n "ckb-fixed-hash") (v "0.115.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "1qixkmz0mr3fr3q812xhmryq29ghw40l8q6wdkx3g6al4xirm3p5")))

(define-public crate-ckb-fixed-hash-0.115.0 (c (n "ckb-fixed-hash") (v "0.115.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.115.0") (d #t) (k 0)))) (h "1mhanrji69b869ffgs8z6ah5a2qzknvgl7szhdd9h2d9y1hia7by")))

(define-public crate-ckb-fixed-hash-0.116.0-rc1 (c (n "ckb-fixed-hash") (v "0.116.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0anvnrvj0whb1lfvgy8s703x4zishylps30ix3lsanwx0cbxi3aa")))

(define-public crate-ckb-fixed-hash-0.116.0-rc2 (c (n "ckb-fixed-hash") (v "0.116.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "1xvpcjfnvvczh2w6bdkg9s9ndpvh1k3kj2jppgfq52kdjwjxiwcb")))

(define-public crate-ckb-fixed-hash-0.116.0 (c (n "ckb-fixed-hash") (v "0.116.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.116.0") (d #t) (k 0)))) (h "0fi4gzrabpjabzwqj4nvw5hx5vnrf907h319amcb4yd59alh3n4n") (y #t)))

(define-public crate-ckb-fixed-hash-0.116.1 (c (n "ckb-fixed-hash") (v "0.116.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-fixed-hash-macros") (r "=0.116.1") (d #t) (k 0)))) (h "16vpgi76kx49s1wb8xz34k4zkk8vvddcqd9rj8klif0phzqr2x2a")))

