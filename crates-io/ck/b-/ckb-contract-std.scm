(define-module (crates-io ck b- ckb-contract-std) #:use-module (crates-io))

(define-public crate-ckb-contract-std-0.1.0-pre (c (n "ckb-contract-std") (v "0.1.0-pre") (h "0h4vl38ahq09gm22cnnx6rl3lkqizisd87l33782lpxdsl0da2nc")))

(define-public crate-ckb-contract-std-0.1.1-pre (c (n "ckb-contract-std") (v "0.1.1-pre") (d (list (d (n "buddy-alloc") (r "^0.1.0-pre.2") (d #t) (k 0)))) (h "0d69p8bwdyhpgacr616ni4ghbs764yi9sgx40i847zw2761sp3rp")))

(define-public crate-ckb-contract-std-0.1.2-pre (c (n "ckb-contract-std") (v "0.1.2-pre") (d (list (d (n "buddy-alloc") (r "^0.1.0") (d #t) (k 0)))) (h "14gnnhvnyx04f5iqrlax3r3gdazpprqcq4hdhdcmn0dh4mjwj7da") (y #t)))

(define-public crate-ckb-contract-std-0.1.0 (c (n "ckb-contract-std") (v "0.1.0") (d (list (d (n "buddy-alloc") (r "^0.1.0") (d #t) (k 0)))) (h "1zqa5r94mrb3s7i2k95adxkjv07hg6crwj4fymc0qj46mxx0375g") (y #t)))

(define-public crate-ckb-contract-std-0.1.3-pre (c (n "ckb-contract-std") (v "0.1.3-pre") (d (list (d (n "buddy-alloc") (r "^0.1.0") (d #t) (k 0)))) (h "084z5la52zpqn1hn36vc3lvsix4aq97x2jymq8bh1qx1lskwac4x") (y #t)))

(define-public crate-ckb-contract-std-0.1.4-pre (c (n "ckb-contract-std") (v "0.1.4-pre") (d (list (d (n "buddy-alloc") (r "^0.1.1") (d #t) (k 0)))) (h "1d6sqyqbi646jbbqf24xmiap8k3qmryfqmswqjha4bh1n4p8mimd") (y #t)))

(define-public crate-ckb-contract-std-0.1.5-pre (c (n "ckb-contract-std") (v "0.1.5-pre") (d (list (d (n "buddy-alloc") (r "^0.1.2") (d #t) (k 0)))) (h "0d8jdz8z2zrswx1fskjhxjxprgk8qbd25ikadm9fx83xj0ggkl1l") (y #t)))

(define-public crate-ckb-contract-std-0.1.1 (c (n "ckb-contract-std") (v "0.1.1") (d (list (d (n "buddy-alloc") (r "^0.1.2") (d #t) (k 0)))) (h "1jjwsqxif9psm6ffd35gnh8z2x3l54xi61qjia0zskpav37hmib8")))

(define-public crate-ckb-contract-std-0.1.2 (c (n "ckb-contract-std") (v "0.1.2") (d (list (d (n "buddy-alloc") (r "^0.2.0") (d #t) (k 0)))) (h "02mca6kwk9zmpgspxchw1p2jg5702dg665zymhhk175381nlijvv")))

(define-public crate-ckb-contract-std-0.1.3 (c (n "ckb-contract-std") (v "0.1.3") (d (list (d (n "buddy-alloc") (r "^0.2.0") (d #t) (k 0)))) (h "1g6vx52rh6q2ff483v8b6p2rl2qcib63lj9k5hdjq8scnnmy7jqs")))

(define-public crate-ckb-contract-std-0.1.4 (c (n "ckb-contract-std") (v "0.1.4") (d (list (d (n "buddy-alloc") (r "^0.2.0") (d #t) (k 0)))) (h "13qki3if90b52mdf6a9ngcn1kijmn33iylljgfsccrc7p6d8hwj5")))

(define-public crate-ckb-contract-std-0.1.5 (c (n "ckb-contract-std") (v "0.1.5") (d (list (d (n "buddy-alloc") (r "^0.2.0") (d #t) (k 0)))) (h "1jms2343bl6ipsi7i4bzdni310nkqm5cj1lyhnyhib7ljc2nhk16")))

