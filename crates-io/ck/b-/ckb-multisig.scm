(define-module (crates-io ck b- ckb-multisig) #:use-module (crates-io))

(define-public crate-ckb-multisig-0.37.0-pre (c (n "ckb-multisig") (v "0.37.0-pre") (d (list (d (n "ckb-crypto") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "04w12lqzcm7wgkqa7pfvrc15xvl8a0a3wj46wydghcwvz7v4ig3s")))

(define-public crate-ckb-multisig-0.37.0 (c (n "ckb-multisig") (v "0.37.0") (d (list (d (n "ckb-crypto") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.37.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "149b7w3v77pcjyzi8ma5vw1ydc1k1vq32654cc6vb3fn2rb6m2h2")))

(define-public crate-ckb-multisig-0.38.0 (c (n "ckb-multisig") (v "0.38.0") (d (list (d (n "ckb-crypto") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.38.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0q9a728gp9n9lciy38dd6qcj4hw798nb2c2w8vxvgdc40p2153l6")))

(define-public crate-ckb-multisig-0.39.0 (c (n "ckb-multisig") (v "0.39.0") (d (list (d (n "ckb-crypto") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.39.0") (d #t) (k 0)))) (h "1ra51larbr1pijgvsahyc7rngcfs6mnbr528y346dgqjbp7xyv43")))

(define-public crate-ckb-multisig-0.39.1 (c (n "ckb-multisig") (v "0.39.1") (d (list (d (n "ckb-crypto") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.39.1") (d #t) (k 0)))) (h "1wz3bvrmvhxnp9szym15ca2llp9f7w04lmjfi486ag9lii66c18b")))

(define-public crate-ckb-multisig-0.40.0 (c (n "ckb-multisig") (v "0.40.0") (d (list (d (n "ckb-crypto") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.40.0") (d #t) (k 0)))) (h "178macqsrp11g3fr2lfvn7x6d9nfj6w682d19ibbzznfpl4bic0f")))

(define-public crate-ckb-multisig-0.42.0 (c (n "ckb-multisig") (v "0.42.0") (d (list (d (n "ckb-crypto") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.42.0") (d #t) (k 0)))) (h "19agvcivyj34hs77jzrmn5kl3ab9ya37m2vnfb2lf1bgj46xfz85")))

(define-public crate-ckb-multisig-0.43.0 (c (n "ckb-multisig") (v "0.43.0") (d (list (d (n "ckb-crypto") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.43.0") (d #t) (k 0)))) (h "1bn208phslbnnlw6v8wr5mgv5aihh4qzaqk143pb2va2ilqxy7i3")))

(define-public crate-ckb-multisig-0.100.0-rc2 (c (n "ckb-multisig") (v "0.100.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "0qnw2ngsv434fw9xdpyddp67h7n0pps9vqyrhhm33wm90hl25gfr")))

(define-public crate-ckb-multisig-0.43.2 (c (n "ckb-multisig") (v "0.43.2") (d (list (d (n "ckb-crypto") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.43.2") (d #t) (k 0)))) (h "072y9zvsy9dgg2jbhsxgvm9i22cypnywc36jn2wn2z0xs4snfbs4")))

(define-public crate-ckb-multisig-0.100.0-rc4 (c (n "ckb-multisig") (v "0.100.0-rc4") (d (list (d (n "ckb-crypto") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "0pkfkjx68b5ml9zgzwfb7qsvcnjkcs6ip3hl8y37vbgpqq3xndy6")))

(define-public crate-ckb-multisig-0.100.0-rc5 (c (n "ckb-multisig") (v "0.100.0-rc5") (d (list (d (n "ckb-crypto") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "1h5bnzifpzpirsgc8bfbshdnyaqg1cj7mnk1d9mgkxv512d0x544")))

(define-public crate-ckb-multisig-0.100.0 (c (n "ckb-multisig") (v "0.100.0") (d (list (d (n "ckb-crypto") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0") (d #t) (k 0)))) (h "01i3vbrk5p8ar1m6r37a4kpv9rwc0f6rxkh1b1q5xrw0cgbl3wfv")))

(define-public crate-ckb-multisig-0.101.0 (c (n "ckb-multisig") (v "0.101.0") (d (list (d (n "ckb-crypto") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.0") (d #t) (k 0)))) (h "1wlj2sx62jl4fz5z59lz2rayjjx46g9b0klr0lsfg8nz2l556gwq")))

(define-public crate-ckb-multisig-0.101.1 (c (n "ckb-multisig") (v "0.101.1") (d (list (d (n "ckb-crypto") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.1") (d #t) (k 0)))) (h "13jlmazw4di3q507p6q7ljc8fm9wwbi39084x3w6xy0k30a4nc7f")))

(define-public crate-ckb-multisig-0.101.2-testnet1 (c (n "ckb-multisig") (v "0.101.2-testnet1") (d (list (d (n "ckb-crypto") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "04d7mglvqmb04q6rgpq97wl4v3xcz7yrcd9f1bp381prbsvaqdal")))

(define-public crate-ckb-multisig-0.101.2 (c (n "ckb-multisig") (v "0.101.2") (d (list (d (n "ckb-crypto") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.2") (d #t) (k 0)))) (h "08ka0jmm0kgj89spia96zl9v64b0s1grzir7blp0xjzjwi0xgm4m")))

(define-public crate-ckb-multisig-0.101.3 (c (n "ckb-multisig") (v "0.101.3") (d (list (d (n "ckb-crypto") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.3") (d #t) (k 0)))) (h "1b3qlxxy293qxf5ky2gfa0jl0dmizhyxjj2cz4y6dbmw7kslfq1l")))

(define-public crate-ckb-multisig-0.101.4 (c (n "ckb-multisig") (v "0.101.4") (d (list (d (n "ckb-crypto") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.4") (d #t) (k 0)))) (h "0ylwb70r05120hrbxpssngwfm3yf9h3d4a80m3nnl9a65nk1lgvg")))

(define-public crate-ckb-multisig-0.101.5 (c (n "ckb-multisig") (v "0.101.5") (d (list (d (n "ckb-crypto") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.5") (d #t) (k 0)))) (h "00s6abbl1vhg1j7ady3afnzm5dszp9m733l1xm5lbg3dcj1ym017") (y #t)))

(define-public crate-ckb-multisig-0.101.6 (c (n "ckb-multisig") (v "0.101.6") (d (list (d (n "ckb-crypto") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.6") (d #t) (k 0)))) (h "04wpmn0shgmadcn9ispgsw7hqalh1v5qqn7jj9fm6sdd55zl9yp4")))

(define-public crate-ckb-multisig-0.101.7 (c (n "ckb-multisig") (v "0.101.7") (d (list (d (n "ckb-crypto") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.7") (d #t) (k 0)))) (h "0w0hbcm4mp4nsvlhvwpqczw7hhyja0dmyd79hcacc5y2bmb1fzvl")))

(define-public crate-ckb-multisig-0.101.8 (c (n "ckb-multisig") (v "0.101.8") (d (list (d (n "ckb-crypto") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.8") (d #t) (k 0)))) (h "1m5fwm8gqkfi7siy9jng1kdnxcyr5mkl1hngzmisqiw30h9sp8f5")))

(define-public crate-ckb-multisig-0.102.0 (c (n "ckb-multisig") (v "0.102.0") (d (list (d (n "ckb-crypto") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.102.0") (d #t) (k 0)))) (h "06d16yr56zn4m0f6lc8477wvj1nb72xj50hj5pnlmxf1fl8g7zag") (y #t)))

(define-public crate-ckb-multisig-0.103.0 (c (n "ckb-multisig") (v "0.103.0") (d (list (d (n "ckb-crypto") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.103.0") (d #t) (k 0)))) (h "0wf4v54v4pzhgzfdfdpp3kg1qrrq8pafzcgk16xhpn4n4y3yk3dg")))

(define-public crate-ckb-multisig-0.104.0 (c (n "ckb-multisig") (v "0.104.0") (d (list (d (n "ckb-crypto") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.104.0") (d #t) (k 0)))) (h "0dvaclsgm2h3z65i2ikpfwzm2zq5mbqaq11b1ym1393a7v2kglw2")))

(define-public crate-ckb-multisig-0.104.1 (c (n "ckb-multisig") (v "0.104.1") (d (list (d (n "ckb-crypto") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.104.1") (d #t) (k 0)))) (h "0cqgf9p95inx6a0ypy9gskzlfcdn9i15jds8ir53dvmlgb221b0i")))

(define-public crate-ckb-multisig-0.105.0 (c (n "ckb-multisig") (v "0.105.0") (d (list (d (n "ckb-crypto") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.105.0") (d #t) (k 0)))) (h "0gj1m3r3cv0s8njhcrbpp4vjx66cpynhd39nhd7alx7wykmhs761") (y #t)))

(define-public crate-ckb-multisig-0.105.1 (c (n "ckb-multisig") (v "0.105.1") (d (list (d (n "ckb-crypto") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.105.1") (d #t) (k 0)))) (h "001kd29c0f7r8lgngls5ajrfqzxya1fmgi1nmvhlbm1nf83lrfh6") (y #t)))

(define-public crate-ckb-multisig-0.106.0 (c (n "ckb-multisig") (v "0.106.0") (d (list (d (n "ckb-crypto") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.106.0") (d #t) (k 0)))) (h "1hg0g5yyl127jali795h3p2l8nwcqpkpqmlcvkiv1n5hgyfas34w")))

(define-public crate-ckb-multisig-0.107.0 (c (n "ckb-multisig") (v "0.107.0") (d (list (d (n "ckb-crypto") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.107.0") (d #t) (k 0)))) (h "0jyqdypn8nvrvqqr31qpzmgl4xmqcqyxbgbwwnb3146syzhzphq1")))

(define-public crate-ckb-multisig-0.108.0 (c (n "ckb-multisig") (v "0.108.0") (d (list (d (n "ckb-crypto") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.108.0") (d #t) (k 0)))) (h "190nwkqlf0mk05nb6q56jq9mkyfljy3gi09cv6h059hw1hgdr3a8")))

(define-public crate-ckb-multisig-0.108.1 (c (n "ckb-multisig") (v "0.108.1") (d (list (d (n "ckb-crypto") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.108.1") (d #t) (k 0)))) (h "1gzhspmddsmwblxrxzc6j91wk01m6hhil8cs91iz7svpz37i4cdk")))

(define-public crate-ckb-multisig-0.109.0 (c (n "ckb-multisig") (v "0.109.0") (d (list (d (n "ckb-crypto") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.109.0") (d #t) (k 0)))) (h "097a2185sgb93cxvi50ajjrw1sff228al2rrlzqg191k0zdpmbnn")))

(define-public crate-ckb-multisig-0.110.0 (c (n "ckb-multisig") (v "0.110.0") (d (list (d (n "ckb-crypto") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.0") (d #t) (k 0)))) (h "11xvsipz4gh23l77jadgc3z012v0kkjpnygy843iwi73rx34rjzb")))

(define-public crate-ckb-multisig-0.110.0-rc1 (c (n "ckb-multisig") (v "0.110.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "039vv9h24qfqrip8638q8m2rswvn4l6g53nmag1is7ni2dq7s0as")))

(define-public crate-ckb-multisig-0.111.0-rc2 (c (n "ckb-multisig") (v "0.111.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "1i93z0ix0akl0s2wi7ksvv6by5k8rj90slyvdk9iyz9s94rnly4c")))

(define-public crate-ckb-multisig-0.111.0-rc3 (c (n "ckb-multisig") (v "0.111.0-rc3") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "17204b0l0svngm6dfw1fi1qm154a6df69ssg6azbslyky53l8gv5")))

(define-public crate-ckb-multisig-0.111.0-rc4 (c (n "ckb-multisig") (v "0.111.0-rc4") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "1j7nx7bb1vqyq3hd53pnfxha8bda197malqq8z8hmqvxl4f6l071")))

(define-public crate-ckb-multisig-0.111.0-rc5 (c (n "ckb-multisig") (v "0.111.0-rc5") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "1q4yqvr4d79j3x2w5cxmzvymqw7wqv6vrxccy85np1wjsrm8dpkf")))

(define-public crate-ckb-multisig-0.111.0-rc6 (c (n "ckb-multisig") (v "0.111.0-rc6") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "0jvx4pv8i82zjb218zf4qxaw9094x89q224w3zq1mpbxppi09pxh")))

(define-public crate-ckb-multisig-0.111.0-rc7 (c (n "ckb-multisig") (v "0.111.0-rc7") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "0xpm09pdm7ff63qmdr7497xjn57q8qsag3frnqlcfd2vj9j9cxww")))

(define-public crate-ckb-multisig-0.111.0-rc8 (c (n "ckb-multisig") (v "0.111.0-rc8") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "145skcria5zlcah196i5ss1f43xjrsig4rq6ry2jv77qwxwnl15b")))

(define-public crate-ckb-multisig-0.110.1-rc1 (c (n "ckb-multisig") (v "0.110.1-rc1") (d (list (d (n "ckb-crypto") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "1ksrwabbhjgabv1sy02yp6xjlpxb741pgnq7w9y9aw17zhv7h957")))

(define-public crate-ckb-multisig-0.110.1-rc2 (c (n "ckb-multisig") (v "0.110.1-rc2") (d (list (d (n "ckb-crypto") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "15m226gpabwygk3v386dll4w9clsr3gn1fahxajdqb1aci3zqrmg")))

(define-public crate-ckb-multisig-0.111.0-rc9 (c (n "ckb-multisig") (v "0.111.0-rc9") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "0ja9ri6niv17v0gkcgd43j658igimgdgdwsjcd1d21kam5pgca99")))

(define-public crate-ckb-multisig-0.111.0-rc10 (c (n "ckb-multisig") (v "0.111.0-rc10") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "0r76h4jkznkcdkkmhgb7fjwhy4v8mpxdvijfrq148vkcamg3rh4a")))

(define-public crate-ckb-multisig-0.110.1 (c (n "ckb-multisig") (v "0.110.1") (d (list (d (n "ckb-crypto") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1") (d #t) (k 0)))) (h "142cb95j27m71nvanymaxgkfp86c5a0r62lk5fc7f36gr9vp3fzz")))

(define-public crate-ckb-multisig-0.110.2-rc1 (c (n "ckb-multisig") (v "0.110.2-rc1") (d (list (d (n "ckb-crypto") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "0vxz7af4rpa4jp57rm03453mbaqkzj6x8sknkfshl41c7hl4wi9h")))

(define-public crate-ckb-multisig-0.111.0-rc11 (c (n "ckb-multisig") (v "0.111.0-rc11") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "1zvwbbvpllcy97sm7c4224g1xvpph0mcvmhc6zwv5by5l8zg4j2s")))

(define-public crate-ckb-multisig-0.110.2-rc2 (c (n "ckb-multisig") (v "0.110.2-rc2") (d (list (d (n "ckb-crypto") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "1zdslfj0nkdpj02pqjxzb48895xjqd9s0s2na7rrfp91iyqpppi0")))

(define-public crate-ckb-multisig-0.111.0-rc12 (c (n "ckb-multisig") (v "0.111.0-rc12") (d (list (d (n "ckb-crypto") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "1wzxkgvcd22p8d0vlrsljfvgqrz3b668cv2m5cwswhffd2xhjk7w")))

(define-public crate-ckb-multisig-0.110.2 (c (n "ckb-multisig") (v "0.110.2") (d (list (d (n "ckb-crypto") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2") (d #t) (k 0)))) (h "1wzq906f182r7vnyykvj70qxg9d6phjyalqfasmqk2v8xkv4sjaz")))

(define-public crate-ckb-multisig-0.111.0 (c (n "ckb-multisig") (v "0.111.0") (d (list (d (n "ckb-crypto") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0") (d #t) (k 0)))) (h "0vimgnywd7d8zvdgfsdj8n7laq7pmsj2l1qp00faqnz3g7gnc3bh")))

(define-public crate-ckb-multisig-0.112.0-rc1 (c (n "ckb-multisig") (v "0.112.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "0yaq9gyxgkm9yrspsyjjjlbng8ds1bm3ifrr0vgmphy1nyvm051g")))

(define-public crate-ckb-multisig-0.112.0-rc2 (c (n "ckb-multisig") (v "0.112.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "0iy3325sf4rwm463dbmlafd3ap9kiray3lv26rvdbivlahxrfr9k")))

(define-public crate-ckb-multisig-0.112.0 (c (n "ckb-multisig") (v "0.112.0") (d (list (d (n "ckb-crypto") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0") (d #t) (k 0)))) (h "0c237np8nh326dvzvh590gmbl2dlqf8c5c9ibyrgfgzsfz6imaw9")))

(define-public crate-ckb-multisig-0.112.0-rc3 (c (n "ckb-multisig") (v "0.112.0-rc3") (d (list (d (n "ckb-crypto") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "1xpv7jglib560ynzwa0pj3jmmcxbpapbjn7jf247xhiply84lz57")))

(define-public crate-ckb-multisig-0.112.1 (c (n "ckb-multisig") (v "0.112.1") (d (list (d (n "ckb-crypto") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.1") (d #t) (k 0)))) (h "1k220mhbf2l01l31l77j69jf293xv9xz29qznv266z338qc2xyv5")))

(define-public crate-ckb-multisig-0.113.0-rc1 (c (n "ckb-multisig") (v "0.113.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "1dgvzri47diplni41wy5z9nd81ybd05xlwgdq3p6izmljxd7bfi7")))

(define-public crate-ckb-multisig-0.113.0-rc2 (c (n "ckb-multisig") (v "0.113.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "0xw1hgdfhny2pj0yrzswhk866akxzccbx052gfvx71rk50s0kip1")))

(define-public crate-ckb-multisig-0.113.0-rc3 (c (n "ckb-multisig") (v "0.113.0-rc3") (d (list (d (n "ckb-crypto") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "17bmag8bzmy4j9k4i1w03n5f46ni4q5zdpp280nny44g6p6kcnl2")))

(define-public crate-ckb-multisig-0.113.0 (c (n "ckb-multisig") (v "0.113.0") (d (list (d (n "ckb-crypto") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0") (d #t) (k 0)))) (h "094h6fzsmds60dls6pn2zvqprxnf21xgrzw9yv5f6fymym7clly7")))

(define-public crate-ckb-multisig-0.113.1-rc1 (c (n "ckb-multisig") (v "0.113.1-rc1") (d (list (d (n "ckb-crypto") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "0a7i5bq9rghw3m0d68z3r4rmvfiacwnv2kwg9xv4k3hh3rdvv6dc")))

(define-public crate-ckb-multisig-0.114.0-rc1 (c (n "ckb-multisig") (v "0.114.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "0d0yqx33hs2a7z7l46f6n0z0ll4a2kvkg1c2jphaa9c9cxjydds9")))

(define-public crate-ckb-multisig-0.113.1-rc2 (c (n "ckb-multisig") (v "0.113.1-rc2") (d (list (d (n "ckb-crypto") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "0kh7qnp3lhvix07xhswh3liyqj5b07qsh47rra61x79iws3kz2jv")))

(define-public crate-ckb-multisig-0.113.1 (c (n "ckb-multisig") (v "0.113.1") (d (list (d (n "ckb-crypto") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1") (d #t) (k 0)))) (h "1chkvli5waaj1g6712kyhb77lypdp20jp48qyv178wh88z8299xx")))

(define-public crate-ckb-multisig-0.114.0-rc2 (c (n "ckb-multisig") (v "0.114.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "0f7lm9igwb1ndy2jdnqac0idxx40vrnjnq6cqd211vd305csc4ab")))

(define-public crate-ckb-multisig-0.114.0-rc3 (c (n "ckb-multisig") (v "0.114.0-rc3") (d (list (d (n "ckb-crypto") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "1g9bv8kvxaar4551m4njlq47d4612x2jgv997rinhsxkkq6k7rmy")))

(define-public crate-ckb-multisig-0.114.0 (c (n "ckb-multisig") (v "0.114.0") (d (list (d (n "ckb-crypto") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0") (d #t) (k 0)))) (h "0lgvgi7b70da871rkmy7bixls82zm2s2prm52907fzk8lz5hx7d3")))

(define-public crate-ckb-multisig-0.115.0-pre (c (n "ckb-multisig") (v "0.115.0-pre") (d (list (d (n "ckb-crypto") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-pre") (d #t) (k 0)))) (h "00038vg9shcgx8jmbmpj1d0hs52946209c3859hciasbk5w194xg")))

(define-public crate-ckb-multisig-0.115.0-rc1 (c (n "ckb-multisig") (v "0.115.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "1aj1k4klb7xf4kfj2sc9kjxs49wqcn7y2n0hasj5x7ka6djgg5c9")))

(define-public crate-ckb-multisig-0.115.0-rc2 (c (n "ckb-multisig") (v "0.115.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "0w2m3byif0y8pjgvnrsvhmgias8qk76vzs5pq2jpmi4iwp9rdff4")))

(define-public crate-ckb-multisig-0.115.0 (c (n "ckb-multisig") (v "0.115.0") (d (list (d (n "ckb-crypto") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0") (d #t) (k 0)))) (h "1kcgvcmqvaqcj2d7y028ygbpn0y22g9xgysixmbg5x06q4jn8mrp")))

(define-public crate-ckb-multisig-0.116.0-rc1 (c (n "ckb-multisig") (v "0.116.0-rc1") (d (list (d (n "ckb-crypto") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0zahdv03kmhg9vlj2s5vc0dj6bfrpdr79igmgp2p8dqxmb8p92rv")))

(define-public crate-ckb-multisig-0.116.0-rc2 (c (n "ckb-multisig") (v "0.116.0-rc2") (d (list (d (n "ckb-crypto") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "1sbd4xa7vc7qxy3gpdglcgmvwwngilwk81yk8sa6l879nwxa2vkp")))

(define-public crate-ckb-multisig-0.116.0 (c (n "ckb-multisig") (v "0.116.0") (d (list (d (n "ckb-crypto") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0") (d #t) (k 0)))) (h "1xrf7dxypiq1n3xwn7whfsfr9qc52b3zx305rb307qzxffjs1vk2") (y #t)))

(define-public crate-ckb-multisig-0.116.1 (c (n "ckb-multisig") (v "0.116.1") (d (list (d (n "ckb-crypto") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.1") (d #t) (k 0)))) (h "1xs973aaj88yvfcyhrvkds4xlrlr98crpjpv28fyibvwchp16pnw")))

