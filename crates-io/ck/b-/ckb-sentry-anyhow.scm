(define-module (crates-io ck b- ckb-sentry-anyhow) #:use-module (crates-io))

(define-public crate-ckb-sentry-anyhow-0.21.0 (c (n "ckb-sentry-anyhow") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "0wf52g96g76x3qx8cbyb66azx7v98dnk5ayagyqr23x4kypkhsqy")))

