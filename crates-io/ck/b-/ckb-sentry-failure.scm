(define-module (crates-io ck b- ckb-sentry-failure) #:use-module (crates-io))

(define-public crate-ckb-sentry-failure-0.21.0 (c (n "ckb-sentry-failure") (v "0.21.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-backtrace")) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "1qhs53xn6fvl8spwqwkjins0c7gknfzqdaad82ia78rrg2m3r319")))

