(define-module (crates-io ck b- ckb-occupied-capacity) #:use-module (crates-io))

(define-public crate-ckb-occupied-capacity-0.37.0-pre (c (n "ckb-occupied-capacity") (v "0.37.0-pre") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.37.0-pre") (d #t) (k 0)))) (h "1nwq8zllkh1bf8sha4lc3kqz207ixlnfb9x6hhza8bgqc2w73zp5")))

(define-public crate-ckb-occupied-capacity-0.37.0 (c (n "ckb-occupied-capacity") (v "0.37.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.37.0") (d #t) (k 0)))) (h "0hs37vdzknaw2cyl75l8z279xfkpsm52cmmix097kp35iz3cv969")))

(define-public crate-ckb-occupied-capacity-0.38.0 (c (n "ckb-occupied-capacity") (v "0.38.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.38.0") (d #t) (k 0)))) (h "0vz4gql8h0cplachr43zq6nlix9wrvkyk4vdv6m90qfbm96z6l6x")))

(define-public crate-ckb-occupied-capacity-0.39.0 (c (n "ckb-occupied-capacity") (v "0.39.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.39.0") (d #t) (k 0)))) (h "0myql90a3il41nvvp7dzs5ln0pipqgi89x8lgvdy2fz0hsz4jq4d")))

(define-public crate-ckb-occupied-capacity-0.39.1 (c (n "ckb-occupied-capacity") (v "0.39.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.39.1") (d #t) (k 0)))) (h "1g73yrq59zhmmp2h1y073qcc5abwgbrbkq57lwr8vgd9p3bn70fs")))

(define-public crate-ckb-occupied-capacity-0.40.0 (c (n "ckb-occupied-capacity") (v "0.40.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.40.0") (d #t) (k 0)))) (h "1p3yw9hirwwmmsz8294dlqzdzwjaki0as0dsk6bn2h1w34fsypiy")))

(define-public crate-ckb-occupied-capacity-0.42.0 (c (n "ckb-occupied-capacity") (v "0.42.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.42.0") (d #t) (k 0)))) (h "1kffqffrmammqrbjb6sq7incidsmpsfq3mpnx0gsg4hwv88z1inh")))

(define-public crate-ckb-occupied-capacity-0.43.0 (c (n "ckb-occupied-capacity") (v "0.43.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.43.0") (d #t) (k 0)))) (h "0dj6p36kz92lrvap7qw4hpfjlqnhs9rh7a8w36j7bql3sy476ymy")))

(define-public crate-ckb-occupied-capacity-0.100.0-rc2 (c (n "ckb-occupied-capacity") (v "0.100.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "1nnd9vdcwv2abwq0rpsjqhakgj9xghrs13svjvcl34ljkrz8fhla")))

(define-public crate-ckb-occupied-capacity-0.43.2 (c (n "ckb-occupied-capacity") (v "0.43.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.43.2") (d #t) (k 0)))) (h "0h15bqda7czr8qy3d81pdk4s90nvri265802fpbhzzx02xar8ww6")))

(define-public crate-ckb-occupied-capacity-0.100.0-rc4 (c (n "ckb-occupied-capacity") (v "0.100.0-rc4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "14whihpa4acgyv4mxw6940r7a3h9vzq4fp534ch7k3g5g2gjvfix")))

(define-public crate-ckb-occupied-capacity-0.100.0-rc5 (c (n "ckb-occupied-capacity") (v "0.100.0-rc5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "0d37xd9csy2rgsgj98fli4wz82gcbail5kvg4g6257ckid0y7fgf")))

(define-public crate-ckb-occupied-capacity-0.100.0 (c (n "ckb-occupied-capacity") (v "0.100.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.100.0") (d #t) (k 0)))) (h "0hy9ngzia84lkyvgvaylh2dibmr6mg95sd77jsi4b7g9d1im214h")))

(define-public crate-ckb-occupied-capacity-0.101.0 (c (n "ckb-occupied-capacity") (v "0.101.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.0") (d #t) (k 0)))) (h "0i1krdvcq620brny5bwvvwixagig2sj3n35r44vingyv1j8g0zbk")))

(define-public crate-ckb-occupied-capacity-0.101.1 (c (n "ckb-occupied-capacity") (v "0.101.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.1") (d #t) (k 0)))) (h "1n0p1dfkbx9dhmmzhljimwhanpnxpr909y39325mhbkqajj1b3fq")))

(define-public crate-ckb-occupied-capacity-0.101.2-testnet1 (c (n "ckb-occupied-capacity") (v "0.101.2-testnet1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "1mhyifigv4k1mk0rk7b924c86iai8nl9g9y11fqpmwdg3g226db4")))

(define-public crate-ckb-occupied-capacity-0.101.2 (c (n "ckb-occupied-capacity") (v "0.101.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.2") (d #t) (k 0)))) (h "1cdr7hmf1h0ka9qnvmdlw3hqy03hb2by8gjr97s4lsca6ls6fhf5")))

(define-public crate-ckb-occupied-capacity-0.101.3 (c (n "ckb-occupied-capacity") (v "0.101.3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.3") (d #t) (k 0)))) (h "15995ayak4jbcvdd5jpfl7qlk0nwnc6shlqs3bxkb3m16d7x0y4f")))

(define-public crate-ckb-occupied-capacity-0.101.4 (c (n "ckb-occupied-capacity") (v "0.101.4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.4") (d #t) (k 0)))) (h "09m3hhr1bzx43bq063b4gqzalbhgky6yvbjxkxvy76bdsc0ljy72")))

(define-public crate-ckb-occupied-capacity-0.101.5 (c (n "ckb-occupied-capacity") (v "0.101.5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.5") (d #t) (k 0)))) (h "0p1y8g9hk9dz7fb44fkkvn8gp90497kphmhbmdqfhdn6ia2bqvb3") (y #t)))

(define-public crate-ckb-occupied-capacity-0.101.6 (c (n "ckb-occupied-capacity") (v "0.101.6") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.6") (d #t) (k 0)))) (h "1i6kxx9v754r54ibvz73px2dfs6wmz77sgrr4vk223g4l4d18vsr")))

(define-public crate-ckb-occupied-capacity-0.101.7 (c (n "ckb-occupied-capacity") (v "0.101.7") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.7") (d #t) (k 0)))) (h "0bqa0fqcjv66hyxi73hywv079npla0hmj6fkrn7bk8vwlilc6mj8")))

(define-public crate-ckb-occupied-capacity-0.101.8 (c (n "ckb-occupied-capacity") (v "0.101.8") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.101.8") (d #t) (k 0)))) (h "1z7dh64p5ikmhn97abxrzh5cx9p8lgzqfmgaml23c26wfyybmpw0")))

(define-public crate-ckb-occupied-capacity-0.102.0 (c (n "ckb-occupied-capacity") (v "0.102.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.102.0") (d #t) (k 0)))) (h "0jn1gllr8pabfmxjmyf73v9s0g5a340rj864d8rvq5wd2xgi7jx3") (y #t)))

(define-public crate-ckb-occupied-capacity-0.103.0 (c (n "ckb-occupied-capacity") (v "0.103.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.103.0") (d #t) (k 0)))) (h "1gz118pih4zw7r70m5b54l6ri36rrgkvh8r4ipq5l9zwrkhab6hc")))

(define-public crate-ckb-occupied-capacity-0.104.0 (c (n "ckb-occupied-capacity") (v "0.104.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.104.0") (d #t) (k 0)))) (h "08apjdzdxnm7wrlaaax7g7xb1clga5vxn2csb0dpkbxnqb5lq3ig")))

(define-public crate-ckb-occupied-capacity-0.104.1 (c (n "ckb-occupied-capacity") (v "0.104.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.104.1") (d #t) (k 0)))) (h "1wdplvispvjb25c9jb01b5p7cm33hj32np4xgqgyrmv2wkyx2mik")))

(define-public crate-ckb-occupied-capacity-0.105.0 (c (n "ckb-occupied-capacity") (v "0.105.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.105.0") (d #t) (k 0)))) (h "1zi1nlja8i3ng2821lvinp7jwl7a3198l0cxpnmhb8g8pq6gzp06") (y #t)))

(define-public crate-ckb-occupied-capacity-0.105.1 (c (n "ckb-occupied-capacity") (v "0.105.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.105.1") (d #t) (k 0)))) (h "1ikidrfa6mi82drn2hhkj4g50h4cfdf6ayaa0wsbg62a0c6027m1") (y #t)))

(define-public crate-ckb-occupied-capacity-0.106.0 (c (n "ckb-occupied-capacity") (v "0.106.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.106.0") (d #t) (k 0)))) (h "1aa6w81rq2f3g1zyhjncrsp55n5h47picvqya5jpy6qvsm3c3xll")))

(define-public crate-ckb-occupied-capacity-0.107.0 (c (n "ckb-occupied-capacity") (v "0.107.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.107.0") (d #t) (k 0)))) (h "17wf2gr370h4akp3vnjpcn8dvqdbwsp4z80bkv2c1lyci6mxnb1b")))

(define-public crate-ckb-occupied-capacity-0.108.0 (c (n "ckb-occupied-capacity") (v "0.108.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.108.0") (d #t) (k 0)))) (h "1sslcxidlnz04d1pkdknvcpg884b2k0kgm1h3sxaypdssk81said")))

(define-public crate-ckb-occupied-capacity-0.108.1 (c (n "ckb-occupied-capacity") (v "0.108.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.108.1") (d #t) (k 0)))) (h "13wf7p2bhkmjz5j7bs6hng4idbwdl7hvpjabskbclqx7kzbzgcw3")))

(define-public crate-ckb-occupied-capacity-0.109.0 (c (n "ckb-occupied-capacity") (v "0.109.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.109.0") (d #t) (k 0)))) (h "1apj6qm9x4pnn4msgxc0r4yn3d9w2p3f7gdfng7hm6lqqkwjmkg3")))

(define-public crate-ckb-occupied-capacity-0.110.0 (c (n "ckb-occupied-capacity") (v "0.110.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.0") (d #t) (k 0)))) (h "0y9dla746g29p0bm6li0xqbc949v3vwpwag1zzsw9cimis4dirnj")))

(define-public crate-ckb-occupied-capacity-0.110.0-rc1 (c (n "ckb-occupied-capacity") (v "0.110.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "0nqmm521msicc47wk8d0ppn2wl7zmk69hl2knlfm1rwz3cq3q29y")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc2 (c (n "ckb-occupied-capacity") (v "0.111.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "0gf0z2g6bvn58yryrcw45ny9zz1rrk4bwhjrj1z5z5c9r86ps07w")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc3 (c (n "ckb-occupied-capacity") (v "0.111.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "1g0g9g0vqj3lz5i2msa0xin1dnlrrn1sr0xj08y2mxh9qxa796if")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc4 (c (n "ckb-occupied-capacity") (v "0.111.0-rc4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "0wzh6zxkyzzbqp7m78nvw2gwpfihqras09rkdq4a4fcqw27a1lr1")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc5 (c (n "ckb-occupied-capacity") (v "0.111.0-rc5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "00ixna21rgsjz7x3d4gqsjgglkgjhrr4pysra1znlfhdd4hkhn6x")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc6 (c (n "ckb-occupied-capacity") (v "0.111.0-rc6") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "0pmisx50n18kipyfgwp3qnrskgl7w1gi2w3rvvhl9ma1bd316x8f")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc7 (c (n "ckb-occupied-capacity") (v "0.111.0-rc7") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "15ffy3sb8q9pgdxbr28q2j703ncyjcbicw6anv4ddjlmvi5shg0k")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc8 (c (n "ckb-occupied-capacity") (v "0.111.0-rc8") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "1ny8gyf3fj8jhvi88cailm1xb1qpnb9pj8iqvp1xnddv87861s80")))

(define-public crate-ckb-occupied-capacity-0.110.1-rc1 (c (n "ckb-occupied-capacity") (v "0.110.1-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "0b06drrc4jba05sy66as4hxxd38bkz63ng2j1zcp0aj30z4grdcv")))

(define-public crate-ckb-occupied-capacity-0.110.1-rc2 (c (n "ckb-occupied-capacity") (v "0.110.1-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "0b6qfcjq2q5r5wr66sbis77jbl7y6cws17hrpa1qgsc0lg27l8qx")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc9 (c (n "ckb-occupied-capacity") (v "0.111.0-rc9") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "01cyy0w0l5jymqbin4m96g90vgzwwfyzb4hv9ij7qb8c9zafxwsp")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc10 (c (n "ckb-occupied-capacity") (v "0.111.0-rc10") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "1sxrw5i9glmbg1yk1lgxvzycv41p5npqj444blxfkl4h83vwynik")))

(define-public crate-ckb-occupied-capacity-0.110.1 (c (n "ckb-occupied-capacity") (v "0.110.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.1") (d #t) (k 0)))) (h "13acgrbwpr8nlx6vn2x1f4yjjsgb8myspqhxxa3gn3v52dxf2j3m")))

(define-public crate-ckb-occupied-capacity-0.110.2-rc1 (c (n "ckb-occupied-capacity") (v "0.110.2-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "13d69jfpv5ws1h1psaggn94q0kfnz63kv1zfn1n66k5g30jr6y24")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc11 (c (n "ckb-occupied-capacity") (v "0.111.0-rc11") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "1m799dkigxd59wl23n9012773rmi8gkz790jyjwdqrawhdjnbdw5")))

(define-public crate-ckb-occupied-capacity-0.110.2-rc2 (c (n "ckb-occupied-capacity") (v "0.110.2-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "0b1g47ix6lb6nlig1c3c3ljy2gy3l9ddl8r6s6hjb9sk8ka4kiyz")))

(define-public crate-ckb-occupied-capacity-0.111.0-rc12 (c (n "ckb-occupied-capacity") (v "0.111.0-rc12") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "0zzqcavxdcwrkywahribslpv50bkr4v0nlq7cgblmkgmcwscghrc")))

(define-public crate-ckb-occupied-capacity-0.110.2 (c (n "ckb-occupied-capacity") (v "0.110.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.110.2") (d #t) (k 0)))) (h "0aj8nd5fps8nirr41kh8y4p51q9ighnqid748bv3f1yv0x4si68k")))

(define-public crate-ckb-occupied-capacity-0.111.0 (c (n "ckb-occupied-capacity") (v "0.111.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.111.0") (d #t) (k 0)))) (h "1z4r3391q8ymvmjrsy42hca5lwx85fai4b34fnakanjs8rjd72im")))

(define-public crate-ckb-occupied-capacity-0.112.0-rc1 (c (n "ckb-occupied-capacity") (v "0.112.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "1srr54x742rxyhyzhg2sqnc2j6ww6cs20ggf9nrvb5qfhdhk1yaq")))

(define-public crate-ckb-occupied-capacity-0.112.0-rc2 (c (n "ckb-occupied-capacity") (v "0.112.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "1bpr4bjvjz9qaa180ikz1xb5ji0p76d9d0nwbzy60zlmgf1m0sbs")))

(define-public crate-ckb-occupied-capacity-0.112.0 (c (n "ckb-occupied-capacity") (v "0.112.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.112.0") (d #t) (k 0)))) (h "19igy82h9rvj2r2njfxv86mxv9vl2qc34rab9j9qdn00r4gmn7g3")))

(define-public crate-ckb-occupied-capacity-0.112.0-rc3 (c (n "ckb-occupied-capacity") (v "0.112.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "0i1c5wnbbfkj95ags0f9qzr2a1i77mvyjl5pfczmwclgibl3dk0i")))

(define-public crate-ckb-occupied-capacity-0.112.1 (c (n "ckb-occupied-capacity") (v "0.112.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.112.1") (d #t) (k 0)))) (h "07j68waari2f8k976h95v709skvp9vxql8947ddlk80j5wv34wdy")))

(define-public crate-ckb-occupied-capacity-0.113.0-rc1 (c (n "ckb-occupied-capacity") (v "0.113.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "0vav8byp7k1sy099kldk56qz5cn9wkia72s7k4l1r9dbjbbqvls8")))

(define-public crate-ckb-occupied-capacity-0.113.0-rc2 (c (n "ckb-occupied-capacity") (v "0.113.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "1vfbqmh947kp4dmnqjxy7blsdrwr2j7fvajk09cgv6zr37n96m6y")))

(define-public crate-ckb-occupied-capacity-0.113.0-rc3 (c (n "ckb-occupied-capacity") (v "0.113.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "0fg4cbwyc8abilrbnqvzyh6w9ndkxm3hwlqf02g6b6zk462qmnlr")))

(define-public crate-ckb-occupied-capacity-0.113.0 (c (n "ckb-occupied-capacity") (v "0.113.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.0") (d #t) (k 0)))) (h "1bgck2dngkadlqwf7if30ismchxv7slass8kw0dcfsbq0k07irmz")))

(define-public crate-ckb-occupied-capacity-0.113.1-rc1 (c (n "ckb-occupied-capacity") (v "0.113.1-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "1sj7n4jmxg7lb7s17f5awm03vzfhnic75bppbip7mq2v8xar6dpj")))

(define-public crate-ckb-occupied-capacity-0.114.0-rc1 (c (n "ckb-occupied-capacity") (v "0.114.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "1ng9r42dqjakf6a4r2cgg71jhan90s4j80f3jdwbfvsjnhkqmgbm")))

(define-public crate-ckb-occupied-capacity-0.113.1-rc2 (c (n "ckb-occupied-capacity") (v "0.113.1-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "1sd684y6snmyd2pin3bc7p52a4ndfwhqkc8hnsxqsj59rdsbmmv9")))

(define-public crate-ckb-occupied-capacity-0.113.1 (c (n "ckb-occupied-capacity") (v "0.113.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.113.1") (d #t) (k 0)))) (h "0n0q8cv618yc1h5bsh0y0ph2l01f2inn4pmw7i43g7khd22m3viv")))

(define-public crate-ckb-occupied-capacity-0.114.0-rc2 (c (n "ckb-occupied-capacity") (v "0.114.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "053pfgzlmh90prg0q12g5wh8v9a19ic074vx45ym50rhxwa03pfs")))

(define-public crate-ckb-occupied-capacity-0.114.0-rc3 (c (n "ckb-occupied-capacity") (v "0.114.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "0wk5i6vr5m3082iv53d2cglvax6khb7hqfwygc3pwhk6spi4al47")))

(define-public crate-ckb-occupied-capacity-0.114.0 (c (n "ckb-occupied-capacity") (v "0.114.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.114.0") (d #t) (k 0)))) (h "0ysg5gms3v229r0i6afgc76fss3w84a7vsrsmqv8df93fy6dr2na")))

(define-public crate-ckb-occupied-capacity-0.115.0-pre (c (n "ckb-occupied-capacity") (v "0.115.0-pre") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.115.0-pre") (d #t) (k 0)))) (h "0jmcnjg1rcdy9fdw7s3895k2hmfb793far85d1bgpfm1z8qwawf6")))

(define-public crate-ckb-occupied-capacity-0.115.0-rc1 (c (n "ckb-occupied-capacity") (v "0.115.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "1m74cr2vjp0753d8wfskhhq16jbyx07zqpwpbfr5grgbdv5rfk38")))

(define-public crate-ckb-occupied-capacity-0.115.0-rc2 (c (n "ckb-occupied-capacity") (v "0.115.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "141s3q3lc2j1hgjwj47ps492gz7zgd8gmwqhbj30nggi866f1kqf")))

(define-public crate-ckb-occupied-capacity-0.115.0 (c (n "ckb-occupied-capacity") (v "0.115.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.115.0") (d #t) (k 0)))) (h "04a9ncvp6j2x2jsc1c5a7v145s6khvig2f9v92r4nvw2d1akcd20")))

(define-public crate-ckb-occupied-capacity-0.116.0-rc1 (c (n "ckb-occupied-capacity") (v "0.116.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0500zr5fdzfsa4fwq4c1yxk6k8iahbdbswam90s6viz1plhqfm7v")))

(define-public crate-ckb-occupied-capacity-0.116.0-rc2 (c (n "ckb-occupied-capacity") (v "0.116.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "0pwswfbab6nppj8pww8wd8mifwjlq39whnhgixdwv463bn72iq62")))

(define-public crate-ckb-occupied-capacity-0.116.0 (c (n "ckb-occupied-capacity") (v "0.116.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.116.0") (d #t) (k 0)))) (h "1d5b20fv943n9dl5fw2rfqh92r7f918690acysd2bwpnw37bz14j") (y #t)))

(define-public crate-ckb-occupied-capacity-0.116.1 (c (n "ckb-occupied-capacity") (v "0.116.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-occupied-capacity-macros") (r "=0.116.1") (d #t) (k 0)))) (h "13bxxrmi10l916yvp5l9wyjabyql0lawxhg4rwaqvhyfyxxa0jpf")))

