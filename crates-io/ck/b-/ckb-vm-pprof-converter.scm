(define-module (crates-io ck b- ckb-vm-pprof-converter) #:use-module (crates-io))

(define-public crate-ckb-vm-pprof-converter-0.112.1 (c (n "ckb-vm-pprof-converter") (v "0.112.1") (d (list (d (n "ckb-vm-pprof-protos") (r "^0.112.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)))) (h "05jz2pd7dj15i4ywbk31yx03s172jcrnfcxq4hjzq5jrmlh54pdw")))

(define-public crate-ckb-vm-pprof-converter-0.113.0 (c (n "ckb-vm-pprof-converter") (v "0.113.0") (d (list (d (n "ckb-vm-pprof-protos") (r "^0.113.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)))) (h "0ninnv3zjf5wczg4y7fv5cxfjr07mkccbd95ih6hjlmqh5midnr5")))

(define-public crate-ckb-vm-pprof-converter-0.114.0 (c (n "ckb-vm-pprof-converter") (v "0.114.0") (d (list (d (n "ckb-vm-pprof-protos") (r "^0.114.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)))) (h "0zvqgzbf1ngd61jrb59q4l89fypf6x1aiznkivdydp9wm64n6hmh")))

(define-public crate-ckb-vm-pprof-converter-0.115.0-rc2 (c (n "ckb-vm-pprof-converter") (v "0.115.0-rc2") (d (list (d (n "ckb-vm-pprof-protos") (r "^0.115.0-rc2") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)))) (h "0pwkqwa7q73agibgg6mnm4gs8a7hi5j66h8slc9iqkidcsqlazp2")))

(define-public crate-ckb-vm-pprof-converter-0.116.1 (c (n "ckb-vm-pprof-converter") (v "0.116.1") (d (list (d (n "ckb-vm-pprof-protos") (r "^0.116.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)))) (h "1x6brznpsrlkjg3h69gc0g5cnzxzwvxfp5a6afajnd8lln7arv2r")))

