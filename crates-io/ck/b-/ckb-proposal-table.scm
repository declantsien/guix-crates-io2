(define-module (crates-io ck b- ckb-proposal-table) #:use-module (crates-io))

(define-public crate-ckb-proposal-table-0.37.0-pre (c (n "ckb-proposal-table") (v "0.37.0-pre") (d (list (d (n "ckb-chain-spec") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0-pre") (d #t) (k 0)))) (h "17f2flhppshirv1rqyfpw1g8k5ncbm9cbb870sysw95knkbs3xjv")))

(define-public crate-ckb-proposal-table-0.37.0 (c (n "ckb-proposal-table") (v "0.37.0") (d (list (d (n "ckb-chain-spec") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0") (d #t) (k 0)))) (h "1qa8dcpkm0scibj0zmwm577z148jjb0l5fb5mhqp1myc3da744k6")))

(define-public crate-ckb-proposal-table-0.38.0 (c (n "ckb-proposal-table") (v "0.38.0") (d (list (d (n "ckb-chain-spec") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.38.0") (d #t) (k 0)))) (h "1pmns82fpf69zpkxak91x58splzqfymvikvixmpy08fspbj6lg55")))

(define-public crate-ckb-proposal-table-0.39.0 (c (n "ckb-proposal-table") (v "0.39.0") (d (list (d (n "ckb-chain-spec") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.0") (d #t) (k 0)))) (h "11cndwkw9wmnyls7j8axikbabmcrazpbb1473dzrgw9jd6q9i6zs")))

(define-public crate-ckb-proposal-table-0.39.1 (c (n "ckb-proposal-table") (v "0.39.1") (d (list (d (n "ckb-chain-spec") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.1") (d #t) (k 0)))) (h "0yvqbk287zdlkxkq8c4sahrz9gnffwkgak37wvxcqs5iqrkn5rrh")))

(define-public crate-ckb-proposal-table-0.40.0 (c (n "ckb-proposal-table") (v "0.40.0") (d (list (d (n "ckb-chain-spec") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.40.0") (d #t) (k 0)))) (h "04xk5qnv1rs65ail1zsv0jrqav9dqnj7fwym8m73118yqymdphy7")))

(define-public crate-ckb-proposal-table-0.42.0 (c (n "ckb-proposal-table") (v "0.42.0") (d (list (d (n "ckb-chain-spec") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.42.0") (d #t) (k 0)))) (h "10py14wp27l7d1vxibys5c9yic6jkhg63n1yf9mxfr8zfg428yc4")))

(define-public crate-ckb-proposal-table-0.43.0 (c (n "ckb-proposal-table") (v "0.43.0") (d (list (d (n "ckb-chain-spec") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.43.0") (d #t) (k 0)))) (h "1wfrqvdfv651c62fbda8j6i76j89g78cx8fr2g2frrf593190l9c")))

(define-public crate-ckb-proposal-table-0.100.0-rc2 (c (n "ckb-proposal-table") (v "0.100.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "1dsqdfj338h5r3vpga9ck2a0lnyv6hr59wsffidxqq0s3s13qpad")))

(define-public crate-ckb-proposal-table-0.43.2 (c (n "ckb-proposal-table") (v "0.43.2") (d (list (d (n "ckb-chain-spec") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.43.2") (d #t) (k 0)))) (h "1jpdvb401ky8llfahapd90drygj5vlwspsx7jsyc92i7b9wbm3ba")))

(define-public crate-ckb-proposal-table-0.100.0-rc4 (c (n "ckb-proposal-table") (v "0.100.0-rc4") (d (list (d (n "ckb-chain-spec") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "1qn6ynn47lsxflwr1nfds0qk8314scf0k7bjdijqv4i099j02zwn")))

(define-public crate-ckb-proposal-table-0.100.0-rc5 (c (n "ckb-proposal-table") (v "0.100.0-rc5") (d (list (d (n "ckb-chain-spec") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "1r791dcydpnffjaryvq4229nj2h58xph50gd60gfghcxkv7ish16")))

(define-public crate-ckb-proposal-table-0.100.0 (c (n "ckb-proposal-table") (v "0.100.0") (d (list (d (n "ckb-chain-spec") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.100.0") (d #t) (k 0)))) (h "0szzadl915dhm3yd7awbln58c19x4ml5nxywm9skj3vgp23vswck")))

(define-public crate-ckb-proposal-table-0.101.0 (c (n "ckb-proposal-table") (v "0.101.0") (d (list (d (n "ckb-chain-spec") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.0") (d #t) (k 0)))) (h "1rmhyhq55giq7wi828bw1s1w4i77h63zzjm0yb66kxbhpzdv761c")))

(define-public crate-ckb-proposal-table-0.101.1 (c (n "ckb-proposal-table") (v "0.101.1") (d (list (d (n "ckb-chain-spec") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.1") (d #t) (k 0)))) (h "1sksriyml6na0gj34gd7sngv6faxqhpb66ybjm55k9rb2nvvfjjz")))

(define-public crate-ckb-proposal-table-0.101.2-testnet1 (c (n "ckb-proposal-table") (v "0.101.2-testnet1") (d (list (d (n "ckb-chain-spec") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "0qb4gwfkdxa0dg24c006g8r9xc5hf7imilf6f0z2xrf9h4p90rwg")))

(define-public crate-ckb-proposal-table-0.101.2 (c (n "ckb-proposal-table") (v "0.101.2") (d (list (d (n "ckb-chain-spec") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.2") (d #t) (k 0)))) (h "0bng00hrry29l1v2yclbl8jrv6kfmd8yj7jl7d1zc7d2qvcgdwi3")))

(define-public crate-ckb-proposal-table-0.101.3 (c (n "ckb-proposal-table") (v "0.101.3") (d (list (d (n "ckb-chain-spec") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.3") (d #t) (k 0)))) (h "1xlsfgana07gx73wd4rhhqy9g3d5ccayyddsnssfgf218dnryi76")))

(define-public crate-ckb-proposal-table-0.101.4 (c (n "ckb-proposal-table") (v "0.101.4") (d (list (d (n "ckb-chain-spec") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.4") (d #t) (k 0)))) (h "1db08zbj452gsjwdk59ssm74wcb9zykvr4y6aywalk15cd1l1llr")))

(define-public crate-ckb-proposal-table-0.101.5 (c (n "ckb-proposal-table") (v "0.101.5") (d (list (d (n "ckb-chain-spec") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.5") (d #t) (k 0)))) (h "0b70j1bd93jphsvlps2plm05rz4553dcw3i7kwpmxv85f0fp3vp9") (y #t)))

(define-public crate-ckb-proposal-table-0.101.6 (c (n "ckb-proposal-table") (v "0.101.6") (d (list (d (n "ckb-chain-spec") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.6") (d #t) (k 0)))) (h "0nfcsc92l1yllzq42fli70010a3dvs4mpwg1xaxrvznx4lks927m")))

(define-public crate-ckb-proposal-table-0.101.7 (c (n "ckb-proposal-table") (v "0.101.7") (d (list (d (n "ckb-chain-spec") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.7") (d #t) (k 0)))) (h "0mg6ks0vivy1blkjs2l83vsqihp4324dw4n7wq1m85lcp33jgqp4")))

(define-public crate-ckb-proposal-table-0.101.8 (c (n "ckb-proposal-table") (v "0.101.8") (d (list (d (n "ckb-chain-spec") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-types") (r "=0.101.8") (d #t) (k 0)))) (h "0zr97nbsflcn8m4bs7xlakxqn5vaa43jms9f2fk63vm9j12csjrj")))

(define-public crate-ckb-proposal-table-0.102.0 (c (n "ckb-proposal-table") (v "0.102.0") (d (list (d (n "ckb-chain-spec") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.102.0") (d #t) (k 0)))) (h "0dqscpdb7yinc9zr0l949kj63f8xcmszphlj2098qbnwzyvqpk0p") (y #t)))

(define-public crate-ckb-proposal-table-0.103.0 (c (n "ckb-proposal-table") (v "0.103.0") (d (list (d (n "ckb-chain-spec") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.103.0") (d #t) (k 0)))) (h "14wmxyp1qcy0hcrvbs7xskxw755vr5zi8pqpr8xvx91nlqikr1hf")))

(define-public crate-ckb-proposal-table-0.104.0 (c (n "ckb-proposal-table") (v "0.104.0") (d (list (d (n "ckb-chain-spec") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.104.0") (d #t) (k 0)))) (h "11w9y3rxpzk7fi605alwhxykcgibd4dd2qjm3g68n7bzxmgb1pfi")))

(define-public crate-ckb-proposal-table-0.104.1 (c (n "ckb-proposal-table") (v "0.104.1") (d (list (d (n "ckb-chain-spec") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.104.1") (d #t) (k 0)))) (h "0lkqayxymswvrhjp63br7pr7rjni0rmb7xfdwdv14qjkrzyp46j5")))

(define-public crate-ckb-proposal-table-0.105.0 (c (n "ckb-proposal-table") (v "0.105.0") (d (list (d (n "ckb-chain-spec") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.105.0") (d #t) (k 0)))) (h "0w235188rn0w4kjnx15wiyzk6v1dvpks2kj7j0rcd3f7zhrsnxhs") (y #t)))

(define-public crate-ckb-proposal-table-0.105.1 (c (n "ckb-proposal-table") (v "0.105.1") (d (list (d (n "ckb-chain-spec") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.105.1") (d #t) (k 0)))) (h "18aidl9g5fqilh7dhm3yckrl6vrwvbvc5i2w5ik9dka45nqv9bnn") (y #t)))

(define-public crate-ckb-proposal-table-0.106.0 (c (n "ckb-proposal-table") (v "0.106.0") (d (list (d (n "ckb-chain-spec") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.106.0") (d #t) (k 0)))) (h "1mjxfpkvyh2cbcicm78imh46nyijz17asgfyygyw4b89kaqr5b1g")))

(define-public crate-ckb-proposal-table-0.107.0 (c (n "ckb-proposal-table") (v "0.107.0") (d (list (d (n "ckb-chain-spec") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.107.0") (d #t) (k 0)))) (h "0qyim2rsdahdjqvcb75kgb6pcjm2rxpwxil48ci8nnx40d9s67qf")))

(define-public crate-ckb-proposal-table-0.108.0 (c (n "ckb-proposal-table") (v "0.108.0") (d (list (d (n "ckb-chain-spec") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.108.0") (d #t) (k 0)))) (h "1zs5x75qj54154nlfc1fvjwr5qrssjbjk8yxwl5np7z1ivpicpxr")))

(define-public crate-ckb-proposal-table-0.108.1 (c (n "ckb-proposal-table") (v "0.108.1") (d (list (d (n "ckb-chain-spec") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.108.1") (d #t) (k 0)))) (h "0vdhdd3199s8vlixg2apn696rx2987n0yhqng62h36az0c28qas3")))

(define-public crate-ckb-proposal-table-0.109.0 (c (n "ckb-proposal-table") (v "0.109.0") (d (list (d (n "ckb-chain-spec") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.109.0") (d #t) (k 0)))) (h "0976n76nl9mlyrs0bha04qgxsgnhmpfw0bz864cirk96q0qr5hdj")))

(define-public crate-ckb-proposal-table-0.110.0 (c (n "ckb-proposal-table") (v "0.110.0") (d (list (d (n "ckb-chain-spec") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.0") (d #t) (k 0)))) (h "0vmy1nmn9078a94ab1z61n06qchlkm7vdiwy473r615y9f38gbav")))

(define-public crate-ckb-proposal-table-0.110.0-rc1 (c (n "ckb-proposal-table") (v "0.110.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "0q8dv657lp0n65lnrcippfhqgfall1a4y2ql6d2w8snpn6an967x")))

(define-public crate-ckb-proposal-table-0.111.0-rc2 (c (n "ckb-proposal-table") (v "0.111.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "1vfh5y0w1x2313rlhvar40dd4wikajzzf3xwdjxagkyjkqwgvhnv")))

(define-public crate-ckb-proposal-table-0.111.0-rc3 (c (n "ckb-proposal-table") (v "0.111.0-rc3") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "1pj2zbgy8pp2h96b9161mb09gngcykfc4fyyd8j5yyrs3sbqfs88")))

(define-public crate-ckb-proposal-table-0.111.0-rc4 (c (n "ckb-proposal-table") (v "0.111.0-rc4") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "04jv5i50rihp9yw85xw11nvw63dpi3a89xw484v96kv7gxn790c5")))

(define-public crate-ckb-proposal-table-0.111.0-rc5 (c (n "ckb-proposal-table") (v "0.111.0-rc5") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "0pkp7832an3rk62dv0j0v25adqhwdj4ha77b98w5c6k6d04afmrf")))

(define-public crate-ckb-proposal-table-0.111.0-rc6 (c (n "ckb-proposal-table") (v "0.111.0-rc6") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "0dfzz4x6ka2zf9c1hm83jav6sy7abd0rmwnmy5g66n0sldccc0kp")))

(define-public crate-ckb-proposal-table-0.111.0-rc7 (c (n "ckb-proposal-table") (v "0.111.0-rc7") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "1gcp0vlkvgaf40c0w1nxk27xw838bwa06lq0lsxvp583zk5kaqrz")))

(define-public crate-ckb-proposal-table-0.111.0-rc8 (c (n "ckb-proposal-table") (v "0.111.0-rc8") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "16799mysjlfcdcksawkw5yg3akmzyl84vkrrc36ac4h5a8rbkamf")))

(define-public crate-ckb-proposal-table-0.110.1-rc1 (c (n "ckb-proposal-table") (v "0.110.1-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "17l3w8zsz66k7i0r25jbbadijmqybq6w2142wv3m0m14vp95k3a9")))

(define-public crate-ckb-proposal-table-0.110.1-rc2 (c (n "ckb-proposal-table") (v "0.110.1-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "1jhdniczy43f094zkyl5gxn0qdn798kgg5z64bfrv30kwrgqpx5h")))

(define-public crate-ckb-proposal-table-0.111.0-rc9 (c (n "ckb-proposal-table") (v "0.111.0-rc9") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "0v67c7inv24dhx77d93jc1n0399i592jw6w1mi2bnc8fqdgpmxbc")))

(define-public crate-ckb-proposal-table-0.111.0-rc10 (c (n "ckb-proposal-table") (v "0.111.0-rc10") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "03k9k8cqhv3w9ayh78wb23hzrsgxi6c8178sq8xqnj6gckx06wbx")))

(define-public crate-ckb-proposal-table-0.110.1 (c (n "ckb-proposal-table") (v "0.110.1") (d (list (d (n "ckb-chain-spec") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.1") (d #t) (k 0)))) (h "1px6243hx6n5xs0g9fsbh9nx2famw0yqryrsf6vbzmzdhg1aya4r")))

(define-public crate-ckb-proposal-table-0.110.2-rc1 (c (n "ckb-proposal-table") (v "0.110.2-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "179j5rh5c2yl0zg993y94sffm9ad06jfn4rxl05ahrbp75yj2kxd")))

(define-public crate-ckb-proposal-table-0.111.0-rc11 (c (n "ckb-proposal-table") (v "0.111.0-rc11") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "156m59jbn4vwzjqwddjqyqwqz5kicppjp73yc0b2z4pww7ag0gbh")))

(define-public crate-ckb-proposal-table-0.110.2-rc2 (c (n "ckb-proposal-table") (v "0.110.2-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "1ygbwccl6lpwjh7hfdjyy5j71d3w90sa5z0rkyj2bpdwmf7yym3x")))

(define-public crate-ckb-proposal-table-0.111.0-rc12 (c (n "ckb-proposal-table") (v "0.111.0-rc12") (d (list (d (n "ckb-chain-spec") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "0h2ba2s1rp5mqg580y0i5gchcjnfpms0jdh7dy3i98ijx7l20hmk")))

(define-public crate-ckb-proposal-table-0.110.2 (c (n "ckb-proposal-table") (v "0.110.2") (d (list (d (n "ckb-chain-spec") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.110.2") (d #t) (k 0)))) (h "0n13fvhiz2df5547q38fvwspkb3mcz020pdxpznyf7rylkil2vra")))

(define-public crate-ckb-proposal-table-0.111.0 (c (n "ckb-proposal-table") (v "0.111.0") (d (list (d (n "ckb-chain-spec") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0") (d #t) (k 0)))) (h "1q5k52d3kqa32sy3lzahgp0jfxaxb4gs7v20d23c88nrz8lmsw55")))

(define-public crate-ckb-proposal-table-0.112.0-rc1 (c (n "ckb-proposal-table") (v "0.112.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "10ni9865c5p1xkxzs318idijj6x1cbcwppi7ksmv2x6j0dr69dfk")))

(define-public crate-ckb-proposal-table-0.112.0-rc2 (c (n "ckb-proposal-table") (v "0.112.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "1vq34azvwnqm63dfxc0qayfd81biz54r40hpqfw42bb9c5czc6bn")))

(define-public crate-ckb-proposal-table-0.112.0 (c (n "ckb-proposal-table") (v "0.112.0") (d (list (d (n "ckb-chain-spec") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0") (d #t) (k 0)))) (h "0zk2klv7sgh6y9ijdvnd20yzmf8ww63kaqy1ykjr64fkkhfjfn88")))

(define-public crate-ckb-proposal-table-0.112.0-rc3 (c (n "ckb-proposal-table") (v "0.112.0-rc3") (d (list (d (n "ckb-chain-spec") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "03v5rpwqw3v7f9lpjx9gvnbwpi01cn9cnxjxrj6l4l55jkwjp8nr")))

(define-public crate-ckb-proposal-table-0.112.1 (c (n "ckb-proposal-table") (v "0.112.1") (d (list (d (n "ckb-chain-spec") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.1") (d #t) (k 0)))) (h "0h8kj6159xmw9gkp88rwgwjkqh5s7idkfhzhjjffdyz2rwzj03z6")))

(define-public crate-ckb-proposal-table-0.113.0-rc1 (c (n "ckb-proposal-table") (v "0.113.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "19jbrl8bf4dyspd8yra3m0vx96j8ai3wav8f7lglw7sr7i3c6cpn")))

(define-public crate-ckb-proposal-table-0.113.0-rc2 (c (n "ckb-proposal-table") (v "0.113.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "0nn52gw3x90wgj26m2r4v9lw2gdd2kprlrcdamhbzb39dfp75vl3")))

(define-public crate-ckb-proposal-table-0.113.0-rc3 (c (n "ckb-proposal-table") (v "0.113.0-rc3") (d (list (d (n "ckb-chain-spec") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "10vv9hxzicr0alai0i04q1l8hzlpbd9glpy07rqahfvkyqhirc9s")))

(define-public crate-ckb-proposal-table-0.113.0 (c (n "ckb-proposal-table") (v "0.113.0") (d (list (d (n "ckb-chain-spec") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0") (d #t) (k 0)))) (h "08yassx47k7pyrmv8xn8bsasmv573nk4bs96xjmn0rl93brri4hb")))

(define-public crate-ckb-proposal-table-0.113.1-rc1 (c (n "ckb-proposal-table") (v "0.113.1-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "1plkf4k0dp8rjzrf6v5w60dsmgzfxdz91vygx2ibwnwjzcqqxmxg")))

(define-public crate-ckb-proposal-table-0.114.0-rc1 (c (n "ckb-proposal-table") (v "0.114.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "07sclj4agsaak6z4wf2x2lvmwmr4mwmp7mgd1xc1ccg28gjqmhii")))

(define-public crate-ckb-proposal-table-0.113.1-rc2 (c (n "ckb-proposal-table") (v "0.113.1-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "1xwmiviihv0p3sp7s7rk9rif6w81j5vdll9mlr1ff9cpqw4y2a9y")))

(define-public crate-ckb-proposal-table-0.113.1 (c (n "ckb-proposal-table") (v "0.113.1") (d (list (d (n "ckb-chain-spec") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.1") (d #t) (k 0)))) (h "1mfn0yxk78rs2p5gphyx25vscfs649mf6m8rr99iwh9vr3s6hfpf")))

(define-public crate-ckb-proposal-table-0.114.0-rc2 (c (n "ckb-proposal-table") (v "0.114.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "0fssa9w7ppg4sx81p157hy2cz292vln9dbk6lfr13ygpk2vgfhqw")))

(define-public crate-ckb-proposal-table-0.114.0-rc3 (c (n "ckb-proposal-table") (v "0.114.0-rc3") (d (list (d (n "ckb-chain-spec") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "1043qqswy1a8hjy4w7zibzchg1sqjxjacmbkfmvjdsrcr27hsx4h")))

(define-public crate-ckb-proposal-table-0.114.0 (c (n "ckb-proposal-table") (v "0.114.0") (d (list (d (n "ckb-chain-spec") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0") (d #t) (k 0)))) (h "1gz0njkagy7nnb6ch8r00p50fhggg3japksm7x5ph0lblgr97lj2")))

(define-public crate-ckb-proposal-table-0.115.0-pre (c (n "ckb-proposal-table") (v "0.115.0-pre") (d (list (d (n "ckb-chain-spec") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-pre") (d #t) (k 0)))) (h "0lv54pq6ch8vr1awffdydlkflswyl6saijpwdihplaswi3nq2gas")))

(define-public crate-ckb-proposal-table-0.115.0-rc1 (c (n "ckb-proposal-table") (v "0.115.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "0mfbx24yd0r5rf1942a51p5xj5px708czpkvw8n3p1858z5j9ym4")))

(define-public crate-ckb-proposal-table-0.115.0-rc2 (c (n "ckb-proposal-table") (v "0.115.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "036apsg025sjxwjq7pd0x09bnaqvbrm1ls017g4sdmkich2pphzw")))

(define-public crate-ckb-proposal-table-0.115.0 (c (n "ckb-proposal-table") (v "0.115.0") (d (list (d (n "ckb-chain-spec") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0") (d #t) (k 0)))) (h "13ylb8i3wf00nd4mzjj9rq73w8liafdh3lydnllrac51k4z5iq86")))

(define-public crate-ckb-proposal-table-0.116.0-rc1 (c (n "ckb-proposal-table") (v "0.116.0-rc1") (d (list (d (n "ckb-chain-spec") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0iad6xv2axi6ivr1qfd9n64h5nh1izxzrjkxv5sd4qmfxpl1jyi2")))

(define-public crate-ckb-proposal-table-0.116.0-rc2 (c (n "ckb-proposal-table") (v "0.116.0-rc2") (d (list (d (n "ckb-chain-spec") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "00mk0g6g85fsv3ziv7z4jqq0r5l41njwcr5pq5lcc3fga01lm268")))

(define-public crate-ckb-proposal-table-0.116.0 (c (n "ckb-proposal-table") (v "0.116.0") (d (list (d (n "ckb-chain-spec") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.0") (d #t) (k 0)))) (h "159d497bbrkbcsgxv32myxldyaiv2909ny3k7jpc78fp002i1x1r") (y #t)))

(define-public crate-ckb-proposal-table-0.116.1 (c (n "ckb-proposal-table") (v "0.116.1") (d (list (d (n "ckb-chain-spec") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-logger") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.1") (d #t) (k 0)))) (h "0fhc45i620749aganlxjnyci790nq5i9iwdwr3611ql8m03ark2y")))

