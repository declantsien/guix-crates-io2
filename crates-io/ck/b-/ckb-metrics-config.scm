(define-module (crates-io ck b- ckb-metrics-config) #:use-module (crates-io))

(define-public crate-ckb-metrics-config-0.37.0-pre (c (n "ckb-metrics-config") (v "0.37.0-pre") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "029dka891vyc9yph2m0p25y8p754svj1s67y2qvh2rnq4pxpq5jb")))

(define-public crate-ckb-metrics-config-0.37.0 (c (n "ckb-metrics-config") (v "0.37.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19qmiw2f3cch9am449jrq7v21vzw3lfjdjh4wbyxpkp2dc03h787")))

(define-public crate-ckb-metrics-config-0.38.0 (c (n "ckb-metrics-config") (v "0.38.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ziprdjnr9pnjnnz7v67q53zbg3z1fdn8ymbm728k65h8xig8qnd")))

(define-public crate-ckb-metrics-config-0.39.0 (c (n "ckb-metrics-config") (v "0.39.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08fqplav381zib2418j39ka6rxpvk6vbdap2114wj090xa1dwc19")))

(define-public crate-ckb-metrics-config-0.39.1 (c (n "ckb-metrics-config") (v "0.39.1") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "090wxpp0648gjbl6vn96ixf4vfjdwi29g1pvd00nnipkc6nvhbkv")))

(define-public crate-ckb-metrics-config-0.40.0 (c (n "ckb-metrics-config") (v "0.40.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pb7dw60yfpz2xzxc67v5wlwn7ndb0yfmdk8b743g46i4yvh35ax")))

(define-public crate-ckb-metrics-config-0.42.0 (c (n "ckb-metrics-config") (v "0.42.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0isjhq16myqlmc9vjb6qvmcvjk477n493fkrw2n9bzj47y0ifr4p")))

(define-public crate-ckb-metrics-config-0.43.0 (c (n "ckb-metrics-config") (v "0.43.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0889m39f1q0m5qwc1cb8s58cjrfvzfk5sqaqlsbwwnv64g39krx8")))

(define-public crate-ckb-metrics-config-0.100.0-rc2 (c (n "ckb-metrics-config") (v "0.100.0-rc2") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2nmw62sards42f03sbhxhsq67carbi615makw2lj327f0w55hj")))

(define-public crate-ckb-metrics-config-0.43.2 (c (n "ckb-metrics-config") (v "0.43.2") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1i1rk6lcfkj85y66nxfk8g5c3pj8ky7ppp8g14v6nmfgfi8qpr")))

(define-public crate-ckb-metrics-config-0.100.0-rc3 (c (n "ckb-metrics-config") (v "0.100.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zf7b546lgpspq266agsyvh6gqikm5l97pi7pyn960vmd4x4cm60") (y #t)))

(define-public crate-ckb-metrics-config-0.100.0-rc4 (c (n "ckb-metrics-config") (v "0.100.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b637mgzzqxcfif7qrpbdljsqbdq6bascyqiysmaglinrhdir4gm")))

(define-public crate-ckb-metrics-config-0.100.0-rc5 (c (n "ckb-metrics-config") (v "0.100.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "061ahjshyy24x9lasjpsllj0d3da2yhpxxdrqdmld98w8anj8psz")))

(define-public crate-ckb-metrics-config-0.100.0 (c (n "ckb-metrics-config") (v "0.100.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dfvsk8gzpnjfgn279hx377vn9484s1hayabk8p3ggqcarfqdvz5")))

(define-public crate-ckb-metrics-config-0.101.0 (c (n "ckb-metrics-config") (v "0.101.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dsgr8ibpax2r65zd6zn2jxhsrbikprmmjsq0mp7yv9ga69mn4db")))

(define-public crate-ckb-metrics-config-0.101.1 (c (n "ckb-metrics-config") (v "0.101.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1is5xjb5dwngvzc3qw7m8g5falv5qa23m2pcj3lvzkxpj5kb1vfw")))

(define-public crate-ckb-metrics-config-0.101.2-testnet1 (c (n "ckb-metrics-config") (v "0.101.2-testnet1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "030m76nklbd53cviq21f2pmb4dq9gmcvp4wjwm8g3rs4h63hyfgg")))

(define-public crate-ckb-metrics-config-0.101.2 (c (n "ckb-metrics-config") (v "0.101.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09ppbg02pzxm78c82fx08w9zxwzrmdqxhn8px7grba08a8sbpas5")))

(define-public crate-ckb-metrics-config-0.101.3 (c (n "ckb-metrics-config") (v "0.101.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16frd7zkhp1798559vip75y9qgq3k5q8ispngk52izakz7blrqyb")))

(define-public crate-ckb-metrics-config-0.101.4 (c (n "ckb-metrics-config") (v "0.101.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hrzc6vlqls3rqblg3fl6q5qk4dm98hh5xsgj4ffd7wq4pwqi34i")))

(define-public crate-ckb-metrics-config-0.101.5 (c (n "ckb-metrics-config") (v "0.101.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z0jinwimpfjbqh36khyllx4cpsaqbqfbghmfql2l6snp3v09p4i") (y #t)))

(define-public crate-ckb-metrics-config-0.101.6 (c (n "ckb-metrics-config") (v "0.101.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yyak2ipam12b6gxz6acgz7xpg890qqyn5jl0bcgads5nz484gyi")))

(define-public crate-ckb-metrics-config-0.101.7 (c (n "ckb-metrics-config") (v "0.101.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ps21n4334iid10rvrry6s3x329rfz143m0wkppfir1dqqkz3cs3")))

(define-public crate-ckb-metrics-config-0.101.8 (c (n "ckb-metrics-config") (v "0.101.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cnsallpb4bpi6xm30kgzg16lpg0cy371qy5va2ly6xz3xm4kxjx")))

(define-public crate-ckb-metrics-config-0.102.0 (c (n "ckb-metrics-config") (v "0.102.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04b459rrdrzcab0b81s3bf3plh1z0m76wr3plwyr13s32rzbdlvh") (y #t)))

(define-public crate-ckb-metrics-config-0.103.0 (c (n "ckb-metrics-config") (v "0.103.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00qgv4nhi0f34sh158jdaz31p6kk83g7v8ckhgqq6n1i7rz2vxdl")))

(define-public crate-ckb-metrics-config-0.104.0 (c (n "ckb-metrics-config") (v "0.104.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mgsq2nwkpcmn77q3wb0j6l76x7swd82ax65i99pl4p99lpy8fnj")))

(define-public crate-ckb-metrics-config-0.104.1 (c (n "ckb-metrics-config") (v "0.104.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1822221xw17d7nwq4zhhprlhdljg1l10a7b4bz0fi2g2n5rxvjr5")))

(define-public crate-ckb-metrics-config-0.105.0 (c (n "ckb-metrics-config") (v "0.105.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bs5xb769jbn9rgv1v6cjjf8g4snm274iv8krxmqlx4yl8hvb9v") (y #t)))

(define-public crate-ckb-metrics-config-0.105.1 (c (n "ckb-metrics-config") (v "0.105.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j8yh6bsia075d5kjyc2k8vphiq6qbz04yk5p2ff5c91dmblswlc") (y #t)))

(define-public crate-ckb-metrics-config-0.106.0 (c (n "ckb-metrics-config") (v "0.106.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17z5xkcxb83q269ak1hhh9jjwpldb5plbf5v3nr85grvwnchx1c3")))

(define-public crate-ckb-metrics-config-0.107.0 (c (n "ckb-metrics-config") (v "0.107.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17w6i3y9a7g7a4097rsg0bd8f1yfhfkq24vcq8n6zmrmc8vm8xvs")))

(define-public crate-ckb-metrics-config-0.108.0 (c (n "ckb-metrics-config") (v "0.108.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18lw505k667097j7yn3pw8i4zzd3r9qhp9wa5l067fwpdb4gjaqa")))

(define-public crate-ckb-metrics-config-0.108.1 (c (n "ckb-metrics-config") (v "0.108.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15icn0dh3zlqlzm8if2k1flwvhbvjnv0d79xm0vzp654xjxda0v8")))

(define-public crate-ckb-metrics-config-0.109.0 (c (n "ckb-metrics-config") (v "0.109.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0518l4c3gg705pir2ffi7y9959wii0wk41135cbnsvi9a6xckza1")))

(define-public crate-ckb-metrics-config-0.110.0 (c (n "ckb-metrics-config") (v "0.110.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12610lsnc8vdpbqlcvc79ywr1bq4bsih8q7lk14h5l38bsk7bxrk")))

(define-public crate-ckb-metrics-config-0.110.0-rc1 (c (n "ckb-metrics-config") (v "0.110.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f3bfz6z0rifcrrskv8rilcyv35vm8j3gp6vrcx8s5cx1c51nzfn")))

(define-public crate-ckb-metrics-config-0.111.0-rc2 (c (n "ckb-metrics-config") (v "0.111.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yslw1n96zr341c0p7hx93h2xabxv1xkws04z5glww18n9ms5w5q")))

(define-public crate-ckb-metrics-config-0.111.0-rc3 (c (n "ckb-metrics-config") (v "0.111.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i6axv2v9jl7202jk298yvci0id414mafj59n8mnbwr93g0b5jnq")))

(define-public crate-ckb-metrics-config-0.111.0-rc4 (c (n "ckb-metrics-config") (v "0.111.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "178xlhmj0hhzcw5hw1qxmfjxkikvlg1ndlklbkb079zr6hm5z03f")))

(define-public crate-ckb-metrics-config-0.111.0-rc5 (c (n "ckb-metrics-config") (v "0.111.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zysa9m8a2r4ixcvbgrvwifw3a0xsxwsqc3b4xnkhvmi65ck1sz2")))

(define-public crate-ckb-metrics-config-0.111.0-rc6 (c (n "ckb-metrics-config") (v "0.111.0-rc6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fdixsl4wydkv8n8lw7n7bm52csy1cmgjzxwfpn9if8qgl2fqx2")))

(define-public crate-ckb-metrics-config-0.111.0-rc7 (c (n "ckb-metrics-config") (v "0.111.0-rc7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "070fnzg4acxcrbi9lg379s3cdyn2sjq9shdv13j0rmflj1h6hsvj")))

(define-public crate-ckb-metrics-config-0.111.0-rc8 (c (n "ckb-metrics-config") (v "0.111.0-rc8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zn0f1d5hsjf08xximvsd28k0ybw2g35xf88gmg9n9l99fkzk3q8")))

(define-public crate-ckb-metrics-config-0.110.1-rc1 (c (n "ckb-metrics-config") (v "0.110.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09p6lr6asc61nb2j482xfrf5nqlv435hhnmpxkwb82wn5k6hvaaj")))

(define-public crate-ckb-metrics-config-0.110.1-rc2 (c (n "ckb-metrics-config") (v "0.110.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mimj5zsbqxbpv7ywjnqzcn8v6pv4zjczm75sg257jkv51l6nax9")))

(define-public crate-ckb-metrics-config-0.111.0-rc9 (c (n "ckb-metrics-config") (v "0.111.0-rc9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fj4jl262chahjqxyiyj26cljs3nva4m2lk1bdszyhvjr34klgyd")))

(define-public crate-ckb-metrics-config-0.111.0-rc10 (c (n "ckb-metrics-config") (v "0.111.0-rc10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bjg0kq6c4lar79awxy02zgn6v8cbaff4b1czn4y2123pzqcd1cd")))

(define-public crate-ckb-metrics-config-0.110.1 (c (n "ckb-metrics-config") (v "0.110.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "019kzp2jdhdqv9xfhkcsa1m5p8hzw811fkkrj7lyjciwlncd1pcv")))

(define-public crate-ckb-metrics-config-0.110.2-rc1 (c (n "ckb-metrics-config") (v "0.110.2-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06qq45fhdrfv097x3vad8kas9czl0ng1yqhhqcmhzsbby5vwizkd")))

(define-public crate-ckb-metrics-config-0.111.0-rc11 (c (n "ckb-metrics-config") (v "0.111.0-rc11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zavmjbscr82kqw5pg1naa44n9vy9j1gzf909c0crlk9wi831sxv")))

(define-public crate-ckb-metrics-config-0.110.2-rc2 (c (n "ckb-metrics-config") (v "0.110.2-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cdr6yip21piib5xsy46yj5j4mp26wd00kbmpq2njhqdx0yw67cb")))

(define-public crate-ckb-metrics-config-0.111.0-rc12 (c (n "ckb-metrics-config") (v "0.111.0-rc12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lh4krsmxd7mp8hj17ah01gjifh57b0w3nd6kdfm8wji2z52rgkd")))

(define-public crate-ckb-metrics-config-0.110.2 (c (n "ckb-metrics-config") (v "0.110.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rahbrrxh70dqzss5qswdg6gmbrmjy3g4xp6hgdcnj6b3liziyii")))

(define-public crate-ckb-metrics-config-0.111.0 (c (n "ckb-metrics-config") (v "0.111.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zdf71g60dj7pxhx1idm7zhclh8pi351p7d40aw8ap98qf7vfsfb")))

(define-public crate-ckb-metrics-config-0.112.0-rc1 (c (n "ckb-metrics-config") (v "0.112.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qnpkyrwi246gcdsacvjxlv9rcasar7isyf50f0qvyzspvklrgnl")))

(define-public crate-ckb-metrics-config-0.112.0-rc2 (c (n "ckb-metrics-config") (v "0.112.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06ly5jzs4z1qccqp0x9g1ln0fhb1lf7mdwk1pis9g4liqd6xzc67")))

(define-public crate-ckb-metrics-config-0.112.0 (c (n "ckb-metrics-config") (v "0.112.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ci45cbhm1hiwxjc6q92cn9bp1fs6rzypa16pvzl4dhzmajygzr1")))

(define-public crate-ckb-metrics-config-0.112.0-rc3 (c (n "ckb-metrics-config") (v "0.112.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11cbp6in2hy6gab1h343p7d0n9f2jvlf244z4733qiiyjb8a5h1k")))

(define-public crate-ckb-metrics-config-0.112.1 (c (n "ckb-metrics-config") (v "0.112.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nqisprghli31ck9a5k7f9wckmk65c1rk07w8zdbgws2wpix7ls4")))

(define-public crate-ckb-metrics-config-0.113.0-rc1 (c (n "ckb-metrics-config") (v "0.113.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "040p9xgbqili55jrkfm21si32m0fa5ns0ilvwi46zcm5w75rxvky")))

(define-public crate-ckb-metrics-config-0.113.0-rc2 (c (n "ckb-metrics-config") (v "0.113.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bri8h4b0wfi42c505c9l8sscf3k4hf78xb7q76fx2dpq6rc4954")))

(define-public crate-ckb-metrics-config-0.113.0-rc3 (c (n "ckb-metrics-config") (v "0.113.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gpadbvx8k4j5dcspgfhq1pqshc50lg5p0h650jckqri6v1y6fm5")))

(define-public crate-ckb-metrics-config-0.113.0 (c (n "ckb-metrics-config") (v "0.113.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18wy0qws9ix9pml662554d4nphg8vj57k7cvbagrbjmi2gi4xp93")))

(define-public crate-ckb-metrics-config-0.113.1-rc1 (c (n "ckb-metrics-config") (v "0.113.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07xirvgjjw79lgx6ds82rv6g49dqyq1zfcy7qn1bxya2k6w02hbb")))

(define-public crate-ckb-metrics-config-0.114.0-rc1 (c (n "ckb-metrics-config") (v "0.114.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b4dwhvpg5kcvcnwz1ncc7cam3r4d8nkk049cphf4rv0g6dbw3a8")))

(define-public crate-ckb-metrics-config-0.113.1-rc2 (c (n "ckb-metrics-config") (v "0.113.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vv8cgpyx1zssf54kxk59sx6asq6vlizpmwcljjmf3c08flc93hd")))

(define-public crate-ckb-metrics-config-0.113.1 (c (n "ckb-metrics-config") (v "0.113.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "152qjli2mlb8gny5cqclr6059lx3q7wxjkrgxajk4ik2vq5lsz2x")))

(define-public crate-ckb-metrics-config-0.114.0-rc2 (c (n "ckb-metrics-config") (v "0.114.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02dng3hb6gyxfn4fk1zz2xp6yb62m66yqpmdijbm2gvy4658fp1b")))

(define-public crate-ckb-metrics-config-0.114.0-rc3 (c (n "ckb-metrics-config") (v "0.114.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y3q290yqkqdw6cflay4p0yx3468ymd89sp5fc84xnwgxz6s64af")))

(define-public crate-ckb-metrics-config-0.114.0 (c (n "ckb-metrics-config") (v "0.114.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18h800pi669vsbnliqmsz6zzsj5v8hr3m4bj0jhmdmgln17kva7b")))

(define-public crate-ckb-metrics-config-0.115.0-pre (c (n "ckb-metrics-config") (v "0.115.0-pre") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ig8802s4azvk05hrmpkpsiwxrzfx2prz57l5zjyg5nivn6pl8g1")))

(define-public crate-ckb-metrics-config-0.115.0-rc1 (c (n "ckb-metrics-config") (v "0.115.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pk89lvv38v1iqpshfk7982dypwax6pv2av9k22xmps3qmsqc9wn")))

(define-public crate-ckb-metrics-config-0.115.0-rc2 (c (n "ckb-metrics-config") (v "0.115.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pzy4dwwikl8ywws27vpfdyqk8q8ns54gnwi8408shgb06ww1w1h")))

(define-public crate-ckb-metrics-config-0.115.0 (c (n "ckb-metrics-config") (v "0.115.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gb4r4q8ayp793br1bvfkclz50lp6bycl1a7kgrsc1na6z1dfzm5")))

(define-public crate-ckb-metrics-config-0.116.0-rc1 (c (n "ckb-metrics-config") (v "0.116.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fy2acqy7rj8w56ia1anaqcx2rj6rnibj58440gdyl1m905nkaqq")))

(define-public crate-ckb-metrics-config-0.116.0-rc2 (c (n "ckb-metrics-config") (v "0.116.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y70iyqk0jnpvwxl06g8rnh60747bpnv9rahc6wx3f2nxli1ji18")))

(define-public crate-ckb-metrics-config-0.116.0 (c (n "ckb-metrics-config") (v "0.116.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rf0pv5c1j9bnikdn3wqahvwbxjkdr58giwxqa0iag4ldjw3zhq2") (y #t)))

(define-public crate-ckb-metrics-config-0.116.1 (c (n "ckb-metrics-config") (v "0.116.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n8dwqxwl9vyfy7zcyzrpqzpfchijdzqbvwq70dcss0l1n4cazar")))

