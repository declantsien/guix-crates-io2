(define-module (crates-io ck b- ckb-mock-tx-types) #:use-module (crates-io))

(define-public crate-ckb-mock-tx-types-0.1.0 (c (n "ckb-mock-tx-types") (v "0.1.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.100.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.100.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.100.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ijvwxvjnqwa79ijp57l3f6kg6sz8nxyxlrdpccsf71mlmq0a43v")))

(define-public crate-ckb-mock-tx-types-0.2.0 (c (n "ckb-mock-tx-types") (v "0.2.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.101.4") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.101.4") (d #t) (k 0)) (d (n "ckb-types") (r "^0.101.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "021xnv7asn3ymqgara08cv6cnalii4a8rlfjvjrn81lsx38gfjfq")))

(define-public crate-ckb-mock-tx-types-0.3.0 (c (n "ckb-mock-tx-types") (v "0.3.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.103.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.103.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.103.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00ha1nyr02q0shxps5jz7l2hhqx0ahkhjbg0i51mj9rls9z6h3c5")))

(define-public crate-ckb-mock-tx-types-0.4.0 (c (n "ckb-mock-tx-types") (v "0.4.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.104.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.104.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.104.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lm9dbdcka3k9awphbmpffcs7gb5018faxnawsrpn9cfg51lrflj")))

(define-public crate-ckb-mock-tx-types-0.5.0 (c (n "ckb-mock-tx-types") (v "0.5.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.105.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.105.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.105.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1fq3h7mkbv2rn2fwcrak9s2wly00bpb8qjdh0bmyq0n9r1xpfy29")))

(define-public crate-ckb-mock-tx-types-0.105.1 (c (n "ckb-mock-tx-types") (v "0.105.1") (d (list (d (n "ckb-jsonrpc-types") (r "^0.105.1") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.105.1") (d #t) (k 0)) (d (n "ckb-types") (r "^0.105.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bdjj3x8bdv8hjssnpchvkla9lk1mgqr4s9nnn2cs7pqkna78df7")))

(define-public crate-ckb-mock-tx-types-0.106.0 (c (n "ckb-mock-tx-types") (v "0.106.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.106.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.106.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.106.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00c4g642l4dkw54y3fvc2si15qihhx1di7hj4ahnw1r6di575b37")))

(define-public crate-ckb-mock-tx-types-0.108.0 (c (n "ckb-mock-tx-types") (v "0.108.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.108.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.108.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.108.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0x6wmmf1qqm6pj28imz4x1qshl4d80hdzpp5p2rrqr28pvbh2si2")))

(define-public crate-ckb-mock-tx-types-0.109.0 (c (n "ckb-mock-tx-types") (v "0.109.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.109.0") (d #t) (k 0)) (d (n "ckb-traits") (r "^0.109.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.109.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xhk4svzvga8yywr6a6nnbrns0j54qm19j5gdzfmwrpb1zm6h0xj")))

(define-public crate-ckb-mock-tx-types-0.111.0-rc9 (c (n "ckb-mock-tx-types") (v "0.111.0-rc9") (d (list (d (n "ckb-jsonrpc-types") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "004mjjf49mdi8v05cpypn89gb01mkidxnn94djhifbzkvfibwiwf")))

(define-public crate-ckb-mock-tx-types-0.111.0-rc10 (c (n "ckb-mock-tx-types") (v "0.111.0-rc10") (d (list (d (n "ckb-jsonrpc-types") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x1vcamm6sybsakj82qx79laiwzn24izi3bhcd9vv36qm9c63fbb")))

(define-public crate-ckb-mock-tx-types-0.111.0-rc11 (c (n "ckb-mock-tx-types") (v "0.111.0-rc11") (d (list (d (n "ckb-jsonrpc-types") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04nd1zjb9wvh54kf719bnlljn00qj7ljdm3icm5h0yflj7ci98f8")))

(define-public crate-ckb-mock-tx-types-0.111.0-rc12 (c (n "ckb-mock-tx-types") (v "0.111.0-rc12") (d (list (d (n "ckb-jsonrpc-types") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f13s30mk5yqcj65a3q5y3gbgw1qkxlzdhdpgl32az5zllyhl5fy")))

(define-public crate-ckb-mock-tx-types-0.111.0 (c (n "ckb-mock-tx-types") (v "0.111.0") (d (list (d (n "ckb-jsonrpc-types") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.111.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzsxhqrip4kxj02wflc17lwi220hxmf4x71adhas0vgqdbb3mfw")))

(define-public crate-ckb-mock-tx-types-0.112.1 (c (n "ckb-mock-tx-types") (v "0.112.1") (d (list (d (n "ckb-jsonrpc-types") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.112.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mxpwh4hnjag34pj6k0pm7bfdrypw284q3k38jc109y9g2ivzjmc")))

(define-public crate-ckb-mock-tx-types-0.113.0 (c (n "ckb-mock-tx-types") (v "0.113.0") (d (list (d (n "ckb-jsonrpc-types") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.113.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vkzkjfi77qds5yiz37bcrj3bhw3m23wmji8fc3fvxgl1y78r6yk")))

(define-public crate-ckb-mock-tx-types-0.114.0 (c (n "ckb-mock-tx-types") (v "0.114.0") (d (list (d (n "ckb-jsonrpc-types") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.114.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0801bvzg2j0hd9h9n0r4x2ixvdmz9qar5qpkr8l4r10nhzawpyc6")))

(define-public crate-ckb-mock-tx-types-0.115.0-rc2 (c (n "ckb-mock-tx-types") (v "0.115.0-rc2") (d (list (d (n "ckb-jsonrpc-types") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-types") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1cqziqmf4iiyx3cbcwsxmalfmdxnzw4gnskvr9lz4s0mgzjbmg")))

(define-public crate-ckb-mock-tx-types-0.116.1 (c (n "ckb-mock-tx-types") (v "0.116.1") (d (list (d (n "ckb-jsonrpc-types") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-traits") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.116.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l1hw6600c6icvmiyqwfqn40irlvqj7ag5c6hbyzcpyniblgxpdi")))

