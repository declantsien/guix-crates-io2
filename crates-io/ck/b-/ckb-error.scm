(define-module (crates-io ck b- ckb-error) #:use-module (crates-io))

(define-public crate-ckb-error-0.37.0-pre (c (n "ckb-error") (v "0.37.0-pre") (d (list (d (n "ckb-occupied-capacity") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)))) (h "10hgwmvc3bi5kl6j9msj79rr1y2jch6m02508pxfqnzynbffar4w")))

(define-public crate-ckb-error-0.37.0 (c (n "ckb-error") (v "0.37.0") (d (list (d (n "ckb-occupied-capacity") (r "=0.37.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)))) (h "1gnfsfcmqc6kngc5mw3y0ahfprv8hqxyijg1gvimyjsjmgcsmnrb")))

(define-public crate-ckb-error-0.38.0 (c (n "ckb-error") (v "0.38.0") (d (list (d (n "ckb-occupied-capacity") (r "=0.38.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "07ya8xwx3zbgzj2nm3jkmvlxxhng5g62awq64qg9r4b3ffrhm20f")))

(define-public crate-ckb-error-0.39.0 (c (n "ckb-error") (v "0.39.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.39.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0bzjhrrs02z6v6hk6mh7h9snqhlkskbmqs5w7z2ky26ncwj713dq")))

(define-public crate-ckb-error-0.39.1 (c (n "ckb-error") (v "0.39.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.39.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1lnba2lg2157a2cfb8kcsczxz9wj9hh0vllaiwsqgn83lp0w40nc")))

(define-public crate-ckb-error-0.40.0 (c (n "ckb-error") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.40.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1ycbld882z12m9rg2ya2p8hm7r49687ci9swmy3phbm8lssd6y44")))

(define-public crate-ckb-error-0.42.0 (c (n "ckb-error") (v "0.42.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.42.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "04d3griqi80a6jpgx6wkqqyw4g743jybgp305v87lp57sg8j4lfr")))

(define-public crate-ckb-error-0.43.0 (c (n "ckb-error") (v "0.43.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.43.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1x3lkydiscrs6f45g9kxa2r5v8v0v9nl30n24brbsgq8rdm9wmaq")))

(define-public crate-ckb-error-0.100.0-rc2 (c (n "ckb-error") (v "0.100.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0k7jzrwzbman04ik71anrk3bpwwk42akd4yin1h31ga8ar7q0pcl")))

(define-public crate-ckb-error-0.43.2 (c (n "ckb-error") (v "0.43.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.43.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11zjp5wxlb86hlcvq7f6k9dk9fyywfxkcc9nnbld65x9pj35pwmk")))

(define-public crate-ckb-error-0.100.0-rc4 (c (n "ckb-error") (v "0.100.0-rc4") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1z7wpc4d54pzk0mw7sq2livh1s2q8iiqkm063xyr1zv94d2kbwv8")))

(define-public crate-ckb-error-0.100.0-rc5 (c (n "ckb-error") (v "0.100.0-rc5") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0wzzbgspwwyyiykfg2wmiv74r2czr25xn8y6yc7ksz3k94q1s2l7")))

(define-public crate-ckb-error-0.100.0 (c (n "ckb-error") (v "0.100.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.100.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1dvshxb9zvmqyahp8ck9q4wwn1nyi1mn4xwn55aa1y4ixb1nlbfk")))

(define-public crate-ckb-error-0.101.0 (c (n "ckb-error") (v "0.101.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "17r2r23qdvpfwpchzsi44ds6x48gi7jxgqn6imhklgiabal7kz13")))

(define-public crate-ckb-error-0.101.1 (c (n "ckb-error") (v "0.101.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "17nyjxb9bf074bpwz8rpql7d5agdmvd3rmxg1r4v5w9mzksrcwq7")))

(define-public crate-ckb-error-0.101.2-testnet1 (c (n "ckb-error") (v "0.101.2-testnet1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ah3vby63kdcfbz1xlbgv5i748yyd3j3hnj7lbw2pi3y0q17ddxr")))

(define-public crate-ckb-error-0.101.2 (c (n "ckb-error") (v "0.101.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11xxqdql78nda8xiip0z4x9vqirzm3gcvq2l2dl00bwl417zwiby")))

(define-public crate-ckb-error-0.101.3 (c (n "ckb-error") (v "0.101.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "14r20xlznla6l42xd97kipfh07cvxvqy3bd8zs1fw8qvfg87az75")))

(define-public crate-ckb-error-0.101.4 (c (n "ckb-error") (v "0.101.4") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1c51r6aqwsjwwgawk65n4hkmcksxfryky6hiqnzlffvy071as7zw")))

(define-public crate-ckb-error-0.101.5 (c (n "ckb-error") (v "0.101.5") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1akmwgsav9rfrfsm6w4f7h0ybh5hd2z53zl61kmh4c8zya0jxd8r") (y #t)))

(define-public crate-ckb-error-0.101.6 (c (n "ckb-error") (v "0.101.6") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.6") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "154p075qhda75ibqdx4wrlgqmp1bi9dd8yiqhzz0dlfibbj4ysvf")))

(define-public crate-ckb-error-0.101.7 (c (n "ckb-error") (v "0.101.7") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.7") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11cmqgl3di98kn591i1z0g8rvw4g87xy2lw2qhxi2adalm9f1dxp")))

(define-public crate-ckb-error-0.101.8 (c (n "ckb-error") (v "0.101.8") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.101.8") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ai7wp3s5maj7mmac2pdvyck55apwxjk9hvb5hbm5j1bznk6iw24")))

(define-public crate-ckb-error-0.102.0 (c (n "ckb-error") (v "0.102.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.102.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1mamgwx6qkp0zy2m76y02g8r58hi7hn9kgvinnrymd0iinszzl9s") (y #t)))

(define-public crate-ckb-error-0.103.0 (c (n "ckb-error") (v "0.103.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.103.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1z97jk33z9mvjz4bnvacpqipqwr0zpj809pv71nrjijq784av3al")))

(define-public crate-ckb-error-0.104.0 (c (n "ckb-error") (v "0.104.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.104.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "10s7k6blhvcwspxrqgzlns41pjlklmd002cmvhpg92f8yjqlpp3k")))

(define-public crate-ckb-error-0.104.1 (c (n "ckb-error") (v "0.104.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.104.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1x4h64988l8s9b2vjbkm5ypghcmkw03pwpcq1195dsy1nmimjnh2")))

(define-public crate-ckb-error-0.105.0 (c (n "ckb-error") (v "0.105.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.105.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1lfyp4pqbd7qvzp36cglngncb5qvr9x19dqdwzyc70hyhppbihvv") (y #t)))

(define-public crate-ckb-error-0.105.1 (c (n "ckb-error") (v "0.105.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.105.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1v80lh29ll06kwf01i222ki9iz5wvhkkxd7lbil6rkldw32qlx2i") (y #t)))

(define-public crate-ckb-error-0.106.0 (c (n "ckb-error") (v "0.106.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.106.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "02gb5k8xw6zjg841dfkiixrbf393drj4g1jq3fczmrl6j5r2nlgx")))

(define-public crate-ckb-error-0.107.0 (c (n "ckb-error") (v "0.107.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.107.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "092sgpv0n6q6yf103fgnhjd7b59ax9bpbfildjxab65jbyvmd11w")))

(define-public crate-ckb-error-0.108.0 (c (n "ckb-error") (v "0.108.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.108.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "19bsba1d1kjwqjb9q93r46d1lxa82zf3krzcr3qrfzc4iafm2sj4")))

(define-public crate-ckb-error-0.108.1 (c (n "ckb-error") (v "0.108.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.108.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1kwkbj9fzxiyffsls4kgy9mvd1q3m520avb09ikw2dxmbyvmsxrj")))

(define-public crate-ckb-error-0.109.0 (c (n "ckb-error") (v "0.109.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.109.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1p5bq25j9p41gikbglgdv4rs0c8iy3jd1x2zfm35nf3p21yisns8")))

(define-public crate-ckb-error-0.110.0 (c (n "ckb-error") (v "0.110.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "15c9i5djjgw29pp1a6k3bw4ngy63z7pn2hkhv47cyi8m60v1w611")))

(define-public crate-ckb-error-0.110.0-rc1 (c (n "ckb-error") (v "0.110.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "06ncn6m95xs11qwssscz0szhp3igmbf0sx5nd9mn9wplwlm3nip5")))

(define-public crate-ckb-error-0.111.0-rc2 (c (n "ckb-error") (v "0.111.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1bvnc7z7wy7173lwakbbw0k4rq690w3snzgmp6khh62mbqxwpcvr")))

(define-public crate-ckb-error-0.111.0-rc3 (c (n "ckb-error") (v "0.111.0-rc3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1fpwf3n0pf03l8gqr182y1lvd8m6ck02ivsqzvpsy88nx5s3cjnz")))

(define-public crate-ckb-error-0.111.0-rc4 (c (n "ckb-error") (v "0.111.0-rc4") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0vy7m4wypvq49cpvxnrj4dyslppiqgxl94rsm48k60a727q3c041")))

(define-public crate-ckb-error-0.111.0-rc5 (c (n "ckb-error") (v "0.111.0-rc5") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "16r2f3w2mp5zndjkq1l5963sa0jf2ymsh6rwla3cma1x40vg9wic")))

(define-public crate-ckb-error-0.111.0-rc6 (c (n "ckb-error") (v "0.111.0-rc6") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1iczln6d3hgj8yn489clhqzvm9fwkmdw12xbdwwx55jx79zph0h8")))

(define-public crate-ckb-error-0.111.0-rc7 (c (n "ckb-error") (v "0.111.0-rc7") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "18qdvrk3xgwp7xn505dad14n3mbd41w2fp7qknz65k07rs2v92d7")))

(define-public crate-ckb-error-0.111.0-rc8 (c (n "ckb-error") (v "0.111.0-rc8") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1r13f2rgfs3yzwyfqf9cx83v7312dd3v29m1n5l8qsqdi5pbwn0d")))

(define-public crate-ckb-error-0.110.1-rc1 (c (n "ckb-error") (v "0.110.1-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "15dbh4grlmvfvf9fc5f1n78nw9p765458pl9rc45dij82983x5nw")))

(define-public crate-ckb-error-0.110.1-rc2 (c (n "ckb-error") (v "0.110.1-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1clgwzkzbysbbv4a3hr8p3k7c7pashdmhfg8sk151i7pr4s658hh")))

(define-public crate-ckb-error-0.111.0-rc9 (c (n "ckb-error") (v "0.111.0-rc9") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1cjjiwj5znv6fgdmnijanl04q200dn1vp18gnygdpm195y5rqlkp")))

(define-public crate-ckb-error-0.111.0-rc10 (c (n "ckb-error") (v "0.111.0-rc10") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "142zamz4rqi3kv86mmk75jmgqvj9y2qjpdpv82ygc5y0vwwv34r4")))

(define-public crate-ckb-error-0.110.1 (c (n "ckb-error") (v "0.110.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1kckihkp6hxazxnsvgszi50j7j4by5czffhddr55720alqmxa9yn")))

(define-public crate-ckb-error-0.110.2-rc1 (c (n "ckb-error") (v "0.110.2-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "03jszv00yq43qdyp4ciayskyn8p9i62sr3nkkr2l580jflwmsqs0")))

(define-public crate-ckb-error-0.111.0-rc11 (c (n "ckb-error") (v "0.111.0-rc11") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "12c2i5070vy42pxi5n1qvr7wimmsz7n78i025svf47h70hrlaqg5")))

(define-public crate-ckb-error-0.110.2-rc2 (c (n "ckb-error") (v "0.110.2-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1fprsdgiq7yr67in56l25yg19gyvdvl90w0vchncswrf46rhjy9f")))

(define-public crate-ckb-error-0.111.0-rc12 (c (n "ckb-error") (v "0.111.0-rc12") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1gm76q6pwyms9paqhy4d857lzq2vf6zx53vsypipjjvw0rxw07ca")))

(define-public crate-ckb-error-0.110.2 (c (n "ckb-error") (v "0.110.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.110.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0vfhxnin4vjm640gb1mh1q94m8v3pidzy4sgbpdg0wrnc0bdsl6b")))

(define-public crate-ckb-error-0.111.0 (c (n "ckb-error") (v "0.111.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.111.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1mlmrlkwf3pyg30anflyhzxch4sbhqrjgh3rwqp2dd5wr8rxgkrl")))

(define-public crate-ckb-error-0.112.0-rc1 (c (n "ckb-error") (v "0.112.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0r2sqxgzjrb2dwafl3zf77qiapjs9r307afsdz0j67ip8rsrbmiq")))

(define-public crate-ckb-error-0.112.0-rc2 (c (n "ckb-error") (v "0.112.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0iql6gfdhq879vjjlrq5fs9jar0ccnfb1q2h70n9l6wqzzi22w70")))

(define-public crate-ckb-error-0.112.0 (c (n "ckb-error") (v "0.112.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.112.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1y7gsqgjmj4a8ww6ivb2s2ir29sm7n5sdb6n87kr2fclrpnir1gv")))

(define-public crate-ckb-error-0.112.0-rc3 (c (n "ckb-error") (v "0.112.0-rc3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "14249ywvn3xxhl58bv689ix0wkwwf6568phmyma8dc3fyyzzbm6q")))

(define-public crate-ckb-error-0.112.1 (c (n "ckb-error") (v "0.112.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.112.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0gpsxvvpfvvrjj49clcghmb91ig53fvw1gjmcbiah58dx80qjhdq")))

(define-public crate-ckb-error-0.113.0-rc1 (c (n "ckb-error") (v "0.113.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ijbf5ry6gb6i24p7hhj3c1yxhjl3il1jhgj83wcdr1lrpcjaci5")))

(define-public crate-ckb-error-0.113.0-rc2 (c (n "ckb-error") (v "0.113.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0hw5ydi60gagmd24l45jsscx1cji3ks8y41p8glwcy8jn7zpq7xi")))

(define-public crate-ckb-error-0.113.0-rc3 (c (n "ckb-error") (v "0.113.0-rc3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "15mgpn0baiin217sf8payac5vgpmm4rp13wd0h40in05n8krkk1l")))

(define-public crate-ckb-error-0.113.0 (c (n "ckb-error") (v "0.113.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1gpvml1qwfymlh5vlackxycr3zg4ji6bdkq7k7n32wd47w6acj4r")))

(define-public crate-ckb-error-0.113.1-rc1 (c (n "ckb-error") (v "0.113.1-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "06nad4260yabprp8q4l0cxb95k748786a86h2fv8a32cky19y1nv")))

(define-public crate-ckb-error-0.114.0-rc1 (c (n "ckb-error") (v "0.114.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1yg60vajx8kvihimj8qvzxbs7rhx0xdpsa4psv2nr5xswk32rsy9")))

(define-public crate-ckb-error-0.113.1-rc2 (c (n "ckb-error") (v "0.113.1-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1jbhl1akg3xlkxvyabjscb0hknrsv3hjs8n2gl3hrh200bhi3ba0")))

(define-public crate-ckb-error-0.113.1 (c (n "ckb-error") (v "0.113.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.113.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "18njzqnyvq9sv798qrc8g9x67cfmh5h3b5pxjgjy3zgs7cys4lw5")))

(define-public crate-ckb-error-0.114.0-rc2 (c (n "ckb-error") (v "0.114.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1v3ky2xwg7g9bn4spai1agv7ws0l2vw0c6fsv1q3kz4zyxnqwys1")))

(define-public crate-ckb-error-0.114.0-rc3 (c (n "ckb-error") (v "0.114.0-rc3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0hxvkf1rwmqn1nyi275jd9f5g91fkgcp24sh4lmwfcl4ajl99zfk")))

(define-public crate-ckb-error-0.114.0 (c (n "ckb-error") (v "0.114.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.114.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1ymwzs5lg9d7xv5iwnd5w73bcw3rai2a3dp3g581jdp08f5brmyn")))

(define-public crate-ckb-error-0.115.0-pre (c (n "ckb-error") (v "0.115.0-pre") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11k64hk6sdwdgd80vj8br93pqihbdik9b3r8i7y9f7scnc9ip9q7")))

(define-public crate-ckb-error-0.115.0-rc1 (c (n "ckb-error") (v "0.115.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0blj582mkasfikdy6h2dnzkgrji452wwlwgyhwqjqz0iy16rlkwq")))

(define-public crate-ckb-error-0.115.0-rc2 (c (n "ckb-error") (v "0.115.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "198kvzn0hka90j1w4zrs492ipc3s5vy8lj15n31h01hf6q6cdw42")))

(define-public crate-ckb-error-0.115.0 (c (n "ckb-error") (v "0.115.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.115.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cpc9rv1wbhwli253x1ix5b64p7khfr74lhlgls7q7xdssjzxxar")))

(define-public crate-ckb-error-0.116.0-rc1 (c (n "ckb-error") (v "0.116.0-rc1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ycsca4pgf2za9isdg4mcznzbvrjznvqqn2hssfd99m9djd3hgvw")))

(define-public crate-ckb-error-0.116.0-rc2 (c (n "ckb-error") (v "0.116.0-rc2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0kxdvi5l4lq2l8aln0080xm869lhr8habqclm2sb31az48rsq7xx")))

(define-public crate-ckb-error-0.116.0 (c (n "ckb-error") (v "0.116.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.116.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cwpdgp799gdfaahwfw32yify5v13bl6fn4prk5f21g103i35w3a") (y #t)))

(define-public crate-ckb-error-0.116.1 (c (n "ckb-error") (v "0.116.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ckb-occupied-capacity") (r "=0.116.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (f (quote ("display"))) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0c5s231np2s3xgbhjibpc96nm7cjinvpnm53mifgisky3n51y101")))

