(define-module (crates-io ck b- ckb-sentry-types) #:use-module (crates-io))

(define-public crate-ckb-sentry-types-0.21.0 (c (n "ckb-sentry-types") (v "0.21.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "debugid") (r "^0.7.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "11l8q0hak3fgdsh2aspyzf3v978lxfhmidjii3ngaajwxpnw6qid") (f (quote (("with_protocol" "protocol") ("protocol") ("default" "protocol"))))))

