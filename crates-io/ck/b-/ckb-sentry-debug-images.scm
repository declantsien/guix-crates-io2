(define-module (crates-io ck b- ckb-sentry-debug-images) #:use-module (crates-io))

(define-public crate-ckb-sentry-debug-images-0.21.0 (c (n "ckb-sentry-debug-images") (v "0.21.0") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "0j4v350iyl1zh2zfszh4qfkrsa2h37q34ii1jwnws71i45dr4rv2")))

