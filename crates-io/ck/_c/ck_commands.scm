(define-module (crates-io ck _c ck_commands) #:use-module (crates-io))

(define-public crate-ck_commands-0.0.4 (c (n "ck_commands") (v "0.0.4") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0ad4y5fsa5rqzmv71xz7hpbwzz0pbf305rlsd2pjvmb3clz0bbps")))

(define-public crate-ck_commands-0.0.5 (c (n "ck_commands") (v "0.0.5") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1n7d054hkp3prbhhncy3gml6q5axgjpr1n690ar2qxn1l534i0br")))

