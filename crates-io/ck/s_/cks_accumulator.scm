(define-module (crates-io ck s_ cks_accumulator) #:use-module (crates-io))

(define-public crate-cks_accumulator-0.1.0 (c (n "cks_accumulator") (v "0.1.0") (d (list (d (n "amcl_wrapper") (r "^0.1.7") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1f9h2w9gb06q11gvfqgbzzdwlh2bgsc5n2n7p102b21b5k22byvh")))

(define-public crate-cks_accumulator-0.1.2 (c (n "cks_accumulator") (v "0.1.2") (d (list (d (n "amcl_wrapper") (r "^0.3.1") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xbsf4whns6f23l4r6j516mlvs8bndzw8fqrnx6ymbc22l1vi3ya")))

