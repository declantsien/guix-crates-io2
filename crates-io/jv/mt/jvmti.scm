(define-module (crates-io jv mt jvmti) #:use-module (crates-io))

(define-public crate-jvmti-0.0.1 (c (n "jvmti") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "jni") (r "^0.18") (d #t) (k 0)))) (h "02ms8kfmi9sr6si9056ia8bw8bgxpb23qx0gz2l1nx81fb4hinli")))

(define-public crate-jvmti-0.5.0 (c (n "jvmti") (v "0.5.0") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "time") (r "0.1.*") (d #t) (k 0)) (d (n "toml") (r "0.4.*") (d #t) (k 0)))) (h "1cigq6bb42lhhsz0vni6r2j5pvvv73rb1556jm26nd0rfpn7kb67")))

