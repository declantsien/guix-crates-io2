(define-module (crates-io jv s- jvs-packets) #:use-module (crates-io))

(define-public crate-jvs-packets-1.0.0 (c (n "jvs-packets") (v "1.0.0") (h "1r5h8s1na75q4bkdfgq2ymsbd4z56lnka3wzip8wg8i1a6fgc0sa") (f (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

(define-public crate-jvs-packets-1.0.1 (c (n "jvs-packets") (v "1.0.1") (h "1dszzyw90syz5lkqxcg9mjk9nwy6c6ifnihrgpqbbvc42mbiqkwl") (f (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

(define-public crate-jvs-packets-1.0.2 (c (n "jvs-packets") (v "1.0.2") (h "07iplb8lf1v2iridg1zcknhcw4ra574n6zrsk3q0y7by9mm35vfn") (f (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

