(define-module (crates-io jv m- jvm-rs) #:use-module (crates-io))

(define-public crate-jvm-rs-0.1.0 (c (n "jvm-rs") (v "0.1.0") (h "1pmndp68cyq63p65rd4ikxhfkqrf3liv6swz52rdq3h0w3ccrkm7")))

(define-public crate-jvm-rs-0.2.0 (c (n "jvm-rs") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bsszbmcl6k26z54hr0bkkdv9ab1id97wswbwq9k9bbck9pifmnr")))

