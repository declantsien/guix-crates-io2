(define-module (crates-io jv m- jvm-find) #:use-module (crates-io))

(define-public crate-jvm-find-0.1.0 (c (n "jvm-find") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0cppkq3xxrjini0rsk6mhn0q1m0ybpg59lf9a2qckahv1l2hykvg") (f (quote (("default" "glob"))))))

