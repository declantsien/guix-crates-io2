(define-module (crates-io jv m- jvm-sys) #:use-module (crates-io))

(define-public crate-jvm-sys-0.1.0 (c (n "jvm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "jvm-find") (r "^0.1.0") (d #t) (k 1)))) (h "0zdyx2s0i2293dqvka2xy61npa9v0lgbp4lfzm7riq2sqpdb5y4p") (f (quote (("link") ("jvmti") ("jni") ("jawt") ("default" "link" "jni")))) (l "jvm")))

