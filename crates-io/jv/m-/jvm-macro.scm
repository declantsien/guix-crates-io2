(define-module (crates-io jv m- jvm-macro) #:use-module (crates-io))

(define-public crate-jvm-macro-0.0.1 (c (n "jvm-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.25.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit"))) (d #t) (k 0)))) (h "0mc27krl725j3wmr74d06hksr89vd6908nmpad3wywz8mg7l053z")))

