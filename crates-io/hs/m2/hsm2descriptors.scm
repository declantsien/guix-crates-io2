(define-module (crates-io hs m2 hsm2descriptors) #:use-module (crates-io))

(define-public crate-hsm2descriptors-0.1.0 (c (n "hsm2descriptors") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.25.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hkdf") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0gy5jxi3amdjh4fk0br93qxc58ynf28k02ci9jcjmr57969j3ygp")))

