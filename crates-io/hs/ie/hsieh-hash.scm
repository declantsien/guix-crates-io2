(define-module (crates-io hs ie hsieh-hash) #:use-module (crates-io))

(define-public crate-hsieh-hash-0.1.0 (c (n "hsieh-hash") (v "0.1.0") (h "0k05jmwrqnapd74gip7a9swrwfvdwmj6vkffbdhawp7f27a1w4l7")))

(define-public crate-hsieh-hash-0.1.1 (c (n "hsieh-hash") (v "0.1.1") (h "06hapz9q7fyqzxcxgv4mj9cz8wk57cws2dd0v5yf4n3a5690yn97")))

