(define-module (crates-io hs #{25}# hs256-bin) #:use-module (crates-io))

(define-public crate-hs256-bin-0.1.0 (c (n "hs256-bin") (v "0.1.0") (d (list (d (n "base64ct") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "0lq7h5ra3z6zsdcqfxljwszpgk48pp137ki6xs6gdln1nxfrqwsa")))

