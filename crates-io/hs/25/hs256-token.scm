(define-module (crates-io hs #{25}# hs256-token) #:use-module (crates-io))

(define-public crate-hs256-token-0.1.0 (c (n "hs256-token") (v "0.1.0") (d (list (d (n "base64ct") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0x6cr179h0gnlsa883yw32864in5irdsqswchkaxl4jawr56lbfd")))

