(define-module (crates-io hs ml hsml) #:use-module (crates-io))

(define-public crate-hsml-0.1.0 (c (n "hsml") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "11y66qzk3srlsxjhfshmn3a4k8h6qxg690bd4zjxvpagxpn7jjkr")))

