(define-module (crates-io hs -h hs-hackathon-macros) #:use-module (crates-io))

(define-public crate-hs-hackathon-macros-0.1.0 (c (n "hs-hackathon-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "proc-macro" "full"))) (d #t) (k 0)))) (h "0ppm44h4jwlpv653bnn5ihr7z4m8nj6k9ahz6cx1myk1x0la0xd7") (f (quote (("default"))))))

(define-public crate-hs-hackathon-macros-0.1.2 (c (n "hs-hackathon-macros") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "proc-macro" "full"))) (d #t) (k 0)))) (h "06lylrzirpd6i153nmwvgld2za92n2m7w0983zbhdiigc32zrly2") (f (quote (("default"))))))

(define-public crate-hs-hackathon-macros-0.1.3 (c (n "hs-hackathon-macros") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "proc-macro" "full"))) (d #t) (k 0)))) (h "0by1sznjdadvggrmgbsnjxhpgf4h25cvhk6aaw9qn0yaj0i9l1q8") (f (quote (("default"))))))

(define-public crate-hs-hackathon-macros-0.1.4 (c (n "hs-hackathon-macros") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "proc-macro" "full"))) (d #t) (k 0)))) (h "03mnb5f9yfw8i6npjf4ys5nz56d2415hk4y3pf9nqjrph4h55ckv") (f (quote (("default"))))))

