(define-module (crates-io hs t- hst-tw-utils) #:use-module (crates-io))

(define-public crate-hst-tw-utils-0.1.0 (c (n "hst-tw-utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1svppqx6cwlnb885qw74zfszayfygb6js05zckiwa956schvp7sr")))

(define-public crate-hst-tw-utils-0.2.0 (c (n "hst-tw-utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1blf8bjky9080n2b5vp9rb00zdd7csqrj1qs5ng70bypbg2sns3n")))

