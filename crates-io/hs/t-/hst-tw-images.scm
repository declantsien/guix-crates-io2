(define-module (crates-io hs t- hst-tw-images) #:use-module (crates-io))

(define-public crate-hst-tw-images-0.1.0 (c (n "hst-tw-images") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "19f5mi6f119kzx487cripknj1nl2ljs09rx28wz0c95d15i360fn")))

(define-public crate-hst-tw-images-0.2.0 (c (n "hst-tw-images") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "10p6j2syr54zdyb46bpwl11sfk6kg8q51937fy2gi7w9k8xvlgjy")))

