(define-module (crates-io hs t- hst-deactivations) #:use-module (crates-io))

(define-public crate-hst-deactivations-0.1.0 (c (n "hst-deactivations") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r4wih0qjqm0bc2lrrksl9w2lif5677wn2h55cm51y9gss3idd4x")))

(define-public crate-hst-deactivations-0.2.0 (c (n "hst-deactivations") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fd-lock") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mv5z0n6xz78qccsi8xsjfxac6y82v7i4yz7x1ycmpcba1swzr9i")))

