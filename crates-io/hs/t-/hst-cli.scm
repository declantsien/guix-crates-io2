(define-module (crates-io hs t- hst-cli) #:use-module (crates-io))

(define-public crate-hst-cli-0.1.0 (c (n "hst-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "0vrm6mvm9fdlb4ibpvxxmxx9c7b7jc814qqvijwf7q9y5qvrwp84")))

(define-public crate-hst-cli-0.2.0 (c (n "hst-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "17k6icwf6yn1c7f8bdg7sjj98fg5v9qn494mbm7xrdxwyyzh7b88")))

