(define-module (crates-io hs lu hsluv) #:use-module (crates-io))

(define-public crate-hsluv-0.1.0 (c (n "hsluv") (v "0.1.0") (d (list (d (n "json") (r "^0.11.5") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)))) (h "0g5p4x9np7292fxinqj34vlys5v20hg5yqqr8vvqbw8xcl5l3rax")))

(define-public crate-hsluv-0.3.0 (c (n "hsluv") (v "0.3.0") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "10icg8bh2vipr78rkxiskxcaadhjph2fhnx5cba4ar1lap1b9466") (f (quote (("std" "alloc" "num-traits/std") ("libm" "num-traits/libm") ("default" "std") ("alloc"))))))

(define-public crate-hsluv-0.3.1 (c (n "hsluv") (v "0.3.1") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1cmn1hixc0gk0v6ljv7mnix2g1n8w6flr1m1dkx1a56rhgk3r1lf") (f (quote (("std" "alloc" "num-traits/std") ("libm" "num-traits/libm") ("default" "std") ("alloc"))))))

