(define-module (crates-io hs on hson) #:use-module (crates-io))

(define-public crate-hson-0.1.0 (c (n "hson") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bq01dj3m4azs0wl09bvx72cg4fg1yv2vyialb5dqzyq3cjdhr3l")))

(define-public crate-hson-0.1.1 (c (n "hson") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04392j5r8abcypvlg38xz00xpqmhx1gq4pk996zjwhnm8iknxqfr")))

(define-public crate-hson-0.1.2 (c (n "hson") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1afj1hpw69mmdva28nmk2za39ads36jmpzgr80h8sfrslz14ynd2")))

(define-public crate-hson-0.1.3 (c (n "hson") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "07z3as185zqb69sszvk0k6a6plkx7rgrn7d0qdzf0slg3yjmz1mj")))

(define-public crate-hson-0.1.4 (c (n "hson") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0a5m042kbj1q62a446qwbxpw0iwbyyikscnd17m2d4in3khl4rgi")))

(define-public crate-hson-0.1.5 (c (n "hson") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0cfpzyfpf0ylp1519r7sfr1s9gszvnbggbbag8f7pxmzxzpn845k")))

(define-public crate-hson-0.1.6 (c (n "hson") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0kdgcjfjd40pjwr16lzxbwbjd8a4cr4c9x2nnvgk6bvdi8xrk0ad")))

(define-public crate-hson-0.1.7 (c (n "hson") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0w0qq9y22cs4c9pw8c0rl662f749d0s1acqil31vjrhl5c3x8i6w")))

(define-public crate-hson-0.1.8 (c (n "hson") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "1f4sz5i6rzad9cls9bdll9rpajgd19jdhs9hg28mdgywhb3gav5h")))

(define-public crate-hson-0.1.9 (c (n "hson") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0nrpc7bpx206ldgndkj1b2fbw7r6jiv5sj0mf03qcvmy0mc755mz")))

(define-public crate-hson-0.1.10 (c (n "hson") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0ic829dv3r55v3lcmy76yx90la5jl6bmxq5723zmzs36fn816yg0")))

(define-public crate-hson-0.1.11 (c (n "hson") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "067nallgsccibn7wav62r46gw6g65pjfv83zp9qvcjm70nhhlapr")))

