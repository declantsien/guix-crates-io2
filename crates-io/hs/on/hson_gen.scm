(define-module (crates-io hs on hson_gen) #:use-module (crates-io))

(define-public crate-hson_gen-0.1.0 (c (n "hson_gen") (v "0.1.0") (d (list (d (n "hson") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0bqahb8hhn8fkl6a1gkx23qiw6hqdyjz082x5k70pa59gjcyf0cz")))

