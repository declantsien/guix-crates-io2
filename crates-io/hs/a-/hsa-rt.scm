(define-module (crates-io hs a- hsa-rt) #:use-module (crates-io))

(define-public crate-hsa-rt-0.1.0 (c (n "hsa-rt") (v "0.1.0") (d (list (d (n "hsa-rt-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jgh56l9vqlrhrhsxdkw506lxpjmp6kdb2br0nayyzzid2wh9n0y")))

