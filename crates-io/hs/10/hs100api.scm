(define-module (crates-io hs #{10}# hs100api) #:use-module (crates-io))

(define-public crate-hs100api-0.1.0 (c (n "hs100api") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w427zcgffshjyab00xapb2ns8mpyxp9ca7mlhw4x5qbf1v6qzjf")))

(define-public crate-hs100api-0.1.1 (c (n "hs100api") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mkqbli9k1n21fzs92jassb1d2zavq48v1bf418fckqnqqcfin7c")))

