(define-module (crates-io hs ta hstats) #:use-module (crates-io))

(define-public crate-hstats-0.1.0 (c (n "hstats") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "rolling-stats") (r "^0.7.0") (d #t) (k 0)))) (h "0zwx6dm8kn02f8iikkzgj46i6dl2yij8sw9c9ifilzc3kdb99wz2")))

