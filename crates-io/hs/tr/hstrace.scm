(define-module (crates-io hs tr hstrace) #:use-module (crates-io))

(define-public crate-hstrace-0.0.1 (c (n "hstrace") (v "0.0.1") (h "1pnjjlanixsv9jdqmn8dj15l8ggh0vk6q4m4d6rkh58jwcghp1a9")))

(define-public crate-hstrace-0.0.2 (c (n "hstrace") (v "0.0.2") (h "0qi82q2w3jxjlalcf0qcwlxy9jr0qlzz3f0rdhpb1z4cpzli6mh8")))

(define-public crate-hstrace-0.0.3 (c (n "hstrace") (v "0.0.3") (h "1zcfrr27770n9l7zhpydl09vnzbhwjh4kza9rpwlrzfz69gbn4gz")))

(define-public crate-hstrace-0.0.4 (c (n "hstrace") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "hstrace_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0qcm06qc456di9hxr779i9lhq421ylbp0x7m3k3024hkxf51ih6h")))

(define-public crate-hstrace-0.0.5 (c (n "hstrace") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "hstrace_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "177r0n38yrjlmjamcbciph4z592h2cdw9lz4vk8df27mzzcmay2s")))

