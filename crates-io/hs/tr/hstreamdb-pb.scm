(define-module (crates-io hs tr hstreamdb-pb) #:use-module (crates-io))

(define-public crate-hstreamdb-pb-0.1.0 (c (n "hstreamdb-pb") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.1") (d #t) (k 0)) (d (n "tonic") (r "^0.8.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.0") (d #t) (k 1)))) (h "01va5jx1ckxlx7mczmssvx7pqm3vxw3naprhlhmpj0zlimnlvl29")))

(define-public crate-hstreamdb-pb-0.2.1 (c (n "hstreamdb-pb") (v "0.2.1") (d (list (d (n "prost") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.5") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "19kngr4syi7afjgn7wq1sl03dqs2hy81m2cbks740rbjvvqfkccx")))

