(define-module (crates-io hs -s hs-scraper) #:use-module (crates-io))

(define-public crate-hs-scraper-0.1.0 (c (n "hs-scraper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "10clznr5gqwswpbp6djxg0milhnj7gljf43cxgpf38p3ihk8kjsl")))

(define-public crate-hs-scraper-0.1.1 (c (n "hs-scraper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "127vc8w2z9pg932q33jipz8qggbzvs3mnxbh75c2g60a9wnqj4pk")))

