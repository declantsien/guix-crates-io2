(define-module (crates-io hs -b hs-bindgen-types) #:use-module (crates-io))

(define-public crate-hs-bindgen-types-0.7.1 (c (n "hs-bindgen-types") (v "0.7.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hrxmsy27gzn1zrwq4q6kwnln720ybi529qjcl1m63gl54nmca3w") (r "1.64.0")))

(define-public crate-hs-bindgen-types-0.7.2 (c (n "hs-bindgen-types") (v "0.7.2") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14q72lcgq8hshdrxflwc9bw1f5fjwhb47jky0xl65n9hwypfcmdl") (r "1.64.0")))

(define-public crate-hs-bindgen-types-0.8.0 (c (n "hs-bindgen-types") (v "0.8.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "133n055bp8b48zfw8wjm1zxn2fa07qjdg2am0al4msj1avg8dnf2") (r "1.64.0")))

(define-public crate-hs-bindgen-types-0.9.0 (c (n "hs-bindgen-types") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01dv79b6k5jmfrgazzxm8kpi3m8la4m0wf6bfgysgjdkiv1c3xmf") (r "1.64.0")))

