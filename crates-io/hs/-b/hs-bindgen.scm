(define-module (crates-io hs -b hs-bindgen) #:use-module (crates-io))

(define-public crate-hs-bindgen-0.1.0 (c (n "hs-bindgen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xk9qbqwqxsff4r7klyxr7f6rqfak2bvwr455jcdya9ppxhpdhsk") (y #t)))

(define-public crate-hs-bindgen-0.1.1 (c (n "hs-bindgen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qn8gmx8b2ankxwvzrj3kr1dwdl2ndgi024b2w6al912794lbrfd")))

(define-public crate-hs-bindgen-0.2.0 (c (n "hs-bindgen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0f78k1p75jpavdrqhkrcx85wvvlzni0s1jwavp666vzaz4h3cz5p") (y #t)))

(define-public crate-hs-bindgen-0.3.0 (c (n "hs-bindgen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1cb02plia961vxwyqaqbf815hnabdk2962sz5cidxzfahwrd2nwm")))

(define-public crate-hs-bindgen-0.3.1 (c (n "hs-bindgen") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1yq8c1vfpi0jpgcb6bgp8h25rxj7kw8j1h7ig6nhp45mlbsinjww")))

(define-public crate-hs-bindgen-0.3.2 (c (n "hs-bindgen") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03rkbq2ifhbysrydy8616kqh8779xsa6w8h7n5bfi6ccn33x16jk") (y #t)))

(define-public crate-hs-bindgen-0.3.3 (c (n "hs-bindgen") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "099qqfp5syj2j4f0zra5w4grz6439qb7l76pzi7y4h6r7q323lbq")))

(define-public crate-hs-bindgen-0.4.0 (c (n "hs-bindgen") (v "0.4.0") (d (list (d (n "hs-bindgen-derive") (r "^0.4") (d #t) (k 0)) (d (n "hs-bindgen-traits") (r "^0.4") (d #t) (k 0)))) (h "05d7lhcijpdas9hryhaqhvszh62sq3850krybm5bylhzw87bjbmi") (y #t)))

(define-public crate-hs-bindgen-0.4.1 (c (n "hs-bindgen") (v "0.4.1") (d (list (d (n "hs-bindgen-derive") (r "^0.4") (d #t) (k 0)) (d (n "hs-bindgen-traits") (r "^0.4") (d #t) (k 0)))) (h "1p5kh8fb7w5n70cdrhz1byx3fip5j7h01491zjkjdzlr90l0bprn")))

(define-public crate-hs-bindgen-0.5.0 (c (n "hs-bindgen") (v "0.5.0") (d (list (d (n "hs-bindgen-derive") (r "^0.5") (d #t) (k 0)) (d (n "hs-bindgen-traits") (r "^0.5") (d #t) (k 0)))) (h "1a5bj6044ln9nyn12vgby3v9crxjvf5z7q2hsv5l3hm5793bya2l")))

(define-public crate-hs-bindgen-0.5.1 (c (n "hs-bindgen") (v "0.5.1") (d (list (d (n "hs-bindgen-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "hs-bindgen-traits") (r "^0.5.1") (d #t) (k 0)))) (h "157n4pxay0pw82y2sn1dr3c7dr54x6qvjfaygkiql2rgskfgd2h9")))

(define-public crate-hs-bindgen-0.5.2 (c (n "hs-bindgen") (v "0.5.2") (d (list (d (n "hs-bindgen-derive") (r "^0.5.2") (k 0)) (d (n "hs-bindgen-traits") (r "^0.5.2") (d #t) (k 0)))) (h "0cvvjvyj7mxhvfvk56k0028ddyiqw2z3f40cdsm16qskly148cnh") (f (quote (("default" "antlion") ("antlion" "hs-bindgen-derive/antlion"))))))

(define-public crate-hs-bindgen-0.6.0 (c (n "hs-bindgen") (v "0.6.0") (d (list (d (n "hs-bindgen-derive") (r "^0.6") (k 0)) (d (n "hs-bindgen-traits") (r "^0.6") (d #t) (k 0)))) (h "0cxqnz0sria58i75plzn99gp7f1kkbvhwjrqjnp69dxf1a90j5yi") (f (quote (("default" "antlion") ("antlion" "hs-bindgen-derive/antlion"))))))

(define-public crate-hs-bindgen-0.6.1 (c (n "hs-bindgen") (v "0.6.1") (d (list (d (n "hs-bindgen-derive") (r "^0.6") (k 0)) (d (n "hs-bindgen-traits") (r "^0.6") (d #t) (k 0)))) (h "06jfpkw6phrny9yapi5dgx0cj5hzjiypdha40yjbski15q6h9f7m") (f (quote (("default" "antlion") ("antlion" "hs-bindgen-derive/antlion"))))))

(define-public crate-hs-bindgen-0.6.2 (c (n "hs-bindgen") (v "0.6.2") (d (list (d (n "hs-bindgen-derive") (r "^0.6") (k 0)) (d (n "hs-bindgen-traits") (r "^0.6") (d #t) (k 0)))) (h "1fjvrc1zdbmrrg8qilw2b2vcx92af11k35cjb5rqk8mq685b0sny") (f (quote (("default" "antlion") ("antlion" "hs-bindgen-derive/antlion")))) (r "1.64.0")))

(define-public crate-hs-bindgen-0.7.0 (c (n "hs-bindgen") (v "0.7.0") (d (list (d (n "hs-bindgen-derive") (r "^0.7") (k 0)) (d (n "hs-bindgen-traits") (r "^0.7") (k 0)))) (h "0pbrdmdciichcbfdbjhyq0bqnv1057xpi1iwk63f7yb3dnlb9x5g") (f (quote (("std" "hs-bindgen-traits/std") ("full" "antlion" "std") ("default" "std") ("antlion" "hs-bindgen-derive/antlion")))) (r "1.64.0")))

(define-public crate-hs-bindgen-0.7.1 (c (n "hs-bindgen") (v "0.7.1") (d (list (d (n "hs-bindgen-attribute") (r "^0.7") (k 0)) (d (n "hs-bindgen-traits") (r "^0.7") (k 0)))) (h "148n3rs32292lmwnwqymsrafxxg1ly3z854q01m9rgs1cg42ghpb") (f (quote (("std" "hs-bindgen-traits/std") ("full" "antlion" "std") ("default" "std") ("antlion" "hs-bindgen-attribute/antlion")))) (r "1.64.0")))

(define-public crate-hs-bindgen-0.8.0 (c (n "hs-bindgen") (v "0.8.0") (d (list (d (n "hs-bindgen-attribute") (r "^0.8") (k 0)) (d (n "hs-bindgen-traits") (r "^0.8") (k 0)))) (h "1iqgs5h55604x8p07c8qhr63iay743dp5kh15gjw68wz3yl275ry") (f (quote (("std" "hs-bindgen-traits/std") ("full" "antlion" "std") ("default" "std") ("antlion" "hs-bindgen-attribute/antlion")))) (r "1.64.0")))

(define-public crate-hs-bindgen-0.9.0 (c (n "hs-bindgen") (v "0.9.0") (d (list (d (n "hs-bindgen-attribute") (r "^0.9") (k 0)) (d (n "hs-bindgen-traits") (r "^0.9") (k 0)))) (h "0xk3n3kx360nz6qak6wnc6m29q2nck2znbm94559jzjh2zi6qn15") (f (quote (("std" "hs-bindgen-traits/std") ("reflexive" "hs-bindgen-attribute/reflexive") ("full" "reflexive" "std") ("default" "std")))) (r "1.64.0")))

