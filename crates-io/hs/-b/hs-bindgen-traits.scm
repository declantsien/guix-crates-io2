(define-module (crates-io hs -b hs-bindgen-traits) #:use-module (crates-io))

(define-public crate-hs-bindgen-traits-0.4.0 (c (n "hs-bindgen-traits") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1p5mzahgy21pfgy7s9l5j2ad2nlq653q5adps54z8wssgipy77js") (y #t)))

(define-public crate-hs-bindgen-traits-0.4.1 (c (n "hs-bindgen-traits") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0fpj513pcr86z4mpdi6kxjy1jmfh6rpalcpqbr1bnyqa1lq8p5m0")))

(define-public crate-hs-bindgen-traits-0.5.0 (c (n "hs-bindgen-traits") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1lcr5nsb3rbqc6w6ivb98rxvbzh8q3br8bbj1q0x526187rrli4x")))

(define-public crate-hs-bindgen-traits-0.5.1 (c (n "hs-bindgen-traits") (v "0.5.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r7hg9w6mgyf0va4jwdnyac3k86q8w4az9x397r5xca90hfnxfk7")))

(define-public crate-hs-bindgen-traits-0.5.2 (c (n "hs-bindgen-traits") (v "0.5.2") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0piy595834q6z3qhbn23n77pm8hh5yavg80w946pnaiqs35aa0mg")))

(define-public crate-hs-bindgen-traits-0.6.0 (c (n "hs-bindgen-traits") (v "0.6.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mqga1vpr1k9p8659j1yrx1j1l2wi7ilg95c48wk6kb2gkas1wfd") (y #t)))

(define-public crate-hs-bindgen-traits-0.6.1 (c (n "hs-bindgen-traits") (v "0.6.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11qgj12g4aa27ckqnmwaqzl30jxq7b2cjwaf8khnzxwa9lb09xsb") (y #t)))

(define-public crate-hs-bindgen-traits-0.6.2 (c (n "hs-bindgen-traits") (v "0.6.2") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xcijan6lajw18dcrficm1lkm1ywh83aqyx5bfws4d9bvvrvwwx0")))

(define-public crate-hs-bindgen-traits-0.6.3 (c (n "hs-bindgen-traits") (v "0.6.3") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16fjkkkixbdnncfs4habcp23yf53chgm618xi8p9xnlmgfnbyfmb") (r "1.64.0")))

(define-public crate-hs-bindgen-traits-0.7.0 (c (n "hs-bindgen-traits") (v "0.7.0") (d (list (d (n "displaydoc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zlkjpjgsjiz62smgsvxyx3fr0lc9nw8fp21nfz7wwc8l66j6nhz") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:displaydoc" "dep:proc-macro2" "dep:quote" "dep:thiserror")))) (r "1.64.0")))

(define-public crate-hs-bindgen-traits-0.7.1 (c (n "hs-bindgen-traits") (v "0.7.1") (h "10i2c5wqp971aqx5r2mi97xlx92ghn9vqc7fs0xi0hx5x7x0vycc") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-hs-bindgen-traits-0.7.2 (c (n "hs-bindgen-traits") (v "0.7.2") (h "1dl7iiqh495gwdky9b5pmbbippc5jzdrpp26311vrkisr16sc4q8") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-hs-bindgen-traits-0.8.0 (c (n "hs-bindgen-traits") (v "0.8.0") (h "0sjs2dpim296ajh560pfwjjlp32l8s1kw634kwlpangfaj986bp9") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-hs-bindgen-traits-0.9.0 (c (n "hs-bindgen-traits") (v "0.9.0") (h "1z19dx2vx1z2cwwxyl3k7816zc0a9mv17679rw16hx21ca9whkmc") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

