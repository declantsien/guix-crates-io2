(define-module (crates-io hs -p hs-pack) #:use-module (crates-io))

(define-public crate-hs-pack-0.2.0 (c (n "hs-pack") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lwvp0dxdldxpylhidb9r5qm43xwh159140d38s4d2l99m6mmrm2") (y #t)))

(define-public crate-hs-pack-0.2.1 (c (n "hs-pack") (v "0.2.1") (h "185gm4dm26cv40ja6b5l2y8n5pvy5lywzsqklxiswm7w9hhn67ji") (y #t)))

(define-public crate-hs-pack-0.2.2 (c (n "hs-pack") (v "0.2.2") (h "0rd74ckg0hib52yir1if181v65crsahyb8v4pg8igfb70z5jiwqj") (r "1.62.1")))

