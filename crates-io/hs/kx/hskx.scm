(define-module (crates-io hs kx hskx) #:use-module (crates-io))

(define-public crate-hskx-1.0.0 (c (n "hskx") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q3s22hydvaw3as6q9xjand46aqys1ic5f5nclcxlh2ks9a82vkp") (r "1.70.0")))

(define-public crate-hskx-1.0.1 (c (n "hskx") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12h3wwv2h8iinf031sppnbng5y6qv4vw3x8xwaa88lpwsx8fiys8") (r "1.70.0")))

