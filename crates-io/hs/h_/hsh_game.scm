(define-module (crates-io hs h_ hsh_game) #:use-module (crates-io))

(define-public crate-hsh_game-0.1.0 (c (n "hsh_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ii5s771dscz21ljcrqb9p5709m7v4va4v9knwi86qlkkjnnv6f2")))

(define-public crate-hsh_game-0.1.1 (c (n "hsh_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1r7a9q4p9xs09gr92xhl47q4qypn77gravlx759iqg8v1hpmjr97")))

