(define-module (crates-io ag pu agpu-macro) #:use-module (crates-io))

(define-public crate-agpu-macro-0.1.0 (c (n "agpu-macro") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.11") (d #t) (k 0)))) (h "1cvby8bswd23i87rspjavkzr4x7xb4bp92689r31d1av2rlcr9lf")))

(define-public crate-agpu-macro-0.1.1 (c (n "agpu-macro") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.11") (d #t) (k 0)))) (h "0ncb3lh7vkj8m1p0fmvrkjz0isgzdp5hiwcxdzxbbrcpjd0nk3c8")))

(define-public crate-agpu-macro-0.1.2 (c (n "agpu-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d4fg812792fj91alk7py4kqqqrw4jl4xjx4iaknnny67pzwv54x")))

