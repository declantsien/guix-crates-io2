(define-module (crates-io ag hp aghpb) #:use-module (crates-io))

(define-public crate-aghpb-0.1.0 (c (n "aghpb") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gq42v3vz4bdyngayrhs5z4bfxfb075bdgan9g5is12in095xfxl")))

(define-public crate-aghpb-1.0.0 (c (n "aghpb") (v "1.0.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07sr2bf5vbjjh362yw4cdi8824dipnfhxa4w6zf8pw8gi5g36a6q")))

(define-public crate-aghpb-1.1.0 (c (n "aghpb") (v "1.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x4902121vp9pwx42pzlmxbmryfznm7caf4jxn9c11fbjz70l8fj")))

(define-public crate-aghpb-1.2.0 (c (n "aghpb") (v "1.2.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1av8k7rfji1khf04xq6mnfkrijkassq194rn9q67ww2ccm3m8s1w")))

(define-public crate-aghpb-1.3.0 (c (n "aghpb") (v "1.3.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0amkxr4p4pmw36pjz3arfckjbc8sfjyvjnba6621r55qxw95ph4f")))

(define-public crate-aghpb-1.3.2 (c (n "aghpb") (v "1.3.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kv3lvwphg8k3zhafs80n8j8hb6jhw0wxksnhyxifmrs3f96wsv8")))

(define-public crate-aghpb-1.4.0 (c (n "aghpb") (v "1.4.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1rjdclbc0isyh5lc6w6wzhi5528f637sk6m6212bcc2mxlb1zxfi")))

(define-public crate-aghpb-1.4.1 (c (n "aghpb") (v "1.4.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1fa5285cfdr93pjq12rr7bkyx37djg144129mfjdn1jn9fpbhkzk")))

