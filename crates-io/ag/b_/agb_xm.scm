(define-module (crates-io ag b_ agb_xm) #:use-module (crates-io))

(define-public crate-agb_xm-0.16.0 (c (n "agb_xm") (v "0.16.0") (d (list (d (n "agb_xm_core") (r "^0.16.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0fm9z0qgb3pgrdmn7gc35d32rbq44lw6vjcvr6mxjsvd5ns593bk")))

(define-public crate-agb_xm-0.17.0 (c (n "agb_xm") (v "0.17.0") (d (list (d (n "agb_xm_core") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1y3knm7g89aklj1qkky9b6rx4l7y7r50dbd05mk7wv8xr3dabkga")))

(define-public crate-agb_xm-0.17.1 (c (n "agb_xm") (v "0.17.1") (d (list (d (n "agb_xm_core") (r "^0.17.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1fgdlg8wkrqjg1xrlxj5xcnaclxnliyf4ahahm76wjr4gdbcg6p8")))

(define-public crate-agb_xm-0.18.0 (c (n "agb_xm") (v "0.18.0") (d (list (d (n "agb_xm_core") (r "^0.18.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "010d38ziwi1qh3zg3annv0383xqck0w7y8ph3y2qw5k3lbv1gn4y")))

(define-public crate-agb_xm-0.18.1 (c (n "agb_xm") (v "0.18.1") (d (list (d (n "agb_xm_core") (r "^0.18.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1hhclys5ywjq6akm4dpcp34v4p0plf3hghi3rqmdccljisal1ss6")))

(define-public crate-agb_xm-0.19.0 (c (n "agb_xm") (v "0.19.0") (d (list (d (n "agb_xm_core") (r "^0.19.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "104y9zj6x8ybz089n07q96r22anwgnh6idg2sbl9sf53cykwi8s5")))

(define-public crate-agb_xm-0.19.1 (c (n "agb_xm") (v "0.19.1") (d (list (d (n "agb_xm_core") (r "^0.19.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1m47rd31crirg921dnnxfqjl6gc3bsbmapf5yaqjwd2ykxx1540c")))

(define-public crate-agb_xm-0.20.0 (c (n "agb_xm") (v "0.20.0") (d (list (d (n "agb_xm_core") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1njk0dc6xk7pdjfzsg8rkb862j8mf65fxdcp1rmbkxb4njdddznz")))

(define-public crate-agb_xm-0.20.1 (c (n "agb_xm") (v "0.20.1") (d (list (d (n "agb_xm_core") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1ywjw1iszs93fhnz93rjbpagm3ycp19513s8mj068hkh5cz67lgl")))

(define-public crate-agb_xm-0.20.2 (c (n "agb_xm") (v "0.20.2") (d (list (d (n "agb_xm_core") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "00b0hgz3mf6pvfkgymixn054bkxvwmibbiz0xqc4cdwiha5pvka4")))

