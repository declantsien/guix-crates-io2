(define-module (crates-io ag b_ agb_fixnum) #:use-module (crates-io))

(define-public crate-agb_fixnum-0.2.1 (c (n "agb_fixnum") (v "0.2.1") (d (list (d (n "agb_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0lnb2rm581c86jbr8gj1d87wc2fpa38rys2b4wvz7i02ifrmq67n")))

(define-public crate-agb_fixnum-0.10.0 (c (n "agb_fixnum") (v "0.10.0") (d (list (d (n "agb_macros") (r "^0.10.0") (d #t) (k 0)))) (h "12b5hv2bzf9dqin3hv29cbcfvgfiy919m7cvy4krqc121vlfk4bn")))

(define-public crate-agb_fixnum-0.11.1 (c (n "agb_fixnum") (v "0.11.1") (d (list (d (n "agb_macros") (r "^0.11.1") (d #t) (k 0)))) (h "10x0990iqr8ln5pf12xax3s1bqwww3k6qbvjn4lfcraczmxsr7k7")))

(define-public crate-agb_fixnum-0.12.0 (c (n "agb_fixnum") (v "0.12.0") (d (list (d (n "agb_macros") (r "^0.12.0") (d #t) (k 0)))) (h "13bw63qs8hy368ns1sn8ycrw1i0bgdlsmvxay4f5849q1jq53cr3")))

(define-public crate-agb_fixnum-0.12.1 (c (n "agb_fixnum") (v "0.12.1") (d (list (d (n "agb_macros") (r "^0.12.1") (d #t) (k 0)))) (h "0j61ysbyr0v5b2b5656dy3950lmlrfxrhslrbpxwy78nz21vrcja")))

(define-public crate-agb_fixnum-0.12.2 (c (n "agb_fixnum") (v "0.12.2") (d (list (d (n "agb_macros") (r "^0.12.2") (d #t) (k 0)))) (h "1j9xczl93bwipgq2r0nyc45kc27ssk56mhl5rj4lbbzqn3xzds5k")))

(define-public crate-agb_fixnum-0.13.0 (c (n "agb_fixnum") (v "0.13.0") (d (list (d (n "agb_macros") (r "^0.13.0") (d #t) (k 0)))) (h "0cmz4jsrfg5l8kfp488dcxb9d23dsg6i3s6ckcccdsl76f41gfwx")))

(define-public crate-agb_fixnum-0.14.0 (c (n "agb_fixnum") (v "0.14.0") (d (list (d (n "agb_macros") (r "^0.14.0") (d #t) (k 0)))) (h "05r4ykv88szxzg9hrywr49xzgr38vqjq6vavwy3p72midlmf0bxn")))

(define-public crate-agb_fixnum-0.15.0 (c (n "agb_fixnum") (v "0.15.0") (d (list (d (n "agb_macros") (r "^0.15.0") (d #t) (k 0)))) (h "1p2mbgif8vyik2pjh0yhpkxzf80jp16j6b5aaxyfs815yydqyzjb")))

(define-public crate-agb_fixnum-0.16.0 (c (n "agb_fixnum") (v "0.16.0") (d (list (d (n "agb_macros") (r "^0.16.0") (d #t) (k 0)))) (h "16sgkm6dgg4l7z62ii5b9wiipikf902kdfm0msqlb2rijy5cxgh1")))

(define-public crate-agb_fixnum-0.17.0 (c (n "agb_fixnum") (v "0.17.0") (d (list (d (n "agb_macros") (r "^0.17.0") (d #t) (k 0)))) (h "1h9pksq7lwvs9p1mmnl5l5wxpagjdlw27g2zmvr02yiy1kfx88jy")))

(define-public crate-agb_fixnum-0.17.1 (c (n "agb_fixnum") (v "0.17.1") (d (list (d (n "agb_macros") (r "^0.17.1") (d #t) (k 0)))) (h "1skaq3zvljqiimg54fs7vy0qip1v90h6ml0pfwirz71z22ci9jhj")))

(define-public crate-agb_fixnum-0.18.0 (c (n "agb_fixnum") (v "0.18.0") (d (list (d (n "agb_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1hll13464gbn0xfg0774j6z2j1p48rmcqxvcamwfac3624ik81df")))

(define-public crate-agb_fixnum-0.18.1 (c (n "agb_fixnum") (v "0.18.1") (d (list (d (n "agb_macros") (r "^0.18.1") (d #t) (k 0)))) (h "07lalzzhai9q2iv3sfdi2zb4glkislw03w7p6rz1k4wivmb64iyj")))

(define-public crate-agb_fixnum-0.19.0 (c (n "agb_fixnum") (v "0.19.0") (d (list (d (n "agb_macros") (r "^0.19.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1brbyq9di2xpl0akw8d1r9ni6hwiapd9zhnbk3d2anmq6ng6f26g")))

(define-public crate-agb_fixnum-0.19.1 (c (n "agb_fixnum") (v "0.19.1") (d (list (d (n "agb_macros") (r "^0.19.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1ixlkf203wz9lw0mfrpxpq5xy0rw4zxa9c97df931qz1af3bzdpw")))

(define-public crate-agb_fixnum-0.20.0 (c (n "agb_fixnum") (v "0.20.0") (d (list (d (n "agb_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0c03vg7dkbpsnv3ivpqfkl4aa0cmd2wz3c9di5pkmjkqcg51a7z5")))

(define-public crate-agb_fixnum-0.20.1 (c (n "agb_fixnum") (v "0.20.1") (d (list (d (n "agb_macros") (r "^0.20.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0r290376nin1p2fzz3cs5d7m3m68fsbqv05dh5wpfppzgsh8njsg")))

(define-public crate-agb_fixnum-0.20.2 (c (n "agb_fixnum") (v "0.20.2") (d (list (d (n "agb_macros") (r "^0.20.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0l5z019r2diyvmbwzqdxa45kaf2if9bsa97zfi71d86sm1njhr6w")))

