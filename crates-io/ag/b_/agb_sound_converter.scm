(define-module (crates-io ag b_ agb_sound_converter) #:use-module (crates-io))

(define-public crate-agb_sound_converter-0.1.0 (c (n "agb_sound_converter") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0nmh894wph8kyxqyv7wkipx73cdvvs9rva2x5vy67j603hrkhqiz")))

(define-public crate-agb_sound_converter-0.2.0 (c (n "agb_sound_converter") (v "0.2.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11m9rqcspsfsk9bgqgh55svkn98arlmyqvbi6dkr306307hr9lvp") (f (quote (("freq18157"))))))

(define-public crate-agb_sound_converter-0.10.0 (c (n "agb_sound_converter") (v "0.10.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ilz0rc208zcnrf48f6425ryyfvnnxy7d1ljb2zz5avvy6sxv28z") (f (quote (("freq32768") ("freq18157"))))))

(define-public crate-agb_sound_converter-0.11.1 (c (n "agb_sound_converter") (v "0.11.1") (d (list (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m7v26cqmfcz1hc0rghd3nw648h9i2al7rpxw3kd5x4awcp33gcw") (f (quote (("freq32768") ("freq18157"))))))

(define-public crate-agb_sound_converter-0.12.0 (c (n "agb_sound_converter") (v "0.12.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00my9fkvscg03mgmavdxy7vgciz8hyc6hcggciln3pqh1cpi507g")))

(define-public crate-agb_sound_converter-0.12.1 (c (n "agb_sound_converter") (v "0.12.1") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1daqfss90b883cmwdrsfb4bqw4cj540g2kazjbaz07hxq2w0vril")))

(define-public crate-agb_sound_converter-0.12.2 (c (n "agb_sound_converter") (v "0.12.2") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1isvv2g8kzv8n8bcn8038d1iqb3vzmysywndvfdzpgmqkiqnxa8p")))

(define-public crate-agb_sound_converter-0.13.0 (c (n "agb_sound_converter") (v "0.13.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i7s687glnxh4cig9w7ndbf0y7sg03aa0sa4k0hl0lwgy1xf4pgs")))

(define-public crate-agb_sound_converter-0.14.0 (c (n "agb_sound_converter") (v "0.14.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zyndkvnwii805sm9r3k32d7fxqav65ldhrjc8c7sssnx8clxdb7")))

(define-public crate-agb_sound_converter-0.15.0 (c (n "agb_sound_converter") (v "0.15.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0prniw3zdmz3xi61dk7x98lgjpxni990srxzckzbyvlxgh0l8jm1")))

(define-public crate-agb_sound_converter-0.16.0 (c (n "agb_sound_converter") (v "0.16.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "12m4h7id2c5n867fmks669bbpjn72hhy9sl54xgwk16hr8q637rp")))

(define-public crate-agb_sound_converter-0.17.0 (c (n "agb_sound_converter") (v "0.17.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "14b173cbbj4gi4yqvl8kvni0r6iq898a6i1akn395j7mvsqnr629")))

(define-public crate-agb_sound_converter-0.17.1 (c (n "agb_sound_converter") (v "0.17.1") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0slaaln6p5vxfj83a788dhmfiyl00iqfa4pakwldb7p3ic9dvxgz")))

(define-public crate-agb_sound_converter-0.18.0 (c (n "agb_sound_converter") (v "0.18.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ipr4f8aizmipxjxl3a9yw1p4axcrxfjig6hi4h9byqmqdx4q15a")))

(define-public crate-agb_sound_converter-0.18.1 (c (n "agb_sound_converter") (v "0.18.1") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0xmabx2jn6ash49xwfvp455jjrpccb4ykjfm70hiym2mmnn6gaw8")))

(define-public crate-agb_sound_converter-0.19.0 (c (n "agb_sound_converter") (v "0.19.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1mip3d2jgy8rbjzhj6lw4pfiwybpzwzwprq0rgk7fk1b8bi96f1c")))

(define-public crate-agb_sound_converter-0.19.1 (c (n "agb_sound_converter") (v "0.19.1") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pr9w3pk7zxzvap7y6dvy23wx6qjkqwdqicshva879b7pwfg7j4s")))

(define-public crate-agb_sound_converter-0.20.0 (c (n "agb_sound_converter") (v "0.20.0") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zb0khr0zl97c7jsxj9f6pqp59gbd9xjmyp3a7zjz6dxc35z2ihj")))

(define-public crate-agb_sound_converter-0.20.1 (c (n "agb_sound_converter") (v "0.20.1") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k6s76wy1da288npv5p06pyr64c7jic0ldrfrjlxnrhrh01jf227")))

(define-public crate-agb_sound_converter-0.20.2 (c (n "agb_sound_converter") (v "0.20.2") (d (list (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pwacqqzhx4krwx937vvwdvcsln8kdmj77v7dd84z56ql0ldpz24")))

