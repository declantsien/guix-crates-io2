(define-module (crates-io ag b_ agb_midi) #:use-module (crates-io))

(define-public crate-agb_midi-0.19.0 (c (n "agb_midi") (v "0.19.0") (d (list (d (n "agb_midi_core") (r "^0.19.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "17d9frnxmp7qra0qygp0pvjwalcvkk6m6c0h89j8bslnri6mq9sv")))

(define-public crate-agb_midi-0.19.1 (c (n "agb_midi") (v "0.19.1") (d (list (d (n "agb_midi_core") (r "^0.19.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "13bllx37xjq0whj5lg4wrjaxz6248bs5rwd93pn9ddscw5f3py9p")))

(define-public crate-agb_midi-0.20.0 (c (n "agb_midi") (v "0.20.0") (d (list (d (n "agb_midi_core") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1jm0nqbzdrzxk0w7dzvc2cflp5wzw7ly8gix1yfi94hl65dgl7r8")))

(define-public crate-agb_midi-0.20.1 (c (n "agb_midi") (v "0.20.1") (d (list (d (n "agb_midi_core") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1plbqxh3nghyz50md82ggyv4gc0zzgy18w84k4fmma6p8s7p7q6b")))

(define-public crate-agb_midi-0.20.2 (c (n "agb_midi") (v "0.20.2") (d (list (d (n "agb_midi_core") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "00l8rn2c46sq89iwry8409zcpcawh9mrvcimys6x83rvyq8q590i")))

