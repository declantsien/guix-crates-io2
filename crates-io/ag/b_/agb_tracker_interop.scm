(define-module (crates-io ag b_ agb_tracker_interop) #:use-module (crates-io))

(define-public crate-agb_tracker_interop-0.16.0 (c (n "agb_tracker_interop") (v "0.16.0") (d (list (d (n "agb_fixnum") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1qsx0b4hb0g8mrqr0qgrn0ra4lyddg2xklwy1flmmqmy4y4alzjf") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.17.0 (c (n "agb_tracker_interop") (v "0.17.0") (d (list (d (n "agb_fixnum") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "13j2qdqgfdyrdzmkpwva2lcwm4fr5zfyhm2il7gf2a86iq9hi4nv") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.17.1 (c (n "agb_tracker_interop") (v "0.17.1") (d (list (d (n "agb_fixnum") (r "^0.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "0vgxa0m3l50xdwy2m7fwf2qvfc0iy1s477lxpn8qynrz8q5xqzbb") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.18.0 (c (n "agb_tracker_interop") (v "0.18.0") (d (list (d (n "agb_fixnum") (r "^0.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1qq6nrif8i5d95g2bwyk8jgipxv7nsv60wvqdwszg131bagykhpv") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.18.1 (c (n "agb_tracker_interop") (v "0.18.1") (d (list (d (n "agb_fixnum") (r "^0.18.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1pvvl8dv1951qwps44qj4rscd6akw3q8rk7ziiddpfk9ljdxc96g") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.19.0 (c (n "agb_tracker_interop") (v "0.19.0") (d (list (d (n "agb_fixnum") (r "^0.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1pln3zxl2aq4l479ml6gbk41n0zb32zdrwmi0lwnlg7q5j179cn5") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.19.1 (c (n "agb_tracker_interop") (v "0.19.1") (d (list (d (n "agb_fixnum") (r "^0.19.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1s0azj8ri01ldr8xxnby7p208lg6613j4ppndziwj262glsg4gmz") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.20.0 (c (n "agb_tracker_interop") (v "0.20.0") (d (list (d (n "agb_fixnum") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "0w97i3gcj0dd735fdvfn60maxinrsd7y5l1p4h8k28vskh41dqpk") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.20.1 (c (n "agb_tracker_interop") (v "0.20.1") (d (list (d (n "agb_fixnum") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1nnx7xd5l4fk5bj67xmf5b64h2d0snw4xpgs7dv3ypp7d7hv7dlh") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

(define-public crate-agb_tracker_interop-0.20.2 (c (n "agb_tracker_interop") (v "0.20.2") (d (list (d (n "agb_fixnum") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)))) (h "1kp1r6q7ayg9kbx6p4kgq861nkfldr9qaxw0cmkcppx8za8gsli7") (f (quote (("std") ("default" "quote")))) (s 2) (e (quote (("quote" "dep:quote" "dep:proc-macro2" "std"))))))

