(define-module (crates-io ag b_ agb_tracker) #:use-module (crates-io))

(define-public crate-agb_tracker-0.16.0 (c (n "agb_tracker") (v "0.16.0") (d (list (d (n "agb") (r "^0.17.0") (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.16.0") (k 0)) (d (n "agb_xm") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0ggml3iw3vpqn1adljpxazc1l979ipcr4qjc4bqal0hxbh8kv3x0") (f (quote (("default" "xm")))) (s 2) (e (quote (("xm" "dep:agb_xm"))))))

(define-public crate-agb_tracker-0.17.0 (c (n "agb_tracker") (v "0.17.0") (d (list (d (n "agb") (r "^0.17.0") (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.17.0") (k 0)) (d (n "agb_xm") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "03vlmgv5bxpy0nn1mwcsylynvpxnd20c9qdfmmwzk2z7z0bkm8pz") (f (quote (("default" "xm")))) (s 2) (e (quote (("xm" "dep:agb_xm"))))))

(define-public crate-agb_tracker-0.17.1 (c (n "agb_tracker") (v "0.17.1") (d (list (d (n "agb") (r "^0.17.1") (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.17.1") (k 0)) (d (n "agb_xm") (r "^0.17.1") (o #t) (d #t) (k 0)))) (h "16nk40byiq2r6904azk2nlksgfbzwvfpm4zxx30q76y7j3aw94g1") (f (quote (("default" "xm")))) (s 2) (e (quote (("xm" "dep:agb_xm"))))))

(define-public crate-agb_tracker-0.18.0 (c (n "agb_tracker") (v "0.18.0") (d (list (d (n "agb") (r "^0.18.0") (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.18.0") (k 0)) (d (n "agb_xm") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1ky4xgq0np9lx096v9vi7g15m3c9cr6gj2z03mvbdm2x05csjzv4") (f (quote (("default" "xm")))) (s 2) (e (quote (("xm" "dep:agb_xm"))))))

(define-public crate-agb_tracker-0.18.1 (c (n "agb_tracker") (v "0.18.1") (d (list (d (n "agb") (r "^0.18.1") (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.18.1") (k 0)) (d (n "agb_xm") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "129yvmva6dlm080jikg9dh76lf4p7ljmmwh9rpr24dwl50j2nvq3") (f (quote (("default" "xm")))) (s 2) (e (quote (("xm" "dep:agb_xm"))))))

(define-public crate-agb_tracker-0.19.0 (c (n "agb_tracker") (v "0.19.0") (d (list (d (n "agb") (r "^0.19.0") (d #t) (k 0)) (d (n "agb_midi") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.19.0") (k 0)) (d (n "agb_xm") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1y045n6fzh305qrcr5ykrm4f6v1sz78z530ms3rqg1f8fgyqj3ki") (f (quote (("default" "xm" "midi")))) (s 2) (e (quote (("xm" "dep:agb_xm") ("midi" "dep:agb_midi"))))))

(define-public crate-agb_tracker-0.19.1 (c (n "agb_tracker") (v "0.19.1") (d (list (d (n "agb") (r "^0.19.1") (d #t) (k 0)) (d (n "agb_midi") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.19.1") (k 0)) (d (n "agb_xm") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "0a58sinv4162761lg13i7cn4ykjgjxg21y27lca38an17b463019") (f (quote (("default" "xm" "midi")))) (s 2) (e (quote (("xm" "dep:agb_xm") ("midi" "dep:agb_midi"))))))

(define-public crate-agb_tracker-0.20.0 (c (n "agb_tracker") (v "0.20.0") (d (list (d (n "agb") (r "^0.20.0") (d #t) (k 0)) (d (n "agb_midi") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.20.0") (k 0)) (d (n "agb_xm") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "142ymhf7mnmpf8fb3mnvj2a058asl59220y96qq0r0cqzcvkfrhb") (f (quote (("default" "xm" "midi")))) (s 2) (e (quote (("xm" "dep:agb_xm") ("midi" "dep:agb_midi"))))))

(define-public crate-agb_tracker-0.20.1 (c (n "agb_tracker") (v "0.20.1") (d (list (d (n "agb") (r "^0.20.1") (d #t) (k 0)) (d (n "agb_midi") (r "^0.20.1") (o #t) (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.20.1") (k 0)) (d (n "agb_xm") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "1lc4kykdqn9fdpw2sp74cjdy1d2yzk6gw3vw8vnk4501dn3ka4y7") (f (quote (("default" "xm" "midi")))) (s 2) (e (quote (("xm" "dep:agb_xm") ("midi" "dep:agb_midi"))))))

(define-public crate-agb_tracker-0.20.2 (c (n "agb_tracker") (v "0.20.2") (d (list (d (n "agb") (r "^0.20.2") (d #t) (k 0)) (d (n "agb_midi") (r "^0.20.2") (o #t) (d #t) (k 0)) (d (n "agb_tracker_interop") (r "^0.20.2") (k 0)) (d (n "agb_xm") (r "^0.20.2") (o #t) (d #t) (k 0)))) (h "06r7zjrmnpvw59lwp3awk6yii0s4w35vcx305d4zbz49bn3kjmd6") (f (quote (("default" "xm" "midi")))) (s 2) (e (quote (("xm" "dep:agb_xm") ("midi" "dep:agb_midi"))))))

