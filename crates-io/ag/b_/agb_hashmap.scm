(define-module (crates-io ag b_ agb_hashmap) #:use-module (crates-io))

(define-public crate-agb_hashmap-0.15.0 (c (n "agb_hashmap") (v "0.15.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "0d5k1y4yxa6ps7qrbk4rm744n6zm8pn5ggsf422pbgcc5x3il6cb")))

(define-public crate-agb_hashmap-0.16.0 (c (n "agb_hashmap") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "0965s9ali7pkqcwk70b1h2xjbbph51ipm9swxyl8imyq6kwk93ma")))

(define-public crate-agb_hashmap-0.17.0 (c (n "agb_hashmap") (v "0.17.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "14wkmqs3y6f7h6j090045w150dkk38bfl8x3mdkxlvkwx6vzk7f1")))

(define-public crate-agb_hashmap-0.17.1 (c (n "agb_hashmap") (v "0.17.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "1k60b7zqmqslin6na02v7qg88125mc0hsx2ippsqp2dwi83g7imm")))

(define-public crate-agb_hashmap-0.18.0 (c (n "agb_hashmap") (v "0.18.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "1zqygh0kpnclljki6v8ali39s8jigxy6c8x020j85y2dxdyrsr2y")))

(define-public crate-agb_hashmap-0.18.1 (c (n "agb_hashmap") (v "0.18.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "0m1h2dafz1la151nk189s5h4lfrs1zbs0r2y9xmch7w1ldnhzxhs")))

(define-public crate-agb_hashmap-0.19.0 (c (n "agb_hashmap") (v "0.19.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "0c7gyi1ziqxjgcd9y8099rpzlnx3n8bfx0jckgrzfnmszbaa99nd")))

(define-public crate-agb_hashmap-0.19.1 (c (n "agb_hashmap") (v "0.19.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "1hq7h8mjl5r78mdh4xk426fl9yhk3xng05azhwn95al9cr7rq18h")))

(define-public crate-agb_hashmap-0.20.0 (c (n "agb_hashmap") (v "0.20.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "1pjry2xwrnpg6ic3cgdv801qgbagp8c44c4nc82cwg2a96kn3c8d") (f (quote (("allocator_api"))))))

(define-public crate-agb_hashmap-0.20.1 (c (n "agb_hashmap") (v "0.20.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "143a3jqxszxjy01rqw88iaxslc4y570aah0182379fv1ffv66gws") (f (quote (("allocator_api"))))))

(define-public crate-agb_hashmap-0.20.2 (c (n "agb_hashmap") (v "0.20.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "rustc-hash") (r "^1") (k 0)))) (h "1lvdpxygiva4kd1vfckcc8q69xy8n7gxcav0c8mqcln83g4m1fz9") (f (quote (("allocator_api"))))))

