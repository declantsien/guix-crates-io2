(define-module (crates-io ag b_ agb_macros) #:use-module (crates-io))

(define-public crate-agb_macros-0.1.0 (c (n "agb_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m04swccd90dbbh7x2ijpqf6i8nh6h5wszrqc6kkff4lrxrhki2m")))

(define-public crate-agb_macros-0.2.0 (c (n "agb_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17jf80k2wcsrmfz9p1fdccj13d89jqyiljkf4c5pzs0ils5j8pfm")))

(define-public crate-agb_macros-0.10.0 (c (n "agb_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01vjrhi8hbsf8sklszqafrmjaj2shk1p5lclhlc5qzz9pcrn2z69")))

(define-public crate-agb_macros-0.11.0 (c (n "agb_macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13pn56j2qbfaf9bh4nkhpwdnfvqr4sfgwpl3ifw76caf6lcvsppx") (y #t)))

(define-public crate-agb_macros-0.11.1 (c (n "agb_macros") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j4x456xlqld071919la39pd0ibdzi19d976zjq47kilz0s7hy1m")))

(define-public crate-agb_macros-0.12.0 (c (n "agb_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wfpk34iq18pyjc91cnhwwdf86q56zx71gd9g2203acnchdidsab")))

(define-public crate-agb_macros-0.12.1 (c (n "agb_macros") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gf0d52s6b8c96m3wfflnl6g550lagzdgysdrbjjx9954mw9wlaj")))

(define-public crate-agb_macros-0.12.2 (c (n "agb_macros") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xbfx0qffw5dwml3yj4vqgriyxkhf5ac329zqvd231gwfhk6rjf8")))

(define-public crate-agb_macros-0.13.0 (c (n "agb_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xk737li7qkxjx8hd8lvr518ji50flnxkqlgjmz5mp6ya0q3v7i4")))

(define-public crate-agb_macros-0.14.0 (c (n "agb_macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r3yy5l80cxi5qx0pfq6q6k2w6xzw3swxs2vw1kmyzv2i5v8zkl7")))

(define-public crate-agb_macros-0.15.0 (c (n "agb_macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rq969wqb2ni74037bi28vmxs10yn9nawdb9mpgnnfflb000r597")))

(define-public crate-agb_macros-0.16.0 (c (n "agb_macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ja1idlmxixfha84hpb8m4i1ir8rd3mkal0h9c5ygicrh10vyslv")))

(define-public crate-agb_macros-0.17.0 (c (n "agb_macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cbyhglwld0dfafgjmy96g4wzgj8m8srpx2f33c5g32bknxrdh9d")))

(define-public crate-agb_macros-0.17.1 (c (n "agb_macros") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sps669nii4mw7587hzijs39lpcp4cxlvqxrmx5p6l91y6bic2vc")))

(define-public crate-agb_macros-0.18.0 (c (n "agb_macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yicf3idyc3jh09d48p7mfb1376qn81gzx8r5shfskb32y9gr96f")))

(define-public crate-agb_macros-0.18.1 (c (n "agb_macros") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jlyw3pjrk4y8qymmrsizamqgg94g4dvzh9wy02i2sbcmsrnsz4h")))

(define-public crate-agb_macros-0.19.0 (c (n "agb_macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kgqg38w9qn7i0w0x4fj36rxxz9ciphaac79ixi5cpf76nbm8xrr")))

(define-public crate-agb_macros-0.19.1 (c (n "agb_macros") (v "0.19.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13pn7dywrg0kzw22nmp1hfxic0igmaz6hmj73fwnkfji7cfxgw24")))

(define-public crate-agb_macros-0.20.0 (c (n "agb_macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d484d2xjjsc3alr29v3aja9qb2hsamdkmgag4q7y38v254bdalx")))

(define-public crate-agb_macros-0.20.1 (c (n "agb_macros") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jzhgmxwd03zbr0b59nfnvyc57jwh62vq6cdhfhx2wymc4bzy02i")))

(define-public crate-agb_macros-0.20.2 (c (n "agb_macros") (v "0.20.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ya5j2avv67sr954f22y2qs3w6gdaxb432zn25xr7jsry5yswm14")))

