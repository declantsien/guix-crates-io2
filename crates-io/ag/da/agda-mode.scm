(define-module (crates-io ag da agda-mode) #:use-module (crates-io))

(define-public crate-agda-mode-0.0.1 (c (n "agda-mode") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "minitt-util") (r "^0.1.1") (f (quote ("cli" "repl"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1068ifb3x82ilz6frsygk0mg2j7lizhb1nwsi7g9m16rsicp23qd") (f (quote (("default" "cli") ("cli" "clap" "structopt" "rustyline" "minitt-util"))))))

(define-public crate-agda-mode-0.0.2 (c (n "agda-mode") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-process") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0vcbpy0gw0clhg2198dj7nqnibbqmjv6fdbi0zgpbin475wd6rrh")))

(define-public crate-agda-mode-0.1.0 (c (n "agda-mode") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha") (f (quote ("io" "rt-full" "codec"))) (k 0)) (d (n "tokio-process") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1xb2xnqyrz9ccwam8ssmfjlnr786pfk2vg0724px5dy61g3lasw1")))

(define-public crate-agda-mode-0.1.1 (c (n "agda-mode") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha") (f (quote ("io" "rt-full" "codec"))) (k 0)) (d (n "tokio-process") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0g51kkq759dqi168rh80559zz6p1falbi9hz01lpf43xmf0kbb55")))

(define-public crate-agda-mode-0.1.2 (c (n "agda-mode") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("io" "rt-full" "codec" "process"))) (k 0)))) (h "0lc2k12blv5izrjnhpi840q73w49dfdvkjffffyx7g55kfqv5w44")))

(define-public crate-agda-mode-0.1.3 (c (n "agda-mode") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("io" "rt-full" "codec" "process"))) (k 0)))) (h "0lf5g89g319i20cw5yb6qv2min2rq33vplgslklnf1dqyzlfd620")))

(define-public crate-agda-mode-0.1.4 (c (n "agda-mode") (v "0.1.4") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("io" "rt-full" "codec" "process"))) (k 0)))) (h "0y29yzqshz44fiy2kd0la4hcizf7nirc39l573byz4m1sm0mnx3v")))

(define-public crate-agda-mode-0.1.5 (c (n "agda-mode") (v "0.1.5") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("io" "rt-full" "codec" "process"))) (k 0)))) (h "1mj3q9wljs5ghf1fqfy38gcnhdrdy9byrwwfjpn4s4b77gnj5p34")))

(define-public crate-agda-mode-0.1.6 (c (n "agda-mode") (v "0.1.6") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("io" "rt-full" "codec" "process"))) (k 0)))) (h "0n0m1vy146zfrw05p401w59diys6rlwxpqc2nrzjwk9qwlyc4pmm")))

(define-public crate-agda-mode-0.1.7 (c (n "agda-mode") (v "0.1.7") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("io-util" "rt-core" "process"))) (d #t) (k 0)))) (h "1q52dzc2fykx37f9pfib6yh9pm0fmwynwb18901kk4v68q4c0mq3")))

(define-public crate-agda-mode-0.1.8 (c (n "agda-mode") (v "0.1.8") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("io-util" "rt" "process"))) (d #t) (k 0)))) (h "0bsw70z4gw0j5an722pfxnna3b14xw296l5y256l841jix02qxmb")))

