(define-module (crates-io ag vt agvtf) #:use-module (crates-io))

(define-public crate-agvtf-1.0.0 (c (n "agvtf") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "1dnfd13pcvrvh9dln9qc2cx85pbi86iqmqhlhrnax7jvs02wf0kg")))

(define-public crate-agvtf-1.0.1 (c (n "agvtf") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "1ckb4jrl74q7hg3y39z1d23mj8912xc9mq6rn7jvlnlx1sa8xv9x")))

(define-public crate-agvtf-1.0.2 (c (n "agvtf") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "0cw1146k9y7d62ylswvsj2hwipr5ph4gzcyws1ka1sqkndrqq7id")))

(define-public crate-agvtf-1.0.3 (c (n "agvtf") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "0hlsw1gfm12jbdlm0hnvs1jj3xvwjx3lg847i6av1dmhikxr3azc")))

(define-public crate-agvtf-1.1.0 (c (n "agvtf") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "0as5if0fxf73nyc15v039bcy4ld4pxbcgwkzs5rz3pl1zc6hyyff")))

(define-public crate-agvtf-1.1.1 (c (n "agvtf") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.13") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "1x1j0ad6mwa0kls5r3vgqpy690p01lp2xx7k8gh7rh0w8h2y9sva")))

(define-public crate-agvtf-1.1.2 (c (n "agvtf") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.14") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "0dz05cbh8mmhmldf16yji9vr91gvd0gflx1ysz1qybdx60jb0xi6")))

(define-public crate-agvtf-1.1.3 (c (n "agvtf") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.15") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "0hcbwyc10010ivwqcfcjyqdkcdbjxiqai4rh4yspkgn7mv5f20dm")))

(define-public crate-agvtf-1.1.4 (c (n "agvtf") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.16") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "009rbpb9wbckcminnfnrf7519vsnwwhnqz7q0d6nhr334d9ds2yn")))

(define-public crate-agvtf-1.1.5 (c (n "agvtf") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "1zb52vaj808z0licw8i37g7ibrxq046j8yh95rh69v46dca0j6yl")))

(define-public crate-agvtf-1.1.6 (c (n "agvtf") (v "1.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (k 0)))) (h "0zb07g36lnjk6znpyw5l1mx3ijzprr4d54yq3kgjfixwsngaa2l4")))

