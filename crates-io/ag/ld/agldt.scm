(define-module (crates-io ag ld agldt) #:use-module (crates-io))

(define-public crate-agldt-0.0.0 (c (n "agldt") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0ma9nqhhifq77hib7dsinfnnbxw3pyzs9kvsppvfjmsawgp06p77") (y #t)))

(define-public crate-agldt-0.1.0 (c (n "agldt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1kywj2l20pm2wp9wxczlyzsbf0hpfs3b27fyjsjmcg6livci0vy5") (y #t)))

(define-public crate-agldt-0.1.1 (c (n "agldt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1j1lbhvwrfnd4v976ba8knaynb63c9kiyrnx9690dcd4s0sixi15")))

(define-public crate-agldt-0.1.2 (c (n "agldt") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1b4hpkibfzbnxdf1nkdxgymqpj09j40ddl2wwqzkzjm61dj8aaay")))

