(define-module (crates-io ag cw agcwd) #:use-module (crates-io))

(define-public crate-agcwd-0.1.0 (c (n "agcwd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1qnghr9fcmv3mhxgvpn249wns2jbw2nsajyrb3niy027dk6kqyf9")))

(define-public crate-agcwd-0.2.0 (c (n "agcwd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0r67mqfsgwa2axzi95frxydfis9v64r41n8pav3c0bk9sh8vcjgx")))

(define-public crate-agcwd-0.3.0 (c (n "agcwd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0w6cl5h5vmphb84vvnr4by380gisfslwcgn2c3xhnf6jscqg6jw5")))

(define-public crate-agcwd-0.3.1 (c (n "agcwd") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "075j57kfi7z1nsfbr10zqg6hcwis3ngih4nm48zf30ay4jl4r77a")))

