(define-module (crates-io ag b- agb-gbafix) #:use-module (crates-io))

(define-public crate-agb-gbafix-0.14.0 (c (n "agb-gbafix") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "gbafix") (r "^1") (d #t) (k 0)))) (h "08y1i5nkpq0b8pnqi18q5cvxkcf0i83qysxxw40i1n1yc7q0mlsx")))

(define-public crate-agb-gbafix-0.15.0 (c (n "agb-gbafix") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "gbafix") (r "^1") (d #t) (k 0)))) (h "11y1hsrfz5syhy73lhdlxgy11lfdxf6jxcnj8ammz346wgigm9lp")))

(define-public crate-agb-gbafix-0.16.0 (c (n "agb-gbafix") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "gbafix") (r "^1") (d #t) (k 0)))) (h "17iadr9piz8kjp09y67k8xfih4nkjhy9rcaipi8rkbac8h1xa273")))

(define-public crate-agb-gbafix-0.17.0 (c (n "agb-gbafix") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "18ff0p85b2cmb5c4i8wzxzzs9al3b7f5ln5p1m1sqpnhrx6kmdiv")))

(define-public crate-agb-gbafix-0.17.1 (c (n "agb-gbafix") (v "0.17.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "1rynwsdwzbpd09ixiqvlnlzm29ayj5yn8w1zqmib6brjxpqnxasl")))

(define-public crate-agb-gbafix-0.18.0 (c (n "agb-gbafix") (v "0.18.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "0xs7a38cbppqjyfmvhaq6a8wwbmn9qc53lfzn41yykwfzj9h0pfy")))

(define-public crate-agb-gbafix-0.18.1 (c (n "agb-gbafix") (v "0.18.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "06d4n03233r0lixw3rfa441wfviq0923xmfyyr2vai61y0c6g40r")))

(define-public crate-agb-gbafix-0.19.0 (c (n "agb-gbafix") (v "0.19.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "1mbklr6639jb2hmaxa8cyb7yyr8fixrai36slqgi7w394i24vni2")))

(define-public crate-agb-gbafix-0.19.1 (c (n "agb-gbafix") (v "0.19.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "0n2lrw45ii8np4yl0s7k5kdb1a3z815f0fbsz5sybrgdcgvjagw0")))

(define-public crate-agb-gbafix-0.20.0 (c (n "agb-gbafix") (v "0.20.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)))) (h "1sz8s3vfmljn9qz9i5gfcng85ir8aqgms2pyvfaiksgxq6nbrs2d")))

(define-public crate-agb-gbafix-0.20.1 (c (n "agb-gbafix") (v "0.20.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)))) (h "1mrb4v2s7cajbn4gp09rqk4qm716zvl4705wcgsnk527xd44ny9x")))

(define-public crate-agb-gbafix-0.20.2 (c (n "agb-gbafix") (v "0.20.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "elf") (r "^0.7") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)))) (h "13mi6gf489jai6xckrgkf0pnaffp19i2d36wc4kxfl23j3rrl8mm")))

