(define-module (crates-io ag b- agb-debug) #:use-module (crates-io))

(define-public crate-agb-debug-0.20.0 (c (n "agb-debug") (v "0.20.0") (d (list (d (n "addr2line") (r "^0.22") (f (quote ("rustc-demangle" "std-object"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r075lj4r16j0ld2ch8szs9bf86xbg8jhfg7fx04sgyyl50fgb97")))

(define-public crate-agb-debug-0.20.1 (c (n "agb-debug") (v "0.20.1") (d (list (d (n "addr2line") (r "^0.22") (f (quote ("rustc-demangle" "std-object"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hf4am3i4isby6siq5gafiscc08llx52qn8b8zr0md44nv20xhg7")))

(define-public crate-agb-debug-0.20.2 (c (n "agb-debug") (v "0.20.2") (d (list (d (n "addr2line") (r "^0.22") (f (quote ("rustc-demangle" "std-object"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fgvgpsgpsyzmax2w10cgrrhxgjdag9icbp7cc7n21l960zsmfcz")))

