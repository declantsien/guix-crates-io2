(define-module (crates-io ag g_ agg_es_testcase) #:use-module (crates-io))

(define-public crate-agg_es_testcase-0.1.0 (c (n "agg_es_testcase") (v "0.1.0") (h "0hwg5hvz486m6gr9xilqyvbj2dah6a48yk2wmh2nclp5prc1bqik") (y #t)))

(define-public crate-agg_es_testcase-0.1.1 (c (n "agg_es_testcase") (v "0.1.1") (h "1ql31ljh2ly1xygl4arpgl5nbyd13rhbbjrdl7dn6cpp205b27bc") (y #t)))

