(define-module (crates-io ag g_ agg_es_testhelper) #:use-module (crates-io))

(define-public crate-agg_es_testhelper-0.1.2 (c (n "agg_es_testhelper") (v "0.1.2") (h "1m4267ckgsbrkvh3l3h9mq56jrws4nm0793kh6k94qnjw1x5swkq") (y #t)))

(define-public crate-agg_es_testhelper-0.1.3 (c (n "agg_es_testhelper") (v "0.1.3") (h "1ac2f2qh4645fglmx2z7kl3c57z9l0q09az35f3zm4xsrdjfh9a5") (y #t)))

