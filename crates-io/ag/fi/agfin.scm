(define-module (crates-io ag fi agfin) #:use-module (crates-io))

(define-public crate-agfin-0.1.0 (c (n "agfin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "1222c6kkbcy8pndqcladpmn4ys42gcz4j0mpkkz5m7ighh2zl9x6")))

(define-public crate-agfin-0.2.0 (c (n "agfin") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "0xfpppl0swvykl1n5kldps5dnxl5mlqc9yragqjamrkw925nihw0")))

(define-public crate-agfin-0.3.0 (c (n "agfin") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "1wp7rzvhhklxwlmn42fx4zr0g922bfy0n1kw97j4zlg4lfh02gja")))

(define-public crate-agfin-0.4.0 (c (n "agfin") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "11fk68v1whkq3d8i8wlg02bl1dv5z11sms93b7x2ka7nk0wi4dqq")))

(define-public crate-agfin-0.4.1 (c (n "agfin") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "05qrhbqpjdmc0xs9nz0h4rxkj5h6n1qcgndsy3ksbdxzzaflx16b")))

(define-public crate-agfin-0.4.2 (c (n "agfin") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "0jwyk6zqsqj98agbi37s02gsxdpdk6z43zh4vyy7lcpxv5l9pwda")))

(define-public crate-agfin-0.4.3 (c (n "agfin") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "0gqw13ib5p8kn1rzrbbch58dhnnfi4gh0ci0fa67imd5m7p6s10j")))

(define-public crate-agfin-0.5.0 (c (n "agfin") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "08v8k90s774psgyxlg4b6cd9h1vbbqi9c0j39f7x6w6cqbfw49hz")))

(define-public crate-agfin-0.6.0 (c (n "agfin") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "12m2ij0rzys3zm0wyw19y4vywn6jgn65v3w5la4yn40mzkby3i10")))

(define-public crate-agfin-0.7.0 (c (n "agfin") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "0v1igmh7ls9v35rxmj24ba2my2kipbwldqk1kis46b5x25xk3jl3")))

(define-public crate-agfin-0.8.0 (c (n "agfin") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (f (quote ("date"))) (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.9.0") (d #t) (k 0)))) (h "1x78paczx37j13s95l6p4x4ylc401f2j73jxzzmf39vn645n2mfq") (y #t)))

