(define-module (crates-io ag ui agui_primitives) #:use-module (crates-io))

(define-public crate-agui_primitives-0.1.0 (c (n "agui_primitives") (v "0.1.0") (d (list (d (n "agui_core") (r "^0.1.0") (d #t) (k 0)) (d (n "agui_macros") (r "^0.1.0") (f (quote ("internal"))) (d #t) (k 0)))) (h "0aravb57kcrrsvwfnknhqv2wrnlqjffnr816qx2hmyiisqbnp3dc")))

(define-public crate-agui_primitives-0.2.0 (c (n "agui_primitives") (v "0.2.0") (d (list (d (n "agui_core") (r "^0.2") (d #t) (k 0)) (d (n "agui_macros") (r "^0.2") (f (quote ("internal"))) (d #t) (k 0)))) (h "1gsgqaqhfqlii7wxkp3p5jp0dp0isxlqk9339dfwwsi9p74654d0")))

(define-public crate-agui_primitives-0.3.0 (c (n "agui_primitives") (v "0.3.0") (d (list (d (n "agui_core") (r "^0.3") (d #t) (k 0)) (d (n "agui_macros") (r "^0.3") (f (quote ("internal"))) (d #t) (k 0)) (d (n "glyph_brush_layout") (r "^0.2") (d #t) (k 0)))) (h "1cd30a16p75gbbj7mqgh32423q3vyqx1aj7h1jqw2b84ibsx2qpj")))

