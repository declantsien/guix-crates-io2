(define-module (crates-io ag ui agui) #:use-module (crates-io))

(define-public crate-agui-0.1.0 (c (n "agui") (v "0.1.0") (h "0li5f7hvn5k05jaay61vyg0a0p6y3jgk2hlnh7b3ahn4g8illh4j")))

(define-public crate-agui-0.1.1 (c (n "agui") (v "0.1.1") (d (list (d (n "agui_core") (r "^0.1") (d #t) (k 0)) (d (n "agui_macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "agui_widgets") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1naigsil8j35drsasnikm8zfvvcdrhcp51yd68pv7zdk7bfxn5sh") (f (quote (("widgets" "primitives" "agui_widgets") ("primitives" "agui_primitives") ("macros" "agui_macros") ("default" "primitives" "widgets" "macros"))))))

(define-public crate-agui-0.2.0 (c (n "agui") (v "0.2.0") (d (list (d (n "agui_core") (r "^0.2") (d #t) (k 0)) (d (n "agui_macros") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "agui_widgets") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1hy8v40p73ksac9vk21jp4df7swl7kigsax8b5am1lwp77a2wfqw") (f (quote (("widgets" "primitives" "agui_widgets") ("primitives" "agui_primitives") ("macros" "agui_macros") ("default" "primitives" "widgets" "macros"))))))

(define-public crate-agui-0.3.0 (c (n "agui") (v "0.3.0") (d (list (d (n "agui_core") (r "^0.3") (d #t) (k 0)) (d (n "agui_macros") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "agui_widgets") (r "^0.3") (o #t) (d #t) (k 0)))) (h "133kdlhy08ikbkjxzjmpbg11qdj3jb3bsb841ypgx7b03r5m5j5m") (f (quote (("widgets" "primitives" "agui_widgets") ("primitives" "agui_primitives") ("macros" "agui_macros") ("default" "primitives" "widgets" "macros"))))))

