(define-module (crates-io ag ui agui_widgets) #:use-module (crates-io))

(define-public crate-agui_widgets-0.1.0 (c (n "agui_widgets") (v "0.1.0") (d (list (d (n "agui_core") (r "^0.1.0") (d #t) (k 0)) (d (n "agui_macros") (r "^0.1.0") (f (quote ("internal"))) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06mpqzavz3i7ckyihkjya55rjjv802k6daa67chr6pkwzq6zq19d")))

(define-public crate-agui_widgets-0.2.0 (c (n "agui_widgets") (v "0.2.0") (d (list (d (n "agui_core") (r "^0.2") (d #t) (k 0)) (d (n "agui_macros") (r "^0.2") (f (quote ("internal"))) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "110amk7inikk3wlb6jflr5xy6dbl26prxcs0svq028wnawpscqip")))

(define-public crate-agui_widgets-0.3.0 (c (n "agui_widgets") (v "0.3.0") (d (list (d (n "agui_core") (r "^0.3") (d #t) (k 0)) (d (n "agui_macros") (r "^0.3") (f (quote ("internal"))) (d #t) (k 0)) (d (n "agui_primitives") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0dja2pvbx2cnc16lag44hya1d7fsjz700kygjmskrwcm35nqgx2g")))

