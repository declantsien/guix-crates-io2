(define-module (crates-io ag ui agui_core) #:use-module (crates-io))

(define-public crate-agui_core-0.1.0 (c (n "agui_core") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "morphorm") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04zk8khpmr7zzdhwxbgfxryw2hgzxh3pp5qg89vvr4pxq786rf6j")))

(define-public crate-agui_core-0.2.0 (c (n "agui_core") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "morphorm") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ra5sknv5vly4lr3siqflrmwkw6ikpxmbwp6v31839rc0pw7y37i")))

(define-public crate-agui_core-0.3.0 (c (n "agui_core") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lyon") (r "^0.17") (d #t) (k 0)) (d (n "morphorm") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16a6wmd6nmr7sy26h2892rkwl3j6n1xirqacakx01m2hh6nkj16s")))

