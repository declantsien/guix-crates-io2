(define-module (crates-io ag ui agui_macros) #:use-module (crates-io))

(define-public crate-agui_macros-0.1.0 (c (n "agui_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hf6b06mx7w8vfrlb8z2kj1ng4w9nan9v7fanr262iw0ivg7zvqj") (f (quote (("internal") ("default"))))))

(define-public crate-agui_macros-0.2.0 (c (n "agui_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k0j494c57r77w3vxq0cs4h92cnvmy2vqzhykygb1n05jy8mn5xg") (f (quote (("internal") ("default"))))))

(define-public crate-agui_macros-0.3.0 (c (n "agui_macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit" "visit-mut" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1xafxi6cbl3fsm3hsxpn0qhx82ybg8v2jijpch09nbxcccx9rhn9") (f (quote (("internal") ("default"))))))

