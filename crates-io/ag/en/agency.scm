(define-module (crates-io ag en agency) #:use-module (crates-io))

(define-public crate-agency-0.1.0 (c (n "agency") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "async_executors") (r "^0.4") (f (quote ("tokio_tp"))) (d #t) (k 0)) (d (n "async_nursery") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "119wj232mp6c7iqr3h5i6in39x5n398i5vsw40jqcbcmv3y8d0s5")))

(define-public crate-agency-0.2.0 (c (n "agency") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_executors") (r "^0.4") (f (quote ("tokio_tp"))) (d #t) (k 0)) (d (n "async_nursery") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0crxx28j06m5wf0j7dx7g667ldz8718jx0h2s5phsa2fxis39b3p")))

(define-public crate-agency-0.3.0 (c (n "agency") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "1whqrnn8gxgaxgh1xzz2mv75jvf496zxmmh9kx18rz057qsf2d9n")))

(define-public crate-agency-0.4.0 (c (n "agency") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qgrxi4i7vd4wy0fvy8xaps4fryswr20rilg7kagbqgwbm2gxkbn")))

(define-public crate-agency-0.4.1 (c (n "agency") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nqnly86v2wziqkn7ykl30a2gignl5ihs6y0k1qlpfddszln6v28")))

(define-public crate-agency-0.4.2 (c (n "agency") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z3s9nk15l8z1h4qv9pqw4qfbvsi77f88qmknijrz2v2wzgf2ld7")))

(define-public crate-agency-0.5.0 (c (n "agency") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "02nbw5n8971a62p8shq31c2jslc2crqqf3ksm2i49kq0q6nig8dg")))

(define-public crate-agency-0.5.1 (c (n "agency") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "10w5rckyk6qlpyph8rkxnrc816mcrs2f4knjklsfhvv47h1v7ik8")))

(define-public crate-agency-0.5.2 (c (n "agency") (v "0.5.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nm1pnmv1r7xfknyfbcay1a4p32l61zykb3w8bskhkccdiplkr9m")))

(define-public crate-agency-0.6.0 (c (n "agency") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "03w3cwym6bcrpk5irm996w7q1ikkjbrn0nf8dazi4ia5xq6j0nm0")))

