(define-module (crates-io ag en agentx) #:use-module (crates-io))

(define-public crate-agentx-0.1.0 (c (n "agentx") (v "0.1.0") (h "0215cdhjbbwdnmkjwf02scawxwq2x0c8b7j9gx4mdclfz23j27y4")))

(define-public crate-agentx-0.1.1 (c (n "agentx") (v "0.1.1") (h "1ml25byjhhc4n6qv1ryqh44n71pcxsjqar5lfhy68dw2122xazvp")))

