(define-module (crates-io ag en agentscript) #:use-module (crates-io))

(define-public crate-agentscript-0.1.1 (c (n "agentscript") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "00hm62rnp7h6rhipx5f2azwbgx502fha5j06njxnb0ml7q40ap6b")))

(define-public crate-agentscript-0.1.2 (c (n "agentscript") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0ncp9waqgizzrizfdkkwiwpdh34zqc6wbzicrlsbk887imsj78l9")))

