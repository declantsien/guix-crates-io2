(define-module (crates-io ag en agent_rt) #:use-module (crates-io))

(define-public crate-agent_rt-0.1.0 (c (n "agent_rt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-channel") (r "^2.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.3.0") (d #t) (k 0)) (d (n "wd_tools") (r "^0.10.0") (f (quote ("point-free" "sync" "ptr" "md5" "hex" "time"))) (d #t) (k 0)))) (h "1jk81valpz1dr603b47gy1cwajjmdap4kvn4lwrz1k3i2saxjbz8")))

