(define-module (crates-io ag no agnostik-attributes) #:use-module (crates-io))

(define-public crate-agnostik-attributes-1.1.1 (c (n "agnostik-attributes") (v "1.1.1") (d (list (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j7q5crc02g1wkkgszykdz9am3znq3al4qwaa4sy5rxiv81srmfa") (y #t)))

(define-public crate-agnostik-attributes-1.2.0 (c (n "agnostik-attributes") (v "1.2.0") (d (list (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjkpppim9b3p78z4irlnc7z69v72g02smvplp5rls00z1wcfzdq")))

