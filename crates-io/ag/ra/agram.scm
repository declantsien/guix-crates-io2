(define-module (crates-io ag ra agram) #:use-module (crates-io))

(define-public crate-agram-1.0.0 (c (n "agram") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)))) (h "1px6116r3p2rk3s1yyb1cwzcai9qqy4yrm5lsimar8ia0sx0bd6k")))

(define-public crate-agram-1.0.1 (c (n "agram") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)))) (h "05ibn0crq8bsqh2hx1mwq8i988l9pyhnm999rq2fh0lbn21pfa30")))

