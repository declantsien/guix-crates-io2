(define-module (crates-io ag br agbrs_flash) #:use-module (crates-io))

(define-public crate-agbrs_flash-0.1.0 (c (n "agbrs_flash") (v "0.1.0") (d (list (d (n "agb") (r "^0.11.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)))) (h "0wp2mxrmp7lb05wn03ww2jqzlhing2nxda2ahx4spd4hgmhdshyi") (y #t)))

(define-public crate-agbrs_flash-0.1.1 (c (n "agbrs_flash") (v "0.1.1") (d (list (d (n "agb") (r "^0.11.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)))) (h "01992scvda4cvv6bb7prq5f8q9bvbbmbkv6s8b9qyxgyxfbjpl90") (y #t)))

(define-public crate-agbrs_flash-0.2.0 (c (n "agbrs_flash") (v "0.2.0") (d (list (d (n "agb") (r "^0.11.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)))) (h "01461lxhlpa1hdqw6iw67cdr75z54g9pbjn0gn9j5rs97a3jsx9w")))

(define-public crate-agbrs_flash-0.2.1 (c (n "agbrs_flash") (v "0.2.1") (d (list (d (n "agb") (r "^0.11.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)))) (h "1v4ga54rmq6qad152i22lkr2ssb48w336s3zs9dcmm8w1k34v7qz")))

