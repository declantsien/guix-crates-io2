(define-module (crates-io ag pm agpm_project) #:use-module (crates-io))

(define-public crate-agpm_project-0.0.1 (c (n "agpm_project") (v "0.0.1") (d (list (d (n "amisgitpm") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hc1y73qyhmlgx3pbsv06b55avb9gdmz9fz8fj500f8qcrabdrc4") (s 2) (e (quote (("serde" "dep:serde"))))))

