(define-module (crates-io ag pm agpm_pm) #:use-module (crates-io))

(define-public crate-agpm_pm-0.0.1 (c (n "agpm_pm") (v "0.0.1") (d (list (d (n "amisgitpm") (r "^0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17jhq6d76c2ljlmqdbmb5b1w3hf3yh4ckm47lnxv30a5lmsrnlbv")))

(define-public crate-agpm_pm-0.0.2 (c (n "agpm_pm") (v "0.0.2") (d (list (d (n "amisgitpm") (r "^0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qjvvh4sqi3v6jlrhpby00642jk9b4bbpmjb8ppgxnnh1k81xz3q")))

