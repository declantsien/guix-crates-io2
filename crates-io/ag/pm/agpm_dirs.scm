(define-module (crates-io ag pm agpm_dirs) #:use-module (crates-io))

(define-public crate-agpm_dirs-0.0.1 (c (n "agpm_dirs") (v "0.0.1") (d (list (d (n "agpm_suggestions") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "amisgitpm") (r "^0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vlak5ff60vm6i9iav3vap7wz02vqap6xn1waxn13g61shmq7cxp") (f (quote (("suggestions" "agpm_suggestions"))))))

