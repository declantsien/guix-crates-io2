(define-module (crates-io ag pm agpm_interactions) #:use-module (crates-io))

(define-public crate-agpm_interactions-0.0.1 (c (n "agpm_interactions") (v "0.0.1") (d (list (d (n "agpm_pm") (r "^0.0") (d #t) (k 0)) (d (n "agpm_project") (r "^0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "agpm_suggestions") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "amisgitpm") (r "^0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h8xi9c20s2hgagv08jpvbq6c3nmxcx6l0zv8qywa4hx9387q6jq") (f (quote (("suggestions" "agpm_suggestions"))))))

