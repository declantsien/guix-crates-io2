(define-module (crates-io ag ni agni) #:use-module (crates-io))

(define-public crate-agni-0.1.0 (c (n "agni") (v "0.1.0") (h "1v8dvflbwqpxc76din1ipakv687nlanjh88rnwc4ns8lp5qb1j8f") (y #t)))

(define-public crate-agni-0.1.1 (c (n "agni") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "0c0vwv8cq44iw3vby4z73h2cry8bqd8w70lzk8nalb7zmnbjb90w") (f (quote (("io_pbrt" "pest" "pest_derive") ("default" "cli" "io_pbrt") ("cli" "clap"))))))

(define-public crate-agni-0.1.2 (c (n "agni") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (o #t) (d #t) (k 0)))) (h "118y0l9qxjjq1cfd7229k1cjb0775pgs525x9b60qy8z3i4j2adz") (f (quote (("io_pbrt" "pest" "pest_derive") ("default" "cli" "io_pbrt") ("cli" "clap"))))))

