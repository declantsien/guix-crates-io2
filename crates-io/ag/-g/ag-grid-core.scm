(define-module (crates-io ag -g ag-grid-core) #:use-module (crates-io))

(define-public crate-ag-grid-core-0.2.0 (c (n "ag-grid-core") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlElement" "Window"))) (d #t) (k 0)))) (h "0c3znak8bmlck3drqndcc86lq7abl24iapkg3gpwzvz2534wgiy1")))

(define-public crate-ag-grid-core-0.2.1 (c (n "ag-grid-core") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlElement" "Window"))) (d #t) (k 0)))) (h "1g9s5czzm3b3249dql1w8pr34lvr281g4f38nscbam9sab3vw81h")))

(define-public crate-ag-grid-core-0.2.2 (c (n "ag-grid-core") (v "0.2.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "HtmlElement" "Window"))) (d #t) (k 0)))) (h "1f236zci1mnvfxb4xb56i8hdiw1x1p5xh0jbhw87shh7p2fiv2qz")))

