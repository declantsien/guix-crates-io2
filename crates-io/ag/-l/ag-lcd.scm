(define-module (crates-io ag -l ag-lcd) #:use-module (crates-io))

(define-public crate-ag-lcd-0.1.0 (c (n "ag-lcd") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "179r2lhbki62vbk92l0n0d62z08x8l7rbmln7z3v7gs1l6crd7pm")))

(define-public crate-ag-lcd-0.1.1 (c (n "ag-lcd") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "00vgjv91yl1vn5bmkjdjhnr56na7rqrh8fn2v2fb3sw73mmiydi1")))

(define-public crate-ag-lcd-0.1.2 (c (n "ag-lcd") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1wnnaf8pr5a04gppilckyv199qm7crl5b27xf8l33yjxbdgljwc5")))

(define-public crate-ag-lcd-0.2.0 (c (n "ag-lcd") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "port-expander") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "shared-bus") (r "^0.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1z6d0ksj737fyrzkn5cmzhnimy6jmh1mhpki5plghh56iwca6zzy") (f (quote (("i2c" "port-expander") ("avr-hal"))))))

