(define-module (crates-io ag le aglet) #:use-module (crates-io))

(define-public crate-aglet-0.1.0 (c (n "aglet") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1k7vncf86jhcx4f1w9mgsdk0ys0z94hqiwibd3d98frxhi1b76mc")))

(define-public crate-aglet-0.1.1 (c (n "aglet") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0r9dvwswqdsh2s07glsk3v9kcvbi28n1h8rirzd196fhmddk6fy1")))

(define-public crate-aglet-0.2.0 (c (n "aglet") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d84k7hp8fyzk9q5npwfn900z83xybg3nahzslzy0cg37p4ydy97") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.3.0 (c (n "aglet") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zzlkdsl89w86mc49h5yz9fshq6j4pcgc8ndays32hlh0jnbs4ni") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.3.1 (c (n "aglet") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xzqdb7nziggig6lj3b5pi5sj2w4rx22bmdrzdq7fykqq4flb00m") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.3.2 (c (n "aglet") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0czrz3z364rjhhdcmafgv1frbjshgm011jq3wdp9pc1r03pskc5d") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.3.3 (c (n "aglet") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vmk2sb9n997sqap77y8hbslh21zwa6w9bkxznrrh02xgiyd3yca") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.3.4 (c (n "aglet") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0infmn9dkclygkrlc2radx4hzhdribb6z4mhsfhd3adw82my7dis") (f (quote (("default" "serde"))))))

(define-public crate-aglet-0.4.0 (c (n "aglet") (v "0.4.0") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02jamdyaxd35zdv5wzvhfwz8506dznbs79xvv12wpa9vsdmd1pbk") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5.0 (c (n "aglet") (v "0.5.0") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0db5cdrwcpca2yl2dhvy3jh0q8c1fvls36wcygcfzn52knfg11k3") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5.1 (c (n "aglet") (v "0.5.1") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xymfbbnj8c73nnmjj6zaiv2bfx1l7qwsn3225zswpvvcd9r6j42") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5.2 (c (n "aglet") (v "0.5.2") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m0rz8wkzfd95c4kk0wcab8qaq186as58i85bcplb6yr3bma6bg7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "enumflags2/serde"))))))

