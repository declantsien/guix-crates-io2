(define-module (crates-io ag _f ag_file_system_scanner) #:use-module (crates-io))

(define-public crate-ag_file_system_scanner-0.1.0 (c (n "ag_file_system_scanner") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1cj5php2zazpnns5ax0xvpi220afy7yqh6k53qp18skjbcd7h7wk") (y #t)))

(define-public crate-ag_file_system_scanner-0.1.1 (c (n "ag_file_system_scanner") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0pbgp68ygmnkfh1iimqsm8wpm51y8gc0g7alxlybwsmd5s865nzc")))

(define-public crate-ag_file_system_scanner-0.1.2 (c (n "ag_file_system_scanner") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0awvm7miv42b8hn14kdwsgdclbkl3cgb3xci82gx5y66mx2h0642")))

(define-public crate-ag_file_system_scanner-0.1.3 (c (n "ag_file_system_scanner") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0ik36gg7mdcvc06pzcjxhqsrhrgqx5kq4af35fq6vim01hcvgais")))

