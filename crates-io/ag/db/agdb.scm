(define-module (crates-io ag db agdb) #:use-module (crates-io))

(define-public crate-agdb-0.1.0 (c (n "agdb") (v "0.1.0") (h "1gv96ff742b7ss52gc9xci838fvx18fiisnz0q14pk55mhs6y33c")))

(define-public crate-agdb-0.2.0 (c (n "agdb") (v "0.2.0") (h "1777rwzi96nya39kl163nvd9qvxf9frfnkkylardrrjzi512aqra")))

(define-public crate-agdb-0.2.1 (c (n "agdb") (v "0.2.1") (h "0mag8h60rsc93s72z16qdfnj41i3l69rdahfd0bi34jvwn8qzagw")))

(define-public crate-agdb-0.3.0 (c (n "agdb") (v "0.3.0") (h "0qqzwkiviazhxybp9rd336bkxn9a1ifppvlzjzx43bs9k30bqb8n")))

(define-public crate-agdb-0.3.1 (c (n "agdb") (v "0.3.1") (h "0nxb5kar373q5i3zz4zh2s3nq06q5qqw0b7j6w0dwzs14rlfmxhv")))

(define-public crate-agdb-0.4.0 (c (n "agdb") (v "0.4.0") (d (list (d (n "agdb_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1j7ag53a6hsd9782lrb547sqgi5s580w6avn5s4cy6nb47vc21j0")))

(define-public crate-agdb-0.4.1 (c (n "agdb") (v "0.4.1") (d (list (d (n "agdb_derive") (r "^0.4.1") (d #t) (k 0)))) (h "1635b1g5p4l1slxj3vic0shg3r3g0avbnnlxk8nsq0v9xbiar8fr")))

(define-public crate-agdb-0.5.0 (c (n "agdb") (v "0.5.0") (d (list (d (n "agdb_derive") (r "^0.5.0") (d #t) (k 0)))) (h "12qvn7vyp27fmfcxvjv1j6zds7a737f6km39dkcb2lsdnzk4rd2j")))

(define-public crate-agdb-0.5.1 (c (n "agdb") (v "0.5.1") (d (list (d (n "agdb_derive") (r "^0.5.1") (d #t) (k 0)))) (h "111gv273kc0zg5r9lblwwq43wsb5r3c4wykp370xdydg8378aprj")))

(define-public crate-agdb-0.5.2 (c (n "agdb") (v "0.5.2") (d (list (d (n "agdb_derive") (r "^0.5.2") (d #t) (k 0)))) (h "0j4mh3r2kbnjy02f4c5da6z5qqnki9w0f3k164pvih4vh13nxvfc")))

(define-public crate-agdb-0.6.2 (c (n "agdb") (v "0.6.2") (d (list (d (n "agdb_derive") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "1lmqcc875nhfnswnyfjysl15n4qa95sg2fv7gjv4s5w5v35l3jxk") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.3 (c (n "agdb") (v "0.6.3") (d (list (d (n "agdb_derive") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0gdz13fqfxcx6q1kiipwmq4hcz2lj2g32nkyvygf8z47cdr22rmw") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.4 (c (n "agdb") (v "0.6.4") (d (list (d (n "agdb_derive") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0iddyv46jr6nxz6lpdk56vi477p49zg0xf63ad0sx48dfi0an3ja") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.5 (c (n "agdb") (v "0.6.5") (d (list (d (n "agdb_derive") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0r2lpljx18pj4y5bb38biyxdzri0dfiyxpy4p82xkrskmz5vi84h") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.6 (c (n "agdb") (v "0.6.6") (d (list (d (n "agdb_derive") (r "^0.6.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "18m498vlby62b76s0f1ww3rlm327hv4zdy85fkfznvszbjydz0yg") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.7 (c (n "agdb") (v "0.6.7") (d (list (d (n "agdb_derive") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0nzin01lhz08l4l9sl18cq287abm1jnp4hply5j4301aldxmk58p") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.8 (c (n "agdb") (v "0.6.8") (d (list (d (n "agdb_derive") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0gdxqhi70jhwznphnvh4ar50qflb1l6akpf7csjz5ccijx8zcacd") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

(define-public crate-agdb-0.6.9 (c (n "agdb") (v "0.6.9") (d (list (d (n "agdb_derive") (r "^0.6.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utoipa") (r "^4") (o #t) (d #t) (k 0)))) (h "0fybfmpsn9mjvvlj40np8ny6sy98fk67fbg0y6mvkqc5m8fp96ad") (f (quote (("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:utoipa") ("derive" "dep:agdb_derive"))))))

