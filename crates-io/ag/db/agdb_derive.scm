(define-module (crates-io ag db agdb_derive) #:use-module (crates-io))

(define-public crate-agdb_derive-0.3.1 (c (n "agdb_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1wdgrl9f62v4anrj1zjvj78l86dnq931ysxznmblycf0zrf6j698")))

(define-public crate-agdb_derive-0.4.0 (c (n "agdb_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0a77d1s0h17ljxw452ccwg2acbfshab8jv42jz4cyz0mhk3jrra9")))

(define-public crate-agdb_derive-0.4.1 (c (n "agdb_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "00gh5mjjl2lhvcw03k67kavfgimxwf1384n05w557rjvpgnn920i")))

(define-public crate-agdb_derive-0.5.0 (c (n "agdb_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0khay7fhvf4n7dzf1wlyv8vf9rp8j5071mafx4jjpqja2hxkhc72")))

(define-public crate-agdb_derive-0.5.1 (c (n "agdb_derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0qhqglx218cdj0anyi0glc08byirv9swrr8d7ylh5py2vnd8hnfa")))

(define-public crate-agdb_derive-0.5.2 (c (n "agdb_derive") (v "0.5.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "01xrz24lzpmda6g4gq22lv2xcyxrbjixyc1d2l7lxvhprbr9cs9j")))

(define-public crate-agdb_derive-0.6.2 (c (n "agdb_derive") (v "0.6.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1q9pz7c48hwjz1j3fjqv9c049f1la823ikpdafflsbycyp2qbsyp")))

(define-public crate-agdb_derive-0.6.3 (c (n "agdb_derive") (v "0.6.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05f6czlqa6v0w57jy4b9m8v0p920w2c6821mjmbb75415jjgqyyb")))

(define-public crate-agdb_derive-0.6.4 (c (n "agdb_derive") (v "0.6.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ry0404isb7dmxw3wgby11sga9lckl6lp0ciclbwwq3x06ihqq57")))

(define-public crate-agdb_derive-0.6.5 (c (n "agdb_derive") (v "0.6.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16z7srkv0k5sl9ncwzq0yj8zykxc1mza7q80j9c42gdgfpcvq7gw")))

(define-public crate-agdb_derive-0.6.6 (c (n "agdb_derive") (v "0.6.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0sydl4pgqxry121qnagnvc4wv34jdplzhnkparkn579a706y4jl8")))

(define-public crate-agdb_derive-0.6.7 (c (n "agdb_derive") (v "0.6.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0g22fjvygqyvnl24z27nq5vjv2j33gvr274aap34ixklgwycc8fk")))

(define-public crate-agdb_derive-0.6.8 (c (n "agdb_derive") (v "0.6.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pbby6kkbjd4kr23grklcdhsj1nby704xsnlnhhlpppbki2rwh8x")))

(define-public crate-agdb_derive-0.6.9 (c (n "agdb_derive") (v "0.6.9") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1shdfyf3d6bhmgwdfba6sfnrrcs4mpb5v4vdd3dgv2wkyin4x4kn")))

