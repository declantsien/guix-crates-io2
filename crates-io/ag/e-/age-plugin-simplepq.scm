(define-module (crates-io ag e- age-plugin-simplepq) #:use-module (crates-io))

(define-public crate-age-plugin-simplepq-0.1.0 (c (n "age-plugin-simplepq") (v "0.1.0") (d (list (d (n "age-plugin-hpke") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)))) (h "1hl0x37p7dxsg1wvqk75hk1hhyn1sdm9h7b9ha419fdcrj197zsk")))

(define-public crate-age-plugin-simplepq-0.1.1 (c (n "age-plugin-simplepq") (v "0.1.1") (d (list (d (n "age-plugin-hpke") (r "^0.1.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)))) (h "0wjdmwb5r1hdkyx9jlshp7j0j2f116n10r4zvrxgrjyc2slgiyha")))

(define-public crate-age-plugin-simplepq-0.1.2 (c (n "age-plugin-simplepq") (v "0.1.2") (d (list (d (n "age-plugin-hpke") (r "^0.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)))) (h "0sa142s087w7yynhvwpna29n08nbcjga4dw6r4g10i0zkr053ydj")))

