(define-module (crates-io ag e- age-plugin-tlock) #:use-module (crates-io))

(define-public crate-age-plugin-tlock-0.1.1 (c (n "age-plugin-tlock") (v "0.1.1") (d (list (d (n "age") (r "^0.9") (d #t) (k 0)) (d (n "age-core") (r "^0.9") (d #t) (k 0)) (d (n "age-plugin") (r "^0.4.0") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "drand_core") (r "^0.0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tlock_age") (r "^0.0.5") (f (quote ("internal"))) (d #t) (k 0)))) (h "05p84fi31sn7qxn3jj9hmx1jfmm9p8ism6821dwwpl5qgw54fbxb")))

