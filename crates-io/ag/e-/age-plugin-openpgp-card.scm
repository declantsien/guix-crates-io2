(define-module (crates-io ag e- age-plugin-openpgp-card) #:use-module (crates-io))

(define-public crate-age-plugin-openpgp-card-0.1.0 (c (n "age-plugin-openpgp-card") (v "0.1.0") (d (list (d (n "age-core") (r "^0.10.0") (d #t) (k 0)) (d (n "age-plugin") (r "^0.5.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "card-backend-pcsc") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openpgp-card") (r "^0.4.2") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "1lnxk02pi0hksf446dwbd3wr7vg1wws0b8g8wqx6jmhj5kij0s69")))

