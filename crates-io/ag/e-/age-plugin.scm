(define-module (crates-io ag e- age-plugin) #:use-module (crates-io))

(define-public crate-age-plugin-0.0.0 (c (n "age-plugin") (v "0.0.0") (h "0n7084s5gmzyxaxkq9x5p9wja1igf2b03mykbphknbim7nlyrs2q")))

(define-public crate-age-plugin-0.1.0 (c (n "age-plugin") (v "0.1.0") (d (list (d (n "age-core") (r "^0.6.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)) (d (n "secrecy") (r "^0.7") (d #t) (k 0)))) (h "1b6vp6rcw15m7mmh6dpkl8nyigbr6lr67gp8f3fnn25nz9784rmb")))

(define-public crate-age-plugin-0.2.0 (c (n "age-plugin") (v "0.2.0") (d (list (d (n "age-core") (r "^0.7.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)))) (h "12hljhgsn6bphw3rfmj0y9azjxym9928056q73kycjf3fasm96b6")))

(define-public crate-age-plugin-0.2.1 (c (n "age-plugin") (v "0.2.1") (d (list (d (n "age-core") (r "^0.7.1") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)))) (h "0avfqcfnwq90s0j5awl960ml69szpkfbj2w851hvkq4isna9w13l")))

(define-public crate-age-plugin-0.3.0 (c (n "age-plugin") (v "0.3.0") (d (list (d (n "age-core") (r "^0.8.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bech32") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)))) (h "0srlwbcsn8ad44iiycq1p0px0qn9qnlwz3c5dbhb393aa4jw6vdd")))

(define-public crate-age-plugin-0.4.0 (c (n "age-plugin") (v "0.4.0") (d (list (d (n "age-core") (r "^0.9.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 2)))) (h "0ydwp3z0x2xd1ppbcmrqh0rr3y0dk4zrnazf11z99ibgfhyvjr25") (r "1.59")))

(define-public crate-age-plugin-0.5.0 (c (n "age-plugin") (v "0.5.0") (d (list (d (n "age-core") (r "^0.10.0") (f (quote ("plugin"))) (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "17gb57sa6vidvwxxbx8cjmznlfm2m2289gygykm0c3mc7achsx04") (r "1.65")))

