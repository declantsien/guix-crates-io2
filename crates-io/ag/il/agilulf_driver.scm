(define-module (crates-io ag il agilulf_driver) #:use-module (crates-io))

(define-public crate-agilulf_driver-0.1.0 (c (n "agilulf_driver") (v "0.1.0") (d (list (d (n "agilulf") (r "^0.1.0") (d #t) (k 2)) (d (n "agilulf_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.17") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "romio") (r "^0.3.0-alpha.9") (d #t) (k 0)))) (h "0qba5p0biaddidl9pgynmhxzqcc1f31vczdzwhw1nbj3jqz4r1vp")))

