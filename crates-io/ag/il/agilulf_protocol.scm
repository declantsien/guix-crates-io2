(define-module (crates-io ag il agilulf_protocol) #:use-module (crates-io))

(define-public crate-agilulf_protocol-0.1.0 (c (n "agilulf_protocol") (v "0.1.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.17") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.9") (d #t) (k 0)))) (h "1lz7g0xh881a17bvvm925k9p4j1zgv1qv8v24sbrc42b3zqbg5mr")))

