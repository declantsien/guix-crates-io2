(define-module (crates-io ag il agilulf_fs) #:use-module (crates-io))

(define-public crate-agilulf_fs-0.1.0 (c (n "agilulf_fs") (v "0.1.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.17") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1snqdwnh6k6xz7yn4sy8clw04qbbx41w493165947zwks43bs73d")))

