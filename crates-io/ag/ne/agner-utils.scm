(define-module (crates-io ag ne agner-utils) #:use-module (crates-io))

(define-public crate-agner-utils-0.2.5 (c (n "agner-utils") (v "0.2.5") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hmdipyc928d0wa7jxjqy9f4gdfpinsc5whwc26n2vxl9q0dnlf9")))

(define-public crate-agner-utils-0.2.6 (c (n "agner-utils") (v "0.2.6") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1i7cjz2z5nm4z7an98nyi3l6yr12wf1bd7jrrqxxma5vf77qj6yl")))

(define-public crate-agner-utils-0.2.7 (c (n "agner-utils") (v "0.2.7") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "084dw3lvngvvwrwdnk9p2kdpzsifaczk1fzpnchdwjc3kwrl87ns")))

(define-public crate-agner-utils-0.2.8 (c (n "agner-utils") (v "0.2.8") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "11jc6879nbl8zd1qbqivagvlhy6qmkdjj0bspm2h8ami6zygs4d3")))

(define-public crate-agner-utils-0.2.9 (c (n "agner-utils") (v "0.2.9") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n04n1aijgkhnkcin8xv42r70lbfwgv7cschlxdnkl7sk5syqmvy")))

(define-public crate-agner-utils-0.2.10 (c (n "agner-utils") (v "0.2.10") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0r5vn343pg9k5qvmjfpyj5y0ibnhpvgph6j5sl6b7ibw1jjkdywv")))

(define-public crate-agner-utils-0.3.0 (c (n "agner-utils") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1qi5x40k59hlzpih9pgszmq8k0ik78jy7f7i06pmkkqrwysshnqd")))

(define-public crate-agner-utils-0.3.1 (c (n "agner-utils") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0b0md1g1v50k277rld5wj7m60wnwp6ny77dj1h82ykra6912xcfc")))

(define-public crate-agner-utils-0.3.2 (c (n "agner-utils") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1x7rzljnw7m21nim1z3gz23fg7ql0nzqpf0lsr7z74igds642z0v")))

(define-public crate-agner-utils-0.3.3 (c (n "agner-utils") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "18slw9vsbjjihfnr3x1wrb31zyx4cvgr680lfzk4f2310kdhcws9")))

(define-public crate-agner-utils-0.3.4 (c (n "agner-utils") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "164365r2vw9yjkpmf6gnbdii7hgrvxg6ic05r757ijjqx2nrc39v")))

(define-public crate-agner-utils-0.3.5 (c (n "agner-utils") (v "0.3.5") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0hxbkik75n0cikrvd1rw345g48bfb6wic5mkv6va0a958hhh3ial")))

(define-public crate-agner-utils-0.3.6 (c (n "agner-utils") (v "0.3.6") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1iwdz4kn0gfi6hbx242zp8n3jfc1yzlk30g4vihn41hg83g9fnc1")))

(define-public crate-agner-utils-0.3.7 (c (n "agner-utils") (v "0.3.7") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1w4xzrazldkk5433zhlq2mvbpbd4mbl6z1rd9pa75yq3p2swm66w")))

(define-public crate-agner-utils-0.3.8 (c (n "agner-utils") (v "0.3.8") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "17kzkllvx4n26836f9ws786a4w9lhl3fmigjyq6d1nqd4cy9qif3")))

(define-public crate-agner-utils-0.3.9 (c (n "agner-utils") (v "0.3.9") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "08xf7k3gi4p703w7rx6wdsxig34r34s7y16ykfaw45nca2c5hrx6")))

(define-public crate-agner-utils-0.3.10 (c (n "agner-utils") (v "0.3.10") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1vsjm70qi26r1f5cp4vqs6rzmyb551sf9mwfm8dna7swn0c709zg")))

(define-public crate-agner-utils-0.3.11 (c (n "agner-utils") (v "0.3.11") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "06msrlmklhp0si07czxr6kfz4ajfyn9wbp8nshizqqx565qnqnl3")))

(define-public crate-agner-utils-0.3.12 (c (n "agner-utils") (v "0.3.12") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0c5irnd648amvwlw86ngn4y5a2jiyy784fxajkgf22jbmbm2xg7d")))

(define-public crate-agner-utils-0.3.13 (c (n "agner-utils") (v "0.3.13") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1xs5rbxxv2qhxdj8fwvnmsllz8jnb2i708l9yzhcz6lzjhfynlhj")))

(define-public crate-agner-utils-0.3.14 (c (n "agner-utils") (v "0.3.14") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0j6qi7cblwvs1j8k9xs8s9yc6av5y8jvwaksr6dq5cv9qk9jf8zn")))

(define-public crate-agner-utils-0.3.15 (c (n "agner-utils") (v "0.3.15") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0qwsyb3j0agr4m6rqsbv7np6mv6wjiz4kjfblf0kd8np2gs76yh8")))

(define-public crate-agner-utils-0.3.16 (c (n "agner-utils") (v "0.3.16") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "123v4bxrwpxvk0yyfk002rp4i4zzc3ngpmci6vllxfc4syyp45wj")))

(define-public crate-agner-utils-0.3.17 (c (n "agner-utils") (v "0.3.17") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "04f2mlwnn654piak9vsxykqqfv8bmshl51fhcg8jjp4f8219vkl2") (y #t)))

(define-public crate-agner-utils-0.4.0 (c (n "agner-utils") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1xmlqarah16vzz43kmkbapvpmnid37s455fbr838x8dw2ss5amsk")))

(define-public crate-agner-utils-0.4.1 (c (n "agner-utils") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("bilock" "unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1xncmz71rqij7gds096azz92bxqvysnhhviqsf07ddm2g8z46ji0")))

