(define-module (crates-io ag ne agner-reg) #:use-module (crates-io))

(define-public crate-agner-reg-0.3.9 (c (n "agner-reg") (v "0.3.9") (d (list (d (n "agner-actors") (r "=0.3.9") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "07815dgj453qqh8j06mmsj2ffb8glqrgryr70jwb7p29f0n2h28i")))

(define-public crate-agner-reg-0.3.10 (c (n "agner-reg") (v "0.3.10") (d (list (d (n "agner-actors") (r "=0.3.10") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1jhgr1cg2dggqkf17n14wk572jg3wyagi88rlwn1jpb3ywgsah6n")))

(define-public crate-agner-reg-0.3.11 (c (n "agner-reg") (v "0.3.11") (d (list (d (n "agner-actors") (r "=0.3.11") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1zxcmgvgc3shxn740s56q6h6c3i4hx57jvidc4ykrfgkly1ax65w")))

(define-public crate-agner-reg-0.3.12 (c (n "agner-reg") (v "0.3.12") (d (list (d (n "agner-actors") (r "=0.3.12") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1ng8nxyfv31p8p277321jknz1kdsqh8m1clfh1baygy7n870a3by")))

(define-public crate-agner-reg-0.3.13 (c (n "agner-reg") (v "0.3.13") (d (list (d (n "agner-actors") (r "=0.3.13") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "05nvk753wn6bxzm0sywr31x6vyxgib0qx1jfgp35sc07l6j4l12r")))

(define-public crate-agner-reg-0.3.14 (c (n "agner-reg") (v "0.3.14") (d (list (d (n "agner-actors") (r "=0.3.14") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "03ma3npn061lgbnxs2x6b2zlsk3kcyv3nzsjnfzhqlpmn7pm2vk8")))

(define-public crate-agner-reg-0.3.15 (c (n "agner-reg") (v "0.3.15") (d (list (d (n "agner-actors") (r "=0.3.15") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1l5d7sxbvbakc1pbkjh2h95nd01gsm8gb6n4jkcikwb25q89wmww")))

(define-public crate-agner-reg-0.3.16 (c (n "agner-reg") (v "0.3.16") (d (list (d (n "agner-actors") (r "=0.3.16") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1rcjdn75x9nvaycj3f9y66hqwcay28laakn2pnvjc5s4np8j8d5i")))

(define-public crate-agner-reg-0.3.17 (c (n "agner-reg") (v "0.3.17") (d (list (d (n "agner-actors") (r "=0.3.17") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0xwxqh4bcxf8fpjrqapg90qi2m2430ngkimb7qngbkvk1c0vvw9m") (y #t)))

(define-public crate-agner-reg-0.4.0 (c (n "agner-reg") (v "0.4.0") (d (list (d (n "agner-actors") (r "=0.4.0") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1b3387gg4x4lms90i09l6xb04bw5q6bfw2wvpxsv6zc7d0q7bbmx")))

(define-public crate-agner-reg-0.4.1 (c (n "agner-reg") (v "0.4.1") (d (list (d (n "agner-actors") (r "=0.4.1") (d #t) (k 0)) (d (n "arc-swap") (r "^1") (f (quote ("weak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0zbdcq6mcvcm03iknsbkwm5s1ys66y81d443dyjk5bwlhkyj08bk")))

