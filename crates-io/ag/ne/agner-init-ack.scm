(define-module (crates-io ag ne agner-init-ack) #:use-module (crates-io))

(define-public crate-agner-init-ack-0.3.8 (c (n "agner-init-ack") (v "0.3.8") (d (list (d (n "agner-actors") (r "=0.3.8") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "01dai8iy4w2dxcxfn625c1z7h5zzpc4ch8pjalqpzxrswqryls9y")))

(define-public crate-agner-init-ack-0.3.9 (c (n "agner-init-ack") (v "0.3.9") (d (list (d (n "agner-actors") (r "=0.3.9") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "10dfiz8x8gvzljqly434nrhy3rkj87l7hinijx10n02w8lmwxm0d")))

(define-public crate-agner-init-ack-0.3.10 (c (n "agner-init-ack") (v "0.3.10") (d (list (d (n "agner-actors") (r "=0.3.10") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "05ai3n8a9y5macj0lz6gam2pwyh3cwlcssljqmnqza8vvrakpdh0")))

(define-public crate-agner-init-ack-0.3.11 (c (n "agner-init-ack") (v "0.3.11") (d (list (d (n "agner-actors") (r "=0.3.11") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0jk9mx22wybr5lmsxgifmnq9aair4k8rbvgn4aqzzhckkccs49l2")))

(define-public crate-agner-init-ack-0.3.12 (c (n "agner-init-ack") (v "0.3.12") (d (list (d (n "agner-actors") (r "=0.3.12") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.12") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1ia26wicm4hjr7iz42ww3whl8fn4dr9wcm3cxnrfp1rz6ny6w0y4")))

(define-public crate-agner-init-ack-0.3.13 (c (n "agner-init-ack") (v "0.3.13") (d (list (d (n "agner-actors") (r "=0.3.13") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0mhb80vaal9vmgad6wwdyxb88v3d76629093n5w30n08nh15a4f2")))

(define-public crate-agner-init-ack-0.3.14 (c (n "agner-init-ack") (v "0.3.14") (d (list (d (n "agner-actors") (r "=0.3.14") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.14") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "13bh38sr674v2vl4k98xnngmwbnz95dzbrncnrb0gm6dg4ybyxaf")))

(define-public crate-agner-init-ack-0.3.15 (c (n "agner-init-ack") (v "0.3.15") (d (list (d (n "agner-actors") (r "=0.3.15") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.15") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0dxwrdii0dpd2fvv2gdab5756sv4w143k30zzcqm5j3lqhbms6rw")))

(define-public crate-agner-init-ack-0.3.16 (c (n "agner-init-ack") (v "0.3.16") (d (list (d (n "agner-actors") (r "=0.3.16") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.16") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1pz74225byjnnsmgcymxiyclad4wazavcfrzxjrlqz4hcxn4w3hq")))

(define-public crate-agner-init-ack-0.3.17 (c (n "agner-init-ack") (v "0.3.17") (d (list (d (n "agner-actors") (r "=0.3.17") (d #t) (k 0)) (d (n "agner-utils") (r "=0.3.17") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1k6w936alznffv0k5n8ikpgjbgadskdp8wd172bs59mgqvbxancd") (y #t)))

(define-public crate-agner-init-ack-0.4.0 (c (n "agner-init-ack") (v "0.4.0") (d (list (d (n "agner-actors") (r "=0.4.0") (d #t) (k 0)) (d (n "agner-utils") (r "=0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "168bisyl39nf6038c1x3cx3hax9h4grck9r65xkfsfrmnqs7zkw2")))

(define-public crate-agner-init-ack-0.4.1 (c (n "agner-init-ack") (v "0.4.1") (d (list (d (n "agner-actors") (r "=0.4.1") (d #t) (k 0)) (d (n "agner-utils") (r "=0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "14y2xm68019jnm4bd1891bnldy0dcx8nx67w5yjhwp49xiz12n8a")))

