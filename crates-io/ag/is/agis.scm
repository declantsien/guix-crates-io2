(define-module (crates-io ag is agis) #:use-module (crates-io))

(define-public crate-agis-0.2.0 (c (n "agis") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tree_magic_mini") (r "^3.0") (d #t) (k 0)))) (h "1p639azcpp5pxm7dl5l0x6rjj35ph4cavmds9xk1psv84sr3wbzj")))

(define-public crate-agis-0.4.0 (c (n "agis") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tree_magic_mini") (r "^3.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1jcsqp1q4kyrhgz1yah7as2mc7a2bai2hk246j55qly0h44855ml")))

