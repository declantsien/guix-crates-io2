(define-module (crates-io ag ar agar) #:use-module (crates-io))

(define-public crate-agar-0.0.1 (c (n "agar") (v "0.0.1") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)))) (h "0r0n3nwx80yac583bplvmzhhd8b9dnqz5fm2757xs6gjyvm69fly")))

(define-public crate-agar-0.0.2 (c (n "agar") (v "0.0.2") (h "02ag6h69dns5gvsn9mywd4gibm3z5bs7ng3mbvdd70fxfh6klpjm")))

