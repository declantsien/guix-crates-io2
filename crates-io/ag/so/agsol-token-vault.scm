(define-module (crates-io ag so agsol-token-vault) #:use-module (crates-io))

(define-public crate-agsol-token-vault-0.0.0-alpha (c (n "agsol-token-vault") (v "0.0.0-alpha") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.0") (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00qwn3y9n57h8wrhhi9inpmzzll9fd1zyshlxzhpb5hcxmn7a95s") (f (quote (("test-bpf") ("no-entrypoint"))))))

