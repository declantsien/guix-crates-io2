(define-module (crates-io ag so agsol-wasm-factory) #:use-module (crates-io))

(define-public crate-agsol-wasm-factory-0.0.1 (c (n "agsol-wasm-factory") (v "0.0.1") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 2)) (d (n "borsh-derive") (r "^0.9") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "solana-program") (r "^1.9.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)))) (h "1i6scb2hp3raa2g7hnw2cjkyy3k285j3598dxl7xs03pjq7835bq")))

