(define-module (crates-io ag so agsol-common-derive) #:use-module (crates-io))

(define-public crate-agsol-common-derive-0.0.1 (c (n "agsol-common-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q9ik83xvd7fn3nxk0hjicd39f1g66b2lv24ifcip9zqkzfhp2cn")))

