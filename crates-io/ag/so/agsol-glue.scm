(define-module (crates-io ag so agsol-glue) #:use-module (crates-io))

(define-public crate-agsol-glue-0.0.1 (c (n "agsol-glue") (v "0.0.1") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vbpfv53birj0arl3hdhnxfvw1rmyr7qa158xy1ypk2p0dg32i1w")))

(define-public crate-agsol-glue-0.1.0-alpha (c (n "agsol-glue") (v "0.1.0-alpha") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ya88l9dsf2az1cbyqf1fr2f7d10i4syncmvd75s41p1ib9r17v8")))

(define-public crate-agsol-glue-0.1.0 (c (n "agsol-glue") (v "0.1.0") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1pz1zivikcdv15sc06shbf4w61fwkrjx3gvlpsf1b1s33wj4y8bm")))

(define-public crate-agsol-glue-0.1.1 (c (n "agsol-glue") (v "0.1.1") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qfmgk1jcq1m14v8snvq9q1j89zkmqr8jnmbhxwa065ngz0mlss0")))

(define-public crate-agsol-glue-0.1.2-alpha (c (n "agsol-glue") (v "0.1.2-alpha") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ba7yf3vk5njyjlgy4nhnqbg6x8p3y0gq51xwwgf2bnjqsymhjg4")))

(define-public crate-agsol-glue-0.1.2-alpha.1 (c (n "agsol-glue") (v "0.1.2-alpha.1") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hs3mn91ra100hlfk782cik0rhsxfh1zbyic6in46bap4ak9zfnd")))

(define-public crate-agsol-glue-0.1.2-alpha.2 (c (n "agsol-glue") (v "0.1.2-alpha.2") (d (list (d (n "agsol-borsh-schema") (r "^0.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08m1p9nwqn8ksk43c6gggj4q9rmrf1gn201mnqgj5x0idx2y72x1")))

