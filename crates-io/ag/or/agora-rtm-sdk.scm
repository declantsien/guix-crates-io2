(define-module (crates-io ag or agora-rtm-sdk) #:use-module (crates-io))

(define-public crate-agora-rtm-sdk-0.1.0 (c (n "agora-rtm-sdk") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0yyp730h1g1k1i0apz49n29rhqskhjmr33hmzqds3i8nm2lra26i")))

(define-public crate-agora-rtm-sdk-0.1.1 (c (n "agora-rtm-sdk") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0apd3d897dpcr75zghyn2iwnqjlfikkkiiak3dapyak2avascyrs")))

