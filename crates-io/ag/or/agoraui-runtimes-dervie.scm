(define-module (crates-io ag or agoraui-runtimes-dervie) #:use-module (crates-io))

(define-public crate-agoraui-runtimes-dervie-0.1.0 (c (n "agoraui-runtimes-dervie") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k5nkyv1by1ama6s4rdpm544pgbhidhna3yrn9i87n7nzar07nkb")))

(define-public crate-agoraui-runtimes-dervie-0.1.1 (c (n "agoraui-runtimes-dervie") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1k3in7xz0qxqlvgqbra0hw47la72bgmhx6vfyg2x0mx7bz56xrxl")))

(define-public crate-agoraui-runtimes-dervie-0.1.2 (c (n "agoraui-runtimes-dervie") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1da0ac4dm5xh7m4pz4frwv0k20cv69av39srgd6nd12bw2mhj2ba")))

