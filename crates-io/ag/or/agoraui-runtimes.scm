(define-module (crates-io ag or agoraui-runtimes) #:use-module (crates-io))

(define-public crate-agoraui-runtimes-0.1.0 (c (n "agoraui-runtimes") (v "0.1.0") (d (list (d (n "agoraui-runtimes-dervie") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.0") (d #t) (k 0)))) (h "0x845dwps8vr0c0x69k1cijdm63h8a4r8nv8af5f9a6f6ndr6yr7") (f (quote (("derive" "agoraui-runtimes-dervie") ("default" "derive"))))))

(define-public crate-agoraui-runtimes-0.1.1 (c (n "agoraui-runtimes") (v "0.1.1") (d (list (d (n "agoraui-runtimes-dervie") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.1") (d #t) (k 0)))) (h "0m6dmpch7i93hvkmflcizqcdcmh629awcwwrk70fnr4fk0i2lw9w") (f (quote (("derive" "agoraui-runtimes-dervie") ("default" "derive"))))))

(define-public crate-agoraui-runtimes-0.1.2 (c (n "agoraui-runtimes") (v "0.1.2") (d (list (d (n "agoraui-runtimes-dervie") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "dyobj") (r "^0.1.2") (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.2") (d #t) (k 0)))) (h "1j31br1lqha59s8p3zdbhb31nanxb413n0r7bplxh9g2sacmccg1") (f (quote (("derive" "agoraui-runtimes-dervie") ("default" "derive"))))))

