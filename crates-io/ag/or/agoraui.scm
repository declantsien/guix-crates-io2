(define-module (crates-io ag or agoraui) #:use-module (crates-io))

(define-public crate-agoraui-0.1.0 (c (n "agoraui") (v "0.1.0") (d (list (d (n "agoraui-runtimes") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.0") (d #t) (k 0)))) (h "17c1ixn12jwilaihrcr7n33j8h948l48yqqkhaivxay1d3wmyqrx")))

(define-public crate-agoraui-0.1.1 (c (n "agoraui") (v "0.1.1") (d (list (d (n "agoraui-runtimes") (r "^0.1.1") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default" "unstable"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.1") (d #t) (k 0)))) (h "0ab05ga5322vxl4l0sq5akix4nflxb6sx95mqmq4sg80wns91fcj")))

(define-public crate-agoraui-0.1.2 (c (n "agoraui") (v "0.1.2") (d (list (d (n "agoraui-runtimes") (r "^0.1.2") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default" "unstable"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "to_any") (r "^0.1.2") (d #t) (k 0)))) (h "0034jp6xv15gjcsbxpbvmxm0hs1ppxf3jx85pwc1nmk1x6h0djrz")))

