(define-module (crates-io ag or agora-rtm) #:use-module (crates-io))

(define-public crate-agora-rtm-0.1.0 (c (n "agora-rtm") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "14rh1bsdn1j0p842spgn2mz6q3cgz4g1izp7jwyjqz9q4a6pd9sc")))

(define-public crate-agora-rtm-0.1.1 (c (n "agora-rtm") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "01zpj7l0jf6273f8r66bd72j3ppw123q255ab144f8p3ka5asr3f")))

(define-public crate-agora-rtm-0.1.2 (c (n "agora-rtm") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0ac7m10wg05isqikwg6zhgfx22x5m0n7pr54fqqavyd16l807n7v")))

(define-public crate-agora-rtm-0.1.3 (c (n "agora-rtm") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0bs8z6z3bvb8cbvdzgmca8yp1qpxxzkpdflh6l25fspwkwcz3v8i")))

