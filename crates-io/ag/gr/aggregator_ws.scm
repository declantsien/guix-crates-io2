(define-module (crates-io ag gr aggregator_ws) #:use-module (crates-io))

(define-public crate-aggregator_ws-0.1.0 (c (n "aggregator_ws") (v "0.1.0") (h "04mpjnr5r7dx7246hk01sgknc7kjzavby0ir8f7xnlgxdgfk1nzf")))

(define-public crate-aggregator_ws-0.1.1 (c (n "aggregator_ws") (v "0.1.1") (h "0wps66gy61wjrl4n8hkf3wzglbmisi7hhv1gsn21rnrv3fxf30zl")))

(define-public crate-aggregator_ws-0.1.2 (c (n "aggregator_ws") (v "0.1.2") (h "1ms551jxhv5svmbs4jjh9gcvhx5mzkwwx8f76wq1bkx9k6fljliq")))

(define-public crate-aggregator_ws-0.1.3 (c (n "aggregator_ws") (v "0.1.3") (h "1gmgsdi6xrrgv53w8iji8slk309c51aalcjb54mwdaqjib607qbc")))

