(define-module (crates-io ag gr aggregate_derive) #:use-module (crates-io))

(define-public crate-aggregate_derive-0.1.0 (c (n "aggregate_derive") (v "0.1.0") (d (list (d (n "aggregate_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1z2adfdvq5p7j0f2b5mqpkhs8vr3p6ddb4knah65w3358ajwmmsa")))

(define-public crate-aggregate_derive-0.1.1 (c (n "aggregate_derive") (v "0.1.1") (d (list (d (n "aggregate_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1y9hbk4gj8f4329i147k0mkl2jbq8ya7z5wax54bn38vxw9z9yzf")))

(define-public crate-aggregate_derive-0.1.2 (c (n "aggregate_derive") (v "0.1.2") (d (list (d (n "aggregate_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0afy8fakx8cak90z6f4a4agv3lxwz4j21ibwgwb41qb35dr0a330")))

(define-public crate-aggregate_derive-0.2.0 (c (n "aggregate_derive") (v "0.2.0") (d (list (d (n "aggregate_types") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "09paij5crnk2jb5q1ip4fdhxc1m7bf5rll1l2rg0qjgmc7p1g832")))

(define-public crate-aggregate_derive-0.2.1 (c (n "aggregate_derive") (v "0.2.1") (d (list (d (n "aggregate_types") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "01ynzxkdjxx391s7z639ixyf8zmsgfs33xvc2wskbn6218ahhz7b")))

(define-public crate-aggregate_derive-0.2.2 (c (n "aggregate_derive") (v "0.2.2") (d (list (d (n "aggregate_types") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0655qnz6n8pndniscrf82yvzm0pzxb6ws6k15bqswvvb0rn2h1kg")))

(define-public crate-aggregate_derive-0.2.3 (c (n "aggregate_derive") (v "0.2.3") (d (list (d (n "aggregate_types") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0a62qhw9dgdsa7zvbly31k0mmmik8zx4abhf5csn9fgqb6xk58vk")))

(define-public crate-aggregate_derive-0.2.4 (c (n "aggregate_derive") (v "0.2.4") (d (list (d (n "aggregate_types") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1vp12rmqmhvq6ry9h54jyla94pciwrg7aiz9gglw7ssys2zf05bs")))

(define-public crate-aggregate_derive-0.2.5 (c (n "aggregate_derive") (v "0.2.5") (d (list (d (n "aggregate_types") (r "^0.2.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "17z6a88i320lwph606zxgbxmcvbxmhym198s8s3f319zs0jzbdxw")))

(define-public crate-aggregate_derive-0.2.6 (c (n "aggregate_derive") (v "0.2.6") (d (list (d (n "aggregate_types") (r "^0.2.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "034p1qm7rnz085n27lxddizmi8hacxwxlya12rxay7sk6vqxms8d")))

(define-public crate-aggregate_derive-0.2.7 (c (n "aggregate_derive") (v "0.2.7") (d (list (d (n "aggregate_types") (r "^0.2.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1y3q6zaizf7rc9j69ds69qmzjhpsn5bby1j0h9khg564jjnj2g78")))

(define-public crate-aggregate_derive-0.3.0 (c (n "aggregate_derive") (v "0.3.0") (d (list (d (n "aggregate_types") (r "^0.3.0") (d #t) (k 0)))) (h "1n1abwhs93h0wj0d64bs38qzmxrcdh4lkhz7g1v820cs6q46qbsm")))

(define-public crate-aggregate_derive-0.3.2 (c (n "aggregate_derive") (v "0.3.2") (d (list (d (n "aggregate_types") (r "^0.3.2") (d #t) (k 0)))) (h "11v24zk0vqz9w0q45hz64xhpm3rgvlysyh8l91g642s7l8d3apjn")))

(define-public crate-aggregate_derive-0.3.3 (c (n "aggregate_derive") (v "0.3.3") (d (list (d (n "aggregate_types") (r "^0.3.3") (d #t) (k 0)))) (h "1bnjixak77g6g4icwy1yi8vc99vjlwy73z3kpa1is95ic05mmkzw")))

