(define-module (crates-io ag gr aggregate-map) #:use-module (crates-io))

(define-public crate-aggregate-map-1.0.0 (c (n "aggregate-map") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14qz5676p6a9a13cjj16dmnvyfjhg1qpvmawjr4gxs6ks6a13xya") (f (quote (("hashmap") ("default" "hashmap") ("btreemap")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-aggregate-map-1.0.1 (c (n "aggregate-map") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0iia745yd0aphlvjf410jbiz0fcigz6v36g8ghw41xr225rpk4s6") (f (quote (("hashmap") ("default" "hashmap") ("btreemap")))) (s 2) (e (quote (("serde" "dep:serde"))))))

