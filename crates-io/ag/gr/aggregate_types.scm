(define-module (crates-io ag gr aggregate_types) #:use-module (crates-io))

(define-public crate-aggregate_types-0.1.0 (c (n "aggregate_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1axsdqc34aqpffn6fca421gwc1k57d9zq0cibclszxnzsi7r7fvc") (f (quote (("debug" "syn/extra-traits")))) (s 2) (e (quote (("serde" "debug" "dep:serde"))))))

(define-public crate-aggregate_types-0.1.1 (c (n "aggregate_types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1jw1xg5l0jnhpd2wzn9gampn5kmh8k2959nclyr0v3myvj8ljs5r") (f (quote (("debug" "syn/extra-traits")))) (s 2) (e (quote (("serde" "debug" "dep:serde"))))))

(define-public crate-aggregate_types-0.1.2 (c (n "aggregate_types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0sfhfy89b13nwjkn4pbmwc9i5728kfnfb3fjgvpli57y1szd253h") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.0 (c (n "aggregate_types") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "19sb8n0wcmqpd7apgml4karkw08r072klpgva63wa1x7ac4qvj9g") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.1 (c (n "aggregate_types") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0mmm4kjixfn1bmiv5kp65r0svg2l1rs36m76p0bn7p78mc4nj3l7") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.2 (c (n "aggregate_types") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1d3sas4in4j849smizidlrja643q0i41nkgs3ysj6f07yqggn863") (f (quote (("shortcuts") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.3 (c (n "aggregate_types") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1nvvrb1ssyh3krqhh58ci68icxn6h332zhl8a1rh0bv1rc3rlrv8") (f (quote (("shortcuts") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.4 (c (n "aggregate_types") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1spd0s70v77s2c003a42fgf9l6m0fxyh0zd5g5mklr5i8syfk2d4") (f (quote (("helper") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.5 (c (n "aggregate_types") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "07a6qxc3637zq8s2ylr15my632krjr6ha8f94a9j5pa02xqxswg6") (f (quote (("helper") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.6 (c (n "aggregate_types") (v "0.2.6") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1418c9ywaw6kzd87m7a7wcrr437z0pkfwjdrxq57br28z7g3bdn8") (f (quote (("helper") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.2.7 (c (n "aggregate_types") (v "0.2.7") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1gmvz4y5j6cwmz6fc4h15c5vxvl4jhswacn6fwp27y9h2hd0m972") (f (quote (("helper") ("debug" "syn/extra-traits"))))))

(define-public crate-aggregate_types-0.3.0 (c (n "aggregate_types") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1ra8c2pfxpjwb29lmylycb2v548x4w11q8bc2cd32c83kgl2nfp4")))

(define-public crate-aggregate_types-0.3.2 (c (n "aggregate_types") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0c99j4hxgfcnm3wdbcb8pjwxns0lmk1qb5sk23nczbrkd4dn0ji8") (f (quote (("fmt") ("default" "fmt"))))))

(define-public crate-aggregate_types-0.3.3 (c (n "aggregate_types") (v "0.3.3") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "03ivhq2zqbkalqn6m19xf7q1p6dswzs6g07i4wqv0yn80saj89hp") (f (quote (("fmt") ("default" "fmt"))))))

