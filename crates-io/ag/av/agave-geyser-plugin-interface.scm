(define-module (crates-io ag av agave-geyser-plugin-interface) #:use-module (crates-io))

(define-public crate-agave-geyser-plugin-interface-0.1.0 (c (n "agave-geyser-plugin-interface") (v "0.1.0") (h "1p3aaar28gf48lh6bwmqy5fl4icbdl1h8x2bwl49cr1svv53cllg")))

(define-public crate-agave-geyser-plugin-interface-1.17.25 (c (n "agave-geyser-plugin-interface") (v "1.17.25") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.25") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "050df2szz1qvnl7x1rd7q4mvw9bs1a40qg8pwabdrkf1wr8dp5mb")))

(define-public crate-agave-geyser-plugin-interface-1.18.5 (c (n "agave-geyser-plugin-interface") (v "1.18.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.5") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "096gig92j0jxbv43r0z3sf4b8dxqp82ddnrppzwyxiflkyynpkx1")))

(define-public crate-agave-geyser-plugin-interface-1.17.26 (c (n "agave-geyser-plugin-interface") (v "1.17.26") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.26") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1s3b20fn2p6r0zk8z9vi8raggnp42jxl2z114f2c76f5xl16ya9c")))

(define-public crate-agave-geyser-plugin-interface-1.18.6 (c (n "agave-geyser-plugin-interface") (v "1.18.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.6") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "04frx4cz0d39l31kzgkfayvayamqfr5s4j48xvrqkl9phf897pww")))

(define-public crate-agave-geyser-plugin-interface-1.17.27 (c (n "agave-geyser-plugin-interface") (v "1.17.27") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.27") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1frvc8qghsa5nylw72cq8df0yqvw7qiq9jf5d9v8f4kcmklf7qb9")))

(define-public crate-agave-geyser-plugin-interface-1.18.7 (c (n "agave-geyser-plugin-interface") (v "1.18.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.7") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1f3lq9rd2la8wxfv32x52jx2q95mpycvzpllhq7h6kfj3pxkg85f")))

(define-public crate-agave-geyser-plugin-interface-1.18.8 (c (n "agave-geyser-plugin-interface") (v "1.18.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.8") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0bwqvicbylyg67hgzcim7yvdymndvxqy0arqxy87ar8x2gwvcrmj")))

(define-public crate-agave-geyser-plugin-interface-1.17.28 (c (n "agave-geyser-plugin-interface") (v "1.17.28") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.28") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "15fgidgn2kjpkxv84x0b2p9s4j1272knpqjxbzdb4a2gxhsk58yv")))

(define-public crate-agave-geyser-plugin-interface-1.18.9 (c (n "agave-geyser-plugin-interface") (v "1.18.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rgklmx19yaxfxafpb7xd7d6wx7g57d3ahfyfw69lfn80ff0zyfw")))

(define-public crate-agave-geyser-plugin-interface-1.17.29 (c (n "agave-geyser-plugin-interface") (v "1.17.29") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.29") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1r6bvz3blg365wnnw13rkj6gk890sawa7bzmbq6w8scfk732h0g8")))

(define-public crate-agave-geyser-plugin-interface-1.17.30 (c (n "agave-geyser-plugin-interface") (v "1.17.30") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.30") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "08jicgn2jbyp11b5jk6sl578sl5y8gczm7q6srmz7hszdhxyrg3k")))

(define-public crate-agave-geyser-plugin-interface-1.18.10 (c (n "agave-geyser-plugin-interface") (v "1.18.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01w47z5zkjbina45b14fskm9cx27xrl5115v7ay4dgidf98bzv7i")))

(define-public crate-agave-geyser-plugin-interface-1.18.11 (c (n "agave-geyser-plugin-interface") (v "1.18.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ix8bcma381m1q8lzsy7ha9w4kpzqjycv8jlr60ziqvlmn89w5wj")))

(define-public crate-agave-geyser-plugin-interface-1.17.31 (c (n "agave-geyser-plugin-interface") (v "1.17.31") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.31") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "09x0nv4w0hg7r5lkmkvff46w9pm33wyxxkziihv5zl6phijdzbzb")))

(define-public crate-agave-geyser-plugin-interface-1.18.12 (c (n "agave-geyser-plugin-interface") (v "1.18.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.12") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1c69zj9xh972p4kxib09avncbrnp3yxifb5bba3pb66zjis9dqfl")))

(define-public crate-agave-geyser-plugin-interface-1.17.32 (c (n "agave-geyser-plugin-interface") (v "1.17.32") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.32") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "185llgr6ddg5vd59fn8ijifnh3khgpl46yvqxgjj3bf4pqaafi9w")))

(define-public crate-agave-geyser-plugin-interface-1.17.33 (c (n "agave-geyser-plugin-interface") (v "1.17.33") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.33") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0khm2n1nd2mw8dn2ddixvj09a0vpfvvwpz6cijvilw6cs81n776h")))

(define-public crate-agave-geyser-plugin-interface-1.18.13 (c (n "agave-geyser-plugin-interface") (v "1.18.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.13") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0smmwyc8l23smg60qy88v3xfb82pmc83l0kiy3daawibvycn5bgp")))

(define-public crate-agave-geyser-plugin-interface-1.18.14 (c (n "agave-geyser-plugin-interface") (v "1.18.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.14") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.18.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0nqwi18wzamcvrsw1ii7mbvn45v54kpizcjcds2ydb32v4462ixj")))

(define-public crate-agave-geyser-plugin-interface-1.17.34 (c (n "agave-geyser-plugin-interface") (v "1.17.34") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.34") (d #t) (k 0)) (d (n "solana-transaction-status") (r "=1.17.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0dj6jisi3ndw3y7mr18qdilwdl27wy44wps8lazpjvd3vcig8vfy")))

