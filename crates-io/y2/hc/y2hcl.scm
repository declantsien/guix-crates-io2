(define-module (crates-io y2 hc y2hcl) #:use-module (crates-io))

(define-public crate-y2hcl-0.1.0 (c (n "y2hcl") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "19wkfy92cyqlb1p5vsvf729kzayzhn5abf6z359zc6bqq8dw5pms")))

