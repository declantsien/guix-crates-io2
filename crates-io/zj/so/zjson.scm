(define-module (crates-io zj so zjson) #:use-module (crates-io))

(define-public crate-zjson-0.1.0 (c (n "zjson") (v "0.1.0") (h "035rx4fp9v7x9y538qnsb5vb5cjshfl877n7n6bj6ksiq0jjparj") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1.1 (c (n "zjson") (v "0.1.1") (h "1ipbq9k24zwzr44f4bviw43l6jfw6cgq9lql5qsx1ginsfrfb79r") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1.2 (c (n "zjson") (v "0.1.2") (h "0mzchvsfqnk0hsj59kgf9n1r81sirgg7xs51190y8k2jil40jl7k") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1.3 (c (n "zjson") (v "0.1.3") (h "0yhf7b3h7490q9ixl4fdhhdqaf03yak1d7fsnlycb21sq4g6cy8g") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1.4 (c (n "zjson") (v "0.1.4") (h "0sdnk90gxg93j49nq8xgi39vn5lsn07b6w4bnrmbn390myp4dnw0") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1.5 (c (n "zjson") (v "0.1.5") (h "11i9xxy9fdz2aqwrps4s656jrdiz60xzcmk601rzk0ga6x5p7ggv") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

