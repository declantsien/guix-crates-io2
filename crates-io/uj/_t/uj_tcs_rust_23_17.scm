(define-module (crates-io uj _t uj_tcs_rust_23_17) #:use-module (crates-io))

(define-public crate-uj_tcs_rust_23_17-0.1.1 (c (n "uj_tcs_rust_23_17") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj_tcs_rust_23_18") (r "^0.1.0") (d #t) (k 0)))) (h "1kzrxff3afbwcbfkpbk9qw4z6ai0fwqk1v1bzz38xl4mii11ja2g")))

(define-public crate-uj_tcs_rust_23_17-0.1.2 (c (n "uj_tcs_rust_23_17") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju_tcs_rust_23_19") (r "^0.1.1") (d #t) (k 0)))) (h "1zr5dcqqj9p5y94f4mgh03ks5al457x1z49j9s1ng7gah2baj7kd")))

