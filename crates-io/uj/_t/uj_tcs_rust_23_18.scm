(define-module (crates-io uj _t uj_tcs_rust_23_18) #:use-module (crates-io))

(define-public crate-uj_tcs_rust_23_18-0.1.0 (c (n "uj_tcs_rust_23_18") (v "0.1.0") (h "0z9dha2nzvpjwdv09n3jipgl9d9l4ablxwb8a6pr1jcdb54y6ryi")))

(define-public crate-uj_tcs_rust_23_18-0.1.1 (c (n "uj_tcs_rust_23_18") (v "0.1.1") (h "03n9j361dvf7ix50ixcyr8lg7l2v6kmi14sjf491svlgiq8h7053")))

