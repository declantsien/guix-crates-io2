(define-module (crates-io uj _t uj_tcs_rust_23_1188) #:use-module (crates-io))

(define-public crate-uj_tcs_rust_23_1188-0.1.0 (c (n "uj_tcs_rust_23_1188") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj_tcs_rust_23_18") (r "^0.1.0") (d #t) (k 0)))) (h "03fxyx31xpf758bnd4m0qm2n9la81nw896k746ma5z91sxcbazml")))

(define-public crate-uj_tcs_rust_23_1188-0.1.1 (c (n "uj_tcs_rust_23_1188") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj_tcs_rust_23_18") (r "^0.1.0") (d #t) (k 0)))) (h "0mmbhbdxs67198sh7c2vhh28j341qvm0fqg70p3lkx6wqa2axf7z")))

