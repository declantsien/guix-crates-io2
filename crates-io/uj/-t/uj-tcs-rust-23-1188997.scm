(define-module (crates-io uj -t uj-tcs-rust-23-1188997) #:use-module (crates-io))

(define-public crate-uj-tcs-rust-23-1188997-0.1.0 (c (n "uj-tcs-rust-23-1188997") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uj_tcs_rust_23_18") (r "^0.1.0") (d #t) (k 0)))) (h "10brj2dx3ijbkl829w7cza96f02g523nkq57aj8xknqyij1hw199")))

