(define-module (crates-io uj -t uj-tcs-rust-23-26) #:use-module (crates-io))

(define-public crate-uj-tcs-rust-23-26-0.1.0 (c (n "uj-tcs-rust-23-26") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j2nc1h0l9bkwm6xqqvyln27ylsw6yxwxn8aw8jfp58haxhibk55")))

(define-public crate-uj-tcs-rust-23-26-0.1.3 (c (n "uj-tcs-rust-23-26") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ju-tcs-rust-23-25") (r "^0.1.2") (d #t) (k 0)))) (h "0yid0lpydgqvgd2lh61qdhfvxydy66z8y46hn071kb4xfp7vbn8d")))

