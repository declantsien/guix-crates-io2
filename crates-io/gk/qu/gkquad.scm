(define-module (crates-io gk qu gkquad) #:use-module (crates-io))

(define-public crate-gkquad-0.0.1 (c (n "gkquad") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "smallbox") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1k5d33qqqis00pjqmg9ki7a6jlsf1qp7yinzb4xgbm2610k6rzp7") (f (quote (("std" "smallbox/std") ("single") ("default" "std" "single"))))))

(define-public crate-gkquad-0.0.2 (c (n "gkquad") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1sxg2vambhvd919dy0as1f8h5v8axjyyk2k5z4xpxgdxcys2ly5g") (f (quote (("std") ("single") ("simd") ("default" "std" "simd" "single"))))))

(define-public crate-gkquad-0.0.3 (c (n "gkquad") (v "0.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0lw9kc5amwacxsxwkz0aad2wgg8fxrizjqjfvnv4m8wbdh96mn1b") (f (quote (("std") ("simd") ("double") ("default" "std" "simd")))) (y #t)))

(define-public crate-gkquad-0.0.4 (c (n "gkquad") (v "0.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "13m4q4liacdliqvr5rwq6b2pkvzkl4qgawsk9fz5ald20lhr23xr") (f (quote (("std") ("simd") ("double") ("default" "std" "simd"))))))

