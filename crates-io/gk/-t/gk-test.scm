(define-module (crates-io gk -t gk-test) #:use-module (crates-io))

(define-public crate-gk-test-0.1.0 (c (n "gk-test") (v "0.1.0") (h "1rwrr2h0r49w2gxjcvld3kzqci01471ddrh7iwpxmxqqxxws6lrr")))

(define-public crate-gk-test-0.1.1 (c (n "gk-test") (v "0.1.1") (h "0za0584by5nrma0viclpswpb8z3rfdmmjbdx1cb5ly1r1plb3rn4")))

