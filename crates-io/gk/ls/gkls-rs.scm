(define-module (crates-io gk ls gkls-rs) #:use-module (crates-io))

(define-public crate-gkls-rs-0.1.0 (c (n "gkls-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0gqcgj8mnki8fsappg9p1avmyd7zsjkbmf4fyd0jr918m0mcycwm") (f (quote (("test_cbinding") ("examples" "plotters") ("default"))))))

