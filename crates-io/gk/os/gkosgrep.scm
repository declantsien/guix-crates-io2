(define-module (crates-io gk os gkosgrep) #:use-module (crates-io))

(define-public crate-gkosgrep-0.1.1 (c (n "gkosgrep") (v "0.1.1") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "1vw3a5qh2p20dj9syxk3p2vqqqk4yqswlkjd4gl1iid6siqrm360")))

(define-public crate-gkosgrep-0.1.2 (c (n "gkosgrep") (v "0.1.2") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "0lmyvhcmhpvqjfxz9wxsvhilb342ssc1k3rm19yfz8ah2p4s17l8")))

(define-public crate-gkosgrep-0.1.3 (c (n "gkosgrep") (v "0.1.3") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "1zf94q8nx5vijs0aw220vky06f6rcq7ybvjmijs2hvbskv9p2jvk")))

(define-public crate-gkosgrep-0.1.4 (c (n "gkosgrep") (v "0.1.4") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "187zbgckv1gb8hf6fcivzyk2crj3y0ajmcgjmrwkbygybd8b821i")))

(define-public crate-gkosgrep-0.1.5 (c (n "gkosgrep") (v "0.1.5") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "05qiih9ddr7anf00c0jwc672dwm8gw835afvp474v2ispccxmfch")))

(define-public crate-gkosgrep-0.1.6 (c (n "gkosgrep") (v "0.1.6") (d (list (d (n "content_inspector") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "0q0y1f1h0cjrfhs25v8vj19id3f3191z1w7lvm48razpgbc0y1kc")))

(define-public crate-gkosgrep-0.1.7 (c (n "gkosgrep") (v "0.1.7") (d (list (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1blaj8ikf6ws3zyg5vg7gnmsf9bgbsjznzpwrskcwqyi7shmgb0h")))

(define-public crate-gkosgrep-0.1.8 (c (n "gkosgrep") (v "0.1.8") (d (list (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "021qhwqpsm34bk15q2syg5bdfxda2546761g5czbivvp8p7vzmzh")))

(define-public crate-gkosgrep-0.1.9 (c (n "gkosgrep") (v "0.1.9") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zy57r4czcscyhcmhvwn8q6jfwf4646lavghakb50nwpl7dhdq4j")))

(define-public crate-gkosgrep-0.1.10 (c (n "gkosgrep") (v "0.1.10") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "1.*") (d #t) (k 0)) (d (n "threadpool") (r "1.*") (d #t) (k 0)))) (h "12yakkax2jv91rixyybbzxw1qi4bisxvl5b478r8dx5afn5j0zlz")))

