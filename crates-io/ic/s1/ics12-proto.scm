(define-module (crates-io ic s1 ics12-proto) #:use-module (crates-io))

(define-public crate-ics12-proto-0.1.1 (c (n "ics12-proto") (v "0.1.1") (d (list (d (n "bytes") (r "^1.2") (k 0)) (d (n "ibc-proto") (r "^0.35.0") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "03zw6jg3281blwym7kg5gdy051jlszhvxrxa9k6m29wr9qd9cgv7") (f (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

(define-public crate-ics12-proto-0.1.2 (c (n "ics12-proto") (v "0.1.2") (d (list (d (n "bytes") (r "^1.2") (k 0)) (d (n "ibc-proto") (r "^0.35.0") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "0f30bzm6hjybvd4fbp7rpw0dbjfggj99d4ig19x4fhhd39yd46by") (f (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

(define-public crate-ics12-proto-0.1.3 (c (n "ics12-proto") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "ibc-proto") (r "^0.38.0") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0iiqwsca3ark4bhvkqywqxw3xs6vk4dkxdjpmbx9yg6pnq2p8bxc") (f (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

