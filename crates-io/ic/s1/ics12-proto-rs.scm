(define-module (crates-io ic s1 ics12-proto-rs) #:use-module (crates-io))

(define-public crate-ics12-proto-rs-0.1.0 (c (n "ics12-proto-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2") (k 0)) (d (n "ibc-proto") (r "^0.35.0") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "17wb84mdfqcjid8kxn14s8g14nfh3hvgxvsshi68nxx5kjdf1axp") (f (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

