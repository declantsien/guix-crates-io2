(define-module (crates-io ic m4 icm42670) #:use-module (crates-io))

(define-public crate-icm42670-0.1.0 (c (n "icm42670") (v "0.1.0") (d (list (d (n "accelerometer") (r "^0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0bxwjzv7g68dj7qhhir0xa78z7r395g2hwy3hfi0n4i1d2q1h9wx") (r "1.56")))

(define-public crate-icm42670-0.1.1 (c (n "icm42670") (v "0.1.1") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1287npq4qlkyz3wynir1l9szz7d4609vbc6vf47pd933n8pkwmyj") (r "1.56")))

(define-public crate-icm42670-0.2.0 (c (n "icm42670") (v "0.2.0") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0z11szbqpn8v4bbpvqcm5y54gnnlya8al2h7gclhfs9cp8sn2nrg") (r "1.60")))

