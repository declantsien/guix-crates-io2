(define-module (crates-io ic al ical_vcard) #:use-module (crates-io))

(define-public crate-ical_vcard-0.1.0 (c (n "ical_vcard") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "1vsq9ngcvvxz89pc51z0r1n8lgqbba2i6q0wmvz51k7a38v3n47y")))

(define-public crate-ical_vcard-0.2.0 (c (n "ical_vcard") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "0q8zng6x9cmsiwbds9zinyrm408pgy8whk0ryfn64ryy5vzxl9ly")))

