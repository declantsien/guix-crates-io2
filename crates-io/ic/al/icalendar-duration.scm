(define-module (crates-io ic al icalendar-duration) #:use-module (crates-io))

(define-public crate-icalendar-duration-0.1.0 (c (n "icalendar-duration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "icalendar") (r "^0.13.1") (f (quote ("parser"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0bmvbd0x8011r675f6kvxczp3x3zzhy32kjjg873f3yjkji9wnq7")))

(define-public crate-icalendar-duration-0.1.1 (c (n "icalendar-duration") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "icalendar") (r "^0.13.1") (f (quote ("parser"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1cd2d7sq8650gdb4bmg6mgi6wgbdk1vd9rqyfjwffyx6cx85hlms")))

