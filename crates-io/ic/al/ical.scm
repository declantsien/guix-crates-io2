(define-module (crates-io ic al ical) #:use-module (crates-io))

(define-public crate-ical-0.1.0 (c (n "ical") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "09vhq0w94147dcvp8njcy49ylsmj7bcgq47wknp6fqlb3iwa83i6") (y #t)))

(define-public crate-ical-0.2.0 (c (n "ical") (v "0.2.0") (h "1qfmpmmkjv4vxcmh5s0gmjgscp12cf45123xj06gq4qv0x54jwqk")))

(define-public crate-ical-0.2.1 (c (n "ical") (v "0.2.1") (h "0jp71dxsbvc5qavb0c0b7nng2c2sv98wc1y6qzk6zbxrgqgrp6vi") (f (quote (("vcard-parser" "line-parser") ("line-reader") ("line-parser" "line-reader") ("ical-parser" "line-parser") ("default" "vcard-parser" "ical-parser"))))))

(define-public crate-ical-0.3.0 (c (n "ical") (v "0.3.0") (h "0mv1jpsjp3ji00f73mz6vsv5aviavd9wwq5w9w9j05d521wcvak8") (f (quote (("vcard-parser" "line-parser") ("line-reader") ("line-parser" "line-reader") ("ical-parser" "line-parser") ("default" "vcard-parser" "ical-parser"))))))

(define-public crate-ical-0.4.0 (c (n "ical") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0djrxcn5daz2cv0s5m9678ccdih5mdplgriphvgy5has3vy7baqw") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.4.1 (c (n "ical") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "15q387lkn38y3xr1s8i4hrgr17qhb2p0b978bsl6rvz9dgh47mac") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.4.2 (c (n "ical") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1zzz9ynlqp53qzy733mgng6d6rp6jp6h3bz7cf6sp50pwsw9yjmh") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.4.3 (c (n "ical") (v "0.4.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1x0a0npayck0dqzi0gmlgxdlz96xar19cdxn2y1smzpd6h6l2hwm") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.5.0 (c (n "ical") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0kq94wy8cb802k0ni24d1lvpi6fkw9ii7x7hhq1d1rl4pwhzk1d9") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.6.0 (c (n "ical") (v "0.6.0") (d (list (d (n "failure") (r "0.1.*") (d #t) (k 0)))) (h "1h0q3wh0hcqahd3rp5bv502c0kbka568apk5ry9gn7whkmx37sf0") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.7.0 (c (n "ical") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1kvk1pgas67rnp0n4424lxxs8y3n1h0fw3ap8jbfcxqdmlap57sa") (f (quote (("vcard" "property") ("property" "line") ("line") ("ical" "property") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.8.0 (c (n "ical") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "07qw4z3zwl60yyjgfhx91kjlkhfashdhiyd6bvaiay4rb2yq4v9m") (f (quote (("vcard" "property") ("serde-derive" "serde") ("property" "line") ("line") ("ical" "property") ("generator" "ical") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.9.0 (c (n "ical") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1y9vhss9943q3zqr2vxaxdzldr241dnf7bw4c1hm2kac5lvkqf96") (f (quote (("vcard" "property") ("serde-derive" "serde") ("property" "line") ("line") ("ical" "property") ("generator" "ical") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.10.0 (c (n "ical") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0w3nj8xjmhdgl5qljn61ldgfmd35xpni88b43s5fad7fk7mx9fnl") (f (quote (("vcard" "property") ("serde-derive" "serde") ("property" "line") ("line") ("ical" "property") ("generator" "ical") ("default" "vcard" "ical"))))))

(define-public crate-ical-0.11.0 (c (n "ical") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1xkrs9a48qzbzf6mbrnsvj9i51h2z44l7h7236d75dx88dssnz4v") (f (quote (("vcard" "property") ("serde-derive" "serde") ("property" "line") ("line") ("ical" "property") ("generator" "ical") ("default" "vcard" "ical"))))))

