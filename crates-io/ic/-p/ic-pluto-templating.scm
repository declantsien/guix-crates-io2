(define-module (crates-io ic -p ic-pluto-templating) #:use-module (crates-io))

(define-public crate-ic-pluto-templating-0.1.0 (c (n "ic-pluto-templating") (v "0.1.0") (d (list (d (n "ructe") (r "^0.17.0") (f (quote ("mime03"))) (d #t) (k 0)))) (h "090la90bwrhjvikpr4kmwa4mwyb4wks09sq6xfcjcwsd37nrbbs1")))

(define-public crate-ic-pluto-templating-0.1.1 (c (n "ic-pluto-templating") (v "0.1.1") (d (list (d (n "ructe") (r "^0.17.0") (f (quote ("mime03"))) (d #t) (k 0)))) (h "13spadkamj9vgmj4zxjncsyhybypvq4r3y52fn5ds07j2g7wfa8q")))

