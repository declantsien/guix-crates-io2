(define-module (crates-io ic -s ic-sqlite) #:use-module (crates-io))

(define-public crate-ic-sqlite-0.1.0 (c (n "ic-sqlite") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled" "serde_json"))) (d #t) (k 0) (p "rusqlite-ic")) (d (n "sqlite-vfs") (r "^0.2") (d #t) (k 0) (p "sqlite-vfs-ic")))) (h "1f2l6yny74sz0yyzrnr8frjizxmnxm4rpmi27s3j5w88c8jymvb6")))

