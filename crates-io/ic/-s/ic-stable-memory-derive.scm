(define-module (crates-io ic -s ic-stable-memory-derive) #:use-module (crates-io))

(define-public crate-ic-stable-memory-derive-0.4.0 (c (n "ic-stable-memory-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "00rl2wh52ni5v2h203rv71p95p9cqs6wyrqpm8yll13j22xrxsjk")))

(define-public crate-ic-stable-memory-derive-0.4.1 (c (n "ic-stable-memory-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "17a4snh9vz03yp3633d1z5b7bwknj4b4gdajv78vfzkv2qvgk5sa")))

(define-public crate-ic-stable-memory-derive-0.4.2 (c (n "ic-stable-memory-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1qzyqw4gd9m7k7xkihw6am0av64ci0a642d6byhdkq4pbr7kmnvg")))

