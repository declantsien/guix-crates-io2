(define-module (crates-io ic -s ic-storage-module) #:use-module (crates-io))

(define-public crate-ic-storage-module-1.0.0 (c (n "ic-storage-module") (v "1.0.0") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("mysql" "chrono"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "intel-cache-lib") (r "^2.0.0") (d #t) (k 0)) (d (n "ipfs-api-backend-hyper") (r "^0.2") (d #t) (k 0)) (d (n "public-ip") (r "^0.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1cfmkib891npcm5gxp1116a6lilj8d9574k27mmsjwqiiaril2b7")))

