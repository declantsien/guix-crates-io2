(define-module (crates-io ic an icanhazdadjoke-sdk) #:use-module (crates-io))

(define-public crate-icanhazdadjoke-sdk-0.1.0 (c (n "icanhazdadjoke-sdk") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("http1" "runtime" "client" "stream"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1pdnn24n7q56xr00kf3xs52wpx1yck5js3wsjp7ziivj3bjmknfk")))

