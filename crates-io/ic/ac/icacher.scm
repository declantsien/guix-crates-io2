(define-module (crates-io ic ac icacher) #:use-module (crates-io))

(define-public crate-icacher-0.1.0 (c (n "icacher") (v "0.1.0") (h "02wjh85x6mgm054rc1rky1ha7mgaidqmrv135x091a8nmp9ljc87")))

(define-public crate-icacher-0.1.2 (c (n "icacher") (v "0.1.2") (h "0c36f3lvdjbsl33gf35mhqv9c38qkq6pnkgnjz6s8719a4ikphq7")))

(define-public crate-icacher-0.1.3 (c (n "icacher") (v "0.1.3") (h "02glfjkz548lk2rgkv8p782asl5jy4z1skb4c3i4vl0yd91nca8z")))

(define-public crate-icacher-0.1.4 (c (n "icacher") (v "0.1.4") (h "14lm2gk3g8ihysj9xdiafkmbp2rm5zji1vzzbv5fs7hwibvx008y")))

