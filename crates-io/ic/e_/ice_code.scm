(define-module (crates-io ic e_ ice_code) #:use-module (crates-io))

(define-public crate-ice_code-0.1.0 (c (n "ice_code") (v "0.1.0") (h "0irwyvpif9r93inyz0zsq3ilh4yl9r5jwn9zg22k6qwrjycjwcmx")))

(define-public crate-ice_code-0.1.1 (c (n "ice_code") (v "0.1.1") (h "0hi5flbgfh2ygr7cwpxwgipq266wpnjdzrnmxfyhsh387hmvxbrx")))

(define-public crate-ice_code-0.1.2 (c (n "ice_code") (v "0.1.2") (h "08gkmha4bsv3j3asc6pzr183hy1j3h9hbnn3v1wwbkl3q00c7h2m")))

(define-public crate-ice_code-0.1.3 (c (n "ice_code") (v "0.1.3") (h "119f9x9r4k3wnqsdicxas7kpvj6q3dmfvn0n8wnphg0rmx9vb2q9")))

(define-public crate-ice_code-0.1.4 (c (n "ice_code") (v "0.1.4") (h "0j3x9bp6vf6785s0iz03pckgj7rz3pqhq07kk5cxms2kym24hlm6")))

