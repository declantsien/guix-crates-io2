(define-module (crates-io ic oe icoextract) #:use-module (crates-io))

(define-public crate-icoextract-0.1.0 (c (n "icoextract") (v "0.1.0") (d (list (d (n "either") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "utf16string") (r "^0.2.0") (d #t) (k 0)))) (h "0811vv4i31yvvx0rsmzp3xbcknmwiqlmb4ya1vgh1bl6qk0y4mk9") (y #t)))

