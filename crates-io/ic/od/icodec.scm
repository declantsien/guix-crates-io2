(define-module (crates-io ic od icodec) #:use-module (crates-io))

(define-public crate-icodec-0.1.0-alpha (c (n "icodec") (v "0.1.0-alpha") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ikon") (r "^0.1.0-beta.11") (d #t) (k 0)))) (h "04h5fl83n45gd6a3h8xayw0n73hypx9h0s0ry04rdxy6q1hh0kgm") (y #t)))

(define-public crate-icodec-0.1.0-alpha.1 (c (n "icodec") (v "0.1.0-alpha.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ikon") (r "^0.1.0-beta.11") (d #t) (k 0)))) (h "0n6pznfmj2dmpjnsx6mdsjzcjp3i4zgrm1n4apxw4661vi3xy2x0") (y #t)))

(define-public crate-icodec-0.1.0-alpha.2 (c (n "icodec") (v "0.1.0-alpha.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ikon") (r "^0.1.0-beta.12") (d #t) (k 0)))) (h "1ri6iz7jy2xf7bc48rvqblsgf8birkgaqm7841yr91yvjz3nl30y") (y #t)))

(define-public crate-icodec-0.1.0-alpha.3 (c (n "icodec") (v "0.1.0-alpha.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ikon") (r "^0.1.0-beta.16") (d #t) (k 0)))) (h "03z21v6bdxsfc9x329nhziqnqghmmvrwix3jh0rjcw5pc5w3bbzm") (y #t)))

