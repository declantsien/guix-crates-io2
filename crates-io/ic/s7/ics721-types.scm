(define-module (crates-io ic s7 ics721-types) #:use-module (crates-io))

(define-public crate-ics721-types-0.1.0 (c (n "ics721-types") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw721") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01g11ms7043b5qy1y8q6vvjx81lr6v9f0g5v8apbs3gdbdf2a87l")))

