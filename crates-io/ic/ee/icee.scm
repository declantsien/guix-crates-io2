(define-module (crates-io ic ee icee) #:use-module (crates-io))

(define-public crate-icee-0.1.0-alpha0.1 (c (n "icee") (v "0.1.0-alpha0.1") (d (list (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "flume") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "iced") (r "^0.12") (f (quote ("advanced" "wgpu"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g8dgkhr7q85cnb6f3kg9kn49yd9v9kf2bddlscm1v24nk8f9p6b") (f (quote (("hot-reload" "notify" "flume") ("default" "hot-reload") ("debug"))))))

(define-public crate-icee-0.1.0-alpha0.2 (c (n "icee") (v "0.1.0-alpha0.2") (d (list (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "flume") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "iced") (r "^0.12") (f (quote ("advanced" "wgpu"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18vh6y7cp3y1fgnha53xb3c54vhzjibw8i4q1n11jniv4j335pr0") (f (quote (("hot-reload" "notify" "flume") ("default" "hot-reload") ("debug"))))))

