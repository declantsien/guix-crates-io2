(define-module (crates-io ic ee icee-container-rs) #:use-module (crates-io))

(define-public crate-icee-container-rs-0.1.0 (c (n "icee-container-rs") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1na2dckiwkkbzwr1zdds7c5q87ynq0l3prqcha8fpxrnqcjkn92q")))

(define-public crate-icee-container-rs-0.1.1 (c (n "icee-container-rs") (v "0.1.1") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hd16kpk84wzxg0irvwgd3f3lgg8pw6r72wsbavm93rklp9c5cdi")))

(define-public crate-icee-container-rs-0.1.2 (c (n "icee-container-rs") (v "0.1.2") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "01w07pw5y5z5wz8j58kg1an118gy15pk1f6lzmr49wi92p3p0kwz")))

