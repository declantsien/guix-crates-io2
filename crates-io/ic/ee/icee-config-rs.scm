(define-module (crates-io ic ee icee-config-rs) #:use-module (crates-io))

(define-public crate-icee-config-rs-0.1.0 (c (n "icee-config-rs") (v "0.1.0") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "15gjiqj6c8b5k9vg37ixb05d0g2fx9q2dmss2sa78icfcmhi5wg4")))

(define-public crate-icee-config-rs-0.1.1 (c (n "icee-config-rs") (v "0.1.1") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s848j06k7mw91rw6rdik01imaymlbcli4xk6f7v40i6wj04yx2f")))

(define-public crate-icee-config-rs-0.1.2 (c (n "icee-config-rs") (v "0.1.2") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g8vkiarid24hc5a7faiicl4213i0flrs78qnsslb8vlw3scxxn3")))

