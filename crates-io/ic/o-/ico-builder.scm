(define-module (crates-io ic o- ico-builder) #:use-module (crates-io))

(define-public crate-ico-builder-0.1.0 (c (n "ico-builder") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (f (quote ("ico"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0f03p93r60akwjy9w47x458lvm0sxw7p2lndcwq1c5xp7zxw2kak") (f (quote (("tiff" "image/tiff") ("jpeg" "image/jpeg") ("gif" "image/gif"))))))

