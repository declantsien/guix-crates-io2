(define-module (crates-io ic ol icolor) #:use-module (crates-io))

(define-public crate-iColor-0.1.0 (c (n "iColor") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "05ihvcaf1id0qnhqv67xdhwjcw0mynpzqrlny0yd19c16bzdhb3c")))

(define-public crate-iColor-0.1.1 (c (n "iColor") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1j3kgrm4safziwlhg6hh3g5wkvilqj519ksvp90kn28by58xadra")))

(define-public crate-iColor-0.1.2 (c (n "iColor") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0i1r1h0mlmwqyhbn1ahwmagk07dl56nlykks1ql3sy56i2qkr9ns")))

(define-public crate-iColor-0.1.3 (c (n "iColor") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0ahfm2pzxky6f7shiywhhznr0375fssa71ji72gw0bw3cnprl2yw")))

(define-public crate-iColor-0.1.4 (c (n "iColor") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "16x926yasss7k7b10zqk3lrqwp5jydv16w9j1kybyy1vrkq65z25")))

