(define-module (crates-io ic y_ icy_sauce) #:use-module (crates-io))

(define-public crate-icy_sauce-0.1.0 (c (n "icy_sauce") (v "0.1.0") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1zkzcm24lixbrs5f49s37g41wij1nxwrbk39fmhbsnpikbvycvyv")))

(define-public crate-icy_sauce-0.1.1 (c (n "icy_sauce") (v "0.1.1") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "01h21f28b0avas4bnprjzslrav0d8yfzs8wqinjv7rx3d4jnr49c")))

