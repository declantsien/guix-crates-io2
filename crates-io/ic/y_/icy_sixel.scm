(define-module (crates-io ic y_ icy_sixel) #:use-module (crates-io))

(define-public crate-icy_sixel-0.1.0 (c (n "icy_sixel") (v "0.1.0") (h "15rkd33f1nn1f6q93hsxbs5vlmclzcxfvgn5ypgnlf7s6w5sbv7a")))

(define-public crate-icy_sixel-0.1.1 (c (n "icy_sixel") (v "0.1.1") (h "0pilrnhbs175f16y471naia7l8dxn96wgim461vj9z632q1d7i0x")))

(define-public crate-icy_sixel-0.1.2 (c (n "icy_sixel") (v "0.1.2") (h "0nqmq5c3hawvpqdl6rskyc93qjf36pa0mjzgzmn5jk9803l8m1c6")))

