(define-module (crates-io ic -r ic-representation-independent-hash) #:use-module (crates-io))

(define-public crate-ic-representation-independent-hash-0.3.0 (c (n "ic-representation-independent-hash") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0ph7jvbp960gkb91jhd2psa9l0zi1lxlnmc3imjcwlip9gzr0msz")))

(define-public crate-ic-representation-independent-hash-1.0.0 (c (n "ic-representation-independent-hash") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "17iz4vzxh6afi6rjwpx9xcsx11q6v72c0ywvxvzzk08s4w34w87j")))

(define-public crate-ic-representation-independent-hash-1.0.1 (c (n "ic-representation-independent-hash") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0np85s5rdwav09a99h1rv9sw3kmmlshgrp5g0fhflhfdsx38nsp1")))

(define-public crate-ic-representation-independent-hash-1.0.2 (c (n "ic-representation-independent-hash") (v "1.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "12lg243jz1p81ji2k96ljhcf120ksphwmgkfavl63pvn1djvlr5b")))

(define-public crate-ic-representation-independent-hash-1.1.0 (c (n "ic-representation-independent-hash") (v "1.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0z6r005cknspwl82572cwggkpxzyk0pjmjb38xcjjwj91k2zf5zd")))

(define-public crate-ic-representation-independent-hash-1.2.0 (c (n "ic-representation-independent-hash") (v "1.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1sk9p3pj3rhxvpsjj7wjy69195pnf09rhw3jr95qqvmq2v5kq3j1")))

(define-public crate-ic-representation-independent-hash-1.3.0 (c (n "ic-representation-independent-hash") (v "1.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "079znd7lg7dhsrisrkkwnsyljj60pd2dlx7glnnva8fbh53ym0li")))

(define-public crate-ic-representation-independent-hash-2.0.0 (c (n "ic-representation-independent-hash") (v "2.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1caz7mscsl05vdzwvwk1m4sv677pi33d7sjrswks84v4ms1ngbvq")))

(define-public crate-ic-representation-independent-hash-2.0.1 (c (n "ic-representation-independent-hash") (v "2.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0pqpi19v8fis6p9rjh2z1322ar1wrcp0wdsz9iv5ycdn11xich3b")))

(define-public crate-ic-representation-independent-hash-2.1.0 (c (n "ic-representation-independent-hash") (v "2.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0aj75bwjmq3jz7i7xkhnj56nr7naphxxfs5lixs5q0h7jmdyyns6")))

(define-public crate-ic-representation-independent-hash-2.2.0 (c (n "ic-representation-independent-hash") (v "2.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0q4l2bnby0gsvbpshi51mvf6dby7a9psbfvi32fl3bdb6iidwpfi")))

(define-public crate-ic-representation-independent-hash-2.3.0 (c (n "ic-representation-independent-hash") (v "2.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "088yrp57vlcf69jvqqdksw1qfwm2vpg4shj61wm9njwmicpxnj5y")))

(define-public crate-ic-representation-independent-hash-2.4.0 (c (n "ic-representation-independent-hash") (v "2.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "120w305xjicb5dcq5i8ma1k49crgmi6h6ga7gxdvzxgv5hvr0z15")))

(define-public crate-ic-representation-independent-hash-2.5.0 (c (n "ic-representation-independent-hash") (v "2.5.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "07yzjv842aqnvwympfanz0z503v8fabsf79lam9496qfr1lwkndl")))

