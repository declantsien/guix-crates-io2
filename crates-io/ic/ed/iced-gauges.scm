(define-module (crates-io ic ed iced-gauges) #:use-module (crates-io))

(define-public crate-iced-gauges-0.0.1 (c (n "iced-gauges") (v "0.0.1") (d (list (d (n "iced") (r "^0.9") (f (quote ("canvas" "debug"))) (d #t) (k 0)) (d (n "iced") (r "^0.9") (f (quote ("tokio" "glow"))) (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 2)))) (h "0wvbhsna83hzqzkpyanab17x67vbdyk3isqpn0vyv01313y7ihnr") (y #t)))

