(define-module (crates-io ic ed iced_style) #:use-module (crates-io))

(define-public crate-iced_style-0.0.0 (c (n "iced_style") (v "0.0.0") (d (list (d (n "iced_core") (r "^0.1.0") (d #t) (k 0)))) (h "1gsmg7ns0mrn4kamwhqyf8pgpmn0jjc4kqcjwxvg6innlhbhnlav")))

(define-public crate-iced_style-0.1.0 (c (n "iced_style") (v "0.1.0") (d (list (d (n "iced_core") (r "^0.2") (d #t) (k 0)))) (h "0ag7smwbfm9vhycr39qbr1msvjixmzdwkrgjc090cw8b9b4wwz5v")))

(define-public crate-iced_style-0.2.0 (c (n "iced_style") (v "0.2.0") (d (list (d (n "iced_core") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "05hxkdnmal7jvhq33lmcr72lgj4r5rqyx6j5g8znnr2xrjq4zszh")))

(define-public crate-iced_style-0.3.0 (c (n "iced_style") (v "0.3.0") (d (list (d (n "iced_core") (r "^0.4") (d #t) (k 0)))) (h "0dvqrpgvlhq171gdl1qnc640nl7p9k54a3chy8qzp6vmh1m0d4ya")))

(define-public crate-iced_style-0.4.0 (c (n "iced_style") (v "0.4.0") (d (list (d (n "iced_core") (r "^0.5") (d #t) (k 0)))) (h "1hkbijrb4187wvlb9i998w6m2l9sb6d4kcbg7g6i6b5n9b4jh059")))

(define-public crate-iced_style-0.5.0 (c (n "iced_style") (v "0.5.0") (d (list (d (n "iced_core") (r "^0.6") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)))) (h "0p369svx8gbvrz9psrs5b76yn8cni49lw7cw991g22b1jd3ssv6a")))

(define-public crate-iced_style-0.5.1 (c (n "iced_style") (v "0.5.1") (d (list (d (n "iced_core") (r "^0.6") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)))) (h "0nsgamsaa9x6pyvq4hh5g82zm3ihz6672h3nb4gqm2aawdivgzx2")))

(define-public crate-iced_style-0.6.0 (c (n "iced_style") (v "0.6.0") (d (list (d (n "iced_core") (r "^0.7") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)))) (h "1x6s0dx097g7qmkndyj1g8088lpg87qpn3pwc4vayqcchbhzb4mr")))

(define-public crate-iced_style-0.7.0 (c (n "iced_style") (v "0.7.0") (d (list (d (n "iced_core") (r "^0.8") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)))) (h "1dh0jwq6nc942qdvkcjqmfx8caqifiwna9lm06dzzf9jgmabjpmd")))

(define-public crate-iced_style-0.8.0 (c (n "iced_style") (v "0.8.0") (d (list (d (n "iced_core") (r "^0.9") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)))) (h "0c7z6b98dpw5xh4507b7ldyd66q5x960vk820ca024lrq8yk6dqf")))

(define-public crate-iced_style-0.9.0 (c (n "iced_style") (v "0.9.0") (d (list (d (n "iced_core") (r "^0.10") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)))) (h "181i3awcicfh3m05r19bplr1c47jv9jwi66zbpvq28iys7clfp6q")))

(define-public crate-iced_style-0.12.0 (c (n "iced_style") (v "0.12.0") (d (list (d (n "iced_core") (r "^0.12") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)))) (h "0n40bsfxixnlppplw050qqblrgl6nxmd10qfrs9i7b7fpj93jszb")))

(define-public crate-iced_style-0.12.1 (c (n "iced_style") (v "0.12.1") (d (list (d (n "iced_core") (r "^0.12") (f (quote ("palette"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)))) (h "0ydmmmn1h7fpxrszdj0bcvxyj5lblaya0flwzylsblhm15s2m91f")))

