(define-module (crates-io ic ed iced_searchable_picklist) #:use-module (crates-io))

(define-public crate-iced_searchable_picklist-0.1.0 (c (n "iced_searchable_picklist") (v "0.1.0") (d (list (d (n "iced_core") (r "^0.5.0") (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.4.0") (d #t) (k 0)))) (h "072z1g32504d9fr88c6vl8522bviz5y76pigmirfsy0acdcik1xh")))

(define-public crate-iced_searchable_picklist-0.1.1 (c (n "iced_searchable_picklist") (v "0.1.1") (d (list (d (n "iced_core") (r "^0.5.0") (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.4.0") (d #t) (k 0)))) (h "0gfqz3wwlsywniw0fjbbw9r9lidh4jnvjdj79yr6n26crnsz843i")))

(define-public crate-iced_searchable_picklist-0.1.2 (c (n "iced_searchable_picklist") (v "0.1.2") (d (list (d (n "iced_core") (r "^0.5.0") (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.4.0") (d #t) (k 0)))) (h "05k6myvx7yf26rc964kkkvpxjfra4nlva74iqxfswawwxlb3ni8g")))

(define-public crate-iced_searchable_picklist-0.1.3 (c (n "iced_searchable_picklist") (v "0.1.3") (d (list (d (n "iced_core") (r "^0.5") (k 0)) (d (n "iced_native") (r "^0.5") (d #t) (k 0)) (d (n "iced_style") (r "^0.4") (d #t) (k 0)))) (h "179kwilmf99r61yissas4lh1x2pmk4ria6bysv9x2hqnl526fpp2")))

(define-public crate-iced_searchable_picklist-0.2.0 (c (n "iced_searchable_picklist") (v "0.2.0") (d (list (d (n "iced_core") (r "^0.6") (k 0)) (d (n "iced_native") (r "^0.6") (d #t) (k 0)) (d (n "iced_style") (r "^0.5") (d #t) (k 0)))) (h "1wjqrj0ypw8n0aka9x6235s1nwafk76b39a7m5ddpklxm28qpwi0")))

