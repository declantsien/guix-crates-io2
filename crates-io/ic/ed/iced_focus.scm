(define-module (crates-io ic ed iced_focus) #:use-module (crates-io))

(define-public crate-iced_focus-0.1.0 (c (n "iced_focus") (v "0.1.0") (d (list (d (n "iced") (r "^0.3.0") (d #t) (k 0)) (d (n "iced_focus_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "0nykv3943cj335i7h3yaqg0n5p7c8kajvfl6b80albx9fb2bxjyr") (f (quote (("derive" "iced_focus_derive"))))))

(define-public crate-iced_focus-0.1.1 (c (n "iced_focus") (v "0.1.1") (d (list (d (n "iced") (r "^0.3.0") (d #t) (k 0)) (d (n "iced_focus_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1hb1bi5pjhs3a3vz4gjajhvjzbhhjabkhkpcmhmqsn8nlysgg348") (f (quote (("derive" "iced_focus_derive"))))))

