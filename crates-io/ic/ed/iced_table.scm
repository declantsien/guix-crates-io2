(define-module (crates-io ic ed iced_table) #:use-module (crates-io))

(define-public crate-iced_table-0.1.0 (c (n "iced_table") (v "0.1.0") (d (list (d (n "iced_core") (r "^0.10") (d #t) (k 0)) (d (n "iced_style") (r "^0.9") (d #t) (k 0)) (d (n "iced_widget") (r "^0.1") (d #t) (k 0)))) (h "182f4yx4zl7994lzawlz5y0zfrqax6nh96hx9flrjh6vnxrapkdx")))

(define-public crate-iced_table-0.2.0 (c (n "iced_table") (v "0.2.0") (d (list (d (n "iced_core") (r "^0.10") (d #t) (k 0)) (d (n "iced_style") (r "^0.9") (d #t) (k 0)) (d (n "iced_widget") (r "^0.1") (d #t) (k 0)))) (h "1bswl6xhkz32y34dmkzi5j3rq097vswfnnz5w3wak5a93pnrn9fz")))

(define-public crate-iced_table-0.12.0 (c (n "iced_table") (v "0.12.0") (d (list (d (n "iced_core") (r "^0.12") (d #t) (k 0)) (d (n "iced_style") (r "^0.12") (d #t) (k 0)) (d (n "iced_widget") (r "^0.12") (d #t) (k 0)))) (h "0lbb5xb0k00wdljld6xylkmbadpk8vfvxh4sdarf7rfpdh6z0is3")))

