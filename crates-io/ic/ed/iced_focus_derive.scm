(define-module (crates-io ic ed iced_focus_derive) #:use-module (crates-io))

(define-public crate-iced_focus_derive-0.1.0 (c (n "iced_focus_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09xjjxaf1w5pv7g91sdlzy90b81590i2gkm1kdlrpihins90nsy2")))

(define-public crate-iced_focus_derive-0.1.1 (c (n "iced_focus_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zl5xngw61r21dlf7gy8jwwnnqizh9x7i1vxrg3xmdpbyl1445bw")))

