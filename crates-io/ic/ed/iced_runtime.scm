(define-module (crates-io ic ed iced_runtime) #:use-module (crates-io))

(define-public crate-iced_runtime-0.0.1 (c (n "iced_runtime") (v "0.0.1") (h "1vaavhai9sn95inpw7v1848y73cpd6qpgmp5ynh3aqyb9q2hp68p")))

(define-public crate-iced_runtime-0.1.0 (c (n "iced_runtime") (v "0.1.0") (d (list (d (n "iced_core") (r "^0.10") (d #t) (k 0)) (d (n "iced_futures") (r "^0.7") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ylv04m4ahr9a43y3v6szz1lrnj5mpkkkgzqgjcs9sfhd1bmrj38") (f (quote (("debug"))))))

(define-public crate-iced_runtime-0.1.1 (c (n "iced_runtime") (v "0.1.1") (d (list (d (n "iced_core") (r "^0.10") (d #t) (k 0)) (d (n "iced_futures") (r "^0.7") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g987sr9iq8hdx2qwjrxnglpnla447x1al1cr3xccl0j7s2qjv3w") (f (quote (("debug"))))))

(define-public crate-iced_runtime-0.12.0 (c (n "iced_runtime") (v "0.12.0") (d (list (d (n "iced_core") (r "^0.12") (d #t) (k 0)) (d (n "iced_futures") (r "^0.12") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nls44nmf1kh0kgqh2zgznj73h22pvqa9dq511gikjjjsxf892k1") (f (quote (("multi-window") ("debug"))))))

(define-public crate-iced_runtime-0.12.1 (c (n "iced_runtime") (v "0.12.1") (d (list (d (n "iced_core") (r "^0.12") (d #t) (k 0)) (d (n "iced_futures") (r "^0.12") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08k8yjaf0wym8zl09i98b8qsvhsajyrrqdwl7ik62vfc04n8b7x7") (f (quote (("multi-window") ("debug"))))))

