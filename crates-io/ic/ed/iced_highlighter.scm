(define-module (crates-io ic ed iced_highlighter) #:use-module (crates-io))

(define-public crate-iced_highlighter-0.0.1 (c (n "iced_highlighter") (v "0.0.1") (h "0d62dh8zi2ywlb1g3snx383mhd6gnqy4n6nfdy36f0myki0gy556")))

(define-public crate-iced_highlighter-0.12.0 (c (n "iced_highlighter") (v "0.12.0") (d (list (d (n "iced_core") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^5.1") (d #t) (k 0)))) (h "1b8575y8v8xb6vgk3pak0sj9i7pzz44pb40296nmaqjjbwg7vlm1")))

