(define-module (crates-io ic ed iced-cpuid) #:use-module (crates-io))

(define-public crate-iced-cpuid-1.0.0 (c (n "iced-cpuid") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "iced-x86") (r "^1.12.0") (f (quote ("std" "decoder" "instr_info" "op_code_info"))) (k 0)) (d (n "object") (r "^0.25.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0csabl6p7wjrkncf1wm56v54b3hgpnwc2is9m6lw4jzdkvqx81m7")))

