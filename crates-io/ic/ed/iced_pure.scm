(define-module (crates-io ic ed iced_pure) #:use-module (crates-io))

(define-public crate-iced_pure-0.1.0 (c (n "iced_pure") (v "0.1.0") (h "1jvpfgr447bwhkdlyni3ysn3hazdks5pd1c4z5rvbgin8cs0pjdh")))

(define-public crate-iced_pure-0.2.0 (c (n "iced_pure") (v "0.2.0") (d (list (d (n "iced_native") (r "^0.5") (d #t) (k 0)) (d (n "iced_style") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02kpx9jkk9bjr3ac3dy8nny9z5q9x2w29fsdaz5alp8azn3b36ai")))

(define-public crate-iced_pure-0.2.1 (c (n "iced_pure") (v "0.2.1") (d (list (d (n "iced_native") (r "^0.5") (d #t) (k 0)) (d (n "iced_style") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18nn56gk64xg62adrph8qxnf3yfwhdjpfpvfalgq4dbww0jyqqkf")))

(define-public crate-iced_pure-0.2.2 (c (n "iced_pure") (v "0.2.2") (d (list (d (n "iced_native") (r "^0.5") (d #t) (k 0)) (d (n "iced_style") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14zkf8g4adr0hlni8s2xqm8nl9fwg8szvgbwg0n2r0ynvz5axbl0")))

