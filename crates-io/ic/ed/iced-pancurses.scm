(define-module (crates-io ic ed iced-pancurses) #:use-module (crates-io))

(define-public crate-iced-pancurses-0.1.0 (c (n "iced-pancurses") (v "0.1.0") (d (list (d (n "iced") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "114f5z4571xb94gzzwli4sycr1g1rnj5ii3vkba8126643lgb429") (y #t)))

(define-public crate-iced-pancurses-0.1.1 (c (n "iced-pancurses") (v "0.1.1") (d (list (d (n "iced") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0yl6213a18l2sdanw4jys29cbpmyfcjzpp6bmcrvbmbn7d71a1hj") (y #t)))

(define-public crate-iced-pancurses-0.1.2 (c (n "iced-pancurses") (v "0.1.2") (d (list (d (n "iced_native") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "1jqgx0my0xgaxzmmjxf8wxahgxd8xg1jqc8gkgibnkjrsl9ccyy3")))

