(define-module (crates-io ic ed iced_audio_widgets) #:use-module (crates-io))

(define-public crate-iced_audio_widgets-0.0.0 (c (n "iced_audio_widgets") (v "0.0.0") (h "1aln2lf02s73kxwvslbpn7js95dgb81hgw1s8fsr57v1zbbkhz95") (y #t)))

(define-public crate-iced_audio_widgets-0.0.1 (c (n "iced_audio_widgets") (v "0.0.1") (h "16i7p7m8kv9ynj4ky7kyl4460i81s1nhkqhjmz398cvmiz4dp4ia")))

