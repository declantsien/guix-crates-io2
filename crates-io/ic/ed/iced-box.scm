(define-module (crates-io ic ed iced-box) #:use-module (crates-io))

(define-public crate-iced-box-0.1.0 (c (n "iced-box") (v "0.1.0") (d (list (d (n "iced") (r "^0.12.1") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1y82c993hwkh35pk4h41y01cmjmdcz0daf1wrd171a9r9kcf5i9m")))

(define-public crate-iced-box-0.2.0 (c (n "iced-box") (v "0.2.0") (h "0sa0fi2yiym90wpbllyp35w673yqv8kipdincvhw6m3gpk3xdvph") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.1 (c (n "iced-box") (v "0.2.1") (d (list (d (n "iced") (r "^0.12.1") (d #t) (k 0)))) (h "1pwdrsjgppm5hckdxv2r24vz8vlpmmyrfqhng650idp07ngmfw53") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.2 (c (n "iced-box") (v "0.2.2") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "04c96kx0s15dkszj2b7yffdpjlpcp33ncmj7v1rnvj5sb8b9hvgy") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.3 (c (n "iced-box") (v "0.2.3") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "0dpmlzxmg2aw3869wpp9c5vbja1b7f0j1cznflpmqmw3rq9lzzbv") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.4 (c (n "iced-box") (v "0.2.4") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "0nxs9pgnprbsmpn8fl0aqk2qgmnwxylhbi0ybx1h82s8n6b7jh20") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.5 (c (n "iced-box") (v "0.2.5") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "0a56rwjsy9vppgjmc4blnd0fqwfg8vb419s4dn66p3vp0a61bsyy") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.6 (c (n "iced-box") (v "0.2.6") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "1nisx9lnmcsc6psc92gbk9wzw75vfckgpv4kb67cidzh8vymqpaj") (f (quote (("icon"))))))

(define-public crate-iced-box-0.2.7 (c (n "iced-box") (v "0.2.7") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "0gix8babm8ligl9dndam330cm9yczk9z3m520gai12irqd5jkv68") (f (quote (("icon"))))))

(define-public crate-iced-box-0.3.0 (c (n "iced-box") (v "0.3.0") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "02qfhlndarcyqskn2r297sx3f9vsv4ws4wkday72galg01fhmdbr") (f (quote (("icon"))))))

(define-public crate-iced-box-0.3.1 (c (n "iced-box") (v "0.3.1") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "15g52mwan5iqc1dg7z62yfh4hc8hbsjp8fs7yxf7m3h8a5qdrs3g") (f (quote (("icon"))))))

(define-public crate-iced-box-0.3.2 (c (n "iced-box") (v "0.3.2") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "1vi0fpr0sn6s1n1pallv18fz8llp9nip24al86cj8g2rqvkbrmqk") (f (quote (("icon"))))))

(define-public crate-iced-box-0.3.3 (c (n "iced-box") (v "0.3.3") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "0r38a5hjamf05s81yl7k9r3s7iq53lqcpb950v08mky4bl8cn8r5") (f (quote (("material_icons" "icon") ("lucide_icons" "icon") ("icon"))))))

(define-public crate-iced-box-0.4.0 (c (n "iced-box") (v "0.4.0") (d (list (d (n "iced") (r "^0.10") (k 0)))) (h "11pzd0i57s5k45bn10gvdp0xg8bqmxsq98fiza0d1w236x16hd96") (f (quote (("material_icons" "icon") ("lucide_icons" "icon") ("icon"))))))

(define-public crate-iced-box-0.5.0 (c (n "iced-box") (v "0.5.0") (d (list (d (n "iced") (r "^0.12.1") (k 0)))) (h "03spq2nvwlwq1mqwxnpb5bhrggfl1r370khpqz78k9y9x3mvhw9n") (f (quote (("material_icons" "icon") ("lucide_icons" "icon") ("icon"))))))

