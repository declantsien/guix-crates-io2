(define-module (crates-io ic ed iced-hex-color) #:use-module (crates-io))

(define-public crate-iced-hex-color-0.1.0 (c (n "iced-hex-color") (v "0.1.0") (d (list (d (n "hex_color") (r "^2.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 2)) (d (n "iced_core") (r "^0.10") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m4733snij55q65kygkmpl7qxh57xgz75g7fg9irx5dp46qsrmgl")))

(define-public crate-iced-hex-color-0.1.1 (c (n "iced-hex-color") (v "0.1.1") (d (list (d (n "hex_color") (r "^2.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 2)) (d (n "iced_core") (r "^0.10") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6a3rjrk0kpdmdccdml2mbvkazq1c6j559sll6ij9cnvhr6nimi")))

