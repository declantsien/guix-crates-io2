(define-module (crates-io ic ed iced_sessionlock) #:use-module (crates-io))

(define-public crate-iced_sessionlock-0.2.4 (c (n "iced_sessionlock") (v "0.2.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "iced") (r "^0.12") (d #t) (k 0)) (d (n "iced_core") (r "^0.12") (d #t) (k 0)) (d (n "iced_futures") (r "^0.12.0") (d #t) (k 0)) (d (n "iced_graphics") (r "^0.12.0") (d #t) (k 0)) (d (n "iced_renderer") (r "^0.12.0") (d #t) (k 0)) (d (n "iced_runtime") (r "^0.12") (f (quote ("multi-window"))) (d #t) (k 0)) (d (n "iced_style") (r "^0.12") (d #t) (k 0)) (d (n "sessionlockev") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1qjp12vn96k374xw770kjwf1r9hw0lz5x6g84dx95z2qrb730yhm")))

