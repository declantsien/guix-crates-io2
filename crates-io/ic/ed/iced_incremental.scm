(define-module (crates-io ic ed iced_incremental) #:use-module (crates-io))

(define-public crate-iced_incremental-0.0.0 (c (n "iced_incremental") (v "0.0.0") (d (list (d (n "iced") (r "^0.1.0") (d #t) (k 0)) (d (n "iced_graphics") (r "^0.0.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.2.2") (d #t) (k 0)))) (h "1mmi7yfm5rbdffb29dd97gqqizqj86mvl47l8nlns2qj526n1wz8")))

