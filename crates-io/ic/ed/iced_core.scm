(define-module (crates-io ic ed iced_core) #:use-module (crates-io))

(define-public crate-iced_core-0.0.0 (c (n "iced_core") (v "0.0.0") (h "19vqrphybiy7ysb4l9f4x1f9lsd8sgc1pb3fm34hqf0d954y66iz")))

(define-public crate-iced_core-0.1.0 (c (n "iced_core") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1za7vsypjryz5vzswb2a5ks039mq467pgxrm7z8qis6zprbyphzc") (f (quote (("command" "futures"))))))

(define-public crate-iced_core-0.2.0 (c (n "iced_core") (v "0.2.0") (h "0f87hiw9b39hdwka7bj2x8qhbgnnwinh50k91qhz3gd8y26v7pgf")))

(define-public crate-iced_core-0.2.1 (c (n "iced_core") (v "0.2.1") (h "1pf2hl9dzkx4lr5knx651z36yd40i9narbspvnf16s81y14rq44c")))

(define-public crate-iced_core-0.3.0 (c (n "iced_core") (v "0.3.0") (d (list (d (n "palette") (r ">=0.5.0, <0.6.0") (o #t) (d #t) (k 0)))) (h "19d3xdbixii8rkphz79vb8wxcdyjm2ghpv9j2j5b61bd5f24ynf1")))

(define-public crate-iced_core-0.4.0 (c (n "iced_core") (v "0.4.0") (d (list (d (n "palette") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1gv9sw6sd8ivdk7ab093vkyfgwz0as236iykpbrpcn78xrg4mzps")))

(define-public crate-iced_core-0.5.0 (c (n "iced_core") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0j419bnxdqk2k9x2gl729kvrr3m8ibxrfmbx0i0ccpilxcy17yfc")))

(define-public crate-iced_core-0.6.0 (c (n "iced_core") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "15kr4a3ml5n7bkx5gxgvj84rlxamc9lvkh0nswhzigpv7zprq6ij")))

(define-public crate-iced_core-0.6.1 (c (n "iced_core") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0fhq9yi7zabx3427a707ypd1z6ic1rv5qwk39xhla32qr2bbsnmm")))

(define-public crate-iced_core-0.6.2 (c (n "iced_core") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "103fjrrg6xywv7b497p1xnyi1zq6fg0iiippd7yz5bmqxhz74b13")))

(define-public crate-iced_core-0.7.0 (c (n "iced_core") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1vicmyzmlpcix3cd8ppw5brm3h0yw7lzm755f4cn52zb2dk6dpka")))

(define-public crate-iced_core-0.8.0 (c (n "iced_core") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)))) (h "14k2vm0y5qqrcxxchqnqz7wlf24d4gj41fzqm6ky747a9lxf8wd5")))

(define-public crate-iced_core-0.8.1 (c (n "iced_core") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1w847gh6xxzwbg5m2k1syp5dr5x30jzxwhc1jc5bqpq5zhvqghlw")))

(define-public crate-iced_core-0.9.0 (c (n "iced_core") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "palette") (r "^0.6") (o #t) (d #t) (k 0)))) (h "10jp2cxsn2gpr5z65ybbzdcixp4chdx7xri7ribfgpny50p99q8i")))

(define-public crate-iced_core-0.10.0 (c (n "iced_core") (v "0.10.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "palette") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (k 0)))) (h "1j7bnkzlv95297i4mq05ph5z3ilc0pk3iy4kip87d181px7vrl34")))

(define-public crate-iced_core-0.12.0 (c (n "iced_core") (v "0.12.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1prpz5i0xmfm6lp9fpvh712c87nwhi6kw8waziaq2zmg31k25yam")))

(define-public crate-iced_core-0.12.1 (c (n "iced_core") (v "0.12.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1axhhryydvi2n47586wm7kjhhvw6n6ak1hahqg30f22mrh283xnq")))

(define-public crate-iced_core-0.12.2 (c (n "iced_core") (v "0.12.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "14aw529f3mx488ngfvy5dcghzqbj8j3acilapkk6smsc464r02rn")))

(define-public crate-iced_core-0.12.3 (c (n "iced_core") (v "0.12.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0c5rr5rl6yls5a3dlb75zlc17q0cdf3m25mpv39iwcbz36ynnzkx")))

