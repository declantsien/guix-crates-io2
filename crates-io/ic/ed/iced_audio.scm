(define-module (crates-io ic ed iced_audio) #:use-module (crates-io))

(define-public crate-iced_audio-0.0.0 (c (n "iced_audio") (v "0.0.0") (h "11yk50n1qbfc6k22j5g1qpyhf435m3k6ccacvfr68cnm5vq1w823")))

(define-public crate-iced_audio-0.0.1 (c (n "iced_audio") (v "0.0.1") (d (list (d (n "iced") (r "^0.1.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2.2") (d #t) (k 0)))) (h "1mcgli7h9f1srbwh37rzmg938d7q794p2v5xzcxk3zd58dfvl74l")))

(define-public crate-iced_audio-0.0.2 (c (n "iced_audio") (v "0.0.2") (d (list (d (n "iced") (r "^0.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "00cmdr4q69inbvrhwgnr6g4fs9y7cm4pa3qk08afgsyl5zim08vp")))

(define-public crate-iced_audio-0.0.3 (c (n "iced_audio") (v "0.0.3") (d (list (d (n "iced") (r "^0.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0jcf4jflh2m2g6n75labclkki8f2nrfarwzafvf39q85yhvigmzg")))

(define-public crate-iced_audio-0.0.4 (c (n "iced_audio") (v "0.0.4") (d (list (d (n "iced") (r "^0.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "14dvprqj9l7figmby9bsg6b498yxqhpvw3p15b5mr77wfmhnfxn5")))

(define-public crate-iced_audio-0.0.5 (c (n "iced_audio") (v "0.0.5") (d (list (d (n "iced") (r "^0.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0gzmrhdyipwr78qlpsx0z6kbxsawn79khnnjdqfjnywvjg82zlj1")))

(define-public crate-iced_audio-0.0.6 (c (n "iced_audio") (v "0.0.6") (d (list (d (n "iced") (r "^0.1") (f (quote ("image"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "01vbk05gcdihry68nvrwm98knlsa9qvj6kbab5yh21zw9d396ip6")))

(define-public crate-iced_audio-0.0.7 (c (n "iced_audio") (v "0.0.7") (d (list (d (n "iced") (r "^0.1") (f (quote ("image"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0fl7ykg2diq0f83kj3wg3gapnfpqkikhn26nypp6hagi7mjacv9q")))

(define-public crate-iced_audio-0.0.8 (c (n "iced_audio") (v "0.0.8") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas" "image"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1r9vwahm9iakjklms2jzljys75mf0agx27lb62y9znbq2mv8svc6")))

(define-public crate-iced_audio-0.0.9 (c (n "iced_audio") (v "0.0.9") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas" "image"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1gyjxp4585rrcnakk3gx0bg7611disbanmr8li2cig18rhkzxqss")))

(define-public crate-iced_audio-0.0.10 (c (n "iced_audio") (v "0.0.10") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0616681qik56w3z2hfa88bvz55m7rl045jf15vsj0rdyx66wlgax") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.0.11 (c (n "iced_audio") (v "0.0.11") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "046wgwyyyz11fa2lnzs778fqsqszkyz4n3zxcjdmp69xjmspbzny") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.0.12 (c (n "iced_audio") (v "0.0.12") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1z8d59y4kn8n9571jilwynll0kr8k411hn8ydb788mkv8lp2k3f2") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.1.0 (c (n "iced_audio") (v "0.1.0") (d (list (d (n "iced") (r "^0.1") (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1qrfa2ddrdrf5yygs2rv1nxb5qlzwyg8bryaazy735g9r84xi95i") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.1.1 (c (n "iced_audio") (v "0.1.1") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1ckcfwpxh2cmzg702kd28sc395d5vs96xambhpja3l6p3xmx4gl8") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.1.2 (c (n "iced_audio") (v "0.1.2") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1fhf06k8nb73fvrlil2sgkwa1s180kzwiwdp6nhhs9iyikx91wa6") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.0 (c (n "iced_audio") (v "0.2.0") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0ab68jqk9a4cn48kbsyx7v1d82xnpgmnmnlvpq5lz38w5l769933") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.1 (c (n "iced_audio") (v "0.2.1") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1lzszxqc096fvf40pwxhxqah8w597xbbgxjjkynd7f0hvq54pad9") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.2 (c (n "iced_audio") (v "0.2.2") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1j2h9iqs6sh87kdfz7mqbg18g9n201gkjnn6svmc79n13ndyjipm") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.3 (c (n "iced_audio") (v "0.2.3") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0aw55l1iblq169zp40imnqcs80bl4ymwb1dqrrym7c3k4vp0k3lb") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.4 (c (n "iced_audio") (v "0.2.4") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0g5z1d4qg31hk2ry6b9vjjngyrjwqfw5h3x2306zsd2g0714785p") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.5 (c (n "iced_audio") (v "0.2.5") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1zimyhzbk2mhflfs7b2x6409zasglpw2jywwsi7lnc7j0fh7gq4r") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.2.6 (c (n "iced_audio") (v "0.2.6") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "11kxfi62v1g1j82m4hf6jfarmqqm403mi2b7zhv587ap7fk0v3wc") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.3.0 (c (n "iced_audio") (v "0.3.0") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1l25qwxmc9mc5q9rpvx331n2rxgg5pnpg5lkyjip42fcv5gxpjaz") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.3.1 (c (n "iced_audio") (v "0.3.1") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "1wkiyh0vkh66jwjv49iqwpxv71z79496p8a0261cl1ai4lgfjy6a") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.4.0 (c (n "iced_audio") (v "0.4.0") (d (list (d (n "iced") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.2") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.2") (d #t) (k 0)))) (h "0fyxxw8y96jfry6ajk5cfqi1zfhz240swrk9gnyi14ngi7aba9kn") (f (quote (("svg" "iced/svg") ("image" "iced/image") ("default" "image"))))))

(define-public crate-iced_audio-0.5.0 (c (n "iced_audio") (v "0.5.0") (d (list (d (n "iced") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "iced_graphics") (r ">=0.1.0, <0.2.0") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "0k46l15c6zqm1iq7j1qg17adzi9pxwdfys16myaplkcxd2fwzrgd")))

(define-public crate-iced_audio-0.5.1 (c (n "iced_audio") (v "0.5.1") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "0937g8wb91j4j6qikp6pxz10ba2h5c215ki13chab0rk211pvffj")))

(define-public crate-iced_audio-0.5.2 (c (n "iced_audio") (v "0.5.2") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "1rir2chzvm9vycpmpny2vgvccxsy9i13vaify552nnsb78cj1fgp")))

(define-public crate-iced_audio-0.5.3 (c (n "iced_audio") (v "0.5.3") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "1wn9hmi4c3v5a26n4j5g8vvpjxifchm94vxylz3jmx2g83k8wz2c")))

(define-public crate-iced_audio-0.5.4 (c (n "iced_audio") (v "0.5.4") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "0zsnmmfaly8xx2biif71l50zw2pjx4i9gdjpczdwj9dl74pzrdnn")))

(define-public crate-iced_audio-0.6.0 (c (n "iced_audio") (v "0.6.0") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "0lx9brqiqpgm52zij1xmyg9g8y9c4lz8046l0xqljh25rk4qdq9w")))

(define-public crate-iced_audio-0.6.1 (c (n "iced_audio") (v "0.6.1") (d (list (d (n "iced") (r "^0.2") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.1") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.3") (d #t) (k 0)))) (h "12wbzksw97kpls4x8qzjzv1w4549gxj0xjbkphm1fav1frdy17a7")))

(define-public crate-iced_audio-0.7.0 (c (n "iced_audio") (v "0.7.0") (d (list (d (n "iced") (r "^0.3") (d #t) (k 2)) (d (n "iced_graphics") (r "^0.2") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.4") (d #t) (k 0)))) (h "1pacn5km8by5sgqpsg418s2jqmk0fk5fcm6mpanlc0yzkb6vv1hs")))

(define-public crate-iced_audio-0.8.0 (c (n "iced_audio") (v "0.8.0") (d (list (d (n "iced") (r "^0.4") (f (quote ("canvas"))) (d #t) (k 2)) (d (n "iced_graphics") (r "^0.3") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.5") (d #t) (k 0)))) (h "0dhdvh3mlxnzq9qxp4k6z57bwj4cqy8jmhdsf8cp6b2dbl027k5b")))

(define-public crate-iced_audio-0.9.0 (c (n "iced_audio") (v "0.9.0") (d (list (d (n "iced") (r "^0.6") (d #t) (k 2)) (d (n "iced_core") (r "^0.6") (d #t) (k 0)) (d (n "iced_glow") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "iced_graphics") (r "^0.5") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.7") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.7") (o #t) (d #t) (k 0)))) (h "094qwkfhq6v9mh7zd483qabr7jlfgaf0vybka8wk3kvwl1bcfq4x") (f (quote (("wgpu" "iced_wgpu") ("glow" "iced_glow") ("default" "wgpu"))))))

(define-public crate-iced_audio-0.10.0 (c (n "iced_audio") (v "0.10.0") (d (list (d (n "iced") (r "^0.7") (d #t) (k 2)) (d (n "iced_core") (r "^0.7") (d #t) (k 0)) (d (n "iced_glow") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "iced_graphics") (r "^0.6") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.8") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.8") (o #t) (d #t) (k 0)))) (h "12pal1l8gakg5mq4z2k8yvsjxj9axzxg3jy2gcacp0sfjfxv63hz") (f (quote (("wgpu" "iced_wgpu") ("glow" "iced_glow") ("default" "wgpu"))))))

(define-public crate-iced_audio-0.11.0 (c (n "iced_audio") (v "0.11.0") (d (list (d (n "iced") (r "^0.8") (d #t) (k 2)) (d (n "iced_core") (r "^0.8") (d #t) (k 0)) (d (n "iced_glow") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "iced_graphics") (r "^0.7") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.9") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.9") (o #t) (d #t) (k 0)))) (h "08kwar20ck71har76clq3fyg4lscmlv12xn7vxv5w2lxzj1yliir") (f (quote (("wgpu" "iced_wgpu") ("glow" "iced_glow") ("default" "wgpu"))))))

(define-public crate-iced_audio-0.12.0 (c (n "iced_audio") (v "0.12.0") (d (list (d (n "iced") (r "^0.9") (d #t) (k 2)) (d (n "iced_core") (r "^0.9") (d #t) (k 0)) (d (n "iced_glow") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "iced_graphics") (r "^0.8") (f (quote ("canvas"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.10") (d #t) (k 0)) (d (n "iced_wgpu") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1xj5sdiacpjjrxxwdl2kyb27wws6y55fw79k3ajhi3sxxaz2fj1m") (f (quote (("wgpu" "iced_wgpu") ("glow" "iced_glow") ("default" "wgpu"))))))

