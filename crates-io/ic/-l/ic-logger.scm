(define-module (crates-io ic -l ic-logger) #:use-module (crates-io))

(define-public crate-ic-logger-0.1.0 (c (n "ic-logger") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "1c6wf0id39n8l5kpm1s3i8p70jl0khx1946xp6aah7lv6kfr6s31")))

