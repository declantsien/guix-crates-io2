(define-module (crates-io ic y- icy-epidural-underdog) #:use-module (crates-io))

(define-public crate-icy-epidural-underdog-0.1.0 (c (n "icy-epidural-underdog") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "axum-extra") (r "^0.8.0") (f (quote ("cookie"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "shuttle-axum") (r "^0.29.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "02fwrxvlmd8xglimn886ab4jrkf9in5fvlvfqbspjx6cnrsxdjwa")))

