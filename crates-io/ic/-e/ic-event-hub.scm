(define-module (crates-io ic -e ic-event-hub) #:use-module (crates-io))

(define-public crate-ic-event-hub-0.1.0 (c (n "ic-event-hub") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1vs662ygkjpi6fc90nc3qqwqs8n662gk3dvgm5vkbqc0fpmfzv0z")))

(define-public crate-ic-event-hub-0.1.1 (c (n "ic-event-hub") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "08va4zz5lzjz2j15m2by8wh1q1387ya39krn4sgs30c1vpr46hjr")))

(define-public crate-ic-event-hub-0.1.2 (c (n "ic-event-hub") (v "0.1.2") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0dqvlhbhqaqsl2lly4hmbdlffi6xzh3biqljhci6i5k35mp36dcd")))

(define-public crate-ic-event-hub-0.1.3 (c (n "ic-event-hub") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0r8r2rim06ysxwcdfpaffcvrwyhrlj89jf27zlpp9x1a4wj73cwn")))

(define-public crate-ic-event-hub-0.1.4 (c (n "ic-event-hub") (v "0.1.4") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "00s8l8s4jzlfhmjxj8712hdakdx4c46vcksls2qlkmskbhqy65yj")))

(define-public crate-ic-event-hub-0.1.5 (c (n "ic-event-hub") (v "0.1.5") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0pzpg4fhd6vq5gpn7sgdw2bbgcf2wm2l3bngdks0mlp6wppi5024")))

(define-public crate-ic-event-hub-0.1.6 (c (n "ic-event-hub") (v "0.1.6") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.2") (d #t) (k 0)))) (h "02lqij2k141mc08wx5zbxlbiddm1n55pl4482a59cmk9vfz2hw1g")))

(define-public crate-ic-event-hub-0.1.7 (c (n "ic-event-hub") (v "0.1.7") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "1rnhvbfmyhvqd73441ics0cm1imwmwybq4zm2nm83k1pdmk987dm")))

(define-public crate-ic-event-hub-0.1.9 (c (n "ic-event-hub") (v "0.1.9") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "1bbdlns2nw80spws98n32bih3cx7kjrgmbmk6x104yx1fivf1bra")))

(define-public crate-ic-event-hub-0.1.10 (c (n "ic-event-hub") (v "0.1.10") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "0r9ffjwcga6c1gv3h2a8iq1q27kc2l026iw2cpkni02fqdg69fjy")))

(define-public crate-ic-event-hub-0.3.0 (c (n "ic-event-hub") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "candid") (r "^0.7.12") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0wlknxfgavfjn2qk8ii9cwznb7dnczhxhcp8k6m2zidszbhcx3lv")))

(define-public crate-ic-event-hub-0.3.1 (c (n "ic-event-hub") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "candid") (r "^0.7.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1ix3zindivshbfg8mk4i5xg9z5z23hnc7jhkxwij03c2065i1gms")))

(define-public crate-ic-event-hub-0.3.2 (c (n "ic-event-hub") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "candid") (r "^0.7.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "19h1s3garn2540rq535i660m5gg14jj4nd928nrwpa72isaw698x")))

