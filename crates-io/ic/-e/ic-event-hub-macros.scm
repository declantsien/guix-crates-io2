(define-module (crates-io ic -e ic-event-hub-macros) #:use-module (crates-io))

(define-public crate-ic-event-hub-macros-0.1.0 (c (n "ic-event-hub-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "137qwl4sxbg8mvvynddv32y1bdy0n6av1zcbndmrrgd3vb7xxix0")))

(define-public crate-ic-event-hub-macros-0.1.1 (c (n "ic-event-hub-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "19zaqhk527zlq4h75hyqilcrh4p4nmcr26asxj12f779x3br72vj")))

(define-public crate-ic-event-hub-macros-0.1.3 (c (n "ic-event-hub-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0rffic9m08fijmw7msz1cx5qd2qjsd0ynkxm2rvmavdn6gh062j9")))

(define-public crate-ic-event-hub-macros-0.1.4 (c (n "ic-event-hub-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "09000aghaza3nmajsrxa25jkanf5y2410mamg0hnrk7bln8v4pp2")))

(define-public crate-ic-event-hub-macros-0.1.5 (c (n "ic-event-hub-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0jfnfcliwgpqrasw1m57q5xl5fnz0svwlq9xc355hnf9r8pss0ah")))

(define-public crate-ic-event-hub-macros-0.1.6 (c (n "ic-event-hub-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0qh8p30d8qkjjp3xllqvcg5cmwlzv7vs0skr51a8akd2qhsw495f")))

(define-public crate-ic-event-hub-macros-0.1.7 (c (n "ic-event-hub-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1zl5yam1ip8jb8p4n42ps0yyhgvn6x3kgiyj13qw0y3yschzs056")))

(define-public crate-ic-event-hub-macros-0.1.8 (c (n "ic-event-hub-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1q116fwv441vs4vzhqbs0ws3wcmrxj6gcvsi34sfi3j3bl3wh4nr") (y #t)))

(define-public crate-ic-event-hub-macros-0.1.9 (c (n "ic-event-hub-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0y4an53l93yg3mh7blhv072fd3hvif32103pzm8vbqcay0iajp8w")))

(define-public crate-ic-event-hub-macros-0.1.10 (c (n "ic-event-hub-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "00j93nr9l25nqc7d9i2vjp0r413z6qxn9kz0wgaxa4hf1hsxshkd")))

(define-public crate-ic-event-hub-macros-0.3.0 (c (n "ic-event-hub-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1dwsrm0xxkcbhlggy3mq8rcfzd7z2mldnb4w9nx6qa30dlp7bcfi")))

(define-public crate-ic-event-hub-macros-0.3.1 (c (n "ic-event-hub-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0y6bcc362d020rqjjdrfmgccacrg29hg4g6d0hg678graqflcywx")))

