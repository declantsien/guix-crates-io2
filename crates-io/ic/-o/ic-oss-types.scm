(define-module (crates-io ic -o ic-oss-types) #:use-module (crates-io))

(define-public crate-ic-oss-types-0.1.0 (c (n "ic-oss-types") (v "0.1.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "04160nk93gmizqr8cslkjhri08sv0gvgp0agm0xx5glsghkl26by")))

(define-public crate-ic-oss-types-0.1.1 (c (n "ic-oss-types") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "05q6xb52ri2mbdbz08s2x9zdyc732nsbx13r5vfjhba33pq61smk")))

(define-public crate-ic-oss-types-0.1.2 (c (n "ic-oss-types") (v "0.1.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0w1nx7c911j7ywbihigan6ky1llp5sy2ym74qn4l86bqxlsi6d10")))

(define-public crate-ic-oss-types-0.1.3 (c (n "ic-oss-types") (v "0.1.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "082v3vrsyfvzlc27wjm8drl56shfdvs65kb1k05zwqd952f5bnp6")))

