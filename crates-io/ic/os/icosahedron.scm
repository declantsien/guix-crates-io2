(define-module (crates-io ic os icosahedron) #:use-module (crates-io))

(define-public crate-icosahedron-0.1.0 (c (n "icosahedron") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0530z759p5wqrpvm59skhp8prfjfv7bra9x7x0ijd4k4bmr4s58f")))

(define-public crate-icosahedron-0.1.1 (c (n "icosahedron") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvx2kjaqglwj3fhllqfwyk48gybi5ahckdgrgyxl1f79l7qzgnk")))

