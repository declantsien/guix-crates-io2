(define-module (crates-io ic p- icp-vanity) #:use-module (crates-io))

(define-public crate-icp-vanity-0.1.0 (c (n "icp-vanity") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "ic-agent") (r "^0.4.0") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.11") (d #t) (k 0)))) (h "08cr1p9hiik1mjd7xjxddv1z1pkzkpyk6crwl1pv5ig0cjh09nlq")))

(define-public crate-icp-vanity-0.1.1 (c (n "icp-vanity") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "ic-agent") (r "^0.4.0") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.11") (d #t) (k 0)))) (h "06zbvlja9flrpj6vv2k18zzsgg0c7vr0rsrlgzcqn0ga184kkhcc")))

