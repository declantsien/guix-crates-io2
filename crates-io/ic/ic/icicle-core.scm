(define-module (crates-io ic ic icicle-core) #:use-module (crates-io))

(define-public crate-icicle-core-1.3.0 (c (n "icicle-core") (v "1.3.0") (d (list (d (n "ark-ec") (r "^0.4.0") (f (quote ("parallel"))) (o #t) (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ark-poly") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "icicle-cuda-runtime") (r "^1.3.0") (d #t) (k 0)))) (h "0v7x6gkfg7a7imvg9mk3kgd4vmqbb1phpppwkpysamnl76l2ihis") (f (quote (("g2") ("ec_ntt") ("default") ("arkworks" "ark-ff" "ark-ec" "ark-poly" "ark-std"))))))

