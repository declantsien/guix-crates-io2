(define-module (crates-io ic ic icicle-cuda-runtime) #:use-module (crates-io))

(define-public crate-icicle-cuda-runtime-1.3.0 (c (n "icicle-cuda-runtime") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1i5gh21zvhj15w3p47mpvmsng7rvfz37hh82i0nbwcsvqlihgljb") (r "1.70.0")))

