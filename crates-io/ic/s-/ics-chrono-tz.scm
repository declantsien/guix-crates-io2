(define-module (crates-io ic s- ics-chrono-tz) #:use-module (crates-io))

(define-public crate-ics-chrono-tz-0.1.0 (c (n "ics-chrono-tz") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono-tz") (r "=0.6.1") (d #t) (k 0)) (d (n "ics") (r "^0.5") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 1)))) (h "1yxdz6zqiq2hbclxzkbjb7yzbj9rk6na1d60vmpl07w0pkrxxhrc")))

(define-public crate-ics-chrono-tz-0.2.0 (c (n "ics-chrono-tz") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono-tz") (r "=0.8.6") (d #t) (k 0)) (d (n "ics") (r "^0.5") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 1)))) (h "0k1vh9vzj2g57sbz03sl7hq974mhz2j3m0mp4j6904mn8mfjhdlc")))

