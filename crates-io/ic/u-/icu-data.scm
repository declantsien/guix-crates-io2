(define-module (crates-io ic u- icu-data) #:use-module (crates-io))

(define-public crate-icu-data-0.1.0 (c (n "icu-data") (v "0.1.0") (d (list (d (n "brotli") (r "^3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_ascii_tree") (r "^0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "19lgkga9a88mjjh9iwlnzcdcqkgxjmcz3pdpw54c4k75rk050kb8")))

