(define-module (crates-io ic rc icrc1-test-env-state-machine) #:use-module (crates-io))

(define-public crate-icrc1-test-env-state-machine-0.1.0 (c (n "icrc1-test-env-state-machine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-test-state-machine-client") (r "^2.2.1") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.0") (d #t) (k 0)))) (h "1m5pk0qb1kwl477sssa9rsq7ml0vpdd8xc35vgr9vhxa5pas1cav") (r "1.31.0")))

(define-public crate-icrc1-test-env-state-machine-0.1.1 (c (n "icrc1-test-env-state-machine") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-test-state-machine-client") (r "^3.0.0") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.1") (d #t) (k 0)))) (h "1ac6d00l6gxza3wg90f1lyhwk3q0hw2gr1ih8pr46dnmc1pdmf8h") (r "1.31.0")))

(define-public crate-icrc1-test-env-state-machine-0.1.2 (c (n "icrc1-test-env-state-machine") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-test-state-machine-client") (r "^3.0.0") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.2") (d #t) (k 0)))) (h "0ja7pg3cyy5b9ds1pr83s514fgycji7djyaxq8xa67mpqgygbkgf") (r "1.31.0")))

