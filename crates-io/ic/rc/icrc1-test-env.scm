(define-module (crates-io ic rc icrc1-test-env) #:use-module (crates-io))

(define-public crate-icrc1-test-env-0.1.0 (c (n "icrc1-test-env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (d #t) (k 0)))) (h "064vdd4ha54sk322bcksdpb3fjgdmxm075r43sdi43ilz5jw0akl") (r "1.31.0")))

(define-public crate-icrc1-test-env-0.1.1 (c (n "icrc1-test-env") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h4fra80digmiis94shm9rmsl7aw3mq3ar0sqkp67gk97a8jiqhx") (r "1.31.0")))

(define-public crate-icrc1-test-env-0.1.11 (c (n "icrc1-test-env") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l1kb2csbv077p501i3hccbxz3fcjp63x56ja2pcfq51kn6i511q") (r "1.31.0")))

(define-public crate-icrc1-test-env-0.1.2 (c (n "icrc1-test-env") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "candid") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.184") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19a3qh94gbpmjicdb2v8zm306lvgl4dc5ghg3cl73w5kk99d708g") (r "1.31.0")))

