(define-module (crates-io ic rc icrc1-test-replica) #:use-module (crates-io))

(define-public crate-icrc1-test-replica-0.1.0 (c (n "icrc1-test-replica") (v "0.1.0") (d (list (d (n "ic-agent") (r "^0.24.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0j9px4ylic1zvvwx8m0sh6s8lir2ag72fs9qw0j8yhbpcn6gn80h") (r "1.31.0")))

(define-public crate-icrc1-test-replica-0.1.1 (c (n "icrc1-test-replica") (v "0.1.1") (d (list (d (n "ic-agent") (r "^0.25.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nllhjkwf2lnp0wp05pv65gxr1mqvqv3yvnw2807fa7j826aykyz") (r "1.31.0")))

(define-public crate-icrc1-test-replica-0.1.2 (c (n "icrc1-test-replica") (v "0.1.2") (d (list (d (n "ic-agent") (r "^0.31.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0faa4g6k4mqwci1vzni6gcdf9y5ns01wxrwfxfhwc0c8xkz0zhx9") (r "1.31.0")))

