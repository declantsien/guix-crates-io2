(define-module (crates-io ic rc icrc1-test-suite) #:use-module (crates-io))

(define-public crate-icrc1-test-suite-0.1.0 (c (n "icrc1-test-suite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "candid") (r "^0.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.0") (d #t) (k 0)))) (h "174avp80zx5jq2shgv70plkv6kv777n39610yal3zz64w0xrjp76") (r "1.31.0")))

(define-public crate-icrc1-test-suite-0.1.1 (c (n "icrc1-test-suite") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.1") (d #t) (k 0)))) (h "09xilk4qi0w6ccwapsgygn307jya5qla28jlsx3ckbyr8gyifzli") (r "1.31.0")))

(define-public crate-icrc1-test-suite-0.1.2 (c (n "icrc1-test-suite") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "candid") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "icrc1-test-env") (r "^0.1.2") (d #t) (k 0)))) (h "1df4fhj95j7q6c0gal37kbggl4kg20i4s0682r9d6h68wzb3gmmy") (r "1.31.0")))

