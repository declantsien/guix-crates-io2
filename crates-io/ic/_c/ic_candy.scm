(define-module (crates-io ic _c ic_candy) #:use-module (crates-io))

(define-public crate-ic_candy-0.1.0 (c (n "ic_candy") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1q9nnfh3b2fspxyzm7zc2pq547yi5z2brr9lxyqfmfzh9znm8n9k")))

