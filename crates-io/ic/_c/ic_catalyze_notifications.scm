(define-module (crates-io ic _c ic_catalyze_notifications) #:use-module (crates-io))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.0 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.0") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0p58y0ar868rixsvh27rl90ilqs1fk89py2xh3zckm549nh0kirm")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.1 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.1") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0g1r3rakrybhx6pldfybff71sci4bsiij0j9jag1fm2dc4mwshj8")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.2 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.2") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13aybf6vkmzxmhl5fg0gqcwhazgmk4zsc9my16majah2mhczf4ff")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.3 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.3") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18jx0f1cm4nb627i1a0w7fxd5pnvvncpkzhlj1kgsy1gq331zzzf")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.4 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.4") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04xw683a68qzaa29gv62c9jpqn0xmhnp6gp8whk8wq3s9vxg0yms")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.5 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.5") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "199mb2hl1k863fc8hc1mpgw49hxdhaqv9h8g419869pr2ninvpb1")))

(define-public crate-ic_catalyze_notifications-0.0.1-beta.6 (c (n "ic_catalyze_notifications") (v "0.0.1-beta.6") (d (list (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0allpma0ibj03vbkrpwii33bri24c2g90dgjwn1gz33isxlz996w")))

