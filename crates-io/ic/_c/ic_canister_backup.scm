(define-module (crates-io ic _c ic_canister_backup) #:use-module (crates-io))

(define-public crate-ic_canister_backup-0.0.1 (c (n "ic_canister_backup") (v "0.0.1") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11lp23jvxmqq02711ccqfkp7xirb2yhxnv6mg2pzfhwqxkh6q1zx")))

(define-public crate-ic_canister_backup-0.0.2 (c (n "ic_canister_backup") (v "0.0.2") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "08kvjj1nxb1qsrvnipgiyyg45rdj1snrj7slqhi6vl5l5f0r6286")))

(define-public crate-ic_canister_backup-0.0.3 (c (n "ic_canister_backup") (v "0.0.3") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0fqa8pp9wmq3ji2j4z7a9shdv697ragddz9f72nhwmvia932r683")))

(define-public crate-ic_canister_backup-0.0.4 (c (n "ic_canister_backup") (v "0.0.4") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1grdsdpcinywwx015mf8vli7c5v3zxp2ksn6qj4g9qipmxqgnzp5")))

(define-public crate-ic_canister_backup-0.0.5 (c (n "ic_canister_backup") (v "0.0.5") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "15452ps38kac87dm9v0qqaik2yiyjlcr5abl0idmvrq1gql7zyan")))

(define-public crate-ic_canister_backup-0.0.6 (c (n "ic_canister_backup") (v "0.0.6") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11p5wxchqp2ld473d08p26gs6j4bl3rdxj7cb57hacyp7gbkpk8k")))

(define-public crate-ic_canister_backup-0.0.7 (c (n "ic_canister_backup") (v "0.0.7") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "074ma8g6gp9irnhdx9c51y7mqn0vwxwg0k3pahkkyyqnn27ykr8x")))

(define-public crate-ic_canister_backup-0.0.8 (c (n "ic_canister_backup") (v "0.0.8") (d (list (d (n "ic-cdk") (r "^0.12.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1521p5s01bpxzrly5j2s9r4cd8azj9149xsqwn3br42aknhasc2c")))

(define-public crate-ic_canister_backup-0.0.9-beta.0 (c (n "ic_canister_backup") (v "0.0.9-beta.0") (d (list (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "06fidifx5qrlbr48ar5d2jrpsnqm5i741a418s4gfggiwplpc9gw")))

(define-public crate-ic_canister_backup-0.0.10 (c (n "ic_canister_backup") (v "0.0.10") (d (list (d (n "ic-cdk") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "14jfaqcxp5jsr6bl9kvri7wgi8hal8z13mgkzhcq7c7p9042lbik")))

