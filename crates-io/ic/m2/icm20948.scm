(define-module (crates-io ic m2 icm20948) #:use-module (crates-io))

(define-public crate-icm20948-0.0.1 (c (n "icm20948") (v "0.0.1") (d (list (d (n "bit-byte-structs") (r "^0.0.3") (f (quote ("cortex-m-debuging"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)))) (h "0ryrh809vj0ix8qk6n71m6xynl8ivi6m880aqmz1d1hvfy4i2w42") (f (quote (("cortex-m-debuging" "cortex-m-semihosting" "cortex-m"))))))

