(define-module (crates-io ic m2 icm20689_driver_rs) #:use-module (crates-io))

(define-public crate-icm20689_driver_rs-0.0.1 (c (n "icm20689_driver_rs") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0z96a3izdcsq6jmh0ibv3m090plb9r543vjirml7h5k62hd936yg")))

(define-public crate-icm20689_driver_rs-0.0.2 (c (n "icm20689_driver_rs") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0215v1pvrdn5qmwkhnqaphrlzqmy67knjmasdbxynibzns5b1hdy")))

