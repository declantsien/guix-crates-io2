(define-module (crates-io ic m2 icm20689) #:use-module (crates-io))

(define-public crate-icm20689-0.1.1 (c (n "icm20689") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "016pxfgdvmw2px9zrlxbfvqlyjryr5qf8rkdxj3i6n1cbaw5c6vq") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

