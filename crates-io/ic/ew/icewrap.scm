(define-module (crates-io ic ew icewrap) #:use-module (crates-io))

(define-public crate-icewrap-0.1.0 (c (n "icewrap") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)))) (h "033ibscgs719h5khl57zg8biq4rcvl46nkzfillx2cahyb7w747r") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-icewrap-0.1.1 (c (n "icewrap") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)))) (h "03xm1x47nz29yacjwj7mhqaf5v40isdg9gaxbjvfr3r1x78v7qp6") (f (quote (("std") ("default" "std"))))))

