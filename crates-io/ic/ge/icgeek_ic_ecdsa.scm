(define-module (crates-io ic ge icgeek_ic_ecdsa) #:use-module (crates-io))

(define-public crate-icgeek_ic_ecdsa-0.1.0 (c (n "icgeek_ic_ecdsa") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0bjqbwbidb81c40md41335y4rh9gajrk557fyq95dkihc64da9v3")))

