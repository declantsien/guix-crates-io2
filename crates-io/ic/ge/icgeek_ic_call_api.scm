(define-module (crates-io ic ge icgeek_ic_call_api) #:use-module (crates-io))

(define-public crate-icgeek_ic_call_api-0.1.0 (c (n "icgeek_ic_call_api") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "040jlv89fy0fiqgi8fgl585w7b5rrxm9b02fl7qzn1bg51x9miy6")))

(define-public crate-icgeek_ic_call_api-0.2.0 (c (n "icgeek_ic_call_api") (v "0.2.0") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "18khs7y1fyf3ry5wifhgki0ylnyshdnayidv95n8bz15rjrkms08")))

