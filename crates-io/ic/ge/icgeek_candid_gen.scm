(define-module (crates-io ic ge icgeek_candid_gen) #:use-module (crates-io))

(define-public crate-icgeek_candid_gen-0.1.0 (c (n "icgeek_candid_gen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1s3sw36m7qxkd5qxzaqcksim2648kac5wpxqfzhhw85cm58nq2q2")))

(define-public crate-icgeek_candid_gen-0.1.1 (c (n "icgeek_candid_gen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "12jnr5csx3g5ms49rfhszqsdj4hmkr8xws018ff8077qcf6h492k")))

(define-public crate-icgeek_candid_gen-0.1.2 (c (n "icgeek_candid_gen") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "071k10awxg413ii808pm2kxq5d6iyx5d83f59cq0bll0pv4nwndg")))

