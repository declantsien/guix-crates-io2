(define-module (crates-io ic ge icgeek_ic_certification) #:use-module (crates-io))

(define-public crate-icgeek_ic_certification-0.1.0 (c (n "icgeek_ic_certification") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-certification") (r "^0.23.0") (d #t) (k 0)) (d (n "ic-verify-bls-signature") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "09dnmv8w27xfmqidg4j3iza5pkd83950ncrqwkysnmn93wyj14q4")))

(define-public crate-icgeek_ic_certification-0.2.1 (c (n "icgeek_ic_certification") (v "0.2.1") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "ic-certification") (r "^0.25.0") (d #t) (k 0)) (d (n "ic-verify-bls-signature") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 0)))) (h "14y3ygdw00dlm9a6a4wskpsm43xd957a18kq3rr723dxfd775y0n")))

