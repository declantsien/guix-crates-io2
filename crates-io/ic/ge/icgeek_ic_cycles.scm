(define-module (crates-io ic ge icgeek_ic_cycles) #:use-module (crates-io))

(define-public crate-icgeek_ic_cycles-0.1.0 (c (n "icgeek_ic_cycles") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "0sapg4cf6bvxcs97jm9rl2si3xm4c67ga5nqrizd0ldq3f7vj4h8")))

(define-public crate-icgeek_ic_cycles-0.1.1 (c (n "icgeek_ic_cycles") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1i67yh5dlq563rixdd72yq7f7q8kmmf7vw2g4m15n39x0y4zrg2a")))

