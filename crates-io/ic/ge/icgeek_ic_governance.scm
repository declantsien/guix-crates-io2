(define-module (crates-io ic ge icgeek_ic_governance) #:use-module (crates-io))

(define-public crate-icgeek_ic_governance-0.1.0 (c (n "icgeek_ic_governance") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "13qiw4x6hd18w6a1b7hq88p4k6rhrwvhgjpx63zn65lrfpzwiyn7")))

(define-public crate-icgeek_ic_governance-0.1.1 (c (n "icgeek_ic_governance") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1m2hl9qy059iv6dkyff58jmvj968dfcy5vxyqw11l8avqccx04d8")))

(define-public crate-icgeek_ic_governance-0.1.2 (c (n "icgeek_ic_governance") (v "0.1.2") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1yh7wb8i6sqq5y1jf6fwg6gxmq0i1z3y3b45kgdd9bj11yzply85")))

(define-public crate-icgeek_ic_governance-0.2.0 (c (n "icgeek_ic_governance") (v "0.2.0") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1gbifb3fii5y424b4x97pgqqcpm5w6dfxqri9akr6yj9hgmjqh0c")))

(define-public crate-icgeek_ic_governance-0.2.1 (c (n "icgeek_ic_governance") (v "0.2.1") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1xqizhdiwx1mh5941ascrkvpiw3bv6dy28d0s4clyx448bvj09k7")))

(define-public crate-icgeek_ic_governance-0.2.2 (c (n "icgeek_ic_governance") (v "0.2.2") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1yi0hldris26ccq8hrnk9bapaw1a8fxkvyhayr91l904jkb3b2xv")))

(define-public crate-icgeek_ic_governance-0.2.3 (c (n "icgeek_ic_governance") (v "0.2.3") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "037r5cpr4h63b7ncc0948gbfg5k7xxwyfrpfpghprvhdwimf4y0j")))

(define-public crate-icgeek_ic_governance-0.2.4 (c (n "icgeek_ic_governance") (v "0.2.4") (d (list (d (n "candid") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1rk6isgs5jq2fqqlpyy77aa4abd2j888f6kk0n57b48fd6pgfyw6")))

