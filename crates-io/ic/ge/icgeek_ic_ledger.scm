(define-module (crates-io ic ge icgeek_ic_ledger) #:use-module (crates-io))

(define-public crate-icgeek_ic_ledger-0.1.0 (c (n "icgeek_ic_ledger") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0qmm4pf2ql8ccz054dl6dhnj99r7ddchgmwmgg13nrpwiv9rk7ik")))

