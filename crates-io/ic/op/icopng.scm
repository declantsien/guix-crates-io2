(define-module (crates-io ic op icopng) #:use-module (crates-io))

(define-public crate-icopng-1.0.1 (c (n "icopng") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "png") (r "^0.17.8") (d #t) (k 0)))) (h "1iqh306zx1n5vm1l04b1h1w904dbg9rdjwjc0b7hc3lh18v7w4jn")))

