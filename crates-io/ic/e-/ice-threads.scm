(define-module (crates-io ic e- ice-threads) #:use-module (crates-io))

(define-public crate-ice-threads-0.0.0 (c (n "ice-threads") (v "0.0.0") (h "1zd4qqlkrg3p7423iwqq743j04fmd9wgqdc5wjh7wgb22kxpvrnz")))

(define-public crate-ice-threads-0.1.0 (c (n "ice-threads") (v "0.1.0") (h "1jiqhn9h72h5dw6anamiqmsci0x2yijzw9lbiijh75dgjw2r81wy")))

