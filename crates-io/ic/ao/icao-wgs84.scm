(define-module (crates-io ic ao icao-wgs84) #:use-module (crates-io))

(define-public crate-icao-wgs84-0.1.0 (c (n "icao-wgs84") (v "0.1.0") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "icao-units") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unit-sphere") (r "^0.1") (d #t) (k 0)))) (h "1gnliay6q84b9akzzj81iralam98srw91vzzp66qz9b3ikxijn72")))

(define-public crate-icao-wgs84-0.1.1 (c (n "icao-wgs84") (v "0.1.1") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "icao-units") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unit-sphere") (r "^0.1") (d #t) (k 0)))) (h "0mzan2dg5wc6gqswd0n9dm4xssy0ip1a6n9j1rq3vcd5zrayw9n1")))

(define-public crate-icao-wgs84-0.1.2 (c (n "icao-wgs84") (v "0.1.2") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "icao-units") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unit-sphere") (r "^0.1") (d #t) (k 0)))) (h "0ik1cdvdcs3qspq2qpawfi347r56jn6zmxdp3b3kc86qmwkqa05q")))

