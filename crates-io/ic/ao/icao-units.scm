(define-module (crates-io ic ao icao-units) #:use-module (crates-io))

(define-public crate-icao-units-0.1.0 (c (n "icao-units") (v "0.1.0") (h "0fjd9wm80cznxb5mz1fby529xflmikasfv6355538n4x19dwn6lp")))

(define-public crate-icao-units-0.1.1 (c (n "icao-units") (v "0.1.1") (h "0a3rwlwywgxry9yi2zccp3lh2ylqs76qxaswfcm1xslyigysxs9d")))

(define-public crate-icao-units-0.1.2 (c (n "icao-units") (v "0.1.2") (h "1lr2l4aavrmadzc6agmv81r8gbrj4ngc1g8gy67kgbiyjl9r2hh3")))

(define-public crate-icao-units-0.1.3 (c (n "icao-units") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cpchvw44zda8l029bgj0lzhrqs5129525xcc9iksshzjkqxdyyx")))

