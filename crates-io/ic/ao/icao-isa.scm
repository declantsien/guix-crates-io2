(define-module (crates-io ic ao icao-isa) #:use-module (crates-io))

(define-public crate-icao-isa-0.1.0 (c (n "icao-isa") (v "0.1.0") (d (list (d (n "icao-units") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1j0p62hd3j0sk32m4d10iclfc59dv1i1j9q8x4yq05ncq0v9ngy4")))

(define-public crate-icao-isa-0.1.2 (c (n "icao-isa") (v "0.1.2") (d (list (d (n "icao-units") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "04ibfagqgwqmfxhfrzfdjwafwvkdhc0wdj3xdm2b5fh050vkpp9d")))

