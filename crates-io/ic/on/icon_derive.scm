(define-module (crates-io ic on icon_derive) #:use-module (crates-io))

(define-public crate-icon_derive-0.1.0 (c (n "icon_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rl6r8mmn98j0dcqpxq7l3479zf6ivj3cz7582b65yjb3nny8fw7")))

