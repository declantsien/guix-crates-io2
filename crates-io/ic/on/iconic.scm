(define-module (crates-io ic on iconic) #:use-module (crates-io))

(define-public crate-iconic-0.1.2 (c (n "iconic") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.6") (d #t) (k 0)) (d (n "photon-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "196j9dynrrl66kmmp2pms5cp8m1hh9l9ic4bz3f3bapjvrgmnl2z")))

