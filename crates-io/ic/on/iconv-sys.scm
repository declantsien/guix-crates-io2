(define-module (crates-io ic on iconv-sys) #:use-module (crates-io))

(define-public crate-iconv-sys-0.0.1 (c (n "iconv-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.124") (k 0)))) (h "0pgr4z42vsh67sc8bqmm1krw8g1jwndzkm3sljldx7j01zbxslqv") (r "1.59")))

(define-public crate-iconv-sys-0.0.2 (c (n "iconv-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.124") (k 0)))) (h "12lakkmf8airf71dy6wi3kr5sk1frjirfxspaxrd7vwx355kr9zm") (r "1.59")))

(define-public crate-iconv-sys-0.0.3 (c (n "iconv-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.124") (k 0)))) (h "11kv4ndhj045h8mgdr4vb76wbpxdx76ybj8v3r2awj1ykwgsqdv9") (r "1.59")))

(define-public crate-iconv-sys-0.0.4 (c (n "iconv-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.124") (k 0)))) (h "1mmjk0jp4hsr874xm5dg9drp3sqgbnsc8p5673fncv5p7r3i2i4z") (r "1.59")))

