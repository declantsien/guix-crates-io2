(define-module (crates-io ic on iconv-compat-win-sys) #:use-module (crates-io))

(define-public crate-iconv-compat-win-sys-0.1.1 (c (n "iconv-compat-win-sys") (v "0.1.1") (d (list (d (n "dyn_buf") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "0jymk58fbc2qbii5bzc971wd9rip88z3l901924clmn68largvmv")))

