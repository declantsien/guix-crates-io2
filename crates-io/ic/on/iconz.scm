(define-module (crates-io ic on iconz) #:use-module (crates-io))

(define-public crate-iconz-0.2.0 (c (n "iconz") (v "0.2.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0wh76gbj840cz9yy485ycdapf2lwrnzp6yzvz14ili2g8fbaqqrm") (y #t)))

(define-public crate-iconz-0.2.1 (c (n "iconz") (v "0.2.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "15zx5r6adi6csgn43hba2is95m2asq5jj7vyy75ywc60qyplsj0y") (y #t)))

(define-public crate-iconz-0.2.2 (c (n "iconz") (v "0.2.2") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1c4j86wkqcs64b5dj477q0yy50pigvcnvrbnr85b4c6wc31sa7zp") (y #t)))

