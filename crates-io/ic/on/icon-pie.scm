(define-module (crates-io ic on icon-pie) #:use-module (crates-io))

(define-public crate-icon-pie-0.1.0-beta (c (n "icon-pie") (v "0.1.0-beta") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^2.1.1") (d #t) (k 0)))) (h "1bs4gsrc6k4f44hs13ch7znaj6g23jzif5r1zg2jrp76n1ji39xz") (y #t)))

(define-public crate-icon-pie-0.1.1-beta (c (n "icon-pie") (v "0.1.1-beta") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^2.1.2") (d #t) (k 0)))) (h "196q059vw8x4l257dlv5vr7m814z1zgyfnbnlggh2nww7d0zmp3b") (y #t)))

(define-public crate-icon-pie-0.1.2-beta (c (n "icon-pie") (v "0.1.2-beta") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^2.1.2") (d #t) (k 0)))) (h "1fkdgasc5fci2nk2hpji3s6ivfvb097ss2vq09ax91hfiyk266jh") (y #t)))

(define-public crate-icon-pie-0.1.3-beta (c (n "icon-pie") (v "0.1.3-beta") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^2.1.2") (d #t) (k 0)))) (h "1d5bg6647sbn2a99dcksc6bs7f6r2wc6yan7gqyy50arfjqgx16x") (y #t)))

(define-public crate-icon-pie-0.1.4-beta (c (n "icon-pie") (v "0.1.4-beta") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^3.1.0") (d #t) (k 0)))) (h "10ql9ilqw52vlppzxkx190vygd6n14ffxy06mlinbqy8z4bn0xbd") (y #t)))

(define-public crate-icon-pie-0.1.0-alpha (c (n "icon-pie") (v "0.1.0-alpha") (d (list (d (n "crossterm") (r "^0.7.0") (d #t) (k 0)) (d (n "icon_baker") (r "^3.2.0-beta.7") (d #t) (k 0)))) (h "0w6kk5yjhdyzqg14dkv1g0wkxkfy8z05k6xrwx2s64zs0r8l7l9x") (y #t)))

