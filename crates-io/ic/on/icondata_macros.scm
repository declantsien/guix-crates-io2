(define-module (crates-io ic on icondata_macros) #:use-module (crates-io))

(define-public crate-icondata_macros-0.1.0 (c (n "icondata_macros") (v "0.1.0") (h "1pgsh432zsy2f1k2xrj2dgpcmc703mk2r5y6c5wi05c83gznmniy")))

(define-public crate-icondata_macros-0.1.1 (c (n "icondata_macros") (v "0.1.1") (h "0w723jyqjgnpraj3igqxyxjy9qaizd24n7v97dsahggb4skaq87q")))

(define-public crate-icondata_macros-0.1.2 (c (n "icondata_macros") (v "0.1.2") (h "0qmip7fmspn4m49v92bxv4b07qd94k2kg4lx7zsaj7zjl043ckkn")))

(define-public crate-icondata_macros-0.1.3 (c (n "icondata_macros") (v "0.1.3") (h "0ach0rbr6jzswdmdz32wra83mcnz7ay6ynlizf8crwvgvmxgiywg")))

(define-public crate-icondata_macros-0.1.4 (c (n "icondata_macros") (v "0.1.4") (h "15c3544l0lpgx3wmnmzn3myrmr7fn8bxcnb1ln4vpn2k8yhpycpc")))

