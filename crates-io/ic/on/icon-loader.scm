(define-module (crates-io ic on icon-loader) #:use-module (crates-io))

(define-public crate-icon-loader-0.1.0 (c (n "icon-loader") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0q77y7bcy2nlfnnsb2l2wcr029smwd2fk25cwg61mn8fhwiib3q4") (f (quote (("sync") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.2.0 (c (n "icon-loader") (v "0.2.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0gprh2g72irz7g4cqc2mng7g70m8xv74y22c78vsjjlcqikz3wv6") (f (quote (("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.2.1 (c (n "icon-loader") (v "0.2.1") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "1pzhxan0c0p7ms37lip4b6hzs1dcls7bkyblm8bnv3n9pz0ivrx0") (f (quote (("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.2.2 (c (n "icon-loader") (v "0.2.2") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0nimqi41qpxmnlcgc1sv5y1q9ysjg5aaicq1psm0imaf6cnln8nw") (f (quote (("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.0 (c (n "icon-loader") (v "0.3.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "1yz9a3b7vjzs95k04bd5vkc5shk48bvaiia5pqj1a9cmwr0lcbsh") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.1 (c (n "icon-loader") (v "0.3.1") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "14icz6yc9bn1ls8nhdh0lsxa2hqbry5n1dspadq5jhwgj0hbl2qn") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.2 (c (n "icon-loader") (v "0.3.2") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0lj5h2kjjz7gqacya6l5srcdswj9hbs2fs6776h4xmkkfnq36lii") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.3 (c (n "icon-loader") (v "0.3.3") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "1biqji5q64pck0hwsh887k09kzdb6d8n6qkllf0ywp3nl7b3xbjw") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.4 (c (n "icon-loader") (v "0.3.4") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "1w9q7y00avs9wri4b21cc9fnhlmxrhral98ajmja2bzfj0s6wa5k") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.5 (c (n "icon-loader") (v "0.3.5") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0bk1imrl051faws3h2zmnscsciw6ygszg3x6jw1im6iyvx25sjd7") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.3.6 (c (n "icon-loader") (v "0.3.6") (d (list (d (n "dashmap") (r "^5.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0bmyl5wgiddz28jfc01zn9g0z4m460c6z7q3yapjvqqcbyfw1a7l") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

(define-public crate-icon-loader-0.4.0 (c (n "icon-loader") (v "0.4.0") (d (list (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "1s7sbf0598m45gi299c2g5kzrdd8y10fkvnjsa1rkv0zbgqvckrp") (f (quote (("theme_error_log" "log") ("kde") ("gtk") ("default" "kde" "gtk"))))))

