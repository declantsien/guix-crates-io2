(define-module (crates-io ic on icon_transaction_serializer) #:use-module (crates-io))

(define-public crate-icon_transaction_serializer-0.1.0 (c (n "icon_transaction_serializer") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gvs92bf6g7rg7fww1dnrsppw45dqkc6ydldlha7vmw5q2l5z9jm")))

