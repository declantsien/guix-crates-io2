(define-module (crates-io ic on icondata_core) #:use-module (crates-io))

(define-public crate-icondata_core-0.0.1 (c (n "icondata_core") (v "0.0.1") (h "0qn6i5a72sxs11lp93g8cacasbsy2fc8l6r1dz85jn8dpawn903c")))

(define-public crate-icondata_core-0.0.2 (c (n "icondata_core") (v "0.0.2") (h "0d7lg6jprrri61nnnrwjjhrm5p535xxglkw5v6qqml6xsp0s8h0n")))

(define-public crate-icondata_core-0.1.0 (c (n "icondata_core") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nd8gy4slcsgkw1mak4vank3h0bigjj9aknq63kdbaqm8a9bx5vc") (s 2) (e (quote (("serde" "dep:serde"))))))

