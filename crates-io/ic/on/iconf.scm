(define-module (crates-io ic on iconf) #:use-module (crates-io))

(define-public crate-iconf-0.1.0 (c (n "iconf") (v "0.1.0") (d (list (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0mzrnnbdc2xzisfv7ybkg0na94qkkb5wcs212r23pjn6284c6mxq") (r "1.74")))

(define-public crate-iconf-0.1.1 (c (n "iconf") (v "0.1.1") (d (list (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "123xak39ix2gmw7avm2adxkci3jyaf081xqxiq0f3jvqrpcz4806") (r "1.74")))

(define-public crate-iconf-0.1.2 (c (n "iconf") (v "0.1.2") (d (list (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1wz2iswyssgb1gckk6drrakiy8j6p1fmcy628i7jcgdhmy33afjq") (r "1.74")))

