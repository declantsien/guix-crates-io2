(define-module (crates-io ic on icon_baker) #:use-module (crates-io))

(define-public crate-icon_baker-0.1.0 (c (n "icon_baker") (v "0.1.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "08rnmykv7v0x4cn4xswb0a5gpgzsmmdjbf5q5x60fqnqrsv9d7l4") (y #t)))

(define-public crate-icon_baker-0.1.1 (c (n "icon_baker") (v "0.1.1") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1n527y8by768hcdxnhkvcdz00plg03x46pnj6k8bf65rabxc52xc") (y #t)))

(define-public crate-icon_baker-0.1.2 (c (n "icon_baker") (v "0.1.2") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1ijndjmmx41ly1bv9qmzlr2rc8pbxr21iajj6w4izwi4cvr8n60j") (y #t)))

(define-public crate-icon_baker-0.1.3 (c (n "icon_baker") (v "0.1.3") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "15rvirp4lg0wn4wclvmqw2bzhz5fwvjsnjw7q28ynisq9i5ryca7") (y #t)))

(define-public crate-icon_baker-1.0.0-beta (c (n "icon_baker") (v "1.0.0-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1n68l9m5r4d60hs87wcdnhh1k87i5aqka41xgd2myn18wssmzw7h") (y #t)))

(define-public crate-icon_baker-1.0.0 (c (n "icon_baker") (v "1.0.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1fwx789cqx6031xvkfqvrv2vcq4m5730l429plmqc9cn4nps7kfs") (y #t)))

(define-public crate-icon_baker-1.0.1 (c (n "icon_baker") (v "1.0.1") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1al44i7dwwpjz6r1sm5x6w0a8rpza88rxz9dy42b2hgzb4p494hh") (y #t)))

(define-public crate-icon_baker-1.1.0 (c (n "icon_baker") (v "1.1.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1v7wp484ncb14g7pxyx38s7rxqnjhw27qw781cvxzdlcqzqwnkg9") (y #t)))

(define-public crate-icon_baker-1.2.0 (c (n "icon_baker") (v "1.2.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "037pwipfb24y8h53rd1k8lmfwypwmspabc6mbdby74wn1ll7dhx3") (y #t)))

(define-public crate-icon_baker-1.3.0-beta (c (n "icon_baker") (v "1.3.0-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1p6pfc6c1pgc9qd6lz1ismph6fal7pq2ipa6idkhqpcvn9pvdsw6") (y #t)))

(define-public crate-icon_baker-1.3.1-beta (c (n "icon_baker") (v "1.3.1-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0cqvpfg4vg3dy8dxhy7j1d5gajpcb788cdwm3i92g5qrahz7kz3k") (y #t)))

(define-public crate-icon_baker-2.0.0-beta (c (n "icon_baker") (v "2.0.0-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0wc8dqrimqv3b50abqm25gvshgaza7zsm6g45r9j6n45wv67ra0x") (y #t)))

(define-public crate-icon_baker-2.0.0 (c (n "icon_baker") (v "2.0.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "09wxvh6p6ncyjvw1afknv38qz0hzkm0zwqj3a8wlcclz8l3fv94x") (y #t)))

(define-public crate-icon_baker-2.1.0-beta (c (n "icon_baker") (v "2.1.0-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0jzn7ni5m8kr7zkq5b4n8h0mnqmc8lzlgg4yxdjyc2h7gb3xxw1f") (y #t)))

(define-public crate-icon_baker-2.1.0 (c (n "icon_baker") (v "2.1.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "16xhb87bhdfvn22ql3rv3wy36rmkpq956zasgz74kdka63zvg4f1") (y #t)))

(define-public crate-icon_baker-2.1.1 (c (n "icon_baker") (v "2.1.1") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0hk7qqlfmfz4ly9m4qjw6vli86915pfn0zhb5pfrxm375j32rf2z") (y #t)))

(define-public crate-icon_baker-2.1.2 (c (n "icon_baker") (v "2.1.2") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0i63v9kwrjdk7d7n11laqsmx9cs4w1x9gpv4ic9bprgxl1b7i235") (y #t)))

(define-public crate-icon_baker-2.2.0 (c (n "icon_baker") (v "2.2.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0szradxhj2cp632wdmn16cancqkwagg11wmja1kl7z1n1m1s589y") (y #t)))

(define-public crate-icon_baker-3.0.0 (c (n "icon_baker") (v "3.0.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1n76ibzbdfmjg1lj2am8nk852jn3i0s5x9b2381dvi4ffwxc3mnm") (y #t)))

(define-public crate-icon_baker-3.0.1 (c (n "icon_baker") (v "3.0.1") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1svmgkw5lx8s7g84nanxp0b81c1wc37rcm59mlm26ds17gc34am6") (y #t)))

(define-public crate-icon_baker-3.1.0 (c (n "icon_baker") (v "3.1.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0jq5xpz0nmygf2l6zhirlp8lx7ck6hvkl1lkw6fqx0qhp5r64qqq") (y #t)))

(define-public crate-icon_baker-3.2.0-beta (c (n "icon_baker") (v "3.2.0-beta") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1bm95clr15lkz3z6am4chj5d3n29qj4hr7zk6h4v964548y95bab") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.1 (c (n "icon_baker") (v "3.2.0-beta.1") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1vpgxp73cw0s7a5q7680pylwz5iik30mnnmmslyj8blsh12rr9wd") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.2 (c (n "icon_baker") (v "3.2.0-beta.2") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1n58jzmg45ib4140xpdhm6f0cd3xxnrkpr0qy2fnn0swnww15skr") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.3 (c (n "icon_baker") (v "3.2.0-beta.3") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "106pg1kgy8vs66vf1xaymbnviq3a0zbwn6igl0fdn9c3bpfj1h5v") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.4 (c (n "icon_baker") (v "3.2.0-beta.4") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "10zj7rd26aqrc46bzayn5jnlkf8s7pjwm2j1xgijw4qpnhf587v6") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.5 (c (n "icon_baker") (v "3.2.0-beta.5") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1yr7gva0kg4syidbjvnf27172x38km2dpnya6vrw1wzl6ng6my67") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.6 (c (n "icon_baker") (v "3.2.0-beta.6") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1vna8sjhv4rp3f6xg8vi5q2c6id5nppbx7jhk399iniyd48wqrnl") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.7 (c (n "icon_baker") (v "3.2.0-beta.7") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "04zjppvyllmbxnzmg0pwzj5sw1akiygw9f6zl63qv0xf6b44as9v") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.8 (c (n "icon_baker") (v "3.2.0-beta.8") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1614cy4w6nd280x7wa1x768hm3l5kgn4pxd54vn18q62lpknv1hj") (y #t)))

(define-public crate-icon_baker-3.2.0-beta.9 (c (n "icon_baker") (v "3.2.0-beta.9") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0mnylsgscck0z5f1h5vv98q3hqppf2xlmizz3g62prxbhd344xls") (y #t)))

(define-public crate-icon_baker-3.2.0 (c (n "icon_baker") (v "3.2.0") (d (list (d (n "icns") (r "^0.3.0") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0zn9cz9ykzhbw18vz0i39skyahlv8d0ay8vii9a2f5dk7wjz1265") (y #t)))

