(define-module (crates-io ic on iconv) #:use-module (crates-io))

(define-public crate-iconv-0.1.0 (c (n "iconv") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "0lgsd01qk184g5zdgz501vm1s5nhyrcdrxn6r7scp2hx9fi2z7ga")))

(define-public crate-iconv-0.1.1 (c (n "iconv") (v "0.1.1") (d (list (d (n "dyn_buf") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "1adip9hl3b2bc5jkc2wi3681yyjwjx8njnzpkqlyy8zq1pdsgrh7")))

