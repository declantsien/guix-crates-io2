(define-module (crates-io ic on iconwriter) #:use-module (crates-io))

(define-public crate-iconwriter-0.1.0 (c (n "iconwriter") (v "0.1.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "00bi5gs8m0a6bz5s8v02xc0phik6qrkm44p2nqm3f4n7813a703y") (y #t)))

(define-public crate-iconwriter-0.1.3 (c (n "iconwriter") (v "0.1.3") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "0dlr0cxavg40mjc2gxq1kzg57kdz6fn95qc8mfa7frhk4abbf6ld") (y #t)))

(define-public crate-iconwriter-0.1.4 (c (n "iconwriter") (v "0.1.4") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "1jz4441yw9icslqsvl0xc46ck1jgh9yzjbnkmaykclims8zaka9k")))

(define-public crate-iconwriter-0.1.5 (c (n "iconwriter") (v "0.1.5") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "1m5q4w6km7cv4nszjh5bvjznmmq02f01n9nks10s8mrrxlc6mffv")))

(define-public crate-iconwriter-0.1.6 (c (n "iconwriter") (v "0.1.6") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "190a22d7vf9l74sfhd3hzhjv9bqxfnf8k8z4dr4wzgd6kwxvw125") (y #t)))

(define-public crate-iconwriter-1.0.0 (c (n "iconwriter") (v "1.0.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "1j31hivl4p44b9wlycf2nv3hjs3z43pzs3y6m6d2njf4mlzz9q5a")))

(define-public crate-iconwriter-1.1.0 (c (n "iconwriter") (v "1.1.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "0dhl5rfxpkxxs4jc0x6zfkkycx51hcc48pqylmny8xhlkh9v04z2")))

(define-public crate-iconwriter-1.1.1 (c (n "iconwriter") (v "1.1.1") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.8") (d #t) (k 0)))) (h "10r5n6ybbkiyd8f6jck7yjkh8lpkjsji7v27h11lbxgbv40h961w")))

(define-public crate-iconwriter-1.2.0-beta (c (n "iconwriter") (v "1.2.0-beta") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "12n8m9h92qmbv2f0zfk215wrj7c3larr80a2czqhk104108fzjx8")))

(define-public crate-iconwriter-1.3.0-beta (c (n "iconwriter") (v "1.3.0-beta") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1gwj2i933achx9x36h5k4dvg7hpx1116wbh33w1z9q5i3vc2yzzv")))

(define-public crate-iconwriter-1.3.0 (c (n "iconwriter") (v "1.3.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1xvb30w7xhr5ns1p45xc2agj6n1vc6zc8g1sq52cdv5cksj1vf2r")))

(define-public crate-iconwriter-1.4.0 (c (n "iconwriter") (v "1.4.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "png_encode_mini") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1mr5xd65k5l9lk8bj7b87b9ilpfp8alcgiqzs3zz620k8j9lk9zh")))

(define-public crate-iconwriter-1.5.0 (c (n "iconwriter") (v "1.5.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "0mri3mm3n9nh2zry0b6wnyr92s0i77qwg2sdamy4dd9qdrxjrwa0")))

(define-public crate-iconwriter-1.6.1 (c (n "iconwriter") (v "1.6.1") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1hqgbzq5ll9mfzlnigaw9bdx27pzz7885nqmwh3xvgxzc9sg2chj")))

(define-public crate-iconwriter-1.7.0 (c (n "iconwriter") (v "1.7.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1ncli2f38zkwmq1xq8n31mk9jwzlm7iv1g4nr6g41ndp4r67mq9g")))

(define-public crate-iconwriter-1.7.0-beta (c (n "iconwriter") (v "1.7.0-beta") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "resvg") (r "^0.7.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "0bxcraj3rqhwq3p41ykm9j6a5kbnpali9b7vh86xsqnq9nh333cr")))

(define-public crate-iconwriter-2.0.0 (c (n "iconwriter") (v "2.0.0") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "096za93b9ni1r1k9iy0aw97mlsqjn1glxf4l42ylnixwfyhx0d86")))

(define-public crate-iconwriter-2.0.1 (c (n "iconwriter") (v "2.0.1") (d (list (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1zbj9v9x7qkyxdv8cz3g1jaa3r9zj4d983dba0q4mmx3prk82ljk")))

