(define-module (crates-io ic ec icecast-stats) #:use-module (crates-io))

(define-public crate-icecast-stats-0.1.0 (c (n "icecast-stats") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1apmvj4ym7107svhd7ykcq8rvips1lj7mi7kyifkka0ivndiix9g")))

(define-public crate-icecast-stats-0.1.1 (c (n "icecast-stats") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0lij7wmd9yyb10j92lwxgn1d7hjjasc9cgz709y8fi2a9va0xm53")))

(define-public crate-icecast-stats-0.1.2 (c (n "icecast-stats") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "17dmgk84g4q06sjvx58mfajcg4yqrnksxhsl21k432sj52wwkg3p")))

