(define-module (crates-io ic ec icecream) #:use-module (crates-io))

(define-public crate-icecream-0.0.1 (c (n "icecream") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "0xqmi39zfxrgw94nzq6gkff8ysx5qn50057dd3yd89jxyfw42i8v")))

(define-public crate-icecream-0.0.2 (c (n "icecream") (v "0.0.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "0ivqwjia28jnb8p2ya0h71d9dlzg721ghq670q2pkamm8848113z")))

(define-public crate-icecream-0.1.0 (c (n "icecream") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "gag") (r "^0.1.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0ganydvvvwvza37rqrqwknhmqa4nz6i55brlga8svd1hhv75zl9c")))

