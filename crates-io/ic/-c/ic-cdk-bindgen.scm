(define-module (crates-io ic -c ic-cdk-bindgen) #:use-module (crates-io))

(define-public crate-ic-cdk-bindgen-0.1.0 (c (n "ic-cdk-bindgen") (v "0.1.0") (d (list (d (n "candid") (r "^0.9") (f (quote ("parser"))) (d #t) (k 0)))) (h "114k3v3nc9hbh08q6rvvxb9b9cgdq7lpgji4p01q5a7nvsvvh4jl") (r "1.65.0")))

(define-public crate-ic-cdk-bindgen-0.1.1 (c (n "ic-cdk-bindgen") (v "0.1.1") (d (list (d (n "candid") (r "^0.9.6") (f (quote ("parser"))) (d #t) (k 0)))) (h "1z9fwxcx8y6lwnz1g7amzx2n51siq4xm44wrsv52nf43k8vx80qv") (r "1.65.0")))

(define-public crate-ic-cdk-bindgen-0.1.2 (c (n "ic-cdk-bindgen") (v "0.1.2") (d (list (d (n "candid_parser") (r "^0.1.0") (d #t) (k 0)))) (h "11d6pjnaxdvj6xq320rpy27hgnp8pa6c92nq483qznv3vf9ryj2m") (r "1.66.0")))

(define-public crate-ic-cdk-bindgen-0.1.3 (c (n "ic-cdk-bindgen") (v "0.1.3") (d (list (d (n "candid_parser") (r "^0.1.0") (d #t) (k 0)))) (h "1k8blq7al7lil3sjrrj5q9wjs81mdn9xn8skzdramggfrbybmnr8") (r "1.70.0")))

