(define-module (crates-io ic -c ic-cbor) #:use-module (crates-io))

(define-public crate-ic-cbor-1.1.0 (c (n "ic-cbor") (v "1.1.0") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-certification") (r "^0.26") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h4dyffdhw99d88yk8l4vwg88ng57qns9ycqza57saybi4if58wy")))

(define-public crate-ic-cbor-1.2.0 (c (n "ic-cbor") (v "1.2.0") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-certification") (r "^1.2.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l5xlmdp728d4wzyv3m2mp5dad9xx6fr0hqxmnnz40jc492f4zvn")))

(define-public crate-ic-cbor-1.3.0 (c (n "ic-cbor") (v "1.3.0") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-certification") (r "^1.3.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10qaz9cycq5slwfwgbkszn4jmd5mpg88qifmw05kfbqi9wz3wqqh")))

(define-public crate-ic-cbor-2.0.0 (c (n "ic-cbor") (v "2.0.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-certification") (r "^2.0.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03x5axcz4kih67i5lbrklsqbyw40a59iq5q6gfys5fnp2w3h7h87")))

(define-public crate-ic-cbor-2.0.1 (c (n "ic-cbor") (v "2.0.1") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-certification") (r "^2.0.1") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qkq62vawbhrfssgs0bciyi94v8fhsd7b3lgx76ylpf42b7gnp73")))

(define-public crate-ic-cbor-2.1.0 (c (n "ic-cbor") (v "2.1.0") (d (list (d (n "candid") (r "^0.9") (d #t) (k 0)) (d (n "ic-certification") (r "^2.1.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10fri3mclb4cam6qwd3jgsgp9nx6pi5kj1ddw2ibcwywiqh2y7mp")))

(define-public crate-ic-cbor-2.2.0 (c (n "ic-cbor") (v "2.2.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-certification") (r "^2.2.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0psv3a2hxvrbc481cw8j92x930gr219vn0cqxm3gkhs86kca6hl0")))

(define-public crate-ic-cbor-2.3.0 (c (n "ic-cbor") (v "2.3.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-certification") (r "^2.3.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05vzs73i2iigybl587qjjj1ar20rd4fzjhxdxdzgq528x7n1cy82")))

(define-public crate-ic-cbor-2.4.0 (c (n "ic-cbor") (v "2.4.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-certification") (r "^2.4.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "176r07zlh58wnnxvq7xjrp0d024rnj1xs89phiyri9d267v307lf")))

(define-public crate-ic-cbor-2.5.0 (c (n "ic-cbor") (v "2.5.0") (d (list (d (n "candid") (r "^0.10") (d #t) (k 0)) (d (n "ic-certification") (r "^2.5.0") (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fhyrbpwmqc72wcrnfn9aizm0kg4f9ip969iivc4pz5qf1q93g07")))

