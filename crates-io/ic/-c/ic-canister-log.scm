(define-module (crates-io ic -c ic-canister-log) #:use-module (crates-io))

(define-public crate-ic-canister-log-0.1.0 (c (n "ic-canister-log") (v "0.1.0") (h "1vm1hz5mink57k6z454qb89922mia3gmr2x0w1rw02c2235gqc6c")))

(define-public crate-ic-canister-log-0.2.0 (c (n "ic-canister-log") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "06ldlkp7hqafl6gdvvp3zwq77v16jr4g0np65x2nxzzc2zvc90nb")))

