(define-module (crates-io ic -c ic-cdk-optimizer) #:use-module (crates-io))

(define-public crate-ic-cdk-optimizer-0.1.0 (c (n "ic-cdk-optimizer") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0c74l6sy9xx73h7pm5rayvrkxgp1p7izzngdhvdg6w32r8cqx2v8")))

(define-public crate-ic-cdk-optimizer-0.2.0 (c (n "ic-cdk-optimizer") (v "0.2.0") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0jmf56bpb415j3xrkzp47agrd4mdpqq82cphp80wgam4zzk5ndrf")))

(define-public crate-ic-cdk-optimizer-0.3.0 (c (n "ic-cdk-optimizer") (v "0.3.0") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0l9nh2glqwxf2dc64y3bb7piavw5kc97qh6dcmxscymrj5n0w1q3")))

(define-public crate-ic-cdk-optimizer-0.3.1 (c (n "ic-cdk-optimizer") (v "0.3.1") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0zvnsdkfk2vx9nswv85s48n7ydsp6plm4r7jk7cf5bdag7hjjx9c")))

(define-public crate-ic-cdk-optimizer-0.3.2 (c (n "ic-cdk-optimizer") (v "0.3.2") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "02ai4cwmg68i83xlvjxm269il8n28carnrxd9f9fss5al565ar53")))

(define-public crate-ic-cdk-optimizer-0.3.3 (c (n "ic-cdk-optimizer") (v "0.3.3") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "1j5aj8j6f0ixacss50c08n19d1svjn0fg98hi4hlsndh4ppmpjrw")))

(define-public crate-ic-cdk-optimizer-0.3.4 (c (n "ic-cdk-optimizer") (v "0.3.4") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0rrkijjz18g7ihishz9521f3b0kkd7bn2vv7smck76x6kki6m3xs")))

(define-public crate-ic-cdk-optimizer-0.3.5 (c (n "ic-cdk-optimizer") (v "0.3.5") (d (list (d (n "binaryen") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 0)))) (h "0pp00xpwwx19addvmlqwg28y3zlcqqkhmhk45n25b0w79l3agj6r") (r "1.60.0")))

