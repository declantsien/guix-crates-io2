(define-module (crates-io ic -c ic-captcha) #:use-module (crates-io))

(define-public crate-ic-captcha-0.1.0 (c (n "ic-captcha") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg"))) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1jygm8id4ayyi56wymnaykj27hrmr0vd2jm1c1i24pbzzxyas34l")))

(define-public crate-ic-captcha-1.0.0 (c (n "ic-captcha") (v "1.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg"))) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "103gz5827fwxag5cdzvhvx5183prvx48fnmgcfkg5a3wx0s4h5ib")))

