(define-module (crates-io ic -c ic-core-module) #:use-module (crates-io))

(define-public crate-ic-core-module-1.0.0 (c (n "ic-core-module") (v "1.0.0") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("mysql" "chrono"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "intel-cache-lib") (r "^2.0.0") (d #t) (k 0)) (d (n "public-ip") (r "^0.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "160fdhgyv8xxbly81d54jlb0zg7hgkws66s1ws0gdq4lxia6ml8w")))

