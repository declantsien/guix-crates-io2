(define-module (crates-io ic -c ic-canister-serve) #:use-module (crates-io))

(define-public crate-ic-canister-serve-0.1.0 (c (n "ic-canister-serve") (v "0.1.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "ic-canister-log") (r "^0.1.0") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-metrics-encoder") (r "^1.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1g8zgxij1ji67fmbnxnq2nv4xhsiilr88515171jjwsnii881q3l")))

