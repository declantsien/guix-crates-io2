(define-module (crates-io ic -c ic-cdk-timers) #:use-module (crates-io))

(define-public crate-ic-cdk-timers-0.1.0 (c (n "ic-cdk-timers") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "055vpdz32kwxz7w9l1s94wpfzs23jcdk1saz7nys6nsgiw2gh064") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.2.0-beta.0 (c (n "ic-cdk-timers") (v "0.2.0-beta.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.8.0-beta.0") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1cs9395x6ipgkh5nmzjxkclx9hl56mxm07kcmrpyffnhykc21vwb") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.1.1 (c (n "ic-cdk-timers") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0b7mm4vg2zh518c4gka8dxs5n47w3lnjmv92ml0s72dzism8m8bp") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.1.2 (c (n "ic-cdk-timers") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1m4vh70djlwmnaqjygljccppi58ikf2w9dn62m7xyrnbjb2yfff7") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.2.0 (c (n "ic-cdk-timers") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.8") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1bqwm9ls826fzapf6c17xzxhdld5cl8ksdw7yi1cmgc4hxa3sqh6") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.3.0 (c (n "ic-cdk-timers") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.9") (d #t) (k 0)) (d (n "ic0") (r "^0.18.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0ylqqjhhnpq1qwyph7ibykr8k1il1b0wxfjn87iqlc276gck7bl3") (r "1.60.0")))

(define-public crate-ic-cdk-timers-0.4.0 (c (n "ic-cdk-timers") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "ic0") (r "^0.18.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "1q7malfjwxrcxlk10kjsinry1xr8zpkfmp51pqzr0sg0v7j5b3hr") (r "1.65.0")))

(define-public crate-ic-cdk-timers-0.5.0 (c (n "ic-cdk-timers") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11") (d #t) (k 0)) (d (n "ic0") (r "^0.18.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "16ggxbjwirpwal60ngvyrmsmamdpmkfm2mj7vn0i3r4vjihcj5i2") (r "1.65.0")))

(define-public crate-ic-cdk-timers-0.5.1 (c (n "ic-cdk-timers") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.11.2") (d #t) (k 0)) (d (n "ic0") (r "^0.21.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "0mkk4ms51677namrisyapfqfhsa7c49x4sldx3m2qh9s7lj65yln") (r "1.66.0")))

(define-public crate-ic-cdk-timers-0.6.0 (c (n "ic-cdk-timers") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.0") (d #t) (k 0)) (d (n "ic0") (r "^0.21.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "0aa36r0p91apy830z7r3d2857nbbs40li8cj846d2fpgdxqbjhwc") (r "1.66.0")))

(define-public crate-ic-cdk-timers-0.7.0 (c (n "ic-cdk-timers") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.13.0") (d #t) (k 0)) (d (n "ic0") (r "^0.21.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "1yzp2s571sjnykg1gx6xdbgp1zwh8baig61ljs5m51n4l6ijfiq5") (r "1.70.0")))

(define-public crate-ic-cdk-timers-0.8.0 (c (n "ic-cdk-timers") (v "0.8.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.14.0") (d #t) (k 0)) (d (n "ic0") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "1vj47zaqsini1s9vzvc0lhdch8krwsc1h1wfq67zaz74d7k44jmw") (r "1.75.0")))

