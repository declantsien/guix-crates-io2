(define-module (crates-io ic -c ic-cron) #:use-module (crates-io))

(define-public crate-ic-cron-0.1.0 (c (n "ic-cron") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.0") (d #t) (k 0)))) (h "0qc1f36nfwr7grndlicnl3kd4cdjksx61zvc41zjbn86yvbdccgn")))

(define-public crate-ic-cron-0.1.1 (c (n "ic-cron") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.0") (d #t) (k 0)))) (h "14n9cpz6pprdrhmz8p0iq4rb7g0zjm61zy8idkaw3nica67a7jq3")))

(define-public crate-ic-cron-0.1.2 (c (n "ic-cron") (v "0.1.2") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1kraacxcq7l3pgiznxy9mv67f99xymk1spsgz1cq0rgv9wd24wpd")))

(define-public crate-ic-cron-0.1.3 (c (n "ic-cron") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1qf4ymq7jxvkbh5lrlx0523qnwaz4h1kpqjfbpdwm64mncvycfb6")))

(define-public crate-ic-cron-0.1.4 (c (n "ic-cron") (v "0.1.4") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "1mcxrc0bbphjnapcrxb3rccklznl6lqmxgpdla06mpdsw6k1qgvs")))

(define-public crate-ic-cron-0.1.5 (c (n "ic-cron") (v "0.1.5") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "19vkysa4hm1dydb7lvkaywfhv4j3i4npp450cw9z1lh0dq66h7pi")))

(define-public crate-ic-cron-0.2.0 (c (n "ic-cron") (v "0.2.0") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "1f049h5r5w2jz282kmpr36xqhr0f4s8c3a6h558lfrsvbsilv1gr")))

(define-public crate-ic-cron-0.2.1 (c (n "ic-cron") (v "0.2.1") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "1xgk3x0iwc1f72y8k2206f300gd85dgh1x8hbhqlwnm2g0jcz0ca")))

(define-public crate-ic-cron-0.2.2 (c (n "ic-cron") (v "0.2.2") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "1mj504kkz35ad66jmksnsags6k65z15zr5qsf5368x7bax4kfqqs")))

(define-public crate-ic-cron-0.2.3 (c (n "ic-cron") (v "0.2.3") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "17f1lbmvx2znszlpdpb31rrb4ygm4bdlngzp17r0ypfhjh22jv8v")))

(define-public crate-ic-cron-0.2.4 (c (n "ic-cron") (v "0.2.4") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.1") (d #t) (k 0)))) (h "1a2p6xhd31p7imym5glk9vkg84k6rljdxpkq4x3rjx6ls6vv0k73")))

(define-public crate-ic-cron-0.2.5 (c (n "ic-cron") (v "0.2.5") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.2") (d #t) (k 0)))) (h "13r55297wf4zrlpccvw87q8x7hc80mgvxpsvpickd1ji02l8ykvw")))

(define-public crate-ic-cron-0.2.6 (c (n "ic-cron") (v "0.2.6") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "1m05rsjwc6y7w7sgaxqrbizh9mz0w8wsva7r546jmg4daaxzvhim")))

(define-public crate-ic-cron-0.2.7 (c (n "ic-cron") (v "0.2.7") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "12r0l04af8s4pbj2mjamp9gscpfgacvwhhip565yrxm3h1vfmax2")))

(define-public crate-ic-cron-0.2.8 (c (n "ic-cron") (v "0.2.8") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.3") (d #t) (k 0)))) (h "035ldg1lynp4fymsfvf8ikpz4936bbj5pw809idfdjyp9cmsphac")))

(define-public crate-ic-cron-0.4.0 (c (n "ic-cron") (v "0.4.0") (d (list (d (n "ic-cdk") (r "^0.3.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "union-utils") (r "^0.1.4") (d #t) (k 0)))) (h "14bgr14xdxqvjk1n4hbky0r76xk5acvxipl2v41y71fif3y4m81f")))

(define-public crate-ic-cron-0.4.1 (c (n "ic-cron") (v "0.4.1") (d (list (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "union-utils") (r "^0.1") (d #t) (k 0)))) (h "19h11rxy1x6ibnsl7ljn77c0nfvbck35q6pamn4rl8ajk26hrgql")))

(define-public crate-ic-cron-0.5.0 (c (n "ic-cron") (v "0.5.0") (d (list (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1y3y666hdpiljiy6pv70shjs2095pfhsdxf98rllwbm5lnd0kqkp") (y #t)))

(define-public crate-ic-cron-0.5.1 (c (n "ic-cron") (v "0.5.1") (d (list (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hjhzg2qldxijvrkdx47b9yiyjlia3mnp2hq75iryvc65pzwg1fb")))

(define-public crate-ic-cron-0.6.0 (c (n "ic-cron") (v "0.6.0") (d (list (d (n "candid") (r "^0.7.11") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "08agcdkzhqsikb9jcsw116z33aabwx074gg70hcizb8nxacfcbj4")))

(define-public crate-ic-cron-0.6.1 (c (n "ic-cron") (v "0.6.1") (d (list (d (n "candid") (r "^0.7.11") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0sqy37mibb3fg5wridbj5ar06237nfkrfzksz8zdnpcz1kdk0wi1")))

(define-public crate-ic-cron-0.7.0 (c (n "ic-cron") (v "0.7.0") (d (list (d (n "candid") (r "^0.7.13") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.4.0") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zirq4xyml7zdkzhyqnqxa18inkpyyx7ni4xp25mqswq7riqwlng")))

(define-public crate-ic-cron-0.7.1 (c (n "ic-cron") (v "0.7.1") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.6") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0kjl1d7g9k7yxr6ck1i1cg5yxsm7f1brfs3k5azqpa4j3zh1ziid")))

