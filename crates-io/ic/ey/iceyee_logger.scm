(define-module (crates-io ic ey iceyee_logger) #:use-module (crates-io))

(define-public crate-iceyee_logger-1.0.0 (c (n "iceyee_logger") (v "1.0.0") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1.0.0") (d #t) (k 0)) (d (n "iceyee_timer") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "=1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zbipk50xn2mrxfz54wj0sfw5x781qr9gwvsmnxqcfmhbrdkn74d")))

(define-public crate-iceyee_logger-1.0.1 (c (n "iceyee_logger") (v "1.0.1") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wa79vyfnxrj2bh612dr11sdfwzi0y2zlbz9icqywn7zy9b5fbn3")))

(define-public crate-iceyee_logger-1.0.2 (c (n "iceyee_logger") (v "1.0.2") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n197v35rn4b3ymnkpmbn1ysgwc49fw323akmsnf91s5am1jh4vq")))

(define-public crate-iceyee_logger-1.0.3 (c (n "iceyee_logger") (v "1.0.3") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vw37npjqld9l9vmpwdxdv8qrxxq6ckavbykhs28y1zdghh9w49y")))

(define-public crate-iceyee_logger-2.0.0 (c (n "iceyee_logger") (v "2.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ahi5mh2cswfjdm17gv4pfkc7g4n6qddy7ljfcidrjjd11j03p7p")))

(define-public crate-iceyee_logger-2.0.1 (c (n "iceyee_logger") (v "2.0.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14yd283nbcdv91w83pl3sd62v0ghpg12piwxjav2xckdxn1v8sms")))

(define-public crate-iceyee_logger-2.1.0 (c (n "iceyee_logger") (v "2.1.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "iceyee_timer") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1skzmsl9vm11yvq0ads82r33cfsqna8hxzs1lp45111r8qza0zlr")))

(define-public crate-iceyee_logger-3.0.0 (c (n "iceyee_logger") (v "3.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^3") (d #t) (k 0)) (d (n "iceyee_timer") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bs3sgl983gl8p9pnq8r2gv89dd7l5pc3cbjydjkvqrhfnsq79da")))

(define-public crate-iceyee_logger-3.0.1 (c (n "iceyee_logger") (v "3.0.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^3") (d #t) (k 0)) (d (n "iceyee_timer") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wgxwc2ms0ibqigbbm4x4hj6mzrzshkpr8010rbyphxgjkq4nd7y")))

(define-public crate-iceyee_logger-3.0.2 (c (n "iceyee_logger") (v "3.0.2") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^3") (d #t) (k 0)) (d (n "iceyee_timer") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdlpbn6vpf5b9c5jsgm1jlps23ccjaa1im9jvwmpx010xpxx62h")))

(define-public crate-iceyee_logger-3.0.3 (c (n "iceyee_logger") (v "3.0.3") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_datetime") (r "^3") (d #t) (k 0)) (d (n "iceyee_timer") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mn5aadx6p9hc3xdl5mzmqggyxwb9kzrb8ircfxmaci9j490aybz")))

(define-public crate-iceyee_logger-4.0.0 (c (n "iceyee_logger") (v "4.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_time") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bkv0wy9yk1mf79jrp41lhv8hziplpv4x0kdc5wrrc9kqwc5w6hg")))

(define-public crate-iceyee_logger-4.0.1 (c (n "iceyee_logger") (v "4.0.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_time") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vn1dbm8mcxbgcp8nbvkgwmcwic8blhh3cckgn917q1ml01132i9")))

(define-public crate-iceyee_logger-4.1.0 (c (n "iceyee_logger") (v "4.1.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_time") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnp3pbqwr611zk1virig6mpz1j5fhjblfzagwvrqlxbhzjf9p0p")))

(define-public crate-iceyee_logger-4.2.0 (c (n "iceyee_logger") (v "4.2.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "iceyee_time") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r75mdwj9hjj5kzdgq5iqdx5rsxkahm8acajm67i6sc1bw22siwp")))

(define-public crate-iceyee_logger-5.0.0 (c (n "iceyee_logger") (v "5.0.0") (d (list (d (n "iceyee_time") (r "^4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qgwsz3laqgw2rvk5j37vd0ih0zl94an0yii3sq7awy44sa71myc")))

(define-public crate-iceyee_logger-6.0.0 (c (n "iceyee_logger") (v "6.0.0") (d (list (d (n "iceyee_time") (r "^6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p1dw4kbq81xa5nfpsxqga57k837jbdnkqnmpd02yjpcw8ssh4r0")))

