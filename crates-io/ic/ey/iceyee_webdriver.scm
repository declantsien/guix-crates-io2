(define-module (crates-io ic ey iceyee_webdriver) #:use-module (crates-io))

(define-public crate-iceyee_webdriver-1.0.0 (c (n "iceyee_webdriver") (v "1.0.0") (d (list (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "iceyee_time") (r "^5") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n2d4fl9gl12hj33s5spn1f4fqq8kpx41ikj2rzx3b0l3jy4v2n3")))

(define-public crate-iceyee_webdriver-1.0.1 (c (n "iceyee_webdriver") (v "1.0.1") (d (list (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "iceyee_logger") (r "^5") (d #t) (k 0)) (d (n "iceyee_time") (r "^5") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zq2h2fg68bvhvv1wx9bz8ax9xzvvp2abm0mcj9x52bsrmpqhny5")))

(define-public crate-iceyee_webdriver-1.0.2 (c (n "iceyee_webdriver") (v "1.0.2") (d (list (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "iceyee_logger") (r "^6") (d #t) (k 0)) (d (n "iceyee_time") (r "^6") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zfz2cvin56kmivid7sxi6nynns3jb4jry3scavhs90gqlkpdqhi")))

(define-public crate-iceyee_webdriver-2.0.0 (c (n "iceyee_webdriver") (v "2.0.0") (d (list (d (n "cookie") (r "^0.16") (d #t) (k 0)) (d (n "iceyee_logger") (r "^6") (d #t) (k 0)) (d (n "iceyee_random") (r "^6") (d #t) (k 0)) (d (n "iceyee_time") (r "^6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05rk6376k5nsh8hqahkmjilwri7fi4dw4xf0bahvjyby4331ky00")))

