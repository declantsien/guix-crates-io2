(define-module (crates-io ic ey iceyee_time) #:use-module (crates-io))

(define-public crate-iceyee_time-4.0.0 (c (n "iceyee_time") (v "4.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ipm3gikiqgkd85dgd25x6y1wbsan8y3a9gs2kdb44nii8h065dg")))

(define-public crate-iceyee_time-4.0.1 (c (n "iceyee_time") (v "4.0.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvlrn61fq6zq5jqd2h5frs02mp6gn1d8b7b9p6xxj864q25zmjf")))

(define-public crate-iceyee_time-5.0.0 (c (n "iceyee_time") (v "5.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j5dyj6lqi0rxi4smz3lmq91hpnhz2zfiay7dxpx4zidjb1pf4w9")))

(define-public crate-iceyee_time-5.0.1 (c (n "iceyee_time") (v "5.0.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k06664agpq60v7x16rlsv6yh5mcp6hydqjxix1rvrsdh3ddshld")))

(define-public crate-iceyee_time-6.0.0 (c (n "iceyee_time") (v "6.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00valpnck4nc4xpn95l94n6ldlfcr272qxz797wjfmrik8j66r5f")))

