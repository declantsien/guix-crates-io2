(define-module (crates-io ic ey iceyee_datetime) #:use-module (crates-io))

(define-public crate-iceyee_datetime-1.0.0 (c (n "iceyee_datetime") (v "1.0.0") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "0fxqmcw0qrbvsvwjdshc6n978dd1pl06nb8lahmjqpxrrfv02nng")))

(define-public crate-iceyee_datetime-1.0.1 (c (n "iceyee_datetime") (v "1.0.1") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "0d11kf8qm8y39dbl8041cw4wwhwcnrds8xls9haqac2rqfxprh67")))

(define-public crate-iceyee_datetime-1.0.2 (c (n "iceyee_datetime") (v "1.0.2") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "0a0m1labv0bqhapf4hgsz6i97j3is0z84pdccmq9vig7civk2k4f")))

(define-public crate-iceyee_datetime-1.0.3 (c (n "iceyee_datetime") (v "1.0.3") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "0159lcrr01qa62ac148qbzh3dblw6cw1djy4aqyw2s63ira9w7xf")))

(define-public crate-iceyee_datetime-1.0.4 (c (n "iceyee_datetime") (v "1.0.4") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "1v0lnrccp46jpyphl5kgkvq80fa1h5i35nc3k9n204akikv4ig25")))

(define-public crate-iceyee_datetime-1.0.5 (c (n "iceyee_datetime") (v "1.0.5") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "139k9x4gl4i3nyrf69w2hf17gmasdppd635l171b9jp4sjxy785z")))

(define-public crate-iceyee_datetime-1.0.6 (c (n "iceyee_datetime") (v "1.0.6") (d (list (d (n "ctor") (r "=0.1.26") (d #t) (k 0)))) (h "1x5zkqrzmrnd754vc8w4pbplgi6hy36ggz9izvmifz7sxmi9gmcp")))

(define-public crate-iceyee_datetime-1.1.0 (c (n "iceyee_datetime") (v "1.1.0") (d (list (d (n "ctor") (r "=0.2.5") (d #t) (k 0)))) (h "15xjyj3lck5a3iw382kj6v7gjrx1hd747fh168fqh9b17ixwn6cj")))

(define-public crate-iceyee_datetime-2.0.0 (c (n "iceyee_datetime") (v "2.0.0") (d (list (d (n "ctor") (r "=0.2.5") (d #t) (k 0)))) (h "06ikhl04x0yj79pnkjg3kgyx0a07scii77bfwga5n8hn4i36vsbd")))

(define-public crate-iceyee_datetime-3.0.0 (c (n "iceyee_datetime") (v "3.0.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18fqrz7pza7ii1ghj73wab2k989z5vfkh2zjlkgabffcs4fa7cb1")))

