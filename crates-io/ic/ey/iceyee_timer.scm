(define-module (crates-io ic ey iceyee_timer) #:use-module (crates-io))

(define-public crate-iceyee_timer-1.0.0 (c (n "iceyee_timer") (v "1.0.0") (d (list (d (n "iceyee_datetime") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g2klib6h5zlabff5x6j25vh2pjgsp6nwmd3jm5371wphkrnlmr6")))

(define-public crate-iceyee_timer-1.0.1 (c (n "iceyee_timer") (v "1.0.1") (d (list (d (n "iceyee_datetime") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16kv3spm2li7b2701zh18ww0s7sn92mrk0mdx9r9jhg13m9cn5sp")))

(define-public crate-iceyee_timer-1.0.3 (c (n "iceyee_timer") (v "1.0.3") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02974k8axsjsi0a2v7j0pglc4scv0j1mb6k08dfv8r139vzrvjxv")))

(define-public crate-iceyee_timer-1.0.4 (c (n "iceyee_timer") (v "1.0.4") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q5ya7b89m2h2i3kpylj9dnpgmqiykxai42vv99da7a2r800zq2i")))

(define-public crate-iceyee_timer-2.0.0 (c (n "iceyee_timer") (v "2.0.0") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06n3mjyl3j9d7rg9gllxh23rd47cpzhaw9s8gy7fjv4r6fsynidl")))

(define-public crate-iceyee_timer-2.0.1 (c (n "iceyee_timer") (v "2.0.1") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zqigbzw317dxafl3zvyns5vdhq9gk16a7z57hmmv4y2gy1y0999")))

(define-public crate-iceyee_timer-3.0.0 (c (n "iceyee_timer") (v "3.0.0") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cpf9dvhjqyxw97ggyiqp0sa07i1i5bfb6lypi1vw0jrvsgjfb8r")))

(define-public crate-iceyee_timer-3.0.1 (c (n "iceyee_timer") (v "3.0.1") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lwa0zxnby5wgrw85p7hyifr7r86329lqbfz6yl4mb6bpap63i49")))

(define-public crate-iceyee_timer-3.0.2 (c (n "iceyee_timer") (v "3.0.2") (d (list (d (n "iceyee_datetime") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ivhbxw1pnx1f1cbjj2qz1635zavh8bdwi2bjg5w6xn77v7lxxik")))

