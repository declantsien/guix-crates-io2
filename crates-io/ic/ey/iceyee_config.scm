(define-module (crates-io ic ey iceyee_config) #:use-module (crates-io))

(define-public crate-iceyee_config-1.0.0 (c (n "iceyee_config") (v "1.0.0") (d (list (d (n "serde") (r "=1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "=0.9.14") (d #t) (k 0)) (d (n "tokio") (r "=1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qz5m5sivl4p1bgmrrqp50ragky0s8li0g71gppmyv4qz87bhq5a")))

(define-public crate-iceyee_config-1.0.1 (c (n "iceyee_config") (v "1.0.1") (d (list (d (n "serde") (r "=1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "=0.9.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1ks7510ni10dr8bljg5lf5a931yxgi0zm6l8fh2447ri40kn53")))

(define-public crate-iceyee_config-1.0.2 (c (n "iceyee_config") (v "1.0.2") (d (list (d (n "serde") (r "=1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "=0.9.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14h1a4iamgk4l2amv8kxpqg7lvx075wqqgqi2pyz535qrqfyi7gs")))

(define-public crate-iceyee_config-1.0.3 (c (n "iceyee_config") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3jv3rcb6jv6irihpcsbpplzzmrv6mkn42z092xv0jch0ry9w4i")))

(define-public crate-iceyee_config-3.0.0 (c (n "iceyee_config") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w353nhw6k1r21byp6y4qhxj8rzsq6r4j1fz1bkl539g01n8crj1")))

