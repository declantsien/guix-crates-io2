(define-module (crates-io ic ey iceyee_error) #:use-module (crates-io))

(define-public crate-iceyee_error-1.0.0 (c (n "iceyee_error") (v "1.0.0") (h "1bwakwix3fq77wsbq2k03pd6k55hhy3lakap0v7qzjwypffsb5bc")))

(define-public crate-iceyee_error-1.0.1 (c (n "iceyee_error") (v "1.0.1") (h "0i4143wlbb5dzyii87gadxfjvyz7zb02zgnhvsxcwi8zrkxx3pc3")))

(define-public crate-iceyee_error-1.0.2 (c (n "iceyee_error") (v "1.0.2") (h "1yimxijnb29dkbs63s09m3ldaawvadfl4ma8an241c5pv94q0mir")))

(define-public crate-iceyee_error-1.0.3 (c (n "iceyee_error") (v "1.0.3") (h "1c561al29bvs575agj0d069shpd8bvblfvfddpaw0ryi23q8qvkx")))

(define-public crate-iceyee_error-3.0.0 (c (n "iceyee_error") (v "3.0.0") (h "15isbvgcvy2di6psb43aw7s8simj8gq1k0jxrs757751xf8vssib")))

(define-public crate-iceyee_error-3.0.1 (c (n "iceyee_error") (v "3.0.1") (h "1xzkw7fb86kp2473k7nsa2ii9p51mqjqc0ix1zs9kvfpsz54p0yp")))

(define-public crate-iceyee_error-3.0.2 (c (n "iceyee_error") (v "3.0.2") (h "1074cmbqywniqjsz970447ssjqfgc8kqcl17graa2z2f940sm01c")))

