(define-module (crates-io ic ap icaparse) #:use-module (crates-io))

(define-public crate-icaparse-0.1.0 (c (n "icaparse") (v "0.1.0") (h "041wgzwgzix4sm16kzv8p8w2wd6wdllwws88r9c2awgvv4l8adhq") (f (quote (("std") ("default" "std"))))))

(define-public crate-icaparse-0.1.1 (c (n "icaparse") (v "0.1.1") (d (list (d (n "httparse") (r "^1.2.3") (d #t) (k 0)))) (h "00gzjrgw08h1yvznd5yzcjk97fcjpdhm4z9y2lvp0xicmyx1zs51") (f (quote (("std") ("default" "std"))))))

(define-public crate-icaparse-0.2.0 (c (n "icaparse") (v "0.2.0") (d (list (d (n "httparse") (r "^1.2.3") (d #t) (k 0)))) (h "1cyvcllgpih7blqxhblfifjb9ghcvjv9qp7jc8a3w0xdl6c299x9") (f (quote (("std") ("default" "std"))))))

