(define-module (crates-io ic p_ icp_2d) #:use-module (crates-io))

(define-public crate-icp_2d-0.1.0 (c (n "icp_2d") (v "0.1.0") (d (list (d (n "lstsq") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "0xv6bisl6x5k7skjv7c84shlrwdr528ixjl85fz0z1qvg9pdz9hq")))

(define-public crate-icp_2d-0.1.1 (c (n "icp_2d") (v "0.1.1") (d (list (d (n "lstsq") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "106jqgf79hnpc6lmm63c3z5661m8im9vbfybpw8icaviawadfvyv")))

(define-public crate-icp_2d-0.1.2 (c (n "icp_2d") (v "0.1.2") (d (list (d (n "lstsq") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "0dn0jypr9ph2d1rwqa812c3h8ak91k30v0v7wfzl9cg1xyklg8rn")))

