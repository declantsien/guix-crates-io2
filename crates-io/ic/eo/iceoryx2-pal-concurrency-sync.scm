(define-module (crates-io ic eo iceoryx2-pal-concurrency-sync) #:use-module (crates-io))

(define-public crate-iceoryx2-pal-concurrency-sync-0.0.1 (c (n "iceoryx2-pal-concurrency-sync") (v "0.0.1") (d (list (d (n "iceoryx2-bb-testing") (r "^0.0.1") (d #t) (k 2)))) (h "1i2zn390yzmccak4w778jvy4npxn3dvy0bpj0fx0an99faa7lidw") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.0.2 (c (n "iceoryx2-pal-concurrency-sync") (v "0.0.2") (d (list (d (n "iceoryx2-bb-testing") (r "^0.0.2") (d #t) (k 2)))) (h "0dazvfxx38qlkb0538873bkyn0fqrir244bxx4k60c4i9m54s6gx") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.0.3 (c (n "iceoryx2-pal-concurrency-sync") (v "0.0.3") (d (list (d (n "iceoryx2-bb-testing") (r "^0.0.3") (d #t) (k 2)))) (h "0rihldgdkl1s3ckm4b5x885ixmm2y3g4anz8k8r9z0p4ax9nln63") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.0.4 (c (n "iceoryx2-pal-concurrency-sync") (v "0.0.4") (d (list (d (n "iceoryx2-bb-testing") (r "^0.0.4") (d #t) (k 2)))) (h "04xnlcq3sjcpyklac2xkgq04w33f08d0z2pk2s9pnf54l8gk0n48") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.0.5 (c (n "iceoryx2-pal-concurrency-sync") (v "0.0.5") (d (list (d (n "iceoryx2-bb-testing") (r "^0.0.5") (d #t) (k 2)))) (h "0grj2xmz01mnw397nygibzaaqws3aj2jf1dy7fljc6d49kcvm8yv") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.1.0 (c (n "iceoryx2-pal-concurrency-sync") (v "0.1.0") (d (list (d (n "iceoryx2-bb-testing") (r "^0.1.0") (d #t) (k 2)))) (h "19kqf66d291rljywb4y20wywgzj3qyrc9166kxq6719za62x6qqp") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.1.1 (c (n "iceoryx2-pal-concurrency-sync") (v "0.1.1") (d (list (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)))) (h "12rpsxd218hkra03l0mv3pnv6vphpjivr9jw04mhml2bg2pc7rk0") (r "1.72.1")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.2.0 (c (n "iceoryx2-pal-concurrency-sync") (v "0.2.0") (d (list (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)))) (h "1c7ykc1hr1l1rcj1nx1vfc468cwhwiw1p40ylnbgnw1r0lbb15h8") (r "1.70")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.2.1 (c (n "iceoryx2-pal-concurrency-sync") (v "0.2.1") (d (list (d (n "iceoryx2-bb-testing") (r "^0.2.1") (d #t) (k 2)))) (h "1hwb08mh6zy4wlwsqv45dc7i9xc1fmf0dzpgvnsn3zpi32wr23xj") (r "1.70")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.2.2 (c (n "iceoryx2-pal-concurrency-sync") (v "0.2.2") (d (list (d (n "iceoryx2-bb-testing") (r "^0.2.2") (d #t) (k 2)))) (h "1gd3mhy5lp0y6rja59m5zba89prgvj4d3w2y3lsg67mppjx8i21d") (r "1.70")))

(define-public crate-iceoryx2-pal-concurrency-sync-0.3.0 (c (n "iceoryx2-pal-concurrency-sync") (v "0.3.0") (d (list (d (n "iceoryx2-bb-testing") (r "^0.3.0") (d #t) (k 2)))) (h "16sfdf7ww9xfh9hc6pwr214xncad14m2zpcb75r2ra2w59rlklyl") (r "1.73")))

