(define-module (crates-io ic eo iceoryx2-bb-testing) #:use-module (crates-io))

(define-public crate-iceoryx2-bb-testing-0.0.1 (c (n "iceoryx2-bb-testing") (v "0.0.1") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.0.1") (d #t) (k 0)))) (h "0j8w7dk2rrgalarziyz46rln4zpsxl3cv08qh4r2z2kdsqs5gprb") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.0.2 (c (n "iceoryx2-bb-testing") (v "0.0.2") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.0.2") (d #t) (k 0)))) (h "1g16mhk6f9i2mf5siz5cw4axl1h823ringda7v02lszx589l1rnb") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.0.3 (c (n "iceoryx2-bb-testing") (v "0.0.3") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.0.3") (d #t) (k 0)))) (h "1y6zifl1m37nxrk800f8ysqi9dpxlqp53pmsbqmbvy5hs7zyln7f") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.0.4 (c (n "iceoryx2-bb-testing") (v "0.0.4") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.0.4") (d #t) (k 0)))) (h "1dacqy32dd10ydp88mszj1mv262mgzrj6ldwq4yclsx2r10xmg97") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.0.5 (c (n "iceoryx2-bb-testing") (v "0.0.5") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.0.5") (d #t) (k 0)))) (h "0gymy2zr35kc5vsjnmq1p5vq9v1nmlzzk4y4jd36a9z9nm2r36v8") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.1.0 (c (n "iceoryx2-bb-testing") (v "0.1.0") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.1.0") (d #t) (k 0)))) (h "0ipka5frcar0vs3xlpa2pxk5lj59q6djhkdb9m8pldj7wh3pnr4h") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.1.1 (c (n "iceoryx2-bb-testing") (v "0.1.1") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.1.1") (d #t) (k 0)))) (h "1iaxf4dxz28c9ccc5gzafcipjcsij27rxr9kx6ji350qvfcbbl0z") (r "1.72.1")))

(define-public crate-iceoryx2-bb-testing-0.2.0 (c (n "iceoryx2-bb-testing") (v "0.2.0") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.1.1") (d #t) (k 0)))) (h "1kwnd5vyvgjv67wxc95s0xzd6lrviwnb9zgwcfg7pkjcrbdihc0p") (r "1.70")))

(define-public crate-iceoryx2-bb-testing-0.2.1 (c (n "iceoryx2-bb-testing") (v "0.2.1") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.2.1") (d #t) (k 0)))) (h "0prnsw0dvrvq8lcdrnlrv7mi55kcnpnaskbcvaykfvlm7ckqb91m") (r "1.70")))

(define-public crate-iceoryx2-bb-testing-0.2.2 (c (n "iceoryx2-bb-testing") (v "0.2.2") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.2.2") (d #t) (k 0)))) (h "124bkqbydl8n0yi8pcl6ni9x98kwdw8g42dgwraj95chbl6b68s0") (r "1.70")))

(define-public crate-iceoryx2-bb-testing-0.3.0 (c (n "iceoryx2-bb-testing") (v "0.3.0") (d (list (d (n "iceoryx2-pal-configuration") (r "^0.3.0") (d #t) (k 0)))) (h "1klgnq54s99iqhk8k5h16jsc310pxvknqmzxmbsvg1hq64prky0a") (r "1.73")))

