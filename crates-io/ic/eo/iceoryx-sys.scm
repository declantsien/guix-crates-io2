(define-module (crates-io ic eo iceoryx-sys) #:use-module (crates-io))

(define-public crate-iceoryx-sys-0.0.13 (c (n "iceoryx-sys") (v "0.0.13") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09461i7z6yckzwg3ww2m4dzymnkf2m2bz1g7hmhmzd7iz9zjxvi8")))

(define-public crate-iceoryx-sys-0.1.0 (c (n "iceoryx-sys") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a558w3wn1laragdzbj5lx4ib36kyxxjlv7l16limmj6j3hkaalq")))

