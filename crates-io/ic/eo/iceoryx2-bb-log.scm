(define-module (crates-io ic eo iceoryx2-bb-log) #:use-module (crates-io))

(define-public crate-iceoryx2-bb-log-0.0.1 (c (n "iceoryx2-bb-log") (v "0.0.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "17dgcv4sb7kr57015shzv2lfddxrcfnw8dnxgdcgqmr927r7zvx0") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.0.2 (c (n "iceoryx2-bb-log") (v "0.0.2") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0n3i9hysr3xb19q0sx577jwyim9csmish8g04lmb5660f3l59fwn") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.0.3 (c (n "iceoryx2-bb-log") (v "0.0.3") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0zfbjzgvwc6gg3m23dzab52dcv1795g6z6i7fg99cwdvjs59msz9") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.0.4 (c (n "iceoryx2-bb-log") (v "0.0.4") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1imwkqlv1k5ai8299n7kaw45nhg8xqqsn9a86a0kcjsn7smyciw0") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.0.5 (c (n "iceoryx2-bb-log") (v "0.0.5") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1bg0hqhv7j2q0q7k4yjhvhl6qb5dpsp864dh1s40nfpr0sly3r4m") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.1.0 (c (n "iceoryx2-bb-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0c0nfkm8wafj5wwvqslqb68yfympla0284f1i32vmp1pabnzl6v3") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.1.1 (c (n "iceoryx2-bb-log") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1r83i2613lkpar8m55v65qi420wzi5cvz6bp6xj58kbg0fcfkr0g") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.72.1")))

(define-public crate-iceoryx2-bb-log-0.2.0 (c (n "iceoryx2-bb-log") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0lng89vy0sjk333agzrcwlkb8q9mnxr7nnaxpkxqxphgi1flpsrf") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.70")))

(define-public crate-iceoryx2-bb-log-0.2.1 (c (n "iceoryx2-bb-log") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1l44zx12xd38swydq8zsb177dk3fvzlghhassh92ijj8wp3zyrwm") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.70")))

(define-public crate-iceoryx2-bb-log-0.2.2 (c (n "iceoryx2-bb-log") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1yyngg47039albxf3qq4z7wljyzsa4xzr398y60093vis691wfvm") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.70")))

(define-public crate-iceoryx2-bb-log-0.3.0 (c (n "iceoryx2-bb-log") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0i5d3mmh9gv9d1l9zhw8l7z77z98i8rjyk4hbgm372bm3506wv10") (s 2) (e (quote (("logger_tracing" "dep:tracing") ("logger_log" "dep:log")))) (r "1.73")))

