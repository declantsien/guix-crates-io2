(define-module (crates-io ic eo iceoryx2-bb-container) #:use-module (crates-io))

(define-public crate-iceoryx2-bb-container-0.0.3 (c (n "iceoryx2-bb-container") (v "0.0.3") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.0.3") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.0.3") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.0.3") (d #t) (k 2)))) (h "1y1vcmqsc679ji0d6rsd7hqbi4rkhsbzmnxh6p6yan7xab3c4xnp") (r "1.72.1")))

(define-public crate-iceoryx2-bb-container-0.0.4 (c (n "iceoryx2-bb-container") (v "0.0.4") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.0.4") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.0.4") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.0.4") (d #t) (k 2)))) (h "1xxpik9vlgsgqzk8mmk6ivvmrz84jdpsyp1k0q02x4nv9m1cpxqd") (r "1.72.1")))

(define-public crate-iceoryx2-bb-container-0.0.5 (c (n "iceoryx2-bb-container") (v "0.0.5") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.0.5") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.0.5") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.0.5") (d #t) (k 2)))) (h "1bz4pvv3h6nyh55hvb528qxawdb71bqfs7pj6i9aavcxk8s4dc06") (r "1.72.1")))

(define-public crate-iceoryx2-bb-container-0.1.0 (c (n "iceoryx2-bb-container") (v "0.1.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.1.0") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.1.0") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.1.0") (d #t) (k 2)))) (h "05x75g1mgj7d7v4x4jdm3j3rwz1xx8hw4s27h0ksngwpw5fiqa4c") (r "1.72.1")))

(define-public crate-iceoryx2-bb-container-0.1.1 (c (n "iceoryx2-bb-container") (v "0.1.1") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.1.1") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.1.1") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)))) (h "0ba0kl0ylx9yw2zccc8c5lk104vwy5a8rpwihgx63x0ang664n0n") (r "1.72.1")))

(define-public crate-iceoryx2-bb-container-0.2.0 (c (n "iceoryx2-bb-container") (v "0.2.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.1.1") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.1.1") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)))) (h "0icy5sw20g6bqbcp08xwbddzcnhcv54hpsx1f9i5vldlv51yprbi") (r "1.70")))

(define-public crate-iceoryx2-bb-container-0.2.1 (c (n "iceoryx2-bb-container") (v "0.2.1") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.2.1") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.2.1") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.2.1") (d #t) (k 2)))) (h "0n6nwcq5rwd83plcs93a41psa5yygvd8jml7kiykhwc782mz06cc") (r "1.70")))

(define-public crate-iceoryx2-bb-container-0.2.2 (c (n "iceoryx2-bb-container") (v "0.2.2") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.2.2") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.2.2") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.2.2") (d #t) (k 2)))) (h "0qn9nvx654ihxy7szby5ha1kl8lbrl2nisc94zq920invspicpbc") (r "1.70")))

(define-public crate-iceoryx2-bb-container-0.3.0 (c (n "iceoryx2-bb-container") (v "0.3.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-elementary") (r "^0.3.0") (d #t) (k 0)) (d (n "iceoryx2-bb-log") (r "^0.3.0") (d #t) (k 0)) (d (n "iceoryx2-bb-testing") (r "^0.3.0") (d #t) (k 2)))) (h "0v5fnfill08i22ciar7aqaibma7sg06biqayairgrq37qgsa73gc") (r "1.73")))

