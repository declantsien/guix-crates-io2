(define-module (crates-io ic eo iceoryx-rs) #:use-module (crates-io))

(define-public crate-iceoryx-rs-0.0.11 (c (n "iceoryx-rs") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vk28d64z00xj0w7yr98779m4hr19jnv82bn1hczmxdffy6qsyrq")))

(define-public crate-iceoryx-rs-0.0.12 (c (n "iceoryx-rs") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zgxkk6vgzp11h33pmfp7fvmq1jrz60bzgjmiclwdk69cf5fzzym")))

(define-public crate-iceoryx-rs-0.0.13 (c (n "iceoryx-rs") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ffi") (r "^0.0.13") (d #t) (k 0) (p "iceoryx-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1acvl8zc2dz2714av4wv9i349zl66pqwlbydd7y97fmxcsiifnwb")))

(define-public crate-iceoryx-rs-0.1.0 (c (n "iceoryx-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "iceoryx-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b4p27grh7khpvsgwp14863bnra2bydlnvlasxmn30z2qcpw6dh2")))

