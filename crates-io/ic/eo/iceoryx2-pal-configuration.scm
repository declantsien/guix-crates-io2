(define-module (crates-io ic eo iceoryx2-pal-configuration) #:use-module (crates-io))

(define-public crate-iceoryx2-pal-configuration-0.0.1 (c (n "iceoryx2-pal-configuration") (v "0.0.1") (h "0k7pvc5cqbmpk2qr7dw76i260s6ds9pbln3h1ksnpqwsnk89famd") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.0.2 (c (n "iceoryx2-pal-configuration") (v "0.0.2") (h "1r7zdckc8m9q19fz4nf4y04mrqxbbzf535n4z7xv3440n74kd7z0") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.0.3 (c (n "iceoryx2-pal-configuration") (v "0.0.3") (h "1ca44cldan6bqfag4cn2m5gjlq3s3785236kn9ykpb8q7dcyqkhn") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.0.4 (c (n "iceoryx2-pal-configuration") (v "0.0.4") (h "0ivs6byf016p6binvb0sq2yhnjqbz51p1rpya4fjq5jwl0y57zr4") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.0.5 (c (n "iceoryx2-pal-configuration") (v "0.0.5") (h "1grqsq7bffn089n0m11armr6p24cpc0g3rr4qf184b9scm1z0xsr") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.1.0 (c (n "iceoryx2-pal-configuration") (v "0.1.0") (h "18sp97myw0mkvvllpi6c1dbbfa73f0m3z37sr0m2y2l1dy1in6jw") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.1.1 (c (n "iceoryx2-pal-configuration") (v "0.1.1") (h "18rvkzsiwi2q9pn9s7mzrg9pc5xfvwqsx03mh2yx9jxdxclac7l8") (r "1.72.1")))

(define-public crate-iceoryx2-pal-configuration-0.2.0 (c (n "iceoryx2-pal-configuration") (v "0.2.0") (h "0kjbksq2nzq2vvnw7h69rg0ra6dphly5rrq4pskwq0ivfi6gxf7k") (r "1.70")))

(define-public crate-iceoryx2-pal-configuration-0.2.1 (c (n "iceoryx2-pal-configuration") (v "0.2.1") (h "0cbr6wbqzs8b7b5dya1fp207wyhqzpijfx391plfz6vlvxsf3zm9") (r "1.70")))

(define-public crate-iceoryx2-pal-configuration-0.2.2 (c (n "iceoryx2-pal-configuration") (v "0.2.2") (h "15kwncv4wyhwa6hw6s2ag6m9lgk7c75xbfdh6nw8jxnskdins6y9") (r "1.70")))

(define-public crate-iceoryx2-pal-configuration-0.3.0 (c (n "iceoryx2-pal-configuration") (v "0.3.0") (h "127jh97xfgmcha210z64l4p91p017lgmnf6qb9377mzkc2lyhzxn") (r "1.73")))

