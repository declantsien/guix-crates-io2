(define-module (crates-io ic eo iceoryx2-bb-elementary) #:use-module (crates-io))

(define-public crate-iceoryx2-bb-elementary-0.0.1 (c (n "iceoryx2-bb-elementary") (v "0.0.1") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.0.1") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "1jj2clq0n6zhvfsyp2zs8db6ppzm0r2r8gp06761ii5rbs70j574") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.0.2 (c (n "iceoryx2-bb-elementary") (v "0.0.2") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.0.2") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0ml9sd382wrkf6ivwyhlvwda5f8lx8hxhyf3mdhy3xczlp69xvy8") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.0.3 (c (n "iceoryx2-bb-elementary") (v "0.0.3") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.0.3") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0jhl3lj54g1lm67bk04dwckad9qgc90q3j2dq0cnpb0xiy2kbj5y") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.0.4 (c (n "iceoryx2-bb-elementary") (v "0.0.4") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.0.4") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0fsw3zz73ya72fsz4h13r7gxaz6kp269mfc3f7sa52w7y9ha66a5") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.0.5 (c (n "iceoryx2-bb-elementary") (v "0.0.5") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.0.5") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0wmnpkfx0i9s153vrwfcmvfdyalyxq0s1g66kp8wdfs39db82hr0") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.1.0 (c (n "iceoryx2-bb-elementary") (v "0.1.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.1.0") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0i5gnzy37xvrpqs892pkylq8h9q1nv18q0s0r1dqiss8m40qzarh") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.1.1 (c (n "iceoryx2-bb-elementary") (v "0.1.1") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "06jf328g8vf735h6zk0y48fhgla12hh9239a18kx5dqj86lqymfl") (r "1.72.1")))

(define-public crate-iceoryx2-bb-elementary-0.2.0 (c (n "iceoryx2-bb-elementary") (v "0.2.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.1.1") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0fwdwawpbpfrc121l40nhxq8v9f27f3pkgq80a3kzq8jixd3ch5l") (r "1.70")))

(define-public crate-iceoryx2-bb-elementary-0.2.1 (c (n "iceoryx2-bb-elementary") (v "0.2.1") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.2.1") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "1bx6pkqah5b55k2ay50pn1ywhncx7v9x657zrxf0y07c93lvrhci") (r "1.70")))

(define-public crate-iceoryx2-bb-elementary-0.2.2 (c (n "iceoryx2-bb-elementary") (v "0.2.2") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.2.2") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "0shpzjg0vvzjwsa3h89sz6d04wrwxzc2ch8s0gfzbwg33ny8p8jf") (r "1.70")))

(define-public crate-iceoryx2-bb-elementary-0.3.0 (c (n "iceoryx2-bb-elementary") (v "0.3.0") (d (list (d (n "generic-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "iceoryx2-bb-testing") (r "^0.3.0") (d #t) (k 2)) (d (n "pin-init") (r "^0.2.0") (d #t) (k 2)))) (h "1llhj2wjm4qpkc3q6lg6n7xnmmkg2qv70ix4zr9bwksxd4a35dzi") (r "1.73")))

