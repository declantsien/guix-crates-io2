(define-module (crates-io ic ef icefalldb) #:use-module (crates-io))

(define-public crate-icefalldb-0.1.0 (c (n "icefalldb") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0xpnp6c0300sv0g1v7lycrb7b1hid4dvljkw5n3ndfqw2fgxxkjr")))

