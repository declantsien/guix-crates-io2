(define-module (crates-io ic ef icefall) #:use-module (crates-io))

(define-public crate-icefall-0.1.0 (c (n "icefall") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jf32sv9ixgwgfrlcfyd96z4j9rcjcpd84h2ln2w4zkwfs6mq2q5")))

