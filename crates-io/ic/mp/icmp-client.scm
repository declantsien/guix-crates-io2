(define-module (crates-io ic mp icmp-client) #:use-module (crates-io))

(define-public crate-icmp-client-0.0.0 (c (n "icmp-client") (v "0.0.0") (h "1a7rki0k41smmyz5a5jkw4rz0fa5x8qj94y55qb3169rdpw5qyz1") (y #t)))

(define-public crate-icmp-client-0.1.0 (c (n "icmp-client") (v "0.1.0") (d (list (d (n "async-io") (r "^1") (o #t) (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "icmp-packet") (r "^0.1") (d #t) (k 2)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "134y9np1nrwmlflqzjmisdsp0lk84z9sg9mpgq099qsvdri0dipy") (f (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

(define-public crate-icmp-client-0.1.1 (c (n "icmp-client") (v "0.1.1") (d (list (d (n "async-io") (r "^1") (o #t) (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "icmp-packet") (r "^0.1") (d #t) (k 2)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0b6mljg2xywv0d9994nml1hlx5bm1ggcqwji48wp80nng3nq42w2") (f (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

(define-public crate-icmp-client-0.2.0 (c (n "icmp-client") (v "0.2.0") (d (list (d (n "async-io") (r "^1") (o #t) (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "icmp-packet") (r "^0.1") (d #t) (k 2)) (d (n "os_info") (r "^3") (d #t) (k 2)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pw4ralaj1xl4l611qlz44dhl56bqjfpmw1xcy8nilw0i5dcm04v") (f (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

