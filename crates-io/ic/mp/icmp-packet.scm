(define-module (crates-io ic mp icmp-packet) #:use-module (crates-io))

(define-public crate-icmp-packet-0.0.0 (c (n "icmp-packet") (v "0.0.0") (h "0c1z2lr7lmcq5mr47rsgviyd127f8wva10fazg5d85s70qwkgb58") (y #t)))

(define-public crate-icmp-packet-0.1.0 (c (n "icmp-packet") (v "0.1.0") (d (list (d (n "pnet_packet") (r "^0.33") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (o #t) (k 0)) (d (n "wrapping-macro") (r "^0.2") (k 0)))) (h "1mnpx27j79xvh5b43lfdvgr7lnfhv32xpwn0ygdlg7my34zdl6m1")))

