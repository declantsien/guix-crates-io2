(define-module (crates-io ic mp icmp-socket) #:use-module (crates-io))

(define-public crate-icmp-socket-0.1.0 (c (n "icmp-socket") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "socket2") (r "^0.3.19") (d #t) (k 0)))) (h "1m0i38vyl74ppkdkchkkys400gnji7pjayhcr9crlak2032vwjys")))

(define-public crate-icmp-socket-0.1.1 (c (n "icmp-socket") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "socket2") (r "^0.3.19") (d #t) (k 0)))) (h "1y7c0bzyd4vb9dlwcz7w4c5ixdgma5wkfrh7sax16c994drv70wc")))

(define-public crate-icmp-socket-0.1.2 (c (n "icmp-socket") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)))) (h "08rfm9fx5dixwq84ipmv7i8980jy5q2i77g6iiifwqz2ymjfc6hx")))

(define-public crate-icmp-socket-0.2.0 (c (n "icmp-socket") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "socket2") (r "^0.4.4") (f (quote ("all"))) (d #t) (k 0)))) (h "01gnga88lzfjpck4giwvymqk80p1fmzw29wlf012ysydhapkvg4q")))

