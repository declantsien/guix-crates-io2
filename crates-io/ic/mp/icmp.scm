(define-module (crates-io ic mp icmp) #:use-module (crates-io))

(define-public crate-icmp-0.1.0 (c (n "icmp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0pqnikzq1ya64n9ivdb64h3r3x3a74iw01rs2wplnbv6c6sh0fr8")))

(define-public crate-icmp-0.1.1 (c (n "icmp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1r05dvs64xn2kmbk7s723ama8a7vy9xx4y3v04n5cziixmvm6svl")))

(define-public crate-icmp-0.1.2 (c (n "icmp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0l9cswppcdf7qc1rjhm7inlfnhljlz10a9z8cssafprgwfd1qvhq")))

(define-public crate-icmp-0.1.3 (c (n "icmp") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.95") (d #t) (k 2)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0nhaxqvdwmwvllpg9agrkwykrc6fi8lx6283yaqksd70wimbckh2")))

(define-public crate-icmp-0.1.4 (c (n "icmp") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0.95") (d #t) (k 2)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0pbgib5mhdy3pzpz4ansxxibkgd8hrycnccqhra4bff7lrdgr776")))

(define-public crate-icmp-0.1.5 (c (n "icmp") (v "0.1.5") (d (list (d (n "clippy") (r "^0.0.95") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "142ic08x4mm9fbsdxim8r6nybhg1k32zh76y9sfp0n67c911qldj") (f (quote (("default"))))))

(define-public crate-icmp-0.3.0 (c (n "icmp") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "05ifpc98k5hc2xrmr3sydgsb3gbf2aiqcs5ligyqpgayh2yyjwxc") (f (quote (("default"))))))

