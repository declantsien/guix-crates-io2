(define-module (crates-io ic mp icmptunnel-rs) #:use-module (crates-io))

(define-public crate-icmptunnel-rs-0.1.0 (c (n "icmptunnel-rs") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.4") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("reusable_secrets"))) (d #t) (k 0)))) (h "1qvaxsq2jaa73rb5gzh4q85gl8ycsjzn8aik1kdmxp8r49qwpxh1")))

