(define-module (crates-io ic as icasadi) #:use-module (crates-io))

(define-public crate-icasadi-0.1.0 (c (n "icasadi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (k 0)))) (h "0lwxjrsgcfqwfvvz7h030d2i8j850plp3sb5aav3gkp7llv9zx10")))

(define-public crate-icasadi-0.1.1 (c (n "icasadi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (k 0)))) (h "167anj4bs7fs8axf7zpsw1q1s2dyyg8ixfisgfq85k1dl5kgnqxp")))

