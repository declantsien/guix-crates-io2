(define-module (crates-io ic u4 icu4x) #:use-module (crates-io))

(define-public crate-icu4x-0.0.1 (c (n "icu4x") (v "0.0.1") (h "06awsw4wci6vi60g44vz0rsb5rknz4zgp9ig676rnyf592zmmcj8")))

(define-public crate-icu4x-0.1.0 (c (n "icu4x") (v "0.1.0") (h "1x2ljpm57v0h7g240f01n62dr2k81804q43yirr6z050gsr8kkv4")))

(define-public crate-icu4x-1.0.0 (c (n "icu4x") (v "1.0.0") (h "09l873bg2fyxsb5maiw2lg98c0y73lbly9gx7x6bqkpfnwhh606a")))

(define-public crate-icu4x-1.0.1 (c (n "icu4x") (v "1.0.1") (h "07fbjjmp7gzlhnf8y7cram2kpzw9bzfmnaidfpqzd1h31bvn9nv9")))

