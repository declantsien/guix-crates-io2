(define-module (crates-io ic -b ic-btc-interface) #:use-module (crates-io))

(define-public crate-ic-btc-interface-0.1.0 (c (n "ic-btc-interface") (v "0.1.0") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1dh2bdn3rgiz874cn45pyrfj35b753m6qzihmcwjnzy8r7myzx78")))

(define-public crate-ic-btc-interface-0.2.0 (c (n "ic-btc-interface") (v "0.2.0") (d (list (d (n "candid") (r "^0.10.0") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1b43pkzppdcsmwhz42sibr861rcsk3hac7xjmav05kazvgpd3hj6")))

