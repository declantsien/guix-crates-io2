(define-module (crates-io ic -b ic-btc-validation) #:use-module (crates-io))

(define-public crate-ic-btc-validation-0.1.0 (c (n "ic-btc-validation") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.28.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)))) (h "0q97360j2scyljxc41k6jwlmh2zgj91vs1j7nvfx6m5r03sbawry")))

