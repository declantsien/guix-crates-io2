(define-module (crates-io ic -b ic-btc-test-utils) #:use-module (crates-io))

(define-public crate-ic-btc-test-utils-0.1.0 (c (n "ic-btc-test-utils") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.28.1") (f (quote ("rand"))) (d #t) (k 0)))) (h "1p92qq1n2acrxz057mzpvphdc3ab646ldy2hj4ivm22i2i1hm3my")))

