(define-module (crates-io ic hi ichiran) #:use-module (crates-io))

(define-public crate-ichiran-0.1.0 (c (n "ichiran") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0324n1c4an1a7y0a7ava7avpvgxdcx7n1grgvnk46nn26sf0diii") (r "1.56")))

(define-public crate-ichiran-0.2.0 (c (n "ichiran") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "06wl7yrp2d5xw9bab5vvmn4qr4dfg8mw48wd3fb8wf5na32an7hx") (r "1.56")))

(define-public crate-ichiran-0.4.0 (c (n "ichiran") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0iqv7k9vgh4f0xfxhq73751xwajzarj1dwx4b4z9d0rvx99rc1r1") (r "1.56.1")))

