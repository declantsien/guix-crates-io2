(define-module (crates-io ic hi iching) #:use-module (crates-io))

(define-public crate-iching-0.1.0 (c (n "iching") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yypmhfxwqccx4blqi8nvwz6q3idal6swpbw1g28ygqqigi370dl")))

(define-public crate-iching-0.2.0 (c (n "iching") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qgvmn25snx05prhab0v19bdr2ylrflrff3ndd0p7pvvm8f2l254")))

(define-public crate-iching-0.3.0 (c (n "iching") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r4jqxninqsv4mkrmsj76rn56q38g8zsc9vh48w25c31kg9y3ayw")))

(define-public crate-iching-0.3.1 (c (n "iching") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jvj9nhqfj2aqvcs16r61agzyx0vq5gs0cnlq0nw7pnb6m62j2lg")))

(define-public crate-iching-0.3.2 (c (n "iching") (v "0.3.2") (d (list (d (n "clap") (r "^2.32") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "~0.7") (d #t) (k 0)) (d (n "rand") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1gsir1fa22dnkcwxkkjvwcmmmwjdwphyznm8v06a4klihni2d1yi")))

(define-public crate-iching-0.4.0 (c (n "iching") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "~0.8") (d #t) (k 0)) (d (n "rand") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1gvv4vzr8nrgwzs18a333h27j2kyjasyg69s43s6w09z977d781q")))

(define-public crate-iching-0.5.0 (c (n "iching") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yb4q2x59mi1v1vsmxd4f8hmnnalcxsph5np9fxlkn736y7r234p")))

