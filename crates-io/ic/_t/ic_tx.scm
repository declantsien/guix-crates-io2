(define-module (crates-io ic _t ic_tx) #:use-module (crates-io))

(define-public crate-ic_tx-0.0.1 (c (n "ic_tx") (v "0.0.1") (d (list (d (n "candid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1rksdnyl12yh4diirjvx5n369vz2gyxjcap0v85rnxx1z7cvm81k") (f (quote (("default")))) (s 2) (e (quote (("candid" "dep:candid" "dep:serde"))))))

