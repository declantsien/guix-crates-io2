(define-module (crates-io ic ns icns-rs) #:use-module (crates-io))

(define-public crate-icns-rs-0.1.0 (c (n "icns-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1wpanjg4bl0w7mq2a7r9nxxfn86frq2sy6lw4l596b51qx9iccwn")))

(define-public crate-icns-rs-0.1.1 (c (n "icns-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "02fq8a06jgrxli2vqxhairzix14h7sffx7mk2s279xvy44ic1004") (y #t)))

(define-public crate-icns-rs-0.1.2 (c (n "icns-rs") (v "0.1.2") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "0lm44xwf2x3jrhh8wcpx9cgwkakfc3mvwlq6dkpfhy8mq246j8gf")))

