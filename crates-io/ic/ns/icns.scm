(define-module (crates-io ic ns icns) #:use-module (crates-io))

(define-public crate-icns-0.1.0 (c (n "icns") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.5") (d #t) (k 0)))) (h "1qp2s0494pspfacfyp2kay2svn62yd6k60gw29zl49g44wcjxjpi")))

(define-public crate-icns-0.2.0 (c (n "icns") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.5") (d #t) (k 0)))) (h "149mss6jhljc0m1109ak6i387fc0wsaa0l0sdr2c499vxh9vfnj2")))

(define-public crate-icns-0.2.1 (c (n "icns") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "png") (r "^0.6") (d #t) (k 0)))) (h "0maf3010n0cxlg9wdyyvjvl9j8izx1ia384ip1v1qrg3fyrjn80n")))

(define-public crate-icns-0.2.2 (c (n "icns") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "0s4fsgv4srnik5h8i2x4jfcdjbf5ycgshs2r6z3lapinjfgz295k")))

(define-public crate-icns-0.3.0 (c (n "icns") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "png") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0bb7p3dwgx95wnj4rp45yk9g2znhj726jga4f6zj62yagg9c4r5f") (f (quote (("pngio" "png") ("default" "pngio"))))))

(define-public crate-icns-0.3.1 (c (n "icns") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "png") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0h4slnysg38bpaqa6iaxrqjk3ndglna4k4lanjjp1nh8gsnzpk55") (f (quote (("pngio" "png") ("default" "pngio"))))))

