(define-module (crates-io ic ns icnsify) #:use-module (crates-io))

(define-public crate-icnsify-0.1.0 (c (n "icnsify") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "icns") (r "^0.1") (d #t) (k 0) (p "tauri-icns")) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0dl8fzyvfwajlis4wga48ambp9x783i50rni2f8gjys6p3q0wn0q")))

