(define-module (crates-io ic ns icnsup) #:use-module (crates-io))

(define-public crate-icnsup-0.1.0 (c (n "icnsup") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0cap44ycy5ji7gqnv75l8kfz5msvn0p7ha853vvrcfiz9qlvbz1s")))

(define-public crate-icnsup-0.2.0 (c (n "icnsup") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "05ba6jjjljq12shkv3y7jhbgwns4fxsczn3bhqklwywn1dk31fsv")))

