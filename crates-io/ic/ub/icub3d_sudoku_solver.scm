(define-module (crates-io ic ub icub3d_sudoku_solver) #:use-module (crates-io))

(define-public crate-icub3d_sudoku_solver-0.1.0 (c (n "icub3d_sudoku_solver") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1hhzmf8jjhs2h2zi9fk8ic4cl702hxkf8fk4l8gba8nhscc01z8b")))

(define-public crate-icub3d_sudoku_solver-0.1.1 (c (n "icub3d_sudoku_solver") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0iwwwrfl6rz0csr0imzkzgcncanyld0n3n8f0h8d0727ay52i6gx")))

(define-public crate-icub3d_sudoku_solver-0.1.2 (c (n "icub3d_sudoku_solver") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "16lscgnb576488c6cn4k259f7fh8c1wnychs1vdkqizvnnsmh9fa")))

