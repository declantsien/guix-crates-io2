(define-module (crates-io ic ub icub3d_combinatorics) #:use-module (crates-io))

(define-public crate-icub3d_combinatorics-0.1.0 (c (n "icub3d_combinatorics") (v "0.1.0") (h "1gs0drfj8bcbfm4a36mhkzvigqanmj7rishhlhmbs45yk5gdvpq9")))

(define-public crate-icub3d_combinatorics-0.1.1 (c (n "icub3d_combinatorics") (v "0.1.1") (h "1lg65rgi2ha7cfwi1g3h197yga478r9bcy1yd0jf150dg87j3lmx")))

