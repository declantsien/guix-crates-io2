(define-module (crates-io ic ep iceportal_derive) #:use-module (crates-io))

(define-public crate-iceportal_derive-1.0.1 (c (n "iceportal_derive") (v "1.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nd3v7z3d10k90i43j9c4nlpla3qfl4b9p2l79nbml0zqbhy5m7i") (y #t)))

(define-public crate-iceportal_derive-1.0.2 (c (n "iceportal_derive") (v "1.0.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "106pcqfi3flya2qj0ffanxhhfk09c612fkv2h8332fw5xq05q3ib") (y #t)))

(define-public crate-iceportal_derive-1.0.3 (c (n "iceportal_derive") (v "1.0.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikdli9psp0d29a1vn91499pfz3ajyvqkcfp8jsyfcg8gaxx7k5c") (y #t)))

