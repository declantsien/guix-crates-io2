(define-module (crates-io ic ep iceprogrs) #:use-module (crates-io))

(define-public crate-iceprogrs-0.1.0 (c (n "iceprogrs") (v "0.1.0") (d (list (d (n "gpiod") (r "^0.2.2") (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)))) (h "1lqc1h91r8ma31n3pnsgswyq74k5dqy0qfv83n8arjyg6f5iidgj")))

(define-public crate-iceprogrs-0.1.1 (c (n "iceprogrs") (v "0.1.1") (d (list (d (n "gpiod") (r "^0.2.2") (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)))) (h "09c6i2iggs0sqx1cx1qnwdw0kq5w1avmbxa7srs34vv121h1ns5n")))

