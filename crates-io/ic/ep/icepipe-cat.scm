(define-module (crates-io ic ep icepipe-cat) #:use-module (crates-io))

(define-public crate-icepipe-cat-0.5.0 (c (n "icepipe-cat") (v "0.5.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "icepipe") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (d #t) (k 0)))) (h "0nd434bl3n564n9i38g63aww1c5z84b7xfl0xckm9pf8xr894pvw")))

