(define-module (crates-io ic ep iceportal) #:use-module (crates-io))

(define-public crate-iceportal-1.0.3 (c (n "iceportal") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iceportal_derive") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1i1p4n8zj2hdp375n22pwxv8y63ixncdg7qsgj02dwsln9vcbnig")))

(define-public crate-iceportal-1.0.4 (c (n "iceportal") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iceportal_derive") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0swz5f251lnzkk7xf27ixcm1jh20gz0pq59w78v62nd3y31zippl")))

(define-public crate-iceportal-1.0.5 (c (n "iceportal") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iceportal_derive") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12nz6n38hva10f2zz0fh0zi6jkmk6dq3mb3xlvpn3y1nz93x2nwi")))

(define-public crate-iceportal-2.0.0 (c (n "iceportal") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1asipnh0hm87dppj4g65k911v8ysl3lmd0mvidhb39q57y88cvxc")))

(define-public crate-iceportal-2.0.1 (c (n "iceportal") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ish1idnmfb29hifh4jv2vl1y4yjlaw296dmvz5xn81jrx0f6bjs")))

(define-public crate-iceportal-2.0.2 (c (n "iceportal") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0gdgp5v6zfc2wfn6saj1in3g1r61zl1wnq181zq13vbc830xz3yf")))

(define-public crate-iceportal-2.0.3 (c (n "iceportal") (v "2.0.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1fv000ikqd8zns4g0sr5yplwmayk3rv6vjlphvm1vkdjlf7pgla1")))

(define-public crate-iceportal-2.0.4 (c (n "iceportal") (v "2.0.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r "~1.23") (f (quote ("macros"))) (d #t) (k 2)))) (h "1cvdhdc47srb7d6jladkds08hzpfwfzbnppzrqp97fin5ys0750z")))

(define-public crate-iceportal-2.0.5 (c (n "iceportal") (v "2.0.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r "~1.23") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ci1d97xpzlgs3cwpsrklrf2xx26p4a3f6i0lcfdw37r8604qpa1")))

