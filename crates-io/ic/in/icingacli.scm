(define-module (crates-io ic in icingacli) #:use-module (crates-io))

(define-public crate-icingacli-0.1.2 (c (n "icingacli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "1cqdrwijsz9s3rpfv0sk61mprsxd6vb55j9ivbwklbcz6qlbvdqa")))

