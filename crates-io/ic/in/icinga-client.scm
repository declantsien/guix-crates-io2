(define-module (crates-io ic in icinga-client) #:use-module (crates-io))

(define-public crate-icinga-client-0.6.0-alpha (c (n "icinga-client") (v "0.6.0-alpha") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "02x2gyqj6zmpvmnmlcl18mf11cdkmsj3qq9dgrps7kd76ywgakbs")))

(define-public crate-icinga-client-0.6.0 (c (n "icinga-client") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1272xvih034bs3payb3dcpdw94mwv7654bd1hpz7a54lhd72zyhp")))

