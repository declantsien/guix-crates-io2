(define-module (crates-io ic om icommit) #:use-module (crates-io))

(define-public crate-icommit-0.0.1 (c (n "icommit") (v "0.0.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "hyper") (r "^0.14.24") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a1ni6jyvcrqjm6ns5qgw1cs1hylqd544hb8gfgjjn48nlqig066")))

(define-public crate-icommit-0.0.2 (c (n "icommit") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "hyper") (r "^0.14.24") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "xshell") (r "^0.2.3") (d #t) (k 0)))) (h "1sgdvkws8am9mab2q044nwr18kmd6a5slclrd3na4yb797i3ngjd")))

