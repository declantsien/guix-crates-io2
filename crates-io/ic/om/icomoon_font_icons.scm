(define-module (crates-io ic om icomoon_font_icons) #:use-module (crates-io))

(define-public crate-icomoon_font_icons-1.0.0 (c (n "icomoon_font_icons") (v "1.0.0") (h "06hxy6r2594qlc1j9xksdw3xm6bsaxwx0qp1rni3hv2vilr8cfzh")))

(define-public crate-icomoon_font_icons-1.0.1 (c (n "icomoon_font_icons") (v "1.0.1") (h "0ylavc549xnm95nm010l3nx3hlp4fswcf0gah9aq7mlhpsbkwvxr")))

