(define-module (crates-io ic hw ichwh) #:use-module (crates-io))

(define-public crate-ichwh-0.1.0 (c (n "ichwh") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rmr36waqr900j0jhvdmadvzy9i4j1hdhxs595a9ypsqyw4lp4sb")))

(define-public crate-ichwh-0.1.1 (c (n "ichwh") (v "0.1.1") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x38knr75j7qc8vdb14b1zzi6p10vhyx9inmccvnvh3gmay5m299") (y #t)))

(define-public crate-ichwh-0.1.2 (c (n "ichwh") (v "0.1.2") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ypajpimnkkyx85cgf0avgnl1h1mxs9k2csn0ghr65rrf6bv9x7y") (y #t)))

(define-public crate-ichwh-0.1.3 (c (n "ichwh") (v "0.1.3") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q8pkh598nvw3bvfn8s2qjvxx6rlcy4lqxbxicflmlxi5xgngy5n") (y #t)))

(define-public crate-ichwh-0.2.0 (c (n "ichwh") (v "0.2.0") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jw996lwxk69fydqmizmfyy262vsv14bg5jrpz4az7mg59abdjvl")))

(define-public crate-ichwh-0.2.1 (c (n "ichwh") (v "0.2.1") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l9849sya2fhpbxi9wl451zkvjfrgn342xjvl2jwsakqmyj0drrf")))

(define-public crate-ichwh-0.2.2 (c (n "ichwh") (v "0.2.2") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qqcags06apbgymsv3vhiw66rpg117z0dqi9pc7kj1al4kic1kw7")))

(define-public crate-ichwh-0.3.0 (c (n "ichwh") (v "0.3.0") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("std" "async-await"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kj2qcqzcpzbwb6z33jpqxv5vnmbb6mr71ib04pfnnbfg02ys9zc")))

(define-public crate-ichwh-0.3.1 (c (n "ichwh") (v "0.3.1") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("std" "async-await"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vcj2q175bpr7ifr8c4b847f88il9wzrg2vgc0dnhd6ayccsyymk")))

(define-public crate-ichwh-0.3.2 (c (n "ichwh") (v "0.3.2") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("std" "async-await"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aqly45w2bafwnqy54bi6brvd1km2j0v5896mz27j65gdxjyw3ki")))

(define-public crate-ichwh-0.3.3 (c (n "ichwh") (v "0.3.3") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("std" "async-await"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a6kbkpnd5l30ingqackm7rvycb8mnvnada6s2bldqra4zvqz85d")))

(define-public crate-ichwh-0.3.4 (c (n "ichwh") (v "0.3.4") (d (list (d (n "async-std") (r "^1.4") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("std" "async-await"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m6628yw3l812hjknmh5b5gcvhn6as9gzjz60h54zjxyy4w5ss7a")))

