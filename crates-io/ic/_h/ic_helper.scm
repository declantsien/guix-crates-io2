(define-module (crates-io ic _h ic_helper) #:use-module (crates-io))

(define-public crate-ic_helper-0.1.0 (c (n "ic_helper") (v "0.1.0") (d (list (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0gyz48xzfx68dsngfsddk8b50v24g98fi3lvxwzg5r7zlav0w0zx")))

