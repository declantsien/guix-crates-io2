(define-module (crates-io ic kl ickle) #:use-module (crates-io))

(define-public crate-ickle-0.1.0 (c (n "ickle") (v "0.1.0") (h "1qwc3dg0fgpp4j2nz6lppid0w2zqha04rb5vwlpl9fl8ll0s7qay")))

(define-public crate-ickle-0.1.1 (c (n "ickle") (v "0.1.1") (h "1whibl5afzj2l3p9wl5cqi8lina6xk4gzjfxr4vf9234hfm5dmch")))

(define-public crate-ickle-0.2.0 (c (n "ickle") (v "0.2.0") (h "0p02qqj070rj8qr3xhlbc86g4i46d6a42jjcz5kifpnwxvafbfyz")))

(define-public crate-ickle-0.3.0 (c (n "ickle") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0flyzi21mzlckfkl544lsgmkhkxyg7c2akxah4n8czz913hjz8x8")))

