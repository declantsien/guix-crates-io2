(define-module (crates-io ic et icetea) #:use-module (crates-io))

(define-public crate-icetea-0.1.0 (c (n "icetea") (v "0.1.0") (h "1wr6rix2xy1w8gw6ndihgh1v1232g7mrgc7qf7nf4yk52bhcmqpb")))

(define-public crate-icetea-0.1.1 (c (n "icetea") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j2aa89q3qdx3gbgm7rg7va79p52da0p9iq6f2ib28dca0xhhwvn")))

(define-public crate-icetea-0.1.2 (c (n "icetea") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v2q06pmnk4q71a4pcs1cr94855s7249kbji5b7s6mb68jrhihq1")))

