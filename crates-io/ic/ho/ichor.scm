(define-module (crates-io ic ho ichor) #:use-module (crates-io))

(define-public crate-ichor-0.1.0 (c (n "ichor") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.6") (f (quote ("default-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0zwzxq5k1w22ndrdjbvvk5fhp0qgw995n66i64ah1zrg6168wa63")))

