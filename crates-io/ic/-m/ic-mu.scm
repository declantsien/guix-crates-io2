(define-module (crates-io ic -m ic-mu) #:use-module (crates-io))

(define-public crate-ic-mu-0.1.0 (c (n "ic-mu") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.1") (d #t) (k 0)))) (h "18lf5d59qmlyic5a0zg422a6h8877ylnrvw2m30vqscxqnfb6fi4")))

