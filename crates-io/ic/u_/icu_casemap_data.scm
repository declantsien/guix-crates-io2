(define-module (crates-io ic u_ icu_casemap_data) #:use-module (crates-io))

(define-public crate-icu_casemap_data-1.3.0 (c (n "icu_casemap_data") (v "1.3.0") (h "1w27x5p6ywa2vlbsd5p6j1lw5k3rgbk29yjy0wg0hkdkcl6zhd5a") (r "1.66")))

(define-public crate-icu_casemap_data-1.3.2 (c (n "icu_casemap_data") (v "1.3.2") (h "1z9gyz22qhsqdhi2lpj877mnwsnfxnf5w0lp1z84rffm2h60fl8j") (r "1.66")))

(define-public crate-icu_casemap_data-1.4.0 (c (n "icu_casemap_data") (v "1.4.0") (h "1ibwww7wc1y62j468zh01qrcf727mjb77n0hhd07ry76ikxhjr7k") (r "1.67")))

(define-public crate-icu_casemap_data-1.5.0 (c (n "icu_casemap_data") (v "1.5.0") (h "020znrhgb8043pwl2pbpw80yh84sgy34ch5y2d2zfj5pb9nrcmsd") (r "1.67")))

