(define-module (crates-io ic u_ icu_calendar_data) #:use-module (crates-io))

(define-public crate-icu_calendar_data-1.3.0 (c (n "icu_calendar_data") (v "1.3.0") (h "0qcb37pl00g5hp2a8y67f3bas65c7zz3y5i1n4yiar9nphc0m4fg") (r "1.66")))

(define-public crate-icu_calendar_data-1.3.2 (c (n "icu_calendar_data") (v "1.3.2") (h "0h48lzvw70fyv8d1rkq7kj87imb1sqphz4xy8z2nszna2jjx3n3m") (r "1.66")))

(define-public crate-icu_calendar_data-1.4.0 (c (n "icu_calendar_data") (v "1.4.0") (h "18jca95ncgd6fqava69gayvw6hxcvhmggvkf4p5rlpbk6b8cgbi2") (r "1.67")))

(define-public crate-icu_calendar_data-1.5.0 (c (n "icu_calendar_data") (v "1.5.0") (h "1l16n4yx3p73mklr61w2hdqhnzirjhsjica0ijr6zvji05zrn04f") (r "1.67")))

