(define-module (crates-io ic u_ icu_pattern) #:use-module (crates-io))

(define-public crate-icu_pattern-0.1.0 (c (n "icu_pattern") (v "0.1.0") (d (list (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "writeable") (r "^0.2") (d #t) (k 0)))) (h "1ddyydra9rgn9ilfssfwvy24wb66l6glh0awqjswkb693hk4f3mi")))

(define-public crate-icu_pattern-0.1.1 (c (n "icu_pattern") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "writeable") (r "^0.2.1") (d #t) (k 0)))) (h "032q2gnm5phljn009fhxjn879m6rhhxadblsgax64vm3nwggf5qa")))

(define-public crate-icu_pattern-0.1.2 (c (n "icu_pattern") (v "0.1.2") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "writeable") (r "^0.4") (d #t) (k 0)))) (h "1nsh422g9vqb360vxn1kcl40jyn9pk5fgw5iync1b1fs0325nsi0")))

(define-public crate-icu_pattern-0.1.3 (c (n "icu_pattern") (v "0.1.3") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "writeable") (r "^0.5.1") (d #t) (k 0)))) (h "1j4gkswpc5kwl0ph3ybwmvixrpmbk2kr1rb5bf0ki0rpjwfi207p")))

(define-public crate-icu_pattern-0.1.4 (c (n "icu_pattern") (v "0.1.4") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "writeable") (r "^0.5.1") (d #t) (k 0)))) (h "1an76hfc8xp5mc8485dfsyd6v6l3g64j1yw2xpjj8h3962jkll57")))

(define-public crate-icu_pattern-0.1.5 (c (n "icu_pattern") (v "0.1.5") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "writeable") (r "^0.5.1") (k 0)))) (h "1zjybq9sibssrljg4brg1zk74q7gwpbhrlbxkp67z36p3i9xxzqd") (r "1.66")))

(define-public crate-icu_pattern-0.2.0 (c (n "icu_pattern") (v "0.2.0") (d (list (d (n "databake") (r "^0.1.8") (f (quote ("derive"))) (o #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "either") (r "^1.9.0") (k 0)) (d (n "litemap") (r "^0.7.3") (o #t) (k 0)) (d (n "postcard") (r "^1.0.1") (f (quote ("use-std"))) (k 2)) (d (n "rmp-serde") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 2)) (d (n "writeable") (r "^0.5.5") (f (quote ("either"))) (k 0)) (d (n "yoke") (r "^0.7.4") (f (quote ("derive"))) (o #t) (k 0)) (d (n "zerofrom") (r "^0.1.3") (f (quote ("derive"))) (o #t) (k 0)) (d (n "zerofrom") (r "^0.1.3") (f (quote ("alloc"))) (k 2)) (d (n "zerovec") (r "^0.10.2") (f (quote ("databake" "serde"))) (k 2)))) (h "1r6p5y5dxdspwwvbj8ms3xf2f8h82al6irilvqbng389znm3czyb") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("zerofrom" "dep:zerofrom") ("yoke" "dep:yoke") ("serde" "alloc" "dep:serde") ("litemap" "dep:litemap") ("databake" "dep:databake")))) (r "1.67")))

