(define-module (crates-io ic u_ icu_locid_transform_data) #:use-module (crates-io))

(define-public crate-icu_locid_transform_data-1.3.0 (c (n "icu_locid_transform_data") (v "1.3.0") (h "01if2kamnq7pcmlr6sm8qjswn0y90lp30ig9bdja8a4qjx8q2pff") (r "1.66")))

(define-public crate-icu_locid_transform_data-1.3.2 (c (n "icu_locid_transform_data") (v "1.3.2") (h "0i6swi9qdfn8fx7gnm0yfykdvjmbzwy2s0pry6r5xxriajx1wx1a") (r "1.66")))

(define-public crate-icu_locid_transform_data-1.4.0 (c (n "icu_locid_transform_data") (v "1.4.0") (h "1p6k8xgziw94fyf7i1wp6npnm0hlxkwyd3gfmwnhwn7ricz6qp2l") (r "1.67")))

(define-public crate-icu_locid_transform_data-1.5.0 (c (n "icu_locid_transform_data") (v "1.5.0") (h "0vkgjixm0wzp2n3v5mw4j89ly05bg3lx96jpdggbwlpqi0rzzj7x") (r "1.67")))

