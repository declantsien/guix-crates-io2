(define-module (crates-io ic u_ icu_locale) #:use-module (crates-io))

(define-public crate-icu_locale-0.1.0 (c (n "icu_locale") (v "0.1.0") (h "1ga3sp3w4z84pv36qyhmib2wh991f0mp2whrlwjgzzwjr5ha4sbi")))

(define-public crate-icu_locale-0.1.1 (c (n "icu_locale") (v "0.1.1") (d (list (d (n "icu_locid") (r "^1.4.0") (d #t) (k 0)))) (h "1b2r69lkxmxwicdb3y6bb4f4gc5d12xkcn8zgj55ywbc1i5dwf31")))

(define-public crate-icu_locale-0.1.2 (c (n "icu_locale") (v "0.1.2") (d (list (d (n "icu_locid") (r "^1.4.0") (d #t) (k 0)))) (h "01gpwb11blga580wqxy74scrpq6a1dzwidjvvywdax48cgdyhjsq")))

