(define-module (crates-io ic u_ icu_list_data) #:use-module (crates-io))

(define-public crate-icu_list_data-1.3.0 (c (n "icu_list_data") (v "1.3.0") (h "18777m1k09svmya6v43vlkdyvv3wmvy0gyqw7hs6r501hnvp821g") (r "1.66")))

(define-public crate-icu_list_data-1.3.2 (c (n "icu_list_data") (v "1.3.2") (h "0vcrvsjkw9r3m6c8fsiz27ad5wcyzs948z2npgxflzyby21pa8yk") (r "1.66")))

(define-public crate-icu_list_data-1.4.0 (c (n "icu_list_data") (v "1.4.0") (h "0ci5jgix2z17s13awdsx2kgvipzclzbifzqbwznfszwsgb7szxj2") (r "1.67")))

(define-public crate-icu_list_data-1.5.0 (c (n "icu_list_data") (v "1.5.0") (h "13r9xl0h36pwzbs2x6pdi9lrzr1ls24sb5mx1nr9qry6s9q530p1") (r "1.67")))

