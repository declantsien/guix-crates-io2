(define-module (crates-io ic u_ icu_timezone_data) #:use-module (crates-io))

(define-public crate-icu_timezone_data-1.3.0 (c (n "icu_timezone_data") (v "1.3.0") (h "022h2slx9ifmnr9c7x7i8r5p6yb2lk5wf177ivn503z5plpx8a4n") (r "1.66")))

(define-public crate-icu_timezone_data-1.3.2 (c (n "icu_timezone_data") (v "1.3.2") (h "0hqvaqy75g5q11hppc4wi9ggjfpiab984by4d2vmkb1vcm522zkd") (r "1.66")))

(define-public crate-icu_timezone_data-1.4.0 (c (n "icu_timezone_data") (v "1.4.0") (h "1m4qlcfcaaac4s1dyw7md59zg3a1nfk3v4lm9ss2mkhw30gf5vkc") (r "1.67")))

(define-public crate-icu_timezone_data-1.5.0 (c (n "icu_timezone_data") (v "1.5.0") (h "13pddk6rvibcahdjahbmd8y4irjkc0lm0g1v6g72lglaa268g265") (r "1.67")))

