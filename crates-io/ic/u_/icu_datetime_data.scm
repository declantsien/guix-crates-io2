(define-module (crates-io ic u_ icu_datetime_data) #:use-module (crates-io))

(define-public crate-icu_datetime_data-1.3.0 (c (n "icu_datetime_data") (v "1.3.0") (h "1z21laqqxz0q7h8laswqfpn38jsxpxp4x5gryfkpyss395kyzpc4") (r "1.66")))

(define-public crate-icu_datetime_data-1.3.2 (c (n "icu_datetime_data") (v "1.3.2") (h "02g69glw57sz3vza1s5c9jp90bmrf3liynzmwx70bxd22vajx2q7") (r "1.66")))

(define-public crate-icu_datetime_data-1.3.3 (c (n "icu_datetime_data") (v "1.3.3") (h "17wmwggcjy53r07fxy3fdm91j4pln8ixyv6w1413jp4979xx6j6p") (r "1.66")))

(define-public crate-icu_datetime_data-1.3.4 (c (n "icu_datetime_data") (v "1.3.4") (h "1x2miw42p5i51x6k45a4l80yiiia0rfahhzlxfpypzw0v4j25n6f") (r "1.66")))

(define-public crate-icu_datetime_data-1.4.0 (c (n "icu_datetime_data") (v "1.4.0") (h "0gsvlzkcadssgwgf877rcpbvxm4ybifg0rkmf0q0ps2frdlwbaz6") (r "1.67")))

(define-public crate-icu_datetime_data-1.5.0 (c (n "icu_datetime_data") (v "1.5.0") (h "1slbz2lm00rswhh336shbysr7xknhr7zz7m3n2pvjs8jl3vyg9rb") (r "1.67")))

