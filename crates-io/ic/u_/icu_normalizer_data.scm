(define-module (crates-io ic u_ icu_normalizer_data) #:use-module (crates-io))

(define-public crate-icu_normalizer_data-1.3.0 (c (n "icu_normalizer_data") (v "1.3.0") (h "1n2i729lbh1sl5c3y5y5wwj2gaslishxv33h43jhpihbk2f7cf4d") (r "1.66")))

(define-public crate-icu_normalizer_data-1.3.2 (c (n "icu_normalizer_data") (v "1.3.2") (h "179n9c4h7blbwh2f64g835kyhy02jlaamadj5m2fdhk683vj53bg") (r "1.66")))

(define-public crate-icu_normalizer_data-1.4.0 (c (n "icu_normalizer_data") (v "1.4.0") (h "08l59kppc00mqv88yljr9hlfbd72a3wn06xh1hrrlshfm0c6j0i2") (r "1.67")))

(define-public crate-icu_normalizer_data-1.4.1 (c (n "icu_normalizer_data") (v "1.4.1") (h "1aq7nv8fk3qjk7s7xmp13i1m7hj87agizbydk6cy376zq3n4yx73") (r "1.67")))

(define-public crate-icu_normalizer_data-1.5.0 (c (n "icu_normalizer_data") (v "1.5.0") (h "05lmk0zf0q7nzjnj5kbmsigj3qgr0rwicnn5pqi9n7krmbvzpjpq") (r "1.67")))

