(define-module (crates-io ic u_ icu_decimal_data) #:use-module (crates-io))

(define-public crate-icu_decimal_data-1.3.0 (c (n "icu_decimal_data") (v "1.3.0") (h "1l3jhksxy8d6m71lnvy6g30yqc634qvhn6a7wfbafw92ffcszdqv") (r "1.66")))

(define-public crate-icu_decimal_data-1.3.2 (c (n "icu_decimal_data") (v "1.3.2") (h "0xsfcpq77w1nrz8vz7i48639avvpxipzzgqhqvw52ccm50w4n1iw") (r "1.66")))

(define-public crate-icu_decimal_data-1.3.3 (c (n "icu_decimal_data") (v "1.3.3") (h "04pir8a6b9gzs8lm8glqzsblvc2xmnz6k1b4xdyz82k019m0r6rp") (r "1.66")))

(define-public crate-icu_decimal_data-1.3.4 (c (n "icu_decimal_data") (v "1.3.4") (h "18i6dpsch3j9zhjcaj3an8z6xkkyz9hy1mgc0i4khx3bnli6q490") (r "1.66")))

(define-public crate-icu_decimal_data-1.4.0 (c (n "icu_decimal_data") (v "1.4.0") (h "0v1jf4mqf97f4iva51hxvw4xn4ljbhww3pih1z3rgdhnhdaf6bfz") (r "1.67")))

(define-public crate-icu_decimal_data-1.5.0 (c (n "icu_decimal_data") (v "1.5.0") (h "08qmnjabq5shmmsnwclm4b4gx1cchvy2b6cr9xjgbiki82clqhld") (r "1.67")))

