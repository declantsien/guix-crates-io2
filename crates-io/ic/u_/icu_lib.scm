(define-module (crates-io ic u_ icu_lib) #:use-module (crates-io))

(define-public crate-icu_lib-0.1.0 (c (n "icu_lib") (v "0.1.0") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "1v3nkv32in30zlvqh9gm52r5w34clcbhzn3chca282r1q6v4zpfc")))

(define-public crate-icu_lib-0.1.1 (c (n "icu_lib") (v "0.1.1") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "190lwhs987izcxnrpvl272h14h9gkx4j86m7y1fsgqc2v2j1m46g")))

(define-public crate-icu_lib-0.1.2 (c (n "icu_lib") (v "0.1.2") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0swaphygfwr1lh9k1b93wvmkibvrgkln38saskd8srpr5avridgl")))

(define-public crate-icu_lib-0.1.3 (c (n "icu_lib") (v "0.1.3") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1wwjhanfbkd8d41p18b3dl7r2iyvhnblbrmhavsqh9wqn0hkv1ls")))

(define-public crate-icu_lib-0.1.4 (c (n "icu_lib") (v "0.1.4") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0ggmshwq8x0cgk1ywkv6f4icgqw05iqh5pzidnh8h5r9ir1xbxgb")))

(define-public crate-icu_lib-0.1.5 (c (n "icu_lib") (v "0.1.5") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1r5ibd9w5ppmnlp27rjnl5aab9a6d2b5g52lalx72jd5cqkpnsgq")))

(define-public crate-icu_lib-0.1.6 (c (n "icu_lib") (v "0.1.6") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hiy03dpm62qgj6x572b23qnifvy84whzqg3d11jx4jrb8fyy2b8")))

(define-public crate-icu_lib-0.1.7 (c (n "icu_lib") (v "0.1.7") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0lcqrqpih0anh7nq4f51dqff4dgn20wsccs5igrbq3ry5qz8zipr")))

(define-public crate-icu_lib-0.1.8 (c (n "icu_lib") (v "0.1.8") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hhm2vfva9zr1qkm9ss1zsv9zz1vk3s3bmfjkfmqhlhv2ggyrzjn")))

(define-public crate-icu_lib-0.1.9 (c (n "icu_lib") (v "0.1.9") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1gyafpq7z59gvgjbcf3i1d31w3nqh2q8p0qdrm3hkqjvk8l4f2ld")))

(define-public crate-icu_lib-0.1.10 (c (n "icu_lib") (v "0.1.10") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "04ajk9d9vg8443iyf1pbkqzf0916cx88hyzhyzk9bg7r179qgbmx")))

(define-public crate-icu_lib-0.1.11 (c (n "icu_lib") (v "0.1.11") (d (list (d (n "color_quant") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "1fzk3s3d3nv7aacpyihll9qh13rdxa929nwib5sf48jyvgb6bblq")))

