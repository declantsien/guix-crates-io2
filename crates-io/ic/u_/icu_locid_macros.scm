(define-module (crates-io ic u_ icu_locid_macros) #:use-module (crates-io))

(define-public crate-icu_locid_macros-0.1.0 (c (n "icu_locid_macros") (v "0.1.0") (d (list (d (n "icu_locid") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.4") (d #t) (k 0)))) (h "1vm91af2395fgqxli1a7ng85a5dcyshmdk1s2gr5yrn1s48d3lii") (y #t)))

(define-public crate-icu_locid_macros-0.2.0 (c (n "icu_locid_macros") (v "0.2.0") (d (list (d (n "icu_locid") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "tinystr") (r "^0.4") (d #t) (k 0)))) (h "049xm03ws9qzgj0si4m5pq0ljywcq7qnkfbhqwwa1xv0x6wsikxk") (y #t)))

(define-public crate-icu_locid_macros-0.3.0 (c (n "icu_locid_macros") (v "0.3.0") (d (list (d (n "icu_locid") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "tinystr") (r "^0.4.10") (f (quote ("alloc"))) (k 0)))) (h "0wyrxkrf56j105fgslz25wglk0d6p1x4jf1has2yws6v2xlfxnss") (y #t)))

(define-public crate-icu_locid_macros-0.4.0 (c (n "icu_locid_macros") (v "0.4.0") (d (list (d (n "icu_locid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "tinystr") (r "^0.4.10") (f (quote ("alloc"))) (k 0)))) (h "147331ijvn8mqvjfiy9hfgy0km92hxhkhfgmllq802m0d5rwjjbv") (y #t)))

(define-public crate-icu_locid_macros-0.5.0 (c (n "icu_locid_macros") (v "0.5.0") (d (list (d (n "icu_locid") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "tinystr") (r "^0.4.10") (f (quote ("alloc"))) (k 0)))) (h "1qbaldb3xz6xzyjqdzhn7rx02n1bnnnyy1sb56h98pgkbmiy6h0c") (y #t)))

