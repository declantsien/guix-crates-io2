(define-module (crates-io ic u_ icu_plurals_data) #:use-module (crates-io))

(define-public crate-icu_plurals_data-1.3.0 (c (n "icu_plurals_data") (v "1.3.0") (h "0gvcmm7bvrknw6d9gkva0081wfa4ikxaa4dbjdaqnqgwcribcz2q") (r "1.66")))

(define-public crate-icu_plurals_data-1.3.2 (c (n "icu_plurals_data") (v "1.3.2") (h "08bax6dmsz8k6b3zrsxc88hcblw62fc9apg0ljm7m694a8hm5igx") (r "1.66")))

(define-public crate-icu_plurals_data-1.4.0 (c (n "icu_plurals_data") (v "1.4.0") (h "06m4mm925fc99cmi8vailw1lxzhdav9w65lkmqnyv27rybqxbb63") (r "1.67")))

(define-public crate-icu_plurals_data-1.5.0 (c (n "icu_plurals_data") (v "1.5.0") (h "027jy1p7zhyr0xj1bsbl3m1sfivj4ai9180li21lap91bdvqygly") (r "1.67")))

