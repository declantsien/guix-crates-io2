(define-module (crates-io ic u_ icu_collator_data) #:use-module (crates-io))

(define-public crate-icu_collator_data-1.3.0 (c (n "icu_collator_data") (v "1.3.0") (h "0jxgbnbjhkw4dhy1vwrrdxl7b1dayx27852i25rgsdsaiyn95g7q") (r "1.66")))

(define-public crate-icu_collator_data-1.3.2 (c (n "icu_collator_data") (v "1.3.2") (h "1a0imyvc6cj5hwmh8gngrzbmlw8hcgzn1ydmv0sh9n48if2adh0y") (r "1.66")))

(define-public crate-icu_collator_data-1.3.3 (c (n "icu_collator_data") (v "1.3.3") (h "09ii0hf2wkmap5j4ag2wjimw75gg46y3fvq20x3ssw75bjz9mgjd") (r "1.66")))

(define-public crate-icu_collator_data-1.4.0 (c (n "icu_collator_data") (v "1.4.0") (h "16997rcwjk4g3h8p7dby2vcn5rmdhgm6y6gk85lh1bdrqlb40xrr") (r "1.67")))

(define-public crate-icu_collator_data-1.5.0 (c (n "icu_collator_data") (v "1.5.0") (h "1raabsflgar4j9n7d3x713s2r2ka7rx85rncd5i7sjrn863ziqwf") (r "1.67")))

