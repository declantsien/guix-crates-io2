(define-module (crates-io ic u_ icu_char16trie) #:use-module (crates-io))

(define-public crate-icu_char16trie-0.1.0 (c (n "icu_char16trie") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "zerovec") (r "^0.7") (f (quote ("yoke"))) (d #t) (k 0)))) (h "0z6dnp9qp17npghkj1cp4lw32ivpinbrn860imw8i5c0384x7amw") (f (quote (("serde_serialize" "serde" "zerovec/serde_serialize") ("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "zerovec/serde"))))))

