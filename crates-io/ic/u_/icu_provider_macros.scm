(define-module (crates-io ic u_ icu_provider_macros) #:use-module (crates-io))

(define-public crate-icu_provider_macros-0.3.0 (c (n "icu_provider_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "11nyjwy7rg4mf2xcqsfnilc9xiw0v417lfabylcfghxyk4wbvshk")))

(define-public crate-icu_provider_macros-0.4.0 (c (n "icu_provider_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0s40j8qzm8j459v0myccph0m18y8bsfj6lyr70kbbqvn736qhf9l")))

(define-public crate-icu_provider_macros-0.5.0 (c (n "icu_provider_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "094mnxpsfzx10758v7bqmb091mpdm2549lf34zhf76vd40zrl6pa")))

(define-public crate-icu_provider_macros-0.6.0 (c (n "icu_provider_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0p77w0f6n21yr18yxynv1gbjwf37cicl4qgiy8b19k1d945spqvi")))

(define-public crate-icu_provider_macros-1.0.0-beta1 (c (n "icu_provider_macros") (v "1.0.0-beta1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1sz460i257ljcbkzmndvblvsarmk8fs4nwiwhyvhrnblgvh1g3kk")))

(define-public crate-icu_provider_macros-1.0.0 (c (n "icu_provider_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "01nnsvlrf9d2swmzjqsjwbh6mznb3884g9ld56sg10fgcmdnzkrq")))

(define-public crate-icu_provider_macros-1.1.0 (c (n "icu_provider_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmrjjflwqi5rzg401q9ry9l61gzajg9wy8fhhl4rz1g9j20gnwx")))

(define-public crate-icu_provider_macros-1.2.0 (c (n "icu_provider_macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "043y9823gww3bijmrja80xg79ykq3c8831lz3pzkxs91jj5p52yx")))

(define-public crate-icu_provider_macros-1.3.0 (c (n "icu_provider_macros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1x0drniqwmig5nhc44d28qk02z0kr6sal7952qawyz0i1fpyi6ya") (r "1.66")))

(define-public crate-icu_provider_macros-1.3.2 (c (n "icu_provider_macros") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "16dwjwph1l606c1lipxymndcbdbc2h7vyjc80nkjrqygvy72aq10") (r "1.66")))

(define-public crate-icu_provider_macros-1.4.0 (c (n "icu_provider_macros") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "06ng7n06q8dsc1na88wgxj40rjh0wscmi70imwvq67jmc8xdvayj") (r "1.67")))

(define-public crate-icu_provider_macros-1.5.0 (c (n "icu_provider_macros") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.61") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)))) (h "1mjs0w7fcm2lcqmbakhninzrjwqs485lkps4hz0cv3k36y9rxj0y") (r "1.67")))

