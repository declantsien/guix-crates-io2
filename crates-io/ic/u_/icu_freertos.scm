(define-module (crates-io ic u_ icu_freertos) #:use-module (crates-io))

(define-public crate-icu_freertos-0.6.0 (c (n "icu_freertos") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "^0.6") (k 0)))) (h "0m0r1cycasjdw982msqfc2vbsfh36kz5l2lzhwkki6zg0pcv1bcy") (f (quote (("wearos"))))))

(define-public crate-icu_freertos-1.0.0 (c (n "icu_freertos") (v "1.0.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "^1.0.0") (k 0)))) (h "0cj2qdhqrwqm1grfhp46lclg2f486bqja6z0l5v26m7kn74370hl") (f (quote (("wearos"))))))

(define-public crate-icu_freertos-1.1.0 (c (n "icu_freertos") (v "1.1.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "^1.1.0") (k 0)))) (h "0pabmpvvgz2xk0fzdmi4jhf3lqk949vc8jx2zjlx2dhvx34zggg9") (f (quote (("wearos"))))))

(define-public crate-icu_freertos-1.2.0 (c (n "icu_freertos") (v "1.2.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "^1.2.0") (k 0)))) (h "0qc6vpqiqnrlzq6drmljwy5xgcdi75ihbikkhwyc5ng2c8gx293i") (f (quote (("wearos") ("default" "icu_capi/default_components"))))))

(define-public crate-icu_freertos-1.2.1 (c (n "icu_freertos") (v "1.2.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "^1.2.0") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1lf3by6fzq3ckkazizcvwa8xhxjc59hsrq6mqrlzwanjxzcyy8nk") (f (quote (("wearos") ("default" "icu_capi/default_components"))))))

(define-public crate-icu_freertos-1.3.0 (c (n "icu_freertos") (v "1.3.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "~1.3.0") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1jcsbgry43rvj02z9iqm0wzayxj7ysisxirvqylh6kqdkjf63hpq") (f (quote (("wearos") ("default" "icu_capi/default_components")))) (r "1.66")))

(define-public crate-icu_freertos-1.3.2 (c (n "icu_freertos") (v "1.3.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "~1.3.2") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0nsdxm8y7jw1b9q42qhw0c9b2zqy7w1cr5lypz3vjljwf4z8i5s3") (f (quote (("wearos") ("default" "icu_capi/default_components")))) (r "1.66")))

(define-public crate-icu_freertos-1.4.0 (c (n "icu_freertos") (v "1.4.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "~1.4.0") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1izvn65p0ym9qjhims130pbapg1c187s3x8nbhxqb2y3yhv60ssp") (f (quote (("wearos") ("default" "icu_capi/default_components")))) (r "1.67")))

(define-public crate-icu_freertos-1.5.0 (c (n "icu_freertos") (v "1.5.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "freertos-rust") (r "^0.1.2") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "icu_capi") (r "~1.5.0") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0vi1wbzxcz508w7vbs9hiijihaip5igxidgpx018x1d7ga9fdymf") (f (quote (("wearos") ("default" "icu_capi/default_components")))) (r "1.67")))

