(define-module (crates-io ic u_ icu_displaynames_data) #:use-module (crates-io))

(define-public crate-icu_displaynames_data-1.3.0 (c (n "icu_displaynames_data") (v "1.3.0") (h "1x0k2zxv5nldkm2ssh9pf16y33xr543w91l3kfpdl6cajw6fcma6") (r "1.66")))

(define-public crate-icu_displaynames_data-1.3.2 (c (n "icu_displaynames_data") (v "1.3.2") (h "086hll3p7ikwc4ak2a1r31bgc1vpq09zps376qw807kz89ngbyb0") (r "1.66")))

(define-public crate-icu_displaynames_data-1.3.3 (c (n "icu_displaynames_data") (v "1.3.3") (h "056rv58h22y7dwhlqy27bs7czhkhxaxjjripvjwj5xg4hh9xv14a") (r "1.66")))

(define-public crate-icu_displaynames_data-1.3.4 (c (n "icu_displaynames_data") (v "1.3.4") (h "01jay5j2n2947sg0592rfpif2b5s4a7vga8vyvz5bcj27sl0n312") (r "1.66")))

(define-public crate-icu_displaynames_data-1.4.0 (c (n "icu_displaynames_data") (v "1.4.0") (h "1xihpf47x50zns15qfxp2smxdffzsr74xfzhkhsxkrp54fvych5g") (r "1.67")))

