(define-module (crates-io ic u_ icu_segmenter_lstm) #:use-module (crates-io))

(define-public crate-icu_segmenter_lstm-0.1.0 (c (n "icu_segmenter_lstm") (v "0.1.0") (d (list (d (n "icu_provider") (r "^0.6") (f (quote ("macros"))) (d #t) (k 0)) (d (n "litemap") (r "^0.4.0") (f (quote ("serde_serialize"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)) (d (n "yoke") (r "^0.5.0") (f (quote ("serde" "derive"))) (d #t) (k 0)))) (h "1lycgkg5jcz3cip39306wa7z9crygmnk2cwpw4l73v3nbdzjsifi") (f (quote (("default") ("bench")))) (y #t)))

(define-public crate-icu_segmenter_lstm-1.0.0 (c (n "icu_segmenter_lstm") (v "1.0.0") (h "07wp17la32c8r4rvi2v9p8dcw597p686ssv8i86khzcxcxim69yq")))

