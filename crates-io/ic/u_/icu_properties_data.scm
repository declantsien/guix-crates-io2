(define-module (crates-io ic u_ icu_properties_data) #:use-module (crates-io))

(define-public crate-icu_properties_data-1.3.0 (c (n "icu_properties_data") (v "1.3.0") (h "1hi300cmlxknj6yls3fh4p0zs2549d28q1mz5a6v05cx30s42zsf") (r "1.66")))

(define-public crate-icu_properties_data-1.3.2 (c (n "icu_properties_data") (v "1.3.2") (h "0n8vyfhgm3f8ryvh979hp6crks3ybjg3cd04b16yjiw3gavb72vw") (r "1.66")))

(define-public crate-icu_properties_data-1.3.3 (c (n "icu_properties_data") (v "1.3.3") (h "0hd4vr1p35lvjw2fb8bybzx3j39jkqrfq5r9h4mf7gdrp9l97cmx") (r "1.66")))

(define-public crate-icu_properties_data-1.3.4 (c (n "icu_properties_data") (v "1.3.4") (h "0q2a42i8lwpin9naipcmc2nxir0j40m635glbswmxx4qh147nl4q") (r "1.66")))

(define-public crate-icu_properties_data-1.4.0 (c (n "icu_properties_data") (v "1.4.0") (h "16cxabrs24sps3003420cidwsfxiq7wi8j0hdimv0cj57076ra7n") (r "1.67")))

(define-public crate-icu_properties_data-1.4.1 (c (n "icu_properties_data") (v "1.4.1") (h "05aql7jwck60zsqlfdvai08c3bq7pk8iv6zf427zzm2xxr8qn2p7") (r "1.67")))

(define-public crate-icu_properties_data-1.5.0 (c (n "icu_properties_data") (v "1.5.0") (h "0scms7pd5a7yxx9hfl167f5qdf44as6r3bd8myhlngnxqgxyza37") (r "1.67")))

