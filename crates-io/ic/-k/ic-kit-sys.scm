(define-module (crates-io ic -k ic-kit-sys) #:use-module (crates-io))

(define-public crate-ic-kit-sys-0.1.0 (c (n "ic-kit-sys") (v "0.1.0") (h "0wqf5nn29p89j23fw2w6azklc0clbx8kyyxag9zch3mxh7pj15km")))

(define-public crate-ic-kit-sys-0.1.1 (c (n "ic-kit-sys") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "10v0hxyil88m6k0zfa30l4n9z9y5s4c5bhp786gz575i45wyx0xl") (f (quote (("runtime" "tokio" "futures") ("default" "runtime"))))))

(define-public crate-ic-kit-sys-0.1.2 (c (n "ic-kit-sys") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "142qxmrhihk9qc5swyb8bp7r6ahp470zrhmd8h5bh2z68pjxxz99")))

(define-public crate-ic-kit-sys-0.1.3 (c (n "ic-kit-sys") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1wfbk722s0d0w8njc98wk1yif7b5mdfjgv2hhaxi4xy3rqsi0km1")))

