(define-module (crates-io ic -k ic-kit-macros) #:use-module (crates-io))

(define-public crate-ic-kit-macros-0.1.0 (c (n "ic-kit-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07acncsd6wawqjww88q7bym6826x31c1i8yzdcvqnc954cg896bf")))

(define-public crate-ic-kit-macros-0.1.0-alpha.0 (c (n "ic-kit-macros") (v "0.1.0-alpha.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dmfmdna3x3c8rwnibgwiwzaa3h8fjsz7ah1srf7dbgszjqk1632")))

(define-public crate-ic-kit-macros-0.1.0-alpha.2 (c (n "ic-kit-macros") (v "0.1.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ably6n8ildzhzgqpqdq8zbm5sj077fd1nck62a9ja4grr7n8i4n")))

(define-public crate-ic-kit-macros-0.1.1-alpha.0 (c (n "ic-kit-macros") (v "0.1.1-alpha.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mdnh5vm1lf09ycgf8as1hpx2v277y20har3anj6nararjk015ls")))

