(define-module (crates-io ic er iceray) #:use-module (crates-io))

(define-public crate-iceray-0.0.9 (c (n "iceray") (v "0.0.9") (d (list (d (n "iceoryx-rs") (r "^0.0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 0)))) (h "12qan4331wccgdw6zwprpzgvdpnshrz4yp84wjipyywcs50ni9b5")))

(define-public crate-iceray-0.1.0 (c (n "iceray") (v "0.1.0") (d (list (d (n "iceoryx-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 0)))) (h "16vpsflm9vpnzlm7rx77q4fb48p5z8r2k3kp0szdqc15zg24242k")))

