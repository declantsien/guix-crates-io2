(define-module (crates-io ic eb iceberg_catalog_rest_rdbms_client) #:use-module (crates-io))

(define-public crate-iceberg_catalog_rest_rdbms_client-0.0.1 (c (n "iceberg_catalog_rest_rdbms_client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "10kls3zffaskdck9j6c98xban9vbpnrb055ydlzj4lwp3g865rfr") (y #t)))

