(define-module (crates-io ic eb iceberg_catalog_rest_client) #:use-module (crates-io))

(define-public crate-iceberg_catalog_rest_client-0.0.1 (c (n "iceberg_catalog_rest_client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1x7n9m4fkfwnp2y8ljy99z6gbh2dqk721znnigscpa8841jwkmxm")))

