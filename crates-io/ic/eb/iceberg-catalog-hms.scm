(define-module (crates-io ic eb iceberg-catalog-hms) #:use-module (crates-io))

(define-public crate-iceberg-catalog-hms-0.2.0 (c (n "iceberg-catalog-hms") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "hive_metastore") (r "^0.0.2") (d #t) (k 0)) (d (n "iceberg") (r "^0.2.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)) (d (n "volo-thrift") (r "^0.9.2") (d #t) (k 0)))) (h "01v0gchzmlvbsc5n21yr4i3663w950frqp0rf67gil1cykc3ys0g")))

