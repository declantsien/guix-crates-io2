(define-module (crates-io ic eb iceberg-rs) #:use-module (crates-io))

(define-public crate-iceberg-rs-0.1.0 (c (n "iceberg-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1xj73sjr0256jhwbi56zcnbmn0lx4f28fj20j61kmlbgdz6q8l0k")))

