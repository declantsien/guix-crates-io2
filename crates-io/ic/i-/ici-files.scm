(define-module (crates-io ic i- ici-files) #:use-module (crates-io))

(define-public crate-ici-files-0.1.0 (c (n "ici-files") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lkbg5lnb8qfn1dzckmjg3pc7fg9s16dd5qqs8wb9firgmh8w54d")))

(define-public crate-ici-files-0.1.1 (c (n "ici-files") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pl7xanwlj84aslcjryk5p47l60chnz105lfgflblh6if5dsmprs")))

(define-public crate-ici-files-0.1.2 (c (n "ici-files") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18ks5z7g9ma3ff6ff0zzklv734wlycirm7fvlgnfpr4yb90ljb0m")))

(define-public crate-ici-files-0.1.3 (c (n "ici-files") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kskf3jkma33p2lpkmfns0l72l59h17pb0091939f27hwlhv1gyy")))

(define-public crate-ici-files-0.1.4 (c (n "ici-files") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15wifp7yac9yhjpfip7w96xn4k4bwbwmzkrjxx9kblydmc8fl4hl")))

(define-public crate-ici-files-0.1.5 (c (n "ici-files") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0swjfzggh51lqvk85vvprph09anah0z38mbjzcx0ysa0iphw23fk")))

(define-public crate-ici-files-0.1.6 (c (n "ici-files") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0fz5v1kh09wxqjkzc3605pmlqd0ylc074xin4nbasyk6l3fq926g")))

(define-public crate-ici-files-0.1.7 (c (n "ici-files") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "02w704qc8750bnky64wmm9yjqvdxb62pbs7sq8jgyxhkhcb9k785")))

(define-public crate-ici-files-0.2.0 (c (n "ici-files") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1f54mp1aggnpnlr7dy76926wngqlqgv8lpy7667rxy2574i1jwzf") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2.1 (c (n "ici-files") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1xyr9gh2lwk8xp9jsl6xgz6s8gvng0fzvmnsd01lp564gqvrxca7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2.2 (c (n "ici-files") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0hna098lkwa0hbms30mx12vwqghx0xrrbaz7abvjg5zig1dkjyy8") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2.3 (c (n "ici-files") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0zhyaa37r0mzr66nylmxmd5fpnlb5f45gsnkbkrfc1iqqmjj8kxv") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

