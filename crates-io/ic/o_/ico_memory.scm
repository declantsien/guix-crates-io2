(define-module (crates-io ic o_ ico_memory) #:use-module (crates-io))

(define-public crate-ico_memory-0.1.0 (c (n "ico_memory") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "03mm4kvgvc415kiq8ps24pc4g3jmxyy940g6vbx9d5bajyq04702")))

(define-public crate-ico_memory-0.1.1 (c (n "ico_memory") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "16hvgg98fpw1rmn3vn50sl1dhzcaixnvp8g0hybrgdp3rd75pz2g")))

(define-public crate-ico_memory-0.1.2 (c (n "ico_memory") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "066jr8rs5q2mwc3gx334f0qmvvl2p8znasj8kjqlbwgz3hmvmbp5") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.3 (c (n "ico_memory") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0abi3wbxp8jil1zzv9l0zis45aag1gxfh1a0ds7v6g51pk6q3y41") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.4 (c (n "ico_memory") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0cll4hj3f56x6b50410w73794f77h3d3dg692vmmns447qkay7j8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.5 (c (n "ico_memory") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0hz6k4kdmh167gaawb24mb70l1mah3yvnv7drm4w8h6rbb2c82kk") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.6 (c (n "ico_memory") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "08laz690gcgi0dk8b2032fgyih4pnz581qnr96cjjgl6fg28rlsp") (f (quote (("std"))))))

(define-public crate-ico_memory-0.1.7 (c (n "ico_memory") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "185sy3fcy8559alfr92zmc6skfkb0mcih3di5rr2xi061iwmjlv1") (f (quote (("std"))))))

(define-public crate-ico_memory-0.1.8 (c (n "ico_memory") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "02rslkr6ffm475lnrdbf3i5zykqnqdxh0ijgqj6yz8674hr8aar8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.9 (c (n "ico_memory") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "1xqkfh8c175yy0mqvnjjvn4dcib4crg22853q5m754s9bydnsb0q") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.10 (c (n "ico_memory") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0ajdhriy0dy1p53srzpvhhkw328zbw5906ldzf6rg9s2rsz192i9") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.12 (c (n "ico_memory") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0xbb4gw86rlsxh81gngwz08rvxyrx1ywcdxdm5sk8amx5agcjajc") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.13 (c (n "ico_memory") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0gq05rf8mfbjrah8zbm8xxgmpbmbvfckdzs15mv8326r7ipv5sy9") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.14 (c (n "ico_memory") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0bsk8f51v82329yb67fkjq7d9bgb8hpwzh5v0r0hp0s40ykfppic") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.15 (c (n "ico_memory") (v "0.1.15") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0yg0ikbpyiyksj2a2jv6zkp1pdzd9f13cvccn50zkvgfqfq5r1l6") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.16 (c (n "ico_memory") (v "0.1.16") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "1am0awrjwgmljlm5y5pldiy53i2c9p8ikx81wxb1s9hg4gzraw6k") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.17 (c (n "ico_memory") (v "0.1.17") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0l7iysvib5lmgbd6583j5zdj0hh0smp3ymkh6z0gl9qk3jrjkwh7") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.18 (c (n "ico_memory") (v "0.1.18") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0zaj2aj8l7pl6v1l6s5l739ip4gr715ixfirifz1sxpbwy1anvm0") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.19 (c (n "ico_memory") (v "0.1.19") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0mmg8qk3aihcy25k32vbwk1h6dkkrb1g7cpnwbx74q639bgh4yw8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.20 (c (n "ico_memory") (v "0.1.20") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "1afizgfgxi5wx74ar1z0hzqlgqidinz6a1frj3w3vvz7bix3njh5") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.21 (c (n "ico_memory") (v "0.1.21") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "1brdybin9b4b9n22zmm7l1zb8wps27xyz0gdkj4r8079fij0z4kh") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.22 (c (n "ico_memory") (v "0.1.22") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "075952g7244fb18qrbvmf2pka6sz7qzrfjx0s3rzfk9gj8znx5d6") (f (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1.23 (c (n "ico_memory") (v "0.1.23") (d (list (d (n "libc") (r "^0.2.61") (k 0)))) (h "0pxbqh5qj68z5sknvl6bja53smyhcnm3dj8zlmli7ncdba17vdq5") (f (quote (("std") ("default" "std"))))))

