(define-module (crates-io ic o_ ico_math) #:use-module (crates-io))

(define-public crate-ico_math-0.1.0 (c (n "ico_math") (v "0.1.0") (h "0db1s5p7c9yzr87c3glbzhk0fbkwkqq9ww10q9iih65p5cwmpg4h")))

(define-public crate-ico_math-0.1.1 (c (n "ico_math") (v "0.1.1") (h "07fcf9addkf0iz1ci4bp4jhidxwqfs6pbib0r1k1py1zl5khxwaj")))

(define-public crate-ico_math-0.1.2 (c (n "ico_math") (v "0.1.2") (h "1dqriz8mnvlqhdrbwahcccwc9mm0ckwrc4v0r0w8400p4595b2w8") (f (quote (("use-std"))))))

(define-public crate-ico_math-0.1.3 (c (n "ico_math") (v "0.1.3") (h "08m977vdyg7lcvdvxr8437x33rhmkaj85ypqvk0jcbgjvd3fr22z") (f (quote (("use-std"))))))

(define-public crate-ico_math-0.1.4 (c (n "ico_math") (v "0.1.4") (h "10nyzajqnlw296255bs1sw3iiik9vg4xhgmvn25xfwh6kx6f7lk4") (f (quote (("use-std") ("default" "use-std"))))))

(define-public crate-ico_math-0.1.5 (c (n "ico_math") (v "0.1.5") (h "083a9z8667ky1pw7nv12wd3aqcg65qy4rbg95yjmwpps1ygd3v79") (f (quote (("use-std") ("default" "use-std"))))))

(define-public crate-ico_math-0.1.6 (c (n "ico_math") (v "0.1.6") (h "07gvxzlyr4g5gfsir911by1n6npb33x4cbj5k4ilqfb8ji4c57xq") (f (quote (("use-std") ("default" "use-std"))))))

