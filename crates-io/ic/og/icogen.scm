(define-module (crates-io ic og icogen) #:use-module (crates-io))

(define-public crate-icogen-1.0.0 (c (n "icogen") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "cargo" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1cfs076xj1slab9v10ahrqmkn1pn8jb4k6swgs84j5xljn6r0hhy")))

(define-public crate-icogen-1.1.0 (c (n "icogen") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "cargo" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "resvg") (r "^0.23") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6") (d #t) (k 0)) (d (n "usvg") (r "^0.23") (d #t) (k 0)))) (h "06gbl42kv63b6nxh6j3rv84l8h93w7gwf9454c244255jpklki0r")))

(define-public crate-icogen-1.2.0 (c (n "icogen") (v "1.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "cargo" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "resvg") (r "^0.23") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6") (d #t) (k 0)) (d (n "usvg") (r "^0.23") (d #t) (k 0)))) (h "1k81cxk2g6nvmy1bl1ciqi543zgm1la8z2x62zbcl1jg4hym7l6v")))

