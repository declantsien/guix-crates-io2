(define-module (crates-io ma nc manchu-converter) #:use-module (crates-io))

(define-public crate-manchu-converter-0.1.0 (c (n "manchu-converter") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "18x6h92xrwv8lbk4hm41jg7zg0dar3mh4mkpi2rcsk7qpg7cy47r")))

(define-public crate-manchu-converter-0.1.1 (c (n "manchu-converter") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1lx7rkxv85yljqxjqsi8hrl5g72vnng1ycdfa387p79v7w5vfrad")))

(define-public crate-manchu-converter-0.2.0 (c (n "manchu-converter") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1l4c61ga7siphmn8nwkymb8mn7crzjcy9vyhw589rf916dvkm9qv")))

(define-public crate-manchu-converter-0.2.1 (c (n "manchu-converter") (v "0.2.1") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1yda6rrcps8bxkagz31ac1s8a60b680q56ybap802ipkd4z1qz2h")))

(define-public crate-manchu-converter-0.2.2 (c (n "manchu-converter") (v "0.2.2") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0dv179swarmgbdqr3vibv9l6rmnfmzqnd4vnd155z6bfjxvsm30m")))

(define-public crate-manchu-converter-0.2.3 (c (n "manchu-converter") (v "0.2.3") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1jg3hzp5pfc5ryiarp4hxmlwhyw63v3hfaw1nbjawqg1c6pbrllj")))

(define-public crate-manchu-converter-0.3.0 (c (n "manchu-converter") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1rnzrq22fp0rwnhyz79w58ma2fixrhgr50fal6k776zwlw88glhn")))

