(define-module (crates-io ma nc manchester-code) #:use-module (crates-io))

(define-public crate-manchester-code-0.1.0 (c (n "manchester-code") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0v815451lfamgnzgaskx3ff1pbz1mxq1f6a6k7mgljcfcp4cc7vn")))

(define-public crate-manchester-code-0.2.0 (c (n "manchester-code") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1zjls1ci18y3q4czigqr5s8nb0w82mrhv8da9mn58jhhfzwgd73i")))

