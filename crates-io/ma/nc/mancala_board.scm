(define-module (crates-io ma nc mancala_board) #:use-module (crates-io))

(define-public crate-mancala_board-0.1.0 (c (n "mancala_board") (v "0.1.0") (h "1fcn5b17czpbqvqqgj7z0fa675bv458zlh1nbp44n19bifqnakzg")))

(define-public crate-mancala_board-0.1.1 (c (n "mancala_board") (v "0.1.1") (h "13wh527ii5yvc5w3kkm4a20dwgjldjlhhn0ghm57d9wg9hy6al82")))

(define-public crate-mancala_board-0.1.2 (c (n "mancala_board") (v "0.1.2") (h "0362nmrv1m9jl3avm8w1apkc8sz4xhq74vgl37j4qynmwalx47gx")))

(define-public crate-mancala_board-0.1.3 (c (n "mancala_board") (v "0.1.3") (h "1nvvm0xi0b2pw39k963wwbijagbl33z3mnw37yiiwpa8vlpgbh7x")))

(define-public crate-mancala_board-0.1.4 (c (n "mancala_board") (v "0.1.4") (h "1gqjp5vg2j7x780zyj1lr92nzqkq4jmyvgivs99l20a251vc2b5b")))

(define-public crate-mancala_board-0.1.5 (c (n "mancala_board") (v "0.1.5") (h "1n6v08jdnakgis15vp99b5xp4ky94jczpg1igavp82jk4j6xzp0d")))

(define-public crate-mancala_board-0.1.6 (c (n "mancala_board") (v "0.1.6") (h "0aikaf3592ms0jwn8hr9b8ml2g64cngp6kajx3dy2wrxpj1zshxi")))

(define-public crate-mancala_board-0.1.7 (c (n "mancala_board") (v "0.1.7") (h "053ahkr6d887h3a5sxmw5v8f6fr4c5lcs9yn7mnj4s8vr6mbcfal")))

(define-public crate-mancala_board-0.1.8 (c (n "mancala_board") (v "0.1.8") (h "0mdsj60j2h6p21i2qrqphzbyaca46d0is1n7divvfpnwwza1ifsf")))

(define-public crate-mancala_board-0.1.9 (c (n "mancala_board") (v "0.1.9") (h "0aicgk7wvgnpdkg0l08gj7p7ml5kin3di6bbcyzfzbvcs94qq7jl")))

(define-public crate-mancala_board-0.1.10 (c (n "mancala_board") (v "0.1.10") (h "19ljmcxjp8y3dsn3p19h9lq27gsshcv0axhlyvl37ns932k9y8yz")))

(define-public crate-mancala_board-0.1.11 (c (n "mancala_board") (v "0.1.11") (h "0h42hw8s48z59hjyy0wfccfmips8gxrz1xq2z83n4bg0cl8kikqr")))

(define-public crate-mancala_board-0.1.12 (c (n "mancala_board") (v "0.1.12") (h "1dm387gsmr204vi579i6arx9i8ka2plm1p8ic2mrwjfswsv6zbs7")))

(define-public crate-mancala_board-0.1.13 (c (n "mancala_board") (v "0.1.13") (h "1rk70d19pwgcn83bws8yrxvlwpygfkychh47h9kl1as5rpgisxk3")))

(define-public crate-mancala_board-0.1.14 (c (n "mancala_board") (v "0.1.14") (h "00hadb6xnj4m8ghi1xpm4klyjikdynprsay5j04fqzcv10rs43mi")))

(define-public crate-mancala_board-0.1.15 (c (n "mancala_board") (v "0.1.15") (h "0fxjl0w5nh7dnq4kqzg0di3zalfgyi51qhy7rwwishb7zj6jbm0r")))

(define-public crate-mancala_board-0.1.16 (c (n "mancala_board") (v "0.1.16") (h "0mafl10c22fdyrmix4d11gj8vzl93ddpdnxvics45ykbr531vfar")))

(define-public crate-mancala_board-0.2.0 (c (n "mancala_board") (v "0.2.0") (h "1abv1r6bm0vwzprmdn9nc0n4wgr3rbdxlw4ghpbfb0xdqngm46l6")))

