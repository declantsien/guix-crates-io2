(define-module (crates-io ma st mastermind-rs) #:use-module (crates-io))

(define-public crate-mastermind-rs-0.2.5 (c (n "mastermind-rs") (v "0.2.5") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("pancurses-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0gb12kdyv650xqh1pfqz5j0i760738kr2r4gwcl2pxc5ffh3chrc")))

(define-public crate-mastermind-rs-0.2.6 (c (n "mastermind-rs") (v "0.2.6") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("pancurses-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0k8sgciavin39919zvbgh02j7is7lar41q3simrplfjfbrscypc6")))

(define-public crate-mastermind-rs-0.2.7 (c (n "mastermind-rs") (v "0.2.7") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("pancurses-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0r0f3ca21wwdj174dipb5miyknn7lzk19xwjsz49690lz79z5inz")))

(define-public crate-mastermind-rs-0.2.8 (c (n "mastermind-rs") (v "0.2.8") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0gapb21bw74q8xi4r0djw6qrsmhl2ixnqr2aqny89l197hsrjrqs")))

(define-public crate-mastermind-rs-0.2.9 (c (n "mastermind-rs") (v "0.2.9") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0rvk8dm47q5ym4fc8yc20fag4nashzaa49krfra0fb2f22n7kvjg")))

(define-public crate-mastermind-rs-0.2.10 (c (n "mastermind-rs") (v "0.2.10") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "1q75iqv9qnidkcjizh6i96j4q8jv4z15bxj9yb28vzk5xgpl9bzn")))

