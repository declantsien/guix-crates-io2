(define-module (crates-io ma st masterstat) #:use-module (crates-io))

(define-public crate-masterstat-0.1.0 (c (n "masterstat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "1z5x3rkp2xfzawa6krh488z0f7ldmxm96iwkdy8m2bb55pfrib1c")))

(define-public crate-masterstat-0.1.1 (c (n "masterstat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0kn1v11j15m0j35jlr5qq8bv0yfkaamblibd7ski6b8xbasd7pw8")))

(define-public crate-masterstat-0.1.2 (c (n "masterstat") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0a8fx0qg01qk1q49k9m4z33af33kifff3mh677yr7vrpp7vr3pmz")))

(define-public crate-masterstat-0.1.3 (c (n "masterstat") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tinyudp") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "11ik1qzkpxmvhcdgwd08i0dy5v7c5315lpwfrbynmglzzdyc8dri")))

