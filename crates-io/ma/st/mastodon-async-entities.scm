(define-module (crates-io ma st mastodon-async-entities) #:use-module (crates-io))

(define-public crate-mastodon-async-entities-1.1.0 (c (n "mastodon-async-entities") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "serde" "std" "kv_unstable_serde" "kv_unstable_std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing" "serde" "formatting"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "09ym11r2gbw10492dqd2pnkcc24xrcn9il8y0amkc0f3n0kipkrr")))

