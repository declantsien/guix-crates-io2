(define-module (crates-io ma st masterpg) #:use-module (crates-io))

(define-public crate-masterpg-1.0.0 (c (n "masterpg") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (f (quote ("file_locks"))) (d #t) (k 0)) (d (n "string_io_and_mock") (r "^1.0.1") (d #t) (k 0)) (d (n "tree_by_path") (r "^1.0.0") (d #t) (k 0)))) (h "0vix6a9p1lhxjgrwqf222769cab5dhfyw6jwfx3k1prybdac6wbh")))

