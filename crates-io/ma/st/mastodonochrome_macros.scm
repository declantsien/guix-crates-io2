(define-module (crates-io ma st mastodonochrome_macros) #:use-module (crates-io))

(define-public crate-mastodonochrome_macros-0.1.1 (c (n "mastodonochrome_macros") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0ci8dg9fyxwgiwvn17wm1b9hvi1q6xyg9i7zq5995af8rcxw9n28")))

