(define-module (crates-io ma st mastodon) #:use-module (crates-io))

(define-public crate-mastodon-0.1.0 (c (n "mastodon") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0f9n4lj76hcqbccg6cqxr5dywb80389n4ggj9qyn4q0gswmhaj1z")))

