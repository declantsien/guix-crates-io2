(define-module (crates-io ma st mast-build) #:use-module (crates-io))

(define-public crate-mast-build-0.1.0 (c (n "mast-build") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "quit") (r "^1.1.4") (d #t) (k 0)) (d (n "strict-yaml-rust") (r "^0.1") (d #t) (k 0)))) (h "1nvmcazlw795p103x8jjh0cbb65gmm8x5cbnhsm1ivlvqqqd3x6c")))

(define-public crate-mast-build-0.1.1 (c (n "mast-build") (v "0.1.1") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "quit") (r "^1.1.4") (d #t) (k 0)) (d (n "strict-yaml-rust") (r "^0.1") (d #t) (k 0)))) (h "0srgdyshfzcddfwaa67f45gg1l7r6zxq9n7wrqgzaijyh5dq58cc")))

(define-public crate-mast-build-0.2.1 (c (n "mast-build") (v "0.2.1") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "quit") (r "^1.1.4") (d #t) (k 0)) (d (n "strict-yaml-rust") (r "^0.1") (d #t) (k 0)))) (h "01a87f5qskpj3z2sr4smsn1ivijk6602qnnnmj6r2qlmcak3dwxd")))

