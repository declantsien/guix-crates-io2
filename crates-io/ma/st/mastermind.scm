(define-module (crates-io ma st mastermind) #:use-module (crates-io))

(define-public crate-mastermind-0.1.0 (c (n "mastermind") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1xgv48h578gnjjx3ainsqncni66awhmsqdwvcxy1yp520c6a7m90")))

(define-public crate-mastermind-0.1.2 (c (n "mastermind") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "10c88z1agqmw8rab56x923zb09rhy8p6429cf2rwjh7p7f8rxghq")))

(define-public crate-mastermind-0.1.3 (c (n "mastermind") (v "0.1.3") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "12raxjpb5snifvw06g03qh06r0amiy98zjx2x1m6jg5wdays8gwy")))

(define-public crate-mastermind-0.1.4 (c (n "mastermind") (v "0.1.4") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0k9ccq3gs7ffkcdfnw23xwcq5hd3z8g9lwnz3pa10i0yg7pm0s00")))

(define-public crate-mastermind-0.1.5 (c (n "mastermind") (v "0.1.5") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0awzkvh4xvqvi5pryibbn67d5x46acjkc58gw1jnxhpg7h2pbfr4")))

