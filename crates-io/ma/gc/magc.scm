(define-module (crates-io ma gc magc) #:use-module (crates-io))

(define-public crate-magc-0.1.0 (c (n "magc") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strontium") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "061lnmbb1bps0j4g0ra6mf6j623kgdr3759aryamahbd7bphp1j6")))

(define-public crate-magc-0.1.1 (c (n "magc") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "strontium") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1picqdizfvcjg9gif3cqnxj8l4ldnp3mgzg1q138yfd8y7gj22jy")))

