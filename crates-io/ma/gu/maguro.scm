(define-module (crates-io ma gu maguro) #:use-module (crates-io))

(define-public crate-maguro-0.0.1 (c (n "maguro") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "fs"))) (d #t) (k 0)))) (h "13vsjj56q6148z844qmy1m68j1rfxzkwzyrah3aas4dgz0bf562g")))

