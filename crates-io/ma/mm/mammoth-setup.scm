(define-module (crates-io ma mm mammoth-setup) #:use-module (crates-io))

(define-public crate-mammoth-setup-0.0.0 (c (n "mammoth-setup") (v "0.0.0") (h "1n56xh77yhyrivxbhkbkpwn3jg8bzna5n3zv85b09fwax9169kjc")))

(define-public crate-mammoth-setup-0.0.1 (c (n "mammoth-setup") (v "0.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3") (d #t) (k 0)) (d (n "libloading") (r "~0.5") (d #t) (k 0)) (d (n "mammoth-macro") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "openssl") (r "~0.10") (d #t) (k 0)) (d (n "regex") (r "~1.1") (d #t) (k 0)) (d (n "semver") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "toml") (r "~0.5") (d #t) (k 0)))) (h "1w9vvqvhh4ddljfz0j0gq22qj18h9f8ynsjdm1kcwdiifyjx2clk") (f (quote (("mammoth_module" "mammoth-macro"))))))

