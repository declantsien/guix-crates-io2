(define-module (crates-io ma mm mammon) #:use-module (crates-io))

(define-public crate-mammon-0.0.1 (c (n "mammon") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.191") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0q8sv4s54gbi2im9z0gqvbwn9fnbadlxdl2pwzc2s5cgknkphgnx")))

