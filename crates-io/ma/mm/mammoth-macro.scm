(define-module (crates-io ma mm mammoth-macro) #:use-module (crates-io))

(define-public crate-mammoth-macro-0.0.1 (c (n "mammoth-macro") (v "0.0.1") (d (list (d (n "mammoth-setup") (r ">= 0.0.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1ajf1bx6j3j9krcfzx3fc8vpr1d7bwva1vsd6s5kdissrbj24z2b")))

