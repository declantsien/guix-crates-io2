(define-module (crates-io ma om maomi-tools) #:use-module (crates-io))

(define-public crate-maomi-tools-0.4.0 (c (n "maomi-tools") (v "0.4.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1h3s87kn94p1n1aafcblzs8jzim9sa0avfpp6p0dy54qaspm4y3b")))

