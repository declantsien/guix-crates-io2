(define-module (crates-io ma om maomi-dom-macro) #:use-module (crates-io))

(define-public crate-maomi-dom-macro-0.4.0 (c (n "maomi-dom-macro") (v "0.4.0") (d (list (d (n "maomi-skin") (r "=0.4.0") (d #t) (k 0)) (d (n "maomi-tools") (r "=0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)))) (h "06bfw0yjbrp944fm721j107cv1mjcjpvads5l9541qf8zd0r8m8m")))

