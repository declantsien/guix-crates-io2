(define-module (crates-io ma om maomi-skin) #:use-module (crates-io))

(define-public crate-maomi-skin-0.3.0 (c (n "maomi-skin") (v "0.3.0") (d (list (d (n "cssparser") (r "^0.27") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c5sk5hdd11faws5dsyzgfhllv2cc2c0ni2p78b35a8i7rmkamyz")))

(define-public crate-maomi-skin-0.4.0 (c (n "maomi-skin") (v "0.4.0") (d (list (d (n "maomi-tools") (r "=0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksfjlsl1mndh8cwysmkhn8k6vrkfh1pm2jyzyw36frspmjm90jh")))

