(define-module (crates-io ma om maomi-macro) #:use-module (crates-io))

(define-public crate-maomi-macro-0.1.0 (c (n "maomi-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11zrwixwibd467b732jz0h7vviinghms2az7wm3pm8r6kdhi31z3")))

(define-public crate-maomi-macro-0.3.0 (c (n "maomi-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12az7cldc1jdd791wikw62gjzw2bc1dqmmiihc5srxwvm6k1rw0g")))

(define-public crate-maomi-macro-0.4.0 (c (n "maomi-macro") (v "0.4.0") (d (list (d (n "maomi-skin") (r "=0.4.0") (d #t) (k 0)) (d (n "maomi-tools") (r "=0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)))) (h "0jwb355hfsdvmlwp8jfhg8mys8191b5nvh7py9zdfzq7myfi5kcj")))

