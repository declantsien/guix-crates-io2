(define-module (crates-io ma om maomi-dom-template) #:use-module (crates-io))

(define-public crate-maomi-dom-template-0.4.0 (c (n "maomi-dom-template") (v "0.4.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "console_log") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maomi") (r "=0.4.0") (d #t) (k 0)) (d (n "maomi-dom") (r "=0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1r0mip3xmcm0xvdr5yyxh4xfmh3ii25i97209k3iin63l281mxk6")))

