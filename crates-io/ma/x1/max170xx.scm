(define-module (crates-io ma x1 max170xx) #:use-module (crates-io))

(define-public crate-max170xx-0.1.0 (c (n "max170xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0m34q2pl7s0rv6yn9kk58ivb89n6nc820q5zd3xpsaxh0rrp2pgr")))

(define-public crate-max170xx-0.1.1 (c (n "max170xx") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "10azgzr6rff3ikfi4sb7sz8kw4lmpg0wp0hhybbg81pwg599l970")))

(define-public crate-max170xx-1.0.0 (c (n "max170xx") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "0rlkm0yniwlq01hilyn5p0vyjqrwnbh585kah2rb4nkqygf1aqz3")))

