(define-module (crates-io ma x1 max11300) #:use-module (crates-io))

(define-public crate-max11300-0.1.0 (c (n "max11300") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1k8adlhjp5fzb15h1nqagxv5bzvng25ry545k04hcwjghcjsm6x2")))

(define-public crate-max11300-0.1.1 (c (n "max11300") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0gh0jak8m6qrl9x80pww9p8xl1zw9gh26213py5m7wvx87rf437h")))

(define-public crate-max11300-0.2.0 (c (n "max11300") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "embassy-sync") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1" "embedded-hal-async"))) (d #t) (k 2)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 2)))) (h "1nn7bjq5yk6z8r7g1582q536b74qwizib66w8kklh6j3q0ixv3zn") (f (quote (("default" "cs") ("cs" "critical-section"))))))

