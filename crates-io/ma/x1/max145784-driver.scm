(define-module (crates-io ma x1 max145784-driver) #:use-module (crates-io))

(define-public crate-max145784-driver-0.1.0 (c (n "max145784-driver") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1c0p46rdg1rdan0hllay4r0mzmbc4q3vjxsivnjyaqi6rynj1g60") (f (quote (("default")))) (y #t) (s 2) (e (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

