(define-module (crates-io ma x1 max17048) #:use-module (crates-io))

(define-public crate-max17048-0.1.0 (c (n "max17048") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.5") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.1") (d #t) (k 2)) (d (n "stm32l4xx-hal") (r "^0.3.2") (f (quote ("rt" "stm32l4x2"))) (d #t) (k 2)))) (h "197k462i0jsgl1d7vqpzc06sxqs3kpg0xvqwjdd0f4h9qgj25icq")))

