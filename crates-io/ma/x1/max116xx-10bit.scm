(define-module (crates-io ma x1 max116xx-10bit) #:use-module (crates-io))

(define-public crate-max116xx-10bit-0.1.0 (c (n "max116xx-10bit") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0azjj2m6fxjkn70cc77v997fm018vh2kna43fxjybr7ls2ckwkis")))

(define-public crate-max116xx-10bit-0.1.1 (c (n "max116xx-10bit") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0fxddgxdsd8grhd8kbdw2982gbvrnlqjal142rvcicbhr8d8gx8n")))

(define-public crate-max116xx-10bit-0.2.0 (c (n "max116xx-10bit") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "04cc1girny4l8l5qvqmrlnr22d1ksp9ijd3zxlyhx86ds4cn9d83")))

(define-public crate-max116xx-10bit-0.2.1 (c (n "max116xx-10bit") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1mv0jpw04yrm51kg0fvqjxiyv2jrmjkvm4asc4j60isaljr1a2m3")))

