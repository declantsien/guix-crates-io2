(define-module (crates-io ma x1 max112x) #:use-module (crates-io))

(define-public crate-max112x-0.1.0 (c (n "max112x") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1pa7wxrbpl9p2s7bng7dppv4j2r4p7wsqzqqfdvavy8r269h0xgi")))

(define-public crate-max112x-0.2.0 (c (n "max112x") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0y6ds8azh15fpqhy7q0dn00z05jxczbi6gvv4qvhsdw5rz3n6i4h")))

(define-public crate-max112x-0.3.0 (c (n "max112x") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0w84apyzmfapdfd2l3iqac3syxk2dgqw6aa7y885am9i95n8nxq1")))

(define-public crate-max112x-0.4.0 (c (n "max112x") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1qhb7vdngpf87qhpmngmryydymzlrq4d5i4gvldf5zcsk6f7hvpr")))

(define-public crate-max112x-0.5.0 (c (n "max112x") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "02km0g3z3x8hg5d05x3y8a5x4k7kl3l5a4px1alqy9ksqc99y02c")))

(define-public crate-max112x-0.6.0 (c (n "max112x") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "0x6vgjbak8f9ninmky6kp8rpp1l14ici2drasjhrcmc8yc93afhm")))

(define-public crate-max112x-0.7.0 (c (n "max112x") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "0vkxdgz4wbzbzcdm8311c8wz874zn6xwbb9i3cr61jsx73vrajg9")))

