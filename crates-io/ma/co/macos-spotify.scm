(define-module (crates-io ma co macos-spotify) #:use-module (crates-io))

(define-public crate-macos-spotify-0.0.1 (c (n "macos-spotify") (v "0.0.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "1wzrxkmjk0z0chjb6xy00ij06nxd8rk4q41z2djqsl3wdijgn0l6")))

(define-public crate-macos-spotify-0.0.2 (c (n "macos-spotify") (v "0.0.2") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "048dch9jc7radxljc3nbkc22a752c1pzjmy46aasmrnsxs8qb3sz")))

(define-public crate-macos-spotify-0.0.3 (c (n "macos-spotify") (v "0.0.3") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "four-char-code") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "1m0gxpk4a5dhblngvfnl2c76iybbaal4j219hay0ywi5a01170fy")))

