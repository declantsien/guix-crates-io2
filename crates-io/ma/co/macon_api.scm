(define-module (crates-io ma co macon_api) #:use-module (crates-io))

(define-public crate-macon_api-0.3.0 (c (n "macon_api") (v "0.3.0") (h "08w3wr3szn4whixj52liwgf2yva9mfvql2icdw1gx19yszl92ixy")))

(define-public crate-macon_api-1.0.0 (c (n "macon_api") (v "1.0.0") (h "1p4jm6100r3safg78q1y0xmb5qj61n20fcri297r4q3fh08bnjrf")))

(define-public crate-macon_api-1.0.1-beta.0 (c (n "macon_api") (v "1.0.1-beta.0") (h "1b0w7frdi7rsaf2nla6nzcyc23mhl6k4c4n314cqh2j2m9i8vikj")))

