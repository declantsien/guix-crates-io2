(define-module (crates-io ma co macos-tun-tap) #:use-module (crates-io))

(define-public crate-macos-tun-tap-0.1.2 (c (n "macos-tun-tap") (v "0.1.2") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "etherparse") (r "~0.9") (d #t) (k 2)) (d (n "futures") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "mio") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "~0.4") (d #t) (k 2)) (d (n "tokio-core") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0l3n0sklsn7grsjh5l7fkakrfbdfqh671k87h29x6q3hm4azfyrh") (f (quote (("tokio" "futures" "libc" "mio" "tokio-core") ("default" "tokio")))) (y #t)))

