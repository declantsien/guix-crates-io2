(define-module (crates-io ma co macon_derive) #:use-module (crates-io))

(define-public crate-macon_derive-0.3.0 (c (n "macon_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.44") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wv0l80b6clg2nbzky7pdb1dr14rz26j6s5x9h6q1yghgqph7xi3")))

(define-public crate-macon_derive-1.0.0 (c (n "macon_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.44") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jx847yyidl68qskzjrcz94cawsqq0n37h79i2gqis3qdzr69mpg")))

(define-public crate-macon_derive-1.0.1-beta.0 (c (n "macon_derive") (v "1.0.1-beta.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.44") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0skbjg7kg2py8z8did26h4paw3p7bmshjrn7hnwzikxzkc37l220")))

