(define-module (crates-io ma co maco) #:use-module (crates-io))

(define-public crate-maco-0.1.0 (c (n "maco") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "1a9bqhmmwdyr148cp8p18ys3p0clnw19fwa95wmd84j1rks9a735")))

