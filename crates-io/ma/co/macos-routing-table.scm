(define-module (crates-io ma co macos-routing-table) #:use-module (crates-io))

(define-public crate-macos-routing-table-0.1.0 (c (n "macos-routing-table") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cidr") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mac_address") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aw12ccchm6zz11z67gkcslrnk1qs4kic8ka2ypl5lx3ah8v26yq")))

