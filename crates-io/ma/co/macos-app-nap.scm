(define-module (crates-io ma co macos-app-nap) #:use-module (crates-io))

(define-public crate-macos-app-nap-0.0.1 (c (n "macos-app-nap") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cocoa-foundation") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "0shcdxw8p80yqp2dz868rjf5z9c9m0q4kzzqpzwvazzzpisbs0cp")))

