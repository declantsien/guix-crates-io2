(define-module (crates-io ma co macos) #:use-module (crates-io))

(define-public crate-macos-0.0.1 (c (n "macos") (v "0.0.1") (h "0g1cnl2a01ghlahpl74hw0m22bf4ghsa7cc35h1m4pj5g2dmy0vz") (y #t)))

(define-public crate-macos-0.0.1-beta (c (n "macos") (v "0.0.1-beta") (h "1sf7mqgk1xiprsryj10c1vr60z5z859r9wdvynf1qrq0s5z7x5fv") (y #t)))

(define-public crate-macos-0.0.2 (c (n "macos") (v "0.0.2") (h "0rpb7s8gqmspaw45qljpf03njxnrkznf4rfwvjc5skivyvjryp7k") (y #t)))

(define-public crate-macos-0.0.3 (c (n "macos") (v "0.0.3") (h "0zwnzjyxip48d52m08yjvz1h535alv639kw49zk9lalyyf1wg2vs") (y #t)))

