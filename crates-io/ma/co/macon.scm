(define-module (crates-io ma co macon) #:use-module (crates-io))

(define-public crate-macon-0.2.0 (c (n "macon") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mcpj1sndhp5dipm7jpn3xpm68ggibkhjdd0an78jgrll2rlqvmp")))

(define-public crate-macon-0.3.0 (c (n "macon") (v "0.3.0") (d (list (d (n "macon_api") (r "=0.3.0") (d #t) (k 0)) (d (n "macon_derive") (r "=0.3.0") (d #t) (k 0)))) (h "14r2i97csyr1s5apw6pv1imjksih5bk3iagcaw7bqk7v0y4w3w46") (y #t)))

(define-public crate-macon-1.0.0 (c (n "macon") (v "1.0.0") (d (list (d (n "macon_api") (r "=1.0.0") (d #t) (k 0)) (d (n "macon_derive") (r "=1.0.0") (d #t) (k 0)))) (h "0xx5xwli4cd1xvs3kgvxnyqa4v70zl6nm2dsx3chfpy5iinlj1yq")))

(define-public crate-macon-1.0.1-beta.0 (c (n "macon") (v "1.0.1-beta.0") (d (list (d (n "macon_api") (r "=1.0.1-beta.0") (d #t) (k 0)) (d (n "macon_derive") (r "=1.0.1-beta.0") (d #t) (k 0)))) (h "03a2c9drgx03v9lj6rhhbk5d9pk37s3a4qfyrzksy06amb4q39c2")))

