(define-module (crates-io ma co macos-perf) #:use-module (crates-io))

(define-public crate-macos-perf-0.1.0 (c (n "macos-perf") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c5kwlimwh6qajy5ksqn0rfhn3i5g74pqczv8sygwx4crwzylk6r") (y #t)))

(define-public crate-macos-perf-0.1.1 (c (n "macos-perf") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vrx5jjnjwklifd8mjq7srzjh9rfkb781jcamfhl3c46m3vlmlbg")))

