(define-module (crates-io ma co macos-tags) #:use-module (crates-io))

(define-public crate-macos-tags-0.1.0 (c (n "macos-tags") (v "0.1.0") (d (list (d (n "plist") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xattr") (r "^1.3.1") (d #t) (k 0)))) (h "05fhw5mmls714pi95q1n18bnvbwxqg2k5zld6qkzm2y11zbblhz3")))

(define-public crate-macos-tags-0.2.0 (c (n "macos-tags") (v "0.2.0") (d (list (d (n "plist") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xattr") (r "^1.3.1") (d #t) (k 0)))) (h "0aazkcnp1m97pmli9yihipj9vwm9r9j6rr2s2lzzq6dkw47q9qrv")))

(define-public crate-macos-tags-0.2.1 (c (n "macos-tags") (v "0.2.1") (d (list (d (n "plist") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xattr") (r "^1.3.1") (d #t) (k 0)))) (h "19ajbwpails57nk1s61ns2xxcrnklyzblr812za3gq1a6zygxyp0")))

