(define-module (crates-io ma ve maven-toolbox) #:use-module (crates-io))

(define-public crate-maven-toolbox-0.0.1 (c (n "maven-toolbox") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.0.0") (f (quote ("tls"))) (o #t) (k 0)))) (h "0g6mmyf73w8ka1a1n89j4x4b49x8wjmj7vdsy29md2pbincklvxd") (f (quote (("default-impl" "ureq" "roxmltree") ("default" "default-impl"))))))

(define-public crate-maven-toolbox-0.0.2 (c (n "maven-toolbox") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.0.0") (f (quote ("tls"))) (o #t) (k 0)))) (h "037fsc6v7s4g88y150qs79v06fa9fpy2nwd27zm5cxi5v1zq3v4n") (f (quote (("default-impl" "ureq" "roxmltree") ("default" "default-impl"))))))

(define-public crate-maven-toolbox-0.0.3 (c (n "maven-toolbox") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls"))) (o #t) (k 0)))) (h "0707na7gl3p7nzpms5r27lzrayl8hdkky38p88i5vskadjrxyiih") (f (quote (("default-impl" "ureq" "roxmltree") ("default" "default-impl"))))))

