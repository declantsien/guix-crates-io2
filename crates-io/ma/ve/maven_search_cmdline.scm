(define-module (crates-io ma ve maven_search_cmdline) #:use-module (crates-io))

(define-public crate-maven_search_cmdline-0.1.0 (c (n "maven_search_cmdline") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0ql6iky45b3imnd1x2b2jh9dnys92nm8bc3i4rasl1fyjnqd9hyc")))

(define-public crate-maven_search_cmdline-0.1.1 (c (n "maven_search_cmdline") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1rshcxipi5zs9xkhjmkpdcn39d21m8qdglgx2r4dm5a9a54kjipw")))

(define-public crate-maven_search_cmdline-0.2.0 (c (n "maven_search_cmdline") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "update-informer") (r "^0.2.0") (d #t) (k 0)))) (h "171g9x2frbhcsxq34c1hdkd3hwar2f6r2kkqx04kyrplx09jhhbg")))

(define-public crate-maven_search_cmdline-0.2.1 (c (n "maven_search_cmdline") (v "0.2.1") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "update-informer") (r "^0.2.0") (d #t) (k 0)))) (h "0j15a6fdpkhv1n35jv35q9mmjjcv4r2ivg3gq8s1v6wakg3lf7hr")))

(define-public crate-maven_search_cmdline-0.2.2 (c (n "maven_search_cmdline") (v "0.2.2") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "update-informer") (r "^0.2.0") (d #t) (k 0)))) (h "0ax8ha8m5nv02n9ijndzjha439swlyr17y6rmlgrl56dd20xm76j")))

(define-public crate-maven_search_cmdline-0.3.0 (c (n "maven_search_cmdline") (v "0.3.0") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getargs") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maven-search-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "update-informer") (r "^1.1.0") (d #t) (k 0)))) (h "0rrjr23a3fkf33y19l0iy2xylfy2f3kgggwfdbmhcidflnjsqpmc") (r "1.72.0")))

