(define-module (crates-io ma ve maven) #:use-module (crates-io))

(define-public crate-maven-0.1.0 (c (n "maven") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1442q153lz1by6mbxy1h00pwihbpfx2vdgk8r69pgnsl09kz5bg0")))

