(define-module (crates-io ma ve mave) #:use-module (crates-io))

(define-public crate-mave-0.1.0 (c (n "mave") (v "0.1.0") (h "161y2zadipz0pw4w7hk5s841kl7h4qivv2zbqvs0b3rp1fwiiy61")))

(define-public crate-mave-0.0.0 (c (n "mave") (v "0.0.0") (h "0ah8fmfxmy0fk6mda75nwi42ka2q4zbwwgysjyssm1fanbr20xb2")))

