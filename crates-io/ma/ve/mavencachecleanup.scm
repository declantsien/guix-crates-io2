(define-module (crates-io ma ve mavencachecleanup) #:use-module (crates-io))

(define-public crate-mavencachecleanup-0.1.0 (c (n "mavencachecleanup") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "0did9kxgyki8dkcra6iz24d2sga55l7dq6rranzxxa5lcqiy5l7m")))

(define-public crate-mavencachecleanup-1.0.0 (c (n "mavencachecleanup") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "0s69zz036lfmqjwk5mhcysyg9h8awlgydipv90q0mmxrig9cwv82")))

