(define-module (crates-io ma ve maver) #:use-module (crates-io))

(define-public crate-maver-0.1.0 (c (n "maver") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0zr5mm414dq5i5i00mzrr29bn0a0j7k9jvvlv3ka2c1z4k2zi4zz")))

