(define-module (crates-io ma c- mac-parser) #:use-module (crates-io))

(define-public crate-mac-parser-0.1.0 (c (n "mac-parser") (v "0.1.0") (d (list (d (n "bin-utils") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0dhh0cydlvg18415xjv1q2lfwivy0d6ggw38rf5spv4z54d1k2j2") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-mac-parser-0.1.1 (c (n "mac-parser") (v "0.1.1") (d (list (d (n "bin-utils") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0lgyvhvg7fc2i7q4a2rv454iwzi5hld00kxbj2garzai2jc2bfgx") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-mac-parser-0.1.2 (c (n "mac-parser") (v "0.1.2") (d (list (d (n "bin-utils") (r "^0.2.1") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0mq2niq9mcq4yklg6nbhighg8pg5g75q5b18rgz3rfyfcbh3wdsq") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-mac-parser-0.1.3 (c (n "mac-parser") (v "0.1.3") (d (list (d (n "bin-utils") (r "^0.2.1") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1aja628820chn71iy4gxdv5jpzanci912lblh0qq5pff9lyhhnah") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-mac-parser-0.1.4 (c (n "mac-parser") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "scroll") (r "^0.12.0") (k 0)))) (h "01yqlvif0apkg67q4fisxwlhk8akxm2xw2j7zcz7nfi8jdr0n003") (f (quote (("default" "debug") ("debug"))))))

