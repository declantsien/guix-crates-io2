(define-module (crates-io ma c- mac-process-info) #:use-module (crates-io))

(define-public crate-mac-process-info-0.1.0 (c (n "mac-process-info") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1w95srypwhssr13x9g3ljl1i6ccnd7nxcb9lgzbg3j75d1qxpp5f")))

(define-public crate-mac-process-info-0.2.0 (c (n "mac-process-info") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.29") (d #t) (k 1)))) (h "12173wnfc3d50w8gvxzfrykk76l5bmxcvci6gab780f4bjv4hrmv")))

