(define-module (crates-io ma c- mac-can-sys) #:use-module (crates-io))

(define-public crate-mac-can-sys-0.12.0 (c (n "mac-can-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "0hqsvh96yx0v2045pdx32xk73fg5vpavw8mwplxyj8ryy3pqrrrx")))

