(define-module (crates-io ma c- mac-disk-monitor) #:use-module (crates-io))

(define-public crate-mac-disk-monitor-0.1.0 (c (n "mac-disk-monitor") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "k9") (r "^0.11.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "speculate") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "timeout-readwrite") (r "^0.3.1") (d #t) (k 0)))) (h "10rs05k26qkydyna88wbf9sv6879pj9dgv3qvzswycqg71hn87ba") (y #t)))

