(define-module (crates-io ma c- mac-mouse-sys) #:use-module (crates-io))

(define-public crate-mac-mouse-sys-0.0.1 (c (n "mac-mouse-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "04b9iqqv79sdckqs9hq7gz9c1a7f6mgr4igh9gmwap919bkvgsvl")))

(define-public crate-mac-mouse-sys-0.0.2 (c (n "mac-mouse-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "1q4b619lmjdw0pgchd511b9v9mimmyhx0nxzr1gw18jb70wbzmfv")))

(define-public crate-mac-mouse-sys-0.0.3 (c (n "mac-mouse-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "13v6pm1r9dvzx8fddxvy7frbx0mspyvkinh5fg9ydkdwza3whm2n")))

(define-public crate-mac-mouse-sys-0.0.4 (c (n "mac-mouse-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "0acacwxjvzxx0cjjq9xkz7372qxckiz3i51fik4hxd93prs0gdza")))

(define-public crate-mac-mouse-sys-0.0.5 (c (n "mac-mouse-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "1zhm3gypv97hl82q12cmg939q0vjmh75hkqgiya6ihsmqv60j7x4")))

(define-public crate-mac-mouse-sys-0.0.6 (c (n "mac-mouse-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bddc00yp4r5qqhzxprq3dxkhsirkb866w1nlsvj99yd24jps93h")))

(define-public crate-mac-mouse-sys-0.0.7 (c (n "mac-mouse-sys") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i65a4n6qhfzj1r52jf3pymgx3cird7zwgzm83x70g6ylyzxjaf2")))

(define-public crate-mac-mouse-sys-0.0.8 (c (n "mac-mouse-sys") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12mkpnfmyzgkmc70mcldq3nqvmknqcg5ic7kcyrlalsh8wc47cxj")))

(define-public crate-mac-mouse-sys-0.0.9 (c (n "mac-mouse-sys") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k3g5gz65y4d1lpvgcqj6jwxs8mw7scnhm6c65zlwpryagraln6j")))

