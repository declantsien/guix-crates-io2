(define-module (crates-io ma c- mac-notification-sys) #:use-module (crates-io))

(define-public crate-mac-notification-sys-0.1.0 (c (n "mac-notification-sys") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "0khmbsvipph81m1k6nfzvq4a80v3nhxj00rnkjfnfglknb4x60zf")))

(define-public crate-mac-notification-sys-0.1.1 (c (n "mac-notification-sys") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "1m77gjvzyby5id9ija6izl780z4apsfggs6c814nca1854q54i2h")))

(define-public crate-mac-notification-sys-0.1.2 (c (n "mac-notification-sys") (v "0.1.2") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "1r709gimp5cp3cb58l8drl1ir08g4vwcx5hvw1ghdadg0l9nmvxi")))

(define-public crate-mac-notification-sys-0.1.3 (c (n "mac-notification-sys") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.45") (d #t) (k 1)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "0rpacpwr3gj8dqyzah61qdzzg0h98lnf306zwm1p9nx2rav3jdka")))

(define-public crate-mac-notification-sys-0.2.0 (c (n "mac-notification-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "0qn15cb9vcr4z736053k2n9ld86j47jlrxiyq8v0bayg4si8ixd7")))

(define-public crate-mac-notification-sys-0.3.0 (c (n "mac-notification-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)))) (h "0xnsrr4102rbka57198v7gkfhivl54a456ax765x7758m5qnpyrx")))

(define-public crate-mac-notification-sys-0.4.0 (c (n "mac-notification-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1avl4fv9mzwfnw6a24nmfj3d7h2dfjabcd8y5avlajj3d8vkxlsl") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.0-pre.1 (c (n "mac-notification-sys") (v "0.5.0-pre.1") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1zq22nib1nhnhs81q0rq4xkbzcd1aypfr5him7fzh462pngc8c54") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.0 (c (n "mac-notification-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0aav5q3dp3753h1lgm35x0l2d5maw200z1ak1qnqpyprizy16z19") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.1 (c (n "mac-notification-sys") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1y2h7np1l216dv575vjrqch86yfgvc75hi4gb26k3c36ha4lhrmb") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.2 (c (n "mac-notification-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "00vjrvxdlvxi2xdqcidgyw1w1xp07y3y051y93574p8p0sk78bq4") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.3 (c (n "mac-notification-sys") (v "0.5.3") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1alk74999zclvg0qba90wbwiqy3gzxpsrzma2b548j4h7jwar927") (y #t) (r "1.56")))

(define-public crate-mac-notification-sys-0.5.5 (c (n "mac-notification-sys") (v "0.5.5") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "00lxriq4igilv6h7dyba1ym6vr3gk012z6hmkmgrisg2iyl33wpz") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.6 (c (n "mac-notification-sys") (v "0.5.6") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1m8sg6ivq27gz6nzlj6xm7mwigi7d4afnllxwy4n8x8pvc7dawiy") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.7 (c (n "mac-notification-sys") (v "0.5.7") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1f6gcdzb6a25hfjhg82xq9hl6gnbixf6p3mvvlh05d4ynmd1f0ls") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.8 (c (n "mac-notification-sys") (v "0.5.8") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0anlkdrd3wz5mdrkcgak2994h7g652m65wppfbbl1rnh99ak9i5b") (r "1.56")))

(define-public crate-mac-notification-sys-0.5.9 (c (n "mac-notification-sys") (v "0.5.9") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1pacnlxq4padiji3xnqxkg4z23b4a22fdllba6zqhpzqhy78a0ll") (r "1.56")))

(define-public crate-mac-notification-sys-0.6.0 (c (n "mac-notification-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "18aq66mijzgi0g1jlhf9mg1m1hxgscgpbi0p9d9azq0wld0kmcls") (r "1.56")))

(define-public crate-mac-notification-sys-0.6.1 (c (n "mac-notification-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0r0zla79lgy7mcrwdhk212x2pylk6sy29f81db0smnzr9zbs9z2i") (r "1.56")))

