(define-module (crates-io ma x5 max553x) #:use-module (crates-io))

(define-public crate-max553x-0.1.0 (c (n "max553x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1p8pxj2mjnvizxpd6z820dpbgba33a06kxx49fk9y5xpxsz6hifj")))

(define-public crate-max553x-0.1.1 (c (n "max553x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0zp3xrqw88qs2h5dlvrj497hyxg3h4yx3m96xghkvjssrpz616v0")))

(define-public crate-max553x-0.2.0 (c (n "max553x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "0fd5xqvwa0zc2kvdpknlrcks60pm7svg2wjxxvaiv7jmnh1dpy8s")))

(define-public crate-max553x-0.3.0 (c (n "max553x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "17mmrfmafnmfdlgwvwsp93d9jyj4nl54cnpwwrybg6r5ly7fiiji")))

