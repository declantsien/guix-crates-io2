(define-module (crates-io ma cr macros-macros) #:use-module (crates-io))

(define-public crate-macros-macros-0.1.1 (c (n "macros-macros") (v "0.1.1") (d (list (d (n "macros-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "102mgb03vswzz716bnylddsjhv5wags12h6iby2qwk924a7v9ffm")))

(define-public crate-macros-macros-0.1.2 (c (n "macros-macros") (v "0.1.2") (d (list (d (n "macros-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0jfflpnrfqcpl6hnvjjvjc8sicz8v5a1v0r3n6i64qq98szhwd60")))

(define-public crate-macros-macros-0.2.0 (c (n "macros-macros") (v "0.2.0") (d (list (d (n "macros-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0vp7qpwyirwk6cfqfa07fr757nj80qz5m6agd6d9nxz8py9h49s2")))

(define-public crate-macros-macros-0.2.1 (c (n "macros-macros") (v "0.2.1") (d (list (d (n "macros-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0whlfsm5dc861qh88w6xqjjm0910szj6nj8np0wrxm40kr1jqffk")))

