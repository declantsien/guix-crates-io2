(define-module (crates-io ma cr macro-attr-2018) #:use-module (crates-io))

(define-public crate-macro-attr-2018-0.3.0 (c (n "macro-attr-2018") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0gq63k7jjfx7f9y3zvqda84r57cmlsxig59b8kjmiqvrn51xzsq9")))

(define-public crate-macro-attr-2018-0.3.1 (c (n "macro-attr-2018") (v "0.3.1") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0iz4v70xmkb9zkkqfvn4x9sgl6yf6z3vr6cxn6h01zazbq5ykibd")))

(define-public crate-macro-attr-2018-0.3.2 (c (n "macro-attr-2018") (v "0.3.2") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0zihg32r3j7szd2jw5y4yyyc16bjy7cw4aai9nk6wwp1vc0afjin")))

(define-public crate-macro-attr-2018-0.3.3 (c (n "macro-attr-2018") (v "0.3.3") (d (list (d (n "enum_derive") (r "^0.1.7") (k 2)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0dyssabc6cl1mdpfnfr4fnf30skvgwivj03dr3kphj1n73193ryg")))

(define-public crate-macro-attr-2018-0.4.0 (c (n "macro-attr-2018") (v "0.4.0") (d (list (d (n "enum_derive") (r "^0.1.7") (k 2)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1d5msi5dz85l8cxmwka53msjabw3grl0wdi4clyfj6f2sdk1slfw")))

(define-public crate-macro-attr-2018-1.0.0 (c (n "macro-attr-2018") (v "1.0.0") (d (list (d (n "enum_derive") (r "^0.1.7") (k 2)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0gbjbch545z596slddbjvk05pxrc29kjl3pgqbr9hqr40iplr2rp")))

(define-public crate-macro-attr-2018-1.1.0 (c (n "macro-attr-2018") (v "1.1.0") (d (list (d (n "enum_derive") (r "^0.1.7") (k 2)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "04xmq2cvaghsr8r5qfxzxl701x8k3r9s39lwaw6ndyryk3qlvk50")))

(define-public crate-macro-attr-2018-1.1.1 (c (n "macro-attr-2018") (v "1.1.1") (h "0w33vl2lpzjyx751ypgxwjd2pkk545q7l1db6fjm9p5njz7plqg7")))

(define-public crate-macro-attr-2018-1.1.2 (c (n "macro-attr-2018") (v "1.1.2") (h "156y53jsicnp3w3ixa71whyibf9diyvigingh0i1l8x03c1yrdmm") (r "1.46")))

(define-public crate-macro-attr-2018-2.0.0 (c (n "macro-attr-2018") (v "2.0.0") (h "1w5k8l1fz388q1azgf4j0482rqzhvynsd2pgzm5g361fm50vpbjc") (r "1.60")))

(define-public crate-macro-attr-2018-2.0.1 (c (n "macro-attr-2018") (v "2.0.1") (h "0iayb0fvcyglj7wgqv7nih5bccsxw485b1i88np7l2jvjc9fjyji") (r "1.60")))

(define-public crate-macro-attr-2018-2.1.0 (c (n "macro-attr-2018") (v "2.1.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0sv0f4da6pavz6w30ka4rlr09j8bia9ylgzklc7a3dbicrjwkgf2") (r "1.60")))

(define-public crate-macro-attr-2018-2.1.1 (c (n "macro-attr-2018") (v "2.1.1") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.9") (d #t) (k 1)))) (h "0jmlp2i50sbhfirillbfgylix7js8q4kq16xjsb6bnazy6l40l7x") (r "1.60")))

(define-public crate-macro-attr-2018-2.1.2 (c (n "macro-attr-2018") (v "2.1.2") (h "0yx35jp4mimxi6rx5jckw44cd3clr2ki9kvlc11ipj9m6qnvzyv6") (r "1.60")))

(define-public crate-macro-attr-2018-3.0.0 (c (n "macro-attr-2018") (v "3.0.0") (h "1rvlg5ikcwhy9gpbv0p7sf1j33vq6q3a7nqlniiidz1n77ama5zj") (r "1.71")))

