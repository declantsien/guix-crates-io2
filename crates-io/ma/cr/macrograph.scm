(define-module (crates-io ma cr macrograph) #:use-module (crates-io))

(define-public crate-macrograph-0.0.0 (c (n "macrograph") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync"))) (d #t) (k 0)))) (h "18l5c1bp7k0ff0jrpwyvjwhbd8bbzg9q6ihrs7gy2lf89vrrrxwg")))

