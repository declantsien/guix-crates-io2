(define-module (crates-io ma cr macro-delegatenode) #:use-module (crates-io))

(define-public crate-macro-delegatenode-0.5.0 (c (n "macro-delegatenode") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rjmh97a2gka60wcrrbdifwr7faw5y9s6i35k30kb7b30m11z7q6")))

