(define-module (crates-io ma cr macroquad-profiler) #:use-module (crates-io))

(define-public crate-macroquad-profiler-0.1.0 (c (n "macroquad-profiler") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0djz7ciqmp2ddjnyx9ysq241misd2pkq7997470mns9zgnhfkysc")))

(define-public crate-macroquad-profiler-0.1.1 (c (n "macroquad-profiler") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1yrmn77x98a3qi59r5la5xjm0z0s05c3d1a4w8zyjzwp0i21s7z0")))

(define-public crate-macroquad-profiler-0.2.0 (c (n "macroquad-profiler") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.4") (d #t) (k 0)))) (h "1r95yc4zi294l6wk6d83ai5dzixdvyxpp0ya4l576aidqs93f3vs")))

