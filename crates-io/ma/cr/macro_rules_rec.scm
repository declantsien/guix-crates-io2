(define-module (crates-io ma cr macro_rules_rec) #:use-module (crates-io))

(define-public crate-macro_rules_rec-0.1.0 (c (n "macro_rules_rec") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "printing" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "template-quote") (r "^0.3.1") (d #t) (k 0)))) (h "1kx9aij2zh63gjvz84b85nnfzsfx3npv30fij0lvd5q1shi831a1")))

