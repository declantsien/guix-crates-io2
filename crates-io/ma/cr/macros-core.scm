(define-module (crates-io ma cr macros-core) #:use-module (crates-io))

(define-public crate-macros-core-0.1.2 (c (n "macros-core") (v "0.1.2") (d (list (d (n "macros-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "macros-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0n0bkb8zynf07sn4jzm02c306v555ar6aiqsh0f1lhzv6hmzlyza")))

(define-public crate-macros-core-0.2.0 (c (n "macros-core") (v "0.2.0") (d (list (d (n "macros-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "macros-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0vzacg1jzsc6l4bvk5g7sn51pi2f8y281142y5swl548vn6xqj4l")))

(define-public crate-macros-core-0.2.1 (c (n "macros-core") (v "0.2.1") (d (list (d (n "macros-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "macros-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0yh1iyb3lbas88ynnszbfrmyf22kq5rx2n2ylabn5kclxkisw1gf")))

