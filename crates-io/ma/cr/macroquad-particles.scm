(define-module (crates-io ma cr macroquad-particles) #:use-module (crates-io))

(define-public crate-macroquad-particles-0.1.0 (c (n "macroquad-particles") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0kii7jij6kkc8qawsm42vk8chgll7hwx1c412l8r5si5789z5m6z")))

(define-public crate-macroquad-particles-0.1.1 (c (n "macroquad-particles") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0h6y2pmq639fmwl5jnrj74042if31gpsynd4afd7js2dbq1xq1c5")))

(define-public crate-macroquad-particles-0.1.2 (c (n "macroquad-particles") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3.0") (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hvkwabbw8iapn4rcwp1agrhbx6qns887d684hkw90d9b299bprk")))

(define-public crate-macroquad-particles-0.2.0 (c (n "macroquad-particles") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.4.0") (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0353xq3p7p9w67w7aqyc8w5g7crsa2ysyldjc4vpk15gc6iaral8")))

(define-public crate-macroquad-particles-0.2.1 (c (n "macroquad-particles") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.4.0") (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)))) (h "109k706m6vj9vq44a1g0izjpi8wjszx0knwqdg6zzs504aakfjg7")))

