(define-module (crates-io ma cr macro-compose) #:use-module (crates-io))

(define-public crate-macro-compose-0.1.0 (c (n "macro-compose") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "06f81f1ny6pxq2rqdbga3vw76p8mlf2hi3sc0lr4a3ds8sj9kjnx")))

