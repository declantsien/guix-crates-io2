(define-module (crates-io ma cr macroquest-macros) #:use-module (crates-io))

(define-public crate-macroquest-macros-0.1.0 (c (n "macroquest-macros") (v "0.1.0") (d (list (d (n "macroquest-build-config") (r "^0.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kqfrarg69vrwvabh2ziw3bi0zwfsq1khbxrqxx5sqpwxw34zxhx")))

(define-public crate-macroquest-macros-0.1.3 (c (n "macroquest-macros") (v "0.1.3") (d (list (d (n "macroquest-build-config") (r "^0.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbj2d3kmnnazbym36h782m8gk9l5mfi49p8w4lk0jrnhgwv7ffl")))

(define-public crate-macroquest-macros-0.1.7 (c (n "macroquest-macros") (v "0.1.7") (d (list (d (n "macroquest-build-config") (r "^0.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "154xbnf5x9z81k66pz8dd437h1xz71z6p26gnr7zv8zq3144l261")))

