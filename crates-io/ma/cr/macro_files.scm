(define-module (crates-io ma cr macro_files) #:use-module (crates-io))

(define-public crate-macro_files-0.1.0 (c (n "macro_files") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0rp8ply9m642lfpv6yyawp33qzk9x5q2syp4717phnd5rl90b1vc") (f (quote (("default" "tempfile")))) (r "1.56")))

