(define-module (crates-io ma cr macros-rs) #:use-module (crates-io))

(define-public crate-macros-rs-0.3.0 (c (n "macros-rs") (v "0.3.0") (h "1qya31q64vjwsbmyj02jnqrcawkcnhc566r60lswjim6cjaxvp3m")))

(define-public crate-macros-rs-0.3.1 (c (n "macros-rs") (v "0.3.1") (h "0lw6dimj3sps6fgvlg7ccr8c2pmcsn736yv0ajh4sgklw6lh20jp")))

(define-public crate-macros-rs-0.3.2 (c (n "macros-rs") (v "0.3.2") (h "1vrqszh4m7cvkzxxdwaz1ddi47xg5xmr9a8124qjbil5l6agky1n")))

(define-public crate-macros-rs-0.3.3 (c (n "macros-rs") (v "0.3.3") (h "0w132kh4mai9vdy3bl6by9pxd4sqjjf6hinbzhnkcq86mdzqllsa")))

(define-public crate-macros-rs-0.3.4 (c (n "macros-rs") (v "0.3.4") (h "1h2k5bj7h1wrzirl67m5srygpidzy4s5by6q659s405k9r1sysc6")))

(define-public crate-macros-rs-0.3.5 (c (n "macros-rs") (v "0.3.5") (h "00jd0wpah7fl386v1636lx0nj1ip8czjxnfssiflvn3nn6r06hxx")))

(define-public crate-macros-rs-0.3.6 (c (n "macros-rs") (v "0.3.6") (h "1vdpk85k22faanl9j8v7xcsqbrb97r2xh4ra01jcapc3qi17yhaj")))

(define-public crate-macros-rs-0.3.7 (c (n "macros-rs") (v "0.3.7") (h "1d9l7irbc3pbp86flzg2pfdaycnfpkpc7algmsbbag0dacbwhxw2")))

(define-public crate-macros-rs-0.4.0 (c (n "macros-rs") (v "0.4.0") (h "10ykdn4v52nk0mgkwkamrgyy4dqy5gl0kvx9bg0yj6l06hxgygwp")))

(define-public crate-macros-rs-0.4.1 (c (n "macros-rs") (v "0.4.1") (h "1fw2y4040clkkdb6ybwzvmm37mgjg87hqhvmymxd1j10h1bqfcvd") (y #t)))

(define-public crate-macros-rs-0.4.2 (c (n "macros-rs") (v "0.4.2") (h "0ikrcrgw7gkdf2jihd1hhy2pjg9r3a76rv8lkb7qcvzziwkw9c0g")))

(define-public crate-macros-rs-0.4.3 (c (n "macros-rs") (v "0.4.3") (h "0sxsfjvdbz67kcrc4as7r9aq4hrk749gv5q90nx4bqpcp83y3pfz") (y #t)))

(define-public crate-macros-rs-0.4.4 (c (n "macros-rs") (v "0.4.4") (h "092h2498rvvv1n8k29zqrhhpndw22lhba72wr1hip9gcqyc8rig7")))

(define-public crate-macros-rs-0.5.0 (c (n "macros-rs") (v "0.5.0") (h "1l2979q474n0hidgxlg3xvplly0qjxx7nyd7847qbfddsz4d7inj")))

(define-public crate-macros-rs-0.5.1 (c (n "macros-rs") (v "0.5.1") (h "119qkj1bv0fn0qisflvm1dncr8q4qlmcy1sxxa80z7k5cx38lnp4") (y #t)))

(define-public crate-macros-rs-0.5.2 (c (n "macros-rs") (v "0.5.2") (h "1mhjdfsrav66wzfsqsi5bxw6mbqvsyxxlfglydl6h21lbjnfl7yb")))

(define-public crate-macros-rs-1.0.0 (c (n "macros-rs") (v "1.0.0") (h "1q0qziw963dm40nk5lr6hb6dkjss61mi5p1yyjx1kdj5bqhxq955")))

(define-public crate-macros-rs-1.1.0 (c (n "macros-rs") (v "1.1.0") (d (list (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0w57rk58qn8j5xbz192gx2zf7asj8bi9pl8818z91bl661slvwmk")))

(define-public crate-macros-rs-1.1.1 (c (n "macros-rs") (v "1.1.1") (d (list (d (n "actix-http") (r "^3.6.0") (d #t) (k 2)) (d (n "actix-http-test") (r "^3.2.0") (d #t) (k 2)) (d (n "actix-utils") (r "^3.0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4.5.1") (d #t) (k 2)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1zsmz00wpnm7ffm9wssrxdwsgd22l5hs2snxgv1xd5vxqqdv5jcr") (s 2) (e (quote (("actix-web" "dep:actix-web"))))))

(define-public crate-macros-rs-1.1.2 (c (n "macros-rs") (v "1.1.2") (d (list (d (n "actix-http") (r "^3.6.0") (d #t) (k 2)) (d (n "actix-http-test") (r "^3.2.0") (d #t) (k 2)) (d (n "actix-utils") (r "^3.0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4.5.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "0niw065la2q6x7vqbq7scfxrz611372lv5flgval3xr0bwazkr3q") (f (quote (("default" "color")))) (s 2) (e (quote (("color" "dep:termcolor") ("actix-web" "dep:actix-web"))))))

(define-public crate-macros-rs-1.2.0 (c (n "macros-rs") (v "1.2.0") (d (list (d (n "actix-http") (r "^3.6.0") (d #t) (k 2)) (d (n "actix-http-test") (r "^3.2.0") (d #t) (k 2)) (d (n "actix-utils") (r "^3.0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4.5.1") (d #t) (k 2)) (d (n "colored") (r "^2.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "13a46924431gy9jw4cdn03rib3wgnlz6sb4pslran14ny18smaja") (f (quote (("default" "color")))) (y #t) (s 2) (e (quote (("color" "dep:termcolor") ("actix-web" "dep:actix-web"))))))

(define-public crate-macros-rs-1.2.1 (c (n "macros-rs") (v "1.2.1") (d (list (d (n "actix-http") (r "^3.6.0") (d #t) (k 2)) (d (n "actix-http-test") (r "^3.2.0") (d #t) (k 2)) (d (n "actix-utils") (r "^3.0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4.5.1") (d #t) (k 2)) (d (n "colored") (r "^2.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "1jwxqjwmc591igs7wf9kchxwp4iqynav3nl3yv6i1zfv9g7kd0dv") (f (quote (("default" "color")))) (s 2) (e (quote (("color" "dep:termcolor") ("actix-web" "dep:actix-web"))))))

