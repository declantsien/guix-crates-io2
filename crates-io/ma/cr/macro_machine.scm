(define-module (crates-io ma cr macro_machine) #:use-module (crates-io))

(define-public crate-macro_machine-0.1.0 (c (n "macro_machine") (v "0.1.0") (h "0rxdh2kx2zizw4kvr0gm370c969nd5rhzilkhwmkarpjsvqk2nkk")))

(define-public crate-macro_machine-0.1.1 (c (n "macro_machine") (v "0.1.1") (h "1gdjfwc9zx4z110c9vfl2pnz41szpk7h2rmlnkdmbmj2893xcaw2")))

(define-public crate-macro_machine-0.2.0 (c (n "macro_machine") (v "0.2.0") (h "07nn287n6r2jv2x2pfax6rd00dqsh0y393q4f3hkrhhiypsb663l")))

