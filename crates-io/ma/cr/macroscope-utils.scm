(define-module (crates-io ma cr macroscope-utils) #:use-module (crates-io))

(define-public crate-macroscope-utils-0.1.0 (c (n "macroscope-utils") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0habnf83m65srr5dldj701xn76l12y5c3vv96cw7zd6hvlaicczm")))

(define-public crate-macroscope-utils-0.1.1 (c (n "macroscope-utils") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1bk8zf1vz1fm1250wn0pzlwip5iz36jay9azzsgrlg16y7dkyg5k")))

