(define-module (crates-io ma cr macro-withstate) #:use-module (crates-io))

(define-public crate-macro-withstate-0.5.0 (c (n "macro-withstate") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h5gynpi31jr4b76inh7hpjjvz70c4p8firf8xrbpzz5mi4bbsc7")))

