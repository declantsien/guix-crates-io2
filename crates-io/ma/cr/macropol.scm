(define-module (crates-io ma cr macropol) #:use-module (crates-io))

(define-public crate-macropol-0.1.0 (c (n "macropol") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1dd3r9hw59ffnxi2kjrfv3hnr94jzzrlw99x7hxwgajnh8gb2zi4") (f (quote (("nightly"))))))

(define-public crate-macropol-0.1.1 (c (n "macropol") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1ym9in3a95q5lv7xgsf7rd9jm409aycmlbkx8bdpa3wm83acgmcf") (f (quote (("nightly"))))))

(define-public crate-macropol-0.1.2 (c (n "macropol") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0ydv8z6dnryg1q5mirjrapgzr3wcl8k9h05srymqa796msjd1ngv") (f (quote (("nightly"))))))

(define-public crate-macropol-0.1.3 (c (n "macropol") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)) (d (n "unstringify") (r "^0.1.4") (d #t) (k 2)))) (h "029pcplg775pzim01rq85yd9mhm4a68wfsz5a77s01zxcfapn0wa") (f (quote (("nightly"))))))

