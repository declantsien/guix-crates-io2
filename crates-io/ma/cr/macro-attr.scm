(define-module (crates-io ma cr macro-attr) #:use-module (crates-io))

(define-public crate-macro-attr-0.2.0 (c (n "macro-attr") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0anc4valrldnn0gwaqlhlr4b5zh25g1lyy1x3fq65y1b1rpirr80") (f (quote (("unstable-macros-1-1") ("std") ("default" "std"))))))

