(define-module (crates-io ma cr macroquad-tiled) #:use-module (crates-io))

(define-public crate-macroquad-tiled-0.1.0 (c (n "macroquad-tiled") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "1vc15n8zglsmbsd8zmkmanx15kcg1p2rmmdmhyi6fqq05z7mgl4b")))

(define-public crate-macroquad-tiled-0.1.1 (c (n "macroquad-tiled") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "0zy901qgcynwih1441fggj8z3lpxhjwiqbl4vyd30fz0dj0qgvkg")))

(define-public crate-macroquad-tiled-0.2.0 (c (n "macroquad-tiled") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.4.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "0x8jw7x07bb699r8pqgn0l46pxbr1w5b5vrw30b3savywmbb06ci")))

(define-public crate-macroquad-tiled-0.2.1 (c (n "macroquad-tiled") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.4.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "0714nyzw7vrzj0sc2dfzamlc08sy81565r79k79zdqjx70g7kf53")))

