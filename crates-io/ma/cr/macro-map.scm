(define-module (crates-io ma cr macro-map) #:use-module (crates-io))

(define-public crate-macro-map-0.1.0 (c (n "macro-map") (v "0.1.0") (d (list (d (n "postfix-macros") (r "^0.1") (d #t) (k 2)))) (h "0817pavyzgmngg86isrmjmg8dmz0vpp2z84gdb5gvlvq0ca89ci0")))

(define-public crate-macro-map-0.2.0 (c (n "macro-map") (v "0.2.0") (d (list (d (n "postfix-macros") (r "^0.1") (d #t) (k 2)))) (h "0cr0jf4p0yf7cg30bdfmwfk86slmv0x4lyj28i4x72wa4bagrnbs")))

