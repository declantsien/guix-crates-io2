(define-module (crates-io ma cr macrors) #:use-module (crates-io))

(define-public crate-macrors-0.1.0 (c (n "macrors") (v "0.1.0") (h "1ph82h8d7ssaj4l4qmbmlzaqvwck9rmxlqdw5rsfpplgn080b3c2")))

(define-public crate-macrors-0.1.1 (c (n "macrors") (v "0.1.1") (h "1l96m9k9yz677jl903p620i34fvlg0yfkvksi77xiqxya0qi2dn1")))

