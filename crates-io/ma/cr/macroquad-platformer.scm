(define-module (crates-io ma cr macroquad-platformer) #:use-module (crates-io))

(define-public crate-macroquad-platformer-0.1.1 (c (n "macroquad-platformer") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)))) (h "0mlda3zvk2lba6fs1rmv08sy3chalfcm7xyhlxxicnjj0inc13bp")))

(define-public crate-macroquad-platformer-0.1.2 (c (n "macroquad-platformer") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)))) (h "18igsm0ydn9x6kajf0phgfbygrz3b06h2qp28a16gp0y78vjp4bs")))

(define-public crate-macroquad-platformer-0.1.3 (c (n "macroquad-platformer") (v "0.1.3") (d (list (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)))) (h "14q7mic1rw4y1571sch2nhsd1adhnw216q8q782829wcxfy02wwj")))

(define-public crate-macroquad-platformer-0.2.0 (c (n "macroquad-platformer") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.4.0") (d #t) (k 0)))) (h "1a1s66klk3fbzxj4rz8mkzmyl80dzdzf1avgfah9445mrms030wn")))

