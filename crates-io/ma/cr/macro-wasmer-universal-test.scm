(define-module (crates-io ma cr macro-wasmer-universal-test) #:use-module (crates-io))

(define-public crate-macro-wasmer-universal-test-3.0.0-beta.2 (c (n "macro-wasmer-universal-test") (v "3.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0r509rywjmff4qki4c5v4f1099kwzyj6gjcgj2aalvv5b3347fj8")))

(define-public crate-macro-wasmer-universal-test-3.0.0 (c (n "macro-wasmer-universal-test") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0b185mrq7fxzxn4rg73wjx1j10jrcdyfl2whmr08snzrm41rjpqp")))

(define-public crate-macro-wasmer-universal-test-3.0.0-rc.1 (c (n "macro-wasmer-universal-test") (v "3.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "043snr2lmhhbd9wb2bwwj8l14sh5lv3rzws2ijssz8cl6aqk8kkk")))

(define-public crate-macro-wasmer-universal-test-3.0.0-rc.2 (c (n "macro-wasmer-universal-test") (v "3.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2wg9zyafncf8877bql47nl23jbzd3mi450xzpv4h6gaybi2scy")))

(define-public crate-macro-wasmer-universal-test-3.0.0-rc.3 (c (n "macro-wasmer-universal-test") (v "3.0.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1jr1n6mahfagzh1f41gqqnxb023rmggxhbscahhzm1gaf6ia9k6d")))

(define-public crate-macro-wasmer-universal-test-3.0.0-rc.4 (c (n "macro-wasmer-universal-test") (v "3.0.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1w88xzxn6vpvwgmwj0wldv8wpmsj6jbdg49fdkvhy6806v432yjj")))

(define-public crate-macro-wasmer-universal-test-3.0.1 (c (n "macro-wasmer-universal-test") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0i5w4f9yyw0yc34cyiay6f3avkpsxd92k41z1xxjvhbdbsflxi1y")))

(define-public crate-macro-wasmer-universal-test-3.0.2 (c (n "macro-wasmer-universal-test") (v "3.0.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "03987dbvi6431bda8vfja70axlwx7wixm0g6rdlpq371z4f8cdcd")))

(define-public crate-macro-wasmer-universal-test-3.1.0 (c (n "macro-wasmer-universal-test") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0a201xnvynmc8ylafhhhs6kjqmh0jg8gkyija5faga4lv3i00jz5")))

(define-public crate-macro-wasmer-universal-test-3.2.0-alpha.1 (c (n "macro-wasmer-universal-test") (v "3.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrsv1wwzyjf4wr4jbam339m8mpvy519fwf8xv3gb36yq7n3d5dj")))

(define-public crate-macro-wasmer-universal-test-3.0.3 (c (n "macro-wasmer-universal-test") (v "3.0.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1fvs1n2ng10ikazhd51y6sfrg91wrpa9xprws7skhzq8vqqrqd18")))

(define-public crate-macro-wasmer-universal-test-3.1.1 (c (n "macro-wasmer-universal-test") (v "3.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwg1f022jlinmclikg61bvcf9cndziqn5163ciwlrnzj5fpdjhx")))

(define-public crate-macro-wasmer-universal-test-3.2.0-beta.1 (c (n "macro-wasmer-universal-test") (v "3.2.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1w2lni3jk6fbisc43lwlki029q85w3rbawg6d64003yn98dq165j")))

(define-public crate-macro-wasmer-universal-test-3.2.0-beta.2 (c (n "macro-wasmer-universal-test") (v "3.2.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8ljvkdpgw230619n9qi0ccas4909gp6rnannlpazc27mmrjjb4")))

(define-public crate-macro-wasmer-universal-test-3.2.0 (c (n "macro-wasmer-universal-test") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0k76hi4cp3y6r38wipf32y6rlq8lphajjzp3i0ina5w4v8ihrbh6")))

(define-public crate-macro-wasmer-universal-test-3.2.1 (c (n "macro-wasmer-universal-test") (v "3.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdnlzqsznbvr4vp3v85rqp7ss5076mpzlp7r9f3ywcd9r06alfc")))

(define-public crate-macro-wasmer-universal-test-3.3.0 (c (n "macro-wasmer-universal-test") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0rxaxzp58f1g8cv171mkdppr05b54h4svk4xqwkc01x0048wmqhp")))

(define-public crate-macro-wasmer-universal-test-4.0.0-alpha.1 (c (n "macro-wasmer-universal-test") (v "4.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1f24w34n5cjbvcis3ywjhsj9nv21mbkg8pfa87v9a8v4ziq53w2l")))

(define-public crate-macro-wasmer-universal-test-4.0.0-beta.1 (c (n "macro-wasmer-universal-test") (v "4.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1z73w2qpdh1v9arjqh6xll212r4zjlj2hv5xamlffi891l0r784j")))

(define-public crate-macro-wasmer-universal-test-4.0.0-beta.2 (c (n "macro-wasmer-universal-test") (v "4.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1mxl04qnccjjh7248924j5bqi19bj1df18lnlyfxj5cf4y1xi0sd")))

(define-public crate-macro-wasmer-universal-test-4.0.0-beta.3 (c (n "macro-wasmer-universal-test") (v "4.0.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1dbq59vwi81nnz994nqx0amr0vcgmmnwx3nm8h5xv4yk5v52kq7s")))

(define-public crate-macro-wasmer-universal-test-4.0.0 (c (n "macro-wasmer-universal-test") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0w41j0i06y4klacalsa6bfvnzar5dd1fhy24jlpsdf1vz2n6mmik")))

(define-public crate-macro-wasmer-universal-test-4.1.0 (c (n "macro-wasmer-universal-test") (v "4.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1hkgzzw8j5f04kgczm80djiyv5yd4vh7mqschcd2qgma2a8sb16q")))

(define-public crate-macro-wasmer-universal-test-4.1.1 (c (n "macro-wasmer-universal-test") (v "4.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "01rzyfy3vbbygl7hrp3d766gklmj22b9lsilf9y9sy3j2cb065nc")))

(define-public crate-macro-wasmer-universal-test-4.1.2 (c (n "macro-wasmer-universal-test") (v "4.1.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1w5n94xjzn1glyqxg831yqdyix73s50yrima112vh0cd29blm8q2")))

(define-public crate-macro-wasmer-universal-test-4.2.0 (c (n "macro-wasmer-universal-test") (v "4.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1jfnh2d8w493ski8cly6l813j1qswykjl2nvs97bb3cns012xj1q")))

(define-public crate-macro-wasmer-universal-test-4.2.1 (c (n "macro-wasmer-universal-test") (v "4.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1b7gri9avifv7rpddzakafl9zj7visqp6kvbhpvffmwq490gssnd")))

(define-public crate-macro-wasmer-universal-test-4.2.2 (c (n "macro-wasmer-universal-test") (v "4.2.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3db2j4ia1s18sxs39icr24ia8vvznq53dr3i9qwk9y7xa2zmsd")))

(define-public crate-macro-wasmer-universal-test-4.2.3 (c (n "macro-wasmer-universal-test") (v "4.2.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2b5ssdkcaimsw6gx1ym89d8klxlvp6ganh77sy476p7r70aaz4")))

(define-public crate-macro-wasmer-universal-test-4.2.4 (c (n "macro-wasmer-universal-test") (v "4.2.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "15zxc66ap070qcnjrzqyhwqwbyx6r10qf7zh3n2a8rd32j1y7pbi")))

(define-public crate-macro-wasmer-universal-test-4.2.5 (c (n "macro-wasmer-universal-test") (v "4.2.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncw44xqlcjk1ds9hjns6sj0hk4bvidbmrp6k4iq0brwdm167nl1")))

(define-public crate-macro-wasmer-universal-test-4.2.6 (c (n "macro-wasmer-universal-test") (v "4.2.6") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1rg1wav3mxcfqzcjkss6afqqy5dh7wb5rs0yjrr8gd3i84h1dc9f")))

(define-public crate-macro-wasmer-universal-test-4.2.7 (c (n "macro-wasmer-universal-test") (v "4.2.7") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1vpvhim0fyag2gfrklnc1asc7c0bwpk769cff8d31zrk76s9rjp3")))

(define-public crate-macro-wasmer-universal-test-4.2.8 (c (n "macro-wasmer-universal-test") (v "4.2.8") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1l3sxyd52f127rm2clmww5z9j0vdc30hwaxybc58vp914pj8mjic")))

(define-public crate-macro-wasmer-universal-test-4.3.0-alpha.1 (c (n "macro-wasmer-universal-test") (v "4.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0gp4fqnjvlwh8zqj9zq6y9vqwfrirximd02zbjpsmjqr0mq8xald")))

(define-public crate-macro-wasmer-universal-test-4.3.0-beta.1 (c (n "macro-wasmer-universal-test") (v "4.3.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0y11381l1pnd065bhs28ymxm99j2j4v6nmqkqzq77417c3ifc9vz")))

(define-public crate-macro-wasmer-universal-test-4.3.0 (c (n "macro-wasmer-universal-test") (v "4.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfy9dyx29vs2mfwzs1afvmqy04xpg3jgnviijc3amczrml7qbq0")))

(define-public crate-macro-wasmer-universal-test-4.3.1 (c (n "macro-wasmer-universal-test") (v "4.3.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0bbcfsjfainc10fwlf4ahl7sf3varjwdg2ik6r5jmhzd9wlcaxf5")))

