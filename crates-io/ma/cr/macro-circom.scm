(define-module (crates-io ma cr macro-circom) #:use-module (crates-io))

(define-public crate-macro-circom-0.1.0 (c (n "macro-circom") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0p6074gdxaf97751hj13gv5lf9zfn4dyai87k0mv5d26vh8493zm")))

