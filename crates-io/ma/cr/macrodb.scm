(define-module (crates-io ma cr macrodb) #:use-module (crates-io))

(define-public crate-macrodb-0.1.0 (c (n "macrodb") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0naww1wvgbb8mzziilsyazw5dm6rxfiqry4xh9byg83w69cwdf38")))

(define-public crate-macrodb-0.1.1 (c (n "macrodb") (v "0.1.1") (d (list (d (n "avl") (r "^0.6.2") (d #t) (k 2)) (d (n "btree-slab") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 2)) (d (n "im") (r "^15.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0j0qx5qrsrhmal82f3fgp8r6b93v8n90yz32jyrn3cjnykvrgnnl")))

