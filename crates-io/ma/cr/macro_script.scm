(define-module (crates-io ma cr macro_script) #:use-module (crates-io))

(define-public crate-macro_script-0.1.0 (c (n "macro_script") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kcxf3bxfk2v95yams9bi5xan2d2bd4jvbjar6rali05s43aqk2x")))

