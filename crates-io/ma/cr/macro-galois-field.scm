(define-module (crates-io ma cr macro-galois-field) #:use-module (crates-io))

(define-public crate-macro-galois-field-0.1.0 (c (n "macro-galois-field") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0p3l96w90ilzalf8gf6lf6j00pw04sbckx5w3q3cwh0p20y3a42a")))

