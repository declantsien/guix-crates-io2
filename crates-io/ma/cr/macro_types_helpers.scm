(define-module (crates-io ma cr macro_types_helpers) #:use-module (crates-io))

(define-public crate-macro_types_helpers-0.1.0 (c (n "macro_types_helpers") (v "0.1.0") (d (list (d (n "macro_types") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kv3bcazgc7adp1l03fjqg092i9bsk12azl4yhlx53cb22lkj0l5")))

(define-public crate-macro_types_helpers-0.2.0 (c (n "macro_types_helpers") (v "0.2.0") (d (list (d (n "macro_types") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0znmj6pzqh5bvh3ds2dlnil477p24l9msv64qlzjxszsqadb3zha")))

(define-public crate-macro_types_helpers-0.2.1 (c (n "macro_types_helpers") (v "0.2.1") (d (list (d (n "macro_types") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18309rwvxsxc4jvbc21gpdr8x0qdp88ybdm6rzwy366rajn44n9c")))

(define-public crate-macro_types_helpers-0.3.0 (c (n "macro_types_helpers") (v "0.3.0") (d (list (d (n "macro_types") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15w8rbwg3p6xgmvcidpzwm3ns9rk2y8cpd96njqnfk01zqs9bgbh")))

