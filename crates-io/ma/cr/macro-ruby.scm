(define-module (crates-io ma cr macro-ruby) #:use-module (crates-io))

(define-public crate-macro-ruby-0.1.0 (c (n "macro-ruby") (v "0.1.0") (h "0b0ybcnqhfp06fakkg2ll2p5rbn0arhhbhnfndy3r8g9m5l451xf")))

(define-public crate-macro-ruby-0.1.1 (c (n "macro-ruby") (v "0.1.1") (h "0308lhfq9llf48cq6hpdkjs2l94k7nqm59h9kxghz50n7mhzbbmz")))

(define-public crate-macro-ruby-1.0.0 (c (n "macro-ruby") (v "1.0.0") (h "0r0vb1392gmar6j7g2acwl5y5302pggijz9ww0qa59jijchz9ijq")))

(define-public crate-macro-ruby-1.1.0 (c (n "macro-ruby") (v "1.1.0") (h "06mjlaphq93sqdsxkmjxp88m8dghwqqgxhgd2cdd2zn2y8snfnaz") (y #t)))

(define-public crate-macro-ruby-1.1.1 (c (n "macro-ruby") (v "1.1.1") (h "0hsj0z23a0vwn6b6mwd11qayr59czlkw29r6cy498hxm8sdl9baw") (f (quote (("yarv") ("full"))))))

