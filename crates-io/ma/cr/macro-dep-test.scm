(define-module (crates-io ma cr macro-dep-test) #:use-module (crates-io))

(define-public crate-macro-dep-test-0.1.0 (c (n "macro-dep-test") (v "0.1.0") (d (list (d (n "macro-dep-test-macros") (r "=0.1.0") (d #t) (t "cfg(never)") (k 0)))) (h "11fcjqwaz70yscazd84knrqpfsy6mdvmc846a6l2649ihqx0kqvs")))

(define-public crate-macro-dep-test-0.1.1 (c (n "macro-dep-test") (v "0.1.1") (d (list (d (n "macro-dep-test-macros") (r "=0.1.1") (d #t) (t "cfg(never)") (k 0)))) (h "1b1dzsk9g9f2npnc2r8dikddc4ckxsmmx0iyb86h2mv3f931gfzs")))

(define-public crate-macro-dep-test-0.1.2 (c (n "macro-dep-test") (v "0.1.2") (d (list (d (n "macro-dep-test-macros") (r "=0.1.2") (d #t) (t "cfg(never)") (k 0)) (d (n "macro-dep-test-macros") (r "=0.1.2") (o #t) (d #t) (k 0)))) (h "1pv6y2rsvf3a7il7gs2z38arg8rczx5r3cqm7nhryb6j2njahny3") (s 2) (e (quote (("derive" "dep:macro-dep-test-macros"))))))

(define-public crate-macro-dep-test-0.1.3 (c (n "macro-dep-test") (v "0.1.3") (d (list (d (n "macro-dep-test-macros") (r "=0.1.3") (d #t) (t "cfg(all(to_be, not(to_be)))") (k 0)) (d (n "macro-dep-test-macros") (r "=0.1.3") (o #t) (d #t) (k 0)))) (h "0wh2cpf27r8fms1xnw9pp05470cxck2cg0bz81vfn10s24xqlcvc") (s 2) (e (quote (("derive" "dep:macro-dep-test-macros"))))))

(define-public crate-macro-dep-test-0.1.4 (c (n "macro-dep-test") (v "0.1.4") (d (list (d (n "macro-dep-test-macros") (r "=0.1.4") (d #t) (t "cfg(any())") (k 0)) (d (n "macro-dep-test-macros") (r "=0.1.4") (o #t) (d #t) (k 0)))) (h "018v11frgnz4454xf0isn4jcgdazzy6nwvgpcx7rhd8z0fhsj4qc") (s 2) (e (quote (("derive" "dep:macro-dep-test-macros"))))))

(define-public crate-macro-dep-test-0.1.5 (c (n "macro-dep-test") (v "0.1.5") (d (list (d (n "macro-dep-test-macros") (r "=0.1.5") (d #t) (t "cfg(any())") (k 0)) (d (n "macro-dep-test-macros") (r "=0.1.5") (o #t) (d #t) (k 0)))) (h "1vby5g1bm7kgziv62b2d0v5rziqkpa1ina2718dw67a47wndcgf4") (s 2) (e (quote (("derive" "dep:macro-dep-test-macros"))))))

