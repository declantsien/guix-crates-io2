(define-module (crates-io ma cr macro-log) #:use-module (crates-io))

(define-public crate-macro-log-0.1.0 (c (n "macro-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "macro-log-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1x0f3fyk8hvry13x67kvzqg3l4vmxkqiyvmya5s70a6z4bvij948")))

(define-public crate-macro-log-0.2.0 (c (n "macro-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "macro-log-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1kc5xj7ckdb3b9rix9187sklnk7a0x6nn758kmkjfq9v5yzrb78q")))

(define-public crate-macro-log-0.3.0 (c (n "macro-log") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "macro-log-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0h651yw0v63ppi2fhcf241y4wm7hgdmm9mj0qwx6lm3kj5ydp971")))

