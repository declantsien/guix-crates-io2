(define-module (crates-io ma cr macroquad-canvas) #:use-module (crates-io))

(define-public crate-macroquad-canvas-0.1.0 (c (n "macroquad-canvas") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "10w09b1dphcx5pkdgmi2dl37khan4qqavk76pa8ix7qah6p7qdif")))

(define-public crate-macroquad-canvas-0.1.1 (c (n "macroquad-canvas") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0pb3dq66j336nhdqincm77k57cl2xlmxv150la2hmc63q0zlz74l")))

(define-public crate-macroquad-canvas-0.2.0 (c (n "macroquad-canvas") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0b8m0chy2dd2abl592qvi2ckc2p0kvr0ihhnx0v34bk3fh5f39ka")))

(define-public crate-macroquad-canvas-0.2.1 (c (n "macroquad-canvas") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0x65yf3rc4rfyf2kh3c2sk2ccc26wxkml2iaahds4m7xvnmk7syr")))

(define-public crate-macroquad-canvas-0.3.0 (c (n "macroquad-canvas") (v "0.3.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "153z4fhnb3632wc5ia0gs9sv1vgpvsxccklf907j2bfjwjqvxg2j")))

(define-public crate-macroquad-canvas-0.3.1 (c (n "macroquad-canvas") (v "0.3.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1nmil1bh8ylgwb5d3m4sgzkfb39vsc8ljrk8wvi24l9ilfdab168")))

(define-public crate-macroquad-canvas-0.3.2 (c (n "macroquad-canvas") (v "0.3.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "05503hbzzp62x3q72agkqbs8cgbw8zbjvq0s9669ahqsl19w3gsp")))

(define-public crate-macroquad-canvas-0.4.0 (c (n "macroquad-canvas") (v "0.4.0") (d (list (d (n "macroquad") (r "^0.4") (d #t) (k 0)))) (h "0hgh8zd88nfjhppcjm62jl7mqfhf58n0pzqcw6d6zvkr00rzy5ni")))

(define-public crate-macroquad-canvas-0.4.1 (c (n "macroquad-canvas") (v "0.4.1") (d (list (d (n "macroquad") (r "^0.4") (d #t) (k 0)))) (h "0bvf3csvjrylwafi8vg8qfqpa13gb2nc7871aqdfkif6lvc67g3q")))

