(define-module (crates-io ma cr macro-dep-test-macros) #:use-module (crates-io))

(define-public crate-macro-dep-test-macros-0.1.0 (c (n "macro-dep-test-macros") (v "0.1.0") (h "0jcrh5ng77rwybnfhnnkfamfc3bs98c8kgqda29vzkmyxx158c0n")))

(define-public crate-macro-dep-test-macros-0.1.1 (c (n "macro-dep-test-macros") (v "0.1.1") (h "028w7k5xzny896cx7l8zhyjgl9rkj9fsc7x2hm4fn2c2sfhlahr9")))

(define-public crate-macro-dep-test-macros-0.1.2 (c (n "macro-dep-test-macros") (v "0.1.2") (h "0wb6cf3i4m8v4m37jpaifjw4fza36h9yiwlp4mdkssz2chnh7wxf")))

(define-public crate-macro-dep-test-macros-0.1.3 (c (n "macro-dep-test-macros") (v "0.1.3") (h "0pahrd2frqy20037h0pj0ig07qyhn1994vf2xzzbaq6igcr8m3wn")))

(define-public crate-macro-dep-test-macros-0.1.4 (c (n "macro-dep-test-macros") (v "0.1.4") (h "0daqdlzprvmqmfjjf5w0f1mm8cpx5g811ydmjk9xl4gsll47xc38")))

(define-public crate-macro-dep-test-macros-0.1.5 (c (n "macro-dep-test-macros") (v "0.1.5") (h "0ww7lsrhk0y4999w6qqw5qpf8inbcxp19hxkg0qf4fh6gf281kr2")))

