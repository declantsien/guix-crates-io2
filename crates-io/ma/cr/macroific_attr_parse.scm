(define-module (crates-io ma cr macroific_attr_parse) #:use-module (crates-io))

(define-public crate-macroific_attr_parse-1.0.0 (c (n "macroific_attr_parse") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0l6hgdxzl6f24dn89bhm9yd3n4chhfzx9l0x0w4vrdk0qczsfskn") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.1.0 (c (n "macroific_attr_parse") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1lqvdv847igh5ybfdx0rcmssdlr6l0n7xdbzpnlf2bxw08l41ghf") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.1.1 (c (n "macroific_attr_parse") (v "1.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "117ayb5jhvjq1bws2rdg1k6b9ds4f8k91hlvabr7wdgkm1g4pz7m") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.1.2 (c (n "macroific_attr_parse") (v "1.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wv4pxmf50ci08kibcd7gdfnz6xkcrzq12l103wmq4j82hsnnsxg") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.2.0 (c (n "macroific_attr_parse") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zmbxgqbqhfyvb1i5pf0bgyw0iikiss67rk4ifllcg33zyncqiq4") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.2.1 (c (n "macroific_attr_parse") (v "1.2.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "169dr6bfi5himmkfcc7pmgwznq8669jb8f4y789dvv01y0nkmidr") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.2.2 (c (n "macroific_attr_parse") (v "1.2.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ikmaaial5r2b1c2f3h4fwsvimi44nbylwxqzfbaw5kdnxnvld6d") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

(define-public crate-macroific_attr_parse-1.3.0 (c (n "macroific_attr_parse") (v "1.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0rrgdbj4a8xnyb0giwv12vcnld0914s05b910vhyc2mkjpddb57x") (f (quote (("nightly") ("full" "syn/full")))) (r "1.60.0")))

