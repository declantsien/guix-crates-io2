(define-module (crates-io ma cr macro_env) #:use-module (crates-io))

(define-public crate-macro_env-0.1.5 (c (n "macro_env") (v "0.1.5") (h "1lp9v6d78yyxfg0806amxl6ns651c168059w94cf0wiibzkwgkxy")))

(define-public crate-macro_env-0.1.6 (c (n "macro_env") (v "0.1.6") (h "077lhhv0lbabq3i5f3dicw7x3spsk05yzqmjxzr2dwxa8qh6ragc")))

(define-public crate-macro_env-0.1.7 (c (n "macro_env") (v "0.1.7") (h "1yhch9053ylqpws09vipf7bb2i6birrpzd7167835jr0mj90j2zc")))

(define-public crate-macro_env-0.1.8 (c (n "macro_env") (v "0.1.8") (d (list (d (n "ron") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04q56akpgdmla7kbnv4mh7z2hdp86nf87y0pk550mvac5b17k96w") (s 2) (e (quote (("typed" "dep:ron" "dep:serde"))))))

