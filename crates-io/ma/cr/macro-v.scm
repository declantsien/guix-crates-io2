(define-module (crates-io ma cr macro-v) #:use-module (crates-io))

(define-public crate-macro-v-0.1.0 (c (n "macro-v") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0knsasznhv4qc0kjmlrwi77ari6xk6jkr19k5cgja2gvhbbs73zl")))

(define-public crate-macro-v-0.1.1 (c (n "macro-v") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1g7ir63zaqqjj90g6pqqw8ssi4117la9qijrbkkl1kwshfv1s5si")))

(define-public crate-macro-v-0.1.2 (c (n "macro-v") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0vpajdpi4hhyc99idqnfanhhfj8fr392isxqvsg687f0jxbxrl6y")))

(define-public crate-macro-v-0.1.3 (c (n "macro-v") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0zbar06sspbww5sgng6maag2kw7yl5ql0lwldjvz76ipx7mcmmva")))

(define-public crate-macro-v-0.1.4 (c (n "macro-v") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0jk4yx9c5w3cmrky1i8hb51jd6lp6w7jvbnvanz6444zzflqh4pr")))

