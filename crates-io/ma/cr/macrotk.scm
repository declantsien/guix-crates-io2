(define-module (crates-io ma cr macrotk) #:use-module (crates-io))

(define-public crate-macrotk-0.1.0 (c (n "macrotk") (v "0.1.0") (d (list (d (n "macrotk-core") (r "^0.1.0") (d #t) (k 0)) (d (n "macrotk-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1blyvicsgilkriyls0z820q8kxywn0839mgj4iaji59i9lz6ihb7")))

(define-public crate-macrotk-0.1.1 (c (n "macrotk") (v "0.1.1") (d (list (d (n "macrotk-core") (r "^0.1.0") (d #t) (k 0)) (d (n "macrotk-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0b0l27js0w5rp884znz9q1nb656idnlq9vsa95m63rcvjkpwbqb9")))

(define-public crate-macrotk-0.2.0 (c (n "macrotk") (v "0.2.0") (d (list (d (n "macrotk-core") (r "^0.2.0") (d #t) (k 0)) (d (n "macrotk-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j7ny0irqhpcj27i1pqm1r95c4rlfszzmraxq3p133fi3p9719kq")))

