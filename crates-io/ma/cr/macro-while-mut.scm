(define-module (crates-io ma cr macro-while-mut) #:use-module (crates-io))

(define-public crate-macro-while-mut-0.1.0 (c (n "macro-while-mut") (v "0.1.0") (h "0ay8m2vg9pcks98nim2qf3ja78yd27k1gwgd65s1ri8xr6f3xcgb")))

(define-public crate-macro-while-mut-0.1.1 (c (n "macro-while-mut") (v "0.1.1") (h "14inivpsla0lb4l0ds6abs7fzf4zfava97cwlplrlf2dhpmm6957")))

