(define-module (crates-io ma cr macroconf) #:use-module (crates-io))

(define-public crate-macroconf-0.1.0 (c (n "macroconf") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "miniconf") (r "^0.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yp7kwgq5asa4y0mps97gsz4nxg3g8dbw9m2i4kwddydza0rks7d")))

