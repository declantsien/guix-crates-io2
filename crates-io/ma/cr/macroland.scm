(define-module (crates-io ma cr macroland) #:use-module (crates-io))

(define-public crate-macroland-0.1.0 (c (n "macroland") (v "0.1.0") (h "0cycz3pzn4hhs1pgzp3jz425f88zm5krwzgvar59h4gyv2r4mf2h") (y #t)))

(define-public crate-macroland-0.1.1 (c (n "macroland") (v "0.1.1") (h "1237991q2mrp7s2ygbvw00pnn47yh3xzidm8ikhnqz7mghv2gh7i")))

(define-public crate-macroland-0.1.2 (c (n "macroland") (v "0.1.2") (h "0zw4q8b94i4zlasd0r68wvk5h045khv9r04vci11ff3r7d1m72hb")))

(define-public crate-macroland-0.1.3 (c (n "macroland") (v "0.1.3") (h "1183cj23b53qqbxarrr72q4kzljf7spy1ba9lwsrnl63j7ffkkz0")))

(define-public crate-macroland-0.1.4 (c (n "macroland") (v "0.1.4") (h "03jznnmp27p5l0i13miif51si4cra3zvwq06y2wmx4rrmxv04c37")))

(define-public crate-macroland-0.1.5 (c (n "macroland") (v "0.1.5") (h "0d2sgni28jn7jpzvrh21vf5lbx0w5201hlvz5k4j8srwz9qlzglf")))

