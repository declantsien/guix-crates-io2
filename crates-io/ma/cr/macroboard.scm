(define-module (crates-io ma cr macroboard) #:use-module (crates-io))

(define-public crate-macroboard-0.1.0 (c (n "macroboard") (v "0.1.0") (d (list (d (n "input") (r "^0.7.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "keycode") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "tinyset") (r "^0.4.10") (d #t) (k 0)))) (h "0ybkfsr1fbbgqy07xz5nixwzk1wzph31i003aajzlhzkwzhqs5wn")))

(define-public crate-macroboard-0.2.0 (c (n "macroboard") (v "0.2.0") (d (list (d (n "input") (r "^0.7.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "keycode") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "tinyset") (r "^0.4.10") (d #t) (k 0)))) (h "0hla0z56wy37172j5y3lz36v68c5kr9ml0m3igm260afbjabkx75")))

