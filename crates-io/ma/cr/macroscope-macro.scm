(define-module (crates-io ma cr macroscope-macro) #:use-module (crates-io))

(define-public crate-macroscope-macro-0.1.0 (c (n "macroscope-macro") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "macroscope-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1m40gj086n4wgy40p13qnkh6jxcgmn3j288x94l0jqjjqzrrg1yl")))

(define-public crate-macroscope-macro-0.1.1 (c (n "macroscope-macro") (v "0.1.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "macroscope-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "155zcidyd1v68hiylv6ibscs347y5fs21a9qh63qs0v2ris67bmq")))

