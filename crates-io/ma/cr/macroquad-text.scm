(define-module (crates-io ma cr macroquad-text) #:use-module (crates-io))

(define-public crate-macroquad-text-0.1.0 (c (n "macroquad-text") (v "0.1.0") (d (list (d (n "fontdue") (r "^0.6") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (k 0)) (d (n "macroquad") (r "^0.3") (k 2)))) (h "1ivgg5djxkq395dkfzg3qnbix0wlz431h3ji0xa40p3kbvxlaw60")))

(define-public crate-macroquad-text-0.1.1 (c (n "macroquad-text") (v "0.1.1") (d (list (d (n "fontdue") (r "^0.6") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (k 0)) (d (n "macroquad") (r "^0.3") (k 2)))) (h "1dywsxgsdzpzl1bjva5p5rmdl08kzihfmdp24zn383spcghyn5ri")))

(define-public crate-macroquad-text-0.2.0 (c (n "macroquad-text") (v "0.2.0") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "macroquad") (r "^0.4") (k 0)) (d (n "macroquad") (r "^0.4") (k 2)))) (h "04c2b35689lsl6iqwls564binmrfri0s6a2npxysmzpv7q9p228d")))

