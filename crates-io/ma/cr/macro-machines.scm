(define-module (crates-io ma cr macro-machines) #:use-module (crates-io))

(define-public crate-macro-machines-0.7.1 (c (n "macro-machines") (v "0.7.1") (d (list (d (n "escapade") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "0zc2k4vpzfpqjrqjmwb8zzz0inf3dq9a4ykbi1wvx19dx0jhnvs8")))

(define-public crate-macro-machines-0.8.0 (c (n "macro-machines") (v "0.8.0") (d (list (d (n "escapade") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "0kdrcywzkw0c18rrf14c6y73sxphgpyf1ap9hc4sw31pfkxw8bm1")))

(define-public crate-macro-machines-0.8.1 (c (n "macro-machines") (v "0.8.1") (d (list (d (n "escapade") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "1ibb426awi6qak6ckaz01y57wjzpfd59m3jhmii2ds4246wm0961")))

(define-public crate-macro-machines-0.8.2 (c (n "macro-machines") (v "0.8.2") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "07z9kzmrs3gazn9g12c873d8aajnivzc7fhzfgqkza2asdwn2z7v")))

(define-public crate-macro-machines-0.8.3 (c (n "macro-machines") (v "0.8.3") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "01zkxrjd0ja8if81m6gwikbn5smpbj48vcb0zyycpf4fppdclvii")))

(define-public crate-macro-machines-0.8.4 (c (n "macro-machines") (v "0.8.4") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "0vqvyy9hr8bck96y01dlp5ylzl7zzqlsh26q8djz1l4wy79mghxw")))

(define-public crate-macro-machines-0.8.6 (c (n "macro-machines") (v "0.8.6") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "1j5xgfd0jj2a362nqf78jsk1vsvh8w3jcbn6iikd5hxa461dddwz")))

(define-public crate-macro-machines-0.8.7 (c (n "macro-machines") (v "0.8.7") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.5.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)))) (h "1kc0h6bfk6h0yikz49njk4xb82lndhw97brp7hikrsb5fqlc5y7f")))

(define-public crate-macro-machines-0.8.8 (c (n "macro-machines") (v "0.8.8") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.7.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "0l2zwgczggwawydn105daizmzm7mdklghhzrwks624zb7a1xaw48")))

(define-public crate-macro-machines-0.8.9 (c (n "macro-machines") (v "0.8.9") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.7.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "0bm73ix0mcnwf2bss2g9i0gjhdyvp54wvkjnqbaghxvr52rl6xir")))

(define-public crate-macro-machines-0.8.10 (c (n "macro-machines") (v "0.8.10") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.7.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "00535lia8rjypvsxizaxcbbkck7k8q4csdwfl92gqzjr12liyk4k")))

(define-public crate-macro-machines-0.9.0 (c (n "macro-machines") (v "0.9.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.7.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "06vi55j1mil4la73lgx3np84kqbc9ncls38dxc4y0yg22n5axk83")))

(define-public crate-macro-machines-0.10.0 (c (n "macro-machines") (v "0.10.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.8.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "159p62djkh0x7ljggksv9v2k6h8rsksdk2gss40qzcs241wpyc0h")))

(define-public crate-macro-machines-0.10.1 (c (n "macro-machines") (v "0.10.1") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.8.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "116fs61zqysdvcxra7wr3lk0vbf871grk30wv0kp2gk9qcghsz0y")))

(define-public crate-macro-machines-0.10.2 (c (n "macro-machines") (v "0.10.2") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "simplelog") (r "0.11.*") (d #t) (k 2)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "1bsrvr0bd8y1bfn2sv85wzz73x0ly2a7vx1zj8j4sl4ply0vsp8w")))

(define-public crate-macro-machines-0.10.3 (c (n "macro-machines") (v "0.10.3") (d (list (d (n "env_logger") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "marksman_escape") (r "0.1.*") (d #t) (k 0)) (d (n "unwrap") (r "1.*") (d #t) (k 2)) (d (n "variant_count") (r "1.*") (d #t) (k 0)))) (h "0913rqksbpnbvy39xgldbar5mgc38rlg7v6272f3bd12zvxrnzci")))

