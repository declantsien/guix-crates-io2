(define-module (crates-io ma cr macrowind) #:use-module (crates-io))

(define-public crate-macrowind-0.1.0 (c (n "macrowind") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tailwind-css") (r "^0.11.9") (d #t) (k 0)))) (h "0ica9b090igmghk26d1dgwmnlz2dcwnw6fph6g21xikpdmbc6cd4")))

(define-public crate-macrowind-0.1.1 (c (n "macrowind") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tailwind-css") (r "^0.11.9") (d #t) (k 0)))) (h "1fzwbkgqzs50dj2gg7bizpkkn70vczp9dks2g198xfws8qjx00pb")))

