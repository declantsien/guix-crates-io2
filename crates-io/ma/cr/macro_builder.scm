(define-module (crates-io ma cr macro_builder) #:use-module (crates-io))

(define-public crate-macro_builder-0.1.1 (c (n "macro_builder") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "164nv7d3hmg05gmsmsimdci3jwwy0p2lai7zxp5y0d2b8jfabi4v")))

(define-public crate-macro_builder-0.1.2 (c (n "macro_builder") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "12m18ahrrcz8sq4m3077ykdv0hg6ybnhrq2zhj1zvmdc5hxvv7n7")))

(define-public crate-macro_builder-0.1.3 (c (n "macro_builder") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0n9n9sia9wzxy1ai6v5ga9wadfkw00z7ac2543lnpf53m7d08mvq")))

