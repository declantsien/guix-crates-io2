(define-module (crates-io ma cr macromath) #:use-module (crates-io))

(define-public crate-macromath-0.1.0 (c (n "macromath") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1xqn145hgrj5x7bp61fbj5qn499msml2vps7xazz3jprkr6hiccx")))

