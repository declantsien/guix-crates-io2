(define-module (crates-io ma cr macro_magic_macros) #:use-module (crates-io))

(define-public crate-macro_magic_macros-0.1.0 (c (n "macro_magic_macros") (v "0.1.0") (d (list (d (n "atomicwrites") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fj3izq4jxxv996283zikrwmssh20zb67iamaxxhhcjzrmwhsmhk") (f (quote (("verbose") ("indirect") ("default"))))))

(define-public crate-macro_magic_macros-0.1.1 (c (n "macro_magic_macros") (v "0.1.1") (d (list (d (n "atomicwrites") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnbxgy63ahb0kjfzim00vj8ywvrpby7fz1kk3wn1865s0vgy2mq") (f (quote (("verbose") ("default")))) (s 2) (e (quote (("indirect" "dep:atomicwrites"))))))

(define-public crate-macro_magic_macros-0.1.2 (c (n "macro_magic_macros") (v "0.1.2") (d (list (d (n "atomicwrites") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vahlf79dfm7z9cavgjqdghl33swarr3r3696cmxg0zxaai0akj0") (f (quote (("verbose") ("default")))) (s 2) (e (quote (("indirect" "dep:atomicwrites"))))))

(define-public crate-macro_magic_macros-0.1.3 (c (n "macro_magic_macros") (v "0.1.3") (d (list (d (n "atomicwrites") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zagn88x4m3v9d1ihaappj5y0zd1v2xwil6vz67d93c78mczrhfj") (f (quote (("verbose") ("default")))) (s 2) (e (quote (("indirect" "dep:atomicwrites"))))))

(define-public crate-macro_magic_macros-0.1.4 (c (n "macro_magic_macros") (v "0.1.4") (d (list (d (n "atomicwrites") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l8k5b38qfpn9aqcbfjxsn693zi7dpngs4gvfsdr0b5xjv77h67c") (f (quote (("verbose") ("indirect-read") ("indirect" "indirect-read" "indirect-write") ("default")))) (s 2) (e (quote (("indirect-write" "dep:atomicwrites"))))))

(define-public crate-macro_magic_macros-0.1.5 (c (n "macro_magic_macros") (v "0.1.5") (d (list (d (n "macro_magic_core") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bzlzgqfdd8hrvxhfcc27bdxd4rskyky5md4rrqfdk6zw86ndwck") (f (quote (("verbose") ("indirect-write" "macro_magic_core/indirect-write") ("indirect-read" "macro_magic_core/indirect-read") ("indirect" "indirect-read" "indirect-write" "macro_magic_core/indirect") ("default"))))))

(define-public crate-macro_magic_macros-0.1.6 (c (n "macro_magic_macros") (v "0.1.6") (d (list (d (n "macro_magic_core") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r0nqp8q4vl2riy5ar9v7g0gh0602h16z4pq3631ahs87srl4qzj") (f (quote (("verbose") ("indirect-write" "macro_magic_core/indirect-write") ("indirect-read" "macro_magic_core/indirect-read") ("indirect" "indirect-read" "indirect-write" "macro_magic_core/indirect") ("default"))))))

(define-public crate-macro_magic_macros-0.1.7 (c (n "macro_magic_macros") (v "0.1.7") (d (list (d (n "macro_magic_core") (r "^0.1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gn2yv41aqfm4jpdk7jxr054pgcayfk9zac8n0v409hhaw38jbwp") (f (quote (("verbose") ("indirect-write" "macro_magic_core/indirect-write") ("indirect-read" "macro_magic_core/indirect-read") ("indirect" "indirect-read" "indirect-write" "macro_magic_core/indirect") ("default"))))))

(define-public crate-macro_magic_macros-0.2.0 (c (n "macro_magic_macros") (v "0.2.0") (d (list (d (n "macro_magic_core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zrrzq8p3wdj1w81nb838knran0k4c2cy7w43yq5hi7psrlgfliy")))

(define-public crate-macro_magic_macros-0.2.1 (c (n "macro_magic_macros") (v "0.2.1") (d (list (d (n "macro_magic_core") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f30ycaz5cpyp2prb3d6k6ii5ax5qkk97lx9m1dx6i41lgwkrajl")))

(define-public crate-macro_magic_macros-0.2.2 (c (n "macro_magic_macros") (v "0.2.2") (d (list (d (n "macro_magic_core") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pzcq7j5xp56bkyc1b9fh5pc8j3c1smmj9pl10xj19g8cnm0r11i")))

(define-public crate-macro_magic_macros-0.2.3 (c (n "macro_magic_macros") (v "0.2.3") (d (list (d (n "macro_magic_core") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12azsq6g1afi7q6nsa1f7ykxz4vkl86dqhax3zjyjr7y4pyrpnqk")))

(define-public crate-macro_magic_macros-0.2.4 (c (n "macro_magic_macros") (v "0.2.4") (d (list (d (n "macro_magic_core") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2axgmacqhmsiqjl9k025njhm7vqw797f11d5jnzc1adyjx1vh5")))

(define-public crate-macro_magic_macros-0.2.5 (c (n "macro_magic_macros") (v "0.2.5") (d (list (d (n "macro_magic_core") (r "^0.2.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5h9dqk2wz0jmm7bhvixpcjvawff5c2bb9pdlgcxv6b6wwikqiq")))

(define-public crate-macro_magic_macros-0.2.6 (c (n "macro_magic_macros") (v "0.2.6") (d (list (d (n "macro_magic_core") (r "^0.2.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnswcwk7r3rqdbrhcsd4yhf3xk86fljija2s2pdmvbg6wrbvayc")))

(define-public crate-macro_magic_macros-0.2.7 (c (n "macro_magic_macros") (v "0.2.7") (d (list (d (n "macro_magic_core") (r "^0.2.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "074g8y6j8a2plb1kg95x0kkbywwv87lm1mx2qi2q07a4awh6nyna")))

(define-public crate-macro_magic_macros-0.2.8 (c (n "macro_magic_macros") (v "0.2.8") (d (list (d (n "macro_magic_core") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00hkr3xl3dl4zwfiblw1cgi6q73lcjqys6r78vjfizlk8ab66nly")))

(define-public crate-macro_magic_macros-0.2.9 (c (n "macro_magic_macros") (v "0.2.9") (d (list (d (n "macro_magic_core") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0shwgdx83ix1255aj57jmcynk2yiph9xxdf2f5xq1zlwf4i9vbb1")))

(define-public crate-macro_magic_macros-0.2.10 (c (n "macro_magic_macros") (v "0.2.10") (d (list (d (n "macro_magic_core") (r "^0.2.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jdc6ifnmz80fxilsdffhi4vij05vba0cjb4kgs0875l88j59yhx")))

(define-public crate-macro_magic_macros-0.2.11 (c (n "macro_magic_macros") (v "0.2.11") (d (list (d (n "macro_magic_core") (r "^0.2.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wcx1v0l30znkgnba9rlcbmylbrvxli1w284lissj6zwm1f6r8dj")))

(define-public crate-macro_magic_macros-0.2.12 (c (n "macro_magic_macros") (v "0.2.12") (d (list (d (n "macro_magic_core") (r "^0.2.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17a7z90i2bapxd4xclp5sirdpakim8pd8ip7s2as18zipyqp8axy")))

(define-public crate-macro_magic_macros-0.3.0 (c (n "macro_magic_macros") (v "0.3.0") (d (list (d (n "macro_magic_core") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zszh82avw9y55h6rpai1ab31m2s08vdy2gczk9vrwgxf78gbl61")))

(define-public crate-macro_magic_macros-0.3.1 (c (n "macro_magic_macros") (v "0.3.1") (d (list (d (n "macro_magic_core") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfax68ib1r1553h5rxis8bqxckgwn5pv38r1pgl2viak2566a55")))

(define-public crate-macro_magic_macros-0.3.2 (c (n "macro_magic_macros") (v "0.3.2") (d (list (d (n "macro_magic_core") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fiw2r5119gbh0mcw8xlxzzjkxmkai2k8v5xhbv8ydy8nyb7hjy2")))

(define-public crate-macro_magic_macros-0.3.3 (c (n "macro_magic_macros") (v "0.3.3") (d (list (d (n "macro_magic_core") (r "^0.3.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rczdhd793kypvrqb775va54k468nmy6hz0a4giiq5dsllxhikv8")))

(define-public crate-macro_magic_macros-0.3.4 (c (n "macro_magic_macros") (v "0.3.4") (d (list (d (n "macro_magic_core") (r "^0.3.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n9s7vjdizl5268pxkjqq1i45xfhfqvy33kxi0m6g69fwng97s29")))

(define-public crate-macro_magic_macros-0.3.5 (c (n "macro_magic_macros") (v "0.3.5") (d (list (d (n "macro_magic_core") (r "^0.3.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ga05m2xib73w70mnw1kqbhdlqlmkdr8yfg26prxfbmb2nn7cwfw")))

(define-public crate-macro_magic_macros-0.4.0 (c (n "macro_magic_macros") (v "0.4.0") (d (list (d (n "macro_magic_core") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bf4w2rw672mhqqlfvm5dgwl0niiygj4vhivp3xz2h3mq9b49fj3")))

(define-public crate-macro_magic_macros-0.4.1 (c (n "macro_magic_macros") (v "0.4.1") (d (list (d (n "macro_magic_core") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "10fd0ss56qha9vxs28mq7sdi1rag4j6fzprad61vvgyjrw9rzlgz")))

(define-public crate-macro_magic_macros-0.4.2 (c (n "macro_magic_macros") (v "0.4.2") (d (list (d (n "macro_magic_core") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0amz5p4q8s458blqg2sxcslqiv6ljws6k9w457grwq902vn8byxq")))

(define-public crate-macro_magic_macros-0.4.3 (c (n "macro_magic_macros") (v "0.4.3") (d (list (d (n "macro_magic_core") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxk635d5c2nk884fbim27qdxwpmaf3awj8pxr46zh8y581ydf3d") (y #t)))

(define-public crate-macro_magic_macros-0.5.0 (c (n "macro_magic_macros") (v "0.5.0") (d (list (d (n "macro_magic_core") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvpjpjmpk19bjca3n43b8gdy5sdwfvb43ijcccq5fmajsp7k7gg")))

