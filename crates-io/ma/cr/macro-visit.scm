(define-module (crates-io ma cr macro-visit) #:use-module (crates-io))

(define-public crate-macro-visit-0.1.0 (c (n "macro-visit") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "visit" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1pq83ibxhhckkzm10256df59qilcny91b9k0sqr5c7kyrhk1wrjw")))

(define-public crate-macro-visit-0.2.0 (c (n "macro-visit") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "visit" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1hhwy9idd8qpybvj42zya37m2aqi2m72chi3a85n92mf9g9dhh1d")))

