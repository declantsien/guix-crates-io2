(define-module (crates-io ma cr macrame) #:use-module (crates-io))

(define-public crate-macrame-0.0.1 (c (n "macrame") (v "0.0.1") (d (list (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1qlw94197q8r7gs4ksawr4ymgmx8zy4v48jm0ba36i7b0wiacf8n")))

