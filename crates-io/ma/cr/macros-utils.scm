(define-module (crates-io ma cr macros-utils) #:use-module (crates-io))

(define-public crate-macros-utils-0.1.0 (c (n "macros-utils") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ml9qrc7z3cq2571sdm16ynzjklj3ag5riisihcwdnag4q3ha2gk")))

(define-public crate-macros-utils-0.1.1 (c (n "macros-utils") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lp2m8rzw9da0vgi6ch8mvmqaiahmlxn67h0np35lw2md5h0xigx")))

(define-public crate-macros-utils-0.1.2 (c (n "macros-utils") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n4cfh70g045n5v7rvpi7jw6wscxk7x8y6697wysai8kdkskxr09")))

(define-public crate-macros-utils-0.2.0 (c (n "macros-utils") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xmss4pm6p9pmfqfcaaflprgm8h67yr768qxq4wgy4cdpy5177ap")))

(define-public crate-macros-utils-0.2.1 (c (n "macros-utils") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wjdpxb41a7vy5zmfa6kc76xlyrvni9yfyays67d85x7kim4yfgr")))

