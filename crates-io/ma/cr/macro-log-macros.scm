(define-module (crates-io ma cr macro-log-macros) #:use-module (crates-io))

(define-public crate-macro-log-macros-0.1.0 (c (n "macro-log-macros") (v "0.1.0") (h "0ra0cw7c9376flgqzqvm4mshhhxc6bwkf3gmlvrz70gjs99h4hix")))

(define-public crate-macro-log-macros-0.2.0 (c (n "macro-log-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1rj30s4iz87ryjkvqbdinavywlcm191xk96qzw2bgr4f2374f2za")))

(define-public crate-macro-log-macros-0.3.0 (c (n "macro-log-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "10dvgaw4zp57pj6vzncss48jrwmz1a5mx0k28jm195sh47yr13dq")))

