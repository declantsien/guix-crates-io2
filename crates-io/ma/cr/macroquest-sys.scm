(define-module (crates-io ma cr macroquest-sys) #:use-module (crates-io))

(define-public crate-macroquest-sys-0.1.7 (c (n "macroquest-sys") (v "0.1.7") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "macroquest-build-config") (r "^0.1") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)) (d (n "windows") (r "0.*") (f (quote ("Win32_Foundation" "Win32_System_SystemServices"))) (d #t) (k 0)))) (h "1fljwhl2377gdkrprx1qdblhpi610gmnrd6amczf719qzp60dc54")))

