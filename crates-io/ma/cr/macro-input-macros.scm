(define-module (crates-io ma cr macro-input-macros) #:use-module (crates-io))

(define-public crate-macro-input-macros-0.1.0-pre (c (n "macro-input-macros") (v "0.1.0-pre") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input-core") (r "^0.1.0-pre") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v2w9332r4wvswb42ii1mwa2n1iar5s8nxg19v6w6vp1zk3yc74c")))

(define-public crate-macro-input-macros-0.1.0 (c (n "macro-input-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.1.0-pre") (d #t) (k 2)) (d (n "macro-input-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bwqcqn1dxp0qnx2mb4valaniy39g08hq1r0lir41qzvilwv418x")))

(define-public crate-macro-input-macros-0.2.0 (c (n "macro-input-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.2.0") (d #t) (k 2)) (d (n "macro-input-core") (r "^0.2.0") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1158f5sjl0v8qyqqyavjjxvyd2y4gacz8fzx0filr1phixd64inq")))

(define-public crate-macro-input-macros-0.3.0 (c (n "macro-input-macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.3.0") (d #t) (k 2)) (d (n "macro-input-core") (r "^0.3.0") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ls4301x2dcj81157rhvcf9p4myr26zkcxzh0skzp2jfp2p972sk")))

(define-public crate-macro-input-macros-0.3.1 (c (n "macro-input-macros") (v "0.3.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.3.1") (d #t) (k 2)) (d (n "macro-input-core") (r "^0.3.1") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13zhs3jmwkrpc0mglhzgbv920y2c8p04dmc451rzqkyxs987w7ag")))

