(define-module (crates-io ma cr macro-gpt) #:use-module (crates-io))

(define-public crate-macro-gpt-0.1.0 (c (n "macro-gpt") (v "0.1.0") (h "00nsryl47yik224izb2r9l1kvpvx2sbm57ibjhx5q2i9ijd2yvsl")))

(define-public crate-macro-gpt-0.1.2 (c (n "macro-gpt") (v "0.1.2") (d (list (d (n "chatgpt-functions") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0r48xjfbi55h2f3i5fipj2jf27rg5ln84mk9fhvs59a9zhivj1ky")))

