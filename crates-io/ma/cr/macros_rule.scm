(define-module (crates-io ma cr macros_rule) #:use-module (crates-io))

(define-public crate-macros_rule-0.1.0 (c (n "macros_rule") (v "0.1.0") (h "0ksgim2d93s2976lrly014w8sbah57skarw4rx8kdz1bkasgnc4s") (y #t)))

(define-public crate-macros_rule-0.1.1 (c (n "macros_rule") (v "0.1.1") (h "00nsbbc19gv7ys8rr2059v2b7fc3znh11d5fac39qnqwcdc7z9wr") (y #t)))

(define-public crate-macros_rule-0.1.2 (c (n "macros_rule") (v "0.1.2") (h "1anrggyd2y1qccf21662wld0fgn6izk7c4fygbippg915h5q550x") (y #t)))

