(define-module (crates-io ma cr macroex-derive) #:use-module (crates-io))

(define-public crate-macroex-derive-0.1.0 (c (n "macroex-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0cb74kbcssh6x1qsil51i9cg3gv6g0pmv98scm99da5xfh7wcwhn")))

(define-public crate-macroex-derive-0.1.1 (c (n "macroex-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "15ywmayqjhac3qvny35mnv0pcar8jmyiklahyy5wzalg8wpnxysl")))

(define-public crate-macroex-derive-0.1.2 (c (n "macroex-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0idsg8brhmq8i4x3svhy0agna7gzjgffp2frw95pbma6rmnfly3y")))

(define-public crate-macroex-derive-0.2.0 (c (n "macroex-derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1h0y6cjshnq6ldzh960kzsyf0rx3yl7g6vad4jj4cx746sz3x3mn")))

