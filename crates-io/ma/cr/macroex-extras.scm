(define-module (crates-io ma cr macroex-extras) #:use-module (crates-io))

(define-public crate-macroex-extras-0.1.0 (c (n "macroex-extras") (v "0.1.0") (d (list (d (n "macroex") (r "^0.1.5") (k 0)) (d (n "parse-color") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)))) (h "1jkyqvf19hxxk9d7nsbh7dz60savhba76bz9filf95yhh8d8dcvw")))

(define-public crate-macroex-extras-0.1.1 (c (n "macroex-extras") (v "0.1.1") (d (list (d (n "macroex") (r "^0.1.5") (k 0)) (d (n "parse-color") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)))) (h "057ss93m6r6y7xpdvgp3ikdzz4x15w08aj5sslw2a2w8vjrwi1gm")))

(define-public crate-macroex-extras-0.1.2 (c (n "macroex-extras") (v "0.1.2") (d (list (d (n "macroex") (r "^0.1.5") (k 0)) (d (n "parse-color") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)))) (h "1wbbzh8qq3kwbhrwcr1h8ahbyql207m0rdbjxhmws8jw4b8ipw4m")))

(define-public crate-macroex-extras-0.1.3 (c (n "macroex-extras") (v "0.1.3") (d (list (d (n "macroex") (r "^0.1.5") (k 0)) (d (n "parse-color") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)))) (h "1k1sh6dhllj931gmfrykykcnnrcy3zim9qhwwab1r1wx4kda4aq4")))

(define-public crate-macroex-extras-0.2.0 (c (n "macroex-extras") (v "0.2.0") (d (list (d (n "macroex") (r "^0.1.5") (k 0)) (d (n "parse-color") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)))) (h "1x748hw39xx8mhrgq5rmrswz4qv9chvbbyhwvxlpfl8dvlk8xgsg")))

