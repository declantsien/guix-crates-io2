(define-module (crates-io ma cr macrrou) #:use-module (crates-io))

(define-public crate-macrrou-0.1.0 (c (n "macrrou") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jb0wxkv7q2zkikmh69jsc2yhw2x1w0znwacm2v3qk00pr7rvhmp") (r "1.60")))

