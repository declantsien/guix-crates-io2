(define-module (crates-io ma cr macro-utils) #:use-module (crates-io))

(define-public crate-macro-utils-0.1.0 (c (n "macro-utils") (v "0.1.0") (h "0ikzrq44cpc2q93ar5xzpndrbhqbjsyy349ibq954485ln3j95f0")))

(define-public crate-macro-utils-0.1.1 (c (n "macro-utils") (v "0.1.1") (h "1grc3a7csh8xwk6xfwxp25f6xf5fjvd9g4f8x9njz9jwhzjf3q2h")))

(define-public crate-macro-utils-0.1.2 (c (n "macro-utils") (v "0.1.2") (h "04551q5rnmbfhx0cw5bl75sn8aym0wpsh2vcq4l6mb9frjndxi7j")))

(define-public crate-macro-utils-0.1.3 (c (n "macro-utils") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1a38zbwalnx20rjalb71zazx0qw7g3xfn2i9gpmakzjqnzggfwhf")))

