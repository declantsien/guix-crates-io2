(define-module (crates-io ma cr macro-field-utils) #:use-module (crates-io))

(define-public crate-macro-field-utils-0.1.0 (c (n "macro-field-utils") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0nmv6wwzgcxpsv93gs8y06ybhk04dlcwvga2935dwi1cqbs8f7ih")))

(define-public crate-macro-field-utils-0.2.0 (c (n "macro-field-utils") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1fy384al08jy5blfhfhyfj38mhviq2ylmfa6adbfc0jc13if1dk6")))

