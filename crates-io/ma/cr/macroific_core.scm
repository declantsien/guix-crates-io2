(define-module (crates-io ma cr macroific_core) #:use-module (crates-io))

(define-public crate-macroific_core-1.0.0 (c (n "macroific_core") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ljvlgacviawj2qq4iq2gy1wfr1mip5835xx7xsf8mzw8lw4q11h") (f (quote (("nightly")))) (r "1.60.0")))

(define-public crate-macroific_core-1.0.1 (c (n "macroific_core") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1yxyxhbhl2invvdga4fdka2hxra1kc0bqfspss018z7kz8nr1965") (f (quote (("nightly")))) (r "1.60.0")))

(define-public crate-macroific_core-1.0.2 (c (n "macroific_core") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0may8c4ryb3khm0bsplpnwr9k5idcx3zjgybbib7l2b41098q68k") (f (quote (("nightly")))) (r "1.60.0")))

