(define-module (crates-io ma cr macro-bits) #:use-module (crates-io))

(define-public crate-macro-bits-0.1.0 (c (n "macro-bits") (v "0.1.0") (d (list (d (n "bin-utils") (r "^0.2.1") (d #t) (k 2)) (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "0009yfw1ywc23p9in9w0cm67mz2rnmk02ljb5dnmmd1b03mxg4gk")))

(define-public crate-macro-bits-0.1.1 (c (n "macro-bits") (v "0.1.1") (d (list (d (n "bin-utils") (r "^0.2.1") (d #t) (k 2)) (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "0fvliks0nhz5xsygkk9wv8b0qin1904k46106y9nzlzxsgjqcs62")))

(define-public crate-macro-bits-0.1.2 (c (n "macro-bits") (v "0.1.2") (d (list (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "0b9540dac684xkif7n2ysl7xzgqzhv9pfdk1wqrba8by3i0q6fcj")))

(define-public crate-macro-bits-0.1.3 (c (n "macro-bits") (v "0.1.3") (d (list (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "1ii47jp4wrmk4vddfca373w61q3z668nqkvilz5y7lqm28rqx8y9")))

(define-public crate-macro-bits-0.1.4 (c (n "macro-bits") (v "0.1.4") (d (list (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "0bjhdh9638hx7bysfrqycv3sq85myjvxqa18xvi248gx8vfy4zv9")))

(define-public crate-macro-bits-0.1.5 (c (n "macro-bits") (v "0.1.5") (d (list (d (n "defile") (r "^0.2.1") (d #t) (k 0)))) (h "16hc4s3w8hzjs0ifabdby7b86xx0lf0k9wv0dkwxkv4i8mz8ddvf")))

