(define-module (crates-io ma cr macro_rules_attribute-proc_macro) #:use-module (crates-io))

(define-public crate-macro_rules_attribute-proc_macro-0.0.1 (c (n "macro_rules_attribute-proc_macro") (v "0.0.1") (h "0icvsnl3nxgyfman2f870rwgbsg3gqcl6mm51cpsbawkkpi3aglv") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.0.2 (c (n "macro_rules_attribute-proc_macro") (v "0.0.2") (h "1635f3wk1fvzs741p3smvyf9757jhh7rm7qcx6vbhiwcbbd6l97b") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.0-rc1 (c (n "macro_rules_attribute-proc_macro") (v "0.1.0-rc1") (h "05101cwdvcnb21iggrjarqgip5i64sdag3m36nydllckdan0gnw1") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.0-rc2 (c (n "macro_rules_attribute-proc_macro") (v "0.1.0-rc2") (h "02641dr0ziikhhv6fpsj73hw6jlf8690cp6z9ka6isky2nvw4v18") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.0 (c (n "macro_rules_attribute-proc_macro") (v "0.1.0") (h "1akldpfd2mpc0vlp59x6rvsv1nkjndgjg42h30qnp70xdx5qymk1") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.1-rc1 (c (n "macro_rules_attribute-proc_macro") (v "0.1.1-rc1") (h "0r6fx3vx9z1hgmz06g8pz4m3wjnaaisaywrycykqfx8drpba28gi") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.1-rc2 (c (n "macro_rules_attribute-proc_macro") (v "0.1.1-rc2") (h "1yx1pd4cfhd8rwxb24xl7x4ygswfnzbni71cl61b45kdn5s0y9vv") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.1 (c (n "macro_rules_attribute-proc_macro") (v "0.1.1") (h "0jhbski28nz1qdhq7xhli1f1vf6jgi0kknldmskv65gmwz992h5v") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.2-rc1 (c (n "macro_rules_attribute-proc_macro") (v "0.1.2-rc1") (h "0nf3n6l9wwjfnp7kbbxkrsxc7anhdfbiq3sky3wri5gxp07irpdp") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.2 (c (n "macro_rules_attribute-proc_macro") (v "0.1.2") (h "1spxkwda0yp7blxwwyaif82ryagbf1sbm57l3r0lvanm08jqsspj") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.3 (c (n "macro_rules_attribute-proc_macro") (v "0.1.3") (h "079bmj5h2yz4zbkhc5fhq2xsz5ikgkkpd3shbiywf02ylha362aq") (f (quote (("verbose-expansions"))))))

(define-public crate-macro_rules_attribute-proc_macro-0.1.4 (c (n "macro_rules_attribute-proc_macro") (v "0.1.4") (h "1jnv478qrcr0fcgnbhqp8nm11zr0mzlnsxkcr3fgkm0b273r9dw9") (f (quote (("verbose-expansions")))) (y #t)))

(define-public crate-macro_rules_attribute-proc_macro-0.2.0 (c (n "macro_rules_attribute-proc_macro") (v "0.2.0") (h "0s45j4zm0a5d041g3vcbanvr76p331dfjb7gw9qdmh0w8mnqbpdq") (f (quote (("verbose-expansions"))))))

