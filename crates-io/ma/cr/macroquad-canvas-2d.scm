(define-module (crates-io ma cr macroquad-canvas-2d) #:use-module (crates-io))

(define-public crate-macroquad-canvas-2d-0.1.0 (c (n "macroquad-canvas-2d") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "1mm86jl0v4xl6d440fp5idwy1lqjiikk6b7jkga5lmpd5dxp1j9m")))

(define-public crate-macroquad-canvas-2d-0.1.1 (c (n "macroquad-canvas-2d") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "1rnbhx6hiw89iybpzqxjpgm9g3in801cmsy4jv9mvb6ip18210w1")))

(define-public crate-macroquad-canvas-2d-0.1.2 (c (n "macroquad-canvas-2d") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1h84bhn1fq6m1psd33p5353xizcgigsn3p30ipc58ba088gd7hv0")))

(define-public crate-macroquad-canvas-2d-0.1.3 (c (n "macroquad-canvas-2d") (v "0.1.3") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0r4rx9a6bpj7rfn5msagkrbgd2ijwq8zjzji7ggs1jzqnwk03v1d")))

(define-public crate-macroquad-canvas-2d-0.2.0 (c (n "macroquad-canvas-2d") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1pb5mnqvy7sin3byj055vpvb0iv8xkbki7fllwm8yf7s1n2pq0nf")))

(define-public crate-macroquad-canvas-2d-0.2.1 (c (n "macroquad-canvas-2d") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0w62kdmj7liwgwg66rflrggj87fpqxbiizsz44bg3ll7xn28x5ax")))

(define-public crate-macroquad-canvas-2d-0.2.2 (c (n "macroquad-canvas-2d") (v "0.2.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0ky36xvv5a906csjs3vl3rcvkmlcs9w6nnkyxaxwr2fcy1z36ria")))

(define-public crate-macroquad-canvas-2d-0.2.3 (c (n "macroquad-canvas-2d") (v "0.2.3") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1fa7h9dy0q08hk67gxz3zdvchslq3dqx4vmv2m7q4csq4kavyglp")))

(define-public crate-macroquad-canvas-2d-0.3.0 (c (n "macroquad-canvas-2d") (v "0.3.0") (d (list (d (n "macroquad") (r "^0.4") (d #t) (k 0)))) (h "0ifp8albxnmdvci1gwfmgiprsrkayxv8fgy88zzwffl2fcqsq2qz")))

(define-public crate-macroquad-canvas-2d-0.3.1 (c (n "macroquad-canvas-2d") (v "0.3.1") (d (list (d (n "macroquad") (r "^0.4") (d #t) (k 0)))) (h "07rb1fgh4vwb688iwbvixv5i1l8z9f6gbskrqgm6qcdfjdng3sh0")))

