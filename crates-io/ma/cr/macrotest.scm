(define-module (crates-io ma cr macrotest) #:use-module (crates-io))

(define-public crate-macrotest-0.1.0 (c (n "macrotest") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "168zgyzmp0hv9a51afdwkn5853jqpx901q7b8a4hykswr2az86v7")))

(define-public crate-macrotest-0.1.1 (c (n "macrotest") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0yd9dxirb0yrravwhrcczvz9639c7lhyvy3930d7cc5xlikvnz8y")))

(define-public crate-macrotest-0.1.2 (c (n "macrotest") (v "0.1.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0v96cgavgpsxqc1dn7vqqmgp1431qp5znk5hkwblzkramwb948l2")))

(define-public crate-macrotest-0.1.3 (c (n "macrotest") (v "0.1.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "17v6dwcybpymdkm9r2sqj703fsgk1vhxvic81iblck5r3l2qpdwb")))

(define-public crate-macrotest-0.1.4 (c (n "macrotest") (v "0.1.4") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0yvr30lxcv67h7zv11ihrr73hjcbx2ypw2n6w2pnv9y0csp86xxr")))

(define-public crate-macrotest-0.1.5 (c (n "macrotest") (v "0.1.5") (d (list (d (n "difference") (r "^2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15rhfp0qf6s1q75njwrpkzkshs7c5wl0ff9w4cnydy1pws2wv8nl")))

(define-public crate-macrotest-0.1.6 (c (n "macrotest") (v "0.1.6") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p8cfg7gq9dpjfd692l5r3rq6hbvh5fm2hhn0qhlmcc83c4aapfz")))

(define-public crate-macrotest-1.0.0 (c (n "macrotest") (v "1.0.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1aa5wyxlk8ysma4c7b8bqv4sha9smw6rs3dkrxgrf7lqr8mahphd")))

(define-public crate-macrotest-1.0.1 (c (n "macrotest") (v "1.0.1") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0f5gv49q9v5jsfa8cp722m9vdhmx5whp4xlxbs5skzm8alhlgqa8") (y #t)))

(define-public crate-macrotest-1.0.2 (c (n "macrotest") (v "1.0.2") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dfsvv16lj3npdii97rhyz120b8lgy1ampshhvl0agiq0k2g5l8q")))

(define-public crate-macrotest-1.0.3 (c (n "macrotest") (v "1.0.3") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bfdc4iivd4i3kiq899xm9gn731x063hws0s5x4f59d17cx8gcia")))

(define-public crate-macrotest-1.0.4 (c (n "macrotest") (v "1.0.4") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0fzb3mw0sbg5scn56n2j47d6hkn651bq13an08zr71h6n7bwm8x8")))

(define-public crate-macrotest-1.0.5 (c (n "macrotest") (v "1.0.5") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "00a9qdv1dada7i2lmm4gwvwj8mc77bkyqvvvm4kd6ixi1vdq3ycw")))

(define-public crate-macrotest-1.0.6 (c (n "macrotest") (v "1.0.6") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "115mvrprqz9n3gbbp7nvvizibkshm5g08nym5b1jwkfgq7hgkqkq")))

(define-public crate-macrotest-1.0.7 (c (n "macrotest") (v "1.0.7") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "11dl8kbmsgigkqjs0d69ir5hm0li6qqsd22h0rsh7b0dqh8knlw2")))

(define-public crate-macrotest-1.0.8 (c (n "macrotest") (v "1.0.8") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06xk9i9amh325vr6w9dmnlxfp6zamrq57zfl031zd0fscqm3vjx2")))

(define-public crate-macrotest-1.0.9 (c (n "macrotest") (v "1.0.9") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "043gc53ch1szw7ihdclnygn464v62viw48iigd5l2iffhq4sx2bl")))

(define-public crate-macrotest-1.0.10 (c (n "macrotest") (v "1.0.10") (d (list (d (n "basic-toml") (r "^0.1") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybna6a1i0nqqg5wz44i3v4l93251k61q5i2d3f0whqnryh4ifkn") (r "1.56")))

(define-public crate-macrotest-1.0.11 (c (n "macrotest") (v "1.0.11") (d (list (d (n "basic-toml") (r "^0.1") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13jj06rlik3br1qkynzpivgibvhcd4d0iwbagmrhp839kn6h578i") (r "1.56")))

(define-public crate-macrotest-1.0.12 (c (n "macrotest") (v "1.0.12") (d (list (d (n "basic-toml") (r "^0.1") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0badhr8qgr80w3r5ngzx238wlrwdwi1qnbjpdnv0fjvijrp08wy3") (r "1.56")))

