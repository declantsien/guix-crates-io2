(define-module (crates-io ma cr macro-input) #:use-module (crates-io))

(define-public crate-macro-input-0.1.0-pre (c (n "macro-input") (v "0.1.0-pre") (d (list (d (n "macro-input-core") (r "^0.1.0-pre") (d #t) (k 0)) (d (n "macro-input-macros") (r "^0.1.0-pre") (o #t) (d #t) (k 0)))) (h "1jdgjclqgz13lyjh10gbwsy6dpf23qmdkwmviq3y4innbrqawz25") (f (quote (("macros" "macro-input-macros") ("default" "macros"))))))

(define-public crate-macro-input-0.1.0 (c (n "macro-input") (v "0.1.0") (d (list (d (n "macro-input-core") (r "^0.1.0") (d #t) (k 0)) (d (n "macro-input-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ayfv8bi8yksplks147iwsh966cq4pdbxvz96cnvmrg2icwcnr33") (f (quote (("macros" "macro-input-macros") ("default" "macros"))))))

(define-public crate-macro-input-0.2.0 (c (n "macro-input") (v "0.2.0") (d (list (d (n "macro-input-core") (r "^0.2.0") (d #t) (k 0)) (d (n "macro-input-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 2)))) (h "1gm9783sqkhgbvjwmwy7h6a73xy85wq2wyahxzi49qsrm0rfwhhg") (f (quote (("macros" "macro-input-macros") ("default" "macros"))))))

(define-public crate-macro-input-0.3.0 (c (n "macro-input") (v "0.3.0") (d (list (d (n "macro-input-core") (r "^0.3.0") (d #t) (k 0)) (d (n "macro-input-macros") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02n2axcyf1g0zsdx36qp69rr45lq6jg94xqi1qxwbv61bz9hgffr") (f (quote (("macros" "macro-input-macros") ("default" "macros"))))))

(define-public crate-macro-input-0.3.1 (c (n "macro-input") (v "0.3.1") (d (list (d (n "macro-input-core") (r "^0.3.1") (d #t) (k 0)) (d (n "macro-input-macros") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "064wwwqrzj19r5kb1457kk3706yr23hm46x3ppxld061za083a4h") (f (quote (("macros" "macro-input-macros") ("default" "macros"))))))

