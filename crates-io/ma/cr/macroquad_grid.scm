(define-module (crates-io ma cr macroquad_grid) #:use-module (crates-io))

(define-public crate-macroquad_grid-0.1.0 (c (n "macroquad_grid") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)))) (h "1byjdrhs5k0ycfpkvgd8x2xysbcivmn7v3m3443z181sib36dhpv")))

(define-public crate-macroquad_grid-0.1.1 (c (n "macroquad_grid") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)))) (h "0jnzmfj80nrbg66mp4c5wh6x4nw6piin14dh0w6fgwaz3p7r0d8n")))

