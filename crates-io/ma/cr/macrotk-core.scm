(define-module (crates-io ma cr macrotk-core) #:use-module (crates-io))

(define-public crate-macrotk-core-0.1.0 (c (n "macrotk-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03lqm4mwgi7lylcgd8668cyv4gj60pdlp4kc25kbsdnrw13km5s0")))

(define-public crate-macrotk-core-0.1.1 (c (n "macrotk-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "19al5pkkyfjpbgwz3fyqhisnn5sv4sn8dfwfcpp6c1rawc3ljn0i")))

(define-public crate-macrotk-core-0.2.0 (c (n "macrotk-core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1bz3hfb5hdcqmc7r8863l3zkr42acv0h96z61d49db8hq0i6kznk")))

