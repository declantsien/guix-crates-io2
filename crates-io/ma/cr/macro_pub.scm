(define-module (crates-io ma cr macro_pub) #:use-module (crates-io))

(define-public crate-macro_pub-0.1.0 (c (n "macro_pub") (v "0.1.0") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "08cgqn1wbhv43lnzjyb2hl1d1hk8psdiplbpvsxcyvr52c729pnf") (r "1.56")))

