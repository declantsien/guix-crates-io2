(define-module (crates-io ma cr macro-input-core) #:use-module (crates-io))

(define-public crate-macro-input-core-0.1.0-pre (c (n "macro-input-core") (v "0.1.0-pre") (d (list (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1affsvbmaxbqarl2ji7s9zayncfyxx230xk14jpdws6dz1q3lqyp")))

(define-public crate-macro-input-core-0.1.0 (c (n "macro-input-core") (v "0.1.0") (d (list (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.1.0-pre") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0agmqz1s6nh0xbk7ksz113g0pa20a3mkpvd9llqq3rvfd8lg3miv")))

(define-public crate-macro-input-core-0.2.0 (c (n "macro-input-core") (v "0.2.0") (d (list (d (n "macro-compose") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vyhwlrccai592mlca91q5x3vpz7m5anspq1hjcl3iar54wr42l7") (f (quote (("legacy" "macro-compose") ("default"))))))

(define-public crate-macro-input-core-0.3.0 (c (n "macro-input-core") (v "0.3.0") (d (list (d (n "macro-compose") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b1yd8yayfgzlxy9jjm5xxd4l3hpvg3m9msilwk0wz7lg003d2qq") (f (quote (("legacy" "macro-compose") ("default"))))))

(define-public crate-macro-input-core-0.3.1 (c (n "macro-input-core") (v "0.3.1") (d (list (d (n "macro-compose") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p5k2sq34khciz74xbr6bib6i207vi6mxyf4sdd7vmlrsjpcxjfc") (f (quote (("legacy" "macro-compose") ("default"))))))

