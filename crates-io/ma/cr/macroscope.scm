(define-module (crates-io ma cr macroscope) #:use-module (crates-io))

(define-public crate-macroscope-0.1.0 (c (n "macroscope") (v "0.1.0") (d (list (d (n "macroscope-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "macroscope-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rvgrxr7l1a3y9kqsnmpl1yij611m34kf2xcjbl20axddl0nia2x")))

(define-public crate-macroscope-0.1.1 (c (n "macroscope") (v "0.1.1") (d (list (d (n "macroscope-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "macroscope-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13522mcch0wwzqc0m0mc8gaghxva8x28n0m0rgw87cc85vg16sa6")))

