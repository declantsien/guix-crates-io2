(define-module (crates-io ma cr macro_state) #:use-module (crates-io))

(define-public crate-macro_state-0.1.0 (c (n "macro_state") (v "0.1.0") (h "1ampxhm02halq3jfv64w1h7wxb33yx6cclvqbr1m4z1hzhxvajqc")))

(define-public crate-macro_state-0.1.1 (c (n "macro_state") (v "0.1.1") (h "0l9wzn9gdhja9iy6b68762ar460vjl8ka85j2bah0gviskvpxyv8")))

(define-public crate-macro_state-0.1.2 (c (n "macro_state") (v "0.1.2") (h "1dmv0rdc46vw7hih5fph4rxh7wfwyi3qg4bp5p52zbw8s97m8m54")))

(define-public crate-macro_state-0.1.3 (c (n "macro_state") (v "0.1.3") (h "1vb0x68r37xxil89l00sl2brhxfmkbzw0sd1xhad43myanfcvg24")))

(define-public crate-macro_state-0.1.4 (c (n "macro_state") (v "0.1.4") (d (list (d (n "macro_state_macros") (r "^0.1.4") (d #t) (k 0)))) (h "0ja2v94c75fpymxxnvvxymbjixksim37p2q7cj327qpiggjlih3c")))

(define-public crate-macro_state-0.1.5 (c (n "macro_state") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.1.4") (d #t) (k 0)))) (h "01332cqfxv0phb66qxcjvd5hixrpj1apn7i2np6ys99ihnwdg8g9")))

(define-public crate-macro_state-0.1.6 (c (n "macro_state") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.1.6") (d #t) (k 0)))) (h "1a84xkxbmviv1pjpzdmncdnqvpsx3qcjh0ksnwi4vgxhl5g20p9v")))

(define-public crate-macro_state-0.1.7 (c (n "macro_state") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.1.7") (d #t) (k 0)))) (h "1rg3m523ls9l60ml5pw2bfc5mlfhw1wdsi6bi4z65qxj30nj3mhw")))

(define-public crate-macro_state-0.1.8 (c (n "macro_state") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.1.7") (d #t) (k 0)))) (h "01wzizkxrbp2pa55nkyhwzz77mx5ilzby7i2msbwnqfl0xh39h1i")))

(define-public crate-macro_state-0.1.9 (c (n "macro_state") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.1.7") (d #t) (k 0)))) (h "100b20wlziykd698hicn97k5gax7a9lj2db6g4r3fva8ji7k081g")))

(define-public crate-macro_state-0.2.0 (c (n "macro_state") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macro_state_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1r0ampbbf9pk5yf6mmc0psmrwzq6qxxl2wiszcadb1adp1i7cbvp")))

