(define-module (crates-io ma cr macro_lisp) #:use-module (crates-io))

(define-public crate-macro_lisp-0.1.0 (c (n "macro_lisp") (v "0.1.0") (h "1gqijngii0zn7sf7w58qky2137d0jm8kh95539bjxprm8ysjvm21")))

(define-public crate-macro_lisp-0.2.0 (c (n "macro_lisp") (v "0.2.0") (h "1miil8zqxxg5sn9vkb9pxmwcimam4gy58l7lfpn8lqswajvss63w")))

