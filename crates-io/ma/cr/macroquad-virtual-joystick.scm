(define-module (crates-io ma cr macroquad-virtual-joystick) #:use-module (crates-io))

(define-public crate-macroquad-virtual-joystick-0.1.0 (c (n "macroquad-virtual-joystick") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0xb9lf13mgj93pj3ha4w7nppdk1nid1qghsr4vd83nys61zizrs8")))

(define-public crate-macroquad-virtual-joystick-0.2.0 (c (n "macroquad-virtual-joystick") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0pkbg2jnxk1vdji33q6fm65g651frlkf9n9mqh5dyxgc4wa71239")))

