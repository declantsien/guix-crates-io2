(define-module (crates-io ma cr macro-vis) #:use-module (crates-io))

(define-public crate-macro-vis-0.1.0 (c (n "macro-vis") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0y8hzw47avkzihyhr3ymjh93aspa83dw81pwi9zl77p50m582x0n") (r "1.53")))

(define-public crate-macro-vis-0.1.1 (c (n "macro-vis") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0ky3695r3ms226gqi3lr2n88q879ya6b7f1fsl6mjl4ir10icl6f") (r "1.53")))

