(define-module (crates-io ma cr macrotk-derive) #:use-module (crates-io))

(define-public crate-macrotk-derive-0.1.0 (c (n "macrotk-derive") (v "0.1.0") (d (list (d (n "macrotk-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0liqxf6r4w5gq80msjixb6b2y210qb485h0rnnvp8m6yqjvb5zrv")))

(define-public crate-macrotk-derive-0.1.1 (c (n "macrotk-derive") (v "0.1.1") (d (list (d (n "macrotk-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kijvhxn7kghhxnga8ycq9k5dlfwzczbwbgxsl4039g97k42dw4g")))

(define-public crate-macrotk-derive-0.2.0 (c (n "macrotk-derive") (v "0.2.0") (d (list (d (n "macrotk-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d6rg9pgqpvmgqmi1qgrr634qmslq7znzdl8k1z7i0vkmzl6xwba")))

