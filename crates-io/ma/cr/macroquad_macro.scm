(define-module (crates-io ma cr macroquad_macro) #:use-module (crates-io))

(define-public crate-macroquad_macro-0.1.0 (c (n "macroquad_macro") (v "0.1.0") (h "02j6n0rxq7wq6wyqndcqfwzmlscyca8mfq65p1aa6p05vi72cg9y")))

(define-public crate-macroquad_macro-0.1.1 (c (n "macroquad_macro") (v "0.1.1") (h "08wj733g9wljkpvnx9qz0cwx8g3zyjgm2vr58giyf99283s8838k")))

(define-public crate-macroquad_macro-0.1.2 (c (n "macroquad_macro") (v "0.1.2") (h "12mgx8v0j94njagfx1smgz4cgx9f90h1nw55rghgcr5kqb77gp48")))

(define-public crate-macroquad_macro-0.1.3 (c (n "macroquad_macro") (v "0.1.3") (h "0xbqaiwsa1v3wh8m1vkrs3dc5vs7qi68zvk2670c0h1l20nl9qi0")))

(define-public crate-macroquad_macro-0.1.4 (c (n "macroquad_macro") (v "0.1.4") (h "1yx11qa1jwaigi8if43y2446n1bjgc5hni3z1s8r1hxps542y3yn")))

(define-public crate-macroquad_macro-0.1.5 (c (n "macroquad_macro") (v "0.1.5") (h "0bdl2dr2ywh0pdvbmyz9znci9bjp526vvh1r8hvvhx3aphsqkjvf")))

(define-public crate-macroquad_macro-0.1.6 (c (n "macroquad_macro") (v "0.1.6") (h "0188wxwjahl094isqs27hxsyyc9pjzw6kv38i14nq5fhmfv43nqm")))

(define-public crate-macroquad_macro-0.1.7 (c (n "macroquad_macro") (v "0.1.7") (h "1j7wwdy9kz1s8bfach9psv9h34a817bg5xw6hsf5jc75w7nwzkpm")))

