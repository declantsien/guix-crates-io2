(define-module (crates-io ma cr macro_colors) #:use-module (crates-io))

(define-public crate-macro_colors-0.1.0 (c (n "macro_colors") (v "0.1.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0x22yiw8kviz18kvjk9ijl0rn0phmsdlanf9ci14yy0946qilfbj")))

(define-public crate-macro_colors-0.2.0 (c (n "macro_colors") (v "0.2.0") (h "06zciv3mf8lspc68xv4lijcln9pywadqj6dqhjk5pn4f4wwll581")))

