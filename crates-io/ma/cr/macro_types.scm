(define-module (crates-io ma cr macro_types) #:use-module (crates-io))

(define-public crate-macro_types-0.1.0 (c (n "macro_types") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06iykjfi612pdpyb1673m99w4sms885i8fkn111rqsqdmpi9fvgb")))

(define-public crate-macro_types-0.2.0 (c (n "macro_types") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b7py98rlhdsimhharil3jmgzc1bqa73812hhvjblhnq1pg3xry3")))

(define-public crate-macro_types-0.3.0 (c (n "macro_types") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17m3gwgznqqpgankvkgzdqjqdlgsby506kwkxh5ii3i2lbq4ss87")))

(define-public crate-macro_types-0.4.0 (c (n "macro_types") (v "0.4.0") (d (list (d (n "macro_types_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h58kzqnvcw88g8mmc9biw4w52yg3skghaafsydslc8mlq8khqj8")))

(define-public crate-macro_types-0.5.0 (c (n "macro_types") (v "0.5.0") (d (list (d (n "macro_types_helpers") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pvwpgyl1p5ydziwcxa6yrws2l4qrjqcnp8fp1y96f3bpv0q9mvm")))

(define-public crate-macro_types-0.5.1 (c (n "macro_types") (v "0.5.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "macro_types_helpers") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17b3lvin4sq91yv3gsfbcs55i44w64vpi1amzg1cxrym1z09cqzd")))

(define-public crate-macro_types-0.5.2 (c (n "macro_types") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "macro_types_helpers") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16jppsamg3n77y40fy22m1faa8jlrv5685g6p2cvlsq24xbn0075")))

