(define-module (crates-io ma cr macro_helper) #:use-module (crates-io))

(define-public crate-macro_helper-0.1.0 (c (n "macro_helper") (v "0.1.0") (h "1m32kvlqkxvd57vnysijzrqqy463dkz4fg6p83l0sjg4dq17y4dw")))

(define-public crate-macro_helper-0.1.1 (c (n "macro_helper") (v "0.1.1") (h "157yqdgvdrsdl38xmlzpkq0gvslk0xknxb30c85kwby8xkc4f7iv")))

(define-public crate-macro_helper-0.1.2 (c (n "macro_helper") (v "0.1.2") (h "01b0hfjlzrq00y5i785dnwwjs8vw70qqs8zrqcjc7hgmgri4hrii")))

(define-public crate-macro_helper-0.1.3 (c (n "macro_helper") (v "0.1.3") (h "12sgva36yy03bxzb9y3vblr08f910w3dp3rxgj92fc0q7wb09k61")))

(define-public crate-macro_helper-0.1.4 (c (n "macro_helper") (v "0.1.4") (h "082c931k7i50zz9csffcwa5iqb7rppj07cj8wmjk0b309l3697l5")))

(define-public crate-macro_helper-0.1.5 (c (n "macro_helper") (v "0.1.5") (h "00a17b1cvpwci7mzlqqpv99vkci3r8wn9yi5n086vk5k77chssd0")))

(define-public crate-macro_helper-0.1.6 (c (n "macro_helper") (v "0.1.6") (h "1xfaz9wr2ifbbfp4vb3vlgkv33lmd7r34x6cgq0031gp08jbymcf")))

