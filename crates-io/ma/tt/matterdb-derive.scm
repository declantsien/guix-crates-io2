(define-module (crates-io ma tt matterdb-derive) #:use-module (crates-io))

(define-public crate-matterdb-derive-1.0.0 (c (n "matterdb-derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s9i85zlvpdp60vl9939zpili9mw5l7rgaqxs0zgnsa295v3k6d1")))

