(define-module (crates-io ma tt mattress) #:use-module (crates-io))

(define-public crate-mattress-0.1.0 (c (n "mattress") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (k 0)))) (h "1c2zdm1d7ds3dwwhk4x9nn83s880373p1vf4s9jb7mh7x0vp3jgw")))

(define-public crate-mattress-0.2.0 (c (n "mattress") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (k 0)))) (h "0lfkv6wy1zf9wjqpg4m47qh3vnngr7ji0rzsy8hip2nggqpmva45")))

(define-public crate-mattress-0.2.1 (c (n "mattress") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (k 0)))) (h "101jww3j81zj4vvm4wpmx3z2bxwykrw6hj14c8hscfbkjrvff603")))

