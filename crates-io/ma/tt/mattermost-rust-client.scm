(define-module (crates-io ma tt mattermost-rust-client) #:use-module (crates-io))

(define-public crate-mattermost-rust-client-4.0.0 (c (n "mattermost-rust-client") (v "4.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0f0hgsbjxb16i2l59ccxjx1v3c69n5qakybmdfzqbzkj1v0nj8la")))

(define-public crate-mattermost-rust-client-4.0.1 (c (n "mattermost-rust-client") (v "4.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0fgrlk313s7il3w5v7z1piasfm74mm5p2w112ygxcki6v2f0dxp3")))

(define-public crate-mattermost-rust-client-4.0.2 (c (n "mattermost-rust-client") (v "4.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "06bmhg0li4llcf2q13m6xpgcpia3iwfg7hn73a4pi9fx0pmgnbhp")))

(define-public crate-mattermost-rust-client-4.0.3 (c (n "mattermost-rust-client") (v "4.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "01452c4fcp7xmkh0n53rm4abfjvg28x51fk2cs4hdckhn8gavvbk")))

(define-public crate-mattermost-rust-client-4.0.4 (c (n "mattermost-rust-client") (v "4.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1qhcqhdb9fmspk9dix8gpm5m3zrsn0plqa6a7b7s4xyamd2d59ig")))

(define-public crate-mattermost-rust-client-4.0.5 (c (n "mattermost-rust-client") (v "4.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rjidx8c4ajlmyl9fhsy0c7hk2g43vp2pf03g2ww62m9kkickdny")))

(define-public crate-mattermost-rust-client-4.0.6 (c (n "mattermost-rust-client") (v "4.0.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "04yp6sa29grsjpcxfpnv5w2nnwhmgn9dk21g7y6pyf9vc54204ka")))

(define-public crate-mattermost-rust-client-4.0.7 (c (n "mattermost-rust-client") (v "4.0.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qnw2linfmc5p8amxs2yppk2839nmlrvaxkjzqf5zmqp2acc83g5")))

(define-public crate-mattermost-rust-client-4.0.8 (c (n "mattermost-rust-client") (v "4.0.8") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "03jsvva38cdnl326sak99h3hk0f1ags6fda6379326gzg36zybp5")))

