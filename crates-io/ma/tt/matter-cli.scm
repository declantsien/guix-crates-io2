(define-module (crates-io ma tt matter-cli) #:use-module (crates-io))

(define-public crate-matter-cli-0.1.0-alpha1 (c (n "matter-cli") (v "0.1.0-alpha1") (d (list (d (n "matter") (r "^0.1.0-alpha1") (f (quote ("toml" "yaml" "json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0anp01s7npfph9amaz7kshzs4nx6j9lsg23igi5yjxqi95w7vrji")))

