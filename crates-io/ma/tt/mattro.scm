(define-module (crates-io ma tt mattro) #:use-module (crates-io))

(define-public crate-mattro-0.1.0 (c (n "mattro") (v "0.1.0") (d (list (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kxp8xa1dynbkvwc8hxdg4rl8g2lq8h6px436q29qhzminaql180")))

(define-public crate-mattro-0.1.1 (c (n "mattro") (v "0.1.1") (d (list (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j93iq4888abgaqkblzi2rqjcf6h4z0nawpr3aqxibhlhg9i4f77")))

(define-public crate-mattro-0.1.2 (c (n "mattro") (v "0.1.2") (d (list (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bgy8km1g0ig4rfbwnspnh0b9x2b9xl5y2wxlvdrfm2x0vmmhdbk")))

