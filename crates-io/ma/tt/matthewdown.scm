(define-module (crates-io ma tt matthewdown) #:use-module (crates-io))

(define-public crate-matthewdown-0.1.0 (c (n "matthewdown") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bcc6c96fx0f33ljzmkwp7c1m34cw3h1nzfzr9m6bknqmwx68960")))

(define-public crate-matthewdown-0.2.0 (c (n "matthewdown") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l8ywjkgv29c3dhnyi911381ghc5nskccmdd3pq4fwm3hyr37427")))

