(define-module (crates-io ma tt mattr) #:use-module (crates-io))

(define-public crate-mattr-0.0.0 (c (n "mattr") (v "0.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1555g4pm54wk4y84dwwdaji3y48sxc33hhrasy3bgpgrzjcyncwl") (y #t)))

(define-public crate-mattr-0.0.1 (c (n "mattr") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0jw666i3bwikcs2ibnhk3lfyjj714gfvnh8y8bvnva7xvbyfsjm1") (y #t)))

(define-public crate-mattr-0.0.2 (c (n "mattr") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xq6jy0wb1sbzi53yrbad8hzbzj8fm1ls44xr13d3rb2r1zdqqsz")))

