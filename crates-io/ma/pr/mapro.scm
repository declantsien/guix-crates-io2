(define-module (crates-io ma pr mapro) #:use-module (crates-io))

(define-public crate-mapro-0.1.0 (c (n "mapro") (v "0.1.0") (h "1l67w25p33gcg6677n8ry5h0xcrxhxx84xly2ladhjh489gvc7lr")))

(define-public crate-mapro-0.1.1 (c (n "mapro") (v "0.1.1") (h "1icjifakk3cpwwwvh3b6s0y268fym2329mn1dqhakqblcaqnzhdh")))

