(define-module (crates-io ma pr mapr) #:use-module (crates-io))

(define-public crate-mapr-0.8.0 (c (n "mapr") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "001mjzk4zavalz9nybrn7lvxsynm6f4hbi13y7vb41f0vdaqm8j6")))

