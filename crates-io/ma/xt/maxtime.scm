(define-module (crates-io ma xt maxtime) #:use-module (crates-io))

(define-public crate-maxtime-0.2.0 (c (n "maxtime") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.2.7") (f (quote ("derive" "std"))) (k 0)) (d (n "filetime") (r "^0.2.21") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "07y91dpvsn4q7qym7in12m3zzbr1brcfk93ab41pi982nyg0hwbn")))

(define-public crate-maxtime-0.4.0 (c (n "maxtime") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.21") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "time") (r "^0.3.21") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1c0mnpm292i446cxchnkdz37gqd62dm5m5r0kv2hzx7r1gn5lrdk")))

