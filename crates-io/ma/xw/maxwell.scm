(define-module (crates-io ma xw maxwell) #:use-module (crates-io))

(define-public crate-maxwell-0.1.0 (c (n "maxwell") (v "0.1.0") (h "1vkiibq73s6cgf81f0yyj87jdlmg36bq86dsxpj5wjshy0yqw4zn")))

(define-public crate-maxwell-0.1.1 (c (n "maxwell") (v "0.1.1") (h "01w9yawyqchp5aas9qy127ndhrrv3glim8q9w2acaas13v503wqd")))

