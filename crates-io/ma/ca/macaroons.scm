(define-module (crates-io ma ca macaroons) #:use-module (crates-io))

(define-public crate-macaroons-0.0.0 (c (n "macaroons") (v "0.0.0") (h "19w8drvxnai1rac8xkccwm6hk46rn0azx8139q2c94sp2db5wvl6")))

(define-public crate-macaroons-0.1.0 (c (n "macaroons") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "07vc9h7q67nwgrlhxpc460mvijn70i8g37pc5n22cb2jga5in53i")))

(define-public crate-macaroons-0.1.1 (c (n "macaroons") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0qmr2xc4z41nj7h1q7pp5imkl34inwjw0dqsyyg2ng3mbnpzjy9z")))

(define-public crate-macaroons-0.2.0 (c (n "macaroons") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "10ci2a8zi73j0pnsmfy7mnyb34nprbgpxbh07kxx2pp8571637lc")))

(define-public crate-macaroons-0.2.1 (c (n "macaroons") (v "0.2.1") (d (list (d (n "libsodium-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0bwggh6skgqmz8ylag2nvg98xgqr1cr8j2nm2hsl7v5jfmy95qmg")))

(define-public crate-macaroons-0.3.0 (c (n "macaroons") (v "0.3.0") (d (list (d (n "libsodium-sys") (r ">= 0.0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r ">= 0.0.10") (d #t) (k 0)))) (h "1v5hv6n8n4q07lqxhzcqy0hq6yn1zf9fgdw1wma5q0yn4cl53c5q") (f (quote (("nightly"))))))

(define-public crate-macaroons-0.3.1 (c (n "macaroons") (v "0.3.1") (d (list (d (n "libsodium-sys") (r ">= 0.0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r ">= 0.0.10") (d #t) (k 0)))) (h "1dscix9i7s63i4zagkd2f90q994ah7wixdxivj5ifl0nzbbbllqf") (f (quote (("nightly"))))))

(define-public crate-macaroons-0.3.2 (c (n "macaroons") (v "0.3.2") (d (list (d (n "libsodium-sys") (r ">= 0.0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r ">= 0.0.10") (d #t) (k 0)))) (h "1gp0422y0n4md2xzdpy1zl42ayg2zgmg5x977768ynxjy1lzfx1b") (f (quote (("nightly"))))))

(define-public crate-macaroons-0.3.3 (c (n "macaroons") (v "0.3.3") (d (list (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0") (d #t) (k 0)))) (h "1yjapq6vxzzwlzdphhbvpzvn2a2j17ha74imh35di65fak7x58bb")))

