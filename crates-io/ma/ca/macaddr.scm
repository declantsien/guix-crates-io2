(define-module (crates-io ma ca macaddr) #:use-module (crates-io))

(define-public crate-macaddr-0.1.0 (c (n "macaddr") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0z43x1hn3jpkjs93p0ksfxa9v8p8mx2d8r3g5hv9nfwdcyfrb2hs") (f (quote (("std") ("serde_std" "std" "serde/std") ("default" "std"))))))

(define-public crate-macaddr-0.1.1 (c (n "macaddr") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "17x00ar1zx4199ipmc4gr7w73j4ah7h25jvxizkb7zswn4n7bx1z") (f (quote (("std") ("serde_std" "std" "serde/std") ("default" "std"))))))

(define-public crate-macaddr-0.1.2 (c (n "macaddr") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1yqr7p1kfx8iniv422h03bnn7bydpxg0x3x21fbpzy1i235kirdy") (f (quote (("std") ("serde_std" "std" "serde/std") ("default" "std"))))))

(define-public crate-macaddr-1.0.0 (c (n "macaddr") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "08djyg2qv2rqk0v27l1bi7a77sgp8bvy9fbk5s3aprg1sgpkga81") (f (quote (("std") ("serde_std" "std" "serde/std") ("default" "std"))))))

(define-public crate-macaddr-1.0.1 (c (n "macaddr") (v "1.0.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1n5jxn79krlql810c4w3hdkvyqc01141dc5y6fr9sxff2yy0pvms") (f (quote (("std") ("serde_std" "std" "serde/std") ("default" "std"))))))

