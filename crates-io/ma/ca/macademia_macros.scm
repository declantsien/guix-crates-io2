(define-module (crates-io ma ca macademia_macros) #:use-module (crates-io))

(define-public crate-macademia_macros-0.1.0 (c (n "macademia_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "06qvgg7kqvjl5wkl2gn17wpxv60h5mmn88n641nh95j82a4r6zxx") (y #t)))

(define-public crate-macademia_macros-0.1.1 (c (n "macademia_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvbjpsyyybzgcf0h58w9i8p1rj9z58qj02a1d6icb04ybsmkx5g") (y #t)))

(define-public crate-macademia_macros-0.2.0 (c (n "macademia_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "04mb2zkbrl62nrj60sz2w092p8yn901irj3i7p2p5nh7a7mb4bgr") (y #t)))

(define-public crate-macademia_macros-0.2.1 (c (n "macademia_macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0wn4h03ymh1f1hsmjhx18cwy43wgby90mq90x2mijai36iqybr3d") (y #t)))

(define-public crate-macademia_macros-0.2.2 (c (n "macademia_macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "07hmyjcyn051vk672bng2732hd9w58a8xh283ixjr4wlwrlqzpan") (y #t)))

(define-public crate-macademia_macros-0.2.3 (c (n "macademia_macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0cw5l4gvmxylyqlbajillnb2q7xy7485s3d77saii9n67r5jdxvy") (y #t)))

(define-public crate-macademia_macros-0.2.4 (c (n "macademia_macros") (v "0.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "11l2lw49ga90sbgkg30naa16n5jmqrlcr111w4m2cl11w01s42lw") (y #t)))

