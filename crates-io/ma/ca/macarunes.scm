(define-module (crates-io ma ca macarunes) #:use-module (crates-io))

(define-public crate-macarunes-0.1.0 (c (n "macarunes") (v "0.1.0") (d (list (d (n "arrrg") (r "^0.1") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.1") (d #t) (k 0)) (d (n "biometrics") (r "^0.3") (d #t) (k 0)) (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19") (d #t) (k 0)) (d (n "prototk") (r "^0.3") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nqpw0v21kr1d6ar0hrx28hv8g3ib2l3xviwlksz856ylihdad4r")))

(define-public crate-macarunes-0.2.0 (c (n "macarunes") (v "0.2.0") (d (list (d (n "arrrg") (r "^0.2") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.2") (d #t) (k 0)) (d (n "biometrics") (r "^0.4") (d #t) (k 0)) (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1p48dfvwr48yn9gx55bm5kvaxy66qfbavjg7xp8cxyzx9p2ydyq5")))

(define-public crate-macarunes-0.3.0 (c (n "macarunes") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)) (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.19") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1lpz4qyk56r6426aiyrj9i8328bbizzvi0zdzjc8fvzihc3d3gm5")))

