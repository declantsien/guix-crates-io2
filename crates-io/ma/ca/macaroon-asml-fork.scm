(define-module (crates-io ma ca macaroon-asml-fork) #:use-module (crates-io))

(define-public crate-macaroon-asml-fork-0.1.0 (c (n "macaroon-asml-fork") (v "0.1.0") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 2)))) (h "0md0ihxs21711r87gsx7wgibf57zplg3gv0fa037gjnal1sbm9q1") (r "1.62")))

