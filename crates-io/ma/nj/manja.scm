(define-module (crates-io ma nj manja) #:use-module (crates-io))

(define-public crate-manja-0.1.0 (c (n "manja") (v "0.1.0") (h "1yng8dj37a27rh9f613b0br7z5n3nqs00jlwnvbfg3ymix30y93w")))

(define-public crate-manja-0.1.1-rc (c (n "manja") (v "0.1.1-rc") (d (list (d (n "base32") (r "^0.5.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fantoccini") (r "^0.19.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "gzip" "zstd"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "totp-rs") (r "^5.5.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "13gngavl6l57hkcfh0mlr0c3mrkrk51kjfy0il6rkl8i9wbjpdlj")))

