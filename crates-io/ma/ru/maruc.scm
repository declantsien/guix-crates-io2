(define-module (crates-io ma ru maruc) #:use-module (crates-io))

(define-public crate-maruc-0.1.0 (c (n "maruc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "druid") (r "^0.7.0") (f (quote ("im"))) (d #t) (k 0)) (d (n "matrix-sdk") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03fvz4rldf0bi04pg66c7k83zqcp3lvidsyzgqzkdj97w2cbsn4i")))

