(define-module (crates-io ma ru marui) #:use-module (crates-io))

(define-public crate-marui-0.1.0 (c (n "marui") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m3399kq5cb4wx1j5x9p9g8hbvpi3ljbnwj0yq4xx2ah4x26pzkw")))

(define-public crate-marui-0.1.1 (c (n "marui") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqj24897hdm4nghig1xnn9mb1411zcjwg0z4q81nhdg7jvvjhrg")))

(define-public crate-marui-0.2.0 (c (n "marui") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ldgrnwcwmzvnabakskvzfqh12936z9rhfgzx2yl4wxwdyf0h9ji")))

