(define-module (crates-io ma uz mauzi_macros) #:use-module (crates-io))

(define-public crate-mauzi_macros-0.0.1 (c (n "mauzi_macros") (v "0.0.1") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)))) (h "0qs4jq6n37wxsbr5p2a30xhjhlnp63xgvvnrxxs3ndfx3m5qaml2")))

(define-public crate-mauzi_macros-0.0.2 (c (n "mauzi_macros") (v "0.0.2") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)))) (h "0147daxnya9yjhy1n3hm7wxx77qazl6fa6dkjg80ilkrc790adjp")))

