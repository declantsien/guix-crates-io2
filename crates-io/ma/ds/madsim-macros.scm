(define-module (crates-io ma ds madsim-macros) #:use-module (crates-io))

(define-public crate-madsim-macros-0.1.0 (c (n "madsim-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02384df9w4w8n0l2gsq3vzc9j5shqby5dmskvlsvc3ab6r7n723p")))

(define-public crate-madsim-macros-0.1.1 (c (n "madsim-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1078177zi0v8q726iqfmsnm4hxq0sbz3xwnv1vb0vn7nd2lddasx")))

(define-public crate-madsim-macros-0.2.0-alpha.1 (c (n "madsim-macros") (v "0.2.0-alpha.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1i23rypgncv59ixaq4mfb0rlm77h4hxrhq179r9fzx16qb03k678")))

(define-public crate-madsim-macros-0.2.0-alpha.2 (c (n "madsim-macros") (v "0.2.0-alpha.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1h1x42qwbw5gzpmcnzayga8zb6h9yr5qr25xl7s81iq2z2s5rkya")))

(define-public crate-madsim-macros-0.2.0-alpha.3 (c (n "madsim-macros") (v "0.2.0-alpha.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mz0978r10yc0zjw8r93nf5as167k7rjc42s90a68yq5dh2jx3f2")))

(define-public crate-madsim-macros-0.2.0-alpha.4 (c (n "madsim-macros") (v "0.2.0-alpha.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18bc7dbfvs4cvp308lws88lcr6xcry6jzm6yh1lxi9ggkaqdb70g")))

(define-public crate-madsim-macros-0.2.0-alpha.5 (c (n "madsim-macros") (v "0.2.0-alpha.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wcyvwqwigd3hasi9kb3d6b679ws4as89ai1s8a8a0ik3xasjaq2")))

(define-public crate-madsim-macros-0.2.0-alpha.6 (c (n "madsim-macros") (v "0.2.0-alpha.6") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01f5h3nhkhk13l70pzs9f7kp87ikbmjmfvqsimzr3b01rflp89di")))

(define-public crate-madsim-macros-0.2.0-alpha.7 (c (n "madsim-macros") (v "0.2.0-alpha.7") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f3718zc4ilbmm02l5hnz2xlg3vm2h254dj2y82jb7dcwaa0y7s0")))

(define-public crate-madsim-macros-0.2.0 (c (n "madsim-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ajxxa8pmngf8zrdqcz5lxsg2s6nxxyapsmkpj9x5awz6q2mbaa5")))

(define-public crate-madsim-macros-0.2.5 (c (n "madsim-macros") (v "0.2.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1sl0kydcj3q69lbz4bk4jijfqjqaxzyv7b5qcv02685zmlvy3fwa")))

(define-public crate-madsim-macros-0.2.12 (c (n "madsim-macros") (v "0.2.12") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qa6r46r34qavms75wab74affj453s98v0n329m84j0sggllilpk")))

