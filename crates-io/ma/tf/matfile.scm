(define-module (crates-io ma tf matfile) #:use-module (crates-io))

(define-public crate-matfile-0.1.0 (c (n "matfile") (v "0.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g20h8qni5lp2f0hqmza9dp1v3c5wmvq29b1ppa7qawrvg6n20r7")))

(define-public crate-matfile-0.2.0 (c (n "matfile") (v "0.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vghrdv23j6cchmcw17cgkd3ghnvcl7mkr73ri9b7myl07cphm46")))

(define-public crate-matfile-0.2.1 (c (n "matfile") (v "0.2.1") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jgx6cd76vchqav62lhlk76jwnzy2pp9kmyaq81bfw2snyrdkdma")))

(define-public crate-matfile-0.3.0 (c (n "matfile") (v "0.3.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "ndarr") (r "^0.15") (o #t) (d #t) (k 0) (p "ndarray")) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bmam34sk873r2brsk7m62vhagcy4340j52q8nlmi20nj51i6wgj") (f (quote (("ndarray" "ndarr" "num-complex"))))))

(define-public crate-matfile-0.3.1 (c (n "matfile") (v "0.3.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "ndarr") (r "^0.15") (o #t) (d #t) (k 0) (p "ndarray")) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xz4jcwbnk27i3zcjfmiwpjj3fd0mayq4w6phmqzm48c4znjmx51") (f (quote (("ndarray" "ndarr" "num-complex"))))))

(define-public crate-matfile-0.4.0 (c (n "matfile") (v "0.4.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "ndarr") (r "^0.15") (o #t) (d #t) (k 0) (p "ndarray")) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0rkrbb7mzkhrcmg72cggxnfrw75f1qi4cj1kr28xn1wmnb2ami9c") (f (quote (("ndarray" "ndarr" "num-complex"))))))

(define-public crate-matfile-0.4.1 (c (n "matfile") (v "0.4.1") (d (list (d (n "enum-primitive-derive") (r "^0.3") (d #t) (k 0)) (d (n "libflate") (r "^2.0") (d #t) (k 0)) (d (n "ndarr") (r "^0.15") (o #t) (d #t) (k 0) (p "ndarray")) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yh6avxryf08hbcvz36g0b34j5ylfr8spb717jx3m0nhv4pvw742") (f (quote (("ndarray" "ndarr" "num-complex"))))))

