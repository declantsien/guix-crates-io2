(define-module (crates-io ma tf matfile-ndarray) #:use-module (crates-io))

(define-public crate-matfile-ndarray-0.1.0 (c (n "matfile-ndarray") (v "0.1.0") (d (list (d (n "matfile") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0nw1bsdwkj1nmxnbkf8m4fq2mbfcsz3nlcja4mi7326z2d75si1d")))

(define-public crate-matfile-ndarray-0.2.0 (c (n "matfile-ndarray") (v "0.2.0") (d (list (d (n "matfile") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0ynjk7rfgl0ggmm6634snfd42myxwd345zgxw4l85pbqw4j96ak8")))

