(define-module (crates-io ma pg mapgen) #:use-module (crates-io))

(define-public crate-mapgen-0.1.0 (c (n "mapgen") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "18cd5gnb851nfli90g6x9m56in3r4gfxxxghxcblg18niywbkkwm")))

(define-public crate-mapgen-0.1.1 (c (n "mapgen") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1r8ilsf891sv5m56n7hgabrk0wxwzqk2pkyl4hq3sl6csybnmqb7")))

(define-public crate-mapgen-0.1.2 (c (n "mapgen") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "14y1ha078xy0axh98iprznrd7v3sj3f2c44bcj1ksxhmznqzb8b0")))

(define-public crate-mapgen-0.2.0 (c (n "mapgen") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1d8cj44m2q1yyvkvyy2la3xs7z64ln2ck3fvchghg97pz2gd59xg")))

(define-public crate-mapgen-0.3.0 (c (n "mapgen") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "11a6vhmxrzf613sdikvvk7ha9j3xnbi5g5pzyg1vk127b5kq4c44")))

(define-public crate-mapgen-0.4.0 (c (n "mapgen") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0h8r3dn98xqw28a5g1wbb8vnawgklv8hk2h3wlzair23hp35zknf")))

(define-public crate-mapgen-0.4.1 (c (n "mapgen") (v "0.4.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "08rm84bziyyh2m3b2y1yhmqb0s3jm3v03cjmnqxylj2d1g1bg752")))

(define-public crate-mapgen-0.4.2 (c (n "mapgen") (v "0.4.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1wj7nm83fq053aqa7i8lhfbd96jrmhpcz9f0j8krl91v79s3kn19")))

(define-public crate-mapgen-0.5.0 (c (n "mapgen") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0m20xzh0q3gx0z1qpbq867xqndxihsqi7aqx67p6m263bwb0lvxf")))

(define-public crate-mapgen-0.5.1 (c (n "mapgen") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19m1x9lk890kb0dz9i16jnxdp6vbfkk5vrypz0p0qw9ys53xg29j")))

(define-public crate-mapgen-0.5.2 (c (n "mapgen") (v "0.5.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mj1pmpfq939lixzb6x414ymvk1myz5s76wddrh4an68q2r549j8")))

(define-public crate-mapgen-0.6.0 (c (n "mapgen") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "112p88jz12vdknyg6n1680gmrc67i3rzghc9hbfgvj0y52csjklz")))

