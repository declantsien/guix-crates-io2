(define-module (crates-io ma id maidsafe_types) #:use-module (crates-io))

(define-public crate-maidsafe_types-0.0.1 (c (n "maidsafe_types") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0xspnxbs12qc6b2g4sd5sj7a2dr176naqyn7p7ldhri66fffjmyl")))

(define-public crate-maidsafe_types-0.0.8 (c (n "maidsafe_types") (v "0.0.8") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "09g51fbvyf2snpkm4fbks7rhn2hl04x4nmjl3h8vg2qlwvcfqkvg")))

(define-public crate-maidsafe_types-0.0.9 (c (n "maidsafe_types") (v "0.0.9") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0qnqki1gx5jz5116bm1spdpjd3wx2cn4ny49qhfnwl02225x7xp9")))

(define-public crate-maidsafe_types-0.1.0 (c (n "maidsafe_types") (v "0.1.0") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "04ydy4z80zkpi02pqngb574xnqlqc1gbwa6pn3shrlbyk6bymaq4")))

(define-public crate-maidsafe_types-0.1.1 (c (n "maidsafe_types") (v "0.1.1") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "1lqm2fgdwjvr8sv72h2pwgqvyzfdkg4ag1fcgq3x1ggx284m6hix")))

(define-public crate-maidsafe_types-0.1.2 (c (n "maidsafe_types") (v "0.1.2") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "05igybpxci7qsy4lx852rbhsnslb62jrgxcnfiyrl38gj9agphfy")))

(define-public crate-maidsafe_types-0.1.3 (c (n "maidsafe_types") (v "0.1.3") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.33") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "0wg367n1sinz8nrm869wgqs1bg5l3j97f4xkb1by3rbhq0pd477w")))

(define-public crate-maidsafe_types-0.1.4 (c (n "maidsafe_types") (v "0.1.4") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.60") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "0kxq9ym2wnisa7mk0j3jyh60lciq2y9nm6ax8mgssjs8wdz9hyla")))

(define-public crate-maidsafe_types-0.1.5 (c (n "maidsafe_types") (v "0.1.5") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.60") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "0prhixzbq6568dl2akafxcmqxwcqgx7cqyz7vir07x1538lpwnbx")))

(define-public crate-maidsafe_types-0.1.51 (c (n "maidsafe_types") (v "0.1.51") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.61") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "0l7fzhzy9ijn59ic1bciar76rya82zff9vg15xzlbv6snaj6175y")))

(define-public crate-maidsafe_types-0.1.52 (c (n "maidsafe_types") (v "0.1.52") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.1.61") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "0wijmpvz9lwdzyivxjyn2ira7aw7rszvp1ww0hhkx82r84cb264h")))

(define-public crate-maidsafe_types-0.2.0 (c (n "maidsafe_types") (v "0.2.0") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0cdn9glkcpzrih113hvq546l3aj4j4dy2i7pjwx8rfag31fh3h46")))

(define-public crate-maidsafe_types-0.2.1 (c (n "maidsafe_types") (v "0.2.1") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0fad6fs3pxkgf4lgh9dydm4m3zfcnpnrdd06f01kbqgf52ck59kc")))

(define-public crate-maidsafe_types-0.2.2 (c (n "maidsafe_types") (v "0.2.2") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "111vj6lx554ljbjz106baay54nykvw0ksbcp4z0zknav58xr5yns")))

(define-public crate-maidsafe_types-0.2.3 (c (n "maidsafe_types") (v "0.2.3") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "0.2.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0jvpcg8i5c43h62n6l8b46jx0gyrj6rj13zjdi3qllq2i0gp2b54")))

