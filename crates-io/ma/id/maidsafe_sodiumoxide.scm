(define-module (crates-io ma id maidsafe_sodiumoxide) #:use-module (crates-io))

(define-public crate-maidsafe_sodiumoxide-0.0.6 (c (n "maidsafe_sodiumoxide") (v "0.0.6") (d (list (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libsodium-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0b8j3dzhn932ilkw958p3yaw7mipfpq66my3j1yn892gj6sb0h7v") (f (quote (("benchmarks")))) (y #t)))

(define-public crate-maidsafe_sodiumoxide-0.0.7 (c (n "maidsafe_sodiumoxide") (v "0.0.7") (d (list (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libsodium-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1s501bzcwvkvgac2havzpvhhni35afrzic2y605kgabns0yq59kk") (f (quote (("benchmarks")))) (y #t)))

(define-public crate-maidsafe_sodiumoxide-0.0.12 (c (n "maidsafe_sodiumoxide") (v "0.0.12") (d (list (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libsodium-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1kpc8aw5swkw5lqyy7b5gmhwd4m4yr8pgjsrkb0i16mqg23ayiiv") (f (quote (("benchmarks")))) (y #t)))

(define-public crate-maidsafe_sodiumoxide-0.0.13 (c (n "maidsafe_sodiumoxide") (v "0.0.13") (d (list (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libsodium-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1mspl599z8hy68j113f1x5vqkmh3npx1csl9liwrnwpwsp7bdn6w") (f (quote (("benchmarks")))) (y #t)))

