(define-module (crates-io ma id maidsafe_client) #:use-module (crates-io))

(define-public crate-maidsafe_client-0.0.1 (c (n "maidsafe_client") (v "0.0.1") (h "0wpq8qz9r6qc7p2czczb9273spk1abyzaicddi3kihz4g302hngw")))

(define-public crate-maidsafe_client-0.1.0 (c (n "maidsafe_client") (v "0.1.0") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "*") (d #t) (k 0)) (d (n "maidsafe_types") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "self_encryption") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1lz97i246ybllf1cygiwxzf5f0kqdhikrhgb5hyysls1ds6bwxln") (f (quote (("USE_ACTUAL_ROUTING"))))))

(define-public crate-maidsafe_client-0.1.1 (c (n "maidsafe_client") (v "0.1.1") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "0.2.*") (d #t) (k 0)) (d (n "maidsafe_types") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "0.2.*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "self_encryption") (r "0.2.*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "09fi8p1h3iw7ric7mcqwgb9yv5j36hk9aix5n3zf2d0zqr1if62f") (f (quote (("USE_ACTUAL_ROUTING"))))))

(define-public crate-maidsafe_client-0.1.2 (c (n "maidsafe_client") (v "0.1.2") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "0.2.*") (d #t) (k 0)) (d (n "maidsafe_types") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "0.2.*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "1b6j0skiwvd7bpbr0zygmjsc8kc7pk5rcsyihav50pska1clr32r") (f (quote (("USE_ACTUAL_ROUTING"))))))

