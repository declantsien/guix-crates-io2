(define-module (crates-io ma id maidsafe_vault) #:use-module (crates-io))

(define-public crate-maidsafe_vault-0.0.1 (c (n "maidsafe_vault") (v "0.0.1") (h "1sfdmrnxy54vxmhmc8bz46kyfjv2lc20sn9v3sw1v2d0ba7731f8")))

(define-public crate-maidsafe_vault-0.0.3 (c (n "maidsafe_vault") (v "0.0.3") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "^0.1.2") (d #t) (k 0)) (d (n "maidsafe_types") (r "^0.0.9") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "routing") (r "^0.0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1h21rgxz6q4ka179jw2451away83kh9fp18mwkr7xz3x8nvmdb61")))

(define-public crate-maidsafe_vault-0.1.0 (c (n "maidsafe_vault") (v "0.1.0") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "crust") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "*") (d #t) (k 0)) (d (n "maidsafe_types") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "routing") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0rk5qyl079jz3k72gw4c1daqb4zd9q19q50zj204gjlw4zh0l5ll")))

(define-public crate-maidsafe_vault-0.1.1 (c (n "maidsafe_vault") (v "0.1.1") (d (list (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "crust") (r "0.1.*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "0.2.*") (d #t) (k 0)) (d (n "maidsafe_types") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "routing") (r "0.2.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1mwl9cq72ds5s5idr539wbm3l4k5sqsj2yrrgrx1z92zimq4bdx9")))

