(define-module (crates-io ma rw marwood) #:use-module (crates-io))

(define-public crate-marwood-0.1.0 (c (n "marwood") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zx7gjwprlvv6y9xzn7hcsk5w2dy0xj1q7fmca63caadsd8v5clk") (y #t)))

(define-public crate-marwood-0.1.1 (c (n "marwood") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "18nh1az7v8wcx17xmczbwphicbldxwkr2sgpry0xlj54c6hp1l1m")))

(define-public crate-marwood-0.1.2 (c (n "marwood") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1iwiabpz571yi6kmrc7f1vbvj45qx4sdiw5a0kfm203pdjj96sn3")))

(define-public crate-marwood-0.1.3 (c (n "marwood") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1iha3rc9xa1fhnfzsn77r5m89v1cfwnygcayn9yglra2x24x0qda")))

(define-public crate-marwood-0.2.0 (c (n "marwood") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0aas32ax4bnpysqap8v55vrgk0bm9rgkzkdgzxyd22prwv82mf8a")))

(define-public crate-marwood-0.2.1 (c (n "marwood") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fhbmbabc97jf3a8nkpdj6x260v1rn90g40qfhz14a3nqmjm0cxn")))

(define-public crate-marwood-0.2.2 (c (n "marwood") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00axdy5gp27cal89862jsrglm62gi0xcghd0krzpn55l8jjqzalj")))

(define-public crate-marwood-0.2.3 (c (n "marwood") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mbv6pcy01zcdqyykfqa5c6nnvy3nsina7ayl5pxgln5hqyi66r6")))

(define-public crate-marwood-0.2.4 (c (n "marwood") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "176689wj6bylapi43ag7ckrsmakjlmnqzla3ncy0jyljrx731lif")))

(define-public crate-marwood-0.2.5 (c (n "marwood") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0h4p7593g5prrviishmgdsldlx9lqpjc3b98pcgc0xz0a7yzak3v")))

(define-public crate-marwood-0.3.0 (c (n "marwood") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05v2w426ixpji74zfpviqfibvd45dfnwf58wx5skailn4hiy1cwc")))

(define-public crate-marwood-0.3.1 (c (n "marwood") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0acq5pkb315c3g26ri1kdkrkjsfnpb58vqxk7ja7mzw4ikqfqbv5")))

(define-public crate-marwood-0.4.0 (c (n "marwood") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17w403rwfkam5rgvhkxsbxjh2n87i46yfcinn78kwn3826zqy712")))

(define-public crate-marwood-0.4.1 (c (n "marwood") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1r9579vzpa2d0kanc4ljwqlvglayfsw2kj3ljp74vxbfjjzs97r6")))

(define-public crate-marwood-0.5.0 (c (n "marwood") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gy185l2bq5dad5wxx4ylrk626d3mx5hkbwnb8dqb27c9z960nbj")))

