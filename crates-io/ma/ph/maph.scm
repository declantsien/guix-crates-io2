(define-module (crates-io ma ph maph) #:use-module (crates-io))

(define-public crate-maph-0.1.0 (c (n "maph") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "05074dswsx1psxm4819hh6ax6z4kvf94irxlrp57mjy6g0cx9f1h")))

(define-public crate-maph-0.1.1 (c (n "maph") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "01vaz7gny9v68mv6ln06iz9lhpw3c1p13rsb6sb8n04hzpz3v9jy")))

(define-public crate-maph-0.1.2 (c (n "maph") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ia077fap0sisl5sfb9bvjjq6gpc0qifc89qlyzxmb1vydw02za")))

(define-public crate-maph-0.1.3 (c (n "maph") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gznn1aj68vzwmpzw82wipp9mxg0gnw92c1qsdia4a64yxi5ndbq")))

(define-public crate-maph-0.2.0 (c (n "maph") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iqmw1ggg15gr7b2ckjlx1842xkfjwyh44khw7dzac29a9x66kg7")))

(define-public crate-maph-0.2.1 (c (n "maph") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c4q69f9s4147wasj7j3hl7vrsaml4fpx8wl7hg9f47djvk9xjgk")))

(define-public crate-maph-0.2.2 (c (n "maph") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0db214bwav2623myzhviifh52jlx5lg74z32yapngic4sy92d239")))

(define-public crate-maph-0.2.3 (c (n "maph") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v6bypwx5v349j1v7z1yckhjg0q1yfilrdm6dln5f1sz2z26a40j")))

(define-public crate-maph-0.2.4 (c (n "maph") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a9nbn36dgkrk9mvnlhmyqw44d0pd93kriv7lq6mgq13i3424166")))

(define-public crate-maph-0.2.5 (c (n "maph") (v "0.2.5") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lxd4pbvnjz9x25jz2pgasmsvd3xwzzlcr9haxwq9xxa2h82aziy")))

(define-public crate-maph-0.2.6 (c (n "maph") (v "0.2.6") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kgapfslnax4v17f7m9bli8cl02ybb94jyq16hggv9qrkg67g7y7")))

(define-public crate-maph-0.6.0 (c (n "maph") (v "0.6.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1im6sw662f53lw2by2pi10xbc21vlriql5m04jn7zpiikcx9g0gj")))

(define-public crate-maph-0.6.1 (c (n "maph") (v "0.6.1") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lgk34yiyrq7lp0xjdka0fnbf0h8r3ncb992vi0s1hlphvj2d826")))

(define-public crate-maph-0.6.2 (c (n "maph") (v "0.6.2") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vx87jq0vxy6l9r3a499g8ivhgxxrar4k6wryx0l160986yhlby2")))

(define-public crate-maph-0.6.3 (c (n "maph") (v "0.6.3") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i5ds1bg8xqb9avwiva2biz1h8qpbwg6yxmlnrhj3kglykgr5wni")))

(define-public crate-maph-0.6.35 (c (n "maph") (v "0.6.35") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "15z86s9a2figc5ql6c74q78gfr30qbgw43x2glq3zm8x7clgb5pc")))

(define-public crate-maph-0.6.36 (c (n "maph") (v "0.6.36") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ppq1gadcmg5j3m1a62m6knszg0r7b4r58zp85m2a3qdvqw7xl5")))

(define-public crate-maph-0.6.4 (c (n "maph") (v "0.6.4") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0725k22xcmd0zrsn88x82q7avhky7b7xx8f71n407qj6bj7n51jk")))

(define-public crate-maph-0.7.0 (c (n "maph") (v "0.7.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "04vssm0garyjfbzsch5f70m35nfhdiqicsqif9z5hcyipvafkxlp")))

(define-public crate-maph-0.7.1 (c (n "maph") (v "0.7.1") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "11wrm9jb78jc6rsc2l94srz0vbhyj3svmy18fn4k7gcd9zbswm0h")))

(define-public crate-maph-0.7.2 (c (n "maph") (v "0.7.2") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1z6c5svql5lx5np6qsxh4iyg4zrgk374nq7qpyc8bz9l3gqcr7ds")))

(define-public crate-maph-0.7.25 (c (n "maph") (v "0.7.25") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0x2afdj6i50gjzdbbbzjx0vm4qfyw8rgy548qamb7hvrdzain9lf") (y #t)))

(define-public crate-maph-0.7.3 (c (n "maph") (v "0.7.3") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0v3bh7zih3asi623whzzsv1cs5px91f2lv7x7g9f2ak4yx6vghqv")))

(define-public crate-maph-0.7.4 (c (n "maph") (v "0.7.4") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1fzbqkimbcg706aqlxx69dwyipmnpdmpzpf1lbr8x2nhrapcipdl")))

(define-public crate-maph-0.7.5 (c (n "maph") (v "0.7.5") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1nz9mw29xa79nn50s4d1fwmcpw9l79zzra0mqcz2is07nwv65jvm")))

(define-public crate-maph-0.7.6 (c (n "maph") (v "0.7.6") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0ajyjkpz526k7mq50yb5rrmr14972f58lslazf1gykgnasiy7zmw")))

(define-public crate-maph-0.7.7 (c (n "maph") (v "0.7.7") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1bz8sy0cf0s4338rn75dbz83njk8nb6mbbsnspy83h9pdfjpddjc")))

(define-public crate-maph-0.7.8 (c (n "maph") (v "0.7.8") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1q755z16x1dv7avmdnlmfz866m5qzx74zspz1kfi8r47k85xy6sa")))

(define-public crate-maph-0.7.9 (c (n "maph") (v "0.7.9") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1sp4micbl1wysi7wk3bis3bgx35v32pggvvbcb6asdj6af9d39hh")))

(define-public crate-maph-0.7.10 (c (n "maph") (v "0.7.10") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0q0mdiijd50l68s4a36cwkgh9rn3ja16mczx6dsfbfw5smv5zjwi")))

(define-public crate-maph-0.7.11 (c (n "maph") (v "0.7.11") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1dif5pc1b80yfnymrpi4zn91wqjbxn55cywk03m7wlf0wsizrv4z")))

(define-public crate-maph-0.8.0 (c (n "maph") (v "0.8.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "07br54dbgg48mq20bdv06v95dl147jrjygmckxwnm2f98hijzkpi")))

(define-public crate-maph-0.8.1 (c (n "maph") (v "0.8.1") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0h17qzyrn5grawipi4mzdikpw8pr2sbgl2cnwkimq76jz1plmvwn")))

