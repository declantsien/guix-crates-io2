(define-module (crates-io ma v- mav-sdk) #:use-module (crates-io))

(define-public crate-mav-sdk-0.1.0 (c (n "mav-sdk") (v "0.1.0") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "1v9si7b5s659hmimf4wsd8vd5sq58gpa8bw1s399f9b0n0z73iz6")))

