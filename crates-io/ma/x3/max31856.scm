(define-module (crates-io ma x3 max31856) #:use-module (crates-io))

(define-public crate-max31856-0.0.1 (c (n "max31856") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "067nbizk15fciq028ajf8rrxlwz0xxli9plcgad5c5haf8fja528")))

