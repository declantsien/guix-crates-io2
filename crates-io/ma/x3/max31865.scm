(define-module (crates-io ma x3 max31865) #:use-module (crates-io))

(define-public crate-max31865-0.1.0 (c (n "max31865") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "151gw13nds3zs8q489p91zbcnahv8wgr9ajlacwrif51kyycy22v")))

(define-public crate-max31865-0.1.1 (c (n "max31865") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "embedded-graphics") (r "^0.6.2") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "ssd1306") (r "^0.5.1") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("stm32f103"))) (d #t) (t "thumbv7m-none-eabi") (k 2)))) (h "1qkqzizv0c1nx9pwzsjz70bwpzqr6flrwra1f0dbbbw6zwww5sg8") (f (quote (("doc"))))))

