(define-module (crates-io ma x3 max32660) #:use-module (crates-io))

(define-public crate-max32660-0.1.0 (c (n "max32660") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1j5p27hxi8ms83rv8ci05ylaprfcjwqnq0fykwhrc0v9a9rf2mqb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max32660-0.2.0 (c (n "max32660") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1g7chw247vfdhmwym1knvxfja428la40xz34zcbqffnzxx7chwyx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max32660-0.2.1 (c (n "max32660") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "09mdwllslhvm9db8h6yhrzlx91n54rzv6d3l71fvnwgsxniz4ybn") (f (quote (("rt" "cortex-m-rt/device"))))))

