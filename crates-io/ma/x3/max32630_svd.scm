(define-module (crates-io ma x3 max32630_svd) #:use-module (crates-io))

(define-public crate-max32630_svd-0.1.0 (c (n "max32630_svd") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "17nhn3bl5y2v5i5q559sc3580qzcj2xp68p3khlfw0za0l7cbn0j") (f (quote (("rt" "cortex-m-rt/device"))))))

