(define-module (crates-io ma x3 max31855-rs) #:use-module (crates-io))

(define-public crate-max31855-rs-0.1.0 (c (n "max31855-rs") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (f (quote ("spidev"))) (d #t) (k 2)))) (h "1lg6zirv4y63yv3nbcv5zmy7r4bc5cfx8gj59iqqdl87cr5jz95y") (f (quote (("std") ("default"))))))

