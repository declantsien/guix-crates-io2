(define-module (crates-io ma x3 max30205) #:use-module (crates-io))

(define-public crate-max30205-0.1.0 (c (n "max30205") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0qhnvhbwa7zc5snhw28af8q2d3cdg2rphj4c0hfcpqip477kj1sw")))

(define-public crate-max30205-0.1.1 (c (n "max30205") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0fp45qsv54mqanbj4ydb0brcvy70l0p14pg3imvdgwcrp1x7z4j8")))

