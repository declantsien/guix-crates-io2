(define-module (crates-io ma x3 max3263x) #:use-module (crates-io))

(define-public crate-max3263x-0.1.0 (c (n "max3263x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ga6zywsrq31wbgymx4raakbb915p9y9xhg6ij4qqsyjaksx4w0j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max3263x-0.2.0 (c (n "max3263x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "15hxq59fzf0bb9gw22jgznlr4gmv63cv5440wb091k1xy6k4dqqs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max3263x-0.3.0 (c (n "max3263x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "13pmhkhl2cfp93blzrjafhpv5b2k549slfpklbqkymg78a9wc4qx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max3263x-0.4.0 (c (n "max3263x") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lknc045y66s1kprwmh7dvigpnbmlsizf9ksqbvfd402ncsrdjpb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max3263x-0.5.0 (c (n "max3263x") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fsmihnwr27p3k0c4wx5ax72saix897nk5m93fias8wlqqy57cyv") (f (quote (("rt" "cortex-m-rt/device"))))))

