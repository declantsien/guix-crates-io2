(define-module (crates-io ma x3 max31855) #:use-module (crates-io))

(define-public crate-max31855-0.1.0 (c (n "max31855") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1j4nkpqhs93r4iynls1z0fmb5nxr1jrkq06x56j9qk6l22y85flp")))

