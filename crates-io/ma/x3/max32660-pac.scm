(define-module (crates-io ma x3 max32660-pac) #:use-module (crates-io))

(define-public crate-max32660-pac-0.1.0 (c (n "max32660-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0njwr2j5nm8k4x750arv47fymvkdr2y4xx7g3bmk0d04h19vp1wa") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max32660-pac-0.1.1 (c (n "max32660-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "040mb9x7gbk0qyn16dvgjlf8zi7z0ywh6qiblwvb2cydcx3vqc4w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-max32660-pac-0.1.2 (c (n "max32660-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18xb62gk38mrhnxvmpc2i8mn1za0f969qdskyqji7g92gxg7mx1y") (f (quote (("rt" "cortex-m-rt/device"))))))

