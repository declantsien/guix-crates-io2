(define-module (crates-io ma x3 max3010x) #:use-module (crates-io))

(define-public crate-max3010x-0.1.0 (c (n "max3010x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1rnvxb2lw4jbw1y9g41xq89l08alcf3n243v1ml7cp9f8bm6njq4")))

