(define-module (crates-io ma ng mango-egui) #:use-module (crates-io))

(define-public crate-mango-egui-0.1.0 (c (n "mango-egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.20.1") (k 0)) (d (n "rfd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0nc62750775dm3rfaz2yn0hvzsvdb6b2bi9679crmvib8405cwzg") (f (quote (("default" "file")))) (s 2) (e (quote (("file" "dep:rfd"))))))

(define-public crate-mango-egui-0.1.1 (c (n "mango-egui") (v "0.1.1") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "egui") (r "^0.20.1") (k 0)) (d (n "rfd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1s7g3zzv1yhr1p49dfic7s53ahj91q46gz2d5j3c0587wr7i9fsg") (f (quote (("default" "file")))) (y #t) (s 2) (e (quote (("file" "dep:rfd"))))))

(define-public crate-mango-egui-0.1.2 (c (n "mango-egui") (v "0.1.2") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "egui") (r "^0.20.1") (k 0)) (d (n "rfd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0x7vjmqnvl4zqlp0qy43y7d8z6hr6rxnjnzy3c0352lslpqnx55j") (f (quote (("default" "file")))) (s 2) (e (quote (("file" "dep:rfd"))))))

(define-public crate-mango-egui-0.1.3 (c (n "mango-egui") (v "0.1.3") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "egui") (r "^0.20.1") (k 0)) (d (n "rfd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "15xx6j66sl18c8pa4pssq4z5y5pxpgqa7b6qpxfkl4mcpcmhpgpa") (f (quote (("default" "file")))) (s 2) (e (quote (("file" "dep:rfd"))))))

