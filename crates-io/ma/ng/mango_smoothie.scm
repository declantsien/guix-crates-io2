(define-module (crates-io ma ng mango_smoothie) #:use-module (crates-io))

(define-public crate-mango_smoothie-0.1.0 (c (n "mango_smoothie") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1nrwi25g901d9c3f0r8rcc5rlni7kvs1s6fmild16yfgqqjqs599")))

(define-public crate-mango_smoothie-0.1.1 (c (n "mango_smoothie") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "~0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~0.8") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "0p1q1cv7qfs3vlgs288566vgzyjfmyldkfr4kahsrndwbqnhh8w4")))

(define-public crate-mango_smoothie-0.1.2 (c (n "mango_smoothie") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "~0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~0.8") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)))) (h "0ckwh00wzxwbg9xxdfd4v2j35hppq2jflys88d0kydwvxb174pmj")))

(define-public crate-mango_smoothie-0.2.0 (c (n "mango_smoothie") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_derive") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "1qc51pcynrd4dlxslm9xdk5lcl2wnifr1p9lybyfirw1xh8c33yk")))

