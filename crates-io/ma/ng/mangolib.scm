(define-module (crates-io ma ng mangolib) #:use-module (crates-io))

(define-public crate-mangolib-0.5.0 (c (n "mangolib") (v "0.5.0") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mango_ir") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=0.4.24") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scoped_name") (r "^0.2.0") (d #t) (k 0)) (d (n "string-interner") (r "^0.7.1") (d #t) (k 0)))) (h "1blndhd16hl9wvvgplv26bbc3n08w002gzzrbil0y087g302v5q5")))

(define-public crate-mangolib-0.6.0 (c (n "mangolib") (v "0.6.0") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mango_ir") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "scoped_name") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "ustr") (r "^0.7.0") (d #t) (k 0)))) (h "1929qmxkjj7m904z45cirrnqhzcy28jvfbbhzsdbnlf7208bnmb6")))

