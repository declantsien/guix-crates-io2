(define-module (crates-io ma ng manga_app_lib) #:use-module (crates-io))

(define-public crate-manga_app_lib-0.1.0 (c (n "manga_app_lib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1a91pf91sini09kiarrm23nsa7s2p515mx3rjzpigz0z0lm7pw0c") (y #t)))

(define-public crate-manga_app_lib-0.1.1 (c (n "manga_app_lib") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)))) (h "17pbxgh5b8as7a63xkdk2c6bmqayc3j7jjcy905yypwnpnj66wrd") (y #t)))

