(define-module (crates-io ma ng manger) #:use-module (crates-io))

(define-public crate-manger-0.1.0 (c (n "manger") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1cdy7d3abfg7y0n9pynpa6iyzmy60a892g2wywdcx4913vwz3078")))

(define-public crate-manger-0.1.1 (c (n "manger") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "14n3h03viqgmmb8zspr5d59ykpvbmilqdclsgw9l46hsi8yywgjm")))

