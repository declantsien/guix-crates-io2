(define-module (crates-io ma ng mangpt) #:use-module (crates-io))

(define-public crate-mangpt-0.1.0 (c (n "mangpt") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.17.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "19jklkr4rsyqg3rmw1l3rz70qfvz94ha7q9nam8siv7ydm46gvmj")))

