(define-module (crates-io ma ng mango) #:use-module (crates-io))

(define-public crate-mango-0.1.0 (c (n "mango") (v "0.1.0") (h "1nq1ih4qambxl3jvk360ibgb1ng0k7hlmn54jzlr8p25cinc5w64") (y #t)))

(define-public crate-mango-1.0.0 (c (n "mango") (v "1.0.0") (h "0xhcxi0z852cflv6d19jfmfy7p333asyl3jl465ckl6jpsdhxvm5") (y #t)))

(define-public crate-mango-0.2.0 (c (n "mango") (v "0.2.0") (h "0wp6526xd4r1p3p0zn93hnv9w71c1lasw8pwc96q8lj37krb31vj")))

(define-public crate-mango-0.3.0 (c (n "mango") (v "0.3.0") (d (list (d (n "derive-new") (r "^0.5.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">= 0.4.24") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (f (quote ("union"))) (d #t) (k 0)) (d (n "string-interner") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)))) (h "18fj32z0lwfhh0wprkbid0fmqrn4w92xcnzgkgpprzpzif42id55")))

(define-public crate-mango-0.4.0 (c (n "mango") (v "0.4.0") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=0.4.24") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.13") (f (quote ("union"))) (d #t) (k 0)) (d (n "string-interner") (r "^0.7.1") (d #t) (k 0)))) (h "1rkgbsh72b2vl68ijcaxbhhdh936l1s2r06ls0hmbx8wghs31gxa")))

