(define-module (crates-io ma ng mangle) #:use-module (crates-io))

(define-public crate-mangle-0.0.1 (c (n "mangle") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0hhiwbvmdcwbs0cvb429jr1y51bwvwhd31y5cmyxb4xn1rp1lfff")))

(define-public crate-mangle-0.0.2 (c (n "mangle") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "12g7jbgb7frraw0crjcxlypl02axlkszi8k6h9a0jpkkdah44dq0")))

(define-public crate-mangle-0.0.3 (c (n "mangle") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0xcb6dx7780kp5y29jy3m1xzjzjnzhnbphir3h7acx004zb8da6j")))

(define-public crate-mangle-0.0.4 (c (n "mangle") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0acgrxfraxnr2da9nrzvryc7v9r057xiqimlx7d2q7vnpjqzbf0m")))

(define-public crate-mangle-0.0.5 (c (n "mangle") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0hrn5ajrycsx9hh7a650jvc62axga8q67bwkp62ms2g3d29xsa0p")))

