(define-module (crates-io ma ng manganese) #:use-module (crates-io))

(define-public crate-manganese-0.0.0 (c (n "manganese") (v "0.0.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "fermium") (r "^200.12") (f (quote ("static_link"))) (k 0)) (d (n "gl33") (r "^0.0.0") (f (quote ("global_loader" "bytemuck" "chlorine" "debug_automatic_glGetError"))) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0yhgf34gzifjfwvw8z4inp7n7sgbrl8vx7k1ljyaa8clmkwmmlzb")))

