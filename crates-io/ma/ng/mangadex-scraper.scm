(define-module (crates-io ma ng mangadex-scraper) #:use-module (crates-io))

(define-public crate-mangadex-scraper-0.1.0 (c (n "mangadex-scraper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1fg26jsjlplyq6grz487y4sa4k2xr7j2pqbd29ba81qykw1828xm")))

(define-public crate-mangadex-scraper-0.2.0 (c (n "mangadex-scraper") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1za53j5czfsiqk1aq858k306dd8rclvy66qq7xv5fvalscq2bhxb")))

(define-public crate-mangadex-scraper-0.2.1 (c (n "mangadex-scraper") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1p1cllnb9bfx2437cq0jn9p8qxidrr9q1x0p7y07p0mvw23k9dsa")))

(define-public crate-mangadex-scraper-0.2.2 (c (n "mangadex-scraper") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "06h9ix28x10jkh77ch2grlrvza0nvw8s0cr016h7ivdc1xj2y10g")))

(define-public crate-mangadex-scraper-0.3.0 (c (n "mangadex-scraper") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0376v4n2aa48srk4c5p8lpv1xjn7655dbn46yk9bvrx688fqg887")))

(define-public crate-mangadex-scraper-0.3.1 (c (n "mangadex-scraper") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0c75nx5b2dig8rvm6s3mdc7s0d2nwr4sq8482qmjw269071l2fyh")))

(define-public crate-mangadex-scraper-0.5.0 (c (n "mangadex-scraper") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0nfzrr8yxbw7l2b9nbfazwijmv77ca9gv5dmy0czyk21k2y21gs8") (y #t)))

(define-public crate-mangadex-scraper-0.5.1 (c (n "mangadex-scraper") (v "0.5.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qmia6kd20d7iwpv887a4g1rzwki46qcfp9mc0as6qz8bkfjfijy")))

(define-public crate-mangadex-scraper-0.5.2 (c (n "mangadex-scraper") (v "0.5.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ywc4b6vn7zimkx18d9d2j40lxhplh15plfj6hj6m7liisvzp6w9")))

