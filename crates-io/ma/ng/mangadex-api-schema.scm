(define-module (crates-io ma ng mangadex-api-schema) #:use-module (crates-io))

(define-public crate-mangadex-api-schema-0.1.0 (c (n "mangadex-api-schema") (v "0.1.0") (d (list (d (n "mangadex-api-types") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("cookies" "json" "multipart" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0x0dlwm509jzxvga4zdbj95xji008pv8sl73rh8li4zdac4hi381") (y #t)))

