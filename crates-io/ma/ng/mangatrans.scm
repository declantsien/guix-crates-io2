(define-module (crates-io ma ng mangatrans) #:use-module (crates-io))

(define-public crate-mangatrans-1.0.0 (c (n "mangatrans") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1y9yyr8iws3fq0076yvqipybvj8csgx5g34dw68irbjamj3742q8") (y #t)))

(define-public crate-mangatrans-1.0.1 (c (n "mangatrans") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1g8qd0i2q3b3jd86vb1w1l34vrmzdpgdnkybdcqg69m7vd8hq06v")))

(define-public crate-mangatrans-1.0.2 (c (n "mangatrans") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "1fs5cy3jz5szwyg8dmzgbplri41r8qls8mi4j01aqliwdmlm607f")))

