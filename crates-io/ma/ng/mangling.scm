(define-module (crates-io ma ng mangling) #:use-module (crates-io))

(define-public crate-mangling-0.1.0 (c (n "mangling") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "11xhypcw28rix799jb9ykxjzi703gbh9d0kszzwlxnbwlikvjal3")))

(define-public crate-mangling-0.1.1 (c (n "mangling") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0q3p37gg81ckds6v11k5ni8xa13r3gjcmc2ncgmx8kw9gk0jhmpm")))

(define-public crate-mangling-0.2.0 (c (n "mangling") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1z4af8vda784jchrv70lpw3n3mg16wy36silzn0iaplhcc1v1lnv")))

(define-public crate-mangling-0.2.1 (c (n "mangling") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0wjyq4pyp2s2n8sz7l4kxcgs670md9g55ly0a7py51ya2hhplxr3")))

(define-public crate-mangling-0.2.2 (c (n "mangling") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1bgrsbq9gmxksp2jar53hlxzg7syl1k22xarrgnc98zpswb6k7ji")))

(define-public crate-mangling-0.2.3 (c (n "mangling") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "15hsjdcjqdi3b18b319szc32f5glxayj00czywynlmlq1760cyjw")))

