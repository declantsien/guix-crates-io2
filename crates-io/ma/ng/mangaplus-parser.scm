(define-module (crates-io ma ng mangaplus-parser) #:use-module (crates-io))

(define-public crate-mangaplus-parser-0.1.1 (c (n "mangaplus-parser") (v "0.1.1") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01dms0f4gxizhcymngx7gindf32asd9psxg91929rmaxd94cq6sd")))

(define-public crate-mangaplus-parser-0.2.0 (c (n "mangaplus-parser") (v "0.2.0") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02vl1l8hyl0msx590nc0sq91v6xq09x20fy5ijhpp5rfrxvzagl7")))

(define-public crate-mangaplus-parser-0.2.1 (c (n "mangaplus-parser") (v "0.2.1") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dk52zw7fwzzms6dnn7fkibszdr7vkr53g6w8c4kpkricvw5j9as")))

(define-public crate-mangaplus-parser-0.2.2 (c (n "mangaplus-parser") (v "0.2.2") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05axz8zbi1rk1bl0ra8n59dkcbknfyf2m1930i21zj78zy9ldgi9")))

(define-public crate-mangaplus-parser-0.2.3 (c (n "mangaplus-parser") (v "0.2.3") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1h7myfjs58gyb8qb7sfziy5x7pkp3g9bdiz94k6dic5xqw32ssdm")))

(define-public crate-mangaplus-parser-0.2.4 (c (n "mangaplus-parser") (v "0.2.4") (d (list (d (n "pb-rs") (r "^0.8") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jy5hyp0m0na539b1cg2gvp78hyj5ddsla3lj14f92wzy1v25mk0")))

(define-public crate-mangaplus-parser-0.2.5 (c (n "mangaplus-parser") (v "0.2.5") (d (list (d (n "pb-rs") (r "^0.8.2") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1f8246gyyq8lllfr8j58i1y9ss3gnlx9n70q69379jnn4wz1kbx6")))

(define-public crate-mangaplus-parser-1.0.0 (c (n "mangaplus-parser") (v "1.0.0") (d (list (d (n "pb-rs") (r "^0.8.2") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jdlvszif00lnjx52bn3z71xa8a9c8kh3xxsi6b0gs0gb215skhk") (f (quote (("test_helpers") ("owned" "test_helpers") ("no_cow") ("default"))))))

(define-public crate-mangaplus-parser-1.0.1 (c (n "mangaplus-parser") (v "1.0.1") (d (list (d (n "pb-rs") (r "^0.8.2") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16x7wpxcrs3ipc4dawagfjxpj18ifdipp287ln3fl583c1wqwzmb") (f (quote (("test_helpers") ("owned" "test_helpers") ("no_cow") ("default"))))))

(define-public crate-mangaplus-parser-1.1.1 (c (n "mangaplus-parser") (v "1.1.1") (d (list (d (n "pb-rs") (r "^0.8.2") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a9l8jc4gwk6dj10r2d0ysjis02w3x156nv5g0swz1m9r4lbdwi4") (f (quote (("no_cow") ("default"))))))

(define-public crate-mangaplus-parser-2.0.0 (c (n "mangaplus-parser") (v "2.0.0") (d (list (d (n "pb-rs") (r "^0.9") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g1fhm7cqm5k2mhwi1gmbl1rhwjrih7l4blvgwyv84a9pyvjks6g") (f (quote (("no_cow") ("default"))))))

