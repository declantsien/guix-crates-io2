(define-module (crates-io ma ng mangadex-api-types) #:use-module (crates-io))

(define-public crate-mangadex-api-types-0.1.0 (c (n "mangadex-api-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("cookies" "json" "multipart" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("serde-human-readable"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "092qmbaqrvaxx6bfprr7ycml9nx888bbg1lcm69d4lsb2l68rrfp") (y #t)))

