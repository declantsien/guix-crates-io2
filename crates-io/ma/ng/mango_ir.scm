(define-module (crates-io ma ng mango_ir) #:use-module (crates-io))

(define-public crate-mango_ir-0.1.0 (c (n "mango_ir") (v "0.1.0") (h "1viznzxg20fxid689bsx9vv2zgpg2cv38i8jqb13va16xyfvzhiv")))

(define-public crate-mango_ir-0.0.1 (c (n "mango_ir") (v "0.0.1") (h "0kw81bzym15v43s9d6q10ap8j7jnb4g2r8rkyg57yxmwmfrbnjyc")))

