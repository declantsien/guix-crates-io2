(define-module (crates-io ma ng manganis) #:use-module (crates-io))

(define-public crate-manganis-0.1.0 (c (n "manganis") (v "0.1.0") (d (list (d (n "manganis-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1r50jmn30fqj8nmvw0dv8bvxkcz14k426jc20qalrxbxwbn52nd2") (f (quote (("html") ("default"))))))

(define-public crate-manganis-0.1.1 (c (n "manganis") (v "0.1.1") (d (list (d (n "manganis-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0ryzzisfsmffgbh199jszakq5ai2i73rr977n73v83ia4asqf6yi") (f (quote (("html") ("default"))))))

(define-public crate-manganis-0.2.0 (c (n "manganis") (v "0.2.0") (d (list (d (n "dioxus-core") (r "^0.5.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "manganis-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1diqx315fv5bnsmd4b6sfifc070yrqlmdss54pcnf6fm9vdic2f1") (f (quote (("html") ("dioxus" "dioxus-core") ("default"))))))

(define-public crate-manganis-0.2.1 (c (n "manganis") (v "0.2.1") (d (list (d (n "dioxus-core") (r "^0.5.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "manganis-macro") (r "^0.2.1") (d #t) (k 0)))) (h "1g14pikkzsxzj4xlfzd1dqzv7yrg5mhr7vyjy5mhvxrrlq5bhbd4") (f (quote (("html") ("dioxus" "dioxus-core") ("default" "dioxus"))))))

(define-public crate-manganis-0.2.2 (c (n "manganis") (v "0.2.2") (d (list (d (n "dioxus-core") (r "^0.5.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "manganis-macro") (r "^0.2.1") (d #t) (k 0)))) (h "0whvknpxfqdib5qvj6ja7cdljpdi3cxfh1mn0wiv6ycwaascy4f2") (f (quote (("html") ("dioxus" "dioxus-core") ("default" "dioxus"))))))

