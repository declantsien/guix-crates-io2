(define-module (crates-io ma ng mangrove-cli) #:use-module (crates-io))

(define-public crate-mangrove-cli-0.1.2 (c (n "mangrove-cli") (v "0.1.2") (d (list (d (n "libmangrove") (r "^0.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "vergen") (r "^7.5.0") (f (quote ("build" "rustc"))) (k 1)))) (h "0x141zpkpwv1sg2ys98fpycfnv0vwi553bwh9qybcia64xfk8sxp")))

