(define-module (crates-io ma tx matx) #:use-module (crates-io))

(define-public crate-matx-0.1.0 (c (n "matx") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "188y7r9r6iqlr5yx1w1d3k1sabpk4g45bw5n0mx4sqabs40lddb8")))

(define-public crate-matx-0.2.0 (c (n "matx") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03znf3w6zj6ia3yx4z4hfz49shb6qk5s50frljxlrz3yvka2fci1")))

