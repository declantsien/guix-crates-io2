(define-module (crates-io ma re marek_translate_locally) #:use-module (crates-io))

(define-public crate-marek_translate_locally-1.0.0 (c (n "marek_translate_locally") (v "1.0.0") (d (list (d (n "async-process") (r "^1.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "marek_translate_api") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1421594hfyw4zshj51hgidd8j25b8snd8mbhphg9my8fa977naz3")))

(define-public crate-marek_translate_locally-1.0.1 (c (n "marek_translate_locally") (v "1.0.1") (d (list (d (n "async-process") (r "^1.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "marek_translate_api") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q7d7asqp7y27y2g1kgq4ssp92q7gm67svnm77pdlmzv18xq0a82")))

