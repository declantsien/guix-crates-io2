(define-module (crates-io ma re marek_google_speech_recognition) #:use-module (crates-io))

(define-public crate-marek_google_speech_recognition-1.0.0 (c (n "marek_google_speech_recognition") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsoda_sys") (r "^1.0") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "0dfblcq4am5xigdj7y0xlkfzh9yrfh5xpvxazn4p2pp6zrh3qvrv")))

(define-public crate-marek_google_speech_recognition-1.0.1 (c (n "marek_google_speech_recognition") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsoda_sys") (r "^1.0") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "0rz7nmkivazdk9r9v993k34wc69sa51wqjb02j663k05naf1xikd")))

(define-public crate-marek_google_speech_recognition-2.0.0 (c (n "marek_google_speech_recognition") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsoda_sys") (r "^1.0") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04szazsw0wjriwmnh46iny51pmlc26srj2sla87qknpwdswxd2z1")))

(define-public crate-marek_google_speech_recognition-2.1.0 (c (n "marek_google_speech_recognition") (v "2.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsoda_sys") (r "^1.0") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^2.1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14y4mrqgxh9ylg4a43mxrjrkrpr3jzwzkng4n57nr94hng93x7hx")))

(define-public crate-marek_google_speech_recognition-2.1.1 (c (n "marek_google_speech_recognition") (v "2.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libsoda_sys") (r "^1.0") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^2.1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g30220j0vpgjx61smic0w2lbz4xra5z9svjcbq3jijazfk9iyck")))

