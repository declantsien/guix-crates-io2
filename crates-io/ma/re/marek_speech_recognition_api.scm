(define-module (crates-io ma re marek_speech_recognition_api) #:use-module (crates-io))

(define-public crate-marek_speech_recognition_api-1.0.0 (c (n "marek_speech_recognition_api") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "16ldrqq088ac2rklnvrgdadxca907jnc0xrgf5zhkn2zf2s6lj60")))

(define-public crate-marek_speech_recognition_api-2.0.0 (c (n "marek_speech_recognition_api") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1y476kh9bjyr3kcnzja96wpb4fnf43vwsykx25ig898lvc1a2nws")))

(define-public crate-marek_speech_recognition_api-2.1.0 (c (n "marek_speech_recognition_api") (v "2.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0q43nw4r1j3jf8brbhx11hmg63phc18wbk8g9pdsp5dv456p7gvb")))

(define-public crate-marek_speech_recognition_api-2.1.1 (c (n "marek_speech_recognition_api") (v "2.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0c9zmdmbj6sdpzazpixvvga7520k5b11a57kwli8d0qmysmskzq1")))

