(define-module (crates-io ma re marek_vosk_speech_recognition) #:use-module (crates-io))

(define-public crate-marek_vosk_speech_recognition-2.0.0 (c (n "marek_vosk_speech_recognition") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^2.0") (d #t) (k 0)) (d (n "vosk") (r "^0.2") (d #t) (k 0)))) (h "0dqbydcjb8dcp8212vfl7va87lrx0s4mqkm77dljbicyqsy3bxpy")))

(define-public crate-marek_vosk_speech_recognition-2.1.0 (c (n "marek_vosk_speech_recognition") (v "2.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "marek_speech_recognition_api") (r "^2.1") (d #t) (k 0)) (d (n "vosk") (r "^0.2") (d #t) (k 0)))) (h "1nvqg0bd449xp4pzrxqhw8rnn7518dba7a5k4dhi7h1kpzsgcnzc")))

