(define-module (crates-io ma rc marching-squares) #:use-module (crates-io))

(define-public crate-marching-squares-0.1.0 (c (n "marching-squares") (v "0.1.0") (d (list (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "05brknk371h089lsy8wbg19wc1bs8xvrvcvjp4x1yhxm3z20vmcm") (f (quote (("parallel" "rayon"))))))

(define-public crate-marching-squares-0.1.1 (c (n "marching-squares") (v "0.1.1") (d (list (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0f0ccddj516001mfxbas3fzvvvjiav6bkjgsx59zv457mn28liq1") (f (quote (("parallel" "rayon"))))))

