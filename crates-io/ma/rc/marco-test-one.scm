(define-module (crates-io ma rc marco-test-one) #:use-module (crates-io))

(define-public crate-marco-test-one-0.1.0 (c (n "marco-test-one") (v "0.1.0") (h "0sr61bvc6lbnl1q03dw3jrjqmzx8j3m0ggkc7qkgwbj6j89sd4a1")))

(define-public crate-marco-test-one-0.1.1 (c (n "marco-test-one") (v "0.1.1") (h "1vnw1svpynyppqy7zplqqvnrx8ykal12h1pgg7srad6wvvmsyvgs")))

(define-public crate-marco-test-one-0.1.2 (c (n "marco-test-one") (v "0.1.2") (h "1bjdi2jxhpsfadm4pyj0lcmswzr38vgzwrpnd6z6myjvczg1i6ls")))

(define-public crate-marco-test-one-0.1.3 (c (n "marco-test-one") (v "0.1.3") (h "14dv8qbmf44bmaj52f4rc3bmnm5kp49j8kl175pika63avlcdxl9")))

(define-public crate-marco-test-one-0.1.4 (c (n "marco-test-one") (v "0.1.4") (h "0ajck0b7hapbxx0xrvsppqrzbivcy62a03rsczz2zi8mlw87fh3p")))

(define-public crate-marco-test-one-0.1.5 (c (n "marco-test-one") (v "0.1.5") (h "1vhnjlb3h66y1l3czmvba32pl6mqccm2ana92cxqqy5pdhrg4afg")))

(define-public crate-marco-test-one-0.1.6 (c (n "marco-test-one") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "00y459p5arlpy3nnjaisr3p85jnj0nk14n17a5z4zj9zv05rad4j")))

(define-public crate-marco-test-one-0.1.7 (c (n "marco-test-one") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "148jiaznmdqnrz939n8ldgmf8ak7fqcc9ixspcyblii94gpprc0b")))

(define-public crate-marco-test-one-0.1.8 (c (n "marco-test-one") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "0sck93hha5lw57612bh76b6zira508zj7ffdpbf8r5v7pdbqrfis")))

(define-public crate-marco-test-one-0.1.9 (c (n "marco-test-one") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "1475ripakmchb91iwdcgyqvaqcp1sfjcq4y5syl9r2i87wi3nk0f")))

(define-public crate-marco-test-one-0.1.10 (c (n "marco-test-one") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "11snz4241w2j4hrqsz3l70yrz3xgk7388mxi5ym28aazjhc29pd0")))

(define-public crate-marco-test-one-0.1.11 (c (n "marco-test-one") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)))) (h "15fmldqkh02ikb5jl84bhhk90h0aqqwpxdzzhjn6lzxdfibn7c8z")))

(define-public crate-marco-test-one-0.1.12 (c (n "marco-test-one") (v "0.1.12") (h "11f2q648fdaffakmiin4l78fxznpf8b16csgw6yyxjkmwnn8r706")))

(define-public crate-marco-test-one-0.1.13 (c (n "marco-test-one") (v "0.1.13") (h "1qmc4386k9zkp34x474q7hh6nrm38avgpll7lxyzn9jrn867h764")))

(define-public crate-marco-test-one-0.1.14 (c (n "marco-test-one") (v "0.1.14") (h "122xkfh5zkfi7hlhfq86gczr22vv4nhvdzx1f461yw7h58pia9jh")))

(define-public crate-marco-test-one-0.1.15 (c (n "marco-test-one") (v "0.1.15") (h "0v3g06jiq91ym748ix40an26d28by3w3z65vkwcg5srdhjr9660h")))

(define-public crate-marco-test-one-0.1.16 (c (n "marco-test-one") (v "0.1.16") (h "03glipcgab6yabb30s55bjc7glkdds131hdl771kffgjlf90w2aw")))

(define-public crate-marco-test-one-0.1.17 (c (n "marco-test-one") (v "0.1.17") (h "0rg294cc9r1bdzakny6smx1bsv6dgsdx22cs1pa483id1iiz5k6n")))

(define-public crate-marco-test-one-0.1.18 (c (n "marco-test-one") (v "0.1.18") (h "07fld9r5pw0nc3ynj1bvmvibf2rq51dp1lpjlyhg02r955h7ssn0")))

(define-public crate-marco-test-one-0.1.19 (c (n "marco-test-one") (v "0.1.19") (h "0w63njljd6awymc1rqhjhpd3gmgpfjk41fw9jw2bvhbvhxv8nm9a")))

(define-public crate-marco-test-one-0.1.20 (c (n "marco-test-one") (v "0.1.20") (h "167pwzfhb87l85hff019w3yr9nqra2lr75dpazydrwzjb6dd1xqa")))

(define-public crate-marco-test-one-0.2.0 (c (n "marco-test-one") (v "0.2.0") (h "0y61jcb79mfrvdzhhpxz3kdvqv0ysmk52q68q3wbr4dbhyrpwynr")))

(define-public crate-marco-test-one-0.2.1 (c (n "marco-test-one") (v "0.2.1") (h "03f46bjqzq89jax95r32fapzfbpadw5qr0ccc6dxalv22f4zrgi6")))

(define-public crate-marco-test-one-0.2.2 (c (n "marco-test-one") (v "0.2.2") (h "1c6rkcnngr6irkf2hsaqjqlgk7w21pxf8n147aznnq8x4ihrmc9y")))

(define-public crate-marco-test-one-0.2.3 (c (n "marco-test-one") (v "0.2.3") (h "14qi1g7daf5gdg2x9ll0bg1zv92x7baa9zq8izz1lyi5dd8zv1m6")))

(define-public crate-marco-test-one-0.2.4 (c (n "marco-test-one") (v "0.2.4") (h "019qdpykh6vmvc386s1lyfpbdrdkby356milj84qk4rm50a2vm5c")))

(define-public crate-marco-test-one-0.2.5 (c (n "marco-test-one") (v "0.2.5") (h "0bdhkqhh5q28p1z02pjjxyl002avs5hhf5pgzy8k9sgfi75dnli8")))

(define-public crate-marco-test-one-0.2.6 (c (n "marco-test-one") (v "0.2.6") (h "1arv9r6qpijvw6jznchdfgvyj7pvgz9v27njyp1ymb9fkv5hwwhy")))

(define-public crate-marco-test-one-0.3.0 (c (n "marco-test-one") (v "0.3.0") (h "0aqzm5s6gx2991zvmy8mkp39v5rgpz2pw3i3b4ifc6v4nxrcyb38")))

(define-public crate-marco-test-one-0.3.1 (c (n "marco-test-one") (v "0.3.1") (h "0qzwraw29pa39h4iixi92vchk7dwbz0nv2rw6ifqmpnybnjkrb2v")))

(define-public crate-marco-test-one-0.3.2 (c (n "marco-test-one") (v "0.3.2") (h "0f4a344qmb1bvplml6agjrxrnkbv6ikzv2b5ff8az6cw6iqfhcn3")))

(define-public crate-marco-test-one-0.3.3 (c (n "marco-test-one") (v "0.3.3") (h "0vb4ryyjy8rr3wjz1pm853pc888dyyd80hdhcyazg41zylw3gw68")))

(define-public crate-marco-test-one-0.3.4 (c (n "marco-test-one") (v "0.3.4") (h "0yvc73nqrcbg9fhk1d13xj21mv8wrg4hzn88y3ky6bmm73r9qlcm")))

(define-public crate-marco-test-one-0.3.5 (c (n "marco-test-one") (v "0.3.5") (h "0kbjz01q7ympz7m52iy8nyfwjrx5d53znfjdy02f9zd6pkfdi5cb")))

(define-public crate-marco-test-one-0.3.6 (c (n "marco-test-one") (v "0.3.6") (h "1q8s1pmw6r1cpkcdkc58ricnkajdkvsjixm0zks8whdd1n2q74y9")))

(define-public crate-marco-test-one-0.3.7 (c (n "marco-test-one") (v "0.3.7") (h "192jxl6mgy6061czcj06ydm81cnvn95ckl9v0bfc4myckay3gy6n")))

(define-public crate-marco-test-one-0.3.8 (c (n "marco-test-one") (v "0.3.8") (h "1kj3n2ryrppwzdp5731vqxxz3b9qzy2r9hwfxjar80vdghks0y8d")))

(define-public crate-marco-test-one-0.3.9 (c (n "marco-test-one") (v "0.3.9") (h "1h0g2dqy6pkm9z0r96lcgdpn1gy4q125k33k9f8r3pxiyx2nfcfz")))

(define-public crate-marco-test-one-0.3.10 (c (n "marco-test-one") (v "0.3.10") (h "0h68z5szhn9yyk15iyxbf2wdscslvhwhy2s0mb9lz1xm6n2mxs3q")))

(define-public crate-marco-test-one-0.3.11 (c (n "marco-test-one") (v "0.3.11") (h "0hn3fkvdfpfgpqwz17c9isk87c2zs0li81slvlhl6xn8zn12lgcb")))

(define-public crate-marco-test-one-0.3.12 (c (n "marco-test-one") (v "0.3.12") (h "0sv16r4ibj4pz4s9s1hwscjpsmvrnb9a31wm6d04xfd5aldgznk3")))

