(define-module (crates-io ma rc marco-test-two) #:use-module (crates-io))

(define-public crate-marco-test-two-0.1.0 (c (n "marco-test-two") (v "0.1.0") (d (list (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1svr53qip00d4z33a2c40s9h4cwcydd0l3nplqlma12lxd2ln5hh")))

(define-public crate-marco-test-two-0.1.1 (c (n "marco-test-two") (v "0.1.1") (d (list (d (n "marco-test-one") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0lxfmirw3yqh737xcmyg7h58jciad1hcr61ivkqwn2x1acw0clfg")))

(define-public crate-marco-test-two-0.1.2 (c (n "marco-test-two") (v "0.1.2") (d (list (d (n "marco-test-one") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "154p0w17k0ivpyhbl3y6l2hiilqjyxxz10h81lhg2rq2j1cli38k")))

(define-public crate-marco-test-two-0.1.3 (c (n "marco-test-two") (v "0.1.3") (d (list (d (n "marco-test-one") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0xpnh0wgf1y3d45q8xiq64wa31ia58bbsjny36fdxydlyqr20v6d")))

(define-public crate-marco-test-two-0.1.4 (c (n "marco-test-two") (v "0.1.4") (d (list (d (n "marco-test-one") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0a1hifxjjpc5rglyr6dacrqc04k631kvvgd191ym9pzm2v66zdri")))

(define-public crate-marco-test-two-0.1.5 (c (n "marco-test-two") (v "0.1.5") (d (list (d (n "marco-test-one") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1grw016kli7z3z0wbamvzh7clgwf32qmzwqz1yb284qb4vfj54zn")))

(define-public crate-marco-test-two-0.1.6 (c (n "marco-test-two") (v "0.1.6") (d (list (d (n "marco-test-one") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0d68avn0mish3c1vaicjc049jm1yys5lss9rwxw4fk8i7gyil1kc")))

(define-public crate-marco-test-two-0.1.7 (c (n "marco-test-two") (v "0.1.7") (d (list (d (n "marco-test-one") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1cipxvb3lmvvnxrnbrs7c9kwnwsn19nrnkkysfc7hccaicds44kg")))

(define-public crate-marco-test-two-0.1.8 (c (n "marco-test-two") (v "0.1.8") (d (list (d (n "marco-test-one") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0vad9ars09y2hmr94lisilypbcw0slnwz4l5x1n44g82gzhwh5g7")))

(define-public crate-marco-test-two-0.1.9 (c (n "marco-test-two") (v "0.1.9") (d (list (d (n "marco-test-one") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "036bqaq2900y9yvzr9db5d22rq0kqwrfcq4pprgd22sgwca4i2rb")))

(define-public crate-marco-test-two-0.1.10 (c (n "marco-test-two") (v "0.1.10") (d (list (d (n "marco-test-one") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "17qshsf9vz4ngjrfbn2b56qxvlp2mppawrvbrqgxc80cy9anvsz0")))

(define-public crate-marco-test-two-0.1.11 (c (n "marco-test-two") (v "0.1.11") (d (list (d (n "marco-test-one") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "19a8986c7fx7vk2b5q6zg11s64dyh2w8y1515pvfml5zdj82kz6s")))

(define-public crate-marco-test-two-0.1.12 (c (n "marco-test-two") (v "0.1.12") (d (list (d (n "marco-test-one") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1s5fgvwz512qifis2nvch960lqcba6yf7lq4xwx17xnscvwjlxwn")))

(define-public crate-marco-test-two-0.1.13 (c (n "marco-test-two") (v "0.1.13") (d (list (d (n "marco-test-one") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "151qda1l9fid2m28251fyg7v8nmxm8x6cbz1x8h21y5g99wa32z4")))

(define-public crate-marco-test-two-0.1.14 (c (n "marco-test-two") (v "0.1.14") (d (list (d (n "marco-test-one") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1wh4n4i8f0sjf9dx8vz4gqrkj06cg6kwy2zpg850vvfnx93qcjxv")))

(define-public crate-marco-test-two-0.1.15 (c (n "marco-test-two") (v "0.1.15") (d (list (d (n "marco-test-one") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0lgnsyb7w50057d3px1w7saa05yqrznz2m9hi825npss21ickbcn")))

(define-public crate-marco-test-two-0.1.16 (c (n "marco-test-two") (v "0.1.16") (d (list (d (n "marco-test-one") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0aqxda4mg5kgdmx8ady6km1fjf5i4g0yqkzk4rmj78r0bhfyw203")))

(define-public crate-marco-test-two-0.1.17 (c (n "marco-test-two") (v "0.1.17") (d (list (d (n "marco-test-one") (r "^0.1.13") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0hni1155qqy99yrr2iskwxj8kxj2a7gcsvdd2i4kxps0q5gw3bz2")))

(define-public crate-marco-test-two-0.1.18 (c (n "marco-test-two") (v "0.1.18") (d (list (d (n "marco-test-one") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1sc9zh4d1smxh260s8izz02943mhnp32fmdgmsi1f68madadsdjl")))

(define-public crate-marco-test-two-0.1.19 (c (n "marco-test-two") (v "0.1.19") (d (list (d (n "marco-test-one") (r "^0.1.15") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0d90k57p7ljpyly4x97g61dn8nd0546jnb8zjxks69div4ab1vgy")))

(define-public crate-marco-test-two-0.1.20 (c (n "marco-test-two") (v "0.1.20") (d (list (d (n "marco-test-one") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1av13b196cx9ycqgwr8jrs222ybrcpmx6h0h7rqmak10r2j63055")))

(define-public crate-marco-test-two-0.1.21 (c (n "marco-test-two") (v "0.1.21") (d (list (d (n "marco-test-one") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1gyndbq1sp5jl5wd9x8gq113i2wc2apizx0sgshignwkayryja7w")))

(define-public crate-marco-test-two-0.1.22 (c (n "marco-test-two") (v "0.1.22") (d (list (d (n "marco-test-one") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "15k2ym4vg6a7nsr85y6ippjqx0bvrmnmpbc4k3ws2hzn8m14hn75")))

(define-public crate-marco-test-two-0.1.23 (c (n "marco-test-two") (v "0.1.23") (d (list (d (n "marco-test-one") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "08677f236i3mlpai2rmkhlgqyd9w11npbi7yfv5rc0apsj4614nw")))

(define-public crate-marco-test-two-0.1.24 (c (n "marco-test-two") (v "0.1.24") (d (list (d (n "marco-test-one") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "18kdwnfzxjrlch69x6i7l3mhvfnfvkfs046w6ji0d8wjpgvymxk4")))

(define-public crate-marco-test-two-0.2.0 (c (n "marco-test-two") (v "0.2.0") (d (list (d (n "marco-test-one") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0bp4a4kim17hv28nzzjzaablsr15fx50dciidk2sz9aj875mk8i4")))

(define-public crate-marco-test-two-0.2.1 (c (n "marco-test-two") (v "0.2.1") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "1h4xpw19a7wz318h0pcg2nb736nibd2x5wrz4dgcawkqmksb2d6f")))

(define-public crate-marco-test-two-0.3.0 (c (n "marco-test-two") (v "0.3.0") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0lmbf283w15228f98ymx1xb5454is74hjgsd6msvkvjjah2ba8b8")))

(define-public crate-marco-test-two-0.4.0 (c (n "marco-test-two") (v "0.4.0") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0g3lx8r3fwrc0xscv9n5c38k0sqmpb4lga182a69c3wmzx9577kq")))

(define-public crate-marco-test-two-0.4.1 (c (n "marco-test-two") (v "0.4.1") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "06w9cafm9jwz27q6kamjq9967w47brl4cyqzfnsqjx6nnl2n1fyh")))

(define-public crate-marco-test-two-0.4.2 (c (n "marco-test-two") (v "0.4.2") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "04pw350d8lk42jzvs3cxiriblr4xacq9cg5pgzygx6q7mzvcgc28")))

(define-public crate-marco-test-two-0.4.3 (c (n "marco-test-two") (v "0.4.3") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "10kl4hxpsmilhmdj8z3mv6dismr6ydfrlz2f0mgvl8hww8wsjl3q")))

(define-public crate-marco-test-two-0.4.4 (c (n "marco-test-two") (v "0.4.4") (d (list (d (n "marco-test-one") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "05ira0wxaj0vpd9gkpjfzchj5jnk8hx43riiw9zp9aqb6gnpqxgm")))

(define-public crate-marco-test-two-0.4.5 (c (n "marco-test-two") (v "0.4.5") (d (list (d (n "marco-test-one") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "0h6r86lxv64b035k4xzhca1aq9nakpyk0qa5caqdsyrmi9wm5vbf")))

(define-public crate-marco-test-two-0.4.6 (c (n "marco-test-two") (v "0.4.6") (d (list (d (n "marco-test-one") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (d #t) (k 0)))) (h "087s3gs0kpazjwgb5kawjsm4iprl6q5v71ha4p77rdch417hqrgj")))

(define-public crate-marco-test-two-0.4.7 (c (n "marco-test-two") (v "0.4.7") (d (list (d (n "marco-test-one") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (d #t) (k 0)))) (h "1ysvczkw67i51nvfp9xajy1m1yclckc1m6b0pzpxb08800by6i3d")))

(define-public crate-marco-test-two-0.4.8 (c (n "marco-test-two") (v "0.4.8") (d (list (d (n "marco-test-one") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (d #t) (k 0)))) (h "06315b19xy09zzfvd618a8lkfpizb283mdvfjcca1cp2f6r299vj")))

(define-public crate-marco-test-two-0.4.9 (c (n "marco-test-two") (v "0.4.9") (d (list (d (n "marco-test-one") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "0rgmx1qj8z38ri50jkrjnsi8ihc12kqbkr0i0nb4zlwa3jzd0x3h")))

(define-public crate-marco-test-two-0.4.10 (c (n "marco-test-two") (v "0.4.10") (d (list (d (n "marco-test-one") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "0hvfc61rd0151qvc9ra815w51bv74ds4zqfs5wprjqd9pkwcva3j")))

(define-public crate-marco-test-two-0.4.11 (c (n "marco-test-two") (v "0.4.11") (d (list (d (n "marco-test-one") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "05rl54zyfbinpsj4cpgc42671b5dmsmvz498x5wzxg2b4nr2j0y9")))

(define-public crate-marco-test-two-0.4.12 (c (n "marco-test-two") (v "0.4.12") (d (list (d (n "marco-test-one") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "0ijfv35cvl5psqvc0nns0ws6dyy8a9s28407gqxay8bxm8x1yzk7")))

(define-public crate-marco-test-two-0.4.13 (c (n "marco-test-two") (v "0.4.13") (d (list (d (n "marco-test-one") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "0whnkr3y8k33ng5qxl3j9z73n4d7ndg7l2kpn5pph40p74imx86f")))

(define-public crate-marco-test-two-0.4.14 (c (n "marco-test-two") (v "0.4.14") (d (list (d (n "marco-test-one") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "011lpx0838v8fm823yzmvq86pnrsjps58jhfblsp5ck3n4xfa57y")))

(define-public crate-marco-test-two-0.4.15 (c (n "marco-test-two") (v "0.4.15") (d (list (d (n "marco-test-one") (r "^0.3.8") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "038y6gdhs97bzbzg77hslq4a134gqdf1r1nzzpik52iqmwpk0if8")))

(define-public crate-marco-test-two-0.4.16 (c (n "marco-test-two") (v "0.4.16") (d (list (d (n "marco-test-one") (r "^0.3.9") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "14m39gjxwh5lg4gplzqjdi6iqbwwiqqf8ay3il7vcaf43567lq22")))

(define-public crate-marco-test-two-0.4.17 (c (n "marco-test-two") (v "0.4.17") (d (list (d (n "marco-test-one") (r "^0.3.10") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "1nfw26qqdsssqgxxmckxhng63mh7dza7i0mig3cdlyk0cryc4dks")))

(define-public crate-marco-test-two-0.4.18 (c (n "marco-test-two") (v "0.4.18") (d (list (d (n "marco-test-one") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "1y7isnr91zjnyp58yfj09dm8igyw6h0c36aiaxbl7d6kpi6jjp37")))

(define-public crate-marco-test-two-0.4.19 (c (n "marco-test-two") (v "0.4.19") (d (list (d (n "marco-test-one") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)))) (h "16zngbmzcds97w16l6ld49vpcifimzshjqnbfh0374wrc93436x1")))

