(define-module (crates-io ma rc marc) #:use-module (crates-io))

(define-public crate-marc-0.1.0 (c (n "marc") (v "0.1.0") (h "1yfmp1y79w6cp5dyvangwfv2h4lwds6gn0nki72q02j7yjkczy7g")))

(define-public crate-marc-0.2.0 (c (n "marc") (v "0.2.0") (h "051lnakcy0bg50yl5ncphn4xd4zsh9m9pp6345715yh521r88iyb")))

(define-public crate-marc-0.2.1 (c (n "marc") (v "0.2.1") (h "0d220rw9g7iy0938h8rkxbgci85cjxw0hi9z26drplbh3q2gwaly")))

(define-public crate-marc-0.2.2 (c (n "marc") (v "0.2.2") (h "0y8say22zvdwmvww2a4q2adhvj2c80w84vp36k7mycjxh2343cfl")))

(define-public crate-marc-0.2.3 (c (n "marc") (v "0.2.3") (h "1mkr9404ssyjan0lfyk1g0a9canh63ki9yk3sfz03jb2aai4z8q6")))

(define-public crate-marc-0.2.4 (c (n "marc") (v "0.2.4") (h "1hpdvbshrbw25k7m18c19vql0hv7i35lhz549y6xsh6ibslvpac6")))

(define-public crate-marc-0.2.5 (c (n "marc") (v "0.2.5") (h "0vyiva04qz110i9bjs99wdr66bn8na8d1zpzd1al19s6bs5q1n0l")))

(define-public crate-marc-0.2.6 (c (n "marc") (v "0.2.6") (h "0lvw68797nhjcgng3s0n8ppq4fim3vmblr2n03y3bj1dqapf7q48") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-0.3.0 (c (n "marc") (v "0.3.0") (h "1wgkmcb8mkvwmhsdw6iwzig5wz890m6fzwamig6809vyfdi324w4") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-0.4.0 (c (n "marc") (v "0.4.0") (h "1srhdnhgcb7lipl3iz0imc8jjwh3j3jq0mba1vqv2qhfyvxnqzjx") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-0.5.0 (c (n "marc") (v "0.5.0") (h "0p1nfp2dawfi0jf94cr54gqvc3dbb3h42pjgqcx1z4804pl6c14c") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.0.0 (c (n "marc") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)))) (h "1y320xjisp5spdwd8610hxr1ln67aja4q6v9bpl8vxjny5j02znx") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.1.0 (c (n "marc") (v "1.1.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)))) (h "0k8sh8vxzwmaxv1dgq7200j3gmi6iqbybhd8pplrib8kd9kkijn2") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.2.0 (c (n "marc") (v "1.2.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)))) (h "0j5ym3fiamj7bxvqgh40zhipfh64b38aczs40cgsqg8sl7xnd3z1") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.2.1 (c (n "marc") (v "1.2.1") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)))) (h "1a3y6fakrr9accrwg3m773pfrx2hnaqls98g1x0x6qab2kvvs7vb") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.3.0 (c (n "marc") (v "1.3.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)))) (h "1i6qi4gyw467xgiclw658vmxfxmm6k67pch94arn2qwih5f0127b") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.4.0 (c (n "marc") (v "1.4.0") (d (list (d (n "error-chain") (r "^0.7.0") (d #t) (k 0)))) (h "1l2z9wqv9jwnb43pak25qpigalcp6liijknyb085zgrf700zi30r") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-1.5.0 (c (n "marc") (v "1.5.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0k46n2qj68w87aqd73z64ysahcz76bgfnajkyrrxr5jic4im7f1l") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-2.0.0 (c (n "marc") (v "2.0.0") (h "0jq57g2cxijv42bm0h08gl3ara3wwndvilqkhi4xhr6gc90y0r7y") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-3.0.0 (c (n "marc") (v "3.0.0") (h "0kxmkwpic53mf0yjhzv33rwpfl5b2qvrrr320qg8x8gw5nwrg88g") (f (quote (("nightly") ("default"))))))

(define-public crate-marc-3.1.0 (c (n "marc") (v "3.1.0") (d (list (d (n "xml-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "107pvjvb3gb509rzv3wli40z59dm3ydgagwbd9n8j75ff7cfwkny") (f (quote (("nightly") ("default")))) (s 2) (e (quote (("xml" "dep:xml-rs"))))))

(define-public crate-marc-3.1.1 (c (n "marc") (v "3.1.1") (d (list (d (n "xml-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0mhabhk43p76k542w3ahm4d9x9n3sd550jwp9vqfs833z9hqh5f7") (f (quote (("nightly") ("default")))) (s 2) (e (quote (("xml" "dep:xml-rs"))))))

