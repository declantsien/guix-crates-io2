(define-module (crates-io ma rc marco_paspuel_adder) #:use-module (crates-io))

(define-public crate-marco_paspuel_adder-0.0.5 (c (n "marco_paspuel_adder") (v "0.0.5") (h "1hf01yb18nlv9jn6ljyz929msq71m3rkwydm7108vj4xxdij0nr0")))

(define-public crate-marco_paspuel_adder-0.0.6 (c (n "marco_paspuel_adder") (v "0.0.6") (h "1srzzs8cwc6p8f9qpd4jyb0sf1qvpxi3agy9819x7skhm4jx18b3")))

