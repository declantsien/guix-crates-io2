(define-module (crates-io ma rc marco_paspuel_add_one) #:use-module (crates-io))

(define-public crate-marco_paspuel_add_one-0.0.5 (c (n "marco_paspuel_add_one") (v "0.0.5") (h "1zz07bvr4r7s34g2z90rzc51dk19xgr0g947c56v5a2r09gcr7zi")))

(define-public crate-marco_paspuel_add_one-0.0.6 (c (n "marco_paspuel_add_one") (v "0.0.6") (h "1jh7m5z8sam3pyas98lfh4cpfiicjpwzijysnf9wa5sp4qik6xn3")))

(define-public crate-marco_paspuel_add_one-0.0.7 (c (n "marco_paspuel_add_one") (v "0.0.7") (h "0ml5rfz5fi3rqzzlg3lf9vlymkisj0ga2nj2nhrs4kpf2pfkqnsi")))

