(define-module (crates-io ma rc marco-test-three) #:use-module (crates-io))

(define-public crate-marco-test-three-0.1.0 (c (n "marco-test-three") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)))) (h "102lqlb74bbsyi3d9ivwqylfpc54vwnb17bw7s4c0df0dakwip5j")))

(define-public crate-marco-test-three-0.1.1 (c (n "marco-test-three") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0hkzmq4b2zlpj6r40g9sxqllis515513g9q21wa5m96a2im2zkjj")))

(define-public crate-marco-test-three-0.1.2 (c (n "marco-test-three") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "01kjcnjsqq646sic24a6hgkyarhif0m3zl9w47gcpix85cd4f6fz")))

(define-public crate-marco-test-three-0.1.3 (c (n "marco-test-three") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0h8809j5jjpcqdkvn9i3i33jp30cfkcf9n5wypbmk6hig7m3riam")))

(define-public crate-marco-test-three-0.1.4 (c (n "marco-test-three") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "02xi74p14v2k303rglpnrg9zl90s90mk5ppa78cdlvs2inssknpp")))

(define-public crate-marco-test-three-0.1.5 (c (n "marco-test-three") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "0bb3y8x4q0ji3bq6yikly8g1xpnymfhms3x9kbir1cngnwcv00k8")))

(define-public crate-marco-test-three-0.1.6 (c (n "marco-test-three") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "13l08kb3hlw3wvih3q9x1zdc3z9d59pc5gyay1drjhzpd0lsnyh7")))

(define-public crate-marco-test-three-0.1.7 (c (n "marco-test-three") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "1ndm97skcclld4w9ifllfkqahqfp6xlabpva7cxwi36dy44dbzy9")))

(define-public crate-marco-test-three-0.1.8 (c (n "marco-test-three") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "03p98hgjl0xpwbp2ql1f43apciigdlrn30d83q1pa8m5bdwi71gs")))

(define-public crate-marco-test-three-0.1.9 (c (n "marco-test-three") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "082mzbdag53kmywrlsj782jjh7d919yxy98n76wjl7g01p8dh98h")))

(define-public crate-marco-test-three-0.1.10 (c (n "marco-test-three") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "1wh0xb3mb6pg6xcy5d4z9k859fsy3pb2cprdblfj70akbxmjg037")))

(define-public crate-marco-test-three-0.1.11 (c (n "marco-test-three") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "0ya57ixkcbmz6lwivrpvm42blawpagrw3r1kshgv6hnmxpnd96dv")))

(define-public crate-marco-test-three-0.1.12 (c (n "marco-test-three") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "0jsaq4c654ymk2mqq69v5jji0140bf5mlg2vi2kw563kgrcq9yj9")))

(define-public crate-marco-test-three-0.1.13 (c (n "marco-test-three") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "0yhw0sb13qdqbpd8qfcix28lds8dan3hhhgyfipg39krx3b3ng95")))

(define-public crate-marco-test-three-0.1.14 (c (n "marco-test-three") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0sjqjrmal3isawkr23wl6shyvjnvx85l9mcs6glgb94a0h31qimb")))

(define-public crate-marco-test-three-0.1.15 (c (n "marco-test-three") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0rjhbd2jshq0dqdajg5n2ydss94b2bsmydqkr28npgwml4z348dk")))

