(define-module (crates-io ma rc marco-gitea-test) #:use-module (crates-io))

(define-public crate-marco-gitea-test-0.1.0 (c (n "marco-gitea-test") (v "0.1.0") (h "18hlqd1r8zlq7m3v43krzghslk1zbji15cky07aqhpalyh0vz0nq")))

(define-public crate-marco-gitea-test-0.1.1 (c (n "marco-gitea-test") (v "0.1.1") (h "1ypppdi63lr50n2r3axl7ivni60ri8xk985d8y1zpqfyw4jdjjnc")))

(define-public crate-marco-gitea-test-0.1.2 (c (n "marco-gitea-test") (v "0.1.2") (h "19vsvwj15kxglhv9klgpf3w8ndh4vcmf9xn3zg10daddmw970676")))

(define-public crate-marco-gitea-test-0.1.3 (c (n "marco-gitea-test") (v "0.1.3") (h "0ch6q5r3i5l5xyqvfsbz81v5iypphkbd7p4cmasaj3099a5s1gzc")))

(define-public crate-marco-gitea-test-0.1.4 (c (n "marco-gitea-test") (v "0.1.4") (h "0lhxw6dls54miicdldc3qb3cyrgxxy02kkrdyfdzx1i7bhv6d680")))

