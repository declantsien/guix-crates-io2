(define-module (crates-io ma rc marcfly_learningrust) #:use-module (crates-io))

(define-public crate-MarcFly_LearningRust-0.1.0 (c (n "MarcFly_LearningRust") (v "0.1.0") (h "1zfc7xi5i52z1vdn1whjfdrd2b728s8sqgdwszbvvf9i351nyd3n")))

(define-public crate-MarcFly_LearningRust-0.1.1 (c (n "MarcFly_LearningRust") (v "0.1.1") (h "0wgg2kz744d30g19xxnyzd6aa0gqv5962h1pr5kn8637jcd0l5aj")))

(define-public crate-MarcFly_LearningRust-0.1.2 (c (n "MarcFly_LearningRust") (v "0.1.2") (h "179pcnva7lapm5z7wm7msdkrbjd93abhii3flzy1q5gwrq710l73")))

(define-public crate-MarcFly_LearningRust-0.1.3 (c (n "MarcFly_LearningRust") (v "0.1.3") (h "0x2v74jdjjfnzy2frf9z6mvkjp7cq55pgr43bvpx9gr3kr1p64az")))

(define-public crate-MarcFly_LearningRust-0.1.4 (c (n "MarcFly_LearningRust") (v "0.1.4") (h "1csy6w8hix16ncnfj2kr70cir1jy42bgrraqbz0qkkkzk07278yj")))

(define-public crate-MarcFly_LearningRust-0.1.5 (c (n "MarcFly_LearningRust") (v "0.1.5") (h "12gizq56zj2ch1ln1lacn7kqgkkhg2k1vzj65gg7dwaq4hn1c83h")))

(define-public crate-MarcFly_LearningRust-0.1.6 (c (n "MarcFly_LearningRust") (v "0.1.6") (h "1gj592jg95i75nrsps4fl713c44sj1d29ksjmq56i2ka5whw3avb")))

(define-public crate-MarcFly_LearningRust-0.1.7 (c (n "MarcFly_LearningRust") (v "0.1.7") (h "1gi3nbaa91k8smz35fc1hxgpl0p3fgiya6mi4y115hpgkbsbb1hi")))

