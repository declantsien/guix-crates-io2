(define-module (crates-io ma rc marco-crate-example) #:use-module (crates-io))

(define-public crate-marco-crate-example-0.1.0 (c (n "marco-crate-example") (v "0.1.0") (h "0mnw341g132jlm4whbiqgab2vvqnj8jgl4ypj7xnl2jc9jmi49dd")))

(define-public crate-marco-crate-example-0.1.1 (c (n "marco-crate-example") (v "0.1.1") (h "18gh6yricwd4rn5km1khcmqv4zfk4zy5v24mw7b57gsgal9bsvmg")))

(define-public crate-marco-crate-example-0.1.2 (c (n "marco-crate-example") (v "0.1.2") (h "08672pv1ql6swq8mw6m6wpbmf04clnk3layi0hqsvpycrckkr2ac")))

(define-public crate-marco-crate-example-0.1.3 (c (n "marco-crate-example") (v "0.1.3") (h "0rp3zwlw5vk1drygwarqna73isgbkbjv3qbnpckv1r5254r34rf1")))

(define-public crate-marco-crate-example-0.1.4 (c (n "marco-crate-example") (v "0.1.4") (h "0lrjl28fknsh1rasg5gab5kfsrv63kdrwqxpivfdmx5x8gcl6jnz")))

(define-public crate-marco-crate-example-0.1.5 (c (n "marco-crate-example") (v "0.1.5") (h "0hb7697jhssn3h01rnyxmv0ldp5fphxdd89kf59lah0cn26isn85")))

(define-public crate-marco-crate-example-0.1.6 (c (n "marco-crate-example") (v "0.1.6") (h "1sn98f71a2f0b49ymr24dlq600kwz86wj7b0dphsq82ax9y3p1c8")))

(define-public crate-marco-crate-example-0.1.7 (c (n "marco-crate-example") (v "0.1.7") (h "06hnd13dn2jz7jnvpa27s1xd48lrmracikjb8375khqriyqgnq4z")))

(define-public crate-marco-crate-example-0.1.8 (c (n "marco-crate-example") (v "0.1.8") (h "0zyikiwdj2cavv3y47wg7wdapkmkq2mv1zwiiyd6prbxpdh6703s")))

(define-public crate-marco-crate-example-0.1.9 (c (n "marco-crate-example") (v "0.1.9") (h "0ziqf1q8fwgpla1sb7fxgr8cz3ga38zvp584nyr385pd4dfjcdmy")))

(define-public crate-marco-crate-example-0.1.10 (c (n "marco-crate-example") (v "0.1.10") (h "012g0792ilfbfr7qqqq7z501dz79szh6wqrywa4hiwc3qnvwx2x5")))

(define-public crate-marco-crate-example-0.1.11 (c (n "marco-crate-example") (v "0.1.11") (h "1qnrn73z8ix4vxap96p5s7sslfr34qac5blb785ihpflgr6b5wia")))

(define-public crate-marco-crate-example-0.1.12 (c (n "marco-crate-example") (v "0.1.12") (h "0alp69a7x2cfcqcy99m08vnxzgl5qw0xfga9xam0l34ln8kv735q")))

(define-public crate-marco-crate-example-0.1.13 (c (n "marco-crate-example") (v "0.1.13") (h "143x3c8cljxv6bvpf82s6ya7pjh7a7qfpnj0qmv5l5mb883xqinb")))

(define-public crate-marco-crate-example-0.1.14 (c (n "marco-crate-example") (v "0.1.14") (h "1d62dxmf013mjb17l119pifp1d3scp65b8npff0y53jqhiickbv3")))

