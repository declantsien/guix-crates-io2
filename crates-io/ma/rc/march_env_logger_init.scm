(define-module (crates-io ma rc march_env_logger_init) #:use-module (crates-io))

(define-public crate-march_env_logger_init-0.1.1 (c (n "march_env_logger_init") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1nhj92j1sq5ksfbvh6m1h7mghmmz68ddshgrcxhxgxfr8ijkj565")))

