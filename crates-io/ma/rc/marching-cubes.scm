(define-module (crates-io ma rc marching-cubes) #:use-module (crates-io))

(define-public crate-marching-cubes-0.1.0 (c (n "marching-cubes") (v "0.1.0") (h "0mg82fv163yjyjkmg971r6hl80nm7diz5iq8fhnxy8pbmg7j98x7")))

(define-public crate-marching-cubes-0.1.1 (c (n "marching-cubes") (v "0.1.1") (h "1hk2mr40ky7a5vlanv31snf9sdv4z0719py38nhjvm3rbpl65jah") (f (quote (("glam"))))))

(define-public crate-marching-cubes-0.1.2 (c (n "marching-cubes") (v "0.1.2") (h "1w187p15zhz64p7kmkfcxbbgwxqpa75csq64l90w0xhnb77d129p") (f (quote (("glam"))))))

