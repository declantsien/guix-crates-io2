(define-module (crates-io ma rc march) #:use-module (crates-io))

(define-public crate-march-0.1.0 (c (n "march") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0jkj1yx37v3xx4xwij11g794pjvbl4rcvswcwsayc66s98w9y8qj")))

