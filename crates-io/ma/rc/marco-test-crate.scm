(define-module (crates-io ma rc marco-test-crate) #:use-module (crates-io))

(define-public crate-marco-test-crate-0.1.0 (c (n "marco-test-crate") (v "0.1.0") (h "17kwcgp2jcdqkbj32m53ybf9m8wv3f2qpdnll1ml1pybmq8zvpil")))

(define-public crate-marco-test-crate-0.2.0 (c (n "marco-test-crate") (v "0.2.0") (h "11s23lin3zgk7nxflh2v3zzss6w206pq3h9qv5mnry9g8jiv488m")))

