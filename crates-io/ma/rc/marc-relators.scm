(define-module (crates-io ma rc marc-relators) #:use-module (crates-io))

(define-public crate-marc-relators-0.1.0 (c (n "marc-relators") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qnnqsnp1508nj0ylkr6kcmwh9ijjkmpvd1lavd4qizvnf3njg1f") (s 2) (e (quote (("serde" "dep:serde"))))))

