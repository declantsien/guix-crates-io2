(define-module (crates-io ma rc marcus) #:use-module (crates-io))

(define-public crate-marcus-0.1.0 (c (n "marcus") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1nrqcflz62dcz0aizyxlfy94bg6wjmyvnkhma6yhj97pgk63z604")))

(define-public crate-marcus-0.1.1 (c (n "marcus") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "15izw7lrz3m0jsmwrwj5hzy85aazmym5srj3bkm6wh4wvc2pd1yl")))

(define-public crate-marcus-0.1.2 (c (n "marcus") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1dhv9a0pzfg3g20qxl0hnfss8pqlp5x3iywhfjsk15jkivh943ml")))

