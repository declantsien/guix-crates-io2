(define-module (crates-io ma rc marcel) #:use-module (crates-io))

(define-public crate-marcel-0.1.0 (c (n "marcel") (v "0.1.0") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ja9jd280szk2x04ncxr8imj1zp6p567mcpa2669qyizh5r7xkp")))

(define-public crate-marcel-0.1.1 (c (n "marcel") (v "0.1.1") (d (list (d (n "iced") (r "^0.6.0") (f (quote ("wgpu" "debug" "default_system_font" "tokio"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.7") (d #t) (k 0)) (d (n "ron") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g4ncrh6yg1sv4i23apb0yq40zhnxrzz6p6v6xzbiffz7q8pgn7k") (f (quote (("dev" "ron"))))))

