(define-module (crates-io ma rc marco) #:use-module (crates-io))

(define-public crate-marco-0.1.0 (c (n "marco") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 0)) (d (n "redis") (r "^0.22.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "15pq785bacmx1nmvf28c5f7jdcazydl722hxll19rqf5s9rnldw0")))

