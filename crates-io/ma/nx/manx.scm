(define-module (crates-io ma nx manx) #:use-module (crates-io))

(define-public crate-manx-0.3.0 (c (n "manx") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.7.1") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "rl-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "url") (r "^0.5.2") (d #t) (k 0)) (d (n "websocket") (r "^0.14.0") (d #t) (k 0)))) (h "1h6gq31jqgdn0dms56iax00yy2zrhp61xm7xh91i3n8873gxzais")))

(define-public crate-manx-0.4.0 (c (n "manx") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "async-native-tls") (r "^0.3.3") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "piper") (r "^0.1.1") (d #t) (k 0)) (d (n "smol") (r "^0.1.4") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1hd1kpd2p50allv8midb715k3zkk70617z5yqqmg97ac4x79wh55")))

