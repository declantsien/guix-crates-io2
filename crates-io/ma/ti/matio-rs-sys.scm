(define-module (crates-io ma ti matio-rs-sys) #:use-module (crates-io))

(define-public crate-matio-rs-sys-0.1.0 (c (n "matio-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "10jj5lliy8c0qjcdpx1vlfwz8v2l6ciimfbi90dbf7a9z1w9wn1s")))

(define-public crate-matio-rs-sys-0.1.1 (c (n "matio-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1lnqkyvl1594g6wckhnb5fqd2vg1z8lhgxba5j4cbrn2mkg1z2vc")))

(define-public crate-matio-rs-sys-0.2.0 (c (n "matio-rs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "03hix72kpv4lbddc6an3iq623pjbg62rfkh7d5nnh5kr14zk5nsp")))

(define-public crate-matio-rs-sys-0.2.1 (c (n "matio-rs-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "19kjmyy6nf5lhi1fanb4az3mjkrbv3jzwwpqva5a09aw517ik767")))

(define-public crate-matio-rs-sys-0.2.2 (c (n "matio-rs-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1crsl5f97fkd9mjpn2ayz1dsqrmq34w3lg7hqn65xgzwy85g2x1n")))

(define-public crate-matio-rs-sys-0.2.3 (c (n "matio-rs-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1sl7bbfwgpqzd42nilkhf4scrdany33x6j8prniap2z5y33j7x0b")))

