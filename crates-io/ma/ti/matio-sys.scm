(define-module (crates-io ma ti matio-sys) #:use-module (crates-io))

(define-public crate-matio-sys-0.1.0 (c (n "matio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)))) (h "058dfmbmglla66fhwx6x5qz3z4pqc5w0fg7z30c4zkk05vh7s4d0")))

(define-public crate-matio-sys-0.1.1 (c (n "matio-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0c6r2m5ml8cljzm5k4f3xf955lvs9j8gh16lizyjq4c8mmm2n9jz")))

(define-public crate-matio-sys-0.1.2 (c (n "matio-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "10s3hxlm881mzwkpbhgd8sn7knyhc702rvjw7m6czxq56k8748jm")))

