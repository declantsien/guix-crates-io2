(define-module (crates-io ma ti matio-rs_derive) #:use-module (crates-io))

(define-public crate-matio-rs_derive-0.1.0 (c (n "matio-rs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "08dgzdzygiw2ggi8fi5lrf7jx18diqbvzy9n6zyji1gxcwhp2bg8")))

