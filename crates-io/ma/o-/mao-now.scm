(define-module (crates-io ma o- mao-now) #:use-module (crates-io))

(define-public crate-mao-now-0.1.0 (c (n "mao-now") (v "0.1.0") (h "0vrnrxgbyx9h0dxzfsjlk6shqlzmik6j2cpzhspbizmgygb6rzg8")))

(define-public crate-mao-now-0.1.1 (c (n "mao-now") (v "0.1.1") (h "0y60k9y5jlw4bihv3f4hppp4p934y3bp0s4cvrkd4q4zr1giyr2p")))

