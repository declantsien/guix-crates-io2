(define-module (crates-io ma rb marbles-rs) #:use-module (crates-io))

(define-public crate-marbles-rs-0.1.0 (c (n "marbles-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "resvg") (r "^0.31.0") (f (quote ("png" "svgfilters" "filter"))) (k 0)) (d (n "zkp-u256") (r "^0.2.1") (f (quote ("std"))) (k 0)))) (h "05gaca68spgn08xngmw4yrwcs7m7rdknk45qjqg9zpnf13lv33jr") (y #t)))

