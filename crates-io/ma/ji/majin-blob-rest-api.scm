(define-module (crates-io ma ji majin-blob-rest-api) #:use-module (crates-io))

(define-public crate-majin-blob-rest-api-0.1.3 (c (n "majin-blob-rest-api") (v "0.1.3") (d (list (d (n "majin-blob-core") (r "^0.1.3") (d #t) (k 0)) (d (n "majin-blob-types") (r "^0.1.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "065mjzfnbad69p1mcg2q8xwqr2044w2qy6h86jvliscdb60in7v6")))

