(define-module (crates-io ma ji majin-blob-wasm) #:use-module (crates-io))

(define-public crate-majin-blob-wasm-0.1.3 (c (n "majin-blob-wasm") (v "0.1.3") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "majin-blob-core") (r "^0.1.3") (d #t) (k 0)) (d (n "majin-blob-types") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "0b26s86nk02s9hp2df23b95rs6fzz877dcvf3dcw78rll4qryiz5") (f (quote (("default" "console_error_panic_hook"))))))

