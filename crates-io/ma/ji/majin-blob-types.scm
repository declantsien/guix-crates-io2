(define-module (crates-io ma ji majin-blob-types) #:use-module (crates-io))

(define-public crate-majin-blob-types-0.1.1 (c (n "majin-blob-types") (v "0.1.1") (d (list (d (n "majin-blob-eip-4844") (r "^0.1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "0ali14xi6mgfrbx9y0kgmq8wc20vg9pzx683hjzia6w1qhwszljr")))

(define-public crate-majin-blob-types-0.1.3 (c (n "majin-blob-types") (v "0.1.3") (d (list (d (n "majin-blob-eip-4844") (r "^0.1.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "1krsiaggnmv2ykz2s46fj8rsihk0mvnif5if9lrqgddmpca9fnqx")))

