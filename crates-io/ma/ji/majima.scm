(define-module (crates-io ma ji majima) #:use-module (crates-io))

(define-public crate-majima-0.1.0 (c (n "majima") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "124mij3h2zj0dcyc4zcd86bdp15jbzy2y20f7cx90fs9xxxsfy4l")))

(define-public crate-majima-0.1.1 (c (n "majima") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cdg6qkr1igv1zjc7dahxlxnbyy2na7n1k07gcj61w19cl8ds7n6")))

(define-public crate-majima-0.2.0 (c (n "majima") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0bnw3d8ykqih9s5jias41qw57c949rybrhzb1s9fs45a7z25x2z8")))

(define-public crate-majima-0.3.0 (c (n "majima") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "00z4lc96mmlwan0aff5bljxhd046cak403qzgk6zhx1x67042sap")))

(define-public crate-majima-0.4.0 (c (n "majima") (v "0.4.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0615qknk68x8x65a9s1skdm7mgvxw6l2sasxf6i7s7967zkxhkn4")))

(define-public crate-majima-0.5.0 (c (n "majima") (v "0.5.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1nf5ayg20nqxdnfi7yvpqc398ksd3jq8rw0jykbmqaac4mhg9gm7")))

