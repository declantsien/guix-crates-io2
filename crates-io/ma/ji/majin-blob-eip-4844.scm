(define-module (crates-io ma ji majin-blob-eip-4844) #:use-module (crates-io))

(define-public crate-majin-blob-eip-4844-0.1.1 (c (n "majin-blob-eip-4844") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "0p6avqp91944k8k7aifnzavsmd4kzx4s04ryvn1j96a6diir779c")))

(define-public crate-majin-blob-eip-4844-0.1.3 (c (n "majin-blob-eip-4844") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "173k4vsc254f7krgv0ppbzs7lfd0ag388mx78mym47p4sxxd11rb")))

