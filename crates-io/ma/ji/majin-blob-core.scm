(define-module (crates-io ma ji majin-blob-core) #:use-module (crates-io))

(define-public crate-majin-blob-core-0.1.1 (c (n "majin-blob-core") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "majin-blob-eip-4844") (r "^0.1.1") (d #t) (k 0)) (d (n "majin-blob-types") (r "^0.1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "0bhkcky4w39xi720dqjknd6lqvw5khyyhifl23v88vhky7dr4yrl")))

(define-public crate-majin-blob-core-0.1.3 (c (n "majin-blob-core") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "majin-blob-eip-4844") (r "^0.1.3") (d #t) (k 0)) (d (n "majin-blob-types") (r "^0.1.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 0)))) (h "1cd200y5m74sl00s6mjrbglh9ryn0lld4a2q8397gm9vpd4xl0xc")))

