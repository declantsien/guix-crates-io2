(define-module (crates-io ma ir mairs) #:use-module (crates-io))

(define-public crate-mairs-0.0.0 (c (n "mairs") (v "0.0.0") (h "07z9g3zfbawz7bmqp70c80zcx3z5ydjrxdxj5lqy50akmj3x5x8k")))

(define-public crate-mairs-0.1.0 (c (n "mairs") (v "0.1.0") (h "0474j11kqzs13vgbw8qq52x1ggdz4fpnsnbda3rx1lsvjka7akcq")))

(define-public crate-mairs-0.1.1 (c (n "mairs") (v "0.1.1") (h "021bi9zqq5c197w48r302km02j6k590qmmm52crswi5lwjqi8qnv")))

(define-public crate-mairs-0.1.2 (c (n "mairs") (v "0.1.2") (h "1bb85kmcx9yydjvk774zi5sphpdgh9n0g2mfgqrgm9j0qiyiv9gv")))

(define-public crate-mairs-0.1.3 (c (n "mairs") (v "0.1.3") (h "03n4v015bqqmmkckhyiw3xzvs7y88r1aczz36zqi2iak5l9h26x5")))

(define-public crate-mairs-0.1.5 (c (n "mairs") (v "0.1.5") (h "0fx2kyd4y8v224gxaxflgswrx9h6s28j8q3ljgvi5w6yc902dvrq")))

(define-public crate-mairs-0.1.6 (c (n "mairs") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0dy6qch9zg980bdilj0wxlr6zf4fpyi3l5c7rkag41faxp1yyl49")))

