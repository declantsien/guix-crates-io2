(define-module (crates-io ma so mason) #:use-module (crates-io))

(define-public crate-mason-0.0.0 (c (n "mason") (v "0.0.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "runtime") (r "= 0.3.0-alpha.4") (d #t) (k 0)))) (h "06k10hzapd118hvfnmvcka6krvk78cfrbnf4jklqssb8abv9f4bq")))

