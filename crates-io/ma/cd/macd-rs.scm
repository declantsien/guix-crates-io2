(define-module (crates-io ma cd macd-rs) #:use-module (crates-io))

(define-public crate-macd-rs-0.1.0 (c (n "macd-rs") (v "0.1.0") (d (list (d (n "ema-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "ta-common") (r "^0.1.0") (d #t) (k 0)))) (h "1fz6rpnv94862ysjbb040k7q42x514l1l94a5bbr1kvh3c0qg6a1")))

