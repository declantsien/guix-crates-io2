(define-module (crates-io ma pq mapquest) #:use-module (crates-io))

(define-public crate-mapquest-0.1.0 (c (n "mapquest") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17vzq3lsmy6kwj2nbwlz5nkfxvdm8dig9lrw9sdiwhdnvampxks3")))

