(define-module (crates-io ma p2 map2struct-derive) #:use-module (crates-io))

(define-public crate-map2struct-derive-0.1.0 (c (n "map2struct-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1vrvzs72aw1m76h9pw87k8zsy0ffq0lphafi3gpxjdisr4shhch7")))

