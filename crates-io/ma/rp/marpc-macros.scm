(define-module (crates-io ma rp marpc-macros) #:use-module (crates-io))

(define-public crate-marpc-macros-0.1.0 (c (n "marpc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqg91kn2whhvx587q9897wmm4mssq0mwhh346wc870yqas6zmba") (f (quote (("server") ("client"))))))

(define-public crate-marpc-macros-0.2.0 (c (n "marpc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9db7vz47i90jc778dn7bvy9cvqbxyc4ambqiy4jnkcg1z842hj") (f (quote (("server") ("client"))))))

