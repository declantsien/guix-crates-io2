(define-module (crates-io ma rp marpa) #:use-module (crates-io))

(define-public crate-marpa-0.1.0 (c (n "marpa") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libmarpa-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1w7bmcvnfl1ghf55yghh2zb6iij07y061flh3yfpd6xzz61xhbkx")))

(define-public crate-marpa-0.1.2 (c (n "marpa") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libmarpa-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0l4vr175p0bqmwrfwwpagi3bfi7dpdz4pc0b0rmhrw1rsnflb9xh")))

(define-public crate-marpa-0.1.3 (c (n "marpa") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libmarpa-sys") (r "^0.1.2") (d #t) (k 0)))) (h "14q7zc5q8by29cd8z9xwzmxh149dfb7n1nhc1zj6m4zf9j61h8f0")))

(define-public crate-marpa-0.1.4 (c (n "marpa") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libmarpa-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1mjjv7sqgi3hw3lb8n1i9gj98fggpvyb2dwwp26m9i2yawvp1szm")))

(define-public crate-marpa-0.2.0 (c (n "marpa") (v "0.2.0") (d (list (d (n "libmarpa-sys") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 0)))) (h "1biijndw5dn1gz1hh0yganpij28322kl2ii0hqrk3ybvjpj40nkh")))

(define-public crate-marpa-0.2.1 (c (n "marpa") (v "0.2.1") (d (list (d (n "libmarpa-sys") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)))) (h "1z98ps26h9y5phwzv4iaq72f91l6k8r99m8hys28252b90vch6id")))

(define-public crate-marpa-0.2.2 (c (n "marpa") (v "0.2.2") (d (list (d (n "libmarpa-sys") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 0)))) (h "0syb5s4v02ialzlxzf5cfzgf9ly2jwnfbx4vxafzc0q3azgb7ywp")))

(define-public crate-marpa-0.2.3 (c (n "marpa") (v "0.2.3") (d (list (d (n "libmarpa-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.4") (d #t) (k 0)))) (h "11i8prghgw1cllq5yhyki5qfdz99wyfqd4aja0ypmh2vysf9lmmd")))

(define-public crate-marpa-0.3.0 (c (n "marpa") (v "0.3.0") (d (list (d (n "libmarpa-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.4") (d #t) (k 0)))) (h "0bf2zpmvm1943kv5cmaq098lga1r8z90rsamrlr76lrrrc6pnn2q")))

