(define-module (crates-io ma nn mann_kendall) #:use-module (crates-io))

(define-public crate-mann_kendall-0.1.0 (c (n "mann_kendall") (v "0.1.0") (d (list (d (n "cbindgen") (r "~0.23") (d #t) (k 1)) (d (n "distrs") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "01abq62w6p54qvc8q6x6rpz01j7ss50c4arvaqwv7vh3gmc6sifd")))

