(define-module (crates-io ma lh malhada) #:use-module (crates-io))

(define-public crate-malhada-0.1.0 (c (n "malhada") (v "0.1.0") (h "15bnr4132bjv2y4nz3j63d4hg2f9nnczcir2azkq2w1b2yx9afqv") (y #t)))

(define-public crate-malhada-0.1.2 (c (n "malhada") (v "0.1.2") (d (list (d (n "hangul") (r "^0.1.1") (d #t) (k 0)))) (h "0d2qmqjbph2v0p6l2di7c8nbg4kz51rfd1nr62bsp9j745p5bisx") (y #t)))

