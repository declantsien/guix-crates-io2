(define-module (crates-io ma ps mapstruct-derive) #:use-module (crates-io))

(define-public crate-mapstruct-derive-0.1.0 (c (n "mapstruct-derive") (v "0.1.0") (d (list (d (n "mapstruct-derive-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0nri9cvm5rmrzn1sc2q0vrizzkdzayrbanw2myd6a02k68a85zh1")))

(define-public crate-mapstruct-derive-0.2.0 (c (n "mapstruct-derive") (v "0.2.0") (d (list (d (n "mapstruct-derive-lib") (r "^0.2.0") (d #t) (k 0)))) (h "0hv8lzk5qgvchglv1025h8njs67a3qgilbx0xplim0fxmvna4r18")))

(define-public crate-mapstruct-derive-0.3.0 (c (n "mapstruct-derive") (v "0.3.0") (d (list (d (n "mapstruct-derive-lib") (r "^0.3.0") (d #t) (k 0)))) (h "1d0z3yxp4pmmvq6vnhmxl855503d7sg5fql2jyskh31k751wd4zb")))

(define-public crate-mapstruct-derive-0.4.0 (c (n "mapstruct-derive") (v "0.4.0") (d (list (d (n "mapstruct-derive-lib") (r "^0.4.0") (d #t) (k 0)))) (h "1lbdj1i0jf3zqmxyk14a9mjq983ncswyyr5pfccwafmk17vqndik")))

