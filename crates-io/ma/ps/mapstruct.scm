(define-module (crates-io ma ps mapstruct) #:use-module (crates-io))

(define-public crate-mapstruct-0.1.0 (c (n "mapstruct") (v "0.1.0") (d (list (d (n "mapstruct-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lbxz1nwci0lyci7kqc765cg9j4mq6cynl93mqna9pc91f8y4w53")))

(define-public crate-mapstruct-0.2.0 (c (n "mapstruct") (v "0.2.0") (d (list (d (n "mapstruct-derive") (r "^0.2.0") (d #t) (k 0)))) (h "10ymbcpfcpd8a7blbakjjpx4i9y3lhq5dihyfbxnfs3y8kfyrcqr")))

(define-public crate-mapstruct-0.3.0 (c (n "mapstruct") (v "0.3.0") (d (list (d (n "mapstruct-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1j5d38dr8lvv8q3cqz210kq9ci0fqhmlqcbc8j9g9sbfdfn6jdnf")))

(define-public crate-mapstruct-0.4.0 (c (n "mapstruct") (v "0.4.0") (d (list (d (n "mapstruct-derive") (r "^0.4.0") (d #t) (k 0)))) (h "09lm2879zlik7clrnj9xadddi2cxlgk47swcvq2rba6wz8gi14cq")))

