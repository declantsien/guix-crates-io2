(define-module (crates-io ma hr mahrs) #:use-module (crates-io))

(define-public crate-mahrs-0.0.0 (c (n "mahrs") (v "0.0.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.9.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0q200vrfhws6pljnnwirvlpqld7q7i20z8dv7jrrm9xpwrzinsva")))

