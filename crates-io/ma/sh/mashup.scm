(define-module (crates-io ma sh mashup) #:use-module (crates-io))

(define-public crate-mashup-0.1.0 (c (n "mashup") (v "0.1.0") (d (list (d (n "mashup-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0z20r6h7fp22mw1wz72cay1rgnim6477bd7jgkpfs3vmj77zi76s")))

(define-public crate-mashup-0.1.1 (c (n "mashup") (v "0.1.1") (d (list (d (n "mashup-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0y5garj37s1nhphpv81npwmx1k1j5n6jajmxrjm7svxsi68pgp4g")))

(define-public crate-mashup-0.1.2 (c (n "mashup") (v "0.1.2") (d (list (d (n "mashup-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "19djnflkq4bhvhn9vqqgirv4mrm6p76cxm28bcjn5zqs0anpvlcl")))

(define-public crate-mashup-0.1.3 (c (n "mashup") (v "0.1.3") (d (list (d (n "mashup-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1n266qpz6y5zpgz2s057r40s9dnlwqvwankd9yh4z53dbicx9d80")))

(define-public crate-mashup-0.1.4 (c (n "mashup") (v "0.1.4") (d (list (d (n "mashup-impl") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1zbyiycpgaab6m7abxh286dj4qc34yyxfhxijk05093asm8wndk8")))

(define-public crate-mashup-0.1.5 (c (n "mashup") (v "0.1.5") (d (list (d (n "mashup-impl") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1kxymps9sr7x3zsp38q1ql7z0dl70wdsbkwllbbwwpdbh628s5pa")))

(define-public crate-mashup-0.1.6 (c (n "mashup") (v "0.1.6") (d (list (d (n "mashup-impl") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "169qr4hrm8sh8pq3xz7wi0l2zxqp2rwbsy21yj7lq0nzkhfw9ahr")))

(define-public crate-mashup-0.1.7 (c (n "mashup") (v "0.1.7") (d (list (d (n "mashup-impl") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1rvdhgbd5da6ygwd78vfr4ng1gv66g09xpwri8jncp4gaiqy71nq")))

(define-public crate-mashup-0.1.8 (c (n "mashup") (v "0.1.8") (d (list (d (n "mashup-impl") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0n7yfnnrhqs9hyl30xdy4v7az67jynw7b6kqryj6pgz8q93ccgkz")))

(define-public crate-mashup-0.1.9 (c (n "mashup") (v "0.1.9") (d (list (d (n "mashup-impl") (r "^0.1.9") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0fwwqxpiza8n02imwwx4bi81sacyb1hc0rclf50vn4gvqws2pn7j")))

(define-public crate-mashup-0.1.10 (c (n "mashup") (v "0.1.10") (d (list (d (n "mashup-impl") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0hpw0q8mvscdzag8m14r8awg4jw0rgh7r9m1s2nr1xdszqp6qh57")))

(define-public crate-mashup-0.1.11 (c (n "mashup") (v "0.1.11") (d (list (d (n "mashup-impl") (r "=0.1.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "067xv16glmjsn0d8m3hvbcky8986lpx5pw8yl9i7wkdmx4z4lflc")))

(define-public crate-mashup-0.1.12 (c (n "mashup") (v "0.1.12") (d (list (d (n "mashup-impl") (r "=0.1.12") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "12a2ghwd27rpn01ck41ip86knrvc6bkckzj34hpzjycqkkns50by")))

(define-public crate-mashup-0.1.13+deprecated (c (n "mashup") (v "0.1.13+deprecated") (d (list (d (n "mashup-impl") (r "=0.1.13") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1f2sp563fy0jzscasgk9h2rjhmdjlxz86xhmllbh7pwfv5mv0hk1")))

(define-public crate-mashup-0.1.14+deprecated (c (n "mashup") (v "0.1.14+deprecated") (d (list (d (n "mashup-impl") (r "=0.1.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1h7xwprqhv8vxdzs4zsxis47md48cm28m06m4mw57c4x0vvb5i8l")))

