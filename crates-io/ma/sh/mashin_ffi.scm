(define-module (crates-io ma sh mashin_ffi) #:use-module (crates-io))

(define-public crate-mashin_ffi-0.1.0 (c (n "mashin_ffi") (v "0.1.0") (d (list (d (n "deno_core") (r "=0.177.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dynasmrt") (r "^1.2.3") (d #t) (k 0)) (d (n "libffi") (r "^3.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "=0.3.9") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1iz7lh08kfcs8ai0k7x5fg0hi6sf5n00s5s9an7xai60clb0a3rr") (y #t)))

