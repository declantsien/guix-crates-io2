(define-module (crates-io ma sh mashin_primitives) #:use-module (crates-io))

(define-public crate-mashin_primitives-0.1.0 (c (n "mashin_primitives") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.149") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0z63ycmpskdllhn7v6pg6ljaaz41lg80qwqzxykfs97n9l3chwx8")))

(define-public crate-mashin_primitives-0.1.1 (c (n "mashin_primitives") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0qmnyjk20kg0rqh2cbva8flc8vvnrh8f5jmpsn81a0synigks6qk")))

