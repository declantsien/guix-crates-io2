(define-module (crates-io ma sh mash-http) #:use-module (crates-io))

(define-public crate-mash-http-0.9.1 (c (n "mash-http") (v "0.9.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("timestamps" "macros"))) (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1kaac66b0yicgdgangbqqqx5px9d630pqlgi7whj9sywimsjndfk")))

