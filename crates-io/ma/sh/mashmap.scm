(define-module (crates-io ma sh mashmap) #:use-module (crates-io))

(define-public crate-mashmap-0.1.0 (c (n "mashmap") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("raw"))) (d #t) (k 0)))) (h "19al0mv15l2zwpcajpaynrxgrx8bw28y1zrscq63gdn0r90j7mmz")))

(define-public crate-mashmap-0.1.1 (c (n "mashmap") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("raw"))) (d #t) (k 0)))) (h "1w88w3kl82828g9djw7dk26pgx9n8kx8553kqhk7kawm9ihpamax")))

