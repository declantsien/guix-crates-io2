(define-module (crates-io ma sh mashup-impl) #:use-module (crates-io))

(define-public crate-mashup-impl-0.1.0 (c (n "mashup-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)))) (h "1w6fz6y01m0xpgimahzmrchsaiknm06jd3aqmgjvbiqspfydi16a")))

(define-public crate-mashup-impl-0.1.1 (c (n "mashup-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)))) (h "113gzfpc7dmq6jjqyrf791ybf7xggysx2g0m07ah4gf63cbzwpnd")))

(define-public crate-mashup-impl-0.1.2 (c (n "mashup-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)))) (h "1ks15w2apyysll6n9mcxhzwb2k5hrm3zxjgcgb2r69nrsllzf8bp")))

(define-public crate-mashup-impl-0.1.3 (c (n "mashup-impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)))) (h "0hwwciamg4l446papj6nn63d8k751p96kyqxnksb0nrmkck7l5rl")))

(define-public crate-mashup-impl-0.1.4 (c (n "mashup-impl") (v "0.1.4") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "1smx44fjwcfm09ywcm1cn6sqmg4svpg77mg7aa9ilfc84wq1nqi7")))

(define-public crate-mashup-impl-0.1.5 (c (n "mashup-impl") (v "0.1.5") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "14ssskiyqbm20wrrpxwizx5nn80m3dabnlfnavpdqdlx9lpkaa7d")))

(define-public crate-mashup-impl-0.1.6 (c (n "mashup-impl") (v "0.1.6") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "0f6afmh0jplkxprhjnp0mm2n1icansfgbm7msbhk8ikp8q0qcz7r")))

(define-public crate-mashup-impl-0.1.7 (c (n "mashup-impl") (v "0.1.7") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "103m90isfp62slg8pvkv4hvjnb24bxhyxpw45ma55aszwd0nfhld")))

(define-public crate-mashup-impl-0.1.8 (c (n "mashup-impl") (v "0.1.8") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "0yj1rag6izd0gsn0bxvpbllwd0lmybgldyf8r41jp91wfmzpgkl0")))

(define-public crate-mashup-impl-0.1.9 (c (n "mashup-impl") (v "0.1.9") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "1yv5b6m7xgzw9vw55z4lz3ims1kb4rj7slhj0lqznkjbczxpnq5a")))

(define-public crate-mashup-impl-0.1.10 (c (n "mashup-impl") (v "0.1.10") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "1qrk411kg4hw0kf35xq4wrzs0x73bxa9fp6vx5dwgy2aix0vfchi")))

(define-public crate-mashup-impl-0.1.11 (c (n "mashup-impl") (v "0.1.11") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "0y0340zcsaq8vyfrxa4hpb8y0a2q2h68vl9mqq1l14iimgqdsy7k")))

(define-public crate-mashup-impl-0.1.12 (c (n "mashup-impl") (v "0.1.12") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "0irhm6lmjyw989xbncw8wqayx248bnhrhsv7p1vdnnzdnyhrcnl7")))

(define-public crate-mashup-impl-0.1.13+deprecated (c (n "mashup-impl") (v "0.1.13+deprecated") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "06c7gawqgavxqrvg69mrh82lp1h5w3mm3r68k2c0x34cr42zw8kf")))

(define-public crate-mashup-impl-0.1.14+deprecated (c (n "mashup-impl") (v "0.1.14+deprecated") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "0c5pzi15wlhx3lrhrrb3fild414nxvcg7gkprxaq7snxjnqx971k")))

