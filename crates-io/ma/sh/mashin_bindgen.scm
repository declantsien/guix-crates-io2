(define-module (crates-io ma sh mashin_bindgen) #:use-module (crates-io))

(define-public crate-mashin_bindgen-0.1.0 (c (n "mashin_bindgen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mashin_primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7wf7vccs0vfqni2n1lgx0dbj135bay2iq5xx96s9yp810rnc0z") (y #t)))

(define-public crate-mashin_bindgen-0.1.1 (c (n "mashin_bindgen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mashin_primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15jxb4yf6dl1kna2gah0fmypldsi6kkjk5zgsjw5amnlzdnvxh50") (y #t)))

(define-public crate-mashin_bindgen-0.1.2 (c (n "mashin_bindgen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mashin_primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("blocking" "json" "rustls-tls" "rustls-tls-native-roots" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qihk60qj5xqnhyr98w43ypg4dhf1w6qlncjnbg2fd9cmbiadxqy") (y #t)))

