(define-module (crates-io ma sh mash) #:use-module (crates-io))

(define-public crate-mash-0.1.0 (c (n "mash") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "tobj") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0fq4yql1lmszxpxdm9wiy81mlbcjn0jqqds3367ahbw3ji1l2n7a") (f (quote (("wavefront" "tobj") ("default" "wavefront"))))))

(define-public crate-mash-0.1.1-pre (c (n "mash") (v "0.1.1-pre") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "tobj") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "04gdq25gfi2c6zm4wxs90hy25ab328k2vrqb24b42mwqifbxkzzf") (f (quote (("wavefront" "tobj") ("default" "wavefront"))))))

(define-public crate-mash-0.1.2 (c (n "mash") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "tobj") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0kbhs852l6fifpc3cc0j868f9897szpp0fv88yvv10xgvjqaay43") (f (quote (("wavefront" "tobj") ("default" "wavefront"))))))

(define-public crate-mash-1.0.0 (c (n "mash") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "tobj") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0j5020r931ygn0h9llwp67wfvjp7cvjsnl8vzwjng103nbjan57b") (f (quote (("wavefront" "tobj") ("default" "wavefront"))))))

(define-public crate-mash-1.0.1 (c (n "mash") (v "1.0.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "tobj") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0cxlg74lmh85ngf6mi1ggs62k97i7r86g98d9qn72fmaj9qzcjkh") (f (quote (("wavefront" "tobj") ("default" "wavefront"))))))

