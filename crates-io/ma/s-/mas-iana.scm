(define-module (crates-io ma s- mas-iana) #:use-module (crates-io))

(define-public crate-mas-iana-0.7.0 (c (n "mas-iana") (v "0.7.0") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18yz6lv54crny8nmx4dpy3j2j00v8s80zsczzcg4fh1jyw6q4j0w") (f (quote (("default" "serde" "schemars")))) (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars"))))))

(define-public crate-mas-iana-0.8.0 (c (n "mas-iana") (v "0.8.0") (d (list (d (n "schemars") (r "^0.8.16") (o #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k6nbnfg7p68kh9k8rp2iyixcsd0wlqs07cn8wcwb31j5rpn91db") (f (quote (("default" "serde" "schemars")))) (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars"))))))

(define-public crate-mas-iana-0.9.0 (c (n "mas-iana") (v "0.9.0") (d (list (d (n "schemars") (r "^0.8.16") (f (quote ("url" "chrono" "preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0aw72w3ilagnpq1z0xj0d50vrp223sm185rdydi6xqwwwimp0cbz") (f (quote (("default" "serde" "schemars")))) (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars"))))))

