(define-module (crates-io ma rl marlowe_rust_sdk) #:use-module (crates-io))

(define-public crate-marlowe_rust_sdk-0.0.5 (c (n "marlowe_rust_sdk") (v "0.0.5") (d (list (d (n "marlowe_client") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0zk0pi280p40zxb1c2b9dg39hhraa6yacrmsmp0pqf5fy15yd9gs")))

