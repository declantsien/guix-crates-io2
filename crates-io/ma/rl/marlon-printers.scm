(define-module (crates-io ma rl marlon-printers) #:use-module (crates-io))

(define-public crate-marlon-printers-2.0.0 (c (n "marlon-printers") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "170mx1yyxsz9jrqfw0vh0mq4azr7sr9pk6nvpy9509p8lpnxqf4k")))

(define-public crate-marlon-printers-2.0.1 (c (n "marlon-printers") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14gs3vzxf2s0gvfzfmiim0yy7xmnxgy0psv5g3vrd0903vqclj8s")))

