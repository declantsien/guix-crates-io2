(define-module (crates-io ma rl marlea_engine) #:use-module (crates-io))

(define-public crate-marlea_engine-0.1.0 (c (n "marlea_engine") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1f47yaa9l26c9ppxiqc7akgfmpwgfap7f5rjw71l7m3xmfr0s3g0")))

(define-public crate-marlea_engine-0.1.1 (c (n "marlea_engine") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1qbggsaf2yi5xrqmdakd845d5nmysgla42b89xcrv0jjs7gvpn5c")))

