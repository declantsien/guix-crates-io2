(define-module (crates-io ma rl marlowe_rust_sdk_test) #:use-module (crates-io))

(define-public crate-marlowe_rust_sdk_test-0.0.5 (c (n "marlowe_rust_sdk_test") (v "0.0.5") (d (list (d (n "marlowe_client") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "04lda7d4w9hra2q9wpcyll2mlylrhj3py5bzj5dmg9ia9swhagai") (y #t)))

