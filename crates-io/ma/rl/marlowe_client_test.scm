(define-module (crates-io ma rl marlowe_client_test) #:use-module (crates-io))

(define-public crate-marlowe_client_test-0.0.5 (c (n "marlowe_client_test") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0zfxjmvlq46hapfyvm10kkv1q7zs3qdgklpk9krjbaqbzprr3bs6") (y #t)))

