(define-module (crates-io ma rl marla) #:use-module (crates-io))

(define-public crate-marla-0.1.0-alpha.0 (c (n "marla") (v "0.1.0-alpha.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x4r6kfvvq1b59ljmxrpcamr17vlwy42w2hlbb0ksb1scmil5smy")))

(define-public crate-marla-0.1.0-alpha.1 (c (n "marla") (v "0.1.0-alpha.1") (d (list (d (n "async-std") (r "^1.9") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.0") (d #t) (k 2)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fbwrnwylzlpvk1as2nd9hpx6sdz6b3rsi4k30mk24bgg0x4rcaj")))

