(define-module (crates-io ma tm matmath) #:use-module (crates-io))

(define-public crate-matmath-1.0.0 (c (n "matmath") (v "1.0.0") (h "1wknr44xyircsw1cak0crlm5y440m8nz4bxfsipxccc7vmiablp0")))

(define-public crate-matmath-1.0.1 (c (n "matmath") (v "1.0.1") (h "134axnhgia82bs4s1hb05rr5b5lzsl5sk8ss2gkqfq86xdhrzjs0")))

(define-public crate-matmath-1.0.2 (c (n "matmath") (v "1.0.2") (h "0i0kf7315gnxm3cv6hg2jqnf91yhckjm8jsyay971kn44fv2xgd7")))

(define-public crate-matmath-1.0.3 (c (n "matmath") (v "1.0.3") (h "17dpjimj99z7hn5bbhga6fn5mdmm20shl65rv55ga44yp15a3brk")))

(define-public crate-matmath-1.0.4 (c (n "matmath") (v "1.0.4") (h "0cnbp9c6ldrivg272k2xwdz78fyyw8ncdq3an86bsx6j1j02n64i")))

(define-public crate-matmath-1.1.0 (c (n "matmath") (v "1.1.0") (h "0x7vkn05pmbjjpr3khb0is0wwwkgxb9xf06iqrnzng3ag3l059g5")))

(define-public crate-matmath-1.1.1 (c (n "matmath") (v "1.1.1") (h "193yq6hna3548094hzd688724kbvs7sqm67bqd0vq64qzd3sjisb")))

(define-public crate-matmath-1.2.0 (c (n "matmath") (v "1.2.0") (h "0ssdh2005qwc493k3vrsmzfj4h2wd7qdv7nylh0mvkv8zsl5s8ar")))

(define-public crate-matmath-1.2.1 (c (n "matmath") (v "1.2.1") (h "015326fzh0cah9bi76dv7psca2y8mys52fw333k7hk6i26r6cl41")))

(define-public crate-matmath-1.2.2 (c (n "matmath") (v "1.2.2") (h "0d8pglx0173dvbjhkbz734wmvvyp85n3wkpghcnkaprrrkfc2p6i")))

(define-public crate-matmath-1.3.0 (c (n "matmath") (v "1.3.0") (h "1npq8wwwqfmszyjs5rspi4dsqgiip7cpb1z6al925kqgs1l05bgj")))

(define-public crate-matmath-1.3.1 (c (n "matmath") (v "1.3.1") (h "1r5j8qa7mjv97g471v1hl7zlb9jjdrkadnwpxgnbv0b82kq71a0s")))

(define-public crate-matmath-1.4.0 (c (n "matmath") (v "1.4.0") (h "18nvq8v4ph7xgjjkri5j8z7rwfmcqp22vfpvz5sg9ks7q5pafm6r")))

(define-public crate-matmath-1.4.1 (c (n "matmath") (v "1.4.1") (h "0rm934zqra2dhy6hcivgmb03sjani3icfnhk7k29faayksdsw3ib")))

