(define-module (crates-io ma xm maxminddb-uf) #:use-module (crates-io))

(define-public crate-maxminddb-uf-0.1.0 (c (n "maxminddb-uf") (v "0.1.0") (d (list (d (n "maxminddb") (r "^0.23.0") (d #t) (k 0)))) (h "0003p1mvq9lndc69jncd9rn73b71sa8hprz53lqkk3i187jdp01k")))

(define-public crate-maxminddb-uf-0.1.1 (c (n "maxminddb-uf") (v "0.1.1") (d (list (d (n "maxminddb") (r "^0.23.0") (d #t) (k 0)))) (h "1cm9nhkbb8wxq1l0vsggs7a1s2bfnvqz7wmfz1070y5y4yd4l9pg")))

(define-public crate-maxminddb-uf-0.1.2 (c (n "maxminddb-uf") (v "0.1.2") (d (list (d (n "maxminddb") (r "^0.23.0") (d #t) (k 0)))) (h "0spbv3qyd9x1chvim8rg2yzbbnrhc2j49bnwggq2k2dmzbhw9vwp")))

(define-public crate-maxminddb-uf-0.1.3 (c (n "maxminddb-uf") (v "0.1.3") (d (list (d (n "maxminddb") (r "^0.23.0") (d #t) (k 0)))) (h "0f5p0mx0sy061rndwxbx9mayywjs3n2ayddprgzv0a2170x1jazn")))

