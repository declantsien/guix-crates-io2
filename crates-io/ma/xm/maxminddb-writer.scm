(define-module (crates-io ma xm maxminddb-writer) #:use-module (crates-io))

(define-public crate-maxminddb-writer-0.1.0 (c (n "maxminddb-writer") (v "0.1.0") (d (list (d (n "maxminddb") (r "^0.23") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sf7y2i01sr6pn5fdd115q714niz289l7qnzp5rp0mq64663kj2y")))

