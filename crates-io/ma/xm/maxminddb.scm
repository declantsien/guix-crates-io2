(define-module (crates-io ma xm maxminddb) #:use-module (crates-io))

(define-public crate-maxminddb-0.1.0-pre (c (n "maxminddb") (v "0.1.0-pre") (h "0mxm2kkm9844zic0i1xc0wp6crvxfvsnnfhldjcv87sm6dxm0h91")))

(define-public crate-maxminddb-0.1.0 (c (n "maxminddb") (v "0.1.0") (h "120ifzyjsw3iwpxab74vi3aw5c9v77v8myy17p242vi2rji7108k")))

(define-public crate-maxminddb-0.1.1 (c (n "maxminddb") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.1.1") (d #t) (k 0)))) (h "00f2c9nrl74yd8qrx03gw5cpsa6q7y1i4jk850arzzicayi74d3l")))

(define-public crate-maxminddb-0.1.2 (c (n "maxminddb") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.2.3") (d #t) (k 0)))) (h "0hg12v3f891yhzcc3m73108c7rhys09c4mqw192p7hisdblyk8d0")))

(define-public crate-maxminddb-0.1.3 (c (n "maxminddb") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "0ri6k6r0skillhwn3n87kh0ghsknxq356crsvl3pqqkzfglzms0q")))

(define-public crate-maxminddb-0.1.4 (c (n "maxminddb") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.2.9") (d #t) (k 0)))) (h "0nx5hkk0ayjf3c49gywj856xx87j9i8zvh35hzv4xn7dwc42pq5f")))

(define-public crate-maxminddb-0.1.5 (c (n "maxminddb") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "0x3rv7iypkafxak7np7ycaf0r1psim67fid8202zdz2f5ysp76c4")))

(define-public crate-maxminddb-0.1.6 (c (n "maxminddb") (v "0.1.6") (d (list (d (n "log") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "02j1lwpa5zdh4gn69hr2z9fn8hp4y5nq63n78x2q3xwm7x96rzdn")))

(define-public crate-maxminddb-0.1.7 (c (n "maxminddb") (v "0.1.7") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.15") (d #t) (k 0)))) (h "12aaf81w4j2h4xyixnin2kfkdmg27ic7p9lrfyg38yxdqhkppb9h")))

(define-public crate-maxminddb-0.1.8 (c (n "maxminddb") (v "0.1.8") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "0lvia4fld5il9a6g63n1mljcvqgk0d2fpamrj2hsj4cqa1aafvs0")))

(define-public crate-maxminddb-0.1.9 (c (n "maxminddb") (v "0.1.9") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.6") (d #t) (k 0)))) (h "1iiyiz9hbyd4ijhpb1yn6s6mzk4b5dj14q8jpyla3kc2rfr7lryq")))

(define-public crate-maxminddb-0.2.0 (c (n "maxminddb") (v "0.2.0") (d (list (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.6") (d #t) (k 0)))) (h "10sbpibnyvqvymi8hqx79bq78am9yq2p4446r8kdr34nb5nnh5s9")))

(define-public crate-maxminddb-0.2.1 (c (n "maxminddb") (v "0.2.1") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.7") (d #t) (k 0)))) (h "06g4vliyb8kls9qxdin2xc7sfp45dqp7gvh8n5an6llxn260vpq5")))

(define-public crate-maxminddb-0.3.0 (c (n "maxminddb") (v "0.3.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "1przvy0mdvylssz2i3bfxbybrxgw4zp68llrz3rwaah63aad017n")))

(define-public crate-maxminddb-0.4.0 (c (n "maxminddb") (v "0.4.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "127x103ja2f45b93mhq0r78cdcdldcc32gwgls55vb895hiawalw")))

(define-public crate-maxminddb-0.5.0 (c (n "maxminddb") (v "0.5.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "1z2qdqcjy49w181bm6dy3zy5rbp9y1lvj0vymvzvb1awv2swgar8")))

(define-public crate-maxminddb-0.5.1 (c (n "maxminddb") (v "0.5.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "0zapbl33c3w4z99galw55fjy7jxksdndqqq3fp8fzv3cgvb5z29d")))

(define-public crate-maxminddb-0.5.2 (c (n "maxminddb") (v "0.5.2") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0rqzzyzsr4v0hxpg68vgr34sx6ckccchh32sddk0ydhblqfvjyyl")))

(define-public crate-maxminddb-0.5.3 (c (n "maxminddb") (v "0.5.3") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0xclgymhm7ygvylp6gcy3cda8q6pkib276p1wxdk0b5x5ga1zmj8")))

(define-public crate-maxminddb-0.5.4 (c (n "maxminddb") (v "0.5.4") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "074ir687g1v6wgjwhmhlw5dkfrys9adpjihw9yjqqvsywjsgxcwd")))

(define-public crate-maxminddb-0.5.5 (c (n "maxminddb") (v "0.5.5") (d (list (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0hjlpx9ymj9qkwaghy2vifxyhy5cjjkm44k17aj327m1q39yhwa3")))

(define-public crate-maxminddb-0.6.0 (c (n "maxminddb") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1kyak7wcpady77wpdpv9b61lhv4gr8nhnvnxlmlwbb6473zp7v0n") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-maxminddb-0.6.1 (c (n "maxminddb") (v "0.6.1") (d (list (d (n "clippy") (r "^0.0.30") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0iylizfphpzlfck57kz19jcg9crc4ab9vypa40sm1qc44l37ncpl") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-maxminddb-0.6.2 (c (n "maxminddb") (v "0.6.2") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1i37kpgj5fv89kj8ilr0yr3v1a12pmk21dj7wy6a7h7xdwcl477m") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-maxminddb-0.7.0 (c (n "maxminddb") (v "0.7.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0r0vr1kawjh4swi7n2qf69sfrdlbjcqp539d7n21x0dlajz8pl7k")))

(define-public crate-maxminddb-0.7.1 (c (n "maxminddb") (v "0.7.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0i7ki0ynd0lk7sv29yxg42cbmlk9lja78f1q26q1z5kqkxnxplxp")))

(define-public crate-maxminddb-0.7.2 (c (n "maxminddb") (v "0.7.2") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1li4jwgij3amq4vr5ximazxhqncbpk5izzqkfj9i54ynmzm8fyik")))

(define-public crate-maxminddb-0.8.0 (c (n "maxminddb") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "06d8va0kgw319sg4cf8qw7bz13a6asa38pb32vm2c95njzyagsd8")))

(define-public crate-maxminddb-0.8.1 (c (n "maxminddb") (v "0.8.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0y37bibf6i8njwpnmhj33y892kqbw53cjgq529bcwba47shcvhm4")))

(define-public crate-maxminddb-0.9.0 (c (n "maxminddb") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "05z4aaa781bkbmzhqd5wqvzyf7a191br8s1radh5xjk77znvi2kc")))

(define-public crate-maxminddb-0.10.0 (c (n "maxminddb") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "142rdimhkk9s8xwxhcblg2vmzk0a60hr8qzkf7p39nv1knh125bz")))

(define-public crate-maxminddb-0.11.0 (c (n "maxminddb") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bdl5v456h7xygr7yblnmawc6wkqh62w6crxq7d1p8m3zrfr18g3") (f (quote (("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.12.0 (c (n "maxminddb") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kqd7973b4wymlf5sy5bph2vwwyd5cpc6pag21n67882zlfdps6n") (f (quote (("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.13.0 (c (n "maxminddb") (v "0.13.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0vj3lzdp8888zk9wgzyd41p8cjncgmazwvzgjbzx2m8kpxaah4ll") (f (quote (("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.14.0 (c (n "maxminddb") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "101x32x9nbiwvpx5v65rv45qkpy3f02yibynhwpnylqghw9g7m59") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.15.0 (c (n "maxminddb") (v "0.15.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1msl3lfdhwi13hd0916ivvrwl757is2hl8vb1h73lrqsvb0ghb4i") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.16.0 (c (n "maxminddb") (v "0.16.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xv27yn4l31lx8q2yn0s89221pfq28vxv6ggc4anlpzb2mx0jdk8") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.17.0 (c (n "maxminddb") (v "0.17.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d0qxq624cjs3dmn28n45xs8nhnni4xl04b6pz21cxzmcr6wbgsg") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.17.1 (c (n "maxminddb") (v "0.17.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dy47g99x1xx9zj0qr3vgyw6g0drx7441xi92y907da65yq2drs6") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.17.2 (c (n "maxminddb") (v "0.17.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qifq0nrsxbraznz2mag2iiv4870gl55mkxxiig8r2lpngiqxx0v") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.17.3 (c (n "maxminddb") (v "0.17.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lbz29pcf60jl51f683zz75wpn4flpd3q6si3slsrwy4vixaagyi") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.18.0 (c (n "maxminddb") (v "0.18.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j7493q28m6k00nh961ynck36vp4xz1lgsirw117srpqh9g4kq6y") (f (quote (("unsafe-str-decode") ("mmap" "memmap") ("default"))))))

(define-public crate-maxminddb-0.19.0 (c (n "maxminddb") (v "0.19.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d9l44spzdi4b1gvr9r358bxa2q83zjhhnsbmdv489fg0lwdf6pp") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

(define-public crate-maxminddb-0.20.0 (c (n "maxminddb") (v "0.20.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f011gi5iy2hl1g8v1mmarlf7rrz8sd1qkzqrkfgw1zad33qhll3") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

(define-public crate-maxminddb-0.21.0 (c (n "maxminddb") (v "0.21.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nzkzhwc441y20lra83m6aqig2arhg5ia8wz8i0w8sbm5n8g8ajs") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

(define-public crate-maxminddb-0.22.0 (c (n "maxminddb") (v "0.22.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yw2y846144ygff6lxw3ffdha5jrvswwlq1kb5r2xfb6rn3lhjnh") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

(define-public crate-maxminddb-0.23.0 (c (n "maxminddb") (v "0.23.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d533wxsipd37cnc8b4wyzrschwwsf19clbwx3qakxzr2c8scazy") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

(define-public crate-maxminddb-0.24.0 (c (n "maxminddb") (v "0.24.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g3w2addx96r9zn8gbhgxz9rhdxyqzxklmvzgjxn2j51irfpw26n") (f (quote (("unsafe-str-decode") ("mmap" "memmap2") ("default"))))))

