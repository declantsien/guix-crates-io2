(define-module (crates-io ma cs macstats) #:use-module (crates-io))

(define-public crate-macstats-0.1.0 (c (n "macstats") (v "0.1.0") (d (list (d (n "macsmc") (r "^0.1.3") (d #t) (k 0)))) (h "1a4r09bzksnvx9xp6icvdq79jmh4w33wzy4mifnlc59q4jr8dmh3")))

(define-public crate-macstats-0.1.1 (c (n "macstats") (v "0.1.1") (d (list (d (n "macsmc") (r "^0.1.3") (d #t) (k 0)))) (h "14yl60znfpclmlk6n4w427iqj655njic17gdhgxz05kwcrf3gcgq")))

(define-public crate-macstats-0.1.2 (c (n "macstats") (v "0.1.2") (d (list (d (n "macsmc") (r "^0.1") (d #t) (k 0)))) (h "1wawq9hh6xb64r18q2jph0x3axs2hyy9z67mi46lr76rdz29x1ky")))

(define-public crate-macstats-0.1.3 (c (n "macstats") (v "0.1.3") (d (list (d (n "macsmc") (r "^0.1") (d #t) (k 0)))) (h "0id2dqlvyjcc5cb5sqhws93pgzvwg8hly9hr8lrr3wz8yjj8ggxd")))

