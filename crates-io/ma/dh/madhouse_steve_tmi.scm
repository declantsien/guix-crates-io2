(define-module (crates-io ma dh madhouse_steve_tmi) #:use-module (crates-io))

(define-public crate-madhouse_steve_tmi-0.1.0 (c (n "madhouse_steve_tmi") (v "0.1.0") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "1m1j10k9i16snxv5cpq68ps9zvv4pmf4jbm2fl8v6r0s73nscdz8")))

(define-public crate-madhouse_steve_tmi-0.1.1 (c (n "madhouse_steve_tmi") (v "0.1.1") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "0079g68v9fdcmvzh9n2mind0k1adgcafqv7k4qg7h9djnp52b5d2")))

(define-public crate-madhouse_steve_tmi-0.1.2 (c (n "madhouse_steve_tmi") (v "0.1.2") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "0gj5zvcl0lqjcazlcyvbakmb8icqfwmbqmaspv41gf2i2wgznwyp")))

(define-public crate-madhouse_steve_tmi-0.2.0 (c (n "madhouse_steve_tmi") (v "0.2.0") (h "0anmxp7z4cbf80j1fb6h4mxxxc2lda46ykdgav280x0nk8nwrzm7")))

