(define-module (crates-io ma sc mascheroni) #:use-module (crates-io))

(define-public crate-mascheroni-0.1.0 (c (n "mascheroni") (v "0.1.0") (h "1jc4dvn4qx2sxcgmdfxpvslpk5v53s1jpypspa0xmsr797n4cgml")))

(define-public crate-mascheroni-0.1.1 (c (n "mascheroni") (v "0.1.1") (h "06higcj3vbkihj7vk5adlazdlyn6f0yc5fr30rl2h73wcaq1lggh")))

(define-public crate-mascheroni-0.1.2 (c (n "mascheroni") (v "0.1.2") (h "1gk678nrnhzrdcgpkj9bkqw4ldf9mj7dl0p6451k9x7bgq01bb86")))

(define-public crate-mascheroni-0.2.0 (c (n "mascheroni") (v "0.2.0") (h "063g8s40xy7lliw42snvw111v388pwmyyn0rgkziambfzpd9bg5m")))

