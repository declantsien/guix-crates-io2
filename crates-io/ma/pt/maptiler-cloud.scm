(define-module (crates-io ma pt maptiler-cloud) #:use-module (crates-io))

(define-public crate-maptiler-cloud-0.1.0 (c (n "maptiler-cloud") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "189l6srj61qbhqz1dx90md4c2gfd9nn3rkg2l7wzhviygn649pmv")))

(define-public crate-maptiler-cloud-0.2.0 (c (n "maptiler-cloud") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1gd068zgf8d13378vdjswjwajnphkgnzrwd1i12ahhpcib20iq6k")))

(define-public crate-maptiler-cloud-0.3.0 (c (n "maptiler-cloud") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "0810jk70zjvlasr7yqjhqsd0i4zppv7gmyzmma0qxadvfy958wvx")))

