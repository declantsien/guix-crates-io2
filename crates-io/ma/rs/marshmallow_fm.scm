(define-module (crates-io ma rs marshmallow_fm) #:use-module (crates-io))

(define-public crate-marshmallow_fm-0.1.0 (c (n "marshmallow_fm") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "marshmallow_gl_bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "marshmallow_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "raw-gl-context") (r "^0.1.2") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0dvk2zijkzwnwaqpx3afw8icvafwzvrqag0c8g2k607m77aqwq26") (y #t)))

