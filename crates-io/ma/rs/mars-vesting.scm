(define-module (crates-io ma rs mars-vesting) #:use-module (crates-io))

(define-public crate-mars-vesting-1.0.0 (c (n "mars-vesting") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0f7na25bvqw6lqd0607kmrljx3515c2gbqia8zqirfwfl6chyfmz") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65")))

(define-public crate-mars-vesting-1.1.0 (c (n "mars-vesting") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "191ycsjvv88vfyqd4z3bgx05yvnyji93i8n5k8y7ccfzkvif4kr7") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.65")))

