(define-module (crates-io ma rs marsbar) #:use-module (crates-io))

(define-public crate-marsbar-0.2.0 (c (n "marsbar") (v "0.2.0") (d (list (d (n "libmars") (r "^0.2.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0j9r0sscbiwa241q8wip2s60pl370x80d3q2ilbsm5xfhr1scj5j")))

(define-public crate-marsbar-0.3.0 (c (n "marsbar") (v "0.3.0") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "16a85ikimc6i3l3rha356ifxjzk4wfx75gsnymyqr9d7mzyvf8p0")))

(define-public crate-marsbar-0.3.1 (c (n "marsbar") (v "0.3.1") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1vxcn64baf9lrx18iba5chmphan94s58nwh3x71zdz0fhdzis22i")))

(define-public crate-marsbar-0.3.2 (c (n "marsbar") (v "0.3.2") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1v3pb7lh2swyqxkhkw0jc9bwv1d5n4vw8jzyml6fqq7d7gnbm8zb")))

(define-public crate-marsbar-0.4.0 (c (n "marsbar") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0n0xxx1axn1sg2jcjzf4dyrln5ar05l94sjva67yqa2gfy1xxbv6")))

(define-public crate-marsbar-0.4.1 (c (n "marsbar") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.1") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0lblqsbh640yq1i2sl5c8z8pp8mf2mv1zdcbq655l4gxgnn9ybk1")))

(define-public crate-marsbar-0.5.0 (c (n "marsbar") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1wx69c9nqd1ab10ls3lvnik7rvbnlnvbk23xg33sg4dm0bylns94")))

(define-public crate-marsbar-0.5.2 (c (n "marsbar") (v "0.5.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.2") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0z16fphyxcalzqwblfz9a1yh50s9947srm5n9iy9sfd0d2816j6c")))

(define-public crate-marsbar-0.5.3 (c (n "marsbar") (v "0.5.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.3") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0hkrq2np7dqcy36mdlawmrksm3ff5gf8q8jqam5ni7nyfk7sy2b7")))

