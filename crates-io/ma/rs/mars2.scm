(define-module (crates-io ma rs mars2) #:use-module (crates-io))

(define-public crate-mars2-0.1.0 (c (n "mars2") (v "0.1.0") (d (list (d (n "extra") (r "^0") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^0") (d #t) (k 0)) (d (n "serde_macros") (r "^0") (d #t) (k 0)) (d (n "url") (r "^1") (f (quote ("query_encoding"))) (d #t) (k 0)))) (h "0wvwilw7c521c0mz5ksh5lncd956pl9fhdsyxhvn48iy965hn86b")))

