(define-module (crates-io ma rs mars-utils) #:use-module (crates-io))

(define-public crate-mars-utils-1.0.0 (c (n "mars-utils") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ml2wj4k84p9a5yfpnw9zxfawv45w2fb12mzwjnl4252xmrfbfhj") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

