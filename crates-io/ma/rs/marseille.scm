(define-module (crates-io ma rs marseille) #:use-module (crates-io))

(define-public crate-marseille-0.0.0 (c (n "marseille") (v "0.0.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "0x1vq59qss7hd4bndd515gphbgf1bdx90m5y262d2q12hg26g7py")))

