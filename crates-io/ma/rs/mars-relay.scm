(define-module (crates-io ma rs mars-relay) #:use-module (crates-io))

(define-public crate-mars-relay-0.2.0 (c (n "mars-relay") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.2.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0llhrdapqbfvly0gsz3a7my03r62m8l507ypyqs55mjw7l68yx4r")))

(define-public crate-mars-relay-0.3.0 (c (n "mars-relay") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.3.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "121gkbvysga5hk20qd976hih4wa4vqhf52c73jhvcq977jvh2l1q")))

(define-public crate-mars-relay-0.3.1 (c (n "mars-relay") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.3.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "14gqvcp6jm0l3yfdvlzjb1m32h5wp1i2h2mf5s5hdc9s9i7izfmm")))

(define-public crate-mars-relay-0.3.2 (c (n "mars-relay") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.3.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0j550isbbaraqrfm13xngvhyw56g32wg4kpczkxzp90jfbh46p86")))

(define-public crate-mars-relay-0.4.0 (c (n "mars-relay") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0461nyppxj3ipc2ws9xy8l9b7d78n95bx9iq8ly3w20hgrc4ryv5")))

(define-public crate-mars-relay-0.4.1 (c (n "mars-relay") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.1") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1j6ai2jybiqbv0zgsvfaks5rwd5gzsghgzjmx93dmrj2wsc2j32n")))

(define-public crate-mars-relay-0.5.0 (c (n "mars-relay") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.0") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0mhj10ab06501a12dnrrh0dm3jm08nz0159srwqnkx4k77qri56i")))

(define-public crate-mars-relay-0.5.2 (c (n "mars-relay") (v "0.5.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.2") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0pn1672clycqi20sjal9fh92nhjjbfbm3fwznmysxanlnns24iiq")))

(define-public crate-mars-relay-0.5.3 (c (n "mars-relay") (v "0.5.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.3") (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0prs1vn73s2qzbdib19gy1rjrggqza27m59zpdzkxhpaa972px54")))

