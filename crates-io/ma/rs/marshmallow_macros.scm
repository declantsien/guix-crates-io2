(define-module (crates-io ma rs marshmallow_macros) #:use-module (crates-io))

(define-public crate-marshmallow_macros-0.1.0 (c (n "marshmallow_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0ypn8dl3pam35mk04ws207n0hgi8y416pazf9rzksvn3bcqxi061") (y #t)))

