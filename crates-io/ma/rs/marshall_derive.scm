(define-module (crates-io ma rs marshall_derive) #:use-module (crates-io))

(define-public crate-marshall_derive-0.1.0 (c (n "marshall_derive") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kq8c83wlbkz1mkvj9p3r8gzl1lkzllm94k738hks4gryrh5r5qi")))

(define-public crate-marshall_derive-0.1.1 (c (n "marshall_derive") (v "0.1.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0znslqbd82i91141x1gszhq0x7xdw7p1ayrx5zlzir28mzdfk9dg")))

