(define-module (crates-io ma rs mars-t) #:use-module (crates-io))

(define-public crate-mars-t-0.0.0-alpha.1 (c (n "mars-t") (v "0.0.0-alpha.1") (h "10kqz8nr7kdhmbv83114fjnb7b24gcgr5xvq1wc2df93h2rv88bs") (y #t)))

(define-public crate-mars-t-0.0.0-alpha.2 (c (n "mars-t") (v "0.0.0-alpha.2") (h "0mj9myqgj2h267clvxnrg187f3jpd1frhm788ff3iyxrbj3q4gk5") (y #t)))

(define-public crate-mars-t-0.1.0 (c (n "mars-t") (v "0.1.0") (d (list (d (n "hifitime") (r "^3.8") (d #t) (k 0)))) (h "0mya9ksj9d9b75srz29apw4rcclzdzz3dh9nc188gy50m13gg267")))

(define-public crate-mars-t-0.2.0 (c (n "mars-t") (v "0.2.0") (d (list (d (n "hifitime") (r "^3.8") (d #t) (k 0)))) (h "1nxgnhknq3jf4wks83hxc6xqdc4wjbhlqkdw9dszqf4shyj1rxa2")))

(define-public crate-mars-t-0.2.1 (c (n "mars-t") (v "0.2.1") (d (list (d (n "hifitime") (r "^3.8") (d #t) (k 0)))) (h "01jp3716pgp3cih0ki4arkpsd4q09m6gzrhmw7kc3s3wycxhn9pd")))

