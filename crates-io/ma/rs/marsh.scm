(define-module (crates-io ma rs marsh) #:use-module (crates-io))

(define-public crate-marsh-0.0.0 (c (n "marsh") (v "0.0.0") (h "0pdban7gfg9zrd8zadm8m1z5j8kigd5da2qmjpmd5xi7lsxqx3vb") (y #t)))

(define-public crate-marsh-0.0.1 (c (n "marsh") (v "0.0.1") (h "0759mzyn0qh2lqjibcyabfk1cr7ggzvhx6wg6q52kbccqqhl79hx") (y #t)))

