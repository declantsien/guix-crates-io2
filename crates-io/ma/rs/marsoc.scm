(define-module (crates-io ma rs marsoc) #:use-module (crates-io))

(define-public crate-marsoc-0.1.1 (c (n "marsoc") (v "0.1.1") (d (list (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0wp9m6mcsjhcrq2cv0nqi1966ng7d4pfngmcihaix0j90lncy3m8") (y #t)))

(define-public crate-marsoc-0.1.2 (c (n "marsoc") (v "0.1.2") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1793h8g1r7hk3sfyy85k03y855615mn6b50m0cnsa96wnqh6q6l3") (y #t)))

(define-public crate-marsoc-0.1.3 (c (n "marsoc") (v "0.1.3") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1rcmc2ikjfp2gdryf43yi6acqb6r2jrhis93pbmh306sjdzzygbf") (y #t)))

