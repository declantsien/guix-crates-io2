(define-module (crates-io ma rs marsrover) #:use-module (crates-io))

(define-public crate-marsrover-0.1.0-alpha.1 (c (n "marsrover") (v "0.1.0-alpha.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0brzpl52yjfp1583x6px48xsahq6hawr66161v72r359l1ydfg8n")))

(define-public crate-marsrover-0.1.0-alpha.2 (c (n "marsrover") (v "0.1.0-alpha.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "125ycqh30hl4c5ymh84fc52ax0pswzbajbw2kibn0bazv9chsdh2")))

(define-public crate-marsrover-0.1.0-alpha.3 (c (n "marsrover") (v "0.1.0-alpha.3") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("std" "clock"))) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0jn65s7436vmvl9h17qrh0x2ky1ynmrh9i3fd0b791w372fdhmha")))

