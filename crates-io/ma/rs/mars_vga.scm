(define-module (crates-io ma rs mars_vga) #:use-module (crates-io))

(define-public crate-mars_vga-0.0.1 (c (n "mars_vga") (v "0.0.1") (d (list (d (n "spin") (r "^0.5.0") (d #t) (k 0)) (d (n "volatile") (r "^0.2.5") (d #t) (k 0)))) (h "1ygpb6f5zckf1vvs7walkmd40q8jx9qh2nvfgvg3610dby9zpkbn")))

(define-public crate-mars_vga-0.0.2 (c (n "mars_vga") (v "0.0.2") (d (list (d (n "spin") (r "^0.5.0") (d #t) (k 0)) (d (n "volatile") (r "^0.2.5") (d #t) (k 0)))) (h "1262skhr2xxwn3p81n0xf42r3bvim6yj3xni1js2bf1zrrwbcr5a")))

