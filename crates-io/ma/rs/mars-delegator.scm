(define-module (crates-io ma rs mars-delegator) #:use-module (crates-io))

(define-public crate-mars-delegator-1.0.0 (c (n "mars-delegator") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16qcbdsiknssxk85c529kswjqrr919jkzil8lqjmw25q22i1l1mr") (r "1.65")))

(define-public crate-mars-delegator-1.1.0 (c (n "mars-delegator") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d2jip3glf7daxv2sy4gpqhk05q38sfl0ffb4d8d23b91kr9ilid") (r "1.65")))

