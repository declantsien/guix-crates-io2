(define-module (crates-io ma rs mars_raw_utils) #:use-module (crates-io))

(define-public crate-mars_raw_utils-0.1.2 (c (n "mars_raw_utils") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lab") (r "^0.10.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0nzhnprx962xvp4j5qkbqmlchfafqjs7mg24zjqy9vmn41mka8sy")))

(define-public crate-mars_raw_utils-0.1.4 (c (n "mars_raw_utils") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.10.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sscanf") (r "^0.1.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0vh20kwf3wak3zrg7f2k8hd78c3sbyg9gk2kddih8is1n2df5fh5")))

(define-public crate-mars_raw_utils-0.1.6 (c (n "mars_raw_utils") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.10.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sscanf") (r "^0.1.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0142ghcdqjr26n6f1xs92nw6vwim6wh96wh1sgq9cskj005w68fy")))

(define-public crate-mars_raw_utils-0.2.0 (c (n "mars_raw_utils") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sciimg") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sscanf") (r "^0.1.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "04qhvrcfiiway6w4b50zsbvi3d6zjqgjwg197x0zlh92gnv5zbhv")))

(define-public crate-mars_raw_utils-0.2.1 (c (n "mars_raw_utils") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sciimg") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sscanf") (r "^0.1.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "15fzif0nw20d3asm452lg70dgi718zlnq0wj6r6wnf43pm3vxgrd")))

(define-public crate-mars_raw_utils-0.2.4 (c (n "mars_raw_utils") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sciimg") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "weezl") (r "^0.1.5") (d #t) (k 0)))) (h "0d0f5s7h5hwn873cjrjyybxy8d99jd881lwmsqvyyncjp7dkcr2m")))

(define-public crate-mars_raw_utils-0.3.0 (c (n "mars_raw_utils") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sciimg") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "weezl") (r "^0.1.5") (d #t) (k 0)))) (h "0m3fgn0jdk62y2g8kkir5cs2j68ja50ycyzh95824ml30infl5ac")))

(define-public crate-mars_raw_utils-0.3.1 (c (n "mars_raw_utils") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sciimg") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "weezl") (r "^0.1.5") (d #t) (k 0)))) (h "0fjx1qmwvn4axrcnzm93v1li22zykpj8rlrggm85yy6lq3mjigc8")))

