(define-module (crates-io ma rs mars-owner) #:use-module (crates-io))

(define-public crate-mars-owner-1.0.0 (c (n "mars-owner") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "095yd7rycdnxa88l6mz0anbl6d9ghw2lhh868z8683bs0j5llr55")))

(define-public crate-mars-owner-1.1.0 (c (n "mars-owner") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0f2ydkm1yb6x99sdawsw55jhghdp48ni6nag4ykmwjjv8vd1184c") (f (quote (("emergency-owner") ("default"))))))

(define-public crate-mars-owner-1.2.0 (c (n "mars-owner") (v "1.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1z4xdn3a1lg6r3kvdh4fy8x1jnpjr5g4zzz5ij3xlqf5zw43kmdc") (f (quote (("emergency-owner") ("default"))))))

(define-public crate-mars-owner-2.0.0 (c (n "mars-owner") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1342qn3sx9a57dak8gfbhy7fjdx36fz0yws6dc1ri2hsz2rf0imb") (f (quote (("emergency-owner") ("default"))))))

