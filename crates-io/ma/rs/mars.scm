(define-module (crates-io ma rs mars) #:use-module (crates-io))

(define-public crate-mars-0.0.0 (c (n "mars") (v "0.0.0") (h "0c8a3grsf74kxjw05kl47s592ygldjk1mb73rksi67r8xqb6fi7m")))

(define-public crate-mars-0.0.1 (c (n "mars") (v "0.0.1") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.9.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "08jb1ygy0l6ql2r5zmsqcnlpcr1q1i45972lcl4ksq0a4im8560b")))

(define-public crate-mars-0.0.2 (c (n "mars") (v "0.0.2") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.9.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0cpwc0kzlcyq238fcmyd1mzja5ba99366ci8jdbp071ppd5ghnqb")))

