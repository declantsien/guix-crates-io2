(define-module (crates-io ma rs marswm) #:use-module (crates-io))

(define-public crate-marswm-0.2.0 (c (n "marswm") (v "0.2.0") (d (list (d (n "libmars") (r "^0.2.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "11knhyj33c77gfpgbdvyg6cxn4wrjvpixhzhnb5fx4058z9cx5iz")))

(define-public crate-marswm-0.3.0 (c (n "marswm") (v "0.3.0") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "08k7wszxhm6ph9hr32li0rmj2xv9vbqrhfc83y42q9zcvb3pfpgj")))

(define-public crate-marswm-0.3.1 (c (n "marswm") (v "0.3.1") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0z0nif6mmfhz84x4wh20v8pd30k1blxfghjzq0lb6dzqpwxhnj0n")))

(define-public crate-marswm-0.3.2 (c (n "marswm") (v "0.3.2") (d (list (d (n "libmars") (r "^0.3.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1msgi6i3wiqhvqjjfi8s0jgwkb6v6ia9fsghd0qf25bq13dpjdy3")))

(define-public crate-marswm-0.4.0 (c (n "marswm") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0yn5gd91chc2yxj8yw9bd5zd9jjjk74x93fjvdfviwrqdgah5kyg")))

(define-public crate-marswm-0.4.1 (c (n "marswm") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.4.1") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1s6dgq454dbhj2g1f8kxqyccdqr2y21nxv011j2zh0zwqxhrxi5x")))

(define-public crate-marswm-0.5.0 (c (n "marswm") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.0") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1f9g5dzzb7s89dfhr3fj9bcs2d7rjkhngsa1y0jm67s48n1xdm0z")))

(define-public crate-marswm-0.5.2 (c (n "marswm") (v "0.5.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.2") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "00j1kzy964vsllpkgz3s44lf1rvmzrcrqgymyswipx0ag416i8iw")))

(define-public crate-marswm-0.5.3 (c (n "marswm") (v "0.5.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmars") (r "^0.5.3") (f (quote ("configuration"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x11") (r "^2.19.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0s2rzvfbqc83pknpz8bspjp25nfvzh4qwnfbvrcw3kk72by9xgxj")))

