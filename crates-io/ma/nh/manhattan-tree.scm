(define-module (crates-io ma nh manhattan-tree) #:use-module (crates-io))

(define-public crate-manhattan-tree-0.1.0 (c (n "manhattan-tree") (v "0.1.0") (d (list (d (n "bonzai") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1mbilb76lkgfbx5gfwqs50n0cbygwcfz4czdacdv50qczh7pi1pg")))

(define-public crate-manhattan-tree-0.1.1 (c (n "manhattan-tree") (v "0.1.1") (d (list (d (n "bonzai") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "smallqueue") (r "^0.1.0") (d #t) (k 0)) (d (n "through") (r "^0.1.0") (d #t) (k 0)))) (h "0kn6z7fnmm6969zkash2pzm413yknqcd995jyfa51cdzl2k0y3xc")))

