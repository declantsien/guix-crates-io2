(define-module (crates-io ma jo majordome-derive) #:use-module (crates-io))

(define-public crate-majordome-derive-1.0.0 (c (n "majordome-derive") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wvzh96kqqhpx7p1v1pklpz4y7dc6cqlmza9g93j4r3ls0fwlxbs")))

(define-public crate-majordome-derive-1.0.1 (c (n "majordome-derive") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0398bhcx2zmizmib1zzf57aasa9iayn3pxfsy5qs1rqsil2s5r53")))

(define-public crate-majordome-derive-1.0.2 (c (n "majordome-derive") (v "1.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v5wpw8r1s8nnajxn5d2rhpsjv0xhs3ygh6s8p9cvsb6pzizcc13")))

