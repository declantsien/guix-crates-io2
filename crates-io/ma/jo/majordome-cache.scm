(define-module (crates-io ma jo majordome-cache) #:use-module (crates-io))

(define-public crate-majordome-cache-1.0.0 (c (n "majordome-cache") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "majordome") (r "^1") (d #t) (k 0)) (d (n "moka") (r "^0.12") (f (quote ("future"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dxznaav673416pmwhk3hbyzm2jnk8zswjc3lz3l4icnyksx99kb")))

(define-public crate-majordome-cache-1.0.1 (c (n "majordome-cache") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "majordome") (r "^1") (d #t) (k 0)) (d (n "moka") (r "^0.12") (f (quote ("future"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zcar9x6i0iag8gzlw6cmxn3idmaz606zqgzpmvsc945gmy31bll")))

(define-public crate-majordome-cache-1.0.2 (c (n "majordome-cache") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "majordome") (r "^1") (d #t) (k 0)) (d (n "moka") (r "^0.12") (f (quote ("future"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1giyjx2g5b7kwj2sdyyagddbwk3h6q7zyinflm8rhig3ar9ghcwi")))

