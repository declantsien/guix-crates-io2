(define-module (crates-io ma jo majority) #:use-module (crates-io))

(define-public crate-majority-0.1.0 (c (n "majority") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0h1hiv8l2ym9qky2294xkwy7dwny5hz1v95md1rdrwrp32cd47jg")))

(define-public crate-majority-0.1.1 (c (n "majority") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1sx1kq54a302c3qsvkhinq6hfk5fs1sar1grvpd6l5x0j3kni492")))

(define-public crate-majority-0.1.2 (c (n "majority") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "16mqbln52di3kq7yys7wbgsd8nv0hs3xs2gkpd18iwynyywkmfzh")))

(define-public crate-majority-0.1.3 (c (n "majority") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0xvkd9djd6lnjf5887i3prpfdl8nm8z16yjc39k7xf5kmpkrqaa8")))

(define-public crate-majority-0.1.4 (c (n "majority") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0l7s98mb85vcalsxfw6907hybqw9dyfnz5c3q2chg5ijmxaj967y")))

(define-public crate-majority-0.1.5 (c (n "majority") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1ymw180zl1iyzcs4az1gqdzjkm9qyqlh13ff1k77068i2smbcrn7")))

(define-public crate-majority-0.1.6 (c (n "majority") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0c8q5lixrgmpr7fys7jkpqjapzwrg2ih1g9ja679x292dx1dpf6z")))

(define-public crate-majority-0.1.7 (c (n "majority") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0vy2ivfpgsy571pkwpsbkhqsyfnq62bdjv9abrrcqxzh1210f97n")))

(define-public crate-majority-0.1.8 (c (n "majority") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1xlvk2hch5vrzxql288ayzlyscwfji2yl8rq09vjw8gqa252nnzw")))

(define-public crate-majority-0.1.9 (c (n "majority") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1xgjn2r5vha6lpwl7nxcv0bbwwri239iisl7i2mnzsvziinff3m6")))

(define-public crate-majority-0.1.10 (c (n "majority") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "193szjq44wfa1vwq8pcrpkng14klwqvi9yy6lcrbzyp1dfbxhs4k")))

(define-public crate-majority-0.1.11 (c (n "majority") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0sqw6bf49lp0yqzlilb3hw50mgk840yjpldp0z5h434yh76rhhl3")))

(define-public crate-majority-0.1.12 (c (n "majority") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1a4jlz5n2jpl2cdxccd1b90svll3sz29ddmr6qhyfnnwbi7ip7z4")))

(define-public crate-majority-0.1.13 (c (n "majority") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0a50sis0j16kjn6z9az6h7swj4y1ixaysk3zr5cxmy7d29vzrg8l")))

(define-public crate-majority-0.1.14 (c (n "majority") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0yjnc0906qc7p51j6b624xm0k0pnq3l6qgfxacgbfbixyfz5dgkh")))

