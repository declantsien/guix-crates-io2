(define-module (crates-io ma ya maya) #:use-module (crates-io))

(define-public crate-maya-0.1.0 (c (n "maya") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "prettycli") (r "^0.1.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "terminal-menu") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xckz38sfqsy6b9nv4srfzis2lxyjkwy477bdlihsdghpb742vj6")))

