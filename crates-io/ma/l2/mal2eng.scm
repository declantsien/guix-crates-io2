(define-module (crates-io ma l2 mal2eng) #:use-module (crates-io))

(define-public crate-mal2eng-0.1.0 (c (n "mal2eng") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0212amsw606whdhzprsj1b1h4g1dlvgn16icambjcn6kh4kaaqg2")))

(define-public crate-mal2eng-0.1.1 (c (n "mal2eng") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0cgiap3mvd8mpwn9zqamzq0npgaqxh1cn7n177sai8l5b9gr86iz")))

