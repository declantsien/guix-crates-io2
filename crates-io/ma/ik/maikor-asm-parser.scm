(define-module (crates-io ma ik maikor-asm-parser) #:use-module (crates-io))

(define-public crate-maikor-asm-parser-0.1.0 (c (n "maikor-asm-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-language") (r "^0.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07mqvyv82ijlhcs7zj2zvc6xfscc14419pmgzy4sphdkcflfz3f5")))

(define-public crate-maikor-asm-parser-0.1.1 (c (n "maikor-asm-parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-language") (r "^0.1.8") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1b0lg3rvf5fsfjkqqi2zk710iwqg91apxyh6fcikb3v0drk8gzm2")))

(define-public crate-maikor-asm-parser-0.1.2 (c (n "maikor-asm-parser") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-language") (r "^0.1.11") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "13skcsszpyxjd2mj85shynhvlz3frm16nchb81zx912d2w3fmqk0")))

(define-public crate-maikor-asm-parser-0.1.3 (c (n "maikor-asm-parser") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0n716yfkfl897j76mbnxf4f18r2wv7dl0gz663k9b57j1k0qyln7")))

(define-public crate-maikor-asm-parser-0.1.4 (c (n "maikor-asm-parser") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "14p16c7v8bzpjmk6sq7zgxpn60fxk8akh8xqny2yq69yw9mscza9")))

(define-public crate-maikor-asm-parser-0.1.5 (c (n "maikor-asm-parser") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1svqjb4i7w4xqccbqjpcl1cj1fy6299r0g7jv1xx9a8lq44plkvg")))

(define-public crate-maikor-asm-parser-0.1.6 (c (n "maikor-asm-parser") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03psi89zwfsbhfmqq1fdhv4y34bk2vb92pvc510asv7bml27lkrr")))

(define-public crate-maikor-asm-parser-0.1.7 (c (n "maikor-asm-parser") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.23") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zgcfz6p6862axmn4448n9zrlhvy9x2mcgx14av5a2llh1xi5gzl")))

(define-public crate-maikor-asm-parser-0.1.8 (c (n "maikor-asm-parser") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1lljxinzr4p28855qss772k98naxzk2yna5hzvjdvhmlz6bi2xxb")))

(define-public crate-maikor-asm-parser-0.1.9 (c (n "maikor-asm-parser") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jxx3y5zwf9mr28ifx41699n2yl28k8gwvnr6bm92nhfzianmpwh")))

(define-public crate-maikor-asm-parser-0.1.10 (c (n "maikor-asm-parser") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1kzjfiarxys336qybrmyq0qkl6pvyn1wrxy1wgnyylq6a8i90kfx")))

