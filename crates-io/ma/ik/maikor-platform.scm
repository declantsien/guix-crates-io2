(define-module (crates-io ma ik maikor-platform) #:use-module (crates-io))

(define-public crate-maikor-platform-0.1.17 (c (n "maikor-platform") (v "0.1.17") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1i21s7qj13mxmnas9jv48dndd696zzqd8hapkphgpklzrffz78ml")))

(define-public crate-maikor-platform-0.1.18 (c (n "maikor-platform") (v "0.1.18") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vcmj72z3jkjay8vbafqjl7j83sz7abw68d773d9lj4m9s72ipz6")))

(define-public crate-maikor-platform-0.1.19 (c (n "maikor-platform") (v "0.1.19") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01qx3rfd9hqrgs9lq4cafkipbagfi3ax5ry09djgy2krh40d6njb")))

(define-public crate-maikor-platform-0.1.20 (c (n "maikor-platform") (v "0.1.20") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y7rhca7bsyzrs8kz7xvmbkii64an2sv64iz12jwmh3xn989rbir")))

(define-public crate-maikor-platform-0.1.21 (c (n "maikor-platform") (v "0.1.21") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lrld1905wpshd9l204q8vdacq2c2zkjm77rdx8yrnw30672srp2")))

(define-public crate-maikor-platform-0.1.22 (c (n "maikor-platform") (v "0.1.22") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0s6pci9p9wkpvgnbzspq9vk5kpissz7fkklm4asia615lhip839r")))

(define-public crate-maikor-platform-0.1.23 (c (n "maikor-platform") (v "0.1.23") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1x3bdf7rnlc2rwpp1l05swvlxn4bnm9p9mkb1ahpx1zk01vi101h")))

(define-public crate-maikor-platform-0.1.24 (c (n "maikor-platform") (v "0.1.24") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0kdbl8w0mrq751g454pl4yhwl342g6kjqhvbgxpggpvna3wd79yc")))

(define-public crate-maikor-platform-0.1.25 (c (n "maikor-platform") (v "0.1.25") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "126hwspk6kdd6lxpczcgsjg8ynxz8xylj7wgixik8m73hc4kx4h8")))

(define-public crate-maikor-platform-0.1.26 (c (n "maikor-platform") (v "0.1.26") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mkfxg1hfwy776p57zld3kk67c2yd7iqxx2h51c8lpxasw9mpddj")))

(define-public crate-maikor-platform-0.1.27 (c (n "maikor-platform") (v "0.1.27") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10fjvlsk0a47gxvljis1ffy9pfxrd2g0y3g9iyhjm84wkf7mk7f4")))

(define-public crate-maikor-platform-0.1.28 (c (n "maikor-platform") (v "0.1.28") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1sn2jw4i7z1fmgjg4c0bvbgflld5yzwp4bza7sqblw1bnj6sv8sj")))

(define-public crate-maikor-platform-0.1.29 (c (n "maikor-platform") (v "0.1.29") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1w3is3cjws0ca44xpvzg5abp2rsmym973jaiiwlv2zqxgdxin1j1")))

