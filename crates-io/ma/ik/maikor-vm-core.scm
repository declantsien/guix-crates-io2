(define-module (crates-io ma ik maikor-vm-core) #:use-module (crates-io))

(define-public crate-maikor-vm-core-0.1.0 (c (n "maikor-vm-core") (v "0.1.0") (h "0yaq8ra424bai8fyc0lipnq83h529r660923jvj8mzz2n2f88lf6")))

(define-public crate-maikor-vm-core-0.1.1 (c (n "maikor-vm-core") (v "0.1.1") (d (list (d (n "maikor-language") (r "^0.1.3") (d #t) (k 0)))) (h "0ppc77wdq57lw4f6wmnn8c1qbvaas4pjnkvykvyybznchhawvq25")))

(define-public crate-maikor-vm-core-0.1.2 (c (n "maikor-vm-core") (v "0.1.2") (d (list (d (n "maikor-language") (r "^0.1.5") (d #t) (k 0)))) (h "0psgsxi4na9sbmjbac3jsbzlgqkzl9zq5qhsn3rjq7i7qca38xxh")))

(define-public crate-maikor-vm-core-0.1.3 (c (n "maikor-vm-core") (v "0.1.3") (d (list (d (n "maikor-language") (r "^0.1.8") (d #t) (k 0)))) (h "1xc7054brpbhx632fjz56iiykzxccf7pw2cm694ygxzi0ax5j5gp")))

(define-public crate-maikor-vm-core-0.1.4 (c (n "maikor-vm-core") (v "0.1.4") (d (list (d (n "maikor-language") (r "^0.1.8") (d #t) (k 0)))) (h "0yr78pj7q4j8ck1fkqah44lqwdb5jm8nvizhf4fkdmfb1cv69gbc")))

(define-public crate-maikor-vm-core-0.1.5 (c (n "maikor-vm-core") (v "0.1.5") (d (list (d (n "maikor-language") (r "^0.1.11") (d #t) (k 0)))) (h "18r8qs14p3gx74dfasw09hw5y9p1b3kwq02vdspdkxndf4jra6wh")))

(define-public crate-maikor-vm-core-0.1.6 (c (n "maikor-vm-core") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.19") (d #t) (k 0)))) (h "0ri2fdjxqgc4mz61j08ahiajwbssppsw7ykj7vlsn68mr28csf50")))

(define-public crate-maikor-vm-core-0.1.7 (c (n "maikor-vm-core") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.19") (d #t) (k 0)))) (h "0rp3yjwl2mplqv3m05jh228gkrls1r56l7lsh2c3q4dkwf3ijxw7")))

(define-public crate-maikor-vm-core-0.1.8 (c (n "maikor-vm-core") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.21") (d #t) (k 0)))) (h "0nsklnkbabpibyxh40nl205sqjblksla2zhyspwiqw5rzc4j0108")))

(define-public crate-maikor-vm-core-0.1.9 (c (n "maikor-vm-core") (v "0.1.9") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.22") (d #t) (k 0)))) (h "048ki75w55hri0gjrwwsyk9syxadi3cfz82kkgdmwvk3qmqa67ik")))

(define-public crate-maikor-vm-core-0.1.10 (c (n "maikor-vm-core") (v "0.1.10") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)))) (h "0igcqndjgbx3cvrf5jb04p02nwbpqjyw8khs130rg2pylblyyc5j")))

(define-public crate-maikor-vm-core-0.1.11 (c (n "maikor-vm-core") (v "0.1.11") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)) (d (n "maikor-vm-file") (r "^0.1.8") (d #t) (k 0)))) (h "1s209gnjqnihfqabhsdcxz9447fvs4h7h8byadnh5qrvcwgia0vy")))

(define-public crate-maikor-vm-core-0.1.12 (c (n "maikor-vm-core") (v "0.1.12") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-file") (r "^0.1.8") (d #t) (k 0)))) (h "0ida1cxmcyzwc7x4p9j0g1n1babyq63qf7d0a82qx03dg3d672ad")))

(define-public crate-maikor-vm-core-0.1.13 (c (n "maikor-vm-core") (v "0.1.13") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-file") (r "^0.1.9") (d #t) (k 0)))) (h "0czbmbm36m6p23c0drjqinkwv3hbpzdxdlqhg3nisbl3cgiqr97h")))

(define-public crate-maikor-vm-core-0.1.14 (c (n "maikor-vm-core") (v "0.1.14") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-file") (r "^0.1.9") (d #t) (k 0)))) (h "0vwaian2p4nr45wg2d0snaaffbi15jg7lgxrm3wcnag5dwhd14aj")))

(define-public crate-maikor-vm-core-0.1.15 (c (n "maikor-vm-core") (v "0.1.15") (d (list (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-file") (r "^0.1.9") (d #t) (k 0)))) (h "1r83y3gj20swqnh1kdc8bgz66ibg6lrzkz6af8nkks9ch6prw884")))

