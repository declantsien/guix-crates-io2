(define-module (crates-io ma ik maikor-vm-interface) #:use-module (crates-io))

(define-public crate-maikor-vm-interface-0.1.0 (c (n "maikor-vm-interface") (v "0.1.0") (d (list (d (n "maikor-vm-core") (r "^0.1.0") (d #t) (k 0)))) (h "1dhs68badmz1maykrf68c704n8n4jvzjm7wm49d7c1v7xrll7jqw") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.1 (c (n "maikor-vm-interface") (v "0.1.1") (d (list (d (n "maikor-language") (r "^0.1.5") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.2") (d #t) (k 0)))) (h "0bk3f05jndj531apjxfpzjbckdj180vf3pcq0rh5l61nvw1xyili") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.2 (c (n "maikor-vm-interface") (v "0.1.2") (d (list (d (n "maikor-language") (r "^0.1.8") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.3") (d #t) (k 0)))) (h "09fvi2saapa1z73a2nr15qbnaz9ckf1znvm9f5n0n1d3i644zz2a") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.3 (c (n "maikor-vm-interface") (v "0.1.3") (d (list (d (n "maikor-language") (r "^0.1.8") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.4") (d #t) (k 0)))) (h "1djsnfvi6gm5h4if1gm0vcpcdw0j6ddi5cgy732dykx9pz5qd6c5") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.4 (c (n "maikor-vm-interface") (v "0.1.4") (d (list (d (n "maikor-language") (r "^0.1.11") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.5") (d #t) (k 0)))) (h "17wma0b0ll4j47pjzic4vcg5vnx2l8f91a83dcg90mq1px1d142j") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.5 (c (n "maikor-vm-interface") (v "0.1.5") (d (list (d (n "maikor-platform") (r "^0.1.19") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.6") (d #t) (k 0)))) (h "08chypn5lml7cv60jmizmpcm38n7c6pc1x2l5maqh57f574nkaz3") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.6 (c (n "maikor-vm-interface") (v "0.1.6") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.22") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.9") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "1vsw9vmyg2dx18mi7sl686mzg5x3s6ql05lq3km7h4vvvfszbphs") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.7 (c (n "maikor-vm-interface") (v "0.1.7") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.25") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.10") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "084hm4vr505n1f42628h45b0lfpmgnrxamyrn4x0980jwcp240s5") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.8 (c (n "maikor-vm-interface") (v "0.1.8") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.13") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "0m7p3gr9bdyxp05jvvbmi4arikpr1k6i4q9zllqamh738dyjlyrm") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.9 (c (n "maikor-vm-interface") (v "0.1.9") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.14") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "0wx1p8a6c1kig8bfxb7hrlka6vjnj5wraw3qbvzvyjckm19ip118") (f (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1.10 (c (n "maikor-vm-interface") (v "0.1.10") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "maikor-platform") (r "^0.1.27") (d #t) (k 0)) (d (n "maikor-vm-core") (r "^0.1.15") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "1920bfc6g193g1l47y0g8w1h3l5gvmf1q9s8z3d828w3jz8l7vxi") (f (quote (("rgba") ("default") ("argb"))))))

