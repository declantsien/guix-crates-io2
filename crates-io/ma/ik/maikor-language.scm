(define-module (crates-io ma ik maikor-language) #:use-module (crates-io))

(define-public crate-maikor-language-0.1.0 (c (n "maikor-language") (v "0.1.0") (h "12farrhzdyhvqnqbkq1a0m14d7v5kiivm70avzaaxgva77npx61a")))

(define-public crate-maikor-language-0.1.1 (c (n "maikor-language") (v "0.1.1") (h "0v1hqbzj6s4fq23zz3k6n50chfqk5akwxa91x89818dy145wvc4h")))

(define-public crate-maikor-language-0.1.2 (c (n "maikor-language") (v "0.1.2") (h "1hr86xayx6pblyyiidw9g84lq2gwinyg910ig6qkppbybxp9qfm8")))

(define-public crate-maikor-language-0.1.3 (c (n "maikor-language") (v "0.1.3") (h "1j989phkqwqsqqh5fq09j5yf9dpmmngm2xijq3hm41xzmcas9wkc")))

(define-public crate-maikor-language-0.1.4 (c (n "maikor-language") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "099jqywydyrnfkzy60npsk4scgrp7cpq9af0561w068fjpm9cppj")))

(define-public crate-maikor-language-0.1.5 (c (n "maikor-language") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0qbnpx87hpcqv5xiyrff2amp6p26icpcivyadin3092j6gbf951w")))

(define-public crate-maikor-language-0.1.6 (c (n "maikor-language") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ryk6rlkj3dszi0wa95kj5ma7jkj58j2zqp7z33a4wij949pv8lp")))

(define-public crate-maikor-language-0.1.7 (c (n "maikor-language") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i6yms8r85yw54ii7sm3xbw5wigyphsajmy6m7my13nccyfaarij")))

(define-public crate-maikor-language-0.1.8 (c (n "maikor-language") (v "0.1.8") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1iirxzjish7z4rd338d2c95vydpvc2hmrrrk4l1swpsjr5p5hx8v")))

(define-public crate-maikor-language-0.1.9 (c (n "maikor-language") (v "0.1.9") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "106spmhcpnqs2l8y3d0whcj1vhvjncfn3fxxfxl6nqnj1gc0baz8")))

(define-public crate-maikor-language-0.1.10 (c (n "maikor-language") (v "0.1.10") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00b754pz1d8h0md403i6w63qsx866q8jdvv5jf56mjc4ay4krnmh")))

(define-public crate-maikor-language-0.1.11 (c (n "maikor-language") (v "0.1.11") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "18ghijvlnfk582ykvlv6cxkd71q30bky6qhw6zgn3nfg9zrmcd9n")))

(define-public crate-maikor-language-0.1.12 (c (n "maikor-language") (v "0.1.12") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0rlj0dbc58v49b54a5bwbmyxxn8swpb9v8vzc22dblcg9xd9gmx8")))

(define-public crate-maikor-language-0.1.13 (c (n "maikor-language") (v "0.1.13") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z0hvsi4y5sj7qhm1lxcc9w23jm66l70p2av8ba3vjiyhn8sfig0")))

(define-public crate-maikor-language-0.1.14 (c (n "maikor-language") (v "0.1.14") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nafychbdri9mhkl22wk4wyvf305rzl7sxw56dfmrxii7r0i6ab4")))

(define-public crate-maikor-language-0.1.15 (c (n "maikor-language") (v "0.1.15") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19ay77mn7lqkfzl875jfjv2rab5ynhmxpww04n4xq2wfw27c5c5z")))

(define-public crate-maikor-language-0.1.16 (c (n "maikor-language") (v "0.1.16") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0x73p907d2j7w6d0j0gvffq5zrmvp3j7rx0h9fi9jf2qidg7fr8g")))

