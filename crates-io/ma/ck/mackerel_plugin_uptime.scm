(define-module (crates-io ma ck mackerel_plugin_uptime) #:use-module (crates-io))

(define-public crate-mackerel_plugin_uptime-0.0.0 (c (n "mackerel_plugin_uptime") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mackerel_plugin") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "122cslcdzmpi0b9hf29jyz12v0a5fhpahnjgx400gwj7slyaij92")))

(define-public crate-mackerel_plugin_uptime-0.1.0 (c (n "mackerel_plugin_uptime") (v "0.1.0") (d (list (d (n "mackerel_plugin") (r "^0.2") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.0") (d #t) (k 0)))) (h "1x1714w0jwfppc3gjd7n737avlxf3jlfd0gb04qqc2xrvh6kfr9b")))

(define-public crate-mackerel_plugin_uptime-0.2.1 (c (n "mackerel_plugin_uptime") (v "0.2.1") (d (list (d (n "mackerel_plugin") (r "^0.2.6") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.3.0") (d #t) (k 0)))) (h "1im55ak1lwba2bymip1sy2v3kzy0718hlhak53r94i4p67y7l1jj")))

(define-public crate-mackerel_plugin_uptime-0.2.2 (c (n "mackerel_plugin_uptime") (v "0.2.2") (d (list (d (n "mackerel_plugin") (r "^0.2.6") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.3.0") (d #t) (k 0)))) (h "0w361nf70q69x5jbh7ak4dvj8kr27f7d0415dyf7vr95z28cnydn")))

