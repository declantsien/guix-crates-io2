(define-module (crates-io ma ck mackerel_plugin_loadavg) #:use-module (crates-io))

(define-public crate-mackerel_plugin_loadavg-0.0.0 (c (n "mackerel_plugin_loadavg") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mackerel_plugin") (r "^0.1") (d #t) (k 0)))) (h "05zx367sig8s8q5wi0cb83h8v90cn4z364sh1dk0kqhciagn5gd4")))

(define-public crate-mackerel_plugin_loadavg-0.2.0 (c (n "mackerel_plugin_loadavg") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "mackerel_plugin") (r "^0.2.6") (d #t) (k 0)))) (h "1cp3hgd13303nkzm6rvd1cpd17mf09ng5b69ay32jb3acml55grb")))

(define-public crate-mackerel_plugin_loadavg-0.2.1 (c (n "mackerel_plugin_loadavg") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "mackerel_plugin") (r "^0.2.6") (d #t) (k 0)))) (h "1w1piqpf13lmrx30zv2k0n75dn841rrj603k49hj278bh5g8hxr9")))

