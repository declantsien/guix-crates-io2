(define-module (crates-io ma ck mackerel_plugin) #:use-module (crates-io))

(define-public crate-mackerel_plugin-0.0.0 (c (n "mackerel_plugin") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1a128ck2lwxfj9jgaqhd4bw69br4r2n7jp7wv29didwzxy0g1fzh")))

(define-public crate-mackerel_plugin-0.1.0 (c (n "mackerel_plugin") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l50nhs8kl9jn49zrm7x60w9l1dwn7fkaq9w8nly3jj6s2aqi16y")))

(define-public crate-mackerel_plugin-0.2.0 (c (n "mackerel_plugin") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1pyr7jhsa46g5wbm7fsl5k98r1h7j31y67ywm1phmcw4xs1ijwy8")))

(define-public crate-mackerel_plugin-0.2.1 (c (n "mackerel_plugin") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fs5rzfmr5l40grr17k435npxj0439drsvhgxgklfrn18b8aqvzc")))

(define-public crate-mackerel_plugin-0.2.2 (c (n "mackerel_plugin") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1paaqfy7z36hkwz4v47w0pipp6hsmjgy4kvx7h9gvbbs8d4csvxq")))

(define-public crate-mackerel_plugin-0.2.3 (c (n "mackerel_plugin") (v "0.2.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ryb5im9q37alw5xl990f5xa0nhw46azkj2ghf3dlxbkxg3zif40")))

(define-public crate-mackerel_plugin-0.2.4 (c (n "mackerel_plugin") (v "0.2.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1p0phn1wx6ihxbc14ygr6k6f4p032j9h0dcs3bhjcxpfpw4wpyx9")))

(define-public crate-mackerel_plugin-0.2.5 (c (n "mackerel_plugin") (v "0.2.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "11rfyqdr0y0gm1mbaidcykaa787ippq800zjmngk6af8bplp5773")))

(define-public crate-mackerel_plugin-0.2.6 (c (n "mackerel_plugin") (v "0.2.6") (d (list (d (n "auto_enums") (r "^0.8.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yhxbdlb2zwfa0qpg58vql5rzqhsddk51r0dvfhlggmyi84rg792")))

