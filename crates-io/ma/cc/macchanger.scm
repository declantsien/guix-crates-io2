(define-module (crates-io ma cc macchanger) #:use-module (crates-io))

(define-public crate-macchanger-0.1.0 (c (n "macchanger") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "netdevice") (r "^0.1.1") (d #t) (k 0)))) (h "1c2l1yyj7z96paz5i52hx12479m0c1ccr647nj9dymx463k1m3hi")))

(define-public crate-macchanger-0.2.0 (c (n "macchanger") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "netdevice") (r "^0.1.1") (d #t) (k 0)))) (h "1h5n2lbranvs8y25i4dblv3a79b83zsh87h0fj8pagsk31a52xsl")))

(define-public crate-macchanger-0.3.0 (c (n "macchanger") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "netdevice") (r "^0.1.1") (d #t) (k 0)))) (h "0mkfcw1cmp57mjwrh1yp8f72z8l9bh12ah23rq4dxzv4cz302dy9")))

(define-public crate-macchanger-0.3.1 (c (n "macchanger") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "netdevice") (r "^0.1.1") (d #t) (k 0)))) (h "1m37iqxic0xlc1qkvs0w4vd5yvqkfszf8qh3d38cyndmkbz0vm9g")))

(define-public crate-macchanger-0.4.0 (c (n "macchanger") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "netdevice") (r "^0.1.1") (d #t) (k 0)))) (h "0h4b2k3jrbbp0rhgjfkhgwa1yn10i3h25spfx2jnhh5v8kaqaim9")))

