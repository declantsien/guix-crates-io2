(define-module (crates-io ma x7 max7456) #:use-module (crates-io))

(define-public crate-max7456-0.1.0 (c (n "max7456") (v "0.1.0") (d (list (d (n "peripheral-register") (r "^0.1.3") (d #t) (k 0)))) (h "0lqfs8z6582wji64m441pl4ximm8xjpjlyjh7k2j6wmchyb42fdj")))

(define-public crate-max7456-0.1.1 (c (n "max7456") (v "0.1.1") (d (list (d (n "peripheral-register") (r "^0.1.3") (d #t) (k 0)))) (h "162qdm1ad5pgz98ki1mz327gf10z4v4110vj4x9aq5j8zchfp5nz")))

(define-public crate-max7456-0.1.2 (c (n "max7456") (v "0.1.2") (d (list (d (n "peripheral-register") (r "^0.1.3") (d #t) (k 0)))) (h "0wf0vgvpnhhsxc85jp2hsl5121ip3rq7paldzjnizhzjmv4fcl46")))

(define-public crate-max7456-0.1.3 (c (n "max7456") (v "0.1.3") (d (list (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "1jyr2dfd546np4w23j5g5nj05308gcwb70krb4blgqy8nmk4nsk4")))

(define-public crate-max7456-0.1.4 (c (n "max7456") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "1mhkm15lw7ff7gx0y9jbcs6b3cmjldp88lckln5bvmgm7smzm24f")))

(define-public crate-max7456-0.1.5 (c (n "max7456") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "07s6f1cjh43wb1r0k91d1h84i6qagma7q5iph8n9kx0ng06rif55")))

(define-public crate-max7456-0.1.6 (c (n "max7456") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "0af5099gmk5kx3742k3bxl82kn4sndn7ql1c3nrnmvl0fn08ls65")))

(define-public crate-max7456-0.1.7 (c (n "max7456") (v "0.1.7") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "0ir1fjmnx1qd0r3v3b60zvr7ma6wmmr7zvifq61c77nwyc86axkl")))

(define-public crate-max7456-0.1.8 (c (n "max7456") (v "0.1.8") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "1yiyxdm3v95a8274j6sag363jxx28liv39kvamwazfjr3gl74r29")))

(define-public crate-max7456-0.1.9 (c (n "max7456") (v "0.1.9") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "0a8mqyiss2j87p731hwy3vwg70jn9ssp7r212g2bb9l15vwi3z1k")))

(define-public crate-max7456-0.1.10 (c (n "max7456") (v "0.1.10") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "peripheral-register") (r "^0.1.4") (d #t) (k 0)))) (h "1w4jh3066bdjl6fnj8nv20sshd3ypdys4mhzq087k90vc1irx04c")))

