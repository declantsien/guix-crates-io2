(define-module (crates-io ma x7 max7219-async) #:use-module (crates-io))

(define-public crate-max7219-async-0.1.0 (c (n "max7219-async") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "1vyz8pjb0zklmq5g3bg9l8fglqhgv2cm6f9ljci9lm94vpbyc729") (s 2) (e (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max7219-async-0.1.1 (c (n "max7219-async") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "1slq37cq9zik33lpjjzw2c8db92wiyx5vrzkcm8srlk51p1zfbq9") (s 2) (e (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

