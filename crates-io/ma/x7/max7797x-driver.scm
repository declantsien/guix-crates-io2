(define-module (crates-io ma x7 max7797x-driver) #:use-module (crates-io))

(define-public crate-max7797x-driver-0.1.0 (c (n "max7797x-driver") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1sia6y0zf5skfkvc0mi9srkshlfajn6jxvam71yg6vg85shvhx7a") (f (quote (("default")))) (s 2) (e (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max7797x-driver-0.1.1 (c (n "max7797x-driver") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1ilf70cnpqqr6pw6j07hfh9982rkczljkf8zjzi4ixdylsrp4438") (f (quote (("default")))) (s 2) (e (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

