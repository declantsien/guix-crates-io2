(define-module (crates-io ma x7 max7219-canvas) #:use-module (crates-io))

(define-public crate-max7219-canvas-0.1.0 (c (n "max7219-canvas") (v "0.1.0") (d (list (d (n "max7219") (r "^0.4.2") (d #t) (k 0)))) (h "04x08gq085dy3dvffs72l603pc5yyrr0nrskh9y1hpcmma5gkq4f")))

(define-public crate-max7219-canvas-0.1.1 (c (n "max7219-canvas") (v "0.1.1") (d (list (d (n "max7219") (r "^0.4.2") (d #t) (k 0)))) (h "0pg32c9rr4vx79h45dc3w4lzlp4sbii3xlx0wkz1bx0gicxqpgkc")))

(define-public crate-max7219-canvas-0.1.2 (c (n "max7219-canvas") (v "0.1.2") (d (list (d (n "max7219") (r "^0.4.2") (d #t) (k 0)))) (h "0n7zfrw7ya2j5pcwmpfhwll55434pl5maymrg6gn3pcg6zsw8nlv")))

