(define-module (crates-io ma x7 max7219) #:use-module (crates-io))

(define-public crate-max7219-0.1.0 (c (n "max7219") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "04xcbdgvijvnrazhn1q68f6v1xdxbgkxvialmlmjhkp9vdzzrym8")))

(define-public crate-max7219-0.1.1 (c (n "max7219") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1636bm9snzynxxa9nxmdcjx75k1p1c22fwd48n8r860jxsciz1f1")))

(define-public crate-max7219-0.1.2 (c (n "max7219") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1kmia4qdp6rki07i9rlxyc7zymi1r8j5zxa2vq8z5xa0dy4fh8nx")))

(define-public crate-max7219-0.2.0 (c (n "max7219") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0b54pk9x742i4lsj2m2w6g381s79rqd54bqvjcfmvwgdb6m0hs00")))

(define-public crate-max7219-0.2.1 (c (n "max7219") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0516058pwfipjsksrnmwlfn971ash1gqsnd656w5s2d07a6mcss2")))

(define-public crate-max7219-0.2.2 (c (n "max7219") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1d5sci3mnm7sc1nyz7s4cwflfyfksmywzyw3vr41w2iq9mprkcli")))

(define-public crate-max7219-0.3.0 (c (n "max7219") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0r333flb3sbzng4pcmpfhvqzg2qp4ckkhwajljxvzckcc83pc23n")))

(define-public crate-max7219-0.3.1 (c (n "max7219") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "04mi95qg3wq3fqky6ja7lbj3yx9adaq41x69ivw03m98pm6l05vw")))

(define-public crate-max7219-0.4.0 (c (n "max7219") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0a9lkpbgd3jigbkaaxnq0w0hkc6yssrvfnx2dz25p63pj7asprmm")))

(define-public crate-max7219-0.4.2 (c (n "max7219") (v "0.4.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "070d80ic88qxjml4n7av0kkg933wlpmjh46gqfbf3rcn9gqkdbcn")))

