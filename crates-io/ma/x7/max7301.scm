(define-module (crates-io ma x7 max7301) #:use-module (crates-io))

(define-public crate-max7301-0.1.0 (c (n "max7301") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "06v8wksvbvqmz56gglaa6pw8w5x4hrcgavsh9qplsbaij30yrj5g") (f (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

(define-public crate-max7301-0.2.0 (c (n "max7301") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "1hwx5k2zlwn95927pnrsvrjkk9g580ymwzra2gh9yvb0cnjdj7i9") (f (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

(define-public crate-max7301-0.3.0 (c (n "max7301") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0ixqzn9yr6bd17jb672n8r5fhdbmxhkha9pk31x25irqx9rd77h8") (f (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

