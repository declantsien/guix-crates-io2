(define-module (crates-io ma x7 max7219-driver) #:use-module (crates-io))

(define-public crate-max7219-driver-0.1.0 (c (n "max7219-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "16pqmjpgavcj8ak6bcxzmpvnndc0hx69nsqf6ijd5kjl7b9xfai7") (y #t)))

(define-public crate-max7219-driver-0.1.1 (c (n "max7219-driver") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "02z7x4gakiqzjvh14zafws5vyxjh5rahamxv8a18h9l25hf9mfkh")))

(define-public crate-max7219-driver-0.2.0 (c (n "max7219-driver") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "05w7c9v2wcyz7qvmbm50fm9gckqnanblm52zilrb2gy96y2ykfyw") (y #t)))

(define-public crate-max7219-driver-0.2.1 (c (n "max7219-driver") (v "0.2.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0w0s5m448x9l93hd3qabqgnfbwd9zvhzcvvxcbqq5nzbl3wbs1w2")))

(define-public crate-max7219-driver-0.2.2 (c (n "max7219-driver") (v "0.2.2") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)))) (h "13j44ylcqw5i456kr2amgilmcaj67rqbg6ahi5m52df9ykxmsbrh")))

