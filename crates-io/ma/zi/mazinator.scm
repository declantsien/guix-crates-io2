(define-module (crates-io ma zi mazinator) #:use-module (crates-io))

(define-public crate-mazinator-0.1.0 (c (n "mazinator") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1fs005yb5yhb7zdxr57ivc5d2b7bdnrjm8knbxgrxiy88dyyjal4")))

(define-public crate-mazinator-0.1.1 (c (n "mazinator") (v "0.1.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1h08c5zcxi30s33jdbkzwyazhg547z1bbzjn0wc99z9gz3lnn109") (y #t)))

(define-public crate-mazinator-0.1.2 (c (n "mazinator") (v "0.1.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1yrni2y6bzs2myp343sn2g9mmvgmg8028q9w5y2wa47637r4mjzv")))

