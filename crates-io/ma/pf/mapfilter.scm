(define-module (crates-io ma pf mapfilter) #:use-module (crates-io))

(define-public crate-mapfilter-0.1.0 (c (n "mapfilter") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "ellipse") (r "^0.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10") (d #t) (k 0)) (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "osmpbf") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thousands") (r "^0.2") (d #t) (k 0)))) (h "0vfz9nx7xmx682grqa7cp646qmsfgkrp5ri4xf146y60bhyb3lx8")))

