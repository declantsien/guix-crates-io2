(define-module (crates-io ma x- max-grrs) #:use-module (crates-io))

(define-public crate-max-grrs-0.1.0 (c (n "max-grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)))) (h "01141hm4y1zrx1nnqqid9cda35ym1pk18pnl11hb8ajk0k6nvm2f")))

