(define-module (crates-io ma x- max-7219-led-matrix-util) #:use-module (crates-io))

(define-public crate-max-7219-led-matrix-util-0.1.0 (c (n "max-7219-led-matrix-util") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (d #t) (k 0)) (d (n "max7219") (r "^0.2.2") (d #t) (k 0)))) (h "0gahk6bxppr17hyw5k59pllhllvksfsjgdk4fy5yirw1nqw61k2d")))

(define-public crate-max-7219-led-matrix-util-0.1.1 (c (n "max-7219-led-matrix-util") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (d #t) (k 0)) (d (n "max7219") (r "^0.2.2") (d #t) (k 0)))) (h "1h4frlvnhmgxf0lx50vq5l8hxxgjjbdisvblafni08xpvf488zcv")))

(define-public crate-max-7219-led-matrix-util-0.1.2 (c (n "max-7219-led-matrix-util") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (d #t) (k 0)) (d (n "max7219") (r "^0.2.2") (d #t) (k 0)))) (h "0v9iy7qlcqz54xf6fcqnjf9x38wwaqr80w6ljfk0c0rsa62a3hfb")))

(define-public crate-max-7219-led-matrix-util-0.1.3 (c (n "max-7219-led-matrix-util") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (d #t) (k 0)) (d (n "max7219") (r "^0.2.2") (d #t) (k 0)))) (h "0c4b2p7p4ab8qvik56mkpadq9flcasnzbj29vb9nwk3adnivvpkb")))

(define-public crate-max-7219-led-matrix-util-0.1.4 (c (n "max-7219-led-matrix-util") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (d #t) (k 0)) (d (n "max7219") (r "^0.2.2") (d #t) (k 0)))) (h "0xx7r4hxyaiw8whybqpa4mvprh4win3f50n6cavgwzgwsfnaf09s")))

(define-public crate-max-7219-led-matrix-util-0.2.0 (c (n "max-7219-led-matrix-util") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "max7219") (r "^0.3.0") (d #t) (k 0)))) (h "1xffwd8wdyxr68j7bsmdrkl11gqfxbasd41hyfj9rxznp9gli36j") (f (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2.1 (c (n "max-7219-led-matrix-util") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "max7219") (r "^0.3.0") (d #t) (k 0)))) (h "1zv0v3hyrlcfb5s66x0ms73zgyqa5jk91i3v7zplx549p3qp94fk") (f (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2.2 (c (n "max-7219-led-matrix-util") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "max7219") (r "^0.3.0") (d #t) (k 0)))) (h "0n2ykcj5apvlcl6avav5hz87j0nxwbvm18llmz68xaa4d56pqxkd") (f (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2.3 (c (n "max-7219-led-matrix-util") (v "0.2.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "max7219") (r "^0.3.0") (d #t) (k 0)))) (h "0qj6dg21rvclfvpcyanpd4xv416qrdf46k2zyhv94xb8fjkyq0b2") (f (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2.4 (c (n "max-7219-led-matrix-util") (v "0.2.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "max7219") (r "^0.3.1") (d #t) (k 0)))) (h "185l93v7b901iz13p8z26c4sv5bb0piyr4znyza0jjbds829fgf9") (f (quote (("std" "gpio-cdev") ("default" "std"))))))

