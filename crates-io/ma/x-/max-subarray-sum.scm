(define-module (crates-io ma x- max-subarray-sum) #:use-module (crates-io))

(define-public crate-max-subarray-sum-0.1.0 (c (n "max-subarray-sum") (v "0.1.0") (h "07wg1icncx9hnb092gfji8ybcvsmfvgyaffa2wmk5024x8gg495h") (y #t)))

(define-public crate-max-subarray-sum-0.1.1 (c (n "max-subarray-sum") (v "0.1.1") (h "05b372nm6lr72sz36xdrh68gnjlqpnz9kxmy3wkkgw5bvhjhqyv0") (y #t)))

(define-public crate-max-subarray-sum-0.1.2 (c (n "max-subarray-sum") (v "0.1.2") (h "1msidnq6jps6r5896hjq0rr7lqdi8ag302a61j0ksvrvan08i4a0") (y #t)))

(define-public crate-max-subarray-sum-0.1.3 (c (n "max-subarray-sum") (v "0.1.3") (h "1p3x3wljfrbk02azvajbzwmlari7626af8i77q88dzafvfvjbgrs") (y #t)))

(define-public crate-max-subarray-sum-0.1.4 (c (n "max-subarray-sum") (v "0.1.4") (h "03334gm22pvrwmxa8ds6wg5gjr3f1saw9xrwhdnvqcg5n4dxlcxs") (y #t)))

(define-public crate-max-subarray-sum-0.1.5 (c (n "max-subarray-sum") (v "0.1.5") (h "085qphf3b75zz4r5mhjv1lg452jyw8sbjzyf6wyr9xmmbzma9gb3") (y #t)))

(define-public crate-max-subarray-sum-0.1.6 (c (n "max-subarray-sum") (v "0.1.6") (h "1ndgljnhqw2ad451xyma1xnpxmb2n4v3a8xprz2b2kq8gq6byr74") (y #t)))

(define-public crate-max-subarray-sum-0.1.7 (c (n "max-subarray-sum") (v "0.1.7") (h "0fyxshvnk69qbmc5cq7lzsanjfb3637429k5v5bddnhajpq89ryk")))

