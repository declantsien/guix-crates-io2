(define-module (crates-io ma cb macbat) #:use-module (crates-io))

(define-public crate-macbat-0.1.0 (c (n "macbat") (v "0.1.0") (h "1mxlmx3mnynqvag0cg5z9cmx3mkn28nxib298iv1drapfsbx5j56")))

(define-public crate-macbat-0.1.1 (c (n "macbat") (v "0.1.1") (h "0h2jppr0b7b3aacmzxqi5vxsv99h1ng4b3170mvq61140g1zfpd9")))

