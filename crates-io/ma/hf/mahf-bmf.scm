(define-module (crates-io ma hf mahf-bmf) #:use-module (crates-io))

(define-public crate-mahf-bmf-0.1.0 (c (n "mahf-bmf") (v "0.1.0") (d (list (d (n "better_any") (r "^0.2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "mahf") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1ndnmknrkwradcpsbrac6r4p7h92xl2zyj2p1089736n1ayj6ppj")))

