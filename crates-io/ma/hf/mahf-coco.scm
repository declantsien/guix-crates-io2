(define-module (crates-io ma hf mahf-coco) #:use-module (crates-io))

(define-public crate-mahf-coco-0.1.0 (c (n "mahf-coco") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "better_any") (r "^0.2.0") (d #t) (k 0)) (d (n "coco-rs") (r "^0.6") (d #t) (k 0)) (d (n "mahf") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvl9sj8zpzd7x27lb7c0y4628gd1m2a3qsghz3v2ngi2nxjfawd")))

