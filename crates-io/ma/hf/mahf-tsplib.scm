(define-module (crates-io ma hf mahf-tsplib) #:use-module (crates-io))

(define-public crate-mahf-tsplib-0.1.0 (c (n "mahf-tsplib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mahf") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tspf") (r "^0.3.1") (d #t) (k 0)))) (h "1xg4kyg2ssbichppa8zx225nwlf4fvh9maa91y3bzhdpja07iq43")))

