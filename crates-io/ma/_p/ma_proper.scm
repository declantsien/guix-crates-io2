(define-module (crates-io ma _p ma_proper) #:use-module (crates-io))

(define-public crate-ma_proper-0.1.0 (c (n "ma_proper") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "07bkk2fcmlc2f567zpl9m029ixmaqhnqx561b2l6sg68izb7jl8s") (f (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1.1 (c (n "ma_proper") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "0s05wl9jgc7gahabb2cpn3xhd28na1lq2v6njl9q9g1jwbsakl8g") (f (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1.2 (c (n "ma_proper") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "0zh0qr5jh5hvmg0byzki67yc2za15as42l4xyprssnim68qqfb6h") (f (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1.3 (c (n "ma_proper") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "1k3wja3srpqzxn9f7sgw3sz1jay84g5bcwji41gv72yfd526a23s") (f (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1.4 (c (n "ma_proper") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "1hp4dvrmg1xpafdac7127ah0rn84vb8hdc7j3pqjbqwdy0jxyipr") (f (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-1.0.0 (c (n "ma_proper") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17pp87lqvbna1506jvbi21ckr451j7pw5n3zmsskl92p63vfv36f") (f (quote (("volatile_fallback") ("trace") ("default"))))))

