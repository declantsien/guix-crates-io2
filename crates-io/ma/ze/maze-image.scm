(define-module (crates-io ma ze maze-image) #:use-module (crates-io))

(define-public crate-maze-image-0.0.0 (c (n "maze-image") (v "0.0.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "maze-core") (r "^0.0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1xv1q80cr7vqlggrqdn1z7mj4b8473l9i88a7y39h8v4ylxhk9qa") (f (quote (("mota") ("default" "mota"))))))

