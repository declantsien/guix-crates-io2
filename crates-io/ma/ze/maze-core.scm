(define-module (crates-io ma ze maze-core) #:use-module (crates-io))

(define-public crate-maze-core-0.0.0 (c (n "maze-core") (v "0.0.0") (d (list (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "1kmkibiv31f2r5x4gvq5259sjcijdn9b13y9851c00h8bikl6qqj") (f (quote (("default"))))))

(define-public crate-maze-core-0.0.1 (c (n "maze-core") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s3cxqw2mc08r450whk94yffqas3g6rz5b0jmzcsnw7yn11df6d2") (f (quote (("default"))))))

(define-public crate-maze-core-0.0.2 (c (n "maze-core") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "taxicab-map") (r "^0.1.3") (d #t) (k 0)))) (h "1djfbpvf0hrgd5w05m0lvb80l6qymdc1w04a2wd4mnf9r5s0cng3") (f (quote (("default"))))))

