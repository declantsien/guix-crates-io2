(define-module (crates-io ma ze maze_generator) #:use-module (crates-io))

(define-public crate-maze_generator-0.1.0 (c (n "maze_generator") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0p4712p7skijgxvd28w04dh88pymvakbrsng481abrzbm50775cv")))

(define-public crate-maze_generator-0.1.1-alpha.0 (c (n "maze_generator") (v "0.1.1-alpha.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0iknxag1kmvhxxgg8z0kf0217c70psgd7xd4kv7rzvxh9m37b570") (y #t)))

(define-public crate-maze_generator-0.1.1 (c (n "maze_generator") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "1vbf0frmnylrw3fakhvxhakdrxin2ssy1mz7wwjm6v6dr0d1mzw5")))

(define-public crate-maze_generator-1.0.0-rc.0 (c (n "maze_generator") (v "1.0.0-rc.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "10ja0vrka9l4c2avnpinqq9pfya2kaqh7dc6yig22qazl2sgc7x8")))

(define-public crate-maze_generator-1.0.0-rc.1 (c (n "maze_generator") (v "1.0.0-rc.1") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0hn2bks1y1h6kx0qsad6yxm6gaw8354w3kg312f4ab5bjmnqpjsc")))

(define-public crate-maze_generator-1.0.0-rc.2 (c (n "maze_generator") (v "1.0.0-rc.2") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0f891hiy4ccll3arcc75vzmzcwymf6958r3q9ycas971ihv2jd19")))

(define-public crate-maze_generator-1.0.0 (c (n "maze_generator") (v "1.0.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "1si5dpb038iilpwbgl0fg1zsrxli5414706y40z9wn6phppak44c")))

(define-public crate-maze_generator-1.1.0 (c (n "maze_generator") (v "1.1.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "07crd7hq937w555d1x68b26jlhaf1f9ssymsv9y7drdilhawn6wp")))

(define-public crate-maze_generator-1.1.1 (c (n "maze_generator") (v "1.1.1") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0zdrmn36hpshvv3za1llcgmxc55adhapkficyhpr6q7522jn15wg")))

(define-public crate-maze_generator-1.2.0 (c (n "maze_generator") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0wxd9rsvswg0rp75n85lvm24qlwfcmplr46zrszj0s3yqcyilg0n")))

(define-public crate-maze_generator-2.0.0 (c (n "maze_generator") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0q7d3m0bjniwna15zmxyffgfwqj70xgxq7hwxz0jz5pnaj5zln54")))

