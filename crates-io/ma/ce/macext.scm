(define-module (crates-io ma ce macext) #:use-module (crates-io))

(define-public crate-macext-0.1.0 (c (n "macext") (v "0.1.0") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "12phd93w6nbrcskiff6xd7kmpasddrvx0pjd1ijjd0xlm3qk55vd")))

(define-public crate-macext-0.1.1 (c (n "macext") (v "0.1.1") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "0cif8ljdjs60gjxbzcz1hxb619mcykw795i2358cqyk01ra7zc7q")))

(define-public crate-macext-0.1.2 (c (n "macext") (v "0.1.2") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "1c5vr2jsjv1i5m7c21agbnlkw1a0r3xc5lwh9ryqj7v4fg2238mm")))

(define-public crate-macext-0.1.3 (c (n "macext") (v "0.1.3") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "08rrzrfc7lv7gc5xjf01cr0f3kdkm20silx8fm0y4vv45zy3cd8f")))

(define-public crate-macext-0.1.4 (c (n "macext") (v "0.1.4") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "0q5h5nzn2vdz96g81a7wvf63wlzwd6id5jbiq8j8lvmjk1ccvylq")))

(define-public crate-macext-0.1.5 (c (n "macext") (v "0.1.5") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "1vvamylr5vkv34h46hrjvl19bg00r8cg98kqwhqb7ff2hy1sc6dy")))

(define-public crate-macext-0.1.6 (c (n "macext") (v "0.1.6") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "14da7f9a26hi95y5g4m5q5m07hd8icrjy9kbb9p3yfszg17my5ds")))

(define-public crate-macext-0.1.7 (c (n "macext") (v "0.1.7") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "1vl4a5nr5yzr5wn4gmwqy0351vah1wz1a8jw1v57hlw872s8q2ah")))

(define-public crate-macext-0.1.8 (c (n "macext") (v "0.1.8") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "1p06pwvgv5pyg0qmz078w379pkbxaxm90jlrngdnsjln58brra0h")))

(define-public crate-macext-0.1.9 (c (n "macext") (v "0.1.9") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "1xnd6rc55l94lnjw95jbx8bvj56plgaq5bz6gvvxx4xd7qis2x61")))

(define-public crate-macext-0.2.0 (c (n "macext") (v "0.2.0") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "0gbghvnb1bmxzzc1c0dy080ddkcq79r87zmhskwqw5agpj1ry5qm")))

(define-public crate-macext-0.2.1 (c (n "macext") (v "0.2.1") (d (list (d (n "mach2") (r "^0.4.2") (d #t) (k 0)) (d (n "process-memory") (r "^0.5.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)))) (h "16cm003vizwkiri76s1hni3n8rdxn0qqm196m11566n4mn6cdrq3")))

