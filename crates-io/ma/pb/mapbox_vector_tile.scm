(define-module (crates-io ma pb mapbox_vector_tile) #:use-module (crates-io))

(define-public crate-mapbox_vector_tile-0.1.0 (c (n "mapbox_vector_tile") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "geo") (r "^0.7.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)) (d (n "slippy-map-tiles") (r "^0.14.0") (d #t) (k 0)))) (h "0k5h0f9a83gpjn47q4dcqgnrq9bqnb19s6h4rgmgql0glnnzijzx")))

(define-public crate-mapbox_vector_tile-0.4.0 (c (n "mapbox_vector_tile") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "geo") (r "^0.12.0") (d #t) (k 0)) (d (n "protobuf") (r "~1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)) (d (n "slippy-map-tiles") (r "^0.14.0") (d #t) (k 0)))) (h "08ynnsvwhsxzw28xajkabl9dbw0zlq5l62qi57jwc3xpzybwky6b")))

