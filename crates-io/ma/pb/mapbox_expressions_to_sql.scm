(define-module (crates-io ma pb mapbox_expressions_to_sql) #:use-module (crates-io))

(define-public crate-mapbox_expressions_to_sql-0.1.0 (c (n "mapbox_expressions_to_sql") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dk2mx7d8r6bg5d95440x9rbm9xyhplkr6zas6cjdz3rmni8wx8f")))

