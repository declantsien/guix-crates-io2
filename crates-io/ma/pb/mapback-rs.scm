(define-module (crates-io ma pb mapback-rs) #:use-module (crates-io))

(define-public crate-mapback-rs-0.1.0 (c (n "mapback-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)))) (h "0ksval675569ag4q400f5lvlyclsyx7ffhqr516b7jcay9452l1a")))

