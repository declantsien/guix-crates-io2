(define-module (crates-io ma ki makima_spline) #:use-module (crates-io))

(define-public crate-makima_spline-1.0.0 (c (n "makima_spline") (v "1.0.0") (h "1gnfn8cnxq4wipjqmzs67giyb5680y11dks63j2mz0x1f75ran7m")))

(define-public crate-makima_spline-1.0.1 (c (n "makima_spline") (v "1.0.1") (h "11c97hsjmi9vaklq49pckbs8xlkciqgp0911c5n4f4za315s7zr1")))

(define-public crate-makima_spline-1.0.2 (c (n "makima_spline") (v "1.0.2") (h "0nnbkqizypd191xjg6z86lrbpa82hif2pbkw3kp36s65mg3r9csf")))

(define-public crate-makima_spline-1.0.3 (c (n "makima_spline") (v "1.0.3") (h "0a1cw77pm49g61f8fwhwnm434kvmbrk2i8cillkfqp4yrapvrjng")))

(define-public crate-makima_spline-1.0.4 (c (n "makima_spline") (v "1.0.4") (h "0vbfh4hd857vninl1jn84hcr5cw6mh07dqhfgr6j8bddbx5a81pj")))

(define-public crate-makima_spline-1.1.0 (c (n "makima_spline") (v "1.1.0") (h "002zdfbv2kjgqszxmj3whl31m5s3m6qf80xf5pmm2ncdsi7l9p22")))

(define-public crate-makima_spline-1.1.1 (c (n "makima_spline") (v "1.1.1") (d (list (d (n "bicubic") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0qxh06y24zya0zxkz5lz3885iq5kkyc2czxbw00q2ragx32sma4f") (f (quote (("default" "bicubic") ("bi_dimensional" "bicubic")))) (y #t)))

(define-public crate-makima_spline-1.1.2 (c (n "makima_spline") (v "1.1.2") (d (list (d (n "bicubic") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "00s1cf58i7nrg2xw68md81b5q89y8agp0x8spz7asbbxh1ps91s9") (f (quote (("n_dimensional" "bicubic"))))))

(define-public crate-makima_spline-1.1.3 (c (n "makima_spline") (v "1.1.3") (d (list (d (n "bicubic") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1i2hviw1rkr89l3mrgnv0bszbhgj9q360sq0f2zy4kxdddxgz8i0") (f (quote (("n_dimensional" "bicubic"))))))

