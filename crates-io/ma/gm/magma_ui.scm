(define-module (crates-io ma gm magma_ui) #:use-module (crates-io))

(define-public crate-magma_ui-0.1.0-alpha.1 (c (n "magma_ui") (v "0.1.0-alpha.1") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "iced_winit") (r "^0.10.1") (d #t) (k 0)) (d (n "magma_app") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "magma_winit") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1jq3glcrdaw1451jbxfp8i0i4q6vmrzgk52i6pwbf9i5m4anvrjh")))

(define-public crate-magma_ui-0.1.0-alpha.2 (c (n "magma_ui") (v "0.1.0-alpha.2") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "iced_winit") (r "^0.10.1") (d #t) (k 0)) (d (n "magma_app") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "magma_winit") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "13avnlpgwy8pxf9ghhqyk77mzzr5gzpxm42zszrnfwrv74za719g")))

