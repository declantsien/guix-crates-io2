(define-module (crates-io ma gm magma) #:use-module (crates-io))

(define-public crate-magma-0.0.0 (c (n "magma") (v "0.0.0") (h "1qy2fa6k7jbm121amwvd0pm0jx4g6mpvvrb6jsx4j43i92a7labp")))

(define-public crate-magma-0.1.0 (c (n "magma") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1i22n1k60x440vyql376c5axsc3qxqvj0hwfa2m9lb99jn8l9mfj")))

(define-public crate-magma-0.1.1 (c (n "magma") (v "0.1.1") (d (list (d (n "block-cipher-trait") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1qr1p93k1kyvisrf3aj02rzv6v0pvsb3v9wwng1mncfrr8alzpfr")))

(define-public crate-magma-0.2.0 (c (n "magma") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "19vv6ymdy1sncqawqwwlwqma8c87d8wcfjzf1z6lf0nfvb2lyz6k")))

(define-public crate-magma-0.3.0 (c (n "magma") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0v79wbjhs5qfrsp721qvbmdgrip3qmy4chxhss8ybbcyldzgd5mj")))

(define-public crate-magma-0.4.0 (c (n "magma") (v "0.4.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0zhabs0p5q8w25d1gi13rim5pv2jhgmfb3nc2sz7zkdwk9b3sc62")))

(define-public crate-magma-0.5.0 (c (n "magma") (v "0.5.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0n2qf8a5n7b4481l7hj4p9qpsn5cm22j7bzdhhx4kb8nk7bv5zbk")))

(define-public crate-magma-0.6.0 (c (n "magma") (v "0.6.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1lg0vwj6znbg6910w3hfx838kq1h0iiww6s1bwq57mp52isbvpqy")))

(define-public crate-magma-0.7.0 (c (n "magma") (v "0.7.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "06zrvwrlc2xnqjmzx06haz5fsa4a6yib1cjs1j43v31lr9zjqyak")))

(define-public crate-magma-0.8.0 (c (n "magma") (v "0.8.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0wdhbcdas5p3pmv79a8f485qdjp9d7nf4ghywbcq7b331rw07866") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-magma-0.8.1 (c (n "magma") (v "0.8.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "1w321p30599kc85bb35ps32ykqc9gzpi5b99dsw793nxrypnj9xb") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-magma-0.9.0 (c (n "magma") (v "0.9.0") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "1d7drri3cdwdzdsgk6n6g372yb2yka7y428sw9z45jszp8jbs2ab") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-magma-0.10.0-pre.0 (c (n "magma") (v "0.10.0-pre.0") (d (list (d (n "cipher") (r "=0.5.0-pre.4") (d #t) (k 0)) (d (n "cipher") (r "=0.5.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "12m5cxqq8vq28axh44msisilhmxpp2qrfp16k168c7v5pbpr6viv") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.65")))

