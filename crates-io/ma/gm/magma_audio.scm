(define-module (crates-io ma gm magma_audio) #:use-module (crates-io))

(define-public crate-magma_audio-0.1.0-alpha (c (n "magma_audio") (v "0.1.0-alpha") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)) (d (n "magma_app") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0gy95qp9krih8vhkkd7r4463nfm3p0rpiqki3bm0al5h73f62mg4")))

(define-public crate-magma_audio-0.1.0-alpha.1 (c (n "magma_audio") (v "0.1.0-alpha.1") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)) (d (n "magma_app") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0qicjdnmlksbvrlas6m0a8qxr0vn3v03wx3lavb1ghrn3k710iq4")))

(define-public crate-magma_audio-0.1.0-alpha.2 (c (n "magma_audio") (v "0.1.0-alpha.2") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)) (d (n "magma_app") (r "^0.1.0-alpha.5") (d #t) (k 0)))) (h "11b9ns71x5i1spgylsfrlfxfiyg07rrhxqwpy4rvcpqp9cx4ndyh")))

