(define-module (crates-io ma gm magma_api) #:use-module (crates-io))

(define-public crate-magma_api-0.0.0 (c (n "magma_api") (v "0.0.0") (h "07ack9rq6q8zai1nqq406k5nmxxz6fajnm0007cskq3knxg3vy61") (y #t)))

(define-public crate-magma_api-0.1.0-alpha (c (n "magma_api") (v "0.1.0-alpha") (d (list (d (n "magma_app") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "magma_audio") (r "^0.1.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "magma_ui") (r "^0.1.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "magma_winit") (r "^0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "1ahwbpn8n3yav1cmfxc54p2wnvkcd64kn3rngbwa1r1j98jx8cfs") (f (quote (("default" "audio" "winit" "ui")))) (s 2) (e (quote (("winit" "dep:magma_winit") ("ui" "dep:magma_ui") ("audio" "dep:magma_audio"))))))

(define-public crate-magma_api-0.1.0-alpha.1 (c (n "magma_api") (v "0.1.0-alpha.1") (d (list (d (n "magma_app") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "magma_audio") (r "^0.1.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "magma_ui") (r "^0.1.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "magma_winit") (r "^0.1.0-alpha.5") (o #t) (d #t) (k 0)))) (h "13y6811vqldvk26dzijhcpk1a5fvbv7jnhkbk6mlvhk01vzaa629") (f (quote (("default" "audio" "winit" "ui")))) (s 2) (e (quote (("winit" "dep:magma_winit") ("ui" "dep:magma_ui") ("audio" "dep:magma_audio"))))))

