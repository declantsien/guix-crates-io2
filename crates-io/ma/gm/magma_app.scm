(define-module (crates-io ma gm magma_app) #:use-module (crates-io))

(define-public crate-magma_app-0.0.0 (c (n "magma_app") (v "0.0.0") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0cqr8l6dmdmmijm2cdgmqszd7wmchi5kkhjcqxfc9q53r6hifj7c") (y #t)))

(define-public crate-magma_app-0.1.0-alpha (c (n "magma_app") (v "0.1.0-alpha") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "19wqkn52axgfnxzm9yll4grdf7kc1v7avg8mi4v23dcf3bh42hm5")))

(define-public crate-magma_app-0.1.0-alpha.1 (c (n "magma_app") (v "0.1.0-alpha.1") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0bzmg012kh8wvg485gj3fwl5cq3f3w8s506lwbg6q78vjy6lgqsr")))

(define-public crate-magma_app-0.1.0-alpha.2 (c (n "magma_app") (v "0.1.0-alpha.2") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "1rjidawp54r6hda2mxqihb95aby7s19s7a4bir0hz894s3k4pqb2")))

(define-public crate-magma_app-0.1.0-alpha.3 (c (n "magma_app") (v "0.1.0-alpha.3") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "1gi2z0ww6fyrgc9bds07mv1s15blb3v7smp3v5vwinrh6zm983ib")))

(define-public crate-magma_app-0.1.0-alpha.4 (c (n "magma_app") (v "0.1.0-alpha.4") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "03ll8scdcgx11xjxqk3baawr9fwv8pv27msr7bjng0xvxg12lmv4")))

(define-public crate-magma_app-0.1.0-alpha.5 (c (n "magma_app") (v "0.1.0-alpha.5") (d (list (d (n "magma_ecs") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "1w8c4j6bbg3940g0iqiwvx5wnmy128bqhlpxkjgwsf2jcmdhp6v2")))

