(define-module (crates-io ma gm magma_winit) #:use-module (crates-io))

(define-public crate-magma_winit-0.1.0-alpha (c (n "magma_winit") (v "0.1.0-alpha") (d (list (d (n "magma_app") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "01f5l1lk8yfssxahdww0970hn0jdbdrc6jx9396s904cx0mlq58d")))

(define-public crate-magma_winit-0.1.0-alpha.1 (c (n "magma_winit") (v "0.1.0-alpha.1") (d (list (d (n "magma_app") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "045bpnvlsxhk95ac1afiadpfp4gri248jbh9cs2v4ilfzkwfbpqg")))

(define-public crate-magma_winit-0.1.0-alpha.2 (c (n "magma_winit") (v "0.1.0-alpha.2") (d (list (d (n "magma_app") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "00v5rhhdijghjj8x3vsy9ivx3kfvvw506hrmns6j44s4c03h50la")))

(define-public crate-magma_winit-0.1.0-alpha.3 (c (n "magma_winit") (v "0.1.0-alpha.3") (d (list (d (n "magma_app") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "1a7gvx3m9i0mpsc32dqwdjg60byv6gwj4bi59rxq4s1gabdyj6ik")))

(define-public crate-magma_winit-0.1.0-alpha.4 (c (n "magma_winit") (v "0.1.0-alpha.4") (d (list (d (n "magma_app") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "1gwl7gs214lfwax8snp33x62v6zn2flfhs6ps1h8w19hr0kbmliv")))

(define-public crate-magma_winit-0.1.0-alpha.5 (c (n "magma_winit") (v "0.1.0-alpha.5") (d (list (d (n "magma_app") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 0)))) (h "0h5lr23iyri2ag5wpi96w4k3cda7rs1wdk8yqpbmy6jg2v2lani9")))

