(define-module (crates-io ma gm magma_ecs) #:use-module (crates-io))

(define-public crate-magma_ecs-0.1.0-alpha (c (n "magma_ecs") (v "0.1.0-alpha") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1rinl2jamh7v8g6m6n9kxc853mpp5j6286gzyr9i70pg36b9s4pn") (y #t)))

(define-public crate-magma_ecs-0.1.0-alpha.1 (c (n "magma_ecs") (v "0.1.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0msav1dgnx0cy58gm775lsg3mlzajim0jz037882x9zb3qf3d1cr")))

(define-public crate-magma_ecs-0.1.0-alpha.2 (c (n "magma_ecs") (v "0.1.0-alpha.2") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0y2l2amnpiws3iv35fpr1nasxx0fqgrbpxrc0lrlf2j4b3j2nwmf")))

(define-public crate-magma_ecs-0.1.0-alpha.3 (c (n "magma_ecs") (v "0.1.0-alpha.3") (h "128xasxvyiac8pnc7810y11ldxjmh0ysircj6pmsdq177xlwwd6q")))

