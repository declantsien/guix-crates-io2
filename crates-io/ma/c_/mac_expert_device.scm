(define-module (crates-io ma c_ mac_expert_device) #:use-module (crates-io))

(define-public crate-mac_expert_device-0.1.0 (c (n "mac_expert_device") (v "0.1.0") (h "1y4lbjq4cv2xz0shjzvjn2szdqk8b4gia0z29pp9ygx4gynsmp4l") (y #t)))

(define-public crate-mac_expert_device-0.1.2 (c (n "mac_expert_device") (v "0.1.2") (h "1jazxbzvv9s9d7mh5zrrqkjln2lrdb8vvkh8nd94qpqpadzfadia") (y #t)))

(define-public crate-mac_expert_device-0.1.3 (c (n "mac_expert_device") (v "0.1.3") (h "02a4zd63f02khv5vqnzm79mbqa5189j549l1mvj6vq1hlyjqzka1") (y #t)))

(define-public crate-mac_expert_device-0.1.4 (c (n "mac_expert_device") (v "0.1.4") (h "1d3vl2hlvrkpp24d3ldygy2ngpm5bm8a5i75yr62w0ilb1bs53r3") (y #t)))

(define-public crate-mac_expert_device-0.1.5 (c (n "mac_expert_device") (v "0.1.5") (h "0qhrb74r086vpfyw3jyb0cnmvy81m38xlaw6zl17kgv5a0fmv9b2") (y #t)))

(define-public crate-mac_expert_device-0.1.6 (c (n "mac_expert_device") (v "0.1.6") (h "1319lif2qgjyi69l636fm2h81jx6l1mxpjm3dhrld8nhpalv5wsi") (y #t)))

(define-public crate-mac_expert_device-0.1.7 (c (n "mac_expert_device") (v "0.1.7") (h "08rdwmfh5yvgkr1rnirkm10h4kv9d48jn1d32ydhxrw0pc095mif") (y #t)))

(define-public crate-mac_expert_device-0.1.8 (c (n "mac_expert_device") (v "0.1.8") (h "097v4j8h4w9g9kqc13q5sfm1bx67vjngkhqmm8y294w5g0agj1pa")))

