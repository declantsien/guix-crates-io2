(define-module (crates-io ma c_ mac_proxy) #:use-module (crates-io))

(define-public crate-mac_proxy-0.1.0 (c (n "mac_proxy") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vf1ylhjscwqf8kp2q36nd93a73m6ll2v0lfnipx93va047aqp0a")))

