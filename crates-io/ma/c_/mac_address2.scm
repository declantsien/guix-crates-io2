(define-module (crates-io ma c_ mac_address2) #:use-module (crates-io))

(define-public crate-mac_address2-1.1.5 (c (n "mac_address2") (v "1.1.5") (d (list (d (n "nix") (r "^0.26") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"android\"))") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "widestring") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0klf5zjhzzrn0r5qgf8xikpdwmdgdk13rmwyssx0i2n64khj0r9a")))

(define-public crate-mac_address2-2.0.0 (c (n "mac_address2") (v "2.0.0") (d (list (d (n "nix") (r "^0.26") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"android\"))") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q7pwlqs1pv2f22q0xgqcfmy559h08qx27arw9achn4484hpgfkq")))

(define-public crate-mac_address2-2.0.1 (c (n "mac_address2") (v "2.0.1") (d (list (d (n "nix") (r "^0.26") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"ios\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"android\"))") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f2dccp4ljp23pyw2lqdbwx7h0s41c6zmgb9r7vgxhb9yrs2317a")))

(define-public crate-mac_address2-2.0.2 (c (n "mac_address2") (v "2.0.2") (d (list (d (n "nix") (r "^0.28") (f (quote ("net"))) (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"ios\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"android\"))") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02ybx2fx74s2j3vn53jnz43gy09jnrhbyryadc3rdp5476m08pnw")))

