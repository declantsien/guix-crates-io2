(define-module (crates-io ma c_ mac_utun) #:use-module (crates-io))

(define-public crate-mac_utun-0.1.0 (c (n "mac_utun") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0w8jmdy64b8p5g5anqgvxll1zwm5p6i68v5nmhrr21r8wwxqccxl")))

(define-public crate-mac_utun-0.1.1 (c (n "mac_utun") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1z1b5is3rpppjamvqbj9i282f0hancdp31bdz900rb8j6zx1a87z")))

(define-public crate-mac_utun-0.2.0 (c (n "mac_utun") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1bkj8f7cs2cdibivgfgh84s5m24a0rp5ydv1arb6ps542srz3pj0")))

(define-public crate-mac_utun-0.3.0 (c (n "mac_utun") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "15vxv355gcnl4ldcwd3xp7ndn1ppwjz0y99y27dl12g6nrsx8szj")))

(define-public crate-mac_utun-0.4.0 (c (n "mac_utun") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0j76wjvwrpsv4mvqchjjmm1kl15blcznrnaf1dfxs4whxp1z2wix")))

(define-public crate-mac_utun-0.5.0 (c (n "mac_utun") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1jb14q8dakynyaplifswqfvaj8i1n77vp2irn97a9qkps8i1r9r2")))

(define-public crate-mac_utun-0.5.1 (c (n "mac_utun") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "0812qwrjpy1993rhsm789fgrbcbrnx12wgs551h5pd9pfxb363yf")))

(define-public crate-mac_utun-0.5.2 (c (n "mac_utun") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1102xs203ikkp5vzqm22w0qbiwql63ayc8wm5a4nxphkaz87wdpr")))

(define-public crate-mac_utun-0.6.0 (c (n "mac_utun") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "060f3z5k9p8xvbdz548jjh9lbswrqy09gzc4sfppryv20r4qjsjz")))

