(define-module (crates-io ma c_ mac_conditions) #:use-module (crates-io))

(define-public crate-mac_conditions-1.0.0 (c (n "mac_conditions") (v "1.0.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0m103mklfc08aca9g3xjhanzq5c1slmskrw5wgs613d0iaskw3g3")))

