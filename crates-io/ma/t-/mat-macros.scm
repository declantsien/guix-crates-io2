(define-module (crates-io ma t- mat-macros) #:use-module (crates-io))

(define-public crate-mat-macros-0.1.0 (c (n "mat-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0zdgp426sida98rckhysvkb5vplyk0aaw35rvkc5bws5n0bs2wdi")))

(define-public crate-mat-macros-0.1.1 (c (n "mat-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wcb3raxfs1v0ipmgv694iiyqgy87hgvc8ys66ba6i106hq506wz")))

