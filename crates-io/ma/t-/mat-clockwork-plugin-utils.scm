(define-module (crates-io ma t- mat-clockwork-plugin-utils) #:use-module (crates-io))

(define-public crate-mat-clockwork-plugin-utils-2.0.41 (c (n "mat-clockwork-plugin-utils") (v "2.0.41") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.14.16") (d #t) (k 0)))) (h "1vipxw98jljcglbsc8id1ly3gig11z6dywlq998y691mhhmfx1bd")))

(define-public crate-mat-clockwork-plugin-utils-2.0.42 (c (n "mat-clockwork-plugin-utils") (v "2.0.42") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.14.16") (d #t) (k 0)))) (h "1i93i0qijym0p21998aj7qiac7jps2p6iapxwnl5xzgmc94dxk51")))

