(define-module (crates-io ma t- mat-clockwork-utils) #:use-module (crates-io))

(define-public crate-mat-clockwork-utils-2.0.2 (c (n "mat-clockwork-utils") (v "2.0.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "04bdf8zm4b0qr5j2qpw7gjxfn590527sjlxvjrdxjkbym00kmvrr") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.5 (c (n "mat-clockwork-utils") (v "2.0.5") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1pbzfm4j850w10y0fgfqmffyl20q5jk6lq96yaxlm2nvdh9lbm97") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.6 (c (n "mat-clockwork-utils") (v "2.0.6") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "19xny8h4yx1fi4mn68vxwxdbdwz6p2w88i56507fsfq58hngl761") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.7 (c (n "mat-clockwork-utils") (v "2.0.7") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "043n1nl6rnfqdzlcnfvmsm3jlyhak83cy9kdsj701pznlqyydknq") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.8 (c (n "mat-clockwork-utils") (v "2.0.8") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "076ssgnqkiv2shih52f4zridpvk45x33cxcl9w3g0yrvba16xa9h") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.9 (c (n "mat-clockwork-utils") (v "2.0.9") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1hqiicglragjvassp4sf0pcbkqmdpqmxsg0bas4hc08wjpxihw85") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.7 (c (n "mat-clockwork-utils") (v "2.8.7") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "11gcrkc54w5brbj8rw8g104852daj57mpgr8qq51djnb8gm3n4ih") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.9 (c (n "mat-clockwork-utils") (v "2.8.9") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "05ah85harl659j821is27w3qp0aans804xr1vs0wlrg82hg2cvil") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.10 (c (n "mat-clockwork-utils") (v "2.8.10") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0jvkrysfzg440kn3l6zaa7aimjcwp485nq1xhv1wnr7fb21jpvyz") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.13 (c (n "mat-clockwork-utils") (v "2.8.13") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "08jqciqiwc9awlz8nq54nzp8s22pz43f25vp2rsj0b1rf969ihdv") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.15 (c (n "mat-clockwork-utils") (v "2.8.15") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1grr6kdacm257irl2dg7cx6abglpz4yrllw8q4bd0hkn4a448a06") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.17 (c (n "mat-clockwork-utils") (v "2.8.17") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0hkx3m1s06cz3sd0bvlkfwhws5v860gzx2vy6v53m8d7jp834fgf") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.20 (c (n "mat-clockwork-utils") (v "2.8.20") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "18wsnl0p2xiv0a27wwfrgkln0q186fhkv1fs1i9azs8n29i92pm3") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.29 (c (n "mat-clockwork-utils") (v "2.8.29") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0lak4i5n2j9zw65yg0ishhy5xmdf5c956h5jdg965pyd06f4fr33") (y #t)))

(define-public crate-mat-clockwork-utils-2.8.8 (c (n "mat-clockwork-utils") (v "2.8.8") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0s1p1xqixwqyj6i9bbh292aq456r0dl0vhkhgcpnzkw53mh6g6pp") (y #t)))

(define-public crate-mat-clockwork-utils-2.9.0 (c (n "mat-clockwork-utils") (v "2.9.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0x45dvki7r4vb35lmp694k0d05603yzrl15v3i41c4ahxl3k93y8") (y #t)))

(define-public crate-mat-clockwork-utils-2.18.0 (c (n "mat-clockwork-utils") (v "2.18.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1vbpzzqb78wvxm6f86n36cq61vn4synh28ky41zx7vwm0g8yig5r") (y #t)))

(define-public crate-mat-clockwork-utils-2.21.0 (c (n "mat-clockwork-utils") (v "2.21.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0qd6922mwa3wkk4bwm573w45j2vbbx107v0wjj5xj359l1b28gc1") (y #t)))

(define-public crate-mat-clockwork-utils-2.10.0 (c (n "mat-clockwork-utils") (v "2.10.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1d8ny33j979xdci92nqzkc7d5kphcdmq4inqi0kn97j5a594q5zk") (y #t)))

(define-public crate-mat-clockwork-utils-2.39.0 (c (n "mat-clockwork-utils") (v "2.39.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1x4qxs1grjacjywawf1qs0kay5q467cy8gn2am8kwad6a5761jda") (y #t)))

(define-public crate-mat-clockwork-utils-2.40.0 (c (n "mat-clockwork-utils") (v "2.40.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0xsss4gm0dmslay5hhfacyqa0mr6rz2mkk0m64f1v8639b3jy1i8") (y #t)))

(define-public crate-mat-clockwork-utils-2.41.0 (c (n "mat-clockwork-utils") (v "2.41.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "15m7f53zvmd9wbxy59k0jrkrv1h5l7s5apax87yp53ziqrppcyqs") (y #t)))

(define-public crate-mat-clockwork-utils-2.42.0 (c (n "mat-clockwork-utils") (v "2.42.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1iwjw7ggh90j45i7jg8sqvxww8i0ribk2na2cjwrf3iplzc20h0c") (y #t)))

(define-public crate-mat-clockwork-utils-2.44.0 (c (n "mat-clockwork-utils") (v "2.44.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1hmwn6hpz4bd5ypzlr22c8m37xbasjdgv19rb94p12xrlb7aj14q") (y #t)))

(define-public crate-mat-clockwork-utils-2.45.0 (c (n "mat-clockwork-utils") (v "2.45.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1yppfb2fg45krv0npy7mrnlak6bzqfzx58nz1ynv2sv7352lhv3i") (y #t)))

(define-public crate-mat-clockwork-utils-2.46.0 (c (n "mat-clockwork-utils") (v "2.46.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1x0n1y9rrvmrf64w4grgci82kfx64pkhw1qgbhdj6nsv6srdphwr") (y #t)))

(define-public crate-mat-clockwork-utils-3.0.0 (c (n "mat-clockwork-utils") (v "3.0.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "04mlf69wayy40y3i61xjvdpkihq2k8jrxh0nv468zkpn9qdc24dj") (y #t)))

(define-public crate-mat-clockwork-utils-4.0.0 (c (n "mat-clockwork-utils") (v "4.0.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0pp39v9790bkayw8s42rbq5dgsrx63fca47ilflc4gd11zrcl4hr") (y #t)))

(define-public crate-mat-clockwork-utils-5.0.0 (c (n "mat-clockwork-utils") (v "5.0.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "01m2sm62pk63lf31wa85bm6sr3jz2c4fc1z4a0kxb3c3h2xlbjj8") (y #t)))

(define-public crate-mat-clockwork-utils-5.1.0 (c (n "mat-clockwork-utils") (v "5.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0lhrd4b2zwy8nlyc0471d11sq1h8y6vi87i32z9xwiz8m84xci48") (y #t)))

(define-public crate-mat-clockwork-utils-5.4.0 (c (n "mat-clockwork-utils") (v "5.4.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "11si8nmxav8hqk1vxg6rqj4pagrp32xxq2n29fjndlzl3qs1jvg9") (y #t)))

(define-public crate-mat-clockwork-utils-5.5.0 (c (n "mat-clockwork-utils") (v "5.5.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0h649mv2baks1aj9br82fnpky1ivhag1iif5k0zi3aqgdac5ywlr") (y #t)))

(define-public crate-mat-clockwork-utils-5.6.0 (c (n "mat-clockwork-utils") (v "5.6.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1mhq29xqkiwa2ca6wmj6akganb1c2fqzwr7vsm196h0qs54sf84z") (y #t)))

(define-public crate-mat-clockwork-utils-5.7.0 (c (n "mat-clockwork-utils") (v "5.7.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1r5af6v52j19h3qky0gb2h025vza10kn4vvhqj7k2anwimy59fwx") (y #t)))

(define-public crate-mat-clockwork-utils-5.8.0 (c (n "mat-clockwork-utils") (v "5.8.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "06y0sj3s0nzm3zsam7a5fak6b4dj8s72iiqx7y4dhk3i8g8y2bxi") (y #t)))

(define-public crate-mat-clockwork-utils-5.9.0 (c (n "mat-clockwork-utils") (v "5.9.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0qc822fxa9a79ha93islnfi63irnl0ihqj1i7l22n8zzfnb9x8c2") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.16 (c (n "mat-clockwork-utils") (v "2.0.16") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0ig3j2clx6z3vi441f1dmiw7gvmy11h8sr6mlq7nrajp6wf2cya3") (y #t)))

(define-public crate-mat-clockwork-utils-2.1.0 (c (n "mat-clockwork-utils") (v "2.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1jqr5c0667288d484mmfjx10f99g9k4xkhb8icdjylkvbp0y0473")))

(define-public crate-mat-clockwork-utils-2.0.17 (c (n "mat-clockwork-utils") (v "2.0.17") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "13agn4snkd3lmza7466dpgj0zcyh9dsznviax8wrr3v5lsl8rfg6") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.18 (c (n "mat-clockwork-utils") (v "2.0.18") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "06pb32kbj08f749xrqgg1yjd9z1l1bynn3yhgdlpbdfxffm6pbzw") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.19 (c (n "mat-clockwork-utils") (v "2.0.19") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1jd2swfvl9jf6kc9amxs3fccdkhq2609v0lc5z7l1z2962z2jm7i") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.23 (c (n "mat-clockwork-utils") (v "2.0.23") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1arn230f0nrw3xxqqaqzfw267cchb1c3fq9q8ipz1yy80fym7hd3") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.24 (c (n "mat-clockwork-utils") (v "2.0.24") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0p1p7gqkp2cnyyhr245kyyc3ayq8qb5f9gvnc78fms12np0mfqkg") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.27 (c (n "mat-clockwork-utils") (v "2.0.27") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "14kwimd2fj497l9m3qhnhqsrfz3hhnkinqp87yn16x4d8fgq6zlr") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.30 (c (n "mat-clockwork-utils") (v "2.0.30") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1zv09d70xl4mv6nnlb7468mg0886l0a3cvaj01b5shy2hd7xd6z8") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.37 (c (n "mat-clockwork-utils") (v "2.0.37") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0h5pml135h1yzg8jhyblv20s6hpym312mzsb6359dwrn1msq6sm5") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.38 (c (n "mat-clockwork-utils") (v "2.0.38") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1n0y5s4kqwdynp038s6n4b28x5dvz98r978hx4n6f835ggz49l2y") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.39 (c (n "mat-clockwork-utils") (v "2.0.39") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0z15icfqg36aqrrhpqvkbj391f5s8qfkbk2cgyksr8j3c0r21pcq") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.40 (c (n "mat-clockwork-utils") (v "2.0.40") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0apsjk8wyxm1w4qsf605l4gyxllvnjn981k6s9171rj8kjc8mz7n") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.41 (c (n "mat-clockwork-utils") (v "2.0.41") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "07f1j2rllwbz45lq006xsa3gsz27qb1ib75wmpynly90slfr4043") (y #t)))

(define-public crate-mat-clockwork-utils-2.0.42 (c (n "mat-clockwork-utils") (v "2.0.42") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "03q71xynly8zzd7sbbqr1wm2phjqab46sv4iq9gj2yjyjq5bj7am") (y #t)))

