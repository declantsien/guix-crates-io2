(define-module (crates-io ma t- mat-clockwork-relayer-api) #:use-module (crates-io))

(define-public crate-mat-clockwork-relayer-api-2.0.2 (c (n "mat-clockwork-relayer-api") (v "2.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.15") (d #t) (k 0)))) (h "1f67jr3lszc3f1azyrn7annlqmd6fjfdnwb030aa398b0pvha2yq") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.21.0 (c (n "mat-clockwork-relayer-api") (v "2.21.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "1gzf7p5xqfvl99d6ymswxvbyw3y7dnd7jlk9h3261c4i9nvnb76p") (y #t)))

(define-public crate-mat-clockwork-relayer-api-3.0.0 (c (n "mat-clockwork-relayer-api") (v "3.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0317d692w5kp7bkshw3blmgydcm86byl5k7my1kq1m9lsz38r2zq") (y #t)))

(define-public crate-mat-clockwork-relayer-api-4.0.0 (c (n "mat-clockwork-relayer-api") (v "4.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1w6zwxfmswn35y2j8pz5myswzb994x3kqxf2zh9fnbpf2wxsrkwv") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.0.0 (c (n "mat-clockwork-relayer-api") (v "5.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1cs4bch98nvxknhnliv7q8fdww7jsjzh48fhr89ngc58jn5cbii5") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.1.0 (c (n "mat-clockwork-relayer-api") (v "5.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1hp8ladnkpskpabr58pmbmn6k9131f47c4c20vw5glnf637pviz1") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.4.0 (c (n "mat-clockwork-relayer-api") (v "5.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1m39sx9l430zk105fss3jmk0v37aax3swprhh542w65pjspdnkgv") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.5.0 (c (n "mat-clockwork-relayer-api") (v "5.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0sk2xzck9r6ypv9s7zix83higpnj7m6xmydcmnd8pmzd5606ccfd") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.7.0 (c (n "mat-clockwork-relayer-api") (v "5.7.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1qkr5bvsjrv41aqm9f1vrla4zfs2gsq827l0yn66cg3q7c1bcln8") (y #t)))

(define-public crate-mat-clockwork-relayer-api-5.9.0 (c (n "mat-clockwork-relayer-api") (v "5.9.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "19riinb8gdcs47x5ks67h5w0cwjw5ngv0zjy8q7br2wky2rm4l2g") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.16 (c (n "mat-clockwork-relayer-api") (v "2.0.16") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0mx0jslgx20kshw1527nzbi1h49ykj1g07ng2scn5ic678v21qh2") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.1.0 (c (n "mat-clockwork-relayer-api") (v "2.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1l11mwbm0r9q5yx18f2ld4csadh783ranydvc59qj7scnnwa1ahg")))

(define-public crate-mat-clockwork-relayer-api-2.0.17 (c (n "mat-clockwork-relayer-api") (v "2.0.17") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0rwfsh8b6i7x0336mqf2hjc6w4wylh0c3ga7b4qimwf08mv99pl2") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.18 (c (n "mat-clockwork-relayer-api") (v "2.0.18") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0zihdk65ym4gzp43pbn53kc6cyzmg86cfqa3p52db9xa7024zdq4") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.19 (c (n "mat-clockwork-relayer-api") (v "2.0.19") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0vvyan2zw732dqy4xc02bnjw9palpf5wnfx03hjpr29pb92i6nb5") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.23 (c (n "mat-clockwork-relayer-api") (v "2.0.23") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "04b3wc35037bvsamwniaz926isrkbx085cj7x2vxk95sy1hl5nd3") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.24 (c (n "mat-clockwork-relayer-api") (v "2.0.24") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1jkdhjybg3mca00c3ijvq6j1q00rdp59d5sqnyxds3z2a629y58y") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.27 (c (n "mat-clockwork-relayer-api") (v "2.0.27") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0lcm9p90vd1dxkbfivhv4fw22d1wx78j411qviw26b7mvmi0h4ab") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.30 (c (n "mat-clockwork-relayer-api") (v "2.0.30") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "06b4yrr7qpnimpdkmgm8axmgnkai2v9hlgdz4cb2sysp5xwal1x7") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.37 (c (n "mat-clockwork-relayer-api") (v "2.0.37") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0pxr2r48v9mz13vmvx71ydda6cqidhdnkwlpl45djrznqp2v43iw") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.38 (c (n "mat-clockwork-relayer-api") (v "2.0.38") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "0yqxmr552r2yri0k97iqwnbpkpp58wrrgg7wlb2m1gv6ajrpviin") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.39 (c (n "mat-clockwork-relayer-api") (v "2.0.39") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "1kc3y9mriifkclrhlbsk0s77jb9mz4haa5vlkhsbrd0sm5s5a2my") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.40 (c (n "mat-clockwork-relayer-api") (v "2.0.40") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "028d89mwrd4shpx5sn0iw9jjfldblhbmyiv2azmqmc4vwzq9wbjl") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.41 (c (n "mat-clockwork-relayer-api") (v "2.0.41") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "05wx8gzraasmrr70f735yx89rn1xdxr0haixdrmggf95n4x6k12p") (y #t)))

(define-public crate-mat-clockwork-relayer-api-2.0.42 (c (n "mat-clockwork-relayer-api") (v "2.0.42") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.14.16") (d #t) (k 0)))) (h "01i0hjj115zid517yb1fyv39wbv6pbs7q5icpqanwgjxg5jb6n42") (y #t)))

