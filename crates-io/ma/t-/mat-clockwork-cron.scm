(define-module (crates-io ma t- mat-clockwork-cron) #:use-module (crates-io))

(define-public crate-mat-clockwork-cron-2.0.2 (c (n "mat-clockwork-cron") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1rdk8lyjb6y3mglcw6glksx7rj54isdli543b1q3ryi981l8f54h") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.5 (c (n "mat-clockwork-cron") (v "2.0.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "059zh0fiji2vxz5g1h72vms0zfzczr2hikyhqn8sd5f25kl45w39") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.6 (c (n "mat-clockwork-cron") (v "2.0.6") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0nb7f8g98frv3m7gd1vw54mlmp8w155w6glrzni1f18vm5s9gb8c") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.7 (c (n "mat-clockwork-cron") (v "2.0.7") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "07qp14w1q38nxmsdwwrpc676hfsir3kg770xqp64yljrnc4xabbh") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.8 (c (n "mat-clockwork-cron") (v "2.0.8") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0w5a0vjkz3k48kg0fkawcx9m3xy9q5qr83ziz8dz2xsxc2dikg23") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.9 (c (n "mat-clockwork-cron") (v "2.0.9") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "02b26d4rmmycw6hm93jm1zwshzssy1316yjjs5va8da8k4syl0zn") (y #t)))

(define-public crate-mat-clockwork-cron-2.8.7 (c (n "mat-clockwork-cron") (v "2.8.7") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "03y068kcxfzbq02mcx0pvzr04zhy8zl2jprm63kinn64asqd5xqv") (y #t)))

(define-public crate-mat-clockwork-cron-2.8.10 (c (n "mat-clockwork-cron") (v "2.8.10") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1q4vamynxflp11vc72093zn0qb6qrq2bsh6z34c9rxlxbmib4k8r") (y #t)))

(define-public crate-mat-clockwork-cron-2.8.29 (c (n "mat-clockwork-cron") (v "2.8.29") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0fj7y0x0lyg7f71n51g62260b0bb6nmdrqw6vh1frmz1v5d5w4ix") (y #t)))

(define-public crate-mat-clockwork-cron-2.8.8 (c (n "mat-clockwork-cron") (v "2.8.8") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1q1zsd9g4kg9vdls0dydb99sdfdy6v874l205rkzqifhfswdkw01") (y #t)))

(define-public crate-mat-clockwork-cron-2.8.20 (c (n "mat-clockwork-cron") (v "2.8.20") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1qvp9byvd6div2hzdv8qzi9pmhb8da7i6xmgfl0yayszvjzi1vga") (y #t)))

(define-public crate-mat-clockwork-cron-2.9.0 (c (n "mat-clockwork-cron") (v "2.9.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0xlzrv7cwm3fjdrmdcdflihqpnjrs5a5ji9wndwm0d7mvbw9x3s7") (y #t)))

(define-public crate-mat-clockwork-cron-2.18.0 (c (n "mat-clockwork-cron") (v "2.18.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0y8nlzc165cx02cylyci0v78662pa7bxdscb643drmsv3i1bvi9k") (y #t)))

(define-public crate-mat-clockwork-cron-2.21.0 (c (n "mat-clockwork-cron") (v "2.21.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1jyazp8i610bi7is6ss46wkhmkrljgljx7yflsrpkgq925jmil1s") (y #t)))

(define-public crate-mat-clockwork-cron-2.10.0 (c (n "mat-clockwork-cron") (v "2.10.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "01j17sznvl2yi34h999psdfzmrbd450c29widd4v0zyg4058xyqi") (y #t)))

(define-public crate-mat-clockwork-cron-2.39.0 (c (n "mat-clockwork-cron") (v "2.39.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1hlxsq2ls77sv535g7lkyx7qaifbc8zrjxcnjvcydlkyl3iykjrf") (y #t)))

(define-public crate-mat-clockwork-cron-2.40.0 (c (n "mat-clockwork-cron") (v "2.40.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "09galvp1cbwljwjy2vqrw9c98gqxrpqmxknjp5k6lp1xna2xn3fy") (y #t)))

(define-public crate-mat-clockwork-cron-2.41.0 (c (n "mat-clockwork-cron") (v "2.41.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0csya374b818kfbkc180ald8047s9r272yz9q5plsybfk15ckyzg") (y #t)))

(define-public crate-mat-clockwork-cron-2.42.0 (c (n "mat-clockwork-cron") (v "2.42.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0xq1llgxxg13cjgl9vz23p9zy2wyhd1z3q2i6sscgkm0mf36vs7h") (y #t)))

(define-public crate-mat-clockwork-cron-2.44.0 (c (n "mat-clockwork-cron") (v "2.44.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1npsqp6sxw2jbflnkk36j9l2v6cg9p52zg8rxih3mm6wb0mdrngd") (y #t)))

(define-public crate-mat-clockwork-cron-2.45.0 (c (n "mat-clockwork-cron") (v "2.45.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0dh7855bksh480i0k3nz8syk0f4qpsi8av9s7ng4pljdjf87xwvc") (y #t)))

(define-public crate-mat-clockwork-cron-2.46.0 (c (n "mat-clockwork-cron") (v "2.46.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0wwyqwq09dg23266szyvp5hfakms6gaczn95739adrf2wn35v8lf") (y #t)))

(define-public crate-mat-clockwork-cron-3.0.0 (c (n "mat-clockwork-cron") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0mcafqkiiwd928l9f7kyrjnvrysd8m4ipr33cgsyhxvhqw23i46z") (y #t)))

(define-public crate-mat-clockwork-cron-4.0.0 (c (n "mat-clockwork-cron") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "17yd0qrccxaqkc4gxsjnwazhninwpsc38rjxadr9365m1z300rab") (y #t)))

(define-public crate-mat-clockwork-cron-5.0.0 (c (n "mat-clockwork-cron") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1kz4yybi83lix6fsz2d7j0g3md2k45k1a7yw41q3z7n7cyg2ymjp") (y #t)))

(define-public crate-mat-clockwork-cron-5.1.0 (c (n "mat-clockwork-cron") (v "5.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0fczjmaqgp9qz6qc32r0094mk9vzx09gr700wc41wq55nmlxiik4") (y #t)))

(define-public crate-mat-clockwork-cron-5.4.0 (c (n "mat-clockwork-cron") (v "5.4.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1ks9ixn0r01d9h22jf5la4ip4h60djkjyxdmbqgcza7yvcn6gm5m") (y #t)))

(define-public crate-mat-clockwork-cron-5.5.0 (c (n "mat-clockwork-cron") (v "5.5.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1zpgw9sjln148g5v1qr8jph8g9qnd7s18jqdb2hpgyi0gnv2a0s3") (y #t)))

(define-public crate-mat-clockwork-cron-5.6.0 (c (n "mat-clockwork-cron") (v "5.6.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0v5wd6fxlanbxp4px1rhxqxbfrv4dcwrdda4dfjn2hrmzi2l8l15") (y #t)))

(define-public crate-mat-clockwork-cron-5.7.0 (c (n "mat-clockwork-cron") (v "5.7.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0pwfh3q4riysg6bacbdv0xi7jj9gpz2njd2ranvbbqa4rx2k4mwj") (y #t)))

(define-public crate-mat-clockwork-cron-5.9.0 (c (n "mat-clockwork-cron") (v "5.9.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "143mdwjpb15qz7drmndxa353imnk0qp7lqykwi1ksz4nw90zab8w") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.16 (c (n "mat-clockwork-cron") (v "2.0.16") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0mn9q6366nb7zgp8kf6sb71bibyz7qpcg4b1nwnkidcc0m540pwl") (y #t)))

(define-public crate-mat-clockwork-cron-2.1.0 (c (n "mat-clockwork-cron") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1nvgxws6jrkkn8bfv6man7sq4ihbqizrvdf16043hwqqhy0d3jwc")))

(define-public crate-mat-clockwork-cron-2.0.17 (c (n "mat-clockwork-cron") (v "2.0.17") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0x9jj30pqjdhm7yw7la4yzp7i6xgfn000lm9h81riybg6cq7r88v") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.18 (c (n "mat-clockwork-cron") (v "2.0.18") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1v6l5lm6ahxzbsm5mkqxic3wjqcb9s61fbb4r4v5isf4bj2yvpzn") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.19 (c (n "mat-clockwork-cron") (v "2.0.19") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "10m67xjjcj14v3k9axsfclwc9jl66vmmslq17if23swir05k35s7") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.23 (c (n "mat-clockwork-cron") (v "2.0.23") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0mp2ikzz8p4b0pizrlfspgj85pjpl2d1zw31l49w2kpcr6pxmv5l") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.24 (c (n "mat-clockwork-cron") (v "2.0.24") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1sadmhfmc4xvwk5bbzzfzjpnmmwz56pmg09fhqivpw8qg33f29wr") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.27 (c (n "mat-clockwork-cron") (v "2.0.27") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1bn2c7kc565qpngx62n8wk9n2ibd8g35sz0x9ysmly93kagzqc00") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.30 (c (n "mat-clockwork-cron") (v "2.0.30") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1y37my21cq7sg76ipagdnd3hjc1c29gqjs2p7k4dc6gk0bdslxbp") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.37 (c (n "mat-clockwork-cron") (v "2.0.37") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1yp5wj8dvd9r1h7pg8fwl246i9q7nc1rgvk0c8i2f0m8h0w5075b") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.38 (c (n "mat-clockwork-cron") (v "2.0.38") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1nv0c7xbn7y797hj3nvi1d4lnihdb3j9q9xwjckl6va3szyccf13") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.39 (c (n "mat-clockwork-cron") (v "2.0.39") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0c5c1al9nvngxpl1n1dlncgmps77nf5xsmrzzrjzd4wqrsq0bm31") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.40 (c (n "mat-clockwork-cron") (v "2.0.40") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1rcs8dd5ikw22hnnjzspbf39kzhb6qj635dpvk9pybl3ci664zkr") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.41 (c (n "mat-clockwork-cron") (v "2.0.41") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1sqn6qspq9ka26jwrbyszd4s6fk04qx093rz45s84i47q6m5v1gb") (y #t)))

(define-public crate-mat-clockwork-cron-2.0.42 (c (n "mat-clockwork-cron") (v "2.0.42") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "00kkrhkgdk71ckcdm992yxif4r7pw4p8cnh0hl5lbqv4c2iwihyl") (y #t)))

