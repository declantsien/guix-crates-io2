(define-module (crates-io ma ny many-macros) #:use-module (crates-io))

(define-public crate-many-macros-0.1.0 (c (n "many-macros") (v "0.1.0") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "15z4ccy711c2sv7nrkfbdv0fmrw1i2xw4r122mipnyb2y3zgifky")))

