(define-module (crates-io ma ny manyhow-macros) #:use-module (crates-io))

(define-public crate-manyhow-macros-0.1.0 (c (n "manyhow-macros") (v "0.1.0") (d (list (d (n "proc-macro-utils") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0xriv5nan91j43wklnv3nnbayryhlxjnlshgycz6in502vr72f2p")))

(define-public crate-manyhow-macros-0.1.1 (c (n "manyhow-macros") (v "0.1.1") (d (list (d (n "proc-macro-utils") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0cdv5a7hz5k3csjbrc73zbgaw31jrrxrc2vlp9nxqirym2ln08rx")))

(define-public crate-manyhow-macros-0.1.2 (c (n "manyhow-macros") (v "0.1.2") (d (list (d (n "proc-macro-utils") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1bwi6122zhnwlh6ji27qb3ac48zg8hqfgr1y1wjjb0rnlq3c0gck")))

(define-public crate-manyhow-macros-0.4.1 (c (n "manyhow-macros") (v "0.4.1") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "08497j6ygmbnbzr20s001i7rzhl6vip3975vrlkny0g1wjvr25qj")))

(define-public crate-manyhow-macros-0.4.2 (c (n "manyhow-macros") (v "0.4.2") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "10hw6dj2gqkg10wi03i63zh2i0sr22iqx5hb633m5j2n6x0k6ag9")))

(define-public crate-manyhow-macros-0.5.0 (c (n "manyhow-macros") (v "0.5.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0fzlbl52s3qz8ndq5yjl8z3kgljayv0cg2h7b1iscp09zzzsd2da")))

(define-public crate-manyhow-macros-0.5.1 (c (n "manyhow-macros") (v "0.5.1") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1lc1wz0i4lg7v8r3047sr7f7pqhakc9s3x0mzjban9xfgf4rjxzp")))

(define-public crate-manyhow-macros-0.6.0 (c (n "manyhow-macros") (v "0.6.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1hkjxns1bbxi77344pvghvlg40r29fbi0667xzmm2x8j3z19a9zn")))

(define-public crate-manyhow-macros-0.7.0 (c (n "manyhow-macros") (v "0.7.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "01gq915pjkx011y7kmzcxk1px2arad885fxd9nvd5h75i60s8zj4")))

(define-public crate-manyhow-macros-0.8.0 (c (n "manyhow-macros") (v "0.8.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "13wpw57nh7qxwz1h8nqbj7gb9r8l475d8w061fjif2ywsfdawb1d")))

(define-public crate-manyhow-macros-0.8.1 (c (n "manyhow-macros") (v "0.8.1") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1w8c5yxclj4fxgi9d028fhwr1y7i24rqjw1f4dh33pnsxb07584b")))

(define-public crate-manyhow-macros-0.9.0 (c (n "manyhow-macros") (v "0.9.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "014d7lwawfvgcr3b0py0cxjsdpp78hkqmiqij28pwnmhv2hchk7p")))

(define-public crate-manyhow-macros-0.10.0 (c (n "manyhow-macros") (v "0.10.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0apsqg6ny2vnvh5bzwbz4r155r9hgs1wg9g5dar0wrbljich9vpw")))

(define-public crate-manyhow-macros-0.10.1 (c (n "manyhow-macros") (v "0.10.1") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0kfb61k1vbyxg3r7rcrdh3qiys5brc4znzc8hhmiyh496n3aafx0")))

(define-public crate-manyhow-macros-0.10.2 (c (n "manyhow-macros") (v "0.10.2") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0yjnqn5magybx8ciyzadxhcjc9ar36gp4dzizhl7gy25c6qxai3g")))

(define-public crate-manyhow-macros-0.10.3 (c (n "manyhow-macros") (v "0.10.3") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0gy5xp1w4hsdisqfz3xr0xnzfw7ki3r16n13amk869glvziznn3s")))

(define-public crate-manyhow-macros-0.10.4 (c (n "manyhow-macros") (v "0.10.4") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1b8f29vp1fzwpvc3rxd4949c0jpjv88vxa2f37j7c9cgq3i22in6")))

(define-public crate-manyhow-macros-0.11.0 (c (n "manyhow-macros") (v "0.11.0") (d (list (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "05ll1zb2py9f2jn5vrik7ccmz2nwfkxrkk9s9d98mcs6b0ns2ajk")))

