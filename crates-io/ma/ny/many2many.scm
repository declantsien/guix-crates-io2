(define-module (crates-io ma ny many2many) #:use-module (crates-io))

(define-public crate-many2many-0.0.1 (c (n "many2many") (v "0.0.1") (h "1iyamcqi37cx6giqz7nwp8wmjc1zlw3fxlpwi751gd9m0mmfh4pm")))

(define-public crate-many2many-0.0.2 (c (n "many2many") (v "0.0.2") (h "18nkzsfsc9r7432qm9rda8zx0jpna43g6czwyz3jw47kw2zvpxzd")))

