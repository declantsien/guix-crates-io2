(define-module (crates-io ma ny many-to-many) #:use-module (crates-io))

(define-public crate-many-to-many-0.1.0 (c (n "many-to-many") (v "0.1.0") (h "1lvh04a2wqlbgr0ldf272ffi51zp011d1wczl2xqwv5yphzfpgg0")))

(define-public crate-many-to-many-0.1.1 (c (n "many-to-many") (v "0.1.1") (h "19arrxnpa2q75w7p17mz0irvjyazccdxd8s86zr63prfmwfqdmlw")))

(define-public crate-many-to-many-0.1.2 (c (n "many-to-many") (v "0.1.2") (h "06fz66z1mw8mfqh1iiaambqxsnxgfy6r9wdczhmy6fhzcw8np018")))

(define-public crate-many-to-many-0.1.3 (c (n "many-to-many") (v "0.1.3") (h "0cr60z6fyk8j4xndjdd3fml96967q43gbyasx9s6bphgxyk3kpkg")))

(define-public crate-many-to-many-0.1.4 (c (n "many-to-many") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "0dvm1897s4byzrixxbyq7lql2b8m1c3g5gsiq152rkkk25sqpf3b") (f (quote (("test" "serde" "serde_json") ("default"))))))

(define-public crate-many-to-many-0.1.5 (c (n "many-to-many") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "15n5x1qmfhn8afaadvyxy1y4lx8qbv5prslbqpgxp260kj2v7936") (f (quote (("test" "serde" "serde_json") ("default"))))))

(define-public crate-many-to-many-0.1.6 (c (n "many-to-many") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "16psvwf3yfiz039zls07gxhfrq9av6fpaah4pk2zcnx30ph3d3d1") (f (quote (("test" "serde" "serde_json") ("default"))))))

(define-public crate-many-to-many-0.1.7 (c (n "many-to-many") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "0ivmfjnkbdb31xndyymx5am9raxakcdg5apvfx85w8dpib40ca7z") (f (quote (("test" "serde" "serde_json") ("default"))))))

