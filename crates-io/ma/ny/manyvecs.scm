(define-module (crates-io ma ny manyvecs) #:use-module (crates-io))

(define-public crate-manyvecs-0.1.0 (c (n "manyvecs") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0whslpjh39x9nfdnpqhm7wjij9flr7h9k9c8lnhjszqwznczmghc")))

(define-public crate-manyvecs-0.2.0 (c (n "manyvecs") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0f78bdvvksnvl9wnjzzwznl0h5g2m1pw5wzav75nf5n0p6dna87x")))

(define-public crate-manyvecs-0.3.0 (c (n "manyvecs") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0g9vbx6flyj06nv7n9qcl93mkh7qq5rm3s9nl6kaczsbv1c55jkf") (f (quote (("macroed") ("legacy" "num-traits") ("default" "legacy"))))))

