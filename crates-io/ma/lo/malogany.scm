(define-module (crates-io ma lo malogany) #:use-module (crates-io))

(define-public crate-malogany-0.1.0 (c (n "malogany") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1z03smikmplw36k5lvyk88wflaaa6fi32wirdl7ykydhll17pvd3")))

(define-public crate-malogany-0.2.0 (c (n "malogany") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0prnr9np6dy056jl6xg7klj16ills0ivahnw204iavm5i1yfq1z5")))

