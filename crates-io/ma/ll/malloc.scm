(define-module (crates-io ma ll malloc) #:use-module (crates-io))

(define-public crate-malloc-0.0.0 (c (n "malloc") (v "0.0.0") (h "1h7mpvav62avs67mv5ciivkfddim1wrli05rl4c98dygq344alsm")))

(define-public crate-malloc-0.0.1 (c (n "malloc") (v "0.0.1") (h "0v3phs30qf3s0cgf1a77xxrf5fvf8x3ynwdm118q5n9dk8hjn9m9")))

(define-public crate-malloc-0.0.2 (c (n "malloc") (v "0.0.2") (h "1hy6643anira0lcsanzivzkjcd8rvpbjmmxzvq692fin9f13c0kb")))

(define-public crate-malloc-0.0.3 (c (n "malloc") (v "0.0.3") (h "1l2wiqxyghawc6c7s39qiz5a25mjr1zr985nxdapgd4skj8vnyss")))

(define-public crate-malloc-0.0.4 (c (n "malloc") (v "0.0.4") (h "0a2x6c9ap30y952bh887d4prrvj8pdp2cw7c1yb3k9l3rs8wn90m")))

