(define-module (crates-io ma ll malloc-array) #:use-module (crates-io))

(define-public crate-malloc-array-1.0.0 (c (n "malloc-array") (v "1.0.0") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xdyr36177ygp27frfxngza8rfhskp1yijk1xsq1n5wa2cfm3df7") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.1.0 (c (n "malloc-array") (v "1.1.0") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06yf4242nk4xq0nfipx4c9xlsgwxaki1xs28wrh7lx8lqzxl5dc9") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.1.1 (c (n "malloc-array") (v "1.1.1") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y98j3vjkdzksklnv650wvxbhmygqm4padlw41q6r888w0zz79ih") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.2.1 (c (n "malloc-array") (v "1.2.1") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qjrg6imi31xjzy6p0jwv6v642m5am01van9wp7skpxcyzgxm8vg") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.2.2 (c (n "malloc-array") (v "1.2.2") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dj103dpz9f7a2czi312knirfj7wqwfppkvsx5kxd8nfgrv9di42") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.2.3 (c (n "malloc-array") (v "1.2.3") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kvb4iz0nlkgpwkvs5x6cgy1j17qw6264vcfdrjzxzqabhrppjvg") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.3.3 (c (n "malloc-array") (v "1.3.3") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06bl6n700p1smykfj6wdschkyjmibyjbyj45jxyvswmix9cxir96") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.4.3 (c (n "malloc-array") (v "1.4.3") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04q04rwmdzm1bn8134vsc0pfb1hi7s6zg5i4695v6j52bpa0az3d") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

(define-public crate-malloc-array-1.4.4 (c (n "malloc-array") (v "1.4.4") (d (list (d (n "jemalloc-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p4ghfwg0m3k7ndfa2n10p3wh548zijhc5n4nxyqygxvqxq4g6cz") (f (quote (("zst_noalloc") ("jemalloc" "jemalloc-sys") ("default" "zst_noalloc") ("assume_libc"))))))

