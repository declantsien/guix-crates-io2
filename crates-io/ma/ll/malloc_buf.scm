(define-module (crates-io ma ll malloc_buf) #:use-module (crates-io))

(define-public crate-malloc_buf-0.0.1 (c (n "malloc_buf") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pr4jk7m33f5fbdk7ycl1ykbpanzdh6a07xq7a2pdh40fzvca88z")))

(define-public crate-malloc_buf-0.0.2 (c (n "malloc_buf") (v "0.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1z4big48yiqzfw6k03hb7pp8vvzlfwwhqrka852j5gc2r8d6xx15")))

(define-public crate-malloc_buf-0.0.3 (c (n "malloc_buf") (v "0.0.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "114bfyhq9wlddqiw6i4b7ykyxjybnzp8xbyxdphw8jb6bm6wp03x")))

(define-public crate-malloc_buf-0.0.4 (c (n "malloc_buf") (v "0.0.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1yaf0ny419nvchqbp94xs26yxbndb107aa6wj1ids7064nd7gpmc")))

(define-public crate-malloc_buf-0.0.5 (c (n "malloc_buf") (v "0.0.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0jp8sfa77qi2r48lr4rsbfzybdgd84kg77fiw5ksici6p3hk7kp7")))

(define-public crate-malloc_buf-0.0.6 (c (n "malloc_buf") (v "0.0.6") (d (list (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)))) (h "1jqr77j89pwszv51fmnknzvd53i1nkmcr8rjrvcxhm4dx1zr1fv2")))

(define-public crate-malloc_buf-1.0.0 (c (n "malloc_buf") (v "1.0.0") (d (list (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)))) (h "1zap9m0xmd5sdsxil7v2rgb1dzlq0308f826pwvqdvjyaz0chciz")))

