(define-module (crates-io ma ll mallumo-gls) #:use-module (crates-io))

(define-public crate-mallumo-gls-0.1.0 (c (n "mallumo-gls") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0fqw4m03i8124vpc1h5wls4w3j5i3lrvkvpspgmvjz1kfblgzxhf")))

(define-public crate-mallumo-gls-0.2.0 (c (n "mallumo-gls") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "18jkky0afnpxlpj782s8zyvv0k0qayykvizqm3a5mxbkzmg9d0wc")))

(define-public crate-mallumo-gls-0.3.0 (c (n "mallumo-gls") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1ki1mm5id2b35w9b2cc8hm9hr3glav50x49rc1k075qjdd9qh302")))

(define-public crate-mallumo-gls-0.3.1 (c (n "mallumo-gls") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1spcplrx2ndikf6763pp32agn6q4v3zi2pfnfbaygpa10mcbmwpp")))

(define-public crate-mallumo-gls-0.3.2 (c (n "mallumo-gls") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "077bavmlah2ks2r6hf1cb6kxvffspwkpp3ga7ir3bc8cz12m0l82")))

(define-public crate-mallumo-gls-0.3.3 (c (n "mallumo-gls") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1scif3dx3ydabp9gvfq67lclygq0yj021h847743al5i056j2ig9")))

(define-public crate-mallumo-gls-0.4.0 (c (n "mallumo-gls") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0ba97m48dmvmg2clhaj2hf3fh1y6w1sppd1lyx8i4zyd6768wdsv")))

(define-public crate-mallumo-gls-0.5.0 (c (n "mallumo-gls") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "14vshicfa9y00j9pzs3zlyv0z92ydp538g6y7h8l3w9cd2bivsxz")))

(define-public crate-mallumo-gls-0.6.0 (c (n "mallumo-gls") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1cwl46xh8yhalbjcvvpny1xf420rzrqy1lyvbl0hibhrm8ll8flz")))

(define-public crate-mallumo-gls-0.7.0 (c (n "mallumo-gls") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0mvar7j0idk7fkqlzqvnpilzdfyhfvgrpvglaljxmnw7fprkr4rb")))

(define-public crate-mallumo-gls-0.8.0 (c (n "mallumo-gls") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1m9h2xhy95y5pxjp1aij1m5nnsibshirjkxii3ji0qxq1yd8h4h6")))

(define-public crate-mallumo-gls-0.9.0 (c (n "mallumo-gls") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0ihkmvrhjkjxhspf3aji6vm8kqjn92h0amaixabkjxb97y81gxwl")))

(define-public crate-mallumo-gls-0.9.1 (c (n "mallumo-gls") (v "0.9.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "123fgk9zydrk7rnbkpaqiwy588hd9mxhz0ccs7dy657q5a5acrpp")))

(define-public crate-mallumo-gls-0.9.2 (c (n "mallumo-gls") (v "0.9.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "00nm1g4miccjaay08c3j85kjjrypgfvljwhi3v99c1jvvchg2fqq")))

(define-public crate-mallumo-gls-0.9.3 (c (n "mallumo-gls") (v "0.9.3") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1h5akjyjh1x648xdxaylrfdv4lbb4452f2r81m9gp36r1d8xh859")))

(define-public crate-mallumo-gls-0.9.4 (c (n "mallumo-gls") (v "0.9.4") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1si6ks61pihrgljgz3chzjr3p8pw9rhd1n1i1ldhv824m28jlrzn")))

(define-public crate-mallumo-gls-0.9.6 (c (n "mallumo-gls") (v "0.9.6") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0r4310slfa01gzmq4p4gqn0hycig1c3cxhh5sgznyacvawd71hnw")))

(define-public crate-mallumo-gls-0.10.0 (c (n "mallumo-gls") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0zxabgvlzmpsi8xjbmk9gg8f3ilw0i0asgmfls9bdy6ac12i1n85")))

(define-public crate-mallumo-gls-0.10.1 (c (n "mallumo-gls") (v "0.10.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0izw8jm6mnrgdi2mnbx2y06kq9vl1yvnswskb96lk33qh6pnlzvn")))

(define-public crate-mallumo-gls-0.11.0 (c (n "mallumo-gls") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0pxwzqilk3r5y5yab276ay2zif1axmlxj7dh0w31cywiqcnzv3mp")))

(define-public crate-mallumo-gls-0.12.0 (c (n "mallumo-gls") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0kiygksj2yxj3p1vj4spx817fwsf43ph572c878y42ndw7mn72dl")))

(define-public crate-mallumo-gls-0.13.0 (c (n "mallumo-gls") (v "0.13.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "054x5hbapsc2930lbyl754s9qrfwy47k2paazml8x3w89nmwbsg4")))

(define-public crate-mallumo-gls-0.14.0 (c (n "mallumo-gls") (v "0.14.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1rylvw874ny1m5nkr6vwlrfagpchr7h0i4faz8c7c604z2r5nj4k")))

(define-public crate-mallumo-gls-0.15.0 (c (n "mallumo-gls") (v "0.15.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1lsyld2vp6bmkwc777bdgxfnzgasbaby1qr7f6jnvpdrsk9q0a6q")))

(define-public crate-mallumo-gls-0.16.0 (c (n "mallumo-gls") (v "0.16.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1dbhqnkk6hpx9cbyav2f2zyshiw4ma3p5k3ac3g2697a9drpmhvg")))

(define-public crate-mallumo-gls-0.17.0 (c (n "mallumo-gls") (v "0.17.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0ipllan9v0nwnl5qandd2dvvss2b6kdpzzx6mfi69lvygsma4x1s")))

(define-public crate-mallumo-gls-0.18.0 (c (n "mallumo-gls") (v "0.18.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "18zd5f4khidqcw4ankdr2xqlc9q582cqn37ldpxnn3cs8spfi985")))

(define-public crate-mallumo-gls-0.18.1 (c (n "mallumo-gls") (v "0.18.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0xhaxf1nvdjd1zhmwb19hx8p8vr5z56kz2i02f32zq9k1n11xgh7")))

(define-public crate-mallumo-gls-0.18.2 (c (n "mallumo-gls") (v "0.18.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1y1y2wyarrc3v6wprp293cnfzq3xb19z3xncb4fm3q2zyvc8bw06")))

(define-public crate-mallumo-gls-0.18.3 (c (n "mallumo-gls") (v "0.18.3") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0xvbv96xa22iw7j0z10ifq1rx6dbzxw17k8azjv44fagw7832cf7")))

(define-public crate-mallumo-gls-0.18.5 (c (n "mallumo-gls") (v "0.18.5") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0y3m9pk5vxh6q9bil4a28bbs4m28dxccr5kis33vwirpg0vzcvm7")))

(define-public crate-mallumo-gls-0.19.0 (c (n "mallumo-gls") (v "0.19.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "0jq2345lr3xrbqp7d9wxdkis6s89dnxbsb5pi622sji3v0vfgcjw")))

(define-public crate-mallumo-gls-0.20.0 (c (n "mallumo-gls") (v "0.20.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1afavwl8h64fxkxvlrzrng241d99anr0mfmdqy26d0n969r52h59")))

(define-public crate-mallumo-gls-0.20.1 (c (n "mallumo-gls") (v "0.20.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1bq0hy2dklyc1k24widsmzzr3k1lhk3chq4saffbwn3wpry66440")))

(define-public crate-mallumo-gls-0.20.2 (c (n "mallumo-gls") (v "0.20.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1bc5gbz41939iwsf1lmv2hzq06g56xhv47948qfs8224fnd3yy67")))

(define-public crate-mallumo-gls-0.21.0 (c (n "mallumo-gls") (v "0.21.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)))) (h "1zg16vphk6qppbabzf4vcqvyl6hyapijhyk6z3whj8n0ixdjqh5a")))

(define-public crate-mallumo-gls-0.23.0 (c (n "mallumo-gls") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.10") (d #t) (k 0)))) (h "0y463i8hn783092vzpdi29nijx2axalwv076k0axfaj2s5z4z9af")))

(define-public crate-mallumo-gls-0.24.0 (c (n "mallumo-gls") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.10") (d #t) (k 0)))) (h "014lp01xa84885lv96yz9bc97yy8fg4ya4ssnwrnqnnkyx0xpgjz")))

(define-public crate-mallumo-gls-0.25.0 (c (n "mallumo-gls") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.10") (d #t) (k 0)))) (h "0zm7wp7ci4rs9rm4rkgbsfc3bb55mfmn18afc31rb5jaysy57cs2")))

(define-public crate-mallumo-gls-0.26.0 (c (n "mallumo-gls") (v "0.26.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.10") (d #t) (k 0)))) (h "1cxwhlcqgzfsfykcfmqd5xxnydvd9y3hfwx6k56077ibxb1f21kd")))

(define-public crate-mallumo-gls-0.27.0 (c (n "mallumo-gls") (v "0.27.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "000c6bdrp5bhx3ixm93d4nmwfaa978myvdh5db4jv1w481q2g9zx")))

(define-public crate-mallumo-gls-0.28.0 (c (n "mallumo-gls") (v "0.28.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0yh1c5daz9zsv3mb7rx1p6iz0fg6fpkhmqpxv6rxbba8hj35d8n6")))

(define-public crate-mallumo-gls-0.29.0 (c (n "mallumo-gls") (v "0.29.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1bx2fdpabrd3dw8n9jixi40cmyh0r2y6bjgvnxp150m2010g8jyb")))

(define-public crate-mallumo-gls-0.30.0 (c (n "mallumo-gls") (v "0.30.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1qwiljhrc5a287say5bnbsahfwjsa9z6cp21ll7jxszn7dk62k5b")))

(define-public crate-mallumo-gls-0.31.0 (c (n "mallumo-gls") (v "0.31.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1x33b2833kq93igk5qz7gnn4qjbi70qk55rq4fmsa2mxahfanv18")))

(define-public crate-mallumo-gls-0.32.0 (c (n "mallumo-gls") (v "0.32.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0ngg9nhzmc0bql4xdqiw9534kg5bx64xis1fgfx0d12h2ab6p0il")))

(define-public crate-mallumo-gls-0.33.0 (c (n "mallumo-gls") (v "0.33.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "049zalbyr7lfcqi4ji24x2nmhrp6i9iajj0m4wq8abdadavj1h83")))

(define-public crate-mallumo-gls-0.34.0 (c (n "mallumo-gls") (v "0.34.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0fs60hbk9g9q19ajh7yfnnkm5cbdvzxhnpzw03kcksrgwcds6sbp")))

(define-public crate-mallumo-gls-0.35.0 (c (n "mallumo-gls") (v "0.35.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "00c38124yij35mi5n90xgppx8drgy5yl9kxqsvakpq9cdcjqiyc4")))

(define-public crate-mallumo-gls-0.36.0 (c (n "mallumo-gls") (v "0.36.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "10f7v1682rd9ifny2l2n6acqg5j6q19ckxf5n4giv3fjx8ckwiz6")))

(define-public crate-mallumo-gls-0.37.0 (c (n "mallumo-gls") (v "0.37.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0g46p8jv8ik83vhlhbflzp5q8njrciwf5vs8mriiz6nc0ymz06k4")))

(define-public crate-mallumo-gls-0.38.0 (c (n "mallumo-gls") (v "0.38.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "073avrkmjplnlrs20ngjxmzifzih6fr5a1g47hfsrynp60fzcckx")))

(define-public crate-mallumo-gls-0.39.0 (c (n "mallumo-gls") (v "0.39.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0p0rvll4ba0lybwqd8wip8x44yiq0sghcsmmdhm4pfxhw9nypyya")))

(define-public crate-mallumo-gls-0.40.0 (c (n "mallumo-gls") (v "0.40.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.6.1") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1ys62379nfhbwd570k3hwyzzmgxah89ckpg32f0p7faz786jrc4r")))

(define-public crate-mallumo-gls-0.42.0 (c (n "mallumo-gls") (v "0.42.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1zpwb1z8iqwn0qqh7prz50pc1xkmivx14x7v8xnmyw85wqzvdi8j")))

(define-public crate-mallumo-gls-0.43.0 (c (n "mallumo-gls") (v "0.43.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "1li0qwk93plzfdfdsn6b2zhqknvlsc344qazrldzmrj9xanw8ggk")))

