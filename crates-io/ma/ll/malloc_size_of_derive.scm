(define-module (crates-io ma ll malloc_size_of_derive) #:use-module (crates-io))

(define-public crate-malloc_size_of_derive-0.0.1 (c (n "malloc_size_of_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "18qyxm4mk81ikijmnj83dvrsyqmwpdwi082fdd9sq12h8xv5r94c")))

(define-public crate-malloc_size_of_derive-0.1.0 (c (n "malloc_size_of_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1hvv31ng0cl6m9n291jvbdzj5wr9a328pd9csq3pvkv2v6gfxb9m")))

(define-public crate-malloc_size_of_derive-0.1.1 (c (n "malloc_size_of_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "02szl1aly2in9sa1bxpxg39rppyl2lzh67qiki65yg27v565sz73")))

(define-public crate-malloc_size_of_derive-0.1.2 (c (n "malloc_size_of_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "10m9c14nvjy3g7yilza202gp2ypszy8lf4q7ii2q5ylb5984f9k3")))

