(define-module (crates-io ma ll malltec_libs) #:use-module (crates-io))

(define-public crate-malltec_libs-0.1.0 (c (n "malltec_libs") (v "0.1.0") (h "059v1vdz9yb598kyg0jp98ry5y2kyjwnxp7m9pfqr95vixci1lac")))

(define-public crate-malltec_libs-0.1.1 (c (n "malltec_libs") (v "0.1.1") (h "1lr6hdbcipahg6clqklhd5w7yz6mpr01c2l9n1ynlas8vl6aqb18")))

