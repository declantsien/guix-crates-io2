(define-module (crates-io ma ll malloc-bind) #:use-module (crates-io))

(define-public crate-malloc-bind-0.1.0 (c (n "malloc-bind") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sysconf") (r "^0.3.0") (d #t) (k 0)))) (h "1my3wb91prlvhqdnxcjhgnj5ixcj2akw8ayf9cjm2c4icr18z1s0")))

