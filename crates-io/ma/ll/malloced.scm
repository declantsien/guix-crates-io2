(define-module (crates-io ma ll malloced) #:use-module (crates-io))

(define-public crate-malloced-1.0.0 (c (n "malloced") (v "1.0.0") (h "06j54xxv3jdd7fxfg3fgrx0phbw361q0f0xnn0f7z77ag5a33zqb") (f (quote (("std") ("pin") ("default" "std"))))))

(define-public crate-malloced-1.0.1 (c (n "malloced") (v "1.0.1") (h "1f15yp6wlq6w7s8ngbdpqkcj5k83p8f5dxj43asq6njjvs24krhn") (f (quote (("std") ("pin") ("default" "std"))))))

(define-public crate-malloced-1.1.0 (c (n "malloced") (v "1.1.0") (h "1j849sbxv837wngwmbnlshxg2p1jpg1b2qz4db60dzjk1ip9iwfi") (f (quote (("std") ("pin") ("default" "std"))))))

(define-public crate-malloced-1.1.1 (c (n "malloced") (v "1.1.1") (h "0licn9y5wykkddagg16mjw8kc5r6v19m9l8jdq9hmckgvdndkph5") (f (quote (("std") ("pin") ("default" "std"))))))

(define-public crate-malloced-1.2.0 (c (n "malloced") (v "1.2.0") (h "1l7bj85x8w6diw7j3fdzrsgwjivkjp6haav98gyxfp4phydl81l4") (f (quote (("std") ("pin") ("default" "std")))) (r "1.42")))

(define-public crate-malloced-1.3.0 (c (n "malloced") (v "1.3.0") (h "1s9njxw0j8dabjibnqgqca5zpnzwv47ksb7vcr21dh8ygnmvx5xg") (f (quote (("std") ("pin") ("default" "std")))) (r "1.64")))

(define-public crate-malloced-1.3.1 (c (n "malloced") (v "1.3.1") (h "1v00ain5cv4p9v7vn4hxnrdx83hcmv3nxbgfcaf50f8bkqpvpzkd") (f (quote (("std") ("pin") ("default" "std")))) (r "1.64")))

