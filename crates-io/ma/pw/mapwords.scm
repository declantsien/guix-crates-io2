(define-module (crates-io ma pw mapwords) #:use-module (crates-io))

(define-public crate-mapwords-0.1.0 (c (n "mapwords") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04yb7h5w2whcnqsiqs89www3zvv6xqxf5f2qj66fgfdavsa1gjvz")))

(define-public crate-mapwords-0.1.1 (c (n "mapwords") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zdycra213ic66b8a0f56b4skwsvyaq1qc0fxrh6gbvkmmw036l9") (y #t)))

(define-public crate-mapwords-0.1.2 (c (n "mapwords") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "081p5h5kb6jm728hszd7kjv4fk764hvxa4z46hxqic7hlndrss2s") (y #t)))

(define-public crate-mapwords-0.1.3 (c (n "mapwords") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0q2nchrz6y7cd152rn0b1wnap854pxcvzy3b70b4bcjm6ah0p9p3")))

(define-public crate-mapwords-0.1.4 (c (n "mapwords") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03xqmbmr0y75lpmaygyd0wvfkixcp17j31kgmhjkqx2fki0fw3if")))

(define-public crate-mapwords-0.1.5 (c (n "mapwords") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1krq0p1md0mgs6a5a9dxp5vc3sp1zkl96wxbif6jrwjb1l3hfgxn")))

