(define-module (crates-io ma ke makepad-example-fractal-zoom) #:use-module (crates-io))

(define-public crate-makepad-example-fractal-zoom-0.3.0 (c (n "makepad-example-fractal-zoom") (v "0.3.0") (d (list (d (n "makepad-widgets") (r "^0.3.0") (d #t) (k 0)))) (h "02fjdrlqbd2dxvc9q3ibafr0pl7axf6m4nqrxh8gbydmc3sdr62c") (f (quote (("nightly" "makepad-widgets/nightly"))))))

(define-public crate-makepad-example-fractal-zoom-0.4.0 (c (n "makepad-example-fractal-zoom") (v "0.4.0") (d (list (d (n "makepad-widgets") (r "^0.4.0") (d #t) (k 0)))) (h "1syf9gylvaqcjli7s4lsdbp2lyp1wnz6ws03pn1i6rg2lgwyffvz")))

(define-public crate-makepad-example-fractal-zoom-0.5.0 (c (n "makepad-example-fractal-zoom") (v "0.5.0") (d (list (d (n "makepad-widgets") (r "^0.5.0") (d #t) (k 0)))) (h "02larkwv7877yb9msb76yn5is1qhjkvaz0hkgkcjjwy03i26kzvs")))

(define-public crate-makepad-example-fractal-zoom-0.6.0 (c (n "makepad-example-fractal-zoom") (v "0.6.0") (d (list (d (n "makepad-widgets") (r "^0.6.0") (d #t) (k 0)))) (h "103jiaswpkkd3pv82nkdk2caq1pqinms07d3smj1h33zvq8m3g47")))

