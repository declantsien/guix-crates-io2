(define-module (crates-io ma ke makepad-wasm-bridge) #:use-module (crates-io))

(define-public crate-makepad-wasm-bridge-0.3.0 (c (n "makepad-wasm-bridge") (v "0.3.0") (d (list (d (n "makepad-derive-wasm-bridge") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-live-id") (r "^0.3.0") (d #t) (k 0)))) (h "0x5b2wrnnvn269z3a8mad9crk9nqzp2b10cgs9c414lg0xf1wrla")))

(define-public crate-makepad-wasm-bridge-0.4.0 (c (n "makepad-wasm-bridge") (v "0.4.0") (d (list (d (n "makepad-derive-wasm-bridge") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)))) (h "0nx6lcjvr5p8cnb8sn556xa5xv2b8vc51pa9y9xg2pil2q0pp3mb")))

