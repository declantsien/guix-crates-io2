(define-module (crates-io ma ke makeup-comparator) #:use-module (crates-io))

(define-public crate-makeup-comparator-1.0.0 (c (n "makeup-comparator") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "scrapped-webs") (r "^1.0.0") (d #t) (k 0)))) (h "1m8m7y0rhbbak5w9zvdayx1qk21cxqm8nyk0db5sbdpcslnzn50f")))

