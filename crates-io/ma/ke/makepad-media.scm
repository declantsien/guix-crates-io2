(define-module (crates-io ma ke makepad-media) #:use-module (crates-io))

(define-public crate-makepad-media-0.3.0 (c (n "makepad-media") (v "0.3.0") (d (list (d (n "makepad-platform") (r "^0.3.0") (d #t) (k 0)))) (h "0plz6f0xp0mwwi6ginjj8qyzpjcxz6ildf0nia9wlffh9qahpcq4") (f (quote (("nightly" "makepad-platform/nightly"))))))

