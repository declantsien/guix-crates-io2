(define-module (crates-io ma ke makepad-ttf-parser) #:use-module (crates-io))

(define-public crate-makepad-ttf-parser-0.1.0 (c (n "makepad-ttf-parser") (v "0.1.0") (d (list (d (n "makepad-font") (r "^0.1") (d #t) (k 0)) (d (n "makepad-geometry") (r "^0.1") (d #t) (k 0)) (d (n "makepad-internal-iter") (r "^0.1") (d #t) (k 0)))) (h "0pzvaricxlmpzbh5526a27h1aqlysi2c5l1iisv82lxwlmc34iz0")))

