(define-module (crates-io ma ke makelink) #:use-module (crates-io))

(define-public crate-makelink-0.1.0 (c (n "makelink") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "1wpa5z3ri2h21qapzai33kmlfz155939w6nnip6m3n3afgw352h5")))

(define-public crate-makelink-0.2.0 (c (n "makelink") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "12xnn4m5a0vdsj3wm2rsj12hi28fp3l7im7zksfh1rch052gl999")))

