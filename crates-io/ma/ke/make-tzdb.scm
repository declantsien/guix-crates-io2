(define-module (crates-io ma ke make-tzdb) #:use-module (crates-io))

(define-public crate-make-tzdb-0.0.0 (c (n "make-tzdb") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tz-rs") (r "^0.6.7") (d #t) (k 0)))) (h "0xf3884rwmksvvm4fbhppfr4dmzag5wy11wyrnhfb377wim1as1g") (y #t)))

