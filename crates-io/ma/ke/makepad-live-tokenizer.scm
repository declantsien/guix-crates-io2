(define-module (crates-io ma ke makepad-live-tokenizer) #:use-module (crates-io))

(define-public crate-makepad-live-tokenizer-0.3.0 (c (n "makepad-live-tokenizer") (v "0.3.0") (d (list (d (n "makepad-live-id") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-math") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.3.0") (d #t) (k 0)))) (h "0zjikzvqx7l4zvwnz228ldp4pzy8yg84g7lsibkppammvqjszn87")))

(define-public crate-makepad-live-tokenizer-0.4.0 (c (n "makepad-live-tokenizer") (v "0.4.0") (d (list (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-math") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.4.0") (d #t) (k 0)))) (h "1h362nkzik404r3rlak396jdk5lh61qmbypq1ibz6ncd2y8mkf00")))

