(define-module (crates-io ma ke make-message-bus) #:use-module (crates-io))

(define-public crate-make-message-bus-0.1.0 (c (n "make-message-bus") (v "0.1.0") (d (list (d (n "message-bus-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0skpxcjh505l8p4brhgy5lqmfw8n3awqr2qpvacxn1i9ihs36nyi")))

(define-public crate-make-message-bus-0.1.1 (c (n "make-message-bus") (v "0.1.1") (d (list (d (n "message-bus-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1az7sb4i6xbz5r8iza42m9hnihzry92cpas1jakqack0vvzgzf9k")))

