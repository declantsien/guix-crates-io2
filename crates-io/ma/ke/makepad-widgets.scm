(define-module (crates-io ma ke makepad-widgets) #:use-module (crates-io))

(define-public crate-makepad-widgets-0.3.0 (c (n "makepad-widgets") (v "0.3.0") (d (list (d (n "makepad-derive-widget") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-draw-2d") (r "^0.3.0") (d #t) (k 0)))) (h "122vq01h26gqmx4gjn03gn90kp709c5w7sxrlqhx4hh5avgfr3i2") (f (quote (("nightly" "makepad-draw-2d/nightly"))))))

(define-public crate-makepad-widgets-0.4.0 (c (n "makepad-widgets") (v "0.4.0") (d (list (d (n "makepad-derive-widget") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-draw") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-zune-jpeg") (r "^0.3.17") (d #t) (k 0)) (d (n "makepad-zune-png") (r "^0.2.1") (d #t) (k 0)))) (h "05ihs9vmcqp8ca3pr664nq424c7d43qq3wpc5dxabdki09b31jwz")))

(define-public crate-makepad-widgets-0.5.0 (c (n "makepad-widgets") (v "0.5.0") (d (list (d (n "makepad-derive-widget") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-draw") (r "^0.5.0") (d #t) (k 0)) (d (n "makepad-zune-jpeg") (r "^0.3.17") (d #t) (k 0)) (d (n "makepad-zune-png") (r "^0.2.1") (d #t) (k 0)))) (h "1yxp5lf0x5yrj1lvdqyxikkpiid192cfr1hn2in2rnn2bfwmgix2")))

(define-public crate-makepad-widgets-0.6.0 (c (n "makepad-widgets") (v "0.6.0") (d (list (d (n "makepad-derive-widget") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-draw") (r "^0.6.0") (d #t) (k 0)) (d (n "makepad-zune-jpeg") (r "^0.3.17") (d #t) (k 0)) (d (n "makepad-zune-png") (r "^0.2.1") (d #t) (k 0)))) (h "0421wf377dp33g065adkmal9ghbyf6dri8dbln95sdd7lsrz8dbh")))

