(define-module (crates-io ma ke makepad-trapezoidator) #:use-module (crates-io))

(define-public crate-makepad-trapezoidator-0.1.0 (c (n "makepad-trapezoidator") (v "0.1.0") (d (list (d (n "makepad-geometry") (r "^0.1") (d #t) (k 0)) (d (n "makepad-internal-iter") (r "^0.1") (d #t) (k 0)) (d (n "makepad-path") (r "^0.1") (d #t) (k 0)))) (h "0dj4s6qpd4asl30s8sgjz802iqa7y62xyzx7nf98javr15bkm9h8")))

