(define-module (crates-io ma ke makepad-example-news-feed) #:use-module (crates-io))

(define-public crate-makepad-example-news-feed-0.5.0 (c (n "makepad-example-news-feed") (v "0.5.0") (d (list (d (n "makepad-widgets") (r "^0.5.0") (d #t) (k 0)))) (h "1s4h5282s271k7mq2nmw56rb5qax89dff8k7gr0n9x8wc7djhgwq")))

(define-public crate-makepad-example-news-feed-0.6.0 (c (n "makepad-example-news-feed") (v "0.6.0") (d (list (d (n "makepad-widgets") (r "^0.6.0") (d #t) (k 0)))) (h "0c5yh0xl4aac98glin6ryjnz4zxvgrh5kkha1dzhw3ipa49rblxx")))

