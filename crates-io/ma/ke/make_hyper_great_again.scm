(define-module (crates-io ma ke make_hyper_great_again) #:use-module (crates-io))

(define-public crate-make_hyper_great_again-0.11.0 (c (n "make_hyper_great_again") (v "0.11.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.7") (d #t) (k 2)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "18inak3vy4gd7mv62xs51zc0w36hmk5py6dwch9k69r51vmh4n9v")))

(define-public crate-make_hyper_great_again-0.11.1 (c (n "make_hyper_great_again") (v "0.11.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.7") (d #t) (k 2)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1fmddysdd5qnwards0irgv0mg1l0z4xw0p3jf1gxvl1pk721pg1q")))

