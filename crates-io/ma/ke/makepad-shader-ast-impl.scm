(define-module (crates-io ma ke makepad-shader-ast-impl) #:use-module (crates-io))

(define-public crate-makepad-shader-ast-impl-0.1.0 (c (n "makepad-shader-ast-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0rcpi5yiinwjz3kmclmqbgd14qz1c3gnbr6gw8ay9662xc419bkq")))

(define-public crate-makepad-shader-ast-impl-0.2.0 (c (n "makepad-shader-ast-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15jbm844wrxxiz5rj6hby842mh7n97d44ymgkxlh6vz89qm45pb4")))

