(define-module (crates-io ma ke makepad-audio-graph) #:use-module (crates-io))

(define-public crate-makepad-audio-graph-0.4.0 (c (n "makepad-audio-graph") (v "0.4.0") (d (list (d (n "makepad-platform") (r "^0.4.0") (d #t) (k 0)))) (h "1w7lr7qgr4g25s2xr5z1gml3dv0qy0amzf5fw5fvmy1d2clfby7k")))

(define-public crate-makepad-audio-graph-0.5.0 (c (n "makepad-audio-graph") (v "0.5.0") (d (list (d (n "makepad-platform") (r "^0.5.0") (d #t) (k 0)))) (h "0aqh6w5qkfin525yww163paqsylp4cysbyb31ajmlrxbwmm9vciy")))

(define-public crate-makepad-audio-graph-0.6.0 (c (n "makepad-audio-graph") (v "0.6.0") (d (list (d (n "makepad-platform") (r "^0.6.0") (d #t) (k 0)))) (h "12cp4bz2c8bmy25mfn842wd5p23fhpbxjwid60340dhc31b9yj0h")))

