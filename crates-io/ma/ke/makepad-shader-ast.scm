(define-module (crates-io ma ke makepad-shader-ast) #:use-module (crates-io))

(define-public crate-makepad-shader-ast-0.1.0 (c (n "makepad-shader-ast") (v "0.1.0") (d (list (d (n "makepad-shader-ast-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0l45v3r2hf0zd3whdv7q52461y3d5m69fq9pa848fgchwjp4s0bx")))

(define-public crate-makepad-shader-ast-0.2.0 (c (n "makepad-shader-ast") (v "0.2.0") (d (list (d (n "makepad-shader-ast-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0mjphnnbk2xq3al5z2h4k10ic9kvg76pip68asrg36mwhqcjgacc")))

