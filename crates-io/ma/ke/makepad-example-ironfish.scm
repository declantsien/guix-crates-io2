(define-module (crates-io ma ke makepad-example-ironfish) #:use-module (crates-io))

(define-public crate-makepad-example-ironfish-0.3.0 (c (n "makepad-example-ironfish") (v "0.3.0") (d (list (d (n "makepad-media") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-widgets") (r "^0.3.0") (d #t) (k 0)))) (h "0pcwidcmch69in5qydb2ic04zjyv1lf2zizay2gffj2i5z0cgs4m") (f (quote (("nightly" "makepad-widgets/nightly" "makepad-media/nightly"))))))

(define-public crate-makepad-example-ironfish-0.4.0 (c (n "makepad-example-ironfish") (v "0.4.0") (d (list (d (n "makepad-audio-widgets") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-synth-ironfish") (r "^0.4.0") (d #t) (k 0)))) (h "0p6fh32fwl2xy4ph3w3da92kn3z5d49sscb4grlv60xf769rdsfv")))

(define-public crate-makepad-example-ironfish-0.5.0 (c (n "makepad-example-ironfish") (v "0.5.0") (d (list (d (n "makepad-audio-widgets") (r "^0.5.0") (d #t) (k 0)) (d (n "makepad-synth-ironfish") (r "^0.5.0") (d #t) (k 0)))) (h "1bz8q2f2zada45wqqyfn8lc09vmf6n5k5rqzz3bsxx01dlhvk8wp")))

(define-public crate-makepad-example-ironfish-0.6.0 (c (n "makepad-example-ironfish") (v "0.6.0") (d (list (d (n "makepad-audio-widgets") (r "^0.6.0") (d #t) (k 0)) (d (n "makepad-synth-ironfish") (r "^0.6.0") (d #t) (k 0)))) (h "0m2r7x4zjfp49za7v4nzjmbszvncknvhc5p6x3ask8377r4w5b17")))

