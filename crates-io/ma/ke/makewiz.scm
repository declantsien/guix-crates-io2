(define-module (crates-io ma ke makewiz) #:use-module (crates-io))

(define-public crate-makewiz-0.6.0 (c (n "makewiz") (v "0.6.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "11ln2lz3dg2wzbqcqhzc7fhkh3fv93rrisn9d797s807l98x9737") (y #t)))

(define-public crate-makewiz-0.6.1 (c (n "makewiz") (v "0.6.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1xjlnnny4ccg6fh8791jd7kn4a3f9vrg201g4z2iwbka8q0yna58")))

(define-public crate-makewiz-0.7.0 (c (n "makewiz") (v "0.7.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.4.0") (d #t) (k 1)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1ygv84jbq24q6vwxwibp9pwg6ydwd4q0vncjl4dz833his9rz5dy") (f (quote (("generate_completions"))))))

(define-public crate-makewiz-0.8.0 (c (n "makewiz") (v "0.8.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1sl5k3961i0y3yg50qh8gi17xz9j32jfcbgp1wdq1zg3j7dh234v") (f (quote (("generate_completions"))))))

