(define-module (crates-io ma ke makeup-macros) #:use-module (crates-io))

(define-public crate-makeup-macros-0.0.1 (c (n "makeup-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwlpir1shn8mk2kz4im5d5qwjzkbdshc4493s07gbmzcidjpxmp")))

(define-public crate-makeup-macros-0.0.2 (c (n "makeup-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1smg3cqxaqn4knvbjv4hp7mn8la52fidkfjgzr6dri8bprhr5zlf")))

