(define-module (crates-io ma ke makepad-micro-proc-macro) #:use-module (crates-io))

(define-public crate-makepad-micro-proc-macro-0.3.0 (c (n "makepad-micro-proc-macro") (v "0.3.0") (h "1i4zsvfa8jign58397lnyzp1aw7c3iky33642a208s3w63p6slng")))

(define-public crate-makepad-micro-proc-macro-0.4.0 (c (n "makepad-micro-proc-macro") (v "0.4.0") (h "01a9h2kxhyq85vpq2kif0jisp5njgjyipk4ljxwqp036pcmpqznm")))

