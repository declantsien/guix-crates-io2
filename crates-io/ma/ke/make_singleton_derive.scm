(define-module (crates-io ma ke make_singleton_derive) #:use-module (crates-io))

(define-public crate-make_singleton_derive-1.0.0 (c (n "make_singleton_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "11jkxqgqhzz8yh7wl8r6v7hy6xf0issilvnj9zaay9kpwz6ipb1m")))

