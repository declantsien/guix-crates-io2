(define-module (crates-io ma ke make_option) #:use-module (crates-io))

(define-public crate-make_option-0.1.2 (c (n "make_option") (v "0.1.2") (h "1r9lrayyvmqlmq14zc39r7985iz57d8l4q6c4hf9cbrznpyd7wwr")))

(define-public crate-make_option-0.1.3 (c (n "make_option") (v "0.1.3") (h "1d9zsjil02axm66bgkszc5v687f3ys5nxnw2975xg30jm06fb563")))

(define-public crate-make_option-0.1.5 (c (n "make_option") (v "0.1.5") (h "1w5hrcpa982mkxs0a96115v3h1d4chp5fkqqcgn3840vr1p09qz2")))

(define-public crate-make_option-0.1.6 (c (n "make_option") (v "0.1.6") (h "087smqmiq9zlrbjx84nz936fqa60cbpf1n9dmf2h1n72szvm1b20")))

(define-public crate-make_option-0.1.7 (c (n "make_option") (v "0.1.7") (h "1qync06rcnb91xmnlwcdhhna2jgvk63drfkz6qvc0z9415jwv9a5")))

