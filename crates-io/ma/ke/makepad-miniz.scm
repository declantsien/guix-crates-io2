(define-module (crates-io ma ke makepad-miniz) #:use-module (crates-io))

(define-public crate-makepad-miniz-0.3.0 (c (n "makepad-miniz") (v "0.3.0") (h "0b6iz59zdlpd6a3blzp6x948nb688pgw1i4avpzkv25b63h2pwg7")))

(define-public crate-makepad-miniz-0.4.0 (c (n "makepad-miniz") (v "0.4.0") (h "1fg0l47njlgy62qcb0k7zy6lwh7bf9bgz7shs7b57f63in413mc2")))

