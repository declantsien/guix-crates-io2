(define-module (crates-io ma ke makeup-ansi) #:use-module (crates-io))

(define-public crate-makeup-ansi-0.0.1 (c (n "makeup-ansi") (v "0.0.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1prpgb77ag16hy6ry9hpmlm9gm7vribrl6f96fxr422624mq1q3r")))

(define-public crate-makeup-ansi-0.0.2 (c (n "makeup-ansi") (v "0.0.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0p6sgazibx10zlmdndyp67zfyl8s0swgcm6fp2vc2xd6q8fbabna")))

(define-public crate-makeup-ansi-0.0.3 (c (n "makeup-ansi") (v "0.0.3") (d (list (d (n "eyre") (r "^0.6.11") (d #t) (k 0)))) (h "1m08rp3lrcinjib2fkib0a08zzsf5qmb9x0b7fblqccw71v55ijq")))

