(define-module (crates-io ma ke make-makefile) #:use-module (crates-io))

(define-public crate-make-makefile-0.1.0 (c (n "make-makefile") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "01jfj44hvlgrq6sydlfrbkg84vvida0dnp80c2dgwanzm3mgg7pb") (y #t)))

(define-public crate-make-makefile-0.1.1 (c (n "make-makefile") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sgg3xgk5qq59119z1yq0rcjf4rr8q0mk019xyvmn8lq5qyxiia3") (y #t)))

(define-public crate-make-makefile-0.1.2 (c (n "make-makefile") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b14n7byvhgk4ddhm0allwy37wrhppjdcx6j2n43cxsaz1gilpyf")))

