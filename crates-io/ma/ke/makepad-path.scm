(define-module (crates-io ma ke makepad-path) #:use-module (crates-io))

(define-public crate-makepad-path-0.1.0 (c (n "makepad-path") (v "0.1.0") (d (list (d (n "makepad-geometry") (r "^0.1") (d #t) (k 0)) (d (n "makepad-internal-iter") (r "^0.1") (d #t) (k 0)))) (h "0h0xl5iapl37w0zqglx24npiahyb6ls3acfbw4m3i4sc08smyn66")))

