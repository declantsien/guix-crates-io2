(define-module (crates-io ma ke makepad-math) #:use-module (crates-io))

(define-public crate-makepad-math-0.3.0 (c (n "makepad-math") (v "0.3.0") (h "1pbcb1qbhw6mk2z71li9knc0rc1xa5rrqyc91d9nplfmvzsm5x8y")))

(define-public crate-makepad-math-0.4.0 (c (n "makepad-math") (v "0.4.0") (h "1n7mp3iymz4xni4jkx1rplyhzf92sx7rld2dx4w7y738casv28qw")))

