(define-module (crates-io ma ke makectl) #:use-module (crates-io))

(define-public crate-makectl-0.1.0 (c (n "makectl") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00iimggy284x6rwy43jk2bakzm832mw1xv9mj0awdm8dcwn1fzfm")))

(define-public crate-makectl-0.1.1 (c (n "makectl") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1a6bscffvm7rgn7ka469f4q9f7w1s3ndl1pc0w3wwraqqf96pc50")))

