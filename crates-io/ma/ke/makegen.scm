(define-module (crates-io ma ke makegen) #:use-module (crates-io))

(define-public crate-makegen-0.1.0 (c (n "makegen") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0lh4iaz80ris6hc81pdirkxfx06ccy5654xyii5rfh94zks4s3ws")))

(define-public crate-makegen-0.1.1 (c (n "makegen") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0cz3yladfzaiz23921m96yxg8pzv0wdhhmr1g018cppnjdkpsj19")))

(define-public crate-makegen-0.2.0 (c (n "makegen") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1q21k0lqdgc2yqcllsghxsc2q5c5kjway53n6q5z1rrm0c698xw2")))

(define-public crate-makegen-0.2.1 (c (n "makegen") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1jvv11n15gkwj0j68fiv7phgk2fx2k97nnvxl9k00yhircbqq1rb")))

(define-public crate-makegen-0.2.2 (c (n "makegen") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1qjfp4rscjshidsn1gkmcyi5rayj3dvvdj8qn58k0gqygcxg8lp4")))

(define-public crate-makegen-0.2.3 (c (n "makegen") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "12ih8bcmwm4c0qa7kkssr3dxw0wd96222n3w8jbdgngh5c23p4cl")))

(define-public crate-makegen-0.2.4 (c (n "makegen") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1s2f6pqgbag0cy7l15jljw4gzfb7zgvlihszm3lfr7ix2rw1vz5j")))

(define-public crate-makegen-0.2.5 (c (n "makegen") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1yxlmd99srw3vzicz1vp58wh216aibsi4d8fwz8kk97w5gm63s3b")))

(define-public crate-makegen-0.2.6 (c (n "makegen") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "02bvly2i3l36xav7cb1v7zg0x838h4575kjnynp2ndrcf2wr8y6c")))

