(define-module (crates-io ma ke makepad-vector) #:use-module (crates-io))

(define-public crate-makepad-vector-0.3.0 (c (n "makepad-vector") (v "0.3.0") (h "1id4mbw8135ir9xvimw2ap4n0fl1my5b0spna4j92aq7l1j5a2m8")))

(define-public crate-makepad-vector-0.4.0 (c (n "makepad-vector") (v "0.4.0") (d (list (d (n "ttf-parser") (r "^0.19") (f (quote ("opentype-layout"))) (k 0)))) (h "071xjvspyc4wz93k0dbj2v15g1y7kmvlmk9argbpxnpcp8fjiz8x")))

