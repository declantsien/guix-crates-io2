(define-module (crates-io ma ke makepad-draw-2d) #:use-module (crates-io))

(define-public crate-makepad-draw-2d-0.3.0 (c (n "makepad-draw-2d") (v "0.3.0") (d (list (d (n "makepad-image-formats") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-platform") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-vector") (r "^0.3.0") (d #t) (k 0)))) (h "136w22x4bcc387ad04ljcn7vdyy0fv9drgwl430nzf79n854zqxr") (f (quote (("nightly" "makepad-platform/nightly" "makepad-image-formats/nightly"))))))

