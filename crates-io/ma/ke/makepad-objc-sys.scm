(define-module (crates-io ma ke makepad-objc-sys) #:use-module (crates-io))

(define-public crate-makepad-objc-sys-0.2.7 (c (n "makepad-objc-sys") (v "0.2.7") (h "113vck6lmz07mb27l3xwmhhy09gs53wbi9v4jb53hf6ldi0a1900")))

(define-public crate-makepad-objc-sys-0.3.0 (c (n "makepad-objc-sys") (v "0.3.0") (h "0i2bk39givykdlyfbv2qh5c46x5ihp5xijib61agh02k538n07jj")))

(define-public crate-makepad-objc-sys-0.4.0 (c (n "makepad-objc-sys") (v "0.4.0") (h "1fg871c94shhdfdqr47032vydwd06il5jab2zgghxdhjdfcxwn64")))

