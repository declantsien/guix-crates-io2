(define-module (crates-io ma ke makereadme) #:use-module (crates-io))

(define-public crate-makereadme-0.2.0 (c (n "makereadme") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.6") (f (quote ("rust-embed"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "10386bl5qy4884qirq8knzbg369wdm6j9c9h0ysrlzjm0m2gy9rk")))

(define-public crate-makereadme-0.3.0 (c (n "makereadme") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.6") (f (quote ("rust-embed"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.160") (d #t) (k 0)))) (h "166d0sypxyxxd5h7bk6mlxvhsyfb4hsx63n8hvvajg6rcqi9fwqr")))

