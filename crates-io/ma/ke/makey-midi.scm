(define-module (crates-io ma ke makey-midi) #:use-module (crates-io))

(define-public crate-makey-midi-0.1.0 (c (n "makey-midi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "09b35wd0pcqmw3xbfgbs5bygn9l376hnbgn8fgfi2c6pqq4dy5z7")))

