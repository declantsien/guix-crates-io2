(define-module (crates-io ma ke makepad-derive-widget) #:use-module (crates-io))

(define-public crate-makepad-derive-widget-0.3.0 (c (n "makepad-derive-widget") (v "0.3.0") (d (list (d (n "makepad-live-id") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-micro-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "08s9vh9n2bbmxgji9xm53aw2f8cv55013gqscphbxgnc78qrsn17") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-makepad-derive-widget-0.4.0 (c (n "makepad-derive-widget") (v "0.4.0") (d (list (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0sd144wxbcgy0zmy2sz72ag1rqf74qm8h4pkmyzwniwxm8kq4yil") (f (quote (("nightly") ("default" "nightly"))))))

