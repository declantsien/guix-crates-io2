(define-module (crates-io ma ke makepad-futures-legacy) #:use-module (crates-io))

(define-public crate-makepad-futures-legacy-0.4.0 (c (n "makepad-futures-legacy") (v "0.4.0") (h "1zwdv7x19na3w49brwmpqb6w3h3sa8ma4iv4f8w2zcfyvjssdq45")))

(define-public crate-makepad-futures-legacy-0.5.0 (c (n "makepad-futures-legacy") (v "0.5.0") (h "069kc26apsmibim6nd8wvjg1vv7zygic3fhfa0pnkhvlnvwqbl2b")))

(define-public crate-makepad-futures-legacy-0.6.0 (c (n "makepad-futures-legacy") (v "0.6.0") (h "1nks48h1hwcnqy9ln14ghc33042m9vlf0g9ccwfcr92lfmsiv15y")))

