(define-module (crates-io ma ke makepad-derive-wasm-bridge) #:use-module (crates-io))

(define-public crate-makepad-derive-wasm-bridge-0.3.0 (c (n "makepad-derive-wasm-bridge") (v "0.3.0") (d (list (d (n "makepad-micro-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0h0vaw5h0h3maagpry7xgis78j8hddd356y3q9i1ckgs8iy5ri00") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-makepad-derive-wasm-bridge-0.4.0 (c (n "makepad-derive-wasm-bridge") (v "0.4.0") (d (list (d (n "makepad-micro-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "1rw81vh9a1472gy3b7pwv18hn2i6vd8mbp00mp6wnfaam03pjj3c") (f (quote (("nightly") ("default" "nightly"))))))

