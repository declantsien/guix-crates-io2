(define-module (crates-io ma ke makepad-font) #:use-module (crates-io))

(define-public crate-makepad-font-0.1.0 (c (n "makepad-font") (v "0.1.0") (d (list (d (n "makepad-geometry") (r "^0.1") (d #t) (k 0)) (d (n "makepad-internal-iter") (r "^0.1") (d #t) (k 0)) (d (n "makepad-path") (r "^0.1") (d #t) (k 0)))) (h "1pys90pdf8rgvgq00fz801m08ny6nby2va098rw4ncipfxvrrrnp")))

