(define-module (crates-io ma ke make-sys) #:use-module (crates-io))

(define-public crate-make-sys-0.1.0 (c (n "make-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "09xzfrvz6kwdl68462aijwhvli7hyb9hxg955649lk2mrw9462sk")))

(define-public crate-make-sys-0.1.1 (c (n "make-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "17ilac3yq2sa7xrj4b7vb84xav5ajpfvav75bm17gq7yjw4g0z24")))

