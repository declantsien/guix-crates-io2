(define-module (crates-io ma ke makepad-live-id) #:use-module (crates-io))

(define-public crate-makepad-live-id-0.3.0 (c (n "makepad-live-id") (v "0.3.0") (d (list (d (n "makepad-error-log") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-live-id-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0r8v73sn3jdxpky0lqi0c9k3g9rhwfs14ypqrlimka0s1jnxsd2w")))

(define-public crate-makepad-live-id-0.4.0 (c (n "makepad-live-id") (v "0.4.0") (d (list (d (n "makepad-error-log") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-live-id-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0wd4clw4fmli85hlfsqkv2csflqdng1hdf51kbaa83knv692zr9d")))

