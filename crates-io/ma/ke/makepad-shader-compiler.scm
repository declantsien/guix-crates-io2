(define-module (crates-io ma ke makepad-shader-compiler) #:use-module (crates-io))

(define-public crate-makepad-shader-compiler-0.3.0 (c (n "makepad-shader-compiler") (v "0.3.0") (d (list (d (n "makepad-live-compiler") (r "^0.3.0") (d #t) (k 0)))) (h "0wxa104sn59xnj7rkick9z6z2y04qg5gllznp4inhfgcwhsnkzza") (f (quote (("nightly" "makepad-live-compiler/nightly"))))))

(define-public crate-makepad-shader-compiler-0.4.0 (c (n "makepad-shader-compiler") (v "0.4.0") (d (list (d (n "makepad-live-compiler") (r "^0.4.0") (d #t) (k 0)))) (h "0k7bgmkm4l7k26vhymkkyi9k1h0rv15da16m3nikzfvrfmiyzhj1")))

(define-public crate-makepad-shader-compiler-0.5.0 (c (n "makepad-shader-compiler") (v "0.5.0") (d (list (d (n "makepad-live-compiler") (r "^0.5.0") (d #t) (k 0)))) (h "173nl1kvaffnkx0rh3m2i5xanc8cfmvcwrcxp5i70i42zwjygp01")))

