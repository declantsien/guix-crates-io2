(define-module (crates-io ma ke makepad-hub) #:use-module (crates-io))

(define-public crate-makepad-hub-0.1.0 (c (n "makepad-hub") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "051yq9xqzis8dk3p76055sfvably64my11jbppx4i7j0h3q7wliw")))

