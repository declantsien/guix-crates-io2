(define-module (crates-io ma ke make_tuple_traits) #:use-module (crates-io))

(define-public crate-make_tuple_traits-0.1.0 (c (n "make_tuple_traits") (v "0.1.0") (h "0mv7xbvv3cwqiy3qhy3hb85w0l4yasj6lk6b13y5vry6lvmkz54d")))

(define-public crate-make_tuple_traits-0.2.0 (c (n "make_tuple_traits") (v "0.2.0") (h "196w5g7n0ydjfg4c1wqfij1cim7zvxpravfs13dwdzs3d6s2152f")))

(define-public crate-make_tuple_traits-0.2.1 (c (n "make_tuple_traits") (v "0.2.1") (h "1j68rwpgybnb3x9kl492dmr71h8hh7ni5nkjv2191pzy245p635j")))

(define-public crate-make_tuple_traits-0.2.2 (c (n "make_tuple_traits") (v "0.2.2") (h "0hlihlf8i7faqwy81v8rgngnrvm4d26xbam0m594wj2c9lh3lpxm")))

