(define-module (crates-io ma ke make-fabric-mod) #:use-module (crates-io))

(define-public crate-make-fabric-mod-0.1.0 (c (n "make-fabric-mod") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "06ljj5q345y1g4lf9gr0lrsf4k2kw7561g3zz7w20bqbczwa15g5")))

(define-public crate-make-fabric-mod-0.1.1 (c (n "make-fabric-mod") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0qhc5i0azxd3ndp6q0ynprxyf0zcv59xzqspr5l9qv2zk6y0b1h6")))

