(define-module (crates-io ma ke makepad-zune-png) #:use-module (crates-io))

(define-public crate-makepad-zune-png-0.2.1 (c (n "makepad-zune-png") (v "0.2.1") (d (list (d (n "makepad-zune-core") (r "^0.2") (d #t) (k 0)) (d (n "makepad-zune-inflate") (r "^0.2") (f (quote ("zlib"))) (k 0)))) (h "10pdd55ip81yh1d9bdiwzhqy1ky0cya1q943zr405nv6bzz0x9af") (f (quote (("std" "makepad-zune-core/std") ("sse") ("default" "sse" "std"))))))

