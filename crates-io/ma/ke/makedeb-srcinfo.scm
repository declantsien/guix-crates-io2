(define-module (crates-io ma ke makedeb-srcinfo) #:use-module (crates-io))

(define-public crate-makedeb-srcinfo-0.6.0 (c (n "makedeb-srcinfo") (v "0.6.0") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "05vf0jgnmmffdgm52h6xkxwwi01nlmxj5lxy95hc5znj18wjlf43")))

(define-public crate-makedeb-srcinfo-0.6.1 (c (n "makedeb-srcinfo") (v "0.6.1") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "187gmlbi6h2r701358rl8mwrnh4kcxwc98p9g5lficxqg5gdqysq")))

(define-public crate-makedeb-srcinfo-0.6.2 (c (n "makedeb-srcinfo") (v "0.6.2") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "049yk84a4cwiki5dha04x8r0pjnrpdcjlq2ziys4hp8ky54xlj06")))

(define-public crate-makedeb-srcinfo-0.7.0 (c (n "makedeb-srcinfo") (v "0.7.0") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "18k1c6iln526ijm2a7c3ipmqx9b3q260s4wm6vapcbmj1wx65fn6")))

(define-public crate-makedeb-srcinfo-0.7.1 (c (n "makedeb-srcinfo") (v "0.7.1") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "19w690zdnyc81acq20m1hz3lkr73afbgid6fd53a0fy7r16435dd") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-makedeb-srcinfo-0.8.0 (c (n "makedeb-srcinfo") (v "0.8.0") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1rn7l89p1hcrb5nas7k5y5sd5zp4szccj20h2j9s6nsls0sw20bb") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-makedeb-srcinfo-0.8.1 (c (n "makedeb-srcinfo") (v "0.8.1") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1yypl3fqnfrsm7k2jmm3zjr26lamhql6r6zag6a9nxxclikj5dvm") (s 2) (e (quote (("python" "dep:pyo3"))))))

