(define-module (crates-io ma ke makepad) #:use-module (crates-io))

(define-public crate-makepad-0.1.0 (c (n "makepad") (v "0.1.0") (d (list (d (n "makepad-hub") (r "^0.1") (d #t) (k 0)) (d (n "makepad-render") (r "^0.1") (d #t) (k 0)) (d (n "makepad-widget") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15njcvbk64k7mygk1qwkz1820vv92fz3ai0mqkh1fk1p7xi92kvl")))

