(define-module (crates-io ma ke makero) #:use-module (crates-io))

(define-public crate-makero-0.1.0 (c (n "makero") (v "0.1.0") (h "041v8r6papi9gka6f2xhyh83vgc9dyym35gsb4fpm7is0n7iarg5")))

(define-public crate-makero-0.1.1 (c (n "makero") (v "0.1.1") (h "0hmwf9jjam1kddlqkkl8ncxdgzab26y3z16aq6kng6wmfzab5mqg")))

(define-public crate-makero-0.1.2 (c (n "makero") (v "0.1.2") (h "0clr884cmcacs9jjwh1q1sc1n3g1y9przfys4yh6ycxg4gpmdhmx")))

