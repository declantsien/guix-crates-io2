(define-module (crates-io ma ke makefile-lossless) #:use-module (crates-io))

(define-public crate-makefile-lossless-0.1.0 (c (n "makefile-lossless") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "0dyz4c2zdfqvcxw1wv2cixajy9km18k1nh39zpl1nnlz020ikyb3")))

(define-public crate-makefile-lossless-0.1.1 (c (n "makefile-lossless") (v "0.1.1") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "0p5k6va7p0m99hz2j5shsamfzv5smrbij3kp61xpgjksyiblfflq")))

(define-public crate-makefile-lossless-0.1.2 (c (n "makefile-lossless") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "00niq1c2wni1z1k7wx2l89969ggqrgbfhn2icnfc3r38k8nhlppx")))

