(define-module (crates-io ma ke makepad-micro-serde) #:use-module (crates-io))

(define-public crate-makepad-micro-serde-0.3.0 (c (n "makepad-micro-serde") (v "0.3.0") (d (list (d (n "makepad-micro-serde-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1vrwari03hra1g08f81ksxgib6nxnqzlgv9857w5vsmvdzagx27m")))

(define-public crate-makepad-micro-serde-0.4.0 (c (n "makepad-micro-serde") (v "0.4.0") (d (list (d (n "makepad-micro-serde-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1dgqayq1dlxc1wa5k0y1mcik9inaw5jk1qr560r0b3sjydfllvrk")))

