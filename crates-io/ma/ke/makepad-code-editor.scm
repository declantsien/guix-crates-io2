(define-module (crates-io ma ke makepad-code-editor) #:use-module (crates-io))

(define-public crate-makepad-code-editor-0.4.0 (c (n "makepad-code-editor") (v "0.4.0") (d (list (d (n "makepad-widgets") (r "^0.4.0") (d #t) (k 0)))) (h "1ffzdp3b8zmsk8qb66gazr4y2i4d2qm9n3shl3j597idvigfnwp4")))

(define-public crate-makepad-code-editor-0.5.0 (c (n "makepad-code-editor") (v "0.5.0") (d (list (d (n "makepad-widgets") (r "^0.5.0") (d #t) (k 0)))) (h "0xvrdinxfap65q9sl7k7anwnmanhg5i9fqwv0j9sr49qdiw1ixwd")))

(define-public crate-makepad-code-editor-0.6.0 (c (n "makepad-code-editor") (v "0.6.0") (d (list (d (n "makepad-widgets") (r "^0.6.0") (d #t) (k 0)))) (h "09wzb1a22w4rlwl1y8dcq3s35lbjyslqfmv070q5fd70j9vfx4j6")))

