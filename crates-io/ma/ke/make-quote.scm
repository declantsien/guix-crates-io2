(define-module (crates-io ma ke make-quote) #:use-module (crates-io))

(define-public crate-make-quote-0.1.0 (c (n "make-quote") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0xv0isyqbvr1m7k0q652s7v0pxk06fgxpp06v84hfb2m0z2492xx")))

(define-public crate-make-quote-0.2.0 (c (n "make-quote") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0kx5qk979gyx36dz3pdpyn142pwwlanv074qps3f5ny0rv2jxsby")))

(define-public crate-make-quote-0.2.1 (c (n "make-quote") (v "0.2.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "08q7rs5s7xkdv8mpgdav73vqlf8wag7srm6hp6wc4fi8azwamxzq")))

(define-public crate-make-quote-0.2.2 (c (n "make-quote") (v "0.2.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "1fmzdd7k2c5gkpfbw0bv4b8548afbhhr5wfs7fdqdjq6xa9zxjfd")))

(define-public crate-make-quote-0.2.3 (c (n "make-quote") (v "0.2.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "03i8pxkla99y2ap5l8p54zb728zcajggrmkgkinsv1bf3fx1d7q5")))

(define-public crate-make-quote-0.2.4 (c (n "make-quote") (v "0.2.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "15glkxqmcfk52sxkn2348a6x9jvs5fwlscc2iw598j77pmzfdqbb")))

(define-public crate-make-quote-0.3.0 (c (n "make-quote") (v "0.3.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0mg01p949ff5ydqkb4hvdm8wyq5ga28j3y13f72d4kg2fhis0kxm")))

(define-public crate-make-quote-0.3.1 (c (n "make-quote") (v "0.3.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "00v6zk7yhc69qahwrn6vrq6cxkdaq1s9m4vqh2wdadw23mi4my88")))

(define-public crate-make-quote-0.3.2 (c (n "make-quote") (v "0.3.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "1alvdgz9ibv6i5md8sp3aj28ybqsqxfypkb144fxsqx61jbnsvkv")))

(define-public crate-make-quote-0.3.3 (c (n "make-quote") (v "0.3.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "14rjs05kp5ks48jn9qpknk95nkvy42b7b9dabm5wmg0kvd0jz3vv")))

(define-public crate-make-quote-0.4.0 (c (n "make-quote") (v "0.4.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0hnmjb9aqip6b84ypsjn9pbg8rlq6nnk8942z3ibyinf9ppzbamn")))

(define-public crate-make-quote-0.5.0 (c (n "make-quote") (v "0.5.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "124qplkqxx8yy5p3misrwid7jiznq7rxslyrwjagjy48ijsv39k8")))

(define-public crate-make-quote-0.5.1 (c (n "make-quote") (v "0.5.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0h4j5z84ygy8g44mbf778iwi5h7ym6pkgd3lclzj5z21bf488vpx")))

(define-public crate-make-quote-0.5.2 (c (n "make-quote") (v "0.5.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.14.0") (d #t) (k 0)))) (h "0y2nckw18s6lrwfms5l7rxkw7sjv2vvwbvlgqgjz7j9r1vi6w0xc")))

(define-public crate-make-quote-0.5.3 (c (n "make-quote") (v "0.5.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.1") (d #t) (k 0)))) (h "0nhqcw41clyk4b1lab96b8njvmggrdsvndlvfjmb158p23q4aylw")))

