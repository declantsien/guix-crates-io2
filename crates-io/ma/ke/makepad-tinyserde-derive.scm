(define-module (crates-io ma ke makepad-tinyserde-derive) #:use-module (crates-io))

(define-public crate-makepad-tinyserde-derive-0.1.0 (c (n "makepad-tinyserde-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09wlff73fvymvx4haw3dd004l49rylh34927xdy57g74xki8zp5s")))

