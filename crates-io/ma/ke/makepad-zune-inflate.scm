(define-module (crates-io ma ke makepad-zune-inflate) #:use-module (crates-io))

(define-public crate-makepad-zune-inflate-0.2.54 (c (n "makepad-zune-inflate") (v "0.2.54") (d (list (d (n "simd-adler32") (r "^0.3.4") (o #t) (k 0)))) (h "0d2zfs9v6abkpbxh56wxmnlbzddais1pf3nrnapl1k8pdjc6fg4q") (f (quote (("zlib" "simd-adler32") ("std" "simd-adler32/std") ("gzip") ("default" "zlib" "gzip" "std"))))))

