(define-module (crates-io ma ke makepad-synth-ironfish) #:use-module (crates-io))

(define-public crate-makepad-synth-ironfish-0.4.0 (c (n "makepad-synth-ironfish") (v "0.4.0") (d (list (d (n "makepad-audio-graph") (r "^0.4.0") (d #t) (k 0)))) (h "1pg7cs5hhn7p54c3smada6374mm9l8lxc83m805cygn8cqnmvpgw")))

(define-public crate-makepad-synth-ironfish-0.5.0 (c (n "makepad-synth-ironfish") (v "0.5.0") (d (list (d (n "makepad-audio-graph") (r "^0.5.0") (d #t) (k 0)))) (h "0mzskq6khaks681mlx30n5y40vrxvsah6a3aakar2144blnzdg1w")))

(define-public crate-makepad-synth-ironfish-0.6.0 (c (n "makepad-synth-ironfish") (v "0.6.0") (d (list (d (n "makepad-audio-graph") (r "^0.6.0") (d #t) (k 0)))) (h "0yiiz9spylvzydrr80kgq2291kkab11zj6yg19nq0wnk3zlaf81k")))

