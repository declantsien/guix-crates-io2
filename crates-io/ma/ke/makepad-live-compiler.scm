(define-module (crates-io ma ke makepad-live-compiler) #:use-module (crates-io))

(define-public crate-makepad-live-compiler-0.3.0 (c (n "makepad-live-compiler") (v "0.3.0") (d (list (d (n "makepad-derive-live") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-live-tokenizer") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-math") (r "^0.3.0") (d #t) (k 0)))) (h "1d3hkkz0wanxb5lafm8v9mx6ryn5nklfxidb7h27nkzw0zwc2kxq") (f (quote (("nightly" "makepad-derive-live/nightly"))))))

(define-public crate-makepad-live-compiler-0.4.0 (c (n "makepad-live-compiler") (v "0.4.0") (d (list (d (n "makepad-derive-live") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-live-tokenizer") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-math") (r "^0.4.0") (d #t) (k 0)))) (h "1dgzsh73cqk9q711p9vrqp6b9vwpb577jp7p4cryi2nlvd5mjzhk")))

(define-public crate-makepad-live-compiler-0.5.0 (c (n "makepad-live-compiler") (v "0.5.0") (d (list (d (n "makepad-derive-live") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-live-tokenizer") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-math") (r "^0.4.0") (d #t) (k 0)))) (h "0apyq34896nqcvrwarw0mafnr1g6d0sd56k0jzszwnqb0bs3nnh9")))

