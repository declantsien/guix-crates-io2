(define-module (crates-io ma ke make_fields) #:use-module (crates-io))

(define-public crate-make_fields-0.1.0 (c (n "make_fields") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "devise") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1isgf1g67swvhzlj49g877spc8v7m93blj2nzdzkz3pwg1x95r7c")))

