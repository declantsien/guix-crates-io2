(define-module (crates-io ma ke makeit) #:use-module (crates-io))

(define-public crate-makeit-0.1.0 (c (n "makeit") (v "0.1.0") (d (list (d (n "makeit-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1sfgr25nh30ra6lqw5bcgk39a77vdkd6d77mhf7mm2dzmx4aqclc")))

(define-public crate-makeit-0.1.1 (c (n "makeit") (v "0.1.1") (d (list (d (n "makeit-derive") (r "^0.1.1") (d #t) (k 0)))) (h "03wfw0naxki37ypnlh80x8i89q85mmlixwdkxv2ilsgr7gmpavcf")))

