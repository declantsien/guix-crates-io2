(define-module (crates-io ma ke makepad-audio-widgets) #:use-module (crates-io))

(define-public crate-makepad-audio-widgets-0.4.0 (c (n "makepad-audio-widgets") (v "0.4.0") (d (list (d (n "makepad-widgets") (r "^0.4.0") (d #t) (k 0)))) (h "0414mvyn21ql3nf1qmj59kqy38551vk5ar5wys9w400sxzw44ddw")))

(define-public crate-makepad-audio-widgets-0.5.0 (c (n "makepad-audio-widgets") (v "0.5.0") (d (list (d (n "makepad-widgets") (r "^0.5.0") (d #t) (k 0)))) (h "0r493rc48yfvnn14w8w4l87zx9swsbsh26xzwl6ky3zcimpzql75")))

(define-public crate-makepad-audio-widgets-0.6.0 (c (n "makepad-audio-widgets") (v "0.6.0") (d (list (d (n "makepad-widgets") (r "^0.6.0") (d #t) (k 0)))) (h "13934565k7bw6mc5s4512950c0vqjsxhw8bx25vhhl6r6z3hq9vg")))

