(define-module (crates-io ma ke makepad-zune-core) #:use-module (crates-io))

(define-public crate-makepad-zune-core-0.2.14 (c (n "makepad-zune-core") (v "0.2.14") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "10m3x42c4ac6zpksx9ffvnkw9w4irdyzqzvxi1bq2s9fck0rxwkj") (f (quote (("std"))))))

