(define-module (crates-io ma ke makeppkg) #:use-module (crates-io))

(define-public crate-makeppkg-1.0.0 (c (n "makeppkg") (v "1.0.0") (d (list (d (n "checksums") (r "^0.5.5") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "184lzznswn3kimsimaxbwbcag0mq00gcjm1yd6i22ql88r5qf79c")))

(define-public crate-makeppkg-1.1.0 (c (n "makeppkg") (v "1.1.0") (d (list (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "md5") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1a88h6xz078f4vp37hvzdl7bf7fqnhqxkapks0ik7pwynhg8slb1")))

(define-public crate-makeppkg-1.1.1 (c (n "makeppkg") (v "1.1.1") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1rhi7i186jia4k5058crhr3c0hv3sgp83vyzrab2xplxcqn648fk")))

