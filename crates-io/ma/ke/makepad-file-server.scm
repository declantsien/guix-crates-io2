(define-module (crates-io ma ke makepad-file-server) #:use-module (crates-io))

(define-public crate-makepad-file-server-0.4.0 (c (n "makepad-file-server") (v "0.4.0") (d (list (d (n "makepad-file-protocol") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.4.0") (d #t) (k 0)))) (h "139dqmj4whky87v96ggy922rz9ijkrdrx7gcwvm6d1ds44l32r5i")))

(define-public crate-makepad-file-server-0.5.0 (c (n "makepad-file-server") (v "0.5.0") (d (list (d (n "makepad-file-protocol") (r "^0.5.0") (d #t) (k 0)) (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.4.0") (d #t) (k 0)))) (h "16wxps5zsc525lkawjagfxysamjl3n86k16ls2b5bk71apf3x0xw")))

