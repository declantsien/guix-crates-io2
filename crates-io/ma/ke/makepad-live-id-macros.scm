(define-module (crates-io ma ke makepad-live-id-macros) #:use-module (crates-io))

(define-public crate-makepad-live-id-macros-0.3.0 (c (n "makepad-live-id-macros") (v "0.3.0") (d (list (d (n "makepad-micro-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0y04v78gvspa189k5jb337qrfqvxpfxrr23hgsq9i7khzaca896s")))

(define-public crate-makepad-live-id-macros-0.4.0 (c (n "makepad-live-id-macros") (v "0.4.0") (d (list (d (n "makepad-micro-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "1h1gikmxyf36j1hr7z2s8jwp5qw30bq6m0n0vrsw679jcd49vipc")))

