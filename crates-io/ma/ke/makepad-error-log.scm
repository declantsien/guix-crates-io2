(define-module (crates-io ma ke makepad-error-log) #:use-module (crates-io))

(define-public crate-makepad-error-log-0.3.0 (c (n "makepad-error-log") (v "0.3.0") (h "0ixddr8i58ggqfgmssx3dd15z29s5vvkl0z2rvjyskg8bl6ci75r") (f (quote (("nightly"))))))

(define-public crate-makepad-error-log-0.4.0 (c (n "makepad-error-log") (v "0.4.0") (h "00hphf4gyffi8ndn665chklqbg929yazl3z24z8kkv830b2v92cx") (f (quote (("nightly"))))))

