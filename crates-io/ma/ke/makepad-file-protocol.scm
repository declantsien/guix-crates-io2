(define-module (crates-io ma ke makepad-file-protocol) #:use-module (crates-io))

(define-public crate-makepad-file-protocol-0.4.0 (c (n "makepad-file-protocol") (v "0.4.0") (d (list (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.4.0") (d #t) (k 0)))) (h "16p4k8kcv34q8c7nkqw0qq6xnzp7vmm3rc0wxsgpaw4zyw9qrjrv")))

(define-public crate-makepad-file-protocol-0.5.0 (c (n "makepad-file-protocol") (v "0.5.0") (d (list (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-serde") (r "^0.4.0") (d #t) (k 0)))) (h "00sffkr8w4ggyh15q1bpvdw22157gqj70p988hcxicaxjv7ny68a")))

