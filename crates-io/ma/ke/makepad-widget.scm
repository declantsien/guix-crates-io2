(define-module (crates-io ma ke makepad-widget) #:use-module (crates-io))

(define-public crate-makepad-widget-0.1.0 (c (n "makepad-widget") (v "0.1.0") (d (list (d (n "makepad-render") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14mxlkvmmiwfgpy57mxlgd274c3cyv68kx9h8hsn1lvhg345m4j1")))

