(define-module (crates-io ma ke makepad-derive-live) #:use-module (crates-io))

(define-public crate-makepad-derive-live-0.3.0 (c (n "makepad-derive-live") (v "0.3.0") (d (list (d (n "makepad-live-id") (r "^0.3.0") (d #t) (k 0)) (d (n "makepad-micro-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0qbv8mgs890x2dyn316i6brd1590isc463amdwpayvailvnbizzv") (f (quote (("nightly"))))))

(define-public crate-makepad-derive-live-0.4.0 (c (n "makepad-derive-live") (v "0.4.0") (d (list (d (n "makepad-live-id") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-micro-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "14q8h02n4ld3f8dv4xvgqcgnwph62ny9x0qnmx4dgsr5g5wskvd7")))

