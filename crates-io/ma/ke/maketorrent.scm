(define-module (crates-io ma ke maketorrent) #:use-module (crates-io))

(define-public crate-maketorrent-0.1.0 (c (n "maketorrent") (v "0.1.0") (d (list (d (n "bip_metainfo") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("color"))) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0") (d #t) (k 0)))) (h "0jav157fycl4pg1h9pshb52c4avqi7vr1vid40zgfc7anj06nprj")))

