(define-module (crates-io ma ge magenta) #:use-module (crates-io))

(define-public crate-magenta-0.1.0 (c (n "magenta") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "magenta-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0q47rpwz020dl8ika3wl4hrjndvacmcb998ahm26xn9vc2ars5cj")))

(define-public crate-magenta-0.1.1 (c (n "magenta") (v "0.1.1") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "magenta-sys") (r "^0.1.1") (d #t) (k 0)))) (h "09y57bz82b2hgbxing81ccsm126fdydvr57pclcnf3j8hrl37w2b")))

(define-public crate-magenta-0.2.0 (c (n "magenta") (v "0.2.0") (d (list (d (n "magenta-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0r2ayljcvwm90sg2rn8wbwxfxv9wyrg3i5z9j4g5vrqgzpsfki3l")))

