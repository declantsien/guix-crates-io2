(define-module (crates-io ma ge magenta-sys) #:use-module (crates-io))

(define-public crate-magenta-sys-0.1.0 (c (n "magenta-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "0wkvdcq081k247bmc5swn5g19sg6bzcnk1v3hx1s5x8rd3fiiz87")))

(define-public crate-magenta-sys-0.1.1 (c (n "magenta-sys") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "16a6bzbsimwsni9i6w1yyy088jpapw16mxz252p71i0s073i9l20")))

(define-public crate-magenta-sys-0.2.0 (c (n "magenta-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "027a4wgrsyb6sypp24ipyxczw2pq46ca0xljxch2scsx0kqbzhqg")))

