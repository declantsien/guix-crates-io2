(define-module (crates-io ma ge mage) #:use-module (crates-io))

(define-public crate-mage-0.1.0 (c (n "mage") (v "0.1.0") (d (list (d (n "eval") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1zwcjbrga854qcnjdnrs7i4risz0mk66zf5mmd1fz5xv9srvrgnr")))

(define-public crate-mage-0.1.1 (c (n "mage") (v "0.1.1") (d (list (d (n "eval") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "19yppi9kbfm63v87yspnivg35lxddjgg6l2ih2vy50czaqznz3rb") (f (quote (("unstable"))))))

(define-public crate-mage-0.2.0 (c (n "mage") (v "0.2.0") (d (list (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "06a3z22lr2kxwfcdhvzdrb3ai68zayifq23kw767ql8bx8yfbrxl") (f (quote (("unstable"))))))

