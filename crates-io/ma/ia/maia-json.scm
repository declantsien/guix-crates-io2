(define-module (crates-io ma ia maia-json) #:use-module (crates-io))

(define-public crate-maia-json-0.1.0 (c (n "maia-json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ch62kp8vcdc0dfbl36h60jdh85qplqlnb5v3ch8fsm1fbk2xgw")))

(define-public crate-maia-json-0.1.1 (c (n "maia-json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gxiq953g7hxcadin7g9x7cffp4ws4wnac8gz8s0wq63gyiar5v0")))

(define-public crate-maia-json-0.2.0 (c (n "maia-json") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fc4593x7y69f1b7dvn5zk8zds5ail7x0hdar0k11gr8lznkjs0p")))

(define-public crate-maia-json-0.3.0 (c (n "maia-json") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iq2gg1jv7vhd6qa4djvfhphh98p6cn09p6wl4j2ckcw31l78fpf")))

(define-public crate-maia-json-0.4.0 (c (n "maia-json") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mc90csgv35xgmcrj77gmfwljgp5f6v23hvx0cw1mw7mjwqyk2qk")))

