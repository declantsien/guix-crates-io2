(define-module (crates-io ma ia maia-pac) #:use-module (crates-io))

(define-public crate-maia-pac-0.1.0 (c (n "maia-pac") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08sws2mg2f0g1lkcgjmbzhqxv9pman4a76cmz59r7gnvcf0vk69r")))

(define-public crate-maia-pac-0.2.0 (c (n "maia-pac") (v "0.2.0") (d (list (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1llx2l1dfsb3lxwih94sf4qvya6zpgqdildcf0xj50dlkjrnn1kw")))

(define-public crate-maia-pac-0.3.0 (c (n "maia-pac") (v "0.3.0") (d (list (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1v29qzcpd1swnhj7crv62xk4j5760ypf3j5km2d9rqkfcpj3jknw")))

(define-public crate-maia-pac-0.4.0 (c (n "maia-pac") (v "0.4.0") (d (list (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "005bgcpy8dqd1fic74k6gybhh83cp2dl1jrnc05fc2q1r3k48kwd")))

