(define-module (crates-io ma ia maia) #:use-module (crates-io))

(define-public crate-maia-0.0.0 (c (n "maia") (v "0.0.0") (h "1lfpidsh4lch6f9f7q2a5mx2in4r3630xvyd7m86r8vfl1mfjrqc")))

(define-public crate-maia-0.1.0 (c (n "maia") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.10") (f (quote ("collections"))) (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.2") (o #t) (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1l3ap62aq8qd3dva31c3491s9hgwli8wm754zxdczqp8nrnvf5pk") (s 2) (e (quote (("window" "dep:raw-window-handle" "dep:raw-window-metal")))) (r "1.60")))

(define-public crate-maia-0.1.1 (c (n "maia") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.10") (f (quote ("collections"))) (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.2") (o #t) (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1m34rkycikygkyqp75jsws3k8zhpn17ciymxph11zwm0hd339k27") (s 2) (e (quote (("window" "dep:raw-window-handle" "dep:raw-window-metal")))) (r "1.60")))

