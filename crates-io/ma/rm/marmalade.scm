(define-module (crates-io ma rm marmalade) #:use-module (crates-io))

(define-public crate-marmalade-0.0.1 (c (n "marmalade") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("HtmlImageElement" "Window" "console" "KeyboardEvent" "HtmlCanvasElement" "CanvasRenderingContext2d" "Document"))) (d #t) (k 0)))) (h "1qvllgw78s5kzyb29n1j1s94md7viq7sgkj6k39gbjxny241jcpg")))

