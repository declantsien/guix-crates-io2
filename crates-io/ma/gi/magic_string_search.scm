(define-module (crates-io ma gi magic_string_search) #:use-module (crates-io))

(define-public crate-magic_string_search-0.1.0 (c (n "magic_string_search") (v "0.1.0") (h "0bqwgz7fn7q0w1bljg269906i8dnk81i5jklnwf1iz01m210n2nr")))

(define-public crate-magic_string_search-0.1.3 (c (n "magic_string_search") (v "0.1.3") (h "1091dpawx2lx0a150pmrsz07szxfjy89an6qd9sbw0i6iiyiz6rm")))

(define-public crate-magic_string_search-0.1.4 (c (n "magic_string_search") (v "0.1.4") (h "1h59jzaqlkwcd8p4zrwc77rq4kllm3zszklhk7qwp5khnmk29lrw")))

(define-public crate-magic_string_search-0.1.5 (c (n "magic_string_search") (v "0.1.5") (h "1b4vggl9bv6nd1q2dmp1hnbqqi4siv9dvrd0p39vnyq83idklvri")))

(define-public crate-magic_string_search-0.2.0 (c (n "magic_string_search") (v "0.2.0") (h "0wqixg2bhgvqzxq8xmn5qhfdp5d87ijcm2d0cs6417f926fvsdkj")))

