(define-module (crates-io ma gi magic-regexp) #:use-module (crates-io))

(define-public crate-magic-regexp-0.1.0 (c (n "magic-regexp") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1kj0a37m2d9fiqk83fmamimqsf00wci87259qyxy1dds0355643q")))

(define-public crate-magic-regexp-0.1.1 (c (n "magic-regexp") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1hhis197chy1s06nx0sia4r1yicslsb1q0j1b0zpk7zzi4v84zqa")))

(define-public crate-magic-regexp-0.1.2 (c (n "magic-regexp") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "07hcpmvxb5041gx85ap9lpfdkxh55xhiwig77s4mwd17w6kcnapk")))

(define-public crate-magic-regexp-0.1.3 (c (n "magic-regexp") (v "0.1.3") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "07aq7g5z8mbn50lsq4nn2v3ly4yi4nlwafyyf7njpx4wng84nv9l")))

(define-public crate-magic-regexp-0.1.4 (c (n "magic-regexp") (v "0.1.4") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "14a0qyf8kqajvwm9y9n7rxkwd8ix1ng6wxzq3hfk2iyqzja75b20")))

(define-public crate-magic-regexp-0.2.0 (c (n "magic-regexp") (v "0.2.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1h057irxvkf7hrqyafvb4a2g9nyzjpkkizk24flysx70l13280as")))

