(define-module (crates-io ma gi magikitten) #:use-module (crates-io))

(define-public crate-magikitten-0.1.0 (c (n "magikitten") (v "0.1.0") (d (list (d (n "ck-meow") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "1b3q608aprhfhmdswbydp5dn7gfx7230bc3cnxyylbsgl884cnjm")))

(define-public crate-magikitten-0.2.0 (c (n "magikitten") (v "0.2.0") (d (list (d (n "ck-meow") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "1fpl952vvlgcig7h27brfl2gx4875siagsiv505czgc4v154az17")))

