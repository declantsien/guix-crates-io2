(define-module (crates-io ma gi magick_rust) #:use-module (crates-io))

(define-public crate-magick_rust-0.4.0 (c (n "magick_rust") (v "0.4.0") (d (list (d (n "libc") (r ">= 0.2") (d #t) (k 0)))) (h "04myi17lada6rqm33wkry5pn976730vqyvby8clkwjd17h82gl3l")))

(define-public crate-magick_rust-0.5.2 (c (n "magick_rust") (v "0.5.2") (d (list (d (n "libc") (r ">= 0.2") (d #t) (k 0)))) (h "05mxfyp7ndn3d3n1qji04dmwmqygi7a9lmdrcsddxz0nyxymh926")))

(define-public crate-magick_rust-0.6.1 (c (n "magick_rust") (v "0.6.1") (d (list (d (n "libc") (r ">= 0.2") (d #t) (k 0)))) (h "0hrk0pf5mdf65xxh8nbdfd9jrjdvhxaynkfzxmhib8ii51rp17j2")))

(define-public crate-magick_rust-0.6.2 (c (n "magick_rust") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "09f6pk0zkrjm8434fnsy9q8gvfw15cr1myxl5mv3hr61n9yx591g")))

(define-public crate-magick_rust-0.6.4 (c (n "magick_rust") (v "0.6.4") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "04mpc4pgrzy3ahh5y9293xl5iszd5fcw8cbhncx8g1jcxai393bj")))

(define-public crate-magick_rust-0.6.6 (c (n "magick_rust") (v "0.6.6") (d (list (d (n "bindgen") (r "^0.25.5") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "077d55d3gqdbfr9bavfjdvdzlzbz2zf9l16r4bimnl4x6fw6v618")))

(define-public crate-magick_rust-0.7.0 (c (n "magick_rust") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1y74g5h17r2jsl8y9mmihj6xd9l1sa3hq7na4mb5hn4mz1c3aagr")))

(define-public crate-magick_rust-0.7.1 (c (n "magick_rust") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "04jn0w0i3dy59wv6nrc34s933rv9llajjlzc3fpmmivpld42fhab")))

(define-public crate-magick_rust-0.8.0 (c (n "magick_rust") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1iwdjgpx3k8mwanmmdlq61g5cl3ha789cdz7bm0cbb78vxghmcwz")))

(define-public crate-magick_rust-0.9.0 (c (n "magick_rust") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "17cjj5fpcd25k637byyn9vgfbvlpzx0g955zz6jlbx3jhynxqz1z")))

(define-public crate-magick_rust-0.10.0 (c (n "magick_rust") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0v73a8lfyndw78k7kqax1891lry0n9fapdyfi6kc8nyyy9ychih9")))

(define-public crate-magick_rust-0.11.0 (c (n "magick_rust") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3") (d #t) (k 1)))) (h "1lkny38gvspa5x2kfhfbbqm7d99d7njnl0i6wmhg9kwznqxfppsi")))

(define-public crate-magick_rust-0.12.0 (c (n "magick_rust") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3") (d #t) (k 1)))) (h "0sfklkmj8qmwjzqlr929551s112byjbmbrhq83z9xbf44k8vj3dl")))

(define-public crate-magick_rust-0.13.0 (c (n "magick_rust") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r ">= 0.2") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3") (d #t) (k 1)))) (h "0gw89jkp32y1y83ivkzsjs3pvw2hawlzs38zzcwqs4zrdr2b3p5z")))

(define-public crate-magick_rust-0.14.0 (c (n "magick_rust") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0cxxhibs9xm6zii8p844i4bpiv11hdqng89j0p21cbvfh7z5h4pg")))

(define-public crate-magick_rust-0.15.0 (c (n "magick_rust") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i2rw8z17lzjal9bdxzsd1z7xa0l99f1bvni8gp6k82cfnflqn0k") (f (quote (("disable-hdri"))))))

(define-public crate-magick_rust-0.16.0 (c (n "magick_rust") (v "0.16.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ss6rm4jaf8isg69ldrdpyfvif2q16ywznikfjjcyc23mwm9xy77") (f (quote (("disable-hdri"))))))

(define-public crate-magick_rust-0.17.0 (c (n "magick_rust") (v "0.17.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qr6bisqlh2w587ygjbi4a4r279ack1ymskg9v5jp3fwdfaqpb9i") (f (quote (("disable-hdri"))))))

(define-public crate-magick_rust-0.18.0 (c (n "magick_rust") (v "0.18.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p4jq3vbb3476rh0ss7kil64dwk2kybc7g1qs6m6q8pw58lfbxaw") (f (quote (("disable-hdri"))))))

(define-public crate-magick_rust-0.19.0 (c (n "magick_rust") (v "0.19.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05yh0ygqwvj8f4xkpx0xi33wfmvnfbs87lssc8f002g8mkv6srnf") (f (quote (("disable-hdri"))))))

(define-public crate-magick_rust-0.19.1 (c (n "magick_rust") (v "0.19.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1y0vj8m66hc7050vfm8shy0xidfkcz2pa6iahljd1mdwa29bc4n9") (f (quote (("disable-hdri") ("default" "disable-hdri"))))))

(define-public crate-magick_rust-0.20.0 (c (n "magick_rust") (v "0.20.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k79hba3ys1la0w7jxnf9lgqpl6ymphlb9gpxw8479xcy9isbkkv") (f (quote (("disable-hdri") ("default" "disable-hdri"))))))

(define-public crate-magick_rust-0.21.0 (c (n "magick_rust") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kl60hpf8blis46br99rp4cmaj8b2b4jbrr0bl1zazysv6p45hss") (f (quote (("disable-hdri") ("default" "disable-hdri"))))))

(define-public crate-magick_rust-1.0.0 (c (n "magick_rust") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0b1pqsdkahkqxryw3dh7hw1cbcmxibag2mh3fdx3armsi7s65y19") (f (quote (("disable-hdri") ("default" "disable-hdri"))))))

