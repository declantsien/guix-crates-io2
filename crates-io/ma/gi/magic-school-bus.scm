(define-module (crates-io ma gi magic-school-bus) #:use-module (crates-io))

(define-public crate-magic-school-bus-0.1.0 (c (n "magic-school-bus") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0hjjsf8x4zkzqglgjmfcvvj7g4vci233c561cv63i5lv5wlyrn9w")))

(define-public crate-magic-school-bus-0.2.0 (c (n "magic-school-bus") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0knlpkspynf7q79qqlvzfpx20297xbq0sp19i2p45ry2ap36i59v")))

(define-public crate-magic-school-bus-0.2.1 (c (n "magic-school-bus") (v "0.2.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0683x93gic187xf5bra8mqdm4xw7f57gw8smblbrv248dinjx7h2")))

(define-public crate-magic-school-bus-0.3.0 (c (n "magic-school-bus") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "1761di48v32x0ry1hlm4byyappfs5l2x0y4a8n4w655h7b5v3xb3")))

(define-public crate-magic-school-bus-0.3.1 (c (n "magic-school-bus") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0l52ikxf8kdrlj0ff2hcgnq6qc3fs6vxgm3i6ny028ig1gg72z4l")))

(define-public crate-magic-school-bus-0.4.0 (c (n "magic-school-bus") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0hcncf71b8bpbiirlsd8y1d7x8jn2l2nzkx739ryax5frs5f4ljc")))

(define-public crate-magic-school-bus-0.5.0 (c (n "magic-school-bus") (v "0.5.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossterm") (r "^0.4.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0gdd6xk8dcql1m3ydzn0ng4vngfap7j5m2vdibl19srzx9ihi4n4")))

(define-public crate-magic-school-bus-0.6.0 (c (n "magic-school-bus") (v "0.6.0") (d (list (d (n "all_term") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "1niz9yp01r8aqw9d5ggn78ki8jckhdrgjlys25kznvf0c6rkni57")))

(define-public crate-magic-school-bus-0.7.0 (c (n "magic-school-bus") (v "0.7.0") (d (list (d (n "all_term") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "090x917y5wrmgxi4yx4n1cj59qdr4fkgy9kwxvdxnqf37brrg7fn")))

