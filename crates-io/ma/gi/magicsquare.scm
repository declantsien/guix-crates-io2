(define-module (crates-io ma gi magicsquare) #:use-module (crates-io))

(define-public crate-magicsquare-0.0.1 (c (n "magicsquare") (v "0.0.1") (h "084m8ammpdc6fr526h71kzj73r0kkig95wycish9gz275pjbb175")))

(define-public crate-magicsquare-0.1.0 (c (n "magicsquare") (v "0.1.0") (h "1zigs12s36k063wncblhp8n9rfhda6r51f3zr0amx37drbazqysq")))

(define-public crate-magicsquare-0.2.0 (c (n "magicsquare") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1rs8h9lwqa50qszc2371m4vs79isnla0jq0h62y68l257al9qlq9") (f (quote (("default" "ndarray-csv"))))))

