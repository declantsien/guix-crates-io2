(define-module (crates-io ma gi magic8ball) #:use-module (crates-io))

(define-public crate-magic8ball-0.1.0 (c (n "magic8ball") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0aznabx360yviwasnacy9i6r0yflqi1ng75s983l0qdz773lqgac") (y #t)))

(define-public crate-magic8ball-0.1.1 (c (n "magic8ball") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1jlfk46yd2412zhfp7w4hywdrr75lz1ljbgh766zcpifvi2ka4nh")))

(define-public crate-magic8ball-0.1.2 (c (n "magic8ball") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0iv0ys4fpcxp8kv8vj9fx24bls8ypi9pxiwvyg3hafvk7zwjyldd")))

