(define-module (crates-io ma gi magic_migrate) #:use-module (crates-io))

(define-public crate-magic_migrate-0.1.0 (c (n "magic_migrate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "0k5ls8bja164w34kxamx30rs3vh9xbkng6zk8ri5k3d7cwri9633")))

(define-public crate-magic_migrate-0.2.0 (c (n "magic_migrate") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1kycs06ykfm61i8vxpkmrv9wh8l8nzpbnqq2fbqkqmi4viqba1cw")))

