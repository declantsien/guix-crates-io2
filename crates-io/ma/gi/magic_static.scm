(define-module (crates-io ma gi magic_static) #:use-module (crates-io))

(define-public crate-magic_static-1.0.0 (c (n "magic_static") (v "1.0.0") (d (list (d (n "magic_static_macro") (r "^1.0.1") (d #t) (k 0)))) (h "0lq2s59diyjwf7i0daia0wv0b00c7b6b6nx6bmlp9x7sv9mcqxs6") (f (quote (("bare-metal")))) (y #t)))

(define-public crate-magic_static-1.0.1 (c (n "magic_static") (v "1.0.1") (d (list (d (n "magic_static_macro") (r "^1.0.1") (d #t) (k 0)))) (h "1wbc35zfi5zg3rlvly5jsfh1vrwibsiwx2s1pjxm7xlal74pywyf") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-1.1.0 (c (n "magic_static") (v "1.1.0") (d (list (d (n "magic_static_macro") (r "^1.1.0") (d #t) (k 0)))) (h "083fhqsibyc4xwh4095zarik8fxrdnasiym95p8v57ajizlqhf94") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-1.2.0 (c (n "magic_static") (v "1.2.0") (d (list (d (n "magic_static_macro") (r "^1.2.0") (d #t) (k 0)))) (h "00py566mn8mqw41i84jpxi1l1v4npgpwj18m69z2s699lm3bakn5") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-2.0.0 (c (n "magic_static") (v "2.0.0") (d (list (d (n "magic_static_macro") (r "^2.0.0") (d #t) (k 0)))) (h "1593jn6dprb4b7ag21pcssdmdis07dc23n989grw6vfd6l41mgwx") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-2.0.1 (c (n "magic_static") (v "2.0.1") (d (list (d (n "magic_static_macro") (r "^2.0.1") (d #t) (k 0)))) (h "02x2jmxmgb2784rwq0j6mqij9ra1g8116g3pivyqnjx1xjs2a9sq") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-3.0.0 (c (n "magic_static") (v "3.0.0") (d (list (d (n "magic_static_macro") (r "^3.0.0") (d #t) (k 0)))) (h "1x3265d3dhy19yvl2xdclrj3057994zfm347dfrf7b9snwxzrmxl") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-3.0.1 (c (n "magic_static") (v "3.0.1") (d (list (d (n "magic_static_macro") (r "^3.0.0") (d #t) (k 0)))) (h "01dk445rq6gmm8gqrj2sm47a73i579r0hfhi8kvnh3fg49ga8ddy") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-3.0.2 (c (n "magic_static") (v "3.0.2") (d (list (d (n "magic_static_macro") (r "^3.0.1") (d #t) (k 0)))) (h "1vb91im6sd4qacjhf8zq5p0x5nwni6faicrv2j4rm4npkwqklip4") (f (quote (("bare-metal"))))))

(define-public crate-magic_static-3.0.3 (c (n "magic_static") (v "3.0.3") (d (list (d (n "magic_static_macro") (r "^3.0.1") (d #t) (k 0)))) (h "0j01l409j1m9g18wkk9hhmbyhqk47ykah9m9qw50fybpssgc1wzr") (f (quote (("bare-metal"))))))

