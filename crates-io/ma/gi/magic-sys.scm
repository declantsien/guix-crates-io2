(define-module (crates-io ma gi magic-sys) #:use-module (crates-io))

(define-public crate-magic-sys-0.0.1 (c (n "magic-sys") (v "0.0.1") (h "0yvj1x78ndzgpgj4vmwx9d8inyipvgl06ijx37n3l6hpgh4iagxx")))

(define-public crate-magic-sys-0.0.2 (c (n "magic-sys") (v "0.0.2") (h "1ji3pdgd1zznzravd0lqlnlg9cqb87dk86nhhmq9z7yn2p5784fp")))

(define-public crate-magic-sys-0.0.3 (c (n "magic-sys") (v "0.0.3") (h "0h93w2yvd76m3gqjkwbix4s7jn3f3fgfinmqzlrlahqv4frp18b3")))

(define-public crate-magic-sys-0.0.4 (c (n "magic-sys") (v "0.0.4") (h "1c8895h7fdm3h264by6cv1w6395gkjxpw7pmaz9ljrz2zyq16xcw")))

(define-public crate-magic-sys-0.0.5 (c (n "magic-sys") (v "0.0.5") (h "0vciz8xd32lcv20y9vkmvcshgdva4mpmmaxg5l4p8imq091bk8qz")))

(define-public crate-magic-sys-0.0.6 (c (n "magic-sys") (v "0.0.6") (h "0s564fywz6bb446bb3mfp3ykx0bvrxviswfkmwkdzcp3csmmgdkl")))

(define-public crate-magic-sys-0.0.7 (c (n "magic-sys") (v "0.0.7") (h "0187xvdi2n8ail7dgs8j8wimslfxr3jxdq0yhknxcabazixs0dmh")))

(define-public crate-magic-sys-0.0.8 (c (n "magic-sys") (v "0.0.8") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0npa0al3mz62zb8bqyfnyjgx2afzyql68rd4vrwlf2mjjy1ylni5")))

(define-public crate-magic-sys-0.1.0 (c (n "magic-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1w5al28225402hqbm4fc8lzwvkqbmbj9aqs0sy5074r5jaz9pq7h")))

(define-public crate-magic-sys-0.2.0 (c (n "magic-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "0v7i5njr6a0wbvlp08xh3b2qfsjbl1nrgg4miic03m9l1v32qi0p")))

(define-public crate-magic-sys-0.2.1 (c (n "magic-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "0qcnywjz4id6fmhz1a0hbsvynm863h8qqrk1qg4pghxr8hg4jic8") (l "magic")))

(define-public crate-magic-sys-0.3.0-alpha.1 (c (n "magic-sys") (v "0.3.0-alpha.1") (d (list (d (n "libc") (r "^0.2.104") (k 0)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "13chrdx5n89k3m1vsm2nj0hyr8maxm2d83whnvk3k2nh4ramy1v8") (f (quote (("v5-40" "v5-38") ("v5-38" "v5-35") ("v5-35" "v5-32") ("v5-32" "v5-27") ("v5-27" "v5-25") ("v5-25" "v5-23") ("v5-23" "v5-22") ("v5-22" "v5-21") ("v5-21" "v5-20") ("v5-20" "v5-13") ("v5-13" "v5-10") ("v5-10" "v5-05") ("v5-05" "v5-04") ("v5-04") ("default" "v5-38")))) (l "magic") (r "1.38")))

(define-public crate-magic-sys-0.3.0-alpha.2 (c (n "magic-sys") (v "0.3.0-alpha.2") (d (list (d (n "libc") (r "^0.2.104") (k 0)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0ci4vh16yiki74ccvcbh6iw8k1cp49zzw2av8mr924cff23impqv") (f (quote (("v5-40" "v5-38") ("v5-38" "v5-35") ("v5-35" "v5-32") ("v5-32" "v5-27") ("v5-27" "v5-25") ("v5-25" "v5-23") ("v5-23" "v5-22") ("v5-22" "v5-21") ("v5-21" "v5-20") ("v5-20" "v5-13") ("v5-13" "v5-10") ("v5-10" "v5-05") ("v5-05" "v5-04") ("v5-04") ("default" "v5-38")))) (l "magic") (r "1.38")))

(define-public crate-magic-sys-0.3.0-alpha.3 (c (n "magic-sys") (v "0.3.0-alpha.3") (d (list (d (n "libc") (r "^0.2.104") (k 0)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1cvjs55g1apcxp5hgkprynfy28nj7swwgcbcn3w3a7fmn7wb8qd7") (f (quote (("v5-40" "v5-38") ("v5-38" "v5-35") ("v5-35" "v5-32") ("v5-32" "v5-27") ("v5-27" "v5-25") ("v5-25" "v5-23") ("v5-23" "v5-22") ("v5-22" "v5-21") ("v5-21" "v5-20") ("v5-20" "v5-13") ("v5-13" "v5-10") ("v5-10" "v5-05") ("v5-05" "v5-04") ("v5-04") ("default" "v5-38")))) (l "magic") (r "1.38")))

(define-public crate-magic-sys-0.3.0 (c (n "magic-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.104") (k 0)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1g5k9d9igxv4h23nbhp8bqa5gdpkd3ahgm0rh5i0s54mi3h6my7g") (f (quote (("v5-40" "v5-38") ("v5-38" "v5-35") ("v5-35" "v5-32") ("v5-32" "v5-27") ("v5-27" "v5-25") ("v5-25" "v5-23") ("v5-23" "v5-22") ("v5-22" "v5-21") ("v5-21" "v5-20") ("v5-20" "v5-13") ("v5-13" "v5-10") ("v5-10" "v5-05") ("v5-05" "v5-04") ("v5-04") ("default" "v5-38")))) (l "magic") (r "1.38")))

