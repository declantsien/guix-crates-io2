(define-module (crates-io ma gi magic-bytes) #:use-module (crates-io))

(define-public crate-magic-bytes-0.1.0 (c (n "magic-bytes") (v "0.1.0") (h "1a6f4iic9rmgq4dqr6d0rn7053b71pwli5hav614nyx2xm14aa5w")))

(define-public crate-magic-bytes-0.1.1 (c (n "magic-bytes") (v "0.1.1") (h "0axxsykbvl121py1fzb6j1vpwldkv2viqjip5xp50yfjpwmiispi")))

