(define-module (crates-io ma gi magicalstick) #:use-module (crates-io))

(define-public crate-magicalstick-0.0.1 (c (n "magicalstick") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "scraper") (r "^0.7.0") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "04jbjd16wzc8xiw1glxs4mzgaf9m4xs2mwwyqdwcbgdi02pps2mf")))

(define-public crate-magicalstick-0.0.2 (c (n "magicalstick") (v "0.0.2") (d (list (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "scraper") (r "^0.7.0") (d #t) (k 0)) (d (n "try_print") (r "^0.0.1") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1npyv1cwmlxbbminq5c8c8vszg2ijmw6671ml9c8wgmfq68i5k4q")))

