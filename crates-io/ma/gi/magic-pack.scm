(define-module (crates-io ma gi magic-pack) #:use-module (crates-io))

(define-public crate-magic-pack-0.3.0 (c (n "magic-pack") (v "0.3.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "18qggp24vjjnnzcxbjxzzk79v53lv18h67mp6w1lsdbf1iwy9c14")))

(define-public crate-magic-pack-0.4.0 (c (n "magic-pack") (v "0.4.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "11733r714cn7mm2kjlfllax6n6yna5aq4b00jqk5f2mdrp6y6lwp")))

(define-public crate-magic-pack-0.5.0 (c (n "magic-pack") (v "0.5.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "19db7pf9h19b95yasyb015wj3h5wlq4vvp27m7if3pky107rgqg3")))

(define-public crate-magic-pack-0.6.0 (c (n "magic-pack") (v "0.6.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "10w4hws8ivap8g01hi382x53x758dlir7wgfqsz813y1jlfrii8p")))

