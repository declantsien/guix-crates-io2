(define-module (crates-io ma gi magic-import) #:use-module (crates-io))

(define-public crate-magic-import-0.2.0 (c (n "magic-import") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bzr6rckgpynrx0h9hyk72rd0psvki6adddw1nkg38rgkfcbh4ky") (f (quote (("stable") ("prefer_core"))))))

(define-public crate-magic-import-0.2.1 (c (n "magic-import") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01v1kxjq4qrfgmwdx2dq019i18mc0dbbm2sn0zlvrc2mwz1bk0jw") (f (quote (("stable") ("prefer_core"))))))

