(define-module (crates-io ma gi magic_static_macro) #:use-module (crates-io))

(define-public crate-magic_static_macro-1.0.0 (c (n "magic_static_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iq1y8ppwm3w0my25jzvx0jgsnc2bggbmah3bb8s80h516hmhc16") (y #t)))

(define-public crate-magic_static_macro-1.0.1 (c (n "magic_static_macro") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j94l5dm9xhy8i776pwyklgqvd3cf647x7n8cmjczsbpvmf17ghp")))

(define-public crate-magic_static_macro-1.1.0 (c (n "magic_static_macro") (v "1.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zxz38g2vhf61zzzaiig33l4wbp4gl8hr34w4vnf5xxkyvdsc4wb")))

(define-public crate-magic_static_macro-1.2.0 (c (n "magic_static_macro") (v "1.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2sa0ja1kxjf0g1h32j2r0svlksxzl3x9rc9ljwx9wzmhxjj13a")))

(define-public crate-magic_static_macro-2.0.0 (c (n "magic_static_macro") (v "2.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02ha0lgggylxg276bdvnhvkdy82zwy1hd22djkcav9bkrjzv7knx")))

(define-public crate-magic_static_macro-2.0.1 (c (n "magic_static_macro") (v "2.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02qg75l9n09j42r5pafbjin67x5cvg5r8nyy5qg067rw6v4qf50c")))

(define-public crate-magic_static_macro-3.0.0 (c (n "magic_static_macro") (v "3.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rszfkkg4wpfl96p0f32ylnachnaf81qb3g5gf9imsgw580wg3m9")))

(define-public crate-magic_static_macro-3.0.1 (c (n "magic_static_macro") (v "3.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1izdylvi65x5h620r12452vzcd24ambb8b6pzqan1jwljnndczz6")))

