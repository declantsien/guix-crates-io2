(define-module (crates-io ma gi magic-number) #:use-module (crates-io))

(define-public crate-magic-number-0.1.0 (c (n "magic-number") (v "0.1.0") (h "0xdpwhz8ci1b5pmaaxhv0066fbw73s6rk5chfb0d6a9y7q6g1fcm")))

(define-public crate-magic-number-0.2.0 (c (n "magic-number") (v "0.2.0") (h "1l1knl18r59sy6csfjsq104jy3nb1inq3sqcgls90bhfn88qgd5b")))

