(define-module (crates-io ma gi magicjson) #:use-module (crates-io))

(define-public crate-magicjson-0.0.1-alpha.1 (c (n "magicjson") (v "0.0.1-alpha.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3-log") (r "^0.9.0") (d #t) (k 0)))) (h "16hlghfqp66s38z0jwbq4x34sz117ik2my1w33jxamihgrq0r0g7") (y #t)))

(define-public crate-magicjson-0.0.1-alpha.2 (c (n "magicjson") (v "0.0.1-alpha.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0qbanrsnjkm7a8l1lpxq8m3svhq4cx6f1368a6hvalsgnrcr8zpz") (y #t)))

(define-public crate-magicjson-0.0.1-alpha.3 (c (n "magicjson") (v "0.0.1-alpha.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1nj7hlal58x543479x470vrmswkan8g6mkxj4x9s6rn96jxdvv2n") (y #t)))

(define-public crate-magicjson-0.0.1-alpha.4 (c (n "magicjson") (v "0.0.1-alpha.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "15cb9brk0f5nsiss91hrds4wcwignj805n3rdczv8qjvp1w4z1wp")))

(define-public crate-magicjson-0.0.1-alpha.5 (c (n "magicjson") (v "0.0.1-alpha.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0day52q5a1abr7yy6fvm5n9psd6if191lbnk4182pm6vs02kwdlf")))

(define-public crate-magicjson-0.0.1-alpha.6 (c (n "magicjson") (v "0.0.1-alpha.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fmqvkxl517pjhbz0kaws19h0p19xfi11azlbai3zf3gck5y1kn2")))

(define-public crate-magicjson-0.0.1-alpha.7 (c (n "magicjson") (v "0.0.1-alpha.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0pjacfy6jrdfll70mwcwym71s9sgjprdqw896gi3nrdylyf9mydi")))

(define-public crate-magicjson-0.0.1-alpha.8 (c (n "magicjson") (v "0.0.1-alpha.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0qdslgmy8w8cki126sfs3h95i7vmjxj845yr4jdkffjg2kycqrhc")))

(define-public crate-magicjson-0.0.1-alpha.9 (c (n "magicjson") (v "0.0.1-alpha.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0db19sc0pcl325g1xp2r9b6pinj8gs35iwkrqmlwa500l2xl9mps")))

