(define-module (crates-io ma gi magic) #:use-module (crates-io))

(define-public crate-magic-0.1.0 (c (n "magic") (v "0.1.0") (h "1c1qj5k3427wm2qj1167ycgidqzaz21a2ld82cn8lll5ivngb6z9")))

(define-public crate-magic-0.2.0 (c (n "magic") (v "0.2.0") (h "1f1a4g6i7psv6ksgyjsgkmvhz9dzvhxp6n29w9qpmkx2m8zymryn")))

(define-public crate-magic-0.3.0 (c (n "magic") (v "0.3.0") (d (list (d (n "magic-sys") (r "^0.0.2") (d #t) (k 0)))) (h "096j9aj54kspwgjdqh2kkpvpf6axp7bi5bnda1ah4bb3bjxksy54")))

(define-public crate-magic-0.4.0 (c (n "magic") (v "0.4.0") (d (list (d (n "magic-sys") (r "^0.0.3") (d #t) (k 0)))) (h "04gdqj0clqd8d17j3y7gq56111q01fi9xaj2mhx38q6f2p04kr24")))

(define-public crate-magic-0.5.0 (c (n "magic") (v "0.5.0") (d (list (d (n "magic-sys") (r "^0.0.3") (d #t) (k 0)))) (h "16jqp6kykdhyv0cqmizzm7apdk71cjdiwa7nbsx8qqbfbr6gj5np")))

(define-public crate-magic-0.5.1 (c (n "magic") (v "0.5.1") (d (list (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0bzjnwcam8683f1wm2k169p42w2lnsbz4qj4c2c34bhbcfqpq17g")))

(define-public crate-magic-0.5.2 (c (n "magic") (v "0.5.2") (d (list (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0ykq0mnwskg7s01paxcflgab33zq8w5imw660d3nklkqx9nvna2a")))

(define-public crate-magic-0.6.0 (c (n "magic") (v "0.6.0") (d (list (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0jsknvb6ljzca41xmwzc35bwkradpx26s1670vlpbikxanf5idvl")))

(define-public crate-magic-0.6.1 (c (n "magic") (v "0.6.1") (d (list (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)))) (h "16d1r29a3q1bgr9g7srgbclxzg0f303s954g67541dw2dmg0yx7m")))

(define-public crate-magic-0.6.2 (c (n "magic") (v "0.6.2") (d (list (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1j3imzal6z1by1rd61iz4g0bwgbf6km959b36rbizk3v89mv91jx")))

(define-public crate-magic-0.6.3 (c (n "magic") (v "0.6.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 2)))) (h "0h56mrjlcflgb1n97zc15xgm96f0sy7n33lz5wdd10sdawyadn61")))

(define-public crate-magic-0.6.4 (c (n "magic") (v "0.6.4") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 2)))) (h "0wv2knbc8lib0q78lv8g6pa5r2pvcc65n1i7y3ml8h4z7ydk8xwh")))

(define-public crate-magic-0.7.0 (c (n "magic") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1.13") (d #t) (k 2)))) (h "1p3rqhasrly0y4js6bw449sbdm7lncmivni74r7a0ci00cq4fmg1")))

(define-public crate-magic-0.8.0 (c (n "magic") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 2)))) (h "149808b2wknldzpx1vdgda0h4lxq6vnlqdla0wlkm2fgnq96zv1n")))

(define-public crate-magic-0.9.0 (c (n "magic") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 2)))) (h "1ija17sha52m8cw0791j8kxlcibvdq7ir8s2g2j18s6662vw115m")))

(define-public crate-magic-0.10.0 (c (n "magic") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "magic-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 2)))) (h "0ic9wg1yjsaaajlh8ddnzw43zr0sswqhi94s37lzapzgpl58jw81")))

(define-public crate-magic-0.11.0 (c (n "magic") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "magic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 2)))) (h "1fwkyivcv43b85pb02js3dwd92a4zs340j18s9ijck4w9b45kdyl")))

(define-public crate-magic-0.12.0 (c (n "magic") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "magic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 2)))) (h "0qy3z1pkq7l1czi58a30wycy3k4afbmblgsbq9vkc4vvfb06w0ak")))

(define-public crate-magic-0.12.1 (c (n "magic") (v "0.12.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "magic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 2)))) (h "08al7c0ak801w70i50gzs6508s2m4rw09crmfib1n056k3ak6fga")))

(define-public crate-magic-0.12.2 (c (n "magic") (v "0.12.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "magic-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 2)))) (h "1akrlybcwnazhn9hbhdh1rrdpba1199ld5gxp05n64hsqjp4rxwn")))

(define-public crate-magic-0.13.0-alpha.1 (c (n "magic") (v "0.13.0-alpha.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "188f041vv4zv7kzs0p7b021hj27j20im75k4ai5nchwy65hxa0x6") (r "1.46")))

(define-public crate-magic-0.13.0-alpha.2 (c (n "magic") (v "0.13.0-alpha.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)))) (h "05f8vvr4yznl5ifi188y10yqd5j3slzslsls249byk18qvpc0rqf") (r "1.46")))

(define-public crate-magic-0.13.0-alpha.3 (c (n "magic") (v "0.13.0-alpha.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1f7mk0xp4rwfdpxs1icpxzbjyhca5a3lwj6dycms5bnphrn0wfzx") (r "1.48")))

(define-public crate-magic-0.13.0 (c (n "magic") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rcnv27pk117zd1zpabp8yfi2d5545a615pax9ialk8zrcx2w547") (r "1.48")))

(define-public crate-magic-0.14.0 (c (n "magic") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1p5r0hiw5l53jg86d1m3xcymfd213c3zpwwh3808dsls54qqlfkb") (r "1.56")))

(define-public crate-magic-0.15.0 (c (n "magic") (v "0.15.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0x91dhn5ysm9bxv2lybdwd9j3dfib5wz0zkmarmqysk88xaxwqy7") (r "1.56")))

(define-public crate-magic-0.15.1 (c (n "magic") (v "0.15.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2.105") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n6p0nz1gq60lgx1lkf145x4gz37wsl6l7ln9jdlbnzdig686kqr") (r "1.56")))

(define-public crate-magic-0.16.0 (c (n "magic") (v "0.16.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "14zpy03zsmd2cfgplfpmby9g3d0rrvy512wbqn3504dw0ad6hz24") (r "1.56")))

(define-public crate-magic-0.16.1 (c (n "magic") (v "0.16.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1pyzja2gq15x409asffbqx5s251b6yfds1nr97s3vsnni7gyg2yn") (r "1.56")))

(define-public crate-magic-0.16.2 (c (n "magic") (v "0.16.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (k 0)) (d (n "magic-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0g9py31aw19j5sr5lznb068byhgbiynflvizjrxcwgccvw1sw052") (r "1.56")))

