(define-module (crates-io ma gi magic-ring-buffer) #:use-module (crates-io))

(define-public crate-magic-ring-buffer-0.1.0 (c (n "magic-ring-buffer") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "file-descriptors") (r "^0.8.6") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "likely") (r "^0.1") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1ghr92y42v8n6k9kixawlzv9bxszxiynz7dkr91krg13680f73rw")))

