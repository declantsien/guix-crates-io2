(define-module (crates-io ma g3 mag3110) #:use-module (crates-io))

(define-public crate-mag3110-0.1.1 (c (n "mag3110") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "1hw3r7rmrzdzdj3krpmlfpihh21dhvcnzy659gccrim07ymhk48m")))

(define-public crate-mag3110-0.1.2 (c (n "mag3110") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "0dpyz1c8hhbzassnwrr0k3i2knlslzmznj3vpxjw7lhv4dkfkp6z")))

(define-public crate-mag3110-0.1.3 (c (n "mag3110") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0mznz17rnnk6mbd7mrxpc5m3pry2h1hq9jk8hgm8xjggx0s5c806")))

(define-public crate-mag3110-0.1.4 (c (n "mag3110") (v "0.1.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "15gdxb16l1b4m0wdkg2p0wicnpc0qy54qf0qhykf961f2gjfnaz9")))

