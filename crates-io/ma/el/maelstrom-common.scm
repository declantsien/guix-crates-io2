(define-module (crates-io ma el maelstrom-common) #:use-module (crates-io))

(define-public crate-maelstrom-common-0.1.0 (c (n "maelstrom-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "05lgdgx54by3p588l9lvd9f4zhppgfdmmvm1rhf7gqq3jgmy3pc5")))

(define-public crate-maelstrom-common-0.1.1 (c (n "maelstrom-common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1v6ka09qys2lljxli72v9n4qnjk0mihy168j9iib804hs6xgzb76")))

