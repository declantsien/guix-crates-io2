(define-module (crates-io ma el maelstrom-simex) #:use-module (crates-io))

(define-public crate-maelstrom-simex-0.4.0 (c (n "maelstrom-simex") (v "0.4.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0imbaa4rav8brhbm5vb98ilkxc87fdj1ydrxm9l6njx65sdjl9wd") (r "1.75")))

(define-public crate-maelstrom-simex-0.4.1 (c (n "maelstrom-simex") (v "0.4.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "071adb4vaaifr1rckjplzrnymvl1ac5vzzgdwx1ic826y31icr56") (r "1.75")))

(define-public crate-maelstrom-simex-0.4.2 (c (n "maelstrom-simex") (v "0.4.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "18h5sdkcflqiamx6gd4blxsf4qwaknjby6028aa95d6fbmcng05y") (r "1.75")))

(define-public crate-maelstrom-simex-0.4.3 (c (n "maelstrom-simex") (v "0.4.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0gbbfdbdvsx0b7fp6p1kvc25slx4iaw8i731isysbyz9qm6qnkqq") (r "1.75")))

(define-public crate-maelstrom-simex-0.5.0 (c (n "maelstrom-simex") (v "0.5.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0d98i1wvwj0yvlz6wvi6fq6fjn31fk8pchnn0cqrd9s11l4g5rkd") (r "1.75")))

(define-public crate-maelstrom-simex-0.6.0 (c (n "maelstrom-simex") (v "0.6.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0hjvqvxcc7s56rfsf4hffv1vh56rjn7hby1jszyc17dkl7skx1pa") (r "1.75")))

(define-public crate-maelstrom-simex-0.7.0 (c (n "maelstrom-simex") (v "0.7.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0957czm6xg44dq45p5hq62rby3h5mzn73v4k7yv0wkg069460fzj") (r "1.77.1")))

(define-public crate-maelstrom-simex-0.8.0 (c (n "maelstrom-simex") (v "0.8.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "08k63vky2gw2v3jymkl7cch7k55f3wwyynq4wg2b2yyr5gpqpl3h") (r "1.77.1")))

(define-public crate-maelstrom-simex-0.9.0 (c (n "maelstrom-simex") (v "0.9.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "099vvvlgzgq7fxjsl4rc1dwhajmb1znibw643sw2bq4yb79zb3rc") (r "1.77.1")))

