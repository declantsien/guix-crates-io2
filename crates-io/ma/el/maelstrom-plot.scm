(define-module (crates-io ma el maelstrom-plot) #:use-module (crates-io))

(define-public crate-maelstrom-plot-0.4.0 (c (n "maelstrom-plot") (v "0.4.0") (d (list (d (n "egui") (r "^0.23") (d #t) (k 0)))) (h "1nnkwfl10v3kzw6xqhscas6n9f2v6bjigxdyg6w7gmj6bbn0f6h4") (r "1.75")))

(define-public crate-maelstrom-plot-0.4.1 (c (n "maelstrom-plot") (v "0.4.1") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "1zdlkp5vjqpsznmkm1yd72dflm2bspfps8lzix2vak50glmkzdsp") (r "1.75")))

(define-public crate-maelstrom-plot-0.4.2 (c (n "maelstrom-plot") (v "0.4.2") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "14jhh8b1yansmnc9d6cwqa8brbjkyd5bj3pnvb8l2n7y1if0vzqi") (r "1.75")))

(define-public crate-maelstrom-plot-0.4.3 (c (n "maelstrom-plot") (v "0.4.3") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0x95rlx6icsy0sg8hl3fsgxsbk4p9kfksnxxb2plciqn45rgbr33") (r "1.75")))

(define-public crate-maelstrom-plot-0.5.0 (c (n "maelstrom-plot") (v "0.5.0") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0jspdpsxjswm8fm68v5cimnr5ygkhnqx06vbmvnx2zm15g207bwp") (r "1.75")))

(define-public crate-maelstrom-plot-0.6.0 (c (n "maelstrom-plot") (v "0.6.0") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0qrvl6lmamc05q2jk0s0d22280ff9x5s8wkp4x98p5x5mk0yw76n") (r "1.75")))

(define-public crate-maelstrom-plot-0.7.0 (c (n "maelstrom-plot") (v "0.7.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "0qinikf6l5bgzxnrkk3zgyzzzhpp81v90vz5bbrsc5drnrqg0k6a") (r "1.77.1")))

(define-public crate-maelstrom-plot-0.8.0 (c (n "maelstrom-plot") (v "0.8.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "1l974p4w2l49qnpffw4c0r76f11dm385gqfgjgyzdivxzh34kqgv") (r "1.77.1")))

(define-public crate-maelstrom-plot-0.9.0 (c (n "maelstrom-plot") (v "0.9.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "1d2cg4f6yj3r3yr9aylsqxwp7cymp8r151gkjkj7wzy3vy578ni3") (r "1.77.1")))

