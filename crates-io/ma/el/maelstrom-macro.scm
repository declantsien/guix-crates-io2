(define-module (crates-io ma el maelstrom-macro) #:use-module (crates-io))

(define-public crate-maelstrom-macro-0.6.0 (c (n "maelstrom-macro") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4by6a1zn3gniyisl2k3cnfvjjh8sil0cbh1c9d15crk6zkki17") (r "1.75")))

(define-public crate-maelstrom-macro-0.7.0 (c (n "maelstrom-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jmzxil5pgcyaakzsy85kiyisxdi1fg1r9kwsw5bs6s4zccdj8sf") (r "1.77.1")))

(define-public crate-maelstrom-macro-0.8.0 (c (n "maelstrom-macro") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h8xaqm3vps74skz5ky011s9afi313zm0lfkl63jc693klpyzvgf") (r "1.77.1")))

(define-public crate-maelstrom-macro-0.9.0 (c (n "maelstrom-macro") (v "0.9.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jpkkas6c6i9a45az42511fkzdnrb9vxb2w7lxma32pb55rgz51j") (r "1.77.1")))

