(define-module (crates-io ma el maelstrom-linux) #:use-module (crates-io))

(define-public crate-maelstrom-linux-0.5.0 (c (n "maelstrom-linux") (v "0.5.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ad5b902hxl6fzk1nqnp69m5a03sffw8frpdcf53a7jld06c1f4g") (f (quote (("test") ("std")))) (r "1.75")))

(define-public crate-maelstrom-linux-0.6.0 (c (n "maelstrom-linux") (v "0.6.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "050winsljljxyjnlf4xdrac9v0sf0rvn9s3z53j98j0qq1p8gc2y") (f (quote (("test") ("std")))) (r "1.75")))

(define-public crate-maelstrom-linux-0.7.0 (c (n "maelstrom-linux") (v "0.7.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "024x893ry2ilxwaxg0lj7xlfxpbjjgjcb0b7l0mj5d1d8p9bv7nr") (f (quote (("test") ("std")))) (r "1.77.1")))

(define-public crate-maelstrom-linux-0.8.0 (c (n "maelstrom-linux") (v "0.8.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gam8b6xah1frv3lw5k94di618q95y2xdxvk5pb5y5i4vivz5zxa") (f (quote (("test") ("std")))) (r "1.77.1")))

(define-public crate-maelstrom-linux-0.9.0 (c (n "maelstrom-linux") (v "0.9.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15rgzb7bhi2ss5ax8vw3sxdgxd33i5y36s5jl4d3skl205hr0g0n") (f (quote (("test") ("std")))) (r "1.77.1")))

