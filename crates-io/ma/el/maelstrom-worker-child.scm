(define-module (crates-io ma el maelstrom-worker-child) #:use-module (crates-io))

(define-public crate-maelstrom-worker-child-0.4.1 (c (n "maelstrom-worker-child") (v "0.4.1") (d (list (d (n "nc") (r "^0.8.18") (k 0)))) (h "1gm2hc4s6ndycv1dyar620jvccz4n5cn8q0fpjq682m1784mlvzi") (r "1.75")))

(define-public crate-maelstrom-worker-child-0.4.2 (c (n "maelstrom-worker-child") (v "0.4.2") (d (list (d (n "nc") (r "^0.8.18") (k 0)))) (h "1h6nh2xd1vm5xrk0kpwq2vgbrfq1p75dgvjf9bg94da8xz1h45ca") (r "1.75")))

(define-public crate-maelstrom-worker-child-0.4.3 (c (n "maelstrom-worker-child") (v "0.4.3") (d (list (d (n "nc") (r "^0.8.18") (k 0)))) (h "14a94psdqbrx0zprbfvh5rchaivf010g6mwh2rya65yxqd6q3i3c") (r "1.75")))

(define-public crate-maelstrom-worker-child-0.5.0 (c (n "maelstrom-worker-child") (v "0.5.0") (d (list (d (n "maelstrom-linux") (r "^0.5.0") (d #t) (k 0)))) (h "1p4vvvmn5g0ziq2d31r98vqp4rrvb1c28nm32zawjfbqsly88biv") (r "1.75")))

(define-public crate-maelstrom-worker-child-0.6.0 (c (n "maelstrom-worker-child") (v "0.6.0") (d (list (d (n "maelstrom-linux") (r "^0.6.0") (d #t) (k 0)))) (h "11nbzb6fdqg2rgp4bxfi1w8gm2a0raq2im6a8xlkj4p9ciglksvi") (r "1.75")))

(define-public crate-maelstrom-worker-child-0.7.0 (c (n "maelstrom-worker-child") (v "0.7.0") (d (list (d (n "maelstrom-linux") (r "^0.7.0") (d #t) (k 0)))) (h "1s46jifn2zw6f14f3792f283lznrcm7h26j0m8kwhq5m4c2qmgdr") (r "1.77.1")))

(define-public crate-maelstrom-worker-child-0.8.0 (c (n "maelstrom-worker-child") (v "0.8.0") (d (list (d (n "maelstrom-linux") (r "^0.8.0") (d #t) (k 0)))) (h "1q7caxxqqnb7ds73jf3ys8chcb2rk3h8nkpnlx8a1y5myspbzh9p") (r "1.77.1")))

(define-public crate-maelstrom-worker-child-0.9.0 (c (n "maelstrom-worker-child") (v "0.9.0") (d (list (d (n "maelstrom-linux") (r "^0.9.0") (d #t) (k 0)))) (h "17zbbzj3yhbn1d44qww8xim58kpr4fq8s43lplfgwwkhk5r038db") (r "1.77.1")))

