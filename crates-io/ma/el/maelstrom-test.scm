(define-module (crates-io ma el maelstrom-test) #:use-module (crates-io))

(define-public crate-maelstrom-test-0.4.0 (c (n "maelstrom-test") (v "0.4.0") (h "04x7sdyh9361hn0gm05qbid4hb3bl339z8dgxwgl2psg3vd1wp49") (r "1.75")))

(define-public crate-maelstrom-test-0.4.1 (c (n "maelstrom-test") (v "0.4.1") (h "0y0d021z00y8f4j4j24g5645d2qf2zn1bshrkmr178nrqc38d7h1") (r "1.75")))

(define-public crate-maelstrom-test-0.4.2 (c (n "maelstrom-test") (v "0.4.2") (h "1id8k8i3w05cvcvmibvkgnshs9x2f5x533llfd6h6fwp78mf4br9") (r "1.75")))

(define-public crate-maelstrom-test-0.4.3 (c (n "maelstrom-test") (v "0.4.3") (h "1yn4lqhghcnlws6wqp63zl82323kszi60f5wmcyy8lpc0mnfigpf") (r "1.75")))

(define-public crate-maelstrom-test-0.5.0 (c (n "maelstrom-test") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 0)) (d (n "maelstrom-base") (r "^0.5.0") (d #t) (k 0)) (d (n "maelstrom-util") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j18wmlx9lsfk7k5mpdhzh6d1js7pjwbbsy5qz2kqqw8jdd8x6yh") (r "1.75")))

(define-public crate-maelstrom-test-0.6.0 (c (n "maelstrom-test") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "maelstrom-base") (r "^0.6.0") (d #t) (k 0)) (d (n "maelstrom-util") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dsw04r7flgsfy1g3xlmmq1c9rpp449wx0qdyxx7d1y8jnqviwzz") (r "1.75")))

(define-public crate-maelstrom-test-0.7.0 (c (n "maelstrom-test") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "maelstrom-base") (r "^0.7.0") (d #t) (k 0)) (d (n "maelstrom-util") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03dn990vh08p8zs8zipvw4mvlqd4dbvy606jazya86ypfh85dbmc") (r "1.77.1")))

(define-public crate-maelstrom-test-0.8.0 (c (n "maelstrom-test") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "maelstrom-base") (r "^0.8.0") (d #t) (k 0)) (d (n "maelstrom-util") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1acq1vxfvj0vr0c8h297fxnawjkqwk2bgpjxqgcifsr03xja29mm") (r "1.77.1")))

(define-public crate-maelstrom-test-0.9.0 (c (n "maelstrom-test") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "maelstrom-base") (r "^0.9.0") (d #t) (k 0)) (d (n "maelstrom-util") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n5n63hwzmj9jsc5xqaj1kz3amdngpxfhwvbijpr0qb8702pmk61") (r "1.77.1")))

