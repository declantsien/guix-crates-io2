(define-module (crates-io ma ud maud_htmlescape) #:use-module (crates-io))

(define-public crate-maud_htmlescape-0.17.0 (c (n "maud_htmlescape") (v "0.17.0") (h "1dxzakhgpgbr7yy7z8vmywbi77d3yrwqdvg1s4m30hpwryy8byyh")))

(define-public crate-maud_htmlescape-0.17.1 (c (n "maud_htmlescape") (v "0.17.1") (h "19kzv4b79qx1x6fr57jh3cvsmhk0nb3wvy7s0srfpdclgyixlm85")))

