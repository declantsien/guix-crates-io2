(define-module (crates-io ma ud maud) #:use-module (crates-io))

(define-public crate-maud-0.1.0 (c (n "maud") (v "0.1.0") (h "1wg6yps1n8b7adldggjkbznkdr4z6lq0jgv6ak1n0bkrd51ldvdk") (y #t)))

(define-public crate-maud-0.1.1 (c (n "maud") (v "0.1.1") (h "0zxx4p6dhhpnvkjqwkd1ywyp0lmx732jvspxlq02qb5dk0ir8hbj")))

(define-public crate-maud-0.1.2 (c (n "maud") (v "0.1.2") (h "0ggm1ym381in60yx3l0q99fp1s3vqm42sypy6vjm5y1lcdp63as2")))

(define-public crate-maud-0.2.0 (c (n "maud") (v "0.2.0") (h "0gdqxkj1hd50qxy8dm96hzd7mb8qb42xrk3gdaj77w9c6zpdyk4f")))

(define-public crate-maud-0.3.0 (c (n "maud") (v "0.3.0") (h "1k9p3iydb5ybjfsmaw5c6fqhkvz2fs1jhh0zfnamlywmbnb9cnq1")))

(define-public crate-maud-0.3.1 (c (n "maud") (v "0.3.1") (h "1x9c3pps2x7mvpid5zydzbvbpifwd7v9fwi1zkm4854p4xxvgi9g")))

(define-public crate-maud-0.3.2 (c (n "maud") (v "0.3.2") (h "0dayywm4gfnk3ggx23cr6cinx5hs2pjbs7n2j2p05i7r0q3g1zdm")))

(define-public crate-maud-0.3.3 (c (n "maud") (v "0.3.3") (h "1mwmnsjsyj8x72y0zgl01wc3qb6bzdlslnnar053hkvpwcibvcj6")))

(define-public crate-maud-0.3.4 (c (n "maud") (v "0.3.4") (h "1hady5p9knh2nwm481yf33fiax4dk5gills7s4655wim4qpcqk7s")))

(define-public crate-maud-0.4.0 (c (n "maud") (v "0.4.0") (h "0djcw9iq271gfbxqnrzn6qlvsgwfrkkzsw8yngzdxg6y57xbr6vy") (y #t)))

(define-public crate-maud-0.4.1 (c (n "maud") (v "0.4.1") (h "11kv5rkj040ddjc82r7pw99zihf4zwynap9fcp37x9v35fbqqh9j")))

(define-public crate-maud-0.4.2 (c (n "maud") (v "0.4.2") (h "1x3c88w0hiij1wwhjqgz1s5prf14zb3ji2cng4ganwqwf4vi3c2k")))

(define-public crate-maud-0.4.3 (c (n "maud") (v "0.4.3") (h "1c0ai488i4sr63hqyb14bm80rdx8y6wr7syna33rnwfdiwxrflwg")))

(define-public crate-maud-0.4.4 (c (n "maud") (v "0.4.4") (h "0fisw0yv01m35ayqnlmdk9lqyg8dk474k24k9l3mrqvzfifq97g1")))

(define-public crate-maud-0.5.0 (c (n "maud") (v "0.5.0") (h "1cp73vi0pjh12a9y8inm8793zxlcz8pnirmvs8018cbp971ni6c3")))

(define-public crate-maud-0.5.1 (c (n "maud") (v "0.5.1") (h "1qsnmkf23glc9c7bfk8pscd26gbp0n4472h69m4vqg0samv0mgpj")))

(define-public crate-maud-0.6.0 (c (n "maud") (v "0.6.0") (h "1drm42mj1gdj7ril73dh19fhk08ydvmln8bccgv9abaia516akah")))

(define-public crate-maud-0.6.1 (c (n "maud") (v "0.6.1") (h "0ijzix2m8ybbai2cg324zwy18w7jp04lh580vszpa4naz7nwdgv2")))

(define-public crate-maud-0.6.2 (c (n "maud") (v "0.6.2") (h "069iv1p8yzjgmn8ijsa0gsp6qsi6ghb8hzczkmpr7llxgq609jdx")))

(define-public crate-maud-0.7.0 (c (n "maud") (v "0.7.0") (h "0w4n34qx9hmdizhcydfqk1svfz4czbc9rp315rk1czl7wjgp2vyq")))

(define-public crate-maud-0.7.1 (c (n "maud") (v "0.7.1") (h "0icp8bqha8wqxwfzw8w4wvzmfdrdblvsvny78yv1cs7jl6jff7q4")))

(define-public crate-maud-0.7.2 (c (n "maud") (v "0.7.2") (h "0dh3grjk8m5ajk2gqk58kl30c6pbway8b90qv2ix8lrk91fv61v6")))

(define-public crate-maud-0.7.3 (c (n "maud") (v "0.7.3") (h "1fqqhp5p5jlhifayivk9hhab4k2fv0gjw3cfj4g54kpxz7kp9ydd")))

(define-public crate-maud-0.7.4 (c (n "maud") (v "0.7.4") (h "0dmj2ddjvacnig0yarimn8f4zv73c5cvwjf2004dk423yqsg7r7v")))

(define-public crate-maud-0.8.0 (c (n "maud") (v "0.8.0") (h "0chffm0irprdlh7s1p846bm6rf4y282g9pwfnxkprnpgqv7iz87n")))

(define-public crate-maud-0.8.1 (c (n "maud") (v "0.8.1") (h "1zdij5qb4fdalqhhnkwcq855pb9m1vxwkj0gf3xr3gbim5hfpf2a")))

(define-public crate-maud-0.9.0 (c (n "maud") (v "0.9.0") (h "057g8bwq81m3f4wa6264ldm87gdfvmv904hl4a1ab7cgny22sadq")))

(define-public crate-maud-0.10.0 (c (n "maud") (v "0.10.0") (h "1x58fkfcvj6zidlw5931k6awv91zsmi12kqy4c1cqjdi6wlpcclk")))

(define-public crate-maud-0.11.0 (c (n "maud") (v "0.11.0") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0ckzz61al1qfl1nf1zgnvpda8m8ny1ccdx2qcwqqvzds0vpzmpa8")))

(define-public crate-maud-0.11.1 (c (n "maud") (v "0.11.1") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "083jk5q7ljvggx822bz39lxahngwv52rlr7iqvarhflip1k1s7x6")))

(define-public crate-maud-0.12.0 (c (n "maud") (v "0.12.0") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "19sp3g0s5s92rymjck62g81p01ci0j34lfpwiy17vsgkvqx7vqj0")))

(define-public crate-maud-0.13.0 (c (n "maud") (v "0.13.0") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "03zrr4mgr6azxh4vvr4pblkbbchbf9d0wj9szgim7zdrc5966qv8")))

(define-public crate-maud-0.14.0 (c (n "maud") (v "0.14.0") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0p9gp44mfg2xyaby18138w551p277vvia271xi60csrmy88250rb")))

(define-public crate-maud-0.15.0 (c (n "maud") (v "0.15.0") (d (list (d (n "iron") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1dyqsq1jc7nmaj81kbjb5pgpjqdkshl2ah0cvi5wf74iiqlg3iwj")))

(define-public crate-maud-0.16.0 (c (n "maud") (v "0.16.0") (d (list (d (n "iron") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1m406mfcim2pyclfil25bq7b0kfmid85f5sda7iffllkbx5jlwb2")))

(define-public crate-maud-0.16.1 (c (n "maud") (v "0.16.1") (d (list (d (n "iron") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "rocket") (r ">= 0.1.2, < 0.3") (o #t) (d #t) (k 0)))) (h "0vp49d7biszgjpr8j4z7hsdd9mi52a35s8iry13s3brd3ykkb6c5")))

(define-public crate-maud-0.16.2 (c (n "maud") (v "0.16.2") (d (list (d (n "iron") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "rocket") (r ">= 0.1.2, < 0.3") (o #t) (d #t) (k 0)))) (h "0mrgz1hgr42l7gi4liy3b77m0i1ry8hqjd6drh41mhw659pl3xad")))

(define-public crate-maud-0.17.0 (c (n "maud") (v "0.17.0") (d (list (d (n "iron") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.17.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0hn6bypn2fq60m0hm4y8nibhby15gjiifbd9p8gd8cspbanwds69")))

(define-public crate-maud-0.17.1 (c (n "maud") (v "0.17.1") (d (list (d (n "iron") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "09wlg90xqjdg0y2v4ry6lafvkijicbpnmx6pvf39viqjiw3gmbc0")))

(define-public crate-maud-0.17.2 (c (n "maud") (v "0.17.2") (d (list (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.17.0") (d #t) (k 2)) (d (n "maud_macros") (r "^0.17.2") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0j1025vhif0ryg2spw749xni5s6w9r5dcak1md35jm2a9y0kq0n0")))

(define-public crate-maud-0.17.3 (c (n "maud") (v "0.17.3") (d (list (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.17.0") (d #t) (k 2)) (d (n "maud_macros") (r "^0.17.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0qny7z2l1h3b6j38xm8vk5b95yha2gyinmy3w8rv4f273r1xc0q5")))

(define-public crate-maud-0.17.4 (c (n "maud") (v "0.17.4") (d (list (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.17.0") (d #t) (k 2)) (d (n "maud_macros") (r "^0.17.4") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "14i3j782d70xsac058wvlvj38vvv4q75256qr3aidvz6k3f1phpk")))

(define-public crate-maud-0.17.5 (c (n "maud") (v "0.17.5") (d (list (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.17.0") (d #t) (k 2)) (d (n "maud_macros") (r "^0.17.5") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1lqz89zbrmlzh4f7bwi5j4py2bvi7bhzwwbvh83xlmsxm2cqjx6m")))

(define-public crate-maud-0.18.0 (c (n "maud") (v "0.18.0") (d (list (d (n "actix-web") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.17.0") (d #t) (k 2)) (d (n "maud_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1c761zlfd54q6xfbnsr59lncm0sv4vq94s9467mdzsfgxk5w2iy2")))

(define-public crate-maud-0.18.1 (c (n "maud") (v "0.18.1") (d (list (d (n "actix-web") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_lints") (r "^0.18.1") (d #t) (k 2)) (d (n "maud_macros") (r "^0.18.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0a2xli00kcs7r6sf28nng0gmiawqjhlz5mb9wlfmv1gsbd5a3q69")))

(define-public crate-maud-0.19.0 (c (n "maud") (v "0.19.0") (d (list (d (n "actix-web") (r ">= 0.6.12, < 0.8.0") (o #t) (d #t) (k 0)) (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.19.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (o #t) (d #t) (k 0)))) (h "18xvpx1ziywqph3cm4945chklwxa6sz5f2vc7fzi2cqsv1mslgyy")))

(define-public crate-maud-0.20.0 (c (n "maud") (v "0.20.0") (d (list (d (n "actix-web") (r ">= 0.6.12, < 0.8.0") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.15") (d #t) (k 2)) (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3, < 0.5") (o #t) (d #t) (k 0)))) (h "0fvrwn6jyhbid5ia3prkwc463p19nprjrndd1d2hk27z28jlnyrk")))

(define-public crate-maud-0.21.0 (c (n "maud") (v "0.21.0") (d (list (d (n "actix-web") (r "^1.0.0") (o #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("stable"))) (d #t) (k 2)) (d (n "iron") (r ">= 0.5.1, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.21.0") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3, < 0.5") (o #t) (d #t) (k 0)))) (h "07f747a152q2r9f2cgxlw4vs3fa12mcnx9h57v6iw5aabb2ixr80")))

(define-public crate-maud-0.22.0 (c (n "maud") (v "0.22.0") (d (list (d (n "actix-web-dep") (r "^2.0.0") (o #t) (k 0) (p "actix-web")) (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("stable"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "iron") (r ">=0.5.1, <0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.22.0") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)))) (h "1nszly79526lpixgxd9zn619xnm8h0drm6ykl8i2x787zh13fqc4") (f (quote (("actix-web" "actix-web-dep" "futures"))))))

(define-public crate-maud-0.22.1 (c (n "maud") (v "0.22.1") (d (list (d (n "actix-web-dep") (r ">=2, <4") (o #t) (k 0) (p "actix-web")) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "iron") (r ">=0.5.1, <0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.22.1") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "0y8p7rkcrca3z1zrdc39bjgnqqmrfswfnxwkbzabadvqzc6xndkf") (f (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.22.2 (c (n "maud") (v "0.22.2") (d (list (d (n "actix-web-dep") (r ">=2, <4") (o #t) (k 0) (p "actix-web")) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "iron") (r ">=0.5.1, <0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.22.2") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yk2a6gy6naq615qssy57yl18r4vvx4101y1cl1zh1hcif0881ys") (f (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.22.3 (c (n "maud") (v "0.22.3") (d (list (d (n "actix-web-dep") (r ">=2, <4") (o #t) (k 0) (p "actix-web")) (d (n "axum") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "iron") (r ">=0.5.1, <0.7.0") (o #t) (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.1") (d #t) (k 0)) (d (n "maud_macros") (r "^0.22.3") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z20q47jpfsmpdj7c0nvllzxz9x86kmgkfhc55dsxlg7gn84km2r") (f (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.23.0 (c (n "maud") (v "0.23.0") (d (list (d (n "actix-web-dep") (r ">=2, <4") (o #t) (k 0) (p "actix-web")) (d (n "axum") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "maud_macros") (r "^0.23.0") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "07hfn5pn8ys9j1y5ha8bkgkw5jj8dw56bcgs5v671jcg9yxzx13k") (f (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.24.0 (c (n "maud") (v "0.24.0") (d (list (d (n "actix-web-dep") (r "^4") (o #t) (k 0) (p "actix-web")) (d (n "axum-core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^0.4.8") (f (quote ("i128"))) (k 0)) (d (n "maud_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "0n0yyvfkyxkz6xp4yiyky4qzgc0k4mjh59pg57gr6jzb376z5bqr") (f (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.25.0 (c (n "maud") (v "0.25.0") (d (list (d (n "actix-web-dep") (r "^4") (o #t) (k 0) (p "actix-web")) (d (n "axum-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "maud_macros") (r "^0.25.0") (d #t) (k 0)) (d (n "rocket") (r ">=0.3, <0.5") (o #t) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)))) (h "006g7szmlhfhbfpc7zmyhjllx7gw7dwq3s23ib0y2zwaxyfb3fmh") (f (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.26.0 (c (n "maud") (v "0.26.0") (d (list (d (n "actix-web-dep") (r "^4") (o #t) (k 0) (p "actix-web")) (d (n "axum-core") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (o #t) (k 0)) (d (n "http") (r "^1") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "maud_macros") (r "^0.26.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (f (quote ("diff"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.6") (o #t) (d #t) (k 0)))) (h "1amw5r3w4v9z2grr2cf90aca9x1244gv18gzvp6qjhkb05sqnlfz") (f (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

