(define-module (crates-io ma ud maud-live-view-macros) #:use-module (crates-io))

(define-public crate-maud-live-view-macros-0.24.0 (c (n "maud-live-view-macros") (v "0.24.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0lc6qmv8m89ycrk3i262k1di3vlflarq22g5n12mrwwx9hrnmghv")))

(define-public crate-maud-live-view-macros-0.24.1 (c (n "maud-live-view-macros") (v "0.24.1") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0nasvin9wm27dnzymmvrmnaq2ps9w9nb5kqp0z0amrfga3lgqk1j")))

(define-public crate-maud-live-view-macros-0.24.2 (c (n "maud-live-view-macros") (v "0.24.2") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "02cd6mdyigr93rp5dnfr7xnyqv1dd66s13n31yhqk66agd5di5vs")))

(define-public crate-maud-live-view-macros-0.24.3 (c (n "maud-live-view-macros") (v "0.24.3") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "02qmycqav5y6r1b1h7kaq5z4pf8dflmcmajk3b9vs7xs4gxx2ny4")))

