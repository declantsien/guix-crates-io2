(define-module (crates-io ma ud maud-pulldown-cmark) #:use-module (crates-io))

(define-public crate-maud-pulldown-cmark-0.0.2 (c (n "maud-pulldown-cmark") (v "0.0.2") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "1g8z05bclfnniy6nj23prhmkxwwal990m7j0dkkcbla4c4204a9l") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.3 (c (n "maud-pulldown-cmark") (v "0.0.3") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "00ix40pv3m593k7sv3a9z8df7qx2npajqbq8vzfpj5vgx3hvy3pc") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.4 (c (n "maud-pulldown-cmark") (v "0.0.4") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "0f8l9y2r23pzj583850fzn8x7lsl3prgri010wf4lq5bz0k4kzaz") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.5 (c (n "maud-pulldown-cmark") (v "0.0.5") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "0ds6bk9yii6csi79flpbpvimycpxh97x587xrzcxdp1fhrqbamaf") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.6 (c (n "maud-pulldown-cmark") (v "0.0.6") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "10c9iy0yh0d3239mgbrfzdxbl69z8srl5y108d34hi8ri7k4m10p") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.7 (c (n "maud-pulldown-cmark") (v "0.0.7") (d (list (d (n "maud") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "09sp5j15vhq481vxqxi06gi95ldvkaa8l3ddgjsz5szyqzda1kwi") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.1.0 (c (n "maud-pulldown-cmark") (v "0.1.0") (d (list (d (n "maud") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.5") (d #t) (k 0)))) (h "037xqzx96kpbrcvpqpfj84g2v23vyiiyyfkx9dp8csxmqb14pd3l") (f (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.2.2 (c (n "maud-pulldown-cmark") (v "0.2.2") (d (list (d (n "maud") (r "^0.9.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0z7nva0fm3y1mzj7rd3pv2kshs7pw9b8is59dzdb0q5767y2cw6d")))

(define-public crate-maud-pulldown-cmark-0.3.0 (c (n "maud-pulldown-cmark") (v "0.3.0") (d (list (d (n "maud") (r "^0.11.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.11.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)))) (h "18vv9dd6q9slv9k0y5lhpccaarc1mcif1ql17cf1z1acsxsqsnfh")))

(define-public crate-maud-pulldown-cmark-0.4.0 (c (n "maud-pulldown-cmark") (v "0.4.0") (d (list (d (n "maud") (r "^0.12.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.12.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)))) (h "085v8pmi7pc2lw72h6n2k8mlxbgb436d2jp1sy52c59i47xv0x6m")))

(define-public crate-maud-pulldown-cmark-0.5.0 (c (n "maud-pulldown-cmark") (v "0.5.0") (d (list (d (n "maud") (r "^0.14.0") (d #t) (k 0)) (d (n "maud_macros") (r "^0.14.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)))) (h "0nl7xcnwi504w9cl378qc7l8kagqgdzjs9r9rsn46rkdc7pqn5xw")))

