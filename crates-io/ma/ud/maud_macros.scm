(define-module (crates-io ma ud maud_macros) #:use-module (crates-io))

(define-public crate-maud_macros-0.1.0 (c (n "maud_macros") (v "0.1.0") (d (list (d (n "maud") (r "= 0.1.0") (d #t) (k 0)))) (h "0zhvc5r84d218v8x2xxdq8aid8pxy03lmafnn1rp29r9vg5mkh0q") (y #t)))

(define-public crate-maud_macros-0.1.1 (c (n "maud_macros") (v "0.1.1") (d (list (d (n "maud") (r "= 0.1.1") (d #t) (k 0)))) (h "1xvixhl8kkf9ac06529gfxgwlznfxpp1cyd1p7lm3nvncxmawlf8")))

(define-public crate-maud_macros-0.1.2 (c (n "maud_macros") (v "0.1.2") (d (list (d (n "maud") (r "= 0.1.2") (d #t) (k 0)))) (h "00nd13sr71bsjhws4cxicy68ap5kp63dri98qmch85lzh6igghv2")))

(define-public crate-maud_macros-0.2.0 (c (n "maud_macros") (v "0.2.0") (d (list (d (n "maud") (r "= 0.2.0") (d #t) (k 0)))) (h "1mx3f5v3mna70rkxchfl3vgi4p6qc1lv840lci3f9zmvwl8ifz55")))

(define-public crate-maud_macros-0.3.0 (c (n "maud_macros") (v "0.3.0") (d (list (d (n "maud") (r "= 0.3.0") (d #t) (k 0)))) (h "0fkm0s28015dh1igfkd4ldc1g2nbhnwv2m696c0rss86577z0r33")))

(define-public crate-maud_macros-0.3.1 (c (n "maud_macros") (v "0.3.1") (d (list (d (n "maud") (r "= 0.3.1") (d #t) (k 0)))) (h "0z6s17gl1jwry7bzpxyxqc5i066jc5s7rljga48na7a5rqkd516z")))

(define-public crate-maud_macros-0.3.2 (c (n "maud_macros") (v "0.3.2") (d (list (d (n "maud") (r "= 0.3.2") (d #t) (k 0)))) (h "1xylvpvlhg72hhiyhpkzrzfha94m6d7hv4ca7j2g0riqj8q91m1m")))

(define-public crate-maud_macros-0.3.3 (c (n "maud_macros") (v "0.3.3") (d (list (d (n "maud") (r "= 0.3.3") (d #t) (k 0)))) (h "0py64frbylj9m0p6bpr589lkvczcs8yghrp9157g9dz3vjc37l9f")))

(define-public crate-maud_macros-0.3.4 (c (n "maud_macros") (v "0.3.4") (d (list (d (n "maud") (r "= 0.3.4") (d #t) (k 0)))) (h "1svc18dh54bz8h5vdkd3zayb6z0baigm106w8sjlcrg9rnryf16w")))

(define-public crate-maud_macros-0.4.0 (c (n "maud_macros") (v "0.4.0") (d (list (d (n "maud") (r "= 0.4.0") (d #t) (k 0)))) (h "0papq7zxz6dmix4vhd7rcy1ab6ncqyhscpxld1zwckxzp63ifx8y") (f (quote (("print-expansion")))) (y #t)))

(define-public crate-maud_macros-0.4.1 (c (n "maud_macros") (v "0.4.1") (d (list (d (n "maud") (r "= 0.4.1") (d #t) (k 0)))) (h "1vbs6y2bdfs6h13hy5njzdfaghln6fjialgkaaxxdydddqsa7kbs")))

(define-public crate-maud_macros-0.4.2 (c (n "maud_macros") (v "0.4.2") (d (list (d (n "maud") (r "= 0.4.2") (d #t) (k 0)))) (h "0yg8g4d3a0jzc79mqqa7a6fivg58kmaydrnhkw3cknqgaaczx6fp")))

(define-public crate-maud_macros-0.4.3 (c (n "maud_macros") (v "0.4.3") (d (list (d (n "maud") (r "= 0.4.3") (d #t) (k 0)))) (h "1lwihc33gcycjgadgkaa5ycz28cm3f45d4cm19mywfppspsj1vvy")))

(define-public crate-maud_macros-0.4.4 (c (n "maud_macros") (v "0.4.4") (d (list (d (n "maud") (r "= 0.4.4") (d #t) (k 0)))) (h "0qc7hg9bbh0a1ln61x4y08hmrn6pq4ij37nmrdn5kpyw6bwcckc1")))

(define-public crate-maud_macros-0.5.0 (c (n "maud_macros") (v "0.5.0") (d (list (d (n "maud") (r "= 0.5.0") (d #t) (k 0)))) (h "1xypbwzndsps5r3ks1kdynm5hfdljhxfmirhiy0sn303ihsirg3z")))

(define-public crate-maud_macros-0.5.1 (c (n "maud_macros") (v "0.5.1") (d (list (d (n "maud") (r "= 0.5.1") (d #t) (k 0)))) (h "0q215a5rdkpnxikfsmk9337xjx7n9vsb7361gpnx601ja3ijn2hy")))

(define-public crate-maud_macros-0.6.0 (c (n "maud_macros") (v "0.6.0") (d (list (d (n "maud") (r "= 0.6.0") (d #t) (k 0)))) (h "02mzbi3d0ci050rpb1ixaki353sbc8y9ylsmhk4m3ijraw2iip08")))

(define-public crate-maud_macros-0.6.1 (c (n "maud_macros") (v "0.6.1") (d (list (d (n "maud") (r "= 0.6.1") (d #t) (k 0)))) (h "12d32sj2p21gcp6lxpi9phb477s312na36w0k0p7q8449pvvz9r1")))

(define-public crate-maud_macros-0.6.2 (c (n "maud_macros") (v "0.6.2") (d (list (d (n "maud") (r "= 0.6.2") (d #t) (k 0)))) (h "00nmgjqdsy9vq5v0v3qa0m059602a75yzbcj79dm7lx6g3rp43cz")))

(define-public crate-maud_macros-0.7.0 (c (n "maud_macros") (v "0.7.0") (d (list (d (n "maud") (r "= 0.7.0") (d #t) (k 0)))) (h "07a1l4ap9vmahl2c8x6760i70m5hh506b1dc71i306fhc5vq92rj")))

(define-public crate-maud_macros-0.7.1 (c (n "maud_macros") (v "0.7.1") (d (list (d (n "maud") (r "= 0.7.1") (d #t) (k 0)))) (h "1vr2bfpn7wbpw7ayvg6hypykd4mdxa1g83nwbcbp1n9714xf9xhk")))

(define-public crate-maud_macros-0.7.2 (c (n "maud_macros") (v "0.7.2") (d (list (d (n "maud") (r "= 0.7.2") (d #t) (k 0)))) (h "0s9kp7b62f5s9zgn45vlr2w78vnplh63rv57pwqb5xah683xvhdr")))

(define-public crate-maud_macros-0.7.3 (c (n "maud_macros") (v "0.7.3") (d (list (d (n "maud") (r "= 0.7.3") (d #t) (k 0)))) (h "0ng5jbs5x3rlg81nhldi0lp6hash52j7vxa3i3xh68jnfimvgb5c")))

(define-public crate-maud_macros-0.7.4 (c (n "maud_macros") (v "0.7.4") (d (list (d (n "maud") (r "= 0.7.4") (d #t) (k 0)))) (h "13f3x1gfqj96vwyc3j5ww9c8g8k7rsbyfvxgb49y558aiij94cs0")))

(define-public crate-maud_macros-0.8.0 (c (n "maud_macros") (v "0.8.0") (d (list (d (n "maud") (r "= 0.8.0") (d #t) (k 0)))) (h "06pin69v4in2gw08l94c6mv4wqhwjjv8xx3j0wlrls2w0zxxr48z")))

(define-public crate-maud_macros-0.8.1 (c (n "maud_macros") (v "0.8.1") (d (list (d (n "maud") (r "= 0.8.1") (d #t) (k 0)))) (h "0cf83cdfzqd50galvqhk1rj38rlw6yrqjj769mff7s6id1n6h5rq")))

(define-public crate-maud_macros-0.9.0 (c (n "maud_macros") (v "0.9.0") (d (list (d (n "maud") (r "= 0.9.0") (d #t) (k 0)))) (h "0ak51i4y2v9rp69mgix6rsqrxk4ygzawavsjmiqqhgf9yii81nbk")))

(define-public crate-maud_macros-0.9.1 (c (n "maud_macros") (v "0.9.1") (d (list (d (n "maud") (r "= 0.9.0") (d #t) (k 0)))) (h "1fq88c2ci538cwvyh0cvbrybq3b8m8iqy6xgqjwl0b1dhdn1ffk0")))

(define-public crate-maud_macros-0.9.2 (c (n "maud_macros") (v "0.9.2") (d (list (d (n "maud") (r "= 0.9.0") (d #t) (k 0)))) (h "08klb0dlwj2skfybd7bn94jgwlj6004lppzf5r2s74x3imwpgy1g")))

(define-public crate-maud_macros-0.10.0 (c (n "maud_macros") (v "0.10.0") (d (list (d (n "maud") (r "= 0.10.0") (d #t) (k 0)))) (h "112nqyck9rqznfcnj1ifbv0gcw0b0n02yf2ry8d57ga04sg382y5")))

(define-public crate-maud_macros-0.11.0 (c (n "maud_macros") (v "0.11.0") (d (list (d (n "maud") (r "= 0.11.0") (d #t) (k 0)))) (h "11f397yxa3svqgr775f14czrclxsgvb500cxf5pagyw6bni28jzz")))

(define-public crate-maud_macros-0.11.1 (c (n "maud_macros") (v "0.11.1") (d (list (d (n "maud") (r "^0.11.1") (d #t) (k 0)))) (h "1x5gqvpdqbfja5vsfc0z1pn0csm91yz3rpm2ylj6qa2jv2879jqp")))

(define-public crate-maud_macros-0.12.0 (c (n "maud_macros") (v "0.12.0") (d (list (d (n "maud") (r "^0.12.0") (d #t) (k 0)))) (h "0cd5751z3cld8svg5xmami9112w45bqnczvf07bfzkq2wz3zydxa")))

(define-public crate-maud_macros-0.13.0 (c (n "maud_macros") (v "0.13.0") (d (list (d (n "maud") (r "^0.13.0") (d #t) (k 0)))) (h "07vv29ryly4sd004wnln24rs3mz8kpmbzzpmpr343jv3r7nds8xm")))

(define-public crate-maud_macros-0.14.0 (c (n "maud_macros") (v "0.14.0") (d (list (d (n "maud") (r "^0.14.0") (d #t) (k 0)))) (h "0wi6jvr77qr80f2flxrjzsgaqr90hqvjh65h1hlbv77hcjx6x09m")))

(define-public crate-maud_macros-0.15.0 (c (n "maud_macros") (v "0.15.0") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.15.0") (d #t) (k 0)))) (h "0i3ixj739ivifvq2w7991l2p84p940l7h65n8vfdfqqa90bv8yvg")))

(define-public crate-maud_macros-0.16.0 (c (n "maud_macros") (v "0.16.0") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.16.0") (d #t) (k 0)))) (h "1g3m5ghpg5zhzdx6hrr13g93ziw1mv2vwhz4ygmmd99ws15js0mz")))

(define-public crate-maud_macros-0.16.1 (c (n "maud_macros") (v "0.16.1") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.16.0") (d #t) (k 0)))) (h "1qz766c5hlxngm94alm44lykpg0pi14dhnhk9vs5h2185jvz8928")))

(define-public crate-maud_macros-0.16.2 (c (n "maud_macros") (v "0.16.2") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.16.0") (d #t) (k 0)))) (h "0g3l7nrv0g03nkqzcjl4vy0vazp5sqn5g8qp5bdxkhd8g9q38j1m")))

(define-public crate-maud_macros-0.16.3 (c (n "maud_macros") (v "0.16.3") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.16.0") (d #t) (k 0)))) (h "0fcabiam0m9f2dgcv9a4lgkp6qkw0hcygpaj6rwpf84gmp928d45")))

(define-public crate-maud_macros-0.17.0 (c (n "maud_macros") (v "0.17.0") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "1jbr96m9klcwbdida36nk2qi30v84vgyy8a1ndbwh91n51bpi7wd")))

(define-public crate-maud_macros-0.17.1 (c (n "maud_macros") (v "0.17.1") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "1wms41r5cwkzm04b0c19gish0ayy7gigfm2gdhj9nyr9262grpkl")))

(define-public crate-maud_macros-0.17.2 (c (n "maud_macros") (v "0.17.2") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "0mffkyyy4vliwhyp9b3hwiyqnk23bg8mjynni95h958rgkbpg0qd")))

(define-public crate-maud_macros-0.17.3 (c (n "maud_macros") (v "0.17.3") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "1lxzykg5n2xxzfx2x13yr05nq6grkip6vq4k4d2vhhvh7skvznqh")))

(define-public crate-maud_macros-0.17.4 (c (n "maud_macros") (v "0.17.4") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "1nq8vscy5mlqzaghflayc448jrdl8fas5s7yg8xr2bp7ds4px18j")))

(define-public crate-maud_macros-0.17.5 (c (n "maud_macros") (v "0.17.5") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "02292vr97bnc8wsbp7jyzpfwkmy7kkln8hl26hnfp3iy6m9hzii1")))

(define-public crate-maud_macros-0.18.0 (c (n "maud_macros") (v "0.18.0") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "02kclcijw1shh3znnr383d5ii2c3frz84yqkd9r5b5b9zvf0likl")))

(define-public crate-maud_macros-0.18.1 (c (n "maud_macros") (v "0.18.1") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "0d9ds2c32w10skz694f5f460zw5g8jbaijyx3vpgfqb8p8nw8isf")))

(define-public crate-maud_macros-0.19.0 (c (n "maud_macros") (v "0.19.0") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "0mx4v89glg37x04y23z8cd8psmk3vbyn1bqf4cmm1azapfnz751z")))

(define-public crate-maud_macros-0.20.0 (c (n "maud_macros") (v "0.20.0") (d (list (d (n "literalext") (r "^0.1") (f (quote ("proc-macro"))) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)))) (h "0hfazfcw7qljlx2pbhrf86ga9p48rmdhxrlccvffwybzv8f7an3g")))

(define-public crate-maud_macros-0.21.0 (c (n "maud_macros") (v "0.21.0") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "0cfr781kq7s0szkybbnb1vvpbrfagwjqb3xggkanlyh5sz8hzbk2")))

(define-public crate-maud_macros-0.22.0 (c (n "maud_macros") (v "0.22.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1kcvbjx36zdvzvi96l4j2cvj388cllzaxvxmjqbwpnjzq3f2ddjy")))

(define-public crate-maud_macros-0.22.1 (c (n "maud_macros") (v "0.22.1") (d (list (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "01qcjy2akhyv40a5gbzk5x4s8rh82xq4mr9fhf4hgv6d1lank2lw")))

(define-public crate-maud_macros-0.22.2 (c (n "maud_macros") (v "0.22.2") (d (list (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "07jvcn3d99xbplimb3la1f6xriz9fy2jrgfqlc3i8rylmdc0383k")))

(define-public crate-maud_macros-0.22.3 (c (n "maud_macros") (v "0.22.3") (d (list (d (n "maud_htmlescape") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0xldpc0y8005ym478ld86w3n93kh6figgwf0kawq1snwij76z2g6")))

(define-public crate-maud_macros-0.23.0 (c (n "maud_macros") (v "0.23.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0m9kiqarn9ayyiv29jcmwz1pp2mxvqljjgz9gmni623l8sn30d22")))

(define-public crate-maud_macros-0.24.0 (c (n "maud_macros") (v "0.24.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1szjhg2h92rf71xj88vpaxmza38lidxbhbsxlbags22bybv19hg5")))

(define-public crate-maud_macros-0.25.0 (c (n "maud_macros") (v "0.25.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1lpfr53x82h69mpgqvr6mzn86yp1pbjmhq1177kgqkq2qdk5vs8b")))

(define-public crate-maud_macros-0.26.0 (c (n "maud_macros") (v "0.26.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "062f4w3367x1la8j6wkvv5ga9hxmsdw5kz0idfps1391xhw34igs")))

