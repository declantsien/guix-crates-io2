(define-module (crates-io ma ud maud_lints) #:use-module (crates-io))

(define-public crate-maud_lints-0.17.0 (c (n "maud_lints") (v "0.17.0") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)))) (h "0gh7vyxp4v3h7qqa7vaac7ph075y3jhynsp0fs4j86hxviwck50m")))

(define-public crate-maud_lints-0.17.4 (c (n "maud_lints") (v "0.17.4") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)))) (h "063j7y3r5v7l00zysx1jrwffxbsf211w569gbpnx8zv38rp113hz")))

(define-public crate-maud_lints-0.18.1 (c (n "maud_lints") (v "0.18.1") (d (list (d (n "if_chain") (r "^0.1") (d #t) (k 0)))) (h "0bhrsb9p85as03h2jz7sas604kfwfxbwn86ynykswi7lds7l86c5")))

