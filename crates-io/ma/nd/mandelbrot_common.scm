(define-module (crates-io ma nd mandelbrot_common) #:use-module (crates-io))

(define-public crate-mandelbrot_common-0.1.0 (c (n "mandelbrot_common") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (t "cfg(not(all(target_arch = \"wasm32\", not(target_os = \"emscripten\"))))") (k 0)))) (h "0szm4a0cyhmck2f6nj6s0qrjxfb6agmahrajx34sw6hw0qgw24gf")))

(define-public crate-mandelbrot_common-0.1.1 (c (n "mandelbrot_common") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (t "cfg(not(all(target_arch = \"wasm32\", not(target_os = \"emscripten\"))))") (k 0)))) (h "0z07kl0q234wba1c2r3gqir77573mrrmqk669d75n702ijz0j4vx")))

(define-public crate-mandelbrot_common-0.1.2 (c (n "mandelbrot_common") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (t "cfg(not(all(target_arch = \"wasm32\", not(target_os = \"emscripten\"))))") (k 0)))) (h "0v1di72hhqvi78d832qwk9q4i13i5a8x2lirrkjzqjaz52cyw16y")))

