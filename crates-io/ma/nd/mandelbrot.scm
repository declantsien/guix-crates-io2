(define-module (crates-io ma nd mandelbrot) #:use-module (crates-io))

(define-public crate-mandelbrot-0.1.0 (c (n "mandelbrot") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0fzxk3wdm8xhssgsfbsm2bfn2ayqch2f0mdn6lka09pw2fmk6ksq")))

(define-public crate-mandelbrot-0.1.1 (c (n "mandelbrot") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0bx971pc12wsvbv1p1s13ad30908g9yyxbkrn5pyk1blwixqv1bp")))

(define-public crate-mandelbrot-0.1.2 (c (n "mandelbrot") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "04as61bqim5qpzzlz48r7rgqbsqw8v6jzk7vkszn3s6jn3ch8mih")))

