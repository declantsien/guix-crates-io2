(define-module (crates-io ma nd mandelbrot-orbits-rust) #:use-module (crates-io))

(define-public crate-mandelbrot-orbits-rust-0.1.0 (c (n "mandelbrot-orbits-rust") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "text-colorizer") (r "^1.0.0") (d #t) (k 0)))) (h "0rvhkrpv7dpgjxxafjvx1svycbx53pr8022l4l2pc50j9zhw088v")))

