(define-module (crates-io ma nd mandarin) #:use-module (crates-io))

(define-public crate-mandarin-0.1.0 (c (n "mandarin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0p95961q6y2ihxp1522nxn4jpr5y29wi3yrx7zkqjg6f8s5qlp5h")))

