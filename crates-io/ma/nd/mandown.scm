(define-module (crates-io ma nd mandown) #:use-module (crates-io))

(define-public crate-mandown-0.1.0 (c (n "mandown") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)))) (h "04319pcqrbks5zx6drhd8lbv7javpr791n0lxvf8r7c67kmzckgz")))

(define-public crate-mandown-0.1.1 (c (n "mandown") (v "0.1.1") (d (list (d (n "deunicode") (r "^1.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)))) (h "1siqk9m9830fil1p1rnapc630b4j69ngsnm1flxln4x96i1hazwz")))

(define-public crate-mandown-0.1.2 (c (n "mandown") (v "0.1.2") (d (list (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1wn5qwknhdjlqhlhxnp6hihhzl277snr8p87svjqm9cwm4v2xikz")))

(define-public crate-mandown-0.1.3 (c (n "mandown") (v "0.1.3") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.0") (d #t) (k 0)))) (h "10ynryx0s7l5pph9sx6y7jyn8kalvhxw8wqgaiwb2q4nir33l90j")))

