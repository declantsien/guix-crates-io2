(define-module (crates-io ma nd mandrill_sender) #:use-module (crates-io))

(define-public crate-mandrill_sender-0.1.0 (c (n "mandrill_sender") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0ks0chrragj28483mpd6w3hpz7bqgcg97i3ddggydf2kdfn6k08n")))

