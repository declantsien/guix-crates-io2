(define-module (crates-io ma nd mandelbruhst-cli) #:use-module (crates-io))

(define-public crate-mandelbruhst-cli-0.1.0 (c (n "mandelbruhst-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron_conf"))) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0s52xgr5b482xvp8nr9a0hrf8jfzq4m3b0bsabgqi9j643fly3m8")))

