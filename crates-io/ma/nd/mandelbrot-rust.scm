(define-module (crates-io ma nd mandelbrot-rust) #:use-module (crates-io))

(define-public crate-mandelbrot-rust-0.1.0 (c (n "mandelbrot-rust") (v "0.1.0") (d (list (d (n "glium") (r "^0.25.1") (f (quote ("glutin"))) (k 0)) (d (n "imgui") (r "^0.1.0") (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.1.0") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.1.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rug") (r "^1.5.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1wq3whg25nrpvxrfc63npyc8fimw2hlicz2hxh90blssccw1vl9i")))

