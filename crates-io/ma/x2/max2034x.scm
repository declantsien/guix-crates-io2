(define-module (crates-io ma x2 max2034x) #:use-module (crates-io))

(define-public crate-max2034x-0.1.0 (c (n "max2034x") (v "0.1.0") (d (list (d (n "device-driver") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0b9alad9rxls434fxmb0v0d196819sgyr2sywwsq4ajqijm4kqdg") (y #t)))

(define-public crate-max2034x-0.1.1 (c (n "max2034x") (v "0.1.1") (d (list (d (n "device-driver") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "002ngrbdx0p2wb7gyh1s6b2fk7pn3flgsqb5asmwb7z6j8bxqsv3")))

(define-public crate-max2034x-0.1.3 (c (n "max2034x") (v "0.1.3") (d (list (d (n "device-driver") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "1b2h734g14kn49p797w6fyrjvf75wlya2phzvwxsdxcqrr26i6wm")))

