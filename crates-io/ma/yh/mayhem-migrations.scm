(define-module (crates-io ma yh mayhem-migrations) #:use-module (crates-io))

(define-public crate-mayhem-migrations-0.2.0-beta.1 (c (n "mayhem-migrations") (v "0.2.0-beta.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.10.0") (f (quote ("runtime-tokio-rustls" "sqlx-postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("test-util" "full"))) (d #t) (k 0)))) (h "0yv24gqhhdbwws50dcxhv4hwx2kc14brsj76kim1zc25cx5ss549")))

