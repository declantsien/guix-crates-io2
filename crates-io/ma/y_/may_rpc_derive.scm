(define-module (crates-io ma y_ may_rpc_derive) #:use-module (crates-io))

(define-public crate-may_rpc_derive-0.1.0 (c (n "may_rpc_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8jq66pk46ivxnmkcm65nyrk25dcw96xqzgcd0svndyqp1v59ry")))

(define-public crate-may_rpc_derive-0.1.1 (c (n "may_rpc_derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i22fzwn7h2d0zmjz5siczyjjcqn8y4818k2z04qc1zriwc1i84x")))

(define-public crate-may_rpc_derive-0.1.2 (c (n "may_rpc_derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnkhk41yz0dpcs47pn77yaxg6yjg9miipdlq9fzza9z9dd1gv1w")))

(define-public crate-may_rpc_derive-0.1.3 (c (n "may_rpc_derive") (v "0.1.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnfmxc7prvqagi8rqml76lfw7hvhymvh3fcgdrv9769ll7yzndn")))

(define-public crate-may_rpc_derive-0.1.4 (c (n "may_rpc_derive") (v "0.1.4") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1999bihj7a6k942mgw26niz67w89ylv97pabl6rw82yfn7a9frlb")))

