(define-module (crates-io ma y_ may_queue) #:use-module (crates-io))

(define-public crate-may_queue-0.1.0 (c (n "may_queue") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "0sqvmfhjmg16rmn24ilyymkw381b50bx4xi8c6p0q9qv4p5s6ff9")))

(define-public crate-may_queue-0.1.1 (c (n "may_queue") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "017j64zf7yffb8qrd5d516pw88vh4yydz7vsxvazx2msnl9zyb78")))

(define-public crate-may_queue-0.1.2 (c (n "may_queue") (v "0.1.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1ga2qkawba2gbf1d0bb4c0hl3vsjsgv3vn20jyywqi7ivmlsakzn")))

(define-public crate-may_queue-0.1.3 (c (n "may_queue") (v "0.1.3") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1ix78sj4nngsf0w182nl5yrppnb7xdcbwi0q5vjchvly863z13p0")))

(define-public crate-may_queue-0.1.4 (c (n "may_queue") (v "0.1.4") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1la455gnqny1mqc55nc49yvd2ian6cssqckli3hnj4si16i78h9j")))

(define-public crate-may_queue-0.1.5 (c (n "may_queue") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "046i6gjcd93syp2kzvhikngf6g42lgg4vxr6ac908caq95cml9yf")))

(define-public crate-may_queue-0.1.6 (c (n "may_queue") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0pdfpmfk98irmc261rxzb7agv7y5j410a2gql5dwnpa181y1105j")))

(define-public crate-may_queue-0.1.7 (c (n "may_queue") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1s3a2agx2ci8mlp3dj8rdaqmdkg367lb2zzsbp64dai9fvbfd37q")))

(define-public crate-may_queue-0.1.8 (c (n "may_queue") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "054nzsvs9ymj62d6zf8rrw9kc8a0m4pzkkkzrwlv4w8pr67vyiqy")))

(define-public crate-may_queue-0.1.9 (c (n "may_queue") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1v12pkv68sq7gk9zb6byncxs3i0910labdd9h5pzl7m4776nwd94")))

(define-public crate-may_queue-0.1.10 (c (n "may_queue") (v "0.1.10") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1plkpnxbyfdbcp7ily7dalbkhdlxyskfn70m0sd9m3kxvs2pggdr")))

(define-public crate-may_queue-0.1.11 (c (n "may_queue") (v "0.1.11") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1s453svmwl8fbdav59dc37z2pncnbmjqi2w22maih52nx8v3p99p")))

(define-public crate-may_queue-0.1.12 (c (n "may_queue") (v "0.1.12") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1z7nw0hzl52azz4ii3x7xx6yh5bcs23mmfh38kpf9qljfn47x1v7")))

(define-public crate-may_queue-0.1.13 (c (n "may_queue") (v "0.1.13") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "19xrik5m5p98rnl9n8mlq114w9cpgm8gx3kqi47ni0igds0acv18")))

(define-public crate-may_queue-0.1.14 (c (n "may_queue") (v "0.1.14") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "02l1br3nfy0l3ws9zyg0zykyb68n1q354i4ga95kxqmq6ylrds05")))

(define-public crate-may_queue-0.1.15 (c (n "may_queue") (v "0.1.15") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "131x7iyry1d3lhdzlaxk8ki1pjwh12sm118wyri81bnnrqnmm08f")))

(define-public crate-may_queue-0.1.16 (c (n "may_queue") (v "0.1.16") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "132gg2x7zjxgdwc440skc86ap8xpmjjzlsfna274l37hjhbls5q6")))

(define-public crate-may_queue-0.1.17 (c (n "may_queue") (v "0.1.17") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0z5j0kmgb7w7dxlfinfffq6w979mdj650ys2raab28fs9r348dzm")))

(define-public crate-may_queue-0.1.18 (c (n "may_queue") (v "0.1.18") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0i1jxs4ic9svdzzi1m5fq9mmmvzki0m9a7zda5h137kdd2y929vx")))

(define-public crate-may_queue-0.1.19 (c (n "may_queue") (v "0.1.19") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0y5hd34dbgvarg87k34qp274k9mdi7x4s4w5x58jch58ycka8s06")))

(define-public crate-may_queue-0.1.20 (c (n "may_queue") (v "0.1.20") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "07lxr5zms0xbfaqfl6flynz2p1mgyzw5n1f3qdbxwd0j2a6yjshx")))

(define-public crate-may_queue-0.1.21 (c (n "may_queue") (v "0.1.21") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0v37n1jfxa0ggxy4ai55ir2iwc3c4n6m1nwzn1ans7qwlycaavsg") (f (quote (("inner_cache") ("default" "inner_cache"))))))

(define-public crate-may_queue-0.1.22 (c (n "may_queue") (v "0.1.22") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1i062ynwfqzf43ifmh08pq9gzq977dvyiy3wwcbdk6p6k9c9lxnf") (f (quote (("inner_cache") ("default" "inner_cache"))))))

