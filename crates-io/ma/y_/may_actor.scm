(define-module (crates-io ma y_ may_actor) #:use-module (crates-io))

(define-public crate-may_actor-0.1.0 (c (n "may_actor") (v "0.1.0") (d (list (d (n "may") (r "^0.2") (d #t) (k 0)))) (h "09fjn24sycgdf2l2xk0lv4lzgqzfp6ca58m17pmy0myxkaj2ic1c")))

(define-public crate-may_actor-0.1.1 (c (n "may_actor") (v "0.1.1") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0s6v4wac69846pzf5nin1fxdandyrfv3an7bpcz75fvj1hybhhb1")))

(define-public crate-may_actor-0.1.2 (c (n "may_actor") (v "0.1.2") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "089lwz65ii6l0xjf06pxs8l14mnvnpxym4lmh6l3kzkll61adphl")))

(define-public crate-may_actor-0.2.0 (c (n "may_actor") (v "0.2.0") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "1y5vv31gx1gx49qzy7n4m9dhv63fnr7qrbny9h99bpq3zi28kifb")))

(define-public crate-may_actor-0.2.1 (c (n "may_actor") (v "0.2.1") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "113ys5q8kkps13qp58hph999qh91hc4f111namawchqdsigrz5sh")))

(define-public crate-may_actor-0.2.2 (c (n "may_actor") (v "0.2.2") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0dv3zqdwkh3274xhvyq5i9i41dl5a1p92h7cmwmlvdn51digd33z")))

