(define-module (crates-io ma y_ may_waiter) #:use-module (crates-io))

(define-public crate-may_waiter-0.1.0 (c (n "may_waiter") (v "0.1.0") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "1352v76fc7z7f1wb9y0m2dfl9zpppfvqry6n4pv9hq17dg1dai3c")))

(define-public crate-may_waiter-0.1.1 (c (n "may_waiter") (v "0.1.1") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "11i2cb78rdmc4ghbhmv31n1pgpimxq62byn5hdfraq8jxzmjgipa")))

(define-public crate-may_waiter-0.1.2 (c (n "may_waiter") (v "0.1.2") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0ws0s34igkszx48i1cbhm9pycrl1n88rv3i36ccqi578dffj4dms")))

(define-public crate-may_waiter-0.1.3 (c (n "may_waiter") (v "0.1.3") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0jgyfr08w2pbdfms6imd07n3gc59zyyfg5mlir44ns4sh3d96l45")))

(define-public crate-may_waiter-0.1.4 (c (n "may_waiter") (v "0.1.4") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0i9jpa6r2pjf9n5zyxz7cdqi5153395i7r001780842d71yi76d6")))

(define-public crate-may_waiter-0.1.5 (c (n "may_waiter") (v "0.1.5") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0r1p7s44mm4z34phaqx3m4mj7a464nwxvdvq3nc9iimbhmlam1nw")))

(define-public crate-may_waiter-0.1.6 (c (n "may_waiter") (v "0.1.6") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "0hpc4480r48853xaamqqbrz299g6n4g6irmbfs05d3iprzvp27kk")))

(define-public crate-may_waiter-0.1.7 (c (n "may_waiter") (v "0.1.7") (d (list (d (n "may") (r "^0.3") (d #t) (k 0)))) (h "1dm7nrmz2kj9ijmn03hfcp7hsxa18p95zhn8qlcsgiphwnsr9kdf")))

(define-public crate-may_waiter-0.1.8 (c (n "may_waiter") (v "0.1.8") (d (list (d (n "may") (r "^0.3.32") (d #t) (k 0)))) (h "0cb0d4dfw11x6d8s0hjjspxcis3fkmbr1ki7d3cji6qbsaryx1hd")))

(define-public crate-may_waiter-0.1.9 (c (n "may_waiter") (v "0.1.9") (d (list (d (n "may") (r "^0.3.32") (d #t) (k 0)))) (h "03yflx5fmp2yd1cpsz5nzbmvvkb98pwwcxwm8nwhb3148nbi66nn")))

(define-public crate-may_waiter-0.1.10 (c (n "may_waiter") (v "0.1.10") (d (list (d (n "may") (r "^0.3.32") (d #t) (k 0)))) (h "0rw0bvay6rrxm104jmfpjwdhf2nw1chj1czj81p5v6k59ha26md6")))

