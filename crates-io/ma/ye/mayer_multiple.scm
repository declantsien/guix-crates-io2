(define-module (crates-io ma ye mayer_multiple) #:use-module (crates-io))

(define-public crate-mayer_multiple-0.0.1 (c (n "mayer_multiple") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0sdvwva6pqaj2in6llsfapxspsvmv6aamvvqyx5xr2pbkavz5kim")))

