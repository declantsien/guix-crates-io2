(define-module (crates-io ma ri marine-timestamp-macro) #:use-module (crates-io))

(define-public crate-marine-timestamp-macro-0.6.2 (c (n "marine-timestamp-macro") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "04r68my6zghg99kwz4kascrgvqc5d2ls3jah7pn3kzfkrh9bdzpn")))

(define-public crate-marine-timestamp-macro-0.6.7-alpha.0 (c (n "marine-timestamp-macro") (v "0.6.7-alpha.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0ddjz3b00rmk78pvmr18rjgv1fhik76w76ign8f311airhq4ydgi")))

(define-public crate-marine-timestamp-macro-0.6.5 (c (n "marine-timestamp-macro") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0az3dy8qprhvbip8zjsdjy2p7h008p8jmgmknkkvvmbqhabdjnaj")))

(define-public crate-marine-timestamp-macro-0.6.6 (c (n "marine-timestamp-macro") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "184pkmjblqdv80px4xy6iajlccna4wb273gas8cjlbrhazbmvmjl")))

(define-public crate-marine-timestamp-macro-0.6.8 (c (n "marine-timestamp-macro") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1cip196qa6l7lhhj93fbxyyaml3s1d3va3rggq5kcqm2fxsh7czc")))

(define-public crate-marine-timestamp-macro-0.6.9 (c (n "marine-timestamp-macro") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1z6pp4cjc587f3bv4dfa2xnghr25kl6s9rd2y84idlk7apdwg52r")))

(define-public crate-marine-timestamp-macro-0.6.10 (c (n "marine-timestamp-macro") (v "0.6.10") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1rpp52p7224pgnzii637vkydqpvsqg1m0ms0fwm6b6xx61b1mbi9")))

(define-public crate-marine-timestamp-macro-0.6.11 (c (n "marine-timestamp-macro") (v "0.6.11") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1mk758gjbfl9kcgw6cyl37czlkkmcm9z5qadhy6gismgqjfb63i8")))

(define-public crate-marine-timestamp-macro-0.6.12 (c (n "marine-timestamp-macro") (v "0.6.12") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "06kidrprzjnabx3q7gpskq68d258nyg2wvwpaabj8jnbacqqy8hq")))

(define-public crate-marine-timestamp-macro-0.6.13 (c (n "marine-timestamp-macro") (v "0.6.13") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0nz8hy04nsqpzk0x9c2mgxn903m7xy8z2v728pwp77dr4dcp8mjn")))

(define-public crate-marine-timestamp-macro-0.6.14 (c (n "marine-timestamp-macro") (v "0.6.14") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1sccpv72yyxiyal88kadzan6jksiyc81xwhbd13gbhn07k082vnb")))

(define-public crate-marine-timestamp-macro-0.6.15 (c (n "marine-timestamp-macro") (v "0.6.15") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "07vxn9ldi7wykyf89lx0lixq0nhq2jl5zz2cl72qmyggmnqzbac5")))

(define-public crate-marine-timestamp-macro-0.7.0 (c (n "marine-timestamp-macro") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0c02wsksbafywnq2ad722l9c7qwig01ajkf9n3z72pfpxiydb5sj")))

(define-public crate-marine-timestamp-macro-0.7.1 (c (n "marine-timestamp-macro") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0bybaxa3bd9cqrbank4a4rinj90s8d3v1bxnl024v7vyfmx5b93y")))

(define-public crate-marine-timestamp-macro-0.8.0 (c (n "marine-timestamp-macro") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "194h37mkqjxk3cfip416cmvdgj17dqlc8lk2z8yvc5qf0la95xgv")))

(define-public crate-marine-timestamp-macro-0.8.1 (c (n "marine-timestamp-macro") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "10i28j3q2xdh9s9y9fp5r586f1jwfxwczz1gqv6dwp7327m59ll0")))

(define-public crate-marine-timestamp-macro-0.9.0 (c (n "marine-timestamp-macro") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "0kkawkrb01lshi4hl2x8r9bfyz48fi9vm91p6xgdbqi1sxnrsfy3")))

(define-public crate-marine-timestamp-macro-0.10.0 (c (n "marine-timestamp-macro") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "1yls9c49ih9dm7iavgzd7z3lynx3i8wdb2a08w5r9gzxfk8mdydc")))

(define-public crate-marine-timestamp-macro-0.10.1 (c (n "marine-timestamp-macro") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "191c4h1pxdkzr87v2fwc2dkh0s6appl0lsx86k6b8bnn1y3vgk2m")))

(define-public crate-marine-timestamp-macro-0.10.2 (c (n "marine-timestamp-macro") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1nws0r86xgr4sbi88lrsrj9q1ydj1gyn939zyc6991bxri6mnini")))

(define-public crate-marine-timestamp-macro-0.10.3 (c (n "marine-timestamp-macro") (v "0.10.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0q95rfxkvv0xjw6nans83f577d64xivkl19qbsvrmdqkrc1jm0bz")))

(define-public crate-marine-timestamp-macro-0.11.0 (c (n "marine-timestamp-macro") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1fp0p98656qw6a2rwfw14v4d7ddq3fn0zp27a7fwh99qw72d65f3")))

(define-public crate-marine-timestamp-macro-0.12.0 (c (n "marine-timestamp-macro") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0i31jqiyvdg7rnfiipryfcikzdhac7d2vfpyl75iwrkh8mk0vv27")))

(define-public crate-marine-timestamp-macro-0.13.0 (c (n "marine-timestamp-macro") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1q3sphdsx6m68b3fny46iwcp36cbsn2rrqxbwxjjmp14fwsqcl0s")))

(define-public crate-marine-timestamp-macro-0.14.0 (c (n "marine-timestamp-macro") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0zaha0ncwh8djgr81ig1csn793nrz2gm26iay4z597x2q1x2cgyh")))

