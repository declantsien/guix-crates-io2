(define-module (crates-io ma ri marine-sqlite-connector) #:use-module (crates-io))

(define-public crate-marine-sqlite-connector-0.4.0 (c (n "marine-sqlite-connector") (v "0.4.0") (d (list (d (n "fluence") (r "^0.6.3") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1yk3jbj4hsir1sjr5dar4x5lqh5rcbwfnb4c51rgnlk3sdxgvhl5")))

(define-public crate-marine-sqlite-connector-0.4.1 (c (n "marine-sqlite-connector") (v "0.4.1") (d (list (d (n "fluence") (r "^0.6.4") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "19dinnrkfah5hjx295n69fl5xc91xzxv904a5dgmk4cxlh0kiz3g")))

(define-public crate-marine-sqlite-connector-0.5.0 (c (n "marine-sqlite-connector") (v "0.5.0") (d (list (d (n "marine-rs-sdk") (r "^0.6.10") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1fqfljh7yvw6c53j1n6h962pkjqycl8zqr7fn9pdhlm8aijzdbnp")))

(define-public crate-marine-sqlite-connector-0.5.1 (c (n "marine-sqlite-connector") (v "0.5.1") (d (list (d (n "marine-rs-sdk") (r "^0.6.10") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.2.0") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1ykf2b15ddaaamjc2pz2nn647nygvp23lv8k065aahym8fmgs565")))

(define-public crate-marine-sqlite-connector-0.5.2 (c (n "marine-sqlite-connector") (v "0.5.2") (d (list (d (n "marine-rs-sdk") (r "^0.6.10") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.2.0") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1liln0y0fzizvfgynikgjh3cdz3sh9x11vhcm7rp9d71kyfgwjis")))

(define-public crate-marine-sqlite-connector-0.6.0 (c (n "marine-sqlite-connector") (v "0.6.0") (d (list (d (n "marine-rs-sdk") (r "^0.7.0") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.2.0") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1z11a0i8qp3kmwz0mskin1282lgfcbdjxaxsbzyh1kbads37jq24")))

(define-public crate-marine-sqlite-connector-0.8.0 (c (n "marine-sqlite-connector") (v "0.8.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.0") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.2.0") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0yw9v729k8rsfwqkynysim3xdg9d9gss0d1wrkxf3rp5m35a47gc")))

(define-public crate-marine-sqlite-connector-0.8.1 (c (n "marine-sqlite-connector") (v "0.8.1") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.9.1") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "01s57kcrm5b32ni6nq34blpr5n3959inqka8bn9ig7q92q2s7sl6")))

(define-public crate-marine-sqlite-connector-0.8.2 (c (n "marine-sqlite-connector") (v "0.8.2") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.9.1") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "13p8694w6blnbjyapggi486j6mq4by9djwbz9ryvhwzg6z0frsv4")))

(define-public crate-marine-sqlite-connector-0.9.0 (c (n "marine-sqlite-connector") (v "0.9.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.10.2") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "1bqv4h7nh766yy41bcng2m6f44sf4qz1756jcxa2vbsn1wmgdch2")))

(define-public crate-marine-sqlite-connector-0.9.1 (c (n "marine-sqlite-connector") (v "0.9.1") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.11.0") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "0z9ddfpsp09x5izcqvmh6c9rxb79va3yciag6an0imwkr062lmcj")))

(define-public crate-marine-sqlite-connector-0.9.2 (c (n "marine-sqlite-connector") (v "0.9.2") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.12.0") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "0l4m0iq1mx37569aj7v3d6k0mqbfcm9p5zra13pdz024b8fabrj1")))

(define-public crate-marine-sqlite-connector-0.9.3 (c (n "marine-sqlite-connector") (v "0.9.3") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.12.1") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "1bc0sbar96b88iji2xgkhi48ig794m7y3ci6d8kqivnpifs7ybf1")))

(define-public crate-marine-sqlite-connector-0.9.4 (c (n "marine-sqlite-connector") (v "0.9.4") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.3") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.12.1") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "0q14za9apxdny7j1blkp04g3m9zlrxhb19gbjq33km4jkam2923w")))

(define-public crate-marine-sqlite-connector-0.10.0 (c (n "marine-sqlite-connector") (v "0.10.0") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.13.0") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "1s8l8gbvz5j4v2jssw42cm4pb9qlyxkw54kxcybpsc42jd0xa8gb")))

(define-public crate-marine-sqlite-connector-0.11.0 (c (n "marine-sqlite-connector") (v "0.11.0") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "marine-rs-sdk-test") (r "^0.15.0") (d #t) (k 2)) (d (n "temporary") (r "^0.7") (d #t) (k 2)))) (h "0lch3l5rp9qqaq12axx72jfzgivyr48bhwb2aam5jzy0x33q4k8w")))

