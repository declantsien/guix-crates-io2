(define-module (crates-io ma ri marid) #:use-module (crates-io))

(define-public crate-marid-0.0.1 (c (n "marid") (v "0.0.1") (d (list (d (n "chan") (r ">= 0.1.14") (d #t) (k 0)) (d (n "chan-signal") (r ">= 0.1.4") (d #t) (k 0)) (d (n "crossbeam") (r ">= 0.1.5") (d #t) (k 0)))) (h "1c2rcrzwgly22vw2b0p79jhiw483w2hmrkn2vx44xflvhqkyxyfp")))

(define-public crate-marid-0.0.2 (c (n "marid") (v "0.0.2") (d (list (d (n "chan") (r ">= 0.1.14") (d #t) (k 0)) (d (n "chan-signal") (r ">= 0.1.4") (d #t) (k 0)) (d (n "crossbeam") (r ">= 0.1.5") (d #t) (k 0)))) (h "1jcaknbaknfxvkm5g3i8k96cw7h0xc81yigpr08ck9zbym2098jv")))

(define-public crate-marid-0.1.0 (c (n "marid") (v "0.1.0") (d (list (d (n "chan") (r ">= 0.1.14") (d #t) (k 0)) (d (n "chan-signal") (r ">= 0.1.4") (d #t) (k 0)) (d (n "crossbeam") (r ">= 0.1.5") (d #t) (k 0)))) (h "04wbdfylpqmgpclpcxa2jaqrkfmsf1f3l7b05laagac9c5gg6zcr")))

