(define-module (crates-io ma ri marine-build-rs-generator) #:use-module (crates-io))

(define-public crate-marine-build-rs-generator-0.4.0 (c (n "marine-build-rs-generator") (v "0.4.0") (d (list (d (n "marine-test-macro-impl") (r "=0.4.0") (d #t) (k 0)))) (h "0z0paxjvb7v94xd5mfs21qcvrl6szij99sx0scilwmczlig747f8")))

(define-public crate-marine-build-rs-generator-0.4.1 (c (n "marine-build-rs-generator") (v "0.4.1") (d (list (d (n "marine-test-macro-impl") (r "=0.4.1") (d #t) (k 0)))) (h "1qgfx7b7jcd3bhq8637n0nr5h5ibamlnsvi2bg6q92ajy1z40201")))

(define-public crate-marine-build-rs-generator-0.5.0 (c (n "marine-build-rs-generator") (v "0.5.0") (d (list (d (n "marine-test-macro-impl") (r "=0.5.0") (d #t) (k 0)))) (h "1986xhlgsvqcg25n4yl819a7cknri8ad94r0v9irg8j3v3qgvi48")))

(define-public crate-marine-build-rs-generator-0.6.0 (c (n "marine-build-rs-generator") (v "0.6.0") (d (list (d (n "marine-test-macro-impl") (r "=0.6.0") (d #t) (k 0)))) (h "1n7lg8lkwfpgymsm8z520sb5p7c6zayq0q1rz4xsi0pjaflzyxp2")))

(define-public crate-marine-build-rs-generator-0.7.0 (c (n "marine-build-rs-generator") (v "0.7.0") (d (list (d (n "marine-test-macro-impl") (r "=0.7.0") (d #t) (k 0)))) (h "1fi4vjdhkl0j29j1ll8ja5pldaympab8n08xjn44v3xbk1bp9sgs")))

(define-public crate-marine-build-rs-generator-0.7.1 (c (n "marine-build-rs-generator") (v "0.7.1") (d (list (d (n "marine-test-macro-impl") (r "=0.7.1") (d #t) (k 0)))) (h "1a2mg1g8znvpdhqi4vsjqsxlpq50d2crw7wd1lqydg8gxyk190g7")))

(define-public crate-marine-build-rs-generator-0.8.0 (c (n "marine-build-rs-generator") (v "0.8.0") (d (list (d (n "marine-test-macro-impl") (r "=0.8.0") (d #t) (k 0)))) (h "0vyl2mmwm31ai0f2chypi0yb6lp1593isjgc1zwiyjbwpj5b2lj1")))

(define-public crate-marine-build-rs-generator-0.8.1 (c (n "marine-build-rs-generator") (v "0.8.1") (d (list (d (n "marine-test-macro-impl") (r "=0.8.1") (d #t) (k 0)))) (h "18laz1jyapadnd7dz0mmjw78p16y6q7fl67ark2z07w782ygmr9q")))

(define-public crate-marine-build-rs-generator-0.8.2 (c (n "marine-build-rs-generator") (v "0.8.2") (d (list (d (n "marine-test-macro-impl") (r "=0.8.2") (d #t) (k 0)))) (h "0fxvc39g7yz414fyksay399wv8xybsydqsnq4335m4vcmdg0li8f")))

(define-public crate-marine-build-rs-generator-0.9.0 (c (n "marine-build-rs-generator") (v "0.9.0") (d (list (d (n "marine-test-macro-impl") (r "=0.9.0") (d #t) (k 0)))) (h "0l5dapg418slamnz6xmp69g9c5q9n0rz7ivzf00yidcala88amg4")))

(define-public crate-marine-build-rs-generator-0.9.1 (c (n "marine-build-rs-generator") (v "0.9.1") (d (list (d (n "marine-test-macro-impl") (r "=0.9.1") (d #t) (k 0)))) (h "04zapv4nzhjznqsr970cq7k6hy15g00cgbkx58fvg0qrg9amyvd3")))

(define-public crate-marine-build-rs-generator-0.10.0 (c (n "marine-build-rs-generator") (v "0.10.0") (d (list (d (n "marine-test-macro-impl") (r "=0.10.0") (d #t) (k 0)))) (h "1y7wc6a645h6wz0mwk3qqnn58f22k6hfm5zkym20vai0if17rn7a")))

(define-public crate-marine-build-rs-generator-0.10.2 (c (n "marine-build-rs-generator") (v "0.10.2") (d (list (d (n "marine-test-macro-impl") (r "=0.10.2") (d #t) (k 0)))) (h "1cc4zmfibja0nlrbrnmmgimfhh2c7mxdk5j6irir8mjs2ydhfpih")))

(define-public crate-marine-build-rs-generator-0.11.0 (c (n "marine-build-rs-generator") (v "0.11.0") (d (list (d (n "marine-test-macro-impl") (r "=0.11.0") (d #t) (k 0)))) (h "17fr8caa8qm7dizypy9i7s3gsvzpqggr0va04fc6gj4wbwlc4w21")))

(define-public crate-marine-build-rs-generator-0.11.1 (c (n "marine-build-rs-generator") (v "0.11.1") (d (list (d (n "marine-test-macro-impl") (r "=0.11.1") (d #t) (k 0)))) (h "1d99w0vsi5rln28cjcp9vkh61blcjsyc6r70w0qsw712cqz08f0p")))

(define-public crate-marine-build-rs-generator-0.12.0 (c (n "marine-build-rs-generator") (v "0.12.0") (d (list (d (n "marine-test-macro-impl") (r "=0.12.0") (d #t) (k 0)))) (h "1ncbprmy5fkb0rn3m0wp5902xnia47pn7q5j4dgax3bqhniq2n5s")))

(define-public crate-marine-build-rs-generator-0.12.1 (c (n "marine-build-rs-generator") (v "0.12.1") (d (list (d (n "marine-test-macro-impl") (r "=0.12.1") (d #t) (k 0)))) (h "09axb6mpjnnyfs8bsr4s3skvl34djizdch9kfgynxpvs49hcffvk")))

(define-public crate-marine-build-rs-generator-0.13.0 (c (n "marine-build-rs-generator") (v "0.13.0") (d (list (d (n "marine-test-macro-impl") (r "=0.13.0") (d #t) (k 0)))) (h "100szqxmdi8f5r6a3p45cvdcmchhj5yy4xb8rrc2jc31m0masn54")))

(define-public crate-marine-build-rs-generator-0.14.0 (c (n "marine-build-rs-generator") (v "0.14.0") (d (list (d (n "marine-test-macro-impl") (r "=0.14.0") (d #t) (k 0)))) (h "1ynyy8pwyziggqqi58yqxpbvzf4fza9kcyc9jgpjnkaca3m569hx")))

(define-public crate-marine-build-rs-generator-0.15.0 (c (n "marine-build-rs-generator") (v "0.15.0") (d (list (d (n "marine-test-macro-impl") (r "=0.15.0") (d #t) (k 0)))) (h "053nxvws8bhpmjln38lmdrqvqlicmfyv8hmifhwyav2gb6hl7yz0")))

(define-public crate-marine-build-rs-generator-0.16.0 (c (n "marine-build-rs-generator") (v "0.16.0") (d (list (d (n "marine-test-macro-impl") (r "=0.16.0") (d #t) (k 0)))) (h "1lr5vqs8b0idbjsygavv51mjybb671iayvcv35r2ki1w1jgga1l7")))

(define-public crate-marine-build-rs-generator-0.16.1 (c (n "marine-build-rs-generator") (v "0.16.1") (d (list (d (n "marine-test-macro-impl") (r "=0.16.1") (d #t) (k 0)))) (h "16jwqb7zv7z27jhd8wn4lrcqcxahsiqmjc1x812zb7ff0asj7a8h")))

