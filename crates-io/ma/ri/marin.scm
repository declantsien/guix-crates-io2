(define-module (crates-io ma ri marin) #:use-module (crates-io))

(define-public crate-marin-0.1.0 (c (n "marin") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0r54apwv3l9p09z7xsa3q60c9yr81n0wsikg08vda1j5kbk6acj2")))

