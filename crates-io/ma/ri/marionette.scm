(define-module (crates-io ma ri marionette) #:use-module (crates-io))

(define-public crate-marionette-0.1.0 (c (n "marionette") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0s70m8sq5nnd6cs0133j6a6anzhxiyjpr31v1jcig6dcn4r8w3xk") (y #t)))

(define-public crate-marionette-0.2.0 (c (n "marionette") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1hqfhaqsizazp291qfs0hi4bp008qbpwkczvmaacdb4092q7gnnr")))

(define-public crate-marionette-0.2.1 (c (n "marionette") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0cwfl5cqfq703az26w41zy0zi7i55c7q8w82xrkb7pxy51xj0ca9") (y #t)))

(define-public crate-marionette-0.3.0 (c (n "marionette") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0sdid9p6wnw5y6cyn849996lx510aacfrdxa341j93i0c3xc5h9q")))

(define-public crate-marionette-0.4.0 (c (n "marionette") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0fn2b9rbyjsws5932iv45cdq8y69i4mldcl8alp8y8gisjxscd5q")))

(define-public crate-marionette-0.5.0 (c (n "marionette") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1ys7xgdw68g47mlqa310lvi8blzgsb07ginb4vrwf818pldwsgbk")))

