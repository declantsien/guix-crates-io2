(define-module (crates-io ma ri marine-min-it-version) #:use-module (crates-io))

(define-public crate-marine-min-it-version-0.1.0 (c (n "marine-min-it-version") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1lfvkxp32mkn24hgzx3zxy1i2770c5hhxw3cq6w1062k99snj0l9")))

(define-public crate-marine-min-it-version-0.1.1 (c (n "marine-min-it-version") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0as03b2dl8hl9raj76pzjfjsylinjwqspd4z7rmvnvf0rjyb1g6h")))

(define-public crate-marine-min-it-version-0.2.0 (c (n "marine-min-it-version") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.13") (d #t) (k 0)))) (h "0p968kspcnbqqyi1lvj5f9b7qvp10gza4s5wv7wnmjpi6c8j13dg")))

(define-public crate-marine-min-it-version-0.2.1 (c (n "marine-min-it-version") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)))) (h "0ngiw56jr9jgicabzp97di23y7cmgiqpx1kfcr0hjny43p9i2cnj")))

(define-public crate-marine-min-it-version-0.3.0 (c (n "marine-min-it-version") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)))) (h "0cjvanwx1k89dm475b5hmadc0mgzlwqh9vcp9gypy3ny4b8iyl2a")))

(define-public crate-marine-min-it-version-0.3.1 (c (n "marine-min-it-version") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "00ai1yyw27ipnkzaqnzx225aq1m85jvbdkns1kskv8ds1b1y6jx8")))

(define-public crate-marine-min-it-version-0.3.2 (c (n "marine-min-it-version") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1mgi6fqp42ij9hv0r3b97xrlkiv3kc3brqxj0sxszj9xkm66wzhl")))

