(define-module (crates-io ma ri maria) #:use-module (crates-io))

(define-public crate-maria-0.7.0 (c (n "maria") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18bh33cxy29292lv4dyj946h7jj20rvqg9y4p5yidmixn75br1kf")))

(define-public crate-maria-0.7.5 (c (n "maria") (v "0.7.5") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12r7yhszp1b14dls2dxqhx29zbq0c83agqz75nsa117w4nzybs1d")))

(define-public crate-maria-0.7.6 (c (n "maria") (v "0.7.6") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "09k89f6cqpxh8kkfcnxnclsgrh6mqdvw2f8kk93x9kgn9hjar1xa")))

(define-public crate-maria-0.8.0 (c (n "maria") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v12cnsm4dab5l21x1365hp0csvnx9c878i7lb094k2awxwic0im")))

(define-public crate-maria-0.8.1 (c (n "maria") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v6rc0wmrh078pysv8xnrww0f07as008w93i0ihc1rmkcv2aajkf")))

