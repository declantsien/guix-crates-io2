(define-module (crates-io ma ri marinefetch) #:use-module (crates-io))

(define-public crate-marinefetch-0.1.0 (c (n "marinefetch") (v "0.1.0") (d (list (d (n "cliprint") (r "^0.1.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.48.0") (d #t) (k 0)) (d (n "sctk") (r "^0.17.0") (d #t) (k 0) (p "smithay-client-toolkit")) (d (n "users") (r "^0.11.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30") (d #t) (k 0)) (d (n "zbus") (r "^3.11.0") (d #t) (k 0)))) (h "1k6w511hbf0zi9m43dgj83016df9sdzp0c0pabb8s8pzjdlk64c5")))

