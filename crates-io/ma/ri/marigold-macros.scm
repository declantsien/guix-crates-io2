(define-module (crates-io ma ri marigold-macros) #:use-module (crates-io))

(define-public crate-marigold-macros-0.1.0 (c (n "marigold-macros") (v "0.1.0") (d (list (d (n "marigold-grammar") (r "^0.1") (d #t) (k 0)))) (h "1lgk0d9fzcvsqmnl7rlzgz58sgqm13k0gj824vdakimnz2faq26w")))

(define-public crate-marigold-macros-0.1.1 (c (n "marigold-macros") (v "0.1.1") (d (list (d (n "marigold-grammar") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "14lq283xzkfv5676s04hspnysmsjdwjyypvqlvs5n874swh595lj")))

(define-public crate-marigold-macros-0.1.2 (c (n "marigold-macros") (v "0.1.2") (d (list (d (n "marigold-grammar") (r "=0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "01anlfsm3i5hizlfpkm170r9nrz37w1akimbmk7cr0s8mqmnw91i")))

(define-public crate-marigold-macros-0.1.3 (c (n "marigold-macros") (v "0.1.3") (d (list (d (n "marigold-grammar") (r "=0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1xrdz78z3j4wjvkjxq8620dd09abk7ik44d5k203vgigbz2in7fg")))

(define-public crate-marigold-macros-0.1.4 (c (n "marigold-macros") (v "0.1.4") (d (list (d (n "marigold-grammar") (r "=0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0cba22g5qvwam7pm8ziszqf72flyc831qqnn0kim0d7d7qj2pq16")))

(define-public crate-marigold-macros-0.1.5 (c (n "marigold-macros") (v "0.1.5") (d (list (d (n "marigold-grammar") (r "=0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0kkqw4vm48nbrz4q24h3kxf6h4qx21wn2ak5srcwgwx7bg6vn6cp")))

(define-public crate-marigold-macros-0.1.6 (c (n "marigold-macros") (v "0.1.6") (d (list (d (n "marigold-grammar") (r "=0.1.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1vc4mb4sc8qzzhy6l2h8ldsix2ybd3jrhq2nkykds7f1cy555jmi")))

(define-public crate-marigold-macros-0.1.7 (c (n "marigold-macros") (v "0.1.7") (d (list (d (n "marigold-grammar") (r "=0.1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0r7fd7msqkfcmmjwc9whngj6j7kksln0vrkibhw1karxjxcz4vyb")))

(define-public crate-marigold-macros-0.1.8 (c (n "marigold-macros") (v "0.1.8") (d (list (d (n "marigold-grammar") (r "=0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1h1sjzimqn9pvrqv3j9m44pp7837jbfq6l75xp9ikwqvs3xcrwa6")))

(define-public crate-marigold-macros-0.1.9 (c (n "marigold-macros") (v "0.1.9") (d (list (d (n "marigold-grammar") (r "=0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0lcmzmsc1yssh5b4j1cans76xafpp0n4cqwjlfwj66gk67rz7i18")))

(define-public crate-marigold-macros-0.1.10 (c (n "marigold-macros") (v "0.1.10") (d (list (d (n "marigold-grammar") (r "=0.1.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "11s1q5vfbbrhaqkw8zy5yahm75aywcn5b37lgzfgvfrx3aa5v80i") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.11 (c (n "marigold-macros") (v "0.1.11") (d (list (d (n "marigold-grammar") (r "=0.1.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0wk62haf43gq54pll7af2gyfdv4n8xpmljwzv75f4mp6wms2wnqn") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.12 (c (n "marigold-macros") (v "0.1.12") (d (list (d (n "marigold-grammar") (r "=0.1.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0w1p6ryj48w5rxk09ax8wiff8kckb6x6s5r1chj3kd1hysldza1v") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.13 (c (n "marigold-macros") (v "0.1.13") (d (list (d (n "marigold-grammar") (r "=0.1.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0hd6apwchd0d6v8k1xrqck2d9bkld7rjz2n75mrwi7jd0hfjzvfw") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.14 (c (n "marigold-macros") (v "0.1.14") (d (list (d (n "marigold-grammar") (r "=0.1.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0lkcq3x8qqd1fa2l23p7yymap0g6pyc3dml7v7603is6rrdzfdxs") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.15 (c (n "marigold-macros") (v "0.1.15") (d (list (d (n "marigold-grammar") (r "=0.1.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1jl6b3k0akf4f70khlz3xxylybw7xrr3pf49qksvbmibi3p8msa2") (f (quote (("io" "marigold-grammar/io"))))))

(define-public crate-marigold-macros-0.1.16 (c (n "marigold-macros") (v "0.1.16") (d (list (d (n "marigold-grammar") (r "=0.1.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0dia8zgxnp1gf1gbljr8hkrk0dylafa2r6j9r49yvclwpxgp03db") (f (quote (("io" "marigold-grammar/io"))))))

