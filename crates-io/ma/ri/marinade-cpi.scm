(define-module (crates-io ma ri marinade-cpi) #:use-module (crates-io))

(define-public crate-marinade-cpi-0.2.0 (c (n "marinade-cpi") (v "0.2.0") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "12m76r1pqn40zfdxyldlxvkzra7l8zh2yc6w6phm2gk5xjjkwf18") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marinade-cpi-0.2.1 (c (n "marinade-cpi") (v "0.2.1") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0fdbar96qr08s1r7sa02mdmyyykzglrrzb5r3qsfzbqj0gwp3bgc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marinade-cpi-0.2.2 (c (n "marinade-cpi") (v "0.2.2") (d (list (d (n "anchor-gen") (r "^0.2.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "1vpz6ki0smgw05xqhxc9bcryb20gx4dj2n0p28h08xck1v7hy2f0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marinade-cpi-0.3.0 (c (n "marinade-cpi") (v "0.3.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "045xq80gh6izg5xpmlhxizhgjjdq2f5lr3cd0qbzdrssnldxmk9g") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

