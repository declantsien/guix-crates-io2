(define-module (crates-io ma ri marinara) #:use-module (crates-io))

(define-public crate-marinara-0.1.0 (c (n "marinara") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "17y1qq96xrm5sql29j9fpd2gnj355hgy2l7i22lnd1vzhwvzg5s9")))

(define-public crate-marinara-0.1.1 (c (n "marinara") (v "0.1.1") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0zsai0qzy5xsw068c8g19dpn0swsjb55nym29ifsns420fyxdhm8")))

(define-public crate-marinara-0.2.0 (c (n "marinara") (v "0.2.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0b05i5n4mli93jp7i8z6r7b3blg1idz9h9bd2di3s46if3fzna3k")))

