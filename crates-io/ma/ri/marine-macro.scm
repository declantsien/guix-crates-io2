(define-module (crates-io ma ri marine-macro) #:use-module (crates-io))

(define-public crate-marine-macro-0.6.2 (c (n "marine-macro") (v "0.6.2") (d (list (d (n "marine-macro-impl") (r "=0.6.2") (d #t) (k 0)))) (h "1bmfzrxqqr4j3hqcjar58sx97h2f0y6is5ngrlay7bkc8p24inpy")))

(define-public crate-marine-macro-0.6.3 (c (n "marine-macro") (v "0.6.3") (d (list (d (n "marine-macro-impl") (r "=0.6.3") (d #t) (k 0)))) (h "0g9hwzy1rkwr05xfxrs57p93ws4g76b2dx7rjd9mjs08414914sv")))

(define-public crate-marine-macro-0.6.4 (c (n "marine-macro") (v "0.6.4") (d (list (d (n "marine-macro-impl") (r "=0.6.4") (d #t) (k 0)))) (h "1x3fm97pca2vii9il88v3xd00kfkkhpqlv61csddxg8vmjv545hw")))

(define-public crate-marine-macro-0.6.7-alpha.0 (c (n "marine-macro") (v "0.6.7-alpha.0") (d (list (d (n "marine-macro-impl") (r "^0.6.7-alpha.0") (d #t) (k 0)))) (h "1gy9w6fzf65fyrjc6hqqa19p3py8bwqdskk7liwi7ab2f6pawsw5")))

(define-public crate-marine-macro-0.6.5 (c (n "marine-macro") (v "0.6.5") (d (list (d (n "marine-macro-impl") (r "^0.6.5") (d #t) (k 0)))) (h "0i1595rr7llj55v63qbgp08riqxv8k10z7kfcgdzvfvwprki3g0a")))

(define-public crate-marine-macro-0.6.6 (c (n "marine-macro") (v "0.6.6") (d (list (d (n "marine-macro-impl") (r "=0.6.6") (d #t) (k 0)))) (h "0hg1fgdxmfkqfkcqk8pdqmn5py2ql7sk4jj6sbbim1qq33lxbf37")))

(define-public crate-marine-macro-0.6.8 (c (n "marine-macro") (v "0.6.8") (d (list (d (n "marine-macro-impl") (r "=0.6.8") (d #t) (k 0)))) (h "0d773mzyi4vn4kh57r0sh20z9bgzdj1n7j8pba0x8y7marcpy45r")))

(define-public crate-marine-macro-0.6.9 (c (n "marine-macro") (v "0.6.9") (d (list (d (n "marine-macro-impl") (r "=0.6.9") (d #t) (k 0)))) (h "06fl3r34vlyra3vbbm9292fqa4xkvq5z3l1vkvfw6z44a5w94ggn")))

(define-public crate-marine-macro-0.6.10 (c (n "marine-macro") (v "0.6.10") (d (list (d (n "marine-macro-impl") (r "=0.6.10") (d #t) (k 0)))) (h "0yynmhq1d4b3hkca0v5xzi66xp192h652726lh4xfbkbpkzdar6d")))

(define-public crate-marine-macro-0.6.11 (c (n "marine-macro") (v "0.6.11") (d (list (d (n "marine-macro-impl") (r "=0.6.11") (d #t) (k 0)))) (h "1q75mwhir67z1yv0apys6iv03yqpfyadrlgzlrg82s3gj00a3461")))

(define-public crate-marine-macro-0.6.12 (c (n "marine-macro") (v "0.6.12") (d (list (d (n "marine-macro-impl") (r "=0.6.12") (d #t) (k 0)))) (h "03i33m5l6mah7sriys6r9vfji2al2h239cskyn6d04313m9vflrk")))

(define-public crate-marine-macro-0.6.13 (c (n "marine-macro") (v "0.6.13") (d (list (d (n "marine-macro-impl") (r "=0.6.13") (d #t) (k 0)))) (h "0d35ibbklbza5774fdqxg1szmxl2xm8rpv9zy94113v9hzh675k7")))

(define-public crate-marine-macro-0.6.14 (c (n "marine-macro") (v "0.6.14") (d (list (d (n "marine-macro-impl") (r "=0.6.14") (d #t) (k 0)))) (h "14s9w31wsgvvr8wbyyc54dfpnrs78k03k70kpyvk1hjm8almvc4l")))

(define-public crate-marine-macro-0.6.15 (c (n "marine-macro") (v "0.6.15") (d (list (d (n "marine-macro-impl") (r "=0.6.15") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.6.15") (d #t) (k 0)))) (h "0wdv7z4vqfixp9jx0bhhxycfhydw14nlcb9aprp9dkvj6ya0gjfz")))

(define-public crate-marine-macro-0.7.0 (c (n "marine-macro") (v "0.7.0") (d (list (d (n "marine-macro-impl") (r "=0.7.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.7.0") (d #t) (k 0)))) (h "0yqvhq22fvgcz98fw4yw9nxn4j8bqd63j7nzlq8a779fc501pwn5")))

(define-public crate-marine-macro-0.7.1 (c (n "marine-macro") (v "0.7.1") (d (list (d (n "marine-macro-impl") (r "=0.7.1") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.7.1") (d #t) (k 0)))) (h "09g5ffxbrmpm2wyqv45zq38rc8ahj38q1xr9aanlm621yqidl0sy")))

(define-public crate-marine-macro-0.8.0 (c (n "marine-macro") (v "0.8.0") (d (list (d (n "marine-macro-impl") (r "=0.8.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.8.0") (d #t) (k 0)))) (h "1yny0zv37ga90brcshw1qjq5597qabxska3xfl7vdl32jsx2ha8z") (y #t)))

(define-public crate-marine-macro-0.8.1 (c (n "marine-macro") (v "0.8.1") (d (list (d (n "marine-macro-impl") (r "=0.8.1") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.8.1") (d #t) (k 0)))) (h "1si81j2hyld03ngm521zj28nn4qmh09m3k9bkc98w3b62dqgm6cw")))

(define-public crate-marine-macro-0.9.0 (c (n "marine-macro") (v "0.9.0") (d (list (d (n "marine-macro-impl") (r "=0.9.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.9.0") (d #t) (k 0)))) (h "1ns9kdg85w36sa4krfv6b8qm90bqadi8vh78j951s50cayalqpgx")))

(define-public crate-marine-macro-0.10.0 (c (n "marine-macro") (v "0.10.0") (d (list (d (n "marine-macro-impl") (r "=0.10.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.10.0") (d #t) (k 0)))) (h "10y8jlryy1jf6pwbg33fas6kd725zqlcphqhllbxqw59nsfgr200")))

(define-public crate-marine-macro-0.10.1 (c (n "marine-macro") (v "0.10.1") (d (list (d (n "marine-macro-impl") (r "=0.10.1") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.10.1") (d #t) (k 0)))) (h "0vn10nvf3zw67j4qvld688ldx3r6i4ylnsp9bixr842nj4c5k0wk")))

(define-public crate-marine-macro-0.10.2 (c (n "marine-macro") (v "0.10.2") (d (list (d (n "marine-macro-impl") (r "=0.10.2") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.10.2") (d #t) (k 0)))) (h "1a1lza9yx19fxq328syxdd5l8yp01bk25iiv0mr054qazxkfyjrz")))

(define-public crate-marine-macro-0.10.3 (c (n "marine-macro") (v "0.10.3") (d (list (d (n "marine-macro-impl") (r "=0.10.3") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.10.3") (d #t) (k 0)))) (h "04wiwxgs756hpf2k94cww9lq3xl8j8hx96caxw2a3brrh87sbnmc")))

(define-public crate-marine-macro-0.11.0 (c (n "marine-macro") (v "0.11.0") (d (list (d (n "marine-macro-impl") (r "=0.11.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.11.0") (d #t) (k 0)))) (h "1c0lvzf9ybx2z4m2525jwcmmrwql4d4bhb2g93697wr80h3sny1a")))

(define-public crate-marine-macro-0.12.0 (c (n "marine-macro") (v "0.12.0") (d (list (d (n "marine-macro-impl") (r "=0.12.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.12.0") (d #t) (k 0)))) (h "06rkahn7rbrfdzr3isadxzf816fw6662mhjzgj1pc5pb95a1lyvj")))

(define-public crate-marine-macro-0.13.0 (c (n "marine-macro") (v "0.13.0") (d (list (d (n "marine-macro-impl") (r "=0.13.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.13.0") (d #t) (k 0)))) (h "1r6x7j5564pz8r9rgc4b6gx0xky5phya7rpis0rrhmfzzqcwvhnf")))

(define-public crate-marine-macro-0.14.0 (c (n "marine-macro") (v "0.14.0") (d (list (d (n "marine-macro-impl") (r "=0.14.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "=0.14.0") (d #t) (k 0)))) (h "023am78pq4mwarq82p2h77yhs9hxl4jgzdnf7hvsk17m2r9ih0pm")))

