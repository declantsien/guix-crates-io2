(define-module (crates-io ma ri maria-linalg) #:use-module (crates-io))

(define-public crate-maria-linalg-0.1.0 (c (n "maria-linalg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1ca8pwgryxcczy1z86vy6cxih3b1iv8c2cqhxwfgzpalpn7nhfxy")))

(define-public crate-maria-linalg-0.2.0 (c (n "maria-linalg") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1z7pv0yvan3jy8mx8d9cs4m4xmjcr94c3n75b16dzbm8ry4mqlxv")))

(define-public crate-maria-linalg-0.3.0 (c (n "maria-linalg") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1hba8ygfh40kyknn95y080lgk3clsvi3lpb0xagfz245yspfn65f")))

(define-public crate-maria-linalg-0.4.0 (c (n "maria-linalg") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "17kqmpzp0wh8g6q24vp9qbh7yxbhv1myvkhr153as7il6b5zy23n")))

(define-public crate-maria-linalg-0.5.0 (c (n "maria-linalg") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1bvi89hbbjbp9ds01sqg1qzhv698a43xcvshx0h70f5lhrpb4n94")))

(define-public crate-maria-linalg-0.6.0 (c (n "maria-linalg") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1jkc2js78g6040fk8hlr6r6f25iv4iyl9c2l6ar5bmv7rlm3gkkf")))

(define-public crate-maria-linalg-0.7.0 (c (n "maria-linalg") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0brzzqlfjvfm2b9j3s5rk1cr0hbjxvpycyz40d819a6n1hggc4rj")))

(define-public crate-maria-linalg-0.7.1 (c (n "maria-linalg") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1xxirfq6zmanm9d1ahrd19sj467xzk56p1i6ddsj5h6ldd2788zs")))

(define-public crate-maria-linalg-0.8.0 (c (n "maria-linalg") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0kb524y0qcg8cpksyk3p6y011s2bzvjjxfgysq5ivabm21bya0hx")))

(define-public crate-maria-linalg-0.9.0 (c (n "maria-linalg") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "18m46fmxd2vlyczvr0cxfbafpif5gxn2flv8ir2yj6a0wvrlipg4")))

(define-public crate-maria-linalg-0.10.0 (c (n "maria-linalg") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1qxlvkbnnpvp4aql3j4bxjgwp659bchbz40rp84pklljn2m42qvl")))

(define-public crate-maria-linalg-0.11.0 (c (n "maria-linalg") (v "0.11.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "08c3hz0jvgxvihrmwrpcl4zv2ppnl1nwjlf68dlzf7annqvyck8p")))

(define-public crate-maria-linalg-0.12.0 (c (n "maria-linalg") (v "0.12.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0ccg0ivxcxbq52302njk32h6jvqddj1gxfihpy3kfcn5ykhg6n2g")))

(define-public crate-maria-linalg-1.0.0 (c (n "maria-linalg") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0f996a083d5qn1j6hbvg3wkrirwajs2fag3r0hm40plhgpk7v05x")))

