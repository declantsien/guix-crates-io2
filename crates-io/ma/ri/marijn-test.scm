(define-module (crates-io ma ri marijn-test) #:use-module (crates-io))

(define-public crate-marijn-test-0.0.0 (c (n "marijn-test") (v "0.0.0") (h "0qkwffhp2cy97n7gfdynfpd8ch15y7n9p8ysgc68mpzz18hl2km0") (y #t)))

(define-public crate-marijn-test-0.0.1 (c (n "marijn-test") (v "0.0.1") (h "18jwh9i0kbhrxzzcylwwd22cjy7658cly8j1np5x9qmb5j5wlj6j") (y #t)))

(define-public crate-marijn-test-0.0.2 (c (n "marijn-test") (v "0.0.2") (h "07k5sn8dlmxv5piihwdfd96191ryzxsagsdn5q1y9xhf8w35nxqn") (y #t)))

