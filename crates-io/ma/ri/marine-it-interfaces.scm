(define-module (crates-io ma ri marine-it-interfaces) #:use-module (crates-io))

(define-public crate-marine-it-interfaces-0.3.0 (c (n "marine-it-interfaces") (v "0.3.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.20.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1s2rsswpxd4grvfq4psjh9i270k3516x3d9mrhq8d03jkrw37icp")))

(define-public crate-marine-it-interfaces-0.4.0 (c (n "marine-it-interfaces") (v "0.4.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.20.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1qfygbv3nbhgvpwrnq1xlnk2b3m6xb23y8sj6rxanlpxa5z1737i")))

(define-public crate-marine-it-interfaces-0.4.1 (c (n "marine-it-interfaces") (v "0.4.1") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.20.2") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0vf25p3icazqjcalgxq1c0bwzq021pznciyyakkj1fkj7qa2kqj2")))

(define-public crate-marine-it-interfaces-0.5.0 (c (n "marine-it-interfaces") (v "0.5.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.21.1") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0kcrj3vhiz3bs29g363aajv0ypjq07ksx8ryz52by63qrcwrr07a")))

(define-public crate-marine-it-interfaces-0.6.0 (c (n "marine-it-interfaces") (v "0.6.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.22.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1iilmb4ckacyfzmsrx7ihysfwc37r0yynm410q39d78hhf80mnh3")))

(define-public crate-marine-it-interfaces-0.7.0 (c (n "marine-it-interfaces") (v "0.7.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.23.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1iyrwn5i4ybk4iyrxzmbjl5i418qd5rwxry398h5wbaimxq92rln")))

(define-public crate-marine-it-interfaces-0.7.1 (c (n "marine-it-interfaces") (v "0.7.1") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.23.1") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "053hhkzy76nnkj3dh5ja6l60q5bwfy5hjjxpgahg387ad086s8xk")))

(define-public crate-marine-it-interfaces-0.7.2 (c (n "marine-it-interfaces") (v "0.7.2") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.23.1") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1hjs04l7xh9dgwakahvilv4mddz1idyagm5gqgw2i3xyv1zhvnwf")))

(define-public crate-marine-it-interfaces-0.7.3 (c (n "marine-it-interfaces") (v "0.7.3") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.24.1") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "094r1xgfpazc3p6a3r36w9vnyfmf580z2zza0dv9si0hpzjkfw75")))

(define-public crate-marine-it-interfaces-0.8.0 (c (n "marine-it-interfaces") (v "0.8.0") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.26.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0wz2nyr87vn7sl9466qsssj25bc0vv6wkzy0qvcpaa7zsv1zdz82")))

(define-public crate-marine-it-interfaces-0.8.1 (c (n "marine-it-interfaces") (v "0.8.1") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.26.1") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "14mxkdjdksh5fmy412z3crchps8xcah8lzsw6z8ghf3gsahc3gfk")))

(define-public crate-marine-it-interfaces-0.9.0 (c (n "marine-it-interfaces") (v "0.9.0") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.27.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1ka2q0ss5sa5jfdj38zkbvqm7zjn3mddbzlx1mah4j39gprhabwl")))

(define-public crate-marine-it-interfaces-0.9.1 (c (n "marine-it-interfaces") (v "0.9.1") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.27.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1gngkdfmnrnda6013gxhdap1n9npzb4k5g5sn8bir49il280sq8w")))

(define-public crate-marine-it-interfaces-0.10.0 (c (n "marine-it-interfaces") (v "0.10.0") (d (list (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "wasmer-it") (r "^0.28.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0gxhpzxpa43alcqjbfj11v4d1slr3gxnms8iv7kcwkyswkk0z5rl")))

