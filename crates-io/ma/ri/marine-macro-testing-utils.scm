(define-module (crates-io ma ri marine-macro-testing-utils) #:use-module (crates-io))

(define-public crate-marine-macro-testing-utils-0.1.0 (c (n "marine-macro-testing-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9d5zq7w602032vn6bqm6cb52s7q8zyk6j3gsmlv05l9m9phyqw")))

(define-public crate-marine-macro-testing-utils-0.8.0 (c (n "marine-macro-testing-utils") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "18f5rskhpqxx9xr8hlq1kgf4wjx45ygry0hda852gv9cx3h3hya0")))

(define-public crate-marine-macro-testing-utils-0.8.1 (c (n "marine-macro-testing-utils") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0myzng32gah8s4vhqvv5785dac7vq1ag7dqjjg5j041k96l4chps")))

(define-public crate-marine-macro-testing-utils-0.9.0 (c (n "marine-macro-testing-utils") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1bpyc2k1ma52rh2j2k4186c6czsb41z8z7fvx2bvdnkn7hhi81pz")))

(define-public crate-marine-macro-testing-utils-0.10.0 (c (n "marine-macro-testing-utils") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1yirjdbgd9zgijn06b12kcpkfj7jfi4f4h0yy7233zwkln728303")))

(define-public crate-marine-macro-testing-utils-0.10.1 (c (n "marine-macro-testing-utils") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0slipxnwp93825jaf1f8bivif8gr0s20i1g449d82rf8sjjf3xq3")))

(define-public crate-marine-macro-testing-utils-0.10.2 (c (n "marine-macro-testing-utils") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0v2kk89jdlj1rgckf7js85gb8fngslhb2xnqa36g3bml7q5sni2d")))

(define-public crate-marine-macro-testing-utils-0.10.3 (c (n "marine-macro-testing-utils") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0by3fhgzm8a1lw5nchvb1f4ahxzl5x643gxlphq02bz0da7qw97s")))

(define-public crate-marine-macro-testing-utils-0.11.0 (c (n "marine-macro-testing-utils") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0dp823rnxjklnyrwi0asnj4yyc45wl5pgziq04djsvdk9siw16x9")))

(define-public crate-marine-macro-testing-utils-0.12.0 (c (n "marine-macro-testing-utils") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "09h62slwv91raa0zpa8ffqiig6166hfzl8bfvxnj4kb7x2x7cji1")))

(define-public crate-marine-macro-testing-utils-0.13.0 (c (n "marine-macro-testing-utils") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhh02cj57m47y1r6hdnwn644kvfyfkk4iflvg289ikhj9rnjs9q")))

(define-public crate-marine-macro-testing-utils-0.14.0 (c (n "marine-macro-testing-utils") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "10grjwcm6vvmdhirysax6wxynaci3s30hk8j997jmc6a7cnfg9pa")))

