(define-module (crates-io ma ri marine-utils) #:use-module (crates-io))

(define-public crate-marine-utils-0.2.0 (c (n "marine-utils") (v "0.2.0") (h "04pq146yfxfr0rdsw8c017khmbjgh78aywnn0bcf8k58rf587icd")))

(define-public crate-marine-utils-0.3.0 (c (n "marine-utils") (v "0.3.0") (h "1k7xyd2g5ffzl92gr1cshdawxkga4ccg5lflv7hlvqw6j1a7y8ja")))

(define-public crate-marine-utils-0.4.0 (c (n "marine-utils") (v "0.4.0") (h "1jimhddan4iyag8ny09zfkbi4h4rhz6vkprl5iqmm4pklwipmzqw")))

(define-public crate-marine-utils-0.5.0 (c (n "marine-utils") (v "0.5.0") (h "104bykhqw3yjhadd1p8yg36faf3x524d6lla327dqbqfzxbwp1ky")))

(define-public crate-marine-utils-0.5.1 (c (n "marine-utils") (v "0.5.1") (h "1r4qz6izisz4blb6rsh01sqh7ak14a6jrhw1zwk57byhpzx6prgw")))

