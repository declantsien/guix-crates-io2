(define-module (crates-io ma dy mady_macro) #:use-module (crates-io))

(define-public crate-mady_macro-0.1.0-beta (c (n "mady_macro") (v "0.1.0-beta") (d (list (d (n "mady_macro_core") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "fold" "parsing"))) (d #t) (k 0)))) (h "013kxlyrg2dzwqzjd2ndrca2j70fab1h7kw5x8r3pb94mg9nklg1")))

