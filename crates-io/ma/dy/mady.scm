(define-module (crates-io ma dy mady) #:use-module (crates-io))

(define-public crate-mady-0.1.0-beta (c (n "mady") (v "0.1.0-beta") (d (list (d (n "mady_macro") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "mady_math") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19a9alyl978avcfmshffllxyl780hi2pqqmmap9959l50nsx3vyc") (f (quote (("math") ("macro") ("default" "macro" "math"))))))

