(define-module (crates-io ma dy mady_macro_core) #:use-module (crates-io))

(define-public crate-mady_macro_core-0.1.0-alpha (c (n "mady_macro_core") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "fold" "parsing"))) (d #t) (k 0)) (d (n "tabbycat") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kr1mnf6qb7kzzsfhldvjszgzz2ysvh6pi6cyis7j77f4akyv60y")))

