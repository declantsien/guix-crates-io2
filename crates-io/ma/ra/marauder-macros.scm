(define-module (crates-io ma ra marauder-macros) #:use-module (crates-io))

(define-public crate-marauder-macros-0.1.0 (c (n "marauder-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "0szjpdxirdwzpns12hssv2a83ivkc1i249ki5x3h1fw6yc4psinf")))

