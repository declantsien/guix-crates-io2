(define-module (crates-io ma rk mark_tuple_traits) #:use-module (crates-io))

(define-public crate-mark_tuple_traits-0.1.0 (c (n "mark_tuple_traits") (v "0.1.0") (h "1yv0drkqihmm2z623d02znvc9cldlvv4hzvbm9lzqr2aj6s1zrx0")))

(define-public crate-mark_tuple_traits-0.1.1 (c (n "mark_tuple_traits") (v "0.1.1") (h "095prd89df3zgjbigzdz26d7sn06xdxx05py1ghzyc6vqqqypnk4")))

