(define-module (crates-io ma rk markdown-renderer) #:use-module (crates-io))

(define-public crate-markdown-renderer-0.0.0 (c (n "markdown-renderer") (v "0.0.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "leptos") (r "^0.2.4") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.2") (f (quote ("cli" "shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.2") (d #t) (k 1)))) (h "0znxz5cj4i8lvz93bl135cp5h1sqcwi30xwba9nbaxqqjppjss52") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

