(define-module (crates-io ma rk market-finance) #:use-module (crates-io))

(define-public crate-market-finance-0.1.0 (c (n "market-finance") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0wdd3bwfl2zkrvlpp7a4vms1na2mp5f5m4hfgbvgxiv4p51h36cd") (f (quote (("formatting" "chrono") ("default" "formatting"))))))

(define-public crate-market-finance-0.2.0 (c (n "market-finance") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "08wpp8iw9h0bafbq1hyh4lp9yjlis2jhdk0wc2hhjnwmlqc60fal") (f (quote (("indicators") ("formatting" "chrono") ("default" "formatting"))))))

(define-public crate-market-finance-0.3.0 (c (n "market-finance") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mr5vdrfk5bhzmh226xhxmpf0pzzq49dvpjqjj9rnn6r791wjbv9") (f (quote (("indicators") ("formatting" "chrono") ("default" "formatting"))))))

