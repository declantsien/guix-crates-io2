(define-module (crates-io ma rk markov_rs) #:use-module (crates-io))

(define-public crate-markov_rs-0.1.0 (c (n "markov_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "weighted_rand") (r "^0.3") (d #t) (k 0)))) (h "0a4xxp39r3wxhsji7n0v8wi99kwr34j2yrnbl77bwvphx7hhbbcn")))

(define-public crate-markov_rs-0.1.1 (c (n "markov_rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "weighted_rand") (r "^0.3") (d #t) (k 0)))) (h "0jmps4q1175j51zzrbj1djhrbrvrsd1icgmnlg0v4qwjmkfnfkk1")))

(define-public crate-markov_rs-0.1.2 (c (n "markov_rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "weighted_rand") (r "^0.3") (d #t) (k 0)))) (h "10v9hnycv774k6yjnblj78v3w2cjp4hm7c2xa0dma1g91x8p0sx2")))

