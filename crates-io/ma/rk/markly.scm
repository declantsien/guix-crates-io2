(define-module (crates-io ma rk markly) #:use-module (crates-io))

(define-public crate-markly-0.3.0 (c (n "markly") (v "0.3.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pipeline") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0nh5qr10r7k2qp0jpsnzaz5hwrjgj60gfarp40944y169y7qyz7b") (f (quote (("unstable") ("default"))))))

