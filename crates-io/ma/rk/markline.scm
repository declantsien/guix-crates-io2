(define-module (crates-io ma rk markline) #:use-module (crates-io))

(define-public crate-markline-1.1.0 (c (n "markline") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("deprecated" "derive" "cargo" "env" "unicode" "wrap_help" "string"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09z247jdmhg0an9bih0nz2fq0whkxsi4hqfddfk20pq18fdfb40v") (y #t)))

(define-public crate-markline-1.1.1 (c (n "markline") (v "1.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("deprecated" "derive" "cargo" "env" "unicode" "wrap_help" "string"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1bnqh3w72clrs5jwhgmjbrj2rfqsh2fv6g4v85h6vv9jls8109b0")))

