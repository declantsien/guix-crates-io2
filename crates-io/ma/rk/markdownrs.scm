(define-module (crates-io ma rk markdownrs) #:use-module (crates-io))

(define-public crate-markdownrs-0.1.0 (c (n "markdownrs") (v "0.1.0") (h "0a65z85rbfy91vd3h5pjr9j9hzgiy5vk0abf3r3f3k7g7rdai6xq")))

(define-public crate-markdownrs-0.1.1 (c (n "markdownrs") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1bymyhqzcwvldivj825jdc09zqk83y221ni2bv9q37lvr11hphav")))

(define-public crate-markdownrs-0.1.1-beta (c (n "markdownrs") (v "0.1.1-beta") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0qv4if456ijy0m09v9k4fssinm54wi7gk5w8fbc6yv88iry9jrmw")))

