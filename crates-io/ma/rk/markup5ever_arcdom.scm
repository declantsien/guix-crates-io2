(define-module (crates-io ma rk markup5ever_arcdom) #:use-module (crates-io))

(define-public crate-markup5ever_arcdom-0.1.0 (c (n "markup5ever_arcdom") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.16") (d #t) (k 0)))) (h "1a5hn26bgzz84y60awgnvpw8cd0s31jfaz31bb0ij27qpa9cqns3")))

(define-public crate-markup5ever_arcdom-0.1.1 (c (n "markup5ever_arcdom") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.16") (d #t) (k 0)))) (h "16miq3asrq8jgh541qqawd399nbbnndizlrr417rzw1p0rn05qla")))

(define-public crate-markup5ever_arcdom-0.1.2 (c (n "markup5ever_arcdom") (v "0.1.2") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.17") (d #t) (k 0)))) (h "034gbvsfriffh7ahsxfavlq40zzap7k4h08ja81nwnkclff3ljp6")))

