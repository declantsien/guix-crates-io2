(define-module (crates-io ma rk marktask) #:use-module (crates-io))

(define-public crate-marktask-0.1.0 (c (n "marktask") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q3waxp7ydid2jskw8jam8mvz0dv232r6ysbg65qywr30cysgf3i")))

(define-public crate-marktask-0.2.0 (c (n "marktask") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f5cb846zwl3ylv2cri7jqwkl45jbn05cfippq1xmrjdyhax3fj3")))

