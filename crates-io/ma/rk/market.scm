(define-module (crates-io ma rk market) #:use-module (crates-io))

(define-public crate-market-0.1.0 (c (n "market") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)))) (h "09qi852j58yvz5jvpk5mylpja79spnam425yf7njsrynh182gw3d")))

(define-public crate-market-0.2.0 (c (n "market") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)))) (h "1d8yjfw1ajckwyml70bhfzi547z19dxdldfv9lknjcl7r8v8gac4")))

(define-public crate-market-0.3.0 (c (n "market") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)))) (h "0hv5jb5bh7f7nxqi4psd984gm428cxfkcfx0j7pky184m37pji40")))

(define-public crate-market-0.4.0 (c (n "market") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1355n1ciyd301qbxny52b5ja1ss4xqmwjn08qqh52x7b3420ml1k")))

(define-public crate-market-0.5.0 (c (n "market") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "18dfx14g6g4np21l9w2qb4g3mp4y90hq7c5b9b17znkrgnrky9y4")))

(define-public crate-market-0.6.0 (c (n "market") (v "0.6.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "17imq46a88xv9q34lgccbs7723ym9ljcc56r9dnq2cclz53f0mzv")))

(define-public crate-market-0.7.0 (c (n "market") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "062hmvvqg8fqidkwvylb6v2q4v649gzdg543c400m11l1dyy6hsd")))

(define-public crate-market-0.8.0 (c (n "market") (v "0.8.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0xf2xrnhhxlfr7nxrk6qb2laf3zk0jmznvdnvjlf50fpi60wnn84")))

(define-public crate-market-0.9.0 (c (n "market") (v "0.9.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0a1xxc48r3ca6wg5pww18bjv57z1fp4wris99j4jx37ih68vzajs")))

(define-public crate-market-0.10.0 (c (n "market") (v "0.10.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1772rqysz32043g04339s9v05ssm101m279gpl2r7axxi9damnsp")))

(define-public crate-market-0.11.0 (c (n "market") (v "0.11.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "07zir54bkby0r2vh7yvsas4k95ln691ml9w8ljy041jgjy23wvb4")))

(define-public crate-market-0.12.0 (c (n "market") (v "0.12.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0qj87y1xkbq3if32p0xc7sjsb5jbn07p5vfmqrg4cick6lqqxfmq")))

(define-public crate-market-0.13.0 (c (n "market") (v "0.13.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "= 0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1qh3armcd9a9qslzyn9pfjy2qfis98drf7f3a2xqan3awzynxjwr")))

(define-public crate-market-0.14.0 (c (n "market") (v "0.14.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1hrvkq79nzhkbfqf61i0pj4q3dmw3shyh1fnwgbrjjsbv744yldi")))

(define-public crate-market-0.15.0 (c (n "market") (v "0.15.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r0yf625l3ixjyrhq8jr3r8by3j98g4r9599nfxl6ncpzbn5jydv")))

(define-public crate-market-0.16.0 (c (n "market") (v "0.16.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02rlv8vwsybicqbhkgkqazdkh3dh6ivbdnnip3lv7h6f251rlra3")))

(define-public crate-market-0.17.0 (c (n "market") (v "0.17.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06hcdi3zybk6c2v2i1zzx6py10wiq6higx3k11dba5fi469768kx")))

(define-public crate-market-0.18.0 (c (n "market") (v "0.18.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z6c6qjm0a4v1pclk0m8jl170qrbmw12bzzlad7s98zqxg4i9ll7")))

(define-public crate-market-0.19.0 (c (n "market") (v "0.19.0") (d (list (d (n "conventus") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p9qxvjbak6qdzv3ykg4q7436c4a0jv5fr3wnrpcxm472pf15ckk")))

(define-public crate-market-0.20.0 (c (n "market") (v "0.20.0") (d (list (d (n "conventus") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "=0.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "never") (r "^0.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cgm28bixqf745fr4anvfxgmn43y2n3rvydwpv2v33lbs8myd2gf")))

(define-public crate-market-0.21.0 (c (n "market") (v "0.21.0") (d (list (d (n "conventus") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "12xpg08yv15kcpkqmiymk2m6y1axdwrf5amhb8pvahn71539s9x3")))

(define-public crate-market-0.22.0 (c (n "market") (v "0.22.0") (d (list (d (n "conventus") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1n8c2y7i5ym0d19bvkak741bvrhj96i1z5saa0n3h4w8bz9srss8")))

(define-public crate-market-0.23.0 (c (n "market") (v "0.23.0") (d (list (d (n "conventus") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1mc7rwr7ipklasvnylzbwfi1d9nl2wz38zk88sw7a92fzpf86vd8")))

(define-public crate-market-0.24.0 (c (n "market") (v "0.24.0") (d (list (d (n "conventus") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "0i09jna8k8h9fnx9wpadn7d1lgbxkrfs2c84blfz9fkbmfbcpgys")))

(define-public crate-market-0.25.0 (c (n "market") (v "0.25.0") (d (list (d (n "conventus") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "0qmg6frb4a5gpyabi4a25y19irkxv5lgzdfz4wdvka2ljzz3wi7q")))

(define-public crate-market-0.26.0 (c (n "market") (v "0.26.0") (d (list (d (n "conventus") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "0jf5ivhfw3555i7vdp3xx7a71p6ma7zmgvj8568ni0is92mnj6lk")))

(define-public crate-market-0.27.0 (c (n "market") (v "0.27.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "0pdr20jjbjcja9i2igzcwa7i3zyw60wpkfswkdwvp29ckvplz2k0")))

(define-public crate-market-0.27.1 (c (n "market") (v "0.27.1") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "market_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "0yzb86shk06dnf4hdrccp724mvgqcx2jh4ba3hcgf0yp8zjprl11")))

(define-public crate-market-0.29.0 (c (n "market") (v "0.29.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1iw177kkbkhl7180km55z0vdlv6h6zmfs21jd7dn6qxb50yhxp76") (f (quote (("unstable-doc-cfg") ("std" "never/std"))))))

(define-public crate-market-0.30.0 (c (n "market") (v "0.30.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (k 0)))) (h "12ap5hjyrb72xy858k6jvif19bc6wxhb4pvffc1xmb17kccdf45l") (f (quote (("unstable-doc-cfg") ("std" "never/std"))))))

(define-public crate-market-0.30.1 (c (n "market") (v "0.30.1") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "never") (r "^0.1.0") (k 0)))) (h "1kmhk2p14pyq9z822g3l94yy19lgixs4sqbhga35ih1dbl58cfd0") (f (quote (("unstable-doc-cfg") ("std" "never/std"))))))

