(define-module (crates-io ma rk markdown-edit) #:use-module (crates-io))

(define-public crate-markdown-edit-0.1.0 (c (n "markdown-edit") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.1.0") (d #t) (k 0)))) (h "1z34fv9a6g19lf3bq3gm4inkh53blyy8p15lmb8g7pbv9pcdmflz")))

