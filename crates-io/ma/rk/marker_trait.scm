(define-module (crates-io ma rk marker_trait) #:use-module (crates-io))

(define-public crate-marker_trait-1.0.0 (c (n "marker_trait") (v "1.0.0") (d (list (d (n "macroific") (r "^1.1.1") (f (quote ("attr_parse"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq06j39ab48zmqb39n86h6f0dgagg8vb5y4pzyyk5b09a1dxq01") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

