(define-module (crates-io ma rk markdown-renderer-ui) #:use-module (crates-io))

(define-public crate-markdown-renderer-ui-0.0.0 (c (n "markdown-renderer-ui") (v "0.0.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "leptos") (r "^0.2") (f (quote ("stable"))) (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0hkvaan1r1yflzjgflmhydm5dgqwba83z4asv374m4widg6jc24r")))

