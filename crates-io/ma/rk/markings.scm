(define-module (crates-io ma rk markings) #:use-module (crates-io))

(define-public crate-markings-0.1.0 (c (n "markings") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1n8v8q9r10k19hx776gmk0srwl29zb6csph0awp0vn3g9v14y8pn")))

(define-public crate-markings-0.1.1 (c (n "markings") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0qpj0ajlckak238m2dr09w42kw1ja69hh2ywmd85s32ngicpy183")))

(define-public crate-markings-0.1.2 (c (n "markings") (v "0.1.2") (h "0czmx30r2yrsyb39k4prbprmpvcdbv2rxl44fnxz7426slxcn2ji")))

(define-public crate-markings-0.2.0 (c (n "markings") (v "0.2.0") (h "0ii5xbl8svqkhssgbbdp5db3l15b9ga6knpprkd3pscka10k6dvk")))

(define-public crate-markings-0.3.0 (c (n "markings") (v "0.3.0") (h "1xpsxsvhkqba0dwm0286l6l77x5vcmmaa8353fagcy8r50xm2lmq")))

(define-public crate-markings-0.3.1 (c (n "markings") (v "0.3.1") (h "038qb90qn34mqndhbfz8yigmy4ad91hpsh4jzz4fr0lqcq4hzij5")))

(define-public crate-markings-0.3.2 (c (n "markings") (v "0.3.2") (h "06cw99nbvq2q3wiirrigvjj5a4zpkqm52yh8vmpl696s7y3c0sw8")))

(define-public crate-markings-0.3.3 (c (n "markings") (v "0.3.3") (h "0i35b2n7m3lrw8gl0b60sydwjlffa97y9ivfz6cmprys3q9kwws3")))

(define-public crate-markings-0.3.4 (c (n "markings") (v "0.3.4") (h "027wlgs9r3lvlsv34pyg5xb5mpi3bsw7p31l1rcsr2k2sw99yb88")))

(define-public crate-markings-0.4.0 (c (n "markings") (v "0.4.0") (h "011sf4w1ap5z0phgbgqlf36f0isxvv1gzmyvh1f4xhbxzwx1schy")))

