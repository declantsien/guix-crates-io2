(define-module (crates-io ma rk mark_last) #:use-module (crates-io))

(define-public crate-mark_last-0.9.0 (c (n "mark_last") (v "0.9.0") (h "1803dhvhfp0d0vwirwj99aim7lw8vyafnfpwqy6wkngjlxc5lmql")))

(define-public crate-mark_last-0.9.1 (c (n "mark_last") (v "0.9.1") (h "0j7pwpvisc8072afhkaynwyxhffzw46xzcx03znl3rr2mrnxvrk4")))

(define-public crate-mark_last-0.9.2 (c (n "mark_last") (v "0.9.2") (h "1y8jdw0jr3ngbv9cixqd2qprfhfkjd5xmhgxvassza1bnhfnl3ag")))

