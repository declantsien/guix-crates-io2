(define-module (crates-io ma rk markdust) #:use-module (crates-io))

(define-public crate-markdust-0.1.0 (c (n "markdust") (v "0.1.0") (d (list (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "14a0fxlg3qf33c22rz1jv088wan85q9dl0y72fmmxz0xpdiwdp02")))

(define-public crate-markdust-0.1.1 (c (n "markdust") (v "0.1.1") (d (list (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0a6mg3m3d9xmn65axhwqbh4mbiigaj99d7dbwfpmi3rcgn95awyg")))

