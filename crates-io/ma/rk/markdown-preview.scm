(define-module (crates-io ma rk markdown-preview) #:use-module (crates-io))

(define-public crate-markdown-preview-0.1.0 (c (n "markdown-preview") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "comrak") (r "^0.15") (d #t) (k 0)))) (h "1gwkkk2ck73pw8anx3l6ycc9h5f143mxsmwa1gjn77bzk5rqkqi6")))

(define-public crate-markdown-preview-0.2.0 (c (n "markdown-preview") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "comrak") (r "^0.15") (d #t) (k 0)))) (h "040aiqanz6icm38m4hx7p3yc7nq74iq5h4wxm6j5nflmblvd4kan")))

