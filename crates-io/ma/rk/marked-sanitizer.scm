(define-module (crates-io ma rk marked-sanitizer) #:use-module (crates-io))

(define-public crate-marked-sanitizer-0.0.0 (c (n "marked-sanitizer") (v "0.0.0") (d (list (d (n "ammonia") (r ">= 3.1.0, < 3.2") (d #t) (k 0)) (d (n "marked") (r ">= 0.1.0, < 0.2") (d #t) (k 0)))) (h "0mpdmlscpda8yzqa5bz6kqdz56vacjswawa14fpr2fjnnfk3x8vq")))

