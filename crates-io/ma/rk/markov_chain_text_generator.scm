(define-module (crates-io ma rk markov_chain_text_generator) #:use-module (crates-io))

(define-public crate-markov_chain_text_generator-0.1.0 (c (n "markov_chain_text_generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1dggrgwa3s60l89byv358l7hpqgs8x70s1p4mwp88wijgr5m25xl") (y #t)))

(define-public crate-markov_chain_text_generator-0.2.0 (c (n "markov_chain_text_generator") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18rjp6v05zvhv2957x99r3hm0snmih96cxay8l0ypm6cikk7qrp0") (y #t)))

