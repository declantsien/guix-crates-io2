(define-module (crates-io ma rk markdown-it-autolink) #:use-module (crates-io))

(define-public crate-markdown-it-autolink-0.1.0 (c (n "markdown-it-autolink") (v "0.1.0") (d (list (d (n "gfm-autolinks") (r "^0.2") (d #t) (k 0)) (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "0lmwq3kkj3pm01hp1c6h6mzz9wf8a058rjzjc6c19kd1y9jkx8kf")))

(define-public crate-markdown-it-autolink-0.2.0 (c (n "markdown-it-autolink") (v "0.2.0") (d (list (d (n "gfm-autolinks") (r "^0.2") (d #t) (k 0)) (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "032a4isvm7gfid4kzhhpwmr0gvmypmc0g58axgcsmkfy4cn88k0p")))

