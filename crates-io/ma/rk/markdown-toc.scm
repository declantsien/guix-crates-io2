(define-module (crates-io ma rk markdown-toc) #:use-module (crates-io))

(define-public crate-markdown-toc-0.1.0 (c (n "markdown-toc") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "slugify") (r "^0.1") (d #t) (k 0)))) (h "0lk8bsncq1zj50p36p2gravaxddkvdrlccjd1dyahbl8jqc9pg0i")))

(define-public crate-markdown-toc-0.1.1 (c (n "markdown-toc") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0w6grnnhgwlxaq3ibdhk8jxfc1aydzsr5kdf7x7b58x8q737nd2q")))

(define-public crate-markdown-toc-0.2.0 (c (n "markdown-toc") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "07y8s71fxlpykllh72z9rfds69gjkrah4alvf354cay6x59z1spl")))

