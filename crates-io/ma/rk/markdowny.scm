(define-module (crates-io ma rk markdowny) #:use-module (crates-io))

(define-public crate-markdowny-0.1.0 (c (n "markdowny") (v "0.1.0") (d (list (d (n "expry") (r "^0.1.0") (d #t) (k 0)))) (h "12rladg6mrw9568qs9bg0sfibsg6dp2s9sjcn4dgymhggbw6mw78")))

(define-public crate-markdowny-0.1.1 (c (n "markdowny") (v "0.1.1") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)))) (h "01bf2yvhgi7aqxms5j719p9g5ixiqmgx9ns31kxy1zg6jvkqwpz8")))

(define-public crate-markdowny-0.1.2 (c (n "markdowny") (v "0.1.2") (d (list (d (n "expry") (r "^0.1") (d #t) (k 0)))) (h "0plznjcwivcr0p3hw4zf3p6ablrx49y0sia2y1whg4j0yfb5vdvm")))

(define-public crate-markdowny-0.2.0 (c (n "markdowny") (v "0.2.0") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)))) (h "18aa1lds1hn3jxfkc4lvgn61llfgx8kgdk65jmppang6wjhbvwzj") (y #t)))

(define-public crate-markdowny-0.2.1 (c (n "markdowny") (v "0.2.1") (d (list (d (n "expry") (r "^0.2") (d #t) (k 0)))) (h "00jbi38qbyj32kdlpxqdz598ddafbky515iicbmrxaap5v6z3gq6")))

(define-public crate-markdowny-0.3.0 (c (n "markdowny") (v "0.3.0") (d (list (d (n "expry") (r "^0.3") (d #t) (k 0)) (d (n "latex2mathml") (r "0.2.*") (o #t) (d #t) (k 0)) (d (n "syntect") (r "^5.0") (o #t) (d #t) (k 0)))) (h "1cy4mn6hd8w22vr0ijv5lj83ha3asxp3j7374snla8qkn0rv5g6v") (f (quote (("default" "latex2mathml" "syntect"))))))

(define-public crate-markdowny-0.4.0 (c (n "markdowny") (v "0.4.0") (d (list (d (n "expry") (r "^0.4") (d #t) (k 0)) (d (n "latex2mathml") (r "0.2.*") (o #t) (d #t) (k 0)) (d (n "syntect") (r "^5.0") (o #t) (d #t) (k 0)))) (h "1jiq3cdgrhzhg3irank8vavm9fkrqz9p4z62qh1sw1g4rsafsmvp") (f (quote (("default" "latex2mathml" "syntect"))))))

