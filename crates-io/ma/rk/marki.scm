(define-module (crates-io ma rk marki) #:use-module (crates-io))

(define-public crate-marki-0.1.0 (c (n "marki") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genanki-rs") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (f (quote ("simd"))) (k 0)))) (h "1v2y95b344v1r8hrza85fh0lhalk8i8v9gd10hp6d80qqiv0n6bn")))

