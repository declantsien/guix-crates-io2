(define-module (crates-io ma rk marksman_escape) #:use-module (crates-io))

(define-public crate-marksman_escape-0.0.1 (c (n "marksman_escape") (v "0.0.1") (h "0bcc99f83v3hlkjg0xbnndgd3blcfgb9z5lmvhann395j509iwr5")))

(define-public crate-marksman_escape-0.1.0 (c (n "marksman_escape") (v "0.1.0") (h "1025918a3y1bdh0b6crkpvknynmxxhx05wq384qf8jam2s17b53j")))

(define-public crate-marksman_escape-0.1.1 (c (n "marksman_escape") (v "0.1.1") (h "02xdnaq9kxsscgv71h44dpqq550wxq8fv822vhxh716175k8rr6n")))

(define-public crate-marksman_escape-0.1.2 (c (n "marksman_escape") (v "0.1.2") (h "1dyfzyddjzxj410j73flwrgxklal18lskmc2vzz2avy94mc95ag1")))

