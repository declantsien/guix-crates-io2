(define-module (crates-io ma rk markdown_to_html_parser) #:use-module (crates-io))

(define-public crate-markdown_to_html_parser-0.1.0 (c (n "markdown_to_html_parser") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xjx62av60hwhcsn3n17aj4lllj9h1xrxcy5myfzhnzhri9n7g8y")))

