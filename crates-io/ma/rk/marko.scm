(define-module (crates-io ma rk marko) #:use-module (crates-io))

(define-public crate-marko-0.1.0 (c (n "marko") (v "0.1.0") (h "0ba8yca9x1a7lhz5rl2q8y2z08jrw37xax38sfwdq0d8c79amda8")))

(define-public crate-marko-0.2.0 (c (n "marko") (v "0.2.0") (h "0b34rs1wkdjcydqi9702g8nvwdfcgnvydibnh0lxv72g4f59hk17")))

(define-public crate-marko-0.3.0 (c (n "marko") (v "0.3.0") (h "08xd7lh920idq33s4ca9wyr57vs6zxg4xzs9k9r6rrqdb5wj4z85")))

