(define-module (crates-io ma rk markifier) #:use-module (crates-io))

(define-public crate-markifier-0.1.0 (c (n "markifier") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "0vbja1g1ca0dzk8nhbx9zswvm44qzaxk2b6s0589pq3d1n6wikai")))

