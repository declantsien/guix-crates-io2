(define-module (crates-io ma rk markdown-to-html) #:use-module (crates-io))

(define-public crate-markdown-to-html-0.1.0 (c (n "markdown-to-html") (v "0.1.0") (d (list (d (n "nom") (r "^6.0.0-alpha") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.62") (d #t) (k 0)))) (h "0n1mzq0rzi98vmyy85g8378qib92pgm989xwly21yn6snr2sn9fg")))

(define-public crate-markdown-to-html-0.1.1 (c (n "markdown-to-html") (v "0.1.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0gqrk9al3ld2mlpa3flbbixsjikv88h2bhyyzhdkafinvcv376rd")))

(define-public crate-markdown-to-html-0.1.2 (c (n "markdown-to-html") (v "0.1.2") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "087y75238x77sq47q5qxr2s3slfylyy7vpx0yhfw9d4cb4j0s6yr")))

(define-public crate-markdown-to-html-0.1.3 (c (n "markdown-to-html") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "18as4d3axkr40nidjgsjwdadvdg3zi67viyyanvq68inlh1c3phc")))

