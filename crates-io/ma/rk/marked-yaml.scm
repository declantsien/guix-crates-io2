(define-module (crates-io ma rk marked-yaml) #:use-module (crates-io))

(define-public crate-marked-yaml-0.1.0 (c (n "marked-yaml") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "= 0.4.3") (d #t) (k 0)))) (h "0v30a8m05q1fn7w8xvxjvylshbpz99392990qwivx7p697zmvf9m")))

(define-public crate-marked-yaml-0.2.0 (c (n "marked-yaml") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "=0.5.3") (d #t) (k 0)) (d (n "yaml-rust") (r "=0.4.5") (d #t) (k 0)))) (h "1zcsa3hzlng03bgw9ivj5j3qf1mvy6lbmgsh1nchaa9mn58x0hk5")))

(define-public crate-marked-yaml-0.2.2 (c (n "marked-yaml") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1x9n5b7k20w8kbnrkbhqgy5r8r3a8sv24aaw8nfnnym21w5lvz4h")))

(define-public crate-marked-yaml-0.3.0 (c (n "marked-yaml") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0xpgl0vnyn07gxdj3i38bzqjgii2667xvhxbbxi0a9zq8y84d3bl")))

(define-public crate-marked-yaml-0.4.0 (c (n "marked-yaml") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.8") (d #t) (k 0) (p "yaml-rust2")))) (h "00168ww4adyw85liw7361vf9ssambyzsy0yim0calphass5bqwpf") (f (quote (("default")))) (s 2) (e (quote (("serde-path" "serde" "dep:serde_path_to_error") ("serde" "dep:serde"))))))

(define-public crate-marked-yaml-0.5.0 (c (n "marked-yaml") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "hashlink") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.8") (d #t) (k 0) (p "yaml-rust2")))) (h "0jfli4k9pk3fq6glzvimq607cn1w5r4xxw1jc5izfwcpzilr6njb") (f (quote (("default")))) (s 2) (e (quote (("serde-path" "serde" "dep:serde_path_to_error") ("serde" "dep:serde"))))))

(define-public crate-marked-yaml-0.5.1 (c (n "marked-yaml") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "hashlink") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.8") (d #t) (k 0) (p "yaml-rust2")))) (h "1fkpz0h3wr6qqgaqc8n8kmy3mwxjydrf419chlv06swjvv6w4x9r") (f (quote (("default")))) (s 2) (e (quote (("serde-path" "serde" "dep:serde_path_to_error") ("serde" "dep:serde"))))))

(define-public crate-marked-yaml-0.5.2 (c (n "marked-yaml") (v "0.5.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "hashlink") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.8") (d #t) (k 0) (p "yaml-rust2")))) (h "08rx8qh2vyh6r0n12aqsf9kq341q96g3b505fyygvxy1hj205bpb") (f (quote (("default")))) (s 2) (e (quote (("serde-path" "serde" "dep:serde_path_to_error") ("serde" "dep:serde"))))))

