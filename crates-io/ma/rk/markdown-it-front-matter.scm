(define-module (crates-io ma rk markdown-it-front-matter) #:use-module (crates-io))

(define-public crate-markdown-it-front-matter-0.1.0 (c (n "markdown-it-front-matter") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "markdown-it") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1hqz48kd7c4j3lqgi28xihdc53170sx6pfl1d9r6lwnkbqsan734")))

(define-public crate-markdown-it-front-matter-0.1.1 (c (n "markdown-it-front-matter") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1mjjva4xd8jr6d36njyr5312r6xpirrgd8ivx6kqqhp41xm48ilx")))

(define-public crate-markdown-it-front-matter-0.2.0 (c (n "markdown-it-front-matter") (v "0.2.0") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "testing") (r "^0.33.13") (d #t) (k 2)))) (h "1xq7hiwq50v71v228nrikp8f7jda67mpyqpwkp7igyqcav2p76gy")))

(define-public crate-markdown-it-front-matter-0.3.0 (c (n "markdown-it-front-matter") (v "0.3.0") (d (list (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "01aj9y769xiij7b83rx2jg5gf4cxhzjg6y2h2x01xqj1xxnxg0ai")))

