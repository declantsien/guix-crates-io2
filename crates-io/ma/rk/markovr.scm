(define-module (crates-io ma rk markovr) #:use-module (crates-io))

(define-public crate-markovr-0.1.0 (c (n "markovr") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "1pdlksa62q3n8sfxj9i3ygyflj9cc3yrm0byzf6vzmkx81ciklb5") (f (quote (("default" "rand"))))))

(define-public crate-markovr-0.1.1 (c (n "markovr") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "05zhl8nvvf95r73cbwsqxd03yxafsrnixddlz71b7cgkn30ffxix") (f (quote (("default" "rand"))))))

(define-public crate-markovr-0.2.0 (c (n "markovr") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "040bdnrp0cf3mx4ax09zc0nm4ryhl8w3hjbpm1lqrpqnf4v0d1bj") (f (quote (("default" "rand"))))))

(define-public crate-markovr-0.3.0 (c (n "markovr") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "0kw8fnry5k3ms17bgqh9kghncxn02qn6d3h9bzibr5fw83ksnf4j") (f (quote (("default" "rand"))))))

(define-public crate-markovr-0.4.0 (c (n "markovr") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "00d4n6q0h6v4ml2gxh4nl4pfmmfyh4cx1am6x3nk58kg2sczv3h1") (f (quote (("default" "rand"))))))

(define-public crate-markovr-0.4.1 (c (n "markovr") (v "0.4.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "0jq90rdxzz48vh1bnk6rkfvspavilh144py138m9ckp54dfn4sid") (f (quote (("default" "rand"))))))

