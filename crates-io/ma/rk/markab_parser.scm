(define-module (crates-io ma rk markab_parser) #:use-module (crates-io))

(define-public crate-markab_parser-0.1.0 (c (n "markab_parser") (v "0.1.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0289r6f3pp52jnxcris3qq36zbr5bjm24ihgjv8613ivjjbq9s8g")))

(define-public crate-markab_parser-0.2.0 (c (n "markab_parser") (v "0.2.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0flwlb5d6wh01h37a70w7ywhdqzfhlrd8n3bnm4jrrsbj7x6a5g7")))

(define-public crate-markab_parser-0.3.0 (c (n "markab_parser") (v "0.3.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0laphkjn5bqj0ilwddz8qkyvzbhz7bmiwsv2izicv2ww8ahn6lsd")))

(define-public crate-markab_parser-0.4.0 (c (n "markab_parser") (v "0.4.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0vv04zq81wgx7scbk10xp1fsanl303mifwzfsnga0rjjr70bi4qm")))

(define-public crate-markab_parser-0.5.0 (c (n "markab_parser") (v "0.5.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0frkxk7441p6wvs6c1y1vvz382wpzmiskjy1xs470b3xhyli7smd")))

(define-public crate-markab_parser-0.6.0 (c (n "markab_parser") (v "0.6.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0xd12sy6igii7s3phbihijj6qj68y5x883rrnh88ampivv7b2sf3")))

(define-public crate-markab_parser-0.7.0 (c (n "markab_parser") (v "0.7.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)))) (h "0g491033260ks6cf93nldz2lvpmv9s34699wfk4bahw8qgkrdlsc")))

