(define-module (crates-io ma rk markdown_replace) #:use-module (crates-io))

(define-public crate-markdown_replace-0.1.0 (c (n "markdown_replace") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wr2cgzhga36x43aigpn31pis9w6brhz5sjags9qdpbzfp1kx85k")))

