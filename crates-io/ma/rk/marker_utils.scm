(define-module (crates-io ma rk marker_utils) #:use-module (crates-io))

(define-public crate-marker_utils-0.1.0 (c (n "marker_utils") (v "0.1.0") (d (list (d (n "marker_api") (r "^0.1.0") (d #t) (k 0)))) (h "02zx22ir7mkgz9s8vlzb2kwvvi8m9isqbqrxjqhp44zgynlwpjyi")))

(define-public crate-marker_utils-0.1.1 (c (n "marker_utils") (v "0.1.1") (d (list (d (n "marker_api") (r "^0.1.1") (d #t) (k 0)))) (h "0v03hhqvf515vmxna5b3lkxb70k7az1dj8v90w932p2b9rwssrv7")))

(define-public crate-marker_utils-0.2.0 (c (n "marker_utils") (v "0.2.0") (d (list (d (n "marker_api") (r "^0.2.0") (d #t) (k 0)))) (h "1fgldz0vngd99kvahfg3j4wpf945cgm2fsral1znq7p0d40nc6n8")))

(define-public crate-marker_utils-0.2.1 (c (n "marker_utils") (v "0.2.1") (d (list (d (n "marker_api") (r "^0.2.1") (d #t) (k 0)))) (h "1w32g6kkz5nx9a108b7x2qsg5i8ay5p1kwngpfiyzjvibwm9n767")))

(define-public crate-marker_utils-0.3.0 (c (n "marker_utils") (v "0.3.0") (d (list (d (n "marker_api") (r "^0.3.0") (d #t) (k 0)))) (h "07ahhny6n5yryai4j7nsksba7i7h7mzv1ndc5lg4v0a8lw49qhr1") (r "1.66")))

(define-public crate-marker_utils-0.4.0 (c (n "marker_utils") (v "0.4.0") (d (list (d (n "marker_api") (r "^0.4.0") (d #t) (k 0)))) (h "1bm5v8bh2qs63q7v2s6gq7w8j7jfnj8rmsgmx0flk6ipjsf92hqh") (r "1.66")))

(define-public crate-marker_utils-0.4.1 (c (n "marker_utils") (v "0.4.1") (d (list (d (n "marker_api") (r "^0.4.1") (d #t) (k 0)))) (h "1wg1ciy8azgzwhdnypjb9zd693clv4grs2sgbypq3pi88a08w4bi") (r "1.66")))

(define-public crate-marker_utils-0.4.2 (c (n "marker_utils") (v "0.4.2") (d (list (d (n "marker_api") (r "^0.4.2") (d #t) (k 0)))) (h "00938pbllhrhhqvdzjqi1mxi5102hjv570b239hg3z4xbhfmq2il") (r "1.66")))

(define-public crate-marker_utils-0.4.3-rc (c (n "marker_utils") (v "0.4.3-rc") (d (list (d (n "marker_api") (r "^0.4.3-rc") (d #t) (k 0)))) (h "11hjzvvqfs3pb4brhv8jn1m68mba2rsrrnmqk0dzwbfcpgcplp2c") (r "1.66")))

(define-public crate-marker_utils-0.4.3 (c (n "marker_utils") (v "0.4.3") (d (list (d (n "marker_api") (r "^0.4.3") (d #t) (k 0)))) (h "1k03vq4q2iql84396cahzqpn3nsjbx1zd2bx08dlp0v4yprpwwkn") (r "1.66")))

(define-public crate-marker_utils-0.5.0 (c (n "marker_utils") (v "0.5.0") (d (list (d (n "marker_api") (r "^0.5.0") (d #t) (k 0)))) (h "1d2cjsnavs1p56swlx3ndpbk0lknfxhq2b25ir631kz0wnsilgk6") (r "1.70")))

