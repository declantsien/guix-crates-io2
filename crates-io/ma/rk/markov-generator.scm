(define-module (crates-io ma rk markov-generator) #:use-module (crates-io))

(define-public crate-markov-generator-0.1.0 (c (n "markov-generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "14ynpccbnx3vmady5cifzwlb5w8g35pnbpd4g9h9mh82mlcqhxyr") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

(define-public crate-markov-generator-0.1.1 (c (n "markov-generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1yk28xhbzplkg59b4cd3hdqb3jn6ms4asz64cqdqxs261f3ydf7a") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

(define-public crate-markov-generator-0.1.2 (c (n "markov-generator") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "18b0jnxwz70hamgnfakyljnbjhhxsb7aw05mbwqc0m1f1psbsgcy") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

(define-public crate-markov-generator-0.1.3 (c (n "markov-generator") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "0rqss06rrn35g1pbjcakv5svvvfqric3xhzrrfmm4a8sb4mxjsgq") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

(define-public crate-markov-generator-0.1.4 (c (n "markov-generator") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1bf3kw7q2sfw8p68ypaxgd67mdpznrjhmkyl02fvj7nrpywz56ka") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

(define-public crate-markov-generator-0.1.5 (c (n "markov-generator") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "165by4pjxm2l0mr81zgyx4cwh2qn9f5jdaqvpc2l4h3ql2bf69j6") (f (quote (("hash" "std") ("doc_cfg") ("default" "serde" "std" "hash")))) (s 2) (e (quote (("std" "serde?/std" "rand/std" "rand/std_rng")))) (r "1.62")))

