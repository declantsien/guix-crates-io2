(define-module (crates-io ma rk markov) #:use-module (crates-io))

(define-public crate-markov-0.0.1 (c (n "markov") (v "0.0.1") (h "071p7dxz69lrb0n29jrvqflf1d9d35j6nysrv8jvhh6yh0gwj48x")))

(define-public crate-markov-0.0.5 (c (n "markov") (v "0.0.5") (h "0qdabc6094qjrldyc4iclvz2w9mf123wijkpnvz4cp6vzfbzhxxz")))

(define-public crate-markov-0.0.6 (c (n "markov") (v "0.0.6") (h "17k6i3fhmh0zsc1aj5g5314y9zlrzqabl7nbicykaxf4bljfhddh")))

(define-public crate-markov-0.0.7 (c (n "markov") (v "0.0.7") (h "0ahi16gzbk20r2ggbam4ly77mn0pail7rq9gfyqv31xd3mkyqjr2")))

(define-public crate-markov-0.0.8 (c (n "markov") (v "0.0.8") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0pxm07iac1i7qvjd4iilf3qfmasgjnbnhi0fgra34jf6njy65fqc")))

(define-public crate-markov-0.0.9 (c (n "markov") (v "0.0.9") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0k964151158na77bcz17mz8s15bkfzpdnq546m0ksx1i3g0fbw2c")))

(define-public crate-markov-0.0.10 (c (n "markov") (v "0.0.10") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "1w6g67glwbhiwigvqwn91dyalm7m9mwms125jn5jkd5y5gs6xyf6")))

(define-public crate-markov-0.0.11 (c (n "markov") (v "0.0.11") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0svrwj92ljlgzb9b1bvhgnaiwf36f4aiw25i5gwxgv09g425grd9")))

(define-public crate-markov-0.0.12 (c (n "markov") (v "0.0.12") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "10hx65s82vabhwgrnilhc267r1mrir7qzm9vvpviwyrcyjvapzzp")))

(define-public crate-markov-0.0.13 (c (n "markov") (v "0.0.13") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "11qnk83dygdmzz7rgazpdgh8gdw0rzalz98qd84liixmf7mg2gry")))

(define-public crate-markov-0.0.14 (c (n "markov") (v "0.0.14") (d (list (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "1fmg8aq0pbxna35vmxs7qh5rmjj0l0c3kz8fbwyyr63qz1yli4dy")))

(define-public crate-markov-0.0.15 (c (n "markov") (v "0.0.15") (d (list (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "1zrpqb69wam09sa22mi85rlrpi51smvzs0vmdin07q349r3kkrhx")))

(define-public crate-markov-0.0.16 (c (n "markov") (v "0.0.16") (d (list (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "1il6yjy2k68mvsiq9mn5y64rcb86kkszslz2d83lbnix5clgqh50")))

(define-public crate-markov-0.0.17 (c (n "markov") (v "0.0.17") (d (list (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "0j63b6bn5jwhc33s7ddvppvzxqi8lyrcs4k39qs42528wkhdd1kg")))

(define-public crate-markov-0.0.19 (c (n "markov") (v "0.0.19") (d (list (d (n "rand") (r "~0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "0smhwk0f8q1qzs8sxni9bwih00k3cnwsrl5kkf0vwhginr2q9qfy")))

(define-public crate-markov-0.0.20 (c (n "markov") (v "0.0.20") (d (list (d (n "rand") (r "~0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "0g4qwfj6bayc1v2wqn6a5axajxj56d1nzh46y4snyfigy2nfwnnc")))

(define-public crate-markov-0.0.21 (c (n "markov") (v "0.0.21") (d (list (d (n "rand") (r "~0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.9") (d #t) (k 0)))) (h "0ravpdpb66f3dy8lwd1zy97s0gqbq95yiakqmi1z5800560v5a5x")))

(define-public crate-markov-0.0.22 (c (n "markov") (v "0.0.22") (d (list (d (n "rand") (r "~0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.15") (d #t) (k 0)))) (h "176gw8yg4iyvg0ai61j6qwx00xi4p20m1qn0lca5vzz0jsac36x1")))

(define-public crate-markov-0.0.23 (c (n "markov") (v "0.0.23") (d (list (d (n "rand") (r "~0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.15") (d #t) (k 0)))) (h "1vxhnvd2dzcljndhq7qj6841vpirahavi6axmrri23m6223cfagc")))

(define-public crate-markov-0.0.24 (c (n "markov") (v "0.0.24") (d (list (d (n "rand") (r "~0.1.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.15") (d #t) (k 0)))) (h "1g3cj2h92ifz2rifjdfm6cg9k8i9a8jllq7lbdn1122nd3n6gxnd")))

(define-public crate-markov-0.0.25 (c (n "markov") (v "0.0.25") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0csjx1yx0fr5a33dxfzx3rv3y82c92lbp1rqgshd023h2q5kd41b")))

(define-public crate-markov-0.0.26 (c (n "markov") (v "0.0.26") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1hhn23zj1rma55nx2zgy433mbgiawk568s7v8rqwm7fmv8mcnk0d")))

(define-public crate-markov-0.0.27 (c (n "markov") (v "0.0.27") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "04x46kr0sbrkqfph8v9hpa7q517c4bkd7ga386ny5qm9jd7fb47j")))

(define-public crate-markov-0.0.28 (c (n "markov") (v "0.0.28") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1zin40xfy9zqzdpz98gbhra0vk7zw8kswxy999k6nlmw9z8bak49")))

(define-public crate-markov-0.0.29 (c (n "markov") (v "0.0.29") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "19m6yj8rxn9plmkaicxql1y06p0y20wkdj9y48p0r7an2asyl8qx")))

(define-public crate-markov-0.0.30 (c (n "markov") (v "0.0.30") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0cpnn2di690kn8j2w6psv7ccq3x2gv85fr67ng4xmrav9a2nchs5")))

(define-public crate-markov-0.0.31 (c (n "markov") (v "0.0.31") (d (list (d (n "getopts") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1fyi00ki94hiaw0ysad7p403m5prv64b6w1zicfy90qs2v6skpm8")))

(define-public crate-markov-0.0.32 (c (n "markov") (v "0.0.32") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1lbnsrx3s1icvlm37j47yy6rkc3123ibpv4id0rfgvbjbwwmvl23")))

(define-public crate-markov-0.0.33 (c (n "markov") (v "0.0.33") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "017pchsip8lv4ak448gf8yd7z0x25xhqdyfxvk5pjwwqfl27fkja")))

(define-public crate-markov-1.0.0 (c (n "markov") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (o #t) (d #t) (k 0)))) (h "03y2dfn0g5jgq6m1d2nkx78k9qdk16k1nnam81b0c53bxkv25iq8") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

(define-public crate-markov-1.0.1 (c (n "markov") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1y51rpav51wv61276gmkpihwch7cphzqs5l17328j0cxfx24i18d") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

(define-public crate-markov-1.0.2 (c (n "markov") (v "1.0.2") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1zrj8v2yvdgfkmaxr6xv9n1f55ad3iy08q5vmm6fqjfp5sj1fl5r") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

(define-public crate-markov-1.0.3 (c (n "markov") (v "1.0.3") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (o #t) (d #t) (k 0)))) (h "021xygxwjrax1gl0hfma33mnh1gjn4c6y4yc1yx72gwb2b1gxf1y") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

(define-public crate-markov-1.0.4 (c (n "markov") (v "1.0.4") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "15q8l1ffx0c8f7y2p5i76l6cf0j1py36cbzbqcn7vhfcw29crkfc") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

(define-public crate-markov-1.1.0 (c (n "markov") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1zg3f6mvk2v66lr7lw9j146n68llbiwvk4syyrc9a6nm4s7dcspw") (f (quote (("yaml" "serde_yaml") ("markgen" "getopts") ("graph" "petgraph" "itertools") ("default" "graph" "markgen" "yaml"))))))

