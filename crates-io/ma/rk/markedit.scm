(define-module (crates-io ma rk markedit) #:use-module (crates-io))

(define-public crate-markedit-0.1.0 (c (n "markedit") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)))) (h "06v97iv23v04142prrkr4r1wdjm0p209cgw8v94339kfqfxrmkyj")))

(define-public crate-markedit-0.2.0 (c (n "markedit") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)))) (h "1jsabnfq1fp5cidaipf4xj1w85wfc09jy9dac55z269fwwcvhis2")))

(define-public crate-markedit-0.2.1 (c (n "markedit") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)))) (h "1gshyv1psik84abv48zsc466isnp97fiqc2kl5r7dwq6q5a4yc41")))

(define-public crate-markedit-0.3.0 (c (n "markedit") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)))) (h "1sk1l9wsx7y75r60h52nr9l0w8gfnij9v0shcq0b1qpr6q35ln8s")))

