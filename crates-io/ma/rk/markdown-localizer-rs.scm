(define-module (crates-io ma rk markdown-localizer-rs) #:use-module (crates-io))

(define-public crate-markdown-localizer-rs-0.1.0 (c (n "markdown-localizer-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0j5qr5q7znxcr4b6dp5rlpfrxmq6bgymzjjqb0hynqffspxp40xa") (y #t)))

