(define-module (crates-io ma rk marker) #:use-module (crates-io))

(define-public crate-marker-0.1.0 (c (n "marker") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.2") (d #t) (k 0)))) (h "10hwjccvr7p7xqdk7d553bdpxqblzlwqb8vyjydajdfd2m05madd")))

(define-public crate-marker-0.2.0 (c (n "marker") (v "0.2.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.2") (d #t) (k 0)))) (h "1ksm1hn84kll53iq5d5g7kgm9v2rjrwk1nk8hi4k1wr47glx43p2")))

(define-public crate-marker-0.3.0 (c (n "marker") (v "0.3.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "rayon") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.2") (d #t) (k 0)))) (h "129qy2q2i58as3r32lf3v2c1kh6jr4hdg6a32h55jmi8aldr7cxw")))

(define-public crate-marker-0.4.0 (c (n "marker") (v "0.4.0") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "rayon") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "05b299qb65h485xkm7bymnjb12rb69czc215xfjz2h353cl2qw78")))

(define-public crate-marker-0.5.0 (c (n "marker") (v "0.5.0") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.15") (d #t) (k 0)) (d (n "rayon") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "1p0whqffchkqzkz0wdxzgiqazg8fnajcax4xyk3ifgw00rwk28zd")))

(define-public crate-marker-0.6.0-dev (c (n "marker") (v "0.6.0-dev") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "0hqacv60s9a4dyw3xvcpckmxaxqmhgw3qcn6cwc5b2cb8zjha2yk") (y #t)))

(define-public crate-marker-0.6.0 (c (n "marker") (v "0.6.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "129n4z7763pvxpdfb2m175z81m7h85knxjzikmyr1lsj2w2lxp7z")))

(define-public crate-marker-0.7.0 (c (n "marker") (v "0.7.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "1v1m74bhrmqh3x9lnk6j1zfqxqxbslbbrq3d38w6hxpscscza19q")))

(define-public crate-marker-0.8.0 (c (n "marker") (v "0.8.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "1l5mpsw81mc1nwhp5fraxxx56n1x43vp2mhx9d92isrc9jll2aqh")))

(define-public crate-marker-0.9.0 (c (n "marker") (v "0.9.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "0db0hxm60hxxjbdai6vdk2zdwvyf2k62mdpg5gzk30sar3rncxhf") (r "1.70.0")))

