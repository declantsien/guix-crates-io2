(define-module (crates-io ma rk markdown-it-heading-anchors) #:use-module (crates-io))

(define-public crate-markdown-it-heading-anchors-0.1.0 (c (n "markdown-it-heading-anchors") (v "0.1.0") (d (list (d (n "github-slugger") (r "^0.1.0") (d #t) (k 0)) (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "07jz9d76n3f5xpkvxkwpwvxjw45nbxfnr19l4pk2bf079gw3dmzm")))

(define-public crate-markdown-it-heading-anchors-0.2.0 (c (n "markdown-it-heading-anchors") (v "0.2.0") (d (list (d (n "github-slugger") (r "^0.1.0") (d #t) (k 0)) (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "1jxdb5zmnf666r9a1crxs4g4xbjwfhm5apaz5af0jb44crs6wbpf")))

(define-public crate-markdown-it-heading-anchors-0.3.0 (c (n "markdown-it-heading-anchors") (v "0.3.0") (d (list (d (n "github-slugger") (r "^0.1.0") (d #t) (k 0)) (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "1zhmwp1cz0gwhhg9iqqvrmxjnmkp2j61hg0kmkyz46y4dyj7331w")))

