(define-module (crates-io ma rk markdown-prune) #:use-module (crates-io))

(define-public crate-markdown-prune-0.0.6 (c (n "markdown-prune") (v "0.0.6") (h "1bcbjbdqyy6dxxh719lxy9wmrf8sxvlf5cnh4b2ax2pz4m6y2h2r")))

(define-public crate-markdown-prune-0.0.7 (c (n "markdown-prune") (v "0.0.7") (h "1xhycfl48wmz3hpwimn61d5jkr1wq0cvd7a0ch877cqlxp0rc38h")))

