(define-module (crates-io ma rk markdown-to-html-rs) #:use-module (crates-io))

(define-public crate-Markdown-to-HTML-rs-0.1.0 (c (n "Markdown-to-HTML-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06v6si5xv16fkf2irzi21wyqggp5dj4rd2c8cfdksr4gsslvvadi")))

