(define-module (crates-io ma rk markdown-table) #:use-module (crates-io))

(define-public crate-markdown-table-0.1.0 (c (n "markdown-table") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)))) (h "11mfl002fdlppjpp4d2fa08dakqbw58n8f2r8fvr5y72ayf9y5h8")))

(define-public crate-markdown-table-0.2.0 (c (n "markdown-table") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0vq6hdmwwhy05n77x02cxy573zqsd188i80j2zap8d7c569s867l")))

