(define-module (crates-io ma rk markovian) #:use-module (crates-io))

(define-public crate-markovian-0.1.0 (c (n "markovian") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "preexplorer") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)))) (h "1z7fmch0bxlmhdbanc8z9sihbv7b3jm3lhiw0warl464d1i3gm39")))

(define-public crate-markovian-0.1.1 (c (n "markovian") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "preexplorer") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)))) (h "1cv4f3xc05b27h3k82pwm41c8mr718ppzx49cb9c72fwf8gcgw1z")))

(define-public crate-markovian-0.1.2 (c (n "markovian") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "preexplorer") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)))) (h "01qhqi3p5a809cshqy280632lwc1i86x2a4kvzirqixxxk5la6zd")))

(define-public crate-markovian-0.1.3 (c (n "markovian") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "preexplorer") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)))) (h "1kp9h5zz9bkgk8xwhmghlc1cnn51d22gyfgnjf464rar2a7xl1v6")))

(define-public crate-markovian-0.2.1 (c (n "markovian") (v "0.2.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "preexplorer") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hkh61zcnbg79dfl33b2cjq7viahlm7ry01m101dfhwsc283v8ik")))

