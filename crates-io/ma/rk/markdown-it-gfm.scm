(define-module (crates-io ma rk markdown-it-gfm) #:use-module (crates-io))

(define-public crate-markdown-it-gfm-0.1.0 (c (n "markdown-it-gfm") (v "0.1.0") (d (list (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "markdown-it-autolink") (r "^0.2.0") (d #t) (k 0)) (d (n "markdown-it-heading-anchors") (r "^0.3.0") (d #t) (k 0)) (d (n "markdown-it-tasklist") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "0pkxy8z031pi22h853sic2giwp1cjzk4mh56fmx400rjlyllpih4")))

