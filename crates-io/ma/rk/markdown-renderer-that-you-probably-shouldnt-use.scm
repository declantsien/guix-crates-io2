(define-module (crates-io ma rk markdown-renderer-that-you-probably-shouldnt-use) #:use-module (crates-io))

(define-public crate-markdown-renderer-that-you-probably-shouldnt-use-0.1.0 (c (n "markdown-renderer-that-you-probably-shouldnt-use") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (d #t) (k 0)))) (h "1wsayig989w1s4jzndli78f5skz2jrj08hbr6ps0qxhwxa296i0f")))

