(define-module (crates-io ma rk markd) #:use-module (crates-io))

(define-public crate-markd-0.1.0 (c (n "markd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tabled") (r "^0.12.2") (f (quote ("std"))) (k 0)))) (h "1rp56sv9fzxn27va1p62qgybvdvaxfdv8cmdw8km902xawzx7z4k")))

(define-public crate-markd-0.1.1 (c (n "markd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tabled") (r "^0.12.2") (f (quote ("std"))) (k 0)))) (h "03dm4x4g0a03pvh8av1dqrgwp0q3pizl2klhpwc782m0nlrkmzk4")))

(define-public crate-markd-0.2.1 (c (n "markd") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tabled") (r "^0.12.2") (f (quote ("std"))) (k 0)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)))) (h "0yyavdmfw0l9jvpwya1pffyw35m72jhwdhkdpx8z0ak8m5vxis5s")))

