(define-module (crates-io ma rk marker_api) #:use-module (crates-io))

(define-public crate-marker_api-0.0.0-placeholder (c (n "marker_api") (v "0.0.0-placeholder") (h "1s78khrkhg2v3760p3v856vawjjbgq6bnrmb3456byd8ah4bcfbq")))

(define-public crate-marker_api-0.1.0 (c (n "marker_api") (v "0.1.0") (d (list (d (n "visibility") (r "^0.0.1") (d #t) (k 0)))) (h "1kd7hvf4xnvh1ywmx42lfya8wkyvam0skkdbrymnfjjpfwvvng76") (f (quote (("driver-api"))))))

(define-public crate-marker_api-0.1.1 (c (n "marker_api") (v "0.1.1") (d (list (d (n "visibility") (r "^0.0.1") (d #t) (k 0)))) (h "0rlmaql3s3rva5s8qihnysz5my9wdy26y5gzc8am0j1l3dxcj6jd") (f (quote (("driver-api"))))))

(define-public crate-marker_api-0.2.0 (c (n "marker_api") (v "0.2.0") (d (list (d (n "visibility") (r "^0.0.1") (d #t) (k 0)))) (h "094n8nc5jdvnsqc45w08ccinpy2wwcrd4rn8fp4yv8129bs7njhp") (f (quote (("driver-api"))))))

(define-public crate-marker_api-0.2.1 (c (n "marker_api") (v "0.2.1") (d (list (d (n "visibility") (r "^0.0.1") (d #t) (k 0)))) (h "12niaj0af06wxvwiyz5zmhyp1l4hph5lpdhf4dcld2v8ghlrcq6g") (f (quote (("driver-api"))))))

(define-public crate-marker_api-0.3.0 (c (n "marker_api") (v "0.3.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0craqn462719i8qv6xyfxksl77ab7wplmmg3fbn9cwkqvyl3la0n") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.4.0 (c (n "marker_api") (v "0.4.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0vxs4znhf2nhcqq55digaypv5phw3kpaz7ms3acrb7fgaid2lvk5") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.4.1 (c (n "marker_api") (v "0.4.1") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "08blp4dvv6czg0n9rw58aa5xk2b362s0jd27fldlk28li0xxf0w0") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.4.2 (c (n "marker_api") (v "0.4.2") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1xh1gh1amxqj37fiwn8i2l8xrbwzpgiaj73ak6vczf2jlim14zmm") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.4.3-rc (c (n "marker_api") (v "0.4.3-rc") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "146cwpxiygn1p22kigcwk8p8xv6cw16cn56pg8vrpjx3ym9f1wib") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.4.3 (c (n "marker_api") (v "0.4.3") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1wvwjjypi4zmnhr1z3w7z5036kp3y7w2frjdh5gjdwspiyrw5752") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.66")))

(define-public crate-marker_api-0.5.0 (c (n "marker_api") (v "0.5.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "visibility") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0s9p1cy7nfwgiiy0frh936fkwxjiv83n84i4a5jdddpkvj2d3vss") (s 2) (e (quote (("driver-api" "dep:visibility" "dep:typed-builder")))) (r "1.70")))

