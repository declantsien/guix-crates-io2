(define-module (crates-io ma rk markdown-it-tasklist) #:use-module (crates-io))

(define-public crate-markdown-it-tasklist-0.1.0 (c (n "markdown-it-tasklist") (v "0.1.0") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "stacker") (r ">=0.1.2, <0.2") (d #t) (k 0)) (d (n "testing") (r "^0.33.13") (d #t) (k 2)))) (h "02c8wdnj5spcr1w0kib959iw99h8ydpf5b0vq01hl0fjccxzp4b2")))

(define-public crate-markdown-it-tasklist-0.1.1 (c (n "markdown-it-tasklist") (v "0.1.1") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "stacker") (r ">=0.1.2, <0.2") (d #t) (k 0)) (d (n "testing") (r "^0.33.13") (d #t) (k 2)))) (h "0m9bf65frprs1v4gbnha8n5flihsv7sj41jyrrlxryzfjajq48vl")))

(define-public crate-markdown-it-tasklist-0.1.2 (c (n "markdown-it-tasklist") (v "0.1.2") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "stacker") (r ">=0.1.2, <0.2") (d #t) (k 0)) (d (n "testing") (r "^0.33.13") (d #t) (k 2)))) (h "19hg9q8vhkr7v0awd76653plfrkjzy84ymw3j3vgdarr7pyjphyv")))

(define-public crate-markdown-it-tasklist-0.1.3 (c (n "markdown-it-tasklist") (v "0.1.3") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "stacker") (r ">=0.1.2, <0.2") (d #t) (k 0)) (d (n "testing") (r "^0.33.13") (d #t) (k 2)))) (h "1n3pgpkv8j9vh6bwa01nd2vvn34b2g7dw0cqq162x01q53m820zx")))

(define-public crate-markdown-it-tasklist-0.2.0 (c (n "markdown-it-tasklist") (v "0.2.0") (d (list (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "stacker") (r ">=0.1.2, <0.2") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "15nlmzmsf50gwzzd7mmjx7ymmv6rz4j81z7fl1njlmcnrpkw40f4")))

