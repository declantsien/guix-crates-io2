(define-module (crates-io ma rk marked-cli) #:use-module (crates-io))

(define-public crate-marked-cli-0.1.0 (c (n "marked-cli") (v "0.1.0") (d (list (d (n "clap") (r ">= 2.33.0, < 2.34") (f (quote ("wrap_help"))) (k 0)) (d (n "encoding_rs") (r ">= 0.8.13, < 2.0") (d #t) (k 0)) (d (n "html5ever") (r ">= 0.25.1, < 2.0") (d #t) (k 0)) (d (n "log") (r ">= 0.4.4, < 0.4.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "marked") (r ">= 0.1.0, < 0.2.0") (d #t) (k 0)))) (h "1rrjg1879yk6bqij4j6y6ifc3xxxbm48fdnpnf4116w5vz1lwc6d")))

(define-public crate-marked-cli-0.2.0 (c (n "marked-cli") (v "0.2.0") (d (list (d (n "clap") (r ">= 2.33.0, < 2.34") (f (quote ("wrap_help"))) (k 0)) (d (n "encoding_rs") (r ">= 0.8.13, < 0.9") (d #t) (k 0)) (d (n "html5ever") (r ">= 0.25.1, < 0.26") (d #t) (k 0)) (d (n "log") (r ">= 0.4.4, < 0.4.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "marked") (r ">= 0.2.0, < 0.3.0") (d #t) (k 0)))) (h "1mmj6a12jggyvq9frm5vvrqgvb4ajd9k12rbpcn5qy7x1vrlwxsv")))

(define-public crate-marked-cli-0.3.0 (c (n "marked-cli") (v "0.3.0") (d (list (d (n "clap") (r ">=2.33.0, <2.34") (f (quote ("wrap_help"))) (k 0)) (d (n "encoding_rs") (r ">=0.8.13, <0.9") (d #t) (k 0)) (d (n "html5ever") (r ">=0.25.1, <0.26") (d #t) (k 0)) (d (n "log") (r ">=0.4.4, <0.4.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "marked") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "0a761k5y8rx10f0s2n8446ria68n9s3cnczli1f7imbn26kv6x8b")))

(define-public crate-marked-cli-0.3.1 (c (n "marked-cli") (v "0.3.1") (d (list (d (n "clap") (r ">=2.33.0, <2.34") (f (quote ("wrap_help"))) (k 0)) (d (n "encoding_rs") (r ">=0.8.13, <0.9") (d #t) (k 0)) (d (n "html5ever") (r ">=0.25.1, <0.26") (d #t) (k 0)) (d (n "log") (r ">=0.4.4, <0.4.15") (f (quote ("std"))) (d #t) (k 0)) (d (n "marked") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "1qmgkiqns3r56pmnpa3cdhkac57cmksbyfy7n39pnyi27phva24d")))

