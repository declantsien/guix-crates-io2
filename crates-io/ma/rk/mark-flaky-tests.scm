(define-module (crates-io ma rk mark-flaky-tests) #:use-module (crates-io))

(define-public crate-mark-flaky-tests-1.0.0 (c (n "mark-flaky-tests") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mark-flaky-tests-macro") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0w4cyh4khhy08mp102sl0pdd2vmsqrpxc7y7krkg2jj704x5x50p") (f (quote (("tokio" "futures"))))))

(define-public crate-mark-flaky-tests-1.0.1 (c (n "mark-flaky-tests") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mark-flaky-tests-macro") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "10fc0325qqnyc55d544qqrw0l1my6drjzqmqx6qlc44qxmbcwwpi") (f (quote (("tokio" "futures"))))))

(define-public crate-mark-flaky-tests-1.0.2 (c (n "mark-flaky-tests") (v "1.0.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mark-flaky-tests-macro") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0c29bflpb5aawl5vzcai2rhvphskvh7gdr5v9sq52lx0jmy4lv2q") (f (quote (("tokio" "futures"))))))

