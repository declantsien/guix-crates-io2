(define-module (crates-io ma rk marketplace) #:use-module (crates-io))

(define-public crate-marketplace-0.1.0 (c (n "marketplace") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.3.1") (d #t) (k 0)) (d (n "cw721") (r "^0.18.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (k 0)))) (h "0f7pqzxn28nkclhs42iixq8m3sy0qk0b9y75z674dcqwyqi2rydy") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

