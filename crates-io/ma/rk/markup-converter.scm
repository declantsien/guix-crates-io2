(define-module (crates-io ma rk markup-converter) #:use-module (crates-io))

(define-public crate-markup-converter-0.1.0 (c (n "markup-converter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "jaq-core") (r "^0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01xw5illfrl66nn1r6xljd4qcxqxsf7xmkrn0vwxf0y3wacnxpa6")))

(define-public crate-markup-converter-0.2.0 (c (n "markup-converter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "jaq-core") (r "^0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bjn1zvv0kwchfgdibmsi2q2m88rhxkvwv7sidriy6qrd3zp6lq4")))

