(define-module (crates-io ma rk markdown-table-formatter) #:use-module (crates-io))

(define-public crate-markdown-table-formatter-0.2.0 (c (n "markdown-table-formatter") (v "0.2.0") (d (list (d (n "comrak") (r "^0.19.0") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)) (d (n "unicode-display-width") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "08hqdg05zws32gf5n9k62n7ic12i8ps17jm5hfpykfsi4z159p4m")))

(define-public crate-markdown-table-formatter-0.3.0 (c (n "markdown-table-formatter") (v "0.3.0") (d (list (d (n "comrak") (r "^0.19.0") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)) (d (n "unicode-display-width") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0s8pz4cm8n40nz65jai805kg2lfpsxxwni0cglla4386jij1fih3")))

