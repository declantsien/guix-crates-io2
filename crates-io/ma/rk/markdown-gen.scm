(define-module (crates-io ma rk markdown-gen) #:use-module (crates-io))

(define-public crate-markdown-gen-0.1.0 (c (n "markdown-gen") (v "0.1.0") (h "0vlgxyhqc91a00pcihphsx26ii6mckfiyill70xn3zy8z4rwmga0")))

(define-public crate-markdown-gen-0.1.1 (c (n "markdown-gen") (v "0.1.1") (h "05n3m4639ijd8wpczwb2zh1pihlk0m1bhdls2cbjaga1bx7w5i9b")))

(define-public crate-markdown-gen-1.0.0 (c (n "markdown-gen") (v "1.0.0") (h "0vjz7s243iikmwz4xy68wrdkcishsyhmsp2qc0k7i8kgbis7239b")))

(define-public crate-markdown-gen-1.0.1 (c (n "markdown-gen") (v "1.0.1") (h "1vrs62b714ba8rg8n2gy27amzn26g5q7nbiw99h6ahjpci9aj7i0")))

(define-public crate-markdown-gen-1.1.1 (c (n "markdown-gen") (v "1.1.1") (h "0zvqw533w685bl4rfsvryaz3flbrm925af9a7j0vkzir2xq03g9m")))

(define-public crate-markdown-gen-1.2.1 (c (n "markdown-gen") (v "1.2.1") (h "0qh567qdlry514564iiambj7xli575g21ffzl5y32n0jgwfn4d40")))

