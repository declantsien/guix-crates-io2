(define-module (crates-io ma rk markout) #:use-module (crates-io))

(define-public crate-markout-0.1.0 (c (n "markout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "comrak") (r "^0.10.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1mam82c4bk462jndg9yrix3wab00hjh7gh5l0mq61knf620k4zvi")))

