(define-module (crates-io ma rk markdown-it-deflist) #:use-module (crates-io))

(define-public crate-markdown-it-deflist-0.1.1 (c (n "markdown-it-deflist") (v "0.1.1") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "177xcglrnqbcpd8xkjxf17sizcakk2l2n497ayda0f84jw6bk7dj")))

(define-public crate-markdown-it-deflist-0.2.0 (c (n "markdown-it-deflist") (v "0.2.0") (d (list (d (n "markdown-it") (r "^0.5") (d #t) (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "1sv30bf5x91g9r9hingbbb1xbv4zdk6zfzhjr3qpayhbq8d0x7xh")))

(define-public crate-markdown-it-deflist-0.3.0 (c (n "markdown-it-deflist") (v "0.3.0") (d (list (d (n "markdown-it") (r "^0.6") (k 0)) (d (n "testing") (r "^0.33") (d #t) (k 2)))) (h "17p5f4lv19s85l6k0mp8ss4k5vpidp3l8bffxmxjyksrr4gzqxhg")))

