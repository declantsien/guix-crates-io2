(define-module (crates-io ma rk markdown-formatter) #:use-module (crates-io))

(define-public crate-markdown-formatter-0.0.1 (c (n "markdown-formatter") (v "0.0.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0mh21fdb7k7vq7svpvi3953ml8gl80s1jjgx66iqfsn15nwj582s")))

(define-public crate-markdown-formatter-0.0.2 (c (n "markdown-formatter") (v "0.0.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1wjak12v05cxlm3y1zx591a62qvc08ywykis6djaxdz4wzkx5ahw")))

(define-public crate-markdown-formatter-0.0.3 (c (n "markdown-formatter") (v "0.0.3") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "12dkd2in3d9nwxnd13pf4jqa001iqvl7jzwif5bp7k2svzqpc4r6")))

(define-public crate-markdown-formatter-0.0.4 (c (n "markdown-formatter") (v "0.0.4") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1c7py6l36f9prxjrvfchchkcpqbxyr8bfcxina7gwfqmj5yx3520") (f (quote (("formatter") ("default" "formatter") ("debug"))))))

(define-public crate-markdown-formatter-0.0.8 (c (n "markdown-formatter") (v "0.0.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0raxdqlbyxbqn4242fxb3mqlw7yl9vw0ypixc3gn20cg862n34df") (f (quote (("formatter") ("default" "formatter") ("debug"))))))

(define-public crate-markdown-formatter-0.0.10 (c (n "markdown-formatter") (v "0.0.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "18a78ah7w5kq86kdx0yawlwr1iyb1c6f50c32dl46kvk36wj8hap") (f (quote (("formatter") ("default" "formatter") ("debug"))))))

(define-public crate-markdown-formatter-0.0.11 (c (n "markdown-formatter") (v "0.0.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "termsize-alt") (r "^0.2.0") (d #t) (k 0)))) (h "0wk1qsrg16d13gm5mickp6dg67sc2cp6ci84b8s2ycsvwd9hrpdm") (f (quote (("formatter") ("default" "formatter") ("debug"))))))

(define-public crate-markdown-formatter-0.0.13 (c (n "markdown-formatter") (v "0.0.13") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "termsize-alt") (r "^0.2.1") (d #t) (k 0)))) (h "0sn9vb0gl4wxvqhjv41baxzkgs4ai03icm5f4rly1zc3lmra89h2") (f (quote (("formatter") ("default" "formatter") ("debug"))))))

