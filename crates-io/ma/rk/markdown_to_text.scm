(define-module (crates-io ma rk markdown_to_text) #:use-module (crates-io))

(define-public crate-markdown_to_text-0.1.0 (c (n "markdown_to_text") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)))) (h "1w0mj9faq7ai6im64jhpah4j1lqvap6ks9w5465lid3q7nl1bl9g")))

(define-public crate-markdown_to_text-1.0.0 (c (n "markdown_to_text") (v "1.0.0") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)))) (h "0h993lshvjx874lic0ln8hwdm61kfwcp4igw3aarvj76ldwfklqx")))

