(define-module (crates-io ma rk markdown_converter) #:use-module (crates-io))

(define-public crate-markdown_converter-0.1.0 (c (n "markdown_converter") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0vx7clnnchzgcy223r7m3yjw0fc1h2l8bmd6iwr18b1qa66p7cdd")))

(define-public crate-markdown_converter-0.2.0 (c (n "markdown_converter") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1cxx40zj79cfkjzjzjkgrdn2xiy7196s06vqqsgvnrlkpvpi9fbs")))

