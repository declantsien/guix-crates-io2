(define-module (crates-io ma rk marksad) #:use-module (crates-io))

(define-public crate-marksad-0.1.0 (c (n "marksad") (v "0.1.0") (h "10fc3vv0fsmdccz17jd0m28kll4m21ng8831anvfvnd4bihz7ad7") (r "1.77")))

(define-public crate-marksad-0.1.1 (c (n "marksad") (v "0.1.1") (h "1grlnmdhc0mwvf6hq15ig2jip6yjzzbc3r9c6w0zyy2dhkliqxs5") (r "1.77")))

