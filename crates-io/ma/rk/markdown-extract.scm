(define-module (crates-io ma rk markdown-extract) #:use-module (crates-io))

(define-public crate-markdown-extract-0.1.0-alpha (c (n "markdown-extract") (v "0.1.0-alpha") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "18mrlci5vksn3db0ngr1fniq49fqan3izksafyp6mqixwnw03zmb")))

(define-public crate-markdown-extract-0.1.1 (c (n "markdown-extract") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rb176n11kh6fbbsjc5k1y137nl0cvffzm9lfg07y2vvzifya3kz")))

(define-public crate-markdown-extract-1.0.0 (c (n "markdown-extract") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09yqj7jssjwp41m0bpjg29vfiw5rhn6divyqzzg7kbphac1fpwch")))

(define-public crate-markdown-extract-1.1.0 (c (n "markdown-extract") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0yl16k02vy1ncc4qhc5c5ym9b46asyrpxvfdq5c19ghy1fxalc43")))

(define-public crate-markdown-extract-2.0.0 (c (n "markdown-extract") (v "2.0.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0bh094rh8nbn66pw89qq018ihr85plwarq9dl1dp2jy2d7kbq2qh")))

