(define-module (crates-io ma rk markov_namegen) #:use-module (crates-io))

(define-public crate-markov_namegen-0.1.0 (c (n "markov_namegen") (v "0.1.0") (d (list (d (n "multimarkov") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "13dxgvpy0dcnjcx5q67zigqrbyc9r8hx8ywq3vxplzvf8p6qzzhg")))

(define-public crate-markov_namegen-0.1.2 (c (n "markov_namegen") (v "0.1.2") (d (list (d (n "multimarkov") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0vpdlbz7jh0fcshlkpaanxdgrrsrlbdsfdw0b5j105v6f8v8pdli")))

(define-public crate-markov_namegen-0.1.3 (c (n "markov_namegen") (v "0.1.3") (d (list (d (n "multimarkov") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "10b5gj7li02nfawapf74f3k60ffwli4ffrxqclds41dpnwc74wr8")))

(define-public crate-markov_namegen-0.2.0 (c (n "markov_namegen") (v "0.2.0") (d (list (d (n "is-vowel") (r "^0.1.0") (d #t) (k 0)) (d (n "multimarkov") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0ks420qwi4qf2krdswakssv2585z6xrz2m2xhzwhdplx4sgxnbpp")))

(define-public crate-markov_namegen-0.2.1 (c (n "markov_namegen") (v "0.2.1") (d (list (d (n "is-vowel") (r "^0.1.0") (d #t) (k 0)) (d (n "multimarkov") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "06y3sb0db670pp9r8r9677b3714dx4ywpp2ph6d8w6c2s2sclhyx")))

(define-public crate-markov_namegen-0.3.0 (c (n "markov_namegen") (v "0.3.0") (d (list (d (n "is-vowel") (r "^0.1.0") (d #t) (k 0)) (d (n "multimarkov") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "04kryb6bfqnm8asycjf5rwvn2zib7nyhnhxwf6rvgwg7qskbksc4")))

(define-public crate-markov_namegen-0.3.1 (c (n "markov_namegen") (v "0.3.1") (d (list (d (n "is-vowel") (r "^0.1.0") (d #t) (k 0)) (d (n "multimarkov") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "12s8p1khsbh665vsshlbmmc4fgbdgj5z0gkplkahd0hm3apf7y0n")))

