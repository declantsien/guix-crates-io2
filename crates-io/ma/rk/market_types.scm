(define-module (crates-io ma rk market_types) #:use-module (crates-io))

(define-public crate-market_types-0.1.0 (c (n "market_types") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "market") (r "^0.30.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("namedpipeapi"))) (d #t) (k 0)))) (h "01yyy94h74zwp4l1p82flrdfw4sb4djcj2jm5g7gr6hqhynsbi68") (f (quote (("unstable-doc-cfg") ("thread" "std" "crossbeam-queue") ("std" "market/std"))))))

