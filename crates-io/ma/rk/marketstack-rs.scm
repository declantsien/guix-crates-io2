(define-module (crates-io ma rk marketstack-rs) #:use-module (crates-io))

(define-public crate-marketstack-rs-0.1.0 (c (n "marketstack-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_builder") (r "~0.12") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "http") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.11.19") (f (quote ("blocking" "json"))) (o #t) (k 0)) (d (n "serde") (r "~1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_urlencoded") (r "~0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "192inp5l8564pgq8dp2qdyyhypadi3aqcl490c8qckyygacypk8w") (y #t)))

