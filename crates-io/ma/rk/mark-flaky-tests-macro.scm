(define-module (crates-io ma rk mark-flaky-tests-macro) #:use-module (crates-io))

(define-public crate-mark-flaky-tests-macro-1.0.0 (c (n "mark-flaky-tests-macro") (v "1.0.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rpy1110691877bal6sp8d2k3gq9iz69bm5p6asprj6kxil69z73")))

(define-public crate-mark-flaky-tests-macro-1.0.1 (c (n "mark-flaky-tests-macro") (v "1.0.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k920an1qcs1r4lfzswhhdafydzdgf4fp3rj4pcj5hn5df4iwvpj")))

(define-public crate-mark-flaky-tests-macro-1.0.2 (c (n "mark-flaky-tests-macro") (v "1.0.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "115bb0pb4vb8pwm6lblcnc6zxxlk6w654njiphp696dj2vyiz2q7")))

