(define-module (crates-io ma rk markup5ever_rcdom) #:use-module (crates-io))

(define-public crate-markup5ever_rcdom-0.1.0 (c (n "markup5ever_rcdom") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.16") (d #t) (k 0)))) (h "0axf6vrms8579lvhbjaj0v7bhs8xb7s26d4sam2g3m6qpi1xl5gh")))

(define-public crate-markup5ever_rcdom-0.2.0 (c (n "markup5ever_rcdom") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.17") (d #t) (k 0)))) (h "1hir73wmvl0i5mfplfjg0qvxxmsn8qp5xmjkdkp813hgfpb1slmr")))

(define-public crate-markup5ever_rcdom-0.3.0 (c (n "markup5ever_rcdom") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.27") (d #t) (k 0)) (d (n "markup5ever") (r "^0.12") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "xml5ever") (r "^0.18") (d #t) (k 0)))) (h "065yb6zn9sfn7kqk5wwc48czsls5z3hzgrddk58fxgq16ymj3apd")))

