(define-module (crates-io ma rk marker_uitest) #:use-module (crates-io))

(define-public crate-marker_uitest-0.0.0-placeholder (c (n "marker_uitest") (v "0.0.0-placeholder") (h "1sinrl8idfs00m9qv11sad8gm2kfd4c07qnmf4w0wvyza51jd6by")))

(define-public crate-marker_uitest-0.1.0 (c (n "marker_uitest") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.11.7") (d #t) (k 0)))) (h "1x6ik9h61hihq2dvsffzg7zj854x28y3c3v3ysa6ks75k4ykk61v") (f (quote (("dev-build") ("default"))))))

(define-public crate-marker_uitest-0.1.1 (c (n "marker_uitest") (v "0.1.1") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.11.7") (d #t) (k 0)))) (h "0plqvqpy1mzxc4ch3xlfxpsv5wg46d82dcrj326qndfn3w9vw3pn") (f (quote (("dev-build") ("default"))))))

(define-public crate-marker_uitest-0.2.0 (c (n "marker_uitest") (v "0.2.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.11.7") (d #t) (k 0)))) (h "0aiqc7b40dxlnv7rclsz724qmybnxw5vngsnkyd0wa4g2kr1vm73") (f (quote (("dev-build") ("default"))))))

(define-public crate-marker_uitest-0.2.1 (c (n "marker_uitest") (v "0.2.1") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.11.7") (d #t) (k 0)))) (h "14daz1kl79x8lrxhxdg1sql3q2byrqdfgdvbkrk984fl5k3aa60q") (f (quote (("dev-build") ("default"))))))

(define-public crate-marker_uitest-0.3.0 (c (n "marker_uitest") (v "0.3.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "0ccm0ffj06wfqd39qjb4vbydldvylafilpnwqyab8z221rnjd38n") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.4.0 (c (n "marker_uitest") (v "0.4.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "19g7bjr4qdabxpzx622drll91pxw7cd8ysj0immfhy8nyqkngqv2") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.4.1 (c (n "marker_uitest") (v "0.4.1") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "1zmib5vnpv8bby6g16f99km6a40xq2xr1q3acgwnzkcryg9vcb56") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.4.2 (c (n "marker_uitest") (v "0.4.2") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "18rzn5j25m8b3gniy16p5jyh211dg3x8kfbrzxa69wldy62d3zj9") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.4.3-rc (c (n "marker_uitest") (v "0.4.3-rc") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "07vnqlajkddjqr5vxbrgs61pl2iwxn6rhn1l002zlk1wbpp4zbmw") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.4.3 (c (n "marker_uitest") (v "0.4.3") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "0szqf5cv08sv836lj386jyylc135nj7rwnfkigrs0zvkwf8adbqy") (f (quote (("dev-build") ("default")))) (r "1.66")))

(define-public crate-marker_uitest-0.5.0 (c (n "marker_uitest") (v "0.5.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "ui_test") (r "^0.21.2") (d #t) (k 0)))) (h "164pf386gi9sh19mxnvq7z3irfhs4kvg6ck4dw6a3zyhabddmwi0") (f (quote (("dev-build") ("default")))) (r "1.70")))

