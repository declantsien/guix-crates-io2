(define-module (crates-io ma rk markup-css-once) #:use-module (crates-io))

(define-public crate-markup-css-once-0.1.0 (c (n "markup-css-once") (v "0.1.0") (d (list (d (n "markup") (r "^0.13") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "1brp0371hgxfc74ypw7n5hywp3f4c86d11lr1y8xgvj9p5hpx6wj")))

