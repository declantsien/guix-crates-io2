(define-module (crates-io ma rk mark_recall) #:use-module (crates-io))

(define-public crate-mark_recall-0.1.0 (c (n "mark_recall") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0x6sdf7l0nwd4f7cvnp17l54m7ll2lbk9vxjnbkbv3fvd3cgfcz7")))

