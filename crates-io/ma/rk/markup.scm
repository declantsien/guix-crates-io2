(define-module (crates-io ma rk markup) #:use-module (crates-io))

(define-public crate-markup-0.1.0 (c (n "markup") (v "0.1.0") (d (list (d (n "markup-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "1xffq667759s7lcf8f95qm9km6c4x4yp48547xziwrv9ldg9xc27")))

(define-public crate-markup-0.1.1 (c (n "markup") (v "0.1.1") (d (list (d (n "markup-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "12hpwsxyd7bxvyc456j09bfzqyjbq60m8sssidlrqpfjz2pmp2yf")))

(define-public crate-markup-0.1.2 (c (n "markup") (v "0.1.2") (d (list (d (n "markup-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "0k4chj21zqdfg1v29a35ajamlhljavzhqrb77pvxm7k9apbfvwld")))

(define-public crate-markup-0.1.3 (c (n "markup") (v "0.1.3") (d (list (d (n "markup-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "01cq4d0s69ycg3fr9arp5gbsv6ks0s88qfh14rdiyky8ww5b5bx2")))

(define-public crate-markup-0.1.4 (c (n "markup") (v "0.1.4") (d (list (d (n "markup-proc-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0fgmb96hplg69mggim4bqbil0qhy5ay3ivcgif0sdghdxvpphasm")))

(define-public crate-markup-0.1.5 (c (n "markup") (v "0.1.5") (d (list (d (n "markup-proc-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1c5rc780c6s0rkwvpa7p8vi0c1wyw0r8lqk5pdlgp03qwhjcx9db")))

(define-public crate-markup-0.2.0 (c (n "markup") (v "0.2.0") (d (list (d (n "markup-proc-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1fv0i66cskl9q7xl78x53hisfs3m2ma9pgzf7dklrm0q6n9y6vsi")))

(define-public crate-markup-0.2.1 (c (n "markup") (v "0.2.1") (d (list (d (n "markup-proc-macro") (r "^0.2.1") (d #t) (k 0)))) (h "0ziji4rpdq2ys4q8lh5xwnf16wvdkm0qk1mbhjcald3i5b81bnl4")))

(define-public crate-markup-0.2.2 (c (n "markup") (v "0.2.2") (d (list (d (n "markup-proc-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0px8lbv2fwa32fs1qm9l245jqjr0irqgiqpgp4sap97kliapvx6n")))

(define-public crate-markup-0.3.0 (c (n "markup") (v "0.3.0") (d (list (d (n "markup-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "13srkfb4r54x2wcpjx8dpzn70d3r4inrhcxr1ga0vghxn99l6aag")))

(define-public crate-markup-0.3.1 (c (n "markup") (v "0.3.1") (d (list (d (n "markup-proc-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1sfwh0c5ihzxy6amcrypr37m9fjx1jgdh3r2602ibhz87bcc83in")))

(define-public crate-markup-0.4.0 (c (n "markup") (v "0.4.0") (d (list (d (n "markup-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "16lgaafmpknaxiccc8631f2kz924p28nl25wwbqardwfr69n9vzw")))

(define-public crate-markup-0.4.1 (c (n "markup") (v "0.4.1") (d (list (d (n "markup-proc-macro") (r "^0.4.1") (d #t) (k 0)))) (h "1d6cn12mm2ysrnj26xwz3rrzh4jjz01c13iyib33x1l65ahwhspa")))

(define-public crate-markup-0.5.0 (c (n "markup") (v "0.5.0") (d (list (d (n "markup-proc-macro") (r "^0.5.0") (d #t) (k 0)))) (h "0aqih8p85zi7whkd1zsin6sz5zkjjjm39l8sb257pvpdbn5c3jyj")))

(define-public crate-markup-0.6.0 (c (n "markup") (v "0.6.0") (d (list (d (n "markup-proc-macro") (r "^0.6.0") (d #t) (k 0)))) (h "1gmynsphp8q7yyayipkrm37w8rfhhxpds9sa2caw7zin132xqgav")))

(define-public crate-markup-0.6.1 (c (n "markup") (v "0.6.1") (d (list (d (n "markup-proc-macro") (r "^0.6.1") (d #t) (k 0)))) (h "0iaj06nkpi0wqf1n3dhbzrv135zjk0c29q8lxj3wgm6qxw9nhi6i")))

(define-public crate-markup-0.6.2 (c (n "markup") (v "0.6.2") (d (list (d (n "markup-proc-macro") (r "^0.6.2") (d #t) (k 0)))) (h "1kfyfxcyryq2fnxcvjfgp31d5ixf3mbbwvlgi770m1w3wq0gx8mz")))

(define-public crate-markup-0.7.0 (c (n "markup") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "markup-proc-macro") (r "^0.7.0") (d #t) (k 0)))) (h "0am8nvwa78hkc4d55zkp3wpzls4vw2h4nfcnn28h7h6m53xsidd5")))

(define-public crate-markup-0.8.0 (c (n "markup") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.8.0") (d #t) (k 0)))) (h "0s6j9capaf20q3796mmfmfq0a52a460w3x9drdmc9d9wj0h5xv14")))

(define-public crate-markup-0.8.1 (c (n "markup") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.8.1") (d #t) (k 0)))) (h "0z6m4bi72ign8yvrrvmraf2y0wrzda4gbysy5kl8zk77afrm3slj")))

(define-public crate-markup-0.8.2 (c (n "markup") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.8.2") (d #t) (k 0)))) (h "1llr5zbcgyxa8zlkj8016l9g7jmh5mdmd5n99sd57m86zhl466x9")))

(define-public crate-markup-0.9.0 (c (n "markup") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.9.0") (d #t) (k 0)))) (h "0vmb6f4cjpsr9i3mlsh13b3gywp2ng20by10xd6as69s7qijx3x0")))

(define-public crate-markup-0.9.1 (c (n "markup") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.9.1") (d #t) (k 0)))) (h "1yafnxzqzzl5a35mllw461sywllxns1y2za6bdhyfmyhpgih3fkk")))

(define-public crate-markup-0.10.0 (c (n "markup") (v "0.10.0") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1rs5sfk2rcq30lyyvzq5hz0hm5nqz5l79j447qvbg0lncl265fn8")))

(define-public crate-markup-0.11.0 (c (n "markup") (v "0.11.0") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.11.0") (d #t) (k 0)))) (h "0ij54dnwa8rb8hd5wkqa48cascq89q3z64qdabqgzl9n8v6lgz07")))

(define-public crate-markup-0.12.0 (c (n "markup") (v "0.12.0") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.0") (d #t) (k 0)))) (h "15w3qwjja9bqmg8wqn4j4b8yy682275rb282fwimvig62fnpzvlv")))

(define-public crate-markup-0.12.1 (c (n "markup") (v "0.12.1") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.1") (d #t) (k 0)))) (h "1xigfsilbigi2lqfqa3x1qv5wn0cp387yls5zm7gvg19z12vjp0p")))

(define-public crate-markup-0.12.2 (c (n "markup") (v "0.12.2") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.2") (d #t) (k 0)))) (h "0i325s4cz9jjl9qnpda9iiqh6vswx0nzbbvrcf7hadhnl998jvbp")))

(define-public crate-markup-0.12.3 (c (n "markup") (v "0.12.3") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.3") (d #t) (k 0)))) (h "14c17m24b534kg5sf34wbxk8l5iba0h6q1lbmxv7n7nd8pbysnxn")))

(define-public crate-markup-0.12.4 (c (n "markup") (v "0.12.4") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.4") (d #t) (k 0)))) (h "0jp8pr47mhnmig59wcapm3483ksr1cn76pqdb0sa4jjyyaqj5ivv")))

(define-public crate-markup-0.12.5 (c (n "markup") (v "0.12.5") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.12.5") (d #t) (k 0)))) (h "1vslxv51nk3g1ijwibhv6lp2ipc3lxi5bcbds89yb82410kljrg5")))

(define-public crate-markup-0.13.0 (c (n "markup") (v "0.13.0") (d (list (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.13.0") (d #t) (k 0)))) (h "09rs7508jjy72n4w530sl0fvvk15axfsipn9hnz6669rypmd2a8z")))

(define-public crate-markup-0.13.1 (c (n "markup") (v "0.13.1") (d (list (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.13.1") (d #t) (k 0)))) (h "03cb086vkqdym3z0wq9h8ywgc2v1w9n4d8pn0j6p76fl6ni9d4dx")))

(define-public crate-markup-0.14.0 (c (n "markup") (v "0.14.0") (d (list (d (n "itoa") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.14.0") (d #t) (k 0)))) (h "146mbnyk399xf4vg2q4p1hfwbdkxj1d3njv4a8lxfjh1xva59cni")))

(define-public crate-markup-0.15.0 (c (n "markup") (v "0.15.0") (d (list (d (n "itoa") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "markup-proc-macro") (r "^0.15.0") (d #t) (k 0)))) (h "161q8mzp195pw5k2w6wfi290wwnkrmzwffilawi05q8gcanqga3l")))

