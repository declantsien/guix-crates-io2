(define-module (crates-io ma rk markable_reader) #:use-module (crates-io))

(define-public crate-markable_reader-1.0.0 (c (n "markable_reader") (v "1.0.0") (h "0861sh5sn6l8dzdmd29kk6lwnf49cv5002rrsq109gs7pg3j4afv")))

(define-public crate-markable_reader-1.1.0 (c (n "markable_reader") (v "1.1.0") (h "0m46dc3jwgc5b5a288rizjh2zr30zdxbfrky4zigkdaar3cnkxv0")))

(define-public crate-markable_reader-2.0.0 (c (n "markable_reader") (v "2.0.0") (h "05bz0pml2kcpwvgqxkf1fxnmm7aj0s455j91r6i00cbv5vf7mynb")))

(define-public crate-markable_reader-2.1.0 (c (n "markable_reader") (v "2.1.0") (h "1v0z3w297mmhgljnq0p0czqyigzrsmhhx4kpf7wvnrrsdidszsj6")))

(define-public crate-markable_reader-2.1.1 (c (n "markable_reader") (v "2.1.1") (h "0z4wa02v073zwzx2v81xvwjb5wpi700s5ffzd6arc9dx1rkfdanr")))

