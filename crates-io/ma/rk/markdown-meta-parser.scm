(define-module (crates-io ma rk markdown-meta-parser) #:use-module (crates-io))

(define-public crate-markdown-meta-parser-0.1.0 (c (n "markdown-meta-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "12rr1zzhr3hqqq71pzs3ifir46br2ia6l417365j5jx893478ncq")))

(define-public crate-markdown-meta-parser-0.1.1 (c (n "markdown-meta-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1r8sw4x5mv15zzkdbjhmjpbbbac7dihn2sz2khdw564rs9vsc43a")))

(define-public crate-markdown-meta-parser-0.1.2 (c (n "markdown-meta-parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1lr6b0gq6v96jgl6qcaiq5riqw2zfy9wwqz77r9mx44d6vlfj7ar")))

(define-public crate-markdown-meta-parser-0.1.3 (c (n "markdown-meta-parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1z2kh1m04c9wh9b7ncskkzivid92w9ff4s734szwjvkv0nzzh79j")))

