(define-module (crates-io ma rk markdown_wasi) #:use-module (crates-io))

(define-public crate-markdown_wasi-0.1.2 (c (n "markdown_wasi") (v "0.1.2") (d (list (d (n "html2md") (r "^0.2.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (k 0)) (d (n "wasm-bindgen") (r "= 0.2.55") (d #t) (k 0)))) (h "1wc8nn9yn933axawymnlzbrqn1pnk9shzvmaigc4ba7p2f53yb78")))

