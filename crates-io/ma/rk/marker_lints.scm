(define-module (crates-io ma rk marker_lints) #:use-module (crates-io))

(define-public crate-marker_lints-0.0.0-placeholder (c (n "marker_lints") (v "0.0.0-placeholder") (h "1avpbgjrhls8fpf150snic6m12q05wsiz3n4p1692r17c570agwa")))

(define-public crate-marker_lints-0.1.0 (c (n "marker_lints") (v "0.1.0") (d (list (d (n "marker_api") (r "^0.1.0") (d #t) (k 0)))) (h "1vi6zfswymr7b1g0hrv1nylny5cg21idbihmy61afc95ic2ggxmb")))

(define-public crate-marker_lints-0.1.1 (c (n "marker_lints") (v "0.1.1") (d (list (d (n "marker_api") (r "^0.1.1") (d #t) (k 0)))) (h "1i8i3sw9x2wjd889xpgz448vlr5g8gpsxzvd9xhi83ff12ldmwd6")))

(define-public crate-marker_lints-0.2.0 (c (n "marker_lints") (v "0.2.0") (d (list (d (n "marker_api") (r "^0.2.0") (d #t) (k 0)))) (h "17a2v3khzdn8r0iiqv777avv63aly4zwx6szaap77j9qp0331zf5")))

(define-public crate-marker_lints-0.2.1 (c (n "marker_lints") (v "0.2.1") (d (list (d (n "marker_api") (r "^0.2.1") (d #t) (k 0)))) (h "0as6sr76hpqsjjk2h8nmwl9k5jhb6vrv75b9v7pawwdcdcfsyisc")))

(define-public crate-marker_lints-0.3.0 (c (n "marker_lints") (v "0.3.0") (d (list (d (n "marker_api") (r "^0.3.0") (d #t) (k 0)))) (h "098kfb5f4w5mwh7khfxa5q7slhixrh38nm30b3kadi2v9kc5bkg3")))

(define-public crate-marker_lints-0.4.0 (c (n "marker_lints") (v "0.4.0") (d (list (d (n "marker_api") (r "^0.4.0") (d #t) (k 0)))) (h "07aq23zm6jf92gxdqfnqkfx2vqbdqsd6w5b5icm2mwmshfsmai1c")))

(define-public crate-marker_lints-0.4.1 (c (n "marker_lints") (v "0.4.1") (d (list (d (n "marker_api") (r "^0.4.1") (d #t) (k 0)))) (h "1i3ffnlnq8i886qlmvbpzdlnc9f0v08pik4fikw44hwgixq72mbi")))

(define-public crate-marker_lints-0.4.2 (c (n "marker_lints") (v "0.4.2") (d (list (d (n "marker_api") (r "^0.4.2") (d #t) (k 0)))) (h "1ym8l7c6dq0495xsidj6in82p84xhclvxdxs8yl13wfq53prjzdc")))

(define-public crate-marker_lints-0.4.3-rc (c (n "marker_lints") (v "0.4.3-rc") (d (list (d (n "marker_api") (r "^0.4.3-rc") (d #t) (k 0)))) (h "1p8y8v2dc7r0vpn1l91kav6mx3kyv5hxiybyqhpf559lw49cryhj")))

(define-public crate-marker_lints-0.4.3 (c (n "marker_lints") (v "0.4.3") (d (list (d (n "marker_api") (r "^0.4.3") (d #t) (k 0)))) (h "1rsz4b7fwbbjs9203x3xmmyzif3ghmqbcppv6a28kkkayahaavl2")))

(define-public crate-marker_lints-0.5.0 (c (n "marker_lints") (v "0.5.0") (d (list (d (n "marker_api") (r "^0.5.0") (d #t) (k 0)))) (h "06aw06b5080illm1yazqrafvav0vhvxv04sa1i9hqwybp9kz1s7c")))

