(define-module (crates-io ma rk markdown2unicode) #:use-module (crates-io))

(define-public crate-markdown2unicode-0.1.0 (c (n "markdown2unicode") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gbm1zk37aq27v8fxf033fvd6faywzmvw088qrp9bn05kvbmvn19")))

