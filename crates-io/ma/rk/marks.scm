(define-module (crates-io ma rk marks) #:use-module (crates-io))

(define-public crate-marks-0.1.0 (c (n "marks") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0d621zmxw8jzvwbw4n4zk9jfgcjqmi3krd97qvq4hg2nyl4w3mg4")))

