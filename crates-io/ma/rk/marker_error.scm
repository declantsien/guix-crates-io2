(define-module (crates-io ma rk marker_error) #:use-module (crates-io))

(define-public crate-marker_error-0.3.0-dev (c (n "marker_error") (v "0.3.0-dev") (d (list (d (n "miette") (r "^5.9") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0gmhblwl8cw9llnmd3dzfhcyh01d3yrpjw12s4wppxaws9qc8n3b")))

(define-public crate-marker_error-0.3.0 (c (n "marker_error") (v "0.3.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1y3z28lcws6zf829627m0rgwz96ar0gg9czhlikiqsjckp0gqs88")))

(define-public crate-marker_error-0.4.0 (c (n "marker_error") (v "0.4.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0zyjaz0i4jicacmk4ms52498nzpgdm87hv4v4w2laqba8im8wc32")))

(define-public crate-marker_error-0.4.1 (c (n "marker_error") (v "0.4.1") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0y0aky3anmk4kbh6hsi0drlcqryw5c69s8r77ni17ihazfz3bprr")))

(define-public crate-marker_error-0.4.2 (c (n "marker_error") (v "0.4.2") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "067rla1kjy63rpig77lq7mpnnqzl7b5dq5d9mmapfjqia2l0zbq9")))

(define-public crate-marker_error-0.4.3-rc (c (n "marker_error") (v "0.4.3-rc") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1nbxbcz2lk36bc89ayj334iw2zdjmkgp09qchwk74ahvxm0ww4zz")))

(define-public crate-marker_error-0.4.3 (c (n "marker_error") (v "0.4.3") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0dcz3ljh19a30nmr0y2pnhwr6b16sphqbb38wzdjyizhbhq4g38v")))

(define-public crate-marker_error-0.5.0 (c (n "marker_error") (v "0.5.0") (d (list (d (n "miette") (r "^5.10") (f (quote ("fancy-no-backtrace" "fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "12nkaabjmgvxhxqvi8zdbpvzrqc5v33wcap88hlr64y1cjm1kx4m")))

