(define-module (crates-io ma rk markov-flow) #:use-module (crates-io))

(define-public crate-markov-flow-0.1.0 (c (n "markov-flow") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "1x39wkd5n6s07cdsz9r9nv14lm6spwsqsmyvwl1k124ngiba6pxh") (y #t)))

(define-public crate-markov-flow-0.1.1 (c (n "markov-flow") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "19g86g5f2lzvjhl2s0c955y3w9ih9vxrddhd4vcsar3iq2y88bpr") (y #t)))

(define-public crate-markov-flow-0.2.0 (c (n "markov-flow") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "13q5qshc7vb62dn79iyad0zq1i1wxsj0lsvnkalkrcsj15v6g2hk") (y #t)))

(define-public crate-markov-flow-0.3.0 (c (n "markov-flow") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "162h20alfykzjfiqyp2z66m8d6bvd40sa4clmrhh1ksss73sf79m") (y #t)))

(define-public crate-markov-flow-0.3.1 (c (n "markov-flow") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "0ch8d06i94yn5gsb3ya0d2ap3w8af7d240bnrvai4wpjidjf0zgn") (y #t)))

(define-public crate-markov-flow-0.3.2 (c (n "markov-flow") (v "0.3.2") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)))) (h "12w7wb15cc0d4gs0p547ifd275f1ap147cprnfm38biy8yg60843")))

