(define-module (crates-io ma ma mama) #:use-module (crates-io))

(define-public crate-mama-0.1.0 (c (n "mama") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "gregorian") (r "^0.2.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "0ms2z22qldm1wxqq5gr15xidjibi1gr4g000hwas9449rgpcjyi8")))

