(define-module (crates-io ma xp maxplus) #:use-module (crates-io))

(define-public crate-maxplus-0.1.0 (c (n "maxplus") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1cza63dssr06zhicvpmw2yzwplknn6z555n0jab98jfdlhiyj5p3")))

