(define-module (crates-io ma dg madgwick) #:use-module (crates-io))

(define-public crate-madgwick-0.1.0 (c (n "madgwick") (v "0.1.0") (d (list (d (n "m") (r "^0.1.1") (d #t) (k 0)) (d (n "mat") (r "^0.1.0") (d #t) (k 0)))) (h "0m726n9vxfpc02xzs82lavbal4ayx084mrbpbchjld9p49vir0kz")))

(define-public crate-madgwick-0.1.1 (c (n "madgwick") (v "0.1.1") (d (list (d (n "m") (r "^0.1.1") (d #t) (k 0)) (d (n "mat") (r "^0.2.0") (d #t) (k 0)))) (h "1g5ifkrdn0jij6n289g2hj2m70d8ym6v33jp7gzm6c1gfj00lxjj")))

