(define-module (crates-io ma tl matlab-mat) #:use-module (crates-io))

(define-public crate-matlab-mat-0.1.0 (c (n "matlab-mat") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarr") (r "^0.15") (o #t) (d #t) (k 0) (p "ndarray")) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "16jg4hsh7gxpn60az2817743k00sa9syn6v5dilzlzf234zysccl") (f (quote (("ndarray" "ndarr" "num-complex"))))))

