(define-module (crates-io ma tl matlab-sys) #:use-module (crates-io))

(define-public crate-matlab-sys-0.1.0 (c (n "matlab-sys") (v "0.1.0") (h "1h64lpllgh5g3q0lhlh76wgw6dgx9788njp3vlrh7anfzr7fg5ih") (f (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.1.1 (c (n "matlab-sys") (v "0.1.1") (h "160xaymfa76m2wpp559bgp2hkw6lny301arbyznjhmkb935q1xpi") (f (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.2.0 (c (n "matlab-sys") (v "0.2.0") (h "14c7rd20952p01kfqlh3hajzl7cmg5qmngvwqy27m2qzilyzqls3") (f (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib")))) (y #t)))

(define-public crate-matlab-sys-0.2.1 (c (n "matlab-sys") (v "0.2.1") (h "0wjlx5yibdw051dk813sqrcsslqzwc3kxvg9hddnpylxpcwfx0j7") (f (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.3.0 (c (n "matlab-sys") (v "0.3.0") (h "08h2ggwz08kjy2pxckggvwjplrplmlgbkqxiqli60dh6hzfrlj2j") (f (quote (("separate-complex") ("interleaved-complex") ("default" "interleaved-complex")))) (l "matlab")))

(define-public crate-matlab-sys-0.3.1 (c (n "matlab-sys") (v "0.3.1") (h "1h77v0wdyc9wizhf1mrqkgp04qw20jzhw60srqzbp2svadnhm76z") (f (quote (("separate-complex") ("interleaved-complex") ("default" "interleaved-complex")))) (l "matlab")))

