(define-module (crates-io ma cv macvtap) #:use-module (crates-io))

(define-public crate-macvtap-0.1.0 (c (n "macvtap") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0ch99fpkvh913iin13q38gzpwp7xv5mhvnk2wf6g293sfyh613a9")))

(define-public crate-macvtap-0.1.1 (c (n "macvtap") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0jy69466kbkkbgd2vrv8rk789az1chnxivrfk9xyplj5dr9hbxfk")))

(define-public crate-macvtap-0.1.2 (c (n "macvtap") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0zaxvqym1xrw8fs0sbz972ix6wq8qkygqgq44rzxl4bakidzpr2j")))

