(define-module (crates-io ma x4 max485) #:use-module (crates-io))

(define-public crate-max485-0.1.0 (c (n "max485") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "1s48jnvd9drqmm25h0chr0d2wcim913q1z7aa5m7k45vz2fghcav")))

(define-public crate-max485-0.2.0 (c (n "max485") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "0symv96syszhmbspcwj4mjrfh368z3ddlpvx1bdskkv37daf04i9")))

