(define-module (crates-io ma x4 max44009) #:use-module (crates-io))

(define-public crate-max44009-0.1.0 (c (n "max44009") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1icn57sv9s40rd5qdc3mvpj7zbmzrl2slsxmpiw9hh8hjnvdww6m")))

(define-public crate-max44009-0.2.0 (c (n "max44009") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0ljwlv6ralg2cwhmb6h16yv39y8hhl43nr3998s7s8jlmp3rc35x")))

