(define-module (crates-io ma pp mappy_parser) #:use-module (crates-io))

(define-public crate-mappy_parser-1.0.0 (c (n "mappy_parser") (v "1.0.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1z7jb3h5h80ihdfcsm72qriix4vdxrj4vjb07ijl9z148n8kcmhg")))

(define-public crate-mappy_parser-1.0.1 (c (n "mappy_parser") (v "1.0.1") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "139b78hbjyapsgmbyn6dzilc2dizd217s8a728kxylpjrfs0ddx4")))

