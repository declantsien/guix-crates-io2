(define-module (crates-io ma pp mappum-rocksdb) #:use-module (crates-io))

(define-public crate-mappum-rocksdb-0.14.0-alpha.1 (c (n "mappum-rocksdb") (v "0.14.0-alpha.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.7.0-alpha.1") (d #t) (k 0) (p "mappum-librocksdb-sys")))) (h "0nymbkrfiyranm03bzf8ra5q6fx61nvg9brg6hjmml1fa99ac7c3") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

