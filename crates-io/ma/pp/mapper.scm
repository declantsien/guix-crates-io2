(define-module (crates-io ma pp mapper) #:use-module (crates-io))

(define-public crate-mapper-1.0.1 (c (n "mapper") (v "1.0.1") (d (list (d (n "mapper-api") (r "=1.0.1") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.1") (d #t) (k 0)))) (h "0wxgl7xnl2bjlbd2nh4gxxcxgwg4ls6cgmgd4180ra2r1crzr9yq") (r "1.56")))

(define-public crate-mapper-1.0.2 (c (n "mapper") (v "1.0.2") (d (list (d (n "mapper-api") (r "=1.0.1") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.1") (d #t) (k 0)))) (h "0g0li7wlnf0ci04nq29j3h6fn8z6az71asncn9ka84q1lx7p7p2n") (r "1.56")))

(define-public crate-mapper-1.0.3 (c (n "mapper") (v "1.0.3") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.2") (d #t) (k 0)))) (h "0xjwy4i6vj9ypnzvaxj01amdf0v2hyljc6lvpl47nxajharzlgcf") (r "1.56")))

(define-public crate-mapper-1.0.4 (c (n "mapper") (v "1.0.4") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.2") (d #t) (k 0)))) (h "0b950a08jfy6frw1nj75vwnk23x2lcvgy4b4x8fa259ygn9whff2") (r "1.56")))

(define-public crate-mapper-1.0.5 (c (n "mapper") (v "1.0.5") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.2") (d #t) (k 0)))) (h "0dm8fjpga7v8v69ar3m1mndfvspvmfl7bf39yq52q25kzprrrp95") (r "1.56")))

(define-public crate-mapper-1.0.6 (c (n "mapper") (v "1.0.6") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.2") (d #t) (k 0)))) (h "1x0dkys5yl7z4vg3a7fzryg8vvm3df5ldiw9vpwlv32ws0p3dbh3") (r "1.56")))

(define-public crate-mapper-1.0.7 (c (n "mapper") (v "1.0.7") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.0.3") (d #t) (k 0)))) (h "1b1607yx5pcdbzn6zw4ia1cl8zjhzb9hzsabd2g6n4wrcxw74xfn") (r "1.56")))

(define-public crate-mapper-1.1.0 (c (n "mapper") (v "1.1.0") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.72") (f (quote ("diff"))) (d #t) (k 2)))) (h "1phj8y9sbsfhq244fs59mngxp59n8h9l7d80gl0zj567fv9jrkwb") (r "1.56")))

(define-public crate-mapper-1.1.1 (c (n "mapper") (v "1.1.1") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.72") (f (quote ("diff"))) (d #t) (k 2)))) (h "1p6kb5kd99pz5kmafhkc49ywpcfn3f41c3dpjpjl6smkq5crfgla") (r "1.56")))

(define-public crate-mapper-1.1.2 (c (n "mapper") (v "1.1.2") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.72") (f (quote ("diff"))) (d #t) (k 2)))) (h "1p64zkq0fzhbghia3n6v06pr4p0k8dqb39fwc1sq4bp8255gdngi") (r "1.56")))

(define-public crate-mapper-1.1.3 (c (n "mapper") (v "1.1.3") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.72") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zqrs6bcj2s5c7akp5wzv51z0cd02sr5qlgjgand7q1av2hy2rfj") (r "1.56")))

(define-public crate-mapper-1.1.4 (c (n "mapper") (v "1.1.4") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "mapper-impl") (r "=1.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.72") (f (quote ("diff"))) (d #t) (k 2)))) (h "1k040dqkka39ay7pi8v8sgz5vb67jnrvwpr0ddn39aslpq4323hn") (r "1.56")))

