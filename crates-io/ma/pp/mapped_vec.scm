(define-module (crates-io ma pp mapped_vec) #:use-module (crates-io))

(define-public crate-mapped_vec-0.1.0 (c (n "mapped_vec") (v "0.1.0") (h "1wxyy9g1736br3k6pswi67ff05i9in3sf8a66ykp0zfz14ih6n5b") (y #t)))

(define-public crate-mapped_vec-0.1.1 (c (n "mapped_vec") (v "0.1.1") (h "0ll3wg8mrp0kc7498l9gzbxwzjrf8dzzqgk2jv7qmbglf2qvn6fh")))

