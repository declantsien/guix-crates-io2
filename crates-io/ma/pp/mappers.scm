(define-module (crates-io ma pp mappers) #:use-module (crates-io))

(define-public crate-mappers-0.0.1 (c (n "mappers") (v "0.0.1") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cnbnhh92zbqlv0jqknzd1dr8kqs4i97mn3jsqy44b7hgq7jk4qq") (y #t)))

(define-public crate-mappers-0.0.2 (c (n "mappers") (v "0.0.2") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gah5ybjai46hy8dqz9k8drbgn9kg8qnv6priz5j7mgqx3vmk68r") (y #t)))

(define-public crate-mappers-0.0.3 (c (n "mappers") (v "0.0.3") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rmvwp854jb4m2yh4gb29h2f1ak7r4qj5nxg86mpc31bwa7vdrh4") (y #t)))

(define-public crate-mappers-0.1.0 (c (n "mappers") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.1") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "09wl13cfk6d30pra4s3m93hnrv91wxfpya87v3qxybz6r7jb4g4a")))

(define-public crate-mappers-0.1.1 (c (n "mappers") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.2") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "11ljxhv72vk0llf79frbihwd7v12lz2398dwgh6170w8qa8kv7vh")))

(define-public crate-mappers-0.2.1 (c (n "mappers") (v "0.2.1") (d (list (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.2") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "02nfjv6xj9l9ij03dvmaqlkgwy95kqpjihkwh0adyg8b16cmbbrh")))

(define-public crate-mappers-0.3.1 (c (n "mappers") (v "0.3.1") (d (list (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.2") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0zj2qbk0j9vbiyvz4rb7p6wclqgh1f1f641il0ash7m7z6h9xk2c")))

(define-public crate-mappers-0.3.2 (c (n "mappers") (v "0.3.2") (d (list (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.2") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "08wh0x7bnydq54kly25fpycwp1ypm430r9vwin7w6pgywn5frddb")))

(define-public crate-mappers-0.4.2 (c (n "mappers") (v "0.4.2") (d (list (d (n "const_soft_float") (r "^0.1.2") (k 0)) (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.3") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1zl7455d2va6g22n6za6h39frnvg6fzvn3lskd001p93mm2qfl92")))

(define-public crate-mappers-0.5.1 (c (n "mappers") (v "0.5.1") (d (list (d (n "const_soft_float") (r "^0.1.2") (k 0)) (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.3") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "00bvix6mqrj0bbvaxgil6zcnxdy43ck1g0rlrg2h4dvj75qyg1z7")))

(define-public crate-mappers-0.5.2 (c (n "mappers") (v "0.5.2") (d (list (d (n "const_soft_float") (r "^0.1.2") (k 0)) (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.3") (k 0)) (d (n "proj") (r "^0.27") (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1m3j4jh7y078hr1idkv5pw2v47a71b89d9560d23w20gm137wznz")))

(define-public crate-mappers-0.6.0 (c (n "mappers") (v "0.6.0") (d (list (d (n "const_soft_float") (r "^0.1.2") (k 0)) (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.3") (k 0)) (d (n "proj") (r "^0.27") (f (quote ("pkg_config"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0wz4wabir4arp0avin30s6m718y3cmpl3mjp3vb059d2cfyvhm60") (f (quote (("multithreading" "rayon") ("default" "multithreading"))))))

(define-public crate-mappers-0.6.1 (c (n "mappers") (v "0.6.1") (d (list (d (n "const_soft_float") (r "^0.1.2") (k 0)) (d (n "dyn-clone") (r "^1.0.10") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.3") (k 0)) (d (n "proj") (r "^0.27") (f (quote ("pkg_config"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1dlly68h2mnvynwkd143xprh2wmsv813cy8mirljrk0nlz1vc68w") (f (quote (("multithreading" "rayon") ("default" "multithreading"))))))

(define-public crate-mappers-0.7.0 (c (n "mappers") (v "0.7.0") (d (list (d (n "const_soft_float") (r "^0.1.4") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.4") (k 0)) (d (n "proj") (r "^0.27") (f (quote ("pkg_config"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0w7wsfjnsgbsc7nym69kpwhvmqh1vwk09q99savxvmg15v7nl1r3")))

(define-public crate-mappers-0.7.1 (c (n "mappers") (v "0.7.1") (d (list (d (n "const_soft_float") (r "^0.1.4") (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 2)) (d (n "geographiclib-rs") (r "^0.2.4") (k 0)) (d (n "proj") (r "^0.27") (f (quote ("pkg_config"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "117k80nid4amzs9cklnv3gdvx8h59lmik9fw3mfdxgbbb8czmb8a")))

