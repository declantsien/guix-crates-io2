(define-module (crates-io ma pp mapped-command) #:use-module (crates-io))

(define-public crate-mapped-command-0.3.0 (c (n "mapped-command") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "028nyb5xwax5dzzc3l41900wfqbi0fa8w861sb7n4j75qfds3aps") (f (quote (("default"))))))

