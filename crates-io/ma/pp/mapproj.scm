(define-module (crates-io ma pp mapproj) #:use-module (crates-io))

(define-public crate-mapproj-0.1.0 (c (n "mapproj") (v "0.1.0") (h "1wasxngxr5iays4b0p1j8k30scdhq7kpg491zd0jypzln30631kc")))

(define-public crate-mapproj-0.2.0 (c (n "mapproj") (v "0.2.0") (h "0jarrsgwzavv8za7jwh5d2hwqafhba73r0fcc0s3fzmdnlv3zc4z")))

(define-public crate-mapproj-0.3.0 (c (n "mapproj") (v "0.3.0") (h "08wjp6vj1w2qbz769k057pdk2w8351i26xj40p8xbxjj3wli7iz3")))

