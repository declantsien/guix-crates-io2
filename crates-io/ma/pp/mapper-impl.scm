(define-module (crates-io ma pp mapper-impl) #:use-module (crates-io))

(define-public crate-mapper-impl-1.0.1 (c (n "mapper-impl") (v "1.0.1") (d (list (d (n "mapper-api") (r "=1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "09wkmwhz1jmrfmkhyd7y08z3csn996s8d0wnq6qwxn9fryrpr7n8") (r "1.56")))

(define-public crate-mapper-impl-1.0.2 (c (n "mapper-impl") (v "1.0.2") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "166k743pllj08vwpv2myxb32fw2qgyrdygjpg7qwzchjh0npiscz") (r "1.56")))

(define-public crate-mapper-impl-1.0.3 (c (n "mapper-impl") (v "1.0.3") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0g5g4w9bjl8zp705yq8dbbc26j2parfmiw7zbpxdcrs5ma58sznw") (r "1.56")))

(define-public crate-mapper-impl-1.1.0 (c (n "mapper-impl") (v "1.1.0") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0jvwhxn98zzccizi3kmh6zlg1yl6m5vzinqfsr643a9vy2czkanb") (r "1.56")))

(define-public crate-mapper-impl-1.1.1 (c (n "mapper-impl") (v "1.1.1") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1aplpriqi3vzkh9rd3wk4m23flxq33fkgpwcv1b3kn9na99s0pxp") (r "1.56")))

(define-public crate-mapper-impl-1.1.2 (c (n "mapper-impl") (v "1.1.2") (d (list (d (n "mapper-api") (r "=1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1kiz62x1k8yd4cj2jc9kww10zgb1fdvhahyw3ll25i1grgbv45hk") (r "1.56")))

