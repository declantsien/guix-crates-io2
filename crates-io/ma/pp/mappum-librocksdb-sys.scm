(define-module (crates-io ma pp mappum-librocksdb-sys) #:use-module (crates-io))

(define-public crate-mappum-librocksdb-sys-6.7.0-alpha.1 (c (n "mappum-librocksdb-sys") (v "6.7.0-alpha.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "const-cstr") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "1awk7vqj1ygw09nx818bdgqc6la1gnnszdfqjgqy4rrhygkfkmf7") (f (quote (("zstd") ("zlib") ("static") ("snappy") ("lz4") ("default" "static") ("bzip2")))) (l "rocksdb")))

