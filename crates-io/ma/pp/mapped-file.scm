(define-module (crates-io ma pp mapped-file) #:use-module (crates-io))

(define-public crate-mapped-file-0.0.1 (c (n "mapped-file") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "0pxm81x4qjhpkdhammkfg9zpk05i1x89g8f0cby06cl7v3rds20g") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.2 (c (n "mapped-file") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "1nqiq6j7zfbr8xayl7w4mja11b2ddsm936xcngb3wfj3srw8s8mh") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.3 (c (n "mapped-file") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "0nwgqdzna0pybgim0rmwgs1rfx1pbn0flj8fmsb504a1xhv58si9") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.4 (c (n "mapped-file") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "0i4n9nybfl1jbjjs31l88cf7xy955nprj7mskcfyimglz1wms9wh") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.5 (c (n "mapped-file") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "04hzbcwryssmgv6mgz0gcjqqfr96k0aarc2zp7af69z83alng8c9") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.6 (c (n "mapped-file") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "0dlivjm8hz9ah0r4f1hhhzabahfjha5ncxw5xhzlq95nqw811fc9") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.7 (c (n "mapped-file") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "0l1ammcxh2g3wivapgwm1ran22j78a7j0950hfw731k65ycqzlmk") (f (quote (("file"))))))

(define-public crate-mapped-file-0.0.8 (c (n "mapped-file") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "1i3wqk8y6fq3sdp6lg3cy594gkj1d1xak0qhf8zwrnms301801cr") (f (quote (("file"))))))

