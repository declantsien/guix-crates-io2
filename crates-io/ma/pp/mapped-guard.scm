(define-module (crates-io ma pp mapped-guard) #:use-module (crates-io))

(define-public crate-mapped-guard-0.0.1 (c (n "mapped-guard") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1kyy1242qm677p1firjd10k6lakrd0mpg02db7f55vgldm24xf09")))

