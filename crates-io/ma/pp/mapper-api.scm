(define-module (crates-io ma pp mapper-api) #:use-module (crates-io))

(define-public crate-mapper-api-1.0.1 (c (n "mapper-api") (v "1.0.1") (h "0v2lplvdg1cn0nhs7jzqw561a7c6737y0xiwdw9wy4ag6x5jlxji") (r "1.56")))

(define-public crate-mapper-api-1.0.2 (c (n "mapper-api") (v "1.0.2") (h "114dsnn8rws8q6vmr70l2npvl0h40csjdb98l6hbpqvbixvkczzj") (r "1.56")))

