(define-module (crates-io ma pp mappable-rc) #:use-module (crates-io))

(define-public crate-mappable-rc-0.1.0 (c (n "mappable-rc") (v "0.1.0") (h "1ickgvzfq2qdyxlcmyzgzapmzmbx0afpqx2pr2iy3gsg5d37ypmn") (f (quote (("std") ("default"))))))

(define-public crate-mappable-rc-0.1.1 (c (n "mappable-rc") (v "0.1.1") (h "15mr9pyqqyn58bjvd9n322r0finr5hvjrffj50hpnsha3grm2ii0") (f (quote (("std") ("default"))))))

