(define-module (crates-io ma ko makods) #:use-module (crates-io))

(define-public crate-makods-0.2.0 (c (n "makods") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "0f26xbm5hr1crzksajgncfih0zvjr9iq7wx1z4p9il15y4vw4cia")))

(define-public crate-makods-0.2.1 (c (n "makods") (v "0.2.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "05qg13w1i5ydb9hlilwqgykw8k7311dz1h2bizxcg44kngmvyfph")))

(define-public crate-makods-0.3.0 (c (n "makods") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "05rhjwfkmq12gafl2vxbpxnwnafzy1g9qvn5cjq2dw8njm3mdp1c")))

(define-public crate-makods-0.4.0 (c (n "makods") (v "0.4.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 2)))) (h "0i293yh5wy9836mp7868vycljgjmhizpb8pdj62asmqq1fsvgy7j")))

(define-public crate-makods-0.4.1 (c (n "makods") (v "0.4.1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 2)))) (h "0kqs79wlq7wbczxb6yxic4axh9ri1339cm637x40jz45i10dx40v")))

