(define-module (crates-io ma ko mako_infinite_shuffle) #:use-module (crates-io))

(define-public crate-mako_infinite_shuffle-0.1.0 (c (n "mako_infinite_shuffle") (v "0.1.0") (h "1941slvhz3n9d16b6f0cipv5m3vf70jgb5xgd4031hxwqp5sqs8j")))

(define-public crate-mako_infinite_shuffle-0.2.0 (c (n "mako_infinite_shuffle") (v "0.2.0") (h "11z16c429sb7pa2q7hv1bam6fznjm2147q1pypq3qjscrd3sywqp")))

(define-public crate-mako_infinite_shuffle-0.2.1 (c (n "mako_infinite_shuffle") (v "0.2.1") (h "14mwna758amf0p45f0iz7ym81ipdhzxwqk0rcx392pi0f68smmvb")))

(define-public crate-mako_infinite_shuffle-0.2.2 (c (n "mako_infinite_shuffle") (v "0.2.2") (h "0zpfk9h6dqqhkbn9zrdm0rv887nspdgjx57jv1ifi0c8nkszvrnf")))

(define-public crate-mako_infinite_shuffle-0.2.3 (c (n "mako_infinite_shuffle") (v "0.2.3") (h "1ynhjynpwnpc3gfpvp0655yr5ss05f6z0d8ab84h8qzfnn8dzd23")))

(define-public crate-mako_infinite_shuffle-0.3.0 (c (n "mako_infinite_shuffle") (v "0.3.0") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "1aq7g3fsnjf5kw67rxh13m9z00d96l9njrfxs5p3cnz2rbfcfzh8")))

(define-public crate-mako_infinite_shuffle-0.3.1 (c (n "mako_infinite_shuffle") (v "0.3.1") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "1v40z9cms1wzcyrrhh1cmk8psij2lia9lb6rhg2knnjn0w73rsdr")))

(define-public crate-mako_infinite_shuffle-0.3.2 (c (n "mako_infinite_shuffle") (v "0.3.2") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "0y1fhg5d3b6gxgqahczrbdraby9dvhdg4l06brq6d0is44m8wr3z")))

(define-public crate-mako_infinite_shuffle-0.3.3 (c (n "mako_infinite_shuffle") (v "0.3.3") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "06cmzaclxvapgf66w4wq9vg9jvvcs0gm3zj9awkgvww1r6ns2asc")))

(define-public crate-mako_infinite_shuffle-0.3.4 (c (n "mako_infinite_shuffle") (v "0.3.4") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "1zjh9sx37ah0787xmfk3gji90kgsf5bxfqd6vdrya8v56k1s1nwf")))

(define-public crate-mako_infinite_shuffle-0.3.5 (c (n "mako_infinite_shuffle") (v "0.3.5") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "0j30qy2br81hz4y484wmil6lascq2pdldiv723m13db1ypl51faa")))

(define-public crate-mako_infinite_shuffle-0.3.6 (c (n "mako_infinite_shuffle") (v "0.3.6") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "07mkv9dyqqnrjrz1wlmp1sss12f5blhzl0js7yrlhlg4q9q4dnxk")))

(define-public crate-mako_infinite_shuffle-0.3.7 (c (n "mako_infinite_shuffle") (v "0.3.7") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "1yrn5sbbnj54rb7qafalw8afyvfzb30wmkzh3z35jkrl8s422dl0")))

(define-public crate-mako_infinite_shuffle-0.3.8 (c (n "mako_infinite_shuffle") (v "0.3.8") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "1d4ipk2hmy5nfv4l8jhjslrni2kwfa7jqhss4226ynhry81nd62p")))

(define-public crate-mako_infinite_shuffle-0.4.0 (c (n "mako_infinite_shuffle") (v "0.4.0") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "0lfyi8s42k69s5hxqmvbczx4jkd1cm6i14a4sn30a4237am1lm83")))

(define-public crate-mako_infinite_shuffle-0.4.1 (c (n "mako_infinite_shuffle") (v "0.4.1") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "15grjk30r11090abrk34rszf1m9lvyimx9k3dck7sjygcv3ig96x")))

(define-public crate-mako_infinite_shuffle-0.4.2 (c (n "mako_infinite_shuffle") (v "0.4.2") (d (list (d (n "number-encoding") (r "^0.2.1") (d #t) (k 0)))) (h "139q6c7z0rk3hg9ax8kd4m9qv3bcd71h7f8xkl46zlxn5xgczkk9")))

