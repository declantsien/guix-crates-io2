(define-module (crates-io ma fs mafs) #:use-module (crates-io))

(define-public crate-mafs-0.1.0 (c (n "mafs") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.11") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1cxgva0qa4ynbg14ag57rd4zx3qwv5irzk2f33vfzzimwgghbj16") (s 2) (e (quote (("bytemuck" "dep:bytemuck"))))))

