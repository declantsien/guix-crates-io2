(define-module (crates-io ma x_ max_rects) #:use-module (crates-io))

(define-public crate-max_rects-1.0.0 (c (n "max_rects") (v "1.0.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0w8lcdybcysx5hxpsay1g15rr1bk9cdhph4vwqx503l0yi16f007")))

(define-public crate-max_rects-1.0.1 (c (n "max_rects") (v "1.0.1") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1zia0jppyl6vw66nys40qijvikpalmzs5rkdza4brs9avxrsljyw")))

(define-public crate-max_rects-1.0.2 (c (n "max_rects") (v "1.0.2") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1bc2vbfihg1js5rw74x9baclmkvxwzp9j430hr6l6x20sagxz2q1")))

