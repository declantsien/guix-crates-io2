(define-module (crates-io ma x_ max_rss) #:use-module (crates-io))

(define-public crate-max_rss-0.1.0 (c (n "max_rss") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1dakyhq15jhj5xb30hfk6lbhbs78wpflll7pshk20ymifbn6hm68")))

(define-public crate-max_rss-0.2.0 (c (n "max_rss") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1j59qkcc33sbi2nlgsmx91jikbhgkp3046yrzj4vckzqj4vdcb0b") (y #t)))

(define-public crate-max_rss-0.2.1 (c (n "max_rss") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01dgcrsmgsxflqrqps3pw1ps7zhvs80wl1fz2yqk2gsfvxp0kz48")))

(define-public crate-max_rss-0.3.0 (c (n "max_rss") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0hh419i1ap829aql3sxivglr311fsimnj96qv0nni3mpdzv94mpm")))

(define-public crate-max_rss-0.3.1 (c (n "max_rss") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "081kv6r20jfn6p7x4gvpr8blfkw1ys42knlndiw3xvs384wxm38n")))

(define-public crate-max_rss-0.3.2 (c (n "max_rss") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1r4gv1wm4lhxv4n8phw8bi4wkjgx96k5mqxaa7za68j12wjxvblx")))

(define-public crate-max_rss-0.3.3 (c (n "max_rss") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1q011lgix8nbxfr0szsnfd2z5dmk75p92n2ijwxms45yb2nsijsr")))

(define-public crate-max_rss-0.4.0 (c (n "max_rss") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "07481i4z51wnkdqvq6iigg4jd2yhxgbcimzkdf46g9bcrydgz75f")))

(define-public crate-max_rss-0.4.1 (c (n "max_rss") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ptrace" "signal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0m988r5gxishy7gljm5s0g2gqspzx3nx1kyv76jbpb4brjprabah")))

