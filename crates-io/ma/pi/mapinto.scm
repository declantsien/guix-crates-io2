(define-module (crates-io ma pi mapinto) #:use-module (crates-io))

(define-public crate-mapinto-0.1.0 (c (n "mapinto") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "104m6ppgnclxrpfj49yppyasc2mlq871ac215fahzi17bvrlfmw7")))

(define-public crate-mapinto-0.2.0 (c (n "mapinto") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "1hqqv87phjrzwnh0w318jiad6nkmi1idgqshsrwxw9czlz2jchzs")))

(define-public crate-mapinto-0.2.1 (c (n "mapinto") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)))) (h "0di12pm3wkj59bx2x6p92gvhy9j9v7sa53jan6xnpbz25yggkzl3")))

