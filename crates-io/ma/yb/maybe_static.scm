(define-module (crates-io ma yb maybe_static) #:use-module (crates-io))

(define-public crate-maybe_static-0.1.0 (c (n "maybe_static") (v "0.1.0") (h "017bb4ljg0lgnhn9qdjxyykka0gazmnr7vh9f9jw8xprs41axayv")))

(define-public crate-maybe_static-0.1.1 (c (n "maybe_static") (v "0.1.1") (h "13vnyy1yzr31skagga44s13byln0s4j104qr2r2fg3ckipma8r6z")))

(define-public crate-maybe_static-0.1.2 (c (n "maybe_static") (v "0.1.2") (h "0n2la4wipj2ipgc4wx39sn9jhi6dc2i3vdz5amcdgrn8a0lhl59z")))

(define-public crate-maybe_static-0.1.3 (c (n "maybe_static") (v "0.1.3") (h "13y522dnkcbcffqlaqxqd02hndlb0fx9camfsy6mlxdkddf0jld8")))

