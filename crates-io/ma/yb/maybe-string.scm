(define-module (crates-io ma yb maybe-string) #:use-module (crates-io))

(define-public crate-maybe-string-0.1.0 (c (n "maybe-string") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)))) (h "1hazg7v1ihvsw9ldpa9cr9n1p01r2nmxbhm7v6jwxrn4z6llxr87")))

