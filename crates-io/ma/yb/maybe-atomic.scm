(define-module (crates-io ma yb maybe-atomic) #:use-module (crates-io))

(define-public crate-maybe-atomic-0.1.0 (c (n "maybe-atomic") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)))) (h "02j53abxg2mmz1xpnrxaxsyd3gfn4ss33f91sbd8wmsgvxcpz2hb") (f (quote (("default" "atomic") ("atomic"))))))

