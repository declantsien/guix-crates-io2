(define-module (crates-io ma yb maybe-std) #:use-module (crates-io))

(define-public crate-maybe-std-0.1.0 (c (n "maybe-std") (v "0.1.0") (h "0s7sy8srhiwwzbz2wkqhcbb0wc08dvy3d5g2f2fnq4vmfx19mx2w") (f (quote (("unstable") ("std") ("default") ("alloc"))))))

(define-public crate-maybe-std-0.1.1 (c (n "maybe-std") (v "0.1.1") (h "1sxz1r47dvhx13r3001mqma02726ksg1w0id4sh6k639icshwb0d") (f (quote (("unstable") ("std") ("default") ("alloc"))))))

(define-public crate-maybe-std-0.1.2 (c (n "maybe-std") (v "0.1.2") (h "1pnxhd06jnc6nc53adr7pvcqnms8rn9l1gw6qzai2l5hjwcyqn29") (f (quote (("unstable") ("std") ("default") ("alloc"))))))

