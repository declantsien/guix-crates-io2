(define-module (crates-io ma yb maybenot-simulator) #:use-module (crates-io))

(define-public crate-maybenot-simulator-0.0.0 (c (n "maybenot-simulator") (v "0.0.0") (h "03wdjx0r54gsz8xwz4ram1vmbj60xm3fi28639hcbi5a25aanjwh")))

(define-public crate-maybenot-simulator-1.0.1 (c (n "maybenot-simulator") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maybenot") (r "^1.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 0)))) (h "1a9v2xibrc7xbpry2nn37r168lmg2xy57v39a4kxv6gy9p93a1z9")))

(define-public crate-maybenot-simulator-1.1.0 (c (n "maybenot-simulator") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maybenot") (r "^1.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 0)))) (h "1j5ghb6s69b1mc8m10pc32mq49lvv71qdpsjfw3i29bj4gcjwj2m")))

(define-public crate-maybenot-simulator-1.1.1 (c (n "maybenot-simulator") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maybenot") (r "^1.1.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 0)))) (h "1785sbca6g5y76xyd41zrmcgk92iv8hs96k55pky4mfpfb9dhk3b")))

