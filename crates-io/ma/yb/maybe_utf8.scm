(define-module (crates-io ma yb maybe_utf8) #:use-module (crates-io))

(define-public crate-maybe_utf8-0.1.0 (c (n "maybe_utf8") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.18") (d #t) (k 2)))) (h "0ppkw02d7mldl0zj00gsan1sv8dar1f4nqmwwhsqds95b9n42q7i")))

(define-public crate-maybe_utf8-0.1.1 (c (n "maybe_utf8") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.18") (d #t) (k 2)))) (h "1php1dbnf6370bg9k12ymd3rlgwrw1jg1y4g1sfhkz0rbgb9x3df")))

(define-public crate-maybe_utf8-0.1.2 (c (n "maybe_utf8") (v "0.1.2") (d (list (d (n "encoding") (r "^0.2.20") (d #t) (k 2)))) (h "1ql4aqrngxgr2vdxcwn41v22wb8l5s8h60yjwfq8q0qimwbkwc18")))

(define-public crate-maybe_utf8-0.1.3 (c (n "maybe_utf8") (v "0.1.3") (d (list (d (n "encoding") (r "^0.2.21") (d #t) (k 2)))) (h "00bsad0q1zfcvdprl8g4jzyil8jd49wakw9l5nlw9vdl02dra5bm")))

(define-public crate-maybe_utf8-0.2.0 (c (n "maybe_utf8") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.21") (d #t) (k 2)))) (h "0ccrviyjwn53vcpdh32llmvbbrb7q7mg5j7wjw9cklas06b84m8v")))

(define-public crate-maybe_utf8-0.2.1 (c (n "maybe_utf8") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2.21") (d #t) (k 2)))) (h "05qw196zcy7gwlgq0mc296yv5z1wkxz3mnv2b34y642dszghww59")))

(define-public crate-maybe_utf8-0.2.2 (c (n "maybe_utf8") (v "0.2.2") (d (list (d (n "encoding") (r "^0.2.21") (d #t) (k 2)))) (h "0ihr7v0sbsq7d06i6ag8k10m3mw78dmwnpnxlgbxq7sbr4wjh6j4")))

(define-public crate-maybe_utf8-0.2.3 (c (n "maybe_utf8") (v "0.2.3") (d (list (d (n "encoding") (r "^0.2.24") (d #t) (k 2)))) (h "0m1g0nx687jqxxdyhc3kb8pb89yfhrc7hhlz27bvv1jy94ml2mx6")))

