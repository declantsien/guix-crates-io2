(define-module (crates-io ma yb maybe_parallel_iterator) #:use-module (crates-io))

(define-public crate-maybe_parallel_iterator-0.1.0 (c (n "maybe_parallel_iterator") (v "0.1.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "07ak625w6c9z7fg7mng74m764jwianypk8gdwbl9g5bswcry5080")))

(define-public crate-maybe_parallel_iterator-0.2.0 (c (n "maybe_parallel_iterator") (v "0.2.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1xggvh5hr2piri1qsczxh97p6g8pss36h12zvb0mrd99v00d0sih") (y #t)))

(define-public crate-maybe_parallel_iterator-0.3.0 (c (n "maybe_parallel_iterator") (v "0.3.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1b3ba63lfpzkg2j35d9wr6wqc07y609s19rakxjf6fa1n3zf1y2d")))

(define-public crate-maybe_parallel_iterator-0.4.0 (c (n "maybe_parallel_iterator") (v "0.4.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1lvz2adrc343cbbgbb5wmqa74zs2rdcjbdqqfc3ld65b2hb7jdjl")))

(define-public crate-maybe_parallel_iterator-0.5.0 (c (n "maybe_parallel_iterator") (v "0.5.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0z4f68pz9ngf420mx0cqcq3xilfzk0jy6c12v8lnii4s365v0448")))

(define-public crate-maybe_parallel_iterator-0.6.0 (c (n "maybe_parallel_iterator") (v "0.6.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "08xv95fzi7f11z0klqgz8qin1ca31w6023rcn78jcsm92sxdn9jm")))

(define-public crate-maybe_parallel_iterator-0.7.0 (c (n "maybe_parallel_iterator") (v "0.7.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1gmkgh6fi0war7vlps4axaw9m95sarp189i342b1mknflp7gcbd7")))

(define-public crate-maybe_parallel_iterator-0.8.0 (c (n "maybe_parallel_iterator") (v "0.8.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "05qggf5q4f7sbkbchg5zzxh3rl9byfv93vn96kkdvwd072wx632r")))

(define-public crate-maybe_parallel_iterator-0.9.0 (c (n "maybe_parallel_iterator") (v "0.9.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "02kaqk2bc08bwzgiddr3pq30cxkky157zfayik4ndp2mjcdliqnj")))

(define-public crate-maybe_parallel_iterator-0.10.0 (c (n "maybe_parallel_iterator") (v "0.10.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0kxj64jnkbdfmxv4pkymlb7c4jfqs2w771dnir7q6p82zx9xbmap")))

(define-public crate-maybe_parallel_iterator-0.11.0 (c (n "maybe_parallel_iterator") (v "0.11.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1gkmmi5n5a3kzxcd9znj5ikqjch9yzj00v9778jx5wy0ycsxlcp0")))

(define-public crate-maybe_parallel_iterator-0.12.0 (c (n "maybe_parallel_iterator") (v "0.12.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0xzl1sfb0dmyxp4kxzy89fbygm6mx9zbs8w673cs4aqxslcv4sah")))

