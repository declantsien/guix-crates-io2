(define-module (crates-io ma yb maybe-dangling) #:use-module (crates-io))

(define-public crate-maybe-dangling-0.0.0 (c (n "maybe-dangling") (v "0.0.0") (h "0k4ag3h91l93gnijd5vidcbi2qmbpmy3ydckl3hfj30qih23pa55")))

(define-public crate-maybe-dangling-0.1.0-rc1 (c (n "maybe-dangling") (v "0.1.0-rc1") (h "0ddmwxixh7ha49j8nj4pmpd9avdi2zcnav2f9p6kv4blv2wl3xdh") (f (quote (("ui-tests" "better-docs") ("nightly-dropck_eyepatch") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.65.0")))

(define-public crate-maybe-dangling-0.1.0 (c (n "maybe-dangling") (v "0.1.0") (h "08z6wz8i829jfi9j3ch8x0bc6c28j9kqg8rpwgb894ak0h9ymnbw") (f (quote (("ui-tests" "better-docs") ("nightly-dropck_eyepatch") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.65.0")))

(define-public crate-maybe-dangling-0.1.1-rc1 (c (n "maybe-dangling") (v "0.1.1-rc1") (h "0fmi2nj8f799napdz41aw5a037ymdia5gmpdybijiq6x8axp23jk") (f (quote (("ui-tests" "better-docs") ("nightly-dropck_eyepatch") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.65.0")))

(define-public crate-maybe-dangling-0.1.1 (c (n "maybe-dangling") (v "0.1.1") (h "17n18mqjx60ac2pa9rn062kl8zx9492cbpcxkrb5nchwhyr03kjf") (f (quote (("ui-tests" "better-docs") ("nightly-dropck_eyepatch") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.65.0")))

