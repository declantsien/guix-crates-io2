(define-module (crates-io ma yb maybe-impl) #:use-module (crates-io))

(define-public crate-maybe-impl-0.1.0 (c (n "maybe-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1b95xn3a3br3ams3y2gcs969y2z19v0ml8acb31dfvr5hmwmil7f")))

