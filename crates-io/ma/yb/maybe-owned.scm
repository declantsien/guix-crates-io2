(define-module (crates-io ma yb maybe-owned) #:use-module (crates-io))

(define-public crate-maybe-owned-0.2.0 (c (n "maybe-owned") (v "0.2.0") (h "10915dh0903fbyx53j5fjw9lqjmiq708mj3cm1j8xpfz7dswwjp5")))

(define-public crate-maybe-owned-0.3.0 (c (n "maybe-owned") (v "0.3.0") (h "0287hr460qhp8kjgn96qksg45xa5yjj5a5bdcahimr7bjxicvbyg")))

(define-public crate-maybe-owned-0.3.2 (c (n "maybe-owned") (v "0.3.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1aaz65bkskrb6ms77zwac8yqkp5f4ir2j9rv124cs06w4dfc6sh0")))

(define-public crate-maybe-owned-0.3.3 (c (n "maybe-owned") (v "0.3.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15g7p8c4gcs3p03ig0g0h86dj9h3bkhiwd84cfwbylz1g5kcxilx")))

(define-public crate-maybe-owned-0.3.4 (c (n "maybe-owned") (v "0.3.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d3sqiv59i06k73x6p7mf294zgdfb2qkky127ipfnjj9mr9wgb2g")))

