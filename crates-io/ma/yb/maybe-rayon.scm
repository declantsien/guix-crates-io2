(define-module (crates-io ma yb maybe-rayon) #:use-module (crates-io))

(define-public crate-maybe-rayon-0.1.0 (c (n "maybe-rayon") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xxp5srjzqb3mi6s1ql6r39imaa4nzyjmp88xgzd45169lsvvh2p") (f (quote (("threads" "rayon") ("default" "threads"))))))

(define-public crate-maybe-rayon-0.1.1 (c (n "maybe-rayon") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06cmvhj4n36459g327ng5dnj8d58qs472pv5ahlhm7ynxl6g78cf") (f (quote (("threads" "rayon") ("default" "threads"))))))

