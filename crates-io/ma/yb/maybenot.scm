(define-module (crates-io ma yb maybenot) #:use-module (crates-io))

(define-public crate-maybenot-0.0.0 (c (n "maybenot") (v "0.0.0") (h "1a1bqlb370h48kxc8pzs2hf2lwiihahg0vg0dpwx1jmg77a4j9nx")))

(define-public crate-maybenot-1.0.0 (c (n "maybenot") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "0khbz1rrr8ish4xf0g9gn6j6xz46hy9x2zba5iynf8wk6kg3j0l6")))

(define-public crate-maybenot-1.0.1 (c (n "maybenot") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "0qn93a01w5kgmmxzab17ricj5zwy8h4a9a8hf4j1xyzmwd7ydhkw")))

(define-public crate-maybenot-1.1.0 (c (n "maybenot") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "05kw9y01hmrxf1axgss7sysr0ykg7sbnlylnmvzsfrgwhrz9gvcl")))

(define-public crate-maybenot-1.1.1 (c (n "maybenot") (v "1.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "1b8x5iy2v9np9g8aky4nr7x7918g55z6if7hmdyr606p6ibj1zm7")))

