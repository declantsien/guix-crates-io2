(define-module (crates-io ma yb maybe-serde) #:use-module (crates-io))

(define-public crate-maybe-serde-0.1.0 (c (n "maybe-serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 2)))) (h "17r6vp0zdk4x2gh3rniqkw0s6iy59wsf8agjfq5zi037k81j0z1z")))

(define-public crate-maybe-serde-0.1.1 (c (n "maybe-serde") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 2)))) (h "01icgzrps20rk33g0aycqkwg9j1iaw6fsmm7cpjiwigma1rbk1l3")))

(define-public crate-maybe-serde-0.2.0 (c (n "maybe-serde") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 2)))) (h "1zp6a89lrmsxmbs5xhsfs5b4xzm54yb15zsd4qdadjyz2vwpis2i")))

(define-public crate-maybe-serde-0.2.1 (c (n "maybe-serde") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 2)))) (h "1qiji14di15cg1dijr92z70r3907j50i3l9r5x4ws9hw8znaxa2d")))

