(define-module (crates-io ma yb maybe-future) #:use-module (crates-io))

(define-public crate-maybe-future-1.0.0 (c (n "maybe-future") (v "1.0.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1p7y4andxrw61a2zfqp79n41hi76qasmr4b5hw4f66r29mcwmzj2")))

(define-public crate-maybe-future-1.0.1 (c (n "maybe-future") (v "1.0.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "02j54bgkyrfwqkvc2fqd793vlqgr8mgpxdzn0g6w03pnp0hrfwi5")))

