(define-module (crates-io ma yb maybe-single) #:use-module (crates-io))

(define-public crate-maybe-single-0.1.0 (c (n "maybe-single") (v "0.1.0") (h "1vr3wdg650v0vzaxaryycr16yc8zbmbby39zj0x4xxx7n3i1hljv")))

(define-public crate-maybe-single-0.2.0 (c (n "maybe-single") (v "0.2.0") (h "0mr2zw5wxs3bvdf3l0261yvj5pzlh2i300d1k6f3j1996fassxam")))

(define-public crate-maybe-single-0.3.0 (c (n "maybe-single") (v "0.3.0") (h "1ns0rl9kdhsc4yy1gvb43nl60jr74kbynwfkx8vd8vc6alkvbixg")))

(define-public crate-maybe-single-0.4.0 (c (n "maybe-single") (v "0.4.0") (h "052j8xiygxlz3c435sdlr0fi2nvi32kvgs33bg9wfbp76zs44v9k")))

(define-public crate-maybe-single-0.5.0 (c (n "maybe-single") (v "0.5.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09hznmzqisz0a40qknis532rnk9zxxfp6ji20mrhs2gf9wyn9ncb")))

(define-public crate-maybe-single-0.6.0 (c (n "maybe-single") (v "0.6.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "12qgyfwxp6p2dqs01p74pqgw18djrpy7jjy40vjhi8ayx4fcndmd")))

(define-public crate-maybe-single-0.7.0 (c (n "maybe-single") (v "0.7.0") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17gghj38arfh7lqwnk8y4vk9a0xjyr0m7sss0pkgvds8payyk040")))

(define-public crate-maybe-single-0.8.0 (c (n "maybe-single") (v "0.8.0") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1alwv6lylp3r52v466p6mi0rciyn4x78rfr8apbvjm6xbk736nif")))

(define-public crate-maybe-single-0.8.0-async-alpha.1 (c (n "maybe-single") (v "0.8.0-async-alpha.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "01fijbckm9fsiam500d17hqi7sggxzcd0sy741ynpil9fqf511qh")))

(define-public crate-maybe-single-0.8.0-async-alpha.2 (c (n "maybe-single") (v "0.8.0-async-alpha.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18zg4k35zcn274dglh59mphhpabid2nmb4y9li9j18lpg53h1jsv")))

(define-public crate-maybe-single-0.8.0-async-alpha.3 (c (n "maybe-single") (v "0.8.0-async-alpha.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0f24hmrj14hc8bksf5hx7cms9m7w3idh588276dh6j619dmk401f")))

(define-public crate-maybe-single-0.8.0-async-alpha.4 (c (n "maybe-single") (v "0.8.0-async-alpha.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0sg3p609h1rchp5n6xjlyszmnff0ickbpajm48dwm8jn5q94nrml")))

(define-public crate-maybe-single-0.8.1 (c (n "maybe-single") (v "0.8.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jmckx3dddl4kwl0z915hyzila3gx5iqs3ycm9v2cm03n99qayhw")))

(define-public crate-maybe-single-0.9.0 (c (n "maybe-single") (v "0.9.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0r5v5h3c186wy70ybgppa47cf1pb1b6kzkv0c126lc53ny7yafk6")))

(define-public crate-maybe-single-0.9.1 (c (n "maybe-single") (v "0.9.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "17r9zc8d3a1bqmibwb7q9rsd8lqmpzilz8wxi06f1vpzsh9dysxd")))

(define-public crate-maybe-single-0.9.2 (c (n "maybe-single") (v "0.9.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "146kimx6wkj8xzc250jmylmgyzfbspixqc03w3qkz72822k2nkhd")))

(define-public crate-maybe-single-0.10.0 (c (n "maybe-single") (v "0.10.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0vjbdj67dgclxdmdglkv934z89cdsgpq9ffzcpyv8vr1bjb9wczz") (f (quote (("default") ("async" "futures"))))))

(define-public crate-maybe-single-0.10.1 (c (n "maybe-single") (v "0.10.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0fxr7wxrqkp3g5sngqh2vfq3lwhq914a749fd0z0c5lqa7racx23") (f (quote (("default") ("async" "futures"))))))

(define-public crate-maybe-single-0.11.0 (c (n "maybe-single") (v "0.11.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "01mrks8jyr2nx52i6vh7v8m6km3wli5knmv966lx1m0r4igr2mzx")))

(define-public crate-maybe-single-0.12.0 (c (n "maybe-single") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1klj06hcs9jfi9lglkgwcq354fapaqw11fmqn6rqj79b3sx2vym0")))

(define-public crate-maybe-single-0.13.0 (c (n "maybe-single") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1w6fvz3h41sppd929cxvkbrib6iydf7s00ayk9z5ha0x91n4v8qx")))

(define-public crate-maybe-single-0.13.1 (c (n "maybe-single") (v "0.13.1") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "16r17kf0zb78y6r7q5i4qf7hymhiw22qfbl684xlh5v1r1krxy9a") (f (quote (("tokio_02" "tokio") ("default") ("blocking" "parking_lot"))))))

(define-public crate-maybe-single-0.13.2 (c (n "maybe-single") (v "0.13.2") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1l5zvdzzvy1wxag8ayxsh9cxcs5x431pnwb1p2cc9gb4zw1pdclq") (f (quote (("tokio_02" "tokio") ("default"))))))

(define-public crate-maybe-single-0.14.0 (c (n "maybe-single") (v "0.14.0") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "00sjw9wms4skhqmyqqg4b4ymn6ayxr0wcv18w8m963l29g95msya") (f (quote (("tokio_1" "tokio") ("default"))))))

(define-public crate-maybe-single-0.15.0 (c (n "maybe-single") (v "0.15.0") (d (list (d (n "async-lock") (r "^2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0bply1vsbsp22gqhyw8y1r5bmaqcaiv7wfyz4wvlsf68izrd8j1v") (f (quote (("default") ("async" "async-lock"))))))

(define-public crate-maybe-single-0.15.1 (c (n "maybe-single") (v "0.15.1") (d (list (d (n "async-lock") (r "^2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0821lvcw5gcbrazr5x2bz7z7lj5kadd3cmxrabbliah88lf44xx0") (f (quote (("default") ("async" "async-lock"))))))

(define-public crate-maybe-single-0.16.0 (c (n "maybe-single") (v "0.16.0") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0rz88p03dzrz5z9s2ab4fm172h9d7abrz7gv44fh6s8apwigf6id") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-maybe-single-0.16.1 (c (n "maybe-single") (v "0.16.1") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "02752572w9g3sw6ihqcz16fzaxkgk6574mms2y6n9cfgq4rp7l1y") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

