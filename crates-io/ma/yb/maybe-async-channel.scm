(define-module (crates-io ma yb maybe-async-channel) #:use-module (crates-io))

(define-public crate-maybe-async-channel-1.0.0 (c (n "maybe-async-channel") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)))) (h "0xl98jbrj96bny8ckwjclpg5hsmkig9384r5kbdlzbfwx2n5ww5z")))

