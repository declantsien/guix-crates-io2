(define-module (crates-io ma yb maybe_atomic_refcell) #:use-module (crates-io))

(define-public crate-maybe_atomic_refcell-0.1.0 (c (n "maybe_atomic_refcell") (v "0.1.0") (d (list (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)))) (h "04a5wy60y8dj87051acv567kpwp2a39667ywgk8lxqxf40rksbxi") (y #t)))

(define-public crate-maybe_atomic_refcell-0.2.0 (c (n "maybe_atomic_refcell") (v "0.2.0") (d (list (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)))) (h "0zp5z014v0fbqp8344gyi10bx3681ak7qxjs3r0a0l482icijffv")))

(define-public crate-maybe_atomic_refcell-0.3.0 (c (n "maybe_atomic_refcell") (v "0.3.0") (d (list (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)))) (h "0qp7mj3qmhnn3x09j0lpdin7b023171x6s92mnrrrx25i0ck1wsl") (f (quote (("safe"))))))

(define-public crate-maybe_atomic_refcell-0.3.1 (c (n "maybe_atomic_refcell") (v "0.3.1") (d (list (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)))) (h "0n64dabxylminmzhadq8kw4kvp3n9n698ij3by1wj8183qb5bfwz") (f (quote (("safe"))))))

