(define-module (crates-io ma yb maybe_xml) #:use-module (crates-io))

(define-public crate-maybe_xml-0.1.0 (c (n "maybe_xml") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1w93zz8psxs4brx1yf1c7adfq1hwgicxymvwj2gvg87dd5ii1wbw") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-maybe_xml-0.2.0 (c (n "maybe_xml") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1yl61dzvwr1rz9m8q4ayajk4sf2nvbbm4ygdh73f4mnjaybf9y0v") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-maybe_xml-0.3.0 (c (n "maybe_xml") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08qwxrfnmahbf4z3spfk0pf7lkvahcrbh6sv8qb0qaj4grg20k6j") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-maybe_xml-0.4.0 (c (n "maybe_xml") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lxc4arg69rj25crs2k10hffmjbp7wba914gqy4ff9jzw07zqddh") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.70.0")))

(define-public crate-maybe_xml-0.5.0 (c (n "maybe_xml") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0h30aka1pa496p37hlpkdp14wzihs7v3in4h12zl75h5jippiv5j") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.60.0")))

(define-public crate-maybe_xml-0.6.0 (c (n "maybe_xml") (v "0.6.0") (h "1ql1wpsgzf9nrszvg8znrcgv9aad454jqs2f5ipkfkc9156icdri") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-maybe_xml-0.7.0 (c (n "maybe_xml") (v "0.7.0") (h "0b9iisyqrzbw64hhmakb0bwm87ck63mmr88jym454ljbb5b8lvav") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-maybe_xml-0.7.1 (c (n "maybe_xml") (v "0.7.1") (h "1ai21h8c2nkiqr9x81xrzlh1pi0lw5ydrdj90bqlzyr9wi5vq0hj") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-maybe_xml-0.7.2 (c (n "maybe_xml") (v "0.7.2") (h "0s48bhhipf807dy7n0alhf08ianmm3xnnrzd4xkm6hhlhr7smczh") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-maybe_xml-0.8.0 (c (n "maybe_xml") (v "0.8.0") (h "1lxcwm1qcw5s7ylz6zigyaq1g1vamny02189filqz1nd9cscivmy") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.56.0")))

(define-public crate-maybe_xml-0.9.0 (c (n "maybe_xml") (v "0.9.0") (h "18l7xyd2xwsx87ww1djzlrlfbr0qd91m9a5c5ns6crkhw4fwwjs1") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.71.0")))

(define-public crate-maybe_xml-0.10.0 (c (n "maybe_xml") (v "0.10.0") (h "11h1p3w4gkxnrr7n7sjggz30wrgf46qyc1djnlcdhrb694ylczlb") (f (quote (("std") ("default" "std") ("alloc")))) (y #t) (r "1.71.0")))

(define-public crate-maybe_xml-0.10.1 (c (n "maybe_xml") (v "0.10.1") (h "0c9n0xbbgy5vs0d4dlc2nma54b4v18l7i6j7nkxq9ycaidp8giis") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.71.0")))

(define-public crate-maybe_xml-0.11.0 (c (n "maybe_xml") (v "0.11.0") (d (list (d (n "proptest") (r "^1.4.0") (d #t) (k 2)))) (h "06qlsywcgb6fydbckc8jk98jm96k0vvbqzsmibrk4a371l4k96pz") (f (quote (("std") ("internal_unstable") ("default" "std") ("alloc")))) (r "1.71.0")))

