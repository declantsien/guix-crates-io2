(define-module (crates-io ma yb maybe-uninit-ext) #:use-module (crates-io))

(define-public crate-maybe-uninit-ext-0.1.0 (c (n "maybe-uninit-ext") (v "0.1.0") (h "1z99npm2fcz7bvkm80kmclqr95ckisvgxjm3r1i6mapzbq2r8lqm")))

(define-public crate-maybe-uninit-ext-0.2.0 (c (n "maybe-uninit-ext") (v "0.2.0") (h "1m6q9kxsl016fbk1idm2ak1pv9gjs7ll4qxgj4jdiggagnrwbhw8") (r "1.61")))

(define-public crate-maybe-uninit-ext-0.3.0 (c (n "maybe-uninit-ext") (v "0.3.0") (d (list (d (n "qualifier_attr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0caajsp6scjm8nlpl5n1sn46qi9qgmhn0sn833zdncv0mzkw3ap7") (s 2) (e (quote (("nightly" "dep:qualifier_attr")))) (r "1.61")))

(define-public crate-maybe-uninit-ext-0.4.0 (c (n "maybe-uninit-ext") (v "0.4.0") (d (list (d (n "qualifier_attr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "198sw41cafi7n5hijbwz71d66yn4fpfwxhq0k38girpfa9j6p8jp") (s 2) (e (quote (("nightly" "dep:qualifier_attr")))) (r "1.61")))

(define-public crate-maybe-uninit-ext-0.5.0 (c (n "maybe-uninit-ext") (v "0.5.0") (d (list (d (n "qualifier_attr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06r413jamwdwj1gjkrj07zcjxcshgzffzg6m6fl73l1j8a5j2pz7") (s 2) (e (quote (("nightly" "dep:qualifier_attr")))) (r "1.71")))

(define-public crate-maybe-uninit-ext-0.6.0 (c (n "maybe-uninit-ext") (v "0.6.0") (d (list (d (n "qualifier_attr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ynv1hf6wfsbn56p11z61np3yy7a79q2dh7fqqizijisd48hi28m") (s 2) (e (quote (("nightly" "dep:qualifier_attr")))) (r "1.75")))

