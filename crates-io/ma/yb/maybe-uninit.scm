(define-module (crates-io ma yb maybe-uninit) #:use-module (crates-io))

(define-public crate-maybe-uninit-1.0.0 (c (n "maybe-uninit") (v "1.0.0") (h "0v747mzf8n6pjppqlq664yqjywf52w6q2x56xxi9vaib5dcddgnn")))

(define-public crate-maybe-uninit-2.0.0 (c (n "maybe-uninit") (v "2.0.0") (h "004y0nzmpfdrhz251278341z6ql34iv1k6dp1h6af7d6nd6jwc30")))

