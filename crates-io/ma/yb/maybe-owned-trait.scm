(define-module (crates-io ma yb maybe-owned-trait) #:use-module (crates-io))

(define-public crate-maybe-owned-trait-0.1.0 (c (n "maybe-owned-trait") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cwzn0r8xzxsa4wqijai2plkrv4v9wi79h221fsgnhg1mx1266jj")))

(define-public crate-maybe-owned-trait-0.2.0 (c (n "maybe-owned-trait") (v "0.2.0") (d (list (d (n "beef") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08y6mlmqlwal6s7kff1h1rfz1zg8cz4vcy2znxyd6ygg6jy7r6zk") (s 2) (e (quote (("beef" "dep:beef"))))))

(define-public crate-maybe-owned-trait-0.2.1 (c (n "maybe-owned-trait") (v "0.2.1") (d (list (d (n "beef") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13cdfv4fzas06w3a6sl8g0ya3sc57rifdms4x2pwwqsbxg24v9gh") (s 2) (e (quote (("beef" "dep:beef"))))))

