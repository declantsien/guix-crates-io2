(define-module (crates-io ma yb maybe-sync) #:use-module (crates-io))

(define-public crate-maybe-sync-0.1.0 (c (n "maybe-sync") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "130cj4phvjk17srhdfkjz3fr4iw2zp9i665gh7wal22j23zrx1dv") (f (quote (("unstable-doc") ("sync" "parking_lot") ("default" "alloc") ("alloc"))))))

(define-public crate-maybe-sync-0.1.1 (c (n "maybe-sync") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "1dw0wv4dgl68h2if6q0k7lfkd43gypbmsblbdjg7dkvyqkydclbm") (f (quote (("unstable-doc") ("sync" "parking_lot") ("default" "alloc") ("alloc"))))))

