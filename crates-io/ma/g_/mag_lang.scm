(define-module (crates-io ma g_ mag_lang) #:use-module (crates-io))

(define-public crate-mag_lang-0.1.0 (c (n "mag_lang") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "magc") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strontium") (r "^0.6.0") (d #t) (k 0)))) (h "0xxwwwb6jinbrqxx0wy0p91ivk3f2v99l0kcmqd584h7i2djqv99")))

