(define-module (crates-io ma la malaga_http_utils) #:use-module (crates-io))

(define-public crate-malaga_http_utils-0.1.0 (c (n "malaga_http_utils") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01ljqfyw0ymvnsflpsq3s32whyz6ggla3wm246w51s6dh8bi2sdg")))

(define-public crate-malaga_http_utils-0.1.1 (c (n "malaga_http_utils") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17gl7sn1r4nq6jspan50w3cvnvjm5k27grjwgi3d9hz3i8sxpvn5")))

(define-public crate-malaga_http_utils-0.1.2 (c (n "malaga_http_utils") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0az8inh6lv204p794dkiz5d9yx6xb7y6b6k1xpyyrr3bnwpnjvv1")))

(define-public crate-malaga_http_utils-0.1.3 (c (n "malaga_http_utils") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09r0q80ck6ybzcwxxsnb6w2mglzq4hb5i847cs6n0r5lkwy4p5c4")))

(define-public crate-malaga_http_utils-0.1.4 (c (n "malaga_http_utils") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19rh25xzj5s3q7djjmigg8yah30d6mvnjri13j0x1kscnhy8p0xa")))

