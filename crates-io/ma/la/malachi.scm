(define-module (crates-io ma la malachi) #:use-module (crates-io))

(define-public crate-malachi-0.1.0 (c (n "malachi") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1x73a5yjbp4zacdiryhzyma2w52bqzkg463rwp6yi1z4y2k82z5x")))

(define-public crate-malachi-0.2.1 (c (n "malachi") (v "0.2.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1rzf91vzz619xphgqisafianlrfdv0dcz5y5gpl048ayf7zkpdlx")))

(define-public crate-malachi-0.3.0 (c (n "malachi") (v "0.3.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "07vp5fsaj9cy9w7sw0c0q97nn3scc09iccnmhav4lv6zhj06w41b")))

(define-public crate-malachi-0.4.0 (c (n "malachi") (v "0.4.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "00i51gz0jnwrhl9b3047z87d63h8rdh76wx019bmv4dgylgd1211")))

(define-public crate-malachi-0.4.1 (c (n "malachi") (v "0.4.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1k03wj95zlykmbrf65dc0n31q441k6p5vdaa6vv53l8yf47vk2fk")))

(define-public crate-malachi-0.4.2 (c (n "malachi") (v "0.4.2") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1vjlyicp55p48dj6jkzh134dv40h94zlsiz7aijr0523ygrgxvbh")))

(define-public crate-malachi-0.5.0 (c (n "malachi") (v "0.5.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1lwcdm76cqgww5kb429v5d0cikrviv43c6nkk9ipk47d8k17m7ar")))

(define-public crate-malachi-0.5.1 (c (n "malachi") (v "0.5.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0gbk4bwnrmllsfp5j2q5zgrg32xh3fz3vqsk7bjf69m96i0k4q3m")))

(define-public crate-malachi-0.5.2 (c (n "malachi") (v "0.5.2") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1ciyxmm0v4x22rnghvxaw1c4rxbax8xrm4wvxqlxvjcb6ywl6k4p")))

(define-public crate-malachi-0.6.0 (c (n "malachi") (v "0.6.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0lj27i704f1y5wpxn1qzzf42lx87fqbvzrwc43wlw748skzizmgg")))

(define-public crate-malachi-0.6.1 (c (n "malachi") (v "0.6.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0k6gyqz90arjp3r6nff5qn7qhyk1253jw1sjylg8yk15hz356d7r")))

(define-public crate-malachi-0.6.2 (c (n "malachi") (v "0.6.2") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0qz636iag58zrdx5mi4rlnz49a8z3qycix13ddnwijrdcq0w2pbq")))

(define-public crate-malachi-0.7.0 (c (n "malachi") (v "0.7.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "131nng04ldjxwphmav1ymjp5pf1kvylg8257p7572wsgb8masmg1")))

(define-public crate-malachi-0.7.1 (c (n "malachi") (v "0.7.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0v2srkn3r2b2hi9w9c380m3lgggs13z7csjanis9bv8gl97hj0i1")))

(define-public crate-malachi-0.7.2 (c (n "malachi") (v "0.7.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "05f4lwc2iv7wvvcv7kxdl1ldm7vf1c5440fc38vimvd9p29zc67l")))

(define-public crate-malachi-0.7.3 (c (n "malachi") (v "0.7.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1j2av713hwvkybavv51jdgf3gk4wbwa62qjj5mmdi9dxiffsd7j3")))

(define-public crate-malachi-0.8.0 (c (n "malachi") (v "0.8.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1h3k3m5kmsb030i151b09wjdsrwb90pbs7v1gvjpqg7kd57zcyjc")))

(define-public crate-malachi-0.9.0 (c (n "malachi") (v "0.9.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "00nj8cf5ld9cxvndp6dsxw0vzmvj4dm0zdlps3vxpycxqc8vlxd9")))

(define-public crate-malachi-0.9.1 (c (n "malachi") (v "0.9.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "138hjdm40akb6rrvrfzx3hrlvsw64kjrpx76b1708b3s4apn13cx")))

(define-public crate-malachi-0.9.3 (c (n "malachi") (v "0.9.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1clcrpx8pwwjjjaqfpz7s05psjaijz18jswiy9ldq9kj61pji6dd")))

(define-public crate-malachi-0.9.4 (c (n "malachi") (v "0.9.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1cl5yb3z8dqbx692ch7v8rq3khqhvma15gy41bzc4cwnacms63pz")))

(define-public crate-malachi-0.9.6 (c (n "malachi") (v "0.9.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1sjmpl63ki39ksf66s2bj4pwaa63kcjf4v0mlapjxkwfh1rpk0dl")))

