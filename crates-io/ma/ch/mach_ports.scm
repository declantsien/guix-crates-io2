(define-module (crates-io ma ch mach_ports) #:use-module (crates-io))

(define-public crate-mach_ports-0.1.0-alpha (c (n "mach_ports") (v "0.1.0-alpha") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "1kn39azdym1skbn4lpisfz8vswl86jibwa587aij9plq6gk5551w") (y #t)))

(define-public crate-mach_ports-0.1.0-alpha1 (c (n "mach_ports") (v "0.1.0-alpha1") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "0mi73d9dkh6qdjfdrwb7lnf0jblbj7zhl859gf5a5jgg13z5dhj7") (y #t)))

(define-public crate-mach_ports-0.1.0-alpha2 (c (n "mach_ports") (v "0.1.0-alpha2") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "13m3vdbprd15jdcba4lnpf4b730asfyq5dinicflp8f5bmhxb7zd") (y #t)))

(define-public crate-mach_ports-0.2.0-alpha (c (n "mach_ports") (v "0.2.0-alpha") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "1zqd5lg3jf5ivbxdnq0bvfrld9faihc2rnq104dy2hdrw1px3jc4")))

(define-public crate-mach_ports-0.3.0-alpha (c (n "mach_ports") (v "0.3.0-alpha") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)) (d (n "page_size") (r "^0.5") (d #t) (k 0)))) (h "06djkdrhas0w746pz5x9q2g85zv60hf4qxx6f23scn9wris494k9")))

