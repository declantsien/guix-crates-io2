(define-module (crates-io ma ch macho-unwind-info) #:use-module (crates-io))

(define-public crate-macho-unwind-info-0.1.0 (c (n "macho-unwind-info") (v "0.1.0") (d (list (d (n "object") (r "^0.28.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0c7i9gw2l24lriyb0l87f9drzckig9fhbw5mid4zjssn5ayf2gi0")))

(define-public crate-macho-unwind-info-0.2.0 (c (n "macho-unwind-info") (v "0.2.0") (d (list (d (n "object") (r "^0.28.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "072p2kppa8gxk3ykvmyzj6sgiq6ig35nj4azgv39rgdx3wj8jcni")))

(define-public crate-macho-unwind-info-0.3.0 (c (n "macho-unwind-info") (v "0.3.0") (d (list (d (n "object") (r "^0.28.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0ky1356bhvhkf9ha79apwb1as4gq5lylj9mrzfdiw6pcvplln099")))

(define-public crate-macho-unwind-info-0.4.0 (c (n "macho-unwind-info") (v "0.4.0") (d (list (d (n "object") (r "^0.32.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0gp0c1fav9xn5gqw1mk8kn226pjh5l4bp20fnrb3zhjbqyn8cq3b")))

