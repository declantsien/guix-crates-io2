(define-module (crates-io ma ch machinecode) #:use-module (crates-io))

(define-public crate-machinecode-1.0.0 (c (n "machinecode") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0akv2gjfzi434f7cq1as237plmmhwy4mh9dvvfyclva6slzakdw4")))

(define-public crate-machinecode-1.0.1 (c (n "machinecode") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12p3pldl86x3af2kizxgq6d22qn7nf649jmpxr43f5afq4clfkhz")))

