(define-module (crates-io ma ch machx) #:use-module (crates-io))

(define-public crate-machx-0.4.2 (c (n "machx") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1l28gwya41mxp7qf5fs7nqbv8501cv9981x37bgcrl6f74cia9f2")))

(define-public crate-machx-0.4.3 (c (n "machx") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1k26hjjbz1yfxizl4mxvwbimcfilz0ymhc3nl6d7m8dg6zjzas35")))

(define-public crate-machx-0.4.4 (c (n "machx") (v "0.4.4") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "14zrl05bq9sfwbm7w6jdlxi1dahqyr42cazaa88hr8wfkzqb8mk9")))

(define-public crate-machx-0.4.5 (c (n "machx") (v "0.4.5") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0m6581ygaw7a774k7bhf0qwxl3jal6ahic0idhgx9fpn5km4c8j4")))

(define-public crate-machx-0.4.6 (c (n "machx") (v "0.4.6") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1f7jhzrdc0f5ab12djbzbdfnnxfvv46sf6ri94iyzhfrm6qqh2my")))

(define-public crate-machx-0.4.7 (c (n "machx") (v "0.4.7") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0nbdi9mpqh1phbz55yg7jpnxgl2y13y5pxz904pyc0jlgj4l0ik8")))

(define-public crate-machx-0.4.8 (c (n "machx") (v "0.4.8") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0nq5y8fl6yphz355yzflmlr35q20c91pkmal4gjq6gmxg0azx59h")))

(define-public crate-machx-0.4.9 (c (n "machx") (v "0.4.9") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "14xxcmliak8q9fiqiwjfxs4bkpjgjv07lfsslpsbqsw3avpvdjas")))

