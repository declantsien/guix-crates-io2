(define-module (crates-io ma ch machine_int) #:use-module (crates-io))

(define-public crate-machine_int-0.1.0 (c (n "machine_int") (v "0.1.0") (h "010g65pl7wzbpv0hj9d54dm2j6krvlkq542dk803zn101iccjjnq")))

(define-public crate-machine_int-0.1.1 (c (n "machine_int") (v "0.1.1") (h "1szqnkdgdbwrjkw664zyynw46m02ncpiny61j0lfi82jlsnqxybc")))

(define-public crate-machine_int-0.1.2 (c (n "machine_int") (v "0.1.2") (h "1ca6lx03rp7gsqn10lsla0hcds56gvprmxv0m5rl6ds9nhb4kan8")))

(define-public crate-machine_int-0.1.3 (c (n "machine_int") (v "0.1.3") (h "0ifar5r5n7wwi8y9pmvkjvxg7q3q8nrxknlyjkiisyjp5yy71swh")))

(define-public crate-machine_int-0.1.4 (c (n "machine_int") (v "0.1.4") (h "009a544vcixmbwwijvl67n5l95y99v01ba5lc0an0jf2jlpvwr0i")))

