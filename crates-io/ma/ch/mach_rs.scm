(define-module (crates-io ma ch mach_rs) #:use-module (crates-io))

(define-public crate-mach_rs-0.1.0-alpha (c (n "mach_rs") (v "0.1.0-alpha") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "1ipc89yqkhhxsd8nhiprlj31sricjxa8ghzhak4qmxznwd3iavih") (y #t)))

(define-public crate-mach_rs-0.2.0-alpha (c (n "mach_rs") (v "0.2.0-alpha") (d (list (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "0c7rlm02kkrr85h6kn2llf8vdh6hr66cz43541i7h034g0faxg3k") (y #t)))

