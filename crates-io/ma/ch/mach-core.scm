(define-module (crates-io ma ch mach-core) #:use-module (crates-io))

(define-public crate-mach-core-0.1.0 (c (n "mach-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1ddbjvnwzj41j6xf804sq3z3bvv4m0s6zpzr26hxai6jnznivjvs") (y #t)))

(define-public crate-mach-core-0.1.1 (c (n "mach-core") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ahmlf4h58z4jnjvxlc2b1mlvyp5fz740z3vf700ijp40pkqz02l")))

