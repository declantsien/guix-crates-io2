(define-module (crates-io ma ch mach-cli) #:use-module (crates-io))

(define-public crate-mach-cli-0.1.0 (c (n "mach-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.2.0") (d #t) (k 0)) (d (n "lets_find_up") (r "^0.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0lbcfp1p24b46m2j440cvqx6figrvzxp98xcn0v6l8h7k7ig0m4c")))

