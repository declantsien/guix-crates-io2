(define-module (crates-io ma ch machine-info) #:use-module (crates-io))

(define-public crate-machine-info-1.0.0 (c (n "machine-info") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)))) (h "0k30rclq789vx0cbzc3qb3rfkrwjlfiisig7qsxkv94hhs1z30h6")))

(define-public crate-machine-info-1.0.1 (c (n "machine-info") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)))) (h "0mbkn2mwsff4fvdxkvv7iki03qb258basvqysc1612s8j8n7qdn2")))

(define-public crate-machine-info-1.0.2 (c (n "machine-info") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)))) (h "1g94snp454qkmhhfzsilsnc1vj258mvxj332qd3bkl1g9q2q77wj")))

(define-public crate-machine-info-1.0.4 (c (n "machine-info") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)) (d (n "v4l") (r "^0.13.0") (d #t) (k 0)))) (h "1qq748imaazkwm82h4im2925c09sadx18mcvnyx20yc1kkml5pwi")))

(define-public crate-machine-info-1.0.5 (c (n "machine-info") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)) (d (n "v4l") (r "^0.13.0") (d #t) (k 0)))) (h "1fal1j00lhv0wm8ckh1j7j7jzv2r6y4vn6b6h1gn018nyw6lpppb")))

(define-public crate-machine-info-1.0.6 (c (n "machine-info") (v "1.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.1") (k 0)) (d (n "v4l") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0fn55c2p5ndvq7zc3694hi9iv6729y8pix7kd1rlk156rpq4954v") (s 2) (e (quote (("v4l" "dep:v4l"))))))

(define-public crate-machine-info-1.0.7 (c (n "machine-info") (v "1.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (k 0)) (d (n "v4l") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1rl2h9dd82bz0an0ifhriawq98f08y8a8fhwv2ppv4d3n33knlmn") (s 2) (e (quote (("v4l" "dep:v4l"))))))

(define-public crate-machine-info-1.0.8 (c (n "machine-info") (v "1.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (k 0)) (d (n "v4l") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0m1hpdf293hlkfzml40p3qd1v88xgrj16jr09jnv6xyz8j6apsqv") (s 2) (e (quote (("v4l" "dep:v4l"))))))

(define-public crate-machine-info-1.0.9 (c (n "machine-info") (v "1.0.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (k 0)) (d (n "v4l") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1kjx15s6wsdzz5yr2bb3v4ajknfi9bd7m05hxl97d4ppa3ics2rd") (s 2) (e (quote (("v4l" "dep:v4l"))))))

