(define-module (crates-io ma ch mach-siegbert-vogt-dxcsa) #:use-module (crates-io))

(define-public crate-mach-siegbert-vogt-dxcsa-0.1.0 (c (n "mach-siegbert-vogt-dxcsa") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)))) (h "1zl9xmvb9a6vrkv4x0rrv8zk5zh059s879x7cb7f27rf6wlrcd3m") (y #t)))

(define-public crate-mach-siegbert-vogt-dxcsa-0.1.1 (c (n "mach-siegbert-vogt-dxcsa") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)))) (h "0gir7nkp3l3mil0p8k9ny7zrngidq4bn0089ksvr2c4fc4gr7wjh") (y #t)))

(define-public crate-mach-siegbert-vogt-dxcsa-0.1.2 (c (n "mach-siegbert-vogt-dxcsa") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)))) (h "09y10whbjg8gwyd58i66g5c9p8n51g8fidxjdihr9gjixbfj93gc") (y #t)))

(define-public crate-mach-siegbert-vogt-dxcsa-0.1.3 (c (n "mach-siegbert-vogt-dxcsa") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)))) (h "0yqan2frdrdkq15a6ywl55v93wz78z6mnzah9vc7l139i0sn4gkx")))

