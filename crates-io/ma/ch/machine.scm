(define-module (crates-io ma ch machine) #:use-module (crates-io))

(define-public crate-machine-0.0.1 (c (n "machine") (v "0.0.1") (h "0cfgz15dh5bfcgzi6n2am8c042nl0gh58axvvpjwna48sazhr2yn")))

(define-public crate-machine-0.1.0 (c (n "machine") (v "0.1.0") (h "1qxsx953y53r6nmcckb0gp2a7v6cfz92s17nxp7gnq9znxvj93w8")))

(define-public crate-machine-0.2.0 (c (n "machine") (v "0.2.0") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d9vdm0y9x7bm39lj3pj7379hmmwjh9v7y7wlyfw2bmibgvf3zjm")))

(define-public crate-machine-0.3.0 (c (n "machine") (v "0.3.0") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pvis76c08np29bnnb1sqc3y5d6xz1z2bq1gir9lmkch7xmx0bx5")))

