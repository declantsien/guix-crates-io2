(define-module (crates-io ma ch mach) #:use-module (crates-io))

(define-public crate-mach-0.0.1 (c (n "mach") (v "0.0.1") (h "1r5z5f5wby4xx67rxbfc7asx1iazqk4cq9p8mw8mkr9zic5w283p")))

(define-public crate-mach-0.0.2 (c (n "mach") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1480k972328ibs9ahkpgngr0payqknz5ln6lni48lys6rhy094m6")))

(define-public crate-mach-0.0.3 (c (n "mach") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "0kvsg6ajp8hddwsin86wrbwjirgskqzdchphgl517ycvfx3jv41k")))

(define-public crate-mach-0.0.4 (c (n "mach") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "024f9riyimx8cqq31r3bjnx5syiz3g85m27b71bhzyis0yv4wmm8")))

(define-public crate-mach-0.0.5 (c (n "mach") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xzjkdlmgd3z271gw7fzh9f066w10hlmpkqr6c6wy8yg2vs9frhr")))

(define-public crate-mach-0.0.6 (c (n "mach") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0563gybzinn227v4qh37aw2j7kjnd37054zip8cfrpiz82xnl4bg")))

(define-public crate-mach-0.1.0 (c (n "mach") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l00nbybjmn2acsk0w14d59gjm4d64z1x7j4sc0492a82a7ll7mr")))

(define-public crate-mach-0.1.1 (c (n "mach") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h6224qcnjjh7gynmf0cpymfs2r8vcm9kq54d3r9ns37sp2r7jmz")))

(define-public crate-mach-0.1.2 (c (n "mach") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s9d61w31ik3f21fy1fnrvvn6gdv61ddwnm07f1q5k31vpi3xl9g")))

(define-public crate-mach-0.2.0 (c (n "mach") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "02x96kbgv24l7g1n0ax22lks0skq3qhkavaxiincyzg2723zljdm") (f (quote (("use_std" "libc/use_std") ("unstable") ("deprecated") ("default" "use_std" "deprecated"))))))

(define-public crate-mach-0.2.1 (c (n "mach") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0qjhglnx395w48x8g7ys642vl38imiialbcvwyw4agizdp19fb7l") (f (quote (("use_std" "libc/use_std") ("unstable") ("deprecated") ("default" "use_std" "deprecated"))))))

(define-public crate-mach-0.2.2 (c (n "mach") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0vglglyy3gm6dlg78x5b049kkb79lv79i20bl56vjxpx3jhn4wlp") (f (quote (("use_std" "libc/use_std") ("unstable") ("deprecated") ("default" "use_std" "deprecated"))))))

(define-public crate-mach-0.2.3 (c (n "mach") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1qdhs16cl1j3w7kvy6ak7h8lbyqmr6i3i15qfzpnv9gyrn3j9pc6") (f (quote (("use_std" "libc/use_std") ("unstable") ("deprecated") ("default" "use_std" "deprecated"))))))

(define-public crate-mach-0.3.0 (c (n "mach") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0a895rhg3a1l3ws4qf83s5mx0g29v1fzgjmbag1h36v62hmg1vi8") (f (quote (("std" "libc/std") ("deprecated") ("default" "std"))))))

(define-public crate-mach-0.3.1 (c (n "mach") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "rustc-std-workspace-core") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1k089ck73gi6d1z7n188vsi0il6k9f8gdwq50x22pypnz4d8af57") (f (quote (("std" "libc/std") ("rustc-dep-of-std" "rustc-std-workspace-core" "libc/rustc-dep-of-std") ("deprecated") ("default" "std"))))))

(define-public crate-mach-0.3.2 (c (n "mach") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "rustc-std-workspace-core") (r "^1.0.0") (o #t) (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1yksa8lwzqh150gr4417rls1wk20asy9vhp8kq5g9n7z58xyh8xq") (f (quote (("rustc-dep-of-std" "rustc-std-workspace-core" "libc/rustc-dep-of-std") ("deprecated") ("default"))))))

