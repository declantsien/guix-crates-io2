(define-module (crates-io ma ch machinate) #:use-module (crates-io))

(define-public crate-machinate-0.0.0 (c (n "machinate") (v "0.0.0") (d (list (d (n "axum") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iy3bk5kldyn1xz48963xci6shhif4f4554cah71j7xanlivhx08") (r "1.74")))

