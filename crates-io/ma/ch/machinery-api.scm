(define-module (crates-io ma ch machinery-api) #:use-module (crates-io))

(define-public crate-machinery-api-0.1.0 (c (n "machinery-api") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "0gyvphayfrki7alv5pj2f7askb21pb3zb4rpz193qdm4ajhq9631")))

(define-public crate-machinery-api-0.2.0 (c (n "machinery-api") (v "0.2.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "0wh85v02qlwv1cg274bc1kx4kw482vymjnq8fmiwvnbcsw15lrap")))

(define-public crate-machinery-api-0.3.0 (c (n "machinery-api") (v "0.3.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "06p8998rry49ncmn3ganya5s6c4c7hqzpn03x2qls0in2vkrp96h")))

(define-public crate-machinery-api-0.3.1 (c (n "machinery-api") (v "0.3.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "1ivlgdf5k6cdxayl58ils8z2gps2ldc17a206yd0jbmq9nby52f1")))

(define-public crate-machinery-api-0.4.0 (c (n "machinery-api") (v "0.4.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "15h2pwrllxqj906j514vz0qdqqz8p3y0nkghf7f7s5g9mvk0njhd")))

(define-public crate-machinery-api-0.5.0 (c (n "machinery-api") (v "0.5.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)))) (h "1lwhzw47myg5294qfl779pvkjz8cq8grba0pmyrgslklj4imxwy4")))

