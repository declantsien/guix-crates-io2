(define-module (crates-io ma ch machinery) #:use-module (crates-io))

(define-public crate-machinery-0.1.0 (c (n "machinery") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "108dwypz4vg3xh2zxc4fxb3lvzhzz0h3zsdcbfnsb22r2yg25672")))

(define-public crate-machinery-0.2.0 (c (n "machinery") (v "0.2.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0lkdi7926z7i363b6k7zk7n6m3jxc92qjqjl80gfm09s7p250s5j")))

(define-public crate-machinery-0.3.0 (c (n "machinery") (v "0.3.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1v9ni8jl32ca0y8jlhbhp6rkhmr9wjfldq2p0cyxnxaw9rq8w5rn")))

(define-public crate-machinery-0.4.0 (c (n "machinery") (v "0.4.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "12mfnsz2b0p09pqs1ypnv6yd873qw6m20svf5im14qnpmb010gxd")))

(define-public crate-machinery-0.5.0 (c (n "machinery") (v "0.5.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "099lsqjhnchamzn10ym1hmca45lxqi5i73ca1mi1dd9s20iqg20s")))

(define-public crate-machinery-0.6.0 (c (n "machinery") (v "0.6.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "15zhf74hnabn63ig44s7zcx4wpvyxy3nsnpiz2z3avsz6khacc5d")))

(define-public crate-machinery-0.7.0 (c (n "machinery") (v "0.7.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0gq30k3bai8smphg3j395c606dv9dgy66gfqqmr4xya4ilzv9qxh")))

(define-public crate-machinery-0.8.0 (c (n "machinery") (v "0.8.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.1.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1y279qs16qb0j2974b4pwmaa5nyxci62vf0zs50sq4f5yyq4zsmj")))

(define-public crate-machinery-0.8.1 (c (n "machinery") (v "0.8.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.1.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1argz2ab0niz4xkbpfl1r46rpm1kzv8j1l6nryn9y68fs6qif5k7")))

(define-public crate-machinery-0.9.0 (c (n "machinery") (v "0.9.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.2.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "15hvav8gw5yjl0v1qpgwivxawhchv36m0lpj3zk05ik110dy1z6z")))

(define-public crate-machinery-0.9.1 (c (n "machinery") (v "0.9.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0l9s7r28vvn0wnpp0flcxihr8y9p9v21pdfa2zzyxgi2mg3zxg8c") (y #t)))

(define-public crate-machinery-0.10.0 (c (n "machinery") (v "0.10.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1k9915s820ppj1fky4szqw3id7wg49zq20lj9jjz15ix1f3s80r0")))

(define-public crate-machinery-0.10.1 (c (n "machinery") (v "0.10.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0ywi61agnrcz04i6w5zqbkvravr9i5paqia4ifk59hv6rwyivp2n")))

(define-public crate-machinery-0.10.2 (c (n "machinery") (v "0.10.2") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0chg6hvvlwiihp27cf70zqzacavfy30x384n5dwnxhigbn58w1l9")))

(define-public crate-machinery-0.10.3 (c (n "machinery") (v "0.10.3") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "025y2p7ssfv5d848910v7h3n86ympw0ra0yf32r4sccyww8n0ag2")))

(define-public crate-machinery-0.10.4 (c (n "machinery") (v "0.10.4") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1x5d4crm06nw21cqnwyzszhwcgv8swaldq25bndnkbyxccsmv1s3")))

(define-public crate-machinery-0.10.5 (c (n "machinery") (v "0.10.5") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "12pa242qyr49nb2gga3qhq169n9z1hxpahih4ghn67sh1adv2yim")))

(define-public crate-machinery-0.11.0 (c (n "machinery") (v "0.11.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.4.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1bwlx6r7xv3ss1r7rp77y2gmphjfl28w78mj7syknawk1qqjr34z")))

(define-public crate-machinery-0.12.0 (c (n "machinery") (v "0.12.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.5.0") (d #t) (k 0)) (d (n "machinery-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1lnmz475zgip3nqq0z9isnm5838vnf65nikahngxmf7xz7rjrrzi")))

