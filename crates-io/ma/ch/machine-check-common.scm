(define-module (crates-io ma ch machine-check-common) #:use-module (crates-io))

(define-public crate-machine-check-common-0.1.0-alpha.1 (c (n "machine-check-common") (v "0.1.0-alpha.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0r85vlmlhg5fc4qcsb67nmgax473jpxm9q5idrh8xc5hsrrvklzr") (r "1.70")))

(define-public crate-machine-check-common-0.1.0-alpha.2 (c (n "machine-check-common") (v "0.1.0-alpha.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "06zb69p5b2lw6jn6664pl2qglm7yplisy5h14r3m52f3l60qdins") (r "1.70")))

(define-public crate-machine-check-common-0.1.0-alpha.3 (c (n "machine-check-common") (v "0.1.0-alpha.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0c2vayr1ygr79kbb7cqjzznaiqmq4d7k2qnlqn1scm2p1hch6dj9") (r "1.70")))

(define-public crate-machine-check-common-0.1.0 (c (n "machine-check-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0mgdnnj4s4vs4xh723dwgsgfb4d0ar1bjhrnxdfxw0x71pp1mxwx") (r "1.70")))

(define-public crate-machine-check-common-0.2.0-alpha.1 (c (n "machine-check-common") (v "0.2.0-alpha.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0byvawnpdykiqpnw9l5f669vkjiw28a4rc7ay9vli6kkam1h7b11") (r "1.75")))

(define-public crate-machine-check-common-0.2.0-alpha.2 (c (n "machine-check-common") (v "0.2.0-alpha.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "19klnlgcnqkfc769nr08jg8h0c70189mza76wnxva202bhqi30kk") (r "1.75")))

(define-public crate-machine-check-common-0.2.0 (c (n "machine-check-common") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0x6bbjxfpszn3si38cg9k4hcgdx84qhsxz9w4l4rfnfnjsczbkvw") (r "1.75")))

