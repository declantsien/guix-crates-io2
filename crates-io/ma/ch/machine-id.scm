(define-module (crates-io ma ch machine-id) #:use-module (crates-io))

(define-public crate-machine-id-0.2.1 (c (n "machine-id") (v "0.2.1") (d (list (d (n "lazy_static") (r "~0.1.14") (d #t) (k 0)) (d (n "log") (r "~0.3.2") (d #t) (k 0)) (d (n "uuid") (r "~0.1.18") (d #t) (k 0)))) (h "09jzv98ax63srgd2hhbx5jirwglra5b98d5h5lys8l3hjjc1dbkl")))

(define-public crate-machine-id-0.2.2 (c (n "machine-id") (v "0.2.2") (d (list (d (n "lazy_static") (r "~0.1.14") (d #t) (k 0)) (d (n "log") (r "~0.3.2") (d #t) (k 0)) (d (n "uuid") (r "~0.1.18") (d #t) (k 0)))) (h "0fshl4rg2r5r7bc1nzvdihshbjp0nhmy2r5h3bi5q6bc3nqp2zxi")))

(define-public crate-machine-id-0.3.0 (c (n "machine-id") (v "0.3.0") (d (list (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "016ysky3m7a0kwlz211134kjvrdyh38wvq3pyfywpm1hb6sjkr66")))

(define-public crate-machine-id-0.4.0 (c (n "machine-id") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jwg64rfr7ddhd0ar41p6c11w1208q1p0ia4r6vw0g0d57g5mk2n")))

