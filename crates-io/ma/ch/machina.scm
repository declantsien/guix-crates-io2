(define-module (crates-io ma ch machina) #:use-module (crates-io))

(define-public crate-machina-0.4.0 (c (n "machina") (v "0.4.0") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "region") (r "^0.1.1") (d #t) (k 0)) (d (n "sam") (r "^0.2.0") (d #t) (k 2)))) (h "1mgbksnvjhk8x9qkpa7xz175qvrbfzajcw82x1cgwpakf8cgi1y4")))

(define-public crate-machina-0.5.0 (c (n "machina") (v "0.5.0") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "region") (r "^0.1.1") (d #t) (k 0)) (d (n "sam") (r "^0.2.0") (d #t) (k 2)))) (h "0qvxwpgbimfh6i8nabbn8y3dp1wm8rhfdly6x00n3saxanjxqnfc")))

(define-public crate-machina-0.5.1 (c (n "machina") (v "0.5.1") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "region") (r "^0.1.1") (d #t) (k 0)) (d (n "sam") (r "^0.2.0") (d #t) (k 2)))) (h "1f1ah5as2jbc700j4cs0n00jqkq0qxl6pqpb27zv4f3qg9zy3ylx") (f (quote (("optimize") ("default"))))))

(define-public crate-machina-0.6.0 (c (n "machina") (v "0.6.0") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "region") (r "^0.1.1") (d #t) (k 0)) (d (n "sam") (r "^0.2.0") (d #t) (k 2)))) (h "094vsh93bfwp1d3l8w5v2rd8mafklad8iak0lgz9q3qjak2ai6s2") (f (quote (("optimize") ("default"))))))

(define-public crate-machina-0.6.1 (c (n "machina") (v "0.6.1") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "region") (r "^0.1.1") (d #t) (k 0)) (d (n "sam") (r "^0.2.0") (d #t) (k 2)))) (h "1qb46yj2cwhihjb47hi8ly64v2sl8pn2kzcn2hl54bsvpvbk7l2c") (f (quote (("optimize") ("default"))))))

