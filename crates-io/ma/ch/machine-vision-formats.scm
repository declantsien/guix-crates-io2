(define-module (crates-io ma ch machine-vision-formats) #:use-module (crates-io))

(define-public crate-machine-vision-formats-0.1.0 (c (n "machine-vision-formats") (v "0.1.0") (h "0pv2kly5063y3hkax0bpxn8yxlwb77nmsr9x1l611x6l2x3rhqyv") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-machine-vision-formats-0.1.1 (c (n "machine-vision-formats") (v "0.1.1") (h "1n3zvkg1f3cnabbg0fbl6sid497b94fazgk03lp1b91c92cd34n0") (f (quote (("std") ("default" "std") ("alloc"))))))

