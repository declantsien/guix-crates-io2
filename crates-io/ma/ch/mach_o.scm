(define-module (crates-io ma ch mach_o) #:use-module (crates-io))

(define-public crate-mach_o-0.1.0 (c (n "mach_o") (v "0.1.0") (d (list (d (n "mach_o_sys") (r "^0.1.1") (d #t) (k 0)))) (h "14c29pm4ykdp9aids6s20013z2lds4cr90aj2sc5s6h8plk6vm3p")))

(define-public crate-mach_o-0.1.1 (c (n "mach_o") (v "0.1.1") (d (list (d (n "mach_o_sys") (r "^0.1.1") (d #t) (k 0)))) (h "0qhmpgf594lhfvcplfz2rvd8fcnw89xvm7qiip53mps46sn27k15")))

(define-public crate-mach_o-0.1.2 (c (n "mach_o") (v "0.1.2") (d (list (d (n "mach_o_sys") (r "^0.1.1") (d #t) (k 0)))) (h "0c0gy7hxms7ygmkp45cxb4lp0frcwv524rvbpd36j5wv548ghk39")))

