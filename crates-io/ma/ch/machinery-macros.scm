(define-module (crates-io ma ch machinery-macros) #:use-module (crates-io))

(define-public crate-machinery-macros-0.1.0 (c (n "machinery-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1zn4l9kcr7yj0q4hl877z65vrpnb1sxcmqjl1x5v6zhh2v5lx8zm")))

(define-public crate-machinery-macros-0.2.0 (c (n "machinery-macros") (v "0.2.0") (d (list (d (n "murmurhash64") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "02f183pjccc10f5krk107lqfqk0jr9vf1v634kby50jjjkjbng06")))

(define-public crate-machinery-macros-0.3.0 (c (n "machinery-macros") (v "0.3.0") (d (list (d (n "murmurhash64") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0ia8mn2cl7d13jlikv1rkmgsrfwmylvy2k19n7pjjw0654adqccn")))

(define-public crate-machinery-macros-0.5.0 (c (n "machinery-macros") (v "0.5.0") (d (list (d (n "murmurhash64") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0xc138isx2nc80l30f3b33fggi6khxly24p93gmawjz7j9bprkwm")))

(define-public crate-machinery-macros-0.5.1 (c (n "machinery-macros") (v "0.5.1") (d (list (d (n "murmurhash64") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1jv9v6226pcfdca1bq10f33h94p602ichahxj5pjqdc2axqim3yd")))

