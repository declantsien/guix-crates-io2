(define-module (crates-io ma ch machinebox) #:use-module (crates-io))

(define-public crate-machinebox-0.1.0 (c (n "machinebox") (v "0.1.0") (d (list (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sazn882hdkkky06511kmcksy8kq1h3xridmyc04rijfpggl2rfj")))

(define-public crate-machinebox-0.2.0 (c (n "machinebox") (v "0.2.0") (d (list (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dbdlcxrzkds9fkj35nyxab47mvqzmfkm24i8r45q439k59dhfvw")))

(define-public crate-machinebox-0.3.0 (c (n "machinebox") (v "0.3.0") (d (list (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iakgkwfnww680d04fi7vhrsf2c4vcplahvbhiphjng29ghzf5na")))

(define-public crate-machinebox-0.4.0 (c (n "machinebox") (v "0.4.0") (d (list (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jlfd9s3mgp6mk2bxr9avbbdbxwsgds2qqmx02p0m5ky2ibs9rxs")))

(define-public crate-machinebox-0.5.0 (c (n "machinebox") (v "0.5.0") (d (list (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lcldcgknjifss84rr7drjz05fddzyy3hhkjlwrxyjm6gydjjqxs")))

