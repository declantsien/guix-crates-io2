(define-module (crates-io ma ch machine-prime) #:use-module (crates-io))

(define-public crate-machine-prime-1.0.0 (c (n "machine-prime") (v "1.0.0") (h "0fqyw67gmmy1yn3fyafkh3lnsh1n5aiz26vrnqbxg5s71mpx2fvl")))

(define-public crate-machine-prime-1.1.0 (c (n "machine-prime") (v "1.1.0") (h "1ll51gf1hij91cafi1his1y5fr0vgzqbixhkabh6qr34rcvidpl7") (f (quote (("tiny") ("small"))))))

(define-public crate-machine-prime-1.2.0 (c (n "machine-prime") (v "1.2.0") (h "12wsf45wnd3z0hhv6xr0qmi54n1mf7ca43j6x813ik631r23njzc") (f (quote (("tiny") ("small"))))))

(define-public crate-machine-prime-1.2.1 (c (n "machine-prime") (v "1.2.1") (h "0ibvfhs8f50z746bssslywx269pnk0ii90y7nk86agdx7g0jd64y") (f (quote (("tiny") ("small"))))))

(define-public crate-machine-prime-1.2.2 (c (n "machine-prime") (v "1.2.2") (h "10gq0cz7g5krzz2ap3x0kf966pcb990694q3z0xhyhjmwylgmc5l") (f (quote (("tiny") ("small"))))))

