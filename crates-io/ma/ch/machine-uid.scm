(define-module (crates-io ma ch machine-uid) #:use-module (crates-io))

(define-public crate-machine-uid-0.1.0 (c (n "machine-uid") (v "0.1.0") (h "1hdaziknip42ly6id986ks7c001rhbmdqapqzslrgjl3a80gs38l")))

(define-public crate-machine-uid-0.2.0 (c (n "machine-uid") (v "0.2.0") (d (list (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "04p20f3ddp6dvhnd61bxj41mavk21ljlv8vbsny8cwqakdq9a58z")))

(define-public crate-machine-uid-0.3.0 (c (n "machine-uid") (v "0.3.0") (d (list (d (n "winreg") (r "^0.11") (d #t) (t "cfg(windows)") (k 0)))) (h "1dlyv5j3cjwvm082lis3lqymir2jlsj42vr62xb0ciphp771l516")))

(define-public crate-machine-uid-0.4.0 (c (n "machine-uid") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (t "cfg(windows)") (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "winreg") (r "^0.11") (d #t) (t "cfg(windows)") (k 0)))) (h "1icm1v7vk96cjw4x9m75c3jf3yikba4ghv14jl67byki550c4l1p")))

(define-public crate-machine-uid-0.5.0 (c (n "machine-uid") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winreg") (r "^0.11") (d #t) (t "cfg(windows)") (k 0)))) (h "04d462bz32kkpcyj807yid5fv6l1vly7fr0wl3dxffpvqlk8n04g")))

(define-public crate-machine-uid-0.5.1 (c (n "machine-uid") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winreg") (r "^0.50") (d #t) (t "cfg(windows)") (k 0)))) (h "0aczd667b4wa4j3m1cw4fnj7gl5wbb58amqalw43n42hw872x3cm")))

