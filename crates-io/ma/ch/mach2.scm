(define-module (crates-io ma ch mach2) #:use-module (crates-io))

(define-public crate-mach2-0.4.0 (c (n "mach2") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1cawlzqlk2g2g4h20000g0kzi1sipm7sqw0kvmfs8xyvm1vsca9c") (f (quote (("unstable") ("default"))))))

(define-public crate-mach2-0.4.1 (c (n "mach2") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1s5dbscwk0w6czzvhxp9ix9c2djv4fpnj4za9byaclfiphq1h3bd") (f (quote (("unstable") ("default"))))))

(define-public crate-mach2-0.4.2 (c (n "mach2") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "02gpyq89rcrqdbz4hgp5bpjas21dllxfc70jgw8vj0iaxg6mbf8r") (f (quote (("unstable") ("default"))))))

