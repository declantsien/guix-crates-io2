(define-module (crates-io ma ch machine-ip) #:use-module (crates-io))

(define-public crate-machine-ip-0.1.0 (c (n "machine-ip") (v "0.1.0") (h "0nnssp468qg3qxxhsjd3ipry61ij2b5073x60g69p3gw0yvxngql")))

(define-public crate-machine-ip-0.2.0 (c (n "machine-ip") (v "0.2.0") (h "12sflgbjllpnim9d0sghy1gjbr17j66bhg0llwmqhzl0bxrdl5j1")))

(define-public crate-machine-ip-0.2.1 (c (n "machine-ip") (v "0.2.1") (h "1vc4xfaw3rslpmkdh7y66ay5y886b6366x8nn9sgnrhmqqlav5w1")))

