(define-module (crates-io ma ch machine-check-avr) #:use-module (crates-io))

(define-public crate-machine-check-avr-0.2.0-alpha.2 (c (n "machine-check-avr") (v "0.2.0-alpha.2") (d (list (d (n "ihex") (r "^3.0.0") (d #t) (k 0)) (d (n "machine-check") (r "=0.2.0-alpha.2") (d #t) (k 0)))) (h "0ygpjkbnalc6rj0084kipbyr1mk6hxdidzpzcj2jr8d8cf1xsqvm") (r "1.75")))

(define-public crate-machine-check-avr-0.2.0 (c (n "machine-check-avr") (v "0.2.0") (d (list (d (n "ihex") (r "^3.0.0") (d #t) (k 0)) (d (n "machine-check") (r "=0.2.0") (d #t) (k 0)))) (h "1ynv4cc1hni0gh59rqqfxzwq11k1zcraj8mac6379z5yh8cshl8w") (r "1.75")))

