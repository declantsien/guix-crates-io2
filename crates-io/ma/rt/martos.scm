(define-module (crates-io ma rt martos) #:use-module (crates-io))

(define-public crate-martos-0.1.0 (c (n "martos") (v "0.1.0") (d (list (d (n "sequential-test") (r "^0.2.4") (d #t) (k 2)))) (h "0nh5gxba2w6dfh7m6zza6dy4qr7df341k06w907sm3n8b473l93i") (f (quote (("default") ("c-library"))))))

(define-public crate-martos-0.2.0 (c (n "martos") (v "0.2.0") (d (list (d (n "esp-alloc") (r "^0.3.0") (d #t) (t "cfg(any(target_arch = \"riscv32\", target_arch = \"xtensa\"))") (k 0)) (d (n "sequential-test") (r "^0.2.4") (d #t) (k 2)))) (h "1smzwn5f8y424zsyrpdx3dx3i3lhh3wjisnr3np9y5qwbpm3kkpa") (f (quote (("default") ("c-library"))))))

