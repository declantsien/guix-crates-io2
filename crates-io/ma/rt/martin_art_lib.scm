(define-module (crates-io ma rt martin_art_lib) #:use-module (crates-io))

(define-public crate-martin_art_lib-0.1.0 (c (n "martin_art_lib") (v "0.1.0") (h "0n6klg2hyb09jiiv6m84ynkld58ws60q3cjbp3a5xmf4m6p37n1p") (y #t)))

(define-public crate-martin_art_lib-0.1.1 (c (n "martin_art_lib") (v "0.1.1") (h "1g2iijhi4a74bly0m7aclhy5xmv12wq22i4fk2ih0h2khkmwy7sg")))

(define-public crate-martin_art_lib-0.1.2 (c (n "martin_art_lib") (v "0.1.2") (h "1vbllb8i7ha8y3klh46zq8vcy9xbby4fzclfy43riw84y8gwk8d4")))

