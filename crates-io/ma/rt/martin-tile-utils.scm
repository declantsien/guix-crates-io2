(define-module (crates-io ma rt martin-tile-utils) #:use-module (crates-io))

(define-public crate-martin-tile-utils-0.1.0 (c (n "martin-tile-utils") (v "0.1.0") (h "117rsrm0c94qii9bp7xjx6sh5lh88pikam095ljn2r86lj73m6nj")))

(define-public crate-martin-tile-utils-0.1.1 (c (n "martin-tile-utils") (v "0.1.1") (h "0dvvqid0gmxksxwyz58jx7f9m9r9jb7sqdrh4nj13iwdgp94md6p") (r "1.65")))

(define-public crate-martin-tile-utils-0.1.2 (c (n "martin-tile-utils") (v "0.1.2") (h "0vlipi96p3mvyir0k3n8g5c7h7dnad29sq8ghxnac3inlpbdj214") (r "1.65")))

(define-public crate-martin-tile-utils-0.1.3 (c (n "martin-tile-utils") (v "0.1.3") (h "0r26hcyxmzvicl01apm5wdb7qgsrl3hz3mqm3lpsrc81mmqy7kii") (r "1.65")))

(define-public crate-martin-tile-utils-0.1.4 (c (n "martin-tile-utils") (v "0.1.4") (h "112kg41jsbvja33fvxyswswry8pzj98ia2dqcb5cing0lisbmzck") (r "1.65")))

(define-public crate-martin-tile-utils-0.1.5 (c (n "martin-tile-utils") (v "0.1.5") (h "01hnz2dy6lc4wn2kh6v9sb74wx7s9xp300wi502jkwci97ifvk90") (r "1.65")))

(define-public crate-martin-tile-utils-0.2.0 (c (n "martin-tile-utils") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)))) (h "0y8sq20gxnvmjiwnwr0qwkfvj46jczzaaz9mpygg2f472p4lg4b8") (r "1.65")))

(define-public crate-martin-tile-utils-0.3.0 (c (n "martin-tile-utils") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)))) (h "1s10lv050kkhwig13xswlbdl2xcfqmqyxxmwz5bpl3vjfbkvnqs3") (r "1.65")))

(define-public crate-martin-tile-utils-0.3.1 (c (n "martin-tile-utils") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)))) (h "1774pafs95hb9s0bfd6xcp4lla0fmdkz6s675q0zfwd7qv9adq5k") (r "1.65")))

(define-public crate-martin-tile-utils-0.4.0 (c (n "martin-tile-utils") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)))) (h "15zilmya6bw83151wl4zjp5ncn9y6zb0v8hfvcbg3qsrwyn9swy4") (r "1.70")))

(define-public crate-martin-tile-utils-0.4.1 (c (n "martin-tile-utils") (v "0.4.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)))) (h "13wi4rn96jvw5kk5hj81ij745ydx812nkpmx44cgng31ipzb39gd") (r "1.70")))

