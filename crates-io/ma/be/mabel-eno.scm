(define-module (crates-io ma be mabel-eno) #:use-module (crates-io))

(define-public crate-mabel-eno-0.4.3 (c (n "mabel-eno") (v "0.4.3") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "1a28s4mccwzndxa1cfy1x8mbil68y85gq62lfs77jfvwjig1qbb9")))

(define-public crate-mabel-eno-0.4.4 (c (n "mabel-eno") (v "0.4.4") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "0jgzfvlps0yf4gfmbbkgrwx5fxkykg86nl97dp9fnhhld19l23kv")))

