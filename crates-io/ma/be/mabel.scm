(define-module (crates-io ma be mabel) #:use-module (crates-io))

(define-public crate-mabel-0.0.1 (c (n "mabel") (v "0.0.1") (h "0mq691y51ci181s6sgaisn8dw15nja9p3km9ggg5npf10rf90pv0")))

(define-public crate-mabel-0.1.0 (c (n "mabel") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-art") (r "^0.3.8") (k 0)) (d (n "mabel-eno") (r "^0.4.4") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "0qfxmyq8qjfj6zi40spjrmxsx34y86j47sr854sabx1vxxvw51x9")))

(define-public crate-mabel-0.2.0 (c (n "mabel") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-art") (r "^0.3.8") (k 0)) (d (n "mabel-aseprite") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "mabel-eno") (r "^0.4.4") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "0b0kzvas7y3rajbbrpdkia3bgyybd8iyjy9rkf7gwbznqkdggmw0") (f (quote (("default" "aseprite")))) (s 2) (e (quote (("aseprite" "dep:mabel-aseprite"))))))

