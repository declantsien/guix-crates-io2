(define-module (crates-io ma nu manual-serializer) #:use-module (crates-io))

(define-public crate-manual-serializer-0.0.0 (c (n "manual-serializer") (v "0.0.0") (h "1bmgdvbc3idlrgmwwb9fjalks64jg11i3x18a6mjvgiv4y4kam8p")))

(define-public crate-manual-serializer-0.1.0 (c (n "manual-serializer") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "127098a66p5qkdpvllv33rhzcqgzr7cfrh6x9nl9aq2hwc8ywbg0")))

