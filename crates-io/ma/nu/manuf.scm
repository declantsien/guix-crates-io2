(define-module (crates-io ma nu manuf) #:use-module (crates-io))

(define-public crate-manuf-0.1.0 (c (n "manuf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)))) (h "0ipm0ax3lx0l6908wxs7amyr5gdasfadax7m2dxh5dg0127d38dk")))

(define-public crate-manuf-0.1.1 (c (n "manuf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)))) (h "1pjv7qfimvdxvsmpd5ay3gj5f8dmmgf14cfzkvhx73p954k08qr3")))

(define-public crate-manuf-0.2.0 (c (n "manuf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "0nbzkhjw1ziqyzdcy0hac7x9sa3c6h42yk9c6dz5n85ynk62ay20") (f (quote (("latest" "reqwest") ("default"))))))

