(define-module (crates-io ma rq marquee) #:use-module (crates-io))

(define-public crate-marquee-0.1.0 (c (n "marquee") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0k51a5apca5lz76p4lrfqhnhw777x8xvflykdn5lwms41csgdyac")))

(define-public crate-marquee-1.0.0 (c (n "marquee") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1pl1iv7xnq72q1ycc5qa8cqbmgf9hcixq912f2amyf3vgs9k5wqi")))

(define-public crate-marquee-1.1.0 (c (n "marquee") (v "1.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1r3fwi6d31ayg35mhbi6kxz0gvirfa1rvmwi18dq7vl6pra5mky9")))

(define-public crate-marquee-1.1.1 (c (n "marquee") (v "1.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1cdyvhx2l0y94k38rqg9rkhng2sbk329gqawqabnpqphc6cnxhzj")))

(define-public crate-marquee-1.1.2 (c (n "marquee") (v "1.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0jp2951g5dj62904npwcs3chl09wi6ms8dadwvpr36syzc7h2cgi")))

