(define-module (crates-io ma gn magnet-uri) #:use-module (crates-io))

(define-public crate-magnet-uri-0.1.0 (c (n "magnet-uri") (v "0.1.0") (d (list (d (n "serde_urlencoded") (r "^0.5.3") (d #t) (k 0)))) (h "1azcvxbp8f9nva90i72hj2km6ql09ivjv35v63rdsi3s1ljmz5xc")))

(define-public crate-magnet-uri-0.2.0 (c (n "magnet-uri") (v "0.2.0") (d (list (d (n "serde_urlencoded") (r "^0.5.3") (d #t) (k 0)))) (h "0i48b5m4ylx28vysxj1qxhiicndh3pnqacqwp0k4bnxq9djqmvbd")))

