(define-module (crates-io ma gn magnet_app) #:use-module (crates-io))

(define-public crate-magnet_app-0.0.1 (c (n "magnet_app") (v "0.0.1") (d (list (d (n "diesel") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "magnet_core") (r "^0.0.1") (d #t) (k 0)) (d (n "magnet_more") (r "^0.0.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2-diesel") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "089d4dwkhjhg1i4h7csf2nqaqjdvf76g7f2xl5ajmhjb6hlqh1lg")))

