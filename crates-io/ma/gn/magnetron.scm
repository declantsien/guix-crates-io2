(define-module (crates-io ma gn magnetron) #:use-module (crates-io))

(define-public crate-magnetron-0.1.0 (c (n "magnetron") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1x9wkr2663hgg2a5mgcp4vgky38zh8kdq6pjwgb9z91c890q557p") (r "1.61")))

(define-public crate-magnetron-0.1.1 (c (n "magnetron") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0z0s8763bxazrr7pvv5w45n1q9nhidw188id3pfwhh2csfzjq599") (r "1.61")))

(define-public crate-magnetron-0.2.0 (c (n "magnetron") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "10pi02ycrbf2sml223sljxdbglswb3yglkzp8lh9d4rha01kvifk") (r "1.61")))

(define-public crate-magnetron-0.3.0 (c (n "magnetron") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ijaw3apc6bac5g1cvsni1alb6jg31w7i3cnpfm9zwsgmqpjmyjb") (r "1.61")))

(define-public crate-magnetron-0.4.0 (c (n "magnetron") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1amgbz8cnr17vngpfbji1h4hdcsw6qar1clyyaja8pgidisr5nv6") (r "1.61")))

(define-public crate-magnetron-0.5.0 (c (n "magnetron") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nr5sf3fvrvzrzkw4v245yip1mf97q8h2vbsbhdmdqx7s8k0v90h") (r "1.66")))

