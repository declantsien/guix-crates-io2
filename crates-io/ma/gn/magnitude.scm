(define-module (crates-io ma gn magnitude) #:use-module (crates-io))

(define-public crate-magnitude-0.1.0 (c (n "magnitude") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1m2n0x6k89lv75mk4iz7vyfs7plkbclcjxlxr36df620am288dfg")))

(define-public crate-magnitude-0.1.1 (c (n "magnitude") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0gj2yd9fd9szi3nmiix0jqmpl8pxdaimh7bx4cmymcmj1b8f1yqz")))

(define-public crate-magnitude-0.2.0 (c (n "magnitude") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0984afikwc5zpxzdr59xm6m019bfmkjnq4n9l3wg0h0ap0dbx204")))

(define-public crate-magnitude-0.3.0 (c (n "magnitude") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0yn9alyw3irwgkrar8l1kazcl6sqwjni74hy4rb1n9cmaqjhix6j")))

(define-public crate-magnitude-0.3.1 (c (n "magnitude") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1k117qcgmci1fw4mabvif1wg8qhwdk1rzpzy60jl94nmkpipj5gd")))

(define-public crate-magnitude-0.3.2 (c (n "magnitude") (v "0.3.2") (d (list (d (n "num-traits") (r ">=0.2.12, <0.3.0") (d #t) (k 0)))) (h "1jyjpd76i3xdx74r99ymliha8c3krxwh0fbvprsx6xmj1gjlrdiv")))

