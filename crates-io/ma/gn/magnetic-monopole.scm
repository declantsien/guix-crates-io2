(define-module (crates-io ma gn magnetic-monopole) #:use-module (crates-io))

(define-public crate-magnetic-monopole-0.1.0 (c (n "magnetic-monopole") (v "0.1.0") (d (list (d (n "mathrs") (r "^0.0.6") (d #t) (k 0)))) (h "1n7h4bja1pcia4pa802bi8vignj3asaql8m2pnb52pc0vy8v2155") (y #t)))

(define-public crate-magnetic-monopole-0.1.1 (c (n "magnetic-monopole") (v "0.1.1") (d (list (d (n "mathrs") (r "^0.0.6") (d #t) (k 0)))) (h "15n0fgbyydjbbv07w3vqw0xdw1ndpbf93j3mc68jr05xi24fv5j7")))

(define-public crate-magnetic-monopole-0.2.0 (c (n "magnetic-monopole") (v "0.2.0") (d (list (d (n "mathrs") (r "^0.0.6") (d #t) (k 0)))) (h "0yp5hcjna9w2bx2rx4n68fq4kr7m9vrxch5bzh3fbhab0ziv6y0d")))

