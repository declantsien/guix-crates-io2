(define-module (crates-io ma gn magnet_more) #:use-module (crates-io))

(define-public crate-magnet_more-0.0.1 (c (n "magnet_more") (v "0.0.1") (d (list (d (n "liquid") (r "^0.8") (d #t) (k 0)) (d (n "magnet_core") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1b7ij7y68829j5dzg39qf07r242m6h968c67dm958911a9nxcr3h")))

