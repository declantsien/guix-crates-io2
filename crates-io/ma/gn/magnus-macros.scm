(define-module (crates-io ma gn magnus-macros) #:use-module (crates-io))

(define-public crate-magnus-macros-0.1.0 (c (n "magnus-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j4vilxz7xsw4p037h92c9prk0i2i1i1mmrihddy7xrfpv58z5i7")))

(define-public crate-magnus-macros-0.2.0 (c (n "magnus-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01cfy7wwiiz2r7h3c6ahs5nnl3hz3b48zd2wwyj6f3yb11lvmj5c")))

(define-public crate-magnus-macros-0.3.0 (c (n "magnus-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkq4d6rjkwwrbihxwyj1rxv2kiawnizcbjjjw61h1gazqxv4v10")))

(define-public crate-magnus-macros-0.4.0 (c (n "magnus-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1snzzfrk4vncf8yzs4aqsaglay753q58dqap27zk49qvi74p3al5")))

(define-public crate-magnus-macros-0.4.1 (c (n "magnus-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ywvfhni3rrncz9blswaxadady6gqrz757apm88w0hjlskqpmhbc")))

(define-public crate-magnus-macros-0.6.0 (c (n "magnus-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qvigw6ndq73fn6xiffaa52qfvidlhl5k7w18zv6a1cnw8hchs2r")))

