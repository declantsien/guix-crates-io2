(define-module (crates-io ma gn magnet_core) #:use-module (crates-io))

(define-public crate-magnet_core-0.0.1 (c (n "magnet_core") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "0r0jzghvwpggcaf3qscyry16azq0x9cvk7wzm72b89vf5wk631cy")))

