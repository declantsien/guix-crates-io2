(define-module (crates-io ma gn magnesia) #:use-module (crates-io))

(define-public crate-magnesia-0.1.0 (c (n "magnesia") (v "0.1.0") (h "0q4h793zhz4gmvzmxkd0ka7iwvd5gk1xrs1hkpdaxzn5izlrpgjh")))

(define-public crate-magnesia-0.1.1 (c (n "magnesia") (v "0.1.1") (h "0jzzbw3dmpwddqslj49gxn9n1sagi3mc3r7ccvyh9bbp097jf892")))

(define-public crate-magnesia-0.1.2 (c (n "magnesia") (v "0.1.2") (h "0h28968i1xh5s3acsfb8d15067m4hxgarb1sgn87q73fbiacs8vf")))

(define-public crate-magnesia-0.2.0 (c (n "magnesia") (v "0.2.0") (h "1w6rg2y8dkcp2q8y7lrdk9nmqlnz9lmj54rbcjn6j54jhwihz3p3")))

(define-public crate-magnesia-0.2.1 (c (n "magnesia") (v "0.2.1") (h "103p5f3y1bc3w6qdbj8ds1mjhjv7p3cj5x3ccp4lp2c2p1q4smhs")))

(define-public crate-magnesia-0.2.2 (c (n "magnesia") (v "0.2.2") (h "0q1nlcvmscsxqfmpi0qpvrqf6s1hmikjf46dnwszyrj5sq5vmsmm")))

(define-public crate-magnesia-0.3.0 (c (n "magnesia") (v "0.3.0") (h "1k0ab7sdzbd7lf4c1d4r2ir35i56jyl7qrnl6sxbc8j70sd4y8px")))

(define-public crate-magnesia-0.3.1 (c (n "magnesia") (v "0.3.1") (h "145xwywl7zgrn5l9hqy0x31i0r7zfwl6r7nwcrqmn5lnci4r7im5")))

(define-public crate-magnesia-0.3.2 (c (n "magnesia") (v "0.3.2") (h "0m4lh64ld2yv96zlk9zy6yxfd4yi8y1sh3yal7yxqkaarbzcd80a")))

(define-public crate-magnesia-0.3.3 (c (n "magnesia") (v "0.3.3") (h "1r1yhbqp1svj98afklf261pyminlkmgnal3kq8fcaxvhdc83dyzb")))

(define-public crate-magnesia-0.4.0 (c (n "magnesia") (v "0.4.0") (h "1gq6psxny2zfa15rbqgp4gp6l945nasjk798sial4babb6ri6gj6")))

(define-public crate-magnesia-0.4.1 (c (n "magnesia") (v "0.4.1") (h "1l1laqb1jvyj1bakvpcmmziba03i7w95sizjrhyg7lhhq8ag1ysy")))

(define-public crate-magnesia-0.5.0 (c (n "magnesia") (v "0.5.0") (h "00nhvs0b940gibiflq40l3bavn0kqsy45055x37s0adyx79va1a2")))

(define-public crate-magnesia-0.6.0 (c (n "magnesia") (v "0.6.0") (h "0x26z7h0my0rm0j726601f7pn3ggnvywc2bawkknmmcphbrqywh5")))

(define-public crate-magnesia-0.6.1 (c (n "magnesia") (v "0.6.1") (h "0irm2kbkf2c5vnm8dychhkpnb965m8y310fl6ikw48rncb1rv7lg")))

(define-public crate-magnesia-0.7.0 (c (n "magnesia") (v "0.7.0") (h "144lw7gasvsqnjncsihiz6bd4v8r57z6v0i0wask33dpdjrcmv9x")))

(define-public crate-magnesia-0.7.1 (c (n "magnesia") (v "0.7.1") (h "08bysa7ik4p62j10yib418z72xa3yzw94bw79wbx454ifczfqb6i")))

(define-public crate-magnesia-0.7.2 (c (n "magnesia") (v "0.7.2") (h "132w41gwxabfbqrx4jm9pfngc5n802i0ckf88lgacy48ll5d10fx")))

(define-public crate-magnesia-0.7.3 (c (n "magnesia") (v "0.7.3") (h "0n7hz8cyycvrsl32112dkbs1c3c134k6bix1k1wnpkkqv05ldkdr")))

(define-public crate-magnesia-0.7.4 (c (n "magnesia") (v "0.7.4") (h "1x8j77pdn6psbvls7xk6vm90mkyvw9dwrqmi73cd5i5q55g4b004")))

(define-public crate-magnesia-0.7.5 (c (n "magnesia") (v "0.7.5") (h "0ci7jqpfzcjghd3zd0hgw0wcx0zxzsl2hn7jwapm7f0jsp43n0iy")))

(define-public crate-magnesia-0.7.6 (c (n "magnesia") (v "0.7.6") (h "10nsmw31yc8i4i6v8r1whmnjkj2x3mfddpgsk8c7203xk69jk64x")))

(define-public crate-magnesia-0.7.7 (c (n "magnesia") (v "0.7.7") (h "0mzby2da3ri8vh0vyzydkhr7nr53ycrjymvw13xgii6i6kdvm9g6")))

(define-public crate-magnesia-0.7.8 (c (n "magnesia") (v "0.7.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0w47f1xabnilv0hwxcrbvcn0pnai4843ic6i3wmac240g8fnsypn")))

(define-public crate-magnesia-0.7.9 (c (n "magnesia") (v "0.7.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01m08ysnr5xi49nly3i9hcjmv3p0vplwkv8s8sdqz5qz2aim6mb3")))

(define-public crate-magnesia-0.8.0 (c (n "magnesia") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "194j71z71y6jan9a8qq163xkifsw8mzyyy48c83jax1g7jnnb9kh")))

(define-public crate-magnesia-0.8.1 (c (n "magnesia") (v "0.8.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dzkzvimyq4mhz5plrd1xjjpswikvg258yjs7n7q5y7jg86m83pa")))

(define-public crate-magnesia-0.8.2 (c (n "magnesia") (v "0.8.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "14y0vd3yisw52wmcj71h0d97kphy5gmi98q0783qyp8w42iqdm6v")))

(define-public crate-magnesia-0.9.0 (c (n "magnesia") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1s0pnl3dxdsrdm6grhfkjr9k9hxz680aqpqkhw03b8j1y43mqd5f")))

(define-public crate-magnesia-0.9.1 (c (n "magnesia") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1r6hlav269407602m9s2hw08raqdlmaalls0y891k36m7nr8nxsh")))

(define-public crate-magnesia-0.9.2 (c (n "magnesia") (v "0.9.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1n49mbg1lawh9y0dcnly3mnc0ig1nlay2g3ama4ll22v7wnqnwfd")))

(define-public crate-magnesia-0.9.3 (c (n "magnesia") (v "0.9.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i965xy5k3g1ib7fcyr6z8iz1davcyh1dkhsdql6hxi5rwcdnq05")))

(define-public crate-magnesia-0.10.0 (c (n "magnesia") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1pwgb20ay2sjjlklp59hm7hp4akwkg98p6s6ij7k4rygmbcsjxxn")))

(define-public crate-magnesia-0.11.0 (c (n "magnesia") (v "0.11.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zwdfyd97rxigrds3jd83x4qscq5pm5jbssab1l7kpms33d1cy9q")))

(define-public crate-magnesia-0.11.1 (c (n "magnesia") (v "0.11.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x5c54m4xrfwl2v7wdnm3g22nk85km1xhjrif4d36ndb579ygh24")))

