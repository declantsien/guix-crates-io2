(define-module (crates-io ma gn magnet_derive) #:use-module (crates-io))

(define-public crate-magnet_derive-0.1.0 (c (n "magnet_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "1bbw6an61r07lpxsgkw8iqfgcm8r8m0lbvs73jz0cqyq9ifbmr06")))

(define-public crate-magnet_derive-0.1.1 (c (n "magnet_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0ghlwah8smvfbgfarmqda6zidw1l8ixqsjng9x73ca2y04ar5g5l")))

(define-public crate-magnet_derive-0.1.2 (c (n "magnet_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0dpwr0c9yg1l86hcmxpda2zsxck5svk3p8d5mgwbxmimlr9c7sbi")))

(define-public crate-magnet_derive-0.1.3 (c (n "magnet_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "04vmw4yrq321n5gj98dhy4i6mqvc1ms51b147ag99hif95hgcxnp")))

(define-public crate-magnet_derive-0.1.4 (c (n "magnet_derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "04k8xi9r275p1f4qpl7varawq0a8mw8y16xfpjps8g4qfd6gym33")))

(define-public crate-magnet_derive-0.2.0 (c (n "magnet_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0m1p7nnsqh7wchncfxdvwxmjd7rx0xn69wix7v4bfv78l28wc6j1")))

(define-public crate-magnet_derive-0.2.1 (c (n "magnet_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.11") (d #t) (k 0)))) (h "0s579707rz8jzx3623ycddb06bwl0fbjv7rhikgnm08lkdxdsr4m")))

(define-public crate-magnet_derive-0.3.0 (c (n "magnet_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "012jr1rw1cxbmi974zsi1q337nj1gk0xjflhf25k2ipf1l1bmnzr")))

(define-public crate-magnet_derive-0.3.1 (c (n "magnet_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1mvjmkbhyldcms7yv6kmq01b2kqwj5rlwpk3322fwmb3np8r3rqz")))

(define-public crate-magnet_derive-0.3.2 (c (n "magnet_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "06zi5b0n25bjnhnjisg6yklla8rssf8zn2kcgkrx3n3s6fpw5kay")))

(define-public crate-magnet_derive-0.3.3 (c (n "magnet_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "0d17mjyj1jqxsy1fyk1bwgfgclgb97k8si6nkqqn8n30idv8nhbd")))

(define-public crate-magnet_derive-0.4.0 (c (n "magnet_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "1ap40n7h01ywmx3wzxxrmb1f2gn6x9had96qv9sg7vmzpi77vhcs")))

(define-public crate-magnet_derive-0.5.0 (c (n "magnet_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "1sr2la0gvkghjg9b85zi1shf556vbnrhz3aznj6p10n7js8k77wv")))

(define-public crate-magnet_derive-0.6.0 (c (n "magnet_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "01s7r82g68cqzmvxwak559jn48c1672c04rv80ng9gzqsbw0if65")))

(define-public crate-magnet_derive-0.7.0 (c (n "magnet_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "0gn503f4x4nkqkgw9hp4hvh7q4cj4kmmjswcxflsa2yrjp27ai9r")))

(define-public crate-magnet_derive-0.8.0 (c (n "magnet_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "1zf883066sxg8aw9dw34knz9cbgcq94jbcgcrm5xvalr8gy4b9wc")))

