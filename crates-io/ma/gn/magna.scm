(define-module (crates-io ma gn magna) #:use-module (crates-io))

(define-public crate-magna-0.1.3 (c (n "magna") (v "0.1.3") (h "1yms2s2i2wb6anx6yk64dgwyr8xcz5pkpq06hvph146grbj2xqx6") (y #t)))

(define-public crate-magna-0.1.4 (c (n "magna") (v "0.1.4") (h "1dn05ccxj5hb2szn6m0jmh60ym7hhds77163yhvl8iwn6qw70yaz") (y #t)))

(define-public crate-magna-0.1.77 (c (n "magna") (v "0.1.77") (h "09h4hk4jdl1rakzqwzbi2v9v357zf6icgxdp7a0g47f3l4qjfbsz") (y #t)))

(define-public crate-magna-0.77.0 (c (n "magna") (v "0.77.0") (h "0214bbh9qv704q5dgvf5s39impavwsa29li1w9cgyxzcgnavhc2l") (y #t)))

(define-public crate-magna-7.7.18 (c (n "magna") (v "7.7.18") (h "0v7kdjy55zxraqpy2k5688c84s3r489dlj4vzw75wx9b06hwdmlv") (y #t)))

(define-public crate-magna-2018.7.7 (c (n "magna") (v "2018.7.7") (h "007agpj6q3kxrglc063v15w4mn4ad7j6qzz71f8h1jk0sap9bij7") (y #t)))

(define-public crate-magna-2019.12.13 (c (n "magna") (v "2019.12.13") (h "10785c7axc9lwsk9092f3z4gfgh7k0szywrc5i9hayg83qji88k3") (y #t)))

(define-public crate-magna-9999.999.99 (c (n "magna") (v "9999.999.99") (h "1kcb2k4sj3nfhv7h37b2psh133jv6k6wlxj0ycs3ymzgxfws651c") (y #t)))

(define-public crate-magna-9.9.9 (c (n "magna") (v "9.9.9") (h "1scd6ilqp25x8p074945wxghq633m5nbqccr5ywdrw7id9qi9fn9") (y #t)))

(define-public crate-magna-99999.99999.99999 (c (n "magna") (v "99999.99999.99999") (h "1cxywmjy4ycaj13d5q0g1dw8xkq3mvilb84r0j8gpywzhmmakx3q") (y #t)))

(define-public crate-magna-9999999.9999999.9999999 (c (n "magna") (v "9999999.9999999.9999999") (h "13lrb3cwlz4p5zsmyy16gqps95xklaad4vhnk5vngxhb4p7dxcqg") (y #t)))

(define-public crate-magna-999999999.999999999.999999999 (c (n "magna") (v "999999999.999999999.999999999") (h "1j7h3yqyy3r700w3zvy0ys92jxyxkpsk0jv57hmrjhcg6iyqdn5m")))

