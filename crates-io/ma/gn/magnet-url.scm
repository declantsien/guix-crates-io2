(define-module (crates-io ma gn magnet-url) #:use-module (crates-io))

(define-public crate-magnet-url-1.0.0 (c (n "magnet-url") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18lzaaq2ml05x9j0kc7xl32h8rvymr59p5vd4lha5718zhzcmm36") (y #t)))

(define-public crate-magnet-url-1.0.1 (c (n "magnet-url") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vmab539gdyggi40n7mnp0kcgav4mpfvyz2ciazvq7a05fiib3yn") (y #t)))

(define-public crate-magnet-url-1.0.2 (c (n "magnet-url") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0miiyqa7kn49g4lk11m4p38f24zcjiy2r6rr6wkdvd487xb6l653") (y #t)))

(define-public crate-magnet-url-1.0.3 (c (n "magnet-url") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rqz8vcy1rlh3cv0w0k0dwpc9hshkxzrw4lz13cf8k0il2w4dfjv")))

(define-public crate-magnet-url-1.0.4 (c (n "magnet-url") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vxpb8yj5xbmwd9h7c32xmbbkwxy8xn2zzj46bh223y65inzxszh")))

(define-public crate-magnet-url-1.0.5 (c (n "magnet-url") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jgw27lsdxzwrpgpz39yp0l9fq6nrsskbjbmlgvca4p76gnf2fcm")))

(define-public crate-magnet-url-1.1.0 (c (n "magnet-url") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w7b13hakspwig4zsxj1nm0agaacvylv8h2wah8il1zbqqxhasdc")))

(define-public crate-magnet-url-1.1.1 (c (n "magnet-url") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "127h3vp6k3qniwykkfwbmbf81ymmffk97xfwqp7c53sqa53j7qiq")))

(define-public crate-magnet-url-1.2.0 (c (n "magnet-url") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x988phsrrnl8fkan9mjh0lsg7qqrcc5qiwskw66kr4sxbj1nffk")))

(define-public crate-magnet-url-1.2.1 (c (n "magnet-url") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1whbrch7cwx1f24vw0q8divp4rkc71vzlin8yf0pq2vf4wp3p0lg")))

(define-public crate-magnet-url-1.2.2 (c (n "magnet-url") (v "1.2.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h8x78d1n2ak41wa75rv33a574da9ac7asn8k3l2h4p7d56wm4p2")))

(define-public crate-magnet-url-2.0.0 (c (n "magnet-url") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x3xfdpag2p51i9rx8gcimigrj22aqp7hq0crh0cm2p80i04qfxc")))

