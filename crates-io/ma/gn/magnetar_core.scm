(define-module (crates-io ma gn magnetar_core) #:use-module (crates-io))

(define-public crate-magnetar_core-0.1.0 (c (n "magnetar_core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0f17i257i1w9z109yd9vj15ai4v3q62hkv2qq8y7ddalwg7i8smb")))

