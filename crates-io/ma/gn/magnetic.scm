(define-module (crates-io ma gn magnetic) #:use-module (crates-io))

(define-public crate-magnetic-1.0.0 (c (n "magnetic") (v "1.0.0") (h "1wl33nkbkr21a8hqzql1jw8qnqcr533y7rzad97pajz6fp0z90g3")))

(define-public crate-magnetic-1.0.1 (c (n "magnetic") (v "1.0.1") (h "1s5wihrfhjk2jv3s2ra9sms7fd8lgn34grxxnm68wr2yz6vv5ivl")))

(define-public crate-magnetic-1.0.2 (c (n "magnetic") (v "1.0.2") (h "1qp2832fllfqxidxrwp8g1hxfzlh184fddqnf82h7n6nsj39kikp")))

(define-public crate-magnetic-2.0.0 (c (n "magnetic") (v "2.0.0") (h "1cy0fda7vkby9xfv573aqmmdrizksdfgi4h8nhv9qv50f16n2rc7") (f (quote (("unstable"))))))

(define-public crate-magnetic-2.0.1 (c (n "magnetic") (v "2.0.1") (h "1y5fl099jyhmm962zpigga6fhvn7i7rhv1h0fvn5y7m5fmfzlkgl") (f (quote (("unstable"))))))

(define-public crate-magnetic-2.1.0 (c (n "magnetic") (v "2.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0x0k2bh75rwikk9p3nnwzvhj9c5hr7lyc6hkx6rxaij3w9angjh4") (f (quote (("unstable"))))))

(define-public crate-magnetic-2.2.0 (c (n "magnetic") (v "2.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0ng47hbsgd8pw489mhhybwv9bjlvlffai7lc9vmvz4kcz3bfgysl") (f (quote (("unstable"))))))

(define-public crate-magnetic-2.3.0 (c (n "magnetic") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0qsnw6r0jqmdzk1aqqxcgk911kf2z86y0qm6yykzd6zyvqdlqhj9") (y #t)))

(define-public crate-magnetic-2.4.0 (c (n "magnetic") (v "2.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0d9mrbsr2b1pxbmd9fhmm1gm9kyn1xjnfvga1hvipa6qkyss77nn")))

(define-public crate-magnetic-2.4.1 (c (n "magnetic") (v "2.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "01zahkp1mrf7b80r00nz76d4a1y0ylmc26dmcshq597npnqpix7x")))

