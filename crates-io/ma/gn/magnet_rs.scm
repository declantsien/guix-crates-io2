(define-module (crates-io ma gn magnet_rs) #:use-module (crates-io))

(define-public crate-magnet_rs-0.1.0 (c (n "magnet_rs") (v "0.1.0") (h "1j3mfxarn3dj7chc03zi5vf63bfri7ifv6v1xm8zn2ckzd5f5s87")))

(define-public crate-magnet_rs-0.2.1 (c (n "magnet_rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ahm26307gp62amnr1k6zw4wza89qskgkb13jca4ccbm0lfklgvr")))

(define-public crate-magnet_rs-0.2.2 (c (n "magnet_rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qa0a1xkc5mfhhwww1kwwg2qi2q9qmfbvgi92i4hp2ry3rzawmm0")))

(define-public crate-magnet_rs-0.2.3 (c (n "magnet_rs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lodestone_core") (r "^0.2") (d #t) (k 0)))) (h "0vxdmxwqyrprylchzf61s3f96g81jbbzj02d2s689q539bgxy8q4")))

