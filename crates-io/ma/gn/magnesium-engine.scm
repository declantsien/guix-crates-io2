(define-module (crates-io ma gn magnesium-engine) #:use-module (crates-io))

(define-public crate-magnesium-engine-0.0.1 (c (n "magnesium-engine") (v "0.0.1") (h "0v61ryvpdv09pnv5z5r8mq9a3g8dzh489wif9db1b835k4spfzl7")))

(define-public crate-magnesium-engine-0.0.2 (c (n "magnesium-engine") (v "0.0.2") (h "1i9hzmq07n09r2cn2x4c7vmxgmbkffn38bq63b4bgni2i7ak7kgv")))

