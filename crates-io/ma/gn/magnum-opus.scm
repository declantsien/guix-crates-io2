(define-module (crates-io ma gn magnum-opus) #:use-module (crates-io))

(define-public crate-magnum-opus-0.3.0 (c (n "magnum-opus") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opusic-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0rf8jqcg25ny4ycp846srzyc081v0mgz8qxfw72nhv4f278bb0l1")))

(define-public crate-magnum-opus-0.3.1 (c (n "magnum-opus") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opusic-sys") (r "^0.2.2") (d #t) (k 0)))) (h "1v9s1dmrlpn67ndnzb7wzy83zsa8fchqnjxvzwyn1g3h0wwk9k6y")))

(define-public crate-magnum-opus-0.3.2 (c (n "magnum-opus") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "opusic-sys") (r "^0.3") (d #t) (k 0)))) (h "1hpcld3qapa5bpc80j6jq4pm9l3sy5min7ic55mlgbhkfcmasjgz")))

