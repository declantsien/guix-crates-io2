(define-module (crates-io ma in main_loop) #:use-module (crates-io))

(define-public crate-main_loop-0.1.0 (c (n "main_loop") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16i9nh3xqzaw0n9lhglvql6dy5987z9l3m8s5bm6ab0fwkm99bxp")))

(define-public crate-main_loop-0.1.1 (c (n "main_loop") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mv3ipbh90c4yn37hffjdxi50qcw0lzarxginyhg4fzhy7ylr22z")))

(define-public crate-main_loop-0.1.2 (c (n "main_loop") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1nqd22c8qskmzk0d1x11jrbii11blj8a6j2shhjbhij2hh12xhw3")))

(define-public crate-main_loop-0.1.3 (c (n "main_loop") (v "0.1.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07i47xglci1d7cd9mi2x6b8avb2v06s4dab6nc8dv2byr4wsc6wn")))

(define-public crate-main_loop-0.2.0 (c (n "main_loop") (v "0.2.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bf98vbhpq39iknfd7qcbwa99rs02xkh6q5c9c4w9yfa257pl5ns")))

(define-public crate-main_loop-0.2.1 (c (n "main_loop") (v "0.2.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.9") (d #t) (k 0)))) (h "1lpabj3v40j99i46xxhhqgnqajvjn08p4ihm50rwg49fhwk93l9s") (f (quote (("use_winit") ("default" "use_winit"))))))

(define-public crate-main_loop-0.2.2 (c (n "main_loop") (v "0.2.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.9") (d #t) (k 0)))) (h "1zn2rnf9df56qqxvsnvqc9wqzd438dqrw2691a28dmic7qgfh2yg") (f (quote (("use_winit") ("default" "use_winit"))))))

(define-public crate-main_loop-0.2.3 (c (n "main_loop") (v "0.2.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.10") (d #t) (k 0)))) (h "0yvv68gfvl6v8hwyi36zri0jripz320mq4813nxi3b69clzmx8sq") (f (quote (("use_winit") ("default" "use_winit"))))))

(define-public crate-main_loop-0.2.4 (c (n "main_loop") (v "0.2.4") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0blh3zmk1wz2v4kdrr5qhdj44bai2b11k1nsxnhihgi09xah84nq") (f (quote (("default" "winit"))))))

(define-public crate-main_loop-0.3.0 (c (n "main_loop") (v "0.3.0") (d (list (d (n "glutin") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0fllbiyks2waj0x27bc63cx8ga2iyak7ikdwx7lbkkxbnf480ysq") (f (quote (("default" "glutin" "winit"))))))

(define-public crate-main_loop-0.3.1 (c (n "main_loop") (v "0.3.1") (d (list (d (n "glutin") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)))) (h "0rsd6cb44vymycbm25wnzi1rjsfgpywmna4javw7q8gwl0qshcv7") (f (quote (("default" "glutin" "winit"))))))

(define-public crate-main_loop-0.3.2 (c (n "main_loop") (v "0.3.2") (d (list (d (n "glutin") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)))) (h "09gya76lg8axm0qwybwcdqzizq0lhjxljhvx3m7xwxydwxynzljm") (f (quote (("default" "glutin" "winit"))))))

(define-public crate-main_loop-0.3.3 (c (n "main_loop") (v "0.3.3") (d (list (d (n "gl") (r "^0.11") (d #t) (k 2)) (d (n "glutin") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r ">= 0.0, < 1.0.0") (o #t) (d #t) (k 0)))) (h "0p7h7kfh7bqfv0wc9fh3yq7y665grk7sjpph3cvm0cmxvh5ls0sx") (f (quote (("default" "glutin" "winit"))))))

