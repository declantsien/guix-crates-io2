(define-module (crates-io ma in main_error) #:use-module (crates-io))

(define-public crate-main_error-0.1.0 (c (n "main_error") (v "0.1.0") (h "017dbrcn2q4d62b5nzlk8xj6idmpvx5257c5drnzx62dnh7xy5im")))

(define-public crate-main_error-0.1.1 (c (n "main_error") (v "0.1.1") (h "0r4a13lgh2513g9dzcj2h6k1w65mgklg1cfx1ax6l2rf50gbnqxv")))

(define-public crate-main_error-0.1.2 (c (n "main_error") (v "0.1.2") (h "06kxls6md5vhl9za5b1g3hazgq8f55dasbzkdd2ywibfdklbap8m")))

