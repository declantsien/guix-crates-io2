(define-module (crates-io ma t2 mat2image) #:use-module (crates-io))

(define-public crate-mat2image-0.1.0 (c (n "mat2image") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "opencv") (r "^0.64") (f (quote ("rgb"))) (k 0)) (d (n "opencv") (r "^0.64") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "094l9zz307v4gsp36pfndwr65ib2vcy0phdagbfjah6p3h2ccbds") (f (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.1.1 (c (n "mat2image") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "opencv") (r "^0.66") (f (quote ("rgb"))) (k 0)) (d (n "opencv") (r "^0.66") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0471qrc6j1bv5da7mkf3160q7vwbq172b2lasvmr43501j5f6zy6") (f (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.1.2 (c (n "mat2image") (v "0.1.2") (d (list (d (n "image") (r "^0.24") (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "opencv") (r "^0.68") (f (quote ("rgb"))) (k 0)) (d (n "opencv") (r "^0.68") (f (quote ("imgcodecs"))) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "141ppxwkiwh99n95mcgkg5bnm7vw0x3pvb1pasyfmpc36r6c93x2") (f (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.2.0 (c (n "mat2image") (v "0.2.0") (d (list (d (n "image") (r "^0.24") (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "opencv") (r "^0.74") (f (quote ("rgb"))) (k 0)) (d (n "opencv") (r "^0.74") (f (quote ("imgcodecs"))) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vgp7pqfa5v59vjim9l9h2szziwlfg7ry4x57pbsk37gfcbnh7vy") (f (quote (("default" "rayon"))))))

