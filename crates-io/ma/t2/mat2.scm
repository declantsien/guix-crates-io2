(define-module (crates-io ma t2 mat2) #:use-module (crates-io))

(define-public crate-mat2-0.1.0 (c (n "mat2") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "103z5ssh27brlag0fm5932k332xiy0xifx3avv7a34ljimgzzc67")))

(define-public crate-mat2-0.1.1 (c (n "mat2") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1h4ffc7lcjv7b4qfkwfnmp2wx89sbrrmkpy72b6cy36ix93vm9k3")))

(define-public crate-mat2-0.1.2 (c (n "mat2") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "11ins3zj2xz68yqy2kvz5m4h9c7j38drq231iaf2pv6j3kyscbmj")))

(define-public crate-mat2-0.1.3 (c (n "mat2") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1nbig5xb1cvf46qimqrs28gk1921farqh0rzdlzg5g0flhk5liv3")))

(define-public crate-mat2-0.1.4 (c (n "mat2") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1dfmxsdin9i3zl5qn2bxbhdp2v12ij3hxj80jprld0f0yj9gnbzp")))

(define-public crate-mat2-0.2.0 (c (n "mat2") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02am0v14wkjashblccaisrj710fcpkcphsyw9dgfpbdf9w0mv17m")))

(define-public crate-mat2-0.2.1 (c (n "mat2") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "075jq65xmh07npd7k1va14g45syi8bv58n460fxhv811i88hh0pi")))

