(define-module (crates-io ma ix maixnor-super-amazing-component-lib) #:use-module (crates-io))

(define-public crate-maixnor-super-amazing-component-lib-0.1.0 (c (n "maixnor-super-amazing-component-lib") (v "0.1.0") (d (list (d (n "gloo-net") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.20") (f (quote ("csr"))) (d #t) (k 0)))) (h "1765ralvr812yspadnvb2zbdj0lv05vy5qgbsz3010nwqf2g52sq")))

