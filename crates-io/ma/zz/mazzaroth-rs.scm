(define-module (crates-io ma zz mazzaroth-rs) #:use-module (crates-io))

(define-public crate-mazzaroth-rs-0.1.0-alpha (c (n "mazzaroth-rs") (v "0.1.0-alpha") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0bx95z6ss9xbhsba4227cd4qiqg0mljz0z1fa829pndiww2vrmv5") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.2.0 (c (n "mazzaroth-rs") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.2.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.0") (d #t) (k 0)))) (h "1z6zgdk75xwilkilwm9wl4rmlv3ps6hciy3aq506bqm402x9ndjn") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.2.1 (c (n "mazzaroth-rs") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.2.5") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.3") (d #t) (k 0)))) (h "08ma9y6zdr37z021x4n0ah8qmcfkl04nsc50lkl3ajpzxibnnwzl") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.2.2 (c (n "mazzaroth-rs") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.2.7") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)))) (h "1xcaxm49ag0qnkbjx3nnkf47aibbxzvhrxvjpi3vbhjpy7aw52nq") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.2.3 (c (n "mazzaroth-rs") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.2.7") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)))) (h "0jrbj7xzwp1vpy6a6qf32vvvjz3vbq3l7dlfvp4d4sdsn6k5j8rm") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.4.0 (c (n "mazzaroth-rs") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.4.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)))) (h "0il4vvsvkdcqjgh3h52grhqcqjijpx2dm9fs3m27ddlihy2223lk") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.5.0 (c (n "mazzaroth-rs") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.5.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "0g70g8vxvq66lw8xysrah9hjzmq6a9pi4vgv3w1lfxjrgc7y4sqi") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.6.0 (c (n "mazzaroth-rs") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.6.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "0s0j4gg9l03way8cnrjh2xi64rch070ijh0qbyig05b7j71f5ap9") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.7.0 (c (n "mazzaroth-rs") (v "0.7.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.7.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "11hfq3y64cjxksmafgqgr2zz31lkili76f74q7j4r1lm7q5vdzm2") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.8.0 (c (n "mazzaroth-rs") (v "0.8.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "17mb8l2ic7fykqmikigy4h1wagpd7zpphh4x2j5rgf9amym6bl89") (f (quote (("host-mock"))))))

(define-public crate-mazzaroth-rs-0.8.1 (c (n "mazzaroth-rs") (v "0.8.1") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "mazzaroth-xdr") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.1") (d #t) (k 0)))) (h "1q7wnlgq1nsp2i1zj0sqrhpb8prg2x7h9a4ph2l1mfv22j84r4yg") (f (quote (("host-mock"))))))

