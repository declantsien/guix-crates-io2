(define-module (crates-io ma zz mazzaroth-xdr) #:use-module (crates-io))

(define-public crate-mazzaroth-xdr-0.1.0-alpha (c (n "mazzaroth-xdr") (v "0.1.0-alpha") (d (list (d (n "xdr-rs-serialize") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0kjggvh25a31m7d2js8fvbyq5gmn7j6y6ag3d2yiv7a6fxinsyfp")))

(define-public crate-mazzaroth-xdr-0.2.2 (c (n "mazzaroth-xdr") (v "0.2.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.0") (d #t) (k 0)))) (h "02383zlwf9ibqfq5my6nd9xn3cpd34f570w6y810hr671rjwxa87")))

(define-public crate-mazzaroth-xdr-0.2.3 (c (n "mazzaroth-xdr") (v "0.2.3") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0rx70ln9kmm9cc3aw6v01dh83gsnw8yb90lhzprl4ib8v66hlxgq")))

(define-public crate-mazzaroth-xdr-0.2.5 (c (n "mazzaroth-xdr") (v "0.2.5") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.3") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.3") (d #t) (k 0)))) (h "1h2ar03ij2iidapr3sfw4g310xzjg0hxjwc9ll2chd9i40yf7362")))

(define-public crate-mazzaroth-xdr-0.2.6 (c (n "mazzaroth-xdr") (v "0.2.6") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "06fkaid3nf9rrb22qvn3khsvfnlszh5m2rxh62ymrfn9s8hk3i1m")))

(define-public crate-mazzaroth-xdr-0.2.7 (c (n "mazzaroth-xdr") (v "0.2.7") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "08mnhrfgk1h36fla96a6fnkz6y3z1nkmc0vwljdqykqqs60c0la5")))

(define-public crate-mazzaroth-xdr-0.3.0 (c (n "mazzaroth-xdr") (v "0.3.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "1scacjpbml95hgh9chk7p0q7crnfmii561gjc6g2gv38p3w67fib")))

(define-public crate-mazzaroth-xdr-0.4.0 (c (n "mazzaroth-xdr") (v "0.4.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "0lp4x2ajfplwxnsg5d34s3wmzcv8ghwrs0p5838vqf697i7gvwfw")))

(define-public crate-mazzaroth-xdr-0.4.1 (c (n "mazzaroth-xdr") (v "0.4.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "1r6qyhwblng201kww2nxxf5qgac31psrkx63fbj1f6ad07hkly7m")))

(define-public crate-mazzaroth-xdr-0.4.2 (c (n "mazzaroth-xdr") (v "0.4.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "09asn9kwq0rdi0za7p1nf4jsbcbxaj9pw1hsvpi4zjny5wbznb3n")))

(define-public crate-mazzaroth-xdr-0.4.3 (c (n "mazzaroth-xdr") (v "0.4.3") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.2.5") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 0)))) (h "0760fbchigk8jxsdcdf3kjwmyir6i0ic6wy8bq9zms348cnc2kqq")))

(define-public crate-mazzaroth-xdr-0.5.0 (c (n "mazzaroth-xdr") (v "0.5.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1vxac0mnsg4d96rhlsqdgfk832c767z39ap4lfpvyn699j04sz8l")))

(define-public crate-mazzaroth-xdr-0.6.0 (c (n "mazzaroth-xdr") (v "0.6.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1pxidnjgqdygn24w5sa51prvm664bq432lm0gj68q0dd03pgirv3")))

(define-public crate-mazzaroth-xdr-0.6.1 (c (n "mazzaroth-xdr") (v "0.6.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "08zn6krx8ics4pc17rssic4gb9nphjg4n4ra54aa1xsgsa7qi10n")))

(define-public crate-mazzaroth-xdr-0.6.2 (c (n "mazzaroth-xdr") (v "0.6.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "19xlapg5ba0ydbf6n34rgzdiff4zmpjwv0dd49q73nzcpysimlf1")))

(define-public crate-mazzaroth-xdr-0.7.0 (c (n "mazzaroth-xdr") (v "0.7.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1rcv909v4dxc6mn0n64z3bcgzmhckzvxxmsxh4qggbak3sbz8jq7")))

(define-public crate-mazzaroth-xdr-0.7.1 (c (n "mazzaroth-xdr") (v "0.7.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1nk58lr6ch9cxal1dzm9ykc9cc8g11x04bx1y89bvivdi95cyzjl")))

(define-public crate-mazzaroth-xdr-0.7.2 (c (n "mazzaroth-xdr") (v "0.7.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0p6zx8dhplwbgs5mwcpwyslwxmqyw21rblm5cvj1m0fqy7ci4fqc")))

(define-public crate-mazzaroth-xdr-0.8.0 (c (n "mazzaroth-xdr") (v "0.8.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1hkprf52cbrlqahs54wprzy3j2w9ss1acdhq4jn8vny20ql49lii")))

(define-public crate-mazzaroth-xdr-0.8.1 (c (n "mazzaroth-xdr") (v "0.8.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0vb22n2md8hmq969bxqbs5hkxarh4ifjwjg692jal18w1dh36ck7")))

(define-public crate-mazzaroth-xdr-0.8.2 (c (n "mazzaroth-xdr") (v "0.8.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.3.1") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.1") (d #t) (k 0)))) (h "0bwhckn9qzh36mykp2b63l6595rpxzsk6mjsq7l4rmcfqp3swvk0")))

