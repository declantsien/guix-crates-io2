(define-module (crates-io ma te material-code-icons) #:use-module (crates-io))

(define-public crate-material-code-icons-0.1.0 (c (n "material-code-icons") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "0iksdvhmd2csz8rqfwwq4jbymcg7qazsxmfzj554hlbhx0shdc8h")))

