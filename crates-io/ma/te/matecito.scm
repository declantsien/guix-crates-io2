(define-module (crates-io ma te matecito) #:use-module (crates-io))

(define-public crate-matecito-0.1.0 (c (n "matecito") (v "0.1.0") (d (list (d (n "matecito-dll") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "0qywllgsga1zj2k79idmmidvp6hz0abkjizcs6rdz9sqg8jsxshy")))

(define-public crate-matecito-0.1.5 (c (n "matecito") (v "0.1.5") (d (list (d (n "matecito-dll") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "1hv6d88zhp7nkff5xfxgnpr00l3vwfck20nxl7lxq95r367zjsn5")))

(define-public crate-matecito-0.1.6 (c (n "matecito") (v "0.1.6") (d (list (d (n "matecito-dll") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "08wdzqwnnp7gw6v3dr5mpqqd29zx48icf82phdf7jzzczplwsd3d")))

