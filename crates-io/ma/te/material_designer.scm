(define-module (crates-io ma te material_designer) #:use-module (crates-io))

(define-public crate-material_designer-0.1.0 (c (n "material_designer") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_asset_ron") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0zx5afbswc7wi19gc7fv1n603m2c71yncja708hl63sqclbwi540")))

(define-public crate-material_designer-0.5.0 (c (n "material_designer") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_asset_ron") (r "^0.2") (d #t) (k 0)) (d (n "bevy_jpeg2k") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "04arcrm77282c1c5zxgms7j76ncylyk5nfg4viqp75x62ghfyh5q")))

