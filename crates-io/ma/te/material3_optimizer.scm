(define-module (crates-io ma te material3_optimizer) #:use-module (crates-io))

(define-public crate-material3_optimizer-0.1.0 (c (n "material3_optimizer") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1v2ml66czcdj2ni9pniazj6d8xqxadr1p1viarp5cjh7bxkq39x8")))

