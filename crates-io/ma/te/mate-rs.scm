(define-module (crates-io ma te mate-rs) #:use-module (crates-io))

(define-public crate-mate-rs-0.1.0 (c (n "mate-rs") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0m602cjkdckg1sqjbbval8cl1ha7m3zyalnjm7k04411jv034hh0")))

(define-public crate-mate-rs-0.1.1 (c (n "mate-rs") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0vhsycqmq2a3kfs7b2fwnbvlclxns9pyymn0m1vhgynpdrpvvbyg")))

(define-public crate-mate-rs-0.1.2 (c (n "mate-rs") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0hslxqhkirfyw15hrw8l8306x3aaq1h2h9ij12w9q8vq7c50h1lx")))

(define-public crate-mate-rs-0.1.3 (c (n "mate-rs") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1x5zr5p43m1k0h28w55b37xcfgx60kblmymqa153mcxpjcz5rafv")))

(define-public crate-mate-rs-0.1.4 (c (n "mate-rs") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0s249gxkpbsfchpm42s6c5l27c31kna6wl4cpx5dm9jwsip4zhy8")))

