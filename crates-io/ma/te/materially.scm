(define-module (crates-io ma te materially) #:use-module (crates-io))

(define-public crate-materially-0.1.0 (c (n "materially") (v "0.1.0") (h "0g9mhsvcd2q3x8cgk1q4azhj39g071maifb5fn9q0ngbjppvd5qy")))

(define-public crate-materially-0.1.1 (c (n "materially") (v "0.1.1") (h "0xr01z3ia1n3jmq9dl1wjrg8pmy2zqhcfrjm9rnzrwd2yf8s87qk")))

(define-public crate-materially-1.0.0 (c (n "materially") (v "1.0.0") (h "1zrgljk7zmrja6zimmq18xnl70nkrcq7xx8gqmaimpn5a993xs7h") (r "1.38")))

