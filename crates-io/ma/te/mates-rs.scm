(define-module (crates-io ma te mates-rs) #:use-module (crates-io))

(define-public crate-mates-rs-1.0.0 (c (n "mates-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "cursive") (r "^0.17") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)) (d (n "vobject") (r "^0.7") (d #t) (k 0)))) (h "1wlk3nvf1cndd70ngp1s6xrsz02dcdbdbvvcbw7p6fyrxmxapnrg")))

(define-public crate-mates-rs-1.0.1 (c (n "mates-rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.2") (d #t) (k 0)) (d (n "cursive") (r "^0.17") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)) (d (n "vobject") (r "^0.7") (d #t) (k 0)))) (h "1nrf3krv53zpxcai3vlf9c6002yy3lx5i2r6yq37jxay51ycjib1")))

