(define-module (crates-io ma te matext4cgmath) #:use-module (crates-io))

(define-public crate-matext4cgmath-0.1.0 (c (n "matext4cgmath") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "katexit") (r "^0.1.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0y4ijiw6d4nkl1sz88qxrjlyzl534cc489gbmkq70zz33lc1wm76")))

