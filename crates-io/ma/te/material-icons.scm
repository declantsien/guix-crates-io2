(define-module (crates-io ma te material-icons) #:use-module (crates-io))

(define-public crate-material-icons-0.1.0 (c (n "material-icons") (v "0.1.0") (h "1d33cjdc7gsivlm9asmsacqdqhi87pfc0b15cffjxl64ngw9qv8k")))

(define-public crate-material-icons-0.2.0 (c (n "material-icons") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^0.8.1") (d #t) (k 0)))) (h "1cx4j02fnc8z3plw3chqxyyz0pdq5kbdi5bfm3hf504whnjnpink")))

