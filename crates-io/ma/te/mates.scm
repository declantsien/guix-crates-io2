(define-module (crates-io ma te mates) #:use-module (crates-io))

(define-public crate-mates-0.1.1 (c (n "mates") (v "0.1.1") (d (list (d (n "atomicwrites") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)) (d (n "vobject") (r "^0.1") (d #t) (k 0)))) (h "1n1fbxrw6nd2k6x9g4h45xx5i29nlgvbk4ln3wipd52iy07p7ari")))

(define-public crate-mates-0.1.2 (c (n "mates") (v "0.1.2") (d (list (d (n "atomicwrites") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)) (d (n "vobject") (r "^0.1") (d #t) (k 0)))) (h "003chj5qfb7nh30l0ndvp0hmn7i2917zlagar57c3hqzcrdcdm26")))

(define-public crate-mates-0.1.3 (c (n "mates") (v "0.1.3") (d (list (d (n "atomicwrites") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)) (d (n "vobject") (r "^0.1") (d #t) (k 0)))) (h "08gf5nivdca7qd0gfl73xpzcfcz9fm753f7z2iypryrhvnhkjncg")))

(define-public crate-mates-0.1.4 (c (n "mates") (v "0.1.4") (d (list (d (n "atomicwrites") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)) (d (n "vobject") (r "^0.1") (d #t) (k 0)))) (h "18w7zs75vs49gxnpy8shy9r746csz7yisl59agd3dyh0lr3wkfj8")))

(define-public crate-mates-0.2.0 (c (n "mates") (v "0.2.0") (d (list (d (n "atomicwrites") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cursive") (r "^0.5") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)) (d (n "vobject") (r "^0.2") (d #t) (k 0)))) (h "12ri5rqsil265bbfrjxk7c09lxzvjjhcf3lxjk54gcinlablyjhz")))

(define-public crate-mates-0.3.0 (c (n "mates") (v "0.3.0") (d (list (d (n "atomicwrites") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 1)) (d (n "cursive") (r "^0.5") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)) (d (n "vobject") (r "^0.2") (d #t) (k 0)))) (h "0pnq9rl6hrnj2a65lfmjj0xy2j9abbhghhk4bi6kmpdb12zblk98")))

