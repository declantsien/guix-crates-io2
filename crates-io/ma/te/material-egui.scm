(define-module (crates-io ma te material-egui) #:use-module (crates-io))

(define-public crate-material-egui-0.1.0 (c (n "material-egui") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "14862w1cz5k35akkvp0macyf6qlgbichd0sb49bswh8p4rjidf3n")))

(define-public crate-material-egui-0.1.1 (c (n "material-egui") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "09pqx3lcfizrrcs3kacwppdldj19mcn10wgk5vra5alqn30ivihb")))

(define-public crate-material-egui-0.1.2 (c (n "material-egui") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "079pcxmw80b031r4xrnz80v908cf78ppc52s7p7vyqhj7xk3abqf")))

(define-public crate-material-egui-0.1.3 (c (n "material-egui") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "05nx6ia35f8ac2l6qdl9px7bcmwrapbm3p3d4lfb8k92r4gh6wl0")))

(define-public crate-material-egui-0.1.5 (c (n "material-egui") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (d #t) (k 0)) (d (n "egui") (r "^0.27.0") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "0w39c93yc83ac8x4rnh00gnc0dkm8721c9g40ifriy7f57yyb1pc")))

(define-public crate-material-egui-0.1.6 (c (n "material-egui") (v "0.1.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (d #t) (k 0)) (d (n "egui") (r "^0.27.0") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "0azv6wqps1amwl1aibq3808y0gm3235czkbc4sk3b9fkvlq5xq1j")))

(define-public crate-material-egui-0.1.7 (c (n "material-egui") (v "0.1.7") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "eframe") (r "^0.27.2") (d #t) (k 2)) (d (n "egui") (r "^0.27.2") (d #t) (k 0)) (d (n "egui") (r "^0.27.2") (d #t) (k 2)) (d (n "material-colors") (r "^0.2.1") (k 0)))) (h "1x8fpldx96a4pvmg40ml2nr3r6grxy9mwrc0mmv06bpg1gplcby3")))

