(define-module (crates-io ma te matex) #:use-module (crates-io))

(define-public crate-matex-0.1.0 (c (n "matex") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0yyra41y0r6n75va7lv9wms7r2ph57zdhl2dfcjgw2mlnk1gl67p") (y #t)))

(define-public crate-matex-0.2.0 (c (n "matex") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1c8wg0s89g8rivx2yilcpi4w4kkph49fa1g2mjgkpy18mn4mbbdc") (y #t)))

