(define-module (crates-io ma te material_rs_color_utilities) #:use-module (crates-io))

(define-public crate-material_rs_color_utilities-0.1.0 (c (n "material_rs_color_utilities") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1zdxbw7pw5k0lny3na6la8z3i1fz3cd3hhx71j60v3hkrh9hwhxj")))

(define-public crate-material_rs_color_utilities-0.1.1 (c (n "material_rs_color_utilities") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "04vv92v92bx8jypjzw41iv5pgv878r9aq5h1yj5f3628ngxz9xfs")))

