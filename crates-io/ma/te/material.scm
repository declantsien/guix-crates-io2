(define-module (crates-io ma te material) #:use-module (crates-io))

(define-public crate-material-0.1.0 (c (n "material") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "1rqv2dns5rda6ciw61pf6cga23h4krwzq5jc3nmizlwizhj5fdaj")))

(define-public crate-material-0.1.1 (c (n "material") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1lw2ilsamfs6g7ixmrm9h4nr7rpbwr95sixgdwfpk25xdyzp1y83") (f (quote (("cli" "ratatui" "crossterm"))))))

