(define-module (crates-io ma te material-symbols) #:use-module (crates-io))

(define-public crate-material-symbols-0.1.0 (c (n "material-symbols") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "syn") (r "^2") (d #t) (k 1)))) (h "0bvfyfjzddcsmfwm2di4isdywrf80nym23akv96p612z93xhr252")))

