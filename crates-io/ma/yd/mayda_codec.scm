(define-module (crates-io ma yd mayda_codec) #:use-module (crates-io))

(define-public crate-mayda_codec-0.1.0 (c (n "mayda_codec") (v "0.1.0") (d (list (d (n "mayda_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 0)))) (h "14ppajfdkg363db1g197api6f47xggga4r0dclg202xz7dim9r55")))

(define-public crate-mayda_codec-0.1.1 (c (n "mayda_codec") (v "0.1.1") (d (list (d (n "mayda_macros") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 0)))) (h "10fq4nc842hzl3mwmipg5yn2z3m75pgjsbs3ppy3ls5nsrs3913m")))

(define-public crate-mayda_codec-0.1.2 (c (n "mayda_codec") (v "0.1.2") (d (list (d (n "mayda_macros") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 0)))) (h "14pa1814w27lqi51xkbqfv2k62jqsg2qw6prfsdk5n71plpnvfrk")))

(define-public crate-mayda_codec-0.1.3 (c (n "mayda_codec") (v "0.1.3") (d (list (d (n "mayda_macros") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 0)))) (h "0x4h2vd1ng484yh2ddix9il9ajijhcl020pirrb8kyyf4vsvni9h")))

(define-public crate-mayda_codec-0.1.4 (c (n "mayda_codec") (v "0.1.4") (d (list (d (n "mayda_macros") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.2") (d #t) (k 0)))) (h "1558ckfap2pqzjkxxgdi2vkakgai7pqk6sd3rvbbjc20r6ykh0n8")))

