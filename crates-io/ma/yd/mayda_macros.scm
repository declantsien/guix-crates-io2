(define-module (crates-io ma yd mayda_macros) #:use-module (crates-io))

(define-public crate-mayda_macros-0.1.0 (c (n "mayda_macros") (v "0.1.0") (h "0dd2gbgzaqnn5k9aghyjwy77hmwla4kq9qzans88x3s9j5zpgwz3")))

(define-public crate-mayda_macros-0.1.1 (c (n "mayda_macros") (v "0.1.1") (h "1d6p1n0nmxba0ar2z2sc63c6bp4kw9lwqn9nxxbcfawv7ggncgzy")))

(define-public crate-mayda_macros-0.1.2 (c (n "mayda_macros") (v "0.1.2") (h "1dy9rr6096p8vv0gwgl86pmw6nmp5igfhwfrzdlnpdzz63nvda0k")))

(define-public crate-mayda_macros-0.1.3 (c (n "mayda_macros") (v "0.1.3") (h "18azi98l4cgqinsrgw24zfd5w0cirlyz7f80k7w9d1wwy1i095s9")))

(define-public crate-mayda_macros-0.1.4 (c (n "mayda_macros") (v "0.1.4") (h "1mghv2jynvm56nf90r4wra94vjwrzh9nf47a2r9nna4ri06s553h")))

