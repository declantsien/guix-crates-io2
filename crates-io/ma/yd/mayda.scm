(define-module (crates-io ma yd mayda) #:use-module (crates-io))

(define-public crate-mayda-0.1.0 (c (n "mayda") (v "0.1.0") (d (list (d (n "mayda_codec") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p3sn0rqcyjcfaawik4wbcpry2yjipxqddi2f2ygg3mhdr76miwf")))

(define-public crate-mayda-0.1.1 (c (n "mayda") (v "0.1.1") (d (list (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0l269ynbbbdv86klfccp9lx0ym50z3sw1n04py1b6ygsgink49yx")))

(define-public crate-mayda-0.2.0 (c (n "mayda") (v "0.2.0") (d (list (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0r67vgd6jx27gx9zdkjp5hgbhdq874540c3a6ihs3rx393dkqbyp")))

(define-public crate-mayda-0.2.1 (c (n "mayda") (v "0.2.1") (d (list (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "092f35nasgqxybq7cmqxsg9mddcm7lay3r0bqz3sx7lg6cna41cd")))

(define-public crate-mayda-0.2.2 (c (n "mayda") (v "0.2.2") (d (list (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1x4dgj003p3qnrc2ckdmllnav6yhadigzp83cksmr0q6wdw728di")))

(define-public crate-mayda-0.2.3 (c (n "mayda") (v "0.2.3") (d (list (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1x1mxqh4p5nb07ljarmv207mnbbl1zanhvfam9a104rsb6zgrhb8")))

(define-public crate-mayda-0.2.4 (c (n "mayda") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 0)) (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1xq9pwpj609b9zinp91fg0fri8sn3nq8hdq5x7ds3rsasvmgxdcp")))

(define-public crate-mayda-0.2.5 (c (n "mayda") (v "0.2.5") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "mayda_codec") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1dc6g7ygigal8s73rii3c2xvxa7jxaaipnrhxq8lh77qh73lhvql") (f (quote (("bench"))))))

