(define-module (crates-io ma iz maize) #:use-module (crates-io))

(define-public crate-maize-0.1.0 (c (n "maize") (v "0.1.0") (d (list (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "01svp2dqiy76kyrbi9gpfdb2v2biaydiqycpywnqb85864g4bkk4") (y #t)))

(define-public crate-maize-0.1.1 (c (n "maize") (v "0.1.1") (d (list (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "1wcv54cm04453i8v2hcbmyxb5iws8g9np32lhjyxw56dddx29mnd")))

