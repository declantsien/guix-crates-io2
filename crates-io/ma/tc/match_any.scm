(define-module (crates-io ma tc match_any) #:use-module (crates-io))

(define-public crate-match_any-0.1.0-dev (c (n "match_any") (v "0.1.0-dev") (h "0jspfb2zrhxjj42d4waqmvvzqqb3cg2gab385hv6hzmwivlr5rlg")))

(define-public crate-match_any-1.0.0 (c (n "match_any") (v "1.0.0") (h "1hbsfbjz42085p9azzcvz47z7d8dwmwp8hdmwvs3k013znixns4h")))

(define-public crate-match_any-1.0.1 (c (n "match_any") (v "1.0.1") (h "18rkdxkm43mx4b2akiarmir6pghfrwnk117497m3vy44q9rrpglm")))

