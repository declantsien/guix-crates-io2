(define-module (crates-io ma tc match-by-hash) #:use-module (crates-io))

(define-public crate-match-by-hash-0.1.0 (c (n "match-by-hash") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "1q954msqlrqhivqhp3iapfmw1g2bjk1lal7kbyfiqdv5hb3vd7ny")))

(define-public crate-match-by-hash-1.0.0 (c (n "match-by-hash") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0l6fyjx6g57fg4w40bd4df8s01jnw9ybmdazivdxhnv93bpxxw97")))

(define-public crate-match-by-hash-1.0.1 (c (n "match-by-hash") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0jf8h6k5zlpcfqi99wlhdg07d7zmkad5v5dhnqmdpm74gyhg746h")))

