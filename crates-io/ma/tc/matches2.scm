(define-module (crates-io ma tc matches2) #:use-module (crates-io))

(define-public crate-matches2-1.0.0 (c (n "matches2") (v "1.0.0") (h "1gjmaj5hiz10m0bf7s5nyxjl9svp8xwbyrcz1x145anbm9fib58k")))

(define-public crate-matches2-1.0.1 (c (n "matches2") (v "1.0.1") (h "1fcipi0hdm46dxsrbvm3zqkmvakdwwi3b9b1xahqhyk6c2jpa3w3")))

(define-public crate-matches2-1.1.0 (c (n "matches2") (v "1.1.0") (h "05g7rwsnxghc72gci0ygyw2p4v4ny8hf13yxammirlrlfyj6vlxd")))

(define-public crate-matches2-1.2.0 (c (n "matches2") (v "1.2.0") (h "0r18iw6bnki9ryzf5i9kxvshw66c444dqr6zyfp8mm0bvw4qymdz") (y #t)))

(define-public crate-matches2-1.2.1 (c (n "matches2") (v "1.2.1") (h "1ibp8v467nhi3sd5qkyfq1virlhzhpxxc7cbiq5pmidl4jjinr4v")))

