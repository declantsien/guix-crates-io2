(define-module (crates-io ma tc match-template) #:use-module (crates-io))

(define-public crate-match-template-0.0.1 (c (n "match-template") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "1n5c40ndzhhpz6wi9jj3qzqsqz0wxx3aqdkp0r6dksszf9ksqd63")))

