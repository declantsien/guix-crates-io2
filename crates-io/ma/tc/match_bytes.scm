(define-module (crates-io ma tc match_bytes) #:use-module (crates-io))

(define-public crate-match_bytes-0.1.0 (c (n "match_bytes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1kibyrij3avnj9g7idp1ajzj3gk90m1x0smli4xacnq054k7bbbz")))

