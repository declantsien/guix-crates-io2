(define-module (crates-io ma tc match_self) #:use-module (crates-io))

(define-public crate-match_self-0.1.0 (c (n "match_self") (v "0.1.0") (h "01hbl9md96380x01yqb9wkinanizp2xyv0aqi2wvqh72ss29xd2d")))

(define-public crate-match_self-0.1.1 (c (n "match_self") (v "0.1.1") (h "10rhy8lg75kj09f41i010im6jqczfs48y82i72v4i17jlywgp4r3")))

