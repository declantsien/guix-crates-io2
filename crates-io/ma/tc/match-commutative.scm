(define-module (crates-io ma tc match-commutative) #:use-module (crates-io))

(define-public crate-match-commutative-0.1.0-alpha.0 (c (n "match-commutative") (v "0.1.0-alpha.0") (h "14nqi2w9yb8ggai5qymjpwp5x8p28cq34850c0r0mrsqbx1hm1g6")))

(define-public crate-match-commutative-0.1.0-alpha.1 (c (n "match-commutative") (v "0.1.0-alpha.1") (h "050jzaad270ph82r827625540rjiv3rsrrm5xabv0fl2mjpx7901")))

(define-public crate-match-commutative-0.1.0 (c (n "match-commutative") (v "0.1.0") (h "1vpa3dc0nbk722n3lywrb6cry4qxb1dw0y52f7s4kkc2vr6hcvni")))

