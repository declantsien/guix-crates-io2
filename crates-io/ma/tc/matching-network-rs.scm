(define-module (crates-io ma tc matching-network-rs) #:use-module (crates-io))

(define-public crate-matching-network-rs-0.0.1 (c (n "matching-network-rs") (v "0.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "dimensioned") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1dzxp3snn1sf32vz2dxsdxpr5pp4pszhib69mzwknfayyvcjg7bg")))

