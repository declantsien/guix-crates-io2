(define-module (crates-io ma tc matchmaker) #:use-module (crates-io))

(define-public crate-matchmaker-0.1.0 (c (n "matchmaker") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m14nhxpzndg7j53i3xrp7gx7j6a0wsm6v5mzxswahkqw4nph0xp")))

