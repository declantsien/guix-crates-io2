(define-module (crates-io ma tc matchbox_protocol) #:use-module (crates-io))

(define-public crate-matchbox_protocol-0.6.0 (c (n "matchbox_protocol") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0dynbgxcx8bdskz36wgwcy8f5r5qvqlshxbv40kmfp8f2kr4r3vl") (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-matchbox_protocol-0.7.0 (c (n "matchbox_protocol") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0sz3j4bl2zcwjbvfisbf93x23s4fjafnnkymk3zssjsgm9xd0rmk") (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-matchbox_protocol-0.8.0 (c (n "matchbox_protocol") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "11gb0vfxrqwb0zgpb6x5h3br4miqv13ifl9hsd9l06ifnlkid2m0") (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-matchbox_protocol-0.9.0 (c (n "matchbox_protocol") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jznl2kqnrb3v2k0m4888vz3hfvvjbpazahpgdd8zscb24pm5vph") (s 2) (e (quote (("json" "dep:serde_json"))))))

