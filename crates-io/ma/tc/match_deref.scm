(define-module (crates-io ma tc match_deref) #:use-module (crates-io))

(define-public crate-match_deref-0.1.0 (c (n "match_deref") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "1swsmzvfmw9kncafnawapgjilwz472lizfhy61ns50vxsd31rcav") (y #t)))

(define-public crate-match_deref-0.1.1-alpha.1 (c (n "match_deref") (v "0.1.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "0mq5pskr17zvn202bs825i7xgqlakf8ryl273bq5jpa9pbmjbsjg") (y #t)))

(define-public crate-match_deref-0.1.1-alpha.2 (c (n "match_deref") (v "0.1.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "1a8faskl6dgd24aq2g2ylphknd2gh6frvg7cf46684fyj658w0ps") (y #t)))

(define-public crate-match_deref-0.1.1 (c (n "match_deref") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "1mxckaf61plqc2iib3liv52ai4xjbsvbgw3gyxlr1kwwpbpjgp9h")))

