(define-module (crates-io ma tc match_any_trait) #:use-module (crates-io))

(define-public crate-match_any_trait-0.0.0 (c (n "match_any_trait") (v "0.0.0") (h "0bkpd9wnzz6mxf4klgz8xf1lwhik79p0h56pb1np6jh6zzzhdlkh") (y #t)))

(define-public crate-match_any_trait-0.1.0 (c (n "match_any_trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1k24h2g36z391gdkb2sq5zlifbv45b9s8d1jip6yjvs1ksq651b3") (y #t)))

(define-public crate-match_any_trait-0.1.1 (c (n "match_any_trait") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "117a32i4mp1r89b6pqx7imlsrmh55mlrazhnfvvnpwqwql9rifx3")))

