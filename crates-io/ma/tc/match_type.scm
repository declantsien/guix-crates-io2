(define-module (crates-io ma tc match_type) #:use-module (crates-io))

(define-public crate-match_type-0.1.0 (c (n "match_type") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06ppvy7wk485dwr92cir9v48g1ly68x7q3i4k5nwpgsywdzayay8")))

(define-public crate-match_type-0.1.1 (c (n "match_type") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wlz2xxvjgaj4bd6r5dy7az79vrwdrb3fk1kwn642dnadix27sh7")))

(define-public crate-match_type-0.1.2 (c (n "match_type") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yabjhmhp6pdgaih4kmgl08lvmiq2bmlhgd8qlyw32a22d7m7x7f")))

(define-public crate-match_type-0.1.3 (c (n "match_type") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hsjd6c7wwqi2g8zbk328aswmm4lhmcbdmdnvkgk3sr2wh2pgljn")))

(define-public crate-match_type-0.1.4 (c (n "match_type") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rwxcg3d36vj8h93im50qyhv64fk8di660vwfx4a75lfilm72sgd")))

(define-public crate-match_type-0.1.5 (c (n "match_type") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09bjbg4brbgzhg6m3js0yjh9yd96l7v6yjgi1kzsv2b0myz8g9y7")))

