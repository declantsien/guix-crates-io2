(define-module (crates-io ma tc match-eq) #:use-module (crates-io))

(define-public crate-match-eq-0.1.0-rc1 (c (n "match-eq") (v "0.1.0-rc1") (h "0cb7m57f7c12xfl01slsdks2cyq61l33qkpp4wd3dyg5f6241rk4") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.0-rc2 (c (n "match-eq") (v "0.1.0-rc2") (h "0x622ryjs5id39clbi3czjy2myk86n0z1jdfavvm2fn98f9gid96") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.0-rc3 (c (n "match-eq") (v "0.1.0-rc3") (h "19j2l2jbf1y74arjwc91rdh6q4mcy5vidvj4i3gwbz2r6fx4m4ni") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.0 (c (n "match-eq") (v "0.1.0") (h "1wyxqx5k6740kd0437mw1y2pfbyrbhpqn74avgxhp2k8x3i583gi") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.1 (c (n "match-eq") (v "0.1.1") (h "152s0w73vcw5gbx1nimf1p1p84w4jnpwmpkpckd4i0aw0ccalhjx") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.2 (c (n "match-eq") (v "0.1.2") (h "0lfz9r81dfdcy2wwccvxhg0zkgzr6jvhnw9f0q1k8vn0h5al8p8z") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.3 (c (n "match-eq") (v "0.1.3") (h "0ggiaqkflf87dmhivc3ciw7w4r6n1b163j60m59xjrd2ai3g4cxx") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.4 (c (n "match-eq") (v "0.1.4") (h "1a1l5lvp2lksb3k958drcjx0ks9b7lgnkcl3gq0f7z5s951hmsix") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.1.5 (c (n "match-eq") (v "0.1.5") (h "0qg5g07gdxdx66bap04ydwafrwmkmh4f0nzf5ykmq4k6i4q4z9i3") (f (quote (("default" "core") ("core")))) (y #t)))

(define-public crate-match-eq-0.2.0 (c (n "match-eq") (v "0.2.0") (h "0s266idv78xv4r7m00pkk9bjv9g9wxl62j8316m0kzbx3f9a37ny") (f (quote (("no-core")))) (y #t)))

(define-public crate-match-eq-0.2.1 (c (n "match-eq") (v "0.2.1") (h "00xkv5ac606q4chsrc6ivw80sfp44nzdb6sxw25vbr25mwm6inpd") (f (quote (("no-core")))) (y #t)))

(define-public crate-match-eq-0.2.2 (c (n "match-eq") (v "0.2.2") (h "09qhqxkv56gn72b9a2h5ran90lx2c27fin30z10j77zjm5n515j9") (f (quote (("no-core")))) (y #t)))

(define-public crate-match-eq-0.2.3 (c (n "match-eq") (v "0.2.3") (h "099qr6dhr0111n3r99angyf5hmbbwx0c2yivm153a5vxxjd34cs0") (f (quote (("no-core")))) (y #t)))

(define-public crate-match-eq-0.2.4 (c (n "match-eq") (v "0.2.4") (h "13v1j61f7kkka1sva81rin8xkbfj6smsi85dff5div9aqpi8j79r") (f (quote (("no-core")))) (y #t)))

