(define-module (crates-io ma tc matchertools) #:use-module (crates-io))

(define-public crate-matchertools-0.1.0 (c (n "matchertools") (v "0.1.0") (h "0qkd1wjndvzws57ph15bcky4qgcp1w3qpz698q3vnp65xqad48ff")))

(define-public crate-matchertools-0.1.1 (c (n "matchertools") (v "0.1.1") (h "0knw7g2fg61r18lkg1w33prpxxljfj8nz8phcsqmp004pjshl64n")))

