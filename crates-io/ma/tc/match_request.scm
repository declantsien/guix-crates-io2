(define-module (crates-io ma tc match_request) #:use-module (crates-io))

(define-public crate-match_request-0.1.0 (c (n "match_request") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "hyper") (r "^0.13.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1krdc5nxnpjjpr8axc0fv05xmji1dq277mpsl9wa3727qh1qawxw")))

