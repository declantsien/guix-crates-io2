(define-module (crates-io ma tc match_cast) #:use-module (crates-io))

(define-public crate-match_cast-0.1.0 (c (n "match_cast") (v "0.1.0") (h "18yzk37payhbsikjbdg9di07kbkplvjjz0vn63r41snd7id9290h") (y #t)))

(define-public crate-match_cast-0.1.1 (c (n "match_cast") (v "0.1.1") (h "0m8m223z7kq3c92x0cf8bw06i9cy4djz57cfj4v3xbl99zkz2wb7")))

(define-public crate-match_cast-0.1.2 (c (n "match_cast") (v "0.1.2") (h "1p88m9sq4nlhd0pvlzq84pgqwsl67dxj78sl4p2qb27c93cigygp")))

