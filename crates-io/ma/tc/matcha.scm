(define-module (crates-io ma tc matcha) #:use-module (crates-io))

(define-public crate-matcha-0.1.0 (c (n "matcha") (v "0.1.0") (d (list (d (n "memcmp") (r "^0.0.6") (d #t) (k 0)))) (h "0nkqg81mkdjncx93ykrn5sy7xq1zlwm54nm27hq2r28zaydnbjx2") (y #t)))

(define-public crate-matcha-0.1.1 (c (n "matcha") (v "0.1.1") (d (list (d (n "memcmp") (r "^0.0.6") (d #t) (k 0)))) (h "0g742ly4s2fm2187ilfs1pmihbh83y8b31cgb3rgkp1hsyrv8ycv") (y #t)))

