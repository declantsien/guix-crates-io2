(define-module (crates-io ma tc matcher) #:use-module (crates-io))

(define-public crate-matcher-0.0.0 (c (n "matcher") (v "0.0.0") (h "1y6zjdbs09zpm2d6nab5qr81z44mz7iiwn9m5kgrrdfzzgxqhvv6")))

(define-public crate-matcher-0.0.1 (c (n "matcher") (v "0.0.1") (h "1bx5s0iabizizd55knjrc16hkix5da18qa46ar8ysvpjjf7mp9pv")))

(define-public crate-matcher-0.0.2 (c (n "matcher") (v "0.0.2") (h "1279nndbl6yw4zmr70ryzwqh629k8yggjzyqa7xnxc069p3xil0z")))

