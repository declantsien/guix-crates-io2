(define-module (crates-io ma tc match_opt) #:use-module (crates-io))

(define-public crate-match_opt-0.1.0 (c (n "match_opt") (v "0.1.0") (h "1ysn69l74qfzg9v12k2qspigv52n80wvp5k6hpg212rc7s0rlcsg")))

(define-public crate-match_opt-0.1.1 (c (n "match_opt") (v "0.1.1") (h "0imk2mxglb1wwgbzwgval229c8wg4ifqgs6g1cwpx82h4sy9ywq5")))

(define-public crate-match_opt-0.1.2 (c (n "match_opt") (v "0.1.2") (h "1aad72v99hnk5l1zjm81gqal1v301hw6csad6dayfshy999a2ns0")))

