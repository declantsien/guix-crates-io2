(define-module (crates-io ma tc match-lookup) #:use-module (crates-io))

(define-public crate-match-lookup-0.1.0 (c (n "match-lookup") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "124m61zg00ppab7ydhdwvs5ayw652nhgx2pzm9clz68c46iskihx")))

(define-public crate-match-lookup-0.1.1 (c (n "match-lookup") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07lq5b2ncjislydxxvkq7bvhr30spywgyvzhn31bp7djii6p4r8j")))

