(define-module (crates-io ma tc matchers) #:use-module (crates-io))

(define-public crate-matchers-0.0.1 (c (n "matchers") (v "0.0.1") (d (list (d (n "regex-automata") (r "^0.1") (d #t) (k 0)))) (h "1q8ckqmkjqkznvdi9x0z769yz2bmvlqcwx51ad2lpk4mfmgpi6gh")))

(define-public crate-matchers-0.1.0 (c (n "matchers") (v "0.1.0") (d (list (d (n "regex-automata") (r "^0.1") (d #t) (k 0)))) (h "0n2mbk7lg2vf962c8xwzdq96yrc9i0p8dbmm4wa1nnkcp1dhfqw2")))

