(define-module (crates-io ma tc matchgen) #:use-module (crates-io))

(define-public crate-matchgen-0.1.0 (c (n "matchgen") (v "0.1.0") (d (list (d (n "bstr") (r "^1.1.0") (f (quote ("std"))) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)))) (h "0l3qhmq8v7kc9lpifpjnyc7bm0r72k090sbcxrinwg6r08cn1fi4") (r "1.60")))

(define-public crate-matchgen-0.2.0 (c (n "matchgen") (v "0.2.0") (d (list (d (n "bstr") (r "^1.1.0") (f (quote ("std"))) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)))) (h "1wc93gvncpvypgdqplyvgf3gbww3xdd64mwwyxmich7pfxx144s2") (r "1.60")))

