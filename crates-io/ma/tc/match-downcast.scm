(define-module (crates-io ma tc match-downcast) #:use-module (crates-io))

(define-public crate-match-downcast-0.1.1 (c (n "match-downcast") (v "0.1.1") (h "0nk6gm5rfpnzpmjhr1spkgkznwrqflnfxc7g73j5kh4g9cy9sqxp")))

(define-public crate-match-downcast-0.1.2 (c (n "match-downcast") (v "0.1.2") (h "0sib4y47c8kqg7zwv4xl0j9b4b2l1nd5b2f0yrf0viimf56ca5ly")))

