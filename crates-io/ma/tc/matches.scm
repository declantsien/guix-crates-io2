(define-module (crates-io ma tc matches) #:use-module (crates-io))

(define-public crate-matches-0.0.1 (c (n "matches") (v "0.0.1") (h "1838bafy53mfvhx5y8p4vji497qy5d3iynyrb0fc2ig27k5fpdvq")))

(define-public crate-matches-0.1.0 (c (n "matches") (v "0.1.0") (h "0zq8rz92pc7nx8lhc1nmml16p5b7k9pvq5n3i4hxf9ahw47xkm8g") (y #t)))

(define-public crate-matches-0.1.1 (c (n "matches") (v "0.1.1") (h "1z1skql8vnbjahkn30l9l569afcd3m6m505l3m4pwlm94x2axja9") (y #t)))

(define-public crate-matches-0.1.2 (c (n "matches") (v "0.1.2") (h "0bllhwhqbvddkgvhy9z1ghcr42c1j9l99whzbf0a9rcwh1b5cc0m")))

(define-public crate-matches-0.1.3 (c (n "matches") (v "0.1.3") (h "1pfhg8bl4rlxdgg9bhgqm9jgb09789019n0ckcpm4jzs160svhxw")))

(define-public crate-matches-0.1.4 (c (n "matches") (v "0.1.4") (h "1c8190j84hbicy8jwscw5icfam12j6lcxi02lvmadq9260p65mzg")))

(define-public crate-matches-0.1.5 (c (n "matches") (v "0.1.5") (h "1c8w8addi7swd7i7npilwgg8by32k8ncccp65l57qigy2lvhgi85")))

(define-public crate-matches-0.1.6 (c (n "matches") (v "0.1.6") (h "0xik34zh2ny2g8h6c95qf83xy0lq6wikbhf169z4lkpzp3kan2hh")))

(define-public crate-matches-0.1.7 (c (n "matches") (v "0.1.7") (h "0jkpr61488m41vwhdddhdliy1z9cx95q8i6blmyw8d3wnfx12mc3")))

(define-public crate-matches-0.1.8 (c (n "matches") (v "0.1.8") (h "020axl4q7rk9vz90phs7f8jas4imxal9y9kxl4z4v7a6719mrz3z")))

(define-public crate-matches-0.1.9 (c (n "matches") (v "0.1.9") (h "0gw5ib38jfgyyah8nyyxr036grqv1arkf1srgfa4h386dav7iqx3")))

(define-public crate-matches-0.1.10 (c (n "matches") (v "0.1.10") (h "1994402fq4viys7pjhzisj4wcw894l53g798kkm2y74laxk0jci5")))

