(define-module (crates-io ma tc matchable) #:use-module (crates-io))

(define-public crate-matchable-0.1.0 (c (n "matchable") (v "0.1.0") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07ark5wacvgad0j3rfzbqcy62b2gqkwri81c25yvirk594jd8c0s") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-matchable-0.1.1 (c (n "matchable") (v "0.1.1") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a7rwvnhfsqwrpq5hnimslwyyjcc7w06zmq718z7vgwh2c3a88sn") (s 2) (e (quote (("serde" "dep:serde"))))))

