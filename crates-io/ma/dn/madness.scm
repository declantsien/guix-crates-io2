(define-module (crates-io ma dn madness) #:use-module (crates-io))

(define-public crate-madness-0.1.0 (c (n "madness") (v "0.1.0") (d (list (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0m7smdiikb6wz8cxwc66byaivq38yfd9lg3s9i37z97r8gy537xj")))

