(define-module (crates-io ma sq masquerade-proxy) #:use-module (crates-io))

(define-public crate-masquerade-proxy-0.1.0 (c (n "masquerade-proxy") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "httparse") (r "^1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "quiche") (r "^0.16.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "socks5-proto") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "15gigpmrm9my8rf5sxciwmq0f9nhfd5bqasvk4vaf7r76yqmvp0g")))

