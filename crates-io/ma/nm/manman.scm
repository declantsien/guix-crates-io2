(define-module (crates-io ma nm manman) #:use-module (crates-io))

(define-public crate-manman-0.1.0 (c (n "manman") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15i314spw0p31fd5acbr631mmil0zjyxng54f7iwz349vr0q22id")))

(define-public crate-manman-0.1.1 (c (n "manman") (v "0.1.1") (h "1d02a7iwjf6acvmpf2d2cy281js5wcvm1i5d5mn634jy5yljh127")))

(define-public crate-manman-0.1.2 (c (n "manman") (v "0.1.2") (h "1zkc2gjwspsfkpp0rcgddhfx0nmv88mi7x0nsim8lks8nkqkjnw9")))

