(define-module (crates-io ma gl maglev) #:use-module (crates-io))

(define-public crate-maglev-0.1.0 (c (n "maglev") (v "0.1.0") (d (list (d (n "fasthash") (r "^0.2") (d #t) (k 2)) (d (n "primal") (r "^0.2") (d #t) (k 0)))) (h "1w039hfh80y2ivbfkg91gwwgkn5gla4g18wqpmr6fa18cdbchnfx")))

(define-public crate-maglev-0.1.1 (c (n "maglev") (v "0.1.1") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)))) (h "1saqyy0abj9slf9xm4f3h684f2i31z2xa2dmjdg1val056bb16ln")))

(define-public crate-maglev-0.1.2 (c (n "maglev") (v "0.1.2") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)))) (h "053wdjfwiak70hys14rvd71yd2zbvm0jc85zckq0x0ld7nqm0wiz")))

(define-public crate-maglev-0.1.3 (c (n "maglev") (v "0.1.3") (d (list (d (n "fasthash") (r "^0.2") (d #t) (k 2)) (d (n "primal") (r "^0.2") (d #t) (k 0)))) (h "1q6h1l1z8rhqkgw3675i75lsf91l464gjq2awsfjmalihdm849f6")))

(define-public crate-maglev-0.2.0 (c (n "maglev") (v "0.2.0") (d (list (d (n "fasthash") (r "^0.4") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 0)))) (h "0l731pzchf6q9s2mgpydpbx62l16hy40s379i7z8kxjdgbhqad12")))

(define-public crate-maglev-0.2.1 (c (n "maglev") (v "0.2.1") (d (list (d (n "fasthash") (r "^0.4") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0w2kkph9yk4aiipx5lwxk3ypsqnnxnb1axyzazsikl0634ga4vn7")))

