(define-module (crates-io ma gl maglio) #:use-module (crates-io))

(define-public crate-maglio-0.1.1 (c (n "maglio") (v "0.1.1") (h "1vbsk9hs61vv72jcrfzkscihabsq827dsxf8gfh4anvsaasv1wdn")))

(define-public crate-maglio-0.1.2 (c (n "maglio") (v "0.1.2") (d (list (d (n "ordered-float") (r "^2.8.0") (d #t) (k 0)))) (h "1pqfx0ypzpqdqv7vk6d2kqskissicsfylbjrgwp599maj4dhg9am")))

