(define-module (crates-io ma nb manber-fingerprinting) #:use-module (crates-io))

(define-public crate-manber-fingerprinting-0.1.0 (c (n "manber-fingerprinting") (v "0.1.0") (h "1p78h2vq08h80sn1gmbdqq5480kxfk1mv735ac1sv51zn2rd7irj")))

(define-public crate-manber-fingerprinting-0.2.0 (c (n "manber-fingerprinting") (v "0.2.0") (h "1rz7cv5zmw3kd33wvgxnn3bzhw3y58v105bm41yhpm37y695zbbg")))

(define-public crate-manber-fingerprinting-0.3.0 (c (n "manber-fingerprinting") (v "0.3.0") (h "086slmn2lifxkkax33ki9d73yd0900qwjx7z8a5a4291sxa1yvil")))

(define-public crate-manber-fingerprinting-0.4.0 (c (n "manber-fingerprinting") (v "0.4.0") (h "1dyynf678mh6qnpvzdlx96yxddz92vy9kdv5wca8gzvf3i2rq3yl")))

(define-public crate-manber-fingerprinting-0.5.0 (c (n "manber-fingerprinting") (v "0.5.0") (h "0bmn6qdgvzrg2j3qs4rbm2rwyblajnjrky2b7xn05jqg8g74h2ij")))

(define-public crate-manber-fingerprinting-0.6.0 (c (n "manber-fingerprinting") (v "0.6.0") (h "1sd23i1v1j97rhlhi7zqw1fyzlbhxvpvkafkb1669k6snf84xvj8")))

