(define-module (crates-io ma zt mazth) #:use-module (crates-io))

(define-public crate-mazth-0.0.0 (c (n "mazth") (v "0.0.0") (h "1fy8zzdsb4ma5npyp6pzy6svf157bghdzlgnja55ds5nndc7znhw")))

(define-public crate-mazth-0.0.1 (c (n "mazth") (v "0.0.1") (h "19mljxycmwk34qbp93yz48m99b0z10fahijpi1bkjan05kqzkasl")))

(define-public crate-mazth-0.1.0 (c (n "mazth") (v "0.1.0") (h "1iw6cq2cb00p326nqp1zma94qlpprbpahaysxzx4sckdn6bpl7il")))

(define-public crate-mazth-0.1.2 (c (n "mazth") (v "0.1.2") (h "19ndbyhqzxcmkn7whqqx3kxn4rbrq1mx9jgwcrbv9wkqh99rv8j0")))

(define-public crate-mazth-0.2.0 (c (n "mazth") (v "0.2.0") (h "189i4nlbqr7ii2lfpd60ffh16mcakykcljypgs9rfqaf9b0xx5dj")))

(define-public crate-mazth-0.2.1 (c (n "mazth") (v "0.2.1") (h "1c349fkkf0w1gvnb62glf9jkwi0lfasav15vzlr9qd8fi97y02k8")))

(define-public crate-mazth-0.3.0 (c (n "mazth") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "1kja456i0j90j7nqp5ba4jvqm0h9h3js3qzimq8rqr97x52jyq74")))

(define-public crate-mazth-0.3.1 (c (n "mazth") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "0d92zyy3s5ljqcscj7fhl5qd5y74w3w55cl8m7y0ay58ssdmn8wf")))

(define-public crate-mazth-0.4.0 (c (n "mazth") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "1mwfnip06yrcb5xz3q9zrvhx2bvpzmzdhgplir0ws5m4nvhy80m2")))

(define-public crate-mazth-0.4.1 (c (n "mazth") (v "0.4.1") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "0101z5pwhnj442k8cbajwww3731idgb2z45sa65417aiax29rhq0")))

(define-public crate-mazth-0.4.2 (c (n "mazth") (v "0.4.2") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "1hjhkmh7g2s977qj13mp2aiv56g4d0kwwr3s89ylw6jq3yrny9bk")))

(define-public crate-mazth-0.5.0 (c (n "mazth") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "0lkhyhc62xix833m5qrpvq7hn2pmk5capl29vy8anp2gfi1dpgyd")))

(define-public crate-mazth-0.6.0 (c (n "mazth") (v "0.6.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "0xmzrk9xks44qi2pwm0mzsfgvcgik9v6blq7bckg7bp2l82fya9y")))

