(define-module (crates-io ma p1 map1) #:use-module (crates-io))

(define-public crate-map1-0.1.1 (c (n "map1") (v "0.1.1") (d (list (d (n "cargo-sync-readme") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0h2vm6fyivvk0a06pnl08xg59bch08smzqmgl4456zhs45hk14my")))

(define-public crate-map1-0.1.2 (c (n "map1") (v "0.1.2") (d (list (d (n "cargo-sync-readme") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1apkscpqkm9d6ghw5f2rm4cl1k722k2fcr1n44lx4hi43m1gmjyw")))

