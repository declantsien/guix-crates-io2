(define-module (crates-io ma p1 map18xx) #:use-module (crates-io))

(define-public crate-map18xx-0.0.1 (c (n "map18xx") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.7") (d #t) (k 0)))) (h "0xb0l0wmfhh855nay4bnfxmys3zwd6fjfi84hqm5zwsll0iimrli")))

(define-public crate-map18xx-0.0.3 (c (n "map18xx") (v "0.0.3") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.7") (d #t) (k 0)))) (h "0xlwy2s2krjhb80wpa6lzlvj3xbz46a9g57zsibs9r4374vpg40w")))

(define-public crate-map18xx-0.0.4 (c (n "map18xx") (v "0.0.4") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.7") (d #t) (k 0)))) (h "1y4jbnm3xcssr0ddlnk387skwn7cf9nj0wf34rrq9p1anx0j9xpy")))

(define-public crate-map18xx-0.0.5 (c (n "map18xx") (v "0.0.5") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.7") (d #t) (k 0)))) (h "0k9mhlzw493g0wmsvxlcifhai3vq6wdsl5xwkki4blmna6k46slh")))

