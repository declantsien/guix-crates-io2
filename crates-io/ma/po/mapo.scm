(define-module (crates-io ma po mapo) #:use-module (crates-io))

(define-public crate-mapo-0.1.0 (c (n "mapo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "to_precision") (r "^0.1.1") (d #t) (k 0)))) (h "09yn11pzk6419m4072nqjwcvrchh03rkalmrkavldwhkkzcalz6p")))

(define-public crate-mapo-0.1.1 (c (n "mapo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "clap") (r "^3.0.14") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "fake") (r "^2.4.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "piet-common") (r "^0.5.0") (d #t) (k 0)) (d (n "piet-common") (r "^0.5.0") (d #t) (k 2)) (d (n "qu") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "to_precision") (r "^0.1.1") (d #t) (k 0)))) (h "1csrb3jcfd5crqaawak00zl4kfz82y2vf705bismp4fhh7igzw0i")))

