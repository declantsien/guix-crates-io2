(define-module (crates-io ma t3 mat32) #:use-module (crates-io))

(define-public crate-mat32-0.1.0 (c (n "mat32") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0rx3apgkz444pw74a2brp37wlcdgsbpna2r7wvpcgbqhqir9cync")))

(define-public crate-mat32-0.1.1 (c (n "mat32") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "1c94xwn088x74ir0ha4jn5wmlm1fi9zik957ra133jn17mfddvc9")))

(define-public crate-mat32-0.1.2 (c (n "mat32") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0yh703qdiradvb78ff1jy3sfa0hdnkif5irds364r61pip3whpva")))

(define-public crate-mat32-0.1.3 (c (n "mat32") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0p4yfm3v0s94bx4aby5hynmz4nkj4micyy5h86vbdnpkwi6ncq3h")))

(define-public crate-mat32-0.1.4 (c (n "mat32") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0m2waaiyv0dkq0qkajcp7nyka4bi6m38h6qynjywlh3i90qb3mn9")))

(define-public crate-mat32-0.1.5 (c (n "mat32") (v "0.1.5") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0xzzs4wlnff2nakzc2axzjb401lifsyyj1iyy2v57phx3r7na8kk")))

(define-public crate-mat32-0.2.0 (c (n "mat32") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)))) (h "0w4j45qwcq7k0gmbn9rhvfx9y3ia75zk3jrs262biz23ymcdvrhv")))

(define-public crate-mat32-0.2.1 (c (n "mat32") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)))) (h "1mgzasa42ahqm5g0kjq24bggx9ffwm5hc3ld5d9mk4pxngafrkri")))

