(define-module (crates-io ma t3 mat3) #:use-module (crates-io))

(define-public crate-mat3-0.1.0 (c (n "mat3") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)))) (h "008ipn66l8kn2pdl1x7icz9l5f5l2fx3a8npdqlbazcrpp3pcp50")))

(define-public crate-mat3-0.1.1 (c (n "mat3") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "07sr8gahix1rs62367fhx47g5hdyrchgqislsibzcys0d8v5lvcf")))

(define-public crate-mat3-0.1.2 (c (n "mat3") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "0dj5qljmparh6inzra4imygv8qwxkky3hbhv69mnfky9waya3n4s")))

(define-public crate-mat3-0.1.3 (c (n "mat3") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "0jpsbjgy59qif70rg2ppxqryfy2pf7m5s5dzi3whlfl5ykc0wlld")))

(define-public crate-mat3-0.1.4 (c (n "mat3") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "09m1qxm0v1k1arc2gv0j8741kl1n3rd8rv6axm6dlhmbvbqr55qc")))

(define-public crate-mat3-0.2.0 (c (n "mat3") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1k2xkn4k97qpf5k3pxxqqsk2r41rhg2k3c50djj9l8gd1qjfrpr1")))

(define-public crate-mat3-0.2.1 (c (n "mat3") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m2gzj3zgm0fpplbrg1v006dg1das0ryc2grn2fw5jhx5f0n04l1")))

