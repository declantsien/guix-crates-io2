(define-module (crates-io ma rg marginfi-cpi) #:use-module (crates-io))

(define-public crate-marginfi-cpi-0.1.0 (c (n "marginfi-cpi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.24.2") (d #t) (k 0)))) (h "0354s67pgmwjfz6x8dr0c0sbac9749301cdas4xcn4rlfvbkl7w8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet-beta") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marginfi-cpi-0.1.1 (c (n "marginfi-cpi") (v "0.1.1") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.24.2") (d #t) (k 0)))) (h "13k5biczsjjdsna96mybr1dy72n7507s1i1zfvv76vplf23mhnzy") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet-beta") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marginfi-cpi-0.1.2 (c (n "marginfi-cpi") (v "0.1.2") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.24.2") (d #t) (k 0)))) (h "1sv5pkc7s9lc5a012lrxyq21yljsrl1hdnz9vdld3s7sa0sabp6f") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet-beta") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-marginfi-cpi-0.1.3 (c (n "marginfi-cpi") (v "0.1.3") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.24.2") (d #t) (k 0)))) (h "139nf11qw6hvd41gm61axbibigq17vi27rgkhkyvk4q8nykyh9gz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet-beta") ("default" "cpi") ("cpi" "no-entrypoint"))))))

