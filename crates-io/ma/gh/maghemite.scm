(define-module (crates-io ma gh maghemite) #:use-module (crates-io))

(define-public crate-maghemite-0.1.0 (c (n "maghemite") (v "0.1.0") (d (list (d (n "iota") (r "^0.2.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0zbs7cafk71cvf1sir63z639kvgxc65a5q6lzrbk2dwmjcycjh13")))

