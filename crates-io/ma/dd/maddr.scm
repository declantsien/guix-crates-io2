(define-module (crates-io ma dd maddr) #:use-module (crates-io))

(define-public crate-maddr-0.1.0 (c (n "maddr") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "mhash") (r "^0.1.1") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "0k99lfgxl1b7z9vhbs0m499mvy2mzv90a5fq1vzscip96f3b9si7") (y #t)))

(define-public crate-maddr-0.2.0 (c (n "maddr") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "mhash") (r "^0.2.0") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "0q4y1ss2wp5rygmpcrx8zdrblp0c6yr9b7cz32n631nvsjlpmwbk") (y #t)))

(define-public crate-maddr-0.2.1 (c (n "maddr") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "mhash") (r "^0.2.0") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "1apzbbrysmig81hb41h77k8j3ajs7yhs6fg7w6kicvgijh08bfz4") (y #t)))

(define-public crate-maddr-0.3.0 (c (n "maddr") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "mhash") (r "^0.3.0") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "06zdl7hbgfrnq8bala6z53jcfrib426yfp0zwb3darxqgjnvd1p3") (y #t)))

(define-public crate-maddr-0.3.1 (c (n "maddr") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "mhash") (r "^0.3.0") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "03zv01wzgk44hkf7djwsay79pf7zi4x2fvqaszb9v4awgkpcyjgd") (y #t)))

