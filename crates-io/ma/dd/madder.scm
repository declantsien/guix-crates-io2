(define-module (crates-io ma dd madder) #:use-module (crates-io))

(define-public crate-madder-0.1.1 (c (n "madder") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0c3xpmb21aq0wji0ka5yblzvlym1ajg7gl406s8skhi5nsf9lnw0")))

