(define-module (crates-io ma t4 mat4) #:use-module (crates-io))

(define-public crate-mat4-0.1.0 (c (n "mat4") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "0lfl4szba571hjrbb98sd62kfj67xz4j913izwclclrsh4zgcffv")))

(define-public crate-mat4-0.1.1 (c (n "mat4") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "0kapwr4yz9sg49lnqdh72ps3khpn3vyyy9vv4xz9c6qnm6zncaih")))

(define-public crate-mat4-0.1.2 (c (n "mat4") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "1plk9nwhqn9b2v63yin343al8dyndixv64785jk7i6cpn3lshdwr")))

(define-public crate-mat4-0.1.3 (c (n "mat4") (v "0.1.3") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "1is0pfhd7gnh0mf8in7kfvwc3qkx4l6cqfb12sgma52q9wiczmcm")))

(define-public crate-mat4-0.1.4 (c (n "mat4") (v "0.1.4") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "06q06ns2fvvcqsa9n42br43q3rlcijv6jzrip458j3np9f4pn6h5")))

(define-public crate-mat4-0.2.0 (c (n "mat4") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "0z1285kmy07s1fq6lhv6c1d60kzjbfrshcw9wn2an0lwgjz7sp3l")))

(define-public crate-mat4-0.2.1 (c (n "mat4") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "1b43djm9p6g0l40jbdz9faha62w5qmsybj5dik5m9y6c844w13lp")))

