(define-module (crates-io ma lb malbolge) #:use-module (crates-io))

(define-public crate-malbolge-1.0.0 (c (n "malbolge") (v "1.0.0") (h "0ga1dw1yrjnflkywjcbjlx1qn53vv43jw0xnlkjplqmr8hi93z45")))

(define-public crate-malbolge-1.0.1 (c (n "malbolge") (v "1.0.1") (h "10rbb9a3ngcs7h6v2i3rafpdyyq44myl0rfyb4kcf4iwkbcssyw2")))

