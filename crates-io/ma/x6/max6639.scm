(define-module (crates-io ma x6 max6639) #:use-module (crates-io))

(define-public crate-max6639-0.1.0 (c (n "max6639") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1awar4hdbapa2xm905ypnvjp7jy471zlc8v7k5c3x3wy0ybnzlbk")))

