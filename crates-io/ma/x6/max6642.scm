(define-module (crates-io ma x6 max6642) #:use-module (crates-io))

(define-public crate-max6642-0.1.0 (c (n "max6642") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0ps1ajnkgyvnq0pjwivgshvqv9ynizib5swjg9ggv0v5dfb7r33q")))

