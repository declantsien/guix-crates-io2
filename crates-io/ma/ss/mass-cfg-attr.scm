(define-module (crates-io ma ss mass-cfg-attr) #:use-module (crates-io))

(define-public crate-mass-cfg-attr-0.1.0 (c (n "mass-cfg-attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "1810dadp3zq48m9ql4xal2367vx2qvx4z7q1b1xm3q8apq6s6wvw") (r "1.65")))

(define-public crate-mass-cfg-attr-0.2.0 (c (n "mass-cfg-attr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0wckghqgqmsylpif6nsw88rxrjhwxqx62226gpi4cd81wqzy17pw") (r "1.65")))

