(define-module (crates-io ma ss massive_functions) #:use-module (crates-io))

(define-public crate-massive_functions-0.1.0 (c (n "massive_functions") (v "0.1.0") (h "1rz216zbjdaf1njzy5s6m57zz0lrwv7phppa6r6si19572mwj8gb") (y #t)))

(define-public crate-massive_functions-0.1.1 (c (n "massive_functions") (v "0.1.1") (h "1lxz5mzl4d9dfj9hr1wd988nj93gf5ayp9iif96vcy4pcmf871k2") (y #t)))

