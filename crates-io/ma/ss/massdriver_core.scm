(define-module (crates-io ma ss massdriver_core) #:use-module (crates-io))

(define-public crate-massdriver_core-0.0.1 (c (n "massdriver_core") (v "0.0.1") (d (list (d (n "gio") (r "^0.8.1") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.8.1") (f (quote ("v3_16"))) (d #t) (k 0)))) (h "0nk9s5gwp4h3r52h34nnl1y1jmz1k1vllcvbvmn50ylmk77rvwah")))

