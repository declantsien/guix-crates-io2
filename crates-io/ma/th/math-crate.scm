(define-module (crates-io ma th math-crate) #:use-module (crates-io))

(define-public crate-math-crate-0.1.0 (c (n "math-crate") (v "0.1.0") (h "073k27b2rl5wnszsw3dx0gkk6434vx98y6dvcwlcjjkxrzqxpwbf")))

(define-public crate-math-crate-0.1.1 (c (n "math-crate") (v "0.1.1") (h "0scpsn3wfbqxv26x5fxgv29vb9i9idafj5kd7z48mj4insj4i0l9")))

(define-public crate-math-crate-0.1.2 (c (n "math-crate") (v "0.1.2") (h "10nxbmx1jylqyqfsbyh6zhyamjmlcn6p9aq6jhhsz4cwh90c5qdf")))

(define-public crate-math-crate-0.1.3 (c (n "math-crate") (v "0.1.3") (h "0wk9sir1g0cvz19grc56x4czwd171jrb1m6mfshzk84881k268wc")))

