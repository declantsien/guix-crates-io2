(define-module (crates-io ma th maths-rs) #:use-module (crates-io))

(define-public crate-maths-rs-0.1.0 (c (n "maths-rs") (v "0.1.0") (h "0g7y1q8265d6yax5zlwwpi2jsd9s94gxfrklmh0j6b69v6c43vvx") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (y #t)))

(define-public crate-maths-rs-0.1.1 (c (n "maths-rs") (v "0.1.1") (h "0ai8064qhv3scyipmcf9q8mpias31zw6p71ywgdal4hdjlfmyvmn") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (y #t)))

(define-public crate-maths-rs-0.0.0 (c (n "maths-rs") (v "0.0.0") (h "17gaa978lipr3x1v25ndvaagfxpbaa5scmnv2ridw1c2i3cra705") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (y #t)))

(define-public crate-maths-rs-0.1.2 (c (n "maths-rs") (v "0.1.2") (h "0syfgra3jbh5y88bjfms2550i787l444hcssfb13bcs8qa6nnvi4") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (y #t)))

(define-public crate-maths-rs-0.1.3 (c (n "maths-rs") (v "0.1.3") (h "0p116xns67skawk0d11svn0k1q95mzc3wymwqz8zrw8ia1si9b1r") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.4 (c (n "maths-rs") (v "0.1.4") (h "0c5038szhmsdbalfcspi21zkx4q93aj80pmfbv2shas0ynnq1nqf") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.5 (c (n "maths-rs") (v "0.1.5") (h "1a7s1dgxyhmz0ks6js2wxgzgsxw4ld6ysc64w0rckzf3pz8dk9d9") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.6 (c (n "maths-rs") (v "0.1.6") (h "1kd10xlj5bfzvhskshmvcywaki5px3x1q4q2z65688vgfwl5686r") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.7 (c (n "maths-rs") (v "0.1.7") (h "150n6k9i4qgr9d8p3cwcrdk36rci1ik698xigwww6m24ffzl6x8m") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.8 (c (n "maths-rs") (v "0.1.8") (h "1n33i5kgki32fcaczbnzzj32nh8gyslriy1gx8jyys8b0vvb3c17") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.9 (c (n "maths-rs") (v "0.1.9") (h "1znpqdp4g707zgzz9fwrirr98kdfk2n2s6h2zdp2209s7xii98pb") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.10 (c (n "maths-rs") (v "0.1.10") (h "1y90ny41hkzavvx5q5vc5kszl2hniwkgn8rmkp0v3jq2cmbs98n1") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.11 (c (n "maths-rs") (v "0.1.11") (h "1yf2a2kmfqhbffh86jjidl5g7n8yjhlhajzj6j2xdpiviljj8lzl") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.12 (c (n "maths-rs") (v "0.1.12") (h "0iq1na98h2r4vfz8kf78fl3bi276rw1z1anx9ijpbzm14s6bqz14") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.13 (c (n "maths-rs") (v "0.1.13") (h "1lkxjsipcj8fhnw1arxq3yf3nfc8q5f3ybjm293r8cr90licrasw") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.1.14 (c (n "maths-rs") (v "0.1.14") (h "0ps8kgzx8jzcfydxjlr3wb6a7qvnd1s686r3fixr26lv3x6yj41p") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts"))))))

(define-public crate-maths-rs-0.2.0 (c (n "maths-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "08ryskry6rd2qajji0nygqqjl5vxfi6xli9l8fyyw2421s44j4ym") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.1 (c (n "maths-rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "13pymlg0f89xv3bmxi0kn3absbwaz065y6j3z4fi93yww8bw9a17") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.2 (c (n "maths-rs") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "1qc071m2vkd966xq163snsqi9ngcyzg8a3q7qi3p3d3akwkjhkqk") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.3 (c (n "maths-rs") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0sc6abb15h2k8g0n9gjmm74mhrwv9avfkpvbry7v1wk5sf1jhml6") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.4 (c (n "maths-rs") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "1pv3p9g7wkxww74arnbqb3zp4nflanr0x9g6i1bk1wrl1rbx6ram") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.5 (c (n "maths-rs") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0h48a4yliar3r3rr0l3ksxrxv4kgaamzdrja8yghnssid4gia7g7") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

(define-public crate-maths-rs-0.2.6 (c (n "maths-rs") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0m95rm4p1g15h18vwgrwgg12brkngjgzmjj49daag03m8qkfigx7") (f (quote (("short_types") ("short_hand_constructors") ("lhs_scalar_vec_ops") ("hash") ("default" "short_hand_constructors" "lhs_scalar_vec_ops" "casts" "short_types") ("casts")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json"))))))

