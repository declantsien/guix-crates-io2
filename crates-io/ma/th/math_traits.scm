(define-module (crates-io ma th math_traits) #:use-module (crates-io))

(define-public crate-math_traits-0.1.0 (c (n "math_traits") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "simd") (r "^0.2.0") (d #t) (k 0)) (d (n "tuple") (r "^0.3.3") (f (quote ("impl_simd"))) (d #t) (k 0)))) (h "05fyzwcarihf3dffm2ai00ldyvsywwjj6km96ilsr37b0dmf9rb8")))

(define-public crate-math_traits-0.1.1 (c (n "math_traits") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "simd") (r "^0.2.0") (d #t) (k 0)) (d (n "tuple") (r "^0.3.3") (f (quote ("impl_simd"))) (d #t) (k 0)))) (h "1asd5aafzmqa2pdvlfish4scxvr17mnnsbqwz9wknn2myx07b6g7")))

(define-public crate-math_traits-0.1.3 (c (n "math_traits") (v "0.1.3") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "tuple") (r "^0.3.8") (d #t) (k 0)))) (h "1n0dp9a03cp2327lslh5kbjskgpwpp218d24sfbyxi0p4x2jas28") (f (quote (("simd" "tuple/impl_simd" "stdsimd"))))))

(define-public crate-math_traits-0.1.4 (c (n "math_traits") (v "0.1.4") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "tuple") (r "^0.3.8") (k 0)))) (h "0k2mhh36gm6h97xnziwykd886v0wb7y6qpsaznrwl1kqdcnn0h6i") (f (quote (("simd" "tuple/impl_simd" "stdsimd"))))))

(define-public crate-math_traits-0.1.5 (c (n "math_traits") (v "0.1.5") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "tuple") (r "^0.4") (k 0)))) (h "0mfya7d5qi3lirqiy9ynwnwzs1pwmhfrax7c3n38n5gpg3h80734") (f (quote (("simd" "tuple/impl_simd"))))))

(define-public crate-math_traits-0.1.6 (c (n "math_traits") (v "0.1.6") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "tuple") (r "^0.4") (k 0)))) (h "0qgzvajz5wcsipywanwpyk5askpnb4m9psjgmhcbfaihs48br9ii") (f (quote (("simd" "tuple/impl_simd"))))))

(define-public crate-math_traits-0.2.0 (c (n "math_traits") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tuple") (r "^0.4") (k 0)))) (h "1ln0zghd263vyab454mfsnkm4ky3zm5l1c67f83hh49b8mih01sr") (f (quote (("simd" "tuple/impl_simd")))) (y #t)))

(define-public crate-math_traits-0.2.1 (c (n "math_traits") (v "0.2.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tuple") (r "^0.4") (k 0)))) (h "0wqm6r94d5k831m9dakjk6gj0wa3q5m2xw3pp96dkzmgg0i8dmng") (f (quote (("impl_simd" "tuple/impl_simd" "simd"))))))

