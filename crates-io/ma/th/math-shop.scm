(define-module (crates-io ma th math-shop) #:use-module (crates-io))

(define-public crate-math-shop-0.1.0 (c (n "math-shop") (v "0.1.0") (h "1ryqyd8fhbkp0wqd9qcqsylakfhp88h6bn6nwjfd6m2xbsks9g8g")))

(define-public crate-math-shop-0.1.1 (c (n "math-shop") (v "0.1.1") (d (list (d (n "refined-float") (r "^0.1.0") (d #t) (k 0)))) (h "1k56qqyr3ihns1794fz3n4akjlixvlab5cslna84y6m7909cmp5x")))

(define-public crate-math-shop-0.1.2 (c (n "math-shop") (v "0.1.2") (d (list (d (n "refined-float") (r "^0.1.1") (d #t) (k 0)))) (h "0b7h622isppvk3b77j0wj73cz0hck9kdklm8yiz88qbxr5l6mfgz")))

(define-public crate-math-shop-0.1.3 (c (n "math-shop") (v "0.1.3") (d (list (d (n "refined-float") (r "^0.2.0") (d #t) (k 0)))) (h "1dsx0xa8n8qglqgjjsw6s19zlcmfn8p5l4cxwfijzq6bfacqy8y9")))

