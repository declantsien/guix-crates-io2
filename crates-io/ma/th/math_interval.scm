(define-module (crates-io ma th math_interval) #:use-module (crates-io))

(define-public crate-math_interval-0.1.0 (c (n "math_interval") (v "0.1.0") (h "04wn34ylx0m11pc6wz821zj69m4ch9inwrv199xkm4vjqz0yr96r")))

(define-public crate-math_interval-0.1.1 (c (n "math_interval") (v "0.1.1") (h "1lzb8v9n0vccd25083js0rpw8l7cvja46aqv81qlmqmh08z93pd3")))

