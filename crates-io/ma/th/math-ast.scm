(define-module (crates-io ma th math-ast) #:use-module (crates-io))

(define-public crate-math-ast-0.1.0 (c (n "math-ast") (v "0.1.0") (d (list (d (n "factorial") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1bkp0mg5k7l6dnrxxh3dnykq0k2f1a9bafb7fn6cqs885rvbr9hy")))

(define-public crate-math-ast-0.2.0 (c (n "math-ast") (v "0.2.0") (d (list (d (n "factorial") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "01x6igqfn8wdbv91wqlc8986wgx5x7n8zlflxaaj5g80gg9g7la3")))

