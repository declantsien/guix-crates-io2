(define-module (crates-io ma th math_parser) #:use-module (crates-io))

(define-public crate-math_parser-0.1.0 (c (n "math_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1hb9m13c2cxnkim11kdwzvb02qpf5rnqm46ygamwjridkxmqhkmd")))

(define-public crate-math_parser-0.2.0 (c (n "math_parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1kkr9a7wzbcx642x5ar6ccfl1dqcpvyxgh0n84n5khy4qdgpjjzl")))

(define-public crate-math_parser-0.2.1 (c (n "math_parser") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "05gb4826lrnlg8ybq5yj9ffz6p70zvygqv675l348giffvca2w5a")))

(define-public crate-math_parser-0.3.0 (c (n "math_parser") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0c7j5a46jld79i03zvcjnaxk7xzvwxz911l21qr2nj22f3cvbbds") (f (quote (("visitor"))))))

(define-public crate-math_parser-0.3.1 (c (n "math_parser") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1qv533gsp8vlm9h3yp1z267nakp8fj7a7m4r3ngsg7grb8b01nxj") (f (quote (("visitor"))))))

(define-public crate-math_parser-0.3.2 (c (n "math_parser") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1r67mjf6k13h99y814hblamp4d8lzs3f7kkhjfr6s6vz6x2ya0qc") (f (quote (("visitor"))))))

