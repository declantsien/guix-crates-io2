(define-module (crates-io ma th math_module) #:use-module (crates-io))

(define-public crate-Math_module-0.1.0 (c (n "Math_module") (v "0.1.0") (h "19wfhv3h2wpcd3ac2vri1fd1gnvgawwy7d4021gsnrqbdqwvyqdw")))

(define-public crate-Math_module-0.1.1 (c (n "Math_module") (v "0.1.1") (h "01n16m8isw4acr6zj9abf0kwns86i8p4f2ghvirl32jc5sk40wcl")))

(define-public crate-Math_module-0.1.2 (c (n "Math_module") (v "0.1.2") (h "1vbr6n3a25ami7ys6d48d2n8m9m1dyj2shlg47mimqgnxyqjszg4")))

