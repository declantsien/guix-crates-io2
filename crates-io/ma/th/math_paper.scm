(define-module (crates-io ma th math_paper) #:use-module (crates-io))

(define-public crate-math_paper-0.3.4 (c (n "math_paper") (v "0.3.4") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15230wrjvkc0aldrv2lwrsjbiv6ii120bp8xwzlsqng2dlg38fj8")))

(define-public crate-math_paper-0.3.5 (c (n "math_paper") (v "0.3.5") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rf4ncff5zsqgrnlawbmmv30y2c6kkhi6ipid235rqq1rbh265kg")))

