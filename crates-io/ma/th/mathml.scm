(define-module (crates-io ma th mathml) #:use-module (crates-io))

(define-public crate-mathml-0.1.0 (c (n "mathml") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "107wvnk31nkw1glc8ni9b9dlp5rngni9xgcz5v9f8nlmba0yydfj")))

(define-public crate-mathml-0.2.0 (c (n "mathml") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "175xgw0abgpi27sgcjfjrc7dwkqq4krp0s4m6s7slp9xamcqnk0x")))

(define-public crate-mathml-0.4.0 (c (n "mathml") (v "0.4.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1zsr7s3xdv7hpf1csm713wwnhs3d8g8i6v9jc62gl8wjlqr2c6s3")))

(define-public crate-mathml-0.4.1 (c (n "mathml") (v "0.4.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1fvxrcbd7v8akczb9gc29gp0xph6xhzrcfqn6mzjv7vbxhlaaldl")))

(define-public crate-mathml-0.4.2 (c (n "mathml") (v "0.4.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "02fhabi572fqgki6qvm58kpnbvprmymqhv28gyp5f7s0y7g9mc1f")))

(define-public crate-mathml-0.4.3 (c (n "mathml") (v "0.4.3") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1fac3vpc2idj2ry4x1i9gfj77381vvb7fj29mqmf8xh4h73xyyai")))

(define-public crate-mathml-0.4.4 (c (n "mathml") (v "0.4.4") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1vq3wrkddvangzj26cisa9d6qz1ravg2kygbj3b5b50lip8q1pmx")))

