(define-module (crates-io ma th mathf) #:use-module (crates-io))

(define-public crate-mathf-0.1.0 (c (n "mathf") (v "0.1.0") (h "01pi293x704fg2rcid832ynd7m1z2fq0fa8vzqhj6asrnhjchpw0")))

(define-public crate-mathf-0.1.1 (c (n "mathf") (v "0.1.1") (h "0lkg1xag40p2xy5n26587j3894f90mglyir36c0dybc9g97hnj33")))

(define-public crate-mathf-0.1.2 (c (n "mathf") (v "0.1.2") (h "0vzv6qksamnp6ajnfv1v9v8q2w0a4g7cvnskbf5q9d6xkk6h6mz4")))

(define-public crate-mathf-0.1.3 (c (n "mathf") (v "0.1.3") (h "1ssmnnbh1n1ji6b1dmwph737s3fg5jw46h3phjxnpp7mp5ya0wly")))

(define-public crate-mathf-0.1.4 (c (n "mathf") (v "0.1.4") (h "049rz7bxiin0ssfa4ldwf5283j58vz1773sfv7hfbhl84b26fpdx")))

(define-public crate-mathf-0.1.5 (c (n "mathf") (v "0.1.5") (h "0xlpgnn6gk15zm7253d6zj13624xwifpa3zd8s7058gcqxb81688")))

(define-public crate-mathf-0.1.6 (c (n "mathf") (v "0.1.6") (h "0y907cq504fyi5f96hvjpsgzy6g19ziiff0nhacy0c78fidb7p1x")))

(define-public crate-mathf-0.1.7 (c (n "mathf") (v "0.1.7") (h "16gg5pzy3pv34gjx1nrkyqlv7dz2jzg5id504pdrvc03csnz0gn6")))

(define-public crate-mathf-0.1.8 (c (n "mathf") (v "0.1.8") (h "09x27s6d42ihg46kwi3jhx7nibhvsrmwscpd3vjcnbzjx1ld9kjk")))

(define-public crate-mathf-0.1.9 (c (n "mathf") (v "0.1.9") (h "0mfsyvp8i903d0gzj8iyx91r4mhj86lhaaw16mk18wmxfn5023w2")))

(define-public crate-mathf-0.1.10 (c (n "mathf") (v "0.1.10") (h "0q58by1rhp69sz8xxdbv8ik94g9qgy4is0xp47d5l4mg34k33fsa")))

(define-public crate-mathf-0.1.11 (c (n "mathf") (v "0.1.11") (h "0qkhsza44r0z6nsp7dpqghgallrc3v2w01gyb00i8n7fy3cr2j95")))

(define-public crate-mathf-0.1.12 (c (n "mathf") (v "0.1.12") (h "16lpn9jphrlbyxcvdcqswyvkcx1n7yxnb48iy0ajdfyjkcaay537")))

(define-public crate-mathf-0.1.13 (c (n "mathf") (v "0.1.13") (h "0g0pxkx5wkc79xn86c0p1wjv5sflff85mpr79kb1q8750wy6xg32")))

(define-public crate-mathf-0.1.14 (c (n "mathf") (v "0.1.14") (h "0msffijsrxvlqj3j43qd1425qrmh6q5qdrsnlahappmp1csl6id7")))

(define-public crate-mathf-0.1.15 (c (n "mathf") (v "0.1.15") (h "1zw9vyzx0dqa52z9sdvwsmqk5ig0ymjivc4rfac3r4vjsqsikm65")))

(define-public crate-mathf-0.1.16 (c (n "mathf") (v "0.1.16") (h "1b3ks9bny5f253k0iy81bzdk7d95j0diad5lb58gxfhi7nbdjr1s")))

