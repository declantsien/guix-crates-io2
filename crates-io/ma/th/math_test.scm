(define-module (crates-io ma th math_test) #:use-module (crates-io))

(define-public crate-math_test-0.1.1 (c (n "math_test") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02hj9h1xbaziasa37jf9pdyv57awviaixpd218gqh9miq3wx16zh") (y #t)))

(define-public crate-math_test-0.1.2 (c (n "math_test") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m1fq89aaj6ixxyh9jl57j1dg187ga3a6i627wj1dl6pk643frvs") (y #t)))

(define-public crate-math_test-0.1.3 (c (n "math_test") (v "0.1.3") (d (list (d (n "genpdf") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b8fc79rhkic8282hi7i7xvi5zk0g408sii5q9kdl1plcshc8i3g") (y #t)))

(define-public crate-math_test-1.0.0 (c (n "math_test") (v "1.0.0") (d (list (d (n "genpdf") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l18bqkfj3ba7dq62qg3wg3xbqxv140zd06mp0hfgjpv1x69spam")))

