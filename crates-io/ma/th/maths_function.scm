(define-module (crates-io ma th maths_function) #:use-module (crates-io))

(define-public crate-maths_function-0.0.5 (c (n "maths_function") (v "0.0.5") (h "0y1z3r8varvs01jraf8k75a4sm9kdf0i2yx0p3rxgzjfmjnqxqwi")))

(define-public crate-maths_function-0.1.0 (c (n "maths_function") (v "0.1.0") (h "0f30qb7f6aa9pfypbavfr0cag6nqg16d8pfaps8i9zp32lagl2qh")))

(define-public crate-maths_function-0.1.1 (c (n "maths_function") (v "0.1.1") (h "1lf18id6zxfy3i9n4z3kckggk05vlw1hnxil2vizs7wpppjy11gi")))

(define-public crate-maths_function-0.1.2 (c (n "maths_function") (v "0.1.2") (h "0i77rnh67micqnrp29b4zg1nn9xc4m28jdnr9qpw3ih81qnvs1g6")))

(define-public crate-maths_function-0.1.3 (c (n "maths_function") (v "0.1.3") (h "0zjpl0raycz6d22nljsmrbbn9wyyc830zwk9xl669kcm4wchcb64")))

(define-public crate-maths_function-0.1.4 (c (n "maths_function") (v "0.1.4") (h "031zgcik2mly24lcl2w3dwjmd1kb4j5zxmvbbncxliki8nx2fw56")))

