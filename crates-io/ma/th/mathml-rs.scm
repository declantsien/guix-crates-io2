(define-module (crates-io ma th mathml-rs) #:use-module (crates-io))

(define-public crate-mathml-rs-0.1.0 (c (n "mathml-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "mathml-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "045l40k3sph06q5cb0s15lhl522jh7rgkcj3yp9sbq5qryqzqfzr") (y #t)))

(define-public crate-mathml-rs-0.1.1-alpha.1 (c (n "mathml-rs") (v "0.1.1-alpha.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "mathml-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0i40z009kpn5ba8gnn8qqqb0yw94sgk4dy00m9k60wbzlqs7b24c")))

(define-public crate-mathml-rs-0.1.1-alpha.2 (c (n "mathml-rs") (v "0.1.1-alpha.2") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "mathml-macros") (r "^0.1.1-alpha.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "04j2922z4wqkmf1m4jx48zdc7zcgpflddh9185an2s9s33ggi312")))

(define-public crate-mathml-rs-0.1.1 (c (n "mathml-rs") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "mathml-macros") (r "^0.1.1-alpha.1") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1wxysd9f6q3zgrncg835ijfb8sbyz444mngdxc7nxiya6ypxwpld")))

(define-public crate-mathml-rs-0.1.2 (c (n "mathml-rs") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "mathml-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "016n85v7n9sz5x1w8vj4m6zs3jbzlgw9yz1rfv6qfsik3q9fhcjh")))

