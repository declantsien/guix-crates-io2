(define-module (crates-io ma th mathml-core) #:use-module (crates-io))

(define-public crate-mathml-core-0.0.0 (c (n "mathml-core") (v "0.0.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0zqq7f60zmdfq7nsa3mgry850kpgxds35j4hax7qnrpdzm5qh7jz") (f (quote (("default"))))))

(define-public crate-mathml-core-0.0.1 (c (n "mathml-core") (v "0.0.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "14y3svqpclddh4b0pywyi7h7b3jksfhhphy4ip70pv7jphi3jpn2") (f (quote (("default"))))))

(define-public crate-mathml-core-0.0.2 (c (n "mathml-core") (v "0.0.2") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)))) (h "193jfwnwx26vsabdgas9qvlgfaz6946qij9hhrxgzxhndpb4c1l3") (f (quote (("default"))))))

(define-public crate-mathml-core-0.0.3 (c (n "mathml-core") (v "0.0.3") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)))) (h "0xqq859zc3raz8hb4npia9hxwyjggka5sk15izrz14wb2rl91lvz") (f (quote (("default"))))))

(define-public crate-mathml-core-0.0.4 (c (n "mathml-core") (v "0.0.4") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)))) (h "0rc8dz6dnk0n55jyzd2jk373mrywdqa8k4hc3rhbxn1md6s86wsv") (f (quote (("default"))))))

(define-public crate-mathml-core-0.0.5 (c (n "mathml-core") (v "0.0.5") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)))) (h "10vxrx6bbmdw3nf6s59004rln3ysb3zj244g4cr9rin2gng8h5b5") (f (quote (("default"))))))

(define-public crate-mathml-core-0.1.0 (c (n "mathml-core") (v "0.1.0") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)))) (h "08ygi5zpdhyscp7q7q6lkx4lyiz23q4kd47kawsb6654d11ac75h") (f (quote (("default"))))))

(define-public crate-mathml-core-0.1.1 (c (n "mathml-core") (v "0.1.1") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cpcn87ab70h3ljrgdryig0w1xf530zv7vqa30zs81fflc0g2ma1") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.2 (c (n "mathml-core") (v "0.1.2") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jyfrg1y08zj5knc7a6vdb1lqwx684xws4sammx8h76d6m3p1q4p") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.3 (c (n "mathml-core") (v "0.1.3") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sbqkv91dliqx0k8idvcmvv7gigqyya5dv6c27lmfm7bj3c7mf8s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.4 (c (n "mathml-core") (v "0.1.4") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06c8di1lhqvd81cb929kzwx11qvknm458rnhbbkirgh6kcy0mxhi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.5 (c (n "mathml-core") (v "0.1.5") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "109c3kz43g5x3qbw6i6q8y8d27a6jsmidqnjxhh3hjg67as40hxc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.6 (c (n "mathml-core") (v "0.1.6") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s152xhyqfgbv5wlkvh0al4nrswzsw8mjnh1nlcwi5xbls4m1m6k") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathml-core-0.1.7 (c (n "mathml-core") (v "0.1.7") (d (list (d (n "latexify") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12i3dk1jx800yv5vc02hrl5yj5q1fz8435kbvf9k96f90h8mg89h") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

