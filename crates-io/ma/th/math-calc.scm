(define-module (crates-io ma th math-calc) #:use-module (crates-io))

(define-public crate-math-calc-0.1.0 (c (n "math-calc") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0kyi6gkkhxx54bzqgix02ww2d4vqzv5wdn5ybvndydrb833h27m4")))

