(define-module (crates-io ma th mathemizer) #:use-module (crates-io))

(define-public crate-mathemizer-0.0.1 (c (n "mathemizer") (v "0.0.1") (h "0f1mbhc0vhnbd7p425ad54j2pd18g2iar4dgzakwpl4rs298k7c4")))

(define-public crate-mathemizer-0.0.2 (c (n "mathemizer") (v "0.0.2") (h "1lskqpv36nnymamqcyl3ql3jbpc9mkfxv8nm00w5p3crjkvg1qp8")))

