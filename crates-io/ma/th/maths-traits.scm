(define-module (crates-io ma th maths-traits) #:use-module (crates-io))

(define-public crate-maths-traits-0.1.0 (c (n "maths-traits") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bqldhm6vkn4vg46nh26a164whw7dvcx2cxm7iw3laxabgdj10bv") (y #t)))

(define-public crate-maths-traits-0.1.1 (c (n "maths-traits") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m1hhrg5bq56600hlgyv6ff3192lbbaryq8ncsvrw1xz4wc9s5zi")))

(define-public crate-maths-traits-0.1.2 (c (n "maths-traits") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1z0bw0lqj3dx1b3a0wyrlmmczspkfw7ip2367142623wydjhayxh")))

(define-public crate-maths-traits-0.1.3 (c (n "maths-traits") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "049zdm0qqbx8p5h3j4rqii6sd6dqi31crnlki3wcvrzkwllnnlcb") (f (quote (("std") ("default" "std"))))))

(define-public crate-maths-traits-0.2.0 (c (n "maths-traits") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1dsa4hjfapz0i61akxbrcj71jknwwakw5n2hmd8m6v1isckwjam3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-maths-traits-0.2.1 (c (n "maths-traits") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0p1dsp6s5parddh75mxj4bggdvnanflrn4afg5d4spqfrphjdw12") (f (quote (("std") ("default" "std"))))))

