(define-module (crates-io ma th mathjax_svg) #:use-module (crates-io))

(define-public crate-mathjax_svg-0.1.0 (c (n "mathjax_svg") (v "0.1.0") (d (list (d (n "rusty_v8") (r "^0.32.1") (d #t) (k 0)))) (h "08wg043mh2jwsv746zvik4bq5cbb0ld59x77wnjg9sq37xr7hd4n")))

(define-public crate-mathjax_svg-1.0.0 (c (n "mathjax_svg") (v "1.0.0") (d (list (d (n "rusty_v8") (r "^0.32.1") (d #t) (k 0)))) (h "1a9b3fma3ix2l63cqqkrvrzyx0j66mgnval7a7pfz0g64bn0x2ll")))

(define-public crate-mathjax_svg-1.0.1 (c (n "mathjax_svg") (v "1.0.1") (d (list (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "12wmz09xdcxhg1zray7m82w4rg9bybrpwy84449x8w9mh44x8k9l")))

(define-public crate-mathjax_svg-2.0.0 (c (n "mathjax_svg") (v "2.0.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "0av68ri3y9d1rgg4ycwyar73xcysl0qddllxzar0r5l12jljkn94")))

(define-public crate-mathjax_svg-3.0.0 (c (n "mathjax_svg") (v "3.0.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "1a666pfjsz1q2nlxm23spmv6v9mh1m868wdfv22cwzv5q8ghap1z")))

(define-public crate-mathjax_svg-3.1.0 (c (n "mathjax_svg") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "11rp7721chijaa6fhqq983yp8348nw9dsqf983biygxflwnk3qbg")))

(define-public crate-mathjax_svg-3.1.1 (c (n "mathjax_svg") (v "3.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "1a97d5xjqpf65dwn2xv8j2s8p7g3yywg2m0dzbwgaa4y8hfl89hy")))

(define-public crate-mathjax_svg-3.1.2 (c (n "mathjax_svg") (v "3.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "v8") (r "^0.80.0") (d #t) (k 0)))) (h "1yigjfs62dlvyvvalf4rfbifhbfhala1xh038ywc4gsw212kg5wd")))

