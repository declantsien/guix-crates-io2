(define-module (crates-io ma th math-rs) #:use-module (crates-io))

(define-public crate-math-rs-0.1.0 (c (n "math-rs") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "0p421091cv08q9c4ld3sysv5wqxi90cdf8in4hw3yy81xyl58ad8")))

(define-public crate-math-rs-0.1.4 (c (n "math-rs") (v "0.1.4") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "09230wsz753dxx46hdhljjgawzhnkjw8wjwzjcvi5ji2gfbi27bg")))

(define-public crate-math-rs-0.1.5 (c (n "math-rs") (v "0.1.5") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "03sh5paixalks33vz72sqhcqy519v47932w0was2yzsav5blnj23")))

