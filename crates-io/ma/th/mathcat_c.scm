(define-module (crates-io ma th mathcat_c) #:use-module (crates-io))

(define-public crate-mathcat_c-0.1.0 (c (n "mathcat_c") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "mathcat") (r "^0.1.23") (d #t) (k 0)) (d (n "mathcat") (r "^0.1.23") (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 1)))) (h "08nxww13f4r03h4kgj3kzya0sbls779gmiz7j4k79ps5c1zrgnn8")))

