(define-module (crates-io ma th mathper) #:use-module (crates-io))

(define-public crate-mathper-0.1.0 (c (n "mathper") (v "0.1.0") (h "1d4rz54yfq5x20jzsghqxdd3wask4gk2mlxgxlzq9x4l24ncgqbf")))

(define-public crate-mathper-0.1.1 (c (n "mathper") (v "0.1.1") (h "0428bl8wp628ari35w93yy9zgcpc9rby5wkinadm25qym8lq3ly5")))

(define-public crate-mathper-0.1.2 (c (n "mathper") (v "0.1.2") (h "1kvhrxwbwkvdn18rrw0cjqyb4m4jkrj6dmf93rjgpsidwqa64fwq")))

