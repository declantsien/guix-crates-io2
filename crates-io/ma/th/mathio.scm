(define-module (crates-io ma th mathio) #:use-module (crates-io))

(define-public crate-mathio-0.1.0 (c (n "mathio") (v "0.1.0") (d (list (d (n "deku") (r "^0.16.0") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (k 0)))) (h "1msbrmhm0f73bx4c5yyic56jn0i4s6lc73gi7yn9vsfa39h3wrx7")))

(define-public crate-mathio-0.2.0 (c (n "mathio") (v "0.2.0") (d (list (d (n "deku") (r "^0.16.0") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (k 0)))) (h "1bmwnvis7wgfwcq0jg8fs2al9rz0108s4s3phrd4875jh4lr5py6")))

(define-public crate-mathio-0.2.1 (c (n "mathio") (v "0.2.1") (d (list (d (n "deku") (r "^0.16.0") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (k 0)))) (h "15flsvkvd9dfis3hqbblw2xdfhkgw4d84szyna1n50gqni480klv")))

(define-public crate-mathio-0.2.2 (c (n "mathio") (v "0.2.2") (d (list (d (n "deku") (r "^0.16.0") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (k 0)))) (h "1a29zwfzbjdf0q1jav0pwksfphp64dq6q1jpzf5f68n8rfcsbmnq")))

(define-public crate-mathio-0.2.3 (c (n "mathio") (v "0.2.3") (d (list (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "deku") (r "^0.16") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "speedy") (r "^0.8") (f (quote ("speedy-derive"))) (o #t) (k 0)))) (h "1cy79is26ak0ma7sz3fmr7c0avsg4mh6k4wvw98b2zymix716kyv") (s 2) (e (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("deku" "dep:deku") ("bevy" "dep:bevy"))))))

(define-public crate-mathio-0.2.4 (c (n "mathio") (v "0.2.4") (d (list (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "deku") (r "^0.16") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "speedy") (r "^0.8") (f (quote ("speedy-derive"))) (o #t) (k 0)))) (h "1s3lpgvjwhpjby1g25xcb21kn4r2yjingaqnph5plqr3f2rrpx8h") (s 2) (e (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("deku" "dep:deku") ("bevy" "dep:bevy"))))))

(define-public crate-mathio-0.2.5 (c (n "mathio") (v "0.2.5") (d (list (d (n "bevy") (r "^0") (o #t) (k 0)) (d (n "deku") (r "^0.16") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "speedy") (r "^0.8") (f (quote ("speedy-derive"))) (o #t) (k 0)))) (h "1f6x9vb9b4h29xpxrv2jw197j3pkcz1jxd3bphbghixfb24jqq2c") (s 2) (e (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("deku" "dep:deku") ("bevy" "dep:bevy"))))))

