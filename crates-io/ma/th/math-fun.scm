(define-module (crates-io ma th math-fun) #:use-module (crates-io))

(define-public crate-math-fun-0.0.1 (c (n "math-fun") (v "0.0.1") (d (list (d (n "plotly") (r "^0.8.3") (f (quote ("kaleido"))) (d #t) (k 2)))) (h "1mgn0gcdfklxh5x9a6kb2q72n98jxw7ph0vy6dh3j4swhq7krccd")))

(define-public crate-math-fun-0.0.2 (c (n "math-fun") (v "0.0.2") (d (list (d (n "plotly") (r "^0.8.3") (f (quote ("kaleido"))) (d #t) (k 2)))) (h "0xzn4zllilqkwzh7pjch2a9vms5pmpwszsns9ip7kd8ssvkmlzxp")))

(define-public crate-math-fun-0.0.3 (c (n "math-fun") (v "0.0.3") (d (list (d (n "plotly") (r "^0.8.3") (f (quote ("kaleido"))) (d #t) (k 2)))) (h "0qkd06rsvpsmj11w01mxm0wi787myz382dr4cy2n9xm2kgzwzxlk")))

