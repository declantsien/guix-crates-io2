(define-module (crates-io ma th mathlogic) #:use-module (crates-io))

(define-public crate-mathlogic-0.1.0 (c (n "mathlogic") (v "0.1.0") (h "1mk4qfa1hgirzgl7db8b65dqhrxbrw3lcsllgjfqrgd5fv46fkdm")))

(define-public crate-mathlogic-0.1.1 (c (n "mathlogic") (v "0.1.1") (h "08n18cxhyxy2mf846czyzxgxzdny2l50xbcq4krb6g114cbysv0s")))

(define-public crate-mathlogic-0.1.2 (c (n "mathlogic") (v "0.1.2") (h "0spazcjq7jas7p1fz2fjz6lbwh9a757q81xdbszaiknyrxlckg9p")))

(define-public crate-mathlogic-0.1.3 (c (n "mathlogic") (v "0.1.3") (h "0q0d4aw80lm6dn854ff03wi315md0qg163pd0kvgvs00vfb5x5fw")))

