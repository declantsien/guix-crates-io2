(define-module (crates-io ma th math_exam) #:use-module (crates-io))

(define-public crate-math_exam-1.0.1 (c (n "math_exam") (v "1.0.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0lv0kcrpkh64ih0qljbf1g59nsamxvf5mcf6ab0rf1b7f0dd729y") (y #t)))

(define-public crate-math_exam-1.0.2 (c (n "math_exam") (v "1.0.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "12nwcm0kgr383hg9d0j4sygq39wip5n3kyy1hxgka9aj5z39k7id") (y #t)))

