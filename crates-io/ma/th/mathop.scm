(define-module (crates-io ma th mathop) #:use-module (crates-io))

(define-public crate-mathop-0.1.0 (c (n "mathop") (v "0.1.0") (h "1imv58z1w8w4n3v3177a7nm903k5nqpaza7kw6mmlldpys93sqm9")))

(define-public crate-mathop-0.1.1 (c (n "mathop") (v "0.1.1") (h "0z3hcjbdm2zm4bssv96m8d76gpwq611yndgki28vfj6s7sbfa5s5")))

(define-public crate-mathop-0.1.2 (c (n "mathop") (v "0.1.2") (h "04p5zxjz0489wv6szy64d86zgqfb9y6hqn517prjpz3x621krlci")))

(define-public crate-mathop-0.1.3 (c (n "mathop") (v "0.1.3") (h "007zqdynndiv2b7rvdbv4linh6i3h93mgzkj8dc9dw8wdnakn5bw")))

(define-public crate-mathop-0.1.4 (c (n "mathop") (v "0.1.4") (h "128x43cs23m6kl9y6jwmr81jscl5f55y7r2ah37rhs6gpli2jkq0")))

(define-public crate-mathop-0.1.5 (c (n "mathop") (v "0.1.5") (h "0lkmpvf21fwgnr6lkyb1ydjzj6320pc3ma78s56mrka6ymcnh2ak")))

(define-public crate-mathop-0.1.6 (c (n "mathop") (v "0.1.6") (h "1arhv2lxgrgr41nsb39mjw5wx1hr33mzv5wbr8jr7gj9gxjnnsz7")))

