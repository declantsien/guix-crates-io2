(define-module (crates-io ma th mathew) #:use-module (crates-io))

(define-public crate-mathew-0.0.1 (c (n "mathew") (v "0.0.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "1k0qb2m23073vqn24mc0vbnnjnmwff9jl087zhdrywass7d3fw6y") (f (quote (("double") ("default"))))))

(define-public crate-mathew-0.0.2 (c (n "mathew") (v "0.0.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "12s6ivlgi1683bry1v2c710q28fy30b85rs6c4b36z9j166zjbkd") (f (quote (("double") ("default"))))))

