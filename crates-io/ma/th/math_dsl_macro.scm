(define-module (crates-io ma th math_dsl_macro) #:use-module (crates-io))

(define-public crate-math_dsl_macro-0.1.0 (c (n "math_dsl_macro") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("parsing" "full" "extra-traits"))) (k 0)))) (h "0a3y1lnpvg4bpw1p0d4g48d7r1gy7b4jqpv6mw4xysvxj98x3a7q")))

