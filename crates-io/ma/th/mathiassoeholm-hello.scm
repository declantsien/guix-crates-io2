(define-module (crates-io ma th mathiassoeholm-hello) #:use-module (crates-io))

(define-public crate-mathiassoeholm-hello-0.1.1 (c (n "mathiassoeholm-hello") (v "0.1.1") (h "04ynpwjr5r5km2bmpzwd1gkv4lji60jxhdjlxp7yhlfkvrdvaiik")))

(define-public crate-mathiassoeholm-hello-0.1.2 (c (n "mathiassoeholm-hello") (v "0.1.2") (h "0ssz0941nln6sj7mkhfbhz6xh6jac4zwr16465mk0h3aib2j6kwa")))

