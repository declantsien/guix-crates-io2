(define-module (crates-io ma th mathvites) #:use-module (crates-io))

(define-public crate-mathvites-0.0.1 (c (n "mathvites") (v "0.0.1") (h "0snyc635g1nh1hx3zdn7slgq6j9prvvbb6cl7vhwwanjxaasbg08")))

(define-public crate-mathvites-0.0.2 (c (n "mathvites") (v "0.0.2") (h "1v1f9f7nrcibi15d3xx4hg422wnwwcccpyqawrlg9bbv49z6aby4")))

(define-public crate-mathvites-0.0.3 (c (n "mathvites") (v "0.0.3") (h "11h1wlbvp0729vl0mj0bpzi7y8z1h6g0px2rddllva6nsm22rhsv")))

