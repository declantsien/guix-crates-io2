(define-module (crates-io ma th mathol) #:use-module (crates-io))

(define-public crate-mathol-0.1.0 (c (n "mathol") (v "0.1.0") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)))) (h "1rj2wmm9gj1fcv07w1sag6pw53fj6vhn6qnkrrd3m0q5qzs6kv6f")))

(define-public crate-mathol-0.1.1 (c (n "mathol") (v "0.1.1") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)))) (h "1iqqjp7hxmazr6w9bnp24q5lh8invx4l1swl25prw00rq71xajwz")))

