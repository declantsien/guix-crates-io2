(define-module (crates-io ma th math_rust) #:use-module (crates-io))

(define-public crate-math_rust-0.1.0 (c (n "math_rust") (v "0.1.0") (h "0b26qkbkb7wlc15p40r4d952i6p76fx3z4q7xzyvi1v4nv8qyq36") (y #t)))

(define-public crate-math_rust-0.1.1 (c (n "math_rust") (v "0.1.1") (h "0lz1p60xfa6n1adc9r4brpqkdm72vxfj8m67s3jqa07r3962bx2r")))

(define-public crate-math_rust-0.1.2 (c (n "math_rust") (v "0.1.2") (h "10nwv6nblzl8805vjwzw0w8glj340yal3j0pv5mcpvqjmcz8rdsn")))

(define-public crate-math_rust-0.1.3 (c (n "math_rust") (v "0.1.3") (h "0g37cx0353wdfkfglmrcck2vd2205yyhswxvxpfj0c3fns6xv65j")))

(define-public crate-math_rust-0.1.4 (c (n "math_rust") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dfm9i5si617hhw73wfxf9amc77cfpmpw5d9jxc4a8vgc6rq7rab")))

(define-public crate-math_rust-0.1.5 (c (n "math_rust") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06xqfh8qin9f7af0yn571fh1043i6br1ca9w3i7ljs4izbwrm0cl")))

(define-public crate-math_rust-0.1.6 (c (n "math_rust") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0wvldiq9pzz7lnb6kxk47d742km528ln0f4aywmg3f7ic8xdhn1b")))

(define-public crate-math_rust-0.1.7 (c (n "math_rust") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1kyyvccyyngx74zlg8sfp3h5fhd4vvkjr5zcvzq2jcz97lxpjyb0")))

