(define-module (crates-io ma th math-engine) #:use-module (crates-io))

(define-public crate-math-engine-0.1.0 (c (n "math-engine") (v "0.1.0") (d (list (d (n "lexgen") (r "^0.14.0") (d #t) (k 0)) (d (n "lexgen_util") (r "^0.14.0") (d #t) (k 0)) (d (n "pomelo") (r "^0.1.5") (d #t) (k 0)))) (h "14najmyhqf4vybcv2r35lv5z5q0yknjhwpa762z5cnwzc0hbpl8k")))

(define-public crate-math-engine-0.2.0 (c (n "math-engine") (v "0.2.0") (d (list (d (n "lexgen") (r "^0.14.0") (d #t) (k 0)) (d (n "lexgen_util") (r "^0.14.0") (d #t) (k 0)) (d (n "pomelo") (r "^0.1.5") (d #t) (k 0)))) (h "0l2vrfwwgxj3d68wj5li51r811ln76h40p5g6yp5z8a5mkzdkqmf")))

