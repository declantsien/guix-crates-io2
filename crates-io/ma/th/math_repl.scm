(define-module (crates-io ma th math_repl) #:use-module (crates-io))

(define-public crate-math_repl-0.1.0 (c (n "math_repl") (v "0.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "math_utils_lib") (r "^0.1.8") (d #t) (k 0)))) (h "17svyfp5jhqdldwhrm3jrwvhxcnr81n20n6al845cw9zp7rmn01l") (f (quote (("doc-images"))))))

(define-public crate-math_repl-0.1.1 (c (n "math_repl") (v "0.1.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "math_utils_lib") (r "^0.2.0") (d #t) (k 0)))) (h "01kwc12kpfqkrfwnmzpkp7vkq21i7jm5xzfxiazgg4887hhjs9jz") (f (quote (("doc-images"))))))

(define-public crate-math_repl-0.2.0 (c (n "math_repl") (v "0.2.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "math_utils_lib") (r "^0.3.0") (d #t) (k 0)))) (h "1qwnd034bdrpwrbpmghn31mp19dc4fkvp2ci9wqzp1swwsvsbd88") (f (quote (("doc-images"))))))

(define-public crate-math_repl-0.2.1 (c (n "math_repl") (v "0.2.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "math_utils_lib") (r "^0.3.0") (d #t) (k 0)))) (h "04jxilr746vign5iarwjkk1pq5m8l3xl7gdimk7id8cdxmahd1qv") (f (quote (("doc-images"))))))

