(define-module (crates-io ma th math-parse) #:use-module (crates-io))

(define-public crate-math-parse-0.1.0 (c (n "math-parse") (v "0.1.0") (h "1bkphw3166kqjb048hasn2va8rd5a3ayi15f76l00hbrkq1rz47z")))

(define-public crate-math-parse-0.2.0 (c (n "math-parse") (v "0.2.0") (h "04ccgd0cb8gqc03rcpav2igxix4sj5razkkzvyh9jmwfq5zym0q4")))

(define-public crate-math-parse-0.3.0 (c (n "math-parse") (v "0.3.0") (h "0alz4a0ilf9y4h7xqgxmspyz5ycmz1gagsj1ly8sdrd815bhc0l1")))

(define-public crate-math-parse-0.4.0 (c (n "math-parse") (v "0.4.0") (h "0xnajs00s5yyapmdzk7dhhbj94xp4wwvhklckp1p7fl0cccavq7c")))

(define-public crate-math-parse-1.0.0 (c (n "math-parse") (v "1.0.0") (h "10xm9j0x6xlldfbq1qcb5b0jgpbzs6k2ar3c3dlz4b20ky1y28kd")))

(define-public crate-math-parse-1.0.1 (c (n "math-parse") (v "1.0.1") (h "0lyjg05fszvi2q8cp5agi9lhl8rjnpsjzv3298v4zffdvaxq7slx")))

(define-public crate-math-parse-1.0.2 (c (n "math-parse") (v "1.0.2") (h "1njzbk917rddgij5isb3ffs7csm723ny9vd5n8cznr3plgbc2rpq")))

