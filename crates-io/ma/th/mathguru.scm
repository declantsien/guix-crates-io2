(define-module (crates-io ma th mathguru) #:use-module (crates-io))

(define-public crate-mathguru-0.1.0 (c (n "mathguru") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07iwlqyfm4wdjf7mkkwfbhq5crmpcc496708px7fvm7lirbvf8ap")))

(define-public crate-mathguru-0.2.0 (c (n "mathguru") (v "0.2.0") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0k35zh6xfzwbs1rvhm9z25akpgswfbz6fnpxbk6rvb9a9pdg27rg")))

