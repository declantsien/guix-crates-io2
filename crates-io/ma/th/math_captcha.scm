(define-module (crates-io ma th math_captcha) #:use-module (crates-io))

(define-public crate-math_captcha-0.1.0 (c (n "math_captcha") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1ra8bvfd9clidcda1z82snnrg1z6fg6ja2x5ggfcgdx04z563mgy")))

(define-public crate-math_captcha-0.1.1 (c (n "math_captcha") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "170f3dqh7adircnd27988i3v6n3357402b9kvxpbr4zmlqdb49s9")))

