(define-module (crates-io ma th mathemascii) #:use-module (crates-io))

(define-public crate-mathemascii-0.1.0 (c (n "mathemascii") (v "0.1.0") (h "0h8nzb16bq6s4qrs57wlblphnbf01qzczvss71idqgsylp0ncfvr")))

(define-public crate-mathemascii-0.2.0 (c (n "mathemascii") (v "0.2.0") (d (list (d (n "alemat") (r "^0.6.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1k5xnifchzw9590rg7rzhbjmyn9xa4gsdjzdn9p0zd81pjygh6sc")))

(define-public crate-mathemascii-0.3.0 (c (n "mathemascii") (v "0.3.0") (d (list (d (n "alemat") (r "^0.7.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "1j8mq0rary1zla0l2mil3xjfqxsrais1gf0zkmim4s89hw9sbc5y")))

(define-public crate-mathemascii-0.3.1 (c (n "mathemascii") (v "0.3.1") (d (list (d (n "alemat") (r "^0.7.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "0cn83vil9rbf08gzmak9y6brd3cw8mzkil5j47mizgwi98x1q441")))

(define-public crate-mathemascii-0.4.0 (c (n "mathemascii") (v "0.4.0") (d (list (d (n "alemat") (r "^0.8.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "xmlem") (r "^0.2.3") (d #t) (k 2)))) (h "0kz2xa3jwzhy2p3pd4izmhb3a6f6g3f7dnmrwaj4wh74r469xpay")))

