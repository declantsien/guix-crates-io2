(define-module (crates-io ma th mathbox) #:use-module (crates-io))

(define-public crate-mathbox-0.1.0 (c (n "mathbox") (v "0.1.0") (h "0rcxs4sxdrl9n2mi4a1a3l7wi9liwzq8j4jpsyp89pcxh8r1lch2") (y #t)))

(define-public crate-mathbox-0.2.0 (c (n "mathbox") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "find_peaks") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0azfm25zgvlr2bfh5lzncbzlwv7px75h88q1azkn2dyzfnxd81hh")))

