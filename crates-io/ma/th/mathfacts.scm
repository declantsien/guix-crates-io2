(define-module (crates-io ma th mathfacts) #:use-module (crates-io))

(define-public crate-mathfacts-0.1.0 (c (n "mathfacts") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0169sf5qjq8xjbn1n87h7brj44zp2dhs89wdcsf321mkghn5p3km")))

(define-public crate-mathfacts-0.1.1 (c (n "mathfacts") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0iwkag0a97r06xfpzmdfgyd90xzhfix44nlsbg30vx11b15n409j")))

(define-public crate-mathfacts-0.1.2 (c (n "mathfacts") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1b0h82awqfvwpdcw3flyxvkh24y4l4xji7vqxsd4dcx3mx03vpkp")))

