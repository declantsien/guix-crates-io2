(define-module (crates-io ma th math-text-transform) #:use-module (crates-io))

(define-public crate-math-text-transform-0.1.0 (c (n "math-text-transform") (v "0.1.0") (h "1jl632jz2lkl78hml0fcpfh35c3a08yhjv78004r830sxqvqi58f")))

(define-public crate-math-text-transform-0.1.1 (c (n "math-text-transform") (v "0.1.1") (h "10vdv1av3acc066pqdfqf1yxn0gqc29v63yh4wkig1gykyc4xfgh")))

