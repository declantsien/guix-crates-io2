(define-module (crates-io ma th math_l) #:use-module (crates-io))

(define-public crate-math_l-0.0.1 (c (n "math_l") (v "0.0.1") (h "1gx64w62na77xv4443hnpyy8f1qgnklqimal0b2r2qij224rrn1q")))

(define-public crate-math_l-0.0.2 (c (n "math_l") (v "0.0.2") (h "1902y0kl74q0xn6djr13bffa6xayrcnc7bigbcpmzz4wk8j73y29")))

(define-public crate-math_l-0.0.3 (c (n "math_l") (v "0.0.3") (h "0cji62gjhnqzxf0b71aa0q5qc713g29h69x3n8avxr9yj06kgc5f")))

