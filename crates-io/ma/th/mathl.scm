(define-module (crates-io ma th mathl) #:use-module (crates-io))

(define-public crate-mathl-0.0.1 (c (n "mathl") (v "0.0.1") (h "0fmfgk889ysrvkdb9xagssw00vz98r0s4z90nhgm1rzrakj387d4")))

(define-public crate-mathl-0.0.2 (c (n "mathl") (v "0.0.2") (h "0di25453hx3x04dlk0gsrzi3wmj60g5b24xv3z86b49kn1xyngk8")))

