(define-module (crates-io ma th math-calc-cli) #:use-module (crates-io))

(define-public crate-math-calc-cli-0.1.0 (c (n "math-calc-cli") (v "0.1.0") (d (list (d (n "math-calc") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1dvcj5vbx1f4q813kgygk03pqp6hpk8zdi7gig63jhvqrj420m4k")))

