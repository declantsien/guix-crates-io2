(define-module (crates-io ma th math_thingies) #:use-module (crates-io))

(define-public crate-math_thingies-0.1.0 (c (n "math_thingies") (v "0.1.0") (d (list (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "02lgywa9jfrazkb339yjaw2icy20fhm8v7806c51vlzjra64rw3c")))

