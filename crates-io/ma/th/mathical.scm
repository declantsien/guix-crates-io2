(define-module (crates-io ma th mathical) #:use-module (crates-io))

(define-public crate-mathical-0.1.0 (c (n "mathical") (v "0.1.0") (h "0mg0yz096l4idhkqsk20cmqij2r2nxl6874yyx21l4imansxd494")))

(define-public crate-mathical-0.1.1 (c (n "mathical") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fltk") (r "^0.15.6") (d #t) (k 0)))) (h "1ssgcxv0q8jz6x8l7l2b0mz9yby27qhph0vglv3q1aqkfkfg75pn")))

(define-public crate-mathical-0.1.2 (c (n "mathical") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fltk") (r "^0.15.6") (d #t) (k 0)))) (h "0xnkwfndykh96mklj1m6qpk4m4ks3lrpp33a8sq52vsx962ir6lj")))

(define-public crate-mathical-0.1.3 (c (n "mathical") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fltk") (r "^0.15.6") (d #t) (k 0)))) (h "0nhdys8frkmi7nc4qzfxpbqmfachmh7fz75jdynmmxf5g4677h5b")))

