(define-module (crates-io ma th math-symbols) #:use-module (crates-io))

(define-public crate-math-symbols-0.1.1 (c (n "math-symbols") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bfyvidz03fp5v58dxspgiaik0syf27l2jz83wx72zzxy00j1s5v")))

