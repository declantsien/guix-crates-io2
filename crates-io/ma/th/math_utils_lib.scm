(define-module (crates-io ma th math_utils_lib) #:use-module (crates-io))

(define-public crate-math_utils_lib-0.1.0 (c (n "math_utils_lib") (v "0.1.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1rcc690sfksrq3n8yvfgm6z9hlnycikxq383bfrdahk9v2gvpci6") (f (quote (("doc-images"))))))

(define-public crate-math_utils_lib-0.1.1 (c (n "math_utils_lib") (v "0.1.1") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0vcxrx40nqb04p29v4fy7bmqkh0vw2x0i5zczx4b5g0wz9ii48va") (f (quote (("doc-images"))))))

(define-public crate-math_utils_lib-0.1.2 (c (n "math_utils_lib") (v "0.1.2") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1wnv5ffkmpddymyvc8rm1a8rdr8gqph2lvrrfgyfgy9n1w13nnjy") (f (quote (("doc-images"))))))

(define-public crate-math_utils_lib-0.1.3 (c (n "math_utils_lib") (v "0.1.3") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1gncjd07c8fpk6xz29xjnaw9lx73jn3x2kpmk0xk134g4236fhns") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.1.4 (c (n "math_utils_lib") (v "0.1.4") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0vpdi8xvqbcs6i82xxmfr32dr8cvfp7vjxh81s91k0nl1xwh03l2") (f (quote (("high-prec") ("doc-images") ("default")))) (y #t)))

(define-public crate-math_utils_lib-0.1.5 (c (n "math_utils_lib") (v "0.1.5") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0li8kf64y3xsi6rbj6nx5qlbphijxz01y0f1hz5q1ykwjg1hn50s") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.1.6 (c (n "math_utils_lib") (v "0.1.6") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0g6008gp2ic3czfdb6cfc74n4m4dhr69q8am7zwqpx6pn08230f3") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.1.7 (c (n "math_utils_lib") (v "0.1.7") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "00vxqms8zcgs7dwlfnrfhjwssbrs7bxin12hl82ys8wfp8ki1g21") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.1.8 (c (n "math_utils_lib") (v "0.1.8") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0lks9sca7kg5jnax6vinl7j7mjn6v5iqqsb9ynvnj8dc91jg2z6y") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.1.9 (c (n "math_utils_lib") (v "0.1.9") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1g735qb1mxawb2wz0bd12f9d6ac5hi97yzkm9a896fykwzi8z14z") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.2.0 (c (n "math_utils_lib") (v "0.2.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1yyzhl99id7f4rbv6hk9pcidvwivvyg2rkzcxflgxh9q26i8wc2x") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.3.0 (c (n "math_utils_lib") (v "0.3.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "18l0d5ipxhqpsh560xwlsd4r06q2ch7lsacm9g9j5d44j8wapqd7") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.3.1 (c (n "math_utils_lib") (v "0.3.1") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1bf3cfw2x2yr39hqfqa10lkfjjlzqfjvfwx5haz0gljz36xr1cd0") (f (quote (("high-prec") ("doc-images") ("default"))))))

(define-public crate-math_utils_lib-0.3.2 (c (n "math_utils_lib") (v "0.3.2") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1ibi7z1ydhdra257g72n0v71n83wsmv2kh02h8y2lm6x2dbhzsi8") (f (quote (("high-prec") ("doc-images") ("default"))))))

