(define-module (crates-io ma th mathlab) #:use-module (crates-io))

(define-public crate-mathlab-0.1.0 (c (n "mathlab") (v "0.1.0") (h "14m8fh5w69w9lbrw1ilja0f1gpayk13sblpsif31appjdni0hndq") (y #t)))

(define-public crate-mathlab-0.1.1 (c (n "mathlab") (v "0.1.1") (h "0n3c00xy5cxh5ivmhswwn43s4cjcc9ahjqxz5i6ixgvpvv21gc1h")))

