(define-module (crates-io ma th math_matrix) #:use-module (crates-io))

(define-public crate-math_matrix-1.0.0 (c (n "math_matrix") (v "1.0.0") (h "1r06bvkq3yymx4slbi3rnpvp4dwhrpq263b5ibqrcwfq98z7l2hq")))

(define-public crate-math_matrix-1.1.0 (c (n "math_matrix") (v "1.1.0") (h "0y4i8d8rh59jzqn8m4xcnx01mg6cg6gfjg8ysknblm1jxl1hmm9z")))

