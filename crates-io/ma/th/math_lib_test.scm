(define-module (crates-io ma th math_lib_test) #:use-module (crates-io))

(define-public crate-math_lib_test-0.1.0 (c (n "math_lib_test") (v "0.1.0") (d (list (d (n "assert-str") (r "^0.1") (d #t) (k 0)))) (h "0ifs4pyqdfvl2v0k5w27933hcm6yspc6s5m2bw5dy5asvzq9h94i")))

(define-public crate-math_lib_test-0.1.1 (c (n "math_lib_test") (v "0.1.1") (d (list (d (n "assert-str") (r "^0.1") (d #t) (k 0)))) (h "0cw919gilhlicdm9a8bjzq00anbhknqj92y0ld9fff866jx5h40j") (y #t)))

(define-public crate-math_lib_test-0.1.2 (c (n "math_lib_test") (v "0.1.2") (d (list (d (n "assert-str") (r "^0.1") (d #t) (k 0)))) (h "09yh28lvjrnxwhpl6v7z1094v125ql9fw1sdgbjw4y1p4dsx0dr5")))

