(define-module (crates-io ma th mathy-notes) #:use-module (crates-io))

(define-public crate-mathy-notes-0.1.0 (c (n "mathy-notes") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1kwqaycfg2ar1in7h3yg694v8jkq9dzv69gi0vs7hg16angh8m2j")))

(define-public crate-mathy-notes-0.1.1 (c (n "mathy-notes") (v "0.1.1") (d (list (d (n "eframe") (r "^0.19") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1c414m1n5i2y6f1jfak0p9ss4kl3r8337dl5rln0l579hwzmy0x0")))

(define-public crate-mathy-notes-0.1.2 (c (n "mathy-notes") (v "0.1.2") (d (list (d (n "eframe") (r "^0.19") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1q11gwaqr8gcdi1nyx4xvywlvdjcy53rvn90rhgr6z5rsscsk1if")))

(define-public crate-mathy-notes-0.1.3 (c (n "mathy-notes") (v "0.1.3") (d (list (d (n "eframe") (r "^0.19") (f (quote ("persistence"))) (d #t) (k 0)))) (h "0xhmhq3k8qikvzxqxdciz7yasbqwvp84ng0dgcbrmh4jzccmdnc7")))

(define-public crate-mathy-notes-0.1.4 (c (n "mathy-notes") (v "0.1.4") (d (list (d (n "eframe") (r "^0.21") (f (quote ("persistence"))) (d #t) (k 0)))) (h "03krc2l3ahvrdq1dv7n6g0k95fr22ngny1r5a4xw8kjyi7x25n2b")))

(define-public crate-mathy-notes-0.1.5 (c (n "mathy-notes") (v "0.1.5") (d (list (d (n "eframe") (r "^0.21") (f (quote ("persistence"))) (d #t) (k 0)))) (h "020dgl4r1bvrhnl256v9q6236yfhf3w886d06gvrrwqzamnl4nfh")))

(define-public crate-mathy-notes-0.1.6 (c (n "mathy-notes") (v "0.1.6") (d (list (d (n "eframe") (r "^0.21") (f (quote ("persistence"))) (d #t) (k 0)))) (h "0kjc7b7n45mwyiblwwz7smamxsix02zqr82bb4fs3s5l1ddd7q2l")))

(define-public crate-mathy-notes-0.1.7 (c (n "mathy-notes") (v "0.1.7") (d (list (d (n "eframe") (r "^0.21") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1m82mzw0gbafnjargdaypnmiw8f9nac8nnfilkdsl6h0vszgrk5a")))

(define-public crate-mathy-notes-0.1.8 (c (n "mathy-notes") (v "0.1.8") (d (list (d (n "eframe") (r "^0.22") (f (quote ("persistence"))) (d #t) (k 0)))) (h "0aajmfgn5za1ka8zrlvy2qrrj66wzsrik42cps3y4pwg627dm6k5")))

(define-public crate-mathy-notes-0.2.0 (c (n "mathy-notes") (v "0.2.0") (d (list (d (n "eframe") (r "^0.23") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1jqwbj7af23vy579p7vyqs1z5fsrl9bjrrlw8d9g4bs4l85ia5sl")))

(define-public crate-mathy-notes-0.2.1 (c (n "mathy-notes") (v "0.2.1") (d (list (d (n "eframe") (r "^0.27") (f (quote ("persistence"))) (d #t) (k 0)))) (h "1pibi1lbixw6jjbmnbw8vyb3vl928m49wirpgpy2k332cwa9kvqy")))

