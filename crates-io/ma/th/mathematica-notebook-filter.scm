(define-module (crates-io ma th mathematica-notebook-filter) #:use-module (crates-io))

(define-public crate-mathematica-notebook-filter-0.1.0 (c (n "mathematica-notebook-filter") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "1422590sxjbfwffhp4lbvxfqa9brcrxxgs4kyjagxrcacgfiv13y")))

(define-public crate-mathematica-notebook-filter-0.1.1 (c (n "mathematica-notebook-filter") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "15xjrbx692frx621mi8jzyj6wimzbvd42gj8r1qc4h7pra3pb79s")))

(define-public crate-mathematica-notebook-filter-0.1.2 (c (n "mathematica-notebook-filter") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "0ww5wwni57dd0w1bxc72cm0z66qj9qmjzv4qb9cpsrz0rxvbm1kb")))

(define-public crate-mathematica-notebook-filter-0.1.3 (c (n "mathematica-notebook-filter") (v "0.1.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "= 2.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "1hsz6j6ab0mca077rb321hlyaqbwd4jd0vhl1c6pzk8s5s1xs9d6")))

(define-public crate-mathematica-notebook-filter-0.1.4 (c (n "mathematica-notebook-filter") (v "0.1.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "08xgccawgcpf15h1yhfb9psfmlipdjcz67c3rdqv3jv3x6qrckki")))

(define-public crate-mathematica-notebook-filter-0.1.5 (c (n "mathematica-notebook-filter") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 0)))) (h "0f3nidh6wlm3h9bbncxl7s887flf0vhwgbhp8sl0dkkdnyx391ip")))

(define-public crate-mathematica-notebook-filter-0.1.6 (c (n "mathematica-notebook-filter") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 0)))) (h "0raq81lf51pgswfxw4w4i9nbgrxx6gvrqs2kfinig2k5ymkr52vv")))

(define-public crate-mathematica-notebook-filter-0.2.0 (c (n "mathematica-notebook-filter") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.10") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "1sww0n4ys7nd1q8a9sslgdpnln7h2d6730y71cakh0cgbg8kda4k")))

(define-public crate-mathematica-notebook-filter-0.2.2 (c (n "mathematica-notebook-filter") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)))) (h "0pm1wnzfc5d8ykf9131la2qd1lhrssg27p8sfwc27c8h45kdy595")))

