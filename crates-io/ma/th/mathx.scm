(define-module (crates-io ma th mathx) #:use-module (crates-io))

(define-public crate-mathx-0.0.1 (c (n "mathx") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13q5bmkxz8sc5p277w4jlmhfyr9kwyv6fpy8nqrv04dfs0c30jqa") (f (quote (("no_std") ("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathx-0.1.0 (c (n "mathx") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00q2gmzxscjaqzj6raqpki74nkp6kl6iq7np49hivm7302vq3k8s") (f (quote (("no_vectors") ("no_std") ("no_colors") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathx-0.3.0 (c (n "mathx") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vmrjss3r8hmg04hbxdaabnqj9ss9kgvfa11dvdn1iqkshz6y4ah") (f (quote (("no_vectors") ("no_std") ("no_rays") ("no_quaternions") ("no_colors") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathx-0.4.0 (c (n "mathx") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m9cwpf36fsmsrcidvcwhjf521b1ikrnh5wfnxaq03rmxyapa4c3") (f (quote (("no_vectors") ("no_std") ("no_rays") ("no_quaternions") ("no_planes") ("no_colors") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-mathx-0.4.1 (c (n "mathx") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sadinrl87nqfy33cv2xzggs312kszc3xx79iw6q928n4k2xhh2x") (f (quote (("no_vectors") ("no_std") ("no_rays") ("no_quaternions") ("no_planes") ("no_colors") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

