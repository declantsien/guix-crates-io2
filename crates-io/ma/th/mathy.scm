(define-module (crates-io ma th mathy) #:use-module (crates-io))

(define-public crate-mathy-0.0.0 (c (n "mathy") (v "0.0.0") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pa9sr6fx5ypfpr1njy7cr49drhbf4cfqpxidv4p1plx6lvkavhh")))

