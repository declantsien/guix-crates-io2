(define-module (crates-io ma lk malk-lexer) #:use-module (crates-io))

(define-public crate-malk-lexer-0.1.0 (c (n "malk-lexer") (v "0.1.0") (d (list (d (n "unicode-brackets") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.0.3") (d #t) (k 0)))) (h "16k6xb600ls2ihvlaynvxrw58205z71bxl2z02mmsybjb3qjdg5j")))

(define-public crate-malk-lexer-0.1.1 (c (n "malk-lexer") (v "0.1.1") (d (list (d (n "unicode-brackets") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.0.3") (d #t) (k 0)))) (h "0l4c67zzxwd9wr8af5dnpy2qrdasq6rdxjvbv68s1wbblk2mpxr5")))

