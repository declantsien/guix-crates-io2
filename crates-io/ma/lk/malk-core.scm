(define-module (crates-io ma lk malk-core) #:use-module (crates-io))

(define-public crate-malk-core-0.1.0 (c (n "malk-core") (v "0.1.0") (h "1y712g26s7yx32653l23kbgr4mn2yg15hjfdjp2x18x8frx2d1c6")))

(define-public crate-malk-core-0.1.1 (c (n "malk-core") (v "0.1.1") (h "1cn32bxir0pdc51rvc3jlw1ir643gqhj28kya1f1sgw04zglbai8")))

