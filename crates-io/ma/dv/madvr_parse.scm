(define-module (crates-io ma dv madvr_parse) #:use-module (crates-io))

(define-public crate-madvr_parse-0.1.0 (c (n "madvr_parse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1js78y6ldv52l097b0gdai5qh8ak7bm52kk65g2wp0354sx27ay8")))

(define-public crate-madvr_parse-0.1.2 (c (n "madvr_parse") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0qrlz78a0dn25wawck22p5dj77p5nlnikadarfq01cqxaw3vrcn2")))

(define-public crate-madvr_parse-0.1.3 (c (n "madvr_parse") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0g59f50745ijn0jfh43p74jxgps04w14s6w28vd20377x83442cd") (r "1.51.0")))

(define-public crate-madvr_parse-1.0.0 (c (n "madvr_parse") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0qnap718dphi3lwqw6vvm8dg1wc1lwnj05ixccxghzpcvdsp04dv") (r "1.56.0")))

(define-public crate-madvr_parse-1.0.1 (c (n "madvr_parse") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "06kk93y5f8s6i5sly4m9rmcwh8zqx7n0zmf59idvz478v5ad3y3b") (r "1.56.0")))

(define-public crate-madvr_parse-1.0.2 (c (n "madvr_parse") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "1rifqjvmbj96mvbws21kv9xafy8j39h78v12wgi4h9sw1azigmmz") (r "1.70.0")))

