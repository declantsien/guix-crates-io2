(define-module (crates-io ma p_ map_retry) #:use-module (crates-io))

(define-public crate-map_retry-0.1.0 (c (n "map_retry") (v "0.1.0") (h "03jlzl44pmaz854w5gksqig2dnyd0qsf1yba91lpnnafyzihbabh")))

(define-public crate-map_retry-0.1.1 (c (n "map_retry") (v "0.1.1") (h "0bqv0gin2hadmzv51bw8hc66a72jghqkhcda8fw9ps0aqqp24qkh")))

(define-public crate-map_retry-0.1.2 (c (n "map_retry") (v "0.1.2") (h "16bynby8mkq6kk2zs1sipdpwlnbqxayigf9m4wby187ywa6dar7f")))

