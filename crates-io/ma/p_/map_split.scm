(define-module (crates-io ma p_ map_split) #:use-module (crates-io))

(define-public crate-map_split-0.1.0 (c (n "map_split") (v "0.1.0") (h "07b61db12dzlyfjnhqhxgk5z06zliwc4m5av6fwcihcfpmnpqpmb")))

(define-public crate-map_split-0.2.0 (c (n "map_split") (v "0.2.0") (h "08jkr7zhd55d381ri6h1pdq34wd47j3z0v3301yd5mgi4jyhq7v4")))

(define-public crate-map_split-0.2.1 (c (n "map_split") (v "0.2.1") (h "12wk4cjg88lkbar1dmwlavwrdhb0sfa70z8xj7dsvhklgcm1s972")))

