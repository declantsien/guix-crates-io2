(define-module (crates-io ma p_ map_for) #:use-module (crates-io))

(define-public crate-map_for-0.1.0 (c (n "map_for") (v "0.1.0") (h "1vp8p0r4k7ngvgn76q398bpcippmla0fjmr1xlk0n6izfdlrix2j")))

(define-public crate-map_for-0.1.1 (c (n "map_for") (v "0.1.1") (h "1af5m0cs5rn9hqfjafqv53wrp8c06fwdmbdycx8swbx478ksblks")))

(define-public crate-map_for-0.2.0 (c (n "map_for") (v "0.2.0") (h "1v02si23b40a52f3jwv0x76zzc3y1dh5gqp3lggh2b66ms3bcmfn")))

(define-public crate-map_for-0.3.0 (c (n "map_for") (v "0.3.0") (h "1w5svdm49s317p89yvkxagw9jds427v3aqnkya9vl5lr1hfm2z8a")))

