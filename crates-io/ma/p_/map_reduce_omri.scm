(define-module (crates-io ma p_ map_reduce_omri) #:use-module (crates-io))

(define-public crate-map_reduce_omri-0.1.0 (c (n "map_reduce_omri") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "19q2yyq4sdl69fml9nix8dsmk0k1cinrfdlcg2gy77n8w8qjj0g5")))

(define-public crate-map_reduce_omri-0.1.1 (c (n "map_reduce_omri") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "1grfxx50lajl1flrk3x3yg6h51lhqmnrl43d7i78h5n8hy4hjzww")))

(define-public crate-map_reduce_omri-0.1.2 (c (n "map_reduce_omri") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "1pw9vdqqnzchzz3ak6asn0a7fz4wn2i4cpywwkpa0d4xpk80xlf6")))

