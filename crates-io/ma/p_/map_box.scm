(define-module (crates-io ma p_ map_box) #:use-module (crates-io))

(define-public crate-map_box-0.1.0 (c (n "map_box") (v "0.1.0") (h "11l6yrcpq7q2z85k3s95w71q3gy8ibx8jic550igfvf43b8icyi1") (y #t)))

(define-public crate-map_box-0.1.1 (c (n "map_box") (v "0.1.1") (h "0f3p74snwdj71ycb8m0i09q040napv4s8mr922b5rqh509mfsssf") (y #t)))

(define-public crate-map_box-0.2.0 (c (n "map_box") (v "0.2.0") (h "1lbp1q6i0bqk7y5pwww24iw95hj3mz0rq8ipsfsf94cwrz0n49yg") (y #t)))

(define-public crate-map_box-0.2.1 (c (n "map_box") (v "0.2.1") (h "0hr8cvwza2bvvacvh7sb1p1cziljlan972wl24m3c9vqv2dzfr6j") (y #t)))

(define-public crate-map_box-0.2.2 (c (n "map_box") (v "0.2.2") (h "1blmybxhchsj2zs51n7q47n5vb56qnasaklfzjh7ifrrzwzlvjbl")))

