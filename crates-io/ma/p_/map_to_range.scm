(define-module (crates-io ma p_ map_to_range) #:use-module (crates-io))

(define-public crate-map_to_range-0.1.0 (c (n "map_to_range") (v "0.1.0") (h "15iclbzvl7db10gysgw4kmyll45ykcdymvcjzsgw0k3b6fxcchzs") (y #t)))

(define-public crate-map_to_range-0.1.1 (c (n "map_to_range") (v "0.1.1") (h "16a29g94r57zdzym234ia6zfxgb5dqchymdgxb61d7631fi0xllc")))

(define-public crate-map_to_range-0.2.0 (c (n "map_to_range") (v "0.2.0") (h "10gfskmxvhgxz1zgn4midf33sp0d0rj1vq4m4m486q200v5slr17")))

