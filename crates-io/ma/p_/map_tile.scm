(define-module (crates-io ma p_ map_tile) #:use-module (crates-io))

(define-public crate-map_tile-0.1.0 (c (n "map_tile") (v "0.1.0") (d (list (d (n "geo") (r "^0.25.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)) (d (n "wkt") (r "^0.10.3") (d #t) (k 0)))) (h "1k73f0zb9m9fllmkcvavkxihjdjxwx4xi6qnlgypycmar5mss93a")))

(define-public crate-map_tile-0.1.1 (c (n "map_tile") (v "0.1.1") (d (list (d (n "geo") (r "^0.25.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)) (d (n "wkt") (r "^0.10.3") (d #t) (k 0)))) (h "0l1bxbfyp15plrrcyi4fbyd124spiar0ir6249lqgdls1ba86cb8")))

