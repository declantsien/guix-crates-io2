(define-module (crates-io ma p_ map_generator) #:use-module (crates-io))

(define-public crate-map_generator-0.1.0 (c (n "map_generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1yalr0jzgw3dw6p92ay4f40xfxhz5zrfw9qsvhdg9rdpn9wlygln")))

(define-public crate-map_generator-0.1.1 (c (n "map_generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0n3qcqfyar204ya0xd66337l19whniyrf0fy27qp69kd1y2xx8m4")))

(define-public crate-map_generator-0.1.2 (c (n "map_generator") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1lcx9k36i9bd5dmkyfwj8ghv1p0ma3sxlz2vg0wcmzx3hc9pq2g3")))

