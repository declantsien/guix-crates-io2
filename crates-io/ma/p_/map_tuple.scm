(define-module (crates-io ma p_ map_tuple) #:use-module (crates-io))

(define-public crate-map_tuple-0.1.0 (c (n "map_tuple") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0s2sh6qf4vjq3fx81im46hs7cqp13gvhz0nxrpxm7igk6hh0w8ib") (f (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1.1 (c (n "map_tuple") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0hb8mp4abd1nzi5cn1239v1prag8z0djzm9y3h0vn67759c94ijv") (f (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1.2 (c (n "map_tuple") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1mn7i4s8xvv6rpm1q5chpn6pk8pd0kkr1cr73ds04m3yhvin19rk") (f (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1.3 (c (n "map_tuple") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1x6b9d8lr9y9wlggv475k5b48rc469nj6bfdiy6x0skwwi0mxbmh") (f (quote (("tuple64" "tuple32") ("tuple32" "tuple16") ("tuple16") ("tuple128" "tuple64"))))))

