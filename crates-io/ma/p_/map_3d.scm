(define-module (crates-io ma p_ map_3d) #:use-module (crates-io))

(define-public crate-map_3d-0.1.0 (c (n "map_3d") (v "0.1.0") (h "1dfb1m68r55vjm6s7qay8kfl8l7ah4rncrfaf7a0mx4aqrd3357h")))

(define-public crate-map_3d-0.1.1 (c (n "map_3d") (v "0.1.1") (h "1932kz6zqmjr09jzk5z84i8anma002y5ls0xh1b72p6s8r2wsp3r")))

(define-public crate-map_3d-0.1.2 (c (n "map_3d") (v "0.1.2") (h "1lg6nxpq6iqiwj7cxfbi4rhfy19m7gfyb4slkffaj92brr59rrl0")))

(define-public crate-map_3d-0.1.3 (c (n "map_3d") (v "0.1.3") (h "0f1mng1hcsfhp9v88wbmgnksi6wl18xh4mz099ywnn3kps61ipwk")))

(define-public crate-map_3d-0.1.4 (c (n "map_3d") (v "0.1.4") (h "06751xxs2b7hyzdlj11h9fmps3aprpmwya82a0l6s1x34a8q777d")))

(define-public crate-map_3d-0.1.5 (c (n "map_3d") (v "0.1.5") (h "1i22xl7xwv3hypqnhnsgp0z8fnspwvrzr0g4wnb6rl7v0xhipz5h")))

