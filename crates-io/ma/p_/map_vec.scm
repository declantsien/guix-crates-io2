(define-module (crates-io ma p_ map_vec) #:use-module (crates-io))

(define-public crate-map_vec-0.1.0 (c (n "map_vec") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1cd3cyrmcdg0wb8fhksd7zahwjhzciy6i9nfb6fpnfp6x2awsw6k")))

(define-public crate-map_vec-0.1.1 (c (n "map_vec") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0f6swbk8xw7kgdr3irarq2ppwflv9dqyjyk5h0ymbjzbsgzm128n")))

(define-public crate-map_vec-0.2.0 (c (n "map_vec") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hm7xdvv1sk47drmd8ddw4y0xnfhv6qh4vclxkswymd9bg89w6kf")))

(define-public crate-map_vec-0.2.1 (c (n "map_vec") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ifn3yg4f6k0xbwndacfwv20vc4k4a0j3b44wg9anyq8afw8rxxz")))

(define-public crate-map_vec-0.2.2 (c (n "map_vec") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "055piqwznrxck6ig4ds08iij29ihkb2s368mb79vzkip2xlil5g2")))

(define-public crate-map_vec-0.2.3 (c (n "map_vec") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05d9hs0iip15sph7wjdqqgw2bfd0bwv1c8f2j7z5dwnz2xsm5180")))

(define-public crate-map_vec-0.2.4 (c (n "map_vec") (v "0.2.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xxixmlja537zl9k9p8kanh5q40macvsk3z20flny460fyipmiqd") (f (quote (("default" "serde"))))))

(define-public crate-map_vec-0.2.5 (c (n "map_vec") (v "0.2.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qfpp78ngpgx3whhgp8qpmfihxb95p1p95sv1l8n8qmqhkq804b5") (f (quote (("default" "serde"))))))

(define-public crate-map_vec-0.2.6 (c (n "map_vec") (v "0.2.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "128hwbv463nc1rjnr4a9zsilzn83yzfncxa22i16hlxjmzjyggbh") (f (quote (("default" "serde"))))))

(define-public crate-map_vec-0.3.0 (c (n "map_vec") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b9cn2phq4ad6x0mh4gzp0wygyhzc19wp4vacgx2jldx0sdq7sa3") (f (quote (("nightly") ("default" "serde"))))))

(define-public crate-map_vec-0.4.0 (c (n "map_vec") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s56vmlgz80ns5v22h1qhkjcyqm6qyjpznddbpydhn3hxm2jw4qi") (f (quote (("nightly") ("default" "serde"))))))

(define-public crate-map_vec-0.5.0 (c (n "map_vec") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pzqxkmsc0c7v2dpx8zkw92h96kvvaz22s0mzhrr2x2lw94wl7hi") (f (quote (("nightly") ("default" "serde"))))))

