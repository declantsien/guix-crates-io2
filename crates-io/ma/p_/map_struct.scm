(define-module (crates-io ma p_ map_struct) #:use-module (crates-io))

(define-public crate-map_struct-0.1.0 (c (n "map_struct") (v "0.1.0") (h "1rzrimfds62yni4xlfcrwyfv1936z2nn278fr91gbims8mkm06nk")))

(define-public crate-map_struct-0.2.0 (c (n "map_struct") (v "0.2.0") (h "0834057cn5wi1i0kyp3wx4sbqs4wzz5zj1qgbqfz2lncczg984nv")))

(define-public crate-map_struct-0.3.0 (c (n "map_struct") (v "0.3.0") (h "0fwn6kf96xg12vky8qqqkw9k99l36iw3ffqzgn2y11gcmipx46d9")))

