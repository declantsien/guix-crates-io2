(define-module (crates-io ma g- mag-image-decoder) #:use-module (crates-io))

(define-public crate-mag-image-decoder-0.1.0 (c (n "mag-image-decoder") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1dvhc0fjybiwdfrd2niqqvvmnkwmncycivhmch0z0n63a6ckqfhl")))

