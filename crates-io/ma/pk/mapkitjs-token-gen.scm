(define-module (crates-io ma pk mapkitjs-token-gen) #:use-module (crates-io))

(define-public crate-mapkitjs-token-gen-0.1.0 (c (n "mapkitjs-token-gen") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1xxnd618bdx3qwnj03qj5lkdp7i0zylcaz3dli6dpcvahcab4lfa")))

(define-public crate-mapkitjs-token-gen-0.2.0 (c (n "mapkitjs-token-gen") (v "0.2.0") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0n8b2zgyznnnyh2b4wv1j9ccwy4jnjlmml1y1ywyrr9qrii0hwha")))

