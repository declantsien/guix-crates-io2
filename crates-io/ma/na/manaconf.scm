(define-module (crates-io ma na manaconf) #:use-module (crates-io))

(define-public crate-manaconf-0.1.0 (c (n "manaconf") (v "0.1.0") (h "0vw85is5izc52rlmcz6hikb6wmkn56v0i9zshrs6gw50zy7p6wkk")))

(define-public crate-manaconf-0.1.1 (c (n "manaconf") (v "0.1.1") (h "0bfg3mm85bm3l59w899iigkfq97a2j0z0g5jvpmd4dj8cq9w75qh")))

(define-public crate-manaconf-0.1.2 (c (n "manaconf") (v "0.1.2") (h "0zh23sixl4bbbphfajlc7zh7c9kxvhx6fpld6m01dyk7kxxfy112")))

(define-public crate-manaconf-0.2.0 (c (n "manaconf") (v "0.2.0") (h "0x1798mb17kjk9m62hqh6wjv5x2bxgi7n2dgw2f0zrpis6yzmizc")))

(define-public crate-manaconf-0.2.1 (c (n "manaconf") (v "0.2.1") (h "1j3kkm7r8i97hp9blcy3v5527ji2mn0lpg8j044j0d21kxsvld7c")))

