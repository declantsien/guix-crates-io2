(define-module (crates-io ma na manas_server_single_fs_wac) #:use-module (crates-io))

(define-public crate-manas_server_single_fs_wac-0.1.0 (c (n "manas_server_single_fs_wac") (v "0.1.0") (d (list (d (n "manas_server") (r "^0.1.0") (f (quote ("backend-fs" "pdp-wac"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0d0z83xc79mz2rr75zf4qiasnikdk5b8q11l3wd0zk8w1z0ip9rc")))

