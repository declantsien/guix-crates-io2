(define-module (crates-io ma na managed-heap) #:use-module (crates-io))

(define-public crate-managed-heap-0.1.0 (c (n "managed-heap") (v "0.1.0") (h "08hipz6052c8wfmw3m4x7mik7bigyfrgh86wlih9379lqadfpkky")))

(define-public crate-managed-heap-0.1.1 (c (n "managed-heap") (v "0.1.1") (h "0zlaqdhbm8cz9yia3yzhgn0r6bfz01r2p6nlci9vlncbzikry0dq")))

(define-public crate-managed-heap-0.1.2 (c (n "managed-heap") (v "0.1.2") (h "0izyjixlq9jbggjp12pgxwfn7kcpxfbppwfpr2smj0bc781yr16a")))

(define-public crate-managed-heap-0.1.3 (c (n "managed-heap") (v "0.1.3") (h "12rpzvylc24rwaazxyvlif1m5zyzz45xpmiwf45im6rdmy54agix")))

(define-public crate-managed-heap-0.1.4 (c (n "managed-heap") (v "0.1.4") (h "1mp9iadlhrqvwwwlr78l3da9vz6krqj35zr1havzach154g2wx63")))

(define-public crate-managed-heap-0.1.5 (c (n "managed-heap") (v "0.1.5") (h "0nq9hfcjmm3vw79d5dyxcbkvmiycardyyf7jhdl1zk82rxpqiz1h")))

