(define-module (crates-io ma na manas_specs) #:use-module (crates-io))

(define-public crate-manas_specs-0.1.0 (c (n "manas_specs") (v "0.1.0") (d (list (d (n "dyn_problem") (r "^0.1.1") (f (quote ("ext-typed-record"))) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "http-api-problem") (r "^0.57.0") (f (quote ("api-error"))) (d #t) (k 0)) (d (n "iri-string") (r "^0.7.0") (d #t) (k 0)) (d (n "manas_http") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.169") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "typed_record") (r "^0.1.1") (d #t) (k 0)))) (h "1766j7vmwddgbw8wvravk6rk3hnbqmdcx8300jb397cvlwxm40gi")))

