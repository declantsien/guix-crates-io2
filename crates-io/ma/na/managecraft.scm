(define-module (crates-io ma na managecraft) #:use-module (crates-io))

(define-public crate-managecraft-0.1.0 (c (n "managecraft") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "rcon") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17x56mr7dc7hvnakcm6gi2q362ycd790vhw72hnsyqld049b1xlg")))

