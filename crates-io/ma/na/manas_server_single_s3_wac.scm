(define-module (crates-io ma na manas_server_single_s3_wac) #:use-module (crates-io))

(define-public crate-manas_server_single_s3_wac-0.1.0 (c (n "manas_server_single_s3_wac") (v "0.1.0") (d (list (d (n "manas_server") (r "^0.1.0") (f (quote ("backend-s3" "pdp-wac"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1y7q8gqra273r6lp13aqgdl7vdn7dl5xqi0pcdmy7fqcchsl8mcl")))

