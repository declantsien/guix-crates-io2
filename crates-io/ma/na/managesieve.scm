(define-module (crates-io ma na managesieve) #:use-module (crates-io))

(define-public crate-managesieve-0.1.0 (c (n "managesieve") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)))) (h "02mqxdyf22ar5sdb9nxgmbbw7dqznhbh64816jvhhigcsi5v7y8q")))

(define-public crate-managesieve-0.1.1 (c (n "managesieve") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)))) (h "0cm8qqfw9644cni7jn5jagh7xlkv76n3wqsl9xiwyr0zgcbn5bfq")))

