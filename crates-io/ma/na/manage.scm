(define-module (crates-io ma na manage) #:use-module (crates-io))

(define-public crate-manage-0.1.0 (c (n "manage") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0isf6rh97n5v5pfh40aa1g3n7zlp3yrdw4z3mzvbghn46cw2q7wz")))

(define-public crate-manage-0.1.1 (c (n "manage") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0b8wz1b7xlf2b260mdckcl19mnhjjm6ps9mmzqhy2fp4lihqrn1c")))

(define-public crate-manage-0.1.2 (c (n "manage") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "11ywqfl37psnycrb1ghn71xnpyy67bf5lh0v5hb014xx071kzgcj")))

(define-public crate-manage-0.2.0 (c (n "manage") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1r9w6hch0y7ginm8z02cwzdpb8a98h1rs7w6h7rs7fqvs45ypvyp")))

(define-public crate-manage-0.2.1 (c (n "manage") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1jksiyj0xypiyfwysasrvd64mw73ks2z6xljygs20pgrf4w4lcrz")))

(define-public crate-manage-0.2.2 (c (n "manage") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0l83lhr1h9kbzhlw14f438rfq8hf3b3yz7x06f08yp68f8y47cb1")))

(define-public crate-manage-0.2.3 (c (n "manage") (v "0.2.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1ldzpv3b074adm605fk2jvnlnc2mk89n0qszv601ykvh090vaz6l")))

(define-public crate-manage-0.3.0 (c (n "manage") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.13") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0vappb2gabnhsaq4p14m8y773prydyb6myd06hvvgw8b7dwwp6ra")))

(define-public crate-manage-0.4.0 (c (n "manage") (v "0.4.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0a4s504zk4rysm011n386kshx79qnha3ydizxi465p09w09k2n1c")))

(define-public crate-manage-0.4.1 (c (n "manage") (v "0.4.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "17fh5q98dq7fbyxzz80wzyhhd5msr7nbpy5lph0k0nhp8c6c9qd5")))

(define-public crate-manage-0.4.2 (c (n "manage") (v "0.4.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "08282rqhhj115v17p77iazy3br23z8i5marg2ln60whvbzs31dk8")))

(define-public crate-manage-0.4.3 (c (n "manage") (v "0.4.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0wvx2i8id7lp02yh4644288mapkp6qad4khwydmn72ifdq81hbn3")))

(define-public crate-manage-0.4.4 (c (n "manage") (v "0.4.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1cr1s4r79fw5n6iacl4smr4w32p4f0ha98ax6kf1j0wqsvsrc94p")))

(define-public crate-manage-0.4.5 (c (n "manage") (v "0.4.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0qvpqni09hsi8ynp6hxqxypj4m9q7g451zq8i6cx1329idgk1as8")))

(define-public crate-manage-0.4.6 (c (n "manage") (v "0.4.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "refs") (r "^0.6.14") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1y4bd9cnj0m48ccb7iw8b8yzrc1ai4inzirk6z2bskg16c2lkli9")))

(define-public crate-manage-0.4.7 (c (n "manage") (v "0.4.7") (d (list (d (n "refs") (r "^0.6.16") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0g48lwl97cc3qizv10zf7byswgzn01j6d6pkjxm2b8x01ivl8vy9")))

(define-public crate-manage-0.4.8 (c (n "manage") (v "0.4.8") (d (list (d (n "refs") (r "^0.7.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1rmsy8i4jwjsyv4fip79q3wagk0smcbcjp70fgnm1dgqhr00k5an")))

(define-public crate-manage-0.4.9 (c (n "manage") (v "0.4.9") (d (list (d (n "refs") (r "^0.8.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "150mndiayhjdsf0y78c4sanfjgcmcvjpqvhzg8mwxvq2jcy3nh90")))

(define-public crate-manage-0.4.10 (c (n "manage") (v "0.4.10") (d (list (d (n "refs") (r "^0.8.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1x6hbav2l0y1nfvspckz0i736f7c1jgpigl4z7g1za9dlfaipzr1")))

(define-public crate-manage-0.4.11 (c (n "manage") (v "0.4.11") (d (list (d (n "refs") (r "^0.8.3") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0rqhqb5k77jj4ihv9ykc7z3cw4ppk9dqz7ncjllblaih2rmqiqzc")))

