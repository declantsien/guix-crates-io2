(define-module (crates-io ma na managed-thread) #:use-module (crates-io))

(define-public crate-managed-thread-0.0.1 (c (n "managed-thread") (v "0.0.1") (h "0sdvkrxcslmbbx9nl63gc077bjjhgvaiqm8h9gybdbbkzzavbrhd")))

(define-public crate-managed-thread-0.0.2 (c (n "managed-thread") (v "0.0.2") (h "0fwsd3llgdaqki873azkx2sacg9asmdfkz0xkiffc1sjzwbajpp1")))

(define-public crate-managed-thread-0.0.3 (c (n "managed-thread") (v "0.0.3") (h "1hp0ndsd2bdpqv5lq9ygifrv1yg9vagbsrsv5bwpba0fww0hvmh2")))

