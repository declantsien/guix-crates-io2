(define-module (crates-io ma pl maplit) #:use-module (crates-io))

(define-public crate-maplit-0.1.0 (c (n "maplit") (v "0.1.0") (h "0za39lncinlfqganl6sipmg8syf24g7fp6z9dqmrjd724n7nfnlv")))

(define-public crate-maplit-0.1.1 (c (n "maplit") (v "0.1.1") (h "13qpdiqn2bm81c8hnskmp8vfqyrxfg2jy30znm2d3w38m5qz91pf")))

(define-public crate-maplit-0.1.2 (c (n "maplit") (v "0.1.2") (h "056iz03nxfzabq9ji6wq60hlya7p7ifrvgij4b6kk6napp7z0b6f")))

(define-public crate-maplit-0.1.3 (c (n "maplit") (v "0.1.3") (h "1gvyizccd1pqcsi4322mjjrp2m9zn93i1ca0b54npi4aqxhfxwl2")))

(define-public crate-maplit-0.1.4 (c (n "maplit") (v "0.1.4") (h "1smz0891r0gawhs5522rdzggy7ha5j6vizwhnmldhfhc1rb4qf5y")))

(define-public crate-maplit-0.1.5 (c (n "maplit") (v "0.1.5") (h "0f808wqi1ssans40zxl4ygwkwn8xcd28zspwlyi8d9w7pck4mx1v")))

(define-public crate-maplit-0.1.6 (c (n "maplit") (v "0.1.6") (h "0rfgsh4y84hxcqxrydl62rnbk7sq6b6qmhk93j34fxyzp0ak0n92")))

(define-public crate-maplit-1.0.0 (c (n "maplit") (v "1.0.0") (h "19ac9s5idvp18r4cw899d9z1yj5d52hbrfmdj5v1m2hbsi4m1nay")))

(define-public crate-maplit-1.0.1 (c (n "maplit") (v "1.0.1") (h "0hsczmvd6zkqgzqdjp5hfyg7f339n68w83n4pxvnsszrzssbdjq8")))

(define-public crate-maplit-1.0.2 (c (n "maplit") (v "1.0.2") (h "07b5kjnhrrmfhgqm9wprjw8adx6i225lqp49gasgqg74lahnabiy")))

