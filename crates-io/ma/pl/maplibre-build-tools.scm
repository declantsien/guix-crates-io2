(define-module (crates-io ma pl maplibre-build-tools) #:use-module (crates-io))

(define-public crate-maplibre-build-tools-0.1.0 (c (n "maplibre-build-tools") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.8") (f (quote ("wgsl-in"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0s7gbgbvvnqkg4rq91nml1zjminn4hjas6pl90sc3nlkj0n1cj09") (y #t)))

(define-public crate-maplibre-build-tools-0.0.3 (c (n "maplibre-build-tools") (v "0.0.3") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "naga") (r "^0.11.0") (f (quote ("wgsl-in"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1gfkf396crdgwym1r1wvy2i5l1rsrli278lc4z5bmbaps76jn21d") (f (quote (("sqlite" "rusqlite")))) (r "1.65")))

