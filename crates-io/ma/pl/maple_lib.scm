(define-module (crates-io ma pl maple_lib) #:use-module (crates-io))

(define-public crate-maple_lib-0.1.0 (c (n "maple_lib") (v "0.1.0") (h "1gfh71pfmvsk4irpx4h9nqnac45x89vcadlgh6hkkf2rv5yrg5rc")))

(define-public crate-maple_lib-0.1.1 (c (n "maple_lib") (v "0.1.1") (h "07ljgjjgs6kvpr7cwsgf8ndviq76xhddbf7iwiwb8y0fwby9kpxn")))

(define-public crate-maple_lib-0.1.2 (c (n "maple_lib") (v "0.1.2") (h "134zp6x449wsnwqhbkazq54gmbhkxx039c04nzh7vz8kr97hb594")))

(define-public crate-maple_lib-0.1.3 (c (n "maple_lib") (v "0.1.3") (h "02kp8z7wbq1kmjvzvm729g7z5jzqpi2g4p26gxc2211pqvw9lk8c")))

(define-public crate-maple_lib-0.1.4 (c (n "maple_lib") (v "0.1.4") (h "0gkxc2kk74vw9f73ialpla7q15zmhkshndm8fjhj33x7sv2nhjm8")))

