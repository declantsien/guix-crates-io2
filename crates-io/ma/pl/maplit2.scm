(define-module (crates-io ma pl maplit2) #:use-module (crates-io))

(define-public crate-maplit2-1.0.2 (c (n "maplit2") (v "1.0.2") (h "0jd3gy8pz6k0yxjg7afzampvcrrfqbcimipwgsw857lnz6rcnc35")))

(define-public crate-maplit2-1.0.3 (c (n "maplit2") (v "1.0.3") (h "1ffa79sdkz784jdp4nw4qhy4chxcixx7gzm84qp7jf0gwibgxwxc")))

(define-public crate-maplit2-1.0.4 (c (n "maplit2") (v "1.0.4") (h "0w2fgp08ijahslr3v7biwc34yfy5wwz4wv1216njda97yyq8lzrm")))

(define-public crate-maplit2-1.0.5 (c (n "maplit2") (v "1.0.5") (h "1nfrljpaxfcb0k7rqnff9ab1y8zgsyqfajlnz8ym3sqwj2agrqsv")))

