(define-module (crates-io ma pl maple-core-macro) #:use-module (crates-io))

(define-public crate-maple-core-macro-0.1.0 (c (n "maple-core-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "132x07k2arizvlvqn651407smhsl6pdlk2h634w0an0bwrf4s4zk")))

(define-public crate-maple-core-macro-0.1.1 (c (n "maple-core-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1px0fa61jxjfqhmjd9jm5yl9z2ny0fpp1sc8ypjqjzr2ykq88cyb")))

(define-public crate-maple-core-macro-0.2.0 (c (n "maple-core-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0435id4r6p4vs2zxfhrrxh7kq7iiqdgpmzz475lk3h79rr8lgjrl")))

(define-public crate-maple-core-macro-0.3.0 (c (n "maple-core-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03p8z9cdw8qdhwp2q1djdhx1pdd7c12l76kdj346lybyvfpvqmv7")))

(define-public crate-maple-core-macro-0.3.1 (c (n "maple-core-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0l46qrmak33xqdmj00741pm82m3k8z1ahq93pp48pxm8sdn10lah")))

(define-public crate-maple-core-macro-0.4.0 (c (n "maple-core-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1pazs13sbd21qzcgmfl79b6llfpg5hl72cq02505zd8k0x7wb7n1")))

(define-public crate-maple-core-macro-0.4.1 (c (n "maple-core-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0gb39af0a2kf65085cdaha7hh63xi2mcqd2rl0mqjdsa0vs87qi8")))

(define-public crate-maple-core-macro-0.4.2 (c (n "maple-core-macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zk242mc2lcyf39g7x36nvd6lp857nm2w32pqbd0nmbkr9fb93rf")))

(define-public crate-maple-core-macro-0.4.3 (c (n "maple-core-macro") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zm994a27sv6mp5yjvpcz21ibphpwzxa2k3hz5rb4f62f7g8jjc8")))

