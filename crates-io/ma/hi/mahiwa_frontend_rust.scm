(define-module (crates-io ma hi mahiwa_frontend_rust) #:use-module (crates-io))

(define-public crate-mahiwa_frontend_rust-0.1.0 (c (n "mahiwa_frontend_rust") (v "0.1.0") (h "0i1cbi4wi7szdh7mkfhjk9sgx9ckr2na2gdlzcycafih32yap8ak")))

(define-public crate-mahiwa_frontend_rust-0.1.1 (c (n "mahiwa_frontend_rust") (v "0.1.1") (h "1amw5flv85cb8wgzvq51n16cq9w7crqmq5r94wrclvjkbc5j6hvk")))

