(define-module (crates-io ma p- map-gen-2d) #:use-module (crates-io))

(define-public crate-map-gen-2d-0.1.0 (c (n "map-gen-2d") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0raxf0drlz29g1pzwxhvxd5s3fd01hbw98810fbxk5syz176ayyb")))

(define-public crate-map-gen-2d-0.1.1 (c (n "map-gen-2d") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0snfd080w18wn1wpam7f1ljvyfgzb37x9kryrxicb7mpk2jfpdl1")))

(define-public crate-map-gen-2d-0.1.15 (c (n "map-gen-2d") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0159l2n0plwakj3ka6m1z3x3xmx63a2ha1x45s7l9axbzbczwi75")))

