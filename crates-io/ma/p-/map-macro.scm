(define-module (crates-io ma p- map-macro) #:use-module (crates-io))

(define-public crate-map-macro-0.1.0 (c (n "map-macro") (v "0.1.0") (h "0gh52pvxyfyinnkmiiw1ilw19x6anf2k5f9bkimqk4y8kbih554m")))

(define-public crate-map-macro-0.2.0 (c (n "map-macro") (v "0.2.0") (h "0c01k9hhxj76q1nmfkk261l3s9l6h0835p4npma1m6s456rc7pmk")))

(define-public crate-map-macro-0.2.1 (c (n "map-macro") (v "0.2.1") (h "0b22d50251lg1bcgnmypmi84a64v3l163mi2c8nns8bfzic0hnwx")))

(define-public crate-map-macro-0.2.2 (c (n "map-macro") (v "0.2.2") (h "1i72mdgc3a6bggmrpglx94rja2azzl58i79lv9qjfcbnqcvfr3ih")))

(define-public crate-map-macro-0.2.3 (c (n "map-macro") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1ds26mi9qca8js3lslvjg4nq777mkwcwksccgv3a1sjzd9nq34pz")))

(define-public crate-map-macro-0.2.4 (c (n "map-macro") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "09cxn8lkvziqc0i2ifmwamns7vljn00gh9gvpv0g5hz6p92bzzqd")))

(define-public crate-map-macro-0.2.5 (c (n "map-macro") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0pzkzawnvn0k0zl0b4da8drjbdbhgid2j863brii3712mjxbxja8")))

(define-public crate-map-macro-0.2.6 (c (n "map-macro") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1rp3vdqjcizkvpr11q8zfivxfjfrb275amifianqvk2s738znbkw")))

(define-public crate-map-macro-0.3.0 (c (n "map-macro") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 2)))) (h "0fldbch9qwy9z4x4d4v9i1j2mvgbgslaq59i92iyahln4m10m5gv") (f (quote (("std") ("hashbrown") ("default" "std")))) (s 2) (e (quote (("__docs" "dep:hashbrown"))))))

