(define-module (crates-io ma p- map-ok) #:use-module (crates-io))

(define-public crate-map-ok-0.1.0 (c (n "map-ok") (v "0.1.0") (h "0bp1q11zkwf4hm2m4xha3dy86cck61iqbdf63k2b6zchfvph1rq4")))

(define-public crate-map-ok-1.0.0 (c (n "map-ok") (v "1.0.0") (h "1rwyhhq40w4bwgxmhmb35kgfvby96d8hvwfrydj8brgpbs6ljpc6")))

