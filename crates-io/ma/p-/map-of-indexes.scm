(define-module (crates-io ma p- map-of-indexes) #:use-module (crates-io))

(define-public crate-map-of-indexes-0.1.0 (c (n "map-of-indexes") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05ans6fv52xzb2f5k0b27dk7w32zf5ij47pghfx3m76swx1hj4m4")))

(define-public crate-map-of-indexes-0.1.1 (c (n "map-of-indexes") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yfhx6wb0cyx3grskhn3k4kw8mvbcll9mzwgxdvl8sm8lxxcb39h")))

(define-public crate-map-of-indexes-0.1.2 (c (n "map-of-indexes") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07f056jgl41krq9h37g0zjzxkwxnbf7c7yfq4y8anmshaa2j8ykk")))

(define-public crate-map-of-indexes-0.1.3 (c (n "map-of-indexes") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "052w0i5v03wyjh16v224hmzsc336635dwr308zzkbrk3343cslac")))

(define-public crate-map-of-indexes-0.1.4 (c (n "map-of-indexes") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q9sc9wnmwwy83581drdxpmc9h4ky1vs05k5zrx8mi1lika258qq")))

