(define-module (crates-io ma p- map-parser) #:use-module (crates-io))

(define-public crate-map-parser-0.1.0 (c (n "map-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "027vnjjz7cdsgwi901wr0qhshmrg36vlda2xjikjj1p0274bg4wb") (y #t)))

(define-public crate-map-parser-0.1.1 (c (n "map-parser") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "11fas6dm4dwzlarnn0imasvcapppy5vgz82sgjgwirlckbdw78b2") (y #t)))

(define-public crate-map-parser-0.1.2 (c (n "map-parser") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "157sl832d5m9l41cn93lwpywvl8qgnh4qhzalxggq3rjs7n2p442") (y #t)))

(define-public crate-map-parser-0.1.3 (c (n "map-parser") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0hpycf94szf7c6vhah9zssnnnzyjkbivxlfggns0cbiy6rs0gbi9") (y #t)))

(define-public crate-map-parser-0.1.4 (c (n "map-parser") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "155p16q83ysjc0f7yysn1bdqp82w41vigdx8ih36jwiky5757q9d") (y #t)))

(define-public crate-map-parser-0.1.5 (c (n "map-parser") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "13zfyyyczlxyjz2yb117qk5vrg6jsdmgl08f6g5388zkm6cxy89c")))

