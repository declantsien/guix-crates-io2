(define-module (crates-io ma p- map-range) #:use-module (crates-io))

(define-public crate-map-range-0.1.0 (c (n "map-range") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1y9baxnd3kihrjjfsqf4w5synjv11r8adzmnw11ajvvybl3fyh99") (y #t)))

(define-public crate-map-range-0.1.1 (c (n "map-range") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1y4hxykavjy3bzrfz18z30zd2hfn9nhi3ffbxl1s8kpj15n07r6y")))

(define-public crate-map-range-0.1.2 (c (n "map-range") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19a5d2k3vnw2yziz9jrmq0lf96ydcjqnvzz51dklkdaigy6mbymz") (f (quote (("std" "num-traits/std") ("default" "std"))))))

