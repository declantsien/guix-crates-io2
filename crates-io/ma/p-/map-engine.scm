(define-module (crates-io ma p- map-engine) #:use-module (crates-io))

(define-public crate-map-engine-0.1.0 (c (n "map-engine") (v "0.1.0") (d (list (d (n "gdal") (r "^0.11") (f (quote ("ndarray"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ziqc7xr8zk25byyfm6j9mbv9i44mms9jc5ijh1hgp43ghx5dwd8")))

