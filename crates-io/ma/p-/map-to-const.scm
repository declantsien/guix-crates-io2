(define-module (crates-io ma p- map-to-const) #:use-module (crates-io))

(define-public crate-map-to-const-0.1.0 (c (n "map-to-const") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "sensible-env-logger") (r "^0.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "06j7a014a0hawfn72npiy4zwkr2jpdzwq5akhplajarqnlalxxb5")))

(define-public crate-map-to-const-0.2.0 (c (n "map-to-const") (v "0.2.0") (d (list (d (n "indoc") (r "^1.0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "sensible-env-logger") (r "^0.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "15iirgi778yvy322ah9rffax1y1nb1bd925zjx0mlrd78qizqcjv")))

