(define-module (crates-io ma tp matplotrust) #:use-module (crates-io))

(define-public crate-matplotrust-0.1.0 (c (n "matplotrust") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0r5qkpy599kp4wyp4iwkiqlgp7gak2fqrnq2lishfq50w3q3r2dx")))

(define-public crate-matplotrust-0.1.1 (c (n "matplotrust") (v "0.1.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0qzip9d12ka73izjfx2f62xxr71gxab8i80rphy0hhk67maah0py")))

(define-public crate-matplotrust-0.1.2 (c (n "matplotrust") (v "0.1.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0s8s3lvhlhx3g9zck3j8ylm454aayfb8pmmmadiblb2505w4r467")))

(define-public crate-matplotrust-0.1.3 (c (n "matplotrust") (v "0.1.3") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xkzngf8rmrb1zq2yz22amlk0s01l6fk3jy88a9zmw3shhkz2821")))

(define-public crate-matplotrust-0.1.4 (c (n "matplotrust") (v "0.1.4") (d (list (d (n "probability") (r "^0.15.12") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1k6ksfcrw8x445jf0y6fp4jvzyqgi02gm4xsdv50zszx7sf2vd1a")))

(define-public crate-matplotrust-0.1.5 (c (n "matplotrust") (v "0.1.5") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "probability") (r "^0.15.12") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0qjvqih89s9i6znnrq48jsl25akd7bnzabb4pmkzi71lcdmzd7zn")))

(define-public crate-matplotrust-0.1.6 (c (n "matplotrust") (v "0.1.6") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "probability") (r "^0.15.12") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1rsq60n99f53ap0nznvs9hlvc7w4wkgsq3bs5m3h2ji2x8v0cs2c")))

(define-public crate-matplotrust-0.1.7 (c (n "matplotrust") (v "0.1.7") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "probability") (r "^0.15.12") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "05dhsns5r87gvb1rbajbkjrr42k4zwxv4sb1y9fri66zayhv5ngx")))

