(define-module (crates-io ma ts matszpk-sokoban) #:use-module (crates-io))

(define-public crate-matszpk-sokoban-0.1.0 (c (n "matszpk-sokoban") (v "0.1.0") (d (list (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0jns5chiw076x5m0sww9m2i58a5mwa8cbb7cjm4fb8jwzp6208jm")))

(define-public crate-matszpk-sokoban-0.1.1 (c (n "matszpk-sokoban") (v "0.1.1") (d (list (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1bqkfz9dr3mima3zgh3y3h79dplsx1wj84ggy9myff7d74zqdqf7")))

