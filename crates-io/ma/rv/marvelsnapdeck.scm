(define-module (crates-io ma rv marvelsnapdeck) #:use-module (crates-io))

(define-public crate-marvelsnapdeck-0.1.0 (c (n "marvelsnapdeck") (v "0.1.0") (d (list (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12d7vjdmx6fq2bhwgsh0ibzwhifki35apznqz1smn9f9aa32pqqd")))

(define-public crate-marvelsnapdeck-0.1.1 (c (n "marvelsnapdeck") (v "0.1.1") (d (list (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c267g80v6gf687qf9zk9mjpwbb5al58v8zk0w8qj7xqjy6b0myv")))

