(define-module (crates-io ma rv marvin32) #:use-module (crates-io))

(define-public crate-marvin32-0.1.0 (c (n "marvin32") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0c5zgzkxyrvgqrxyc6xkicgp44zk3k58z71sh4v2c247s0033jyw")))

(define-public crate-marvin32-0.1.1 (c (n "marvin32") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0bzj5z6jgzmb8bdm0854km2wl5h57x9a9xxcy5sd87m507h0yr6j")))

