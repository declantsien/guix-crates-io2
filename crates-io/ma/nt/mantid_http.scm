(define-module (crates-io ma nt mantid_http) #:use-module (crates-io))

(define-public crate-mantid_http-0.0.1 (c (n "mantid_http") (v "0.0.1") (d (list (d (n "mantid_core") (r "^0.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0qf6wjyf7svkvqbwmzcbzcmihdsgk8d480vaa8zxikwnnwsqkm2a")))

