(define-module (crates-io ma nt mantle-test) #:use-module (crates-io))

(define-public crate-mantle-test-0.1.0 (c (n "mantle-test") (v "0.1.0") (d (list (d (n "mantle") (r "^0.1") (d #t) (k 0)) (d (n "mantle-macros") (r "^0.1") (d #t) (k 0)))) (h "0h6nmp4blmhhgjzi9ljq2fc7vly0ca6p4di12xvj0x8gsc7bl6dc")))

(define-public crate-mantle-test-0.2.0 (c (n "mantle-test") (v "0.2.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "mantle-macros") (r "^0.2") (d #t) (k 0)) (d (n "mantle-types") (r "^0.2") (d #t) (k 0)) (d (n "memchain") (r "^0.2") (d #t) (k 0)))) (h "0nqvs2xcznpdf547kkp12hakawp6jm18myb34gi9ncbadk66jp05")))

