(define-module (crates-io ma nt mantle-rpc) #:use-module (crates-io))

(define-public crate-mantle-rpc-0.1.0 (c (n "mantle-rpc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04zslg5380jg4vzv4fv759ynibbn0jis28jdlwyr9x88pd5drhcc")))

(define-public crate-mantle-rpc-0.1.1 (c (n "mantle-rpc") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "walrus") (r "^0.8") (o #t) (d #t) (k 0)))) (h "18yhqfisrcw1c7n1bn89aw467p67hmp0f03p3k7cyaqhd9ship0a") (f (quote (("saveload" "libflate" "serde_json" "failure") ("resolve" "import") ("import" "saveload" "url" "walrus"))))))

