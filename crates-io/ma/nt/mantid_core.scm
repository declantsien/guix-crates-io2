(define-module (crates-io ma nt mantid_core) #:use-module (crates-io))

(define-public crate-mantid_core-0.0.1 (c (n "mantid_core") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05k92gcyfa77gjdpkbcvz6641xgxnhyb7xrhphax1vxv5nh7ph0v")))

(define-public crate-mantid_core-0.0.2 (c (n "mantid_core") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0528chl9ihqgp8wjyw75f4hk9d26h6yydfj12bajl99ivczac2ra")))

