(define-module (crates-io ma nt mantle-macros) #:use-module (crates-io))

(define-public crate-mantle-macros-0.1.0 (c (n "mantle-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1fbr1jcfgp088dssrkwa8slnhb1qq835w9bpa227b4vh72mcld7j")))

(define-public crate-mantle-macros-0.2.0 (c (n "mantle-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "06l9sza5wcinaafv44ml8arxbqidi3vp6zd76p9qj285hyhq2w37")))

(define-public crate-mantle-macros-0.2.1 (c (n "mantle-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1r2kmsajiwrbyvsjgydpvh1ind74m28nb61x4q5cxasxnbi3rfyg")))

