(define-module (crates-io ma nt mantid_grep) #:use-module (crates-io))

(define-public crate-mantid_grep-0.0.1 (c (n "mantid_grep") (v "0.0.1") (d (list (d (n "mantid_core") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1dk18kba3x5grnqfkzzfjgn0qpym70yf7nb83xw5fdl7shwg4rpb")))

