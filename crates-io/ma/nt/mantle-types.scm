(define-module (crates-io ma nt mantle-types) #:use-module (crates-io))

(define-public crate-mantle-types-0.1.0 (c (n "mantle-types") (v "0.1.0") (d (list (d (n "blockchain-traits") (r "^0.1") (d #t) (k 0)) (d (n "fixed-hash") (r "^0.3") (f (quote ("byteorder"))) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uint") (r "^0.6") (d #t) (k 0)))) (h "05amr5hisl88hkxibcq48vwrnxqyn6fv258k9n61dp48vjz3v5zh")))

(define-public crate-mantle-types-0.2.0 (c (n "mantle-types") (v "0.2.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "046fxnnm9s0xzkxg095md0nmcbclr4jlbzi6h9v91qsy55qd86db")))

