(define-module (crates-io ma nt mantra-rust-macros) #:use-module (crates-io))

(define-public crate-mantra-rust-macros-0.1.0 (c (n "mantra-rust-macros") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "mantra-rust-procm") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)))) (h "0v850hmhj16kq4sx4k8pwj6smj7njjzfj7sy4igj4vz38hj29c79") (f (quote (("stdout" "std") ("std")))) (s 2) (e (quote (("log" "dep:log") ("extract" "dep:regex" "dep:once_cell" "std") ("defmt" "dep:defmt"))))))

(define-public crate-mantra-rust-macros-0.3.0 (c (n "mantra-rust-macros") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "mantra-rust-procm") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)))) (h "15f9358llsfyhrgqi6388m2hqpkib56drbs95wk0rs1j74rk623a") (f (quote (("stdout" "std") ("std")))) (s 2) (e (quote (("log" "dep:log") ("extract" "dep:regex" "dep:once_cell" "std") ("defmt" "dep:defmt"))))))

