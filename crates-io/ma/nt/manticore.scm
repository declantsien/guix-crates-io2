(define-module (crates-io ma nt manticore) #:use-module (crates-io))

(define-public crate-manticore-0.0.0 (c (n "manticore") (v "0.0.0") (h "0q8sddd9ymp6lf0mn6jawvlxhvlwqyvhrv344k995ibapwqmbfsl")))

(define-public crate-manticore-0.0.1 (c (n "manticore") (v "0.0.1") (h "1prhhniw47njd88ds2xfw9rcahwhhjnc7s6sby4k3ss476f1pizl")))

