(define-module (crates-io ma nt mantra-rust-procm) #:use-module (crates-io))

(define-public crate-mantra-rust-procm-0.1.0 (c (n "mantra-rust-procm") (v "0.1.0") (d (list (d (n "mantra-lang-tracing") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00f864kqdsl4cimkfhm7srb1x0zywd6zf687z30hmfy5f3i03r5r")))

(define-public crate-mantra-rust-procm-0.3.0 (c (n "mantra-rust-procm") (v "0.3.0") (d (list (d (n "mantra-lang-tracing") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10bkn57sl1cjjrpqxw3hj906y7yqdy0aqcbs2v1yqy0anxhg53yj")))

