(define-module (crates-io ma nt mantra-lang-tracing) #:use-module (crates-io))

(define-public crate-mantra-lang-tracing-0.1.0 (c (n "mantra-lang-tracing") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20.4") (d #t) (k 0)))) (h "0qlkh1qjbrz05mihyy6449hba96dy7dbkcn95khfrnr9f5d27ba1")))

(define-public crate-mantra-lang-tracing-0.3.0 (c (n "mantra-lang-tracing") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "mantra-rust-macros") (r "^0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.21.0") (d #t) (k 0)))) (h "0nxdq8993vws1cg3wfvpsgypbccgpj48y9gfr4yz9zlfry7dg77g")))

