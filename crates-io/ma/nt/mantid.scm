(define-module (crates-io ma nt mantid) #:use-module (crates-io))

(define-public crate-mantid-0.0.1 (c (n "mantid") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mantid_core") (r "^0.0.1") (d #t) (k 0)) (d (n "mantid_grep") (r "^0.0.1") (d #t) (k 0)) (d (n "mantid_hash") (r "^0.0.1") (d #t) (k 0)))) (h "1s5mamhcdk84qmc9qg30hhvq2n72yibl999qdznw8g6hckbkcz6x")))

(define-public crate-mantid-0.0.2 (c (n "mantid") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mantid_core") (r "^0.0.2") (d #t) (k 0)) (d (n "mantid_grep") (r "^0.0.1") (d #t) (k 0)) (d (n "mantid_hash") (r "^0.0.1") (d #t) (k 0)) (d (n "mantid_http") (r "^0.0.1") (d #t) (k 0)))) (h "02myxwhnr7bcqwjynv846msgw10pg55vbk3bqs5rah6pl6b5daqj")))

