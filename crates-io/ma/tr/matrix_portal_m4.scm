(define-module (crates-io ma tr matrix_portal_m4) #:use-module (crates-io))

(define-public crate-matrix_portal_m4-0.1.0 (c (n "matrix_portal_m4") (v "0.1.0") (d (list (d (n "atsamd-hal") (r "^0.16.0") (k 0)) (d (n "cortex-m") (r "^0.7") (f (quote ("critical-section-single-core"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "usb-device") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0j3l5m9qkkc3n2d9q6dl22wdx9qh2c8vkg3gxi7dddp4kf42adq4") (f (quote (("usb" "atsamd-hal/usb" "usb-device") ("unproven" "atsamd-hal/unproven") ("rt" "cortex-m-rt" "atsamd-hal/samd51j-rt") ("default" "rt" "atsamd-hal/samd51j"))))))

