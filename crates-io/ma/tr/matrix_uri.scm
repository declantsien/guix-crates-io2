(define-module (crates-io ma tr matrix_uri) #:use-module (crates-io))

(define-public crate-matrix_uri-0.1.0 (c (n "matrix_uri") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "ruma-identifiers") (r "^0.17.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "0w7fd3nvzncr2rw91vdgjamr2cia0pw8fafypbwdzihi62kx35gb") (f (quote (("ruma" "ruma-identifiers"))))))

(define-public crate-matrix_uri-0.1.1 (c (n "matrix_uri") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "ruma-identifiers") (r "^0.17.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "10ajv8vdyn6dfrb5d6nwbiq20nbmlzbgsb6ns1bzngnnjyra0h3m") (f (quote (("ruma" "ruma-identifiers"))))))

