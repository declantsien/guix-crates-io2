(define-module (crates-io ma tr matry) #:use-module (crates-io))

(define-public crate-matry-0.1.0 (c (n "matry") (v "0.1.0") (h "04hyx8l7q286lnybiz1dadw99lqhg0nxmlqz5s155x14dzflaakx")))

(define-public crate-matry-0.0.0 (c (n "matry") (v "0.0.0") (h "1yin014i6w0akczgyfmpb7ny8dqm8771n9n8ix7vfdcn4vab0la6")))

