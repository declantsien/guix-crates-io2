(define-module (crates-io ma tr matricks_plugin) #:use-module (crates-io))

(define-public crate-matricks_plugin-0.1.0 (c (n "matricks_plugin") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1066i30hw9aqmldiq432hvm6vagmrppkhmlixd9isl9fk9cjzlcm") (y #t)))

(define-public crate-matricks_plugin-0.1.1 (c (n "matricks_plugin") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w34yy1xrsmp8j59jcalvpb3jyzmhbd5kd4xpibzwp0nv4hr5y6j") (y #t)))

(define-public crate-matricks_plugin-0.1.2 (c (n "matricks_plugin") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hdin5fr1y336r2l8z7gbkk6qskrpnx47lpqyjdskrgbb1hhwqak") (y #t)))

(define-public crate-matricks_plugin-0.1.3 (c (n "matricks_plugin") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a7nsclrn4kix658hv6jqpcrwamz3y7fbdmm4rw21s6xdl1n71b4")))

(define-public crate-matricks_plugin-0.1.4 (c (n "matricks_plugin") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yimmkvc3cvw7psikvq22y6ksf5b7ndplzs3fr484w2mbp68j225")))

