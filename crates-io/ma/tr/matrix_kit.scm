(define-module (crates-io ma tr matrix_kit) #:use-module (crates-io))

(define-public crate-matrix_kit-0.1.0 (c (n "matrix_kit") (v "0.1.0") (h "037bjdp17vlj89qf596w3m8vwksmqsaaqrv9q1hsxr9zig85nrgm")))

(define-public crate-matrix_kit-0.1.1 (c (n "matrix_kit") (v "0.1.1") (h "1dmbza3b1wwaisj94qkyghy0891qyjk9kffj6dy4m5f8n62npdik")))

(define-public crate-matrix_kit-0.1.2 (c (n "matrix_kit") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y9djsyyvjqfvzb78sm52vab818l3zic8rwsyw53asfwl9wyx495")))

(define-public crate-matrix_kit-0.1.3 (c (n "matrix_kit") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ahg14b2a3swhawx2v9j782wh0p4rm200yailxy2qq5accvp6afi")))

(define-public crate-matrix_kit-0.1.4 (c (n "matrix_kit") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y5w2c90ax1w6ikmwaniklx68jw75n7n72pd6qafsjhy6rcbq62s")))

