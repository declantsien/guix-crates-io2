(define-module (crates-io ma tr matrix_operations_cuda) #:use-module (crates-io))

(define-public crate-matrix_operations_cuda-0.1.0 (c (n "matrix_operations_cuda") (v "0.1.0") (d (list (d (n "cuda-driver-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "matrix_operations") (r "^0.1.3") (d #t) (k 0)))) (h "1arsvjinzxkls4mgsnjbkwpc8h21y3pabhzhs6y100f8cjla0a46")))

(define-public crate-matrix_operations_cuda-0.1.1 (c (n "matrix_operations_cuda") (v "0.1.1") (d (list (d (n "cuda-driver-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "matrix_operations") (r "^0.1.3") (d #t) (k 0)))) (h "0dgb6rn8312d1gixlddscgr9gly9rws2920zvprvgqiawvdqqxlr")))

(define-public crate-matrix_operations_cuda-0.1.2 (c (n "matrix_operations_cuda") (v "0.1.2") (d (list (d (n "cuda-driver-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "matrix_operations") (r "^0.1.3") (d #t) (k 0)))) (h "19irr85ps3ni84hrm5szpgnrzjqfk6add65f8yclhg2qbc1nvdqz")))

