(define-module (crates-io ma tr matrix_engine) #:use-module (crates-io))

(define-public crate-matrix_engine-0.1.0 (c (n "matrix_engine") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (d #t) (k 0)) (d (n "winit") (r "^0.28.3") (d #t) (k 0)))) (h "170583wck94iqx0ra5lxbyh0i69cf92j252n9r7yv7hllhk6hxnq")))

