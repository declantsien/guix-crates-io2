(define-module (crates-io ma tr matrixstack) #:use-module (crates-io))

(define-public crate-matrixstack-0.1.0 (c (n "matrixstack") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0yilbsl8m70m2z1zgvrclg5f5mmkr2i49zdqv3yr41mvln3cxnfg")))

(define-public crate-matrixstack-0.1.1 (c (n "matrixstack") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0s9zbh4ccfidac2nn3ycdy87nf2bdd4djgcxbqi4mga2xh26ql28")))

(define-public crate-matrixstack-0.1.2 (c (n "matrixstack") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1hs2kh809vwwpsi3w42r7nin5a4as00cl8y7bcvyw3p6xmi8d0fs")))

(define-public crate-matrixstack-0.1.3 (c (n "matrixstack") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0p8s7c3dffkjscb8bfsxmwnfvh60ai1llw4ahs95q7zkx3c1iwri")))

(define-public crate-matrixstack-0.1.4 (c (n "matrixstack") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1bi2ajznl8dl3bdhs4awv8iw3y86pd3s8p0mc5sqin2ij45s1djl")))

