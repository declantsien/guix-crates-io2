(define-module (crates-io ma tr matrix-sdk-test-macros) #:use-module (crates-io))

(define-public crate-matrix-sdk-test-macros-0.1.0 (c (n "matrix-sdk-test-macros") (v "0.1.0") (h "05pdlhppmcxd6kz5pidp0m8dykams9rpi76cjqc4lsh9cc0ya529")))

(define-public crate-matrix-sdk-test-macros-0.2.0 (c (n "matrix-sdk-test-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v0snp52sb8z1g583cfivg4d3hkwrp8zrxqdjipp3dvfq2vfnnx3") (r "1.60")))

(define-public crate-matrix-sdk-test-macros-0.3.0 (c (n "matrix-sdk-test-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hfpj21mrw02ii7skpx4s4g0yv5p9rhm627viz7qsbklvfw9ws2v") (r "1.60")))

(define-public crate-matrix-sdk-test-macros-0.7.0 (c (n "matrix-sdk-test-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yr48204a44irzg1l3yx1dlfskj8i71c97i6xki4n92i8pw0qnpb") (r "1.70")))

