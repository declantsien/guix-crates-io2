(define-module (crates-io ma tr matrix_display) #:use-module (crates-io))

(define-public crate-matrix_display-0.1.0 (c (n "matrix_display") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)))) (h "01acfxik0wx8gnbrmsvk78mvqlb8snfkcd5fpcxgcs8vlcrliwi2")))

(define-public crate-matrix_display-0.2.0 (c (n "matrix_display") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)))) (h "12hya8lq8zrljc5jkx3fals3j5pd5qvqmnlahal5f9k1ndri1z6h")))

(define-public crate-matrix_display-0.3.0 (c (n "matrix_display") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)))) (h "1x2lir3vnka9mnq4fz2879qavlir4l3hvchb7a1ap7gdg16ppc81")))

(define-public crate-matrix_display-0.4.0 (c (n "matrix_display") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)))) (h "0sl3hh7inqjc9nz1ml8whvfbb231chpizhs9q416yqbdalrq90pk")))

(define-public crate-matrix_display-0.4.1 (c (n "matrix_display") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)))) (h "05yxhij318jakmp4772dz76hh9mvh3xanllsplgr09h73ws4jhnp")))

(define-public crate-matrix_display-0.5.0 (c (n "matrix_display") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "03wz9ym5zcxa90jq46vq592mw376grkk6qjqzck2ch7x0ab349vy")))

(define-public crate-matrix_display-0.6.0 (c (n "matrix_display") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06w89g4rw8dcz2zvy01jqpk6r8k6qw78wgcw4vkjq9zg4f267nc9")))

(define-public crate-matrix_display-0.7.0 (c (n "matrix_display") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1iyssycpqjfhyrgx5gdy1l9fa3f3k11kflib6fz8ljh0m54vkzx8")))

(define-public crate-matrix_display-0.8.0 (c (n "matrix_display") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0z8v70zjs5kcp2zg22dbk93v5qql4nfwvcgpm3n0521q88j70x9m")))

(define-public crate-matrix_display-0.9.0 (c (n "matrix_display") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0vvg9zq8cbi6w4lnyc2qpamc7nq3wl44ddznky0jxkmafii415hz")))

(define-public crate-matrix_display-1.0.0 (c (n "matrix_display") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode_types") (r "^0.2.0") (d #t) (k 0)))) (h "1zgpfr8ilvyqyp57q3wwj0hahk60jxsh40iscxizs9w1d5p03qd6")))

