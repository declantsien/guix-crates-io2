(define-module (crates-io ma tr matr) #:use-module (crates-io))

(define-public crate-matr-0.1.0 (c (n "matr") (v "0.1.0") (h "01m1sk51z3qpfv534bqwnwznn6v1s7097h5f38bgxzfgr0byzvpp")))

(define-public crate-matr-0.2.0 (c (n "matr") (v "0.2.0") (h "1yq03rz3hllyl8afrwlm3xx57s96zqznyw45zs39l2ay5l7wn5wb")))

