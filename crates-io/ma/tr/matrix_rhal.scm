(define-module (crates-io ma tr matrix_rhal) #:use-module (crates-io))

(define-public crate-matrix_rhal-0.0.0 (c (n "matrix_rhal") (v "0.0.0") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "0866nnfmkwl73m6ff31f5jdwv6hax3x5x2ra559nmnpww63l6grh") (y #t)))

(define-public crate-matrix_rhal-0.0.1 (c (n "matrix_rhal") (v "0.0.1") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1rw6qab2sqvfl0h9ry3mbgfsb1rpfkxpk1havr4z6hckgc3k5ws5")))

(define-public crate-matrix_rhal-0.0.2 (c (n "matrix_rhal") (v "0.0.2") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1xr2gn2hpx5blwnx1bfv4dln1bvpxs9vgskpr0vi96caiykh8qys")))

(define-public crate-matrix_rhal-0.0.3 (c (n "matrix_rhal") (v "0.0.3") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1gvqmxxdaihri1gycqcrzsdfql860gn872nbiidg4nr9xvm85v3n")))

(define-public crate-matrix_rhal-0.0.4 (c (n "matrix_rhal") (v "0.0.4") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "0qx8rw1nqdljr4x961rc1w7gwf2v62bz8m4grv6rskpmmns4nfs0")))

(define-public crate-matrix_rhal-0.0.5 (c (n "matrix_rhal") (v "0.0.5") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1166mlh3xqdwchpqmqlgv3vjxnwpzjgcl1ws20c61vyqy36gkac2")))

(define-public crate-matrix_rhal-0.0.6 (c (n "matrix_rhal") (v "0.0.6") (d (list (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1k3wc28khiy0v1dw7r2jwzfjkxi0dz5gz5kcyl437rh0d0mkq91b")))

