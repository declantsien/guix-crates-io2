(define-module (crates-io ma tr matroska-demuxer) #:use-module (crates-io))

(define-public crate-matroska-demuxer-0.1.0 (c (n "matroska-demuxer") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0pqih0sa2bidr9zl3ibp9hwgzxx6is3hvjmjvci65vzcssniw1wl") (y #t)))

(define-public crate-matroska-demuxer-0.1.1 (c (n "matroska-demuxer") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0l6r86aapjvlqzzbi757f0yn6266ym7qvv84v0sylx73r3vcgljk")))

(define-public crate-matroska-demuxer-0.2.0 (c (n "matroska-demuxer") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "13a0y2d27af6n04c7dazq2g0ig4iy9z2lmr0jjk3zmq15d2b2kgk")))

(define-public crate-matroska-demuxer-0.3.0 (c (n "matroska-demuxer") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "089zrk5xnqvssr2k9dbk94hvgs46zvda7ykrkaf8zpy91bz41khn")))

(define-public crate-matroska-demuxer-0.4.0 (c (n "matroska-demuxer") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1baxmyk64svxhiw7hmypjxzdw7rzqprgcna5k0nj742md8zm8df2")))

(define-public crate-matroska-demuxer-0.5.0 (c (n "matroska-demuxer") (v "0.5.0") (h "143sir61gmpar9fhpd5a91x1l0cay0b8k769vh9dsrj3kf6lv6ij") (r "1.70")))

