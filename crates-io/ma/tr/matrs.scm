(define-module (crates-io ma tr matrs) #:use-module (crates-io))

(define-public crate-matrs-0.1.0 (c (n "matrs") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0rr6mgn2sy7k11mq0wl4lz6w4s6zz12dafsycljl6b2q9g92czpg") (y #t)))

(define-public crate-matrs-0.1.1 (c (n "matrs") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "10gnyg01j7ifnk5bjw1z5gjadgc6si42faah2vi5bdhjmbzbh5v5") (y #t)))

(define-public crate-matrs-0.2.0 (c (n "matrs") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1vj19l38babsbbav4k3pp2rsrm05n7qzilz8qqxa28mipwrrvndr") (f (quote (("experimental")))) (y #t)))

