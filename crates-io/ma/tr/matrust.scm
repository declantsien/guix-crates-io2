(define-module (crates-io ma tr matrust) #:use-module (crates-io))

(define-public crate-matrust-0.0.1 (c (n "matrust") (v "0.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "024bn6ggkx830g0y6sp9cn6sf6m7mc6wzzkcw0vkqz5r92cxighi")))

(define-public crate-matrust-0.0.2 (c (n "matrust") (v "0.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zjj0a0h9shiwkm0p2za8c441f24mykmxgj3jf73vm3qajr32kdx")))

(define-public crate-matrust-0.0.3 (c (n "matrust") (v "0.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0am68q4m2a6x20269w0a1jcg9kblqyxhislpyyb1nvsxic98cg78")))

(define-public crate-matrust-0.0.4 (c (n "matrust") (v "0.0.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10fqr8c59bh0b6w9slja3mjri59disqi566gwd3lwixsj3skr3cx")))

(define-public crate-matrust-0.0.5 (c (n "matrust") (v "0.0.5") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0y4qlcp8x9kqia7sidsznblwf2jy515azmcsdqrzvb91p6mjmd9i")))

(define-public crate-matrust-0.0.6 (c (n "matrust") (v "0.0.6") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "1j82dkgibzisw51rrcz1zlr0x00bp318yyx6bb6iiz9hwq8dr6nf")))

