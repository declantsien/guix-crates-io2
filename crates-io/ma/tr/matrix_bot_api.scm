(define-module (crates-io ma tr matrix_bot_api) #:use-module (crates-io))

(define-public crate-matrix_bot_api-0.1.0 (c (n "matrix_bot_api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fractal-matrix-api") (r "^3.29.0") (d #t) (k 0)))) (h "06s5iw6pz0ni3dq3mc9b0x020i3z2kbvf4sba7fv7znkh9yfwqg6")))

(define-public crate-matrix_bot_api-0.2.0 (c (n "matrix_bot_api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^3.29.0") (d #t) (k 0)))) (h "1piv23b802vbk974dl4w195yxkq0ndrp6wh53llrycccsncwdd5l")))

(define-public crate-matrix_bot_api-0.3.0 (c (n "matrix_bot_api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^3.29.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0m27ky60crrlq4mz085wycw5dpg41q7kkhmrdympwim7jvnq1wa5")))

(define-public crate-matrix_bot_api-0.3.1 (c (n "matrix_bot_api") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.8") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^3.29.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0islx71nszc4jbkmmg76xfa4jkw9g34sid97lh19m3j187v4xn8v")))

(define-public crate-matrix_bot_api-0.4.0 (c (n "matrix_bot_api") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.9.2") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1ym8j771zkjbvbad2nlwc5qxxzn50sq28cvjkd03g7lnnk8bsbv7")))

(define-public crate-matrix_bot_api-0.5.0 (c (n "matrix_bot_api") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.9.3") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0vpsx68x971h793a4g0n53i9kwwpxg2dpar0wxljdn0k5x7g2pr0")))

(define-public crate-matrix_bot_api-0.5.1 (c (n "matrix_bot_api") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.9.3") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0wj621yvzn84xkvsgs4kxrfvi7285djs5zcd8vgj339w2yd3lsd6")))

(define-public crate-matrix_bot_api-0.5.2 (c (n "matrix_bot_api") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.9.3") (d #t) (k 2)) (d (n "fractal-matrix-api") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1x8is7wyc62vighm3vl7fdyjnyh7q3lg7v7pbnyr1vrrd1dxzk41")))

