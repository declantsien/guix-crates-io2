(define-module (crates-io ma tr matrix-pickle) #:use-module (crates-io))

(define-public crate-matrix-pickle-0.1.0 (c (n "matrix-pickle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "matrix-pickle-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "09fj2m26mlxgq4r1mxclf3wiwp5hms92vyfv814d9j2cysrqb0di") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:matrix-pickle-derive")))) (r "1.65")))

(define-public crate-matrix-pickle-0.1.1 (c (n "matrix-pickle") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "matrix-pickle-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07c2kmlswi7ir79ldgcizcibm61i8lni9i5rkf6npn757i32dzfp") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:matrix-pickle-derive")))) (r "1.65")))

(define-public crate-matrix-pickle-0.2.0 (c (n "matrix-pickle") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "matrix-pickle-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "09mq9xxhnw7rywavc8nddfn0zp3lp2z500ppa4h7mi980ccj3dby") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:matrix-pickle-derive")))) (r "1.65")))

