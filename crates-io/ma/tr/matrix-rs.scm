(define-module (crates-io ma tr matrix-rs) #:use-module (crates-io))

(define-public crate-matrix-rs-1.0.0 (c (n "matrix-rs") (v "1.0.0") (h "0xkydf9ajjain47fv1v861gc19nydy8fm3f8544s6xssm7rlr7gf")))

(define-public crate-matrix-rs-1.0.1 (c (n "matrix-rs") (v "1.0.1") (d (list (d (n "checks") (r "^1.0.0") (d #t) (k 0)))) (h "0007cml10yvdnjhxag42vk902biyb54cd338ycr8h9lw6raxv0h4")))

