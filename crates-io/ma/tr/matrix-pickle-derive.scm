(define-module (crates-io ma tr matrix-pickle-derive) #:use-module (crates-io))

(define-public crate-matrix-pickle-derive-0.1.0 (c (n "matrix-pickle-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.47") (k 0)) (d (n "quote") (r "^1.0.21") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "10av7wa3149gngcbbkk3gh389gmm4v225r4571fi5mfh92xqzw6f") (r "1.65")))

(define-public crate-matrix-pickle-derive-0.1.1 (c (n "matrix-pickle-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.47") (k 0)) (d (n "quote") (r "^1.0.21") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1xf0mjcl1mkbyzznf5xqy65cz4k1hq5b31v2fhsgxhirinkrlxwk") (r "1.65")))

(define-public crate-matrix-pickle-derive-0.2.0 (c (n "matrix-pickle-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.79") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "13ny2wka322y3ki1zandharq6lgnxgnia5l7a2ygpdyb65r3ryy6") (r "1.65")))

