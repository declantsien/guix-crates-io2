(define-module (crates-io ma tr matroska) #:use-module (crates-io))

(define-public crate-matroska-0.3.0 (c (n "matroska") (v "0.3.0") (d (list (d (n "bitstream-io") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "15wn0bqnn42jik1i6ciijsays8d2z8s8qd2vidwyxnj5hxb21nbl")))

(define-public crate-matroska-0.4.0 (c (n "matroska") (v "0.4.0") (d (list (d (n "bitstream-io") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "17wakvcbrys9b14k8fbrlz2xwwm0sl5wggp0vhaj05nk8rlal0d9")))

(define-public crate-matroska-0.5.0 (c (n "matroska") (v "0.5.0") (d (list (d (n "bitstream-io") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0wlwfhwhcxrrcgwgxa0hrxp183x4s25103d84v42546vjpprjm29")))

(define-public crate-matroska-0.5.1 (c (n "matroska") (v "0.5.1") (d (list (d (n "bitstream-io") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0jk4xrz7k5m7bb0mswrq960l06hfazy7n18wvhcmspfxmx0ngp16")))

(define-public crate-matroska-0.5.3 (c (n "matroska") (v "0.5.3") (d (list (d (n "bitstream-io") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ps7rpk7qm4vw3ks5kqg1wzxwvkzd9w2i7l37al1kiiwn07i2vcb") (y #t)))

(define-public crate-matroska-0.5.4 (c (n "matroska") (v "0.5.4") (d (list (d (n "bitstream-io") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0f08nzn1hn32krgshlqv0647iyjv30gqv3jcd20x1s4fksjsd9k5")))

(define-public crate-matroska-0.5.5 (c (n "matroska") (v "0.5.5") (d (list (d (n "bitstream-io") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0wn107ys9z57bvcc3y77m7xmns2ar819rwvx3gv4asjnbp09zjp5")))

(define-public crate-matroska-0.6.0 (c (n "matroska") (v "0.6.0") (d (list (d (n "bitstream-io") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "08vskhm95xff8shsmzdvf5s0xh83ixdkkd27j0pr8j6d5fwlzcdl")))

(define-public crate-matroska-0.7.0 (c (n "matroska") (v "0.7.0") (d (list (d (n "bitstream-io") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "18qwapmx66w8sfbp1m51fl5nr79dxa60r23880jqvcsdix3hhxs0")))

(define-public crate-matroska-0.8.0 (c (n "matroska") (v "0.8.0") (d (list (d (n "bitstream-io") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "03x1xbva1v01bcakk494ly0jnc1mc1ynbqpx7wynis1rikkylzh0")))

(define-public crate-matroska-0.9.0 (c (n "matroska") (v "0.9.0") (d (list (d (n "bitstream-io") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rccs1hknm6s88hhc7sk2ypxbdssix7h9aiyj3gk16wqd0402jbs")))

(define-public crate-matroska-0.10.0 (c (n "matroska") (v "0.10.0") (d (list (d (n "bitstream-io") (r "^1.2") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "06ir4s4szamswzhl8wgaxinpahy3h24n6w7xjvjm704bw6n5i4z2")))

(define-public crate-matroska-0.11.0 (c (n "matroska") (v "0.11.0") (d (list (d (n "bitstream-io") (r "^1.2") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "0xsxwfvwkx6zm4rvvm3gr5yrpwddr0av7kxa5l3airyhycp3w2gf")))

(define-public crate-matroska-0.12.0 (c (n "matroska") (v "0.12.0") (d (list (d (n "bitstream-io") (r "^1.2") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ql5i6gx6kvlcwkvkr6spv6asys27a4k0zk1y782a821xhvcjhxw")))

(define-public crate-matroska-0.13.0 (c (n "matroska") (v "0.13.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "0h9ibn719nr6r5lyaq6akn3jxbr84j0m9891ij1z0m2wlx0b91zm")))

(define-public crate-matroska-0.14.0 (c (n "matroska") (v "0.14.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "08vm8z16ipj15b8ablzx9h0a2509gkrmw6zpdwrqg33kb9zczdmf")))

(define-public crate-matroska-0.15.0 (c (n "matroska") (v "0.15.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "1051vkcvgdmk3m3m1fqghzni1ll989ix85whyy0vswlg8rfxmcyg")))

(define-public crate-matroska-0.16.0 (c (n "matroska") (v "0.16.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "01w2bblxk8vshgpzxcr7m1g6ad6wswvmdzh6iplmnxs91g1vdh2b")))

(define-public crate-matroska-0.17.0 (c (n "matroska") (v "0.17.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "1zb5s1wn15ghiqv540i9l0g13q6scpdw2vj4lnjjwi0ajafm924j")))

(define-public crate-matroska-0.18.0 (c (n "matroska") (v "0.18.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "0xss4vcsy9gd4zgj2141myw6hlbng3i86za477qbq4dscqsy0wxm")))

(define-public crate-matroska-0.19.0 (c (n "matroska") (v "0.19.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "178rd5jyqhfv40n2j010av7fpf050pdl7bj7p783hyr5nqqqnn13")))

(define-public crate-matroska-0.20.0 (c (n "matroska") (v "0.20.0") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rw3vj86h932kgdkgpgxvj5j7zfrzzyc3w2jhni2jprlsb9lb784")))

(define-public crate-matroska-0.20.1 (c (n "matroska") (v "0.20.1") (d (list (d (n "bitstream-io") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "139wl0n3gz6s1w4wg7lz85ki0r5rwgy8il0x3dbs4vw3f856z4s1")))

(define-public crate-matroska-0.21.0 (c (n "matroska") (v "0.21.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "1xd3nr7nip8w0r9175d8ah1cbfxnsc59xxcj5zrbdk1fii5c04d1")))

(define-public crate-matroska-0.22.0 (c (n "matroska") (v "0.22.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "1qx26qv1zka551r4gqvpwqc5k1vbfj3ypn5vd8if6v8mzm96306s")))

(define-public crate-matroska-0.23.0 (c (n "matroska") (v "0.23.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rchsql7p5ilirngf9p9dkyw7s5y9yd3axiaf6dk31dvhvj4jaz6")))

(define-public crate-matroska-0.24.0 (c (n "matroska") (v "0.24.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "0cbklq3yx70r3f0jgcrh5piwh3084bv63sj8bdddcvqzzv9vqj32")))

(define-public crate-matroska-0.25.0 (c (n "matroska") (v "0.25.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "0np7zjrxcawvscrqqal6xnjlw0idhzd2fgbzlkkxr1kikj727mvw")))

(define-public crate-matroska-0.26.0 (c (n "matroska") (v "0.26.0") (d (list (d (n "bitstream-io") (r "^1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "0hfr814k4rw767lwhx5f9741h18mbf4vf3lrj72zi9zz8dn6yll9")))

(define-public crate-matroska-0.26.1 (c (n "matroska") (v "0.26.1") (d (list (d (n "bitstream-io") (r "^2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "1l2rssz07jf14axvyyvhfs4hbgw4ixryfny8bfw48pnlnzxj0jx7")))

