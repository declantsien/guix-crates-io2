(define-module (crates-io ma tr matrix-market-rs) #:use-module (crates-io))

(define-public crate-matrix-market-rs-0.1.0 (c (n "matrix-market-rs") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1z91by4pr65m71dy6w1ab175w4sq17lphf6gz8h1yjfs0n8ja1ld")))

(define-public crate-matrix-market-rs-0.1.1 (c (n "matrix-market-rs") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "04qjwimqwiml8jaijvvjwniyi97nljvrwf68n7b32h4vkkazasdr")))

(define-public crate-matrix-market-rs-0.1.2 (c (n "matrix-market-rs") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10rchkk8cxhr7snyvc5kkdjanjz8dvid9qxjn3ydip8s9yiyv7ka")))

(define-public crate-matrix-market-rs-0.1.3 (c (n "matrix-market-rs") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pr8wp3p1gkwk9pr446y2778mm7gwjnfqnyqsnjgqqqxs0kxrci4")))

