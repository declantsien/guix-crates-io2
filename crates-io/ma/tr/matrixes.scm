(define-module (crates-io ma tr matrixes) #:use-module (crates-io))

(define-public crate-matrixes-1.0.0 (c (n "matrixes") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1116iqg8d2qfjylvdivqrq0mcvri0bgydg1vva2mcgzjp1ayma8r")))

(define-public crate-matrixes-1.1.0 (c (n "matrixes") (v "1.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1lyr78g023iih95aiawkqqbcpsrkzdbjb6d5s4y148g0246b5k99")))

(define-public crate-matrixes-1.2.0 (c (n "matrixes") (v "1.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0pi16wp247a6j2063pwfv4w58g63rc8d0l1r544m8pqirnv7vqp6")))

(define-public crate-matrixes-1.3.0 (c (n "matrixes") (v "1.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0byh13xqg99jzxp3jq87braf9nljcxiqb4ajf22xajnrgq9zawlf")))

(define-public crate-matrixes-1.4.0 (c (n "matrixes") (v "1.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0abx3kkg5x8cbr2mi67s1v3xgqz0nphypah2krcafy5kqzhhwifp")))

(define-public crate-matrixes-2.0.0 (c (n "matrixes") (v "2.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0kszb2dd8bl5cnz5yir73dqgxh8g04i68sqirpwmnisx8cr2f5f0")))

(define-public crate-matrixes-2.0.1 (c (n "matrixes") (v "2.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lq80nk6v4d0xi0pv37gyvkf6i3i3qyidmg3wc8da9iq3l28x5yh")))

(define-public crate-matrixes-2.1.0 (c (n "matrixes") (v "2.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1hsfzbljka32xp5bv209cp7v5cglw6b3zxs212dyb8qjaxz5mmd4")))

(define-public crate-matrixes-2.2.0 (c (n "matrixes") (v "2.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08m11iprzs30mhmz3fmnlidvlk2vay62jd8lg7kdwpv7w66jzqvn")))

(define-public crate-matrixes-2.3.0 (c (n "matrixes") (v "2.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "109p77d97ncb4m1s6mmzyk9rh5irmgbgmvpl77p1qdq8hlann469")))

(define-public crate-matrixes-2.4.0 (c (n "matrixes") (v "2.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1iyppdkh0xxnydpny1fa31d8c6xbnz516fhgqp3g2c1shyfpc3ml")))

(define-public crate-matrixes-2.5.0 (c (n "matrixes") (v "2.5.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17j0cbnqzj10cs5cq93c2bx6pny3ava5qkspachdv0lv3bc7fbpv")))

