(define-module (crates-io ma tr matrixcompare-mock) #:use-module (crates-io))

(define-public crate-matrixcompare-mock-0.1.0 (c (n "matrixcompare-mock") (v "0.1.0") (d (list (d (n "matrixcompare-core") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 0)))) (h "1iqk7p9a3y057qzjjz4hfc1kq48vb76bf1lljqcm5r6xxmlz785y")))

