(define-module (crates-io ma tr matrix-bot) #:use-module (crates-io))

(define-public crate-matrix-bot-0.1.0 (c (n "matrix-bot") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "matrix-sdk") (r "^0.2.0") (f (quote ("encryption" "sqlite_cryptostore"))) (d #t) (k 0)) (d (n "matrix-sdk-test") (r "^0.2.0") (d #t) (k 2)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.17") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0hzna6rx0ipnrs8bzkdp7rmycm828c34qdni3gaflj098iv22nyg")))

