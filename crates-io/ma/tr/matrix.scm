(define-module (crates-io ma tr matrix) #:use-module (crates-io))

(define-public crate-matrix-0.0.1 (c (n "matrix") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "1grbdxxxplvz81nmxfdg33vf7kcg8bdb1vr1rw6a3l5q4zx2y925")))

(define-public crate-matrix-0.0.2 (c (n "matrix") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)) (d (n "blas") (r "^0.0.2") (d #t) (k 0)) (d (n "lapack") (r "^0.0.2") (d #t) (k 0)))) (h "01h98hk0v1kdyx25bxmqva91lrp9p2cfj93ykhh7v5b0phb7l9db")))

(define-public crate-matrix-0.0.3 (c (n "matrix") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "blas") (r "^0.0.3") (d #t) (k 0)) (d (n "lapack") (r "^0.0.3") (d #t) (k 0)))) (h "0285ggvaimi9aqsxrr52p7kvyhvcv1sglr4sqplvz68gd4pfm9cr")))

(define-public crate-matrix-0.0.4 (c (n "matrix") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "blas") (r "^0.0.4") (d #t) (k 0)) (d (n "lapack") (r "^0.0.4") (d #t) (k 0)))) (h "1b34km53qqsmlr8csbs2gpabvp7hfv6ymn8j71lb6c7zmjfqsysh")))

(define-public crate-matrix-0.0.5 (c (n "matrix") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "blas") (r "^0.0.5") (d #t) (k 0)) (d (n "lapack") (r "^0.0.5") (d #t) (k 0)))) (h "117l51j3blii496ahmwkv2iv811ybvb8bk924mwih06nkg4qm5s9")))

(define-public crate-matrix-0.0.6 (c (n "matrix") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "blas") (r "^0.0.5") (d #t) (k 0)) (d (n "lapack") (r "^0.0.5") (d #t) (k 0)))) (h "06j8lxdqs5dcssm832q37capgkbc460labm1k5imph4sqxzpdkfi")))

(define-public crate-matrix-0.0.7 (c (n "matrix") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)) (d (n "blas") (r "^0.0.6") (d #t) (k 0)) (d (n "lapack") (r "^0.0.6") (d #t) (k 0)))) (h "1xjhg679w2gy3ni21x4xr3wkcrw1794rbk2mgssak5b37n1q5ghx")))

(define-public crate-matrix-0.0.8 (c (n "matrix") (v "0.0.8") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "blas") (r "^0.0.8") (d #t) (k 0)) (d (n "lapack") (r "^0.0.8") (d #t) (k 0)))) (h "0l2g3xpv23y9ffib33xlpy0rw4wb6q11g6csjrbmqyi872mcpd7x")))

(define-public crate-matrix-0.0.9 (c (n "matrix") (v "0.0.9") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.0.9") (d #t) (k 0)) (d (n "lapack") (r "^0.0.9") (d #t) (k 0)))) (h "1bw8h4sy39rpsn4xabcqwxdngfx86c9fnay6w2ih56gz04pnjjdw")))

(define-public crate-matrix-0.0.11 (c (n "matrix") (v "0.0.11") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.0.11") (d #t) (k 0)) (d (n "lapack") (r "^0.0.11") (d #t) (k 0)))) (h "04ww0p1h85pfasyx6arpq81gpq9y74hgk8kh2bxnaq831agq68mr")))

(define-public crate-matrix-0.0.13 (c (n "matrix") (v "0.0.13") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.0.13") (d #t) (k 0)) (d (n "lapack") (r "^0.0.13") (d #t) (k 0)))) (h "1lqs5xvdg0pqxcvi3q3x3zciylxm0dq0py27i0na02sd697l8ycv")))

(define-public crate-matrix-0.0.14 (c (n "matrix") (v "0.0.14") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.0.14") (d #t) (k 0)) (d (n "lapack") (r "^0.0.14") (d #t) (k 0)))) (h "15c176al8x77wfhg64561j2drqw6gqjgvy2ma9sr4a5l3b9jhg7g")))

(define-public crate-matrix-0.0.15 (c (n "matrix") (v "0.0.15") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "1v8w5fc6zzl46nxpbfmgnsldmmm0q61qkb7gzmdnnd8vnwik7wys")))

(define-public crate-matrix-0.1.0 (c (n "matrix") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.3") (d #t) (k 0)) (d (n "lapack") (r "^0.3") (d #t) (k 0)))) (h "18hf5rk0hk5r585bgcc0qan0s864ry0frghk0aqhycxv37sibphi")))

(define-public crate-matrix-0.2.0 (c (n "matrix") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "1wbbairnl98r1vg3ikxlxx0xmbzlrf837pfhl0mw2jjrijgxgi0a")))

(define-public crate-matrix-0.2.1 (c (n "matrix") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "0wb4q6kacabl64542labfxnib012dv27gflam1vbx2r0lr0yi5ij")))

(define-public crate-matrix-0.2.2 (c (n "matrix") (v "0.2.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "160hpi1nkri2pn6j4av7xfs7nf0nv9hsksxsh3fn6rv5fnafkxqx")))

(define-public crate-matrix-0.3.0 (c (n "matrix") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "08almdlnfb465sdfixysbfz899ln9sap1hpm06ccxyplwhrcr81i")))

(define-public crate-matrix-0.4.0 (c (n "matrix") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1plkmss6hw23zqi39hsrn1praxmybbxvq1cz3p38p6rjqr65280i")))

(define-public crate-matrix-0.5.0 (c (n "matrix") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "01y1fddwi23bxib4mb7fvj6yrnala2r0pirgx2yplf95qqk13sjy")))

(define-public crate-matrix-0.5.1 (c (n "matrix") (v "0.5.1") (d (list (d (n "num") (r "*") (k 0)))) (h "17j9r34zgl5pmmimgap6xfvy9by5fr1jsgm1xa7a1n7vcm987m61")))

(define-public crate-matrix-0.6.0 (c (n "matrix") (v "0.6.0") (h "1wm7r97rq4fpsfx29hjlpx01fplx0qnqzrs4f0xjbi93d6w1r1zj")))

(define-public crate-matrix-0.6.1 (c (n "matrix") (v "0.6.1") (h "0y1kw1bar879vqhayvgmx18yzhfhb6bpqbl1xaq04hf11p12mi36")))

(define-public crate-matrix-0.6.2 (c (n "matrix") (v "0.6.2") (h "15zsbm9q8szrwcwrkr540939l66cpcxqslzjzs16diiga6157yhp")))

(define-public crate-matrix-0.7.0 (c (n "matrix") (v "0.7.0") (h "16dm7pz89ka5l3609dwfg81s19318bz65w2z8lf1wvs3gh9ir3ml")))

(define-public crate-matrix-0.8.0 (c (n "matrix") (v "0.8.0") (h "0vyd3ghz33q4ynrnl4l4gf9qmiiv4mpcizxbaryaf6c1w4q0bac4")))

(define-public crate-matrix-0.9.0 (c (n "matrix") (v "0.9.0") (h "1582yy4lsxq5lr20xxpiw3rc7khcf827xfj2g7nl4g2dmr6gj5p1")))

(define-public crate-matrix-0.10.0 (c (n "matrix") (v "0.10.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "05fi4p3wx0b0dm2r462zfbhsmlxasw85lm9lnf74qjwyxs078vri")))

(define-public crate-matrix-0.11.0 (c (n "matrix") (v "0.11.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0g1dj8570zn51c9yzng1szppv7fyaymi64p0ibrb7r1dkdv5k6d6")))

(define-public crate-matrix-0.11.1 (c (n "matrix") (v "0.11.1") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0343y7gdxxlrc0nyqrsw6i111l33g0d8xrw3ny89qm8mm4qkrnap")))

(define-public crate-matrix-0.11.2 (c (n "matrix") (v "0.11.2") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0gqb54wg8v8jgfa94a320fip65jzzl0jpb6q97qk2dgl1n79ysvy")))

(define-public crate-matrix-0.11.3 (c (n "matrix") (v "0.11.3") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "03ldg2mwgn5fmxwvb2fn3ci9blhqg2y7d91w9if8icg76kbcqyaf")))

(define-public crate-matrix-0.12.0 (c (n "matrix") (v "0.12.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1d9pak0dnldlxwy4wyyibvk8kh3q60zzpyb91y6y53b5p1dkfvik")))

(define-public crate-matrix-0.13.0 (c (n "matrix") (v "0.13.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "13dy0b3jkp07j71axjb0m6aliw8fpzrvz9fbv41nsvs0m15grhs3")))

(define-public crate-matrix-0.13.1 (c (n "matrix") (v "0.13.1") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "04fsg7h0rdb7j8b021rnjddmf50y4k9llpcznx1v04mbg8hyi8ap")))

(define-public crate-matrix-0.14.0 (c (n "matrix") (v "0.14.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "01yhv6z6qjsy5pabn3dgcrlbpnx54j55yz3mb61bw3rqasb6snxz")))

(define-public crate-matrix-0.14.1 (c (n "matrix") (v "0.14.1") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0696ka7bf7h1j1j55a517m7vfp3p77r9h482cbb38qz0wj4amfmn")))

(define-public crate-matrix-0.14.2 (c (n "matrix") (v "0.14.2") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0xb8dfcw12sg1mza2apbbm5wxrzw0q16g52ksi5699kph658ihh7")))

(define-public crate-matrix-0.15.0 (c (n "matrix") (v "0.15.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0zh55kpka77c48n34yqgj5jj5sy08jyd68xa9r9jacm06idw185m")))

(define-public crate-matrix-0.15.1 (c (n "matrix") (v "0.15.1") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0rjymn4pimssi3y43xjx8s3xv7b64m7rg4gqha41nqcs64a4xix7")))

(define-public crate-matrix-0.15.2 (c (n "matrix") (v "0.15.2") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0a89in9z6m2plvsh1fbdg84ha78kas410zvxh2faxki4s6j3lwsg")))

(define-public crate-matrix-0.15.3 (c (n "matrix") (v "0.15.3") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0ds17a1nx62rclv0993zr4x8kjv7mcadmgffc6gps911cjz4wllw")))

(define-public crate-matrix-0.16.0 (c (n "matrix") (v "0.16.0") (d (list (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0fvpnrzhs3d8fcnfhiryfr0mgrx6h1mirisxjavklpn9pw0fr40p")))

(define-public crate-matrix-0.17.0 (c (n "matrix") (v "0.17.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0hiv9p31w1h17a5q55y4zw7bnzhk9lr3q321risf1gxnqcfhsja1") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.1 (c (n "matrix") (v "0.17.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1y2ln6gw8i3swp0q5zfl8qc7dlldf9yizx8wpjwp03jvm8lsbyy5") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.2 (c (n "matrix") (v "0.17.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1q440p5ds5fqb8b75rc86jgmkbks19jw1n8mdz9589pwrvr8vlqv") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.3 (c (n "matrix") (v "0.17.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1pg161ldbry1ijgajdjv1azfd8zpdl5gxw0z6m01cw1hv4g59grd") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.4 (c (n "matrix") (v "0.17.4") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1b1kzy5429dhsdil9y8rx9166qp54gm7xa42zx6hvd9gqxs7l8bi") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.5 (c (n "matrix") (v "0.17.5") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0p2ql3v0brd28g3jkzc5shc7aia9wrnkhz62f5pmb1300dsdp689") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.6 (c (n "matrix") (v "0.17.6") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "13jq7xnxr49blgjslj3mj6xc01i8mdfyya8g34bbrizydjmz1kfp") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.7 (c (n "matrix") (v "0.17.7") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)))) (h "00g41xly6xml5blabjxjim9wl4xv0fjrfp4qnmpy4lyamkbv2mbw") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.8 (c (n "matrix") (v "0.17.8") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "01zhq5gqpbhbqd0356l1sr73mwnx52q0pg790ndkwyz8j3q0nxd9") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.9 (c (n "matrix") (v "0.17.9") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1nammax7p0m72va9da1w80pnkag9lmmvs9mgkwsif5dn593h2v96") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.10 (c (n "matrix") (v "0.17.10") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1a10c2vrhwkr32jibni82zm0i60gmrhnzj5nxddrsppl12rsnnfw") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.11 (c (n "matrix") (v "0.17.11") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "18b94lffrlpnfg550804ximdd5g4yrq49qwzvygkwm0zfszdym7r") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.17.12 (c (n "matrix") (v "0.17.12") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "0inv9ds8n5mhhkk62w4s6yc2gvw2acw9kl7jv8ln3clv8jddcgda") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.18.0 (c (n "matrix") (v "0.18.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "08s63p427clj34a6k3lrcc3b68b7pw68fw37pgis1mgky2gwjq01") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.18.1 (c (n "matrix") (v "0.18.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "0wjj4sy37dm25mx6xa91hii7452r2l7yyaaqmmvwjwgpxigzmwm7") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.18.2 (c (n "matrix") (v "0.18.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "0k7l0p9yz6zybdfs5k5lfzlvp5akq127rs37ajn9mszz8bdhf1sq") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.18.3 (c (n "matrix") (v "0.18.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1hz5hn974wr0l9nrjx9a644i0m48iypvwx82gx9g6hdmbhhn7i58") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.19.0 (c (n "matrix") (v "0.19.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "06zhscggcyg22db54lmwvcvz3mh7n2fi0ad8rp72srl9cmqa3cpm") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.19.1 (c (n "matrix") (v "0.19.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "0m9m87j1x9kyadf6mgx6fr40pgd3qwqg2d3rl3b49hg0xzbsrf2f") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.19.2 (c (n "matrix") (v "0.19.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "complex") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "18880lbw0v0fn5sk7fphhxvf8vi5py2mqcn7yxp6ikk51ycqi1gf") (f (quote (("default" "acceleration" "complex") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.20.0 (c (n "matrix") (v "0.20.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1c42mzp5nw9gh86gw6aj45waylw0s3hgk707345mc3704kn20mp0") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.0 (c (n "matrix") (v "0.21.0") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "blas") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "0zaj5rjb15bv6rk6r47fgwfcfchrjldn2a1qv0z6vwsyxbval9a9") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.1 (c (n "matrix") (v "0.21.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "1wkjg8mbiq0qgrb3hq7q2csffw5p8gfr07m417xk5gljahfigscs") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.2 (c (n "matrix") (v "0.21.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "1vzldb0gs635acxz76l0582a7fd7jda12zkd9whc4grw54x2mc26") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.3 (c (n "matrix") (v "0.21.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)))) (h "0hcdhsj528vjs3qa6w54ymjwqdifkp4s2w5xk7nfw2b5v7g0pzxr") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.4 (c (n "matrix") (v "0.21.4") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0l730kwgxp18rbimfxs9zf5zzmq17zpd7qzx1n0lddp1dy4l9g7d") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.5 (c (n "matrix") (v "0.21.5") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "171xwd8z4188sn5asg2i8p8l8j06qk0pd7hx8xfm973y5dlqsw7v") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.6 (c (n "matrix") (v "0.21.6") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0qiw8y29jkxfgddh9w0lkgydlm3wmfxyw98gzvjh4za1s8s1gmb9") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.7 (c (n "matrix") (v "0.21.7") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0564cs3klwx25s1makpcg0c96dgdnmxi7a1hg2x235p3zpfgmf99") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.8 (c (n "matrix") (v "0.21.8") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1wkidjfqghphnfsr7v5wwrdbdr47xiqgvc3wkyjv2j260la554zz") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.9 (c (n "matrix") (v "0.21.9") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0lw289yv7s5wxy4bqm9d3ambh7mnk42avds31z9889niqj4dyb8p") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.10 (c (n "matrix") (v "0.21.10") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "05r5v2g3y7zljwsnf5k7a0bglpwr4qg66sw2hmmfsbphdr018wa0") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack"))))))

(define-public crate-matrix-0.21.11 (c (n "matrix") (v "0.21.11") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "openblas-src") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1wcgm46hwk9g0djc20km5jzssn4v393qbsjnlrd9zjarams4vlm1") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack" "openblas-src"))))))

(define-public crate-matrix-0.21.12 (c (n "matrix") (v "0.21.12") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "openblas-src") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0s0792mlrrj7wzq9q38hzhhi4rk1q7slip7rva45y0mcngw7p9j4") (f (quote (("default" "acceleration") ("acceleration" "blas" "lapack" "openblas-src"))))))

(define-public crate-matrix-0.22.0 (c (n "matrix") (v "0.22.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "blas") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "lapack") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "openblas-src") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1g7gq24srxhf4bnaibvzl1id8sfic5i53n4y0prck9hbia5y2iwb") (f (quote (("acceleration-src" "openblas-src") ("acceleration" "blas" "lapack"))))))

