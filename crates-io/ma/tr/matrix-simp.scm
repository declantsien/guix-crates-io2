(define-module (crates-io ma tr matrix-simp) #:use-module (crates-io))

(define-public crate-matrix-simp-0.1.0 (c (n "matrix-simp") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "01m20i6mgdxhh00bxrim2j3dcvmg03jklgdvirms1w4w44fzbcl4")))

(define-public crate-matrix-simp-0.2.0 (c (n "matrix-simp") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1f8n19k42l7sl0kxlrrn5sn82vzbqgmpgmsgzzzfhs4msiqdhrxb")))

(define-public crate-matrix-simp-0.3.0 (c (n "matrix-simp") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0l78lg3rflmil397g99sf4xa7kkmach865d26928l14rb1ivcvf3")))

