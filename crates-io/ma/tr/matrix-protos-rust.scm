(define-module (crates-io ma tr matrix-protos-rust) #:use-module (crates-io))

(define-public crate-matrix-protos-rust-0.1.0 (c (n "matrix-protos-rust") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.25.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.25.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 1)) (d (n "zip") (r "^0.5.13") (d #t) (k 1)))) (h "09ns7bs9myw0mn4mgsymja0ka7177g9izyymraqg6c57g0alpj7q")))

