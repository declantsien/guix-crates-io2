(define-module (crates-io ma tr matrijs) #:use-module (crates-io))

(define-public crate-matrijs-0.1.0 (c (n "matrijs") (v "0.1.0") (h "1rglg7qiidk8ra0g2hzzzkndqp8akxxls2mx3i6hywi212k1q5l3")))

(define-public crate-matrijs-0.1.1 (c (n "matrijs") (v "0.1.1") (h "0jyaw0ixknxl6fngvnsmhbzkkwg9b2iqsfrfz6x1j1kgs1nswqg1")))

