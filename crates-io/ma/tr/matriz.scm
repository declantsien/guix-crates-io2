(define-module (crates-io ma tr matriz) #:use-module (crates-io))

(define-public crate-matriz-0.0.1 (c (n "matriz") (v "0.0.1") (h "0n4vfdkdj1a5h92xjfbwb50nsvcxllfvbxzfi9w2547mry2ssvkf")))

(define-public crate-matriz-0.0.2 (c (n "matriz") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 2)))) (h "0b1x04qzjsaw8k21f3knw2dsra3ndrmwgwjkyi5d4xd2njqh5p92") (f (quote (("std") ("full" "std") ("default" "std"))))))

