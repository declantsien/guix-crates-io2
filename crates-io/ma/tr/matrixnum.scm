(define-module (crates-io ma tr matrixnum) #:use-module (crates-io))

(define-public crate-matrixnum-0.1.0 (c (n "matrixnum") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1p1bgs9llr9rbzrmc85vadjqqqibjjss14lj8l69wiaxkzvhbwyr")))

