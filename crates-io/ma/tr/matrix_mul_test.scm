(define-module (crates-io ma tr matrix_mul_test) #:use-module (crates-io))

(define-public crate-matrix_mul_test-0.0.1 (c (n "matrix_mul_test") (v "0.0.1") (h "127yd1f1490vcfrrzp23wngi0mjmsfzk2gva17lklrj3wvi3ajfx")))

(define-public crate-matrix_mul_test-0.0.2 (c (n "matrix_mul_test") (v "0.0.2") (h "0ijkhwgacrrhbmf3fca8cnl717bm6hbl0ghgla6g5s0g07wwjfgk")))

(define-public crate-matrix_mul_test-0.0.3 (c (n "matrix_mul_test") (v "0.0.3") (h "1crwjxj0czx4y8rj042w8n64qxvyby88vkypivm50bb33ypg6l0r")))

(define-public crate-matrix_mul_test-0.0.4 (c (n "matrix_mul_test") (v "0.0.4") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "1npwf4vfmw7qkam48inaxbwlhbzzvvrx3mysiv99ka77dpq18yam") (f (quote (("blas" "ndarray/blas"))))))

(define-public crate-matrix_mul_test-0.0.5 (c (n "matrix_mul_test") (v "0.0.5") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("openblas"))) (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "system"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "05b91adrnpy2sillirvbli910lbrm78zgfw48pbmxq1mdg3411nn") (f (quote (("openblas" "ndarray/blas" "openblas-src" "blas-src") ("blas" "ndarray/blas"))))))

(define-public crate-matrix_mul_test-0.0.6 (c (n "matrix_mul_test") (v "0.0.6") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("openblas"))) (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "system"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "04k8vl9sg4qvc0hwfbmfdj7w23wclcgd3glzzyvl3cxcf7sdxy2a") (f (quote (("openblas" "ndarray/blas" "openblas-src" "blas-src") ("blas" "ndarray/blas"))))))

(define-public crate-matrix_mul_test-0.0.7 (c (n "matrix_mul_test") (v "0.0.7") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("openblas"))) (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "system"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "05cpl0x7z3pawgcrjss6i3zjxm3vjrllm41rknrw5av88z6d7kx8") (f (quote (("openblas" "ndarray/blas" "openblas-src" "blas-src") ("blas" "ndarray/blas"))))))

(define-public crate-matrix_mul_test-0.0.8 (c (n "matrix_mul_test") (v "0.0.8") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("openblas"))) (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "system"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "1k1kv7kq82xdw6dccz4f34j0ml2q5q0v4gbvjw1jr94bcj9j9nnv") (f (quote (("openblas" "ndarray/blas" "openblas-src" "blas-src") ("blas" "ndarray/blas"))))))

