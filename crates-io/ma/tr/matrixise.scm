(define-module (crates-io ma tr matrixise) #:use-module (crates-io))

(define-public crate-matrixise-0.1.0 (c (n "matrixise") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lszlm99izrynhnw332xbadk3pxq025iqh6mpja0y3mhlrvh6srp")))

(define-public crate-matrixise-0.2.0 (c (n "matrixise") (v "0.2.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1v302mcks2fw85hk942q9rk5qgypv1gikc3gq0fishjbq3kz0gqw")))

(define-public crate-matrixise-0.2.1 (c (n "matrixise") (v "0.2.1") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rd1fd0xq4bkr4280lw782i00nbdqrayj851lk1rsbv33w7fldix")))

(define-public crate-matrixise-0.2.2 (c (n "matrixise") (v "0.2.2") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1n0lbjng62lxgya0m8rah4fv0akrq3fwp254vf4wfn70d87ymzx2")))

(define-public crate-matrixise-1.0.0 (c (n "matrixise") (v "1.0.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0rhl7pnlf3j8ckpkz3lzpnv0lsmhlygkx3z154irvgbpz18w0c8s")))

(define-public crate-matrixise-1.0.1 (c (n "matrixise") (v "1.0.1") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14mmyldar56b54fplfs8mhk6pvmdzxqzgwcsacllwvxylhpsmf69")))

(define-public crate-matrixise-1.1.0 (c (n "matrixise") (v "1.1.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0234v6004r6mayyar90csl3lw3cwfkv1kj3fggl6d2zidkyvha55")))

(define-public crate-matrixise-2.0.0 (c (n "matrixise") (v "2.0.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pmqr59xw324yhlwlffc7y67cy15jip85qkwahhqpkhwd3i5rv8s")))

(define-public crate-matrixise-2.0.1 (c (n "matrixise") (v "2.0.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xfn4hapn2lq9a2xqmm9piy8v7hha9n8pg4jafpsl0c3bppdqqmy")))

