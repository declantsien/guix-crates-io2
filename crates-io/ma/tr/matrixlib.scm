(define-module (crates-io ma tr matrixlib) #:use-module (crates-io))

(define-public crate-matrixlib-0.1.0 (c (n "matrixlib") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1w9la6dzgm8d1inmkkwy8py77q0sn7s71mxs3khkwbp9dkbs40xw")))

(define-public crate-matrixlib-0.1.1 (c (n "matrixlib") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1yp5kx5sf0c1l6x3djql9y5pq2r1yvl28xc2nr8prvpl2nljqmvi")))

(define-public crate-matrixlib-0.1.2 (c (n "matrixlib") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1ih2pkjn9hb0nw4a0lffrc9z13hdz2b9x7h3f2bdb7q016pz7yh2")))

