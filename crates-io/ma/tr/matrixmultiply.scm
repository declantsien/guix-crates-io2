(define-module (crates-io ma tr matrixmultiply) #:use-module (crates-io))

(define-public crate-matrixmultiply-0.1.0 (c (n "matrixmultiply") (v "0.1.0") (h "1b2ykx6f7l83b9syz15j0bhdxiadafc48hzf0vhn1d63ih4nr5f6")))

(define-public crate-matrixmultiply-0.1.1 (c (n "matrixmultiply") (v "0.1.1") (h "0rs64cjv1s286gk1qrl3qvziavf29kj94na97kx84svis0hbma08")))

(define-public crate-matrixmultiply-0.1.2 (c (n "matrixmultiply") (v "0.1.2") (h "1bvf27skrcyjmf52zbhzy3zqqihw4mgwgvizhmyw60pyy04jxiip")))

(define-public crate-matrixmultiply-0.1.3 (c (n "matrixmultiply") (v "0.1.3") (h "1xfq7j698941q24kafhsn20bjclkvmls6zn6fddcf9gkrgb23dsl")))

(define-public crate-matrixmultiply-0.1.4 (c (n "matrixmultiply") (v "0.1.4") (h "1rdh7zlny81lgv6pzqsnifxyvz8c5a9yfmp9yywxgqk7fa1viria")))

(define-public crate-matrixmultiply-0.1.5 (c (n "matrixmultiply") (v "0.1.5") (h "0pfpbp7kmkmnrz7dvf7g5fbn4jd6d66s3x9mk7c0yx26m9ym9i48")))

(define-public crate-matrixmultiply-0.1.6 (c (n "matrixmultiply") (v "0.1.6") (h "17hgribap1m0inz33zyy6cly1zvngivmg2vxcnsf73c4x25hjc41")))

(define-public crate-matrixmultiply-0.1.7 (c (n "matrixmultiply") (v "0.1.7") (h "182kjl33nayxgapm12x6w3ya5gk7k08h6w6whxx6rzgglzgqhd1v")))

(define-public crate-matrixmultiply-0.1.8 (c (n "matrixmultiply") (v "0.1.8") (h "006i0czg8ggd5vz5fbyf8k6y65r3z368yy3mm4cg0njaazzhmcln")))

(define-public crate-matrixmultiply-0.1.9 (c (n "matrixmultiply") (v "0.1.9") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)))) (h "1wxwhgy3pdq5d75aimp562crvk8ixaaiii3g87szhs2d0knr1qyy")))

(define-public crate-matrixmultiply-0.1.10 (c (n "matrixmultiply") (v "0.1.10") (d (list (d (n "bencher") (r "^0.1.1") (d #t) (k 2)))) (h "088l7mva0g6h8rvlahrklfrsp99gz9fq2frcnifllnw17cmr70wh")))

(define-public crate-matrixmultiply-0.1.11 (c (n "matrixmultiply") (v "0.1.11") (d (list (d (n "bencher") (r "^0.1.1") (d #t) (k 2)))) (h "1a4f770xq9hfkfmsrd26g7v1c5l6vc2hgzilhgv4lc37gdmh52wl")))

(define-public crate-matrixmultiply-0.1.12 (c (n "matrixmultiply") (v "0.1.12") (d (list (d (n "bencher") (r "^0.1.1") (d #t) (k 2)))) (h "0a32sx5j0y6phs2bb9rwqh32axi211dj4jsr8binvw54bz4pv89q")))

(define-public crate-matrixmultiply-0.1.13 (c (n "matrixmultiply") (v "0.1.13") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "0n4ifzi2f3z15dq8caqyj1wi952ik9zalgi8fix2ciihqk915q3w")))

(define-public crate-matrixmultiply-0.1.14 (c (n "matrixmultiply") (v "0.1.14") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "1xqb4y9mnbjpgndsa0jc7xpdq8s2l40k22gahnpkcq1mmdpadhfa")))

(define-public crate-matrixmultiply-0.1.15 (c (n "matrixmultiply") (v "0.1.15") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "00p0fpjhm45qdzi37mgv7ggsy8b9gqvq4999yrbgyn1dxkf6gbfw")))

(define-public crate-matrixmultiply-0.2.0 (c (n "matrixmultiply") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "05bp15q1bhbxb10kwcpycl6lh0gznl3g682li347fjnxv3cmqra4")))

(define-public crate-matrixmultiply-0.2.1 (c (n "matrixmultiply") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "05kn0slry9sv1qmp6rsdvpjakz3z6y6wr4zi8kswqhsyx1fy8ph5")))

(define-public crate-matrixmultiply-0.2.2 (c (n "matrixmultiply") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.11") (d #t) (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "16sgc1j87hmsqmhlqpqgcpbrb00f267ikbr55fhxla8nhwnxgznw")))

(define-public crate-matrixmultiply-0.2.3 (c (n "matrixmultiply") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.11") (d #t) (k 2)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)))) (h "13s7nfd3dfcsrixld2lk8c563ih5xzczl2w36hprfc016rkfrxyl")))

(define-public crate-matrixmultiply-0.2.4 (c (n "matrixmultiply") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)))) (h "1hc4vp19x823xgkm374wsxnzmqbjhmyaj5nr0lhm9k9i02x0cs4i") (f (quote (("std") ("default" "std"))))))

(define-public crate-matrixmultiply-0.3.0 (c (n "matrixmultiply") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3") (o #t) (d #t) (k 0)))) (h "18f3ajqah1rlk6vxk50zqg86dbcbxmp5l7zz05mq7hiylfzbs00k") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std"))))))

(define-public crate-matrixmultiply-0.3.1 (c (n "matrixmultiply") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0hdpsvw28in1m9c5h8a7abzxv70rq8l5hg5h8k6sxpyrfsvib2js") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std"))))))

(define-public crate-matrixmultiply-0.3.2 (c (n "matrixmultiply") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "112dbbjfiw4yji8vf94hk9hlhd9sa67qr3v0vkzfcx2hsd6mvn5d") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.3 (c (n "matrixmultiply") (v "0.3.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1zp9s52k8ic9y3w6nhq5nxqpwyzrm4yg0wrn2gwin3i5msaw76dv") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.4 (c (n "matrixmultiply") (v "0.3.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1y6skx8ab878v4zyrr8mklhfcmam6bq1kdz7743pbvfrsvz48as3") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.5 (c (n "matrixmultiply") (v "0.3.5") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1gp87w024dzxngappnjy3mrqkycj4y32rbl4yb7q7gzfvj7wpw7h") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.6 (c (n "matrixmultiply") (v "0.3.6") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "00jqgqni0hdwavys2n2frg9fzf0c8sshwa1c328x3kvnkr68bkpw") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.7 (c (n "matrixmultiply") (v "0.3.7") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0xqyprn2rnj9m34fvhb4l0697cvlsjyn27y9q78w0pgr0kf2c089") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

(define-public crate-matrixmultiply-0.3.8 (c (n "matrixmultiply") (v "0.3.8") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thread-tree") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1whgrp8ph7904aslqx87h9qm0ks4pxdj2nysffmrhiys6v7w2x3m") (f (quote (("threading" "thread-tree" "std" "once_cell" "num_cpus") ("std") ("default" "std") ("constconf") ("cgemm"))))))

