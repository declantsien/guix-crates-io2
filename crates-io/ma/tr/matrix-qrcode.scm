(define-module (crates-io ma tr matrix-qrcode) #:use-module (crates-io))

(define-public crate-matrix-qrcode-0.1.0 (c (n "matrix-qrcode") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (k 0)) (d (n "rqrr") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "ruma-identifiers") (r "^0.19.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "06glyc3xx2qvq30202sxwp3bqp7cyhkpnjcmj3inybrxbcmh4b19") (f (quote (("docs" "decode_image") ("default" "decode_image") ("decode_image" "image" "rqrr" "qrcode/image" "qrcode/svg"))))))

(define-public crate-matrix-qrcode-0.2.0 (c (n "matrix-qrcode") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (k 0)) (d (n "rqrr") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "ruma-identifiers") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "13c3lydi0v9k6427wbzwhqm655mlaaf1gmq7bg2wd47zlad76ca2") (f (quote (("docs" "decode_image") ("default" "decode_image") ("decode_image" "image" "rqrr" "qrcode/image" "qrcode/svg"))))))

(define-public crate-matrix-qrcode-0.3.0 (c (n "matrix-qrcode") (v "0.3.0") (d (list (d (n "matrix-sdk-qrcode") (r "^0.3.0") (d #t) (k 0)))) (h "0a1fy0vaxf4y1b7himb0n8gawn6g7ifznz173phc5lcs9d360c6s") (f (quote (("default" "decode_image") ("decode_image" "matrix-sdk-qrcode/decode_image")))) (r "1.60")))

