(define-module (crates-io ma tr matrixable) #:use-module (crates-io))

(define-public crate-matrixable-0.1.0 (c (n "matrixable") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nj2rr0rxd410skz09jyg7096q2px0jf34c3n16cby9105bjfmik") (y #t)))

(define-public crate-matrixable-0.1.1 (c (n "matrixable") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gjq4m6f7pq0lqfc4nilj6smnii70rb1r779jlk9j8zc1hf7x4hm") (y #t)))

(define-public crate-matrixable-0.1.2 (c (n "matrixable") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dhf15l8hhs84d1v2ch6yiwz0pap77g1iqhhzr7sdwydyagj1lan")))

(define-public crate-matrixable-0.1.3 (c (n "matrixable") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a6cnz94v7755lr52jwmd83mrk8sxnmcbjqni42znpjg9zlpbg68")))

(define-public crate-matrixable-0.2.0 (c (n "matrixable") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ix3zh4ma0iw26yxqj022d72hsdl4gv3c3qxg82z8lbf421wlvyr")))

(define-public crate-matrixable-0.3.0 (c (n "matrixable") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06n46bfxzcvj76qyrfblry5iyjzll42argssc1xzh0qm5aagmb34")))

(define-public crate-matrixable-0.4.0 (c (n "matrixable") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vilmzjgg6bs4gl21jbi1kmzjmxs24r46halhpyjniyjgb4ms0by")))

(define-public crate-matrixable-0.5.0 (c (n "matrixable") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vj3dkd3qaijhdqyv62l2pplciwrmkmpm6bg8dl02vm7rl3s6fcx") (f (quote (("impls") ("default" "impls"))))))

(define-public crate-matrixable-0.6.0 (c (n "matrixable") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0klx9bf6b3bkgk61jr43j4jkza2d0w5q6js8wqc4hnsav8hbbmlv") (f (quote (("impls") ("default" "impls"))))))

