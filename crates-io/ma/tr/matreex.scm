(define-module (crates-io ma tr matreex) #:use-module (crates-io))

(define-public crate-matreex-0.1.0 (c (n "matreex") (v "0.1.0") (h "1ygjl19hfkpmz05n4w133h3qi61q3pks5ndj713pylsqbb5lby8z")))

(define-public crate-matreex-0.2.0 (c (n "matreex") (v "0.2.0") (h "1nb453r0zwxhl00sy7cwvhliw0z958qmzsgc2h19wimlbi5znyvc")))

(define-public crate-matreex-0.3.0 (c (n "matreex") (v "0.3.0") (h "1zwfc74j1ajbkbz3v80n3j8yvscg6ii72rw9gbraf6ql8432kgdz")))

(define-public crate-matreex-0.3.1 (c (n "matreex") (v "0.3.1") (h "1f8g93gahsmp2mvdl7nfm3p25kcx0pf7g3d8q8zkqzdm84aigb6p")))

(define-public crate-matreex-0.4.0 (c (n "matreex") (v "0.4.0") (h "1nxnh6af569824f89xri4hn6ry8h5jgr8sgra7sl5l0nk17amkk0")))

(define-public crate-matreex-0.4.1 (c (n "matreex") (v "0.4.1") (h "0j01w3qk7zn407wpvgmiy8ycf0mvd5crzlmsh8hmjsfi9g3vav8q")))

(define-public crate-matreex-0.5.0 (c (n "matreex") (v "0.5.0") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "1v9fhg5shm53mh388w2jhr5wqb5gpalh0zqdx8c8ix4pgmccf5ck") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.5.1 (c (n "matreex") (v "0.5.1") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "19blbwdhzkjg0hx6kz7g39jckgvj9xh01s4b69r0b471znkg2agj") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.6.0 (c (n "matreex") (v "0.6.0") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "08h3jivhn34qgzfj431vb2rggsisvpl26wz3ai5dh5afrs9daq8i") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.7.0 (c (n "matreex") (v "0.7.0") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "17w3py0p0p2aaf1g1vcrbq769d6k9d3cq1y61dx3bkilmyi7zccy") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.7.1 (c (n "matreex") (v "0.7.1") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "1npfvwpmhfgvp99g79r4b358d1b54yc719dv7bkwqqj217zazdin") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.7.2 (c (n "matreex") (v "0.7.2") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "0vw8ik2j80f06ghkwcj30qhm84whgzjgd8kyfrngl5icr0lrpiff") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-matreex-0.7.3 (c (n "matreex") (v "0.7.3") (d (list (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "1g9l5hrkg1c0jgj587gw92fdyyaf0pwvwxyxzh08ngnr4p4sb2xx") (f (quote (("default")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

