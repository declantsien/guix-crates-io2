(define-module (crates-io ma tr matrixlab) #:use-module (crates-io))

(define-public crate-matrixlab-0.1.0 (c (n "matrixlab") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1bjm770w6mp6ng35hvdr5hc2m1lh5xdhhd4ylczyflriwiblnpcs")))

(define-public crate-matrixlab-0.1.1 (c (n "matrixlab") (v "0.1.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1pigv5wbq1ni5a8qp4nb55jprfksmvybljjaplzgws8f179jb0xb")))

(define-public crate-matrixlab-0.1.2 (c (n "matrixlab") (v "0.1.2") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0h95wpb80vzfxxw4g7myzari8l0v27dzpa9wiq0qsd82p3mly6np")))

(define-public crate-matrixlab-0.1.3 (c (n "matrixlab") (v "0.1.3") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "07lyljjswxizyx9in4vzj1lysz8ipvvp3kzcml7gnp4yxrhwxg27")))

(define-public crate-matrixlab-0.1.4 (c (n "matrixlab") (v "0.1.4") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "05x2b8rlsjf23bhz23ycy7xjmrcynd42qsd3aa4f39piawkk6lis")))

(define-public crate-matrixlab-0.1.5 (c (n "matrixlab") (v "0.1.5") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1iy5nnbqll1j2kq3p1ashvalj4hkf0da3v7xsz7ydx9lran8lc04")))

