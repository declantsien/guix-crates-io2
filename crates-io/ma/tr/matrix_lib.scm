(define-module (crates-io ma tr matrix_lib) #:use-module (crates-io))

(define-public crate-matrix_lib-0.1.0 (c (n "matrix_lib") (v "0.1.0") (h "1b6q6vbgv4ic4rl8djkd0fkin7kqhzvrawlzdgc6r0n6p21wxi01") (y #t)))

(define-public crate-matrix_lib-0.1.1 (c (n "matrix_lib") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1c6r3dm4c8bpni0ab3l7kzx48pfkzv6k97mxrnc93693zq80n1y0") (y #t)))

(define-public crate-matrix_lib-0.2.0 (c (n "matrix_lib") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1bvl0bm50xksmy1yqpzdfl35nlfqgql1scc87s2529524ylqad3g") (y #t)))

(define-public crate-matrix_lib-0.2.1 (c (n "matrix_lib") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0j7ihx5cbsqjhx81vp9fw37b1hkmraknwg4h5nmgil8viwci0fdb") (y #t)))

(define-public crate-matrix_lib-0.2.2 (c (n "matrix_lib") (v "0.2.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0xngaj2r9q8vb450nslip3fkykhnyfcvqkbjfxkry9gfsdgchmqi") (y #t)))

(define-public crate-matrix_lib-0.3.0 (c (n "matrix_lib") (v "0.3.0") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yj8mn351camd7w2fimbjid2x9aqiqr3zqnj7avywb3sx1p46skl") (y #t)))

(define-public crate-matrix_lib-0.4.0 (c (n "matrix_lib") (v "0.4.0") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bljazs6l5f0grqg5bfbznsjczrak26ndv6rs394b27cwn5lp3ar") (y #t)))

(define-public crate-matrix_lib-0.4.1 (c (n "matrix_lib") (v "0.4.1") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10g5m5jvykrrva6hm01a5xckb0vfw39p96sndgkib599h9g129zw") (y #t)))

(define-public crate-matrix_lib-0.4.2 (c (n "matrix_lib") (v "0.4.2") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4s59zw1k2qql4jy6vx5afvixl7b4rnbyqlgb1sqj3x1sxsy540") (y #t)))

(define-public crate-matrix_lib-0.4.3 (c (n "matrix_lib") (v "0.4.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09lcq26jg8aa5d3y44vjvp67c6hf4ssami7kcd5kmr265xi2fzbg") (y #t)))

(define-public crate-matrix_lib-0.5.0 (c (n "matrix_lib") (v "0.5.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xjiq5c98d5c899fphxh173mp7vcbq8r0h5vxj1hjcn2y3zkslp5") (y #t)))

(define-public crate-matrix_lib-0.5.1 (c (n "matrix_lib") (v "0.5.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "14adr3bwgq6s2i5byh9cqda05k75bwq2savn3mq6sh4hlsg9b5kh") (y #t)))

(define-public crate-matrix_lib-0.5.2 (c (n "matrix_lib") (v "0.5.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1576pi147l3dqzcx24br4hzqqxbv0mi6rljv7cl24jbxix3mr6d5")))

(define-public crate-matrix_lib-0.6.0 (c (n "matrix_lib") (v "0.6.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "059dlh3h2aw6d9bjv6xi63l3w5sl0vkkbzcdipv9cv96wsi5i9g8") (y #t)))

(define-public crate-matrix_lib-0.6.1 (c (n "matrix_lib") (v "0.6.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxjn3qsnvxm3pbwxcbc6aalmn4bc6anppw5fzrfv8fgimc6irax")))

