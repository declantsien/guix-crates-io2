(define-module (crates-io ma sk masked) #:use-module (crates-io))

(define-public crate-masked-0.1.0 (c (n "masked") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1xvjaz5l0i4fsnrx2537n7qsgpg105rw5dyhkwm4xa24hgr9z8fk")))

(define-public crate-masked-0.1.1 (c (n "masked") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0fd0yllxdd385rvqzi1rrkcjmzv16hb2vd0pnpwmidrgvmbp57ry")))

(define-public crate-masked-0.1.2 (c (n "masked") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1iwhl1ai6d8qpw84lwbdjj2nwryv8awdp75wnafmf6bvvw42f5rr")))

(define-public crate-masked-0.1.3 (c (n "masked") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d55zmxni6anrscmly0hakp19mzav59d14024xfpl1vy565ndp42")))

(define-public crate-masked-0.1.4 (c (n "masked") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0xy4kh81azm3lqpiv4s4alfznpnlmgj6w9ar1xkna388zpk498an")))

