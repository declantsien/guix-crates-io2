(define-module (crates-io ma sk mask-parser) #:use-module (crates-io))

(define-public crate-mask-parser-0.1.0 (c (n "mask-parser") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d8fha4y3yv98f24hxf474pk5q3rwazirqz6fmcnkqd8pkcwgjdn")))

(define-public crate-mask-parser-0.2.0 (c (n "mask-parser") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mlzg07wn0k72nl9m51gra7n0rlg6f2pp9ll2bd3f7wi0sjiyqap")))

(define-public crate-mask-parser-0.2.1 (c (n "mask-parser") (v "0.2.1") (d (list (d (n "pulldown-cmark") (r "^0.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nlryma1yl8hvb851a7yifkdy3gl2g9ar59nnzzxfkqniwq6bd62")))

