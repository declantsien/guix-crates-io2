(define-module (crates-io ma sk maskedvbyte-rs) #:use-module (crates-io))

(define-public crate-maskedvbyte-rs-0.1.0 (c (n "maskedvbyte-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0zjf1g0vn1rvp4d7q2154cwcbk4cbra1dcvkzkbir2bzmllqym15")))

(define-public crate-maskedvbyte-rs-0.1.1 (c (n "maskedvbyte-rs") (v "0.1.1") (d (list (d (n "maskedvbyte-sys") (r "^0.1.0") (d #t) (k 0)))) (h "12si6fd3jgb9ga195ncrin2h5cmafjs7dzsy1j6liva2s20f3x2r")))

