(define-module (crates-io ma sk maskerad_memory_allocators) #:use-module (crates-io))

(define-public crate-maskerad_memory_allocators-2.0.0 (c (n "maskerad_memory_allocators") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "0yf7ysxg4c1ci6f5z09zk5j6km9i7vmlwhncg7rf9l27d305f4ab")))

(define-public crate-maskerad_memory_allocators-2.0.1 (c (n "maskerad_memory_allocators") (v "2.0.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "06iqy91lka3q6c3rczb051fq81b4lrw1cx01yqbj5lym9rhxcd1h")))

(define-public crate-maskerad_memory_allocators-3.0.0 (c (n "maskerad_memory_allocators") (v "3.0.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "139h5jrmmddkjq01fjmmdh5p3d73rddmh9pdb76zp4qwrz4bgg68")))

(define-public crate-maskerad_memory_allocators-3.0.1 (c (n "maskerad_memory_allocators") (v "3.0.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "0sydkvjphxx224kahf1xgvmb9jri5m6r57wrlk4aa5yi56qw83vg")))

(define-public crate-maskerad_memory_allocators-3.1.0 (c (n "maskerad_memory_allocators") (v "3.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "1swrl3lghxdzk8sd46myjkqd55k3zmrqzbvhqmawa2y00i3s6sv8")))

(define-public crate-maskerad_memory_allocators-3.1.1 (c (n "maskerad_memory_allocators") (v "3.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "0x7n0nvsnyrk3zp008r6gf1is32jm5dbndkw3svqm2vvd4k9f7rv")))

(define-public crate-maskerad_memory_allocators-3.1.2 (c (n "maskerad_memory_allocators") (v "3.1.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)))) (h "1sgazbp92bawiqxq6392dkm6m30mqc4bxw28rgf6cfqapxs8kbxr")))

(define-public crate-maskerad_memory_allocators-4.0.0 (c (n "maskerad_memory_allocators") (v "4.0.0") (h "06pjx227kmx6hxsjdlfgma6n4n4kmwgyvhdl74j8hrp8z155zsrj")))

(define-public crate-maskerad_memory_allocators-4.0.1 (c (n "maskerad_memory_allocators") (v "4.0.1") (h "125scvqmz7gffns7vp7nc9q8y72rcmcqb63n9wdd44i5dk2f551r")))

(define-public crate-maskerad_memory_allocators-4.0.2 (c (n "maskerad_memory_allocators") (v "4.0.2") (h "1r77rb38lwcqxksv5ksv27528awjf0vfw118cqib7z6ma0xkyy1p")))

(define-public crate-maskerad_memory_allocators-5.0.0 (c (n "maskerad_memory_allocators") (v "5.0.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cw6i98jkjh2qn9idfpsyk68gh8hd1qqjm3d6wni5119z60kb7mz")))

(define-public crate-maskerad_memory_allocators-5.0.1 (c (n "maskerad_memory_allocators") (v "5.0.1") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vl8fffdva5kxiv247zp47x6z4ksjdcwfpln6mivyjl94f2r48m7")))

(define-public crate-maskerad_memory_allocators-5.1.0 (c (n "maskerad_memory_allocators") (v "5.1.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1409y7a0as7bj2cjqa34ql5lby1irra9j4g44qy9gjqbx221362n")))

(define-public crate-maskerad_memory_allocators-5.2.0 (c (n "maskerad_memory_allocators") (v "5.2.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y9hc8sjza75zghlzzkh28ai56iq7avxbdph7wawc7xhv6rbycj0")))

