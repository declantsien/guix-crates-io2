(define-module (crates-io ma sk maskerad_stack_allocator) #:use-module (crates-io))

(define-public crate-maskerad_stack_allocator-0.1.0 (c (n "maskerad_stack_allocator") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "03icyiw2sf1a74jhylyi071wgbmmnc8kkf9p466phvvfsiqs0sh7")))

(define-public crate-maskerad_stack_allocator-0.1.1 (c (n "maskerad_stack_allocator") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "0j09qs202rh46hwnxq1vwwi4mrqj84aikd1fb8j7h0059bvbzk9d")))

(define-public crate-maskerad_stack_allocator-0.1.2 (c (n "maskerad_stack_allocator") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "0z60mj0sxm31lsab3g3wy56hy2nwq8j3cz34zhi4bq9q8d1cd07v")))

(define-public crate-maskerad_stack_allocator-1.0.0 (c (n "maskerad_stack_allocator") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "0lh1c3dsgah8j7c4njzljln5j3h59d0i9jciirh4r2fkdjk2dvqs")))

(define-public crate-maskerad_stack_allocator-1.0.1 (c (n "maskerad_stack_allocator") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "1q2iqz4pjrdm5k85swr009d2wm5ly21146pia15aijzjicyj42fr")))

(define-public crate-maskerad_stack_allocator-1.1.0 (c (n "maskerad_stack_allocator") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "0rp6xh3j2s7k6dc52sb5rxl8ymc9zx0wrnva3h9vqk542a7jg8j9")))

(define-public crate-maskerad_stack_allocator-1.1.1 (c (n "maskerad_stack_allocator") (v "1.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "19dg67rbxah7q10b05qa68d7mdndrmpv1lfmp4chkf8vcafq9dpz")))

(define-public crate-maskerad_stack_allocator-1.1.2 (c (n "maskerad_stack_allocator") (v "1.1.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "114na0xfkhl4al66i38g219ihg55bgysdk1lhmc4bdgpj2f0hbaf")))

