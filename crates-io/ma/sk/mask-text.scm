(define-module (crates-io ma sk mask-text) #:use-module (crates-io))

(define-public crate-mask-text-0.1.0 (c (n "mask-text") (v "0.1.0") (d (list (d (n "insta") (r "^1.17.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "13w5r4c79bkmyqzhfgvk241wpr34lfki99s8q1rvxyar39njplm2")))

(define-public crate-mask-text-0.1.1 (c (n "mask-text") (v "0.1.1") (d (list (d (n "insta") (r "^1.17.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "07zm08082hgy3h0fggqg2422ymglx3d9pdbqm27allc1hbdfk7v2")))

(define-public crate-mask-text-0.1.2 (c (n "mask-text") (v "0.1.2") (d (list (d (n "insta") (r "^1.17.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "1ghpmlcri2bfch3k6v4wql5bhvg9h0jrx1hmszskk1k105jrgw7x")))

