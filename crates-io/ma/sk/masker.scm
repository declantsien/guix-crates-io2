(define-module (crates-io ma sk masker) #:use-module (crates-io))

(define-public crate-masker-0.0.1 (c (n "masker") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xjvm0cs4kgngjh2dccmj3fc7dfffhm2gwpslqhnxyasnr3p43q4")))

(define-public crate-masker-0.0.2 (c (n "masker") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hihq0wvjdhg564i99b7vyhgpp1laz3n9fdy56s4i74cxzpkr6my")))

(define-public crate-masker-0.0.3 (c (n "masker") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0b3cg9ndl5z0h17i2a4yaaqzbymb1n75fy9mcancisc1fk8rli2c")))

(define-public crate-masker-0.0.4 (c (n "masker") (v "0.0.4") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "07w8h7kym616ip6h4sawcgq53dqgabi78acjj8gjyla47k5k1pjf") (f (quote (("streams" "bytes" "futures"))))))

