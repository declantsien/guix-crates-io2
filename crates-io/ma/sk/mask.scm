(define-module (crates-io ma sk mask) #:use-module (crates-io))

(define-public crate-mask-0.1.0 (c (n "mask") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "0yfy75w4fkfxh5m3dx6knsdjkyhsb3b6jdcdjhgxrxf9v0i01kiq")))

(define-public crate-mask-0.2.0 (c (n "mask") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1w4qb03z3nysklljdmy4ywvs4hw6bydh13pvlh5h4m0x5nmciv89")))

(define-public crate-mask-0.2.1 (c (n "mask") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "17cirqvqpvl5w5nhrwsnysd59wmhia758a7n01hngibr9zffgjxn")))

(define-public crate-mask-0.3.0 (c (n "mask") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "16kp476jjkk70gv73c47w5fcdfw5y7i85mwq67agpihllxq03max")))

(define-public crate-mask-0.3.1 (c (n "mask") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1r6788yh6b1dk33rxhg3xpyiw60wfdhsmn6i38cj16a1c9a00d35")))

(define-public crate-mask-0.4.0 (c (n "mask") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1r99nml3km4gjd8rsd43d4i8ffw0pvn7mv7rnsdri55kzs6kh09l")))

(define-public crate-mask-0.5.0 (c (n "mask") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "17ha3mmf8mk7sqyc3s0yv563pnhv8kz2gwfi3bchfkmb8x6kgqry")))

(define-public crate-mask-0.5.1 (c (n "mask") (v "0.5.1") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1bzgk207r83fs8k2583b5k8c4mcnrzhs7yy4k16w36107hpyzvw8")))

(define-public crate-mask-0.5.2 (c (n "mask") (v "0.5.2") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "15k11p984s21y013in8mhzandj9rqfp23618db5r5dy0basxxf8d")))

(define-public crate-mask-0.6.0 (c (n "mask") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "0lvpg0waqayx3c6x83l9184xfb3nj3ricgqz9997yq0460a29b8k")))

(define-public crate-mask-0.7.0 (c (n "mask") (v "0.7.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1mwcn90sfpb0xn9h4qfx3f9jm59nhy6jd28xbjfq2i3r959vsjvp")))

(define-public crate-mask-0.7.1 (c (n "mask") (v "0.7.1") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "0swzk506nxkfja0g85w1z3rnf8cpfigamk1rx9w12rzzs66xiidx")))

(define-public crate-mask-0.8.0 (c (n "mask") (v "0.8.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1wrv649ibnpxzf6iw58304pfn4zbcgw0f3z5d1x23v5g29dknrnd")))

(define-public crate-mask-0.9.0 (c (n "mask") (v "0.9.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "1rbzsqai6b23vplvfsxz4p12v35lvsrkmrz0qawdf38878chivw9")))

(define-public crate-mask-0.10.0 (c (n "mask") (v "0.10.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5") (k 0)))) (h "10vc7ill5z9asdxlwvfvvm556639vm1l651j92qmpa3a4z3rdyj9")))

(define-public crate-mask-0.11.0 (c (n "mask") (v "0.11.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mask-parser") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fp3yhrss7amy45nqih8flfwmkys2mfvv8cv6wz8wdc0j23c98g9")))

(define-public crate-mask-0.11.1 (c (n "mask") (v "0.11.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mask-parser") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b787f2skqg2ndn4f0mmiaiqzh8l4816mpgli47wvv8d87qi5p1n")))

(define-public crate-mask-0.11.2 (c (n "mask") (v "0.11.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mask-parser") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14jwbpzxprjwv44r3cbaxvf2vi79aar5bg9rsdnqpqckmy8wpxln")))

(define-public crate-mask-0.11.3 (c (n "mask") (v "0.11.3") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mask-parser") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vykrajvpk2bfrlvkkhvz21rspjin4jv96857fqxjn13klm0cxmm")))

(define-public crate-mask-0.11.4 (c (n "mask") (v "0.11.4") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mask-parser") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11j0w1p4wgclqsn5xlmg5zfksq7rav8vjnix72zmdigdgy16b19a")))

