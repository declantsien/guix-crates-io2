(define-module (crates-io ma sk maskerad_object_pool) #:use-module (crates-io))

(define-public crate-maskerad_object_pool-0.1.0 (c (n "maskerad_object_pool") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "1a0il5m7jg87k36fklffy42r5612y3mr6jf3vlvjg05d9fpnls8c")))

(define-public crate-maskerad_object_pool-0.1.1 (c (n "maskerad_object_pool") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "1chzwx4rvg46wx8mfnc6hpy1svvfb0hicj7nzczkca2mys6q2ak6")))

(define-public crate-maskerad_object_pool-0.1.2 (c (n "maskerad_object_pool") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 2)))) (h "0rws9wbh9dj9xv211bryxi9d8dv2c45yj9rm55smfczlwkg3clq0")))

(define-public crate-maskerad_object_pool-0.2.0 (c (n "maskerad_object_pool") (v "0.2.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rdiigbmb7xbid33f7m4vd2fvd6dfcslhk9y6q56lkpcy3kwp17m")))

(define-public crate-maskerad_object_pool-0.2.1 (c (n "maskerad_object_pool") (v "0.2.1") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gn97i99crg3vavdg3k5rxjnr9qc6plg1iy4kxmjv838x9b3hrfz")))

(define-public crate-maskerad_object_pool-0.2.2 (c (n "maskerad_object_pool") (v "0.2.2") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m2wrjm140ghc1dlq3shzlgxylmplvwyjxqfcvmgbxzq8hx0r7yw")))

(define-public crate-maskerad_object_pool-0.3.0 (c (n "maskerad_object_pool") (v "0.3.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v5i8fvkwwfkf190xiva2jjb92q134caq0cb4inc12lb4zr9lyxy")))

