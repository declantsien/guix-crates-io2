(define-module (crates-io ma o_ mao_core) #:use-module (crates-io))

(define-public crate-mao_core-0.1.0 (c (n "mao_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "164i3k6ci33ni51bcmhy30cl11c8g32r03zl1rq8k79v5dfgi382")))

