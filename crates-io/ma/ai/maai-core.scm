(define-module (crates-io ma ai maai-core) #:use-module (crates-io))

(define-public crate-maai-core-0.0.2 (c (n "maai-core") (v "0.0.2") (h "1i8x5ndrrjgynkahwanj2sdr3qppzrs7w15lzkixdf44hp7b3ykg") (y #t)))

(define-public crate-maai-core-0.0.3 (c (n "maai-core") (v "0.0.3") (h "186y0jlis090vs4yzb551iidbv0vjpvxqxd3ca4652hdjx6hdrls") (y #t)))

(define-public crate-maai-core-0.0.31 (c (n "maai-core") (v "0.0.31") (h "1fci1jmnb5s07acdi6hj5yxmnh25lijsvisyc098jhysx6kw6w1h") (y #t)))

