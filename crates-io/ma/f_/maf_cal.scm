(define-module (crates-io ma f_ maf_cal) #:use-module (crates-io))

(define-public crate-maf_cal-0.1.0 (c (n "maf_cal") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "10llhdhq3bqpa3z6lq08bzx1avbx1fj407bjsxkcdqj1j4axjmsg")))

(define-public crate-maf_cal-0.1.1 (c (n "maf_cal") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "15h3v3306ga76sylkfql2jrp0bdr88492faww683hlnpb01fap5x")))

(define-public crate-maf_cal-0.1.2 (c (n "maf_cal") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "04vlyva0sm7469irm5i2w8h0yi5d16x721cldgvs6530b08kfz8y")))

