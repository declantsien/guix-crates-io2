(define-module (crates-io ma me mamenoki) #:use-module (crates-io))

(define-public crate-mamenoki-0.1.0 (c (n "mamenoki") (v "0.1.0") (d (list (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r ">=1.0.158") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1i53cjl9nzwqzwq800sz4rbzvglipvzi45h34kx2bmmc4q9f310g")))

