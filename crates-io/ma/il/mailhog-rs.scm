(define-module (crates-io ma il mailhog-rs) #:use-module (crates-io))

(define-public crate-mailhog-rs-0.1.0 (c (n "mailhog-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "lettre") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "testcontainers") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0zdv8sz0pz0wqvfziz7s0mj3j8kw2s56nd9j4r1xv6cpml0jqzpn")))

(define-public crate-mailhog-rs-0.2.0 (c (n "mailhog-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "lettre") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "testcontainers") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "1rdl2lh0rzi69d288fr43lraix6apskpy7avym4swzpahpx81r98")))

