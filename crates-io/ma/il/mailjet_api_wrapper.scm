(define-module (crates-io ma il mailjet_api_wrapper) #:use-module (crates-io))

(define-public crate-mailjet_api_wrapper-0.1.0 (c (n "mailjet_api_wrapper") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "192p2whlx26nh08hc05hr33khpjhzm70bb2nfaiy5n28wqvhafly")))

(define-public crate-mailjet_api_wrapper-0.2.0 (c (n "mailjet_api_wrapper") (v "0.2.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "url-builder") (r "^0.1.1") (d #t) (k 0)))) (h "18hgf9ck9bbxq3zjam15xs4xz3mrbpjq9xarrh30pd2gnj43x52w")))

(define-public crate-mailjet_api_wrapper-0.3.0 (c (n "mailjet_api_wrapper") (v "0.3.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "url-builder") (r "^0.1.1") (d #t) (k 0)))) (h "1vfk19ybdvqix5wg9a22xj765s8p6w3516hbndg9lp8iq19ighf3")))

(define-public crate-mailjet_api_wrapper-0.3.1 (c (n "mailjet_api_wrapper") (v "0.3.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "url-builder") (r "^0.1.1") (d #t) (k 0)))) (h "1lnm6vyc8s4k7k5x3y4yal6grwslj0ww09xxb0aaygbry9lha9bb")))

(define-public crate-mailjet_api_wrapper-0.4.0 (c (n "mailjet_api_wrapper") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "url-builder") (r "^0.1.1") (d #t) (k 0)))) (h "0gjc4d16sqc8yk267qkh5shf4mz6kw8sdqrblq4f3dxfz9h6px4j") (s 2) (e (quote (("log" "dep:log"))))))

