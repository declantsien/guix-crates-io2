(define-module (crates-io ma il mail-test-account) #:use-module (crates-io))

(define-public crate-mail-test-account-0.1.1 (c (n "mail-test-account") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "1prv9fwzcp8j7mx2isgvl17i131nql1dif93752gpi1wxi4piaz9")))

