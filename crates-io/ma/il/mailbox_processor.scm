(define-module (crates-io ma il mailbox_processor) #:use-module (crates-io))

(define-public crate-mailbox_processor-0.1.0 (c (n "mailbox_processor") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0zwbc6qa1ykhcnypnrc2hamnkkh3sjyi9a64cnrvql0y09ihm62j")))

(define-public crate-mailbox_processor-0.1.1 (c (n "mailbox_processor") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)))) (h "0cw0fa8i00djad0f34inmiyd2vz9mrpx2agbp04yr033rmd8c5lw")))

(define-public crate-mailbox_processor-0.1.2 (c (n "mailbox_processor") (v "0.1.2") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)))) (h "1lpzwaiskkj5wgpgvjfrzawc8f5zsqxzwghsynws39885zclf9xq")))

(define-public crate-mailbox_processor-0.1.3 (c (n "mailbox_processor") (v "0.1.3") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)))) (h "1s6nyyaqzwghz5khcrfwr8865xixn9zx4b3aybpqh0hcyy4vg2ih")))

(define-public crate-mailbox_processor-0.1.4 (c (n "mailbox_processor") (v "0.1.4") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)))) (h "08da8jqaawz5ya7m9n94ivxvarl54spici5pkxk8mkd5x3pjnlli")))

(define-public crate-mailbox_processor-0.1.5 (c (n "mailbox_processor") (v "0.1.5") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1al4ky9mbnq1w1fm9mraxrvgbfgiwnixx9als30ysim12ici18qk")))

(define-public crate-mailbox_processor-0.1.6 (c (n "mailbox_processor") (v "0.1.6") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yf19qrvyd2fjjfzbj3hamiss1akr3cawqzdqqpmf87093a24l67")))

