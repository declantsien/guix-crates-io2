(define-module (crates-io ma il maildir-pack) #:use-module (crates-io))

(define-public crate-maildir-pack-0.1.0 (c (n "maildir-pack") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "leak") (r "^0.1.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)) (d (n "xz2") (r "^0.1.4") (d #t) (k 0)))) (h "1br5ih7q01zwmsnsx08pxv5jc22mxjxhglm3wb166blhph8rp6ni")))

