(define-module (crates-io ma il mailgun-sdk) #:use-module (crates-io))

(define-public crate-mailgun-sdk-0.1.0 (c (n "mailgun-sdk") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (d #t) (k 0)))) (h "0c4s2wq6qzi6cwnc59i2ycsy6dfzby4p93nccdx0dkzgq05s5jbs")))

(define-public crate-mailgun-sdk-0.1.1 (c (n "mailgun-sdk") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.3") (d #t) (k 0)))) (h "0fwsx565iib1vgh8nr4yhmwb9nbc3yf3j543mgx9g5vjcg565w74")))

