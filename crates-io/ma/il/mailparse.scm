(define-module (crates-io ma il mailparse) #:use-module (crates-io))

(define-public crate-mailparse-0.1.0 (c (n "mailparse") (v "0.1.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "0irigm7dfz2cml45mrrmv0hrw2p6n084alhvr6qpl01ik4cjqd18")))

(define-public crate-mailparse-0.2.0 (c (n "mailparse") (v "0.2.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "0h82qj5smysknfs210yx8fv94df82wy5ijs2pl6qd65ic4lf73kj")))

(define-public crate-mailparse-0.3.0 (c (n "mailparse") (v "0.3.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "1sxaradmf5kydb0v4f5i7563b1p69qpdhygsq1fffr43zij20fx3")))

(define-public crate-mailparse-0.4.0 (c (n "mailparse") (v "0.4.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "1aa7hah05hz348mz3qhjn393c46kf56nzqjcv1023m88zrq17av4")))

(define-public crate-mailparse-0.4.1 (c (n "mailparse") (v "0.4.1") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "0g63cn1l0wwbimhx2c3z2chwh0gk148p19yzqqgn2f3m7wmx16q7")))

(define-public crate-mailparse-0.4.2 (c (n "mailparse") (v "0.4.2") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "0rcb726a8yn24jxhwd86slw76w0ssbc5qk9i9n6qdcw0mgw3mfm9")))

(define-public crate-mailparse-0.5.0 (c (n "mailparse") (v "0.5.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "0r2vgyj90ra2cq9khfmfwny054m37ip9d3xv5d50zishgfz3a1mg")))

(define-public crate-mailparse-0.5.1 (c (n "mailparse") (v "0.5.1") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "17rkcwpxw924aap2arx4k3y2d2w1wmiqgy6dvk9bndx0061fjyji")))

(define-public crate-mailparse-0.6.0 (c (n "mailparse") (v "0.6.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.3.0") (d #t) (k 0)))) (h "1ryf7fk9i8m28vbpbbbz6jg3664fmzz14vs8s39w3q9kmg3a1y40")))

(define-public crate-mailparse-0.6.1 (c (n "mailparse") (v "0.6.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.0") (d #t) (k 0)))) (h "00vjh7qlxgg8gglhyw8ms8fiqj6ckznfadxadxcaghhza9dl9xgy")))

(define-public crate-mailparse-0.6.2 (c (n "mailparse") (v "0.6.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.0") (d #t) (k 0)))) (h "11an8vqfsldcysdrkklrkzvqcvswgz0lx0j5n84cbwrx6iqymmfi")))

(define-public crate-mailparse-0.6.3 (c (n "mailparse") (v "0.6.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.0") (d #t) (k 0)))) (h "0jm0b43m4h2848476paxdg2nmzz7jx0mq2vypd10dgciais0jhyl")))

(define-public crate-mailparse-0.6.4 (c (n "mailparse") (v "0.6.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.0") (d #t) (k 0)))) (h "0l0g87mhlxzi4mv0fz51g8i07r1r48yrhpg8h90qg40mnn41i7bp")))

(define-public crate-mailparse-0.6.5 (c (n "mailparse") (v "0.6.5") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.0") (d #t) (k 0)))) (h "1qszid44wvaifwr2m37xlj6b1nh457m9jcvn12wpn5pvmrxazh4r")))

(define-public crate-mailparse-0.7.0 (c (n "mailparse") (v "0.7.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1h20icqi71va0w5wbpm4dyp9j3cdkxf6b3iz9vzibzz2hys6b02l")))

(define-public crate-mailparse-0.8.0 (c (n "mailparse") (v "0.8.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1dnhx03g9ivsjm0mn0sx61fpjjkrklcd2npy2ngmhza7z7328fa1")))

(define-public crate-mailparse-0.8.1 (c (n "mailparse") (v "0.8.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "15964w9i7xbj2mkmnwjfzfr9dk6xar74qx46q6dmkfm5pm63kzk8")))

(define-public crate-mailparse-0.8.2 (c (n "mailparse") (v "0.8.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "054rqhvywhgbh964g1c02hbgyiav7cyig0g3lbg2vcv09ybgavbi")))

(define-public crate-mailparse-0.9.0 (c (n "mailparse") (v "0.9.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "07gh06wr3bmnqhryfalyx194lfzb3nx3q1q8m71njls3mqdmjl8x")))

(define-public crate-mailparse-0.9.1 (c (n "mailparse") (v "0.9.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "12vdfj5iix13ak7chvbkzys7y8ql8rkrxgsx8fidil4zyw6ya4qb")))

(define-public crate-mailparse-0.9.2 (c (n "mailparse") (v "0.9.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1qng7rzxrmsz5m9cylzacg4idvylg8h9y8yg65fr1anq02nhp9ji")))

(define-public crate-mailparse-0.10.0 (c (n "mailparse") (v "0.10.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "0hv89z70bpjni1msm3n0d3cpyh3yjkb7l5agh610z9pzb943bh65")))

(define-public crate-mailparse-0.10.1 (c (n "mailparse") (v "0.10.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1wbvgzwd451lzpdlmdg5v144kscvrj6k4218s2nsl24gq5ybj3n0")))

(define-public crate-mailparse-0.10.2 (c (n "mailparse") (v "0.10.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1ysrg48p6llz79vsbznmn6wv63ypi5v50ngl1jz2ml55sfa4xih4")))

(define-public crate-mailparse-0.10.3 (c (n "mailparse") (v "0.10.3") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1h5rd0ll7kbrnpwxzzqxn54v833mp8cx8wqam5h38hipzqh143xw")))

(define-public crate-mailparse-0.10.4 (c (n "mailparse") (v "0.10.4") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1n5xp99nzi01123490kgc3yhmph0fspbl4zklam3ic5swizxy0vc")))

(define-public crate-mailparse-0.11.0 (c (n "mailparse") (v "0.11.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1hzf9lnj8d2552y3rvswibrnq878aka0rqgm48lvydfmm26fw6bh")))

(define-public crate-mailparse-0.12.0 (c (n "mailparse") (v "0.12.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1llw8rw74lv4q2yn6fdbllfqf86axx1z3h5h27q23ygyd1x510bi")))

(define-public crate-mailparse-0.12.1 (c (n "mailparse") (v "0.12.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1q5l3f15ksinrn5b82pk09lj7k9dyik41jq9z8z3qnv1ckx5lhxn")))

(define-public crate-mailparse-0.12.2 (c (n "mailparse") (v "0.12.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1bwzjsrpm2vw5hxnvdp0dhm9fjawbgpsvgc9z8ir8d0k0pjmjv9k")))

(define-public crate-mailparse-0.13.0 (c (n "mailparse") (v "0.13.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "0nh5lmqlz82815fkby8z7sv78c91d05llzyj71b8gzm03ri996s7")))

(define-public crate-mailparse-0.13.1 (c (n "mailparse") (v "0.13.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "0g6sc98r71k6rxmyzjbzg55v6rjrxnw0chs02h1jvcv1y6kpg2iq")))

(define-public crate-mailparse-0.13.2 (c (n "mailparse") (v "0.13.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "08lv05ja6km7xzm7x9l88in2pw8dc1bdx4xxgpgfz0n58f81zpii")))

(define-public crate-mailparse-0.13.3 (c (n "mailparse") (v "0.13.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "19mpqzsyz6b9mlz0ghhbiryljq0brmjfqr38r5nycnj81hpnqa1h")))

(define-public crate-mailparse-0.13.4 (c (n "mailparse") (v "0.13.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "11kk8mvkbj00h95ax53j3x4j7z5xhg0xbw4chnlf7c223bzp7nv2")))

(define-public crate-mailparse-0.13.5 (c (n "mailparse") (v "0.13.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.2") (d #t) (k 0)))) (h "1qacyzfl3wsl745b92w9gj0mjg43rcwg99l96rmg8l1sq5pm4vy0")))

(define-public crate-mailparse-0.13.6 (c (n "mailparse") (v "0.13.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.3") (d #t) (k 0)))) (h "0d375mgnblqrjm3hlry8hkn7pm8dk3v7d08jz1cdm5l33k5f3rjy")))

(define-public crate-mailparse-0.13.7 (c (n "mailparse") (v "0.13.7") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "charset") (r "^0.1.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.3") (d #t) (k 0)))) (h "0rywd032gyh26ax6ijgx5p2y79r0hzrpavn43myjyahr1f2f02np")))

(define-public crate-mailparse-0.13.8 (c (n "mailparse") (v "0.13.8") (d (list (d (n "charset") (r "^0.1.3") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "ouroboros") (r "^0.14.0") (d #t) (k 2)) (d (n "quoted_printable") (r "^0.4.3") (d #t) (k 0)))) (h "0cka4vpk7dpyx22l3csff8c82wkwkz2py9wrjms5fmc3a257dblc")))

(define-public crate-mailparse-0.14.0 (c (n "mailparse") (v "0.14.0") (d (list (d (n "charset") (r "^0.1.3") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "ouroboros") (r "^0.14.0") (d #t) (k 2)) (d (n "quoted_printable") (r "^0.4.6") (d #t) (k 0)))) (h "1al0yb9wgy26ihd0gm4x1dk9rsv23wrmp2qw1lk4f04cbw7mfmkb")))

(define-public crate-mailparse-0.14.1 (c (n "mailparse") (v "0.14.1") (d (list (d (n "charset") (r "^0.1.3") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "ouroboros") (r "^0.17.0") (d #t) (k 2)) (d (n "quoted_printable") (r "^0.5.0") (d #t) (k 0)))) (h "0mwpkxi41ak8pabknmvj49dpsbs0q6w7w12f0lp49avcjaa6a29d")))

(define-public crate-mailparse-0.15.0 (c (n "mailparse") (v "0.15.0") (d (list (d (n "charset") (r "^0.1.3") (d #t) (k 0)) (d (n "data-encoding") (r "^2.6.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.17.0") (d #t) (k 2)) (d (n "quoted_printable") (r "^0.5.0") (d #t) (k 0)))) (h "0zkwbrzgr7pp1wyywjgvlxayr1p3nnkn2yxgi97746j1h1ckv81x")))

