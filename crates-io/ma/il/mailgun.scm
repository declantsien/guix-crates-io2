(define-module (crates-io ma il mailgun) #:use-module (crates-io))

(define-public crate-mailgun-0.1.0 (c (n "mailgun") (v "0.1.0") (h "0sc92rd0mq7xlw9xvs77xy9zimnkz2m44z5ziyzjmgnvfgwvky77")))

(define-public crate-mailgun-0.1.1 (c (n "mailgun") (v "0.1.1") (h "0pr23bqb2hdh819ajxldgz69qasays2fpi0y8gsy8vdv99xrbxs7")))

(define-public crate-mailgun-0.1.2 (c (n "mailgun") (v "0.1.2") (h "1nhb8a9q1zv3gs6d1npfjv861kd67w6qxfmxj309k1zbjydr1qgv")))

(define-public crate-mailgun-0.1.3 (c (n "mailgun") (v "0.1.3") (h "14dz3jb7b15qsmj8nrhvmrwmaa1v6bh0szhib5kfaqcaddfw05lq")))

