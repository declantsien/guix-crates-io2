(define-module (crates-io ma il mail_extractor_binary) #:use-module (crates-io))

(define-public crate-mail_extractor_binary-0.1.2 (c (n "mail_extractor_binary") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "mail_extractor") (r "^0.1.3") (d #t) (k 0)))) (h "0cyqb1yp6qdx3ggi63kxmy89lmcqc1ca3f7k4byap9nvgwmwn35v")))

