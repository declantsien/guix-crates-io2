(define-module (crates-io ma il mailgun_api) #:use-module (crates-io))

(define-public crate-mailgun_api-0.0.1 (c (n "mailgun_api") (v "0.0.1") (h "154syqsp4w8akbybgxcb9jmy7lvqdzw83ccb5cn67y87n2g58wfv")))

(define-public crate-mailgun_api-0.1.0 (c (n "mailgun_api") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0b973gh57cj60367sp41lrx5c8k2592cvr0bhld2wmac3w45vp5m")))

(define-public crate-mailgun_api-0.1.1 (c (n "mailgun_api") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "05g05ns187xpzkq44izay8ixmwzxk8m6axrz8qjyb0jmlmnhmixj")))

(define-public crate-mailgun_api-0.2.0 (c (n "mailgun_api") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0l023mii2x5gn9a58pifddfjq480291hfp1fn0hgh7fkkqvfybv2")))

(define-public crate-mailgun_api-0.2.1 (c (n "mailgun_api") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0gkqzwhf4z7spa6i1v5g21f637xrwdflay18w2cmlvx51clxfyrv")))

