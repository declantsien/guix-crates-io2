(define-module (crates-io ma il mail_slot) #:use-module (crates-io))

(define-public crate-mail_slot-0.1.0 (c (n "mail_slot") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("everything"))) (d #t) (k 0)))) (h "16nr1vncjqcnvhnzsjjp7mh0makrdksn2pk5bpcrylwxb3h1yk7c")))

(define-public crate-mail_slot-0.1.1 (c (n "mail_slot") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("everything"))) (d #t) (k 0)))) (h "0mqms6yinyczw1smx0jl32i9hg1gi041fh14a0i4z4ql69808s5a")))

(define-public crate-mail_slot-0.1.2 (c (n "mail_slot") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("handleapi" "winbase" "winnt"))) (d #t) (k 0)))) (h "124fn94yfpgsfr6ic55syb278fiydhxdzvhm1fppjxh4pjaww8hf")))

(define-public crate-mail_slot-0.1.3 (c (n "mail_slot") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("handleapi" "winbase" "winnt"))) (d #t) (k 0)))) (h "19dsgkvc3xg8pg0yc3n18ngz0mgj7xcmcq7q9kcv8ics88vrzqxq")))

