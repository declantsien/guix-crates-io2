(define-module (crates-io ma il mailbox_processor_rs) #:use-module (crates-io))

(define-public crate-mailbox_processor_rs-0.3.0 (c (n "mailbox_processor_rs") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0cln4p2w2fmsp84myzkyawl1slcryj71bnfasjg3s2drwhyi32xn")))

