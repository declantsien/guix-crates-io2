(define-module (crates-io ma il mail-chars) #:use-module (crates-io))

(define-public crate-mail-chars-0.1.0 (c (n "mail-chars") (v "0.1.0") (h "04019x43n1sp7mbkx1kyj8kg4prgpq2msyx9x324z4yjkgfrzjk7")))

(define-public crate-mail-chars-0.1.1 (c (n "mail-chars") (v "0.1.1") (h "0rc1h3f179vvgs9sihczkl6v1jczgjspblrjarpr9ij8hxbhi74q")))

(define-public crate-mail-chars-0.2.1 (c (n "mail-chars") (v "0.2.1") (h "0i7yl8qq804h9y777mkxn3asmpsafpbw9y2xwc0853gqk7l8kp5n")))

