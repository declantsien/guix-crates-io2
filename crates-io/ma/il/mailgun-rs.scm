(define-module (crates-io ma il mailgun-rs) #:use-module (crates-io))

(define-public crate-mailgun-rs-0.1.0 (c (n "mailgun-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "= 0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)))) (h "0p5lh4l7y0lqk8a34jc0dy5arykkdpg15kx423krrs2ps3vrc7kg")))

(define-public crate-mailgun-rs-0.1.1 (c (n "mailgun-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "= 0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)))) (h "01l4bwspmdpqspqng0lhvlzjb5qr2g6xgmgfl12crqzrdazsq301")))

(define-public crate-mailgun-rs-0.1.2 (c (n "mailgun-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "= 0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)))) (h "1qdgmmgghxhb12hs0541y927z4hs1mbmbhxgbl3hvdh9xmpplyby")))

(define-public crate-mailgun-rs-0.1.3 (c (n "mailgun-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "= 0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)))) (h "0prsqx4grnxxvyfk7q59d2b0zr6h2x722gbf4sccwk7a37wrv4h5")))

(define-public crate-mailgun-rs-0.1.4 (c (n "mailgun-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y3s9c9hs6pk3rzms99jr9c8407n2n41fb8bjy17wim5iny6gj9k")))

(define-public crate-mailgun-rs-0.1.5 (c (n "mailgun-rs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1lncj9ik1j58gvfjar23x6q0q65p3l0810qa31d4aik8kay882jf")))

(define-public crate-mailgun-rs-0.1.6 (c (n "mailgun-rs") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0a48sb2d0r31il5sars05rzyjjlmzil23d0hdvrrmgdm64049b7h")))

(define-public crate-mailgun-rs-0.1.7 (c (n "mailgun-rs") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0rxwh5j81j2vlarpg6arciyqh076isq4npnxfr2dvdynslwhl7h8")))

(define-public crate-mailgun-rs-0.1.8 (c (n "mailgun-rs") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "124xwx6kb4vfjqyab61hqp19jyq7k6grfnn3n27hi8m29879pp8y")))

(define-public crate-mailgun-rs-0.1.9 (c (n "mailgun-rs") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.2") (d #t) (k 0)))) (h "0i481jlg4d5lszxc44yarppvpcxhca53n10zgvbxk8kj326szwpb")))

(define-public crate-mailgun-rs-0.1.10 (c (n "mailgun-rs") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.2") (d #t) (k 0)))) (h "1224yd7b8xq8dba7kmn3r00h6sw18yqfihx1w41rifcl90c3md0r")))

(define-public crate-mailgun-rs-0.1.11 (c (n "mailgun-rs") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.2") (d #t) (k 0)))) (h "16967xk2clc1zjg8f5y9dlablc6y02wlk8r98bm7h4dc32pav1z8")))

