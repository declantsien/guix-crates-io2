(define-module (crates-io ma il mailboxvalidator) #:use-module (crates-io))

(define-public crate-mailboxvalidator-1.0.0 (c (n "mailboxvalidator") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "0b6xs0h5m3dzd0298ghgkw47ssmdb6n5rplapj43xhidl7hlcsyi")))

(define-public crate-mailboxvalidator-1.1.0 (c (n "mailboxvalidator") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "175qsjrq8ypw2a434nkc0ravagxdbx6i80gfwkmznbkhcw2n3icw")))

(define-public crate-mailboxvalidator-1.1.1 (c (n "mailboxvalidator") (v "1.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "0309p6l2ax5kyn2x2rxrph2z1jp065pcnjnz3zmv1synayjgnmzb")))

