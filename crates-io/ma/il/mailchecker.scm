(define-module (crates-io ma il mailchecker) #:use-module (crates-io))

(define-public crate-mailchecker-0.1.0 (c (n "mailchecker") (v "0.1.0") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "091vpgz99z2j0k82mnc593g5ds409x3bcnzkda1j37cfvdyqfiq0")))

(define-public crate-mailchecker-3.0.20 (c (n "mailchecker") (v "3.0.20") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)))) (h "04iwq35fxjr8479jyna51mdzy1h7brdx4h48jpsiqdg3x17acc45")))

(define-public crate-mailchecker-3.0.21 (c (n "mailchecker") (v "3.0.21") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)))) (h "0pjwr205k7gvpm2blg80caz667fdpcklsvjhifnmjplv81q2q2dk")))

(define-public crate-mailchecker-3.0.22 (c (n "mailchecker") (v "3.0.22") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)))) (h "09g8kii5yiqn9byz18w19mfd86ysx82z5ahh03n7yivacn8rhl9a")))

(define-public crate-mailchecker-3.0.23 (c (n "mailchecker") (v "3.0.23") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)))) (h "00dr82lzk588cig6n3bylnxj66060c3xln9g4fsb7r711ylwrysd")))

(define-public crate-mailchecker-3.0.24 (c (n "mailchecker") (v "3.0.24") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)))) (h "0i95llk6i8issdgc42hapc3ad3l2mj4nr7g67chz6cjmd8mfy2yq")))

(define-public crate-mailchecker-3.0.25 (c (n "mailchecker") (v "3.0.25") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "14zksf5qnpl1m5ccyi01ga2gd2b8fz1sf1abxh2ixffngfabz56a")))

(define-public crate-mailchecker-3.0.26 (c (n "mailchecker") (v "3.0.26") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1pn0rqavms6728q69mnrxr04hsn54ymi6riq8x4ym7yy36k3xwkv")))

(define-public crate-mailchecker-3.0.27 (c (n "mailchecker") (v "3.0.27") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1mypxcy0bkj7c3652fk9vi8hn1s32v0y967277a6jxk970k1a001")))

(define-public crate-mailchecker-3.0.28 (c (n "mailchecker") (v "3.0.28") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "190n7fccgbdvxj1yp82g9rz5x9d79d01vqi72di0gh4wbzlss2k1")))

(define-public crate-mailchecker-3.0.29 (c (n "mailchecker") (v "3.0.29") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0c6yhfwc7hfpn2wgsaszpssl2jqxsqv681lfwf18qnhap2p81j95")))

(define-public crate-mailchecker-3.0.30 (c (n "mailchecker") (v "3.0.30") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1xa7y64znj603n0ddcrf8iyik72w3h34bgqj8l2n2ws6xhyw6mcp")))

(define-public crate-mailchecker-3.0.31 (c (n "mailchecker") (v "3.0.31") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "18qhzh5fgvm34wlbf0ibarzw5i2by4vx0dy0kblz8bi6bmymkzaq")))

(define-public crate-mailchecker-3.0.32 (c (n "mailchecker") (v "3.0.32") (d (list (d (n "fast_chemail") (r "^0.9.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1j02b36z5fzmlxkc2cfv1c380jjy19rymg61f1xxqk6733m3wmr4")))

(define-public crate-mailchecker-3.0.33 (c (n "mailchecker") (v "3.0.33") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0h529n390339qlj8405y9hnhivkxgw45d1milvhmjda3rmglqm0n")))

(define-public crate-mailchecker-3.0.34 (c (n "mailchecker") (v "3.0.34") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1509qbhw55hwllf2j926crxaskzfw8qdynavsfarvp9x97fvwd1w")))

(define-public crate-mailchecker-3.0.35 (c (n "mailchecker") (v "3.0.35") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0b45bzbh6x89jc21ixdribzqjyy2qpmkfgd1agaqv9x3hmnly3yh")))

(define-public crate-mailchecker-3.0.36 (c (n "mailchecker") (v "3.0.36") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "12879lmkdd8r95dakjdyl0g841m9z5zd75q15rl8989d019ln78h")))

(define-public crate-mailchecker-3.0.37 (c (n "mailchecker") (v "3.0.37") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "005rl12hm5rwswj6wmrccqpr083lkm9nviq5wi52i50hzwh4gxam")))

(define-public crate-mailchecker-3.0.38 (c (n "mailchecker") (v "3.0.38") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1l7c3mzk6ba1x4hsdzznryaxfd0a1gbhbnja0wfym6z3m77k8fgf")))

(define-public crate-mailchecker-3.0.39 (c (n "mailchecker") (v "3.0.39") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "13lyyqy32rifragicqzkxv2dmb870mnjsa2d52bm7lh2wm4m7gvr")))

(define-public crate-mailchecker-3.0.40 (c (n "mailchecker") (v "3.0.40") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0hiv112i8dz5mwb5xj2jg5icxh7hwq0pmz1r22hxdarl0g3jaqkh")))

(define-public crate-mailchecker-3.1.0 (c (n "mailchecker") (v "3.1.0") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1lmca74wp1gaqgmqi3xzvh7mbppf4lpmhd3vnms8k5bzgfzmsmdf")))

(define-public crate-mailchecker-3.2.0 (c (n "mailchecker") (v "3.2.0") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "13m89332w62sxljxc1ba3585h1ddqi9sg1rvlsyqdscb21pl39rx")))

(define-public crate-mailchecker-3.2.1 (c (n "mailchecker") (v "3.2.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1ccf9yzz7cr1b3xmqj6yfw1cncjzm03wa8m3bdrcymncxqrj5d7v")))

(define-public crate-mailchecker-3.2.2 (c (n "mailchecker") (v "3.2.2") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0rsp7d152bqp32p93bz0jvr3l34yj3alwjvsgp53n4vppm343hhx")))

(define-public crate-mailchecker-3.2.3 (c (n "mailchecker") (v "3.2.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "09g7x8bri33103f0zsy8hfnbh6cvxpbnarvr8zdhh3yfvjzdm3mj")))

(define-public crate-mailchecker-3.2.4 (c (n "mailchecker") (v "3.2.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "09a7l1n8v2qg5mkvxiz2hny0dvrg2ryfw1wxgy2dm15fzgxn9i5r")))

(define-public crate-mailchecker-3.2.5 (c (n "mailchecker") (v "3.2.5") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "18phm5acxlmri6l0yxr345cr2mrrff3piazc0nnvjdx9jjxr4vsi")))

(define-public crate-mailchecker-3.2.6 (c (n "mailchecker") (v "3.2.6") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0v76s43swxmxfms3pxpnllvrd95mddidakida9halwbn6yq46577")))

(define-public crate-mailchecker-3.2.7 (c (n "mailchecker") (v "3.2.7") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "025b75dl3mqq6jkwqm7zgb5ic60hbzfznapiz245s336k01zi5i4")))

(define-public crate-mailchecker-3.2.8 (c (n "mailchecker") (v "3.2.8") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1kaqhr8khmsw8fb7433af33h5rxjji3knqchfs0z70yr8rzkrzzv")))

(define-public crate-mailchecker-3.2.9 (c (n "mailchecker") (v "3.2.9") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1n2sxm74xixrwndhz866qz1rhhqnpii4i2308fpc8d6sbcn5b4v9")))

(define-public crate-mailchecker-3.2.10 (c (n "mailchecker") (v "3.2.10") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1canb80sn91jwyk9nyx160a89c6h7ic5igjj0baahsa1xx16y302")))

(define-public crate-mailchecker-3.2.11 (c (n "mailchecker") (v "3.2.11") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1iyci8fhdfj6y61zlbxf90h6szny6yljlprnqmr5kz39758fgvdk")))

(define-public crate-mailchecker-3.2.12 (c (n "mailchecker") (v "3.2.12") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "10yg2nql75pnr67srwdgm6fc9bvadz2z232p16sqag3scsr2sczc")))

(define-public crate-mailchecker-3.2.13 (c (n "mailchecker") (v "3.2.13") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0kb9snnz3gqlbmprikia7dvb2adypbnpc6xrqn9203zj825x6c58")))

(define-public crate-mailchecker-3.2.14 (c (n "mailchecker") (v "3.2.14") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1isl09ghcccpzn5srskxg6hj75m3jahbw4nrkb5h9xnq1091b3pr")))

(define-public crate-mailchecker-3.2.15 (c (n "mailchecker") (v "3.2.15") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0arsf3yn1p9i50501z4jaycbcblg1cgv2hsmny4ycj8awh5j4ndg")))

(define-public crate-mailchecker-3.2.16 (c (n "mailchecker") (v "3.2.16") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "10zrj399pcbgj94656qm0qb51r5l3lzsl824rw93zd6vxi7qx169")))

(define-public crate-mailchecker-3.2.17 (c (n "mailchecker") (v "3.2.17") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0fvw31xvf3c1qnciwcpqhm3pfs8q78sw0y1aspq30yn1ah8ai1as")))

(define-public crate-mailchecker-3.2.18 (c (n "mailchecker") (v "3.2.18") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0npcx247c36pr9gcij6kcr403yqhwna6yn1jmsnbfgy5z5cd9l2i")))

(define-public crate-mailchecker-3.2.20 (c (n "mailchecker") (v "3.2.20") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0c354yzmkgqykbnysh21fq7mkmzqm3lka22fccjjwvcc65di9dpr")))

(define-public crate-mailchecker-3.2.21 (c (n "mailchecker") (v "3.2.21") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "00f88wzlnnf15ixj00iz2xdas7s2ypa35i1pxpzj0z1b5km28vsi")))

(define-public crate-mailchecker-3.2.22 (c (n "mailchecker") (v "3.2.22") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0nc393jn4hgf8gf2yc1wdl5h24vix8ihf3ijzxfl0njq7hg4yphi")))

(define-public crate-mailchecker-3.2.23 (c (n "mailchecker") (v "3.2.23") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0af8fylga9l0p5cdij59p5wwi5gjy7mdvwl8d6cwvibz9xgkfswf")))

(define-public crate-mailchecker-3.2.24 (c (n "mailchecker") (v "3.2.24") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "03wv3md0ls98vxs2d3afcpbs6cs9i6yp143437jfac54lhhg8i8f")))

(define-public crate-mailchecker-3.2.25 (c (n "mailchecker") (v "3.2.25") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "00nams2927iafjbc3l14p4j4732y13fbh3qk37lzm10s2fx311iv")))

(define-public crate-mailchecker-3.2.26 (c (n "mailchecker") (v "3.2.26") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1rsbk314bcgq2kx17x0j3i37a9bss2fkbn84ngvwz2c2yzad8946")))

(define-public crate-mailchecker-3.2.27 (c (n "mailchecker") (v "3.2.27") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0g2kkcr5whwjv3a43b8ja0hln89mkfaiyqq0dmnb3i249sw5g2zf")))

(define-public crate-mailchecker-3.2.28 (c (n "mailchecker") (v "3.2.28") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0pcj8j7n55bgydnddy0pmghsvchm3rz4sa3daq8idncn13zdcz7w")))

(define-public crate-mailchecker-3.2.29 (c (n "mailchecker") (v "3.2.29") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1bffgd4mcf1xi573xklrwdwq840ccfssg3nqxmghppl5qvm1fybf")))

(define-public crate-mailchecker-3.2.30 (c (n "mailchecker") (v "3.2.30") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0f0blnq10674z1i7wbqk9knsr1p9djs6w1swrqlf6ydkligl6qcg")))

(define-public crate-mailchecker-3.2.31 (c (n "mailchecker") (v "3.2.31") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1lcy3x2pm33mbjvzj7xnp8v3da4m8pf81lpa0s1nbb9yrd25ka6w")))

(define-public crate-mailchecker-3.2.32 (c (n "mailchecker") (v "3.2.32") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0rhy6ym26fp1qknnnhc5nqgh047vy36bx40lcn2mq2g4qkdl0p5h")))

(define-public crate-mailchecker-3.2.33 (c (n "mailchecker") (v "3.2.33") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "01n1i4p44vsafwa23wa3zks0fcxiih8iz1xr4hxa64b6kvsvi1mp")))

(define-public crate-mailchecker-3.2.34 (c (n "mailchecker") (v "3.2.34") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0ir9wh6w60ymdw5pn4ir7dppjkw98p969va0d2slzxwn8z52ih0d")))

(define-public crate-mailchecker-3.2.35 (c (n "mailchecker") (v "3.2.35") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0dv4kfp7ijnpffmx02yi571ac50n10733zwsjs532nj9h3lgx6pv")))

(define-public crate-mailchecker-3.2.36 (c (n "mailchecker") (v "3.2.36") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0ajaphq8l62pjmqg0wrdhq8f8315n935gbiivzsbl03aan5wfcp0")))

(define-public crate-mailchecker-3.2.37 (c (n "mailchecker") (v "3.2.37") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0d764ls5m51qxi6xv9nsq3ry4mhy2n0i0b7ya39zil0f0zrzscp3")))

(define-public crate-mailchecker-3.2.38 (c (n "mailchecker") (v "3.2.38") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1ncaai7sq14k6w7nzznl6c8mj3998z705nvvsnln794asmr6xgqv")))

(define-public crate-mailchecker-3.2.39 (c (n "mailchecker") (v "3.2.39") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0f8kyvzfvlww971bgwfda8y7xm5dzgcjcgyi2c9ccsdc6xjbjwsi")))

(define-public crate-mailchecker-3.3.0 (c (n "mailchecker") (v "3.3.0") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1p0j14smbhn41ly5d47s1732nbilhw6p16r7a06gg89836rx956b")))

(define-public crate-mailchecker-3.3.1 (c (n "mailchecker") (v "3.3.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0xclsx6bdx850aq91jxwr126a2wd7ik9y9zf1x8hkgyxlnwd0nkv")))

(define-public crate-mailchecker-3.3.2 (c (n "mailchecker") (v "3.3.2") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "01fly5wfg07vxb4w3d64w27s0ysd6zbb52h5a50d6psz0jj541g0")))

(define-public crate-mailchecker-3.3.3 (c (n "mailchecker") (v "3.3.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1p7265aymfl4wf84d318wk88y5v6lz0czhkvpnk800zkzqnq5fp9")))

(define-public crate-mailchecker-3.3.4 (c (n "mailchecker") (v "3.3.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "04wn6dqfgm50n1frn5x1gh2fq74y784sc0pmmfkir2sbrcfj07br")))

(define-public crate-mailchecker-3.3.6 (c (n "mailchecker") (v "3.3.6") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0ka55zzbk6pr2pc8jsh9js88sq7bnilqa35kp9ygal4qj9xyvbmi")))

(define-public crate-mailchecker-3.3.9 (c (n "mailchecker") (v "3.3.9") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "141lap0hbsx4jq0926cj9p9mk2169h19sdjs8n64d21zd9kyz103")))

(define-public crate-mailchecker-3.3.10 (c (n "mailchecker") (v "3.3.10") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0hlzv87llwx8ia2i0zy98djir3r56ccxx0q2cq2r36pgvg3iqm1m")))

(define-public crate-mailchecker-3.3.11 (c (n "mailchecker") (v "3.3.11") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "18whwggdknqacb4m2mnk3rywd750b2h0w0j9c88aahb0yq4mjdim")))

(define-public crate-mailchecker-3.3.12 (c (n "mailchecker") (v "3.3.12") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0yi6skk947mdymk2cyf4chhbpbmnqivc8v2vm49rabacarb1a3p5")))

(define-public crate-mailchecker-3.3.13 (c (n "mailchecker") (v "3.3.13") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0gbgp0y6dmha35g5llpg53ssi11dk4vy4v1xp9jbq45d7nxrp1zs")))

(define-public crate-mailchecker-3.3.14 (c (n "mailchecker") (v "3.3.14") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1xyk9clk7lff190y68blvvrd9p2yplh0c4h3v4zvhpz6b2hc74f7")))

(define-public crate-mailchecker-3.3.15 (c (n "mailchecker") (v "3.3.15") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1srapiv8bkwkg3jnliqvq2b9knv65wzhz6d7pagfn7l5kwidp6p1")))

(define-public crate-mailchecker-3.3.16 (c (n "mailchecker") (v "3.3.16") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "08zbga8x9ynz2rr8kqbiy3bziyzvdkzgzvs3lxqimw3749hv20j7")))

(define-public crate-mailchecker-3.3.17 (c (n "mailchecker") (v "3.3.17") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1gg794jlxc3p9d9szh7c9rbyq3vliq92zfk3b9wj5zbc3bph3m2z")))

(define-public crate-mailchecker-4.0.0 (c (n "mailchecker") (v "4.0.0") (d (list (d (n "fast_chemail") (r ">=0.9.5, <0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r ">=0.1.2, <0.2.0") (d #t) (k 2)))) (h "1mx1j4sm3bqqa33s0v5xmm9akq1s2wkshk051yvpmqk1wsnf3qpl")))

(define-public crate-mailchecker-4.0.1 (c (n "mailchecker") (v "4.0.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "137q80af6lc177ahvsqi9hq77hwc3lymyvpww4wvj2ppv4vbhmys")))

(define-public crate-mailchecker-4.0.2 (c (n "mailchecker") (v "4.0.2") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0k8129d1n79s25vfyxnsgd98w7b1xyja189w33jzmj6gg9gxgh13")))

(define-public crate-mailchecker-4.0.3 (c (n "mailchecker") (v "4.0.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "17iakgd2008nlwyjlgaw9w9znm7s7y5sw8sz3mj9zzinv9yjln25")))

(define-public crate-mailchecker-4.0.4 (c (n "mailchecker") (v "4.0.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1hw6kc9j35z1dpjyz1ny59gmg56km39nk3mwwf83qi898slpgil0")))

(define-public crate-mailchecker-4.0.5 (c (n "mailchecker") (v "4.0.5") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "183621g466npafz4g76g0hwjkqi6fzcwvg6gcmdih2mlrc3hgq01")))

(define-public crate-mailchecker-4.0.6 (c (n "mailchecker") (v "4.0.6") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0m9xdmwvqva7hpkzkz4j8xn4090kwyy44yy01cb7kayyi96qis8v")))

(define-public crate-mailchecker-4.0.7 (c (n "mailchecker") (v "4.0.7") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "07r11lrnr0nxvqr8zggs6fdly763jaz39b5jbbavgmvmk8b49hv9")))

(define-public crate-mailchecker-4.0.8 (c (n "mailchecker") (v "4.0.8") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1cskdm1qxh18wcqard5z8whzsyckanylanmq56a41s9sxb9p9vkb")))

(define-public crate-mailchecker-4.0.9 (c (n "mailchecker") (v "4.0.9") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0493kxargpprrvn9jrx61va3mz2gpagps8b9yvnfdq3hnzp6zqz7")))

(define-public crate-mailchecker-4.0.10 (c (n "mailchecker") (v "4.0.10") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "17qg7ki0kwlpjp3zx8d0xx9flgqkrxchazzfl8swz4jkc5hgl76f")))

(define-public crate-mailchecker-4.0.11 (c (n "mailchecker") (v "4.0.11") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "14ybq751ni1shs729n9mxgqbxzjwajpc39h7l7ky1fczjs3r52fs")))

(define-public crate-mailchecker-4.0.12 (c (n "mailchecker") (v "4.0.12") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0g2h6z85pz87nd8sdbx2ryxazj6jmdydaykz0b13ss4xfzpx27wz")))

(define-public crate-mailchecker-4.0.13 (c (n "mailchecker") (v "4.0.13") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "01rgw4496xmlyq778cmrlh1ffsdgvwbbir3mnl15d5n5gc2qprks")))

(define-public crate-mailchecker-4.0.14 (c (n "mailchecker") (v "4.0.14") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "19nbs2mz1vf7yrv8g32rkyq2brcgqza6rxpjbkq4jvfcpf9xy81x")))

(define-public crate-mailchecker-4.0.15 (c (n "mailchecker") (v "4.0.15") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "03nqwk6c9j9p14p21h5bqcf6js7vw3p5nmc92ifii6dk0j2xi50d")))

(define-public crate-mailchecker-4.0.16 (c (n "mailchecker") (v "4.0.16") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "12rn35s32hb2idb3b8r58a2d344s3nd2i0gw7cqbxziqkz3i5l40")))

(define-public crate-mailchecker-4.1.0 (c (n "mailchecker") (v "4.1.0") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "04p2545f4mhgpyxkyvb25lvmnq28pkx41agc3y7pa1z9kjxvm2ph")))

(define-public crate-mailchecker-4.1.1 (c (n "mailchecker") (v "4.1.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1hli8sq8bhcxmdbgl2ypj6gbhz0myqy3s9f666qkazpbkzzjxi67")))

(define-public crate-mailchecker-4.1.2 (c (n "mailchecker") (v "4.1.2") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1ih4bkdsy7vihvl5hlhxwdac3q5ag085lgs6dcl92w2mg3m4j1rk")))

(define-public crate-mailchecker-4.1.3 (c (n "mailchecker") (v "4.1.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1y9zc604wg9dywjnbsc6jjk6nhb1zfkh241vqvd9b8snj0zzg2y8")))

(define-public crate-mailchecker-4.1.4 (c (n "mailchecker") (v "4.1.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "086y8r8i5mx1awmnbpn44ws3r1957gl3xq1sjdmbmwcwkdi1p228")))

(define-public crate-mailchecker-4.1.5 (c (n "mailchecker") (v "4.1.5") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0p5s7zpl2j6szz3zl74qhvfpclr4mjcrjclab6lzbrqqmwdi56s6")))

(define-public crate-mailchecker-4.1.6 (c (n "mailchecker") (v "4.1.6") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1qq0lgnbl2sjlbzcxqsp8gsvi79pfpyv0cz5nhfcys80q8ggvxjs")))

(define-public crate-mailchecker-4.1.7 (c (n "mailchecker") (v "4.1.7") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1gnhr9q0afics1hl4vrjppy37n8x96askv278r03lx1r1vf289lc")))

(define-public crate-mailchecker-4.1.8 (c (n "mailchecker") (v "4.1.8") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "130zqzk13yb78bnchzj8lhybaz81mbkaagf9x7m7gfspgg46al1i")))

(define-public crate-mailchecker-4.1.9 (c (n "mailchecker") (v "4.1.9") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "042wflq0rkp4511vivfrmh2ymmkrqrdcksyy3c96gnlpn1cnrcqg")))

(define-public crate-mailchecker-4.1.10 (c (n "mailchecker") (v "4.1.10") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1a5br261k3iw3ks7fpkhn4kvn341l053wf0gcr6rwcq78kdrvsgi")))

(define-public crate-mailchecker-4.1.11 (c (n "mailchecker") (v "4.1.11") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "008va9lnv5m701b1j85n7l1cyqc4x6wm27n602wfmspbpmd2sgg7")))

(define-public crate-mailchecker-4.1.12 (c (n "mailchecker") (v "4.1.12") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0wfzrzvpdfih62skr41mfv5pnv1gnmlnhw2g2hh62mzbc9hlpdmk")))

(define-public crate-mailchecker-4.1.13 (c (n "mailchecker") (v "4.1.13") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1a3s5pn5ql2dm18bxvfysk12rd72c3jb5iwybxjgrv0g4nr6a7rj")))

(define-public crate-mailchecker-4.1.14 (c (n "mailchecker") (v "4.1.14") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "04m1kp15vzxhfl1cljyqi98x991pl4yf8llz5saizdzmi89mzfv9")))

(define-public crate-mailchecker-4.1.15 (c (n "mailchecker") (v "4.1.15") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "106ccx67mii8sbqcqhx3zdnvjkk7vy5ljbfymqpd1r1mryg0j4zj")))

(define-public crate-mailchecker-4.1.16 (c (n "mailchecker") (v "4.1.16") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0k6vspwc1qp9dydl9k0id4vcvmjd8pkf0wvzgqg2v144kn1br5b9")))

(define-public crate-mailchecker-4.1.17 (c (n "mailchecker") (v "4.1.17") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1lql17qaykccj2cgxdhv6mmni4n4h08smadymwm7ki46fixvkkwf")))

(define-public crate-mailchecker-4.1.18 (c (n "mailchecker") (v "4.1.18") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0zkbqnfdy8jj3fdnakn46h312pqpvlpibjfsh179f2kf8xkpqn3g")))

(define-public crate-mailchecker-4.1.19 (c (n "mailchecker") (v "4.1.19") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1s4sdyfg1qbxfigca5zpx2d76458621pwbdpfpwlp0509ap7kpyg")))

(define-public crate-mailchecker-5.0.0 (c (n "mailchecker") (v "5.0.0") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "18ylpawcbrfh3clb09031mx61pj5cj04wxn2plsklzmjndsfx24i")))

(define-public crate-mailchecker-5.0.1 (c (n "mailchecker") (v "5.0.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1gf39kx1y015chsdv3w3jixpzh8wqqa6b64pjgyhysc1zds8q693")))

(define-public crate-mailchecker-5.0.2 (c (n "mailchecker") (v "5.0.2") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "038f8g0wd2z6zgfhl5vgn1ib6a3i45wn8awz6lpqml80zkz8p407")))

(define-public crate-mailchecker-5.0.3 (c (n "mailchecker") (v "5.0.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "169hqzv2icg6pp21mw7bm0hy861l6fyhhl5j9ph8dylig4fdc1w4")))

(define-public crate-mailchecker-5.0.4 (c (n "mailchecker") (v "5.0.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1321n9vdx7y3qavnpp28ynj716dsyb0vc5v9svmiqdwcxi5h209d")))

(define-public crate-mailchecker-5.0.5 (c (n "mailchecker") (v "5.0.5") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "0hgc99d9a4ifcz77gbw83yyx3lf00nm2slxv0rkhchjr9bfxxs8z")))

(define-public crate-mailchecker-5.0.6 (c (n "mailchecker") (v "5.0.6") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1jb00w2gx8sy07li2gkvlbkdldjfjw6s43pgkb8f161wk224in78")))

(define-public crate-mailchecker-5.0.7 (c (n "mailchecker") (v "5.0.7") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1hnvj8ndlbsp7w6fjc6bgw1r298ry7x9r4a681mhwsf2jfr1pgn9")))

(define-public crate-mailchecker-5.0.8 (c (n "mailchecker") (v "5.0.8") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "11zfmzamk8kc0w3jsfp32n4sh609y7j24vh4b1c5gyvy4wdih8hx")))

(define-public crate-mailchecker-5.0.9 (c (n "mailchecker") (v "5.0.9") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "01cbcjm5yz7361vxsj6jdw05dwkqq8x34vs9zsyrc246z5xglr2c")))

(define-public crate-mailchecker-6.0.1 (c (n "mailchecker") (v "6.0.1") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1vc566546gm7wpa2hqksg5i9j2sbl7fcl3y6r6sk03l4al89b89j")))

(define-public crate-mailchecker-6.0.3 (c (n "mailchecker") (v "6.0.3") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "1cxindnd37b6kdad14j94s013hwy136qr70s0qglnh7cszmmv24f")))

(define-public crate-mailchecker-6.0.4 (c (n "mailchecker") (v "6.0.4") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "05cf3dhdphr9ny630adj5x5rs068is096bqshini0fy8ngsrmzjh")))

(define-public crate-mailchecker-6.0.5 (c (n "mailchecker") (v "6.0.5") (d (list (d (n "fast_chemail") (r "^0.9.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.1.2") (d #t) (k 2)))) (h "15xchpwmc4s58aprki3abl18wpa39dgpg0mw4nhd9n0zqqc4yq9p")))

