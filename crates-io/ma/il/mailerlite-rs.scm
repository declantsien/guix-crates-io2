(define-module (crates-io ma il mailerlite-rs) #:use-module (crates-io))

(define-public crate-mailerlite-rs-1.0.0 (c (n "mailerlite-rs") (v "1.0.0") (d (list (d (n "mockito") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6ys18xnxmiz2fpp7dign33h34yn15d305x3c1hglngvv5h89j3")))

