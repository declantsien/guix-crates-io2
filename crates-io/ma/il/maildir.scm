(define-module (crates-io ma il maildir) #:use-module (crates-io))

(define-public crate-maildir-0.1.0 (c (n "maildir") (v "0.1.0") (d (list (d (n "mailparse") (r "^0.5.0") (d #t) (k 0)))) (h "03pkb41fxzmayid3g91jly8idmj9igsnv0kcgzmvzr4fmpx18j82")))

(define-public crate-maildir-0.1.1 (c (n "maildir") (v "0.1.1") (d (list (d (n "mailparse") (r "^0.5.0") (d #t) (k 0)))) (h "0wvc8dg41ms0k1igr04lmi0lkiw0ccln6j84v9gdn8zznr4v9nc3")))

(define-public crate-maildir-0.1.2 (c (n "maildir") (v "0.1.2") (d (list (d (n "mailparse") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "1mwzaqpn4pw1hmnq156803j1h3mxp49npvs1lxwwb842629x8n81")))

(define-public crate-maildir-0.2.0 (c (n "maildir") (v "0.2.0") (d (list (d (n "mailparse") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1vgmsmvhs9imzknpbc627ffjzkwijdjg615ykkj5hl8mhfp7f9r1")))

(define-public crate-maildir-0.3.0 (c (n "maildir") (v "0.3.0") (d (list (d (n "mailparse") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "040959jl4m8abrzvq6z75ga8hvcxx7n4bdhw1i00mhhvbdxvps3g") (y #t)))

(define-public crate-maildir-0.3.1 (c (n "maildir") (v "0.3.1") (d (list (d (n "mailparse") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1dyks632fpkf0p6rw148034zars7mp0rm201r01zx73c7jy1i9lf")))

(define-public crate-maildir-0.3.2 (c (n "maildir") (v "0.3.2") (d (list (d (n "mailparse") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0kmbh075mm57bnwmmsjb5crdmhl8d5laq7m3nb03qdq0cmmqf0vj")))

(define-public crate-maildir-0.3.3 (c (n "maildir") (v "0.3.3") (d (list (d (n "mailparse") (r "^0.9") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0qb4n1npbbgx9x3ka1n6pq1xg3q3gpj3gv3wpi2rcgcfay7mgha9")))

(define-public crate-maildir-0.3.4 (c (n "maildir") (v "0.3.4") (d (list (d (n "mailparse") (r "^0.10") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0z5nk7nxig0fjmv3p80sxaszm0knif59yhi25llhb37k4qarcnih")))

(define-public crate-maildir-0.4.0 (c (n "maildir") (v "0.4.0") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.10") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0q99dd8sx4f5102b4j4kfjnvbn65drnf0f7kbbaspwravq9n9jm6")))

(define-public crate-maildir-0.4.1 (c (n "maildir") (v "0.4.1") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.10") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "005h68gpy9sk1h19g2wm3kdb800lyxg5gcbm7snlbgsbsc7pbq7k")))

(define-public crate-maildir-0.4.2 (c (n "maildir") (v "0.4.2") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.12") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0rxbz3kw32zr4a407151x1jfgvgvhh4sd55xpmpxadjp3afcxm04")))

(define-public crate-maildir-0.4.3 (c (n "maildir") (v "0.4.3") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "11dbrjhj341gz6qf1xgzkkjdq2b77a4zk4jq16lgnjgbm6r65qfw")))

(define-public crate-maildir-0.4.4 (c (n "maildir") (v "0.4.4") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "01ca8y5j4m7m07f0jv3xiad354l8qvids9a9jm7cfwviw4rzqdg2") (f (quote (("mmap" "memmap"))))))

(define-public crate-maildir-0.4.5 (c (n "maildir") (v "0.4.5") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0i0i9dai5caziqy979c43yhj7bmrwfk4f9d2zxf69sadskkp0qd2") (f (quote (("mmap" "memmap")))) (y #t)))

(define-public crate-maildir-0.5.0 (c (n "maildir") (v "0.5.0") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0pivq6njjmfnf0jn6i8sihbfgly6v674zwncd6f5nwiw79lz9p3a") (f (quote (("mmap" "memmap"))))))

(define-public crate-maildir-0.6.0 (c (n "maildir") (v "0.6.0") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1wrznchiq9zba859k345i1iq20wdzsa8l975gyckbxsnn0g4hirw") (f (quote (("mmap" "memmap"))))))

(define-public crate-maildir-0.6.1 (c (n "maildir") (v "0.6.1") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1da5xrj0r8n6vrxaf3vy457n8x3y2j71a294r6njcnj6mlc6iqzq") (f (quote (("mmap" "memmap"))))))

(define-public crate-maildir-0.6.2 (c (n "maildir") (v "0.6.2") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.7") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0xcp2c7jm4jvccwpd5g3i5svsgg2j20z9d9llm0fv1vy3yy2daiy") (f (quote (("mmap" "memmap2"))))))

(define-public crate-maildir-0.6.3 (c (n "maildir") (v "0.6.3") (d (list (d (n "gethostname") (r "^0.2.3") (d #t) (k 0)) (d (n "mailparse") (r "^0.14") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0vs2phdvv3irrhl0dvh4i2awpc9p2blyk3wzm7np4llj5bahhbax") (f (quote (("mmap" "memmap2"))))))

(define-public crate-maildir-0.6.4 (c (n "maildir") (v "0.6.4") (d (list (d (n "gethostname") (r "^0.2.3") (d #t) (k 0)) (d (n "mailparse") (r "^0.14") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0dqlkvhcrigs4y5vg0jf8ccgwns8jj85cjp6vsgj3f1sfkk6m6l7") (f (quote (("mmap" "memmap2"))))))

