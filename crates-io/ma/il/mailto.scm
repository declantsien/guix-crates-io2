(define-module (crates-io ma il mailto) #:use-module (crates-io))

(define-public crate-mailto-0.1.0 (c (n "mailto") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "16qd5sjs6xhmwxnlxi6gn3rzg7vymn35q3fwq7a13n64p3qwdh4m")))

