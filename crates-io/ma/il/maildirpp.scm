(define-module (crates-io ma il maildirpp) #:use-module (crates-io))

(define-public crate-maildirpp-0.0.1 (c (n "maildirpp") (v "0.0.1") (d (list (d (n "gethostname") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8") (d #t) (k 2)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "02n85jmycwmfkgyaq47d6zg6f2yg0f2nsnwp0l91v84bary2i94l")))

(define-public crate-maildirpp-0.0.2 (c (n "maildirpp") (v "0.0.2") (d (list (d (n "gethostname") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8") (d #t) (k 2)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1rg6y2135534k9q1w512n8hid7p2qkzq5khy385ib4jm13xm57vk")))

(define-public crate-maildirpp-0.1.0 (c (n "maildirpp") (v "0.1.0") (d (list (d (n "gethostname") (r "^0.4.3") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8.2") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "07s5q3dgvgc0hxaqqhzc1agipi6mxzfhjs99ig4598cmc9jsyi6m")))

(define-public crate-maildirpp-0.2.0 (c (n "maildirpp") (v "0.2.0") (d (list (d (n "gethostname") (r "^0.4.3") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8.2") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "1n7j5vqkmcgs520zv09504nirc8nhigssi45f3y75b2vabdid9pc")))

(define-public crate-maildirpp-0.3.0 (c (n "maildirpp") (v "0.3.0") (d (list (d (n "gethostname") (r "^0.4.3") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8.2") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "0v53rr4xzzi0n157y966kay5kqnp63vkjarc2dmxzdk3fc7mfnlv")))

(define-public crate-maildirpp-0.0.3 (c (n "maildirpp") (v "0.0.3") (d (list (d (n "gethostname") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mail-parser") (r "^0.8") (d #t) (k 2)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "159cfslzdbfg7jzrk3zmac4wbm2vw36m430g8mc6ll2xscqyzfbf")))

