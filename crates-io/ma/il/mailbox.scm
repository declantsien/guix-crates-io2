(define-module (crates-io ma il mailbox) #:use-module (crates-io))

(define-public crate-mailbox-0.1.0 (c (n "mailbox") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nwwix0apg0lqym5nli5dnigw8s8cy13vamkbmwrhy5jc7hkkq5k")))

(define-public crate-mailbox-0.1.1 (c (n "mailbox") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "case") (r "^0.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "064f5ib8im24vk9zha30firdhlcnhfv74h5bblkm3jhy41z0f52f")))

(define-public crate-mailbox-0.1.2 (c (n "mailbox") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "casing") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2.1") (d #t) (k 0)))) (h "0lbsr2l3scazck0wl8ik7glklv2x3c5ihzdcrkvrh18q9614clza")))

(define-public crate-mailbox-0.1.3 (c (n "mailbox") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "casing") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2.1") (d #t) (k 0)))) (h "0gnif0bxz78l0yi9sw6p6r30ng4fkk8gqcm5jlchsfkc4775vzia")))

(define-public crate-mailbox-0.1.4 (c (n "mailbox") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "casing") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3") (d #t) (k 0)))) (h "093q9q8qllzpscix0sv3s0cl9sbcb4y22spf6lyi0q5b3azi58xl")))

(define-public crate-mailbox-0.2.0 (c (n "mailbox") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "casing") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3") (d #t) (k 0)))) (h "1xfrbvns4f6q67p26wfqx223qzpipa8g17wlr7hhbri42bxf6b30")))

