(define-module (crates-io ma il mailin) #:use-module (crates-io))

(define-public crate-mailin-0.1.0 (c (n "mailin") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "149iwqmxidy5y5yd3csixk24bz0aw41a6mnsjk25z4ixbs92mzf6")))

(define-public crate-mailin-0.1.1 (c (n "mailin") (v "0.1.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0p7smb3069p76wgbh1gf0ck6pwcp5hg9np9gb0sp10zwf2p3f96a")))

(define-public crate-mailin-0.1.2 (c (n "mailin") (v "0.1.2") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0fcsymwfx4ffhsz3b080dayvp0ik2pvjqk70caz58dqrg79i6cdb")))

(define-public crate-mailin-0.2.0 (c (n "mailin") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0v1y5ydlxgadqrarlbz7r74bs6c2mwic775f8amhlksxfbsk6cq5")))

(define-public crate-mailin-0.3.0 (c (n "mailin") (v "0.3.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0q6ymy50c3fbyvx6431b79h8yxwfy0pi4x3qmf8l5pwx24a1xdsz")))

(define-public crate-mailin-0.4.0 (c (n "mailin") (v "0.4.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "1lgma1v84k3fm9yksvldpccqg7c2njjx0yh9pvp9pxq17dagy9g0")))

(define-public crate-mailin-0.4.1 (c (n "mailin") (v "0.4.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "1zhqwns84a39lnwkciybyb0mwp4hc3d516sqh9k9mm8d3s7g62f3")))

(define-public crate-mailin-0.5.0 (c (n "mailin") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "1f24ng5ksxcv4k4bpf09r7x45xkww8dra3vq7l9642jd0x4zwcjp")))

(define-public crate-mailin-0.6.0 (c (n "mailin") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0hlg722sq8asqrhdxs4c4cabz934rya7j7hz1x29jj4vsck2wlgq")))

(define-public crate-mailin-0.6.1 (c (n "mailin") (v "0.6.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "02l9vaw790fqpihyzpcrmrl9qax3n32isiipmsnbmxiwdlfl3l14")))

(define-public crate-mailin-0.6.2 (c (n "mailin") (v "0.6.2") (d (list (d (n "base64-compat") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0i5sfz588kq5k6v5ydzv9dpnr8mcbh65mr93i6f1gd8mqqz4iwyp")))

(define-public crate-mailin-0.6.3 (c (n "mailin") (v "0.6.3") (d (list (d (n "base64-compat") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0adl4ixb126r00rf21fgzh4r1xwsqhsrirqbzlhc7adbgilwr1fn")))

(define-public crate-mailin-0.6.4 (c (n "mailin") (v "0.6.4") (d (list (d (n "base64-compat") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ternop") (r "^1.0") (d #t) (k 0)))) (h "0ynm84jhxxpw65xm239jg3mvdf0xhjj87g9iplfgpa0k17xgw27z")))

