(define-module (crates-io ma il mailgang) #:use-module (crates-io))

(define-public crate-mailgang-0.1.0 (c (n "mailgang") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lettre") (r "^0.9.3") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ai2mpi036gnazycgik97a221n3dr3qkbk4bk5hbwd174c8c1vr8")))

(define-public crate-mailgang-0.1.1 (c (n "mailgang") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lettre") (r "^0.9.3") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qrawki54vafrxb83nwfarqps3hs210rsq0fq8w4j1nzdrh86mld")))

(define-public crate-mailgang-0.1.2 (c (n "mailgang") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lettre") (r "^0.9.3") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x6rz43qcpn3xch1c3ng90blc29d8xbwy8yx7vvyxv7xwrw2s60c")))

(define-public crate-mailgang-0.1.3 (c (n "mailgang") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lettre") (r "^0.9.3") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "120dsrqczq6adi8gl25mrn5dqrgzwf6qglkj0b4l6sskz46vh1qg")))

(define-public crate-mailgang-0.2.0 (c (n "mailgang") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sparkpost") (r "^0.5.4") (d #t) (k 0)))) (h "108dxgrv8yhldv9jj2m79gaw9l3r2blalan8wnz12v10v5wb93sk")))

(define-public crate-mailgang-0.2.1 (c (n "mailgang") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sparkpost") (r "^0.5.4") (d #t) (k 0)))) (h "0k2zr2mmfx3ljr1h3ramanni2as7xyn7cfhzrzplxl8iaxw6r4yy")))

(define-public crate-mailgang-0.2.2 (c (n "mailgang") (v "0.2.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sparkpost") (r "^0.5.4") (d #t) (k 0)))) (h "0b7nrinbix35r86hzdvk184hjmxca54q8912cds560zpr400jfnd")))

