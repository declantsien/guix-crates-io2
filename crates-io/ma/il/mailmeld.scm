(define-module (crates-io ma il mailmeld) #:use-module (crates-io))

(define-public crate-mailmeld-1.0.0 (c (n "mailmeld") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "lettre") (r "^0.11.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k60bbrhxkijl50lpb1rhcrby2hnfywqvbgrhj3q074gp4v81fda") (y #t)))

