(define-module (crates-io ma ni manishs-test-crate-11) #:use-module (crates-io))

(define-public crate-manishs-test-crate-11-0.1.0 (c (n "manishs-test-crate-11") (v "0.1.0") (h "09ldh6nllhp1aj9zsp8vhqcrlqi1qrqzkaqr1lngh38c6x5p9r93")))

(define-public crate-manishs-test-crate-11-0.1.1 (c (n "manishs-test-crate-11") (v "0.1.1") (h "0p5canz0p52bwh7qkm98xml9rbprf6k8qp5fdiji111w4ynp6lgr")))

