(define-module (crates-io ma ni manifest-gen) #:use-module (crates-io))

(define-public crate-manifest-gen-0.1.0 (c (n "manifest-gen") (v "0.1.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rxd7a6xnybc2y4bag0m7k0ki44xca7by44f4660fb44pfb92rn3")))

(define-public crate-manifest-gen-0.1.1 (c (n "manifest-gen") (v "0.1.1") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n699vj3dgjhrsgx91vj267vzwnvyvdk8if5jwx3j47iarv3gjcb")))

(define-public crate-manifest-gen-0.2.1 (c (n "manifest-gen") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dkfldwcdw13w1164hfqzmdvpnq5s2kfcg6k77h3fg1k7rxp390x")))

(define-public crate-manifest-gen-0.2.2 (c (n "manifest-gen") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gi205hrz5dcdbc20lhv55zx5kkfzxjryh2603q79919qw5h54sw")))

(define-public crate-manifest-gen-0.2.3 (c (n "manifest-gen") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cvvfl7whmq0gj4pqfaj283sb6j5ramw9sb1s3n9nmha8c5qnlrd")))

