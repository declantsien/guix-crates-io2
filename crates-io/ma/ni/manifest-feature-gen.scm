(define-module (crates-io ma ni manifest-feature-gen) #:use-module (crates-io))

(define-public crate-manifest-feature-gen-0.1.0 (c (n "manifest-feature-gen") (v "0.1.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15.0") (d #t) (k 0)))) (h "16yd1d7brcf2yw6xqim2c8alcl28zg87rpj5hv3nypsajj8jmnjz")))

(define-public crate-manifest-feature-gen-0.1.1 (c (n "manifest-feature-gen") (v "0.1.1") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15.0") (d #t) (k 0)))) (h "006vj7vnxah8zwzs5hx16c9zm9gn92ha1xqnmpkap1m947sklzmn")))

(define-public crate-manifest-feature-gen-0.1.2 (c (n "manifest-feature-gen") (v "0.1.2") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15.0") (d #t) (k 0)))) (h "070mj7scl4m27prsys7ad4f73clbi5gwlmmw3l0z7ir6b66filpj")))

(define-public crate-manifest-feature-gen-0.1.3 (c (n "manifest-feature-gen") (v "0.1.3") (d (list (d (n "fallible-iterator") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "16w37a2ii6vac9sflnqjmn1qxh19wvyhk1ih690ihq4awspgbwjy")))

