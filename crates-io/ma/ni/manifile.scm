(define-module (crates-io ma ni manifile) #:use-module (crates-io))

(define-public crate-manifile-0.0.0 (c (n "manifile") (v "0.0.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0zlc4i8q2dggyphgzj9rnzp1vnyj8x946wzx1rsw5kn5bray2jck") (f (quote (("std" "thiserror") ("default" "std") ("alloc")))) (r "1.56")))

