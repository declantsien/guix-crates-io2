(define-module (crates-io ma ni manish_this_is_a_test) #:use-module (crates-io))

(define-public crate-manish_this_is_a_test-0.1.0 (c (n "manish_this_is_a_test") (v "0.1.0") (h "0j04f69f0f18xrljrx24mkc0nf9sz6n2p4l4q8s99scbcb9ps7jn")))

(define-public crate-manish_this_is_a_test-0.1.1 (c (n "manish_this_is_a_test") (v "0.1.1") (h "0pzs5chjc3d245g5grvbzrc4dqw9crx2d5hmrllr13ly81rxic1z")))

(define-public crate-manish_this_is_a_test-0.1.2 (c (n "manish_this_is_a_test") (v "0.1.2") (h "0gk78xhwccb75pabyc1b2l0ww8xvl85ywdjkrbzn3d6cf0lc847g")))

