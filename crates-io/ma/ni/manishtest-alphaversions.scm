(define-module (crates-io ma ni manishtest-alphaversions) #:use-module (crates-io))

(define-public crate-manishtest-alphaversions-1.0.0-experimental (c (n "manishtest-alphaversions") (v "1.0.0-experimental") (h "13095c61jp8xjl4izrw2gv95kfb58cmhjz3m422n3y1wxnpjg58a")))

(define-public crate-manishtest-alphaversions-1.1.0-experimental (c (n "manishtest-alphaversions") (v "1.1.0-experimental") (h "0f8acp38mpx760akxxm2wqjk5xz7z8wfiqi6rs0z7g4qny7pklni")))

(define-public crate-manishtest-alphaversions-1.1.0 (c (n "manishtest-alphaversions") (v "1.1.0") (h "1bs61b857nbj9vchbd7had51yxk5z1rbzdd2lbj2hxagi3a1qblc")))

