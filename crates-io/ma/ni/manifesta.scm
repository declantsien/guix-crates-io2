(define-module (crates-io ma ni manifesta) #:use-module (crates-io))

(define-public crate-manifesta-0.1.0 (c (n "manifesta") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.16.1") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dgvfzngn1wjjdzapwwb8ca8s1vvkwgwh8v5h2p85wq6xcxsxl91")))

(define-public crate-manifesta-0.1.1 (c (n "manifesta") (v "0.1.1") (d (list (d (n "attohttpc") (r "^0.16.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yare") (r "^1.0.0") (d #t) (k 2)))) (h "00n7sqy4vnamkk55x2yr958ihddslnw968lbzb784vnpaxcyzr62")))

