(define-module (crates-io ma ni manifold-markets) #:use-module (crates-io))

(define-public crate-manifold-markets-0.1.0 (c (n "manifold-markets") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0dplrkvcas53pnjacni2mb55z85hfjr3d9pa8pf0wf8j7bkjh7vm")))

