(define-module (crates-io ma ct mactypes-sys) #:use-module (crates-io))

(define-public crate-MacTypes-sys-1.0.0 (c (n "MacTypes-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0xwkpiv8hrwya1gv7n7m5187ncqbmzkjwl852mnn8i0ccm5zn3si")))

(define-public crate-MacTypes-sys-1.0.1 (c (n "MacTypes-sys") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wpghb36sbbignjkspgqxy4andrsxy05swc2xw35z7m4qyzvqnz8")))

(define-public crate-MacTypes-sys-1.0.2 (c (n "MacTypes-sys") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nl60rhav0042vibsadfipxdq48jghm9dvjqdh9xyajanndzllsh")))

(define-public crate-MacTypes-sys-1.0.3 (c (n "MacTypes-sys") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "090pziwqpnsj1nlwh6iw3dr8jp9k58klazmj9wxkqk24w649k860")))

(define-public crate-MacTypes-sys-1.0.4 (c (n "MacTypes-sys") (v "1.0.4") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0nhcvryi111n4wkgrpdhnhlx1i6r8j8rzzp082jc503v8dq7d651")))

(define-public crate-MacTypes-sys-1.1.0 (c (n "MacTypes-sys") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1yqs4blabbb2hvwlvnik908k52d1z4gyzvc3ba6ix61r2r7caizx")))

(define-public crate-MacTypes-sys-1.2.0 (c (n "MacTypes-sys") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "02bxxdnnc5kdhqil9s6valnw75zdyx27l6jp7fzjwjr3372wwggf") (f (quote (("use_std" "libc/use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-MacTypes-sys-1.3.0 (c (n "MacTypes-sys") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "16261z2sg7bp6h99l924vfqrz8jjv4r72z2i3251kqjak4ry1fvx") (f (quote (("use_std" "libc/use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-MacTypes-sys-2.0.0 (c (n "MacTypes-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "19b04dakijil01hf77gv2xbar7q32fcnyq8wjh5y6qjnm5hj4y8s") (f (quote (("use_std" "libc/use_std") ("default" "use_std")))) (y #t)))

(define-public crate-MacTypes-sys-2.0.1 (c (n "MacTypes-sys") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0zyjw09m7bc4lv7w5fk1qn5z6xirsc0al9mvda3ccgbzqcrdpng3") (f (quote (("use_std" "libc/use_std") ("default" "use_std"))))))

(define-public crate-MacTypes-sys-2.0.2 (c (n "MacTypes-sys") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1zygx2ixi3wqs8wzxazm6qiaxhbh0fqmvg8qqiqy9gdxqb1j8il2") (f (quote (("use_std" "libc/use_std") ("default" "use_std"))))))

(define-public crate-MacTypes-sys-2.1.0 (c (n "MacTypes-sys") (v "2.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0vyq3akxh9vk0p28m75j9ppn4d60xarb8kz1mv9a8cycn78g1yga") (f (quote (("use_std" "libc/use_std") ("default" "use_std"))))))

