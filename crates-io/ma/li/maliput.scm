(define-module (crates-io ma li maliput) #:use-module (crates-io))

(define-public crate-maliput-0.1.0 (c (n "maliput") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "maliput-sdk") (r "^0.1.2") (d #t) (k 0)) (d (n "maliput-sys") (r "^0.1.0") (d #t) (k 0)))) (h "14n8q64dmll457fh3srnl8abdmhwyqdqy1n4zbf2wfjdp1ldr2pn") (y #t)))

(define-public crate-maliput-0.1.1 (c (n "maliput") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "maliput-sdk") (r "^0.1.2") (d #t) (k 0)) (d (n "maliput-sys") (r "^0.1.0") (d #t) (k 0)))) (h "12vb502692zxlnpmgpvb8323n31v22f18y4a0zsp1ha75s2wk4gb")))

(define-public crate-maliput-0.1.2 (c (n "maliput") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "maliput-sdk") (r "^0.1.3") (d #t) (k 0)) (d (n "maliput-sys") (r "^0.1.1") (d #t) (k 0)))) (h "193cbj0x7cx108kdd0mivq1adnc5mi10paghcx1kkxy6v6s7vjqf")))

