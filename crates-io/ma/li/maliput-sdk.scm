(define-module (crates-io ma li maliput-sdk) #:use-module (crates-io))

(define-public crate-maliput-sdk-0.1.0 (c (n "maliput-sdk") (v "0.1.0") (h "0mlfkvk946bismn36bx1bg7l0kd89k1rm654nkvwnikgkbbg618i") (y #t) (l "maliput-sdk")))

(define-public crate-maliput-sdk-0.1.1 (c (n "maliput-sdk") (v "0.1.1") (h "1dnn43nqdnszs7zmqadk1vah1cp89kxzq78p3yfnh76h2rgwb6dx") (y #t) (l "maliput-sdk")))

(define-public crate-maliput-sdk-0.1.2 (c (n "maliput-sdk") (v "0.1.2") (h "19qlypdwi8yrprvqwkhj8hmwwfn1nq7ivb3141s2sc2jzw0zn84a") (l "maliput-sdk")))

(define-public crate-maliput-sdk-0.1.3 (c (n "maliput-sdk") (v "0.1.3") (h "1vq3czra8w9zs1hsx40si8hgdfd6g9n3qdagjpddzl1nzn1hhjp1") (l "maliput-sdk")))

