(define-module (crates-io ma li malignius) #:use-module (crates-io))

(define-public crate-malignius-0.0.1 (c (n "malignius") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)))) (h "02ic7q4m57g5x5cg02hz4yndw91xangghgcxkf7bpx4v3l11jlzc")))

