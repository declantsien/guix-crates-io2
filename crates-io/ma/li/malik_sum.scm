(define-module (crates-io ma li malik_sum) #:use-module (crates-io))

(define-public crate-malik_sum-0.1.0 (c (n "malik_sum") (v "0.1.0") (h "0x5qr38l85wp19yvs6fzpgl2qv0jrw4a393njw4kzbxgmymgpqpv")))

(define-public crate-malik_sum-0.1.1 (c (n "malik_sum") (v "0.1.1") (h "1wdhqqyz0ysp1n3w4hpfyhpv78b2gs1pmxdz4cfcck5ifw1k68x9")))

(define-public crate-malik_sum-0.1.2 (c (n "malik_sum") (v "0.1.2") (h "0315ayrc88rzif5izfrd5iyqzymxwwp6bw714pz39mbv9kavm0vn") (y #t)))

(define-public crate-malik_sum-0.1.3 (c (n "malik_sum") (v "0.1.3") (h "0qlaps3qvd134msmaj998c3kwwda3f51vqv5n3731cxlaid1jj08")))

(define-public crate-malik_sum-0.1.4 (c (n "malik_sum") (v "0.1.4") (h "04gsqnc153w47wkb7p43lb4kbkbx7ywx4pf704s58b8rmwxjz77r")))

(define-public crate-malik_sum-0.1.5 (c (n "malik_sum") (v "0.1.5") (h "07f6x7s7jlgij7sl8a3dgb1ffid2kdff76b88ly51ax2003hqpff")))

(define-public crate-malik_sum-0.1.6 (c (n "malik_sum") (v "0.1.6") (h "12b7hphl3lhylin49zx1hgbmy0qb70b892gmqi24f8qz58ddxccw")))

(define-public crate-malik_sum-0.1.7 (c (n "malik_sum") (v "0.1.7") (h "0jsiy4wjmbmdjhh22lwdg523m7l5bd698zyqb51xbcwayzix0zdl")))

