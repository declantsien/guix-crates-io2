(define-module (crates-io ma li maligned) #:use-module (crates-io))

(define-public crate-maligned-0.0.0 (c (n "maligned") (v "0.0.0") (h "0w97ds6pj400a82pss7v2fnqdsssa2fgsbr0bgd02zq4h3wg512x")))

(define-public crate-maligned-0.1.0 (c (n "maligned") (v "0.1.0") (h "01cafaw9qpxpgian0acz2jswhqkdsa3g2ryivsifmdr6jcyj3d0i") (f (quote (("default" "alloc" "align-128k") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

(define-public crate-maligned-0.2.0 (c (n "maligned") (v "0.2.0") (h "1dj6jc1m16nf5sj020rn1l1yhpg3aqb2w6lriirjwj20hkfcj4w4") (f (quote (("default" "alloc") ("clippy") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

(define-public crate-maligned-0.2.1 (c (n "maligned") (v "0.2.1") (h "0r1lx0ll7sk8gzx3kn560g9fyg9f4frji2p4jgr7g3r8x35w723y") (f (quote (("default" "alloc") ("clippy") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

