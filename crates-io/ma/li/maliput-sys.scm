(define-module (crates-io ma li maliput-sys) #:use-module (crates-io))

(define-public crate-maliput-sys-0.1.0 (c (n "maliput-sys") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.78") (d #t) (k 1)) (d (n "maliput-sdk") (r "^0.1.2") (d #t) (k 0)))) (h "0wny603bxh7wwxddj2c5gfbj87n4a00p0p1qln19lh8nwcp8ac2h")))

(define-public crate-maliput-sys-0.1.1 (c (n "maliput-sys") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.78") (d #t) (k 1)) (d (n "maliput-sdk") (r "^0.1.3") (d #t) (k 0)))) (h "1v63jvn9aj041bsqckk5rd9m9071if25fryr12qm4d20v1qxx2x2")))

