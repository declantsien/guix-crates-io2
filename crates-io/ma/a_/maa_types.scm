(define-module (crates-io ma a_ maa_types) #:use-module (crates-io))

(define-public crate-maa_types-0.3.0 (c (n "maa_types") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_default") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hi75nf7zb23rgswarg1z1wjna1mcikgnnsrczj1igb3drs2j5xk") (f (quote (("task") ("message") ("default" "task" "message"))))))

(define-public crate-maa_types-0.3.1 (c (n "maa_types") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_default") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a9g82cffh193qmyh4p6rzn63qh568samjlha5s6s8k0v0b640v0") (f (quote (("task") ("message") ("default" "task" "message"))))))

(define-public crate-maa_types-0.3.2 (c (n "maa_types") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_default") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "157b40i4bji1xxgisy7sd6xhvqdksxkai9kj5rabs54fazjcwii4") (f (quote (("task") ("message") ("default" "task" "message"))))))

