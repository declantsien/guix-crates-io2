(define-module (crates-io ma ku makudaun) #:use-module (crates-io))

(define-public crate-makudaun-0.0.0 (c (n "makudaun") (v "0.0.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.7") (d #t) (k 0)))) (h "1dvma3qjvd33w6sqy583h7n893cc64kjj78wcsph4gik7afdgns3")))

(define-public crate-makudaun-0.0.1 (c (n "makudaun") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.7") (d #t) (k 0)))) (h "1riiqk3k64lxpcja2lw20wm7rfidpagxfrpcmqyrnk3rvwax3fzk")))

(define-public crate-makudaun-0.0.2 (c (n "makudaun") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.7") (d #t) (k 0)))) (h "137wg6i7nyng1xjn31im22afxh7rjjn74zbj0c7s42ppg48hqvzn")))

