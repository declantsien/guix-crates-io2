(define-module (crates-io ma ka makai_waveform_db) #:use-module (crates-io))

(define-public crate-makai_waveform_db-0.1.0 (c (n "makai_waveform_db") (v "0.1.0") (d (list (d (n "indiscriminant") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lzkqi0ycp2yjax1rc2pqkxd415k3kvsc34b6n3vxxiwg7hvsdgm")))

