(define-module (crates-io ma sa masala) #:use-module (crates-io))

(define-public crate-masala-0.1.0 (c (n "masala") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10ykb8qhaqnbdxxmdk5hx10wkzzk4pv15ai8sf55gnlmn48zn0fv")))

(define-public crate-masala-0.2.0 (c (n "masala") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mnjqipjrvm6cqs1s8909g7piwh4mngmhhkw6b61lcgic22wxh5i")))

(define-public crate-masala-0.2.1 (c (n "masala") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kxayy5c603lskjk7x5wf6r8k1v559vw3lx38mnj27yamharx5j1")))

