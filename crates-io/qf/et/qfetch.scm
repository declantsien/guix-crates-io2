(define-module (crates-io qf et qfetch) #:use-module (crates-io))

(define-public crate-qfetch-0.1.0 (c (n "qfetch") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1s8xmv6xk1d823j7iiydapibslkrl7ialz8qs67jhk4nayj5m2ll") (y #t) (r "1.60")))

(define-public crate-qfetch-0.1.1 (c (n "qfetch") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "13xkxlacymis0a8am4dbm2x9zhqpijxpc41ppqv5pcgbvp12w6z8") (r "1.60")))

(define-public crate-qfetch-0.2.0 (c (n "qfetch") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0m4bjc4fgxl2fipdzi5zqrbllj84i4c8wvzpra0szywh40vwslx5") (y #t) (r "1.60")))

(define-public crate-qfetch-0.2.1 (c (n "qfetch") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0a2r9najlzji8rmfzah6dlw93bci173ipw49fq1aqyr3q93yacx5") (r "1.60")))

(define-public crate-qfetch-0.3.0 (c (n "qfetch") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0r7lgjil6v96k90limj0xsjhfl1bmrriiri0v4n540kad04abq71") (r "1.60")))

(define-public crate-qfetch-0.4.0 (c (n "qfetch") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1kw519caq6jdb74qrdnil7243falz8n3vqwix8g5694p90w820zw") (r "1.60")))

(define-public crate-qfetch-0.5.0 (c (n "qfetch") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qxm6kyfivfbkc2fg2sxymfygvvpzyp0b736gh5zrf3l4z2ymbj4") (r "1.60")))

(define-public crate-qfetch-0.6.0 (c (n "qfetch") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0padl4rsl5bpg0647ix65kzpm3w0kn217mcq7vwa9xq4vwv6l5p5") (r "1.61")))

(define-public crate-qfetch-0.7.0 (c (n "qfetch") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bf32mngyb80d1ds7wpr43v5zcqd3bqhz75n9ba876kx5075bcfp") (r "1.61")))

(define-public crate-qfetch-0.8.0 (c (n "qfetch") (v "0.8.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os_type") (r "^2.4") (d #t) (k 0)))) (h "1irn3gxbi0mg5g245hb57s3d9l9xarvbkyczhs006ldzf2mnx6v9") (r "1.61")))

(define-public crate-qfetch-0.9.0 (c (n "qfetch") (v "0.9.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os_type") (r "^2.4") (d #t) (k 0)))) (h "0lawn2k2kcqwljgssd289d7vwxmqyka00syp7wyz587xyzkkgl6s") (y #t) (r "1.61")))

(define-public crate-qfetch-0.9.1 (c (n "qfetch") (v "0.9.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os_type") (r "^2.4") (d #t) (k 0)))) (h "016vssvvbwjn8xkkvbq4a3qc4v6bls5zx7dqkkp1fx0m2ri328ll") (r "1.61")))

