(define-module (crates-io qf il qfile) #:use-module (crates-io))

(define-public crate-qfile-0.1.0 (c (n "qfile") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1mncva3qy1vfdix6x58ab7gx9ffyf3a5m5nn74gybl6k303v6227") (y #t)))

(define-public crate-qfile-0.1.1 (c (n "qfile") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1kq47nr0dfqxwmp0f279qy0zgz3rw1lzgakcijqfai1wpv8276ns") (y #t)))

(define-public crate-qfile-0.1.2 (c (n "qfile") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1hcc4i3nawsxq5g3nbvi621268b47xsi969cfsxsrbbkcfzzznq0") (y #t)))

(define-public crate-qfile-0.1.3 (c (n "qfile") (v "0.1.3") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "003q0bggbhwm9z4xaa0c88z49l25iyzmz9qnbsgwsqhp4y1cwx78") (y #t)))

(define-public crate-qfile-0.1.4 (c (n "qfile") (v "0.1.4") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0n30pr1806lmmalzd2a8q9drlikzc2bwhyvva6ckfkrxnwc6xhrh") (y #t)))

(define-public crate-qfile-0.1.5 (c (n "qfile") (v "0.1.5") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "04cpcang5qbnnrksi7dds1cddm2pqxaqxq297pllik3wd6yv20h0") (y #t)))

(define-public crate-qfile-0.1.6 (c (n "qfile") (v "0.1.6") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0pjn1v3vd88s15wdsai5a7srkvs1ylvcay0if0d770cqpayhcjpq") (y #t)))

(define-public crate-qfile-1.0.0 (c (n "qfile") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "02fgr0as4r8v5kl8jhxw2rc8db06v97q868ihd20pmjnjf1rz8hg") (y #t)))

(define-public crate-qfile-1.0.1 (c (n "qfile") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0dzkqpqca7cr09r58j7flf2lcwzzi8zms7haynvkmlgi37a29ryl") (y #t)))

(define-public crate-qfile-1.1.0 (c (n "qfile") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0f5jzvkhrmilgddk2mnj7dncsw6lqmakvmrzqpx5wzwaz6h722cb") (y #t)))

(define-public crate-qfile-1.1.1 (c (n "qfile") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1ci6rx4gm35269b2qpcbcjh93rm4hzdgrca1q256x8ysy3213pjy") (y #t)))

(define-public crate-qfile-1.1.2 (c (n "qfile") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0x6a4jgkq1ca2add9bkrcyj3lzzrdajsfh2im9l5qxnrk3621vl3") (y #t)))

(define-public crate-qfile-1.1.3 (c (n "qfile") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "17y3b39pzqkzpxab9y8j4p308dsmizw6h0kgrms2z4cvssp0cs9r") (y #t)))

(define-public crate-qfile-1.1.4 (c (n "qfile") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "02gi6knndck9yq5ldbsi3ymlx6zpdl421ddb9bsn69h54mqiw5r9") (y #t)))

(define-public crate-qfile-2.0.0 (c (n "qfile") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "10p72qiacvclhifdphcgvzbydw39yv85f79yhpz9q0d26fzjx7v4") (y #t)))

(define-public crate-qfile-2.1.0 (c (n "qfile") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "10lrly2jxv0kcdvbpkqhqspp2drlgnrpcwfqbm7xcdh61jil9n75") (y #t)))

(define-public crate-qfile-2.2.0 (c (n "qfile") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "13rmxm3nyk49lfrijg8l2pgnw79y787va423f0pbsj4hhy5zp8n6") (y #t)))

(define-public crate-qfile-2.2.1 (c (n "qfile") (v "2.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0b9vbi3ikah0yy31kpa5jl7gvkidcm2l42c12waiah1i9fk7ykzl") (y #t)))

(define-public crate-qfile-2.2.2 (c (n "qfile") (v "2.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0zsd7qdmym3bk0hp27wif7b50j4x42pmkjn4fb8shk465bignbf7") (y #t)))

(define-public crate-qfile-2.2.3 (c (n "qfile") (v "2.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0n3iq94fdfcws3p79n1a760agb9dw82k879hjjcxxr1ax772ck3x") (y #t)))

(define-public crate-qfile-3.0.0 (c (n "qfile") (v "3.0.0") (d (list (d (n "async-fs") (r "^1.6.0") (d #t) (k 0)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.62") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "080j9l27fv6yj3x1yjm40r13cd13zyjpwggllg380kbjk54k53n1") (y #t)))

(define-public crate-qfile-3.0.1 (c (n "qfile") (v "3.0.1") (d (list (d (n "async-fs") (r "^1.6.0") (d #t) (k 0)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.62") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "109v47kg4z0as5r0imch3w8lhw4j9h3pr1s03179a4m3ix55cjdg")))

