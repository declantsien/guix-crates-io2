(define-module (crates-io qf ra qframework) #:use-module (crates-io))

(define-public crate-qframework-0.1.0 (c (n "qframework") (v "0.1.0") (h "0f63aad2fn17zbx0rxam4zisf0s3zxq6bzzl91alwjilb6r5b25m")))

(define-public crate-qframework-0.1.1 (c (n "qframework") (v "0.1.1") (h "0mhx3dv5phkx8ql1l6y8x8ks89kmkrzdynkr9avh6d51kvmmihx4")))

(define-public crate-qframework-0.1.2 (c (n "qframework") (v "0.1.2") (h "1r8gcyq0f9739crzmlpyawmk13glk25fs48ydgg8mxlky6c1nbm2")))

