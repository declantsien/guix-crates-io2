(define-module (crates-io yx y- yxy-ffi) #:use-module (crates-io))

(define-public crate-yxy-ffi-0.1.0-alpha.1 (c (n "yxy-ffi") (v "0.1.0-alpha.1") (d (list (d (n "yxy") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1gxfbd96dli024ygsy71dxihqij2ixm73xf3g7lazmpn2arfxzkh") (y #t)))

(define-public crate-yxy-ffi-0.1.0-alpha.2 (c (n "yxy-ffi") (v "0.1.0-alpha.2") (d (list (d (n "yxy") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "1ipsnqjws1lsqc158rrgs4alq4ghm9ravj63211vyqyb0458sic7") (y #t)))

(define-public crate-yxy-ffi-0.1.0-alpha.3 (c (n "yxy-ffi") (v "0.1.0-alpha.3") (d (list (d (n "yxy") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "0n2l678jlhxdc9d62vwy70j2kvlqvx7h1a3pjcblvx2ddazyhdmx") (y #t)))

(define-public crate-yxy-ffi-0.1.0-alpha.4 (c (n "yxy-ffi") (v "0.1.0-alpha.4") (d (list (d (n "yxy") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "1iqs9kymspihhrhryqjxc4d7hy322hk6jici9vyfzr09z94hs874") (y #t)))

(define-public crate-yxy-ffi-0.1.0-alpha.5 (c (n "yxy-ffi") (v "0.1.0-alpha.5") (d (list (d (n "yxy") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "13a7x92x2v7rsmc6s2d08mvrn0m4ys9p12l5qvyp97qxm0yi21pc") (y #t)))

(define-public crate-yxy-ffi-0.1.0 (c (n "yxy-ffi") (v "0.1.0") (h "08mdy3mlicj9cvjnircxm2dgk9f5axrs7pap9vnxd8fr936fpd77")))

