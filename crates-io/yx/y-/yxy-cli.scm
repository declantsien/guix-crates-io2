(define-module (crates-io yx y- yxy-cli) #:use-module (crates-io))

(define-public crate-yxy-cli-0.1.0-alpha.1 (c (n "yxy-cli") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "02v4g4rsi02d8fvfp8zslmdnammvylirqhdmaxwz2byi49fzkcpg")))

(define-public crate-yxy-cli-0.1.0-alpha.2 (c (n "yxy-cli") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0ga5g2n8y6xr1x5mh2gaw0qdvb44l8igr6vkcx70al21r0087wg7")))

(define-public crate-yxy-cli-0.1.0-alpha.3 (c (n "yxy-cli") (v "0.1.0-alpha.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "17f0rpbyz3kk13m35snwvppi86v3xxnwscy9wk6g01k48fyhr1pv")))

(define-public crate-yxy-cli-0.1.0-alpha.4 (c (n "yxy-cli") (v "0.1.0-alpha.4") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "10xja22ymhfq61vqg7caqi0qpzh2gf07qr46ss6kjg90svjyd2pb")))

(define-public crate-yxy-cli-0.1.0-alpha.5 (c (n "yxy-cli") (v "0.1.0-alpha.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "09hxswi0h0isv5awpb3x0hlj9vxyn4jnc3k8ny5f0dwc45397jns")))

(define-public crate-yxy-cli-0.1.0 (c (n "yxy-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2.0") (d #t) (k 0)))) (h "1aqdk1z08jjg5f32b007kxf4xcwp0pm00kjyy843fzzyxyn7hd8r")))

(define-public crate-yxy-cli-0.1.1 (c (n "yxy-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "yxy") (r "^0.2") (d #t) (k 0)))) (h "1g0lhs4zszbxf22cdld5620k6kc7xq7vf4zjj9k6yzrab6y1hll2")))

(define-public crate-yxy-cli-0.2.0 (c (n "yxy-cli") (v "0.2.0") (h "1mcy4qgv0w1wym08m2xfplz1zd423ah6v41drrff4sa8v4f8zliz")))

