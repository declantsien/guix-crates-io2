(define-module (crates-io yx or yxorp) #:use-module (crates-io))

(define-public crate-yxorp-0.1.0 (c (n "yxorp") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.1") (d #t) (k 0)))) (h "1zii5hx86s55z43s89vd6iv205m950g6vybbim32pxn84niriaa4")))

