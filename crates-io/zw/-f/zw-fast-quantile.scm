(define-module (crates-io zw -f zw-fast-quantile) #:use-module (crates-io))

(define-public crate-zw-fast-quantile-0.1.0 (c (n "zw-fast-quantile") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dxlags38klfq4yvk9khblcf4kcrw6ny8v7h900qyq4kwprbbh1v")))

(define-public crate-zw-fast-quantile-0.1.1 (c (n "zw-fast-quantile") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0nm5nxkv5b1ddvcfcd0s2kn74pgs9dav2ja283f50qsvyfyc7zdd")))

(define-public crate-zw-fast-quantile-0.1.2 (c (n "zw-fast-quantile") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0a4whf0wg0rf33jkwf1h9wh7in9fi6y41y9w42sf969cyfrisx8f")))

(define-public crate-zw-fast-quantile-0.2.0 (c (n "zw-fast-quantile") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 2)) (d (n "quantiles") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "01bks5jp1v4g45gqf54gwz4fz8h6lfybjy2wplyv2nmn1s0wjxfj")))

(define-public crate-zw-fast-quantile-0.2.1 (c (n "zw-fast-quantile") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 2)) (d (n "quantiles") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1srk31s0rs9mzmf4nr3igi1mvs3rlk15lzrky8x9bvvpz5h7pdsi")))

(define-public crate-zw-fast-quantile-0.2.2 (c (n "zw-fast-quantile") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 2)) (d (n "quantiles") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1mc1ms0yadjqnchwf8ihpjg5v0bw6bi0rs172rmiqwn9c058azcd")))

(define-public crate-zw-fast-quantile-0.2.3 (c (n "zw-fast-quantile") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 2)) (d (n "quantiles") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "18qh4skhnygi6yjbf8ajv0wiqx11hsvsvqg5b740kksjzrphvm73")))

