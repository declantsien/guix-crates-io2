(define-module (crates-io zw oh zwohash) #:use-module (crates-io))

(define-public crate-zwohash-0.1.0 (c (n "zwohash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "13pczwan7sygibaig9dky8ii9s3n7ii5lildylc94i31ajy16hnd") (f (quote (("std") ("default" "std"))))))

(define-public crate-zwohash-0.1.1 (c (n "zwohash") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0jwvjarsn2y6rgzjis2s0dgplgi26sc8xxfl5nic2rgp31x3qzhi") (f (quote (("std") ("default" "std"))))))

(define-public crate-zwohash-0.1.2 (c (n "zwohash") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0iq4dmkj7889s8f35mmcmm82l9n8icd62ffyhp597shcfkh67bxy") (f (quote (("std") ("default" "std"))))))

