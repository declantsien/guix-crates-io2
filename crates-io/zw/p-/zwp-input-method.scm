(define-module (crates-io zw p- zwp-input-method) #:use-module (crates-io))

(define-public crate-zwp-input-method-0.1.0 (c (n "zwp-input-method") (v "0.1.0") (d (list (d (n "wayland-client") (r "^0.27.0") (d #t) (k 0)) (d (n "wayland-commons") (r "^0.27.0") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.27") (f (quote ("client" "unstable_protocols"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.27.0") (d #t) (k 1)))) (h "0slfvaip6f5dznb318gvr6r4k2ma977wxmfsh5hi3myp0djqippl") (y #t)))

(define-public crate-zwp-input-method-0.2.0 (c (n "zwp-input-method") (v "0.2.0") (d (list (d (n "wayland-client") (r "^0.28.0") (d #t) (k 0)) (d (n "wayland-commons") (r "^0.28.0") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.28") (f (quote ("client" "unstable_protocols"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.28.0") (d #t) (k 1)))) (h "0lg38bcx9ikjr8bcq8kplxiddh5galmhz6g8z1h70hsvdi70s5sc") (y #t)))

(define-public crate-zwp-input-method-0.2.1 (c (n "zwp-input-method") (v "0.2.1") (d (list (d (n "wayland-client") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-commons") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29") (f (quote ("client" "unstable_protocols"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.29.0") (d #t) (k 1)))) (h "08dwm263d2590dscjk5wcdyz5jnvmbrgkv5s5aw81w6hkg8j2k3i") (y #t)))

(define-public crate-zwp-input-method-0.3.0 (c (n "zwp-input-method") (v "0.3.0") (d (list (d (n "wayland-client") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-commons") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29") (f (quote ("client" "unstable_protocols"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.29.0") (d #t) (k 1)))) (h "1clzx65kc6j7qv5827r2xx8hwi7mdkpag9qj14idf86kj5ad9ihm") (y #t)))

(define-public crate-zwp-input-method-0.3.1 (c (n "zwp-input-method") (v "0.3.1") (d (list (d (n "wayland-client") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-commons") (r "^0.29.0") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29") (f (quote ("client" "unstable_protocols"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.29.0") (d #t) (k 1)))) (h "10qrgjn64hvqmfrs2hg3xqmmsp1zgpr6bx6glnsnhahkqgc2sbbd")))

