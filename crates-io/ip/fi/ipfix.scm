(define-module (crates-io ip fi ipfix) #:use-module (crates-io))

(define-public crate-ipfix-0.1.0 (c (n "ipfix") (v "0.1.0") (d (list (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1nzxpxdwipds9wa9d70sjgqalf3xdpd53b5lscgi0r4wsjg7d5rr")))

(define-public crate-ipfix-0.1.1 (c (n "ipfix") (v "0.1.1") (d (list (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1nzc40ik33byf1a68b5rdhlpicfcx6vx9zhymrhp09ks3xh4xhgf")))

