(define-module (crates-io ip #{2p}# ip2proxy) #:use-module (crates-io))

(define-public crate-ip2proxy-1.0.0 (c (n "ip2proxy") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.3") (d #t) (k 0) (p "positioned-io-preview")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "156vjc8adknc4sfvr1fi9angaix9ld93izf43apjm7vg57d0z0sl")))

(define-public crate-ip2proxy-2.0.0 (c (n "ip2proxy") (v "2.0.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "positioned-io") (r "^0.3") (d #t) (k 0) (p "positioned-io-preview")) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j7pnr4k3xcn7nak6bg5vs2lxn2068zk3kj8p84yzppb0p5i1v6r")))

(define-public crate-ip2proxy-2.0.1 (c (n "ip2proxy") (v "2.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "positioned-io") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04qjl2h76rcgjaz2v24w1axw03nysb8ikk3816zpvdlsjfa9hkzr")))

