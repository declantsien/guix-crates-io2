(define-module (crates-io ip as ipasir-sys) #:use-module (crates-io))

(define-public crate-ipasir-sys-0.1.0 (c (n "ipasir-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.3") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1ab6dqyphid5hc2n6lbxm1cl7bizsq8rhmyfpp68jdbbx6kxbgk9")))

(define-public crate-ipasir-sys-0.1.1 (c (n "ipasir-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.3") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1s5ggw5xk4kphcagwmv9m2r9gs7vclrsbc74ilz4h3q5m35mipx4")))

(define-public crate-ipasir-sys-0.2.0 (c (n "ipasir-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0dvkfl8922zxvja52l7xaszm3950vq6jb8bch16ipznqjxzyc7lg")))

(define-public crate-ipasir-sys-0.3.0 (c (n "ipasir-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1v12kajfjs9qvwlf80nikar8c842mgwxxf6gvnrarmzwjdxjrkdd")))

