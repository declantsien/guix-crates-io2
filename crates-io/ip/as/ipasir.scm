(define-module (crates-io ip as ipasir) #:use-module (crates-io))

(define-public crate-ipasir-0.1.0 (c (n "ipasir") (v "0.1.0") (h "1nx8kh09dqmgl7xwdriinawvmxb7k41khbb9zvgw7zvlw6rz8kj3")))

(define-public crate-ipasir-0.2.0 (c (n "ipasir") (v "0.2.0") (h "0ll8f10qm1k6vpch651hl2s47a21lax2z4dwg5slqdaqpphnbrxa")))

(define-public crate-ipasir-0.3.0 (c (n "ipasir") (v "0.3.0") (h "19pcbk5njrq1q6yc4qnhm8vzav69zsmic3lnjk323vr98xk5izlw") (f (quote (("ffi") ("default"))))))

(define-public crate-ipasir-0.3.1 (c (n "ipasir") (v "0.3.1") (h "0csfzc1n6j4awjkwj1ndf2sh6g7fmbk9ll9qjlls5v02g7mb6m2b") (f (quote (("ffi") ("default"))))))

