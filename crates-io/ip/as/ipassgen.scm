(define-module (crates-io ip as ipassgen) #:use-module (crates-io))

(define-public crate-ipassgen-0.2.0 (c (n "ipassgen") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pwhash") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)))) (h "1m65ly5ff4c6km260najg03wrd5fdik5h4wn8cf3jqbjh4gbq4wd")))

(define-public crate-ipassgen-0.2.1 (c (n "ipassgen") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pwhash") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)))) (h "0qmr70alj58c7j14mna548m40db0z4dmprxgkq78km1bdfz4032s")))

