(define-module (crates-io ip mb ipmb-derive) #:use-module (crates-io))

(define-public crate-ipmb-derive-0.5.0 (c (n "ipmb-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1kv8qdsjc5kwvhlnkkj7qmx3632gp1lh7m2dxw6h3az8ff6rgwlh")))

(define-public crate-ipmb-derive-0.5.1 (c (n "ipmb-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0cly4d7lv0dxwhgg0nrsb4bjn48mf3lva6b0n3iflkzvfmbrdv9x")))

