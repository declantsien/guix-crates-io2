(define-module (crates-io ip ca ipcap) #:use-module (crates-io))

(define-public crate-ipcap-0.0.0 (c (n "ipcap") (v "0.0.0") (h "1yzwkr0a011iynm14ljkrffwmybkhc9s948f3krn18zminj19y05")))

(define-public crate-ipcap-0.0.1 (c (n "ipcap") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1jhkj93pic7gypla094n138yqv49ch147sbchrjm0iqrxc6mrh4y")))

(define-public crate-ipcap-0.0.2 (c (n "ipcap") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "13klkz28szmqwv1l1fv2z4xc2mwzrgljb0sb4wwn0kyqxwq0gc55")))

(define-public crate-ipcap-0.1.0 (c (n "ipcap") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1a9sahlb7wh8k709ywjs91jmz1pds3mx7zy2jnvncqikbi5vqnad")))

(define-public crate-ipcap-0.1.1 (c (n "ipcap") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0nvyif58l15s5vb398z4nv2l6qiwxf85mfpzc4q0l6i6yzr5l09c") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.2 (c (n "ipcap") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1mlr9dc03f8hyval3lk4487brwvcb5hw8idm9xqrm4y6sm6gh70m") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.3 (c (n "ipcap") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "151v03mhffqhf8y3hik45b0nfxvkkddljp7lm0kvvsqr0jdpjzdw") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.4 (c (n "ipcap") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1psfzffhywxnmpb24ph7d2lazzld07j96shg090inw6zarf0szcz") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.5 (c (n "ipcap") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ipcap-codegen") (r "^0.1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0rq8540z3s8biilm872lcl3z10ilw66xcddaxyggskwkjj95m4v8") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.6 (c (n "ipcap") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ipcap-codegen") (r "^0.1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0h89683jkkcy5mj78jk4afp1slzj9q8qy3cp92lkhh8m4jncxifw") (f (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1.7 (c (n "ipcap") (v "0.1.7") (d (list (d (n "bump2version") (r "^0.1.3") (d #t) (k 2)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ipcap-codegen") (r "^0.1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1800iawdrwhi2gp051lhnf5ayzcs2kg5r5a8sqxwp54q0r5ml40v") (f (quote (("cli" "clap"))))))

