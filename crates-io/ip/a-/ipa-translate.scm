(define-module (crates-io ip a- ipa-translate) #:use-module (crates-io))

(define-public crate-ipa-translate-0.1.0 (c (n "ipa-translate") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0w3myxj1qk6s3aff928z2ilv9zx4y7drpgh9cbkvagi6nicd2fg2")))

(define-public crate-ipa-translate-0.1.1 (c (n "ipa-translate") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0if2s478l31akbgh63xxxpzllwyjhbphmynxswb085wgb6jp3x6q")))

(define-public crate-ipa-translate-0.1.2 (c (n "ipa-translate") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1kfxnxn3wfdac1fcjznmisq8q20njv9cfsm9yh7ipgwa5dd2rmnf")))

(define-public crate-ipa-translate-0.1.3 (c (n "ipa-translate") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1pqfr9km70va7zqb957w9v9vma55vcpz8ma4i31b3zxd7g0wrfx9")))

(define-public crate-ipa-translate-0.1.4 (c (n "ipa-translate") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1i43rkgm0ahl7r3m0q1p85s8qzz46x616354xfy1vl5f3xzfq9lw")))

(define-public crate-ipa-translate-0.1.5 (c (n "ipa-translate") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0d6nzisa3fy4bbzx3cs1j9avsb76b4nk6yw5gjyfbccs08xll13w")))

(define-public crate-ipa-translate-0.1.6 (c (n "ipa-translate") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "036g3rq75svfhaxq767mwb2sc6vc6gzicwibicvddj4k0svznhjh")))

(define-public crate-ipa-translate-0.1.7 (c (n "ipa-translate") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1y85pd6k4ggvm7ih8k77hd9gxvp3x89jxwgngkkja4d82mzkiaw9")))

(define-public crate-ipa-translate-0.1.8 (c (n "ipa-translate") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1r5zq95yl7m4g73d76wgqvn8wyl7gn9r16x6nr7qrvcx66zw0rsm")))

(define-public crate-ipa-translate-0.2.0 (c (n "ipa-translate") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gb8vkj6njpgx5iwd4dwfw5asvwjnfcy3954d1536pkzwrk0lisc")))

