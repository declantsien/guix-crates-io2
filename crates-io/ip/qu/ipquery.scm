(define-module (crates-io ip qu ipquery) #:use-module (crates-io))

(define-public crate-ipquery-0.1.0 (c (n "ipquery") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "trust-dns") (r "^0.16") (d #t) (k 0)))) (h "1vq1jp5pnv7834laqm6r81j39rgdfm6i2r12cn9i8yb1r7lbp6hq")))

(define-public crate-ipquery-0.1.1 (c (n "ipquery") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "trust-dns") (r "^0.16") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.11.1") (d #t) (k 0)))) (h "1lhzfm8f4b5w213a6d63gw6c105fzjmidnzn9gadh1lswcydzisn")))

