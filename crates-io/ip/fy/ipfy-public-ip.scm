(define-module (crates-io ip fy ipfy-public-ip) #:use-module (crates-io))

(define-public crate-ipfy-public-ip-0.1.0 (c (n "ipfy-public-ip") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kn0bwgk4khh08kp2kr1dfki5vp79lc08fapyfcpp1fy6ii8x5z1")))

(define-public crate-ipfy-public-ip-0.1.1 (c (n "ipfy-public-ip") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03ynxyk4vi5gp69vl9k470d4c7sckwcriav09wf8inl1qfdy07p4")))

(define-public crate-ipfy-public-ip-0.1.2 (c (n "ipfy-public-ip") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9sz901ffd5ci2yd745sa5pfmv3gjsqzr5xrww8i90nw4bmhpmw")))

