(define-module (crates-io ip -s ip-server) #:use-module (crates-io))

(define-public crate-ip-server-0.1.0 (c (n "ip-server") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0q93clvx570sb3svpagxplir2ars3dnk4src0ldpqg37sdz6gi4p")))

(define-public crate-ip-server-0.1.1 (c (n "ip-server") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "16q8354x03sm92rbly8hri2dlfvgmn80f58z76rj72jfn4xwd460")))

(define-public crate-ip-server-0.1.2 (c (n "ip-server") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1jmwpck67lpd44sjlyys9af88i4845361ba7p5l4jsq4jkrhh6qb")))

(define-public crate-ip-server-0.1.3 (c (n "ip-server") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "036h5hgw93h4v6xx27a5kzxw399p9nwgnj9617665qr7l7d29a8q")))

(define-public crate-ip-server-0.1.5 (c (n "ip-server") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1kicsdfcrwfnf80va8998cgw956878pa3ny2sw8j4m90ljrk3qdx")))

(define-public crate-ip-server-0.1.4 (c (n "ip-server") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0wq9j21kpcjpy4h2r9bcf81cqsx8kj2im5yjbh7g9khgz2hqj0ix")))

(define-public crate-ip-server-0.1.7 (c (n "ip-server") (v "0.1.7") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0c10qigpyak4n2khnlljhxvvnqy5zxkrg4rbymwwfrmv5ssx3md7")))

(define-public crate-ip-server-0.1.8 (c (n "ip-server") (v "0.1.8") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "17g5ypxlcp61ljsla5l7fld8p5dq8nhw35yvcnycqw7nip1lqycq")))

(define-public crate-ip-server-0.1.10 (c (n "ip-server") (v "0.1.10") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0lpp0va3wsshjj4yvq103bfnnakx6ik5r76kh7cc7pbp3bbcdnk5")))

(define-public crate-ip-server-0.1.9 (c (n "ip-server") (v "0.1.9") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1d3125hcxa0qfiip3h3jkjz1ymy72dg8h47bsvfk7lfb146apbjz")))

(define-public crate-ip-server-0.1.11 (c (n "ip-server") (v "0.1.11") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1v43kshirl7q59l3z49sry4kmpcf3b2paynkhc8vfj26iacs3kcw")))

(define-public crate-ip-server-0.1.12 (c (n "ip-server") (v "0.1.12") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "09zjww51bvll53kvfb51nhvp32fzvpy11sja3ag0sfyyvq07a6yv")))

(define-public crate-ip-server-0.1.13 (c (n "ip-server") (v "0.1.13") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "04hkqh1c3cznlznyvpgks7by6s5b06mzvaahfgq5g8vvwnlkh0az")))

