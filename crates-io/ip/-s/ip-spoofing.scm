(define-module (crates-io ip -s ip-spoofing) #:use-module (crates-io))

(define-public crate-ip-spoofing-0.1.0 (c (n "ip-spoofing") (v "0.1.0") (d (list (d (n "etherparse") (r "^0.13.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "073lycdvq6ajcq4icm0qfpsy4b8hq4yfhjvh24dfaxlpk37n5q8x")))

