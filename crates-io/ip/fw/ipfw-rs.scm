(define-module (crates-io ip fw ipfw-rs) #:use-module (crates-io))

(define-public crate-ipfw-rs-0.1.0 (c (n "ipfw-rs") (v "0.1.0") (d (list (d (n "cdns-rs") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.5") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("socket"))) (d #t) (k 0)))) (h "1d633bnn73a8cxp0phlk4npxhhg89yknyzxdh9qn63r6rmfzswvk") (f (quote (("default"))))))

