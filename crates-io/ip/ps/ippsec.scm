(define-module (crates-io ip ps ippsec) #:use-module (crates-io))

(define-public crate-ippsec-0.1.0 (c (n "ippsec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "163ph41y31gdrlq2lzdv9cvykyf6wqpfcm0qnnyl78b8dnc6scw1") (f (quote (("nix") ("default")))) (r "1.57")))

