(define-module (crates-io ip qs ipqs_db_reader) #:use-module (crates-io))

(define-public crate-ipqs_db_reader-1.0.0 (c (n "ipqs_db_reader") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (o #t) (d #t) (k 0)))) (h "0sva8sbjskv23kpana2xd9x68g3l3mmc7rrb2mz1naz2qr8llr5w") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde" "dep:serde_json"))))))

