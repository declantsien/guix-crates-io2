(define-module (crates-io ip l3 ipl3checksum) #:use-module (crates-io))

(define-public crate-ipl3checksum-1.1.0 (c (n "ipl3checksum") (v "1.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0vgl0g1svxixn1bi92fzkhsnd66lbkagsrx133jk0l27x5cirw6q") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-ipl3checksum-1.1.1 (c (n "ipl3checksum") (v "1.1.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0wpms8728ssca72fhhvbxabn9mck92hrcmzh9bkwvwdad6fy6ybs") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-ipl3checksum-1.2.0 (c (n "ipl3checksum") (v "1.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17cpll7sm2d3lwv939s6pxpllq5c01msf17c6jj32iccds0xzhga") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

