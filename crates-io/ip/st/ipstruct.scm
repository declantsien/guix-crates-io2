(define-module (crates-io ip st ipstruct) #:use-module (crates-io))

(define-public crate-ipstruct-0.1.0 (c (n "ipstruct") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json" "rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "18mdyaa4y13qq4sn98fyrmchkgm99b5sx25kvbliwn2nbgjsgjgv") (y #t)))

(define-public crate-ipstruct-0.2.0 (c (n "ipstruct") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json" "rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "06kzbnlqh9h91443c4zc7953s623zkrb035bsq60c64n03pnp4f3")))

