(define-module (crates-io ip lo iplocate) #:use-module (crates-io))

(define-public crate-iplocate-0.1.0 (c (n "iplocate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00arfnbwxkxwiccm5gfh18kmvpdp5p0r4wil8a6hyqypq5r0hgbh")))

