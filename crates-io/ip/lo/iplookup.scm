(define-module (crates-io ip lo iplookup) #:use-module (crates-io))

(define-public crate-iplookup-0.0.1 (c (n "iplookup") (v "0.0.1") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "stun_codec") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros" "rt-core" "time" "udp"))) (d #t) (k 0)))) (h "0hw191yr2kjbn43pgij8bhp3vpvjk7bzdgqi1kryw4q6g63rb43v")))

(define-public crate-iplookup-0.0.2 (c (n "iplookup") (v "0.0.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "stun_codec") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros" "rt-core" "time" "udp"))) (d #t) (k 0)))) (h "0pajjp61x0xm4aaiqc97m00gbq55mzcdldy3mn4c7nfbipxqwq9v")))

(define-public crate-iplookup-0.0.3 (c (n "iplookup") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "stun_codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "time" "udp"))) (d #t) (k 0)))) (h "15wbxinbavb3bdlilmy4x2lbdkplfbsb1q90pdpcnxvkmlh1m3nq")))

(define-public crate-iplookup-0.0.4 (c (n "iplookup") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "stun_codec") (r "^0.1") (d #t) (k 0)))) (h "1nl6dhpxb7smcdhl2x4s8k5hsx42h6k9342gyag62zmb43bav26f")))

(define-public crate-iplookup-0.1.0 (c (n "iplookup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "stun_codec") (r "^0.1") (d #t) (k 0)))) (h "1ap75nh4sp8qbnm95xwqanxz56a4hcvfywf1s92449hyz24cvlx5")))

