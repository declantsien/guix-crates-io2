(define-module (crates-io ip lo iplookupapi-rs) #:use-module (crates-io))

(define-public crate-iplookupapi-rs-0.1.0 (c (n "iplookupapi-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.7") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "057fk8qc663jdrv7rma0igbjibf628f21b54ydjcbyq6scwma02c")))

