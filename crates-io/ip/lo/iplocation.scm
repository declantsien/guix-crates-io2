(define-module (crates-io ip lo iplocation) #:use-module (crates-io))

(define-public crate-iplocation-0.0.1 (c (n "iplocation") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0ynnxi3arvif4mis2p7db7kjqnnf4pc08b06w3n0p67rr7gfg5ic")))

(define-public crate-iplocation-0.0.2 (c (n "iplocation") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "0ryn1kanrdblhwwn1dhr81zz0xy2fn65gjsmdhn7r6f7f52wja3b")))

(define-public crate-iplocation-0.0.3 (c (n "iplocation") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)))) (h "1rfxylz46y00gr4933ym948i583br716zqxjwwngks9f55iy01qx")))

