(define-module (crates-io ip ro iproto) #:use-module (crates-io))

(define-public crate-iproto-0.1.0 (c (n "iproto") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 0)) (d (n "rmpv") (r "^1.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "sync" "io-util" "time" "test-util" "macros" "parking_lot"))) (d #t) (k 0)))) (h "1w1j6dds6cazq6j6flzzz0yiiqf86jndvrh2l0l80dgznzvnfvwk")))

