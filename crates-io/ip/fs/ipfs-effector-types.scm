(define-module (crates-io ip fs ipfs-effector-types) #:use-module (crates-io))

(define-public crate-ipfs-effector-types-0.1.1 (c (n "ipfs-effector-types") (v "0.1.1") (d (list (d (n "marine-rs-sdk") (r "^0.14.0") (f (quote ("logger"))) (d #t) (k 0)))) (h "0pyjkfnk9ck2hani02ahk6v4gxgszry201y8kbndb8z7khg61mmj")))

