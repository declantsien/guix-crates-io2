(define-module (crates-io ip fs ipfs-effector-cid) #:use-module (crates-io))

(define-public crate-ipfs-effector-cid-0.1.1 (c (n "ipfs-effector-cid") (v "0.1.1") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)))) (h "1c58jqg5jv3g3sff174rjakglpmin5q071mf2ak5ym3p8qm026nm")))

