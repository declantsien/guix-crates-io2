(define-module (crates-io ip fs ipfs-api-versions) #:use-module (crates-io))

(define-public crate-ipfs-api-versions-0.1.0 (c (n "ipfs-api-versions") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2") (d #t) (k 2)))) (h "0q0bza0hrpkshdamqgi8khy4hfm4lbg8z0whbmxdgsf3bky7zy4c")))

