(define-module (crates-io ip fs ipfs-embed-core) #:use-module (crates-io))

(define-public crate-ipfs-embed-core-0.6.0 (c (n "ipfs-embed-core") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "libipld") (r "^0.5.1") (k 0)) (d (n "libp2p-core") (r "^0.22.1") (d #t) (k 0)))) (h "1bczxwv1xb1l551sh451pici9zsx1089i4zdmkbb35rl7r3am1vk")))

(define-public crate-ipfs-embed-core-0.7.0 (c (n "ipfs-embed-core") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "libipld") (r "^0.6.0") (k 0)) (d (n "libp2p-core") (r "^0.22.1") (d #t) (k 0)))) (h "17z3gxpy5k3phnp4zlp1y9azcz0q3kqa737q2v1rdapllznyap9s")))

(define-public crate-ipfs-embed-core-0.8.0 (c (n "ipfs-embed-core") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "libipld") (r "^0.7.1") (k 0)) (d (n "libp2p-bitswap") (r "^0.8.0") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0g0h2s708zy06wzlc8z1jmfc9plcyl81w7nvs0jv3jgj089z7cpa")))

(define-public crate-ipfs-embed-core-0.9.0 (c (n "ipfs-embed-core") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "libipld") (r "^0.7.1") (k 0)) (d (n "libp2p-bitswap") (r "^0.9.0") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.24.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "07v6rpn3dmm0rwq3m7227zys3n4nk0il9k4790n2rj0rnlvd4vdv")))

(define-public crate-ipfs-embed-core-0.10.0 (c (n "ipfs-embed-core") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "libipld") (r "^0.8.2") (k 0)) (d (n "libp2p") (r "^0.32.2") (k 0)) (d (n "libp2p-bitswap") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1398c03j3bj1x4056rn61h8vxj3q2pb11k5psag13z7kgrggk8il")))

