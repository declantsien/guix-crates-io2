(define-module (crates-io ip fs ipfsapi) #:use-module (crates-io))

(define-public crate-ipfsapi-0.1.0 (c (n "ipfsapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15p40imv8wd19zc1lwhphjcn3q933d3lwl408khdfs50vrxkcpkz")))

(define-public crate-ipfsapi-0.1.1 (c (n "ipfsapi") (v "0.1.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1clyzcab9p4b1c6dggphvkw06rrlnk1wi4m5xs90qqy7m7z9zmbf")))

(define-public crate-ipfsapi-0.1.2 (c (n "ipfsapi") (v "0.1.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mzxpjyr7y2w0aj4n01fvpv707jk7cqj96gvwpcdc1vkrn63g0a7")))

(define-public crate-ipfsapi-0.1.3 (c (n "ipfsapi") (v "0.1.3") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g54driv4178lq372zwm4qj4g35prx6z4zq5d6imfhlvzdyhc540")))

(define-public crate-ipfsapi-0.1.4 (c (n "ipfsapi") (v "0.1.4") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rjp4vsv8d0d0vng6zbxnvli6wg8mpgbxwyybigwrc078wjdfkjk")))

(define-public crate-ipfsapi-0.1.5 (c (n "ipfsapi") (v "0.1.5") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18nwlday0ib907ih4vi9b0k4x7bl7ldqrqkddssk6nb1wnn5pd66")))

(define-public crate-ipfsapi-0.1.6 (c (n "ipfsapi") (v "0.1.6") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iybs1c4srdw94caiby8k3681f9lrdh1y99lc70cv36z3p3m84id")))

(define-public crate-ipfsapi-0.1.7 (c (n "ipfsapi") (v "0.1.7") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bhxp7bh6lvkzlbi6pslvh6b0hfyvy3vp1892i2jlhc92s5nnfxr")))

(define-public crate-ipfsapi-0.2.0 (c (n "ipfsapi") (v "0.2.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "028nbljgnhljcaypn72sqyjfl38aifvy52j5bv6z6pq3kv8p8zzn")))

(define-public crate-ipfsapi-0.2.1 (c (n "ipfsapi") (v "0.2.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12ajx6jgsi2fqxv144ispw0nmz721w4h2gj3ah6ckdqr29nphbdz")))

(define-public crate-ipfsapi-0.2.2 (c (n "ipfsapi") (v "0.2.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w6hgyhnnpa3jkfgw30mblm9b0v4ixjbk421m1ij12bjjxzrqnzg")))

(define-public crate-ipfsapi-0.3.0 (c (n "ipfsapi") (v "0.3.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cv42xn6hs2pr7i5c046ygxscrfp2agbhpjp90km3nbsixfj7p73")))

(define-public crate-ipfsapi-0.4.0 (c (n "ipfsapi") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)))) (h "19mdxxxb6w7j8nbb9mhxdrkixvzhji08j2bkgd0ps8ycfs7g1vmq")))

