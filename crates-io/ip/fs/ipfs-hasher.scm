(define-module (crates-io ip fs ipfs-hasher) #:use-module (crates-io))

(define-public crate-ipfs-hasher-0.13.0 (c (n "ipfs-hasher") (v "0.13.0") (d (list (d (n "ipfs-unixfs") (r "^0.2") (d #t) (k 0)) (d (n "wasm-loader") (r "^0.13.0") (d #t) (k 2)))) (h "0j4z5cm7fcs9wwxp4p8yrxpb0mq01qalk5m8593svnfdm9gh0bc0")))

