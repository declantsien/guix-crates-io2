(define-module (crates-io ip fs ipfs-embed-cli) #:use-module (crates-io))

(define-public crate-ipfs-embed-cli-0.2.0 (c (n "ipfs-embed-cli") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "ipfs-embed") (r "^0.2.0") (d #t) (k 0)) (d (n "libipld") (r "^0.3.0") (f (quote ("dag-cbor" "dag-json" "dag-pb"))) (d #t) (k 0)))) (h "0pzzp3ayrv12zki63ibrfqg2ch0pi5snk34b1ihv055hicag1c1v")))

(define-public crate-ipfs-embed-cli-0.3.0 (c (n "ipfs-embed-cli") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "ipfs-embed") (r "^0.3.0") (d #t) (k 0)) (d (n "libipld") (r "^0.3.0") (f (quote ("dag-cbor" "dag-json" "dag-pb"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.0") (d #t) (k 0)))) (h "0052i4m4rdg2crpk9wjjwxs01ia7n1bn5n9gg607wrfnxjhkhns3")))

(define-public crate-ipfs-embed-cli-0.4.0 (c (n "ipfs-embed-cli") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipfs-embed") (r "^0.4.0") (d #t) (k 0)) (d (n "libipld") (r "^0.4.0") (f (quote ("dag-cbor" "dag-json" "dag-pb"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.3") (d #t) (k 0)))) (h "07lzmhys08m26m3z85ksbjvwapfkn4si8hqiv704ly2xsfypawyg")))

(define-public crate-ipfs-embed-cli-0.6.0 (c (n "ipfs-embed-cli") (v "0.6.0") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipfs-embed-db") (r "^0.6.0") (d #t) (k 0)) (d (n "libipld") (r "^0.5.1") (f (quote ("dag-cbor" "dag-json" "dag-pb"))) (d #t) (k 0)))) (h "1pp8ajsb2a64l78498m02x166ax4nshp02ad9ml9mhg39chy451j")))

