(define-module (crates-io ip fs ipfs-effector-imports) #:use-module (crates-io))

(define-public crate-ipfs-effector-imports-0.1.1 (c (n "ipfs-effector-imports") (v "0.1.1") (d (list (d (n "ipfs-effector-types") (r "^0.1.1") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (f (quote ("logger"))) (d #t) (k 0)))) (h "0abj5xrl5pb1w7hxwbfvp9ahr865vs67wn3ck30n0445jnmlcbn7")))

