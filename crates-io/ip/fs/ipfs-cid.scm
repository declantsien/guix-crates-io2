(define-module (crates-io ip fs ipfs-cid) #:use-module (crates-io))

(define-public crate-ipfs-cid-1.0.0 (c (n "ipfs-cid") (v "1.0.0") (d (list (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "cid") (r "^0.5.1") (k 0)) (d (n "ipfs-unixfs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1wdx2c8831a0cdr0q2wx7liy496vpahizyjyn9xb8xz8ziq45yi8") (r "1.70.0")))

(define-public crate-ipfs-cid-2.0.0 (c (n "ipfs-cid") (v "2.0.0") (d (list (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "cid") (r "^0.10.1") (d #t) (k 0)) (d (n "ipfs-unixfs") (r "^0.2.0") (d #t) (k 0)) (d (n "multibase") (r "^0.9.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "131xjsn1fycv95fvls39kr12c57z99vry6nw86xr33g2cqjnvfq7") (r "1.70.0")))

