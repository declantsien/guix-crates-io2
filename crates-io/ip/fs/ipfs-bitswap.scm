(define-module (crates-io ip fs ipfs-bitswap) #:use-module (crates-io))

(define-public crate-ipfs-bitswap-0.1.0 (c (n "ipfs-bitswap") (v "0.1.0") (d (list (d (n "cid") (r "^0.5") (k 0)) (d (n "fnv") (r "^1.0") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "libp2p-core") (r "^0.22") (k 0)) (d (n "libp2p-swarm") (r "^0.22") (k 0)) (d (n "multihash") (r "^0.11") (k 0)) (d (n "prost") (r "^0.6") (k 0)) (d (n "prost-build") (r "^0.6") (k 1)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tokio") (r "^0.2") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "unsigned-varint") (r "^0.3") (k 0)))) (h "0sf882mmvpxrpb9c5hcnvkzhdnxmkip27ycn7kcg210xarndn5gf")))

