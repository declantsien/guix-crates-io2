(define-module (crates-io ip -r ip-rs) #:use-module (crates-io))

(define-public crate-ip-rs-0.1.0 (c (n "ip-rs") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0gdxjjj0rdirj02r7b74jw9hab1yl5ig29hq4l7rkkjgqp7syys5")))

(define-public crate-ip-rs-0.2.0 (c (n "ip-rs") (v "0.2.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "02q5c3gv7s4dprf9v81blm4yn21rqy27hy4bpnj6adsqxl10p192")))

(define-public crate-ip-rs-0.2.1 (c (n "ip-rs") (v "0.2.1") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "19r7b9fbz4jphsl1svli63nj34did1iivrh6c26zljggzcwfrgdr")))

(define-public crate-ip-rs-0.2.2 (c (n "ip-rs") (v "0.2.2") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "153lzbwjrkyc6x5264i58wpjlfj07jaac7pdb9x91lncz5g3fixf")))

