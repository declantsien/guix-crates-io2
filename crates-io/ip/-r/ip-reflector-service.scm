(define-module (crates-io ip -r ip-reflector-service) #:use-module (crates-io))

(define-public crate-ip-reflector-service-0.1.0 (c (n "ip-reflector-service") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n0797plhvdqdq1qdqrjhhdz4cg4bvl4g8l3kc39pcd62jaf4acz")))

(define-public crate-ip-reflector-service-0.1.1 (c (n "ip-reflector-service") (v "0.1.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zkryr387ypcq37hgv263i9hd3k009myzwxp4fjahwx5bp5sy7wj")))

(define-public crate-ip-reflector-service-0.1.2 (c (n "ip-reflector-service") (v "0.1.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14vi69c69cayvr9cnlcg43yy5qrv6c8gmkj6mx4df6x31dxj4rn4")))

(define-public crate-ip-reflector-service-0.1.3 (c (n "ip-reflector-service") (v "0.1.3") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07nnjnb8fjbvh4h1b4fpc98nzbcf6wvgslcmjm7gggiwh01cfvrl")))

(define-public crate-ip-reflector-service-0.1.4 (c (n "ip-reflector-service") (v "0.1.4") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "axum-client-ip") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c64s8dy9w3b0sz4d6cm0zmw9hycinbwm22c77ry2aa5mrikji11")))

