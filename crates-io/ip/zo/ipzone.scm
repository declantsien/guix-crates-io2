(define-module (crates-io ip zo ipzone) #:use-module (crates-io))

(define-public crate-ipzone-0.1.0 (c (n "ipzone") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1095jjp5xmdkmffikl6fpfwxfv3fx2gnwvd5qdgpla01i05hql12") (f (quote (("json" "serde_json") ("default"))))))

(define-public crate-ipzone-0.2.0 (c (n "ipzone") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1v1grvcqdrxjhrzccfz3fa09r8hjvcp14x8a2kzx0lkxdm5nij4i")))

(define-public crate-ipzone-0.3.0 (c (n "ipzone") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "178x0bsh10ik6d3sqrq3lffrc9i1pkhr4i3vk81z0irmhnv8f6km")))

(define-public crate-ipzone-0.4.0 (c (n "ipzone") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n0gx0ag4wr56i193ma32fixf10mwd97hw7awf4smdlxn05iv2n1")))

