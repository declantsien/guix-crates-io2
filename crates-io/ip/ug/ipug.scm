(define-module (crates-io ip ug ipug) #:use-module (crates-io))

(define-public crate-ipug-0.2.0 (c (n "ipug") (v "0.2.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hmd89rf48zy9v5p1254n6nvzwj8nc9863m7ncxc0r2hfcmcrx0m")))

(define-public crate-ipug-0.2.1 (c (n "ipug") (v "0.2.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "14as191qyxcyafrmnpd4vwdpy1rk1xzdz1ib5rsc22pdvkzj2dgi")))

