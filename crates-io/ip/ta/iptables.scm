(define-module (crates-io ip ta iptables) #:use-module (crates-io))

(define-public crate-iptables-0.1.0 (c (n "iptables") (v "0.1.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0vskd7mrq9vp3qf29p1a2w9jfdkzsn4k2yz917mkcpnz8vnp6kk0")))

(define-public crate-iptables-0.1.1 (c (n "iptables") (v "0.1.1") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17bg8a6nsng8ar0g9jcg9iiywjmmgb6zqvfkvf2iar87gp3zpfb3")))

(define-public crate-iptables-0.1.2 (c (n "iptables") (v "0.1.2") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0p6vjfspny2nv68x9c2bi7xxvmljqzzji2sxp1rz1sy5cx04f01x")))

(define-public crate-iptables-0.2.0 (c (n "iptables") (v "0.2.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "18nbf1rv3chsrj42svnky2cn241ksrplm69y7qrvqyvn33pf30fj")))

(define-public crate-iptables-0.2.1 (c (n "iptables") (v "0.2.1") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1p26bp6pl08h74jyhbakmhfq7kjb7dnlkicn3jf28wj3riyb6w2z")))

(define-public crate-iptables-0.2.2 (c (n "iptables") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1phh01idlqcd78n4n0k1dhdjh8r5wv1s8cd6xq932bh2rd4hb4gp")))

(define-public crate-iptables-0.3.0 (c (n "iptables") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0srdg0i4fjgnrwrxp3bv34jydpr227w7rlr3yfdh54l270a8a0wk")))

(define-public crate-iptables-0.4.0 (c (n "iptables") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "15ymcfmq6z0a1mvwysnnl3j8kgvbmzd78phklh5pi7327gqf57n1")))

(define-public crate-iptables-0.4.1 (c (n "iptables") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0m8589rfzrh2lbgd11qvin7v73pfhpbwqkxmdnkfvnyps5h9cw2s")))

(define-public crate-iptables-0.4.2 (c (n "iptables") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "05lf14p2xzn28p6m6gi7g7i6axi5jhlyf96i4ic2byxhxl23i7yj")))

(define-public crate-iptables-0.4.3 (c (n "iptables") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0c08nn4qmhkl69g7d1i9869vvc2kqnkfsq7hhyr987dgrrqdjlal")))

(define-public crate-iptables-0.5.0 (c (n "iptables") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0iqi5303wk2gx5fdbr5d1507mk87l6yifnjdaiwlb0a3wzc8lj0w")))

(define-public crate-iptables-0.5.1 (c (n "iptables") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a96p8fq70v3z2rjf4c461pqhad6nkibs7sfgydkrf7ys1r0v7yk")))

