(define-module (crates-io ip #{2d}# ip2d) #:use-module (crates-io))

(define-public crate-ip2d-0.1.0 (c (n "ip2d") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "06m2w70y8s7qvf40r69l3bjd9v6p1v9b205ph7ilpdp7sh102j9p")))

(define-public crate-ip2d-0.1.1 (c (n "ip2d") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "01bjpxbzaz1pm7liv6gfqafr5yxr47379xncsfpw505bp6gh4hlj")))

(define-public crate-ip2d-0.1.2 (c (n "ip2d") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "02z5j2341xf1wkmhhg5hy99s7a74bcrv30fsh37pw64jihcwr5ds")))

(define-public crate-ip2d-0.1.3 (c (n "ip2d") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0g28g0igvxmpjdrq487hxdbi7gcyqx88c2ysnr4i256q2la5767y")))

(define-public crate-ip2d-0.1.4 (c (n "ip2d") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)))) (h "0ff6d0v45qym3d18612snw0894s28hfwfgh5g8yylmjg1m80239n")))

(define-public crate-ip2d-0.1.5 (c (n "ip2d") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)))) (h "00qnbi4jiv569l6p2m73d8y95qdw06gcm25qrpzja2vhv0xiwy8n")))

(define-public crate-ip2d-0.2.0 (c (n "ip2d") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)))) (h "1i10b3lmh8s2vrxkmgffs8rslxqwimgwnp00gxnwvfbbvi3l10jf")))

(define-public crate-ip2d-0.3.0 (c (n "ip2d") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.0") (d #t) (k 0)))) (h "00nijjdvi2jmx4rhafbawwb54kw3dg1z4mgh4b7vdr30cpcmyczk")))

(define-public crate-ip2d-0.4.0 (c (n "ip2d") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)))) (h "1mssc9690j0ngj93xy8d5yqvcvagkimxdwgmwzgh761i6hnr90rm")))

(define-public crate-ip2d-0.4.1 (c (n "ip2d") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)))) (h "114mcb3dwvn4p34j6vrjb3856g8jwnr4rn76xnnh2z2bc8kbgijs")))

(define-public crate-ip2d-0.4.2 (c (n "ip2d") (v "0.4.2") (d (list (d (n "clap") (r "^4.4.16") (d #t) (k 0)))) (h "0m7acw9x0pn5h5z1yix9kxhmr1gl1dzwx7j5b5w2nwi2bq2aflnc")))

(define-public crate-ip2d-0.5.0 (c (n "ip2d") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.16") (d #t) (k 0)))) (h "01az3y2vrd2bylyjmbbwvz9w387zb8js9575h4vxc395bx5zxn03")))

