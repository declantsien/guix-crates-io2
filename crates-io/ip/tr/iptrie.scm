(define-module (crates-io ip tr iptrie) #:use-module (crates-io))

(define-public crate-iptrie-0.6.0 (c (n "iptrie") (v "0.6.0") (d (list (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 2)))) (h "0ys3c1kf6x14n5rddb45j2q3mh9zfd647ashlwry2rbmnc7z3isf") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.6.1 (c (n "iptrie") (v "0.6.1") (d (list (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 2)))) (h "0gnyr2ma6apff3d7a4lc6l3r0gzk9c2389ci2ykbw38d8p58rlrn") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.6.2 (c (n "iptrie") (v "0.6.2") (d (list (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0n27frmncx9wh0vah11lh5hvlg9isjbdi25n8lrnlg2q3bcxz59m") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.6.3 (c (n "iptrie") (v "0.6.3") (d (list (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0bhzrwhgm8fz491wfn24wzfmn9251amwlawwv7hsapk8gmhp0184") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.7.0 (c (n "iptrie") (v "0.7.0") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1z92992rhl1yrhg8c5xbdaihhss3g5hgzbasbbxpmzpmal5ir824") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.7.1 (c (n "iptrie") (v "0.7.1") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ya0di3iqzj4srr61wnl9azv389180cng2w54f25gq14y41wvpbq") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.7.2 (c (n "iptrie") (v "0.7.2") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1sqj786c3jpaqlhkg9mf6n7zm8a0jvvlnfv30a594b88q6aklzjy") (f (quote (("graphviz"))))))

(define-public crate-iptrie-0.7.3 (c (n "iptrie") (v "0.7.3") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xzfgrccsqzi77c1r62wfgif3b611gqmf1aycv93iqw2k7kfh0vd") (f (quote (("graphviz"))))))

(define-public crate-iptrie-0.7.4 (c (n "iptrie") (v "0.7.4") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1wn075gfmn51c5rx7g01k5hp4lf8lrx8h96lhpwaq02z0jpbmzzl") (f (quote (("graphviz"))))))

(define-public crate-iptrie-0.7.5 (c (n "iptrie") (v "0.7.5") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ldnrvxhsff4qd46biaxvvqvfr3xzhf28akgwhskcrlm43sf0pl9") (f (quote (("graphviz"))))))

(define-public crate-iptrie-0.8.0 (c (n "iptrie") (v "0.8.0") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10qzp2brdx9kavnwpgw8dynnqcg5p3rh4hf8ybk4x73f8zzrpikc") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.8.1 (c (n "iptrie") (v "0.8.1") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13wiyjxy4615racrnp5kb2w2plza3d060jj9kdizqa2svd371qs3") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.8.2 (c (n "iptrie") (v "0.8.2") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0rs6m5s6vhpd3ab3zab3hg83y0a21cbvbf2qz6k47km46jcn5rz6") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.8.3 (c (n "iptrie") (v "0.8.3") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1kn7w1v27rhw8mbblznvnrcvslvcyqvkl220gyd21mds5daszn0i") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.8.4 (c (n "iptrie") (v "0.8.4") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0s1rgsaaqqdnd8iv59d6y1sripnw8ynnfgjcsqmj0w50lfcg93wr") (f (quote (("graphviz")))) (y #t)))

(define-public crate-iptrie-0.8.5 (c (n "iptrie") (v "0.8.5") (d (list (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jgivfaxz6aj2b747z7wx3683q0aklyx8jzw6474hi1gkqk45qqd") (f (quote (("graphviz"))))))

