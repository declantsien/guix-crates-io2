(define-module (crates-io ip tr iptrap) #:use-module (crates-io))

(define-public crate-iptrap-1.0.0 (c (n "iptrap") (v "1.0.0") (d (list (d (n "time") (r "*") (d #t) (k 0)) (d (n "zmq") (r "*") (d #t) (k 0)))) (h "03nqbal3vh2x5zhm17qmgib5zznvviwcwcfficz6y7lqjdn4nb4x")))

(define-public crate-iptrap-1.0.1 (c (n "iptrap") (v "1.0.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "zmq") (r "*") (d #t) (k 0)))) (h "03c0qd9lrjccj00b6jgwxhbrzfdiyrvkgbkg9yn0bcbffyd5cpx8")))

(define-public crate-iptrap-1.0.4 (c (n "iptrap") (v "1.0.4") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "siphasher") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)) (d (n "zmq") (r "~0.8") (d #t) (k 0)))) (h "0dbdxzp71xscy5di4qc11rifxbv987zvndaxih2sn9q7vghggwhw")))

(define-public crate-iptrap-1.0.5 (c (n "iptrap") (v "1.0.5") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "siphasher") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)) (d (n "zmq") (r "~0.8") (d #t) (k 0)))) (h "1ra7a1lw11195bb76xf1xfy1v1f57vbvs5vbp82rjz29znxsv5mb")))

(define-public crate-iptrap-1.0.6 (c (n "iptrap") (v "1.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0ijxal41d8w4ldk9ysq8xi3ldvfdbrrk5zhzpb32wzmy7m09aqss")))

(define-public crate-iptrap-1.0.7 (c (n "iptrap") (v "1.0.7") (d (list (d (n "coarsetime") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1f0r1bcmz9wbxav14fq2afvlgl1s3rgajalh1k278zhz1h9bxwv8")))

