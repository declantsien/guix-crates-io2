(define-module (crates-io ip fe ipfetch) #:use-module (crates-io))

(define-public crate-ipfetch-1.0.0 (c (n "ipfetch") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "seeip") (r "^3.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.6.0") (d #t) (k 0)))) (h "0yva0xh3dx6y236az017mwliji8zm1bj4svwdcij5ydwxw1fn38k")))

