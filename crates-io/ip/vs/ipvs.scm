(define-module (crates-io ip vs ipvs) #:use-module (crates-io))

(define-public crate-ipvs-0.1.0 (c (n "ipvs") (v "0.1.0") (h "1flyqibikdr9nj40lcq81pyj5pyir7f1q6i1xr6zicm7xhvbkhpx")))

(define-public crate-ipvs-0.1.1 (c (n "ipvs") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "121v2q2xp7fkgfyp13zjjw5aw7v6kx617q5q27hhin6z2jmmsc6x")))

