(define-module (crates-io ip op ipopt_bindgen) #:use-module (crates-io))

(define-public crate-ipopt_bindgen-0.1.0 (c (n "ipopt_bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0kg63jmcy3vghzixw6m9589i7x0zpfyy53jqlmjk4y6azk05l658")))

(define-public crate-ipopt_bindgen-0.1.1 (c (n "ipopt_bindgen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "1k04q2a8qy2lmifsi10m6arjhc0pssanzb2c2hl53ji7yd5xrrc9")))

(define-public crate-ipopt_bindgen-0.2.0 (c (n "ipopt_bindgen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "052zxgqz19hhpmsz96fd65k2dlzdmlwixf8kx1by1rj7yj0clys0")))

(define-public crate-ipopt_bindgen-0.2.1 (c (n "ipopt_bindgen") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0c55yvccfnkyw20alwb9xrwfjjgbnqh44wkbdk3g7wzwn7z99rcq")))

(define-public crate-ipopt_bindgen-0.2.2 (c (n "ipopt_bindgen") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "1jdsxivbh9nkachsahfh14ycpgg5zx1pd4zsjfph98g9ic80n5gg")))

