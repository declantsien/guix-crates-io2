(define-module (crates-io ip op ipopt) #:use-module (crates-io))

(define-public crate-ipopt-0.1.0 (c (n "ipopt") (v "0.1.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1labvg625fx26r1wvlkhhcj3934zrdm1qczxqwffs6wxwsvy7jrd")))

(define-public crate-ipopt-0.1.1 (c (n "ipopt") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0i0amyn6vi2c8han0cz56r5glwsl3nxih0d3jnyny3s59ryz7rcx")))

(define-public crate-ipopt-0.1.2 (c (n "ipopt") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.1.2") (d #t) (k 0)))) (h "11qspaqg3rinsrcz6ckl9hky4dp2313i69yb5ag4qrzfn8lxnhjq")))

(define-public crate-ipopt-0.2.0 (c (n "ipopt") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0bln6ncsl5dv1wac1m0asm3hxf0khn387c9ja0azw72mp6gp7llf")))

(define-public crate-ipopt-0.3.0 (c (n "ipopt") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0g7ihnhhslzjc42c124frgmk1hmmdazbjqmqwflnx79gjiqp9k2q")))

(define-public crate-ipopt-0.3.1 (c (n "ipopt") (v "0.3.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0kgv5wv85qkrzvzg6vcmg4vyym65fmlpb04lzlzq5vszillkmfca")))

(define-public crate-ipopt-0.4.0 (c (n "ipopt") (v "0.4.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0ifbbwiwfl0xhy7jqn0qxz5yzyqb93mh8nq5zfk676gb66v47qsk")))

(define-public crate-ipopt-0.5.0 (c (n "ipopt") (v "0.5.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0nxjfdr656f83xd70cni161nw97rqr19jy79ypgdgz6rf5md1l2h")))

(define-public crate-ipopt-0.5.1 (c (n "ipopt") (v "0.5.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1hqjkmmkbsxmr96y8ifcsfw3ax0i5188i64yaiga1rwvwrgb2iyj")))

(define-public crate-ipopt-0.5.2 (c (n "ipopt") (v "0.5.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.5.1") (d #t) (k 0)))) (h "05cwmfv7nm843c5hbkmn0pzrsmd48rc10a4578yrkxgwywvnczd2")))

(define-public crate-ipopt-0.5.3 (c (n "ipopt") (v "0.5.3") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.5.3") (d #t) (k 0)))) (h "0k6bwd80ky0n5w9id65kbxdyx2l78r4gx52dcr4d3l91wmlsp38b")))

(define-public crate-ipopt-0.5.4 (c (n "ipopt") (v "0.5.4") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "ipopt-sys") (r "^0.5") (d #t) (k 0)))) (h "04r97fw5i00q34hppw17zn0slr9283f5iqhw3cgf6znn155x9r6j")))

