(define-module (crates-io ip mi ipmiraw) #:use-module (crates-io))

(define-public crate-ipmiraw-0.1.0 (c (n "ipmiraw") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dqc3x1sb5mb9r5hh7mqd3rf1cb0k03yajbx7nb5va2kb5sa5n87")))

(define-public crate-ipmiraw-0.2.0 (c (n "ipmiraw") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0djmx0yg09xg25scnrjrzkvc2rhanqam2qhxw2g6wlps5gpnfcwc")))

(define-public crate-ipmiraw-0.3.0 (c (n "ipmiraw") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vacmv2hba5bnxvhk4p0nwya8xil0vz74lbvaq9pqmv2rpx5yi80")))

(define-public crate-ipmiraw-0.4.0 (c (n "ipmiraw") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "189nggihv6bayh978jdgssz1wzx1p9mgz2vxx28fqlxdrl5bc27v")))

(define-public crate-ipmiraw-0.5.0 (c (n "ipmiraw") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14r7zhs01cgmlnks7bk3isprka27m2f3lvad65w1218qa6dhkk74")))

