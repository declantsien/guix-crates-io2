(define-module (crates-io ip mi ipmitool-dcmi-power-reading) #:use-module (crates-io))

(define-public crate-ipmitool-dcmi-power-reading-1.0.0 (c (n "ipmitool-dcmi-power-reading") (v "1.0.0") (d (list (d (n "binary-layout") (r "^4.0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipmi-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1js25lv92i6j7xv39cjlrd396j7r3xk3y0lc7nirss8s18dghaf7") (r "1.76.0")))

