(define-module (crates-io ip mi ipmimonitoring-sys) #:use-module (crates-io))

(define-public crate-ipmimonitoring-sys-0.1.0 (c (n "ipmimonitoring-sys") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "0pprjaaw20sfprl7qrh2cizmxmp4sq4h97hak5mf4fyspi3216jp") (l "ipmimonitoring")))

(define-public crate-ipmimonitoring-sys-0.1.1 (c (n "ipmimonitoring-sys") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "0mg6q2lfwl5cgiy525ndj2cmiijxvkam08ibypzfr4k625x7nhn1") (l "ipmimonitoring")))

(define-public crate-ipmimonitoring-sys-0.1.2 (c (n "ipmimonitoring-sys") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "1jb0kqhi9adhlb7as3w6a3mnvnmyf70v2fvz8xjf4drl4x5k46r9") (l "ipmimonitoring")))

(define-public crate-ipmimonitoring-sys-0.1.4 (c (n "ipmimonitoring-sys") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "0y9six34pa7djpmpxzj41s4775r5rkypd3386yqnmyxafj7fvaxh") (l "ipmimonitoring")))

