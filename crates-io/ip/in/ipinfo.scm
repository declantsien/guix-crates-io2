(define-module (crates-io ip in ipinfo) #:use-module (crates-io))

(define-public crate-ipinfo-0.1.0 (c (n "ipinfo") (v "0.1.0") (d (list (d (n "lru") (r "^0.1") (d #t) (k 0)) (d (n "mockito") (r "^0.21") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vdzm75b6az37cw1cb23iq9xna2xw0jgh77cgapdvqmswvnwida7") (y #t)))

(define-public crate-ipinfo-0.1.1 (c (n "ipinfo") (v "0.1.1") (d (list (d (n "lru") (r "^0.1") (d #t) (k 0)) (d (n "mockito") (r "^0.21") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ic2ihnp8kvd48dcymbnl309wxagbbjxhj6l42nbvc4kn2ak9m1m")))

(define-public crate-ipinfo-0.1.2 (c (n "ipinfo") (v "0.1.2") (d (list (d (n "lru") (r "^0.1") (d #t) (k 0)) (d (n "mockito") (r "^0.21") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "176napgqvdjwyjz97ax3101ysiyigl8ajhlgkpwh2xgwm45768iy")))

(define-public crate-ipinfo-0.1.3 (c (n "ipinfo") (v "0.1.3") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wd6f269762maf2vic9bccg3madhz3bs2hdqnc4hywhhg6pwzvn7")))

(define-public crate-ipinfo-0.2.0 (c (n "ipinfo") (v "0.2.0") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a6026bqck02dmgvf8wzm6kiy67f0hcmsvl1b9piwv68n03n5i87")))

(define-public crate-ipinfo-0.3.0 (c (n "ipinfo") (v "0.3.0") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xz02p8zk98vdrgxyy24pbix5m78x40mj01bbk9h7kjc9cpy2700")))

(define-public crate-ipinfo-0.3.1 (c (n "ipinfo") (v "0.3.1") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1za99zikf7285zmnqrjchx795mln0472db666qg9rkbqy5vzrnpp")))

(define-public crate-ipinfo-0.4.0 (c (n "ipinfo") (v "0.4.0") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nnvwny4kcqnbanw5jn3189d2za7bbandixnm8in1clhmfkldphy") (y #t)))

(define-public crate-ipinfo-0.5.0 (c (n "ipinfo") (v "0.5.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r1fnbaw2y11wjn7baymw6q4d4bb9vzqzsb3d71l6qisqsj16bgd")))

(define-public crate-ipinfo-0.5.1 (c (n "ipinfo") (v "0.5.1") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "105bqdy21rc0kjm0rm13kss4n9q0df26b9nzcfdi8m4bnqzqgxm8")))

(define-public crate-ipinfo-1.0.0 (c (n "ipinfo") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09vg3khp1x3asy9niwnkgnq45nj2i6dpv6cd2cwxk7zf6pd9xrxi")))

(define-public crate-ipinfo-1.1.0 (c (n "ipinfo") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lru") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0hj62m1d8868hysls2498hdrmzacakhpabmfx428fnx7jkb67pmn") (y #t)))

(define-public crate-ipinfo-2.0.0 (c (n "ipinfo") (v "2.0.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lru") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0gzlxp4nacwzbg76i264vq2a1wwmzzyxsmk5m4m38sdb9pn0x4zf")))

(define-public crate-ipinfo-2.1.0 (c (n "ipinfo") (v "2.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0nyqw6rbd71k8npvhjzcszs48qbg5b0pmsy62bwmd6y6z0x71rji")))

(define-public crate-ipinfo-2.2.0 (c (n "ipinfo") (v "2.2.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1xjg4zr6l6byl9hw729kv3is9ga4rnxvjyb40s4m47vfc9pprvr7")))

(define-public crate-ipinfo-3.0.0 (c (n "ipinfo") (v "3.0.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "0nqjpp4sqqwpk2zmzg372krq1db4bpcb0q9z0awx708x5iwkqiq5")))

