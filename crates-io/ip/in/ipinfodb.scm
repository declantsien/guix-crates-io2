(define-module (crates-io ip in ipinfodb) #:use-module (crates-io))

(define-public crate-ipinfodb-0.1.0 (c (n "ipinfodb") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q521i6i8yvw3kix4xi3kjhl9y8xhz8yv9pcwgv752zqc5pbi435")))

(define-public crate-ipinfodb-0.1.1 (c (n "ipinfodb") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sg6mh0n5ybyba4v53jd2jhfdshl2yjkwv5f5i0ymz8yv4qqw27n")))

