(define-module (crates-io ip v4 ipv4-display) #:use-module (crates-io))

(define-public crate-ipv4-display-0.1.0 (c (n "ipv4-display") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nhn2w2gl2lm7bkdfb7qkhkplbz2f78fqbnprns42vkbpqja4xzw")))

(define-public crate-ipv4-display-0.2.0 (c (n "ipv4-display") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 2)))) (h "0ijjlxxfh8k17a8h7wmd6rmxi4fpshzji6whmdiwrs5w11579vp2")))

(define-public crate-ipv4-display-0.2.1 (c (n "ipv4-display") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 2)))) (h "1jpcpljaifn12nl6qyl3f2ypin58dsj2f3406ypkl1m68z4cgif0")))

