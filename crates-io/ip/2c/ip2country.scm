(define-module (crates-io ip #{2c}# ip2country) #:use-module (crates-io))

(define-public crate-ip2country-0.1.0 (c (n "ip2country") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "00kmksldk2vfpppckbjinz16bkb6crdgagfz4708xcrhgxrpgmng")))

(define-public crate-ip2country-0.2.0 (c (n "ip2country") (v "0.2.0") (d (list (d (n "ascii") (r "^1.0") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wqnw7shiaf64609ns0hbs5pspzayxsmxb31rk9kaab0dsbsn205")))

(define-public crate-ip2country-0.3.0 (c (n "ip2country") (v "0.3.0") (d (list (d (n "ascii") (r "^1.0") (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02ydq3bv3jf3ws86a7gl16gxdb64dcpbm0a96yypd41vigi9f4rp")))

