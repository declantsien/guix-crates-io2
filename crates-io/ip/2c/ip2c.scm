(define-module (crates-io ip #{2c}# ip2c) #:use-module (crates-io))

(define-public crate-ip2c-0.1.0 (c (n "ip2c") (v "0.1.0") (h "1qbh9kqx6a5ckfwfk9vc8hmadd1fd769ll947b2ipkwzb9hmmpay")))

(define-public crate-ip2c-0.1.1 (c (n "ip2c") (v "0.1.1") (h "17bm7cgkqi1460jxmh159xrr14vj3j7zhhv6r8bnb31gwgccgnvd")))

(define-public crate-ip2c-0.1.3 (c (n "ip2c") (v "0.1.3") (h "0k18sr56ns6g4msf2yb47cwqzlmmk914i3cx711j8yffc942jymf")))

(define-public crate-ip2c-0.1.4 (c (n "ip2c") (v "0.1.4") (h "0biww7iqazpx42c49fq3iafyhjjbqhdbal1qb7c4pm2nbysb4w3q")))

(define-public crate-ip2c-0.1.5 (c (n "ip2c") (v "0.1.5") (h "1qs4ssiia2d1wsvnfxck132p7vxq9iskgvsyvfs42kyfs7bfd5si")))

