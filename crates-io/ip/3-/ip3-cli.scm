(define-module (crates-io ip #{3-}# ip3-cli) #:use-module (crates-io))

(define-public crate-ip3-cli-1.0.0 (c (n "ip3-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "ip3") (r "^1.0.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.6") (d #t) (k 0)) (d (n "public-ip") (r "^0.2.2") (f (quote ("dns-resolver"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "18gw68igifazy2s42khxkzqkx1kpdf79rb809fnkwignscga5v83")))

