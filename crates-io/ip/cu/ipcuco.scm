(define-module (crates-io ip cu ipcuco) #:use-module (crates-io))

(define-public crate-ipcuco-0.1.1 (c (n "ipcuco") (v "0.1.1") (h "0izpc6n5d46r9bvdvhpmz3pc743xrli9h8sf46pd2hsvssh6n9hd")))

(define-public crate-ipcuco-0.1.2 (c (n "ipcuco") (v "0.1.2") (h "10c4xjihxbbdy7ykj286w8h25x45a8f5gq2i06hnjhakjp1plips")))

