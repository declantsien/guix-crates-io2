(define-module (crates-io ip _n ip_network_table) #:use-module (crates-io))

(define-public crate-ip_network_table-0.1.0 (c (n "ip_network_table") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "ip_network") (r "^0.3.0") (d #t) (k 0)) (d (n "treebitmap") (r "^0.3.1") (d #t) (k 0)))) (h "0zc48vryrdq2nlp99qa7dinwvmdq2cykig259z21xsyg9pc9zyql")))

(define-public crate-ip_network_table-0.1.1 (c (n "ip_network_table") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "ip_network") (r "^0.3.0") (d #t) (k 0)) (d (n "treebitmap") (r "^0.4.0") (d #t) (k 0)))) (h "1hj4zqj3v1hy1xq70vvyic28wqz4vrf687q371p5b8r1kphka2bg")))

(define-public crate-ip_network_table-0.1.2 (c (n "ip_network_table") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "ip_network") (r "^0.3.0") (d #t) (k 0)) (d (n "ip_network_table-deps-treebitmap") (r "^0.5.0") (d #t) (k 0)))) (h "1gi8yz6vip891zyfph0mp8292z6w9i95cnv1gmn3cp14c5pc4ngs")))

(define-public crate-ip_network_table-0.2.0 (c (n "ip_network_table") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "ip_network") (r "^0.4.0") (d #t) (k 0)) (d (n "ip_network_table-deps-treebitmap") (r "^0.5.0") (d #t) (k 0)))) (h "1h5jghwk1nm5lmilylwpqsf4qwbsvxx6ygyzbs6gxqn5qp7vg6a0")))

