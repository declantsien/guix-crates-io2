(define-module (crates-io ip _n ip_network) #:use-module (crates-io))

(define-public crate-ip_network-0.1.0 (c (n "ip_network") (v "0.1.0") (d (list (d (n "extprim") (r "^1.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jwjdrsbq50lqqb50gwy3lxl5gbf8ad8mlch2x5qaf6mfj9q323j") (f (quote (("default"))))))

(define-public crate-ip_network-0.1.1 (c (n "ip_network") (v "0.1.1") (d (list (d (n "extprim") (r "^1.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0asbnns1fy1agrjwq883v9syscllkqixz699f1vmz9f7nkqzpsrg") (f (quote (("default"))))))

(define-public crate-ip_network-0.1.2 (c (n "ip_network") (v "0.1.2") (d (list (d (n "extprim") (r "~1.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fgm0gfxbypd4dgi3cys36b1p9jr94w5qijwmz89rqxg23byfj3n") (f (quote (("default"))))))

(define-public crate-ip_network-0.1.3 (c (n "ip_network") (v "0.1.3") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "extprim") (r "~1.5") (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13jds0s0jxpswy80ra210984i7hxhgcab037732229dgcbmbz082")))

(define-public crate-ip_network-0.2.0 (c (n "ip_network") (v "0.2.0") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l0rhvclrxx1f4wimf2wil5xyj4nk3d8ndn126gcvhsbv9h3nxs1")))

(define-public crate-ip_network-0.2.1 (c (n "ip_network") (v "0.2.1") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qg0087ykp5klzvmyyag2qq265r2czibnxrfffgvr5ifcgzj6yrq")))

(define-public crate-ip_network-0.2.2 (c (n "ip_network") (v "0.2.2") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hw30lvyrl51x7jg6r8qrdw2jx3z2z63xjpxzklv5nzlbznbhprn")))

(define-public crate-ip_network-0.2.3 (c (n "ip_network") (v "0.2.3") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0935fn35a21yn5ylyqvsg4h23zydf8r0zlmnaijg2ps11vmm8j7s")))

(define-public crate-ip_network-0.3.0 (c (n "ip_network") (v "0.3.0") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1xm4gzsi8r4zrxa25nrmdxnyyzk7nxwpbjhwqy6hl0xk6qvwr05d")))

(define-public crate-ip_network-0.3.1 (c (n "ip_network") (v "0.3.1") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1rqh0ciwv9ly1mbxb5qfs63zvxg15jslrixqwqv9w1l75brcsb9j")))

(define-public crate-ip_network-0.3.2 (c (n "ip_network") (v "0.3.2") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0k5n7xgqrsg7zyckqm1aipp63zjxwnw5d3x7g35nzx3kyq67pmr7")))

(define-public crate-ip_network-0.3.3 (c (n "ip_network") (v "0.3.3") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "15ppi1r258zpwh2bphvnwzinv22jjlbpllbnabg0rzv56kprplrs")))

(define-public crate-ip_network-0.3.4 (c (n "ip_network") (v "0.3.4") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0vvw901wcg3jmfcdznpib12kzxk2r4g62igppvfrxxrmq18mkq9f")))

(define-public crate-ip_network-0.4.0 (c (n "ip_network") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "00qkd4q3v3hac9qkbx9l3w4pl8gvvm1rk4xbdyi1qjig7malddq9")))

(define-public crate-ip_network-0.4.1 (c (n "ip_network") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1c8fkq601gj8bqqf36jf96pglw1m8j470vaxmacz5clq19y08bxa")))

