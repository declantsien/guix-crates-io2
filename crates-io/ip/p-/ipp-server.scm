(define-module (crates-io ip p- ipp-server) #:use-module (crates-io))

(define-public crate-ipp-server-0.3.0 (c (n "ipp-server") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)) (d (n "ipp-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1gmj230bbh6qxvm6n4lsnpxvymmnrs8x0gjn9k79d1yss6ijx0sz")))

