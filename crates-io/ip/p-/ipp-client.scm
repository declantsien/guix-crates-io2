(define-module (crates-io ip p- ipp-client) #:use-module (crates-io))

(define-public crate-ipp-client-0.3.0 (c (n "ipp-client") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "ipp-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1qfw2vyhqx7ry8ng736xn1l4wz30qsdsizp8lvkvplyqsy82wwch")))

