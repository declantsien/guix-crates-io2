(define-module (crates-io ip p- ipp-headers-sys) #:use-module (crates-io))

(define-public crate-ipp-headers-sys-0.1.0 (c (n "ipp-headers-sys") (v "0.1.0") (d (list (d (n "ipp-ctypes") (r "^0.1") (d #t) (k 0)))) (h "145f1cqm79vfynix894yspw87ik6jyq1cqcrbnzasjzyfvc5avqv")))

(define-public crate-ipp-headers-sys-0.2.0 (c (n "ipp-headers-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "ipp-ctypes") (r "^0.1") (d #t) (k 0)))) (h "1svry5jhwh4k56hj0lkwkjw4cacqlwipwwy9ign7nyf41nlzxh5c")))

(define-public crate-ipp-headers-sys-0.3.0 (c (n "ipp-headers-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "ipp-ctypes") (r "^0.1") (d #t) (k 0)))) (h "1fmqycvp8fm79yg8cbzpgsyg862vlyg7mkccr8zwph37n9k2y07j")))

(define-public crate-ipp-headers-sys-0.4.0 (c (n "ipp-headers-sys") (v "0.4.0") (h "0ilxf1qp9f92np3kdr1rsvnl1vwg201xd4bkmh4my7d2amgzc5a1") (f (quote (("2019") ("2018") ("2017"))))))

(define-public crate-ipp-headers-sys-0.4.2 (c (n "ipp-headers-sys") (v "0.4.2") (h "0ziszfbvfgw3ah6m2k8zhx1a9nycg9sv8wml9c5nd63kflsmm13h") (f (quote (("2019") ("2018") ("2017"))))))

(define-public crate-ipp-headers-sys-0.4.3 (c (n "ipp-headers-sys") (v "0.4.3") (h "1447h3j8zbfkvmnsajmfy0r6gjchsimgrcwg1cw7w8xs2sscxkgs") (f (quote (("2019") ("2018") ("2017"))))))

