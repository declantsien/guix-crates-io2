(define-module (crates-io ip p- ipp-util) #:use-module (crates-io))

(define-public crate-ipp-util-0.3.0 (c (n "ipp-util") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "ipp-client") (r "^0.3.0") (d #t) (k 0)) (d (n "ipp-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1a9kffx65lnbmf5q2whqpz4mrjax8png352wb58wkdlahmagk7a0")))

(define-public crate-ipp-util-1.0.0 (c (n "ipp-util") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ipp") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qngbp9daqdmvgrdbjkqyhr9lb1365d02k5vc96sjcsf993gxmwf")))

(define-public crate-ipp-util-1.0.1 (c (n "ipp-util") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ipp") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "03wzmcxg6xxkic34afm8s94p7akhb72wz5h0597fzhpdicppqn0z")))

(define-public crate-ipp-util-2.0.0 (c (n "ipp-util") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ipp") (r "^2.0.0") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "0wyav0adiywql2jcp54wk0myxb2a3s8412wkrhgmi8za2fwm8646") (f (quote (("default" "client-isahc") ("client-reqwest" "ipp/client-reqwest" "tokio") ("client-isahc" "ipp/client-isahc"))))))

(define-public crate-ipp-util-3.0.0 (c (n "ipp-util") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "ipp") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0nkq6i6cs0sa4xi60zyl6kaksc7yqpyk6dw9g11mwxnskbydb6d6")))

(define-public crate-ipp-util-3.0.1 (c (n "ipp-util") (v "3.0.1") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ipp") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0b3kiifj7jgjdirm4fj7jq802pggpyp06mv6jf9y0ajc5a4c05gg")))

(define-public crate-ipp-util-3.0.2 (c (n "ipp-util") (v "3.0.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ipp") (r "^3.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "11kkd3s70zhxw596814n3ichilfg7wgk7h34npnxm0ipbz198lkb")))

(define-public crate-ipp-util-3.0.3 (c (n "ipp-util") (v "3.0.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ipp") (r "^3.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0wh4vpdym7gpqs5jn2y5qnlmra1ahndbab8lvql4drca8rw0766g")))

(define-public crate-ipp-util-4.0.0 (c (n "ipp-util") (v "4.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipp") (r "^4.0.0") (f (quote ("client-tls"))) (k 0)))) (h "12axyj0yw826dw3prp05s5x3mz60grj58v4fxbg7wd8rk3xsd6f1")))

(define-public crate-ipp-util-5.0.0 (c (n "ipp-util") (v "5.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipp") (r "^5.0.0") (f (quote ("client-tls"))) (k 0)))) (h "1p4fdb7k4pis7z9p35qxqws32pfip76mdygn7samfddmyz6b5g95")))

(define-public crate-ipp-util-5.0.1 (c (n "ipp-util") (v "5.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipp") (r "^5.0.1") (f (quote ("client-tls"))) (k 0)))) (h "04182wlx1z8cyrv0chf9vxw1n2707f6nm0z9jlidxskx5v50xqim")))

(define-public crate-ipp-util-5.0.2 (c (n "ipp-util") (v "5.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipp") (r "^5.0.2") (f (quote ("client-tls"))) (k 0)))) (h "156jcqk0h9sl4n8j134k84i6p0m8nmf14k2c7gicz0l7q4i5aibn")))

(define-public crate-ipp-util-5.0.3 (c (n "ipp-util") (v "5.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipp") (r "^5.0.3") (f (quote ("client-tls"))) (k 0)))) (h "0q2rqqa4s6qz0l9z3b2z0wvzl06i9v31q7il93bbl5ynb7lbnxgn")))

