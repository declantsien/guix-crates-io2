(define-module (crates-io ip p- ipp-proto) #:use-module (crates-io))

(define-public crate-ipp-proto-0.3.0 (c (n "ipp-proto") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.2") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "04y60ra4rj3svp343xvlpns0vx7zrigvcp6v3md2rglw6ng91qm4")))

