(define-module (crates-io ip p- ipp-sys-build-help) #:use-module (crates-io))

(define-public crate-ipp-sys-build-help-0.1.0 (c (n "ipp-sys-build-help") (v "0.1.0") (h "06j060xg9crh5md0yc71y5hmzly5043xgnlwyndk8lj37y0fn47k")))

(define-public crate-ipp-sys-build-help-0.1.1 (c (n "ipp-sys-build-help") (v "0.1.1") (h "0rjdg6r5fkw7azgq2bn5j2jmizx32nwmj66bh636jd9w7qsj92xg")))

(define-public crate-ipp-sys-build-help-0.1.2 (c (n "ipp-sys-build-help") (v "0.1.2") (h "1yn3xrd533pcyrxajhqmyz7hdfjmcihkf30f3df5k7wkmr9nmlbq")))

