(define-module (crates-io ip jd ipjdb) #:use-module (crates-io))

(define-public crate-ipjdb-0.1.0 (c (n "ipjdb") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1igpdcdwvgilkac0kwxhzf7hvwgznba9wjgcz2v0j7xc8arr02ic")))

(define-public crate-ipjdb-0.2.0 (c (n "ipjdb") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0acxmph5zdapg6b6pcjdvjzkan3ll83vgm6fflphyfcb9pj71avz")))

(define-public crate-ipjdb-0.3.0 (c (n "ipjdb") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1b11q4gb2k3cbib28rwnphjzk9ja4xskpgg13rh9mymfm2vdrjvr")))

(define-public crate-ipjdb-0.4.0 (c (n "ipjdb") (v "0.4.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0zhpsxwxbrqy6xnshswcyzg5zzx437xmnh0vzr8qwssybiwzh1z6")))

(define-public crate-ipjdb-0.5.0 (c (n "ipjdb") (v "0.5.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1xyds833vx1k1davqqicxr1q1rsnavgxnqsg544736l3l8plvj4b")))

(define-public crate-ipjdb-0.6.0 (c (n "ipjdb") (v "0.6.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1sdkkkk9jzzq3nrx1qp158djblq3j67i5f82v1w0rw4xr2ivi3zn")))

(define-public crate-ipjdb-0.7.0 (c (n "ipjdb") (v "0.7.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1skxcr3bjxjysx1fc6192j53vpzrgfzmv399dfhj5l0v9mnxbbfd")))

(define-public crate-ipjdb-0.8.0 (c (n "ipjdb") (v "0.8.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0bx6h3bmpprlilxb6wa1x3ib8bnfzxzxr3z8fabxxknj6943n2jg")))

(define-public crate-ipjdb-0.9.0 (c (n "ipjdb") (v "0.9.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1jzmybk8a422vca45h7ba927qmxvhfizjl0z2l9k1p7cg4xln0xb")))

(define-public crate-ipjdb-0.9.1 (c (n "ipjdb") (v "0.9.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0gkf24w9n59c3cbraj3khhkjy55i7frb6dhb32iqich29q14pi64")))

(define-public crate-ipjdb-0.10.0 (c (n "ipjdb") (v "0.10.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1p89k1jkbhvyvvjiaywkbdvm5bq40pv750vyv9b3hv9c3m67mkfn")))

(define-public crate-ipjdb-0.11.0 (c (n "ipjdb") (v "0.11.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "13qr8lf40lp2bzdb0nhykv6hi31iigkkr1wpsiba2iwj63qg6bsy")))

(define-public crate-ipjdb-0.11.1 (c (n "ipjdb") (v "0.11.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1d40mkwwmk8iy9cw2nrcwinr0dc1hsn1ypp5qf88mnh61fpfzf8n")))

(define-public crate-ipjdb-0.12.0 (c (n "ipjdb") (v "0.12.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "14g35z8jjxd8jnchkh4xn1xdrgxngx45jbmpyqii96if2izy7jdj")))

(define-public crate-ipjdb-0.13.0 (c (n "ipjdb") (v "0.13.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "02rd6cs2v11w4jagxdlmbpz49kicc56pk3bvm5qd6j6828fffmjv")))

(define-public crate-ipjdb-0.14.0 (c (n "ipjdb") (v "0.14.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0an3zmdmfjl7gm9jicrf1qc0phyfmi6q7zrh00lr7a98nzcbslcd")))

(define-public crate-ipjdb-0.15.0 (c (n "ipjdb") (v "0.15.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ri2mdvb1g6jmcx98imdzdf5qnn4zw37syvr42pwiqrggpixhjcd")))

(define-public crate-ipjdb-0.16.0 (c (n "ipjdb") (v "0.16.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1l8hzr1khym8bbzvg8a3hcr6g821ijgl4yzn5q3mn80lyyzbnpls")))

(define-public crate-ipjdb-0.16.1 (c (n "ipjdb") (v "0.16.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04dp5zmx9fxr7gakvrqlz8p91hargvgc5smsmygxhd9mjw00kak5")))

