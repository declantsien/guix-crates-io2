(define-module (crates-io ip #{2l}# ip2location-iso3166-2) #:use-module (crates-io))

(define-public crate-ip2location-iso3166-2-0.0.0 (c (n "ip2location-iso3166-2") (v "0.0.0") (h "0cfvsxqnqv3qs8dzw2kkzvs659aaa0vxxj5g2l60h6ahrd20qdlg")))

(define-public crate-ip2location-iso3166-2-0.1.0 (c (n "ip2location-iso3166-2") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "0b3fhcx0lc17amkwrpwh2gyfqm4v8d424wzgn9v7v0yzrp2mwj49") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-iso3166-2-0.2.0 (c (n "ip2location-iso3166-2") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "1vwb2mdivdgg98wqh6qvny3bin2w7wg8hqaimqfnlxm9rg2k3aq9") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-iso3166-2-0.3.0 (c (n "ip2location-iso3166-2") (v "0.3.0") (d (list (d (n "country-code") (r "^0.2.1") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "1xny2ydb4w5443lyvf66xnfb3dcrfhv70nrdjra50hjdbx1dk25d") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-iso3166-2-0.4.0 (c (n "ip2location-iso3166-2") (v "0.4.0") (d (list (d (n "country-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)))) (h "0swaqw37v2cpcjdi1r00vy8l8gp0bhv8slz0hcm98myq7g39m4k2") (f (quote (("default" "once_cell"))))))

