(define-module (crates-io ip #{2l}# ip2location-continent-multilingual) #:use-module (crates-io))

(define-public crate-ip2location-continent-multilingual-0.0.0 (c (n "ip2location-continent-multilingual") (v "0.0.0") (h "01aa0zf54w71mswyiqsdgifchn0jz9qaiqrdszlbia3s20acyz6r")))

(define-public crate-ip2location-continent-multilingual-0.1.0 (c (n "ip2location-continent-multilingual") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "0a9d0hr8xplp8bfh8lcxfwwl4a2l7whfmnnbkgcj9ly54qznziav") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-continent-multilingual-0.2.0 (c (n "ip2location-continent-multilingual") (v "0.2.0") (d (list (d (n "continent-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "country-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "language-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "0zkpvzv732aimhn33lcvk2r4s41dw56j2zi72gh3i8xgza7g9f20") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-continent-multilingual-0.3.0 (c (n "ip2location-continent-multilingual") (v "0.3.0") (d (list (d (n "continent-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "country-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1") (k 0)) (d (n "language-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)))) (h "0hk68vhps13r2xlvbjnc6153rg2g5wwbq6xcf69wkwd65j0jxj5i") (f (quote (("default" "once_cell"))))))

