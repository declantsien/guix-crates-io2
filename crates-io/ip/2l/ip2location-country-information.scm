(define-module (crates-io ip #{2l}# ip2location-country-information) #:use-module (crates-io))

(define-public crate-ip2location-country-information-0.0.0 (c (n "ip2location-country-information") (v "0.0.0") (h "026sflqpvrar7n5hcjd4yfnx6avfmk0spi84jkh8pj5xdxys4608")))

(define-public crate-ip2location-country-information-0.1.0 (c (n "ip2location-country-information") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "0w67d575sfl38wzwkklxxjrk73n3swgvdx6jl7fqdm49zvvlagax") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-country-information-0.2.0 (c (n "ip2location-country-information") (v "0.2.0") (d (list (d (n "country-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "currency-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "language-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "1ddildfkdmh7wai19drxjh7c90y0hdbmknrhd8zbxa47b4arlbb9") (f (quote (("default" "once_cell"))))))

(define-public crate-ip2location-country-information-0.3.0 (c (n "ip2location-country-information") (v "0.3.0") (d (list (d (n "country-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1") (k 0)) (d (n "currency-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "language-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)))) (h "18s3833cw950fyji33jsb7i2rrr0j1ywpja05civv79lrbv6sr5l") (f (quote (("default" "once_cell"))))))

