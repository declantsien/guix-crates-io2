(define-module (crates-io ip #{2l}# ip2location) #:use-module (crates-io))

(define-public crate-ip2location-0.1.0 (c (n "ip2location") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "1x5w4y9n9v1a0fdq4ggdc55gd5hzs89xqcsdld6hcp0ng9fp0gq4") (y #t)))

(define-public crate-ip2location-0.1.1 (c (n "ip2location") (v "0.1.1") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "18xfvii6hfilwcmw3gh7cv93d8wkpjiyzdr5iw0wfzjka9q6p1z0")))

(define-public crate-ip2location-0.1.2 (c (n "ip2location") (v "0.1.2") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "1z7vk8kv8prck013s73i2sh7arw3v98dck17k8fgvmglfxb2ixqv")))

(define-public crate-ip2location-0.1.3 (c (n "ip2location") (v "0.1.3") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "0867pf8s2fr6d0a14v0piw5wv92axrnyj8mbq7zyjqffkfyz8ixs")))

(define-public crate-ip2location-0.1.4 (c (n "ip2location") (v "0.1.4") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "16n27cig83d6d39ry18hfq1xv5b3ipcrzjh5ba23cj2ibzy588ql")))

(define-public crate-ip2location-0.1.5 (c (n "ip2location") (v "0.1.5") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "0z0hx89n2skxdna0611p819hy8xrc7hp1nfzfz1dabhbn7cj8qjf")))

(define-public crate-ip2location-0.2.0 (c (n "ip2location") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.0") (d #t) (k 0)))) (h "1fp4zvkzd036rp9ixsbxjc2s7hprb9x093vcs7w067g3d2w292s6")))

(define-public crate-ip2location-0.3.0 (c (n "ip2location") (v "0.3.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0nqiq24nq6w0lzdrjxy52dfc24h27cdywlmpv7wmp0a8wj49msgr")))

(define-public crate-ip2location-0.3.1 (c (n "ip2location") (v "0.3.1") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1rsmx2pdg76q598x2cvnwvnavnhk38a5afgbf4jvvva6s3b0g74c")))

(define-public crate-ip2location-0.3.2 (c (n "ip2location") (v "0.3.2") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0ah5amp9flhp8dwirzs824l4lgildmf4wf37gqrqhmimv01ay3x3")))

(define-public crate-ip2location-0.3.3 (c (n "ip2location") (v "0.3.3") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1br5q2lp2m7aky06nshyh2lw504xkl4clk3gfyngwcknx96v4lib")))

(define-public crate-ip2location-0.3.4 (c (n "ip2location") (v "0.3.4") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1lffdg2way25la0fgg0i7j4ab9zs8yw2fqqlk9496z1pzkiifrx0")))

(define-public crate-ip2location-0.3.5 (c (n "ip2location") (v "0.3.5") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0513kavby32fasbi5mic7n8r58vkd928alz5zpz752dpyyf5bbwl")))

(define-public crate-ip2location-0.4.1 (c (n "ip2location") (v "0.4.1") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "11ih0cwvw8drwh14lmlj0slk7h7zygqsjywgnz88n5wv067yd6pv")))

(define-public crate-ip2location-0.4.2 (c (n "ip2location") (v "0.4.2") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1l25jbarb6r4xpizvjxs81iqfbb01mp9s8rrvv0vp2z5b66l74az")))

(define-public crate-ip2location-0.4.3 (c (n "ip2location") (v "0.4.3") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1rbhznxa0mshdcbil9nmni5nlfn2n4li9cq4nfqsniqclcr668l6")))

(define-public crate-ip2location-0.5.0 (c (n "ip2location") (v "0.5.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "0sbcq5a0lwgrmfgk6773zlvx5765lbpwyqsv5prsp5cj7c4i25bw")))

