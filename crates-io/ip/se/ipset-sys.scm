(define-module (crates-io ip se ipset-sys) #:use-module (crates-io))

(define-public crate-ipset-sys-0.1.0 (c (n "ipset-sys") (v "0.1.0") (h "0s007l5b4cq8fvmp3gjgmfs7q1ca8kvi2lf7a0c2ljmllmvmq9n7")))

(define-public crate-ipset-sys-0.1.1 (c (n "ipset-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1l2zijgd9717qkipz1vd4kjbj8ymq7lkrbjslv3hwinh3z2cnyii")))

(define-public crate-ipset-sys-0.1.2 (c (n "ipset-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "050bqbp0mpgxhdm17p90sinllxsmamklv9qb3m5kq6p8cfrldwlr")))

(define-public crate-ipset-sys-0.1.4 (c (n "ipset-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14sfqxzz9ljkyl6ggabv9njzmnssy2bcmb3bzpymg113bxw5fypj")))

(define-public crate-ipset-sys-0.1.5 (c (n "ipset-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r8p5cznqy24kn6540xhxzk69n2bmxdljknjcp0q77gmv6f54bx3")))

