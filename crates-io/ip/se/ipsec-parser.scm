(define-module (crates-io ip se ipsec-parser) #:use-module (crates-io))

(define-public crate-ipsec-parser-0.1.0 (c (n "ipsec-parser") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4.0") (d #t) (k 0)))) (h "095fv1fz71vnx8w7i2c4wb5za8scgydxpx7r39jxy44r6s1h9xss")))

(define-public crate-ipsec-parser-0.2.0 (c (n "ipsec-parser") (v "0.2.0") (d (list (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1lcn8fj2idmxwdqyrzk330w7z0nh4w70lam8bv3kq30zpn8h0kcz")))

(define-public crate-ipsec-parser-0.3.0 (c (n "ipsec-parser") (v "0.3.0") (d (list (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0x81qaw6jda7kzq9z06ri1d4dmqg9lss9dxxy1qwqa3ndlx1hq35")))

(define-public crate-ipsec-parser-0.4.0 (c (n "ipsec-parser") (v "0.4.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "072594gmwdd3sgnhm2s2l5h850a4qi42vmak0459cm909y7n34ak")))

(define-public crate-ipsec-parser-0.4.1 (c (n "ipsec-parser") (v "0.4.1") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "0dyv2qffpl6gsgkhmgdfc85nxwng7s12y64hpbgzgrcp4k51s5m1")))

(define-public crate-ipsec-parser-0.5.0 (c (n "ipsec-parser") (v "0.5.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^2.0") (d #t) (k 0)))) (h "02p99y7bjl71v1pkccnm62zqqhivyk4b0wl72z4sy9ykn62rg82g")))

(define-public crate-ipsec-parser-0.6.0 (c (n "ipsec-parser") (v "0.6.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)))) (h "1fpd0mvyd08ygf93pgdgnfq64ba6pwg6032amwljc5785vgrv3ka")))

(define-public crate-ipsec-parser-0.7.0 (c (n "ipsec-parser") (v "0.7.0") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)))) (h "1lfxjk27sg3xhx5qpyvfpsd721a1nvs73zw0332wp2z7blz43y1c")))

