(define-module (crates-io ip se ipset) #:use-module (crates-io))

(define-public crate-ipset-0.1.0 (c (n "ipset") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bjaf4rf24m7jqf442khsszyyhmbkjvgyr8yrxqrn9i4jly6lhan")))

(define-public crate-ipset-0.1.1 (c (n "ipset") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13nv2fd59m0k46jprh2gb5g8hy9s1syfh5a5incph5lqy79jziip")))

(define-public crate-ipset-0.1.2 (c (n "ipset") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00q5gr7bmf0nzz81krbqiig5bln8gnisdnaqx9vydgmkp74gryvj")))

(define-public crate-ipset-0.2.0 (c (n "ipset") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s7wdwf9hicysvyq317dwiw28v57sd4yk396fm5qrysz7zf3b7kg")))

(define-public crate-ipset-0.2.1 (c (n "ipset") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z5rjp087b3snbjk0f58n6bmml8icmkp56g321kn8ppcrkr2nr4v")))

(define-public crate-ipset-0.2.2 (c (n "ipset") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h612y5pk9lfj8zfd2mhcq6xfn2aa7hr3k9l25qf79qa5s5yvzv4")))

(define-public crate-ipset-0.2.3 (c (n "ipset") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hj6319d67046pf18sgj7nx10cdf52p2gj7bv3v4fnbicnvmhpqm")))

(define-public crate-ipset-0.2.4 (c (n "ipset") (v "0.2.4") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bkvdsfh6abqnai3ckzl1bsv450y08ryw3ixvkscl63rnhaz4bsr")))

(define-public crate-ipset-0.3.0 (c (n "ipset") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z06zwqlwc0rwhknxq13ni3aw3skk66y2i34k0zhhrxsa7q8zkw2")))

(define-public crate-ipset-0.4.0 (c (n "ipset") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xdddrxmfm9jvw2027vd5pasf66l666rsl31w3z86pflk68rvvz1")))

(define-public crate-ipset-0.5.0 (c (n "ipset") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipset_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13jhqnrc0wph6rrdcif5pz57w2d3a81hbmh54wdsdyjinvpl420r") (l "ipset")))

(define-public crate-ipset-0.6.0 (c (n "ipset") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipset_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "037hv1m559bxn0p19c7a7ldn23pfn6sk8igrpafjppjb5cq54ykm") (l "ipset")))

(define-public crate-ipset-0.7.1 (c (n "ipset") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipset_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mwy0qa0pjfr006a37r4wmdm7mj2q78cpamxng6yjp5slvfmqpmm") (l "ipset")))

