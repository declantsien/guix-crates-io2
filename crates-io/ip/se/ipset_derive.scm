(define-module (crates-io ip se ipset_derive) #:use-module (crates-io))

(define-public crate-ipset_derive-0.1.0 (c (n "ipset_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hrj595yy255xmkglflrvplpb0vj09bg6ij5js1z0cam254bxlff")))

(define-public crate-ipset_derive-0.1.1 (c (n "ipset_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q01lsi839lry938l7xn5l52jbchn8f87wrxx4wxzfkrx1j6vny4")))

