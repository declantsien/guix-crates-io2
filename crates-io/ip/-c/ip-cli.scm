(define-module (crates-io ip -c ip-cli) #:use-module (crates-io))

(define-public crate-ip-cli-0.1.2 (c (n "ip-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "ipdb") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1ci2wfp80vzpl51plaq7pbvkzil48s5kzl978nyr2jiq69503qrc")))

(define-public crate-ip-cli-0.1.3 (c (n "ip-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "copy_to_output") (r "^2") (d #t) (k 1)) (d (n "ipdb") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "17mgpv9xss744jcjp8h9brpg2rp6s3z8hhxxv9n0w6bs4gy0vz3f")))

(define-public crate-ip-cli-0.1.4 (c (n "ip-cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 1)) (d (n "ipdb") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1p41z77m4z2jfqarn0hy4ma7fgydh9lnqwbzgwxsb48ynmacd642")))

(define-public crate-ip-cli-0.1.5 (c (n "ip-cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 1)) (d (n "ipdb") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1ic4lsib7pf753ma4p6m72mi9lrkf7ksp626d5pn0pckpvf2h1al")))

