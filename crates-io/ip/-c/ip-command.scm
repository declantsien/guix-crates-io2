(define-module (crates-io ip -c ip-command) #:use-module (crates-io))

(define-public crate-ip-command-0.1.0 (c (n "ip-command") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-command-opts") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "snafu") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1cz06gpl2dy2d9xhhvzhp02b3g5mngmpkncyhly0a133fww35w")))

