(define-module (crates-io ip -c ip-combinator) #:use-module (crates-io))

(define-public crate-ip-combinator-0.1.0 (c (n "ip-combinator") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)))) (h "14s5a34ph2z3jwapfvzb7liy62prhb5gk3lvasp7d3l4j5bdlfz9")))

(define-public crate-ip-combinator-0.1.1 (c (n "ip-combinator") (v "0.1.1") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)))) (h "13lxq594c7aj5paw5dmkmhy3qpl6v0yrrblwfxw46g0x4q4g8kv1")))

