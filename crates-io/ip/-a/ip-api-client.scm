(define-module (crates-io ip -a ip-api-client) #:use-module (crates-io))

(define-public crate-ip-api-client-0.2.0 (c (n "ip-api-client") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14.18") (f (quote ("client" "http1" "runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1pa5vif5manjcmgb9jlwv5vqndjhwwva0z9vzq9165xv235c50z2")))

(define-public crate-ip-api-client-0.3.0 (c (n "ip-api-client") (v "0.3.0") (d (list (d (n "hyper") (r "^0.14.18") (f (quote ("client" "http1" "runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "09i47y5qsjbx8dyk0538dkaj533akgjnfm7z579hw96fahn58xps")))

(define-public crate-ip-api-client-0.4.0 (c (n "ip-api-client") (v "0.4.0") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("client" "http1" "runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0npvdmshnj8vdplav5xsi1bgq6jr0cs0q8j774xc3crlcy5r8g2h")))

(define-public crate-ip-api-client-0.4.1 (c (n "ip-api-client") (v "0.4.1") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("client" "http1" "runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0brr6cy5qa2afx5qkj7nsnd4ciwm55azfpxk8qjvdrx83zr486j9")))

(define-public crate-ip-api-client-0.4.2 (c (n "ip-api-client") (v "0.4.2") (d (list (d (n "hyper") (r "^0.14.26") (f (quote ("client" "http1" "runtime"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "1rkv20n5cw1646cz6is03kyls8hchqbw5y41ahf8d5zwdj3cnsly")))

(define-public crate-ip-api-client-0.5.0 (c (n "ip-api-client") (v "0.5.0") (d (list (d (n "hyper") (r "^0.14.28") (f (quote ("client" "http1" "runtime"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0jkd44sxd9csyigawvw2cbaz41lk8xib7i893hn0fk6jppq07835")))

(define-public crate-ip-api-client-0.5.1 (c (n "ip-api-client") (v "0.5.1") (d (list (d (n "hyper") (r "^0.14.28") (f (quote ("client" "http1" "runtime"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1wyvpwjddi10xcf9sfycljqnqhpd3l9xfhj4n9r4jgli68pw7qcp")))

