(define-module (crates-io ip ne ipnet) #:use-module (crates-io))

(define-public crate-ipnet-0.1.0 (c (n "ipnet") (v "0.1.0") (h "0xzk2006f2cprd6k4f48l68ylrkwrqxzw9ys411qixa2gz1gdqvq")))

(define-public crate-ipnet-0.11.0 (c (n "ipnet") (v "0.11.0") (h "1dr1ipr92546ml89w2yy86bpcdwg47xswkjbf84chr6xzlnwzq9j")))

(define-public crate-ipnet-0.11.1 (c (n "ipnet") (v "0.11.1") (h "1vn9rhadh7p3nagzbg9rizhiinxpbcq2rbs1ym15bv7pgrwbkfbv")))

(define-public crate-ipnet-0.12.0 (c (n "ipnet") (v "0.12.0") (h "0qnfp9zlamj32hnvwg949h39fypbksw3hn0zvp9cc5zas3nr97b5")))

(define-public crate-ipnet-0.13.0 (c (n "ipnet") (v "0.13.0") (h "07xydvwyr15fnkb6jj026hq4vwcr5fkfwfc38pnqaf9y69s07hzz")))

(define-public crate-ipnet-0.13.1 (c (n "ipnet") (v "0.13.1") (h "0q6jar6n95gir9blwfqyl1fvjqsmsq773m3vw6xd9rx0mqb4rbl7")))

(define-public crate-ipnet-0.13.2 (c (n "ipnet") (v "0.13.2") (h "0ixy6nan4ygzlk4kwbkn152adpc3mmjvvxn1aal7fqslrjk78qqy")))

(define-public crate-ipnet-0.13.3 (c (n "ipnet") (v "0.13.3") (h "0yyj3h3xrrmg74k87sdm0m429n1nxvfj1gm5wymrnn11myb0zb20")))

(define-public crate-ipnet-0.14.0 (c (n "ipnet") (v "0.14.0") (h "1xxz8bsr8a20zma4zh9f0psirgcqdh3nl3w5wbaf5ml4v4r6wiir")))

(define-public crate-ipnet-0.15.0 (c (n "ipnet") (v "0.15.0") (h "0xzdcfkiisf984q8ma4lq61ndps0cpsfyyg1hc1kdvx11klalivx")))

(define-public crate-ipnet-0.15.1 (c (n "ipnet") (v "0.15.1") (h "0h5c4frrwl1am53nl00xavvch9y083r4j9s6bmrlnrrhjng4ba9n")))

(define-public crate-ipnet-0.15.2 (c (n "ipnet") (v "0.15.2") (h "1r464mxvc8jqf29asmrfgi21q49pi7yk17sii13pkx1nl1zzdnl5")))

(define-public crate-ipnet-0.15.3 (c (n "ipnet") (v "0.15.3") (h "0829c0r9wfd4sk0xnhzfxvcdp6krn2ykjklyq1c3m15pdrw7dbib")))

(define-public crate-ipnet-0.16.0 (c (n "ipnet") (v "0.16.0") (h "0pqfd3q87620pmgns59d5cbw89g40d5mqchykvwxbmhc6fxh1kh9")))

(define-public crate-ipnet-0.16.1 (c (n "ipnet") (v "0.16.1") (h "1q1ilww76grmskvmxn1liw683x0q647cjkp75vji7732xf6a58fv")))

(define-public crate-ipnet-0.16.2 (c (n "ipnet") (v "0.16.2") (h "0l1x71xjsn828vigbrk281k1m0b6qmbvc3s6yv143xml6d7v398r")))

(define-public crate-ipnet-0.17.0 (c (n "ipnet") (v "0.17.0") (h "0y9a1ihz733vxyvaq1k4h0byf9yxicpscpl3vm1p32qd4p0lj7q7")))

(define-public crate-ipnet-0.17.1 (c (n "ipnet") (v "0.17.1") (h "03m6z3ix0xx0afyadcah3yp42crc4vgihml0vfrs18c8v245vz4r")))

(define-public crate-ipnet-0.18.0 (c (n "ipnet") (v "0.18.0") (h "0rmh8iby55slw5r9xpcsfqh3j4c1g2v5k6vyywlcz75bl5zg0wy7")))

(define-public crate-ipnet-0.18.1 (c (n "ipnet") (v "0.18.1") (h "18pbm4b0gvl70ms932qm056wbsfnyj624piqh56i7pdnvz6wh9qg")))

(define-public crate-ipnet-0.18.2 (c (n "ipnet") (v "0.18.2") (h "0qlm4i342xsdg93x6yhkq2vxzicghinbg7fxfspv1iwz9vqjcgyf")))

(define-public crate-ipnet-0.18.3 (c (n "ipnet") (v "0.18.3") (h "03hc64l4dnz0fh5jnywjzx5l0m64ql6srywcmarh8c4p6vpiq9f5")))

(define-public crate-ipnet-0.19.0 (c (n "ipnet") (v "0.19.0") (h "1jldpcql7kga7xwx72a2d8p0x72xgi877ihk7lmkan45x5fjr03v")))

(define-public crate-ipnet-0.20.0 (c (n "ipnet") (v "0.20.0") (h "0bb6a3fnng4qyaw4nm39ma6ykm2d8r1ds72yks9r3kns5hvz8p8h")))

(define-public crate-ipnet-0.20.1 (c (n "ipnet") (v "0.20.1") (h "0n6lmq6sml210splxbdzzfyxz8cczpj15625srlf6prqrijflirv")))

(define-public crate-ipnet-0.21.0 (c (n "ipnet") (v "0.21.0") (h "01cznf0mcqc1wd7011lxkb39c95n93adqbfr07hz63zm17rad0jf")))

(define-public crate-ipnet-0.21.1 (c (n "ipnet") (v "0.21.1") (h "0k67h0ldnj3zcs6zybhzdk12n7mxki0bxx6ccnp8ws3499naa85n")))

(define-public crate-ipnet-0.22.0 (c (n "ipnet") (v "0.22.0") (h "19738nqy36rvmaszn1hwh9yj4zb9cgq767f967av0rv3xhf1m5ay")))

(define-public crate-ipnet-0.23.0 (c (n "ipnet") (v "0.23.0") (h "1g3my1fzl0jil4ych16bpx5qrkrfms8crgc0vqgdscxqnj89gbzs")))

(define-public crate-ipnet-0.23.1 (c (n "ipnet") (v "0.23.1") (h "0gbg0dzgygnhh9fzylay21fjsj1w26i45pf49s274kw2zly3wrwd")))

(define-public crate-ipnet-0.24.0 (c (n "ipnet") (v "0.24.0") (h "09s03x50zxvakp7msr1q1c62r2fkgpbs2i530mgyq8qiimsfmabw")))

(define-public crate-ipnet-0.24.1 (c (n "ipnet") (v "0.24.1") (h "17j7hlqikgwxli898y6vd3z30yfgrh8dsksvv6s7izb3nrc84nlq")))

(define-public crate-ipnet-0.24.2 (c (n "ipnet") (v "0.24.2") (h "10xrzj7vw6x57mwrpqg8mdaq8c6gmw83my393axxqm65jx80mc6g")))

(define-public crate-ipnet-0.24.3 (c (n "ipnet") (v "0.24.3") (h "0c14c4lqnjb6b95kb08jsifng4sm0bx5bq92cdxxajxh810420d9")))

(define-public crate-ipnet-0.25.0 (c (n "ipnet") (v "0.25.0") (h "0c04nh4n4n8njnllilr93vk3c7hf138hghrhrq293mzxb8d1dp6m")))

(define-public crate-ipnet-0.25.1 (c (n "ipnet") (v "0.25.1") (h "1gj2i34xfm4apzfa5b2h0p0n69lip616ifss893rpab1gv1618ci")))

(define-public crate-ipnet-0.26.0 (c (n "ipnet") (v "0.26.0") (h "1a9dms6i71ja7z3wzwvnnlr41iayk2vr5v7laryd5xdmvcarfanh")))

(define-public crate-ipnet-0.26.1 (c (n "ipnet") (v "0.26.1") (h "0faj3784akzmq1952m1jkvgjwf0vzjbv17i6qjgic0ij7hvcrpf4")))

(define-public crate-ipnet-0.26.2 (c (n "ipnet") (v "0.26.2") (h "0vazmnvi88vinicmaw1nsv5659k6xkkgcpa7zabvmvfa5d4ldyf8")))

(define-public crate-ipnet-0.26.3 (c (n "ipnet") (v "0.26.3") (h "1wic7l53gvjw91hr37l5lwnvc8dka0xav6n6wf4bvy9cm1pw7xzn")))

(define-public crate-ipnet-0.26.4 (c (n "ipnet") (v "0.26.4") (h "1zqwmw0hqjv2i7if44ylfkcp62zn13gazz4lhi4gpxbd7ph98975")))

(define-public crate-ipnet-0.26.5 (c (n "ipnet") (v "0.26.5") (h "1dgc28cy8m27hr7vmghyfsic3vyb456f2q12qswxiyk089f8k182")))

(define-public crate-ipnet-0.27.0 (c (n "ipnet") (v "0.27.0") (h "0wnxrfqfpwfs9hmrzy5mgb4ar4f6rw12lmb85zplv10qvl89l1wa")))

(define-public crate-ipnet-0.28.0 (c (n "ipnet") (v "0.28.0") (h "1ir7l6jz51jdjwx2a3l0kgyp6b2jn6f8n4s79ivy5n6bl3m7lw3w")))

(define-public crate-ipnet-0.28.1 (c (n "ipnet") (v "0.28.1") (h "1djjb05g83h5kp5wfgh9ylj86lhzzrk2qjbk6ampqahrn0cizq36")))

(define-public crate-ipnet-1.0.0-rc1 (c (n "ipnet") (v "1.0.0-rc1") (h "1lrvfkgjqh2va3rp3iqlyrc3dawb522asjg6a410y1l5jvsb5pj2")))

(define-public crate-ipnet-1.0.0 (c (n "ipnet") (v "1.0.0") (h "1vv3nwsgiw4mq4v4flx7wv9rybmylliz9fx0rk8syimd4wx8q9ji")))

(define-public crate-ipnet-1.1.0 (c (n "ipnet") (v "1.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ck0gwsl26y1j87ymqyi477zq78ww0bmqli04vnkf3ka9mj8y5n8") (f (quote (("with-serde" "serde" "serde_derive") ("default")))) (y #t)))

(define-public crate-ipnet-1.2.0 (c (n "ipnet") (v "1.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ny7hddkv9i54bh4gfcsskljjv9vil1870lv7ppn4v024dqm25p1") (f (quote (("with-serde" "serde"))))))

(define-public crate-ipnet-1.2.1 (c (n "ipnet") (v "1.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1n4sxvdxak56y8ki3bbwfxpq7lk05sqjfp5prj01x6bz17yz9j27") (f (quote (("with-serde" "serde"))))))

(define-public crate-ipnet-2.0.0-rc1 (c (n "ipnet") (v "2.0.0-rc1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0zx90i8q0skmkjy0r71969s5zkdy4wasdxps5kdiqm6x2g2301wn")))

(define-public crate-ipnet-2.0.0 (c (n "ipnet") (v "2.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1by2yinh68b6w3z82ricl5gj67g4jd7vyg9i5mywf07ps2h2s776")))

(define-public crate-ipnet-2.0.1 (c (n "ipnet") (v "2.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0j3a562796hsgxlaak5hn1x9j8xb41lyz2c90yx64vc8w315mhbc")))

(define-public crate-ipnet-2.1.0 (c (n "ipnet") (v "2.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "017g9pc4flwq6r07liq7pp3cfjbc2391gzcd1j32h0nv45mv1x7j")))

(define-public crate-ipnet-2.2.0 (c (n "ipnet") (v "2.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1yqn565vp0xq8pl0dva6q1ghfa96i6hrhvw13s63ilb3qmyhand8")))

(define-public crate-ipnet-2.3.0 (c (n "ipnet") (v "2.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0db147nh8jnxr23yxa7hwqn7dcjivdqi3aq4mgf2zgkqqqa2zgj7")))

(define-public crate-ipnet-2.3.1 (c (n "ipnet") (v "2.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1ad32j3kkbb0bgf5whzfkdw6843ywr48245dhk7c9gny5r7xdwk8")))

(define-public crate-ipnet-2.4.0 (c (n "ipnet") (v "2.4.0") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0z5wa0m27w3clja717kwmkbdn8hg1504kbgx2ffgs0nwjkh0xrrm") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.5.0 (c (n "ipnet") (v "2.5.0") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0asr5bwhbfxgxwappmvs0rvb0ncc5adnhfi9yiz4axlc9j1m97c7") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.5.1 (c (n "ipnet") (v "2.5.1") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ic7pm4df3waxc0vi9vy1wq5i5azzlccz2yrz6fyd28i2xhmb37q") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.6.0 (c (n "ipnet") (v "2.6.0") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1v8da7lr5kqbrjjsxifnqyv2bl14w5yaxaskwf3knbp19ix7p57c") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.7.0 (c (n "ipnet") (v "2.7.0") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0zli80q3brzac2nikawxldnzazbqz4606n0y4lxm95h6crpdkc0i") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.7.1 (c (n "ipnet") (v "2.7.1") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ilidjg9v561449x7cd3jqdmcl68fsksg7mma1a8jnckcbc2pqih") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.7.2 (c (n "ipnet") (v "2.7.2") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0zxmnidy5qha1i384fzjfxcsi0qvkbcp730h26q4z3dg54hyxdhj") (f (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2.8.0 (c (n "ipnet") (v "2.8.0") (d (list (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1rhlmw1cpgpba6414m05d2s0xbiblkmkmzjfjfc4a3sgswy9mci8") (f (quote (("std") ("json" "serde" "schemars") ("default" "std"))))))

(define-public crate-ipnet-2.9.0 (c (n "ipnet") (v "2.9.0") (d (list (d (n "heapless") (r "^0") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1hzrcysgwf0knf83ahb3535hrkw63mil88iqc6kjaryfblrqylcg") (f (quote (("std") ("ser_as_str" "heapless") ("json" "serde" "schemars") ("default" "std"))))))

