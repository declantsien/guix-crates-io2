(define-module (crates-io ip ne ipnet-trie) #:use-module (crates-io))

(define-public crate-ipnet-trie-0.0.1 (c (n "ipnet-trie") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)))) (h "0c4x2igfv7hv4lkg6cdsfy6vcjwsn6nkhhv1rkiq7aawqh06sh00")))

(define-public crate-ipnet-trie-0.0.2 (c (n "ipnet-trie") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)))) (h "10ibvdw1ff58bsvxix8sb6zgqlxcsqx1m0mikng7jhz2ag9w1z58")))

(define-public crate-ipnet-trie-0.1.0 (c (n "ipnet-trie") (v "0.1.0") (d (list (d (n "bgpkit-parser") (r "^0.10.0-beta.1") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ip_network_table-deps-treebitmap") (r "^0.5") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "oneio") (r "^0.15") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0r2vr81ffdmpj7hprnx89ilzl3wvx1zqkc4k6k864f8zrdnb8vdh") (f (quote (("export" "bincode") ("default"))))))

(define-public crate-ipnet-trie-0.2.0-beta.1 (c (n "ipnet-trie") (v "0.2.0-beta.1") (d (list (d (n "bgpkit-parser") (r "^0.10.0") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "oneio") (r "^0.16") (d #t) (k 2)) (d (n "prefix-trie") (r "^0.2.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0915b1gq7as3450xhilcbv3pwjfzhhpv707qvqkkm8kymcirfh6l") (f (quote (("export" "bincode") ("default"))))))

(define-public crate-ipnet-trie-0.2.0-beta.2 (c (n "ipnet-trie") (v "0.2.0-beta.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bgpkit-parser") (r "^0.10") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "oneio") (r "^0.16") (d #t) (k 2)) (d (n "prefix-trie") (r "^0.2.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0dr2mxs31gal3bk4p73hvis1b94p3yr0kcxkcj2npr3b3qac2bl5") (f (quote (("export" "bincode") ("default"))))))

