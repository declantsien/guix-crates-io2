(define-module (crates-io ip ne ipnetwork) #:use-module (crates-io))

(define-public crate-ipnetwork-0.1.0 (c (n "ipnetwork") (v "0.1.0") (h "1b93lm5h18nih4nigw2bd0mqiw72wb06ff3076zwgxarh9hnbahz")))

(define-public crate-ipnetwork-0.1.1 (c (n "ipnetwork") (v "0.1.1") (h "1nabrc8akx88axqliiyk5crbni9244iqj83w0d40hphi5h43risv")))

(define-public crate-ipnetwork-0.1.2 (c (n "ipnetwork") (v "0.1.2") (h "0x0xgdz07l7gs3ykd66zgh5npnnr1i8q2xlmg04nqi8x71iliak6")))

(define-public crate-ipnetwork-0.1.3 (c (n "ipnetwork") (v "0.1.3") (h "08zbz5rgsdb6cnpiifplb3i9ys5sxvh5w67ryk6k394llp5ifbx5")))

(define-public crate-ipnetwork-0.1.4 (c (n "ipnetwork") (v "0.1.4") (h "1s4pdpnmaz5fikyz5n12mn2zdx55sajcd0g6ynm5rly1yf0gwl48")))

(define-public crate-ipnetwork-0.1.5 (c (n "ipnetwork") (v "0.1.5") (h "1zn350kf5b3j34q5rf44lk13igc8zd9im5z2h3qc1mdmviidl3k8")))

(define-public crate-ipnetwork-0.1.6 (c (n "ipnetwork") (v "0.1.6") (h "0ry54scm8gjbfxg6gvz967j7m7d44n0y9zc2h64ly97jj4k83zvv")))

(define-public crate-ipnetwork-0.1.7 (c (n "ipnetwork") (v "0.1.7") (h "07z1a7y33w05kgh1ajdm2ljcgdsys064zzmbhgky5n763n0hg8sb")))

(define-public crate-ipnetwork-0.1.8 (c (n "ipnetwork") (v "0.1.8") (h "03kkn2ryd3hhgkp08219wv2f7biczyndc9pkf6ah36ddq60awglk")))

(define-public crate-ipnetwork-0.1.9 (c (n "ipnetwork") (v "0.1.9") (h "1p9dx7xzxw4dcrykjdl9pbajrhf979vmcv0nk6rmv373n6drbbmx")))

(define-public crate-ipnetwork-0.1.10 (c (n "ipnetwork") (v "0.1.10") (h "0i3h2fglvqcayfzlid1g4l36a4djw62c3dszq8wxgx0jm4kn3mj2")))

(define-public crate-ipnetwork-0.1.11 (c (n "ipnetwork") (v "0.1.11") (h "0v29dsxmqbz4fz3q98cmh9la7r99cpqadd29qg6gmiq8ga12dz49")))

(define-public crate-ipnetwork-0.2.1 (c (n "ipnetwork") (v "0.2.1") (h "07875f1p5r0n16wc38k99y8cc56g9azdghgzabbn402qpih0w9sz")))

(define-public crate-ipnetwork-0.2.2 (c (n "ipnetwork") (v "0.2.2") (h "1bbhijh5650j5qhyw775y69pch4mwk6ywqizvvb3gdih4y9wmyny")))

(define-public crate-ipnetwork-0.3.0 (c (n "ipnetwork") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "ip") (r "^1.0.0") (d #t) (k 0)))) (h "08wz2n2n815jaqagx91sfnva40alf6d6zd79k67if2s44bi1y9va") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.4.0 (c (n "ipnetwork") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "ip") (r "^1.0.0") (d #t) (k 0)))) (h "1l7bl8wfkj8fshw1c671xn5l080ca653jzckyz31q33hgzx25vgw") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.5.0 (c (n "ipnetwork") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "103cpsc0xvp82aivjlkwjm2cg3889c7ngc038qbfrjk5yw9byagi") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6.0 (c (n "ipnetwork") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "ip") (r "^1.0.0") (d #t) (k 0)))) (h "08v2shrqd2kv9cahj6waymw5qvbjxgq5phsz2jpq0v53v54ijrwl") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6.1 (c (n "ipnetwork") (v "0.6.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "1c9bvrdz9br270lksdxs7nix998xkrwy7dj623kgf0707hxq0403") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6.2 (c (n "ipnetwork") (v "0.6.2") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "0zzwj0babvlfly0hbv2w542ikpj8n66dgm90fgsmzd3749bmhf1y") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.7.0 (c (n "ipnetwork") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "02cgvjyjmld2l998q0ips15mkwy1wc930mbvzabni5wwdyvw38qg") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.8.0 (c (n "ipnetwork") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "1nhjs95arw13455adx0ancnlb16840nw71913f0y5c92mdkxj932") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.9.0 (c (n "ipnetwork") (v "0.9.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "1xp5crsc156qc57mn125am5v9pi1gn06wzzhi5c1cl71cdnapva6") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.10.0 (c (n "ipnetwork") (v "0.10.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "0rnnkm9hlcrzd7p0j59l29lq688phzjhhf6mlypvn20gjxpa2frs") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.11.0 (c (n "ipnetwork") (v "0.11.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "030jij4mfwhkygwnhaf6cdjb88p400q3586rsn6n8gz1af10w42r") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.0 (c (n "ipnetwork") (v "0.12.0") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "0iaplvhxj6y9bhm2dz662ps22221l4j9prgy2s56ypadfvyk86by") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.1 (c (n "ipnetwork") (v "0.12.1") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "00fasz01q5lk0mq18zv95xya798ki9pn8m4g6ydykm0q0czy3qwj") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.2 (c (n "ipnetwork") (v "0.12.2") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "1rng1508rblscfb5kmqd51bcax91qv85jfbbamli04ng6rcpn1gz") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.3 (c (n "ipnetwork") (v "0.12.3") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "06a4mg9prry53676hasz5nqh1xm95p4ag77y31ash0nyrw4zfhp8") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.4 (c (n "ipnetwork") (v "0.12.4") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "0fvlcpsfsh61ip5zs8ibsggfgd6qxz22w4i3khb1x26n6bi0kn10") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.5 (c (n "ipnetwork") (v "0.12.5") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "0q1r0381h06ai6sa9vk3s2931k69b44xxbcgrai9v2j4hfpgkgll") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.6 (c (n "ipnetwork") (v "0.12.6") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "0w7k2rxh8d8n7bgk0yzpr50mac7h3rrqy1iix20560435297cbi3") (f (quote (("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.7 (c (n "ipnetwork") (v "0.12.7") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)))) (h "0dmkcdpv6zy64jcsrs8jidzwx8bibxnmbqch9xlba950w88f4d11") (f (quote (("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12.8 (c (n "ipnetwork") (v "0.12.8") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.8.0, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (o #t) (d #t) (k 0)))) (h "1aq1109s1vrqa9z678zfin1vihf66bdkkbhym6mqm0lhmhck2y3h") (f (quote (("with-serde" "serde" "serde_derive") ("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.13.0 (c (n "ipnetwork") (v "0.13.0") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12cbcriz1x4g400y4zh7fg5v9jz0zrj3w5qa5388czy9bhphnxbv") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.13.1 (c (n "ipnetwork") (v "0.13.1") (d (list (d (n "clippy") (r "^0.0.104") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0770gaf64b4bpl82sw15kq54qfl3s5qvygdcdy0b1d910scqn78x") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.14.0 (c (n "ipnetwork") (v "0.14.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "serde") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1laaf6h7aml156vbjh227pjq4b95w1jngj1yd6dz2rvqdz465n5k") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.15.0 (c (n "ipnetwork") (v "0.15.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0h3zrz822g8ardxfbn5xd109spd12m7isalrxz5q1b9hnki64xxz") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.15.1 (c (n "ipnetwork") (v "0.15.1") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r ">= 0.8.0, < 2.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kdx1lfy5la37nk1wr0pmp83pgis2m81f98wm16yfx1kc7ixb7d6") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.16.0 (c (n "ipnetwork") (v "0.16.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r ">= 0.8.0, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8.0, < 2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07nkh9djfmkkwd0phkgrv977kfmvw4hmrn1xxw4cjyx23psskv5q") (f (quote (("dev" "clippy") ("default" "serde"))))))

(define-public crate-ipnetwork-0.17.0 (c (n "ipnetwork") (v "0.17.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sviri9ksb3cmhx3h0rcfy8pvpx7f0cx5ba1z87ydvf07amymhq2") (f (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.18.0 (c (n "ipnetwork") (v "0.18.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mfkcrw8dxys6vi9bpvk2x1dyc8qi5wvrpc8jqinnm43n4wxg220") (f (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.19.0 (c (n "ipnetwork") (v "0.19.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09yil123s063qqn9n9m6fg2v9rbgzlp9lkjs40zpbwq64rhz310z") (f (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.20.0 (c (n "ipnetwork") (v "0.20.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "does-it-json") (r "^0.0.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03hhmxyimz0800z44wl3z1ak8iw91xcnk7sgx5p5jinmx50naimz") (f (quote (("default" "serde"))))))

