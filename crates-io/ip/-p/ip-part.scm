(define-module (crates-io ip -p ip-part) #:use-module (crates-io))

(define-public crate-ip-part-0.1.0 (c (n "ip-part") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "01fpl119bal76wgdl4b9zw1yyp66sycljy3h9np3aibbdskix01l") (y #t)))

(define-public crate-ip-part-0.1.1 (c (n "ip-part") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1iqwcdy7z2hs8yzkf46ma08zgglan5l57gyg7avz0xb5vpzbspi1")))

