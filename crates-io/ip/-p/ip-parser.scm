(define-module (crates-io ip -p ip-parser) #:use-module (crates-io))

(define-public crate-ip-parser-0.1.0 (c (n "ip-parser") (v "0.1.0") (d (list (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "05704bmdy7kc6mlzw0yl1d4w2mznprvmv0yrjycv2d91jn4xvk4b")))

(define-public crate-ip-parser-0.1.1 (c (n "ip-parser") (v "0.1.1") (d (list (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1yypxdbn3iaiyp2bgb33rvp2b0jgy72kr5fmwcg252sqwqx5yh0i")))

(define-public crate-ip-parser-0.1.2 (c (n "ip-parser") (v "0.1.2") (d (list (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1gqnbbdvqza31l5h468bzvqd9s8h8pz6jsfhssr51l5p6qwms179")))

