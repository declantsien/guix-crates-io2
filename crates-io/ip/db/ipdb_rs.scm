(define-module (crates-io ip db ipdb_rs) #:use-module (crates-io))

(define-public crate-ipdb_rs-0.1.0 (c (n "ipdb_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pzy74gjckhc43bm6hmkhvd88gf0cd3ngrqq98d5p4qzgghcz5hl")))

