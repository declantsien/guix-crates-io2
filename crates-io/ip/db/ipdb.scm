(define-module (crates-io ip db ipdb) #:use-module (crates-io))

(define-public crate-ipdb-0.1.0 (c (n "ipdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipdb_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02l4j0hcjdh7njjr3gprvflhwacb73k11nc1cqw1hfh22pmhwz75")))

(define-public crate-ipdb-0.1.1 (c (n "ipdb") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipdb_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r4mqfvrh6r7dhcgjdg62spbic4826j4as7qd1bp5jfgfx1v0brj")))

(define-public crate-ipdb-0.1.2 (c (n "ipdb") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipdb_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s56slzzcf0i2lmr9l0r7lbjgnicga70lsawfcsfjpbc66v23c60")))

(define-public crate-ipdb-0.1.3 (c (n "ipdb") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipdb_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11zgg5x9sjifmv89q6v08i6y8dl1hf8v7g991rg68x5l39z91bl5")))

(define-public crate-ipdb-0.1.4 (c (n "ipdb") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipdb_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "145ap5z5x9dzpca3gbpdal91ishkk9xfjmabrbj6cckm9mlbc1sd")))

