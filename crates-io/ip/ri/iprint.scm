(define-module (crates-io ip ri iprint) #:use-module (crates-io))

(define-public crate-iprint-0.1.0 (c (n "iprint") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "1n873983ha5p5fbk699g83pngqnmiwg4k2c72iy1xnkrx61y66s2") (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1.1 (c (n "iprint") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "00wlqjidxrkajw97blwmpz7ljifwj61gs5pdcv8j1g9hif64knq1") (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1.2 (c (n "iprint") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "0xhw59wc57m6w6c706virlshwcflrgc9qf918576x9lfzav0vfwf") (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1.3 (c (n "iprint") (v "0.1.3") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "002wh9a367h7j2if1i2c0w84k3gbd7da5r2n702q99wljbcgkgf5") (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1.4 (c (n "iprint") (v "0.1.4") (h "0wvhb7mrylayxzgyqa50mwwv1vc951cwxkam2f0cidndx39rd4wl") (f (quote (("log"))))))

