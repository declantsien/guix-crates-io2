(define-module (crates-io ip ku ipku) #:use-module (crates-io))

(define-public crate-ipku-0.0.1 (c (n "ipku") (v "0.0.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l59cvcfkln2ipxbw92603ixp4dwpnb84m9l7x91vnzcm6jpfk8b")))

