(define-module (crates-io ip cs ipcs-cli) #:use-module (crates-io))

(define-public crate-ipcs-cli-0.0.0 (c (n "ipcs-cli") (v "0.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipcs-api") (r "^0.0.0") (d #t) (k 0)) (d (n "ipcs-node") (r "^0.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "02bdvb8i1nkbg44bmzi1fcgwflxqh7gmrd0iand6i8glf9y9v3kw")))

(define-public crate-ipcs-cli-0.0.1-alpha.1 (c (n "ipcs-cli") (v "0.0.1-alpha.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipcs-api") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "ipcs-node") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "19fp5fiszil4i1ipg0w7c2pkll4drl3g95d2rq783xam3a903mkl")))

(define-public crate-ipcs-cli-0.0.1 (c (n "ipcs-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipcs-api") (r "^0.0.1") (d #t) (k 0)) (d (n "ipcs-node") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0cbfw222rv0vqvizkbrqcggnnnf1hlkddacm9n60h0wv9k3n9sgq")))

(define-public crate-ipcs-cli-0.0.2 (c (n "ipcs-cli") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "ipcs-api") (r "^0.0.2") (d #t) (k 0)) (d (n "ipcs-node") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0vwqhi5wfij4w36wgfxbp0w22blkdjnialgdxapc5xwy5lsqnmx6")))

