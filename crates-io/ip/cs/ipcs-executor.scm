(define-module (crates-io ip cs ipcs-executor) #:use-module (crates-io))

(define-public crate-ipcs-executor-0.0.0 (c (n "ipcs-executor") (v "0.0.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wasmer") (r "^0.16.0") (d #t) (k 0) (p "wasmer-runtime")))) (h "14cyz9mb3pckp606j6gaq599l1h474rb55a7gr81ii23lj8p021j")))

(define-public crate-ipcs-executor-0.0.1-alpha.1 (c (n "ipcs-executor") (v "0.0.1-alpha.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wasmer") (r "^0.16.0") (d #t) (k 0) (p "wasmer-runtime")))) (h "0j7xbc3ycs2202cxm12mnclafn6g0m8vkc6fm69fx2888l9immnj")))

(define-public crate-ipcs-executor-0.0.1 (c (n "ipcs-executor") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wasmer") (r "^0.16.0") (d #t) (k 0) (p "wasmer-runtime")))) (h "057604c8v79za7i3k4jnyynjz85428cb9jv42davrs1adsaxv5af")))

(define-public crate-ipcs-executor-0.0.2 (c (n "ipcs-executor") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wasmer") (r "^0.16.0") (d #t) (k 0) (p "wasmer-runtime")))) (h "010kk4r7q5kbcpdjm190qijmpd12gqqrimwj41x3gzgqd7h8f12i")))

