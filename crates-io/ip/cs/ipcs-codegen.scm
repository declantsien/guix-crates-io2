(define-module (crates-io ip cs ipcs-codegen) #:use-module (crates-io))

(define-public crate-ipcs-codegen-0.0.0 (c (n "ipcs-codegen") (v "0.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0kqnghcxccgvmy2drq7rrlm5pm4jwzgcxfaq1zvja1xcjnmi05j3")))

(define-public crate-ipcs-codegen-0.0.1-alpha.1 (c (n "ipcs-codegen") (v "0.0.1-alpha.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "012kqssjnq082q316qh4qfa3adhcn1qjh6l8m9b7zp3cc2k95w4d")))

(define-public crate-ipcs-codegen-0.0.1 (c (n "ipcs-codegen") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0av1y2wkm0wcjc7qwlr31h3qj614gkx3yv8y8ip9b59f4wakix5c")))

(define-public crate-ipcs-codegen-0.0.2 (c (n "ipcs-codegen") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0akm394s8hfk80585nvkhvfm0lm3lg8076w7l6ah7w9jybjgv2w7")))

