(define-module (crates-io ip cs ipcs-api) #:use-module (crates-io))

(define-public crate-ipcs-api-0.0.0 (c (n "ipcs-api") (v "0.0.0") (d (list (d (n "apidefs") (r "^0.0.0") (d #t) (k 0) (p "ipcs-apidefs")) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0yi2zbanfd9j3i6f9kx53flm8ni26xwrfy7x432005fv43wkclw1")))

(define-public crate-ipcs-api-0.0.1-alpha.1 (c (n "ipcs-api") (v "0.0.1-alpha.1") (d (list (d (n "apidefs") (r "^0.0.1-alpha.1") (d #t) (k 0) (p "ipcs-apidefs")) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "176mj1a3l1zwzdzb56y4jr8lw8kyic71ph18jiw5qqj4m56nbb18")))

(define-public crate-ipcs-api-0.0.1 (c (n "ipcs-api") (v "0.0.1") (d (list (d (n "apidefs") (r "^0.0.1") (d #t) (k 0) (p "ipcs-apidefs")) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1s78xmpa0ln50kkfi34sbkr1c3klpg3syp94305pkgrqxjw1nxjd")))

(define-public crate-ipcs-api-0.0.2 (c (n "ipcs-api") (v "0.0.2") (d (list (d (n "apidefs") (r "^0.0.2") (d #t) (k 0) (p "ipcs-apidefs")) (d (n "reqwest") (r "^0.10.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0r2hjlhbcqvrlzmsy608alvmbp6zkb3ni58bvsrpajga4ang5gcg")))

