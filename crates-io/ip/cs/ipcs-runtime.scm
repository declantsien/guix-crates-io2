(define-module (crates-io ip cs ipcs-runtime) #:use-module (crates-io))

(define-public crate-ipcs-runtime-0.0.0 (c (n "ipcs-runtime") (v "0.0.0") (d (list (d (n "codegen") (r "^0.0.0") (d #t) (k 0) (p "ipcs-codegen")))) (h "1shiy0kzv8v7f56skp6j65avsns68d854rbpywmmxgli8agxaszy")))

(define-public crate-ipcs-runtime-0.0.1-alpha.1 (c (n "ipcs-runtime") (v "0.0.1-alpha.1") (d (list (d (n "codegen") (r "^0.0.1-alpha.1") (d #t) (k 0) (p "ipcs-codegen")))) (h "0hdhzjjmdgh2lwa9m9gw5lpxy89k1xxmc5x4f5minjdhgmpspvw2")))

(define-public crate-ipcs-runtime-0.0.1 (c (n "ipcs-runtime") (v "0.0.1") (d (list (d (n "codegen") (r "^0.0.1") (d #t) (k 0) (p "ipcs-codegen")))) (h "0y0pc17d5w9xnh7mjfd6vna8qd6bhls69zis1az4kgj7ac55z9ph")))

(define-public crate-ipcs-runtime-0.0.2 (c (n "ipcs-runtime") (v "0.0.2") (d (list (d (n "codegen") (r "^0.0.2") (d #t) (k 0) (p "ipcs-codegen")))) (h "0gdqry37cq9x1iy14fjda4hins0l573n6g4m8yrrrbfz1jr2jf8f")))

