(define-module (crates-io ip cs ipcs-apidefs) #:use-module (crates-io))

(define-public crate-ipcs-apidefs-0.0.0 (c (n "ipcs-apidefs") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kjm4w8zblwqcbqkfk09lj2lcpn6ds11671gwgzmz3g3ybjq9xl")))

(define-public crate-ipcs-apidefs-0.0.1-alpha.1 (c (n "ipcs-apidefs") (v "0.0.1-alpha.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ayf3p5d257cl82nmk5p4hi7x9f7vp5nkghdg6sq92khixc1ad5w")))

(define-public crate-ipcs-apidefs-0.0.1 (c (n "ipcs-apidefs") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08kfyrjlxcsrxc7mqczmza7252nfgd1gzcdzvfpc5b93qammm7yw")))

(define-public crate-ipcs-apidefs-0.0.2 (c (n "ipcs-apidefs") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l9v8j0s5kc3jigrmlc6zic23kj4cwayh0gx8h96by5w5vzassf2")))

