(define-module (crates-io ip #{2g}# ip2geo) #:use-module (crates-io))

(define-public crate-ip2geo-0.1.0 (c (n "ip2geo") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g1chbx5rykg1firrfr3far1hy8dmiiqgrzlnwz5i9gz97r55bdn")))

