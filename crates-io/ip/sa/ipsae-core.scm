(define-module (crates-io ip sa ipsae-core) #:use-module (crates-io))

(define-public crate-ipsae-core-0.1.0 (c (n "ipsae-core") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1f6xw32i617ns1n1w8z0kxbig4h0gyr2bi74fa9im6ql5wpqigzd")))

(define-public crate-ipsae-core-0.1.1 (c (n "ipsae-core") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kwk9lyc599iznmcinwqkj31b5ak5walhg72ll4xxn1kn1hkgb91")))

