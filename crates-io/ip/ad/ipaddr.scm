(define-module (crates-io ip ad ipaddr) #:use-module (crates-io))

(define-public crate-ipaddr-0.0.1 (c (n "ipaddr") (v "0.0.1") (h "0i43aci4as569rqr1a84wxhk1bag562a6qyc1p6q79yp2w2fyl1r") (y #t)))

(define-public crate-ipaddr-0.0.2 (c (n "ipaddr") (v "0.0.2") (h "0v3ff4pqadkwsvnz07rphvd3qfni1h33lwrf94xhmalsiqwlzpyk") (y #t)))

(define-public crate-ipaddr-0.0.3 (c (n "ipaddr") (v "0.0.3") (h "16alhb19b0dh3ms3pldxwq5pq48k2rj5fdq5kdv5dpgm6c5h14s6") (y #t)))

