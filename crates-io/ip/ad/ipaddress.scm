(define-module (crates-io ip ad ipaddress) #:use-module (crates-io))

(define-public crate-ipaddress-0.1.0 (c (n "ipaddress") (v "0.1.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0iybs740akrl64wavwzich09nijgy4q0zmady25bqs7p4hbq0w7w")))

(define-public crate-ipaddress-0.1.1 (c (n "ipaddress") (v "0.1.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1v7ylxljcg4wpzd471xjv67bl6w4y7y6vl3bksmvswc8nz4nxs2s")))

(define-public crate-ipaddress-0.1.2 (c (n "ipaddress") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1x85d87m93wigz34k4ccwkg8lk8mwmh6f0i1indspl54apk9ksf9")))

(define-public crate-ipaddress-0.1.3 (c (n "ipaddress") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1gvw8zf2f2hpd8bpr7c196bsfsl6nh2m37grdprvfssxckrvjywm")))

