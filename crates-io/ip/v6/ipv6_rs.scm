(define-module (crates-io ip v6 ipv6_rs) #:use-module (crates-io))

(define-public crate-ipv6_rs-0.1.0 (c (n "ipv6_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08vma7izxzq9rci7hilipc4n132bxy647a5qinvyxa2si5sp65hs")))

