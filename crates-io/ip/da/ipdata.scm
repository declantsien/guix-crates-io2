(define-module (crates-io ip da ipdata) #:use-module (crates-io))

(define-public crate-ipdata-0.1.0 (c (n "ipdata") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "16bsklv2sca3scl8zql095h8vvb1czxip1f8bah2ypn6zvvdcvl9")))

