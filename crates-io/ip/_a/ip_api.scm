(define-module (crates-io ip _a ip_api) #:use-module (crates-io))

(define-public crate-ip_api-0.1.0 (c (n "ip_api") (v "0.1.0") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1mb301mqfdaldgy27w2j7pvqfi6lna2mdqw3h4jwlw4fdm8ph1y9")))

(define-public crate-ip_api-0.1.1 (c (n "ip_api") (v "0.1.1") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1985b8v195qdvvm0wj7p5g7vqq62cvw0dbz35nix9phx1yjvd9mg")))

(define-public crate-ip_api-0.1.2 (c (n "ip_api") (v "0.1.2") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "08rg5cpvccl77aj1qpp6vcgha9af9ccswinh10mr03162qkda3hz")))

(define-public crate-ip_api-0.1.3 (c (n "ip_api") (v "0.1.3") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "07jcyvavs6jdg1szd32r6k3l985m6ir9c7hb3l61nkbnx60172bx")))

