(define-module (crates-io ip if ipify-client) #:use-module (crates-io))

(define-public crate-ipify-client-0.1.0 (c (n "ipify-client") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "hyper") (r "^0.12.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "1sxrd3k1hrg1sbvv3kwbzn7d9y6alp6x0yhpr5j9mmyladjc11s0")))

