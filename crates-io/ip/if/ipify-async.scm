(define-module (crates-io ip if ipify-async) #:use-module (crates-io))

(define-public crate-ipify-async-0.1.0 (c (n "ipify-async") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1dqn7sca0j2ayjxa0n4g44aqp7y4dfj7imq4g5fz8l0w8dxilvfg")))

(define-public crate-ipify-async-0.1.1 (c (n "ipify-async") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0w5z30xhvqvi5dsx6hhwvdjy7cv1zbgv6z61y2fib4j7pbf9bb0x")))

