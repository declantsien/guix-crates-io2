(define-module (crates-io ip if ipify-cli) #:use-module (crates-io))

(define-public crate-ipify-cli-0.1.0 (c (n "ipify-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "hyper") (r "^0.13.5") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1y7hphjvcm8y18ma399yxhfa8mqrzgy9v0x1b43kg9qg3x8jw29i")))

