(define-module (crates-io ip if ipify) #:use-module (crates-io))

(define-public crate-ipify-0.1.0 (c (n "ipify") (v "0.1.0") (d (list (d (n "hyper") (r "> 0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "> 0.2.0") (d #t) (k 0)))) (h "0qlw6ijcj61k0iihmwd8jaqpz87lj386iqwdi3sqygbyvsfg430j")))

