(define-module (crates-io ip ge ipgen) #:use-module (crates-io))

(define-public crate-ipgen-0.0.1 (c (n "ipgen") (v "0.0.1") (d (list (d (n "ipnetwork") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1yaxx0c9dniyzs3yxz492057zjd7mx9ldjjlxskn5i8i4aw26asa")))

(define-public crate-ipgen-0.0.2 (c (n "ipgen") (v "0.0.2") (d (list (d (n "ipnetwork") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1c4kxyrv94swb2c7vnab05an3m0cfkxc48cwya9bydpz7rw2ci1i")))

(define-public crate-ipgen-0.0.3 (c (n "ipgen") (v "0.0.3") (d (list (d (n "ipnetwork") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "060s34j9rnmjnlhz4sfvybif6z5693hmn69zb2xi0rg8hgvw5iby")))

(define-public crate-ipgen-0.0.4 (c (n "ipgen") (v "0.0.4") (d (list (d (n "ipnetwork") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "05r0rh0ypyiw3h59w5qfdk1k1qag2h9hxv2pc59hbqqiqpxlvdzn")))

(define-public crate-ipgen-0.0.5 (c (n "ipgen") (v "0.0.5") (d (list (d (n "blake2") (r "^0.9.1") (k 0)) (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)))) (h "0qq81p9a80v3ibmzqha8zrqp4yqp7b1z1amy2li2lgs0x4h4682r")))

(define-public crate-ipgen-1.0.0 (c (n "ipgen") (v "1.0.0") (d (list (d (n "blake2") (r "^0.9.1") (k 0)) (d (n "ipnetwork") (r "^0.17.0") (k 0)))) (h "0aw769c3mkl9bmsz1mxap073n09a8zwxl25rqgjzwh4y9fal0kpd")))

(define-public crate-ipgen-1.0.1 (c (n "ipgen") (v "1.0.1") (d (list (d (n "blake2") (r "^0.9.1") (k 0)) (d (n "ipnetwork") (r "^0.17.0") (k 0)))) (h "18vk83pnfzy1qyxb9mlj5hyv03pxh2sm8fi5pvqsfg5q2kyzc4rq")))

(define-public crate-ipgen-1.0.2 (c (n "ipgen") (v "1.0.2") (d (list (d (n "blake2") (r "^0.10.4") (k 0)) (d (n "ipnetwork") (r "^0.20.0") (k 0)))) (h "1lw7y4p9zpfc0lc016w9f0v71dxrigx9n4c7m05i01vk5l7gkvrg")))

