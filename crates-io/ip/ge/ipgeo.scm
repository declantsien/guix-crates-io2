(define-module (crates-io ip ge ipgeo) #:use-module (crates-io))

(define-public crate-ipgeo-0.1.0 (c (n "ipgeo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0s8h124j5m0rfmxp77694vxgipir5g68mv6n9gx01kgw165i93c6")))

(define-public crate-ipgeo-0.1.1 (c (n "ipgeo") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1a87kfv90fjs41x7kyhrl5vjhscs53174fg6h8zs4dx9y1ps818n")))

(define-public crate-ipgeo-0.1.2 (c (n "ipgeo") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0hf5y4m8ykmbikmma82pgxl1izfning5b3r0qmn7xprza0iz9nmm")))

(define-public crate-ipgeo-0.1.3 (c (n "ipgeo") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "14v68csr037xyhrb9anbiijwq7sbqhgxqf9jmnbyvfd0w8ida25m")))

(define-public crate-ipgeo-0.1.4 (c (n "ipgeo") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "05b5dcabpswxxfcdrsv0rh2hd70w9gbq4gyxq126qpvyi812w3xs")))

(define-public crate-ipgeo-0.1.6 (c (n "ipgeo") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0kqxa28kpczlyamh63yjaybwmmdk4s44hiwc2mf032wjldlmckh9")))

(define-public crate-ipgeo-0.1.7 (c (n "ipgeo") (v "0.1.7") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "1ph4m8q84w4w6zwcxyb2zvyz5fsc0fv4y218w6wq7xxxkc4x0rnk")))

(define-public crate-ipgeo-0.1.8 (c (n "ipgeo") (v "0.1.8") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)) (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)))) (h "1fjm55lihg74mq5mrblw3q2n44ds6sjlhyxfwsbg7z8gwi5s02as")))

