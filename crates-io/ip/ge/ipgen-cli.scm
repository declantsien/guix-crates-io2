(define-module (crates-io ip ge ipgen-cli) #:use-module (crates-io))

(define-public crate-ipgen-cli-0.0.1 (c (n "ipgen-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "ipgen") (r "^0.0.2") (d #t) (k 0)))) (h "1fb6kicwi5hccix18j7ai8yz024s81cmi0fm58h8j377v8fgvjq1")))

(define-public crate-ipgen-cli-0.0.2 (c (n "ipgen-cli") (v "0.0.2") (d (list (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "ipgen") (r "^0.0.4") (d #t) (k 0)))) (h "0ni4kypvdaj27ahr3wmgc5rzs2a738asr1m4isjmlgiq32ghkwgi")))

(define-public crate-ipgen-cli-0.0.3 (c (n "ipgen-cli") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ipgen") (r "^0.0.5") (d #t) (k 0)))) (h "1bap7j6aag44s58kjnz61mxam7gnqzjdnrx8ks5bcp7l35l2w06d")))

(define-public crate-ipgen-cli-1.0.0 (c (n "ipgen-cli") (v "1.0.0") (d (list (d (n "ipgen") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "09vzsq3n1r7x0a9syp4jykah1z3hn4a3sla702mvxkb7mx6in3lr")))

(define-public crate-ipgen-cli-1.0.1 (c (n "ipgen-cli") (v "1.0.1") (d (list (d (n "ipgen") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0h152slfk7f9mpn4l057z886v12yq9j6gh859r9jply5dg7vz6b7")))

(define-public crate-ipgen-cli-1.0.2 (c (n "ipgen-cli") (v "1.0.2") (d (list (d (n "ipgen") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1n37rwclwinzb3p9dmlfx23wxqq0y51csnw22dba942bkcs9ijjs")))

(define-public crate-ipgen-cli-1.0.3 (c (n "ipgen-cli") (v "1.0.3") (d (list (d (n "ipgen") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0a202g0bnpkkcqbifwynddm39xr49xjdrz6pwixzmb0ac4plw68r")))

