(define-module (crates-io ip ge ipgeolocate) #:use-module (crates-io))

(define-public crate-ipgeolocate-0.2.2 (c (n "ipgeolocate") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "145anxxb4w3x6by1yxd8l4d2qgvpiqmb276n4vwc9p6xq01x3sk6")))

(define-public crate-ipgeolocate-0.2.3 (c (n "ipgeolocate") (v "0.2.3") (d (list (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "ureq") (r ">=1.5.1, <2.0.0") (d #t) (k 0)))) (h "1565qpw207vgzynh8vv6ig3lankxmihkbfjfdys2q5ww6yg64cmp")))

(define-public crate-ipgeolocate-0.2.4 (c (n "ipgeolocate") (v "0.2.4") (d (list (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "ureq") (r ">=1.5.1, <2.0.0") (d #t) (k 0)))) (h "1sqmy6kb6dzzib3qxnl8hf34vyqgqr6phrx3wgsinqqa9h3wxvvr")))

(define-public crate-ipgeolocate-0.2.5 (c (n "ipgeolocate") (v "0.2.5") (d (list (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "ureq") (r ">=1.5.1, <2.0.0") (d #t) (k 0)))) (h "1prhqv1nds5h6q7hivdznz251vs941qd1xqa3z5ffn137n7jlari")))

(define-public crate-ipgeolocate-0.2.6 (c (n "ipgeolocate") (v "0.2.6") (d (list (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "ureq") (r ">=1.5.1, <2.0.0") (d #t) (k 0)))) (h "01902lw4z6f2a5c01hpxp414s0zxkalqkcnr5hgl22ach9sdvcfl")))

(define-public crate-ipgeolocate-0.2.7 (c (n "ipgeolocate") (v "0.2.7") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "10k57zs5myqm7s20i2620p8czpv2kdib0kz67rwhpka3x88qxb9x")))

(define-public crate-ipgeolocate-0.2.8 (c (n "ipgeolocate") (v "0.2.8") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1pygwbz5s6xn9gd494lqj6niigr99dlnfd73a6jfwvgjr28msphi")))

(define-public crate-ipgeolocate-0.2.9 (c (n "ipgeolocate") (v "0.2.9") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "08llnk1dh5qqi5ywymmxvxn1xi7mjy0051gsrm52jd8prk8q13il")))

(define-public crate-ipgeolocate-0.3.0 (c (n "ipgeolocate") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "046hqy6wa705pj435z7s5vpi4k023jkkjlcvp0r4qdj96rk6w1nd")))

(define-public crate-ipgeolocate-0.3.1 (c (n "ipgeolocate") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0bw4m1nmlw967yfvq4jpc9jb2l4lid71crknjncv9mx3cqppzrfd")))

(define-public crate-ipgeolocate-0.3.2 (c (n "ipgeolocate") (v "0.3.2") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0crws940lpqb2vm75zyad25spwg3pqdii5gly9pcpah6jvgp2gw7")))

(define-public crate-ipgeolocate-0.3.3 (c (n "ipgeolocate") (v "0.3.3") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1s1dqbdwcavpx4h9lxdgmkga9v0xhsnz5sybrm0agb73i0y9rbmi")))

(define-public crate-ipgeolocate-0.3.4 (c (n "ipgeolocate") (v "0.3.4") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0vmgmz8vnnmqgd6yid544bx2xm24g1xacsaq0zcnqpyw03h12qxd")))

(define-public crate-ipgeolocate-0.3.5 (c (n "ipgeolocate") (v "0.3.5") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hpzqmmzr8zrl5flx8ckggzbg0nfslysqj4bhm7c9pcspsva6gyd")))

(define-public crate-ipgeolocate-0.3.6 (c (n "ipgeolocate") (v "0.3.6") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1k6nlrnrwn71m98hs7dv23wifz0pql0i3ragmz16amq922kh5vq3")))

