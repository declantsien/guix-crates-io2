(define-module (crates-io ip _e ip_extractor) #:use-module (crates-io))

(define-public crate-ip_extractor-0.1.0 (c (n "ip_extractor") (v "0.1.0") (h "1vc6nw226ziafc1m3yg7nrx9llanbb6ghia41w79zkhkjj2i17lm")))

(define-public crate-ip_extractor-0.1.1 (c (n "ip_extractor") (v "0.1.1") (h "0hphp46wfzlrxxggbqlp6mcd7rh3a9i9z2fm03c7ppkprm9hy91y")))

