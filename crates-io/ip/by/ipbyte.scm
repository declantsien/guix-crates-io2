(define-module (crates-io ip by ipbyte) #:use-module (crates-io))

(define-public crate-ipbyte-0.1.0 (c (n "ipbyte") (v "0.1.0") (h "0n8254idz90h8zbhqz63ra9ngx4dbqlh4pxl554dya7riwcysakq")))

(define-public crate-ipbyte-0.1.1 (c (n "ipbyte") (v "0.1.1") (h "1d4ylb32rimxwlz329g542l5asp1zlkaqs80122i7kxvwi83lhac")))

