(define-module (crates-io ip to iptools) #:use-module (crates-io))

(define-public crate-iptools-0.1.0 (c (n "iptools") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13zqsj1a0ddjx6zjj1k1gn6bdk939xn7if82hyf3qh7kldgzyjy4")))

(define-public crate-iptools-0.1.1 (c (n "iptools") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04bkxxq2y9bfngpz45xwf5klqyr1pzvjvz06ydp97lxcn2gpcd66")))

(define-public crate-iptools-0.1.2 (c (n "iptools") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wmj4w80bajrfw8xmm7l7rb76yf49h60pfqzqppqp559q54w99j6")))

(define-public crate-iptools-0.1.3 (c (n "iptools") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1cmd54b0vlfcv8z25l85lv7anfcmsgs7rdqglbal38aq715kxism")))

(define-public crate-iptools-0.1.4 (c (n "iptools") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1mq3hwkmqspd5k55w7jqymic14i7nav7j20k7n1zcvwwycfa2ql9")))

(define-public crate-iptools-0.1.5 (c (n "iptools") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1lpvm854mxg1mbfv4w3s7vgzspp2jmp610nps37pljp19mirk4ka")))

(define-public crate-iptools-0.2.0 (c (n "iptools") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wp5fwwyjsbdyz5nf4yyri8z92i299iq9nqqxnzh21kmxy0jrvdd")))

(define-public crate-iptools-0.2.2 (c (n "iptools") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1g4w103lpvhnfsgq99bzdlvri89is7ccb92cwsin9xyx7zmihvf8")))

(define-public crate-iptools-0.2.3 (c (n "iptools") (v "0.2.3") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1c8024gm8nvhddn5vzvy1n840jp2fxgqckxahvgyl2z09qk76f25")))

(define-public crate-iptools-0.2.4 (c (n "iptools") (v "0.2.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "19b4l43bl35kyyf93dpkql72jg907gb56ik43ca6mkkr123znfy0")))

(define-public crate-iptools-0.2.5 (c (n "iptools") (v "0.2.5") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1xqds285c6n81zd18ckid6zwvz1866pifxl84bh400m1l9dw05n4")))

