(define-module (crates-io ip c_ ipc_channel_adapter) #:use-module (crates-io))

(define-public crate-ipc_channel_adapter-0.1.0 (c (n "ipc_channel_adapter") (v "0.1.0") (d (list (d (n "ipc-channel") (r "^0.18.0") (f (quote ("memfd"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1cmb0yahs7b5ddj8wz09qy2j90lh2j2q2an4xym1nxqk69pn2fpw") (f (quote (("async" "tokio"))))))

(define-public crate-ipc_channel_adapter-0.1.1 (c (n "ipc_channel_adapter") (v "0.1.1") (d (list (d (n "ipc-channel") (r "^0.18.0") (f (quote ("memfd"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0j2djjwr81ls7xzj00dys39mp7n9xn2sybxa9p9f3dph2zkdldrm") (f (quote (("async" "tokio"))))))

(define-public crate-ipc_channel_adapter-0.1.2 (c (n "ipc_channel_adapter") (v "0.1.2") (d (list (d (n "ipc-channel") (r "^0.18.0") (f (quote ("memfd"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "07j6nb17bywb3d2pmbxf1wxvzpkm2bi93zgx6ww8pdi274mbb600") (f (quote (("async" "tokio"))))))

