(define-module (crates-io ip c- ipc-rs) #:use-module (crates-io))

(define-public crate-ipc-rs-0.4.2 (c (n "ipc-rs") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)))) (h "1nmgmh49d6n7bnni9zrjv5dgiazxn3hqsk923gj3ffi2pz17gv8w")))

