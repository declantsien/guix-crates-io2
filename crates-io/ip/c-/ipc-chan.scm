(define-module (crates-io ip c- ipc-chan) #:use-module (crates-io))

(define-public crate-ipc-chan-0.3.0 (c (n "ipc-chan") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0zz514mhakjb5h4imir7vwkn43i571w1fn3sq4hck2m4km799k72")))

(define-public crate-ipc-chan-0.4.0 (c (n "ipc-chan") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0f21vgr1vbza28xb4p4nikz4c4yi8pr8pvhxzgi0ci6lnxzpxv87")))

(define-public crate-ipc-chan-0.6.0 (c (n "ipc-chan") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1q0wlw5g6ax2dlqhvxxdjkbywfqnxbhlp2l2nzy4ybc2b8arfjvb")))

(define-public crate-ipc-chan-0.7.0 (c (n "ipc-chan") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1r9240yidkzvcnz4vkrh91dlfyjslza2n6b5l4ym58n1xvzmak2y")))

(define-public crate-ipc-chan-0.8.0 (c (n "ipc-chan") (v "0.8.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0sbddy6l40j36smpjvli80bjmidp8yn46wcdrpva84j7aiq4hifs")))

