(define-module (crates-io ip ld ipld-extract-links) #:use-module (crates-io))

(define-public crate-ipld-extract-links-0.1.0 (c (n "ipld-extract-links") (v "0.1.0") (d (list (d (n "cid") (r "^0.11.0") (f (quote ("serde-codec"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_ipld_dagjson") (r "^0.1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "11xp0s5pgc8pz4b0gf5qpg0im1jivisxybgip5cpf3jgmbz7k8a2") (y #t)))

