(define-module (crates-io ip ld ipld-dagpb) #:use-module (crates-io))

(define-public crate-ipld-dagpb-0.1.0 (c (n "ipld-dagpb") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "cid") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0n507mcg68bgwkkb1fv6hs48fvp63sk8qjdh0lx0myksk0ik7a0y")))

(define-public crate-ipld-dagpb-0.1.1 (c (n "ipld-dagpb") (v "0.1.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1gcvpw12h8d1ix8yraq2m4x265v6q47dg46grwfi6b9pg2igifrh")))

(define-public crate-ipld-dagpb-0.1.2 (c (n "ipld-dagpb") (v "0.1.2") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xq7zv3bbwjk163432b8gqklhvm4r0qyc5hxwqp1prv82jbv5r6y")))

(define-public crate-ipld-dagpb-0.2.0 (c (n "ipld-dagpb") (v "0.2.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ipld-core") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0kwr12bvb79vk786g9gdylcr7v7ggz47gv6j8jsjcg5d8pbg02jh")))

(define-public crate-ipld-dagpb-0.2.1 (c (n "ipld-dagpb") (v "0.2.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ipld-core") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ixwk8kjy4yvjrawhhim9c1yplhnl6pmm00szqpsd3shy443rsy5")))

