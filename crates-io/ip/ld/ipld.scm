(define-module (crates-io ip ld ipld) #:use-module (crates-io))

(define-public crate-ipld-0.0.1 (c (n "ipld") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cid") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "multibase") (r "^0.7") (d #t) (k 0)) (d (n "multihash") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1m4dw74cgxp6z88pqsl4s979y7snvkvj1gl2w6yq7sqdf15hri85") (f (quote (("default"))))))

(define-public crate-ipld-0.0.2 (c (n "ipld") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cid") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "multibase") (r "^0.7") (d #t) (k 0)) (d (n "multihash") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "196kz5qhn6y9gv4hzk031dhrjcl9nw80qm3cpkrbbjykbk50sxiz") (f (quote (("default"))))))

