(define-module (crates-io ip ld ipld_blockstore) #:use-module (crates-io))

(define-public crate-ipld_blockstore-0.1.1 (c (n "ipld_blockstore") (v "0.1.1") (d (list (d (n "cid") (r "^0.3") (f (quote ("cbor"))) (d #t) (k 0) (p "forest_cid")) (d (n "db") (r "^0.1") (d #t) (k 0) (p "forest_db")) (d (n "encoding") (r "^0.2") (d #t) (k 0) (p "forest_encoding")) (d (n "forest_ipld") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06y1zj58k8b2jr2hlsb41vgkp2zvsw9k8w2zigrhwjgchbn9f0mh") (f (quote (("tracking") ("sled" "db/sled") ("rocksdb" "db/rocksdb") ("resolve" "forest_ipld") ("buffered" "forest_ipld"))))))

