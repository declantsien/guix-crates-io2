(define-module (crates-io ip ld ipldfs) #:use-module (crates-io))

(define-public crate-ipldfs-0.1.0 (c (n "ipldfs") (v "0.1.0") (d (list (d (n "libipld") (r "^0.16") (f (quote ("serde-codec"))) (d #t) (k 0)) (d (n "lru-mem") (r "^0.2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bx8rrg2c4h4cnyr63i8f1c2f30nxyhksdrgb3bsg8lg4n61kzdb")))

