(define-module (crates-io ip ra iprange) #:use-module (crates-io))

(define-public crate-iprange-0.1.0 (c (n "iprange") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "1jylc2nkr5pzc93j4nknf7lbmkpk3f7l4rsbdrnifss7nhvk4pp2")))

(define-public crate-iprange-0.2.0 (c (n "iprange") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "14hwb2gh8srga10appv15046l5jdwm0kmfkbp4fibkxkgrvkhi2p")))

(define-public crate-iprange-0.2.1 (c (n "iprange") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "0j1m94k25yf3bakv5fmxf3z8zwm69w02kb591xpcziaw6rfk79ag")))

(define-public crate-iprange-0.2.2 (c (n "iprange") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "1856xd78mv37djfcdqy336z958q7xylmza8j4q1jy21m1pv18z13")))

(define-public crate-iprange-0.3.0 (c (n "iprange") (v "0.3.0") (d (list (d (n "ipnet") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "155swmsab7rbm711js8wxmj7jwb4mgzb8zw4wcwmigk63pns8qc0")))

(define-public crate-iprange-0.4.0 (c (n "iprange") (v "0.4.0") (d (list (d (n "ipnet") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "14l3w7gqcgzbd75inzck3zi3rq5wib879z3sd757pj8p3bh8zjpm")))

(define-public crate-iprange-0.4.1 (c (n "iprange") (v "0.4.1") (d (list (d (n "ipnet") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "1bk70nk167kzml2r447w8g41bw46ybsl80p00rs1cjvy169qhy45")))

(define-public crate-iprange-0.5.0 (c (n "iprange") (v "0.5.0") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0ap9bxvqfbsdn9wzsc5g50n6xjliax5m30yisfvvrvkr9vnmjcfl") (y #t)))

(define-public crate-iprange-0.5.1 (c (n "iprange") (v "0.5.1") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0fwxqxrpzqvv9km45rg0gp0dxs526bwxl0jaizk8vilmc9lipyrk")))

(define-public crate-iprange-0.6.0 (c (n "iprange") (v "0.6.0") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0kn7ndxdifrcvqs4brs0v4qcigr0w5a9vgkgnmf582hv4n9zxnig")))

(define-public crate-iprange-0.6.1 (c (n "iprange") (v "0.6.1") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "1vlh3nzv491d6a3719ci5ywg1p9bkva9j6f416sj84chh392h21y")))

(define-public crate-iprange-0.6.2 (c (n "iprange") (v "0.6.2") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "1fks910s80z11yacwbr6mwk8l5vg9iwqgs3wws8gsjp4nsmy76f7")))

(define-public crate-iprange-0.6.3 (c (n "iprange") (v "0.6.3") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "1060d148azad3gcw8vn5rxziyp0gaggxi4gcd36xlr8xgk01cwgr")))

(define-public crate-iprange-0.6.4 (c (n "iprange") (v "0.6.4") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0my0ykx9njrbb32wk141pi5sadnc3mg2cyczaq517cg7jyy1kbka")))

(define-public crate-iprange-0.6.5 (c (n "iprange") (v "0.6.5") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "13nf71lpg79pajh8lkiz7172dpcvlvqidj8bm6d4pfyqj5vjn64c")))

(define-public crate-iprange-0.6.6 (c (n "iprange") (v "0.6.6") (d (list (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0zzybr6jxqmp7zh5snpgkr69y3vhvfgc0aa1a89mis0417a3fp63")))

(define-public crate-iprange-0.6.7 (c (n "iprange") (v "0.6.7") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "ipnet") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "007cljpvhpwg6cvvdycpa8x4bqj8wwai8h0l73k5fm12mph9n81p")))

