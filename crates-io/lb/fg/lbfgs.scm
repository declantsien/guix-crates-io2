(define-module (crates-io lb fg lbfgs) #:use-module (crates-io))

(define-public crate-lbfgs-0.1.1 (c (n "lbfgs") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "unit_test_utils") (r "^0.1.3") (d #t) (k 2)))) (h "0g0cpssv7ns9i6gj95hrban7wqj9x93vl5nhppwv1z3y8s4a3ssa")))

(define-public crate-lbfgs-0.2.0 (c (n "lbfgs") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "unit_test_utils") (r "^0.1.3") (d #t) (k 2)))) (h "18iqbal080dw7i4qnmdpk6l77hvzbwf225d1s5xsd0c52z8xqyjj")))

(define-public crate-lbfgs-0.2.1 (c (n "lbfgs") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "unit_test_utils") (r "^0.1.3") (d #t) (k 2)))) (h "1x0904g114g6i2s3lrjch7pl5nmq7jap8d34vbxw7lskapv9dp8b")))

