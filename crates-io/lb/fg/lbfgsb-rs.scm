(define-module (crates-io lb fg lbfgsb-rs) #:use-module (crates-io))

(define-public crate-lbfgsb-rs-0.1.0 (c (n "lbfgsb-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "vecfx") (r "^0.1") (d #t) (k 2)))) (h "0cqyc2yx3z43n9lykvkis7c909da160c0ddkxsrcvlm8ll2cn4dc") (y #t)))

