(define-module (crates-io lb fg lbfgsb) #:use-module (crates-io))

(define-public crate-lbfgsb-0.1.0 (c (n "lbfgsb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "vecfx") (r "^0.1") (d #t) (k 2)))) (h "18nh5dcfn68wqv91g67grph8jwraipqfp5cnx8v6hkix24lzq58a")))

