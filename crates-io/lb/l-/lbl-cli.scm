(define-module (crates-io lb l- lbl-cli) #:use-module (crates-io))

(define-public crate-lbl-cli-0.1.0 (c (n "lbl-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive" "color" "unstable-styles"))) (d #t) (k 0)) (d (n "inquire") (r "^0.7.1") (d #t) (k 0)) (d (n "lbl-core") (r "^0.1.0") (d #t) (k 0)) (d (n "polars") (r "^0.38.2") (f (quote ("csv" "polars-io" "parquet" "lazy" "polars-ops" "binary_encoding" "string_encoding" "strings" "regex" "partition_by" "is_in"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toolstr") (r "^0.1.3") (d #t) (k 0)))) (h "1m3vhrxzhwz9zl4z2s9h4aw2p0mnjnw1v9izddpxl3hirg9c8mk1")))

