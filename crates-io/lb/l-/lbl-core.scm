(define-module (crates-io lb l- lbl-core) #:use-module (crates-io))

(define-public crate-lbl-core-0.1.0 (c (n "lbl-core") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "polars") (r "^0.38.2") (f (quote ("csv" "polars-io" "parquet" "lazy" "polars-ops" "binary_encoding" "string_encoding" "strings" "regex" "partition_by" "is_in"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "17dgsb5s65p7cal1bl317r9cpmjqzy0ph0c3vszb02g56gmmiv4w")))

