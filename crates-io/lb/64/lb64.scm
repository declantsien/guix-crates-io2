(define-module (crates-io lb #{64}# lb64) #:use-module (crates-io))

(define-public crate-lb64-0.1.0 (c (n "lb64") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "06qbxi5vmd6nch7ch6kx6wswhx5w2gmjc6wfdzwjmbpybizlmz1r")))

(define-public crate-lb64-0.1.1 (c (n "lb64") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "07yl1cmm1052f8ynkq0gpkvz17nhqiybkl29izs9pxqwl5qwy6z2")))

(define-public crate-lb64-0.1.2 (c (n "lb64") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0mbmkph4qc3753yp2jw98kgmj1xfwab9b4za8p3q32jaqx64spqn")))

