(define-module (crates-io lb er lber) #:use-module (crates-io))

(define-public crate-lber-0.1.2 (c (n "lber") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0yrk8nbgxsvg5yn75mcq3l8vrgfx9ma726psfhj2n1isnwchkx5h")))

(define-public crate-lber-0.1.3 (c (n "lber") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0cnzvhx6d34q3i5pcm0vksc9mxipww4gh96haziaiv3a5cg8nrv2")))

(define-public crate-lber-0.1.4 (c (n "lber") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "1vkbgfcc0c5vnbgh2j5qa9scpr5pxzw61bynh0vvbxaz49h1yr4s")))

(define-public crate-lber-0.1.5 (c (n "lber") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0pldihkdzwgi7wmmjiwasryqvkxvfymb5dcw09hdlkv00fg3vfmj")))

(define-public crate-lber-0.1.6 (c (n "lber") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0dhv2i5r2zzb74krn1522qnk57hqb9rqbl03nn9laxr6vlsjl5xa")))

(define-public crate-lber-0.1.7 (c (n "lber") (v "0.1.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "00l50r29c69wncbhmjmm8nmm0xcagbfdd2n6fkra6s10z0bphssy") (y #t)))

(define-public crate-lber-0.2.0 (c (n "lber") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "19bqhaysk4q3hd7jyqhdb7h56l06j7r6q35ah51qvyzw8d6rajd7")))

(define-public crate-lber-0.3.0 (c (n "lber") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "0c69hs628kdjps6d9wjcr8cdqwv5fbs3qr1jmgxac89vk4hbb6ca")))

(define-public crate-lber-0.4.0 (c (n "lber") (v "0.4.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0ih4kr9qmqwhm78492v09kmlcrgzvbqc3cf3f06bab7101g5zn5m")))

(define-public crate-lber-0.4.2 (c (n "lber") (v "0.4.2") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "02pghdykbsffimswayixinrsaxfmwwzpb854w5cqzkv4kzyzkxrd")))

