(define-module (crates-io lb -f lb-fonts) #:use-module (crates-io))

(define-public crate-lb-fonts-0.1.0 (c (n "lb-fonts") (v "0.1.0") (h "02jvannmsj86wgp3c32588l38gpiwra2sb4gssdyhfjsz0z765q1")))

(define-public crate-lb-fonts-0.1.1 (c (n "lb-fonts") (v "0.1.1") (h "07h04mi5imyajscvbi2mp0xzbxr8yjdvs9nnq6ck3r9ajxiffsxz")))

(define-public crate-lb-fonts-0.1.2 (c (n "lb-fonts") (v "0.1.2") (h "1cia9szyybdgmnwcain8bb0is3zmxbhzn9lah42v67r6c3746pk1")))

