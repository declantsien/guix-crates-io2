(define-module (crates-io lb r- lbr-prelude-derive) #:use-module (crates-io))

(define-public crate-lbr-prelude-derive-0.1.0 (c (n "lbr-prelude-derive") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 0)))) (h "0mvrmgnv5yi6icyli4v3hvxmaksx5c7m93kyd5wnhvba453529nk")))

