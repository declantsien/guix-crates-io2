(define-module (crates-io lb r- lbr-prelude) #:use-module (crates-io))

(define-public crate-lbr-prelude-0.1.0 (c (n "lbr-prelude") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "lbr-prelude-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("arbitrary_precision"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ifc25kjb7cn70j96iryx9pqw5wjj1alqzwas42k6nky2yqk9cvh") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:lbr-prelude-derive"))))))

