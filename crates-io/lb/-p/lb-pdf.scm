(define-module (crates-io lb -p lb-pdf) #:use-module (crates-io))

(define-public crate-lb-pdf-0.1.0 (c (n "lb-pdf") (v "0.1.0") (d (list (d (n "pdfium-render") (r "^0.8.11") (d #t) (k 0)))) (h "182wdz59x5z34hizz7mx81149616dqra3x1rli4rm6d8h05ykcr8")))

(define-public crate-lb-pdf-0.1.1 (c (n "lb-pdf") (v "0.1.1") (d (list (d (n "pdfium-render") (r "^0.8.11") (d #t) (k 0)))) (h "1i8y7kql42msfi5cd7km5mhdjvpahicc6vhpz3vx1j5k3px9n5il")))

