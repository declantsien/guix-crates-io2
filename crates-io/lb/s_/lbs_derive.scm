(define-module (crates-io lb s_ lbs_derive) #:use-module (crates-io))

(define-public crate-lbs_derive-0.1.0 (c (n "lbs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09r2l96vxwv2yhqprzhqgbamc761pl9yzrv56pz5v9kzmabvi19x")))

(define-public crate-lbs_derive-0.2.0 (c (n "lbs_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hh4amnlv6nvilg3iaz78ggayrixn4knp49yfq0pldy0pbbj0d2f")))

(define-public crate-lbs_derive-0.3.1 (c (n "lbs_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qpg3x4ida00iz765bszkwl6nm84ws034w1y01481h6zds9j8c39")))

(define-public crate-lbs_derive-0.4.0 (c (n "lbs_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ii9yf85kqyq6zzfirdvimw9ljzky62nhvglpaq1dqhvh971f19k")))

(define-public crate-lbs_derive-0.4.1 (c (n "lbs_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "119p3x2jrdnr5n8hhizzmbjbnympsc5dai2aj7c2xrpwazqfwc8p")))

