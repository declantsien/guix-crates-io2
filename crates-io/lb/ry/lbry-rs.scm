(define-module (crates-io lb ry lbry-rs) #:use-module (crates-io))

(define-public crate-lbry-rs-0.0.1 (c (n "lbry-rs") (v "0.0.1") (d (list (d (n "jsonrpc-client-core") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-client-http") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "071l7859hzvgmbbibgc9la7g7bqm30bjwl9xfws8bivk7l9amq4i")))

