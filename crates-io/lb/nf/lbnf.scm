(define-module (crates-io lb nf lbnf) #:use-module (crates-io))

(define-public crate-lbnf-0.1.0 (c (n "lbnf") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "026wn17r0ld7gwnkdarm4awnjx11q28krr0dvvpaaivfkglsx670")))

