(define-module (crates-io ca lp calpro) #:use-module (crates-io))

(define-public crate-calpro-0.1.0 (c (n "calpro") (v "0.1.0") (h "03pci4jkkl08kis8njia53kzlc4hc027qsnihjcagzdwdr6izh0z")))

(define-public crate-calpro-0.1.2 (c (n "calpro") (v "0.1.2") (h "1m02k8d8k7s674iw7ddq7cns07nv98x0xz2rrbgmmbhaymv1m9xl")))

(define-public crate-calpro-0.1.3 (c (n "calpro") (v "0.1.3") (h "1k3ivdjmzhqma5g4igvgcaij1in5nn96bvx8df5dmw2gbbglvx3s")))

(define-public crate-calpro-0.1.4 (c (n "calpro") (v "0.1.4") (h "0iq2k60pkzqp53bgcnyqvgixqic371l4y00y6af1hpm3l548hi2d")))

(define-public crate-calpro-0.1.5 (c (n "calpro") (v "0.1.5") (h "0mqczhh2zljj0bkf164xx528mv6v5741sg0x1jpwc9q76mr73wpf")))

(define-public crate-calpro-0.1.6 (c (n "calpro") (v "0.1.6") (h "06p64syi200ygr9cffpysbclbrfp23lfkjzngal8i7xrpm1wxgjj")))

(define-public crate-calpro-0.1.7 (c (n "calpro") (v "0.1.7") (h "1lf90jj46mfqsrsylpvfxz0c36kfir5g9jsryj2f80c1vyj4kq9q")))

