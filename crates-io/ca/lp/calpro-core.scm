(define-module (crates-io ca lp calpro-core) #:use-module (crates-io))

(define-public crate-calpro-core-0.1.0 (c (n "calpro-core") (v "0.1.0") (h "0jxy14bgdmmr5jkrc9yiwa61p0s75vr8297lxdwh4yf523mwqx01")))

(define-public crate-calpro-core-0.1.1 (c (n "calpro-core") (v "0.1.1") (h "172d1djch0mdv2hfn9lf98q6rfxajnplfj13jray6whidvr4frrx")))

