(define-module (crates-io ca ca caca-sys) #:use-module (crates-io))

(define-public crate-caca-sys-0.1.0 (c (n "caca-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1hk3bpnq4vnsg0jd7pykpfy5jsws3jafi1j0ljz4aqzcji8ld451") (f (quote (("enable-x11") ("enable-win32") ("enable-slang") ("enable-network") ("enable-ncurses") ("enable-gl") ("enable-conio"))))))

