(define-module (crates-io ca ca caca) #:use-module (crates-io))

(define-public crate-caca-0.1.0 (c (n "caca") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "caca-sys") (r "^0.1.0") (f (quote ("enable-x11" "enable-ncurses"))) (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "181wnyfyclxip4c08q8jqxqyf1zzjy122crq9cqn0lw7cd9kwk1a")))

