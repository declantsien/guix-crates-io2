(define-module (crates-io ca rm carmen) #:use-module (crates-io))

(define-public crate-carmen-0.1.0 (c (n "carmen") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "11nj4ii0y0axp0dav0kdxrmji9ykplvwdgb7icnasm4dk9qm3x3c")))

