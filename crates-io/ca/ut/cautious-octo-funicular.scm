(define-module (crates-io ca ut cautious-octo-funicular) #:use-module (crates-io))

(define-public crate-cautious-octo-funicular-0.1.0 (c (n "cautious-octo-funicular") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.2.3") (d #t) (k 1)))) (h "1gv1x3nii7q5vqq5ip134xagpzd9gwarsqkmfvhzr28p1rz1mv2l")))

(define-public crate-cautious-octo-funicular-0.1.1 (c (n "cautious-octo-funicular") (v "0.1.1") (d (list (d (n "mdbook") (r "^0.2.3") (d #t) (k 1)))) (h "077l8nbqbvvrrgz0xd4gbv4dk352i9wqfmiygil7wmx1ciirwgr3")))

(define-public crate-cautious-octo-funicular-0.1.2 (c (n "cautious-octo-funicular") (v "0.1.2") (d (list (d (n "mdbook") (r "^0.2.3") (d #t) (k 1)))) (h "1wgcl0fs8xsfskc4xv7w80n1jpj4d0vrsjdsj8ni321w7nrfzni7")))

(define-public crate-cautious-octo-funicular-0.1.3 (c (n "cautious-octo-funicular") (v "0.1.3") (d (list (d (n "mdbook") (r "^0.2.3") (d #t) (k 1)))) (h "09kswc8yp666s962vafvb4ymnakhc8qv5ghwl5hmzpil5l6y1b5h")))

(define-public crate-cautious-octo-funicular-0.1.4 (c (n "cautious-octo-funicular") (v "0.1.4") (d (list (d (n "mdbook") (r "^0.4.7") (d #t) (k 1)))) (h "0fx35dy1xj0cjwawrzgx3lzf2kw858371ym6gpahy2riwjmffgxs")))

(define-public crate-cautious-octo-funicular-0.1.5 (c (n "cautious-octo-funicular") (v "0.1.5") (d (list (d (n "mdbook") (r "^0.4.7") (d #t) (k 1)))) (h "1nmwkn4ajqk5lwhjrb4fhx9dig1y6n3g521qg2gvq9fac6c2hrg8")))

