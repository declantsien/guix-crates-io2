(define-module (crates-io ca i_ cai_cargo) #:use-module (crates-io))

(define-public crate-cai_cargo-0.1.0 (c (n "cai_cargo") (v "0.1.0") (h "1jya4qmp4m41r3hp7fj7qkimf0k57g6fbh5181yzn9arrvp70pda")))

(define-public crate-cai_cargo-0.1.0-SNAPSHOT (c (n "cai_cargo") (v "0.1.0-SNAPSHOT") (h "0v6dqr450xazhlm8dww1pri8cafhh0mwf7g5xwp957l0lrpl3brc")))

