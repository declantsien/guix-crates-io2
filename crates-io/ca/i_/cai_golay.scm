(define-module (crates-io ca i_ cai_golay) #:use-module (crates-io))

(define-public crate-cai_golay-0.1.0 (c (n "cai_golay") (v "0.1.0") (d (list (d (n "binfield_matrix") (r "^0.1.0") (d #t) (k 0)))) (h "0v26c3rwsyipsy36i9l0abrpv6v375r6mdxprl2164diq342d29c")))

(define-public crate-cai_golay-0.1.1 (c (n "cai_golay") (v "0.1.1") (d (list (d (n "binfield_matrix") (r "^0.2.0") (d #t) (k 0)))) (h "0768n92z17lfaav1aski0q5r0l9ys5crpvcr2m99qg0rlma26rnb")))

