(define-module (crates-io ca i_ cai_cyclic) #:use-module (crates-io))

(define-public crate-cai_cyclic-0.1.0 (c (n "cai_cyclic") (v "0.1.0") (d (list (d (n "binfield_matrix") (r "^0.1.0") (d #t) (k 0)))) (h "1mw8j37qf75jidzwvva3wpcjs92n9vgcd4xxbp4kk34mhb8lk64j")))

(define-public crate-cai_cyclic-0.1.1 (c (n "cai_cyclic") (v "0.1.1") (d (list (d (n "binfield_matrix") (r "^0.1.0") (d #t) (k 0)))) (h "10y3kzxip7i9js9vvsbgw035w2yygr75rds8113mx3sl30ash0bl")))

(define-public crate-cai_cyclic-0.1.2 (c (n "cai_cyclic") (v "0.1.2") (d (list (d (n "binfield_matrix") (r "^0.2.0") (d #t) (k 0)))) (h "17i605x6wcz6r6brdflnynhz5rdqfw04mxx931jxh61y2z36irpa")))

