(define-module (crates-io ca i_ cai_hamming) #:use-module (crates-io))

(define-public crate-cai_hamming-0.1.0 (c (n "cai_hamming") (v "0.1.0") (d (list (d (n "binfield_matrix") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1347kgpqhhmphh0l7yfsbfpxmm98dnfjhd3x1d3c06d3ifk5m6hc")))

