(define-module (crates-io ca co caco3-build-info) #:use-module (crates-io))

(define-public crate-caco3-build-info-0.1.0 (c (n "caco3-build-info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.17") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k5ac6zmgf4l1rx6qn8d0iinpdkc5irz8ckcwhk8lfdli17r7pcx")))

(define-public crate-caco3-build-info-0.1.1 (c (n "caco3-build-info") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.17") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b3pm3a7za2g3k3psig8vqbfc4kxhvxrml74jlzbx3pjq6q2d5rr")))

(define-public crate-caco3-build-info-0.2.0 (c (n "caco3-build-info") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gzcnv1n4r11v7i4gmckimr22gri16imnpniyhqlzd5lp8imrwl0")))

(define-public crate-caco3-build-info-0.2.1 (c (n "caco3-build-info") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0znnvfn045v69c0ijykkj9gk8ms43rzw32n9q8608368f5a1p8wd")))

