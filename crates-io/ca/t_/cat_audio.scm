(define-module (crates-io ca t_ cat_audio) #:use-module (crates-io))

(define-public crate-cat_audio-0.0.0 (c (n "cat_audio") (v "0.0.0") (d (list (d (n "cpal") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "minimp3") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1zb42p3ngpvxw2634vfmi8x65yfbhxzlfyh08djgkjca5pg2k3ww") (f (quote (("raw" "extended") ("extended")))) (y #t)))

(define-public crate-cat_audio-0.0.1 (c (n "cat_audio") (v "0.0.1") (d (list (d (n "cpal") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "minimp3") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1a4xvj209vq3k98n78kqsphhimx38d1g3pqplfifj13lm7ry5sdb") (f (quote (("raw" "extended") ("extended")))) (y #t)))

(define-public crate-cat_audio-0.0.2 (c (n "cat_audio") (v "0.0.2") (d (list (d (n "cpal") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "minimp3") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "0969yfaxjc72ad5myaifj2xlibw6lzxnyc347j0ys63mb9kyc7i5") (f (quote (("raw" "extended") ("extended")))) (y #t)))

(define-public crate-cat_audio-0.0.3 (c (n "cat_audio") (v "0.0.3") (d (list (d (n "cpal") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "minimp3") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1iaqv8ds99qlc67dxzdyif6s58kjgsdkw9yjbwna7da4d9jnvcz6") (f (quote (("raw" "extended") ("extended"))))))

(define-public crate-cat_audio-0.0.4 (c (n "cat_audio") (v "0.0.4") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "minimp3") (r "^0.5.0") (d #t) (k 0)))) (h "07yw1dbgkjygcnxs4hqc9id5pvqwnrrxrid9mfm0issm6bzy0cgk") (f (quote (("raw" "extended") ("extended"))))))

