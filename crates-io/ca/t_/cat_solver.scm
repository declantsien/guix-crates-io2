(define-module (crates-io ca t_ cat_solver) #:use-module (crates-io))

(define-public crate-cat_solver-0.1.0 (c (n "cat_solver") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1bxmby9z7j117gm884z4318h7z5np2aw3gndmqkajv6qvzqkhfkg") (l "kissat")))

(define-public crate-cat_solver-3.1.1 (c (n "cat_solver") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0r20fx140244xx02d8dbaj3ci2bm6hc8hswxc1lya1qf1szkwnx8") (l "kissat")))

