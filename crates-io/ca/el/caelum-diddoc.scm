(define-module (crates-io ca el caelum-diddoc) #:use-module (crates-io))

(define-public crate-caelum-diddoc-0.1.1 (c (n "caelum-diddoc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0x0jp7rdjc1r64dwz23q8wdvbpsy8lii1qgqybrbsr18qp3dl8fi")))

(define-public crate-caelum-diddoc-0.1.3 (c (n "caelum-diddoc") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "149lllwvd7fg96g1f9aaijmk7j98rj9sbpqbr58mg9wf9vl7dbbn")))

