(define-module (crates-io ca el caelum-id) #:use-module (crates-io))

(define-public crate-caelum-id-0.1.0 (c (n "caelum-id") (v "0.1.0") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "1pbigzraxwqvmqpxhs30p4makl6d31sx8shqy8zvw7aw4ayvk96x")))

(define-public crate-caelum-id-0.1.1 (c (n "caelum-id") (v "0.1.1") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "1h590f7glkryvydp5qg9baz797s5n7a1p1102dbdxyhdpbry77pp")))

(define-public crate-caelum-id-0.1.2 (c (n "caelum-id") (v "0.1.2") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "12zbwlc7926rfvhndppj6kx299jhbp7szh2lk5vri8mprd28y1c2")))

(define-public crate-caelum-id-0.1.3 (c (n "caelum-id") (v "0.1.3") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "07hq5kyyp3g74cm3bnsl3gixr06nn97ygrw2xm25mavdfh8mpz5d")))

(define-public crate-caelum-id-0.1.4 (c (n "caelum-id") (v "0.1.4") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "07agb4n8m5dscw1rq478gm52vpm8nlcwsfqq97ibgw02i1sqn786")))

(define-public crate-caelum-id-0.1.5 (c (n "caelum-id") (v "0.1.5") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.5.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "1wlwz6c4qkxk0bx832wqkkyiq59zdh89b2xyw4c31isx9naw02v4")))

(define-public crate-caelum-id-0.1.6 (c (n "caelum-id") (v "0.1.6") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.5.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "15wr7dm06icxjf216aaqv8hfqyn87jp84a137l0b7wwy2awbrizx")))

(define-public crate-caelum-id-0.1.7 (c (n "caelum-id") (v "0.1.7") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "0pyvzzadw4ayhbzpb5sr2bjyb8jq0b473ras8clpkqawjzqxlsv0")))

(define-public crate-caelum-id-0.1.8 (c (n "caelum-id") (v "0.1.8") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "1ibc5x1gz01gilzyvi931nw7li9fd068z5i13i25hmcb748gkapi")))

(define-public crate-caelum-id-0.1.9 (c (n "caelum-id") (v "0.1.9") (d (list (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.5.1") (d #t) (k 0)))) (h "1isph6rlicqx6lz4pxhbbnzdfin7hm384nw452z187h41rxcl46w") (y #t)))

