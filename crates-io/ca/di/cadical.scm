(define-module (crates-io ca di cadical) #:use-module (crates-io))

(define-public crate-cadical-0.1.1 (c (n "cadical") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0gfnn8smnjbv7gpn0qja32ys8yrb5b1b5jfzmw8lh3fc404552k9") (l "ccadical")))

(define-public crate-cadical-0.1.2 (c (n "cadical") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10djwkilk1bfxhf9d7zc1iayd6kl3akcmj5s1md86cdmd613cngf") (l "ccadical")))

(define-public crate-cadical-0.1.3 (c (n "cadical") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0djz8b512rhbvyv9lyyq9nq2hx2yk03lqhbss3ak7yr4ihn3qj6g") (l "ccadical")))

(define-public crate-cadical-0.1.4 (c (n "cadical") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0lbh8lbnjq56c6yjdrfki409684q3gashgmdsa9rdzxb1irj1nvd") (l "ccadical")))

(define-public crate-cadical-0.1.5 (c (n "cadical") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "03n7j0r96b2hb6sdgs2al6xb4141wwwf4vfpv0gxz42wjs56dwg2") (l "ccadical")))

(define-public crate-cadical-0.1.6 (c (n "cadical") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1i2k63hprx9flnvp5l6fphd5fnx5lm7caj4vqjvh5khzrkhx5k53") (l "ccadical")))

(define-public crate-cadical-0.1.7 (c (n "cadical") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0n5is3ln0088pi01755smk6xaym5xcv9zj1b3073yzdkw2s0clw1") (l "ccadical")))

(define-public crate-cadical-0.1.8 (c (n "cadical") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1qnz1zaxpz1kidncvvmkwschkm6fkvbjyambk65h7ssnsig365xa") (l "ccadical")))

(define-public crate-cadical-0.1.9 (c (n "cadical") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "08p7rsnh8cnw8cr7q049a0ssxl7w16nzk0yayi2jrcjcnvsizzw7") (l "ccadical")))

(define-public crate-cadical-0.1.10 (c (n "cadical") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0ivxik17gjzsflabj78r3vjw3pcy2vk7vr1v8kgil3fyhqr8bw9y") (f (quote (("cpp-debug")))) (l "ccadical")))

(define-public crate-cadical-0.1.11 (c (n "cadical") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "13922jwwdwbw8i599hsgwyplb36476y6y53m2j7lahb4l86wp22y") (f (quote (("cpp-debug")))) (l "ccadical")))

(define-public crate-cadical-0.1.12 (c (n "cadical") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1sc7r38jp7k2clvzwg1rvm2p6i7gr1c79fmi4ih1b9qyviiz0mf0") (f (quote (("cpp-debug")))) (l "ccadical")))

(define-public crate-cadical-0.1.13 (c (n "cadical") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1x8gpydxfi4grxghh904a1vlk6kafwspdkyj0l0ylljl0agyv6jh") (f (quote (("cpp-debug")))) (l "ccadical")))

(define-public crate-cadical-0.1.14 (c (n "cadical") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1f2ifimpl2y2rr8npnd31hbb1lrkvplbaj2dwvfggcv8dh2d75jf") (f (quote (("cpp-debug")))) (l "ccadical")))

