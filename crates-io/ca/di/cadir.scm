(define-module (crates-io ca di cadir) #:use-module (crates-io))

(define-public crate-cadir-0.1.0 (c (n "cadir") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vqsnnhzgjhm9nrvpmbbjayga70i9p3wyar7rjy1cfr7irdpdscr")))

(define-public crate-cadir-0.1.1 (c (n "cadir") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1v8araizwwnx4n460q6icxvknbnswjpcj3ff117gdh9z5a326pz3")))

(define-public crate-cadir-0.1.2 (c (n "cadir") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1qqjmf7jar1bvik4hnp5zk80wmsalpjaxg608mx6l0ggj7qj60yl")))

