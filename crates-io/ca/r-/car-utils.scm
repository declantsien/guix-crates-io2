(define-module (crates-io ca r- car-utils) #:use-module (crates-io))

(define-public crate-car-utils-0.1.0 (c (n "car-utils") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (d #t) (k 0)) (d (n "rust-car") (r "^0.1.3") (d #t) (k 0)))) (h "12560p8xla35wbc9sm2zh0dv9bnszwa4g6qcrf6aspkmixwmyy2f")))

(define-public crate-car-utils-0.1.1 (c (n "car-utils") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (d #t) (k 0)) (d (n "rust-car") (r "^0.1.4") (d #t) (k 0)))) (h "1hs23pcahg3i51ivjibii8wgdgi7yjnrh37q30n2qj99ii93bgma")))

(define-public crate-car-utils-0.1.2 (c (n "car-utils") (v "0.1.2") (d (list (d (n "blockless-car") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)))) (h "0f3wrpsm73l24w9ai2bwaw322saz6nj3vwnqwf3rsmykspal47cz")))

(define-public crate-car-utils-0.1.3 (c (n "car-utils") (v "0.1.3") (d (list (d (n "blockless-car") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)))) (h "0b3dspy81w81qb65p7yy5b4jkq8gr3zg184cghn8x5lpwkx3arix")))

(define-public crate-car-utils-0.1.5 (c (n "car-utils") (v "0.1.5") (d (list (d (n "blockless-car") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)))) (h "0xr70r19ih62inasvilkmsgsc6lk8f1mwj8gm22c20mzkqc395nn")))

(define-public crate-car-utils-0.2.0 (c (n "car-utils") (v "0.2.0") (d (list (d (n "blockless-car") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ah2rg06chblflvr5nwm8kiqr9xzdcg4xdlmsdj8gg4q087c51rc")))

(define-public crate-car-utils-0.3.1 (c (n "car-utils") (v "0.3.1") (d (list (d (n "blockless-car") (r "^0.3.1") (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "multicodec") (r "^0.1.0") (d #t) (k 0)))) (h "084sgddpvrk14c249yw7ks9qzspy1vbjsj6qq3r2i9ccimpjqqbh")))

(define-public crate-car-utils-0.3.2 (c (n "car-utils") (v "0.3.2") (d (list (d (n "blockless-car") (r "^0.3.2") (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "multicodec") (r "^0.1.0") (d #t) (k 0)))) (h "1qzngckinzmwb6p311c601l0cg1qjav38b5sag18s0kpmgqx1gig")))

