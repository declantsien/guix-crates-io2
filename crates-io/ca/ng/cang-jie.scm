(define-module (crates-io ca ng cang-jie) #:use-module (crates-io))

(define-public crate-cang-jie-0.1.0 (c (n "cang-jie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tantivy") (r "^0.7") (d #t) (k 0)))) (h "1qk1rmahk8lg62cbgzx6y5mwlcfrzwy49v3k09lvq7maa2z55kdp")))

(define-public crate-cang-jie-0.2.0 (c (n "cang-jie") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.10") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tantivy") (r "^0.8") (d #t) (k 0)))) (h "1lyrass37ffa5rzgbl1pp7klhbr12ak17xydy3z9b723ci1g6lfx")))

(define-public crate-cang-jie-0.3.0 (c (n "cang-jie") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.11") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tantivy") (r "^0.9") (d #t) (k 0)))) (h "1bh77jnmq2m7ppj6q25z7ihv9gz6k8kf4kgn0afh6018i6mzd475")))

(define-public crate-cang-jie-0.4.0 (c (n "cang-jie") (v "0.4.0") (d (list (d (n "flexi_logger") (r "^0.13.2") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.4.7") (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tantivy") (r "^0.9.1") (d #t) (k 0)))) (h "1jh4bjz61hbqksvylpy7k0irm3rfzi246snyl58inqd36d4m56p5")))

(define-public crate-cang-jie-0.5.0 (c (n "cang-jie") (v "0.5.0") (d (list (d (n "flexi_logger") (r "^0.13.3") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.4.8") (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "tantivy") (r "^0.10.0") (d #t) (k 0)))) (h "1vl5wbswalfqjkyjf9zajj2xq0chpspr5g8y3vqnk0124nxvar0g")))

(define-public crate-cang-jie-0.6.0 (c (n "cang-jie") (v "0.6.0") (d (list (d (n "flexi_logger") (r "^0.14.5") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.4.10") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tantivy") (r "^0.11.2") (d #t) (k 0)))) (h "0khhd4n2fscdrbkaancphd9bfa7m2798gjww9p6rq94q53yjs15j")))

(define-public crate-cang-jie-0.7.0 (c (n "cang-jie") (v "0.7.0") (d (list (d (n "flexi_logger") (r "^0.14.8") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.4.10") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tantivy") (r "^0.12.0") (d #t) (k 0)))) (h "0jzvl4qr9l9bflf9vx7n4jfb6v4mn8a1mp8lzrvpkv7c6ssm7fkc")))

(define-public crate-cang-jie-0.8.0 (c (n "cang-jie") (v "0.8.0") (d (list (d (n "flexi_logger") (r "^0.15.2") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.5.0") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tantivy") (r "^0.12.0") (d #t) (k 0)))) (h "033m3q5n2k8ds906988w3v9w9ngnjivndli9b1as229lw22j45wf")))

(define-public crate-cang-jie-0.9.0 (c (n "cang-jie") (v "0.9.0") (d (list (d (n "flexi_logger") (r "^0.15.10") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.0") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tantivy") (r "^0.12.0") (d #t) (k 0)))) (h "0mmykdra4anc6a86n4gylrk4p60fgind8qqkykh52bzxwlp3sj6i")))

(define-public crate-cang-jie-0.10.0 (c (n "cang-jie") (v "0.10.0") (d (list (d (n "flexi_logger") (r "^0.15.11") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.0") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tantivy") (r "^0.13.0") (d #t) (k 0)))) (h "038g9z2askyj6yr0qlbpc07qzl645n31zkmiwgsc7xf24wfydmds") (y #t)))

(define-public crate-cang-jie-0.11.0 (c (n "cang-jie") (v "0.11.0") (d (list (d (n "flexi_logger") (r "^0.17.1") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tantivy") (r "^0.14.0") (d #t) (k 0)))) (h "1k895v026ka275b2fz3ml31j26byqp0qn49y88jl1388gk5hjjra") (y #t)))

(define-public crate-cang-jie-0.11.1 (c (n "cang-jie") (v "0.11.1") (d (list (d (n "flexi_logger") (r "^0.17.1") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tantivy") (r "^0.14.0") (d #t) (k 0)))) (h "0qhxc1q585c1rzg3hv01a4ff8sszkvlpi2qzcgksl11ich8a2pbr")))

(define-public crate-cang-jie-0.12.0 (c (n "cang-jie") (v "0.12.0") (d (list (d (n "flexi_logger") (r "^0.17.1") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tantivy") (r "^0.14.0") (d #t) (k 0)))) (h "17rg3hlm8l6a26gxn0ijlbi60p9ww64f5iq0x3wn44i7b7dp3mwp") (y #t)))

(define-public crate-cang-jie-0.12.1 (c (n "cang-jie") (v "0.12.1") (d (list (d (n "flexi_logger") (r "^0.18.0") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.5") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tantivy") (r "^0.15.3") (d #t) (k 0)))) (h "02r239afz8mrvnk1f15dkklk337j28n8g357svpl30305npgiqps")))

(define-public crate-cang-jie-0.13.0 (c (n "cang-jie") (v "0.13.0") (d (list (d (n "flexi_logger") (r "^0.19.4") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.5") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tantivy") (r "^0.16.1") (d #t) (k 0)))) (h "0jicwn7lnha5l1j6vvv04i9iszjl468l52bywgasy58v76axsg12")))

(define-public crate-cang-jie-0.14.0 (c (n "cang-jie") (v "0.14.0") (d (list (d (n "flexi_logger") (r "^0.22.5") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.6") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tantivy") (r "^0.18.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 2)))) (h "1350bf6hi9k1pfvzhga9nfim2r58l5rijp4sr3y75gjg2g9092jh")))

(define-public crate-cang-jie-0.15.0 (c (n "cang-jie") (v "0.15.0") (d (list (d (n "flexi_logger") (r "^0.25.1") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.7") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.1") (d #t) (k 0)))) (h "14jv97wyiv0q3qapgnnbdiccswscb91af8wh62i4fzv21lrysf0j")))

(define-public crate-cang-jie-0.16.0 (c (n "cang-jie") (v "0.16.0") (d (list (d (n "flexi_logger") (r "^0.25.5") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.7") (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "tantivy") (r "^0.20") (d #t) (k 0)))) (h "0gm0s0vlpvcpcbj4rqd7wdm357b3vwzd41gawdyial9apwxg5szh")))

(define-public crate-cang-jie-0.17.0 (c (n "cang-jie") (v "0.17.0") (d (list (d (n "flexi_logger") (r "^0.25.6") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.8") (k 0)) (d (n "jieba-rs") (r "^0.6.8") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tantivy") (r "^0.20.2") (d #t) (k 0)))) (h "12579jkwih27jbc8qzn0whr02rhhgp2vx4b6qdghjbc8s49d1bxn")))

(define-public crate-cang-jie-0.18.0 (c (n "cang-jie") (v "0.18.0") (d (list (d (n "flexi_logger") (r "^0.27.2") (d #t) (k 2)) (d (n "jieba-rs") (r "^0.6.8") (k 0)) (d (n "jieba-rs") (r "^0.6.8") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "0z1szyijsidkqs8z5wsqsi1xcrwr88aswggd7pflcrbpg2rkg5in")))

