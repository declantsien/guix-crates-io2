(define-module (crates-io ca rp carpenter) #:use-module (crates-io))

(define-public crate-carpenter-0.1.0 (c (n "carpenter") (v "0.1.0") (d (list (d (n "carpenter-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0xbac5ah8azs27rb068ag3s2y500w9gpncklnc0yhnkhvkskzpmq")))

