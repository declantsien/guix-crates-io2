(define-module (crates-io ca rp carp) #:use-module (crates-io))

(define-public crate-carp-0.1.0 (c (n "carp") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)) (d (n "pcap") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "1k2fcmjvkjk8fm8qkalil48w3h519d9hcmycr8yaa2sxggk2r6md")))

