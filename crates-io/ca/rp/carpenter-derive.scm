(define-module (crates-io ca rp carpenter-derive) #:use-module (crates-io))

(define-public crate-carpenter-derive-0.1.0 (c (n "carpenter-derive") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "bytestream") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits" "visit" "visit-mut" "fold"))) (d #t) (k 0)))) (h "1hcz965srig8ivfvyd4rgqz1amyqdqdg636mxx807zda1vqarzcc")))

