(define-module (crates-io ca rp carpio_endpoint) #:use-module (crates-io))

(define-public crate-carpio_endpoint-0.1.0 (c (n "carpio_endpoint") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schema") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rkglvii0040fnmfxlg006cmmcnh21jx2zr630a73rr5n0y952kw") (y #t)))

