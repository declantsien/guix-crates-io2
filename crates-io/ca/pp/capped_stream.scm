(define-module (crates-io ca pp capped_stream) #:use-module (crates-io))

(define-public crate-capped_stream-0.1.0 (c (n "capped_stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1khb4fvkg6bsgnqrjc0qxy2470zmrh092j8aj7xrq371zq6xml1x")))

(define-public crate-capped_stream-0.1.1 (c (n "capped_stream") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0vvk6km5s6qac8z8kc10a47llq1y479p5r9xxvdy0s82m4wczz9z")))

