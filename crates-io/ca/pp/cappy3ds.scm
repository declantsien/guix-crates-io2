(define-module (crates-io ca pp cappy3ds) #:use-module (crates-io))

(define-public crate-cappy3ds-0.0.1 (c (n "cappy3ds") (v "0.0.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.4") (d #t) (k 0)) (d (n "memchr") (r "^2.6.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "0pls33jzzs1y0k7ysrkicmc8lry1xw9avffslpmvgzari1qxx6rl")))

