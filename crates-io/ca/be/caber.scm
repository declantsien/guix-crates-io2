(define-module (crates-io ca be caber) #:use-module (crates-io))

(define-public crate-caber-0.1.0 (c (n "caber") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1rf0gqqvhdqr31g8186k7vg0kh8ng7fxsf2w8rir9c92jl71c45v") (f (quote (("cli" "clap"))))))

(define-public crate-caber-0.1.1 (c (n "caber") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0md94ixsc8brh46d2zr9x9mdlaxa1k2ja5pqqdxkyl5qdn7w1hvz") (f (quote (("cli" "clap"))))))

