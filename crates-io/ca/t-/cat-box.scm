(define-module (crates-io ca t- cat-box) #:use-module (crates-io))

(define-public crate-cat-box-0.1.0 (c (n "cat-box") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "0f9dq76a5w5dp8gjag766ias0vgk7rygz5ql80fg4zlgdbfncpmz")))

(define-public crate-cat-box-0.1.1 (c (n "cat-box") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "0djk92jzhai6fi6yncyzlfq12ryf0blqai3wzj9wqqnlpbj7m02l")))

(define-public crate-cat-box-0.1.2 (c (n "cat-box") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "0vfb3ivhnqjk366w0mkd58mrhc9x1m9qi35zcs93r2lkryjhn59n")))

(define-public crate-cat-box-0.1.3 (c (n "cat-box") (v "0.1.3") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "1k85l8picwcfkn9mqa0ifp6brdmzbzbdppxhsbmjx8i6yv8365qq")))

(define-public crate-cat-box-0.1.4 (c (n "cat-box") (v "0.1.4") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "0ahwagi5a06ykd47hzx9g5ix5hiks19rfahziif8ynbpsmxn9nix")))

(define-public crate-cat-box-0.1.5 (c (n "cat-box") (v "0.1.5") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "11l8sssa2yh5yv3304lydchqkmyr0s344qvxkn3fb8nys1nps73g")))

(define-public crate-cat-box-0.1.6 (c (n "cat-box") (v "0.1.6") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "06dxmz64y5rl5h64n2rifmqzxwz13mmjirmgr63a5icxzgba1g94") (f (quote (("static" "sdl2/bundled") ("default"))))))

(define-public crate-cat-box-0.1.7 (c (n "cat-box") (v "0.1.7") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "09np89lx9n9c4xsy176wvfkkm94njjnx2zazjfh2020xynyai9gf") (f (quote (("static" "sdl2/bundled") ("default"))))))

(define-public crate-cat-box-0.1.8 (c (n "cat-box") (v "0.1.8") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "1fpiscq7yh88lki6y7dn71v30819c7xnmb4gynhcjyias4zr60cf") (f (quote (("static" "sdl2/bundled") ("default"))))))

(define-public crate-cat-box-22.6.21 (c (n "cat-box") (v "22.6.21") (d (list (d (n "sdl2") (r "^0.35.2") (f (quote ("image" "ttf"))) (d #t) (k 0)))) (h "0fxpvkm2nzndjh59ily2la8pli1652yqrbab2m65pihg9gawx3vi") (f (quote (("static" "sdl2/static-link" "sdl2/bundled") ("default"))))))

