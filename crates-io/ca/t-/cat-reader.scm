(define-module (crates-io ca t- cat-reader) #:use-module (crates-io))

(define-public crate-cat-reader-0.1.0 (c (n "cat-reader") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "1y9d2brnl13bwddlvxr7b5acc58ljc1giqjrim4s7sfsrh7scgmn")))

(define-public crate-cat-reader-0.1.1 (c (n "cat-reader") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "0iy4x0axbhc3mfp71lab6s9flf67n65xkpnzi5y8n7z0v7lx7gvb")))

(define-public crate-cat-reader-0.1.2 (c (n "cat-reader") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "12av82kihd4a1kpvz6gnm4zvcm4125nxqx0dhj4f5nphp687zbdq")))

(define-public crate-cat-reader-0.1.3 (c (n "cat-reader") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "1pzbzxf865kqgkisyyqsms4k07d71c4xad257jq6n009k9dwm78w")))

(define-public crate-cat-reader-1.0.0 (c (n "cat-reader") (v "1.0.0") (h "05h68pzg970c6jj6gy7vnypn0d42irhxj1rjvf7innh51hshs423")))

