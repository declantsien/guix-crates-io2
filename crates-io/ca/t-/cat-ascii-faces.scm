(define-module (crates-io ca t- cat-ascii-faces) #:use-module (crates-io))

(define-public crate-cat-ascii-faces-0.1.0 (c (n "cat-ascii-faces") (v "0.1.0") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "048p5qmizi0lsvzhjjahzi2fqcn757dbwy9g4w8qiq4dz18dc8p8")))

(define-public crate-cat-ascii-faces-0.1.1 (c (n "cat-ascii-faces") (v "0.1.1") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "10xj29j3615f646w5m93jw5i3fbjd1jnqgi8wnki3yl6fmqbvka6")))

(define-public crate-cat-ascii-faces-0.1.2 (c (n "cat-ascii-faces") (v "0.1.2") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "0sl630gkckihrxidkwfvjvvy3pidxkid0bi0a2zj8ggjn548xk38")))

(define-public crate-cat-ascii-faces-0.1.3 (c (n "cat-ascii-faces") (v "0.1.3") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "0lv822y2gcf6zqwm8ci0bhk7cvxzzii6gkhdqkli77bywn0l39hk")))

(define-public crate-cat-ascii-faces-0.1.4 (c (n "cat-ascii-faces") (v "0.1.4") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "1ykna3jvgjn2wm3dyf63sji2xs8s0sig13q89b33sf9n7q4xfqxi")))

(define-public crate-cat-ascii-faces-0.1.5 (c (n "cat-ascii-faces") (v "0.1.5") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "0drdzzvg47v6l847nc1zn5x44i92ckv30wss1w2arz8sb4z5xzg6")))

