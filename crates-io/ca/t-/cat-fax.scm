(define-module (crates-io ca t- cat-fax) #:use-module (crates-io))

(define-public crate-cat-fax-0.1.0 (c (n "cat-fax") (v "0.1.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06q27xjlw4a34acnm0150r15klizavylnd68s27vknjrzg9pl07y")))

(define-public crate-cat-fax-0.2.0 (c (n "cat-fax") (v "0.2.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0zlmn7di1ihyxdd3mk2lhwvk1xw1wjv99bkrs4hkmdvq429dglqy")))

(define-public crate-cat-fax-0.2.1 (c (n "cat-fax") (v "0.2.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0iwxfpkb93isi5jhd8lrpn19cpjww0kpayjyy23pmaljszafysjj")))

(define-public crate-cat-fax-0.3.0 (c (n "cat-fax") (v "0.3.0") (d (list (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1lkq5zvkrq5zwyw4b7hj47hw315chsdmhbr466lgvpk82g51jlpq")))

(define-public crate-cat-fax-0.4.0 (c (n "cat-fax") (v "0.4.0") (d (list (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xpb5g407j53vy0d8cm2z76q3l7w1svl0r8iqygnqbkyvbxwnndz")))

(define-public crate-cat-fax-0.5.0 (c (n "cat-fax") (v "0.5.0") (d (list (d (n "gfx") (r "^0.18.2") (d #t) (k 0)) (d (n "gfx_glyph") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "glyph_brush") (r "^0.7.3") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ppgsyrih7sqv380dvnqm1w1kxkcr5nvra17bps68v0kqisksdf9")))

(define-public crate-cat-fax-1.0.0 (c (n "cat-fax") (v "1.0.0") (d (list (d (n "better_term") (r "^1.4.5") (f (quote ("output" "fancy"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nvz0vx3sqwfa26cv2hhggr5z0rpi0br15144c0rnjs1dpkgfcdh")))

(define-public crate-cat-fax-1.0.1 (c (n "cat-fax") (v "1.0.1") (d (list (d (n "better_term") (r "^1.4.5") (f (quote ("output" "fancy"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p9xcj8ik2hrs1ic9c1wf8l6fzhm81lm79ii3fx9qlxy7krjid2w")))

(define-public crate-cat-fax-1.1.0 (c (n "cat-fax") (v "1.1.0") (d (list (d (n "better_term") (r "^1.4.5") (f (quote ("output" "fancy"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1284d4vxv6pax1ksq97k11x9ldmy4m11nfymdbzkkm1kafw9m8n4")))

