(define-module (crates-io ca t- cat-clone) #:use-module (crates-io))

(define-public crate-cat-clone-0.1.0 (c (n "cat-clone") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0yvly9sd0j8abris0fv3qywr5iad8gwmvkgbldaipmzjy4lv40cw")))

