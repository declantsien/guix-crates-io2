(define-module (crates-io ca t- cat-loggr) #:use-module (crates-io))

(define-public crate-cat-loggr-1.0.0 (c (n "cat-loggr") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "14frky8pjlfnb3dd7dhw2dvdjxcwa1z4sk5ff9faa2fndwh9wbkk") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-cat-loggr-1.0.1 (c (n "cat-loggr") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0c7rcv28i6mll7smi6m3s4vs452fc4jvjyfiwxvzc3lrsajp3rih") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-cat-loggr-1.0.2 (c (n "cat-loggr") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1fjqigwlhxbffb3qmia9h6jjhy5ra7c1xdxjjy5binwzm7ds28pw") (f (quote (("macros") ("default" "macros"))))))

