(define-module (crates-io ca ra caracall) #:use-module (crates-io))

(define-public crate-caracall-0.1.0 (c (n "caracall") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a2vbkkpcy5xzsaznnv3x6j3ynbzr3bc0xdndxcv6hd6qygkxf9h")))

(define-public crate-caracall-0.1.1 (c (n "caracall") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s83ikd0q6rmlv9q3zfl272rfaxwcr3hcr0l8wwli0lvhbjgc076")))

(define-public crate-caracall-0.1.1-1 (c (n "caracall") (v "0.1.1-1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ngy095lap7lkghqk0kf6fzmdhd079s53i1vpaf2bi2p4aimqahi")))

