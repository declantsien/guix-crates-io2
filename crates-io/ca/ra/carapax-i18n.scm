(define-module (crates-io ca ra carapax-i18n) #:use-module (crates-io))

(define-public crate-carapax-i18n-0.1.0 (c (n "carapax-i18n") (v "0.1.0") (d (list (d (n "carapax") (r "^0.2.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 2)) (d (n "gettext") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)))) (h "1z1avl5cxs8989whv6knq87nqfyzd32ay3jwaz7j21ra9y63kzi4")))

(define-public crate-carapax-i18n-0.2.0 (c (n "carapax-i18n") (v "0.2.0") (d (list (d (n "carapax") (r "^0.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "07r05yk4qk80v84f56hb76jlfc6nxiilsc53s2sby7qabfzsrwq2")))

(define-public crate-carapax-i18n-0.2.1 (c (n "carapax-i18n") (v "0.2.1") (d (list (d (n "carapax") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1mv88rcxz728bn5fsd68s4lb2fwhfka13gqqrdhfllbsy1rjxdif")))

