(define-module (crates-io ca ra caravel_export_poc) #:use-module (crates-io))

(define-public crate-caravel_export_poc-0.1.0 (c (n "caravel_export_poc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0my504agidygq7km3bq3bk75h8ncpwnr78mgj0wfrb0ciikqb7lh")))

(define-public crate-caravel_export_poc-0.1.1 (c (n "caravel_export_poc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1aswg78xfnh3bfqg5rv430q1hdp6b5hfyyrvsrfci9k0d07wyc1y")))

(define-public crate-caravel_export_poc-0.1.2 (c (n "caravel_export_poc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qbk7jzi2zgfcfcp2nh1jwdw6avfxcygdlbw19hz25cfzbrfhyj6")))

