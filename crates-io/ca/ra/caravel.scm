(define-module (crates-io ca ra caravel) #:use-module (crates-io))

(define-public crate-caravel-0.1.0 (c (n "caravel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "typetag") (r "^0.2.16") (d #t) (k 0)))) (h "07ibghfphpy4v8h3lasg3wysly80rigw0l900vai7jkf8fyc0kqa")))

