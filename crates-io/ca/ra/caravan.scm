(define-module (crates-io ca ra caravan) #:use-module (crates-io))

(define-public crate-caravan-0.1.0 (c (n "caravan") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b7cbz0pc6rm4r9ndcb11p1hyvwzx2s6z9wgyh0zhn1v9cy1khlc")))

