(define-module (crates-io ca ra carapax-access) #:use-module (crates-io))

(define-public crate-carapax-access-0.1.0 (c (n "carapax-access") (v "0.1.0") (d (list (d (n "carapax") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)))) (h "1hxsbw9rdxmryyy23maqjpz1jqka6mn39pdy3n48zj2rsr8h9p3d")))

(define-public crate-carapax-access-0.2.0 (c (n "carapax-access") (v "0.2.0") (d (list (d (n "carapax") (r "^0.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "09gzybz32l5ph92jwclsw5ng2rag1f7jwcxq7l1vgsqbsa588w3y")))

(define-public crate-carapax-access-0.2.1 (c (n "carapax-access") (v "0.2.1") (d (list (d (n "carapax") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0km6pp3h4jzclv6jf4039nx45dg0k4kdkvin16nxdijimpxzqvp8")))

