(define-module (crates-io ca ra carapax-codegen) #:use-module (crates-io))

(define-public crate-carapax-codegen-0.1.0 (c (n "carapax-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x18ll63iajpzknymi3f0xjan2lwylw8jrji6kp4z9lwrwpgjhvz")))

(define-public crate-carapax-codegen-0.1.1 (c (n "carapax-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdlfv1fznyq2fz9sq55rxrkxhhsmfs4dv38xh0qr2x25p7ygfmc")))

(define-public crate-carapax-codegen-0.2.0 (c (n "carapax-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qd6pdmx9ihdg3palndbn9p6hal9xc4yy9awk34mc4w9bgy86h9q")))

