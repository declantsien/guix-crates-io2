(define-module (crates-io ca oe caoe) #:use-module (crates-io))

(define-public crate-caoe-0.1.0 (c (n "caoe") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "prctl") (r "^1.0.0") (d #t) (k 0)) (d (n "procspawn") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1irw06szzqdjw48h67m96xjnihvif3d7rq2akdhh7mv171bcl65q")))

(define-public crate-caoe-0.1.1 (c (n "caoe") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "prctl") (r "^1.0.0") (d #t) (k 0)) (d (n "procspawn") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "10m6mswg17fcc9aaiw21iw1vb1hz9ck0mydcnpmrybg6bhiz507v")))

(define-public crate-caoe-0.1.2 (c (n "caoe") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "prctl") (r "^1.0.0") (d #t) (k 0)) (d (n "procspawn") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "000ccwv5wg4fh9pdz1x84qjdpdlj9mrq4znzb2y549615lw3yvn7")))

