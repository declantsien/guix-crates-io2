(define-module (crates-io ca dd caddyfile) #:use-module (crates-io))

(define-public crate-caddyfile-0.1.0 (c (n "caddyfile") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "03jmkz9d5h8llidzwy2nnb4rbn3ip4za7hlc2p364wi8mxbjrj1x")))

(define-public crate-caddyfile-0.1.1 (c (n "caddyfile") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "03ahgv77f0ap0rliq5x4rhdg4cmhvapfjs3z9qhd4qylr5i4xr05")))

