(define-module (crates-io ca pg capgrok) #:use-module (crates-io))

(define-public crate-capgrok-0.1.0 (c (n "capgrok") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mg35s1pxqfja4bin27cmhvvikqw12as4jy25g13srxf46n40bsd")))

(define-public crate-capgrok-0.2.0 (c (n "capgrok") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0iw0l7d33ipfvk22jl05c8n9b2fwxd556gkbkzi52cxpd1syvaf5")))

(define-public crate-capgrok-0.2.1 (c (n "capgrok") (v "0.2.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05nq60x91lbk3id242zx3k1qmn0s5440kadcbk6wlw3hblfiff62")))

(define-public crate-capgrok-0.2.2 (c (n "capgrok") (v "0.2.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0aks3r0zzgnlaih5i0f7m9n1zk37hqdrya2fzrpx230r2fy6wjxb")))

(define-public crate-capgrok-0.3.0 (c (n "capgrok") (v "0.3.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jzf0rjg335nshid4b74r1269bhzphansn1mx4q6p3x62jfz9c8p")))

(define-public crate-capgrok-0.3.1 (c (n "capgrok") (v "0.3.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "iri-string") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "siwe") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s7sbwdl785cqwbxzka99hcj11kqn242gaz5vqhmicjqar2cbzn5")))

