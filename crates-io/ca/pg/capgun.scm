(define-module (crates-io ca pg capgun) #:use-module (crates-io))

(define-public crate-capgun-0.1.0 (c (n "capgun") (v "0.1.0") (d (list (d (n "clap") (r "^0.11.0") (d #t) (k 0)) (d (n "notify") (r "^2.1") (d #t) (k 0)) (d (n "term") (r "^0.2.9") (d #t) (k 0)))) (h "0gn75b64ww1073fa0wkwq3nply453s7mcz49zg0593pbqia2h3cq")))

(define-public crate-capgun-0.1.1 (c (n "capgun") (v "0.1.1") (d (list (d (n "clap") (r "^0.11.0") (d #t) (k 0)) (d (n "notify") (r "^2.1") (d #t) (k 0)) (d (n "term") (r "^0.2.9") (d #t) (k 0)))) (h "1bv80fsqkirwd2d6l1vv7jhnc81apqqssgl2d7zsyw54j3qh0i3c")))

