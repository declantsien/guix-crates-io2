(define-module (crates-io ca ll call-by) #:use-module (crates-io))

(define-public crate-call-by-0.1.0 (c (n "call-by") (v "0.1.0") (h "0m0ld9jh8jcdz895l1fim7541hnwnwlf39rpfcm1gskwr1rnaa39")))

(define-public crate-call-by-0.2.0 (c (n "call-by") (v "0.2.0") (h "0g70416ddvbr9dm7ax870b32b0jqqg03hskqqrqgwzpn81kkjcr9")))

(define-public crate-call-by-0.2.1 (c (n "call-by") (v "0.2.1") (h "1v9wlyskmaci0347wrvagilqn095z1l6csg06ym2ifyr8i6sihd4")))

(define-public crate-call-by-0.2.2 (c (n "call-by") (v "0.2.2") (h "02vnjvpk1pc18rs8s3nrmxd6zi9iqvppiyigpcwvw469r7d640rj")))

(define-public crate-call-by-0.2.3 (c (n "call-by") (v "0.2.3") (h "0dirkwn1danhyvshygjs5zkbksbmk8ll67bq626388c22qv24v80")))

