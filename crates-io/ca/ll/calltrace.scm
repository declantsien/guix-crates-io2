(define-module (crates-io ca ll calltrace) #:use-module (crates-io))

(define-public crate-calltrace-0.1.0 (c (n "calltrace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)))) (h "0s8g188si10chwzxi1r4mzmnki49s8cmbd943wwkygjfxhn4v7sk")))

(define-public crate-calltrace-0.1.1 (c (n "calltrace") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)))) (h "0fqa6z7phcbzqkc8233f8mm0db0ca6dz1pvgnrz0wm8wjcp4hkxj")))

(define-public crate-calltrace-0.1.2 (c (n "calltrace") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)))) (h "14n0dg44f7mvl2jl59imb8jsvk668b04jvkx8l7sc8yh9hzkqig8")))

(define-public crate-calltrace-0.1.3 (c (n "calltrace") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "04rn0qxl9d2rc9iigrnhmbiw5h8z29idzdxr2l6mczvw4dk83b0a")))

(define-public crate-calltrace-0.1.4 (c (n "calltrace") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0fnh2y9j1pg42n1gnl9ispjvv7fharlcmbcrvparf3fya9fd0fgs")))

(define-public crate-calltrace-0.1.5 (c (n "calltrace") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1x9ynwy81kd9iv4bhnxpi6b1vgxwhlyfa82qh09dqr859sszqb4h")))

(define-public crate-calltrace-0.1.6 (c (n "calltrace") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "035b106lj9c8siarzy8c68ikjg9waf2qmx14kl5pbh7anfg4mph3")))

(define-public crate-calltrace-0.1.7 (c (n "calltrace") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1h9g3kk3gxpvacjsca0sk7n7b1pyrxpj192i29ml7jv24lf96f25")))

(define-public crate-calltrace-0.1.8 (c (n "calltrace") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "160pm4ji6k3z8l9gnzz09sww6p2kbpwcfqqckb1rgy45bli4ng90")))

(define-public crate-calltrace-0.1.10 (c (n "calltrace") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1km121q4yyzzi61fn80fkkp3q8j8ldnv41lmappg4368p6fl2wsx")))

(define-public crate-calltrace-0.1.11 (c (n "calltrace") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0w617wpmxcqyzq1m10s53n1aaxnl498k9ac75cbwjv7m9jg9jf6h")))

(define-public crate-calltrace-0.1.12 (c (n "calltrace") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1ydxkn0j37bw2x1w5y1980kmjd0d7ndxg6fg346yybs8q6x778rg")))

