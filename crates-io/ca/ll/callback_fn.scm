(define-module (crates-io ca ll callback_fn) #:use-module (crates-io))

(define-public crate-callback_fn-0.1.0 (c (n "callback_fn") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 2)) (d (n "strum_macros") (r "^0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dpw3n7fbpzlshncmfd06zm7c3yqqyg1g91m4zfg0973qs835706")))

