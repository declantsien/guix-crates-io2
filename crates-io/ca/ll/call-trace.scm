(define-module (crates-io ca ll call-trace) #:use-module (crates-io))

(define-public crate-call-trace-0.1.0 (c (n "call-trace") (v "0.1.0") (d (list (d (n "call-trace-macro") (r "^0.1") (d #t) (k 0)))) (h "0giin408fyvvrlrjd3iznj62zmzx286cxn2bp3q20dqg7z57fz4k")))

(define-public crate-call-trace-0.2.0 (c (n "call-trace") (v "0.2.0") (d (list (d (n "call-trace-macro") (r "^0.2") (d #t) (k 0)))) (h "0n85ynyp6ixznpvxxm45gbpyw8spww7fw045sw45vqg0yxj2rxgh")))

(define-public crate-call-trace-0.3.0 (c (n "call-trace") (v "0.3.0") (d (list (d (n "call-trace-macro") (r "^0.3") (d #t) (k 0)))) (h "1wbhg4z6cd3s3g17hvrlwqn98sifqbzwvivkv31j0i78909rsmm6")))

(define-public crate-call-trace-0.4.0 (c (n "call-trace") (v "0.4.0") (d (list (d (n "call-trace-macro") (r "^0.4") (d #t) (k 0)))) (h "060k37dczr44v70202s2p65ywz7a5liby7r210z1ws278x40qbdg")))

