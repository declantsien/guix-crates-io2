(define-module (crates-io ca ll callback-trait) #:use-module (crates-io))

(define-public crate-callback-trait-0.1.0 (c (n "callback-trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "12j1zqxwgg4i6f5g725j1wvn6zpc6hr1475wcjbsms03i37gz6fh")))

(define-public crate-callback-trait-0.1.1 (c (n "callback-trait") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0dxj2jva2fbs3sg11bzpavc2fkvr0jx9ipgcnn9kfk7yrbbwn5f1")))

(define-public crate-callback-trait-0.1.2 (c (n "callback-trait") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "13lmqjjkssjnkn353v34ggzm05r1f4x6dnfyc0cxkgrbqx7h32sw")))

(define-public crate-callback-trait-0.1.3 (c (n "callback-trait") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0jis95047ybydp2qfwymwf5bjqyb7hrn4jn85aqmi7d9yl26p63z")))

