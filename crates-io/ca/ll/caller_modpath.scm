(define-module (crates-io ca ll caller_modpath) #:use-module (crates-io))

(define-public crate-caller_modpath-0.1.0 (c (n "caller_modpath") (v "0.1.0") (d (list (d (n "caller_modpath_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.20") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z3lphin4sl5646fa3c67p2y9dyhj73g2124ag0zj0jb9qsj3d5w")))

(define-public crate-caller_modpath-0.1.1 (c (n "caller_modpath") (v "0.1.1") (d (list (d (n "caller_modpath_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ckng6kpq51xppcipp8l5y4rbsdhf421qv4fn5qaqd4c53qbdsmm")))

