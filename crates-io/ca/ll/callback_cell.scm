(define-module (crates-io ca ll callback_cell) #:use-module (crates-io))

(define-public crate-callback_cell-0.1.0 (c (n "callback_cell") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0an6wh95mz10bf95gk7s1nmafir57496g2wkw471ff622hx0v8zr")))

