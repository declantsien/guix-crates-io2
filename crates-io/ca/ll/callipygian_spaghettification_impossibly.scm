(define-module (crates-io ca ll callipygian_spaghettification_impossibly) #:use-module (crates-io))

(define-public crate-callipygian_spaghettification_impossibly-0.0.0 (c (n "callipygian_spaghettification_impossibly") (v "0.0.0") (h "0whgvj6977mfwx6wh95r6jhy080k5gq5ikzy5c5xh8fhhgprrc9y") (y #t)))

(define-public crate-callipygian_spaghettification_impossibly-0.0.1 (c (n "callipygian_spaghettification_impossibly") (v "0.0.1") (h "1ramqkwqwd2dpyvkz0yznp8s7x2axfsxfaa7h217v22s0pa9qrhc") (y #t)))

(define-public crate-callipygian_spaghettification_impossibly-0.0.2 (c (n "callipygian_spaghettification_impossibly") (v "0.0.2") (d (list (d (n "eyre") (r "^0.6.10") (d #t) (k 0)))) (h "1l3kb8wmffraigylfhfn10ki51xq1drhy07a8rrlidqr4a4bxm2g") (y #t)))

