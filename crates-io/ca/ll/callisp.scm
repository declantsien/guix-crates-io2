(define-module (crates-io ca ll callisp) #:use-module (crates-io))

(define-public crate-callisp-0.1.0 (c (n "callisp") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "025ddylc5mmb6isrwkan1gy0ji5za8syn40ln1p0cshyz7k14frz")))

(define-public crate-callisp-0.2.0 (c (n "callisp") (v "0.2.0") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1wzfgax7iagfmcmd3gcbaczczsd5j2536l9i5sf10dj9mp6j5h8v")))

(define-public crate-callisp-0.2.1 (c (n "callisp") (v "0.2.1") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1bznlx0xh38k3w0yhsarxaj470pncv6wqpkpqd7synz7834y590g")))

