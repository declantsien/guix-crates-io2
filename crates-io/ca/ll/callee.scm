(define-module (crates-io ca ll callee) #:use-module (crates-io))

(define-public crate-callee-0.1.0 (c (n "callee") (v "0.1.0") (h "0cnk1v958hk1lv0062jizhnwj58qxwz69hb0pihab19fijrzb1mi")))

(define-public crate-callee-0.1.1 (c (n "callee") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0x7b7d832r0fxzg2dyn797fck65ps7mindai883la2c8k50bdvy8") (f (quote (("default" "serde"))))))

(define-public crate-callee-0.1.2 (c (n "callee") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "05h6ajk3f79ywkzkgc4j2kcrdzz3dnfynbmgcaafrvx1b7sk4rsl") (f (quote (("default" "serde"))))))

(define-public crate-callee-0.1.3 (c (n "callee") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p5yg0r6w2ygcnmwz4p5vn87fb4gliiwb1kvfiq2z96kzirccn3m") (f (quote (("default" "serde"))))))

(define-public crate-callee-0.1.4 (c (n "callee") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hybrsmysyz8jbns3jayf0i8ampn52sfcj18jh93jj5rndzlzr8w") (f (quote (("default" "serde"))))))

