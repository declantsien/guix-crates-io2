(define-module (crates-io ca ll calloop-notify) #:use-module (crates-io))

(define-public crate-calloop-notify-0.1.0 (c (n "calloop-notify") (v "0.1.0") (d (list (d (n "calloop") (r "^0.10.1") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.15") (d #t) (k 0)))) (h "1fsss0k4mrqfw7qapg6pjq5jzi0c09ksnj1pl2m8gq2r2avhzs9d") (r "1.62.1")))

(define-public crate-calloop-notify-0.1.1 (c (n "calloop-notify") (v "0.1.1") (d (list (d (n "calloop") (r "^0.10.1") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.15") (d #t) (k 0)))) (h "19fp70ykdc03pxmvpmwf7nv3aj62gnzwgjfij2pkc7q4y83iir61") (r "1.62.1")))

