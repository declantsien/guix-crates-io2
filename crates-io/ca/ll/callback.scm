(define-module (crates-io ca ll callback) #:use-module (crates-io))

(define-public crate-callback-0.0.1 (c (n "callback") (v "0.0.1") (h "0fcjc0ijf2d7r1i4wimw0hiyk7hzm1c9bp124glaki2z6f99wnjb")))

(define-public crate-callback-0.0.2 (c (n "callback") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)))) (h "0knf41vbbhklvh7rjqw531s857b8ifa22avzwm004pph1wsy2m5i")))

(define-public crate-callback-0.0.3 (c (n "callback") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)))) (h "003w0p73w87wrdijw53gwq2p4c9c2qhqwnhaf2i5i4z4vhggmq96")))

(define-public crate-callback-0.0.4 (c (n "callback") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)))) (h "0gai0l3q2lzf783cyn1389hib5abnv8ihpqa18k0mliky3wcj3g0")))

(define-public crate-callback-0.0.5 (c (n "callback") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)))) (h "01pcbjfbapz03hrwyfvia9ygl46mjz0p868q6yjqjghq76z93jzl")))

(define-public crate-callback-0.0.6 (c (n "callback") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "08cs8mzy5i7xfnp0pz3kzn4mlwr4jl3z1rl2qppaf9bwc1as1jba")))

(define-public crate-callback-0.1.0 (c (n "callback") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "1irn35abhl1s65iq3yj0ywi1b75sa2y10s0qmv9akks10hc5343h")))

(define-public crate-callback-0.2.0 (c (n "callback") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "1805n0gzi68qh3dqs2nkz80k3qngj2sbj58nnjyfhik1yq2l6l0f")))

(define-public crate-callback-0.3.0 (c (n "callback") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "0pfpq78rlw0bnpy1pzlr0l3xv5xnsifaq7lrvfhj6f4lal90xcha")))

(define-public crate-callback-0.4.0 (c (n "callback") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0") (d #t) (k 0)))) (h "18k658wx72h8bhnf102pb57a4app54jhd5lj12pj3kykd0rmlp6c")))

(define-public crate-callback-0.4.2 (c (n "callback") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0") (d #t) (k 0)))) (h "04k0wjv8z9y7rpw0ii15ibqjsrffysmdknbfl4xy62chwms6akkv")))

(define-public crate-callback-0.4.3 (c (n "callback") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0") (d #t) (k 0)))) (h "0dajdaf588p5szvq8q7dsrv9kx1yjm5wym1g5mqg74j734daxax7")))

(define-public crate-callback-0.5.0 (c (n "callback") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0") (d #t) (k 0)))) (h "1gqkwjmxss3ni1yl573xdi0agpbybc4y8c08vqk6fdla29a3bf9z")))

(define-public crate-callback-0.5.1 (c (n "callback") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0q7m6bbiqa5lmy356ji9021nmpyalnnx4y6c5ng0fzyq165d8bcf")))

(define-public crate-callback-0.5.2 (c (n "callback") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "17yhh829s91bbwkplqyp0cgpyzhc5nmy4i5qg0a9i729jvmgdps7")))

(define-public crate-callback-0.5.3 (c (n "callback") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0") (d #t) (k 0)))) (h "08h4gzrbgvqgyi442sw4h47pf0agn7fkhar60nwi799y1c6d3a3p")))

(define-public crate-callback-0.5.4 (c (n "callback") (v "0.5.4") (d (list (d (n "spin") (r "^0") (d #t) (k 0)))) (h "02qpn1zwj5ky8yxjzamwzfi0c6x3i6vlg1ki3k23hq0w1iszah8v") (r "1.63")))

