(define-module (crates-io ca ll calliope-common) #:use-module (crates-io))

(define-public crate-calliope-common-0.0.1 (c (n "calliope-common") (v "0.0.1") (d (list (d (n "nrf51-hal") (r "^0.14.0") (d #t) (k 0)))) (h "18pzns4d2x4rwf93f3va5lcbqpxvwmg814pgjjrbnw6x4l68l847") (y #t)))

(define-public crate-calliope-common-0.0.2 (c (n "calliope-common") (v "0.0.2") (d (list (d (n "nrf51-hal") (r "^0.14.0") (d #t) (k 0)))) (h "1skq2dr2ipxvsjlwd5a1jfwmmqrxabvsjkh8r0346ji4sz1hi9km")))

