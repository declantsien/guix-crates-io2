(define-module (crates-io ca ll callable) #:use-module (crates-io))

(define-public crate-callable-0.1.0 (c (n "callable") (v "0.1.0") (h "1czxcqc5vm6cjyr1fa9ynk85539zjifbzisl6hpgbzahjhcbvy0w") (f (quote (("impl_with_macro_rules"))))))

(define-public crate-callable-0.2.0 (c (n "callable") (v "0.2.0") (h "1pfz1y363a28rny9i8v00pw13f6mlvs22hl78km5728dm0n2ppnc") (f (quote (("impl_with_macro_rules"))))))

(define-public crate-callable-0.3.0 (c (n "callable") (v "0.3.0") (h "0f9s1y7dkhhwbx440caf2ra4qxmc8y4rcdx4b9mp21nj0w1wiqfz") (f (quote (("impl_with_macro_rules"))))))

(define-public crate-callable-0.4.0 (c (n "callable") (v "0.4.0") (h "018wqrliz9lmxqf7l01n15sadv86xh5s4yl2hfkzn6a3gm4hhxsa") (f (quote (("impl_with_macro_rules") ("gat"))))))

(define-public crate-callable-0.5.0 (c (n "callable") (v "0.5.0") (h "1qbmibhbqv57l2qz9iznr3an59dczmc42jl8zrjvpynnsq44ac5v") (f (quote (("impl_with_macro_rules") ("gat"))))))

