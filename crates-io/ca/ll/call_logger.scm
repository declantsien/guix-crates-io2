(define-module (crates-io ca ll call_logger) #:use-module (crates-io))

(define-public crate-call_logger-0.0.1 (c (n "call_logger") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0k9g9kwva7qca44vlf52bi66zrkbrd7gq7fg9vrkay4ni0l8xbix") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.0.2 (c (n "call_logger") (v "0.0.2") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0wcrxng06gwcprh35zrb7ycmmd3ksx4a12jp4c09254vjfwd4gav") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.0.3 (c (n "call_logger") (v "0.0.3") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0zbv3y1hwmzvmb84py2ymcdcm0ip6q8sm6np3q139yj12mfrj864") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.0.4 (c (n "call_logger") (v "0.0.4") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "15bj60a8xa4ax94bhxb1i5hmmqj1is7dgchxzngj9g17iv3wpg21") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.0.5 (c (n "call_logger") (v "0.0.5") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "multi_log") (r "^0.1.2") (d #t) (k 2)))) (h "0lrgpgk4ahcl6cdz89q0rrfhs645fjnv9cfbav0r2q9v46g4av4v") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.0.6 (c (n "call_logger") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "multi_log") (r "^0.1.2") (d #t) (k 2)))) (h "0fdwkqb8ggiyifi65kwgw6mjzf106v9sdjcmw44k6xvxr8iygpws") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

(define-public crate-call_logger-0.1.0 (c (n "call_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("clock"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std" "kv"))) (d #t) (k 0)) (d (n "multi_log") (r "^0.1.2") (d #t) (k 2)))) (h "0cf1sywgc6jzcqddwmslxxd26ygbd47hcysrkgpmllzjil1bm7yn") (f (quote (("timestamps" "chrono") ("minimal") ("default" "timestamps")))) (r "1.58")))

