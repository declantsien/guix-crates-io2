(define-module (crates-io ca ll callback-future) #:use-module (crates-io))

(define-public crate-callback-future-0.1.0 (c (n "callback-future") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("async-await" "executor"))) (k 0)))) (h "0gqw7mradqixbz7rpzqfafg3sv8wa6c3syl1vn899fph3nj66smp")))

