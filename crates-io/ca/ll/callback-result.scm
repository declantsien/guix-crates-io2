(define-module (crates-io ca ll callback-result) #:use-module (crates-io))

(define-public crate-callback-result-0.1.0 (c (n "callback-result") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.1") (d #t) (k 0)))) (h "12i3bsdw5k9y54gf6j8s5zvl5qsyh40bbfyzqxf9ajkl3y99d9m7")))

(define-public crate-callback-result-0.1.1 (c (n "callback-result") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.1") (d #t) (k 0)))) (h "1yy7f81xczaizr60ri59lwxwsn162g49hn2nccxv5s3x8ady1isl")))

(define-public crate-callback-result-0.1.2 (c (n "callback-result") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.4") (d #t) (k 0)))) (h "12fipxf3ndgbwjz1rgbhdbkxb1jd71r58pmpigzma4qja9c34g45")))

(define-public crate-callback-result-0.1.3 (c (n "callback-result") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.4") (d #t) (k 0)))) (h "1jivigz1y1686f0xlm4qizck43lrx3iykwg5n72r4zjgznmlmz1m")))

