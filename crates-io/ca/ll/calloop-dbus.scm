(define-module (crates-io ca ll calloop-dbus) #:use-module (crates-io))

(define-public crate-calloop-dbus-0.1.0 (c (n "calloop-dbus") (v "0.1.0") (d (list (d (n "calloop") (r "^0.6") (d #t) (k 0)) (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qypsdszrzm0w3yc76k3fq38cq4gw0myp0pibrinsvak7i5p5x7g")))

(define-public crate-calloop-dbus-0.1.1 (c (n "calloop-dbus") (v "0.1.1") (d (list (d (n "calloop") (r "^0.6") (d #t) (k 0)) (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cks7ywgwk1wixdqmwxmi61hr942ajnsvv1inrn8j3hg0bipc66z")))

