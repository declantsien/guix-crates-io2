(define-module (crates-io ca ll callgrind) #:use-module (crates-io))

(define-public crate-callgrind-1.1.0 (c (n "callgrind") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "valgrind_request") (r "^1.1") (d #t) (k 0)))) (h "0dly9dxp1l64dy33xks6kd5nr92m2aaqkkqs3qy5lirrybm8ixzp")))

