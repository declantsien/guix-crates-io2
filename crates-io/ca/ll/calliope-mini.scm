(define-module (crates-io ca ll calliope-mini) #:use-module (crates-io))

(define-public crate-calliope-mini-0.1.0 (c (n "calliope-mini") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nrf51-hal") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "nrf52833-hal") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "tiny-led-matrix") (r "^1.0.1") (d #t) (k 0)))) (h "09yzjxg39733z2h054kv5hjc5brcbkgcspj51ii7mwq190x1ghkv") (f (quote (("v2" "nrf52833-hal") ("v1" "nrf51-hal") ("doc"))))))

