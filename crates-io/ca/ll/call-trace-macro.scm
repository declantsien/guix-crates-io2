(define-module (crates-io ca ll call-trace-macro) #:use-module (crates-io))

(define-public crate-call-trace-macro-0.1.0 (c (n "call-trace-macro") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0syxwnfj5b4d95igvzchvy7wnhpibz0w9z6x82w1bfvlb0lbyy4p")))

(define-public crate-call-trace-macro-0.2.0 (c (n "call-trace-macro") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0z67i0r4iybl90q0srr3lahkmzy6rpgbliri2zcmim66qmzvhzpy")))

(define-public crate-call-trace-macro-0.3.0 (c (n "call-trace-macro") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0il4sn9id2qwq1imbqn9im2c2zljqgqwc1vknv52yym7g59kljib")))

(define-public crate-call-trace-macro-0.4.0 (c (n "call-trace-macro") (v "0.4.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1vxvrq5rsf06hd4a67l8d7alzjvg1kmv8lsvn524hcn0dm9qz360")))

