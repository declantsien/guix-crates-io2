(define-module (crates-io ca ll calloop-subproc) #:use-module (crates-io))

(define-public crate-calloop-subproc-1.0.0 (c (n "calloop-subproc") (v "1.0.0") (d (list (d (n "async-process") (r "^1.0") (d #t) (k 0)) (d (n "calloop") (r "^0.10.0") (f (quote ("executor"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-binary") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0v5z3llsa9bn0xw9s0g617vqgzl59c2yi7n10c2zkmrgbxrawfl9")))

