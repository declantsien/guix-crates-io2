(define-module (crates-io ca ll calliope) #:use-module (crates-io))

(define-public crate-calliope-0.1.0 (c (n "calliope") (v "0.1.0") (h "1hwnlsb1vclp2r6izlra752dzq2fjm6ckmfmmip5qbz335b7v6iw") (y #t)))

(define-public crate-calliope-0.0.1 (c (n "calliope") (v "0.0.1") (d (list (d (n "calliope-common") (r "^0.0.1") (d #t) (k 0)))) (h "1kglplb4iabzhw4jb9h8vwd73p07d2mnh3xa8yz8b0v8kv6dy6xg")))

(define-public crate-calliope-0.0.2 (c (n "calliope") (v "0.0.2") (d (list (d (n "calliope-common") (r "^0.0.2") (d #t) (k 0)))) (h "03y6pfn5kiyh1f5ji54hkx55kjn64nwhvsax7sg1rlk2kmjpq5r6")))

