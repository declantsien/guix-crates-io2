(define-module (crates-io ca ll callpass) #:use-module (crates-io))

(define-public crate-callpass-0.2.0 (c (n "callpass") (v "0.2.0") (h "1dg94ygfx09vyyjmnad807z9jd6fcpa2q12r3cqq54lj1p47xm4b")))

(define-public crate-callpass-0.3.0 (c (n "callpass") (v "0.3.0") (h "1vb7v9d0kd7gap9zs65fxrk3d7ir7lxqkp2wdvyl7fcmdargr1qm")))

(define-public crate-callpass-0.3.1 (c (n "callpass") (v "0.3.1") (h "0cc8542a0wvkmjrf7h1qij8pjwlk1dzb60q444pzpircqld91xgw")))

(define-public crate-callpass-0.3.2 (c (n "callpass") (v "0.3.2") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)))) (h "1hmx781hk968a7ii1hh8ligcj12s55rvwl8cj9bis0qqi9vkbb2j") (f (quote (("std") ("default" "std"))))))

(define-public crate-callpass-1.0.0-rc.1 (c (n "callpass") (v "1.0.0-rc.1") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)))) (h "05f1mc6x5axdj9l3655a4fxii7sxn846x70rjamvd8k4l73arggk") (f (quote (("std") ("default" "std"))))))

(define-public crate-callpass-1.0.0 (c (n "callpass") (v "1.0.0") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)))) (h "014injpnchdzjx05izyk7hj80by5x89nfys9d2qrgvzw2abflcnf") (f (quote (("std") ("default" "std"))))))

(define-public crate-callpass-1.0.1 (c (n "callpass") (v "1.0.1") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mqc7c30xk9qma1sx7krfnpdsxsnf4aw0ygmm96lyx7nzvlfpsq3") (f (quote (("tools" "docopt" "serde" "serde_derive") ("std") ("default" "std" "tools"))))))

(define-public crate-callpass-1.0.2 (c (n "callpass") (v "1.0.2") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1syam8skl3cysdyjmbvx2hl0rdjvyx8ahz9k2ia6n3zfhdfwgvbs") (f (quote (("tools" "docopt" "serde" "serde_derive") ("std") ("default" "std" "tools"))))))

(define-public crate-callpass-1.0.3 (c (n "callpass") (v "1.0.3") (d (list (d (n "ascii") (r "0.9.*") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ppniincjz5y0dzrs25w3bhcr7im0yrcifhd560qxw99b484jxrk") (f (quote (("tools" "docopt" "serde" "serde_derive") ("std") ("default" "std" "tools"))))))

(define-public crate-callpass-1.0.4 (c (n "callpass") (v "1.0.4") (d (list (d (n "ascii") (r "1.*") (d #t) (k 0)) (d (n "docopt") (r "1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (o #t) (d #t) (k 0)))) (h "09a529ma1gq4p9zw7g6nz85cmcysqzc207pvgcjkvhq5k0gyh0v0") (f (quote (("tools" "docopt" "serde" "serde_derive") ("std") ("default" "std" "tools"))))))

(define-public crate-callpass-1.0.5 (c (n "callpass") (v "1.0.5") (d (list (d (n "ascii") (r "1.*") (d #t) (k 0)) (d (n "docopt") (r "1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (o #t) (d #t) (k 0)))) (h "16f99daahpkd3179idym84pl595zjpbsj4609lcdgi751xkanwzz") (f (quote (("tools" "docopt" "serde" "serde_derive") ("std") ("default" "std" "tools"))))))

