(define-module (crates-io ca ll caller_modpath_macros) #:use-module (crates-io))

(define-public crate-caller_modpath_macros-0.1.0 (c (n "caller_modpath_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0x0kgphj5cqhic6r54wk3dvrrff9n8nziqqz10xjb4ghwfbgp4sj")))

(define-public crate-caller_modpath_macros-0.1.1 (c (n "caller_modpath_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0rl58wpy4z6jf9mam17wpvfnbp7kxcacg7rbv70dn6flnwamj3ad")))

