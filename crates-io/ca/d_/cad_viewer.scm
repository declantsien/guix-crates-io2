(define-module (crates-io ca d_ cad_viewer) #:use-module (crates-io))

(define-public crate-cad_viewer-0.1.0 (c (n "cad_viewer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cad_import") (r "^0.2") (d #t) (k 0)) (d (n "glow") (r "^0.12") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "0ilvv12dx75kg831cni1rv4cx53m1gh72mwkmfzmvk4cdmkvp3d0")))

(define-public crate-cad_viewer-0.2.0 (c (n "cad_viewer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cad_import") (r "^0.3") (d #t) (k 0)) (d (n "glow") (r "^0.12") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1cy30p6wj43bm3jvmfnyfnm9fiw2fr9r0lvx12vkfh0jkjbn3j6v")))

