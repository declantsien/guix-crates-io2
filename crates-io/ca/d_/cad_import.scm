(define-module (crates-io ca d_ cad_import) #:use-module (crates-io))

(define-public crate-cad_import-0.1.0 (c (n "cad_import") (v "0.1.0") (d (list (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "01j7n43aqyfhmr3b05s67a30p6gaj3hsr39fvw8nf4mh6v66lyvi")))

(define-public crate-cad_import-0.2.0 (c (n "cad_import") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1lw1ggi4mwf0zq3vsa26lgywas444ssin76bjvinqbcw9k9cxvdj")))

(define-public crate-cad_import-0.2.1 (c (n "cad_import") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "193whb3hvnhfqpjdxmlcc83wj97yw0mmdhcaa6dffwxyqsafdf4z")))

(define-public crate-cad_import-0.3.0 (c (n "cad_import") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "gltf") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 0)) (d (n "path-clean") (r "^1.0") (d #t) (k 2)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)))) (h "04dln0dibgyzn6nyq3qwrb7i1nd5sl3ccb3sfw3cq6r39w0i6sdh")))

(define-public crate-cad_import-0.3.1 (c (n "cad_import") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "gltf") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18") (d #t) (k 0)) (d (n "path-clean") (r "^1.0") (d #t) (k 2)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)))) (h "0v1gb5rmc2q8z2dj97iiv33667yf34ll4l7nmigh5mmvgp8j38g9")))

