(define-module (crates-io ca bb cabbage) #:use-module (crates-io))

(define-public crate-cabbage-0.1.0 (c (n "cabbage") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1qn7cd0gc3b61vdn5bjr4gcxpqdxcvxq9ypwsr7h8hgpw3kbv036")))

(define-public crate-cabbage-0.1.1 (c (n "cabbage") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0ivj5gcfgsl9yy3jb081f3z52qsp4nil59k1hc433l7wnypxjijx")))

(define-public crate-cabbage-0.1.2 (c (n "cabbage") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0lx02yd47xfs7lp2p3hx8hm8p96g5w2brqapcfcin5abi29f85gc")))

(define-public crate-cabbage-0.1.3 (c (n "cabbage") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1g6slwpvcnnpv3kkk157kxyzsqnq4z7g7l4rbspvph5hd1s66ldv")))

