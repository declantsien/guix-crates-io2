(define-module (crates-io ca mt camtrap_dp) #:use-module (crates-io))

(define-public crate-camtrap_dp-0.1.0 (c (n "camtrap_dp") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0cx5d6cpd5qnp16rj3d7fh3s57hxbdjdh0wkclanp07iv96q96d4")))

