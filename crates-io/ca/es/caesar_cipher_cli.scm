(define-module (crates-io ca es caesar_cipher_cli) #:use-module (crates-io))

(define-public crate-caesar_cipher_cli-0.1.0 (c (n "caesar_cipher_cli") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "1hv8xcgyxn9sbkfd803cq5qnbkc6daj6w2rz5l101ajq9ba0ljhd")))

(define-public crate-caesar_cipher_cli-0.1.1 (c (n "caesar_cipher_cli") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0mdkcml6s48vlf3dm69rv06f7gxfg8c5kj2vfjp46hwqhfggiarw")))

(define-public crate-caesar_cipher_cli-0.1.2 (c (n "caesar_cipher_cli") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0amndbw6rakxy7wcmzbwqrhas2980v1i4y3sgswhsdknfnz09zz2")))

