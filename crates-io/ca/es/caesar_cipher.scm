(define-module (crates-io ca es caesar_cipher) #:use-module (crates-io))

(define-public crate-caesar_cipher-0.1.0 (c (n "caesar_cipher") (v "0.1.0") (h "1dfbfvr5c6rlr0zd1xm8xih3m594vqjck9zqcknw326m250yvc9f")))

(define-public crate-caesar_cipher-0.1.1 (c (n "caesar_cipher") (v "0.1.1") (h "1kxhmi5msh9zv2j2rb66vyspf3aprf551agfx9fncipi6mvsdfy1")))

