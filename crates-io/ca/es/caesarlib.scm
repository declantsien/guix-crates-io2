(define-module (crates-io ca es caesarlib) #:use-module (crates-io))

(define-public crate-caesarlib-0.1.1 (c (n "caesarlib") (v "0.1.1") (h "1r0p795fr182nfixaryzbr7avkbsws3nhrhpk60aix7v8m3cxvcf")))

(define-public crate-caesarlib-0.1.2 (c (n "caesarlib") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1icyyrsk6zgn6ydvcyqjcx8w0xjl1d5nvcn31xhmm0ijry4gshp9")))

(define-public crate-caesarlib-0.1.3 (c (n "caesarlib") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0dk4si4fvd5n87slfbmnci2pn17c250d9sa60qqasiw0b26ikmg3")))

(define-public crate-caesarlib-0.2.0 (c (n "caesarlib") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1dhp3lw2q42pl6v5pss4a54mnwj2g5rrbp723hfygrxdspvmww5m")))

