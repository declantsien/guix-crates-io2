(define-module (crates-io ca es caesar) #:use-module (crates-io))

(define-public crate-caesar-0.1.0 (c (n "caesar") (v "0.1.0") (d (list (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "0r5fg1hrg03fmjwnxndczznhfwx6wc9f07q8bbphgskmwnglpw6k") (f (quote (("tlsv1_2" "openssl/tlsv1_2") ("rfc5114" "openssl/rfc5114") ("default" "tlsv1_2" "rfc5114"))))))

(define-public crate-caesar-0.2.0 (c (n "caesar") (v "0.2.0") (d (list (d (n "openssl") (r "^0.7.6") (d #t) (k 0)))) (h "1mjskf14rpd7m7059q8h0wanvxxzgr2pncx2gdj1hmxy2ww1yj1g") (f (quote (("tlsv1_2" "openssl/tlsv1_2") ("rfc5114" "openssl/rfc5114") ("default" "tlsv1_2" "rfc5114"))))))

