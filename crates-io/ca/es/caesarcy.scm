(define-module (crates-io ca es caesarcy) #:use-module (crates-io))

(define-public crate-caesarcy-0.1.0 (c (n "caesarcy") (v "0.1.0") (h "1ysl6m72rz0qhd6ysii99ji7xavwrbfrhwzqci17sf86f0zafjmi")))

(define-public crate-caesarcy-0.1.1 (c (n "caesarcy") (v "0.1.1") (h "13s4fi8d5sj7h2n5mrg5w5aqi4njjb1lvdmcwlbl5pvw4ga0c4vd")))

