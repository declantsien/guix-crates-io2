(define-module (crates-io ca gr cagra-parser) #:use-module (crates-io))

(define-public crate-cagra-parser-0.1.0 (c (n "cagra-parser") (v "0.1.0") (d (list (d (n "cagra") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0rq4hky2195m1398f6q4a08rszgvkqlb8s9qjgnfzwqnfdc62kmz")))

