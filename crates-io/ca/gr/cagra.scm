(define-module (crates-io ca gr cagra) #:use-module (crates-io))

(define-public crate-cagra-0.1.0 (c (n "cagra") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "procedurals") (r "^0.2") (d #t) (k 0)))) (h "042ia07mgp4mzx88znpzl38i07npmwcp1m3avx6bpwbgmmfxy2bf")))

(define-public crate-cagra-0.2.0 (c (n "cagra") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "cagra-parser") (r "^0.1") (d #t) (k 0)) (d (n "cauchy") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gkbchspb1mwhbvqg67380vxi5ljmp5bgarq922fz9bdwd4x6knv")))

