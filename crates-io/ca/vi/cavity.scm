(define-module (crates-io ca vi cavity) #:use-module (crates-io))

(define-public crate-cavity-1.0.0 (c (n "cavity") (v "1.0.0") (h "14zrr6rnmwzl6xk5ph31vbaz3v7spsadfa55mdm8v9y7fjbpyvlp")))

(define-public crate-cavity-1.1.0 (c (n "cavity") (v "1.1.0") (h "12fxanigayzf7zs1qq7vsp43jg5613493vn3yyp7m7qwr7bmkpv4")))

