(define-module (crates-io ca vi cavint) #:use-module (crates-io))

(define-public crate-cavint-1.0.4 (c (n "cavint") (v "1.0.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1lnj46hq4pifi6a5638knlz9dgdyfv35xdfhi99rbcf2asazz0is")))

