(define-module (crates-io ca br cabrillo) #:use-module (crates-io))

(define-public crate-cabrillo-0.1.0 (c (n "cabrillo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (f (quote ("regexp_macros"))) (d #t) (k 0)))) (h "0r2pixsw2anhv3phz9ajgm2smbf6nl2p46idbzb7mig94pc5zlmq")))

(define-public crate-cabrillo-0.2.0 (c (n "cabrillo") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (f (quote ("regexp_macros"))) (d #t) (k 0)))) (h "0y057pq69a3nwwz6gsx0d73yh0ywvzfr2z4l5agbdi8z4n6kfyhg")))

(define-public crate-cabrillo-0.3.0 (c (n "cabrillo") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0i8160s8k5xr34mivx2dgsplfpvs062xc6pwfv6xb2zvqajs1c20")))

