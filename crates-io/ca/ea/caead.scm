(define-module (crates-io ca ea caead) #:use-module (crates-io))

(define-public crate-caead-0.0.1 (c (n "caead") (v "0.0.1") (d (list (d (n "aead") (r "^0.5") (k 0)) (d (n "digest") (r "^0.10") (k 0)))) (h "0qgh2y0a9iffmfm7bxyx6h64zxrnpj30kqzkldbnyrkfwbbhdrd3") (f (quote (("std" "aead/std" "digest/std") ("alloc" "aead/alloc" "digest/alloc")))) (r "1.68")))

