(define-module (crates-io ca pr capra-core) #:use-module (crates-io))

(define-public crate-capra-core-0.1.0 (c (n "capra-core") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)))) (h "100fg1cfvhrz0i43xy4hbp8a3irzw2cjy0nq188kjjwxndvz0xcv") (f (quote (("use-serde" "serde/derive" "time/serde") ("std" "time/default" "num-traits/default") ("default" "std"))))))

(define-public crate-capra-core-0.2.0 (c (n "capra-core") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)))) (h "1bjqpnfwya5nxc8jj61aiwvc174cbh44mr4jy7ysr3avvs9lppz1") (f (quote (("use-serde" "serde/derive" "time/serde"))))))

(define-public crate-capra-core-0.3.0 (c (n "capra-core") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)))) (h "0k2prsmxmxzsa9l00q1hz79nn90k9db7n2skvf9ly61s507wy6av") (f (quote (("use-serde" "serde/derive" "time/serde"))))))

(define-public crate-capra-core-0.4.0 (c (n "capra-core") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)))) (h "0180advdj75bxjwx8mw18c8j3ljk1f6gvq1qaii6chpm8nvr85i9") (f (quote (("use-serde" "serde/derive" "time/serde"))))))

(define-public crate-capra-core-0.4.1 (c (n "capra-core") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)))) (h "0vc8m5f9g4s1ryl1m7nykvm4pf73lav6ryvl618f1w9z98kqmcn9") (f (quote (("use-serde" "serde/derive" "time/serde"))))))

