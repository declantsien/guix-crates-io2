(define-module (crates-io ca pr capra) #:use-module (crates-io))

(define-public crate-capra-0.1.0 (c (n "capra") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "03y2bl7v6xmcki6zjx7a7bmzm7p3kv4nk1fzj33qhnjnja5vl9wq") (f (quote (("use-serde" "serde/derive" "time/serde"))))))

(define-public crate-capra-0.2.0 (c (n "capra") (v "0.2.0") (d (list (d (n "capra-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "1h1gb10nv20vg5lqz2afccs29c37gqk5ah0ph9yl2ig1ci3kswnb") (f (quote (("use-serde" "capra-core/use-serde" "serde/derive"))))))

(define-public crate-capra-0.2.1 (c (n "capra") (v "0.2.1") (d (list (d (n "capra-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "0arwvqiccs5pfxgzrajsvvms3jxy845m0qx7kj1ngzf03zp9b7y2") (f (quote (("use-serde" "capra-core/use-serde" "serde/derive"))))))

(define-public crate-capra-0.2.2 (c (n "capra") (v "0.2.2") (d (list (d (n "capra-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "1frqhn00v1hpim7zxwgqxnjjkdcpyzqv3jlq8zqh2389fwsp1ia1") (f (quote (("use-serde" "capra-core/use-serde" "serde/derive"))))))

(define-public crate-capra-0.3.1 (c (n "capra") (v "0.3.1") (d (list (d (n "capra-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "0ka9idvjl9wqrjcwy0926nyx9qaf83r0v5dzpbb6az6lb5nx6ghh") (f (quote (("use-serde" "capra-core/use-serde" "serde/derive"))))))

(define-public crate-capra-0.3.2 (c (n "capra") (v "0.3.2") (d (list (d (n "capra-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "12chnv6s7mdb0jlpriv7z2fkzv5amgdd0prmnpy49fhsi9rpx9rb") (f (quote (("use-serde" "capra-core/use-serde" "serde/derive"))))))

