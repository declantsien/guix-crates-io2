(define-module (crates-io ca ld caldera-bindings) #:use-module (crates-io))

(define-public crate-caldera-bindings-0.0.1 (c (n "caldera-bindings") (v "0.0.1") (h "0c1slmr00xbq30wrrhy78882qwb3m4y25cgbs74n0z8qsfs2y9z4")))

(define-public crate-caldera-bindings-0.0.2 (c (n "caldera-bindings") (v "0.0.2") (h "0lfcydhpvb0p101nf37qan07rj30xl3a33k3c1qy188rcdaaw7ig")))

(define-public crate-caldera-bindings-0.1.0 (c (n "caldera-bindings") (v "0.1.0") (h "0ciygaz6p4i1ch74c98nkb5g347c1ngx225iwl4r8hzjw1xkzkx7")))

