(define-module (crates-io ca ld caldyn) #:use-module (crates-io))

(define-public crate-caldyn-0.1.0 (c (n "caldyn") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0n0m728zhln90im87jvvhibj25rhhyah4ma9q2annz3nw4p4m35a")))

(define-public crate-caldyn-0.2.0 (c (n "caldyn") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "0hwbdiwjm5d9ac6yzazfpihg8q7r2pav74ms6cxpsh23dyljyv8z")))

(define-public crate-caldyn-0.3.0 (c (n "caldyn") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "12274m5pvd6mgzcaqbqrwiqn57vj7wm7qijyh7q8zx66jnw37s9f")))

(define-public crate-caldyn-0.4.0 (c (n "caldyn") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "18pkyvbnyra7icf4hsgm8qdad3k8n8bdbgx4sg4c1z0falqj94sw")))

(define-public crate-caldyn-0.4.1 (c (n "caldyn") (v "0.4.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "1fmlshhv1ik1ijcggp61kc1x4fzkl8f4dbysvq2bvx0was5kxsk9")))

(define-public crate-caldyn-0.4.2 (c (n "caldyn") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "1d93fl9jwk0dpb0fv3ilgy4j8bg7f8jd9qplxka8j4va69ws14ay")))

(define-public crate-caldyn-0.4.3 (c (n "caldyn") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^1") (d #t) (k 2)) (d (n "shellexpand") (r "^1") (d #t) (k 2)))) (h "1hx927k25gj8apx1zhx8z3q00kzfvasskr8kllgzpfa0cdbszpvi")))

