(define-module (crates-io ca ld calder) #:use-module (crates-io))

(define-public crate-calder-0.1.0 (c (n "calder") (v "0.1.0") (h "01p8m6c2fz55hib9vbjdbrnspkvry0xzyfn1kvfdk2w7frixa86z")))

(define-public crate-calder-0.1.1 (c (n "calder") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "html-minifier") (r "^3.0.15") (d #t) (k 0)) (d (n "minifier") (r "^0.0.43") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "0kwg0ab19y4hxkmlah7dlfk9dclvnj7xrw69ccvnv2gjsx3cxvxv")))

