(define-module (crates-io ca rt cartesi-rollups-contracts) #:use-module (crates-io))

(define-public crate-cartesi-rollups-contracts-2.0.0-rc.1 (c (n "cartesi-rollups-contracts") (v "2.0.0-rc.1") (d (list (d (n "ethers") (r "^2") (f (quote ("abigen"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ypf9dlqs6ramjivnswqyck0gbk11f43mzsrjmq1f0qs58h1c3wa")))

(define-public crate-cartesi-rollups-contracts-2.0.0-rc.2 (c (n "cartesi-rollups-contracts") (v "2.0.0-rc.2") (d (list (d (n "ethers") (r "^2") (f (quote ("abigen"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ichvb16vfa7mvaqp19g4n54f0w93ywhb8wslc40qvlmwlc1hlsv")))

(define-public crate-cartesi-rollups-contracts-2.0.0-rc.3 (c (n "cartesi-rollups-contracts") (v "2.0.0-rc.3") (d (list (d (n "ethers") (r "^2") (f (quote ("abigen"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "17fzdqy9ggnmlhc00n8bxqwcwx7ka1qqxmqjrsnpk9l4wd29gfpb")))

