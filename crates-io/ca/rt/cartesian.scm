(define-module (crates-io ca rt cartesian) #:use-module (crates-io))

(define-public crate-cartesian-0.1.0 (c (n "cartesian") (v "0.1.0") (h "0ja9yspmwmys3i5sc6753p8hjs7y0iqc3205dbvi2p84m7mxm3qz")))

(define-public crate-cartesian-0.2.0 (c (n "cartesian") (v "0.2.0") (h "1yvl7phdm4iy02ixgqwfvfnrg9y5m5nl806xqnk75nr651hvh9rv")))

(define-public crate-cartesian-0.2.1 (c (n "cartesian") (v "0.2.1") (h "0ps9mmc48r49s3nfj2hn02sw05ihwirym28lzzliz6nbxik5pvf0")))

