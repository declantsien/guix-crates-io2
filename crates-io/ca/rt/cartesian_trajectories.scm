(define-module (crates-io ca rt cartesian_trajectories) #:use-module (crates-io))

(define-public crate-cartesian_trajectories-0.1.0 (c (n "cartesian_trajectories") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "s_curve") (r "^0.1") (d #t) (k 0)))) (h "17s7iz3bqxh4f1c8472dhn4zn92kzrxp01jc00625ij4nfzxc3fb")))

