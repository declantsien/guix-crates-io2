(define-module (crates-io ca rt cartographer-rs) #:use-module (crates-io))

(define-public crate-cartographer-rs-0.1.1 (c (n "cartographer-rs") (v "0.1.1") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "enum-variants-strings") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0g0i5zbyxia978jm3rd6qqa8n3ph4jf2300z91vlvmksc1z0bam8") (s 2) (e (quote (("serde_serialize" "dep:serde"))))))

(define-public crate-cartographer-rs-0.1.2 (c (n "cartographer-rs") (v "0.1.2") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "enum-variants-strings") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0pn8yz8ja89swk7l7bygwzs98sjzg3cjqjf5axna1sgqprvc41s1") (s 2) (e (quote (("serde_serialize" "dep:serde"))))))

(define-public crate-cartographer-rs-0.1.3 (c (n "cartographer-rs") (v "0.1.3") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "enum-variants-strings") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0imbpajy4k7ca60mpf70ghi0zwl4dyi73y54mv11adzp0fdxybif") (s 2) (e (quote (("serde_serialize" "dep:serde"))))))

