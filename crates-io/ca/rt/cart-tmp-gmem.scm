(define-module (crates-io ca rt cart-tmp-gmem) #:use-module (crates-io))

(define-public crate-cart-tmp-gmem-0.1.0 (c (n "cart-tmp-gmem") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "hibitset") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0xss6s7fr1kvs820cc77fvzy2ip2nxcgl3b2gg593i5lzgj7922h")))

