(define-module (crates-io ca rt cart-tmp-gdesc) #:use-module (crates-io))

(define-public crate-cart-tmp-gdesc-0.1.0 (c (n "cart-tmp-gdesc") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hal") (r "^0.5") (d #t) (k 0) (p "gfx-hal")) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01g0apvy20npv9aljvshkifvx756p131zc2345nz5abl3a7wkpi4")))

