(define-module (crates-io ca rt cartesian_array_product) #:use-module (crates-io))

(define-public crate-cartesian_array_product-1.0.0 (c (n "cartesian_array_product") (v "1.0.0") (h "1czrn6891mdjibnf2f04rl2akkb05zvy9gnbgnjq2d1xg2n86qp7")))

(define-public crate-cartesian_array_product-1.0.1 (c (n "cartesian_array_product") (v "1.0.1") (h "164db67ryryqrlsznqd88lmzw7npk92l3qqs46fd3aggyd325v28")))

(define-public crate-cartesian_array_product-1.1.0 (c (n "cartesian_array_product") (v "1.1.0") (d (list (d (n "const-anonymous-functions") (r "^1.0.0") (d #t) (k 0)))) (h "1a5ybl0z169sfn95n7hpk9l3yp4myn2h6jf9x5plfw6g77y55bly")))

(define-public crate-cartesian_array_product-1.1.1 (c (n "cartesian_array_product") (v "1.1.1") (d (list (d (n "const-anonymous-functions") (r "^1.1.0") (d #t) (k 0)))) (h "0r6vagqiy5q9z44nbd1qqym943mxmvjmwavcwzj78yj6ix3pfykv")))

