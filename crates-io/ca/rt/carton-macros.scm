(define-module (crates-io ca rt carton-macros) #:use-module (crates-io))

(define-public crate-carton-macros-0.0.0 (c (n "carton-macros") (v "0.0.0") (h "0n8ga6hvvd2k8bgiz3wsl5av94cyclfh68g839dm4j8v8mjk997q")))

(define-public crate-carton-macros-0.0.1 (c (n "carton-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zp5ja2cky56aq6p13ia06453xx6p5ph4bk8dsskbf4xlv5wbw4q")))

