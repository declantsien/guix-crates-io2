(define-module (crates-io ca rt cart-cache) #:use-module (crates-io))

(define-public crate-cart-cache-0.1.1 (c (n "cart-cache") (v "0.1.1") (d (list (d (n "rand") (r "~0.3.14") (d #t) (k 2)) (d (n "slab") (r "~0.2.0") (d #t) (k 0)))) (h "0dj01d1mzcakb7ga00j1jds1fxy708afblk8nsmgm5cqz83mqaid")))

(define-public crate-cart-cache-0.1.2 (c (n "cart-cache") (v "0.1.2") (d (list (d (n "rand") (r "~0.3.15") (d #t) (k 2)) (d (n "slab") (r "~0.3.0") (d #t) (k 0)))) (h "1a4ifx25rgpkdipp8x2j4slpix6lda3ml3isjavwcz3x52aidnb3")))

(define-public crate-cart-cache-0.1.4 (c (n "cart-cache") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1gsyr94c9nwjc2s7v9ck1vq9mmzqnc5dbs2xsz7lvakmj3m0jk7v")))

(define-public crate-cart-cache-0.1.5 (c (n "cart-cache") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1g2xdbhr9jsim5pfcy8wfwf7kv4l50p02ygrvgs312gavzp322kf")))

(define-public crate-cart-cache-0.1.6 (c (n "cart-cache") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0m1ipgis33rr2fa99n77kgkacrhribwm9gykcc024bwcfw70vfh7")))

