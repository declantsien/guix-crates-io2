(define-module (crates-io ca rt cart-tmp-nga) #:use-module (crates-io))

(define-public crate-cart-tmp-nga-0.1.0 (c (n "cart-tmp-nga") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "glsl") (r "^4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "spirv") (r "^1.4.2") (d #t) (k 0) (p "spirv_headers")))) (h "0p8bf6jiwyh9jmx9p0gj5mv7drga1r83kqfp267hlzvn7xmnfw61") (f (quote (("glsl_preprocessor" "glsl"))))))

