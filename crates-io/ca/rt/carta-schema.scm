(define-module (crates-io ca rt carta-schema) #:use-module (crates-io))

(define-public crate-carta-schema-1.0.0 (c (n "carta-schema") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dlwh7g80b6mrgbb1kwgm5zqyg8ggm7d77hvnbvqmv4c1bk70hr9")))

(define-public crate-carta-schema-1.0.1 (c (n "carta-schema") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "196ynr4gdmaxljgjz2jd3v2akx4rp120084bj1j8a1fsjvfad6gr")))

