(define-module (crates-io ca rt carton_shell) #:use-module (crates-io))

(define-public crate-carton_shell-0.1.0 (c (n "carton_shell") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.53.1, <0.54.0") (d #t) (k 1)))) (h "13g9i218smxg3afirz8fycb8rpsic1p2irl46xxgqahzlgnxlzra") (f (quote (("x11") ("windows") ("wayland") ("macos") ("default" "macos" "windows" "x11"))))))

