(define-module (crates-io ca tc catcat) #:use-module (crates-io))

(define-public crate-catcat-0.1.0 (c (n "catcat") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.15") (d #t) (k 0)))) (h "0w3ana8c3bsqvpn946cx32nvv6k5c5j5ry1za4z6lw4h99ypaab4") (y #t)))

(define-public crate-catcat-0.1.1 (c (n "catcat") (v "0.1.1") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.15") (d #t) (k 0)))) (h "0xxhn2ibgf6f97f7p4pqz3n51p9h79iyhbvcp8gbchbca7lx6zz2")))

(define-public crate-catcat-0.2.0 (c (n "catcat") (v "0.2.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)))) (h "0fniq9g849c5cxcdxjijmb5xddf5ghjbii066mynsvvnfrhk17qm")))

(define-public crate-catcat-0.2.1 (c (n "catcat") (v "0.2.1") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)))) (h "0vl6mx3zvgsw6wh7ngn09b4b7bk52x6zj12aq5901iq1346y445g")))

