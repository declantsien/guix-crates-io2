(define-module (crates-io ca tc catconf) #:use-module (crates-io))

(define-public crate-catconf-0.1.0 (c (n "catconf") (v "0.1.0") (h "0nilhpckh5v0y5ihlx8z7plxj79za70mrylvmf6m6jl0rxjfdcbv")))

(define-public crate-catconf-0.1.1 (c (n "catconf") (v "0.1.1") (h "0q8hlhzrmnnabz7z4q3h8wg5wm6agk02lav29jnri119bfqcylz9")))

(define-public crate-catconf-0.1.2 (c (n "catconf") (v "0.1.2") (h "1b8i6kaskgy2hrc87vw108mk2v7ns0qmk4yn25l0lgm3c17qijzp")))

