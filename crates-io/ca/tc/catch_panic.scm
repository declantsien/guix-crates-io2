(define-module (crates-io ca tc catch_panic) #:use-module (crates-io))

(define-public crate-catch_panic-1.0.0 (c (n "catch_panic") (v "1.0.0") (d (list (d (n "catch_panic_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "jni") (r "^0") (d #t) (k 0)) (d (n "jni") (r "^0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0sbcq4h91qcyxlrdxf9aamzvp4y1h492icn7pmq97i96z5z8h45y") (f (quote (("internal-doctests"))))))

