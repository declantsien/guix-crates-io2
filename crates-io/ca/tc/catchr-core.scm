(define-module (crates-io ca tc catchr-core) #:use-module (crates-io))

(define-public crate-catchr-core-0.1.0 (c (n "catchr-core") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w7nsgf8fbghmaqp9vwwhsh0yx43d7zb6nyxhgha8wg5gy9jnvmh")))

(define-public crate-catchr-core-0.1.1 (c (n "catchr-core") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cicv21b881pa8xaav0bx9410r6v4hgkpiq49vb26p77wq8i460r")))

(define-public crate-catchr-core-0.1.2 (c (n "catchr-core") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "12vwrbckg549wxw0w3sajnjqcfybygq51l4j2rqf5nmph33iqm7m")))

(define-public crate-catchr-core-0.1.3 (c (n "catchr-core") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "097p0xbx0rv1cw3mcjsdj6s5ism520xbjz9xnbjqhw09vf6pis5i")))

(define-public crate-catchr-core-0.2.0 (c (n "catchr-core") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1vf6dzgzv0mcvmfmi31frbxy6hfm9hadkrikpbzrl0ivv942qqwa")))

