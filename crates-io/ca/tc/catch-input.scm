(define-module (crates-io ca tc catch-input) #:use-module (crates-io))

(define-public crate-catch-input-1.0.0 (c (n "catch-input") (v "1.0.0") (h "16yhxrjdfj506cg8g3c1nnmd4qd727263j6fizm9n8fks8bjnirl")))

(define-public crate-catch-input-1.0.1 (c (n "catch-input") (v "1.0.1") (h "1a35sv5gspjm9788n5dwf5id8pg6ad1i444r4zfzg8n5lm7xm488")))

(define-public crate-catch-input-1.1.1 (c (n "catch-input") (v "1.1.1") (h "07zcq11bcva7xgcfh2vlsazz84i0c7nnqqwljjydfq5rlsqppxgy")))

(define-public crate-catch-input-1.1.2 (c (n "catch-input") (v "1.1.2") (h "0wbpsdplccbhqmbqjpd0ybax35b63w1m6il7nm0bspr5r9mkfd1k")))

