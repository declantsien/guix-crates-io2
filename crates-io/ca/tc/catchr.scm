(define-module (crates-io ca tc catchr) #:use-module (crates-io))

(define-public crate-catchr-0.1.0 (c (n "catchr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yy9xyk6gjfwwyvffqvxnkgg6a5ccckipm9i7qs723yb9lh6f563")))

(define-public crate-catchr-0.2.0 (c (n "catchr") (v "0.2.0") (d (list (d (n "catchr-core") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iny88rwqw9hqpaxw3z626h4x81j03dwgvr5jb135bvad0pf1hkl")))

(define-public crate-catchr-0.2.1 (c (n "catchr") (v "0.2.1") (d (list (d (n "catchr-core") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14wh0kj7h82vxq1mj65yhfpg2jmymjahfdgydn704642xsahh047")))

(define-public crate-catchr-0.2.2 (c (n "catchr") (v "0.2.2") (d (list (d (n "catchr-core") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m06brham1mrr2bsnh7arc9c4sxjqgnmaqg67fscsca3gkz7822d")))

(define-public crate-catchr-0.2.3 (c (n "catchr") (v "0.2.3") (d (list (d (n "catchr-core") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cgq5wnc2bw3qvmx8sm24mhpx13km6rs8m4z6gkzd1bl1i9pcqzp")))

(define-public crate-catchr-0.3.0 (c (n "catchr") (v "0.3.0") (d (list (d (n "catchr-core") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bwb39y3snagd1mdp7n5q4i3inpnqh6gqgcy8b8cf3ms5dm89rq0")))

