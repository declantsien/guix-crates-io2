(define-module (crates-io ca pl caplog) #:use-module (crates-io))

(define-public crate-caplog-0.1.0 (c (n "caplog") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0fyc002b6xbn25w8q6i03mvlgsr273cqb6glmnn86svmd8cvzg6p") (y #t)))

(define-public crate-caplog-0.2.0 (c (n "caplog") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "04pcl5dqc67jwzigzblf2ik05chdr9pvgwahl1i6iaj1myzqw5s1")))

