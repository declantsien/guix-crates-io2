(define-module (crates-io ca ts catsay-ag) #:use-module (crates-io))

(define-public crate-catsay-ag-0.1.0 (c (n "catsay-ag") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "197arxgqlyrg7lzspp3jlxiwqb8pds7h0ypnd1hf7vcs5c34lj63")))

