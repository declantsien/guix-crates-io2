(define-module (crates-io ca ts catsay) #:use-module (crates-io))

(define-public crate-catsay-0.1.0 (c (n "catsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0bil19w4i18s424yb7vgbgwkqzdb13jrv3n489m4hxyyzh2938k0")))

