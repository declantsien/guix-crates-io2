(define-module (crates-io ca ts catsay-bpv) #:use-module (crates-io))

(define-public crate-catsay-bpv-0.1.0 (c (n "catsay-bpv") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1v030p7g0azm2pcv47jn2v7k90y2ixiq50v4l64iv7vgqsf72m3s")))

(define-public crate-catsay-bpv-0.2.0 (c (n "catsay-bpv") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "0rc1dl48zqmy9y71l20aafgp9acdmwd77r4dbxig9bm67x3aw43l")))

