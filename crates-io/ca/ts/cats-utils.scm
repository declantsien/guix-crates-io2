(define-module (crates-io ca ts cats-utils) #:use-module (crates-io))

(define-public crate-cats-utils-0.1.0 (c (n "cats-utils") (v "0.1.0") (h "0yhzk0i7pds9fzpkxd91q15lksdmz7fznjb4p5a62w4ipiikq0cr") (y #t)))

(define-public crate-cats-utils-0.1.1 (c (n "cats-utils") (v "0.1.1") (h "15s9zcwa4ihjpss1mlnjgkr6508b1dmxpdzq6h37gv5m1az40bgy")))

