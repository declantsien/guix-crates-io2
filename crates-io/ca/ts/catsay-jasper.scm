(define-module (crates-io ca ts catsay-jasper) #:use-module (crates-io))

(define-public crate-catsay-jasper-0.1.0 (c (n "catsay-jasper") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "05vn5jsgj582aqybkdlxhwk7s43c6vd8wlxfd3395nvxrs0kz1nk")))

