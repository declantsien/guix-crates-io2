(define-module (crates-io ca ts catsay-maicmi) #:use-module (crates-io))

(define-public crate-catsay-maicmi-0.1.0 (c (n "catsay-maicmi") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "predicates") (r "^3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fb7k43c4yr8v3q6swaxh136d7pylp5wvva7zsh8gnb8zav980sl")))

