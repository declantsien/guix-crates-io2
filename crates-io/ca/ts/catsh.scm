(define-module (crates-io ca ts catsh) #:use-module (crates-io))

(define-public crate-catsh-0.1.0 (c (n "catsh") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)))) (h "0xzhl8530vsd9yik6cw8fxzz0p9lnsdrz7zr2df65j77gfc3bsrn") (y #t)))

(define-public crate-catsh-0.0.4-dev (c (n "catsh") (v "0.0.4-dev") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "0q39mwxv7gvx9dydsz60dc1ycsb35pin52r9hx4vp8rs5wbq7a81")))

