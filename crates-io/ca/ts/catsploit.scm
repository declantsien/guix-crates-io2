(define-module (crates-io ca ts catsploit) #:use-module (crates-io))

(define-public crate-catsploit-0.1.0 (c (n "catsploit") (v "0.1.0") (d (list (d (n "catsploit_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "1a7vp4llbncm3m2q9wcllqsmkcvczyy0z4ahmz1im8c2w98sh3bb")))

(define-public crate-catsploit-0.1.2 (c (n "catsploit") (v "0.1.2") (d (list (d (n "catsploit_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "1vcibxjp9f5aspziamq31s8bq8001q7gxgr9jx14xh5wjwk4pra9")))

