(define-module (crates-io ca ts catsploit_lib) #:use-module (crates-io))

(define-public crate-catsploit_lib-0.1.0 (c (n "catsploit_lib") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)))) (h "08q16dw9sdg7aw3z06lvsh5w38jpb130r05mwlhnz156lan0avwc")))

