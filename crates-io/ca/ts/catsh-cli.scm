(define-module (crates-io ca ts catsh-cli) #:use-module (crates-io))

(define-public crate-catsh-cli-0.2.2-alpha.0 (c (n "catsh-cli") (v "0.2.2-alpha.0") (d (list (d (n "catsh-core") (r "^0.2.2-alpha.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "145wpmk2xr3nvwr6vjwl1w0y2p4xgscvx103lds2xsf2l8fac8iz") (y #t)))

(define-public crate-catsh-cli-0.1.0-alpha.1 (c (n "catsh-cli") (v "0.1.0-alpha.1") (d (list (d (n "catsh-core") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "13ynj9lsx6cf30ixp7zwyz4mvhvc5xp5l0nr515aiqffyyxss3sk") (y #t)))

(define-public crate-catsh-cli-0.1.0-beta.0 (c (n "catsh-cli") (v "0.1.0-beta.0") (d (list (d (n "catsh-core") (r "^0.1.0-beta.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0zv1asv5dja473v3gykykl8054336r51nvm495y93qgsb3kk1lip") (y #t)))

(define-public crate-catsh-cli-0.2.3-beta.0 (c (n "catsh-cli") (v "0.2.3-beta.0") (d (list (d (n "catsh-core") (r "^0.2.3-beta.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "10fjymdcnn3l2lvb5jpw7bcdg4715w5mg7wjq49fkcwimq510q41") (y #t)))

(define-public crate-catsh-cli-0.2.4-beta.0 (c (n "catsh-cli") (v "0.2.4-beta.0") (d (list (d (n "catsh-core") (r "^0.2.4-beta.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "13r05vdssng6cdc3jvz81wqcyjw7wmbx4kv0xy61lsw968sk0q2i") (y #t)))

(define-public crate-catsh-cli-0.2.9-beta.0 (c (n "catsh-cli") (v "0.2.9-beta.0") (d (list (d (n "catsh-core") (r "^0.2.9-beta.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0jhindmalgj5s7jmcvfrrmbnh2bmbcd71p6lckpp3r4m5h0qnp0r") (y #t)))

(define-public crate-catsh-cli-0.1.0 (c (n "catsh-cli") (v "0.1.0") (d (list (d (n "catsh-core") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "00r0dfl5bfvk6ca70gvjv6df447khwb2vf2ygnhwghklz0y6gbrj")))

(define-public crate-catsh-cli-0.1.1 (c (n "catsh-cli") (v "0.1.1") (d (list (d (n "catsh-core") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "07r0p7arglv5yg6v5800j30fbv1vplsmlaad75i49hqf308iqd4b")))

