(define-module (crates-io ca ts catsay-1020) #:use-module (crates-io))

(define-public crate-catsay-1020-0.1.0 (c (n "catsay-1020") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1asnm7jd17pp6z72p1xqnn26rpplqm6j5ngj1ibr7pq3pipjvchv")))

