(define-module (crates-io ca ts catsay_1estart) #:use-module (crates-io))

(define-public crate-catsay_1estart-0.1.0 (c (n "catsay_1estart") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1kg7axbgq74g67pjwzi6v0ky22885kx0xqb4j1nx21bc3d8zjnkm")))

