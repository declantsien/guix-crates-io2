(define-module (crates-io ca ts catsh-core) #:use-module (crates-io))

(define-public crate-catsh-core-0.2.2-alpha.0 (c (n "catsh-core") (v "0.2.2-alpha.0") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0l765d1k1m1xzsb4nvzxq82kr3k18872xa5jmczyyjl5q6g4nmx0") (y #t)))

(define-public crate-catsh-core-0.1.0-alpha.1 (c (n "catsh-core") (v "0.1.0-alpha.1") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1lj8jn0v7mnvzwi2ixm5bkr9mkfni8hs98avgyv53m2y6nkp6sfh") (y #t)))

(define-public crate-catsh-core-0.1.0-beta.0 (c (n "catsh-core") (v "0.1.0-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1xk96y82d1xiisagixkms8ir95342psr05cc3vm9w0pxrsphghbj") (y #t)))

(define-public crate-catsh-core-0.2.3-beta.0 (c (n "catsh-core") (v "0.2.3-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hh2rzpb8g2f6qw9213gbvdg72ir1amxxsjzic583xk63ghpj1b4") (y #t)))

(define-public crate-catsh-core-0.2.4-beta.0 (c (n "catsh-core") (v "0.2.4-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0w4h883dfxp3lqpabllnna8rdj9w5wmzjiix35x57bfx89n9sya0") (y #t)))

(define-public crate-catsh-core-0.2.7-beta.0 (c (n "catsh-core") (v "0.2.7-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10d25jfyq29digw6f6npzkw93wwv6a7r0vfw6nlwlx7dcdxi8arr") (y #t)))

(define-public crate-catsh-core-0.2.9-beta.0 (c (n "catsh-core") (v "0.2.9-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10svxsdrakmgcmpwzxw9mfhc55m2q18p6zg9bhfid09a1gzxlfx2") (y #t)))

(define-public crate-catsh-core-0.1.0 (c (n "catsh-core") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1p8ciajbbjdi3azmj9qzhlswshkk4s5w3i7mizx1b0l475gg9crv") (y #t)))

(define-public crate-catsh-core-0.1.1 (c (n "catsh-core") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1p9qx22xd9crm04l03scp1ssvrl2ybbwrknjmbl5kab3slkw7kdx")))

