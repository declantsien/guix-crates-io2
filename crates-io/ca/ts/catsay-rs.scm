(define-module (crates-io ca ts catsay-rs) #:use-module (crates-io))

(define-public crate-catsay-rs-1.0.1 (c (n "catsay-rs") (v "1.0.1") (h "1vzrzcr37km22jxbby7da3294gjrgfbjgzq59aplpj3036was6y9") (y #t)))

(define-public crate-catsay-rs-1.0.0 (c (n "catsay-rs") (v "1.0.0") (h "0xw112kgswpyry1l5xamqdqnjgwqz78h21pvsvk1zn3k5zqms9yl")))

(define-public crate-catsay-rs-1.1.0 (c (n "catsay-rs") (v "1.1.0") (h "19w4k9j9mpi9z4pyzmbq2mjajb7wgjql2sf6ck1n172l8rxprml3")))

(define-public crate-catsay-rs-1.1.1 (c (n "catsay-rs") (v "1.1.1") (h "1banljndwkhhbarlci9q1z56i34sybr1jd5b7yppas2il9kk1vvg")))

