(define-module (crates-io ca it caith) #:use-module (crates-io))

(define-public crate-caith-0.1.0 (c (n "caith") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rmghw3wdkrijfcps9lg995k7v0skya33wb345bsvs034np80n1m")))

(define-public crate-caith-0.1.1 (c (n "caith") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1r0apd9nk89cnqicivjfaa00wchk2kssxk0x1akl7qsm3igq4hny")))

(define-public crate-caith-0.1.2 (c (n "caith") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0k56b9xrymnb41m5raqdmm6i56d73whbk66ga28c0zq9xvrhhna4")))

(define-public crate-caith-0.1.3 (c (n "caith") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1l7lxjhgayzgxsf6mmc5dssibbdn2jix3sqv0rpazw7syiz1rrwi")))

(define-public crate-caith-0.1.4 (c (n "caith") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02rv75w6v0x9i2s46nsn5sh1m92mzgpli8h75ss65fm4hwpvlmg6")))

(define-public crate-caith-0.2.0 (c (n "caith") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "15sn61pv3vjdrmlg3hb0j6rnryj5niyz8yx5ry14pl6yxzff0zls")))

(define-public crate-caith-0.3.0 (c (n "caith") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "12cpvw3w652x1v6bbi751wgk86hlmpq7dhi8wi4h1iap5m2r1fps")))

(define-public crate-caith-0.4.0 (c (n "caith") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1h3jpshi7fqqrdhjjzy6yncic7sfpiy55cyjq7kizh22w0l4gb8s")))

(define-public crate-caith-0.5.0 (c (n "caith") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1yp07irzr26hgwph2fbrn3jx3j884jkrinmyk2c83g5cxm401m5x")))

(define-public crate-caith-1.0.0 (c (n "caith") (v "1.0.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nb4g7d0w7zddn7bal4qyvyrgx1f8zr1hflbhgwaim2v88mfc2bz")))

(define-public crate-caith-1.0.1 (c (n "caith") (v "1.0.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0l3224mcyv65jwngs8m8rmv042cwl5ikrsg3mc96brl412kkzvki")))

(define-public crate-caith-2.0.0 (c (n "caith") (v "2.0.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00ij627gdq6swp0f491gz1d7irp0r48mql0p1xx75nv0nhqlq5s7")))

(define-public crate-caith-2.0.1 (c (n "caith") (v "2.0.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ghxgmpkgl37r7ms6hsz81gvx8jw84wcci5w2gf2i12hspjyn45z")))

(define-public crate-caith-2.1.0 (c (n "caith") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0fh4sjy358p12xfl02fbyysv3g63xqvvi1dlshv6bq0w3asy1wmr")))

(define-public crate-caith-2.2.0 (c (n "caith") (v "2.2.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0drxq1qi65a6zay48ikibk5487chsgyyniv6j9wsh4ka7ydvd3d6")))

(define-public crate-caith-2.2.1 (c (n "caith") (v "2.2.1") (d (list (d (n "pest") (r ">=2.1.3, <3.0.0") (d #t) (k 0)) (d (n "pest_derive") (r ">=2.1.0, <3.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)))) (h "18hxzc33ircipxcff66b2v6ms9k0kp5l4ijfmd4c2dhcqqxp26bg")))

(define-public crate-caith-3.0.0 (c (n "caith") (v "3.0.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04a0z5qpcrcmdjj75wfzd47dj2fxm749g0lvjivv2rkwz8ycixmf")))

(define-public crate-caith-3.0.1 (c (n "caith") (v "3.0.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "13xj0jd5a5vl7xi4pwl8c62lj0j4z32j06widp20vgmsr6a08a68") (y #t)))

(define-public crate-caith-3.0.2 (c (n "caith") (v "3.0.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hkn129simswypcw6qf9w7y2qyjb1qslrfszjhpjw7h1jw8hvzzs") (y #t)))

(define-public crate-caith-3.0.3 (c (n "caith") (v "3.0.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "140rcaqjqwwnq8d3jvirqcbymh5mndrxc1qmdgmc68s9wwlnajad")))

(define-public crate-caith-3.1.0 (c (n "caith") (v "3.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "0b4m8g8bnvrvz0r9y74qm8ihbkhcdgp81k1p41z0s67qh8ssv0rs")))

(define-public crate-caith-4.0.0 (c (n "caith") (v "4.0.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 2)))) (h "1f15c9bbdcfxzm7nbn3d7ih4wm807i9j4apv9xq9n43bbnqf9yxg") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.1.0 (c (n "caith") (v "4.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 2)))) (h "076r9623phqlczy4xwd7zgxag0pgkivxbsid8ccclb3mjbd5z1ha") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.1.1 (c (n "caith") (v "4.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 2)))) (h "1x5x1scf1gm1pq0a9h27krxiywkb4k6jbxf2lqx4hplignpshci6") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.1.2 (c (n "caith") (v "4.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 2)))) (h "004clvivsbymm08pbh7va3z6vlx72qmaq0wipkxw33w5jqaj7nhj") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.2.0 (c (n "caith") (v "4.2.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)))) (h "1hh5vyvjvs48gn0glj05wzyr8pw5cw0f7sq9hyb1kyxrfyn7y314") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.2.1 (c (n "caith") (v "4.2.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)))) (h "0w99427600jydkfi5isj4shqsw960ykz4g6n89fndgf1n4v50hg1") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.2.2 (c (n "caith") (v "4.2.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)))) (h "1qn3c40l6hp1v65vak2s4kld9l52firyda6x2zzif24z4gp4jpkj") (f (quote (("ova") ("default") ("cde") ("cards"))))))

(define-public crate-caith-4.2.3 (c (n "caith") (v "4.2.3") (d (list (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 2)))) (h "1gx68sgxsscgkavjrxzbsw8q86rba88bx6bk8f4rwdg6m4kfj3c6") (f (quote (("ova") ("default") ("cde") ("cards"))))))

