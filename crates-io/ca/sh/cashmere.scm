(define-module (crates-io ca sh cashmere) #:use-module (crates-io))

(define-public crate-cashmere-0.0.1 (c (n "cashmere") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 0)))) (h "02yxz3qygqaqsgb5a56aapv18jakl12vz5jq4d17jm98dx4rfrvx") (f (quote (("full_validation"))))))

