(define-module (crates-io ca sh cashaddr) #:use-module (crates-io))

(define-public crate-cashaddr-0.1.0 (c (n "cashaddr") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "15zpa7qramrdhr37jx744685vnm8vi9c4i16i05a7i85jij17mi8")))

(define-public crate-cashaddr-0.1.1 (c (n "cashaddr") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0a4g2fgqyz407jpv425sqm9xcj30wmqby6ir82jvn08phy3b2zsb")))

(define-public crate-cashaddr-0.1.2 (c (n "cashaddr") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "08bsd6zfbaqagg6w1sml5cm3qpz6pbpqzqhdhn7ky6hd2qzv6bs4")))

(define-public crate-cashaddr-0.1.3 (c (n "cashaddr") (v "0.1.3") (d (list (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1vnca9jad7mlm2zxk72923lr5w5kfkps5w9phqhxcd4jqzsvja9w")))

(define-public crate-cashaddr-0.2.0 (c (n "cashaddr") (v "0.2.0") (d (list (d (n "bs58") (r "^0.4.0") (f (quote ("check"))) (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "0xlngm12b3cx8n2gnyzifzk07wks1xqkq1xib2x7py5pqwdmkd6h") (f (quote (("convert" "bs58"))))))

