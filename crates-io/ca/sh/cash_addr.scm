(define-module (crates-io ca sh cash_addr) #:use-module (crates-io))

(define-public crate-cash_addr-0.1.0 (c (n "cash_addr") (v "0.1.0") (d (list (d (n "bech32") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1gk99gj4jvs9rrn4i02npvvsjb9g5ddrmy2rpxdmvw24d8jcg7bs")))

