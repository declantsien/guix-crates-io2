(define-module (crates-io ca sh cashaccount-sys) #:use-module (crates-io))

(define-public crate-cashaccount-sys-0.1.0 (c (n "cashaccount-sys") (v "0.1.0") (d (list (d (n "bindgen") (r ">= 0.47.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hragk1n7n62d9ff7i33fvg4x760611nd7fhr3w9ckh5zx3jngz3")))

(define-public crate-cashaccount-sys-0.1.1 (c (n "cashaccount-sys") (v "0.1.1") (d (list (d (n "bindgen") (r ">= 0.47.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01y8jdv24qcpsq4b5aahl116pyf9c4xca4ivddxgyhnpb5zbq1km")))

