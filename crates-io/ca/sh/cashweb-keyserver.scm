(define-module (crates-io ca sh cashweb-keyserver) #:use-module (crates-io))

(define-public crate-cashweb-keyserver-0.1.0-alpha.0 (c (n "cashweb-keyserver") (v "0.1.0-alpha.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "14hda2hddngbdnspisp211mjwj7l4ww2fr79m2i14yqy47viqzny")))

(define-public crate-cashweb-keyserver-0.1.0-alpha.1 (c (n "cashweb-keyserver") (v "0.1.0-alpha.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0b53qyn2z6z0lh2dp7ip0dxvm5d8wrw4f1zmvydz7nb209radqhn")))

(define-public crate-cashweb-keyserver-0.1.0-alpha.2 (c (n "cashweb-keyserver") (v "0.1.0-alpha.2") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wkpd0ldlq0a8clfjws3vbn1h7zw7dv9b4j4h0kdgnrhg913j6r1")))

(define-public crate-cashweb-keyserver-0.1.0-alpha.3 (c (n "cashweb-keyserver") (v "0.1.0-alpha.3") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "060x3svmsqy2ycn9y01fric6yjggxjwwghgdrpvaw3mqnxik3myj")))

(define-public crate-cashweb-keyserver-0.1.0-alpha.4 (c (n "cashweb-keyserver") (v "0.1.0-alpha.4") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rgpg4hdyavg5160wljihyjrbrs7yhmvpyrayvbgbf4ikx573zap")))

