(define-module (crates-io ca sh cashaccount) #:use-module (crates-io))

(define-public crate-cashaccount-0.1.0 (c (n "cashaccount") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.21") (d #t) (k 0)) (d (n "c_fixed_string") (r "^0.2.0") (d #t) (k 0)) (d (n "cashaccount-sys") (r ">= 0.1.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "18ywrahq4l6z3ygmfvvhb5h5l7k2r27477lhxqz3mpbnvnvn8cd8")))

