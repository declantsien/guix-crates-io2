(define-module (crates-io ca sh cashu) #:use-module (crates-io))

(define-public crate-cashu-0.0.1 (c (n "cashu") (v "0.0.1") (h "0xgnjwf4angxlfvp73ff5iqkdvdcjgd4gxyiwzm2gz4qjs1gxs98")))

(define-public crate-cashu-0.0.2 (c (n "cashu") (v "0.0.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.0") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1rs01wlh2hmxaib5mpr9hckv54h1z7ffdl5j2rgjkfqr58c0a4ls")))

