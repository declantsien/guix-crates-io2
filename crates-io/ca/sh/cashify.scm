(define-module (crates-io ca sh cashify) #:use-module (crates-io))

(define-public crate-cashify-0.1.0 (c (n "cashify") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "070nni62n283ik2fb6x1gwn4px732206ycycf8kf7h5i53v48d2c")))

(define-public crate-cashify-0.1.1 (c (n "cashify") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0li0gq389h8k4b2j87lnhwdn1d7163ixg79zvjz9k8ymh2kc6xjn")))

