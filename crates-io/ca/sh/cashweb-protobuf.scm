(define-module (crates-io ca sh cashweb-protobuf) #:use-module (crates-io))

(define-public crate-cashweb-protobuf-0.1.0-alpha.0 (c (n "cashweb-protobuf") (v "0.1.0-alpha.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "158axaiy0j6mnh81l6filn8vvnd7wy0lacjmf2wp786qwz6gf97v") (y #t)))

