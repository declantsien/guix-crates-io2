(define-module (crates-io ca sh cashweb-secp256k1-sys) #:use-module (crates-io))

(define-public crate-cashweb-secp256k1-sys-0.1.2 (c (n "cashweb-secp256k1-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.56") (d #t) (k 1)))) (h "15yszzy2gc6nrv3i7f4lav5mqlljgyqyhlv2s3hjg8h765cxaji2") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_1_1")))

(define-public crate-cashweb-secp256k1-sys-0.3.1 (c (n "cashweb-secp256k1-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0030a7rvvb9gjpzdnx6fsb4nmrc0aw94gv12zxkp57ccn4dl3fl2") (f (quote (("std") ("recovery") ("lowmemory") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_3_1")))

