(define-module (crates-io ca sh cashweb-auth-wrapper) #:use-module (crates-io))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.0 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.3") (d #t) (k 0) (p "cashweb-secp256k1")))) (h "18y61pjg20z0in7drp83a6780zr2w71lij7zdhxvmjv4h2nz3cfz")))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.1 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.3") (d #t) (k 0) (p "cashweb-secp256k1")) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1vhd4g9ibwlq7znlgn5p0lga9psjp1cpix13mfv9bw6rnz2lbzgw")))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.2 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.2") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.3") (d #t) (k 0) (p "cashweb-secp256k1")) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0d95326clafg65smm07f91zh5paskxz2yy10s9iijk6jly1b1pmg")))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.3 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.3") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.3") (d #t) (k 0) (p "cashweb-secp256k1")) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "01676wm4gcia11cdmp1xiqbgnc29zqyj2nhz8q5kx2rcnq6k0yqf")))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.4 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.4") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.3") (d #t) (k 0) (p "cashweb-secp256k1")) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1iym9p5z381fhz9w306xi76lqi6k151m6m9w0d17p2grx07qs7xn")))

(define-public crate-cashweb-auth-wrapper-0.1.0-alpha.5 (c (n "cashweb-auth-wrapper") (v "0.1.0-alpha.5") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19") (d #t) (k 0) (p "cashweb-secp256k1")) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qyjr2pwsj3yfidx75ym5km91gbiz086b1h6xsm16i31pfx2l5cl")))

