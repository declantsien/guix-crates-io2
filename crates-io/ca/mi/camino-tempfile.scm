(define-module (crates-io ca mi camino-tempfile) #:use-module (crates-io))

(define-public crate-camino-tempfile-1.0.2 (c (n "camino-tempfile") (v "1.0.2") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "15xh3c04xxqf1a83p3gmdr3jidk0s7pvv0phhsymvxqk7nl1bayj") (f (quote (("nightly" "tempfile/nightly")))) (r "1.48")))

(define-public crate-camino-tempfile-1.0.3 (c (n "camino-tempfile") (v "1.0.3") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "15mxi13c2dgrixf2a7kiqn2vj0bpqkdrgpx7pfici0mdbf8zl79g") (f (quote (("nightly" "tempfile/nightly")))) (y #t) (r "1.48")))

(define-public crate-camino-tempfile-1.1.0 (c (n "camino-tempfile") (v "1.1.0") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "14w61ah5vwrx1vx4p8wbs5in4rpyg4yn45fk95z24jhzvdkcl786") (f (quote (("nightly" "tempfile/nightly")))) (r "1.63")))

(define-public crate-camino-tempfile-1.1.1 (c (n "camino-tempfile") (v "1.1.1") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "1hk3a8x7950qg9vfl9fjwxyjd659fq6wvchrz4kx9r41z9am146b") (f (quote (("nightly" "tempfile/nightly")))) (r "1.63")))

