(define-module (crates-io ca mi camila) #:use-module (crates-io))

(define-public crate-camila-0.2.0 (c (n "camila") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "discord-sdk") (r "^0.3.2") (d #t) (k 0)) (d (n "mpris") (r "^2.0.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1qi0v2qp00rcwicxd2ikvfgq7s4nc5v94n2mdr2ir9vpwnaiv87k")))

(define-public crate-camila-0.2.1 (c (n "camila") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "discord-sdk") (r "^0.3.2") (d #t) (k 0)) (d (n "mpris") (r "^2.0.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0kgmjkp998csdfs09b69cac6rrsd48jb7x9bvr0dvr775z923dsh")))

