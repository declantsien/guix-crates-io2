(define-module (crates-io ca bo cabocha) #:use-module (crates-io))

(define-public crate-cabocha-0.0.1 (c (n "cabocha") (v "0.0.1") (h "0yq8jimllzqabb7lx1mi2ww8s3zbld2gb3c3xx1zrf01j0vcgxlm")))

(define-public crate-cabocha-0.1.0 (c (n "cabocha") (v "0.1.0") (h "1lp7fdv8r3hqp6j0nkxs9mls2q1k8p0iiz58psk32ys01ncr3d9p")))

(define-public crate-cabocha-0.1.1 (c (n "cabocha") (v "0.1.1") (h "0ph14x8mdddx386k72jf4r2l3jh9gg16xirqyaqd8p4pfbjzr9r8")))

(define-public crate-cabocha-0.1.2 (c (n "cabocha") (v "0.1.2") (h "0wvqdfl059b50gma7mjd7p8lq6pjp1lncihg19mcpbbph84jrqw4")))

(define-public crate-cabocha-0.1.3 (c (n "cabocha") (v "0.1.3") (h "0w8dzxk649zyn6n0b5zzxgxaj97cdb2nrlw3svgc4zqpzzsgk6yf")))

(define-public crate-cabocha-0.1.4 (c (n "cabocha") (v "0.1.4") (h "0ki1s3h5mlqlcx3ykadfsmdcrz7m1zzizzmlbp98y8yp1246i0cb")))

(define-public crate-cabocha-0.1.5 (c (n "cabocha") (v "0.1.5") (h "1y9kzfhhy1wdwj9zbzjydyphvz52mh9iyrq1cks2mhxbbbw434bi")))

(define-public crate-cabocha-0.1.6 (c (n "cabocha") (v "0.1.6") (h "085d6r2gq8l3f79mqdwm542brzaahmzfcng0mvyr49z50ih9mdir")))

(define-public crate-cabocha-0.1.7 (c (n "cabocha") (v "0.1.7") (h "13cyg73ny59fdgk7fpfq0lgrjp9ps94zww4dc44y1qb7g93bafv0")))

(define-public crate-cabocha-0.2.0 (c (n "cabocha") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "07vxjma822pq3anskild931w384m75v3ld37qrdlscddr9vp7p2h")))

