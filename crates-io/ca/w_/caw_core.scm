(define-module (crates-io ca w_ caw_core) #:use-module (crates-io))

(define-public crate-caw_core-0.1.0 (c (n "caw_core") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nwaj97g3wb1bp2alj42g0hsqr3b24b24jx9fmazhmmnwpnc2nf6") (f (quote (("web" "getrandom/js") ("midi" "midly") ("default" "midi"))))))

