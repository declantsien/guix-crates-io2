(define-module (crates-io ca w_ caw_interactive) #:use-module (crates-io))

(define-public crate-caw_interactive-0.1.0 (c (n "caw_interactive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "caw") (r "^0.1") (d #t) (k 0)) (d (n "caw_core") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "line_2d") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.36") (d #t) (k 0)))) (h "1wmy46w0p1b6jrjm69f69642nk112ylsiscw71vanfmcxwxvmf5p") (f (quote (("midi" "caw/midi") ("default" "midi"))))))

