(define-module (crates-io ca po capo) #:use-module (crates-io))

(define-public crate-capo-0.0.0 (c (n "capo") (v "0.0.0") (h "0ym75xnph95kf3741cbgfxadp10y5skc9psqpcwya2ap951j3ks3")))

(define-public crate-capo-0.0.1 (c (n "capo") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0h8i6gcf8a30s9ywz74rbqmx73j5f30fsx9s19y0n1iq6dn4i941")))

