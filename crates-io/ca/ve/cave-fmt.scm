(define-module (crates-io ca ve cave-fmt) #:use-module (crates-io))

(define-public crate-cave-fmt-0.1.0 (c (n "cave-fmt") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (d #t) (k 0)))) (h "0xap36y32kxmbw299wlp122gn24hipjxcx5rhwa8xpqnynlvjwhp")))

(define-public crate-cave-fmt-0.1.2 (c (n "cave-fmt") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (d #t) (k 0)))) (h "19ssixgnl12s05qmlbx5jfyxahvsd91adm49kl1n7pf6zlzl0wia")))

(define-public crate-cave-fmt-0.1.1 (c (n "cave-fmt") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (d #t) (k 0)))) (h "1gjnazck3c7676w2y26r07wc1gb6c27nmwh5w3z5xzbkq7lc2jl0")))

(define-public crate-cave-fmt-0.1.3 (c (n "cave-fmt") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (d #t) (k 0)))) (h "1619gmwk6l6gx1wdc6hm9kc4vsiy0h853wv8m38wmrsllx5h8dbl")))

