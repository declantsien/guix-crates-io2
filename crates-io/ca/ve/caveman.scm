(define-module (crates-io ca ve caveman) #:use-module (crates-io))

(define-public crate-caveman-0.1.0 (c (n "caveman") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 1)) (d (n "rustc_version_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "11vbh5cxxy4fjjqk4482z7f3434i9hc6my1xw68wscnifc1bz913")))

