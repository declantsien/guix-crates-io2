(define-module (crates-io ca ve cavestory_save_editor) #:use-module (crates-io))

(define-public crate-cavestory_save_editor-0.1.0 (c (n "cavestory_save_editor") (v "0.1.0") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "1zrscap7cmf59lm411cjggrxhxc3wr64yb3k4hknjfzcnyshz2d8")))

(define-public crate-cavestory_save_editor-1.0.0 (c (n "cavestory_save_editor") (v "1.0.0") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "0csvrp2klqshfn966psv56vk0857030gdvdjqwnk2rf26wzazl8l")))

(define-public crate-cavestory_save_editor-1.0.1 (c (n "cavestory_save_editor") (v "1.0.1") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "16xyixdi37wfzhyjzqcxxfrzl9mlr4k0v7lxwh0mnhpmiyraj6dz")))

(define-public crate-cavestory_save_editor-1.0.2 (c (n "cavestory_save_editor") (v "1.0.2") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "00m0cn77kcfnhsqk5x7691svarkxd21gfx019g6i9db6m9bxdab5")))

(define-public crate-cavestory_save_editor-1.0.3 (c (n "cavestory_save_editor") (v "1.0.3") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "0fz28893ikw3d0jjlspl6g9isyisdmhfsjwcf3w5p3zpk65gwcwi")))

(define-public crate-cavestory_save_editor-1.1.0 (c (n "cavestory_save_editor") (v "1.1.0") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "1ldm0yzqaf7b11k2b884sd81qwgkp06kbpyxn4w1a6m6jrs2n04b")))

(define-public crate-cavestory_save_editor-1.1.1 (c (n "cavestory_save_editor") (v "1.1.1") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "18nxr0kz1s3gx8fmp2ndkvmswr683i963m5igdq2rf5dm96kwiqp")))

