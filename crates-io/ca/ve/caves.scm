(define-module (crates-io ca ve caves) #:use-module (crates-io))

(define-public crate-caves-0.1.0 (c (n "caves") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_fs") (r "0.11.*") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "1.0.*") (d #t) (k 2)) (d (n "rocksdb") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iz8xpb3f4z725m55f3c9bfgcp7l7q27xsm6lb7lgcn5sm4zh0qd")))

(define-public crate-caves-0.2.0 (c (n "caves") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rocksdb") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07ls7bd6m5qs3ayb15ywcmfwnw27rigr7cy3pk3m5z3rkjf9nqrc") (f (quote (("with-rocksdb" "rocksdb"))))))

(define-public crate-caves-0.2.1 (c (n "caves") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rocksdb") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "060wigf7p6561llizp9aw4ahajyzy7vs18y0yfqfkzwywiikszy6") (f (quote (("with-rocksdb" "rocksdb"))))))

