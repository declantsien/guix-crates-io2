(define-module (crates-io ca ve cavestory-save-lib) #:use-module (crates-io))

(define-public crate-cavestory-save-lib-1.1.1 (c (n "cavestory-save-lib") (v "1.1.1") (h "07cm07v3mnr7dd42n5cxi6xlcyzlvgkqjypjqnbnl2rc3k0mi52y")))

(define-public crate-cavestory-save-lib-2.0.0 (c (n "cavestory-save-lib") (v "2.0.0") (h "0mr9ijhb53lnl3pp70x84m0s3rafihb8jw989izdb79c6rcy8q24")))

(define-public crate-cavestory-save-lib-2.0.1 (c (n "cavestory-save-lib") (v "2.0.1") (h "0dm0k7ydbpp49yz1q7r23i6svqxsnafilkvr56ynzpw3y6wg6pkz")))

(define-public crate-cavestory-save-lib-2.1.0 (c (n "cavestory-save-lib") (v "2.1.0") (h "1xarxj5i27pmwrd4gjgcl8iz7vwd4cwrgx6lczfr1kdyrsnl6d63")))

(define-public crate-cavestory-save-lib-2.2.0 (c (n "cavestory-save-lib") (v "2.2.0") (h "16ys521c914g3gjfh3mvj1agf3y6b8y9ljkw3p7bi482wixz862k")))

(define-public crate-cavestory-save-lib-2.3.0 (c (n "cavestory-save-lib") (v "2.3.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zwg2qshkgmg47in8k9l52v6r7krrs89nqpshhzjvkxss8irsiip")))

(define-public crate-cavestory-save-lib-2.3.2 (c (n "cavestory-save-lib") (v "2.3.2") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kwdnwz3524x8bv37svpjg54ljvnrkb62rcz81amdf9wbzf8h6cl")))

(define-public crate-cavestory-save-lib-2.4.0 (c (n "cavestory-save-lib") (v "2.4.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1knxsjapnkqxicjy4k5qkj0348cmfna2ap5gwd8iab7ylsk6idji")))

(define-public crate-cavestory-save-lib-2.4.1 (c (n "cavestory-save-lib") (v "2.4.1") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ijh2a1ml52n7d3qdmrf6f2kmwpyky9kw63ppm9nzc6n6ar7f9m")))

(define-public crate-cavestory-save-lib-2.4.2 (c (n "cavestory-save-lib") (v "2.4.2") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ksam44srrvvf164c618n8n2aabmga488djc2hydnjivcgid1pg")))

(define-public crate-cavestory-save-lib-2.4.3 (c (n "cavestory-save-lib") (v "2.4.3") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "074h9j7xm4wnpcdwhydkw4qsgj4iih9n3anl013cnzahlzarr3rj")))

(define-public crate-cavestory-save-lib-2.4.4 (c (n "cavestory-save-lib") (v "2.4.4") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kcvjxq1rc6sagbikkxmz3qlmpx4sqzwcal2qg5wfabq9pmqjf5i")))

(define-public crate-cavestory-save-lib-2.5.0 (c (n "cavestory-save-lib") (v "2.5.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gi11bwaxji7a0zbzmm53sa0q40nna2hgp7nijxaqnrcz2kg5pmy")))

(define-public crate-cavestory-save-lib-2.6.0 (c (n "cavestory-save-lib") (v "2.6.0") (d (list (d (n "bit-struct") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aicls3glc1vbggibnk83h5jlifjwsyfq86i6vgrl2wb7l760c0m")))

(define-public crate-cavestory-save-lib-2.6.1 (c (n "cavestory-save-lib") (v "2.6.1") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "121m63nhqilxzbyy1i4g55hyfrc0q28qsn0b1wv7fhwqqim29q1p")))

(define-public crate-cavestory-save-lib-2.6.2 (c (n "cavestory-save-lib") (v "2.6.2") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rk41067vh98xk80a816m6wzgzfw4r0ixcicdw739snvh2kqm0ln")))

(define-public crate-cavestory-save-lib-2.7.0 (c (n "cavestory-save-lib") (v "2.7.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wqzxghb35218n1lr859cgds76y6izj61zkcwbfvf8ccbmzq5k7p")))

(define-public crate-cavestory-save-lib-2.8.0 (c (n "cavestory-save-lib") (v "2.8.0") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1xwd3y9rk2qqjmmd0a3p2hpvrnik419agv23xi7wika4a288q9y7")))

(define-public crate-cavestory-save-lib-2.8.1 (c (n "cavestory-save-lib") (v "2.8.1") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "13sfrwq2vb6bvia4k8wlrm4khn5grbkb9h0rpagv4c4zxv2vwyw8")))

(define-public crate-cavestory-save-lib-2.8.2 (c (n "cavestory-save-lib") (v "2.8.2") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0mwghpwx5byix9lnid3yik0ynvl049qw8vsp3p0ij0bq12ljkgxl")))

(define-public crate-cavestory-save-lib-2.8.3 (c (n "cavestory-save-lib") (v "2.8.3") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "01qsrrsqn48121zmdq79dmqqvv3f7kklq9766s1hfb9p81wk61qp")))

(define-public crate-cavestory-save-lib-2.8.4 (c (n "cavestory-save-lib") (v "2.8.4") (d (list (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ndhd297fwdd4kh75ckjs32679piy60hdws8dni2idi6f57hiy84")))

(define-public crate-cavestory-save-lib-2.8.5 (c (n "cavestory-save-lib") (v "2.8.5") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "10qpvmfnfb2hsc28lx4695k8mkbqrlsb5d6wr4asn4zcbx5pwn73")))

(define-public crate-cavestory-save-lib-2.9.0 (c (n "cavestory-save-lib") (v "2.9.0") (d (list (d (n "strum") (r "^0.26.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ydzqyzpmiydk7bisydmi7nzsibcswk83h9hr8amadkwp392dngv") (f (quote (("strum") ("default" "strum")))) (r "1.75")))

(define-public crate-cavestory-save-lib-2.9.1 (c (n "cavestory-save-lib") (v "2.9.1") (d (list (d (n "strum") (r "^0.26.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1h26ycnkvbcs6x6wxmkaz1kybkd7a51dqq41ji926dq73049bpr7") (f (quote (("strum") ("default" "strum")))) (r "1.75")))

(define-public crate-cavestory-save-lib-2.9.2 (c (n "cavestory-save-lib") (v "2.9.2") (d (list (d (n "strum") (r "^0.26.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "01lm5w03kvib3jkk92hb2n8vmihsgl8mh2v5ghqjz631clncv0iz") (f (quote (("strum") ("default" "strum")))) (r "1.75")))

