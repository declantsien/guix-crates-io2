(define-module (crates-io ca sb casbab) #:use-module (crates-io))

(define-public crate-casbab-0.1.0 (c (n "casbab") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "00qval5d7mmyzi17dgzm2qsnwczpv5vbm4z0bknb9mpivi1jygxr")))

(define-public crate-casbab-0.1.1 (c (n "casbab") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "18mr0sgvs0dw8m8zkw4s111nqv6gczk02a45m28a1dq3zk787sr2") (f (quote (("build-binary" "atty" "clap"))))))

