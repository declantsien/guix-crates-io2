(define-module (crates-io ca sb casbin-rbatis-adapter) #:use-module (crates-io))

(define-public crate-casbin-rbatis-adapter-0.1.0 (c (n "casbin-rbatis-adapter") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "casbin") (r "^2.0.9") (f (quote ("runtime-async-std" "logging" "incremental"))) (k 0)) (d (n "rbatis") (r "^4.0.43") (d #t) (k 0)) (d (n "rbdc-mysql") (r "^0.1.18") (d #t) (k 0)) (d (n "rbs") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "13abnwwvavyn4gvqx9qv533mmppx2qlzsmq1d4p1r6bnai57c9fq")))

