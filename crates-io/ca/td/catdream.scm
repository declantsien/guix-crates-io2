(define-module (crates-io ca td catdream) #:use-module (crates-io))

(define-public crate-catdream-0.1.0 (c (n "catdream") (v "0.1.0") (d (list (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0k46i0iz778m1zrdhg9ha6zm6f6x3lqifh23fb3wzzmbz8v28rrg")))

