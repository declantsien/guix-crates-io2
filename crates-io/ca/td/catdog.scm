(define-module (crates-io ca td catdog) #:use-module (crates-io))

(define-public crate-catdog-0.1.0 (c (n "catdog") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07fff4mxi9lr24wfa694sbl7fk2w31j8jsgwbkmdrzddcp9nydx0")))

