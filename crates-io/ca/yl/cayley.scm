(define-module (crates-io ca yl cayley) #:use-module (crates-io))

(define-public crate-cayley-0.1.0 (c (n "cayley") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1gm85b6n1icf45dlmgw7g63649cbqa5nvscv6f1b8pv4wslcl7y1")))

(define-public crate-cayley-0.2.0 (c (n "cayley") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1wf6h42rjnm4y4w50w6dnknfif8h921ipv8awky19qwzphf9fazk")))

