(define-module (crates-io ca re caret) #:use-module (crates-io))

(define-public crate-caret-0.0.0 (c (n "caret") (v "0.0.0") (h "16qa2xbm1hgyzg67p6yazzlsykmxa58x62vpz9fzad7k31qb71mp")))

(define-public crate-caret-0.0.1 (c (n "caret") (v "0.0.1") (h "07jkrwjlx3vb4i501grmf97xbp563cawr0i2rxxvwp1a8imivfia")))

(define-public crate-caret-0.0.2 (c (n "caret") (v "0.0.2") (h "1qkh2sihbr323zl8k3w3nmlf4lldyp0zw73bzi2wf588j39yrklm")))

(define-public crate-caret-0.0.3 (c (n "caret") (v "0.0.3") (h "1sg4y841hrwswwksirrdzwcsavl32cbipwx2xpymxrlhpj8gnmrl")))

(define-public crate-caret-0.1.0 (c (n "caret") (v "0.1.0") (h "068mv8ln6ykb8a4mc862bnd4zc3mx96iq2r7pn8mr9hjkz02q489")))

(define-public crate-caret-0.1.1 (c (n "caret") (v "0.1.1") (h "0vifh2r0svmpckpp750wsav71zkix7x2rmbl2bx74dd6i3v4n0bc")))

(define-public crate-caret-0.2.0 (c (n "caret") (v "0.2.0") (h "0laxl3pcnxqwb79ca5mg6zx4f4b6nzpsk7dnr0hdk5xya90yakki") (r "1.56")))

(define-public crate-caret-0.2.1 (c (n "caret") (v "0.2.1") (h "18pi7y59aqil7qi5h7bml6iw1c41b00rf0n7yyi5j67y0dk2qi49") (r "1.56")))

(define-public crate-caret-0.2.2 (c (n "caret") (v "0.2.2") (h "1wmkk5b9wb8g74xxgl08hkw2fvgwxpcnzkjsz4nqs4ghzm7nksnf") (r "1.56")))

(define-public crate-caret-0.3.0 (c (n "caret") (v "0.3.0") (h "1m9xd82ywvfzzfv9s4nsq0a0412ig2cn3x9wik3dd15dg54076cd") (r "1.60")))

(define-public crate-caret-0.3.1 (c (n "caret") (v "0.3.1") (h "0h26c2904bka9ad55l0ag6fq50civfgynrnwblgcjfch35ddg0sp") (r "1.60")))

(define-public crate-caret-0.3.2 (c (n "caret") (v "0.3.2") (h "0gcy9wpj0j7jvx6kvgjvwf6i6i5kqnnvm37fq51392n0vs5r20b9") (r "1.60")))

(define-public crate-caret-0.4.0 (c (n "caret") (v "0.4.0") (h "0br038cmg8f6wrh88wc2zx5qkqj6k8a2n7r3h4dar0s5vqr7nhcj") (r "1.65")))

(define-public crate-caret-0.4.1 (c (n "caret") (v "0.4.1") (h "1m6fp9cqw28qf625v90gkvd15bsfj7141r23yblnzfwn1492kyb9") (f (quote (("full")))) (r "1.65")))

(define-public crate-caret-0.4.2 (c (n "caret") (v "0.4.2") (h "13782q7b9f0g9wbzp9i217w6b77kkifh7xahpmfqj8s6wjf43vx9") (f (quote (("full")))) (r "1.65")))

(define-public crate-caret-0.4.3 (c (n "caret") (v "0.4.3") (h "0s77pp1lrzh8lyi728gdh78ncb2dp4q4vzg1akvhc7p91zypfdma") (f (quote (("full")))) (r "1.65")))

(define-public crate-caret-0.4.4 (c (n "caret") (v "0.4.4") (h "0jlwcmmw955b7y7r5l6z414sqcmqsb1pc118zg73xdx8nwc2sivs") (f (quote (("full")))) (r "1.65")))

(define-public crate-caret-0.4.5 (c (n "caret") (v "0.4.5") (h "1nqq0a83pqfjn0b10p0hh5kr7b5cxjjcnr36srzq3vxq0jcvnm9d") (f (quote (("full")))) (r "1.70")))

