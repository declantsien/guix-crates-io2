(define-module (crates-io ca ni canistergeek_ic_rust) #:use-module (crates-io))

(define-public crate-canistergeek_ic_rust-0.1.1 (c (n "canistergeek_ic_rust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02jv0vyf1yrfs8ddsrl0wy305psv522nzwj3rv3kzdv0gx6rl8jl")))

(define-public crate-canistergeek_ic_rust-0.1.2 (c (n "canistergeek_ic_rust") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ny2vi1a338yv723lqvgfy4wf8vmvq6g69lbiffv5jmpqwm54rav")))

(define-public crate-canistergeek_ic_rust-0.2.1 (c (n "canistergeek_ic_rust") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1h4xycq8cwbwzylc6li7r78774gginfwjf9x6ycah7ymciyfl5d8")))

(define-public crate-canistergeek_ic_rust-0.2.2 (c (n "canistergeek_ic_rust") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hq533ppy7w2dfpr69sf53x0aay46w6rj6qrybp58b4dn6smhq0d")))

(define-public crate-canistergeek_ic_rust-0.2.3 (c (n "canistergeek_ic_rust") (v "0.2.3") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "1dlk03qqfkajs2hncr0nwvxgwh53x0r09i6203k0xjgndcjs6yws")))

(define-public crate-canistergeek_ic_rust-0.3.0 (c (n "canistergeek_ic_rust") (v "0.3.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "09cxc4f5svpldr08bwpk41hy2ay4qcqjpmkn5dba1izkibw1znjb")))

(define-public crate-canistergeek_ic_rust-0.3.1 (c (n "canistergeek_ic_rust") (v "0.3.1") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "00pawfkk39hfi6q3gh5lz4q02xjlccrg7dqnnk0hjgl5wrcj7xnd")))

(define-public crate-canistergeek_ic_rust-0.3.2 (c (n "canistergeek_ic_rust") (v "0.3.2") (d (list (d (n "candid") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0r45847yh0nb8pqxdyl3jf31qwp8mj4s03a80vg5gc0j4z3ngjcy")))

(define-public crate-canistergeek_ic_rust-0.3.3 (c (n "canistergeek_ic_rust") (v "0.3.3") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "14bmqf5wb5z44941k8h1nbzmlywi5d7wl4nfwas5gr96sd7w4hpx")))

(define-public crate-canistergeek_ic_rust-0.4.1 (c (n "canistergeek_ic_rust") (v "0.4.1") (d (list (d (n "candid") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "13cx4wajyjk7r19m21xvchc8nwp90lc3n88ic7svksgcd0l6ivk0")))

(define-public crate-canistergeek_ic_rust-0.4.2 (c (n "canistergeek_ic_rust") (v "0.4.2") (d (list (d (n "candid") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0gvsr7d5ay6kg65n6h6c5zh2ddafyfaq5v4kl9icfx0xxs9d4y9p")))

(define-public crate-canistergeek_ic_rust-0.4.3 (c (n "canistergeek_ic_rust") (v "0.4.3") (d (list (d (n "candid") (r "^0.10.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (f (quote ("clock" "std"))) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)))) (h "1h2rcsjd103a28wxh7x1gxi21cajz95ikkkz6agarf8gkw8561cm")))

