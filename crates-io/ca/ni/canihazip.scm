(define-module (crates-io ca ni canihazip) #:use-module (crates-io))

(define-public crate-canihazip-1.0.0 (c (n "canihazip") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "03lapnsvm7w2bilj69gd7iv0i2m5n8rmyiz0bx3zh9w6a4cdy3wv")))

(define-public crate-canihazip-1.0.1 (c (n "canihazip") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1k6bg33bhz97f68pw355nw1g8dbqzn3mb25qyzfgvcrfk04hwc5x")))

(define-public crate-canihazip-2.0.0 (c (n "canihazip") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0qmkr7a9bansyb11jkh5ydnmlgnh7rpg64h99sqx4qxfba78q139")))

(define-public crate-canihazip-2.0.2 (c (n "canihazip") (v "2.0.2") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0b98bjasdif63fb77vnrjibhs6mqz3a04lx5dy0jngrmq4bnj40g")))

(define-public crate-canihazip-3.0.0 (c (n "canihazip") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0w0nn237l7yz1k2hhvmm2zznjzyicbm5iaka4aqhvyrvs117sm85")))

