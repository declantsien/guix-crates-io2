(define-module (crates-io ca ni canistreamit) #:use-module (crates-io))

(define-public crate-canistreamit-0.1.0 (c (n "canistreamit") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "03pprygm0a8a68gk2zzn09nygq84lihgwsbsc95513y5zgwkqjwz")))

(define-public crate-canistreamit-0.1.1 (c (n "canistreamit") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0ppvqf35fx2fhbhg83wm3lw4022pq084z4ia4lbg3slr1inrp6mg")))

(define-public crate-canistreamit-0.2.0 (c (n "canistreamit") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1jfppx47rbg26dyrshl8ch0p8sf1l8qzw56ngjyxbdiq68y8c5wm")))

