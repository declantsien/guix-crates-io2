(define-module (crates-io ca ni caniuse-db) #:use-module (crates-io))

(define-public crate-caniuse-db-1.0.30000909 (c (n "caniuse-db") (v "1.0.30000909") (d (list (d (n "inflect") (r "^0.1") (d #t) (k 1)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 1)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 1)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1vbmgmb9wab05vias9n4g3ah2yymcpi062nqqyg66nfrvqsa45iz")))

