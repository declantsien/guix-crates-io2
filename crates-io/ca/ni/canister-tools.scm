(define-module (crates-io ca ni canister-tools) #:use-module (crates-io))

(define-public crate-canister-tools-0.1.0 (c (n "canister-tools") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.8.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "0156ddd73mj4zr90pi6fg2klw36x0md7acj35c4p4hgkxf6fvdnr")))

(define-public crate-canister-tools-0.1.1 (c (n "canister-tools") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.8.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "14i4ifp31mp69d7bb85qdck2p632pggsc7ziv0l9szqmf44hhhb3")))

(define-public crate-canister-tools-0.1.2 (c (n "canister-tools") (v "0.1.2") (d (list (d (n "ic-cdk") (r "^0.8.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "12kbfwf9c3npa9ahfc57qlg02bjb2cp7q7dgbw98l3ncdg3qay6h")))

(define-public crate-canister-tools-0.1.3 (c (n "canister-tools") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "0nxscgadkx75qvyf8zcazy4578h8p15x03bdgnl6mw3kd57rww01") (y #t)))

(define-public crate-canister-tools-0.1.4 (c (n "canister-tools") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "candid") (r "^0.9.6") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "1236yamn3df6ag4gpn8q0krfngn4679q115rvfcb0bf5d1s13i36")))

(define-public crate-canister-tools-0.2.0 (c (n "canister-tools") (v "0.2.0") (d (list (d (n "candid") (r "^0.9.11") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "1679dny1zwfjdd33mpndg7g5l2ai1q7nxnvkmzh66kyv9lr4fc2b")))

(define-public crate-canister-tools-0.2.1 (c (n "canister-tools") (v "0.2.1") (d (list (d (n "candid") (r "^0.9.11") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "1l6ffc3pzgh9mlw8adg3kiwp19hi9k1lk3a64l0skrwr6kj9hdlh")))

(define-public crate-canister-tools-0.2.2 (c (n "canister-tools") (v "0.2.2") (d (list (d (n "candid") (r "^0.10.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "0dyy9rqfk4za81a17hffjqplcsynhahj56pw7g7qmg0igzfydlby")))

(define-public crate-canister-tools-0.2.3 (c (n "canister-tools") (v "0.2.3") (d (list (d (n "candid") (r "^0.10.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.0") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "0q4264zi01limkrm0zbvfq01ibk0a5bxrbn9shhjyjacqrkrwi54")))

