(define-module (crates-io ca ch cacher_hasher) #:use-module (crates-io))

(define-public crate-cacher_hasher-0.1.0 (c (n "cacher_hasher") (v "0.1.0") (h "1ndmd92bgxszhfz0x5dzwm866is3vhrwcadmz3k1p3776j7x1bna")))

(define-public crate-cacher_hasher-0.1.1 (c (n "cacher_hasher") (v "0.1.1") (h "08gw3s8qw7w9jwlsv8dw8ap5cp9m96c4b1771ax4s2pxn6f5zhrg")))

