(define-module (crates-io ca ch cache_2q) #:use-module (crates-io))

(define-public crate-cache_2q-0.8.0 (c (n "cache_2q") (v "0.8.0") (h "1zvv7y005fc3ssvrhbp50z2jzawiajjnqam7fmcifxby4h54rjck")))

(define-public crate-cache_2q-0.8.1 (c (n "cache_2q") (v "0.8.1") (h "0hv511svg0ssg39yrpx33wvpd2mdbfpc6cq11ljjjipvn59j3c5y")))

(define-public crate-cache_2q-0.8.2 (c (n "cache_2q") (v "0.8.2") (h "0k508805kw1r0mqj332rsd0cdcmhik4hgfj1c6bn7pdpiwfr8q0y")))

(define-public crate-cache_2q-0.8.3 (c (n "cache_2q") (v "0.8.3") (h "0aljzpyk0xdwir99bzimn92ipr6y8xk1r9azhkfdd91l71hgf1k2")))

(define-public crate-cache_2q-0.8.4 (c (n "cache_2q") (v "0.8.4") (h "0z29jc6xpx4nyx18a87g4qhbrbnbgldbz6fkghin2zq3r1379348")))

(define-public crate-cache_2q-0.9.0 (c (n "cache_2q") (v "0.9.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "03rk4fd9d443h6qi3l1fclfhhsaxljbdn3n9mn0h2pvcccg0106a")))

(define-public crate-cache_2q-0.10.0 (c (n "cache_2q") (v "0.10.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0wmd3fp05q86gljqvgp59id3did2a07r8g55mcxwji90f58mryfz") (y #t)))

(define-public crate-cache_2q-0.10.1 (c (n "cache_2q") (v "0.10.1") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)))) (h "17az9wyy6lar8nqbk70ibnjxqjfsvib2r8was8ax12zczks1yhn3")))

