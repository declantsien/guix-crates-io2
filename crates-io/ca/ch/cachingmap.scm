(define-module (crates-io ca ch cachingmap) #:use-module (crates-io))

(define-public crate-cachingmap-0.1.0 (c (n "cachingmap") (v "0.1.0") (h "0wsp00h9jisyg5kaicpmrc1llv9k6z4yqanz52a601k33gdrxlln") (y #t)))

(define-public crate-cachingmap-0.2.0 (c (n "cachingmap") (v "0.2.0") (h "1nkppjph6l06rmrr9w0gp4m4jyp9g71cd89qgx75r0vjqp2yik25") (y #t)))

(define-public crate-cachingmap-0.2.1 (c (n "cachingmap") (v "0.2.1") (h "0zlfs31vri96fkqqcflih4iav1qckdpfqhm4817xw5618bcbp5lm")))

