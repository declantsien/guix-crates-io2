(define-module (crates-io ca ch cachier) #:use-module (crates-io))

(define-public crate-cachier-0.1.0-beta.2 (c (n "cachier") (v "0.1.0-beta.2") (d (list (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (d #t) (k 0)))) (h "12dzvpbnvmpdbddrz8ah21l6m8sgcbwgkp8fgvhnkv3zzgmadnc6")))

(define-public crate-cachier-0.1.0-beta.4 (c (n "cachier") (v "0.1.0-beta.4") (d (list (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0zawji75isj1fr26ycibd90myx958xh3wx70pdpjy6q3d2wv3h1s")))

