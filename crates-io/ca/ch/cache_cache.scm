(define-module (crates-io ca ch cache_cache) #:use-module (crates-io))

(define-public crate-cache_cache-0.1.0 (c (n "cache_cache") (v "0.1.0") (h "05gpw6chqypjaq6v33xndk8g31rdbnjzgxmbhk5gj1dndn3652zr")))

(define-public crate-cache_cache-0.1.1 (c (n "cache_cache") (v "0.1.1") (h "1klz9fj86z209zfdq6jfimax2f01s19zlsnn9lqz9pw9ks2k01g9")))

