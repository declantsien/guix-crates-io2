(define-module (crates-io ca ch cache-padded) #:use-module (crates-io))

(define-public crate-cache-padded-1.0.0 (c (n "cache-padded") (v "1.0.0") (h "14l32n90zhhpsb9yr1hrgr85c4bn7mgjmz1wwhf2i4adjwczm3bh")))

(define-public crate-cache-padded-1.1.0 (c (n "cache-padded") (v "1.1.0") (h "018h5zlk26kvglws2agr9vnzidps4j0x5x101hw5r1vpqql8wl14")))

(define-public crate-cache-padded-1.1.1 (c (n "cache-padded") (v "1.1.1") (h "1fkdwv9vjazm6fs3s5v56mm4carwswdmw8fcwm9ygrcvihcya6k3")))

(define-public crate-cache-padded-1.2.0 (c (n "cache-padded") (v "1.2.0") (h "0b39fmvn6j47xcyc03biyh8kdd52qwhb55xmx72hj3y73ri5kny1")))

(define-public crate-cache-padded-1.3.0 (c (n "cache-padded") (v "1.3.0") (h "08gb1407k0cvhfllgg06j45r0lv99qrmraf19mccqbs2iz4j05cq") (r "1.31")))

