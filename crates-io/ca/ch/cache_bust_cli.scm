(define-module (crates-io ca ch cache_bust_cli) #:use-module (crates-io))

(define-public crate-cache_bust_cli-0.1.0 (c (n "cache_bust_cli") (v "0.1.0") (d (list (d (n "cache_bust") (r "^0.1.0") (f (quote ("build"))) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r2phfwvy22hwgrngmalmcxjcghyw0rlpsxglw1l1n0vg97z9c5c")))

(define-public crate-cache_bust_cli-0.2.0 (c (n "cache_bust_cli") (v "0.2.0") (d (list (d (n "cache_bust") (r "^0.2.0") (f (quote ("build"))) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "13v5kc9awxv1zvhczr2a6m507r3frwv75nnq57b94nynipzcgsf0")))

