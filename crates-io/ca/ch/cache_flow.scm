(define-module (crates-io ca ch cache_flow) #:use-module (crates-io))

(define-public crate-cache_flow-0.1.0 (c (n "cache_flow") (v "0.1.0") (h "1xhsjijxanmc39mdv0j332fcnlil9xaymck4wpwpm0sdkhzy90rz") (y #t)))

(define-public crate-cache_flow-0.2.0 (c (n "cache_flow") (v "0.2.0") (h "02gpanalpphb5jrymkn7q0grh7gsfdffqw6j824fiavch1gic845")))

(define-public crate-cache_flow-0.2.1 (c (n "cache_flow") (v "0.2.1") (h "0sfa98fwk3xfv0glrrqmjcc652ry90dlxrpqndryclrm4fj35bxa")))

(define-public crate-cache_flow-0.2.2 (c (n "cache_flow") (v "0.2.2") (h "1r55g0cg8rxw4jwjfvccin7kxlg1hykf2c7d4hmf0744jsr764iv")))

(define-public crate-cache_flow-0.2.3 (c (n "cache_flow") (v "0.2.3") (h "0lpl4lk7nnw1fdzvvinxa23bkjz6dqpk1ki879c04sn12vp9syfc")))

