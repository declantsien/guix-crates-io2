(define-module (crates-io ca ch cache_line_size) #:use-module (crates-io))

(define-public crate-cache_line_size-0.1.0 (c (n "cache_line_size") (v "0.1.0") (h "15bqnhb3j2c4b6nd4ywf6vzh1qg3crnf313qhj9nq7f5l7vzp7n6")))

(define-public crate-cache_line_size-0.2.0 (c (n "cache_line_size") (v "0.2.0") (h "08dr8ifjya6nmlln33jjcgfw167hskq20lida75wcpnlb97pz884")))

(define-public crate-cache_line_size-1.0.0 (c (n "cache_line_size") (v "1.0.0") (h "0db57r546g52kljf5llxl3b0c0m5n797xs36wiq31jhc8sjbpk47")))

