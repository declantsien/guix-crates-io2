(define-module (crates-io ca ch cacheapp) #:use-module (crates-io))

(define-public crate-cacheapp-0.1.3 (c (n "cacheapp") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "0snj5zhp22p8fxn155izirvpabysyi7nxw67hwg43qfg7wwgc6ak")))

