(define-module (crates-io ca ch cache-advisor) #:use-module (crates-io))

(define-public crate-cache-advisor-1.0.8 (c (n "cache-advisor") (v "1.0.8") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "1bcgqdcsm8104x2pliidfchdvv59zfdpjbqqxfps22l6w4rr46fm")))

(define-public crate-cache-advisor-1.0.9 (c (n "cache-advisor") (v "1.0.9") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0l3hsnya93l3ypz9yagbw5wkkfxfrgjijyp5cv74dl70f11iz6gm")))

(define-public crate-cache-advisor-1.0.10 (c (n "cache-advisor") (v "1.0.10") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0k2ss69jv9cxl44080c42pzdndvd7ww4l9xfcn8hfxp8d3409in7")))

(define-public crate-cache-advisor-1.0.11 (c (n "cache-advisor") (v "1.0.11") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0r3nsgr541cicl555migr8ly7yzrc1hh199h7x6p36xmakdpav35")))

(define-public crate-cache-advisor-1.0.12 (c (n "cache-advisor") (v "1.0.12") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0f5djmx9yvssbjkwpv0qijr0z9x54g75qvvm6imi7zm02gs8yy0i")))

(define-public crate-cache-advisor-1.0.13 (c (n "cache-advisor") (v "1.0.13") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0ws2wahwmphr9b5zzjnfjbhda7m75gf2hs8wmgygygrrfsnyq457")))

(define-public crate-cache-advisor-1.0.14 (c (n "cache-advisor") (v "1.0.14") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0l5i51b19gdkpc2h3a2hwhp5xd2d1x8w2qb2a613kjlk49m622vd")))

(define-public crate-cache-advisor-1.0.15 (c (n "cache-advisor") (v "1.0.15") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "1krbc2vq2zl9bx0725fx9dxwy5kjim5g0l36m3a9d82x3zahabhh")))

(define-public crate-cache-advisor-1.0.16 (c (n "cache-advisor") (v "1.0.16") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "0l3lba9mjcnwc2hbz2srfr56xmhsvd35670afjh4csjfr9asp2bz")))

