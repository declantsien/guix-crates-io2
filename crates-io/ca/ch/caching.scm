(define-module (crates-io ca ch caching) #:use-module (crates-io))

(define-public crate-Caching-0.1.0 (c (n "Caching") (v "0.1.0") (h "0yv9gxff13f7zg25mn79ykknw2b67nqxq8p2p6pda65v5139nqzj")))

(define-public crate-Caching-0.2.0 (c (n "Caching") (v "0.2.0") (h "1x8x41v0g1f52gwsac45c8x48r4cl6lfnmacipfjkkaq0dw91vyl")))

(define-public crate-Caching-0.2.5 (c (n "Caching") (v "0.2.5") (h "0sf9rbm36pyr75wl5zjzqv26sibhhdkbgw2527mridiss2w4kw54")))

