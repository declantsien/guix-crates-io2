(define-module (crates-io ca ch cache-this) #:use-module (crates-io))

(define-public crate-cache-this-0.1.0 (c (n "cache-this") (v "0.1.0") (d (list (d (n "cache-this-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p0510n136acw6pd8cw7081b7k3hbi54adz01kdsmf2gn0w69vh3")))

(define-public crate-cache-this-0.1.1 (c (n "cache-this") (v "0.1.1") (d (list (d (n "cache-this-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ssb2bfhdac3b6xdb21jcmr11fl8avyn295d62hqipzr6p0ypzf7")))

(define-public crate-cache-this-0.1.2 (c (n "cache-this") (v "0.1.2") (d (list (d (n "cache-this-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c9ch89ax3lxcl2l6lips590kkbn1ng01mjjqjcs2miiv5s2axxn")))

