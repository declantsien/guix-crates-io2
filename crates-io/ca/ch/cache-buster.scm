(define-module (crates-io ca ch cache-buster) #:use-module (crates-io))

(define-public crate-cache-buster-0.1.0 (c (n "cache-buster") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lol_html") (r "^0.2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1h1b0ll0vp4p9nw2zbfv1ivnlmm8kmdlbjgw82ca4kkmg9cikhls") (y #t)))

(define-public crate-cache-buster-0.1.1 (c (n "cache-buster") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lol_html") (r "^0.2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "178qd0m0493fbj8x2mlaxbjp0yz5nafwj6qzamn7pi65wnf1zk62") (y #t)))

(define-public crate-cache-buster-0.1.2 (c (n "cache-buster") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lol_html") (r "^0.2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1ad5lp6m9j161kpgy6cyf835n5dnzr6rmrbccysdchgva6fxbg1v") (y #t)))

