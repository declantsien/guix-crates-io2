(define-module (crates-io ca ch cachedir) #:use-module (crates-io))

(define-public crate-cachedir-0.1.0 (c (n "cachedir") (v "0.1.0") (h "0gsd3dwrffdbnrmzyij2zjzh0r46y3vnwiqimdx56b27s9j89agg") (y #t)))

(define-public crate-cachedir-0.1.1 (c (n "cachedir") (v "0.1.1") (h "1j18j73fxrr82marcdrn86123vr9v5n0fgyjkf9mi9pzyk8hjrf0") (y #t)))

(define-public crate-cachedir-0.2.0 (c (n "cachedir") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0srhqwrwwy9a9jaxxh4g4y41vjg0fdjd3gs00mx1drq7xh8a8bf3")))

(define-public crate-cachedir-0.3.0 (c (n "cachedir") (v "0.3.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1wwlsrrs0m7wmhqp2mjimszid280vbs9flj4fwlfqmzafdcbydp2")))

(define-public crate-cachedir-0.3.1 (c (n "cachedir") (v "0.3.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wyqx30crm2qsq4ny57hhljyq6iw6j4qfg7fbfiqznvpf29z60s7")))

