(define-module (crates-io ca ch cached_proc_macro) #:use-module (crates-io))

(define-public crate-cached_proc_macro-0.1.0 (c (n "cached_proc_macro") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0m5mpggprwcnc5mn6k00zap58a5g6b3022idxn9frbnz9fbiih")))

(define-public crate-cached_proc_macro-0.1.1 (c (n "cached_proc_macro") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0g3f9ja0pjc3c0p9bamwf6ksvi0bwaxgp8brq3ilsyj2xikbnlx3")))

(define-public crate-cached_proc_macro-0.1.2 (c (n "cached_proc_macro") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "18jcilih4y9sla466py73hgfk8cn2y6idxvsgrph9fv55y8px9gp")))

(define-public crate-cached_proc_macro-0.2.0 (c (n "cached_proc_macro") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "03mdbnpkzm4g7cb7nvdkyhqi52lnm8bzbrf5yqv31bcvj6469wps")))

(define-public crate-cached_proc_macro-0.3.0 (c (n "cached_proc_macro") (v "0.3.0") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfz4d7qhhnly93pqb6w5xi3y7s6k1j2jg1hhgfk64yyiswxrjl8")))

(define-public crate-cached_proc_macro-0.4.0 (c (n "cached_proc_macro") (v "0.4.0") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "01lg6fyidbkmlhg0a4qq66aj3p58llr2wq7662yd16gvfy6kxcnq")))

(define-public crate-cached_proc_macro-0.5.0 (c (n "cached_proc_macro") (v "0.5.0") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "18yhii0qrxk8mwkjnsyggw38x960admxgjvbi29vlvla4hjjh5fv")))

(define-public crate-cached_proc_macro-0.6.0 (c (n "cached_proc_macro") (v "0.6.0") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "134477s7x3xb912d2fzyw9y5jyhd9dl64vhqqpjys2li5pj7m1dz")))

(define-public crate-cached_proc_macro-0.6.1 (c (n "cached_proc_macro") (v "0.6.1") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0mn4l0lkbv0bxa0n1zyi0kignl2ghppwp78l7njr2ymm76x8b9i5")))

(define-public crate-cached_proc_macro-0.6.2 (c (n "cached_proc_macro") (v "0.6.2") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "11dava0008ks2k34q805y6afpw6vnb2p5wdfpw210x6vypcvhc22")))

(define-public crate-cached_proc_macro-0.7.0 (c (n "cached_proc_macro") (v "0.7.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0aa2d0nmpyb44a9548yppgypshkwvy7hf60vn2cfhd6109pgahsv")))

(define-public crate-cached_proc_macro-0.7.1 (c (n "cached_proc_macro") (v "0.7.1") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "149an94igsr50xyrqj484krzzhg7mrdm3vd7g2iliprr7f832ddc")))

(define-public crate-cached_proc_macro-0.8.0 (c (n "cached_proc_macro") (v "0.8.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "114wgqcbwrkqvfivwmk5raav9gv1n8m45zcc97mmsia7xar7gkxr")))

(define-public crate-cached_proc_macro-0.9.0 (c (n "cached_proc_macro") (v "0.9.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "04cmpjxmyrkrq86j2h3lzzz874wal8ncc1frkfc4p0d2dm6l6pvj")))

(define-public crate-cached_proc_macro-0.10.0 (c (n "cached_proc_macro") (v "0.10.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqvbxhmlbkadjgkx6insqllmx07cdgrdkgzgj3cc258lhahz3aj")))

(define-public crate-cached_proc_macro-0.11.0 (c (n "cached_proc_macro") (v "0.11.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4gad541ac0d8lb5ncqqmmnsaccwp1z849wizp8cxkp8ywyg3fw")))

(define-public crate-cached_proc_macro-0.12.0 (c (n "cached_proc_macro") (v "0.12.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0bydrvl47d8s7lycpjjjn0m3sc6amn4hq1izvwybkikpkdzz7q5w")))

(define-public crate-cached_proc_macro-0.13.0 (c (n "cached_proc_macro") (v "0.13.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "10iv5zkhwx4n9f0c4wipfvgzyyfjy7dixkcwmddva2blbx3dz5s7")))

(define-public crate-cached_proc_macro-0.14.0 (c (n "cached_proc_macro") (v "0.14.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "00yz02fq8czp02yfzwxxhlkwvk0rnam2lrddaqa96k29ld38q75h")))

(define-public crate-cached_proc_macro-0.15.0 (c (n "cached_proc_macro") (v "0.15.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1whnvirs3jm16kifrydipy3pxvpa0k2aqny6yvkla589g977y7vm")))

(define-public crate-cached_proc_macro-0.16.0 (c (n "cached_proc_macro") (v "0.16.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1p73gc4cb7pz67m4azl9li5wkns55in5waxyvd4sk8xah5yah371")))

(define-public crate-cached_proc_macro-0.17.0 (c (n "cached_proc_macro") (v "0.17.0") (d (list (d (n "cached_proc_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "10ph449clvmsgwpw1qlyfxkva2hslk13751b1p2h9mig5nb1925l")))

(define-public crate-cached_proc_macro-0.18.0 (c (n "cached_proc_macro") (v "0.18.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0xji01ybw59y0crggd7fkh9l63jvq5a7n93n7cfa8xpmsmfj9a3x")))

(define-public crate-cached_proc_macro-0.18.1 (c (n "cached_proc_macro") (v "0.18.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "07ynvsav7jbihl1cd1s6a9044fjqcydaaf10f9c21ai150fcfy68")))

(define-public crate-cached_proc_macro-0.19.0 (c (n "cached_proc_macro") (v "0.19.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwayzd0x5z5s5skn6cbpsb2iq7ykwxp9yjxvv7mc0m89hmi40lp")))

(define-public crate-cached_proc_macro-0.19.1 (c (n "cached_proc_macro") (v "0.19.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "18g6fv3f70rqi8x30b4jaq2jkxm9ck4shv45jsr7bj6bywkgallx")))

(define-public crate-cached_proc_macro-0.20.0 (c (n "cached_proc_macro") (v "0.20.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0anbr5hry4a3pfrlwingfmq1yqql7iwggxgxnwm1mqsdv301d7xd")))

(define-public crate-cached_proc_macro-0.21.0 (c (n "cached_proc_macro") (v "0.21.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0sd6a4b0x7ijgkk448z7c20irj4vxjwpn66bra5nrnhp7dzsa6kp")))

