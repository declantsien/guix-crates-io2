(define-module (crates-io ca ch cache_service) #:use-module (crates-io))

(define-public crate-cache_service-1.0.0 (c (n "cache_service") (v "1.0.0") (d (list (d (n "redis") (r "^0.25.3") (d #t) (k 0)))) (h "1a444dn6qr1x0f7dw3dwgq04qk4cnvx9i9rss7y5kmljkq7k6jq6")))

(define-public crate-cache_service-1.1.0 (c (n "cache_service") (v "1.1.0") (d (list (d (n "redis") (r "^0.25.3") (d #t) (k 0)))) (h "04c0qxwpy5rghyjr2n2593q19rw91rx1avam53kf8ix3qfkn66al")))

