(define-module (crates-io ca ch cachemap3) #:use-module (crates-io))

(define-public crate-cachemap3-0.0.1 (c (n "cachemap3") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.14.3") (f (quote ("raw"))) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("ahash"))) (d #t) (k 2)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)))) (h "0w44jpx80addm0hamqi5982990gzvk8lfr8is74aq1ln97ipx7z1") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:parking_lot"))))))

