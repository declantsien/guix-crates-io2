(define-module (crates-io ca ch cachewipe) #:use-module (crates-io))

(define-public crate-cachewipe-0.1.0 (c (n "cachewipe") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "gitignore") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bcd2a7q3njmgwkw7vkhvbf7v7iv4m0g8vwqbx01ifgw6wy1w2lm")))

