(define-module (crates-io ca ch cache-size) #:use-module (crates-io))

(define-public crate-cache-size-0.2.0 (c (n "cache-size") (v "0.2.0") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "133wnamh6yrd9gjv8hrap0v0r23r7njml84pk1whqklrq54577w6")))

(define-public crate-cache-size-0.2.1 (c (n "cache-size") (v "0.2.1") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "1apzha4i3fmgl2mq50mkgq5k7z6g7zc6apfcy7ds3g1ibzdvxvfb")))

(define-public crate-cache-size-0.3.0 (c (n "cache-size") (v "0.3.0") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "061mf98gv6zymkbd7c3vazialsc5yfqz803pnl38v23h7xw11n4p")))

(define-public crate-cache-size-0.3.1 (c (n "cache-size") (v "0.3.1") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "1cxqcss138qhkkssrca84544sg28vp08nm5hy82pg39isxnff3mw")))

(define-public crate-cache-size-0.3.2 (c (n "cache-size") (v "0.3.2") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "1pnxa5jjgvvbm774wqrhz9gm4yzx4x5394hn5sggif0afpi6li6g")))

(define-public crate-cache-size-0.3.3 (c (n "cache-size") (v "0.3.3") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "0lgrvn7v5699zcavm9z24vhcpvc9c62pg3msbkfvjgzas09grvwl")))

(define-public crate-cache-size-0.3.4 (c (n "cache-size") (v "0.3.4") (d (list (d (n "raw-cpuid") (r "^5.0.0") (d #t) (k 0)))) (h "1xgjx1nbk56pk5bv8l7dbkdl2czj4xf79hriqads89rjwzfj1096")))

(define-public crate-cache-size-0.3.5 (c (n "cache-size") (v "0.3.5") (d (list (d (n "raw-cpuid") (r "^6.1.0") (d #t) (k 0)))) (h "19k30hy35cqxa6iszxikvpray9sl5m8i2jdhlccz8msdvwz7166g")))

(define-public crate-cache-size-0.4.0 (c (n "cache-size") (v "0.4.0") (d (list (d (n "raw-cpuid") (r "^7.0.3") (d #t) (k 0)))) (h "1bl6fp8bvb1z9npkkdlhqnlb9gzprgjhvvmkdl4bwxxkhyqybh7r")))

(define-public crate-cache-size-0.5.0 (c (n "cache-size") (v "0.5.0") (d (list (d (n "raw-cpuid") (r "^7.0.3") (d #t) (t "cfg(target_arch = \"x86\")") (k 0)) (d (n "raw-cpuid") (r "^7.0.3") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "075c9hb6dhcxjgg1dg5jgwfi4n78wsfsr79p16lqqrh179297nc1")))

(define-public crate-cache-size-0.5.1 (c (n "cache-size") (v "0.5.1") (d (list (d (n "raw-cpuid") (r "^8.0.0") (d #t) (t "cfg(target_arch = \"x86\")") (k 0)) (d (n "raw-cpuid") (r "^8.0.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "1pz5sqc0vrqda5iznfj06niafsfq53dk9cgm3h2s4yn5gmyd2xq8")))

(define-public crate-cache-size-0.6.0 (c (n "cache-size") (v "0.6.0") (d (list (d (n "raw-cpuid") (r "^10.2.0") (d #t) (t "cfg(target_arch = \"x86\")") (k 0)) (d (n "raw-cpuid") (r "^10.2.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "15200kkrpsdwlfhz5hmajvjgqc5mx0kj3gx0cl8bf3rf2262ij0y")))

