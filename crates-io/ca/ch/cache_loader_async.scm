(define-module (crates-io ca ch cache_loader_async) #:use-module (crates-io))

(define-public crate-cache_loader_async-0.0.1 (c (n "cache_loader_async") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12z80brp4l7mm3iz5wxswwi0l7spxr0133vv3jcpc422l334ghi6")))

(define-public crate-cache_loader_async-0.0.2 (c (n "cache_loader_async") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0khkpkkb5gik241kx81qnas85r7ljdwlpx6lkaw6h9c71as12ph1")))

(define-public crate-cache_loader_async-0.0.3 (c (n "cache_loader_async") (v "0.0.3") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l23hvcgink5f6yip4j77zifbyflkhapymz3jx8s8rbapq17l37d") (f (quote (("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.0.4 (c (n "cache_loader_async") (v "0.0.4") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i3mzlg1djgw7g25y1v56b3mrr7v5s18zfg8w4agyng5s49jmrax") (f (quote (("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.0.5 (c (n "cache_loader_async") (v "0.0.5") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j44rzg8rri3530h2ig66851yr2yyf2f1az3sahkf8imjj3m4wmi") (f (quote (("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.1.0 (c (n "cache_loader_async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "097jmvifjwmlyi6q3ngd2ani809p21p6p4wk2sy5fcbqp7divqw1") (f (quote (("ttl-cache") ("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.1.1 (c (n "cache_loader_async") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gkn2c79dx2y4n0js160jmnr3spkl8qpi2mmk5l3vd4812ljwdvw") (f (quote (("ttl-cache") ("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.1.2 (c (n "cache_loader_async") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hsydl2d8cx5qjzyv18d32skk79bjmirvrb4hap4scmyfhik0vb0") (f (quote (("ttl-cache") ("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.2.0 (c (n "cache_loader_async") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1hlmngmfkwwi02jdzmk5vmg0bczlxdnbsgwpdqmd48zspc86781i") (f (quote (("ttl-cache") ("lru-cache" "lru") ("default"))))))

(define-public crate-cache_loader_async-0.2.1 (c (n "cache_loader_async") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "lru") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "06afglsy4kg458rr66sd5wcpbi1ynql6g2i0pkjyyz8q9nghizyf") (f (quote (("ttl-cache") ("lru-cache" "lru") ("default"))))))

