(define-module (crates-io ca ch cache-ru) #:use-module (crates-io))

(define-public crate-cache-ru-0.0.1 (c (n "cache-ru") (v "0.0.1") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("tokio-comp"))) (d #t) (k 0)))) (h "160z1yxv3szk24039nws5797x1q11294dv1mhcxgnwdvgkk8vihi")))

(define-public crate-cache-ru-0.0.2 (c (n "cache-ru") (v "0.0.2") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1k7hwy2gg7ka7f5wi25yya1kq9r7893hhlfq133i1p2lkvkmaqmb")))

