(define-module (crates-io ca ch cache_browns) #:use-module (crates-io))

(define-public crate-cache_browns-0.0.1-poc.0 (c (n "cache_browns") (v "0.0.1-poc.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "interruptible_polling") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bhlj9vf5394w2k8qva8w85f5yvymxlcqjrl0jbzqbg22anry2q4")))

