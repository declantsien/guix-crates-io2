(define-module (crates-io ca ch cache-macro) #:use-module (crates-io))

(define-public crate-cache-macro-0.4.0 (c (n "cache-macro") (v "0.4.0") (d (list (d (n "expiring_map") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g0ni57m4j6b1hyvx108k8667py7fbig2791yh560k2l6h82lnap")))

(define-public crate-cache-macro-0.4.1 (c (n "cache-macro") (v "0.4.1") (d (list (d (n "expiring_map") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bzlskvlgya4cg60fa2xcl2jnmgz6cw1h0r2ly32kj4ssrwp2v18")))

