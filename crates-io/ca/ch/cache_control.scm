(define-module (crates-io ca ch cache_control) #:use-module (crates-io))

(define-public crate-cache_control-0.1.0 (c (n "cache_control") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ac25z3zirp5liipwd5h4lg6vn34lb9gwp7rf99lr9y1r5hafp1g")))

(define-public crate-cache_control-0.2.0 (c (n "cache_control") (v "0.2.0") (h "1vir1yj1phyc2xsc68d086n5rskzjrgi9g4f41fjph876bxsbwhv")))

