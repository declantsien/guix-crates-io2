(define-module (crates-io ca ch cachemap2) #:use-module (crates-io))

(define-public crate-cachemap2-0.2.0 (c (n "cachemap2") (v "0.2.0") (d (list (d (n "abi_stable") (r ">=0.9") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "12v43y3a7jcnvcbrbkgixb0xbyamqvkpv2gd1y3wmvwzibva5fyp")))

(define-public crate-cachemap2-0.3.0 (c (n "cachemap2") (v "0.3.0") (d (list (d (n "abi_stable") (r ">=0.9") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.1") (o #t) (d #t) (k 0)))) (h "0vs3i31x1j0xqkfhxbjhdkfy90ffzwvpamgzbqpkn5daacqvvk38")))

