(define-module (crates-io ca ch cachier-rust) #:use-module (crates-io))

(define-public crate-cachier-rust-0.1.0-beta.0 (c (n "cachier-rust") (v "0.1.0-beta.0") (d (list (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (d #t) (k 0)))) (h "1i3s18zn3yan0hymlb1d84a1v1q7r4y6q7is8z74lz8hch214j5k")))

(define-public crate-cachier-rust-0.1.0-beta.1 (c (n "cachier-rust") (v "0.1.0-beta.1") (d (list (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (d #t) (k 0)))) (h "08l6w4qmc4nfvs53530inn5yjwjinvyaxd4zw2a5bpfs9lyzg2f3")))

