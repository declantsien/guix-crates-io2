(define-module (crates-io ca ch cache-fs) #:use-module (crates-io))

(define-public crate-cache-fs-0.1.0 (c (n "cache-fs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fuser") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04gns7dgnwchj46gvqxv46b546i29kpf8gi37wrgx8q4bz0s915v")))

