(define-module (crates-io ca ch cache) #:use-module (crates-io))

(define-public crate-cache-0.1.0 (c (n "cache") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "0733ryfnhgwq877hbgd5gs5rblp7q6aaq94hjk4xdhf2d2xighdy") (y #t)))

(define-public crate-cache-0.1.1 (c (n "cache") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "0kyp6y04ag4qb91vw13gds01qv0if3r1p0klk8d2pfhrbddm0aal") (y #t)))

(define-public crate-cache-0.1.2 (c (n "cache") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "1hqx8gfw2x686hgcpfhn5y5bdz5vny271mlglvi3spb2n1cx7q2s") (y #t)))

(define-public crate-cache-0.1.3 (c (n "cache") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "0dnv62xb6qbh5g3fis2yiwgw6w696fzy26yax3cyz6ahk2gw1xfc") (y #t)))

(define-public crate-cache-0.1.4 (c (n "cache") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "173h0bfa9h5yv2fdnf237lvhbxnvn7s7xpa9zfc6z9bfmm26dq4y") (y #t)))

(define-public crate-cache-0.2.0 (c (n "cache") (v "0.2.0") (d (list (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "stable-heap") (r "^0.1.0") (d #t) (k 0)))) (h "09wkp7hcy5vjcghnkrsjfsbmpdah9b3dpjd77xzbkpfzfpax23hq") (y #t)))

