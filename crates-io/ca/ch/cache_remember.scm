(define-module (crates-io ca ch cache_remember) #:use-module (crates-io))

(define-public crate-cache_remember-0.1.0 (c (n "cache_remember") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "0xqrd25hxw4ag1i323a67imhyspkgypgv5qkavzf12ir4p15c8aa") (y #t)))

(define-public crate-cache_remember-0.1.1 (c (n "cache_remember") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "054q8rp3dlayx5n9ch64jsdzqv37snslh2aklxqakg13hb205iqa") (y #t)))

(define-public crate-cache_remember-0.1.2 (c (n "cache_remember") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "0vdm2sbcq2iwzqa1pgdyqardddcyk896jkg5sapdmmhffdblc5sv") (y #t)))

(define-public crate-cache_remember-0.1.3 (c (n "cache_remember") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1izklvfh6qlwbq671cmax6h741zr2ndbwxkvajvq5h0fz9ah3mzd") (y #t)))

(define-public crate-cache_remember-0.1.4 (c (n "cache_remember") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1vvq589jx0h9kk2wzxakisi6b6xd8h62vgvv8x0nx0cqmmaydfdv") (y #t)))

