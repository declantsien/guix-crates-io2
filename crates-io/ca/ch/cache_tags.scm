(define-module (crates-io ca ch cache_tags) #:use-module (crates-io))

(define-public crate-cache_tags-1.0.0 (c (n "cache_tags") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0rr18cz46xlai3xascvcm5n1lgbixciiznwrkbagnzdlmz21hjm3")))

(define-public crate-cache_tags-1.0.2 (c (n "cache_tags") (v "1.0.2") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1lkdmgf33gx31dijh2d5sx2rc76gf05krhjakj6dfg47c7fiq8zf")))

(define-public crate-cache_tags-2.0.3 (c (n "cache_tags") (v "2.0.3") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1i8ba9x6fk220jp22k3xf0ws0xjs3dmqy7i6kcjsxagm7z5fc5qn")))

(define-public crate-cache_tags-2.0.4 (c (n "cache_tags") (v "2.0.4") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0m9wvg881c8dvjbspmv4ya0329gqbz32la3v80hhk44y2k12kaxn")))

(define-public crate-cache_tags-2.0.7 (c (n "cache_tags") (v "2.0.7") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0gqly825mwccllqfmsm3v8myq117xasn6ma8cvwcdcmc0bv5qzkf")))

(define-public crate-cache_tags-2.0.8 (c (n "cache_tags") (v "2.0.8") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1qcz64lzwm08h1n7jfjscaci3vgmmpc6d8y9n9y4wzw1lq3zy59h")))

(define-public crate-cache_tags-2.0.9 (c (n "cache_tags") (v "2.0.9") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1h0y1i2h1w8c6798fky1s3xrxhbnqz6fq4mw5f4x2yxnr71swzw9")))

(define-public crate-cache_tags-2.0.10 (c (n "cache_tags") (v "2.0.10") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "041qbh3vn2drw8nww0y7rrbrj99bvwzw954lb4cv5d0bi7zzv9cn")))

(define-public crate-cache_tags-2.0.11 (c (n "cache_tags") (v "2.0.11") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "16wy6ixn8qpp7i7qlqgm6qbav6z7y0cv1rqn365cvwbzccs0kpnp")))

(define-public crate-cache_tags-2.0.12 (c (n "cache_tags") (v "2.0.12") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1gbbq030m71j7k4fcj1ijwvm0vyh56axm6548rcnl484vdjq5sr0")))

(define-public crate-cache_tags-2.0.13 (c (n "cache_tags") (v "2.0.13") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1aglc4a61gv8iirgmf72vyg5h5y2897q409prignla4sn2v2mn8j")))

(define-public crate-cache_tags-2.0.14 (c (n "cache_tags") (v "2.0.14") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1pk2d1x8m2rf1z6z0cmxcx30r5qqw6k7v6d7lld74rlyviqjqp9q")))

(define-public crate-cache_tags-2.0.15 (c (n "cache_tags") (v "2.0.15") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1lsgy4plazrjhw8mxi3ndf8mjrd70qp3wl8q8agsrkqbbpjyp1s1")))

(define-public crate-cache_tags-2.0.16 (c (n "cache_tags") (v "2.0.16") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "007wc3r4bqyvm8s5d65aiwmpyfmcs8kd206yax1dr60qzagnyqxa")))

(define-public crate-cache_tags-2.0.17 (c (n "cache_tags") (v "2.0.17") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "0hg1a1zp939amnfssm5xi72gpm1wk75z7jpx5z7v7k6i6g6pr35a")))

