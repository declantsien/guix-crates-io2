(define-module (crates-io ca ch cachy) #:use-module (crates-io))

(define-public crate-cachy-0.1.0 (c (n "cachy") (v "0.1.0") (h "06lpb85gabnsjbc21i0dkcsgw91jq2k6fl3507j7q9hpjixr9vda")))

(define-public crate-cachy-0.2.0 (c (n "cachy") (v "0.2.0") (d (list (d (n "bus") (r "^1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "01az0gv5azr4491d6gsj0qjy679bqvi62irgyyphig6s4rjyp2c3")))

