(define-module (crates-io ca ch cache-testing-lib-1) #:use-module (crates-io))

(define-public crate-cache-testing-lib-1-0.1.0 (c (n "cache-testing-lib-1") (v "0.1.0") (h "0g1j6267q71rwznkf8q93f7xxvdwznvv5l1ifi6gb3g3mk06dswh")))

(define-public crate-cache-testing-lib-1-0.1.1 (c (n "cache-testing-lib-1") (v "0.1.1") (h "1hfr4pp11zj0j1clzjrb35fy96aickdgcrn17h7kkd6jwnrsv8y2")))

