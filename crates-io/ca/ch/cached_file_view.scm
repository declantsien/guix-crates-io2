(define-module (crates-io ca ch cached_file_view) #:use-module (crates-io))

(define-public crate-cached_file_view-0.1.0 (c (n "cached_file_view") (v "0.1.0") (h "1xdj9p346nsn3s8blv1l6clzl5i6k1dw857qm54kb6hpznxxgfd6")))

(define-public crate-cached_file_view-0.1.1 (c (n "cached_file_view") (v "0.1.1") (h "064qm0a7nf63lnym6ya5kw4skfy01rdzr2cr1yvrjkc5820mriab")))

(define-public crate-cached_file_view-0.1.2 (c (n "cached_file_view") (v "0.1.2") (h "1w1yfcs80i0wb2prz3mj2hnh7c676xc00pikh1v2spd385l864hz")))

(define-public crate-cached_file_view-0.1.3 (c (n "cached_file_view") (v "0.1.3") (h "1371w34wd67aanp762qpnhdh9knlz2p6jqxrs6fxp855p9qlm397")))

(define-public crate-cached_file_view-0.1.4 (c (n "cached_file_view") (v "0.1.4") (h "14541g5hgbllrk590pwan5vaa9446w8mpn5hszxi4546mazh2z7z")))

