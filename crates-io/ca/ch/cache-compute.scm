(define-module (crates-io ca ch cache-compute) #:use-module (crates-io))

(define-public crate-cache-compute-0.1.0 (c (n "cache-compute") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.12") (f (quote ("send_guard"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros" "test-util"))) (d #t) (k 0)))) (h "0r2qrs5fc0agbgs68sh8d45i3b2m68xfgpcnr4z82ry1ikx1had1")))

(define-public crate-cache-compute-0.2.0 (c (n "cache-compute") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.12") (f (quote ("send_guard"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros" "test-util"))) (d #t) (k 0)))) (h "0zxzf457md4ipmqj6jilyrhq75qf5f1n501lfvidahffjgf3rqlz")))

(define-public crate-cache-compute-0.3.0 (c (n "cache-compute") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.12") (f (quote ("send_guard"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1klaxz3dyv9db4ifrja6ccklmqa8gr9zw6bcr0s7j5m07df94967")))

