(define-module (crates-io ca ch cached-run) #:use-module (crates-io))

(define-public crate-cached-run-0.1.0 (c (n "cached-run") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "14gh5sa7r7hirabnv15wnnqmp6zga571y21zzzpciqihaw4nfc5p")))

(define-public crate-cached-run-0.1.1 (c (n "cached-run") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "0qn3bmg0ba9ycx2iy259v4f3j94ar8i8vsv9hh879j5bn8ggs9v6")))

(define-public crate-cached-run-0.1.2 (c (n "cached-run") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1sd3bf93rynza1vgbilnv32412kg9129949l1njs90nl0rprs35i")))

