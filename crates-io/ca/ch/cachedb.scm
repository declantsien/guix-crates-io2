(define-module (crates-io ca ch cachedb) #:use-module (crates-io))

(define-public crate-cachedb-0.0.0 (c (n "cachedb") (v "0.0.0") (h "03mdfii95271m32bhrx93l4an6d06bs4zl5077j1dfdx56j3mlwr")))

(define-public crate-cachedb-0.0.1 (c (n "cachedb") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0w7xnjcz9mgmkh1abrqxkq02gj283qg9l6cljh6sf20i7nclrqlz") (f (quote (("logging"))))))

(define-public crate-cachedb-0.1.0 (c (n "cachedb") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "13pmkax88qrcd2jc4yfyskkm4zqpdz4a1vxh2wkdv0hjz2lprq81") (f (quote (("logging"))))))

(define-public crate-cachedb-0.1.1 (c (n "cachedb") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0rbzvz3nzhsswf9qnsn7b8fgvh37naivnrkb45ccsz7c5z03wwmb") (f (quote (("logging"))))))

(define-public crate-cachedb-0.2.0 (c (n "cachedb") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "105kw82as6iys0ba7679wv9ib9scarixsrsf1xy53m0dhg47k36c") (f (quote (("logging"))))))

(define-public crate-cachedb-0.3.0 (c (n "cachedb") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "039qhsy0shs9jhhjiz6dixbb1nniqfiv2496cf7wxx8l919sclbi") (f (quote (("logging"))))))

(define-public crate-cachedb-0.4.0 (c (n "cachedb") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "19szi44pqxpyy07jp3rqm6mijcr51smsxq03p0rspc4kp4rraa8v") (f (quote (("logging"))))))

(define-public crate-cachedb-0.4.1 (c (n "cachedb") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0y8bbqhw2filr2izbav267wwa16lpzr90cpf93aj80ldxlxdwpjy") (f (quote (("logging"))))))

(define-public crate-cachedb-0.4.2 (c (n "cachedb") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1fynwgps2c9wsllhsm32pqjkq9kfm32lyr8r037qiakkz7kwjqjj") (f (quote (("logging"))))))

(define-public crate-cachedb-0.4.3 (c (n "cachedb") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0d4v4c6w3bzk81agrv3cw7902w6qpakgwj2702k0v03jk1qdz9lp") (f (quote (("logging"))))))

(define-public crate-cachedb-0.4.4 (c (n "cachedb") (v "0.4.4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0czs2ww43al8km8pnkrzpgyjmba2m7x5wmcfx2iajshswg5hg99m") (f (quote (("logging"))))))

(define-public crate-cachedb-0.5.0 (c (n "cachedb") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jccrsnj658rsay6fsmjcycyv9msgqaskjdmjl6y5dp4w4j34cjf") (f (quote (("logging"))))))

(define-public crate-cachedb-0.6.0 (c (n "cachedb") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0qw8m4yxc6s78qyn2z66nmd7fr3r8kx8mmnwmkz4m1rr5gzwl4y5") (f (quote (("logging") ("default" "logging"))))))

(define-public crate-cachedb-0.7.0 (c (n "cachedb") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_method") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1rcsc7gaz7vgnbkrynx2a7w1jkhx9a6xgz41ymwbqcc8phwjx2ss") (f (quote (("logging") ("default" "logging"))))))

(define-public crate-cachedb-0.8.0 (c (n "cachedb") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_method") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1yyxraydb99id13q991n268hbk35d8f1pdldand9qjzpjhn5q4pq") (f (quote (("logging") ("default" "logging"))))))

(define-public crate-cachedb-0.8.1 (c (n "cachedb") (v "0.8.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_method") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0xwn0lgjy6ri93bk7cwb4n40k8r7m1sxhs59vjf5dqhrs9y2dlzw") (f (quote (("logging") ("default" "logging"))))))

(define-public crate-cachedb-0.8.2 (c (n "cachedb") (v "0.8.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_method") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "01nbazrcxpj7549hhp3n3p2nbqyv0xf56xnc7bg6cnz9ngbadfd4") (f (quote (("logging") ("default" "logging"))))))

