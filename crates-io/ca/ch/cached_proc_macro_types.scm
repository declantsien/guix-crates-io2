(define-module (crates-io ca ch cached_proc_macro_types) #:use-module (crates-io))

(define-public crate-cached_proc_macro_types-0.1.0 (c (n "cached_proc_macro_types") (v "0.1.0") (h "0qrni8q6vj1l1d62fgyln1s1scchk2q9jv8whl0p6dmlj58r4krs")))

(define-public crate-cached_proc_macro_types-0.1.1 (c (n "cached_proc_macro_types") (v "0.1.1") (h "1h3gw61v1inay4g3b8pirxlz18m81k63dw2q18zj9fnmidmkds5d")))

