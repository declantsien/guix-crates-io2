(define-module (crates-io ca ch cachesim) #:use-module (crates-io))

(define-public crate-cachesim-0.1.0 (c (n "cachesim") (v "0.1.0") (h "0mvyhf26ayzqilzsr8z17rywsyavrlbk1gfrsz1r36iwsjlr060i")))

(define-public crate-cachesim-0.1.1 (c (n "cachesim") (v "0.1.1") (h "0zps7fa4rp3nhi035n3y7qphajq33k8gqnx5d45657frxfvnc9v7")))

(define-public crate-cachesim-0.1.2 (c (n "cachesim") (v "0.1.2") (h "1gldmnwzy2war2pf143zispqm5fl8akrd0nf6w3p9d8g9pwhjxya")))

(define-public crate-cachesim-0.1.3 (c (n "cachesim") (v "0.1.3") (h "1h9ha3h4pwm3mqw0rfixnxjhky5xfp3xdcyfag1d8fw7k9305z66")))

