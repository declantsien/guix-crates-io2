(define-module (crates-io ca ch cache-macro-stable-rust) #:use-module (crates-io))

(define-public crate-cache-macro-stable-rust-0.4.1 (c (n "cache-macro-stable-rust") (v "0.4.1") (d (list (d (n "expiring_map") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p3shabd1jzzh8b2rx7nflidigmj8mq332k99ig6bx558p96caxl")))

