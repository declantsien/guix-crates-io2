(define-module (crates-io ca ch cache-this-proc-macro) #:use-module (crates-io))

(define-public crate-cache-this-proc-macro-0.1.0 (c (n "cache-this-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07d1ps12f7867v4s8dbhr1ffrqiq9izgz415y7xxija36lvbvf2d")))

(define-public crate-cache-this-proc-macro-0.1.1 (c (n "cache-this-proc-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0az19yznmyvsdg1crdaz6r4nkbig55g49ihcs99cshl6cwk00iyv")))

(define-public crate-cache-this-proc-macro-0.1.2 (c (n "cache-this-proc-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4dl1w924xrbhmvzai03hgqbaz06x6m7z90hdxq3vvr2yc64d1f")))

