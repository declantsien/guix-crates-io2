(define-module (crates-io ca ch cache_bust_core) #:use-module (crates-io))

(define-public crate-cache_bust_core-0.1.0 (c (n "cache_bust_core") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0qwxh9siv8c0k3z81z4gvjbpw8an91mmxzdqzyyapg2av5cr1748")))

(define-public crate-cache_bust_core-0.2.0 (c (n "cache_bust_core") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1chny8fn0k5vr3vj41fmvaxlp1nf4b05c793sxwawgz8l3xkscv5")))

