(define-module (crates-io ca ch cached_property) #:use-module (crates-io))

(define-public crate-cached_property-0.1.0 (c (n "cached_property") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("clone-impls" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0f1sqd13x52gxff99by8lj11isacaqyxasyjmvy60jhszf4hhim6")))

