(define-module (crates-io ca ch cachedhash) #:use-module (crates-io))

(define-public crate-cachedhash-0.1.0 (c (n "cachedhash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 2)))) (h "10lp5qwibg77id20h8njnw3lhvvz7d7p97zc3v5lmsypbbyw4m93") (y #t)))

(define-public crate-cachedhash-0.1.1 (c (n "cachedhash") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 2)))) (h "1lw8hdbns2654w3cq8xs341mq5vgkqds19pab1rqynch78r8qla0")))

(define-public crate-cachedhash-0.1.2 (c (n "cachedhash") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 2)))) (h "0cr7dxnsyrxx153zi8kn248dzf709a5as94cilgzz09alvvmhlaa")))

(define-public crate-cachedhash-0.2.0 (c (n "cachedhash") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 2)))) (h "1xnwk67692g57igijyif7mwbpj7w7c6wz5bg5m2f1c0gfy0pjjxp")))

