(define-module (crates-io ca ch cache_bust_macro) #:use-module (crates-io))

(define-public crate-cache_bust_macro-0.1.0 (c (n "cache_bust_macro") (v "0.1.0") (d (list (d (n "cache_bust_core") (r "^0.1.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "1wngff6ivbg15a18yhf7q9gifmy27zfqymrrfh0mj5j8d6ksy2w9")))

(define-public crate-cache_bust_macro-0.2.0 (c (n "cache_bust_macro") (v "0.2.0") (d (list (d (n "cache_bust_core") (r "^0.2.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "0blndrcg68148308gbzgd2ks0g2h39lda8h958vl58fn91cl0q10")))

