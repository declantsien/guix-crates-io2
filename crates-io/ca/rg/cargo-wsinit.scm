(define-module (crates-io ca rg cargo-wsinit) #:use-module (crates-io))

(define-public crate-cargo-wsinit-0.1.0 (c (n "cargo-wsinit") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0b5qznqr3z0jnnhb19l3ar34lh1gdxchlzp8q7b1zcdk592n5hkk")))

(define-public crate-cargo-wsinit-0.1.1 (c (n "cargo-wsinit") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "12c78h09i6y16f0rs3dcrxjlkfh4is97m33krxqldz5x2b6yr8zx")))

(define-public crate-cargo-wsinit-0.1.2 (c (n "cargo-wsinit") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1fwppb0jdjcwhi0jdj4v2x5i90y437zpn3i2mwz031vkrsq2lr11")))

(define-public crate-cargo-wsinit-0.1.3 (c (n "cargo-wsinit") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rc5p33r7vxv30qvbpznyvs456k4qbih1w52g4rdzxa5h0wkjwkv")))

(define-public crate-cargo-wsinit-0.1.4 (c (n "cargo-wsinit") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "01x6d0lsyrsasxndllrdy9p1hmcabp0f5sh37q83d1qf0mblmibg")))

