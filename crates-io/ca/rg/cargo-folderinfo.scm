(define-module (crates-io ca rg cargo-folderinfo) #:use-module (crates-io))

(define-public crate-cargo-folderinfo-0.1.0 (c (n "cargo-folderinfo") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1xlcclcpyv3hr8jsxrk901r73fqqcl2amjbrfxzph3wy9brcvig5")))

(define-public crate-cargo-folderinfo-0.2.0 (c (n "cargo-folderinfo") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0fnxfpdaj04pb09n887k91lzkw2q37l8jbc0h3pv14sik28m2vsf")))

