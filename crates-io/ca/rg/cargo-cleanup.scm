(define-module (crates-io ca rg cargo-cleanup) #:use-module (crates-io))

(define-public crate-cargo-cleanup-0.1.0 (c (n "cargo-cleanup") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "0my74ja72vdcpczpdchj523h3ps0c5966rasjxjrljbhq67av982")))

(define-public crate-cargo-cleanup-0.1.1 (c (n "cargo-cleanup") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "0qybmaa3q3z72ky79byxmfwb6pvnryykx1a6pn6d4n3a8ziyk1bd")))

(define-public crate-cargo-cleanup-0.1.2 (c (n "cargo-cleanup") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 0)))) (h "1jmnsl9mnkgrhhh8xsg8g2pbz0g1hqmmxhrw01j4pz7kilb864sw")))

