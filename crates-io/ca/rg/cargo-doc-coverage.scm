(define-module (crates-io ca rg cargo-doc-coverage) #:use-module (crates-io))

(define-public crate-cargo-doc-coverage-0.1.0 (c (n "cargo-doc-coverage") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mj07b9j9675f89g19gsf5lziddr16myfgbww6v7pk9z37c3267h")))

(define-public crate-cargo-doc-coverage-1.0.0 (c (n "cargo-doc-coverage") (v "1.0.0") (d (list (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qa5a674vrvg30nqa1hwq3dbn6ykff03d4ihijc9ays86qv5dcnf")))

