(define-module (crates-io ca rg cargo-my-bin) #:use-module (crates-io))

(define-public crate-cargo-my-bin-0.1.0 (c (n "cargo-my-bin") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "0fvicyvncykc7cy5b7d6pazj2mx11hidqd5c79bqv1c9bmikm8jh")))

(define-public crate-cargo-my-bin-0.1.1 (c (n "cargo-my-bin") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1ny9sz7yg54lrfvz768za4i5vqyc5jhl62m9zsrgirr897i82fjg")))

(define-public crate-cargo-my-bin-0.1.2 (c (n "cargo-my-bin") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1asib3cshy0481hmyxc630fhfjc02nv7szl1jla1w5hz0x4jhgcd")))

(define-public crate-cargo-my-bin-0.2.0 (c (n "cargo-my-bin") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1hdwf9b7iy4fbb0x1ba30bm5isjip6xz51yl4byqc015y3jcm40s")))

(define-public crate-cargo-my-bin-0.2.1 (c (n "cargo-my-bin") (v "0.2.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "0wj29q2q0yf49dhrx6mv4wl720m1czcjxksp86zvd0hylxql68z6")))

