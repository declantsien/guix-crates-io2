(define-module (crates-io ca rg cargo-newcpp) #:use-module (crates-io))

(define-public crate-cargo-newcpp-0.2.16 (c (n "cargo-newcpp") (v "0.2.16") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0wpm6gy5rpi1yz19nx7r7g4ismf3xc0mj411xyw3gwb79b10dl4l")))

(define-public crate-cargo-newcpp-0.2.17 (c (n "cargo-newcpp") (v "0.2.17") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0z0h1v7m6kksa9ljj4fjmp3k5hdxydzgxz8km7imaccpzrplv825")))

(define-public crate-cargo-newcpp-0.3.20 (c (n "cargo-newcpp") (v "0.3.20") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0fayyqb42sz2bf3xwf4if516g83xqcrxlnvmnc7il5qvs8id4j8p") (f (quote (("gtest") ("default" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.3.22 (c (n "cargo-newcpp") (v "0.3.22") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "17w52ssd8ryvd76bxrgyqds0j3pfa96a82w0rkxniw4c948xlq07") (f (quote (("gtest") ("default" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.4.23 (c (n "cargo-newcpp") (v "0.4.23") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "08zc8rwanhgyjhrvfy09w9hih6hq70y0bbpg29k8vabh5nbs5f3d") (f (quote (("gtest") ("default" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.4.24 (c (n "cargo-newcpp") (v "0.4.24") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "1f5anwypfbkaqsgssna18rdacsrkgcbkk17r044vi38hm5rkd9hq") (f (quote (("gtest") ("default" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.4.25 (c (n "cargo-newcpp") (v "0.4.25") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0vpqan30rb82539b6jlc9mdxigfmhp2cd6dwqqp5qd0zs42ahs9p") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.4.26 (c (n "cargo-newcpp") (v "0.4.26") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0kvgh4bwsqcdhq9z1mxw1iqwsziy6628qxl79kp9mjcnmm3bmjjp") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.4.27 (c (n "cargo-newcpp") (v "0.4.27") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0h23x985mbnlxdydbbw5vk3v24j0x2fj2895qdfpl6885jvm45i8") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.5.0 (c (n "cargo-newcpp") (v "0.5.0") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0igns9ilxc4849gxm1icw6lj6kilc9b35mmc9yp7mmzlzb97mqhx") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.5.1 (c (n "cargo-newcpp") (v "0.5.1") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "16l8fny9xzc41rn94mvhp3gnny5mfzk6ikh83q5a71s6cj9p0hmi") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

(define-public crate-cargo-newcpp-0.5.2 (c (n "cargo-newcpp") (v "0.5.2") (d (list (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)))) (h "0ykyvfrxa43yc8gp6aqllla9cq37irj3ghz9iibpvkq06ik5ryvq") (f (quote (("new") ("gtest") ("default" "new" "build" "gtest") ("build"))))))

