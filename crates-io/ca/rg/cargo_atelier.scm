(define-module (crates-io ca rg cargo_atelier) #:use-module (crates-io))

(define-public crate-cargo_atelier-0.1.0 (c (n "cargo_atelier") (v "0.1.0") (d (list (d (n "atelier_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1nfzl88wmgxxbrymz3rq7kpmhyaja2vdac502wnfyzd91z4fffr2")))

(define-public crate-cargo_atelier-0.1.1 (c (n "cargo_atelier") (v "0.1.1") (d (list (d (n "atelier_lib") (r "^0.1.4") (f (quote ("json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0ilqj7f61vg7dnng91sbq7ymyw49hga7y0i3kb8ljl11h4pq7hx4")))

(define-public crate-cargo_atelier-0.1.2 (c (n "cargo_atelier") (v "0.1.2") (d (list (d (n "atelier_lib") (r "^0.1.5") (f (quote ("json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1kkhbcpy0sff6pq8y3g273wmvhiyfcch3xwnmkp4c6ri99xy2ir2")))

(define-public crate-cargo_atelier-0.2.0 (c (n "cargo_atelier") (v "0.2.0") (d (list (d (n "atelier_lib") (r "^0.2.0") (f (quote ("json" "openapi" "smithy" "uml"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0xai42x326cdf5wih6r2xndgbiimpyfd9sj7122qc0gwqpki178h") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.1 (c (n "cargo_atelier") (v "0.2.1") (d (list (d (n "atelier_lib") (r "^0.2.1") (f (quote ("describe" "json" "openapi" "smithy" "uml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1bl6vha09qjjggxa9xghljrb4njyh2ncg3aj1jx8z48k0985nk4s") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.2 (c (n "cargo_atelier") (v "0.2.2") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy" "uml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "149kpbr6nnga7ganji947a1l5v64f4wc5022zm9hfcpyafclpfmq") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.3 (c (n "cargo_atelier") (v "0.2.3") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy" "uml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0ba8va65x6d60csqx5bw95zzqzdsgj334bgv7hgicqhkjk546s93") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.4 (c (n "cargo_atelier") (v "0.2.4") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1pmbc72qp84x28fvnnxn63h5s4gspqm35ghz4g5csr235jdr6nsy") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.5 (c (n "cargo_atelier") (v "0.2.5") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0ad3fgc3bn5jhn6gn98ks2x0q61iw80ynk9vjh6qbjjq8mrb7yc9") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.6 (c (n "cargo_atelier") (v "0.2.6") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0ciyziddwiigmilkbk75dmlvcfw0xjy9z28h5szvj3s7x0pja6c9") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-cargo_atelier-0.2.7 (c (n "cargo_atelier") (v "0.2.7") (d (list (d (n "atelier_lib") (r "~0.2") (f (quote ("describe" "json" "openapi" "smithy"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1vam25351d1xfnxq51s7g300shccrs341ipb4lw7i65pn6z03yxf") (f (quote (("default" "color") ("color" "colored"))))))

