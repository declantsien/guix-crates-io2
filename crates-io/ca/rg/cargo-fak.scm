(define-module (crates-io ca rg cargo-fak) #:use-module (crates-io))

(define-public crate-cargo-fak-0.1.0 (c (n "cargo-fak") (v "0.1.0") (h "18sjk8baqa0jrmdd90y3mmziwq9sdzprhxw4x7l49sbdcclkk7hy") (y #t)))

(define-public crate-cargo-fak-1.0.0 (c (n "cargo-fak") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "12q0b0hlhzarv4gi4yxhagn2r1cz34wzr83nc7469syc7fwpmym6") (y #t)))

(define-public crate-cargo-fak-1.0.1 (c (n "cargo-fak") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0ph1p21d6b4p1mghnlm134gmwx23vgrdx8604qb501xc9r5244v9")))

(define-public crate-cargo-fak-1.0.2 (c (n "cargo-fak") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0qyn3ib2xnd2g2v49bnjlpj2cg0gph2mrzc3sm45dbrpjf23vx9j")))

(define-public crate-cargo-fak-1.0.3 (c (n "cargo-fak") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0x4l8xay4dnaz1j4aw02d2g308x60ir2w12bvxriasg59x42412r")))

