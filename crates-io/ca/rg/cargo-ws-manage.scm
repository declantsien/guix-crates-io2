(define-module (crates-io ca rg cargo-ws-manage) #:use-module (crates-io))

(define-public crate-cargo-ws-manage-0.0.0 (c (n "cargo-ws-manage") (v "0.0.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)))) (h "1ms2wwvhci98jak6phkfl4abrp6nqj8gfkx68fslxqm52fjls9ib") (y #t)))

(define-public crate-cargo-ws-manage-0.1.0 (c (n "cargo-ws-manage") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "test-context") (r "^0.1.4") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "069ndqrjdbdl1y051l5j2nfvpxn9q9gdn1fkri6k4qn66y9pg008") (y #t)))

(define-public crate-cargo-ws-manage-0.2.0 (c (n "cargo-ws-manage") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "test-context") (r "^0.1.4") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1h40vnp965h3mk1xhjp41l6gyryx18wdjhzs0w0zzsgnsj4q1shf") (y #t)))

