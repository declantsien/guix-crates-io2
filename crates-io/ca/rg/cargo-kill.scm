(define-module (crates-io ca rg cargo-kill) #:use-module (crates-io))

(define-public crate-cargo-kill-0.1.0 (c (n "cargo-kill") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "spinners") (r "^3.0.1") (d #t) (k 0)))) (h "1isa6f3r5iifhcnbwn1p18rsk2rczyi91i3a56iqpvzxy8m0fy6r")))

