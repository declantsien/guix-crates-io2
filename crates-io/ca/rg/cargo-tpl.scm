(define-module (crates-io ca rg cargo-tpl) #:use-module (crates-io))

(define-public crate-cargo-tpl-0.1.0 (c (n "cargo-tpl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fm3cvzqajjjbknaw4cg5zdfc9v6vawjqiv8885m3dl7qahqdmc3") (y #t)))

(define-public crate-cargo-tpl-0.1.1 (c (n "cargo-tpl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1l05am5j949l3vh9sq8rni1fq2y56ds3j5jp4imwrq3v056an48h") (y #t)))

(define-public crate-cargo-tpl-0.1.2 (c (n "cargo-tpl") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09drkx3sx462r83n7wqqbzrlwbqs5ljf2jacjm54m4plxmyqfdk7") (y #t)))

(define-public crate-cargo-tpl-0.1.3 (c (n "cargo-tpl") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09cdpng0z6fw6i1mqvg59mqr8a23y8jxvalb0dgxdbsrgjarwf01") (y #t)))

