(define-module (crates-io ca rg cargo-dependency-inheritor) #:use-module (crates-io))

(define-public crate-cargo-dependency-inheritor-0.1.0 (c (n "cargo-dependency-inheritor") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "0bjgw8fji7mlndj9h2rx83qsqw9lgmc81l7c5xfd6mp9dqf444im")))

(define-public crate-cargo-dependency-inheritor-0.1.1 (c (n "cargo-dependency-inheritor") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dunce") (r "^1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "0nb7yidmaax276f1s4bgffriha81z0nvbqm8jgkh3ab1ssvf3pjb")))

