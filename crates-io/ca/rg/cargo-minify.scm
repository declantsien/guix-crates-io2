(define-module (crates-io ca rg cargo-minify) #:use-module (crates-io))

(define-public crate-cargo-minify-0.5.0 (c (n "cargo-minify") (v "0.5.0") (d (list (d (n "cargo_metadata") (r "^0.17") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "glob-match") (r "^0.2.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.49.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0ais0w5gqg1rrwhwi7b9jy90ynxp5gpzdxpn3p51zlmgbprm9279")))

