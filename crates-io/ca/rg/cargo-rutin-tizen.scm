(define-module (crates-io ca rg cargo-rutin-tizen) #:use-module (crates-io))

(define-public crate-cargo-rutin-tizen-0.1.0 (c (n "cargo-rutin-tizen") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "~0.8.0") (d #t) (k 0)) (d (n "sxd-document") (r "~0.3.2") (d #t) (k 0)) (d (n "sxd-xpath") (r "~0.4.2") (d #t) (k 0)) (d (n "toml") (r "~0.5.8") (d #t) (k 0)))) (h "0xkwfdv1qad3yihisv833i81vd33gxn2qg3a8z5ggwfzpgxpw7yk")))

