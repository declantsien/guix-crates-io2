(define-module (crates-io ca rg cargo-local-pkgs) #:use-module (crates-io))

(define-public crate-cargo-local-pkgs-0.1.0 (c (n "cargo-local-pkgs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1fiwg20x6ci7yiljqg7pc16ngmzy9hrqwlwjq0nii7sv75laqn2b") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.1.1 (c (n "cargo-local-pkgs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "0m7lkqbyk2lj83nlrn3084cncxzm9qqzdipl2wqbyrfr8bs8bmr0") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.2.0 (c (n "cargo-local-pkgs") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "12z0wxbzngxfyh5y9pf2zm6sgi9rzg3sm7185bm299fbilw165gb") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.2.1 (c (n "cargo-local-pkgs") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1vz7kzr560j216wca8bgrknqy68lmpcv1p2fq8pi3hd3pr7l94b6") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.3.0 (c (n "cargo-local-pkgs") (v "0.3.0") (d (list (d (n "clap") (r "^2.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0rxscpfbbnx2rqkz0irgpvrb4fpp7zxbz788zgfnsngmv5zfybi2") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.3.1 (c (n "cargo-local-pkgs") (v "0.3.1") (d (list (d (n "clap") (r "^2.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "17i2nlajhn22jlky34vp21qgyzhr7z45hvq6ckqhx02arpgxa50d") (f (quote (("logging" "env_logger"))))))

(define-public crate-cargo-local-pkgs-0.3.2 (c (n "cargo-local-pkgs") (v "0.3.2") (d (list (d (n "clap") (r "^2.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0fqz2s85ra17gq2gadj3qdkpfgifjrfd3m8v1hc8ffhcij5yghxg") (f (quote (("logging" "env_logger"))))))

