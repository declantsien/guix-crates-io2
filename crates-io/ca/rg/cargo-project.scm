(define-module (crates-io ca rg cargo-project) #:use-module (crates-io))

(define-public crate-cargo-project-0.1.0 (c (n "cargo-project") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0c4fmmaz3mcmrg334ibl0h1xcfvnnsa5xyngs06q03mrbi36qapx")))

(define-public crate-cargo-project-0.2.0 (c (n "cargo-project") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "11ggms221qby87fs3fz0xyr0yzymg6ij01k592pxsbkm0bh9fxqq")))

(define-public crate-cargo-project-0.2.1 (c (n "cargo-project") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0a61irmyz3hr65q3g4yzbphlkyd4d9nkn6x7av8jyl1d2gcrms21")))

(define-public crate-cargo-project-0.2.2 (c (n "cargo-project") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "06wj67svskq3il1yhq81gmrpx04pbv5475v0vrhjk7slbgpl60d3")))

(define-public crate-cargo-project-0.2.3 (c (n "cargo-project") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1c744gib5y4wdnixasn51m3xs6c5y0cbd8jlqnda58kpv5zl2x8k")))

(define-public crate-cargo-project-0.2.4 (c (n "cargo-project") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0fmxxia30vvbad77wdhdf11z6cwbbfxaa3dj8znqxg7k5hdmnwvb")))

(define-public crate-cargo-project-0.2.5 (c (n "cargo-project") (v "0.2.5") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "02cb5b0igfj6ing8nz60wjwhyyxsbdwwrkjgxqg593yh4k12lpb3") (y #t)))

(define-public crate-cargo-project-0.2.6 (c (n "cargo-project") (v "0.2.6") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1gcgwpc3q728nk38360hyd2iyiiidvigqxirqwnrvj3g34hpl1wk")))

(define-public crate-cargo-project-0.2.7 (c (n "cargo-project") (v "0.2.7") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0kw7wfljcczh8ndsynpk4pn3fip38zwy8lb5lv39gbzz9fbppr84")))

(define-public crate-cargo-project-0.3.0 (c (n "cargo-project") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1w3az77bgqy48ai9v00ipqd610x88r6vfxip5iqawnvgx6f5swvr")))

