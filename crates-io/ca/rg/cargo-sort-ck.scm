(define-module (crates-io ca rg cargo-sort-ck) #:use-module (crates-io))

(define-public crate-cargo-sort-ck-0.1.0 (c (n "cargo-sort-ck") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ib7fp1zfhhgjphyn9gmclkk5nnmnhyln5z0y8ijsin2r4a0vdak")))

(define-public crate-cargo-sort-ck-0.1.1 (c (n "cargo-sort-ck") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ckja8crg8d5mxb8nd79nj91i2gp6g1fki0nsg18s968j3rd7mrs")))

(define-public crate-cargo-sort-ck-0.1.2 (c (n "cargo-sort-ck") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02hz9ac1gvcyfg73vxh6ww3dlkhz84430c81md0rzld1zsax641g")))

(define-public crate-cargo-sort-ck-0.2.0 (c (n "cargo-sort-ck") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "18iv3q20h1dj19cmhzifkb40rqzkisrbcrih3c9igr0rzyajn24b")))

(define-public crate-cargo-sort-ck-0.2.5 (c (n "cargo-sort-ck") (v "0.2.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0akiv692cvd6fjak32fz2iv95h1slfphdf91fdnxnf7jcwlkbqp7")))

(define-public crate-cargo-sort-ck-0.2.7 (c (n "cargo-sort-ck") (v "0.2.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1x852n09ba2csnkzjjhjclp3hf2bwdf9nzl8ksnfx3kijj4w5wh2")))

(define-public crate-cargo-sort-ck-0.3.0 (c (n "cargo-sort-ck") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0a1mfcksaczzvvx30bfddidh089izqjqws18y901pc70drpc44rh")))

(define-public crate-cargo-sort-ck-0.3.2 (c (n "cargo-sort-ck") (v "0.3.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0k4y9526w90vy7j9425pivx3bmxf98g012gaw3qf9347hq4qwplh")))

(define-public crate-cargo-sort-ck-0.3.3 (c (n "cargo-sort-ck") (v "0.3.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "129bfp8nwm6y3awlfv1147znzgzcqqaiykg8prxdk4i33787p3px")))

(define-public crate-cargo-sort-ck-0.3.4 (c (n "cargo-sort-ck") (v "0.3.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1m1scbqasxxc1my4zg6yd8lab8vrbdj2mpxwpv9jsa16nsjj8ihx")))

(define-public crate-cargo-sort-ck-0.3.5 (c (n "cargo-sort-ck") (v "0.3.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1f8489h7jbxgw3nfxi5vd6v969l0lhqfvmpvg1hd30ccc1h9a1f5")))

(define-public crate-cargo-sort-ck-1.0.0 (c (n "cargo-sort-ck") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0shbscz59pvp8lmrv6k98my18nvgmb0j37jwgm8bwdbw6bn5qanx")))

(define-public crate-cargo-sort-ck-1.0.1 (c (n "cargo-sort-ck") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0dvm6x4c8gy05m336l04nikkdfk1zaf59syczcfbm7nb5h1v9ly4")))

(define-public crate-cargo-sort-ck-1.1.0 (c (n "cargo-sort-ck") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1f201aqckr43ig3152kw9qi2b1k29hic32zpkb6mcipmjkaa429f")))

(define-public crate-cargo-sort-ck-1.5.0 (c (n "cargo-sort-ck") (v "1.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "toml-parse") (r "^0.2.5") (d #t) (k 0)))) (h "1rmpgiic9lm5kzkbidb7g0lafmsd0q80inkhrcqyfx19nvi9205r")))

(define-public crate-cargo-sort-ck-2.0.0 (c (n "cargo-sort-ck") (v "2.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "toml-parse") (r "^0.2.7") (d #t) (k 0)))) (h "1m01m5ajrhkvqyaphzd3afq2lwp6xhdna8wwrgk2apy9rkvaq3fg")))

(define-public crate-cargo-sort-ck-2.1.0 (c (n "cargo-sort-ck") (v "2.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "toml-parse") (r "^0.2.9") (d #t) (k 0)))) (h "17q2lxwczhry20ca9yhd9g5c7jjjs79cj7qr7llxrb7y7q0kaxcr")))

(define-public crate-cargo-sort-ck-2.1.1 (c (n "cargo-sort-ck") (v "2.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "toml-parse") (r "^0.2.11") (d #t) (k 0)))) (h "1b8kh98wyv639xpsc6rb38823i6gx62fnvq8k4m208knbygcixz1")))

(define-public crate-cargo-sort-ck-2.2.2 (c (n "cargo-sort-ck") (v "2.2.2") (h "0j1wr8xpd0n4sq7wvlmvxgj7dl5nf1qjy1r4hv3x1nicmpyw7vy0")))

