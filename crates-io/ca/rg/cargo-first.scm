(define-module (crates-io ca rg cargo-first) #:use-module (crates-io))

(define-public crate-cargo-first-0.1.0 (c (n "cargo-first") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "027lgpj9afvya4p49f2ydvsxxpj2bsn0pjn5n6894v4g8ijz9zqh")))

(define-public crate-cargo-first-0.1.1 (c (n "cargo-first") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "15shb37fy6mcz32fazx0jh4dfdh0d1nhw4mn6jrqp3kdgk6yqka8")))

(define-public crate-cargo-first-0.1.2 (c (n "cargo-first") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "00yscc004a1ka1xs7bqvaiqi4d5mdqqxzpzr7ndbbgw04763c8jr")))

(define-public crate-cargo-first-0.1.3 (c (n "cargo-first") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "0sqy0m6bpsgv2zz59c7700lwmkm6pmig3rmblyra86r72wy813lb")))

(define-public crate-cargo-first-0.1.4 (c (n "cargo-first") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "01684ccyqf18hwrm2yq2azk05gvcz5gg1ddp5i7acghqk7hq2lcp")))

