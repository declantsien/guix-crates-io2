(define-module (crates-io ca rg cargo-generate-type) #:use-module (crates-io))

(define-public crate-cargo-generate-type-0.1.0 (c (n "cargo-generate-type") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0bb9g4vx77nscin539d3jsw05cqi05jwgyzdvk8s5ya7ihva7q71")))

(define-public crate-cargo-generate-type-0.1.1 (c (n "cargo-generate-type") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0cvngzk47cbw5dy1vnx0rrncz92f1zfn2hx6h4d1241gscqpia08")))

(define-public crate-cargo-generate-type-0.1.2 (c (n "cargo-generate-type") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1c3850kscdaajqs7rbf37fx6f356w3iddkqiwzyb6r2zcq1d4igi")))

(define-public crate-cargo-generate-type-0.1.3 (c (n "cargo-generate-type") (v "0.1.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)))) (h "0xi575rhaaskz68bz5h8r5iqgbkcvra1yqdysq7r0ylmxn4wrys4")))

(define-public crate-cargo-generate-type-0.1.4 (c (n "cargo-generate-type") (v "0.1.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)))) (h "1xyzyg8phikxj5a6andvnn07jp9y0hjqnjab1rmir78nqhpsf138")))

