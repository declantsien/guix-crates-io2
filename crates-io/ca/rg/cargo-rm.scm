(define-module (crates-io ca rg cargo-rm) #:use-module (crates-io))

(define-public crate-cargo-rm-1.0.0 (c (n "cargo-rm") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wgnyxlxci67g076hhd3k245qi67pj23nv8iwxva0jj6a8mhpj3z") (y #t)))

(define-public crate-cargo-rm-1.0.1 (c (n "cargo-rm") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nmlrzkj4p7dg0jkznacb6gagyaz8xyjnvxpa18scwim1x22hdz2") (y #t) (r "1.38")))

(define-public crate-cargo-rm-1.0.2 (c (n "cargo-rm") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-subcommand-metadata") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rjglzf65bz6jwg2nyn0a10x9q8di7p7bgjd6069q4j7nm7aai4v") (y #t) (r "1.38")))

(define-public crate-cargo-rm-1.0.3 (c (n "cargo-rm") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-subcommand-metadata") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("deprecated" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qq6m9hi9lqwxqaclnshaa1crslkagx2k65gbv42s6l6ix1h0l7k") (y #t) (r "1.58")))

