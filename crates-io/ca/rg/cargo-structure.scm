(define-module (crates-io ca rg cargo-structure) #:use-module (crates-io))

(define-public crate-cargo-structure-0.1.0 (c (n "cargo-structure") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1qwjgf5n6c48mgsr2ja34gp2qx4vlqifkrfiihssdzfzwgi35mfd")))

(define-public crate-cargo-structure-0.1.1 (c (n "cargo-structure") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qzl9kbyp3lhlzzhwijm4r5gmmglgq4znswycpdarj02v2fphyi9")))

(define-public crate-cargo-structure-0.2.0 (c (n "cargo-structure") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qdn4bzzf7fa1djh79q1jm6ac504wn8q91gx3xvm6s0xff5xshn9")))

(define-public crate-cargo-structure-0.3.0 (c (n "cargo-structure") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "07klghr7q2hiywbpi6irzx0kw713pvi92gg1gh7xfs0z590crzzb")))

(define-public crate-cargo-structure-0.4.0 (c (n "cargo-structure") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09nrrggl5rgsmxsf8qb8bddbal3hq9izw9jp6v2zkc6nddllyk3f")))

(define-public crate-cargo-structure-0.4.1 (c (n "cargo-structure") (v "0.4.1") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ckxb0ili8k5pb1cbbbgrn8s3ld0jkb4g01jl6p27abhhkss7ha7")))

