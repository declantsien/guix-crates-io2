(define-module (crates-io ca rg cargo-tomlfmt) #:use-module (crates-io))

(define-public crate-cargo-tomlfmt-0.1.0 (c (n "cargo-tomlfmt") (v "0.1.0") (d (list (d (n "cargo") (r "^0.32.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0p4fh5icapv03chj1pgcvi3xymm29b6aw9r5d1pii8i0k98jj0fy")))

(define-public crate-cargo-tomlfmt-0.1.1 (c (n "cargo-tomlfmt") (v "0.1.1") (d (list (d (n "cargo") (r "^0.32.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1w5abnrgb1srni5ypghj5vq50ic4r4h3sk8nw2d3pf6injzap14p")))

(define-public crate-cargo-tomlfmt-0.1.3 (c (n "cargo-tomlfmt") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "0fncsbpypyp1195rc44yws3fxnfkyxnsw8wfl0xkny9q1cjj8d0l")))

