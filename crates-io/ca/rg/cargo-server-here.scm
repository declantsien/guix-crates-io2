(define-module (crates-io ca rg cargo-server-here) #:use-module (crates-io))

(define-public crate-cargo-server-here-0.1.0 (c (n "cargo-server-here") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)))) (h "0dx43pcxjn7x7sbmnm4m0jqzjafi82r3ad1j5cjl4kbrw27ch56d")))

(define-public crate-cargo-server-here-0.2.0 (c (n "cargo-server-here") (v "0.2.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)))) (h "1a4irbg28qbb3pkwdg47527ihxar05gg2q7r4m8cq0f2i43mhhsz")))

(define-public crate-cargo-server-here-0.3.0 (c (n "cargo-server-here") (v "0.3.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)))) (h "1an0zdhb7f0b358i85fi4jwiigid4jq968ivjzxslzck56dkcmk7")))

