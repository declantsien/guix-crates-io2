(define-module (crates-io ca rg cargo-dover) #:use-module (crates-io))

(define-public crate-cargo-dover-0.0.4 (c (n "cargo-dover") (v "0.0.4") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0f3cwcrzl03yc5pw9rgjsy0ylxsha6cjgq84r9jhz8lc5i4wsc03")))

(define-public crate-cargo-dover-0.0.5 (c (n "cargo-dover") (v "0.0.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "12c1r9p1a4rzc6lyal03ssw28w7y52lk8ndyc4kjs1z46n1c1gvb")))

