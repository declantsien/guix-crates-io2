(define-module (crates-io ca rg cargo-gc-target) #:use-module (crates-io))

(define-public crate-cargo-gc-target-0.2.0 (c (n "cargo-gc-target") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "cargo") (r "^0.52.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1r10r3ril2ij8999rdakryc9p1hqcbn87hck2pw53dxhlkq3g4xh") (y #t)))

