(define-module (crates-io ca rg cargo-autobindings) #:use-module (crates-io))

(define-public crate-cargo-autobindings-0.1.0 (c (n "cargo-autobindings") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r5nv20nxk2fmvvf31i9c5saifnbrvgkcrrr9hygravj4zkiksvz")))

