(define-module (crates-io ca rg cargo-docker-build) #:use-module (crates-io))

(define-public crate-cargo-docker-build-0.1.0 (c (n "cargo-docker-build") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0sx6m5x9z7qpb2gn36sn7pca4yvpqbv7x87s3jwpkmnkli7q3y53")))

(define-public crate-cargo-docker-build-0.1.1 (c (n "cargo-docker-build") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1j5bfydm7fqj6kq8vyj2fr1azlvhiprd83mxhk0bjqw17v7j1cwi")))

(define-public crate-cargo-docker-build-0.1.2 (c (n "cargo-docker-build") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0b8safqp5v6ll7609k1kvzg3psvcffrw78pj598g8i9dhn7kv77j")))

