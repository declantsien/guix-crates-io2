(define-module (crates-io ca rg cargo-ament-build) #:use-module (crates-io))

(define-public crate-cargo-ament-build-0.1.0 (c (n "cargo-ament-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1g5xgy3vjc5sgxxj2fkrc7vm4hmwz4bcadmxkxkqibyzd6w339c0")))

(define-public crate-cargo-ament-build-0.1.1 (c (n "cargo-ament-build") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0myzqygbfydpn37ilh5aj4bxz023338aiz8fr7n4vj6kk1nivbql")))

(define-public crate-cargo-ament-build-0.1.2 (c (n "cargo-ament-build") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1z7j6gdw362zgspbyny77wvbcsz39pqr387746angkgq6bfqaq3k")))

(define-public crate-cargo-ament-build-0.1.3 (c (n "cargo-ament-build") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "011fxa06m5wjzz0ln9xinbhzn05hy4ipjwkjv3rf1x0h4lq0l5ac")))

(define-public crate-cargo-ament-build-0.1.4 (c (n "cargo-ament-build") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1jpmxm6m5r6zncafs14n1npf9kjp2wl0dlfb70m4hbhq8c0lranj")))

(define-public crate-cargo-ament-build-0.1.5 (c (n "cargo-ament-build") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0xcxq1hdxglxa6il85ip8ii7sh677dadl53n20qlm5ls9m6zrmlb")))

(define-public crate-cargo-ament-build-0.1.6 (c (n "cargo-ament-build") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0q17833h7whf3gf508vd8dkbz1i30lkgdf97vpj2d7rr8zds5zqb")))

