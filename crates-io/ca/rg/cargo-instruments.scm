(define-module (crates-io ca rg cargo-instruments) #:use-module (crates-io))

(define-public crate-cargo-instruments-0.1.0 (c (n "cargo-instruments") (v "0.1.0") (d (list (d (n "cargo") (r "^0.34.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "19fl4ngg2715mbcbnhmm3ipq3r7rc841vlqggf76f929w24d3mwl")))

(define-public crate-cargo-instruments-0.1.1 (c (n "cargo-instruments") (v "0.1.1") (d (list (d (n "cargo") (r "^0.34.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "0iw37dhcy913waj5i1ray2xc07xmpa07mp53ghnjiv9ynl39rbkv")))

(define-public crate-cargo-instruments-0.1.2 (c (n "cargo-instruments") (v "0.1.2") (d (list (d (n "cargo") (r "^0.34.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "09ywyn3i2ygmnagcwivj6n58xd3nrgjzhb1ycmmk3ghf0x4axyg0")))

(define-public crate-cargo-instruments-0.2.0 (c (n "cargo-instruments") (v "0.2.0") (d (list (d (n "cargo") (r "^0.34.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1zyagsbbrqri961nhrpayraahj4ras6mjx9rv6x36afrvh0m81n3")))

(define-public crate-cargo-instruments-0.2.1 (c (n "cargo-instruments") (v "0.2.1") (d (list (d (n "cargo") (r "^0.34.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1i0855h79l7sz7y2kndylw8clii5damlk95gwmikcld1m6cvj9b5")))

(define-public crate-cargo-instruments-0.3.0 (c (n "cargo-instruments") (v "0.3.0") (d (list (d (n "cargo") (r "^0.42.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1wwafm0gsnhzm1wqjalr8dv49sv853hzjdnaslf1pkajpphqz7vl")))

(define-public crate-cargo-instruments-0.3.1 (c (n "cargo-instruments") (v "0.3.1") (d (list (d (n "cargo") (r "^0.42.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0ayf6isysph5h8vp1mck5sbv15hyi1s0m0d46kdlgzihygilp2vk")))

(define-public crate-cargo-instruments-0.3.2 (c (n "cargo-instruments") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.47.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0fgcm3jw90n1b9q5wgzchxsq63ayyd3czhjqsr4ihl8vh7h4q90q") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-instruments-0.3.3 (c (n "cargo-instruments") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.47.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0qmxvnqgah38zmaclsj6j8kn6pcy9hbn4xk1nc8z6zd98fpnwmzn") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-instruments-0.3.4 (c (n "cargo-instruments") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.47.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0v4qspfrg4abp8w7xvdcyfpgadijmc5235dyn5zbmfhhwh7678gg") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-instruments-0.4.0 (c (n "cargo-instruments") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1xahw9v7vij55lmx4xilg4jmg85g4jn1yrr35557j94i9ssk8naj")))

(define-public crate-cargo-instruments-0.4.1 (c (n "cargo-instruments") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0ja23av01kd4hw544q9pxbi10s4dlcjjlvh9y9jlxkl02iza0cfm")))

(define-public crate-cargo-instruments-0.4.2 (c (n "cargo-instruments") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.55") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "13fvpmvwcbrfhcqq6zi0q2mbj0xsa419j9mdsbwfpxqhi4jxavw9")))

(define-public crate-cargo-instruments-0.4.3 (c (n "cargo-instruments") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.55") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "12kb7gh65km6wrss97l4v1yp462vrgsj5q1a1kpzw3p1q9qd9rb6")))

(define-public crate-cargo-instruments-0.4.4 (c (n "cargo-instruments") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.57") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1y15whwwvki6d67m72vyp7bcsqrjb2l3vl210rqy154pipk4kw2x")))

(define-public crate-cargo-instruments-0.4.5 (c (n "cargo-instruments") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.60") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "18kff75wg438kfk7yblxwpfnc9lgdz170bdsgvm8vkwxbm834j3v")))

(define-public crate-cargo-instruments-0.4.6 (c (n "cargo-instruments") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.63") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "16ryjg662imj5qv9b631k5hksn203q2mxhn0abrk0yy19jgyc06n")))

(define-public crate-cargo-instruments-0.4.7 (c (n "cargo-instruments") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.63") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0bxqpgydgxp2x9wg07mqwbdnxqzjqpsvn9gqg9l06x782jsv52qp") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-instruments-0.4.8 (c (n "cargo-instruments") (v "0.4.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1z6gqax4dh96yjldhvnhav2s7fzj87xx4hlwvvm70fmllyk7bgad") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-instruments-0.4.9 (c (n "cargo-instruments") (v "0.4.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.74") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "06s6incd7nsgd2nn8jjykna6bm2rwwhjag24v33aq45i461fbf3d") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

