(define-module (crates-io ca rg cargo-indicate) #:use-module (crates-io))

(define-public crate-cargo-indicate-0.1.0 (c (n "cargo-indicate") (v "0.1.0") (h "1g97l3sgmcdpn8sj36m9a5cbv0sn09rqp6aipd8n03v1mrifjym5")))

(define-public crate-cargo-indicate-0.2.1 (c (n "cargo-indicate") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "indicate") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.12") (d #t) (k 2)))) (h "0adjhncn57fgsnrqp0vjas8maabzf9i51f4grpv2lhhqz4391j5w")))

