(define-module (crates-io ca rg cargo-linked) #:use-module (crates-io))

(define-public crate-cargo-linked-0.1.0 (c (n "cargo-linked") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.11.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0f20b9lxrm2xm0psqzjq26bsf06lr3gxgaizc0mzr7c0w53ccbi9")))

