(define-module (crates-io ca rg cargo-mdparse) #:use-module (crates-io))

(define-public crate-cargo-mdparse-0.1.0 (c (n "cargo-mdparse") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ccd44wdx14ds37jvnh8rsn7rp9gk2qy3rmqcrsc0nd1znnb8r5i")))

