(define-module (crates-io ca rg cargo-ado) #:use-module (crates-io))

(define-public crate-cargo-ado-0.0.1 (c (n "cargo-ado") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0i8pvmnf19g8xr89la1lr3n3wli44aw1ibc316wfks72a0w7p3hz")))

(define-public crate-cargo-ado-0.0.2 (c (n "cargo-ado") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)))) (h "1051qkdcda1g8v8m7k8znxxrm9vckdmv4a4190zbd3p3lmiqq1li")))

