(define-module (crates-io ca rg cargo-deps) #:use-module (crates-io))

(define-public crate-cargo-deps-1.0.0 (c (n "cargo-deps") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1imswq6mq820qw5gfnsw7d1r6yyb03b87y6j1gjyb9xsji71x2hp")))

(define-public crate-cargo-deps-1.0.1 (c (n "cargo-deps") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0ca35dhpyagx67qa78jn3c22917a65b4yvaigakl8ashdjmii6bz")))

(define-public crate-cargo-deps-1.0.2 (c (n "cargo-deps") (v "1.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0aqs6fcid9schis160smmya344bydc17y8kzf9a3v9xc23m6l4nr")))

(define-public crate-cargo-deps-1.0.3 (c (n "cargo-deps") (v "1.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0vz0kzhzz4lqgzim9p8q3pvdcs7ahzprf51s7551r3v84ka59m2k")))

(define-public crate-cargo-deps-1.0.4 (c (n "cargo-deps") (v "1.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15cwzh8897py0blwbrf0cz6v3x5iq27476adscjbh2cbwbkcz4w7")))

(define-public crate-cargo-deps-1.1.0 (c (n "cargo-deps") (v "1.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gvidzygzh3vab7zmkdpikpbm529yajskc6635bx2bhpzs87a67b")))

(define-public crate-cargo-deps-1.1.1 (c (n "cargo-deps") (v "1.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "059jxvrdm738wmxqjhk4xs9mmng0cnyh1lys27vy6fjcna0g46nf")))

(define-public crate-cargo-deps-1.2.0 (c (n "cargo-deps") (v "1.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02pk8mdcik5dq5gxvj59q3mpvb9zhif3wf0sq2nxrraaj3b3v9m0")))

(define-public crate-cargo-deps-1.3.0 (c (n "cargo-deps") (v "1.3.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1irwv3i3lag8izb7cqvpzgvn7ciazh1dzglrp5hfan9d50cak9jx")))

(define-public crate-cargo-deps-1.4.0 (c (n "cargo-deps") (v "1.4.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13sgqq124qgid4zmsdmwp0iv291imfmnnk6vvglbk0grb41sq854")))

(define-public crate-cargo-deps-1.4.1 (c (n "cargo-deps") (v "1.4.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1i2d68fmmz5v1vw6z54hmi9vpcr5h26zh7icf45c8sam7divphw5")))

(define-public crate-cargo-deps-1.5.0 (c (n "cargo-deps") (v "1.5.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1s1yw23nrqcbaaysldhysg0kyvwjhz9x6ff5zpyldhaw4jgsxmqx")))

(define-public crate-cargo-deps-1.5.1 (c (n "cargo-deps") (v "1.5.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0iyzsfiaiv7n06zc697jlj291mb6b0f6ypkxil0n5pry8vc7i3lm")))

