(define-module (crates-io ca rg cargo-makepkg) #:use-module (crates-io))

(define-public crate-cargo-makepkg-0.1.0 (c (n "cargo-makepkg") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0cyk2n6d2qqx6vz5zd410w4aqv207z7ppvgcra28gd0y68mqwwnz")))

