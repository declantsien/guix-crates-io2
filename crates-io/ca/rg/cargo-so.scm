(define-module (crates-io ca rg cargo-so) #:use-module (crates-io))

(define-public crate-cargo-so-0.1.0 (c (n "cargo-so") (v "0.1.0") (d (list (d (n "cargo-subcommand") (r "^0.5.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "ndk-build") (r "^0.4.3") (d #t) (k 0)))) (h "0d53yfp4f912i3ar05rgxh1cdhlq94dvipk2q86d11ai9cc3gjm4")))

(define-public crate-cargo-so-0.1.1 (c (n "cargo-so") (v "0.1.1") (d (list (d (n "cargo-subcommand") (r "^0.5.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "ndk-build") (r "^0.4.3") (d #t) (k 0)))) (h "14q0jhxwhavx65gz92v2zh777lmvnfwk8h3g41xaay84gph64yhf")))

(define-public crate-cargo-so-0.1.2 (c (n "cargo-so") (v "0.1.2") (d (list (d (n "cargo-subcommand") (r "^0.5.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "ndk-build") (r "^0.4.3") (d #t) (k 0)))) (h "0f5g2vc6c2hfbq2akrg7cbnsizqrfmks9cszw61s9kvbzw7krqxv")))

(define-public crate-cargo-so-0.1.3 (c (n "cargo-so") (v "0.1.3") (d (list (d (n "cargo-subcommand") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "ndk-build") (r "^0.8.0") (d #t) (k 0)))) (h "1hrfmzaqm6lb784h9fpf1bqz7dlc528lx17xlbqvyb0jgmy3aibj")))

(define-public crate-cargo-so-0.2.0 (c (n "cargo-so") (v "0.2.0") (d (list (d (n "cargo-subcommand") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ndk-build") (r "^0.9.0") (d #t) (k 0)))) (h "0fhwyc44vc5n2n6p2dzkhsf5qkbcl0ynrkssbhhrzsfcih4ijy2k")))

