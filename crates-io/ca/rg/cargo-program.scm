(define-module (crates-io ca rg cargo-program) #:use-module (crates-io))

(define-public crate-cargo-program-0.0.1 (c (n "cargo-program") (v "0.0.1") (h "1g6x1qm1hc1jgyky3zfjn905xab7lspc1aazjkj2l255pjrr73xr") (y #t)))

(define-public crate-cargo-program-0.0.2 (c (n "cargo-program") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0hc0m6l427dyzl9m9afy6ahwpzsygshp80jmbffi33c3fkr7s6iv")))

(define-public crate-cargo-program-0.0.3 (c (n "cargo-program") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.18.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qba4h78l1m702ln50pxirzfvbgvxpy143xb6mrqjy8vj9zlrfpl")))

(define-public crate-cargo-program-0.0.4 (c (n "cargo-program") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.18.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rrxfvvdbk5cy4a2gxibry8vvvirz7nbiky0j17lfb524mlj4r1c")))

