(define-module (crates-io ca rg cargo-prefetch) #:use-module (crates-io))

(define-public crate-cargo-prefetch-0.1.0 (c (n "cargo-prefetch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gsyglzp4bhw60csvgp8hvigks9r4dvdx4cajjiylhgysg2vfaj8")))

