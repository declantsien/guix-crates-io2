(define-module (crates-io ca rg cargo-shell) #:use-module (crates-io))

(define-public crate-cargo-shell-0.1.0 (c (n "cargo-shell") (v "0.1.0") (d (list (d (n "cargo") (r "^0.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1lpjb99k21bjqg61rbkjywz6qpmgzv19fiarzaibdyv9js07da2z")))

(define-public crate-cargo-shell-0.1.1 (c (n "cargo-shell") (v "0.1.1") (d (list (d (n "cargo") (r "^0.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0mmq4a26xsnw10n8ldbyk6zc313hq9bqaigpdrcarjis6pfr35v4")))

