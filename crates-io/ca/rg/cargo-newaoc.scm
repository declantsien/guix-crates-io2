(define-module (crates-io ca rg cargo-newaoc) #:use-module (crates-io))

(define-public crate-cargo-newaoc-0.1.0 (c (n "cargo-newaoc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g58lfqbmj2fprcmdircfn3n3mdz9zcpz7zhiggwxpzanq6m6gim")))

(define-public crate-cargo-newaoc-0.1.1 (c (n "cargo-newaoc") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "16jfm3wxqzvr5bzgq6n5mrg1is2rd2xb0yhh67rsyxdci62vxfxj")))

(define-public crate-cargo-newaoc-0.1.2 (c (n "cargo-newaoc") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "14j10sbbaf5q6yyiv7scf3b47l61r73xljn2x2h8p4s7ps6zg28i")))

