(define-module (crates-io ca rg cargo-funnel) #:use-module (crates-io))

(define-public crate-cargo-funnel-0.1.0 (c (n "cargo-funnel") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1glfh6pd3j8m6cg6qy4y3k10yypqcmd183s6djcnq2niklabg4d8")))

(define-public crate-cargo-funnel-0.2.0 (c (n "cargo-funnel") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1pilssh15p6zha4gr6s1cjmymdaqb61dmsblmvv7l6gz39hv7gvx")))

(define-public crate-cargo-funnel-0.2.3 (c (n "cargo-funnel") (v "0.2.3") (d (list (d (n "cargo_metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "02s43bqga50qvc6xg13y3xfvv6zrazqzr0pihh84pvj1hkif7a4n")))

(define-public crate-cargo-funnel-0.2.4 (c (n "cargo-funnel") (v "0.2.4") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1mds740dyjl16iidif37cjd6awyg0g7xyba9xhmx3arzs8013y7x")))

(define-public crate-cargo-funnel-0.2.5 (c (n "cargo-funnel") (v "0.2.5") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "055ac8dqnbzl4i46hmcsd2clxvbxc2vfg4pynsw1jdjqmh7wp9wc")))

(define-public crate-cargo-funnel-0.3.0 (c (n "cargo-funnel") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1wnv4dzh0ax4g87k1ksl8fs4ln8x17s6p58yv695v7d7vwlx36zc")))

(define-public crate-cargo-funnel-0.4.0 (c (n "cargo-funnel") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "0iw0i2ckpixk42wfawdjbc3akkwhlqs3xxfxb82f2najdpc9i359")))

(define-public crate-cargo-funnel-0.4.1 (c (n "cargo-funnel") (v "0.4.1") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "09a1mfg0dzq1i3idr7skz1n5r7lic40xyhq664sdsqpj0pf7hfzp")))

(define-public crate-cargo-funnel-0.5.1 (c (n "cargo-funnel") (v "0.5.1") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1w23wyird70brj7900igqr8rrvqzaxr21wj8f9xs117xwrxm0qgk")))

