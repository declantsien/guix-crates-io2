(define-module (crates-io ca rg cargo-parcel) #:use-module (crates-io))

(define-public crate-cargo-parcel-0.0.1 (c (n "cargo-parcel") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.20") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "08k4kpcmh85sr318i6a1hcsbxbl737qdz6whqzm8yc5jxmhhf9i0")))

(define-public crate-cargo-parcel-0.0.2 (c (n "cargo-parcel") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.20") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "07np486amcfc8jdki8k6n8sjmbycbwmvx38r1j7n9fv4xka4dkgy")))

(define-public crate-cargo-parcel-0.0.3 (c (n "cargo-parcel") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.20") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0iygsdb4phw1s8nxn9ld2qmpbg1z2yl4ph03ng6z328xvsznj52h")))

(define-public crate-cargo-parcel-0.0.4 (c (n "cargo-parcel") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.20") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0dpsbxww0w6p5m9zs7fa08gkwmbhzm20siqj1i2cw0fawpkpyk9l")))

