(define-module (crates-io ca rg cargo-check-export) #:use-module (crates-io))

(define-public crate-cargo-check-export-0.0.0 (c (n "cargo-check-export") (v "0.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03ys9azfga2rvndknkxy921lx1pw19iw8jaqs5yd5rjrv81n7m9x")))

(define-public crate-cargo-check-export-0.0.1 (c (n "cargo-check-export") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13kfrbji8bynfqqikdlmnw2qxrpx7z2pm59dgrvck2sy8pi3awf8")))

