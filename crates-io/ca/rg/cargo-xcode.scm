(define-module (crates-io ca rg cargo-xcode) #:use-module (crates-io))

(define-public crate-cargo-xcode-0.1.0 (c (n "cargo-xcode") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.3.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.3.0") (d #t) (k 0)))) (h "1857dw0sf4avcjbswfzk6s4fhdnalyzhrzd440a6g815h50pn3np") (y #t)))

(define-public crate-cargo-xcode-0.1.1 (c (n "cargo-xcode") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.3.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.3.0") (d #t) (k 0)))) (h "1fqngqfxqm1ridis8zi3z5vfbna69fmidr791lmbhhrz9hpf9207") (y #t)))

(define-public crate-cargo-xcode-1.0.0 (c (n "cargo-xcode") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.4.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "0zjzv7nds67zhkrjjvfh8j34iqy1v1g47g99kkjila93s6zfpciz") (y #t)))

(define-public crate-cargo-xcode-1.1.0 (c (n "cargo-xcode") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.5.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1nvv6l8p463jr71fhbm1541k9k4z5shqvncqi5l7323k1zwn8l2i") (y #t)))

(define-public crate-cargo-xcode-1.1.1 (c (n "cargo-xcode") (v "1.1.1") (d (list (d (n "cargo_metadata") (r "^0.5.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "0sjn6bf3mpxxmlrsavzg8cd1irpnnfccb6kp4h6dlgm6vkn8m6ll") (y #t)))

(define-public crate-cargo-xcode-1.1.2 (c (n "cargo-xcode") (v "1.1.2") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "0k6ijz5z3aw1f850i0wfxk28mr7apl11jxkx8wj5ph85brsn4622") (y #t)))

(define-public crate-cargo-xcode-1.1.3 (c (n "cargo-xcode") (v "1.1.3") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "0jamy7zgqbjghc8qn40v8znw475zsamh4kxxr1xgnj0492kmjha9") (y #t)))

(define-public crate-cargo-xcode-1.1.4 (c (n "cargo-xcode") (v "1.1.4") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1znyy7zrvafz8k6npgz50cy3822d5vvjxq2glx92ai9l5zy9l5mr") (y #t)))

(define-public crate-cargo-xcode-1.3.0 (c (n "cargo-xcode") (v "1.3.0") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "0nsrz2zj23mafl1x92dkhkwjjkvzr09q1gxpvcfxnjx5sysfwdw9") (y #t)))

(define-public crate-cargo-xcode-1.3.1 (c (n "cargo-xcode") (v "1.3.1") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1h4d9lsiqs3a5hkgvmdqrv6fysv7059gspvzyfblvjc4z9n0kkby") (y #t)))

(define-public crate-cargo-xcode-1.3.2 (c (n "cargo-xcode") (v "1.3.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)))) (h "1qlw43nf2np6wlzxfspvvzpq4p3h9hqc6b59zbgd007pf9d4y8vv")))

(define-public crate-cargo-xcode-1.3.3 (c (n "cargo-xcode") (v "1.3.3") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0259yzzjr0gaahk24s38mmvv5jxflld8ah6rlglfd69h32rb82jv")))

(define-public crate-cargo-xcode-1.4.0 (c (n "cargo-xcode") (v "1.4.0") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1wwj59pgv95368nz1pvc30bsqm0kxvf8s4x7xzr9i5jrmjni1sm8")))

(define-public crate-cargo-xcode-1.4.1 (c (n "cargo-xcode") (v "1.4.1") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0w3dna21gkk1wnhcxswhkz2ri2i9sp11v2vzzlm2b3wy1jbs2rig")))

(define-public crate-cargo-xcode-1.4.2 (c (n "cargo-xcode") (v "1.4.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "18qh74s4116nb4kpgbsw2bmlwvgqqj823b7sqjmvrklhq785lx17")))

(define-public crate-cargo-xcode-1.4.3 (c (n "cargo-xcode") (v "1.4.3") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0diij78j4ly677ha47y8h3ncinr7p1avp53pzmqjc6lfmmj3pcbb")))

(define-public crate-cargo-xcode-1.5.0 (c (n "cargo-xcode") (v "1.5.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1gz33p4akj38h0yq46nw3az0j4pl23l2idnrysavj9zwrvml9dab")))

(define-public crate-cargo-xcode-1.6.0 (c (n "cargo-xcode") (v "1.6.0") (d (list (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0gxzvj3igk79kbcw07xc6njmv18pkfgzxbd2wzkzl69psgpp475h")))

(define-public crate-cargo-xcode-1.6.1 (c (n "cargo-xcode") (v "1.6.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1lzfsbl7p2w7hxzhq6ji54m5l7r135655clc2p2r37rhxzxlf18r")))

(define-public crate-cargo-xcode-1.7.0 (c (n "cargo-xcode") (v "1.7.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0sfzfbggz71slvq1mq4czxkssl48n35yb3vmnip53scrlsrakxba")))

(define-public crate-cargo-xcode-1.8.0 (c (n "cargo-xcode") (v "1.8.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "15h5dijf1i7dxfy1f3ziykmlz0yxnpjfzgrz7lqr3ydhw9gl90vm")))

(define-public crate-cargo-xcode-1.8.2 (c (n "cargo-xcode") (v "1.8.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1jpz6kp8sc5dbknyzwpafjd4g5gl4lwcdvl8ka2d69jdj0z2p186")))

(define-public crate-cargo-xcode-1.9.0-alpha.1 (c (n "cargo-xcode") (v "1.9.0-alpha.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "15b096ffxx3gczn24k6a66nsc5is0ray9876x0r462affbqw9c1m")))

(define-public crate-cargo-xcode-1.9.0 (c (n "cargo-xcode") (v "1.9.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1i40kva6bmbb79di2201vyxai90qiiyqq89fx1jgk0sljdmc3v64")))

(define-public crate-cargo-xcode-1.10.0 (c (n "cargo-xcode") (v "1.10.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1imsqbngq8km9rswz5z9qigbwyim3xg6jgysvzlv3rrvrnmim9f1") (r "1.61")))

