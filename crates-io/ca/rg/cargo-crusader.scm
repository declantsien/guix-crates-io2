(define-module (crates-io ca rg cargo-crusader) #:use-module (crates-io))

(define-public crate-cargo-crusader-0.1.0 (c (n "cargo-crusader") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)) (d (n "toml") (r "^0.1.21") (d #t) (k 0)))) (h "0rm5yky5yd8nddp8x6f3c0770j38wywv9knwd6893d8rf193jlar")))

(define-public crate-cargo-crusader-0.1.1 (c (n "cargo-crusader") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)) (d (n "term") (r "^0.2.11") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.1.21") (d #t) (k 0)))) (h "0i1saxrc6kibf4cii1x814l46wddz5giv3qc1nnn6c749iz5m2jn")))

