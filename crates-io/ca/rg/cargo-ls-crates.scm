(define-module (crates-io ca rg cargo-ls-crates) #:use-module (crates-io))

(define-public crate-cargo-ls-crates-0.2.2 (c (n "cargo-ls-crates") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "014bqs20gicqzflz9km0fvhlg4kc0gnakr889jh3ppblfg3ijmy6")))

(define-public crate-cargo-ls-crates-0.2.3 (c (n "cargo-ls-crates") (v "0.2.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "062287m5y39b644kkcr9bksb6w1w9jlyh1zqhhpp0gnaadim8pva")))

(define-public crate-cargo-ls-crates-0.2.4 (c (n "cargo-ls-crates") (v "0.2.4") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0rj6rzymy94knnb3zl851ni9bcd423mfqvaxrdd6ph6n6vmixmls")))

(define-public crate-cargo-ls-crates-0.2.5 (c (n "cargo-ls-crates") (v "0.2.5") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "097nw5dyiff9fa8zxphaqah5mj1ss16i99h10vl3p8ngfys3aaw7")))

(define-public crate-cargo-ls-crates-0.2.6 (c (n "cargo-ls-crates") (v "0.2.6") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0adm8fg8xqxz77hs8vb5zfjw83wzmip461jqx73szh9s774gq2s1")))

(define-public crate-cargo-ls-crates-0.3.0 (c (n "cargo-ls-crates") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0vi10nmvc0zg58wvs5zhi91b40zqxxxrjidvsvrky0vjcjph7m3q")))

(define-public crate-cargo-ls-crates-0.3.1 (c (n "cargo-ls-crates") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0l4vmcnns43dl1r9z3brvxs70a0qfa6c9697ydah8xf068azycmp")))

(define-public crate-cargo-ls-crates-0.3.2 (c (n "cargo-ls-crates") (v "0.3.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1dczjppx5njqv5mp5dqmwm5g0z6rk4m4ap9gymvzv105l3k2bb5q")))

(define-public crate-cargo-ls-crates-0.3.3 (c (n "cargo-ls-crates") (v "0.3.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1kmhfqayqlgy2yf28xhkgfl1gj1lyrcqpjwrhd8x7bb9f20gccf4")))

(define-public crate-cargo-ls-crates-0.4.0 (c (n "cargo-ls-crates") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1wqi82wdzz7q6csd3iams8zg51wdjndbvidddd3lcahp9gz6clvd")))

(define-public crate-cargo-ls-crates-0.4.1 (c (n "cargo-ls-crates") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0is75c6xgqpgf6gmwvw5lvcg7xjflzjgmqbsvfn9g13jz0y9kkx9")))

(define-public crate-cargo-ls-crates-0.4.2 (c (n "cargo-ls-crates") (v "0.4.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "194kfd1i3x629xhk9a2082h5s67jnn8m6367yyv9m103wjq7hchk")))

(define-public crate-cargo-ls-crates-0.4.3 (c (n "cargo-ls-crates") (v "0.4.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "09xsb3qvd5i61skypifknpvf2aaxfi744crmqfq76ng3zmhvx9h8")))

(define-public crate-cargo-ls-crates-0.4.5 (c (n "cargo-ls-crates") (v "0.4.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0cd8rcxf7qg2wmcpi8wjicz2msm2mq7wy052bly5l2d1i9c54cjh")))

(define-public crate-cargo-ls-crates-0.4.6 (c (n "cargo-ls-crates") (v "0.4.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1ikcckvv5z805qqs8bjxdi6qjayc08ywx9yrqlq15x25g0q82nzn")))

(define-public crate-cargo-ls-crates-0.4.7 (c (n "cargo-ls-crates") (v "0.4.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0xqz9llk9v4ghysls6l9y6z7ahrsyvsrbscjr6w2rgsf0cvs6qzr")))

