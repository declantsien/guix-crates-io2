(define-module (crates-io ca rg cargo-bavy) #:use-module (crates-io))

(define-public crate-cargo-bavy-0.1.0 (c (n "cargo-bavy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "1w5jyaa7dr9gnxvwhrdhr2q7hxxpn9pycrlb2adljhrc73gn5c0j")))

(define-public crate-cargo-bavy-0.2.0 (c (n "cargo-bavy") (v "0.2.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.1.0") (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "141knvqcv1apyrwdqdqwnwy6vwz523y2mdkgdyczc6bdvcv2y69k")))

