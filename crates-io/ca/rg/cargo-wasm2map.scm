(define-module (crates-io ca rg cargo-wasm2map) #:use-module (crates-io))

(define-public crate-cargo-wasm2map-0.1.0 (c (n "cargo-wasm2map") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm2map") (r "^0.1.0") (d #t) (k 0)))) (h "0i5155wkqlwwk60iy3qi00scyy195780kvrdy910x422jnhrvv2j") (r "1.64.0")))

