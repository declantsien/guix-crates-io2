(define-module (crates-io ca rg cargo-epublish) #:use-module (crates-io))

(define-public crate-cargo-epublish-0.0.2 (c (n "cargo-epublish") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1nyyi9z43qmqhsvgrh0psdh1mmrpchrix8ivfhy6asz6bawmsjkk")))

