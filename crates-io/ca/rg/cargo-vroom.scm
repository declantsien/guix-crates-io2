(define-module (crates-io ca rg cargo-vroom) #:use-module (crates-io))

(define-public crate-cargo-vroom-4.2.0-alpha.69 (c (n "cargo-vroom") (v "4.2.0-alpha.69") (h "13820iqla0ncfbw0y54nl2wii67z9n5h9cz1xd9jmnmlcsranhgi")))

(define-public crate-cargo-vroom-4.2.0 (c (n "cargo-vroom") (v "4.2.0") (h "0i8y7vaxnwr45m4n7a3x23cgkng1mm1z5w7f89yp7j55744gws4a")))

(define-public crate-cargo-vroom-0.0.1111111111 (c (n "cargo-vroom") (v "0.0.1111111111") (h "0w30dklbhkzvpfyi755mmwf9r49r5w5mk2s0biigdj01rfwvkabq")))

(define-public crate-cargo-vroom-4.2.1 (c (n "cargo-vroom") (v "4.2.1") (h "1ch2sp56viydi5rn0216fvg1pa1wnxgvcvmn8315xp8myvflmbis")))

