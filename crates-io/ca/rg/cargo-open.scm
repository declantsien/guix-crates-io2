(define-module (crates-io ca rg cargo-open) #:use-module (crates-io))

(define-public crate-cargo-open-0.1.0 (c (n "cargo-open") (v "0.1.0") (h "15ga54bqzbcd7c8r9g3bs6ac81kyhlj5qx6wmag59byci31bcyc8") (y #t)))

(define-public crate-cargo-open-0.3.0 (c (n "cargo-open") (v "0.3.0") (d (list (d (n "cargo") (r "*") (d #t) (k 0)) (d (n "clap") (r "*") (d #t) (k 0)))) (h "03m3j2z9g4b0lx67bimlvivvwwggh8cnchrxlar1nhanqn49nyj9") (y #t)))

(define-public crate-cargo-open-0.4.0 (c (n "cargo-open") (v "0.4.0") (d (list (d (n "cargo") (r "^0.37.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "0ngmq9bh47xl9i02bycy36wzbb3fl083fy6df16i5mczgivxics4") (y #t)))

(define-public crate-cargo-open-1.0.0 (c (n "cargo-open") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kqivy5r621r32nwdcpbi5xc4v5z49xig899h24p3xv2n1gh90f8")))

(define-public crate-cargo-open-1.0.1 (c (n "cargo-open") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m80lc26zlpkyqb7n7wm5b5la8ngxpdnzsbhzxl42lqhiz3pvy3b")))

