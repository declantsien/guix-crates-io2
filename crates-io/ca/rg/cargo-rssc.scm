(define-module (crates-io ca rg cargo-rssc) #:use-module (crates-io))

(define-public crate-cargo-rssc-0.3.41 (c (n "cargo-rssc") (v "0.3.41") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1cc7bbav722s2wak3da71a5n2jsdnibn9c9injcbakzl5rjfxlh5") (y #t)))

(define-public crate-cargo-rssc-0.3.42 (c (n "cargo-rssc") (v "0.3.42") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0g8r8gndn0fld0q8dsgday5zfikyf0srm9nqkyqynlsq5nhax6x1") (y #t)))

