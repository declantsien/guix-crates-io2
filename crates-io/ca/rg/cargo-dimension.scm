(define-module (crates-io ca rg cargo-dimension) #:use-module (crates-io))

(define-public crate-cargo-dimension-1.0.0 (c (n "cargo-dimension") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colour") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "09p9jg9ag85dbl247cyap17i0rxnv2i22c0aiwmb5lf56ks7yd3i")))

