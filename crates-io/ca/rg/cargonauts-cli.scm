(define-module (crates-io ca rg cargonauts-cli) #:use-module (crates-io))

(define-public crate-cargonauts-cli-0.2.0 (c (n "cargonauts-cli") (v "0.2.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0vljp0cih3648rpk9glygs6c9h4nzbv8rp82wk45j1hazb4l07l2")))

(define-public crate-cargonauts-cli-0.2.1 (c (n "cargonauts-cli") (v "0.2.1") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "heck") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "12zirv8mi40yax6n7iplilk4s1zvwmx9vgc4bf4pwy7madya4yd1")))

(define-public crate-cargonauts-cli-0.2.2 (c (n "cargonauts-cli") (v "0.2.2") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "heck") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "097k0f7q0rznvyx4rcwvdfmdalrwfbqqajpcyn6507nksix6l2g1")))

(define-public crate-cargonauts-cli-0.2.3 (c (n "cargonauts-cli") (v "0.2.3") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "heck") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1vc35wicjg95ip8ig6nbms34sh5bilc6dxy7rf4yqric23sh5gwi")))

