(define-module (crates-io ca rg cargo-plonk) #:use-module (crates-io))

(define-public crate-cargo-plonk-0.1.0 (c (n "cargo-plonk") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "dynasmrt") (r "^2.0.0") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.4.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rustc-demangle") (r "^0.1.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "synchapi"))) (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0zxarcwwmq73rffjxzykfhbiagd35v8g6ywncfzs7hffzylzacfb")))

