(define-module (crates-io ca rg cargo-permissions) #:use-module (crates-io))

(define-public crate-cargo-permissions-0.1.0 (c (n "cargo-permissions") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1cpyq7ffgk5mfijm202qvsj5gf58h95yyksfjd2pw64y9himqnyq")))

(define-public crate-cargo-permissions-0.1.1 (c (n "cargo-permissions") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0rlvz6ddpiklyqb8238x6wqb2md51d06241qblvlm3iqx1lj9ryr")))

