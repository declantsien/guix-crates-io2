(define-module (crates-io ca rg cargo-todo) #:use-module (crates-io))

(define-public crate-cargo-todo-0.1.0 (c (n "cargo-todo") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "11pf2fj7q4g61qy4idznjils5aqykq0s3kmlcz85k85g5v6gvpfp")))

(define-public crate-cargo-todo-0.1.1 (c (n "cargo-todo") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1arpzr7r8qladi5a9h3hv3dnv9ii09ni95jgzv25k1nmxifla5iy")))

(define-public crate-cargo-todo-0.1.2 (c (n "cargo-todo") (v "0.1.2") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0gzjj742cjmf8l9prxwwx39bymki6xnkahgky9dn2rbv41ryggs0")))

(define-public crate-cargo-todo-0.1.3 (c (n "cargo-todo") (v "0.1.3") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "158drfakwhpr38plnjr0ggv94sbixqzna1rgjxzfw7c2xn3qmls0")))

(define-public crate-cargo-todo-0.1.4 (c (n "cargo-todo") (v "0.1.4") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1jv44m9bjjvrip3bv0k1iwnz87149yslgnzly2mp3y78fbpns6ig")))

(define-public crate-cargo-todo-0.1.5 (c (n "cargo-todo") (v "0.1.5") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "111yx9invh1jmsz6skv2bfgq9pz5lp5s0xfqai1lwd4jcyld55ss")))

(define-public crate-cargo-todo-0.1.6 (c (n "cargo-todo") (v "0.1.6") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "string_format") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "024bbbz9m0526qnssblvy8jgm1sbpxs925f6ziygrhynkp4z0z08")))

(define-public crate-cargo-todo-0.1.7 (c (n "cargo-todo") (v "0.1.7") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "string_format") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "17a83fhx0065hz0qkfjp6p4prncmd2ihgr4lkwd7a4w5wha8gq5l")))

(define-public crate-cargo-todo-0.2.0 (c (n "cargo-todo") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "string_format") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "032ppadwpa61yj7j78q05c5482nga4vlrdzbkz6515jjc5bkhyxg")))

(define-public crate-cargo-todo-0.2.1 (c (n "cargo-todo") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.5") (d #t) (k 0)) (d (n "string_format") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0qml1s0vlsxpfczywls4w9y982rr8xmyjq5zmnj23mh4fzamq342")))

