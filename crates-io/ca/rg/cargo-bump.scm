(define-module (crates-io ca rg cargo-bump) #:use-module (crates-io))

(define-public crate-cargo-bump-0.0.0 (c (n "cargo-bump") (v "0.0.0") (d (list (d (n "cargo") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^1") (d #t) (k 0)))) (h "1k18q6ifjwhzya5vxjk3hswjsmch8adxkkvzmli4hcfr04bv6gkg")))

(define-public crate-cargo-bump-0.0.1 (c (n "cargo-bump") (v "0.0.1") (d (list (d (n "clap") (r "~1.5.4") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1v59mp1kn66pjl6v3lj25ysfk12vrgjh0k49l3f560dylffl54is")))

(define-public crate-cargo-bump-1.0.0 (c (n "cargo-bump") (v "1.0.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "semver") (r "^0.7.0") (d #t) (k 0)) (d (n "tomllib") (r "^0.1.2") (d #t) (k 0)))) (h "12ab8c124halgglrcs7z5shyk84cwkb4pb3h28ba4x791a01a4p8")))

(define-public crate-cargo-bump-1.0.1 (c (n "cargo-bump") (v "1.0.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "semver") (r "^0.7.0") (d #t) (k 0)) (d (n "tomllib") (r "^0.1.2") (d #t) (k 0)))) (h "19jpk3iffzzps6zv3sh9a0x8csfr5xi1mgpzirpnxzbxv7kqvarv")))

(define-public crate-cargo-bump-1.0.2 (c (n "cargo-bump") (v "1.0.2") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "semver") (r "^0.7.0") (d #t) (k 0)) (d (n "tomllib") (r "^0.1.2") (d #t) (k 0)))) (h "0ynkr0m3ny088q94wkl5n1161arxhjfgakmynyy292wdw7lzzqdr")))

(define-public crate-cargo-bump-1.1.0 (c (n "cargo-bump") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "0rqhzmdwr09saqbv4xamjkd5z859yaxmxxxjkkbcf3lq51724fhz")))

