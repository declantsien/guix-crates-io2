(define-module (crates-io ca rg cargo-clone-core) #:use-module (crates-io))

(define-public crate-cargo-clone-core-0.1.0 (c (n "cargo-clone-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1c27fxlahz51waq729xh97g7jbzpm0ymfva97vjkxvd1ry1vxpj0")))

(define-public crate-cargo-clone-core-0.2.0 (c (n "cargo-clone-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cargo") (r "^0.66.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "05vkmj4r7s3wdmyc41yz04ahiymw9fbmvmhiz7704vafh28qswmn")))

(define-public crate-cargo-clone-core-0.2.1 (c (n "cargo-clone-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo") (r "^0.75.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0jipykb49j3xmxxnjjja0s0i7y0zrdpinnn0a07673wg2xl2qv6b")))

