(define-module (crates-io ca rg cargo-cryptile) #:use-module (crates-io))

(define-public crate-cargo-cryptile-0.1.0 (c (n "cargo-cryptile") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.5") (d #t) (k 0)) (d (n "threads_pool") (r "^0.2.6") (d #t) (k 0)))) (h "05p7rz1b1kr4c69qaq52h3j595p7avcpnm35vh7zmq579qjvr320") (y #t)))

(define-public crate-cargo-cryptile-0.1.1 (c (n "cargo-cryptile") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.5") (d #t) (k 0)) (d (n "threads_pool") (r "^0.2.6") (d #t) (k 0)))) (h "1vic1yd5v32vn67k8nq6dxchjl79ws5wmx8is4kway4q5v1x9p7i") (y #t)))

(define-public crate-cargo-cryptile-0.1.2 (c (n "cargo-cryptile") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.5") (d #t) (k 0)) (d (n "threads_pool") (r "^0.2.6") (d #t) (k 0)))) (h "1j9w61crqw1ab8254pcjdbn459yq7j9rrw2qkz01rhpi0nbsal56") (y #t)))

(define-public crate-cargo-cryptile-0.2.0 (c (n "cargo-cryptile") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("password"))) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (k 0)) (d (n "threads_pool") (r "^0.2.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "05h93yab0my8h7wqalgcm3icx4fwqnjrb713hyq80125bd4xmaqh")))

