(define-module (crates-io ca rg cargo-foreach) #:use-module (crates-io))

(define-public crate-cargo-foreach-0.0.0 (c (n "cargo-foreach") (v "0.0.0") (h "1fzg4yvpmigarwpxwj2wry3ln2pvfvg1y2y47ljmvaqc27kbm772")))

(define-public crate-cargo-foreach-1.0.0 (c (n "cargo-foreach") (v "1.0.0") (d (list (d (n "rustc_tools_util") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc_tools_util") (r "^0.2.1") (d #t) (k 1)))) (h "1fvgk40ws7prm4yfilmi97fx3nj5fsln8wfbs7d2km3fmdbvz9k8")))

(define-public crate-cargo-foreach-1.0.1 (c (n "cargo-foreach") (v "1.0.1") (d (list (d (n "rustc_tools_util") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc_tools_util") (r "^0.2.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1m4zj5dbllp08jgp0hj2fjv8gskadaxvwjzm134n5654ayzsgmmz")))

(define-public crate-cargo-foreach-1.0.2 (c (n "cargo-foreach") (v "1.0.2") (d (list (d (n "rustc_tools_util") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_tools_util") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1hm1m92y1a9m9wan4qv064klr7hq8rip8p2w58ycnzix0p4fmxkl") (y #t)))

(define-public crate-cargo-foreach-1.0.3 (c (n "cargo-foreach") (v "1.0.3") (d (list (d (n "rustc_tools_util") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_tools_util") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0x8qlywrcsdwffk3l6853fbflqs8brraxvagiw33q8x327lrcll1")))

