(define-module (crates-io ca rg cargo-component-macro) #:use-module (crates-io))

(define-public crate-cargo-component-macro-0.0.0 (c (n "cargo-component-macro") (v "0.0.0") (h "0lm9a7b4ph95kjh3pwa1imp065nq969ldnyf9b7ahc4gcr7da81f")))

(define-public crate-cargo-component-macro-0.2.0 (c (n "cargo-component-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.12.0") (d #t) (k 0)) (d (n "wit-bindgen-rust") (r "^0.12.0") (d #t) (k 0)) (d (n "wit-bindgen-rust-lib") (r "^0.12.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.4") (d #t) (k 0)))) (h "10kgyg60451xy42wfwc7dcij2iyap3rxh3gzrn4cd7abyhjgjbr7")))

(define-public crate-cargo-component-macro-0.3.0 (c (n "cargo-component-macro") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.13.0") (d #t) (k 0)) (d (n "wit-bindgen-rust") (r "^0.13.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.16.0") (d #t) (k 0)))) (h "1kwxcg1xpykdnffs9sp5nxr38gclwshs8jvcz8qdhwyqxhp66kdk")))

(define-public crate-cargo-component-macro-0.3.1 (c (n "cargo-component-macro") (v "0.3.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.13.0") (d #t) (k 0)) (d (n "wit-bindgen-rust") (r "^0.13.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.16.0") (d #t) (k 0)))) (h "10bf07azd3r11gjh41wj8z0a2dg634xwswkih2vnxl03wnkp8fhn")))

(define-public crate-cargo-component-macro-0.4.0 (c (n "cargo-component-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "09s72yic5lnhjhq4zr8zjpinyhh3vi7xilgb7s083ki2cim65s8a")))

(define-public crate-cargo-component-macro-0.4.1 (c (n "cargo-component-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0amrp6yl18jcr7raj7f3gpa7xqdkzldws65q9l6dd3jp55pjjd3z")))

(define-public crate-cargo-component-macro-0.5.0 (c (n "cargo-component-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1cnpxyml1fiv3mv22qzzx4hpf1q3jnl0jgv7gfz4i0rywn69q56r")))

(define-public crate-cargo-component-macro-0.6.0 (c (n "cargo-component-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1xfbad84bsnfw5kbmcr94iix549q19ifklp7nm1jp44fcq5yx671")))

