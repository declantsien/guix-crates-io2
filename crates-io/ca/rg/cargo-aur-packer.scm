(define-module (crates-io ca rg cargo-aur-packer) #:use-module (crates-io))

(define-public crate-cargo-aur-packer-0.1.0 (c (n "cargo-aur-packer") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0vgyfifpcwahcxik777h9rcqzm3yn8gfmxjw9f6k3157cs37s5s8")))

(define-public crate-cargo-aur-packer-0.1.1 (c (n "cargo-aur-packer") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0p4qhsghybr3iil3isnjh3plg12ap9w6kf8xka3g0hyf3z3n9638")))

(define-public crate-cargo-aur-packer-0.1.2 (c (n "cargo-aur-packer") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "152ymy5jhcxq7ig94n474faiwmr9qy1p8ia7xxa6k6kl0ylsbfjc")))

