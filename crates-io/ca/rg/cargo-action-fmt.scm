(define-module (crates-io ca rg cargo-action-fmt) #:use-module (crates-io))

(define-public crate-cargo-action-fmt-0.1.0 (c (n "cargo-action-fmt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0mb0kkcni235368kq7wxabivcvmrczg9k45ycmhwb8qqrpqar2wy")))

(define-public crate-cargo-action-fmt-0.1.1 (c (n "cargo-action-fmt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0znbrrabcz56kd1k172caidppzg9klakbvnj5ssvbmb8nz3jwl5x")))

(define-public crate-cargo-action-fmt-0.1.2 (c (n "cargo-action-fmt") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cr8r4gr2dw55f268dyijlhrk27xpcksbkzqx6m9vrxfp1hmgf41")))

(define-public crate-cargo-action-fmt-0.1.3 (c (n "cargo-action-fmt") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l8mnk652kv01s9hainy6mzwpyfrp3rl7n7qnnv7wpvpnw1k7f2v")))

(define-public crate-cargo-action-fmt-1.0.0 (c (n "cargo-action-fmt") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r4kwj319sqamfqch48ahbxk7s53n5bmdc8qzgn4vzl65jrx6m9c")))

(define-public crate-cargo-action-fmt-1.0.1 (c (n "cargo-action-fmt") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0c8z533h40qqp392xn1s7crxnhrf79clxgvqhql1djbhhgm8p6z1")))

(define-public crate-cargo-action-fmt-1.0.2 (c (n "cargo-action-fmt") (v "1.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "064v4d2s6m2vk92lgc0yjp3vz65795r8b9iz83w9xr9bk17n6dhx")))

(define-public crate-cargo-action-fmt-1.0.3 (c (n "cargo-action-fmt") (v "1.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sqfnswk28496fkpghw51mkkp9qy8nvf73mjzcy14bdqpqgqvz0b")))

(define-public crate-cargo-action-fmt-1.0.4 (c (n "cargo-action-fmt") (v "1.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1d5h0m1bi5v964r5caffx2gc33f8j0drk1rj0xfmyag7cnb0fwpw")))

