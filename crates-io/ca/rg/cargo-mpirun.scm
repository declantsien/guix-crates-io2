(define-module (crates-io ca rg cargo-mpirun) #:use-module (crates-io))

(define-public crate-cargo-mpirun-0.1.0 (c (n "cargo-mpirun") (v "0.1.0") (d (list (d (n "clap") (r "~2.29") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kk6k2a6vn9qa9l98a0yismq240448cbm5an7adi8871n4a13ayd")))

(define-public crate-cargo-mpirun-0.1.1 (c (n "cargo-mpirun") (v "0.1.1") (d (list (d (n "clap") (r "~2.29") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s7jr6zfwprsyksmxq53kzs90fyvm5wdvah4zsx1r5mlryin2mfb")))

(define-public crate-cargo-mpirun-0.1.2 (c (n "cargo-mpirun") (v "0.1.2") (d (list (d (n "clap") (r "~2.29") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03xk6sjxivhf2wqhc84yns2b2vj3adx4gkfp314fz20wzg6b8wvd")))

(define-public crate-cargo-mpirun-0.1.3 (c (n "cargo-mpirun") (v "0.1.3") (d (list (d (n "clap") (r "~2.30") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15gp661nfz2j9pvryx1wx47j6gmc74jrsf680s0g2jchdw1hfwin")))

(define-public crate-cargo-mpirun-0.1.4 (c (n "cargo-mpirun") (v "0.1.4") (d (list (d (n "clap") (r "~2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05i0q46ha7wxzccmkfzy4znvy290wnz8qf6yw46z71hczvkv00qz")))

(define-public crate-cargo-mpirun-0.1.5 (c (n "cargo-mpirun") (v "0.1.5") (d (list (d (n "clap") (r "~2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m31vjrhadqzgw9sgpwzqckal9hvkjryrbddbrrr378x80bp154q")))

(define-public crate-cargo-mpirun-0.1.6 (c (n "cargo-mpirun") (v "0.1.6") (d (list (d (n "clap") (r "~2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xhykb3v35zwa2bldid4rag1488pz8cdq8wjys2lh18jlz2gv6z4")))

(define-public crate-cargo-mpirun-0.1.7 (c (n "cargo-mpirun") (v "0.1.7") (d (list (d (n "cargo_metadata") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "~2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "09zzbfmw7jhxvlp3zygxz7zlzdfsiq7hfgbxymjvnk9lhmi80237")))

(define-public crate-cargo-mpirun-0.1.8 (c (n "cargo-mpirun") (v "0.1.8") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1vqf9hckx719h42hn74ajnf0lqldqxbr520z1v41ra242ca8mvqq")))

