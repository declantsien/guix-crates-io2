(define-module (crates-io ca rg cargo-weblabtest) #:use-module (crates-io))

(define-public crate-cargo-weblabtest-0.2.2 (c (n "cargo-weblabtest") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0f05g1l4xcdpnaq82sjjc68gf9zlvc83ac35d3vvsf2aahchxg92")))

(define-public crate-cargo-weblabtest-0.2.3 (c (n "cargo-weblabtest") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0rgw152skg1izcip2j4hb16pspck0nq8sksnd5m7b9p5a1vsbg5x")))

(define-public crate-cargo-weblabtest-0.2.4 (c (n "cargo-weblabtest") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "158710vcflhdqha98vajp6ghc8xf1p0sj4kmjmnznhjxy7vak70h")))

(define-public crate-cargo-weblabtest-0.2.5 (c (n "cargo-weblabtest") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0r52j55j9x5aj01iy061ajg52c9b9qxb10qhsmdb53vcmj2zmsy1")))

(define-public crate-cargo-weblabtest-0.2.7 (c (n "cargo-weblabtest") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0hd1lpdy8ln1jn1jchw12yaifwcrxxsd532h8m7p767n35pyfyak")))

(define-public crate-cargo-weblabtest-0.2.10 (c (n "cargo-weblabtest") (v "0.2.10") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "1kpdaf52fcsz04mq3bhy827ykyz0cddgw9nhrh76yfh3slji1j1w")))

(define-public crate-cargo-weblabtest-0.2.12 (c (n "cargo-weblabtest") (v "0.2.12") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0x51ddqsj7pp5xr9wvqinhc7m7m4iks5hgmgn3hdpb6717mp7bkz")))

(define-public crate-cargo-weblabtest-0.2.36 (c (n "cargo-weblabtest") (v "0.2.36") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_xml_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "simple_xml_serialize_macro") (r "^0.3.0") (f (quote ("process_options"))) (d #t) (k 0)))) (h "0qqsy3gbmv8kkhxv69b4jk68jzsrppfh6m96q5xfxrni619c0hp0")))

