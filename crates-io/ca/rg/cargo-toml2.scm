(define-module (crates-io ca rg cargo-toml2) #:use-module (crates-io))

(define-public crate-cargo-toml2-1.0.0 (c (n "cargo-toml2") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0axbiimzy1q06909ffi6djfd59vagkbxaj4ql991pgphrkh7w3r6") (y #t)))

(define-public crate-cargo-toml2-1.1.0 (c (n "cargo-toml2") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "110kflb25lda4ig1wbx2yggs2if5lsidwjkxn7gp2b9lz7zgbqbl") (y #t)))

(define-public crate-cargo-toml2-1.2.0 (c (n "cargo-toml2") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1khcfh6phzhvgzvv2ij5aqdnrl15rqpwpvz50bwny37hy38z3dz9") (y #t)))

(define-public crate-cargo-toml2-1.2.1 (c (n "cargo-toml2") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0nl6gr8dln8z7ivcgdcpmj6zk8v3v507qhr1l9cy1ijp290cszwa") (y #t)))

(define-public crate-cargo-toml2-1.2.2 (c (n "cargo-toml2") (v "1.2.2") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1rxby81wqs4881paqb01rbcwapsjanbc2cy2kdpw71mkcbnmh9n6") (y #t)))

(define-public crate-cargo-toml2-1.2.3 (c (n "cargo-toml2") (v "1.2.3") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "04p7xsgbp2ikj0swjlx2y5pi48n86frckyrv2zgc0mfmf827409v")))

(define-public crate-cargo-toml2-1.3.0 (c (n "cargo-toml2") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1f6axpklmv69avgdqx6pv62r34i9r6j2qlp0s1jgvc7422w7rp07")))

(define-public crate-cargo-toml2-1.3.1 (c (n "cargo-toml2") (v "1.3.1") (d (list (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "18zvsk8ik0mi3llcyjdzfvchba4nq3adpck6gxc0qxaa97rfwlwa")))

(define-public crate-cargo-toml2-1.3.2 (c (n "cargo-toml2") (v "1.3.2") (d (list (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0zc1jxczg626a00f9l71jav6aq1ks0a187j37dsl4qgs0aq8kdi2")))

