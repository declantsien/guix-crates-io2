(define-module (crates-io ca rg cargo-godot) #:use-module (crates-io))

(define-public crate-cargo-godot-0.1.0 (c (n "cargo-godot") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "1zlqq4w41icf35y46amz76mzrmb98zi9qx485gb7kvqcrrshpzh3")))

