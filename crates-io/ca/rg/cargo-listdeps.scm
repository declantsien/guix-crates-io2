(define-module (crates-io ca rg cargo-listdeps) #:use-module (crates-io))

(define-public crate-cargo-listdeps-0.1.0 (c (n "cargo-listdeps") (v "0.1.0") (d (list (d (n "cargo-license") (r "^0.4.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1m69hhmx6iqhhil0a6akx0wqrbcqv30xfdcnjd4jqwvw8szylh8w")))

