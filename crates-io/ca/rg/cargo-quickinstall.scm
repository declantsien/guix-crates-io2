(define-module (crates-io ca rg cargo-quickinstall) #:use-module (crates-io))

(define-public crate-cargo-quickinstall-0.1.0 (c (n "cargo-quickinstall") (v "0.1.0") (h "00ns3n180c9lrmbrpbkx97xiy9bijhymkzz48k4w58gbbcqh0245")))

(define-public crate-cargo-quickinstall-0.1.1 (c (n "cargo-quickinstall") (v "0.1.1") (h "0c8pnk4kf5qmh32zgkcjdx18rjhnnwg6qq56iwkjps6zydi36b1a")))

(define-public crate-cargo-quickinstall-0.1.2 (c (n "cargo-quickinstall") (v "0.1.2") (h "1fyz66604286agpmi30fj29hpg6zqisf70rmsv49c8l2i7g09d4p")))

(define-public crate-cargo-quickinstall-0.1.3 (c (n "cargo-quickinstall") (v "0.1.3") (h "1msrj203xb5zwb2y3mvahq59dx2vf911lpm1wqayjwmhb3bj5x9v")))

(define-public crate-cargo-quickinstall-0.1.4 (c (n "cargo-quickinstall") (v "0.1.4") (h "03vij684ydd4g48p8lklsbnm57sd6dcz2wvng940fv23b5fjacq9")))

(define-public crate-cargo-quickinstall-0.1.5 (c (n "cargo-quickinstall") (v "0.1.5") (d (list (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "03fijc2m0a2nc5z0nvh0p846i6zdk1ng59zzgz1c73qfhd5bwdkm")))

(define-public crate-cargo-quickinstall-0.1.6 (c (n "cargo-quickinstall") (v "0.1.6") (d (list (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1n543in48njn0bzypgw8is03i0lqz02kvmjic4nld82nygq5sxil")))

(define-public crate-cargo-quickinstall-0.1.7 (c (n "cargo-quickinstall") (v "0.1.7") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1qa4gpkynhbb049l680bmzvhpzlp4ijmy67aw2f4wjjjyfzdnlkp")))

(define-public crate-cargo-quickinstall-0.1.8 (c (n "cargo-quickinstall") (v "0.1.8") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "06vh64d9wps0bkgfrarxy0k6gacw91c72n864ay3dmjqfqr10sdz")))

(define-public crate-cargo-quickinstall-0.1.9 (c (n "cargo-quickinstall") (v "0.1.9") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0yym3rjl2vm3mygq476dssbzpmv5r8jw28y8ckp10xl138n86rlc")))

(define-public crate-cargo-quickinstall-0.2.0 (c (n "cargo-quickinstall") (v "0.2.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0hwzabdilsn16nkbgwwcrxcg5xspnl4h9ap3lhrm8y6qpj7lmf12")))

(define-public crate-cargo-quickinstall-0.2.1 (c (n "cargo-quickinstall") (v "0.2.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1rbjl82rzgq21rp2b4b6d2sicgn7ksnahjlrcnrfljflqwny4wn6")))

(define-public crate-cargo-quickinstall-0.2.2 (c (n "cargo-quickinstall") (v "0.2.2") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0lipvc5n42rb71p4gg4qn516vhabm24jz0aabi1ybjd11jl3k53g")))

(define-public crate-cargo-quickinstall-0.2.3 (c (n "cargo-quickinstall") (v "0.2.3") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0lajdyi1zxlsizdrvx4b813fvqjav5lkibxg6cff3wqwcd7pdhs2")))

(define-public crate-cargo-quickinstall-0.2.4 (c (n "cargo-quickinstall") (v "0.2.4") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0xkghrq8r2iwwn7myzwh56d00fi93spm1wvnrf9ghq4jyri6s4bl")))

(define-public crate-cargo-quickinstall-0.2.5 (c (n "cargo-quickinstall") (v "0.2.5") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "17ygpgh68761vvrqlqc6a01vl6vbrz2bjch2nfgm0d6bzf5f8afs")))

(define-public crate-cargo-quickinstall-0.2.6 (c (n "cargo-quickinstall") (v "0.2.6") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1g83cgkd0p7svb3avzaybii1xx0m4lcp22nbjp3j3y5h2qnrhgca")))

(define-public crate-cargo-quickinstall-0.2.7 (c (n "cargo-quickinstall") (v "0.2.7") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1ccadb2lhp60mzbx13hj41dfd2r6ri9f2hzrx49y0zr9xxpx0a7m")))

(define-public crate-cargo-quickinstall-0.2.8 (c (n "cargo-quickinstall") (v "0.2.8") (d (list (d (n "guess_host_triple") (r "^0.1.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (f (quote ("alloc"))) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0cqrb173nwmp8sfynj8s992yr2qk2k57n4a09039g5ryqwhw0v84")))

(define-public crate-cargo-quickinstall-0.2.9 (c (n "cargo-quickinstall") (v "0.2.9") (d (list (d (n "guess_host_triple") (r "^0.1.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "13p2srvdhs143khlc3a73w0d8pjwi1gwhf3q1magicpc2zxzypfb")))

(define-public crate-cargo-quickinstall-0.2.10 (c (n "cargo-quickinstall") (v "0.2.10") (d (list (d (n "guess_host_triple") (r "^0.1.3") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.1") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0j27rqx7nlb26zak0yjdva4ynxs4zgaw9nl67kai2jxgpqfnjkvi")))

