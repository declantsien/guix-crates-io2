(define-module (crates-io ca rg cargo-breaking) #:use-module (crates-io))

(define-public crate-cargo-breaking-0.0.1 (c (n "cargo-breaking") (v "0.0.1") (h "0z4ncgm9zhvzlvax0fjckcznhmpnmwk30151mdyfbv51h8gn48id")))

(define-public crate-cargo-breaking-0.0.2 (c (n "cargo-breaking") (v "0.0.2") (h "01pgas0717j734vzcaba7kxgild03ibnb1li5b2ki9gr3wxxyj2w")))

