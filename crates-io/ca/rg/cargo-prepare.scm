(define-module (crates-io ca rg cargo-prepare) #:use-module (crates-io))

(define-public crate-cargo-prepare-0.0.0 (c (n "cargo-prepare") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1g1l3sfk9x8wf659l5hqnfibrcia15ajcfnrpirnhixbp14mirkb")))

