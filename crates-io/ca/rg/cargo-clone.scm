(define-module (crates-io ca rg cargo-clone) #:use-module (crates-io))

(define-public crate-cargo-clone-0.1.0 (c (n "cargo-clone") (v "0.1.0") (d (list (d (n "cargo") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1m6h67g946nvwvb8lm95w2z7bdqsc3w3qh6f1yypx3s4c94x9k5d")))

(define-public crate-cargo-clone-0.1.1 (c (n "cargo-clone") (v "0.1.1") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "10svb26bmbdpri8n5flpcdrrzs2h1lal0wxai9gknrcys3p8n2ay")))

(define-public crate-cargo-clone-0.1.2 (c (n "cargo-clone") (v "0.1.2") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1s9jaj60mgk82kwflp19b86fr81gycpazm9pz0gszspq716scynm")))

(define-public crate-cargo-clone-0.1.3 (c (n "cargo-clone") (v "0.1.3") (d (list (d (n "cargo") (r "^0.30.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0hlmwr0wc1zdbsynnxvs4v4r20kw8ywp45fbzrzwm1lb51rya0pc")))

(define-public crate-cargo-clone-0.1.4 (c (n "cargo-clone") (v "0.1.4") (d (list (d (n "cargo") (r "^0.40.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0ai15jqlxnkwwkdqcyzlbh7phl1li6cas5xzyzk78af2rx7g833q")))

(define-public crate-cargo-clone-0.2.0 (c (n "cargo-clone") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cargo") (r "^0.58.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1ngx97kk5f5cawysm92cmgif2130w4mwvlgxm0hkracc41aqxy64")))

(define-public crate-cargo-clone-1.0.0 (c (n "cargo-clone") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cargo") (r "^0.58.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0sb3q655clwmvy3937649f3a64gcb5axk3838rc4mp5ddnsv8nrq")))

(define-public crate-cargo-clone-1.0.1 (c (n "cargo-clone") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1k2zksf13hxjjbis9m17b8w7ld3k028l1032z10aryw5jljy0azx")))

(define-public crate-cargo-clone-1.1.0 (c (n "cargo-clone") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "cargo-clone-core") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1z2k93x1arwpsfh87qsg2zil4pp9v1r5b4csbbyqpzafgl4w9f94") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-clone-1.2.0 (c (n "cargo-clone") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo") (r "^0.66.0") (d #t) (k 0)) (d (n "cargo-clone-core") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0vjvlcimg5d83z5rk6jmg7ch7nk2m948hy20bpl0dg3vlhyrizsj") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

(define-public crate-cargo-clone-1.2.1 (c (n "cargo-clone") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo") (r "^0.75.1") (d #t) (k 0)) (d (n "cargo-clone-core") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0w2bnsggkm9n8m0dad4l6w1gjyw3282580dan0v4nrj2rwlvnvh7") (f (quote (("vendored-openssl" "cargo/vendored-openssl"))))))

