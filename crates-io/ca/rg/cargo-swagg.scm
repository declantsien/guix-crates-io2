(define-module (crates-io ca rg cargo-swagg) #:use-module (crates-io))

(define-public crate-cargo-swagg-0.0.0 (c (n "cargo-swagg") (v "0.0.0") (d (list (d (n "actix-swagger") (r "^0.0") (d #t) (k 0)))) (h "1qbbnw7fhd2a6c68vy7xlijnw05gkn265fs07ksg5v7di42b14im")))

(define-public crate-cargo-swagg-0.1.0 (c (n "cargo-swagg") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "swagg") (r "^0.2.0") (d #t) (k 0)))) (h "1bxa2dcmbb7068crnyckq6gz592rjmk5ffgn1wcsdpjixa65blmb")))

(define-public crate-cargo-swagg-0.3.0 (c (n "cargo-swagg") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "swagg") (r "^0.3.0") (d #t) (k 0)))) (h "0ssbvidm8s7hkqskm63chp75lkgvqq9ahs4gfyd4sws19mricksl")))

