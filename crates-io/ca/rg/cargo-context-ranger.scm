(define-module (crates-io ca rg cargo-context-ranger) #:use-module (crates-io))

(define-public crate-cargo-context-ranger-0.0.1 (c (n "cargo-context-ranger") (v "0.0.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)) (d (n "wl-clipboard-rs") (r "^0.8.1") (d #t) (k 0)))) (h "11c2mq2wcngpdia1kzngr1c62z4z0ld13ac9z4h4dbp1ysi7v5qr") (y #t)))

(define-public crate-cargo-context-ranger-0.0.2 (c (n "cargo-context-ranger") (v "0.0.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)) (d (n "wl-clipboard-rs") (r "^0.8.1") (d #t) (k 0)))) (h "08smnnd4xr2i1l4mzj8nwf1dmh03b2ncmjlnrgammllgidp9n5mr")))

