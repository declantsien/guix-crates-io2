(define-module (crates-io ca rg cargo-test-junit) #:use-module (crates-io))

(define-public crate-cargo-test-junit-0.1.0 (c (n "cargo-test-junit") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1wzz9zspvpdnphvx240wx7gz2hy6kdcpvzdmqzzvzib1yyhsg6b2")))

(define-public crate-cargo-test-junit-0.2.0 (c (n "cargo-test-junit") (v "0.2.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1w91a3p04qpzsh3d0icbz8iw2mcd80i89kr7n1ccg2llxhp1v9mk")))

(define-public crate-cargo-test-junit-0.3.0 (c (n "cargo-test-junit") (v "0.3.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "0yw15mbm4asmj3wzcair167zv94aq1ybbbafjq8acjrs7j4gmbwx") (y #t)))

(define-public crate-cargo-test-junit-0.4.0 (c (n "cargo-test-junit") (v "0.4.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "06r177zmqv2p2c5694p8k2z7p9vriafgvbfsbsb1h3h3q8qchcfa") (y #t)))

(define-public crate-cargo-test-junit-0.5.0 (c (n "cargo-test-junit") (v "0.5.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1wpnqlz6jarcr27bbajbxm2jvsg1xlp441q79zfbvjz43w52bnkc") (y #t)))

(define-public crate-cargo-test-junit-0.6.0 (c (n "cargo-test-junit") (v "0.6.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1gibc7gm8ibz036vfglc7957nwzh2b6f14nfc02z24ns0a8a39g3") (y #t)))

(define-public crate-cargo-test-junit-0.6.1 (c (n "cargo-test-junit") (v "0.6.1") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.1") (d #t) (k 0)))) (h "1iymxdr7x5px8j8r5mn68p56nk5y7hdlshxr6iywzv6pya3pb37w")))

(define-public crate-cargo-test-junit-0.6.2 (c (n "cargo-test-junit") (v "0.6.2") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.2") (d #t) (k 0)))) (h "0p139iz67ksjmyzndksq5jc9f83x1nr4hipqphp5n2fawkdc7wwx")))

(define-public crate-cargo-test-junit-0.6.3 (c (n "cargo-test-junit") (v "0.6.3") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1z8bjpwh8zllg6rs0fc6x5mj5fy22xjv0pw8v91km8d4qvlcgxpn")))

(define-public crate-cargo-test-junit-0.6.4 (c (n "cargo-test-junit") (v "0.6.4") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "15i5j0mkary35p9c5i87nd0wbglrcjwphba5mnl8ixgj4yv5px5r")))

(define-public crate-cargo-test-junit-0.6.5 (c (n "cargo-test-junit") (v "0.6.5") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "16yigi4ycp1n083nx7hg4mpiwmijxv2ncpgrm267c94hcsxzvja8")))

(define-public crate-cargo-test-junit-0.6.6 (c (n "cargo-test-junit") (v "0.6.6") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0sk369q8xkj6i0pncw4l082wn9h7jf9aiv0bnc9q1qz08aqcjgyc")))

(define-public crate-cargo-test-junit-0.6.7 (c (n "cargo-test-junit") (v "0.6.7") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0k0nrjl6h9ry0s9rjmrj66l05x5if202g908800rbxrcr4jkkndz")))

