(define-module (crates-io ca rg cargo-single-pyo3) #:use-module (crates-io))

(define-public crate-cargo-single-pyo3-0.1.0 (c (n "cargo-single-pyo3") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1glb1rg4i5kqckipbsk6lia4d0m0shp6gz61n2dswbw2fd0glk34")))

(define-public crate-cargo-single-pyo3-0.1.1 (c (n "cargo-single-pyo3") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1cw47afb732sv2jbrzr9n8g82sa4q46cy26gll1znnzndmgmlfcs")))

(define-public crate-cargo-single-pyo3-0.1.2 (c (n "cargo-single-pyo3") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02nsq23c73xyx9h2b0gkmqyb6smj3qr9w42wb65xvnklc403j0xy")))

(define-public crate-cargo-single-pyo3-0.1.3 (c (n "cargo-single-pyo3") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0m9g233d9pzii0wyd6x70h4f2spi5nz0nsqf7lw72l9sr4h1gynz")))

