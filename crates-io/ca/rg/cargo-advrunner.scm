(define-module (crates-io ca rg cargo-advrunner) #:use-module (crates-io))

(define-public crate-cargo-advrunner-0.1.0 (c (n "cargo-advrunner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0qiqq623j3dpvr4bcan839m7q2i6y87h3h9pag52dgzz1wb2qsz0")))

(define-public crate-cargo-advrunner-0.2.0 (c (n "cargo-advrunner") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0lfkdknldmafxrchw1z0wi1jdnzvr2s05avg22bx4zka0r8v3f0k")))

