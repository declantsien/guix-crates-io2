(define-module (crates-io ca rg cargo-dotenv) #:use-module (crates-io))

(define-public crate-cargo-dotenv-1.0.0 (c (n "cargo-dotenv") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env") (r "^0.3.2") (d #t) (k 0) (p "env2")))) (h "0qmiv6yl94w58valphsywb9k91f6bckv52x4lz42ycnb2prk140y")))

(define-public crate-cargo-dotenv-1.0.1 (c (n "cargo-dotenv") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env") (r "^0.3.2") (d #t) (k 0) (p "env2")))) (h "03kb0q0npkw1c4jb1w35b4d7aw87dc60mbs1qkiqnzgd2g5jaw1z")))

