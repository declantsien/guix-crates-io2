(define-module (crates-io ca rg cargo-bin) #:use-module (crates-io))

(define-public crate-cargo-bin-0.1.0 (c (n "cargo-bin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1chwig502vy94v2lzlyz9qikmd832a4yzp61mady41iradxmd9h4")))

(define-public crate-cargo-bin-0.2.0 (c (n "cargo-bin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0iqk14r8l7ix2b9b2vskcz44n6yxwkrfw3ij0whm5z38r4sghdmq")))

(define-public crate-cargo-bin-0.3.0 (c (n "cargo-bin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1snrywignfl9pjya419c3ibdy48sk0dqdblh15k9f7bnadaflq52")))

