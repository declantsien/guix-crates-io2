(define-module (crates-io ca rg cargo-bootimage) #:use-module (crates-io))

(define-public crate-cargo-bootimage-0.1.0 (c (n "cargo-bootimage") (v "0.1.0") (h "0f89bsw5sfh2zkakvd7686d85p5avhbb712014s6206kksszhmx6")))

(define-public crate-cargo-bootimage-0.1.1 (c (n "cargo-bootimage") (v "0.1.1") (h "0r03p03s6w8a5nwf27c7vn13gqb89bjwp235m1npx7sc31lcyla9")))

(define-public crate-cargo-bootimage-0.1.2 (c (n "cargo-bootimage") (v "0.1.2") (h "0ansfpckrah9q8m1grjbss9w8wgyg25pxdca22633ajr86cd5n8s")))

(define-public crate-cargo-bootimage-0.1.3 (c (n "cargo-bootimage") (v "0.1.3") (h "0q728hx39gx9c3nxbn1zdjl9q2rbgv3l8j2ly04i2v0bryycm5dz")))

