(define-module (crates-io ca rg cargo-builds) #:use-module (crates-io))

(define-public crate-cargo-builds-0.1.0 (c (n "cargo-builds") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0fggxs0sx03xsfhbwaai1sjq91kq5rsips421pmp3pqxrbggglzm")))

