(define-module (crates-io ca rg cargo-edit-version) #:use-module (crates-io))

(define-public crate-cargo-edit-version-0.1.0 (c (n "cargo-edit-version") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0z2cfl4rw5x9li74a26v5mhk64gis5w8k0hr81nkch8941nbhcxg")))

