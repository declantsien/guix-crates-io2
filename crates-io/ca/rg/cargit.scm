(define-module (crates-io ca rg cargit) #:use-module (crates-io))

(define-public crate-cargit-0.2.7 (c (n "cargit") (v "0.2.7") (d (list (d (n "gmec") (r "^0.0.1") (d #t) (k 0)))) (h "02zl0lsarbq69rmnam3hl0fpnpcs4410wywrwxg9w0g267gfk776")))

(define-public crate-cargit-0.2.9 (c (n "cargit") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "gmec") (r "^0.0.1") (d #t) (k 0)))) (h "0szwzwrs9wij5lni6rigcr9a7b04lc8x3fkb5kc33jd5qdy4zwlf")))

(define-public crate-cargit-0.2.10 (c (n "cargit") (v "0.2.10") (d (list (d (n "gmec") (r "^0.0.3") (d #t) (k 0)))) (h "1kzyyxzn2y5k4n3ikkvva8ivx33ziq72m7cnkhw2ikpb6pp5sy1h")))

(define-public crate-cargit-0.2.11 (c (n "cargit") (v "0.2.11") (d (list (d (n "gmec") (r "^0.0.3") (d #t) (k 0)))) (h "190nxx59c28akx0x2fpafk7gx47bhf815hfayn3c9bb34xzsbnnq")))

(define-public crate-cargit-0.3.0 (c (n "cargit") (v "0.3.0") (d (list (d (n "gmec") (r "^0.0.3") (d #t) (k 0)))) (h "1hrpb1cmmhgaamsavpqi5911yry5k7phkqa10c777dqblv4ah8hg")))

