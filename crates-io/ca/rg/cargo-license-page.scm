(define-module (crates-io ca rg cargo-license-page) #:use-module (crates-io))

(define-public crate-cargo-license-page-0.1.0 (c (n "cargo-license-page") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11") (d #t) (k 0)) (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "0bnakrbqs0hzglzgyfc3i8kq320cxlqjsq2haz5ljy26b60dhcq4")))

