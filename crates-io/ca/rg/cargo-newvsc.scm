(define-module (crates-io ca rg cargo-newvsc) #:use-module (crates-io))

(define-public crate-cargo-newvsc-1.0.0 (c (n "cargo-newvsc") (v "1.0.0") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "16x7f0ssvs194z1x5hzbr9cm4cdyiqiw04dcmfwpnarpim44zxsg")))

(define-public crate-cargo-newvsc-1.0.1 (c (n "cargo-newvsc") (v "1.0.1") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "094mq5sjigzh8la8pmlq10xni8hymi173wylk4c4sf5sv1ylpwnj")))

(define-public crate-cargo-newvsc-1.0.2-alpha.0 (c (n "cargo-newvsc") (v "1.0.2-alpha.0") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "18pfa7la6id7rfzsqlzs38abjg9lll5j3jqm0xp8kf09hn8pqmqg")))

(define-public crate-cargo-newvsc-1.0.2 (c (n "cargo-newvsc") (v "1.0.2") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "1rf07fyswl0av7483rl3ynpwhvv88hqqrnk9012cim7b6009f9gx")))

(define-public crate-cargo-newvsc-1.0.3 (c (n "cargo-newvsc") (v "1.0.3") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "1g2mc27mya3pcdg7bklk10i5lmny5da6757b0l1dx0fwdyvynn2c")))

(define-public crate-cargo-newvsc-1.0.4 (c (n "cargo-newvsc") (v "1.0.4") (d (list (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)))) (h "0yxzfigxwllmg166lhxb0903fp0csqr8y89kl660jrxdn6rxh5ag")))

