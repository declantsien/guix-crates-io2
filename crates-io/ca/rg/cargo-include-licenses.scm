(define-module (crates-io ca rg cargo-include-licenses) #:use-module (crates-io))

(define-public crate-cargo-include-licenses-0.0.1 (c (n "cargo-include-licenses") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1wj4pi9hjr2qj1zkk7j2z39hg69mjfz6ds0s8996v3jraj63zdah")))

