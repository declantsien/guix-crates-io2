(define-module (crates-io ca rg cargo-run-script) #:use-module (crates-io))

(define-public crate-cargo-run-script-0.1.0 (c (n "cargo-run-script") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0lrdsh5brnkhkr27a91zrc2rqk0l8h0yb6978j0d2075hamz5cmx")))

(define-public crate-cargo-run-script-0.2.0 (c (n "cargo-run-script") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1nj8jjjjnikicmjvcsw9ybkfjzgfhi6xjpp3g5bzmdpfc1622w1x")))

