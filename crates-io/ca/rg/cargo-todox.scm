(define-module (crates-io ca rg cargo-todox) #:use-module (crates-io))

(define-public crate-cargo-todox-0.1.0 (c (n "cargo-todox") (v "0.1.0") (h "03linba996ch2j5aqp8dwlg5ssg4v0f08w7jl0p8bamq3c539bm1") (y #t)))

(define-public crate-cargo-todox-0.1.1 (c (n "cargo-todox") (v "0.1.1") (h "0bvgpkg6vsgjgqi2hmx313vrqnqvwrhdj5iqr2alfxy7aycaf929") (y #t)))

(define-public crate-cargo-todox-0.1.2 (c (n "cargo-todox") (v "0.1.2") (h "0wy2qr0g30dmxr1251sq0yjrq0qfsalsasx2k0wzgdb24nm1m3w4") (y #t)))

(define-public crate-cargo-todox-0.1.3 (c (n "cargo-todox") (v "0.1.3") (h "0dq7ryvi8nf2x6phz882fqf15gxp198k3k73y10kyz8f8a66382f") (y #t)))

(define-public crate-cargo-todox-0.1.4 (c (n "cargo-todox") (v "0.1.4") (h "0fm8p05l6wi81jvqcissw9b7m5s3jda4zcwdvf43lbjddnh65w17") (y #t)))

(define-public crate-cargo-todox-0.1.5 (c (n "cargo-todox") (v "0.1.5") (h "1bdsmsvvga1j10zc62ipiabfjyavaz0pn3gzfmzavlvqlbi2anca") (y #t)))

(define-public crate-cargo-todox-0.1.6 (c (n "cargo-todox") (v "0.1.6") (d (list (d (n "version") (r "^2.0.1") (d #t) (k 0)))) (h "1x05ch9b6d9gh8cgaam1asxszqqx8ckkv29kjm4c92lvgs8flbcz")))

(define-public crate-cargo-todox-0.1.7 (c (n "cargo-todox") (v "0.1.7") (d (list (d (n "version") (r "^2.0.1") (d #t) (k 0)))) (h "01s64g4d5ii0amycs046cbdxy4z37j0y12467jjqcv9d9n60j302")))

(define-public crate-cargo-todox-0.1.8 (c (n "cargo-todox") (v "0.1.8") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 2)))) (h "19mf545v7x5h8y7l2al5cpydn0xl3jyq1zlfyv7vgln6h6hyk6la")))

(define-public crate-cargo-todox-0.1.9 (c (n "cargo-todox") (v "0.1.9") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 2)))) (h "0kinb4f72fjl04swwjw3lhr55z5ic1nch7xqc923mpkflz760nw0")))

(define-public crate-cargo-todox-0.2.0 (c (n "cargo-todox") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 2)))) (h "1c2d0jfr3lbniayjagh6cnyki35y4wh0pjnj7sjwc9fp3hvail55")))

(define-public crate-cargo-todox-0.2.1 (c (n "cargo-todox") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 2)))) (h "1sb0di8dfp82h51hbwb65aflqv51zxi763j83afzwc3swsjqc5nd")))

(define-public crate-cargo-todox-0.2.2 (c (n "cargo-todox") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 2)))) (h "1qry0gxxhcs164wrcvbxi5r2cjdd7ib0k33i59z22swhyhnpp2w3")))

(define-public crate-cargo-todox-0.2.3 (c (n "cargo-todox") (v "0.2.3") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.7") (d #t) (k 2)))) (h "0hx7xdbflwswmkv9vmx5yzcdpj69zk5xnqipsbdp95r2kaxxi68v")))

(define-public crate-cargo-todox-0.2.4 (c (n "cargo-todox") (v "0.2.4") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.7") (d #t) (k 2)))) (h "0f0b4wf48lqr7z5a43aipgyzvz4j6yqnr2jb91jf6b46jh0yn93c")))

