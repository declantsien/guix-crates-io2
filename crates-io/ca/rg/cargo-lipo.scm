(define-module (crates-io ca rg cargo-lipo) #:use-module (crates-io))

(define-public crate-cargo-lipo-0.1.0 (c (n "cargo-lipo") (v "0.1.0") (d (list (d (n "clap") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "130cpc01w50sxdfrdva7r7i05fric4p3ppaq3hdmvq0ilb3n6pbn")))

(define-public crate-cargo-lipo-1.0.0 (c (n "cargo-lipo") (v "1.0.0") (d (list (d (n "clap") (r "^2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0gp0fqc0idwjpdxrpyqxr1xpwgx9qfmasx7h8s9r5b3zks3y440k")))

(define-public crate-cargo-lipo-1.0.1 (c (n "cargo-lipo") (v "1.0.1") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "115r0a8s7zhzzg7hbp0rvqxpdp3gk2drahyim9n3wma7rsam7r0v")))

(define-public crate-cargo-lipo-1.0.2 (c (n "cargo-lipo") (v "1.0.2") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0if5w88fpfr0v5srr54kziybp7g5xlg3c3ni7h45cqx7chsh77b8")))

(define-public crate-cargo-lipo-1.0.3 (c (n "cargo-lipo") (v "1.0.3") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0azr40gj3dj4s6gndk1vvch5shyllr92hp8id68ppdl7fl0wslwn")))

(define-public crate-cargo-lipo-2.0.0-beta-1 (c (n "cargo-lipo") (v "2.0.0-beta-1") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "1jm3lqvdhnmiqwqb8jwviq0pw1q9j9n45y69d9fldldprgh6g2zf")))

(define-public crate-cargo-lipo-2.0.0-beta-2 (c (n "cargo-lipo") (v "2.0.0-beta-2") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0806fb87qzy7yf8fika0fc8y4nsikdlmsqbrxffzkyhf82rky4vh")))

(define-public crate-cargo-lipo-2.0.0-beta-3 (c (n "cargo-lipo") (v "2.0.0-beta-3") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "11iha9k65kgrrv20lr49mq6pbhqyw24gfc9p54a13md2mxmw3vqx")))

(define-public crate-cargo-lipo-2.0.0 (c (n "cargo-lipo") (v "2.0.0") (d (list (d (n "clap") (r "~2.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0k9n7wxn3qkj8m01apmkwpwdq3vis68rvj77ky49lr9mbppn0pgh")))

(define-public crate-cargo-lipo-3.0.0 (c (n "cargo-lipo") (v "3.0.0") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0sbd5a8p5mfaasjv6vpc0b32l57mkmzmzb8h53ijlm1jxz15qy1d")))

(define-public crate-cargo-lipo-3.0.1 (c (n "cargo-lipo") (v "3.0.1") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0lvsakn42ni8yxnpsbs3xqcrccdpxy4nnzj3pc7bqbazhbr0wz4q")))

(define-public crate-cargo-lipo-3.1.0 (c (n "cargo-lipo") (v "3.1.0") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0p3r4m8pwf0dqi5jmqg1kw5is81mlbx8nnqap5jldmm5nyk73iln")))

(define-public crate-cargo-lipo-3.1.1 (c (n "cargo-lipo") (v "3.1.1") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "15i9ispjrzdyjjw8jjjhhxxdka5dxx62dsa721zppp2jrk1464qz")))

(define-public crate-cargo-lipo-3.2.0 (c (n "cargo-lipo") (v "3.2.0") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0bwi6gibankz1kiv6lsh21qqxsxhag80mkmm73byk73wpcw1hmh2")))

(define-public crate-cargo-lipo-3.3.0 (c (n "cargo-lipo") (v "3.3.0") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0wa6k3pfif4w2z76myiyc83bapbqv6gzrv1j0qi1qqc8i2v2m6i2")))

(define-public crate-cargo-lipo-3.3.1 (c (n "cargo-lipo") (v "3.3.1") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0l0asyj5iv2dvb587s2k7qp3pqzls98ilnijb6d1iw161rykbl4w")))

