(define-module (crates-io ca rg cargo-works) #:use-module (crates-io))

(define-public crate-cargo-works-0.1.0 (c (n "cargo-works") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0hsaxggyj516f7hg028r7ilgl0jcn5issb8xahcvxdjmryrxmfyc")))

(define-public crate-cargo-works-0.1.1 (c (n "cargo-works") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "17gjwh7m3kvpjfynbv3jq8q9bamn6ch7m5qxgix8zs5m7v7aymaq")))

(define-public crate-cargo-works-0.1.2 (c (n "cargo-works") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0s9068pk97ngl34d9g29szgcm7pzprr3k1i47zk10529pk2g36jp")))

(define-public crate-cargo-works-0.1.3 (c (n "cargo-works") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1cqbiqnk26qkbn35yh4dpsqs5ivyln6da3995pxwrd15byazbi6c")))

(define-public crate-cargo-works-0.1.4 (c (n "cargo-works") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0hs587wgna97lazv0hqr6qarv9342n19l6dnz0njl4wak5smiz2n")))

(define-public crate-cargo-works-0.1.5 (c (n "cargo-works") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0xns4gkaf6m75fj3y1hl5adinlr5c67hz1qw50hiwpd6fjkzmhbh")))

(define-public crate-cargo-works-0.1.6 (c (n "cargo-works") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1750fmw9cjjm19d75jiy82sy5nghw4d1bgpm1jcyccphiss715ag")))

(define-public crate-cargo-works-0.1.7 (c (n "cargo-works") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "147kqi6sqvxcxpiwri3z99hx57ic94q4bkvbn893j2736d5fp6wi")))

(define-public crate-cargo-works-0.1.8 (c (n "cargo-works") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1lsq62sg178jy9aq9xcqm181rkqbgchr2i1zaxrrkm2ii5hz41d6")))

(define-public crate-cargo-works-0.1.9 (c (n "cargo-works") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0xbwcrvsmqlwk5rm5dlf6cplg50adm4fkzxpfgi6qmcis9r8gbza")))

(define-public crate-cargo-works-0.2.0 (c (n "cargo-works") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0fnskmpkv8khmfh6w32p34qmpv2viqw45drm0n9g5b1wc86g3xw4")))

(define-public crate-cargo-works-0.2.1 (c (n "cargo-works") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "06lwmmgiraqwrfpxy3inpxqyvagnyysx64s9s5gky7zfindys6xk")))

(define-public crate-cargo-works-0.2.2 (c (n "cargo-works") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1v9i5v1wjq420qj0j3lnh93wncjhvx3xhpzcij51bnihkwj1b6nx")))

