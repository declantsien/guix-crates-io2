(define-module (crates-io ca rg cargo-subcommand) #:use-module (crates-io))

(define-public crate-cargo-subcommand-0.1.0 (c (n "cargo-subcommand") (v "0.1.0") (d (list (d (n "cargo-project") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0wyi5ch3lhrrbis4q0fqd66kf1b00x40bpakl2haaa0814mz6753")))

(define-public crate-cargo-subcommand-0.2.0 (c (n "cargo-subcommand") (v "0.2.0") (d (list (d (n "cargo-project") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "012f7s9h9wxazbs3837vqgp5hlxl7a1wyqb065hii09l5s2bgm0f")))

(define-public crate-cargo-subcommand-0.3.0 (c (n "cargo-subcommand") (v "0.3.0") (d (list (d (n "cargo-project") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1m86fpanhq2cw51c5al6f94ivmzdjdd2f4w50gh6pjlfh9j5ay2q")))

(define-public crate-cargo-subcommand-0.4.0 (c (n "cargo-subcommand") (v "0.4.0") (d (list (d (n "cargo-project") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1b49pf26lkkynfpjd0gqfg9rb0hpd6znw2v07zj9ackgll7lfs5g")))

(define-public crate-cargo-subcommand-0.4.1 (c (n "cargo-subcommand") (v "0.4.1") (d (list (d (n "cargo-project") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0m5c512jp08r4k1fr8zd2zihvkdblscn0fbhsk8ain869d4nil4i")))

(define-public crate-cargo-subcommand-0.4.2 (c (n "cargo-subcommand") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0c7qm9lv0kgh4rwbiwv9pk7i71z2q1sdfmhqra0gwj2111v547vg")))

(define-public crate-cargo-subcommand-0.4.3 (c (n "cargo-subcommand") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0i0kgk32dc9xsbqaic727f104gnnjjv3f6xd72zai4yjz8vp15ps")))

(define-public crate-cargo-subcommand-0.4.4 (c (n "cargo-subcommand") (v "0.4.4") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1f3h17yhqg0s7b41n59g8197kff48rahq09ydb2rvj9cz6czdwll")))

(define-public crate-cargo-subcommand-0.4.5 (c (n "cargo-subcommand") (v "0.4.5") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1a0dskbkwbw5rr67yj1j12vvbnnwhpf4lgy80cc38wpflh7qs1b7")))

(define-public crate-cargo-subcommand-0.4.6 (c (n "cargo-subcommand") (v "0.4.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0jqqn2ivsalmx0k1029yi9klam3cn0x970agbg8v4zqdgcn3ydl6")))

(define-public crate-cargo-subcommand-0.4.7 (c (n "cargo-subcommand") (v "0.4.7") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1k6iqb9yb7r4jgjir90cagbjaxp7r95x6sig07z67i0ngjp8hvwr")))

(define-public crate-cargo-subcommand-0.4.8 (c (n "cargo-subcommand") (v "0.4.8") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0hjkkwj5r4f59bharm975w1zv0jbdhavdmabbp68za6rd1ribr6w")))

(define-public crate-cargo-subcommand-0.4.9 (c (n "cargo-subcommand") (v "0.4.9") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0p5bzkaycdi5v8jydsrm8jrqs2dzpb1zlsc28lbdygmk3cm40vp8")))

(define-public crate-cargo-subcommand-0.4.10 (c (n "cargo-subcommand") (v "0.4.10") (d (list (d (n "dunce") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0h230kqbds4g2vi19lrawr14gy4zxjg2f3jbj81iqvpjyn17avlb")))

(define-public crate-cargo-subcommand-0.5.0 (c (n "cargo-subcommand") (v "0.5.0") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1fay69wmbzd0fxgya6ia2wsmijq0lymmnh7lfscc74qni5shhqpl")))

(define-public crate-cargo-subcommand-0.5.1 (c (n "cargo-subcommand") (v "0.5.1") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0m9kbis7p4k15aq797x32ivswk2bsyh7g8myqd8rdvzg54rlxwr2")))

(define-public crate-cargo-subcommand-0.6.0 (c (n "cargo-subcommand") (v "0.6.0") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1dvab98b3lsd2252c0gq44yl88lijbw05b8psiizp913k7drm174")))

(define-public crate-cargo-subcommand-0.7.0 (c (n "cargo-subcommand") (v "0.7.0") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0yab5vj7iljphcvv97a1ia01mdj38fzxp70ijny0dm8clzk0yibw")))

(define-public crate-cargo-subcommand-0.8.0 (c (n "cargo-subcommand") (v "0.8.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "current_platform") (r "^0.2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1ng4nx0j1pxjmg33d054m24jf3w3iid59nf3fg2dmdbzy8a2vgvk") (f (quote (("default" "clap"))))))

(define-public crate-cargo-subcommand-0.9.0 (c (n "cargo-subcommand") (v "0.9.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "current_platform") (r "^0.2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "15rvkxmpw51bgbzx6l047w8i3lq66sk1qp3190fd8d94b1s1mhm0") (f (quote (("default" "clap"))))))

(define-public crate-cargo-subcommand-0.10.0 (c (n "cargo-subcommand") (v "0.10.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "current_platform") (r "^0.2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1dw8ygdcjj5mfy05rs4s26gmggcd5kss9yflzq7wkjb6q6kz143l") (f (quote (("default" "clap"))))))

(define-public crate-cargo-subcommand-0.11.0 (c (n "cargo-subcommand") (v "0.11.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "current_platform") (r "^0.2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1insijzrbs4sfqfr9k388bx227nhs8wiyp39sj88qkjky4xgx60q") (f (quote (("default" "clap"))))))

(define-public crate-cargo-subcommand-0.12.0 (c (n "cargo-subcommand") (v "0.12.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "current_platform") (r "^0.2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1lq5j2gq2rw99scb5x4zbw99m4a5lmq6agzxwhppgpfh9zjrlzbq") (f (quote (("default" "clap"))))))

