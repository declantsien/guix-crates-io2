(define-module (crates-io ca rg cargo-xcodebuild) #:use-module (crates-io))

(define-public crate-cargo-xcodebuild-0.1.0 (c (n "cargo-xcodebuild") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-subcommand") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md") (r "^0.1") (d #t) (k 0) (p "mobile-device")) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "x509-parser") (r "^0.12") (d #t) (k 0)))) (h "04x60lxip62jqg7gxjpvjjwpx0b3m34fdyj2lcr0c2mhpxgv6wsi")))

