(define-module (crates-io ca rg cargo-vsc) #:use-module (crates-io))

(define-public crate-cargo-vsc-0.1.0 (c (n "cargo-vsc") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xq3yha3lv580g6sxijz41zlf5v4a7yfbrvfm85amxb9q84v8al4")))

(define-public crate-cargo-vsc-0.1.1 (c (n "cargo-vsc") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lm475sg9s8mcalqhpn4vkfxijiqkf3a9bgy0j9qmaqml0frk6px")))

