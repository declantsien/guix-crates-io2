(define-module (crates-io ca rg cargo-atomic-patch) #:use-module (crates-io))

(define-public crate-cargo-atomic-patch-0.0.1 (c (n "cargo-atomic-patch") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "14jvlmlbdnw6qsw2h3kk33ang9rnnwf8a4zz14r27s4h29xs1pgb")))

