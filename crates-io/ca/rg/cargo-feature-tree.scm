(define-module (crates-io ca rg cargo-feature-tree) #:use-module (crates-io))

(define-public crate-cargo-feature-tree-0.1.0 (c (n "cargo-feature-tree") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)))) (h "18q8rxan9wk5g6qcmmyrz5gsz0v7din2indirf16shdywk0yzqxf")))

(define-public crate-cargo-feature-tree-0.1.1 (c (n "cargo-feature-tree") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xksy3cdx0qmzbz8cr4askkv3dcqzbrrnnrlf8d9r7ipz8k3w4a9")))

(define-public crate-cargo-feature-tree-0.2.0 (c (n "cargo-feature-tree") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.15.2") (d #t) (k 0)) (d (n "termtree") (r "^0.4.1") (d #t) (k 0)))) (h "1xgqmybc7yzfy53m8616gf1r1xnidkmsnvavxh5pl0nlg0kpkq5r")))

(define-public crate-cargo-feature-tree-0.2.1 (c (n "cargo-feature-tree") (v "0.2.1") (d (list (d (n "cargo_toml") (r "^0.15.2") (d #t) (k 0)) (d (n "termtree") (r "^0.4.1") (d #t) (k 0)))) (h "0jni8iqk9x3yzraw44wy6nghw8r4imhfy8yyawdfjfyjr9r7v01z")))

(define-public crate-cargo-feature-tree-0.2.2 (c (n "cargo-feature-tree") (v "0.2.2") (d (list (d (n "cargo_toml") (r "^0.15.3") (d #t) (k 0)) (d (n "termtree") (r "^0.4.1") (d #t) (k 0)))) (h "0p31k5c1wmnj0ap7kih0r6drz5fwfxi8k7dyjzi9xqc5hx693awq")))

