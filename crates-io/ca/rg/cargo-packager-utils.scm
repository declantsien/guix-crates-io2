(define-module (crates-io ca rg cargo-packager-utils) #:use-module (crates-io))

(define-public crate-cargo-packager-utils-0.0.0 (c (n "cargo-packager-utils") (v "0.0.0") (h "1ijgl49qn842iv86rzrlldrp1f48dx61bman7ad8x45kv0gkcmqg")))

(define-public crate-cargo-packager-utils-0.1.0 (c (n "cargo-packager-utils") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("url" "preserve_order" "derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ry1pwrnx35vlbz86ban6hw4v80mnjh5gy2iy41fbzlwg6jv2dfl") (f (quote (("schema" "schemars") ("default" "cli") ("cli")))) (s 2) (e (quote (("serde" "dep:serde") ("clap" "dep:clap"))))))

