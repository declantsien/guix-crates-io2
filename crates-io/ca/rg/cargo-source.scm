(define-module (crates-io ca rg cargo-source) #:use-module (crates-io))

(define-public crate-cargo-source-0.0.1 (c (n "cargo-source") (v "0.0.1") (d (list (d (n "clap") (r "^4.3.15") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1wj6qygwmllpcwsvway0m8r9xsif5f9fi5rxwsf1c78jj0xaaq9i") (y #t)))

(define-public crate-cargo-source-0.0.2 (c (n "cargo-source") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "0q4hjhbai22dk91h44l0grh3jygw7f6wi8dim8m2k0bna8qvq0sj") (y #t)))

(define-public crate-cargo-source-0.0.3 (c (n "cargo-source") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "0kz45r9l2llnas5gxd3pd6qf35w1q85s019m7mwh1l5cblvp00c1")))

(define-public crate-cargo-source-0.0.31 (c (n "cargo-source") (v "0.0.31") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "1p41s7fcndrqj083fgar64svqw5l9psxn169fmxha8x06dzgjyqx")))

(define-public crate-cargo-source-0.1.0-beta.1 (c (n "cargo-source") (v "0.1.0-beta.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "0h05spkjm32qg08hkvbsm21ja5gw13sfnzwy60z111wy33jqz43n")))

(define-public crate-cargo-source-0.1.1 (c (n "cargo-source") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "15bbgrzax5361gyq2n24c655chns4a7124nrk3q894w8hzkmghiw") (y #t)))

(define-public crate-cargo-source-0.1.2 (c (n "cargo-source") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "1dgpglbg460wx3sjmnv7z2h80xwhmkj8zvcy7fvl0n9n2961ls24")))

