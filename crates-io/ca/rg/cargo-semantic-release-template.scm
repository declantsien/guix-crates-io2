(define-module (crates-io ca rg cargo-semantic-release-template) #:use-module (crates-io))

(define-public crate-cargo-semantic-release-template-0.1.0 (c (n "cargo-semantic-release-template") (v "0.1.0") (h "053kd8swpwdsp6ypmjwslgxxhg8lsywhjkdrlm9dhkjznqc29gnm")))

(define-public crate-cargo-semantic-release-template-1.4.0 (c (n "cargo-semantic-release-template") (v "1.4.0") (h "09bbh95zrcf8sym136aq5rzbin6v69fw44gmlg847v0851r495w5")))

(define-public crate-cargo-semantic-release-template-1.5.0 (c (n "cargo-semantic-release-template") (v "1.5.0") (h "02hi8kyfm1m19vw5akx7p0ma8bvrqjn0nd448mpk41xshp369j68")))

(define-public crate-cargo-semantic-release-template-1.6.0 (c (n "cargo-semantic-release-template") (v "1.6.0") (h "169kkhv1sh0jz5272dk5879jkxizxxdlb3bb42yn2wr1kr71gqii")))

(define-public crate-cargo-semantic-release-template-1.7.0 (c (n "cargo-semantic-release-template") (v "1.7.0") (h "0jq2x9xcgcrjbzv1brm7babvg22gpamdv8w65il61hpmbm249c9m")))

(define-public crate-cargo-semantic-release-template-1.8.0 (c (n "cargo-semantic-release-template") (v "1.8.0") (h "16gzwm7hxji0jwcxfjmnlsrr2yp4l5dk7zcq9p7j8hzllglc1xs1")))

(define-public crate-cargo-semantic-release-template-1.9.0 (c (n "cargo-semantic-release-template") (v "1.9.0") (h "1lx4zwbzwldy1mg6sh6g3m5nwy9v18ivzsyfly4ajk2f05qr432p")))

(define-public crate-cargo-semantic-release-template-1.10.0 (c (n "cargo-semantic-release-template") (v "1.10.0") (h "0rwkpl45sjrwhzspkw7c1hgwpbz5rj3a4zw3phaq9zhqjklhkapk")))

(define-public crate-cargo-semantic-release-template-1.11.0 (c (n "cargo-semantic-release-template") (v "1.11.0") (h "1dm4w43kkfgnhswk882phg8s9s43j7w6946a3r1ajfh2d0gcmg39")))

