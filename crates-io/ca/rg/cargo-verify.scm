(define-module (crates-io ca rg cargo-verify) #:use-module (crates-io))

(define-public crate-cargo-verify-0.2.3 (c (n "cargo-verify") (v "0.2.3") (d (list (d (n "combine") (r "^4.2.1") (f (quote ("regex"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "verified") (r "^0.2.3") (d #t) (k 0)))) (h "0y7hwprw57wnamyhsymggjzri33pdqv3xh6dkafxw682kw7x0zcb")))

