(define-module (crates-io ca rg cargo-semver) #:use-module (crates-io))

(define-public crate-cargo-semver-1.0.0-alpha.1 (c (n "cargo-semver") (v "1.0.0-alpha.1") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "predicates") (r "^1.0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "seahorse") (r "^1.1") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02fv3g62psyfb4ghs8scfvh2dk1cwpvxigaf6yvlp6fb51fnqm4h")))

(define-public crate-cargo-semver-1.0.0-alpha.2 (c (n "cargo-semver") (v "1.0.0-alpha.2") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "predicates") (r "^1.0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1l5xch3q23in6qna01jd3kvqs2945h324p4xlbgrf747gi5wy1nq")))

(define-public crate-cargo-semver-1.0.0-alpha.3 (c (n "cargo-semver") (v "1.0.0-alpha.3") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "predicates") (r "^1.0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1m61xiw2gq4g4sj5r5nf9a4sjhr1nw7il2cc3hry813q6xnsp6qm")))

