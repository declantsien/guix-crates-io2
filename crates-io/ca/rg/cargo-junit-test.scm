(define-module (crates-io ca rg cargo-junit-test) #:use-module (crates-io))

(define-public crate-cargo-junit-test-0.1.0 (c (n "cargo-junit-test") (v "0.1.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1av4nvv9bx9zq8bi92bc8zb6al661f20kfm9wy6f32k7ax447ln3") (y #t)))

(define-public crate-cargo-junit-test-0.1.1 (c (n "cargo-junit-test") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1y0gjl5320z6dn90a5yxbidq6z77cny4b8rxj8j7f47n8nh5p6v1") (y #t)))

(define-public crate-cargo-junit-test-0.2.0 (c (n "cargo-junit-test") (v "0.2.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "18l9rf35ll347xwrh6f2r3x48yy65q48qd6bw8f6gnp17v88xwdz") (y #t)))

