(define-module (crates-io ca rg cargo-post) #:use-module (crates-io))

(define-public crate-cargo-post-0.1.0 (c (n "cargo-post") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)))) (h "1yl5ws52iv84pl4f3dgvx18j7m652a2hi2dh5n91vhnmsgmdaa2b")))

(define-public crate-cargo-post-0.1.1 (c (n "cargo-post") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)))) (h "1r3sryisk4h0x3srj4zcd0rx9h843nhbyggphic4n5s2dwz1fzj7")))

(define-public crate-cargo-post-0.1.2 (c (n "cargo-post") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "12kzg46201qh1wqd1hk9q4rwipldsj80hlnyl170vq4lzvnjnny0")))

(define-public crate-cargo-post-0.1.3 (c (n "cargo-post") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1f53fgdz1arjxsjhbsrdy3axi2gan969k05l84pgxfzq4qkn1l3h")))

(define-public crate-cargo-post-0.1.4 (c (n "cargo-post") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0225229w5cm1qackysfji30cj1igj35h2qz8ykpd85z19h8jdk98")))

(define-public crate-cargo-post-0.1.5 (c (n "cargo-post") (v "0.1.5") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1md4235091zw4049cckgva4yp6by575gsi7yrpylwflqmz4bz2pi")))

(define-public crate-cargo-post-0.1.6 (c (n "cargo-post") (v "0.1.6") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0b5nan3d86hfkakkpzacnpg8hfsrcz73835drgj6sx18smahbav0")))

(define-public crate-cargo-post-0.1.7 (c (n "cargo-post") (v "0.1.7") (d (list (d (n "cargo_metadata") (r "^0.7.3") (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0y6hwqrwpfgk1nl24g9w7ms2164i85jm36z3zh8hiywfgp38y1mp")))

