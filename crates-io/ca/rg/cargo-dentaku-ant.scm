(define-module (crates-io ca rg cargo-dentaku-ant) #:use-module (crates-io))

(define-public crate-cargo-dentaku-ant-0.1.0 (c (n "cargo-dentaku-ant") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0zdx0m3ggbmaf33cb4xafahwn56sla85dz8hvmc11px0hixvc3a2")))

(define-public crate-cargo-dentaku-ant-0.1.1 (c (n "cargo-dentaku-ant") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0vfin1imvvr8lf7cbjafp8wdvqr3gf225fppczjviwgknbf9sz74")))

