(define-module (crates-io ca rg cargo-canoe) #:use-module (crates-io))

(define-public crate-cargo-canoe-0.1.0 (c (n "cargo-canoe") (v "0.1.0") (h "01mcjng8621k9bg2zmjn8jjwi0s94sz8a0vq76a2yizi10565gl6") (y #t)))

(define-public crate-cargo-canoe-0.1.1 (c (n "cargo-canoe") (v "0.1.1") (h "07ij47z7ysy1ylz9mr8yjc9l0a5jqyjnnm99068p9klg4jsv6ps5") (y #t)))

(define-public crate-cargo-canoe-0.1.2 (c (n "cargo-canoe") (v "0.1.2") (h "07cps8a57fmsbcp2izkk41lxjxl46n6j2h43av0xjwh3gvh5rl21") (y #t)))

(define-public crate-cargo-canoe-6.6.6 (c (n "cargo-canoe") (v "6.6.6") (h "1lfrbwf5xnqqk8zrggry4cv68ff9c8w4va2k8dg24klf6xbk1wr7") (y #t)))

(define-public crate-cargo-canoe-6.6.6-ferris (c (n "cargo-canoe") (v "6.6.6-ferris") (d (list (d (n "term-painter") (r "^0.2.4") (d #t) (k 0)))) (h "0panhxqs0p7mkqlcsl21178rny9vrd1fpalb9iwfxq43qjfmnwsp") (y #t)))

(define-public crate-cargo-canoe-6.6.6-69 (c (n "cargo-canoe") (v "6.6.6-69") (h "1v1dwn4234y9x2asfvcrvk7b0rqvv08mwqa8n30qyak7h4r76khr") (y #t)))

(define-public crate-cargo-canoe-6.7.8-putting-the-rust-in-crustacean (c (n "cargo-canoe") (v "6.7.8-putting-the-rust-in-crustacean") (d (list (d (n "term-painter") (r "^0.2.4") (d #t) (k 0)))) (h "1h5y7bfydak6jpcdpp3j2hf67vz8q0c02gaz19i2bk8y5y1hvzd2") (y #t)))

