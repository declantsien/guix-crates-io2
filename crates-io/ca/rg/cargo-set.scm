(define-module (crates-io ca rg cargo-set) #:use-module (crates-io))

(define-public crate-cargo-set-0.1.0 (c (n "cargo-set") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env" "cargo"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (d #t) (k 0)))) (h "1cwsq7zz0lxrypjl35i0qb60cixq6amqzzy2xqzzi7910l964557")))

(define-public crate-cargo-set-0.1.1 (c (n "cargo-set") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env" "cargo"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (d #t) (k 0)))) (h "07jplhazzvdkgq0fb51viz1anwkwi49bjkf1wfz79lwmdrn3wkl7")))

(define-public crate-cargo-set-0.1.2 (c (n "cargo-set") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env" "cargo"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (d #t) (k 0)))) (h "02z95c7z7i13xrszlgvivm43mijggfa2gqv4p9kpk9n8kiiaxd3x")))

