(define-module (crates-io ca rg cargo-genner) #:use-module (crates-io))

(define-public crate-cargo-genner-0.1.0 (c (n "cargo-genner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("fs" "rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1bwh8pp0m7nrxa1lzsndnyaq1py632la1shzqzyfck2iq95ka89n")))

(define-public crate-cargo-genner-0.1.1 (c (n "cargo-genner") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("fs" "rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0z570nxb0g18s4h5dbynlb1zihjqkg4a01pihfkvr60gjjyxh6wf")))

