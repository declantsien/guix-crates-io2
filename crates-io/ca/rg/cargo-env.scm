(define-module (crates-io ca rg cargo-env) #:use-module (crates-io))

(define-public crate-cargo-env-0.1.0 (c (n "cargo-env") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)))) (h "1ypxpfvj5g62by0zgcd9a2r7h75ah2rbs5lgcj3b9a1xa4p76d1m")))

(define-public crate-cargo-env-0.1.1 (c (n "cargo-env") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)))) (h "0hnyljcnwygnsnisablc1q24v86bw6hkmijqca2jg4wdcrzhmh6x")))

