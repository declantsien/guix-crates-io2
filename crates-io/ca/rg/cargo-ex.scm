(define-module (crates-io ca rg cargo-ex) #:use-module (crates-io))

(define-public crate-cargo-ex-0.1.0 (c (n "cargo-ex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "084pm522gdw1mv1ikpk7ca0kdy1rby5rhi2i812jiklvz28kjsby")))

(define-public crate-cargo-ex-0.1.1 (c (n "cargo-ex") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "173mln6hhvwhyfmv01h8d6n0wnr3in59l2n5alshww6mscq083j3")))

