(define-module (crates-io ca rg cargo-metrics) #:use-module (crates-io))

(define-public crate-cargo-metrics-0.1.0 (c (n "cargo-metrics") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)))) (h "0f5jn4zavb3hzrs5ifrmnf1hwjkbvip4bkminqi060q7fgzzczdd")))

(define-public crate-cargo-metrics-0.1.1 (c (n "cargo-metrics") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)))) (h "1rf8kd7ijfxmagpmbrg113w5zdzlrarhcj4rfh9yc12234c17s7p")))

