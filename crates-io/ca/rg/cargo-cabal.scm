(define-module (crates-io ca rg cargo-cabal) #:use-module (crates-io))

(define-public crate-cargo-cabal-0.1.0 (c (n "cargo-cabal") (v "0.1.0") (h "1a4zdgg0wspwrvjjd2qgb7nqbk8cb2pn8qyw1blfqrjkzs45vrrv")))

(define-public crate-cargo-cabal-0.7.0 (c (n "cargo-cabal") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q2jvqjn00a49hlf0nza9h37w1w5x22j68i0i98a0bbinvmihdks") (r "1.62.1")))

(define-public crate-cargo-cabal-0.7.1 (c (n "cargo-cabal") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "16qbjmgb4scp16x5dyk2wzxwwy33idi14lyjhmhavggbr0zk7daw") (y #t) (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.0 (c (n "cargo-cabal") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0v71143c7xnxddb0z0lkckmsq2377565lbpcd7z37h5rkg1wvgy8") (y #t) (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.1 (c (n "cargo-cabal") (v "0.8.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "046nbwz9xcq4fq038z1ydbc54ki73yn5010sbvgia55x998p2xvb") (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.2 (c (n "cargo-cabal") (v "0.8.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05z37rghqsrsxnsp0vp7534s9p4r1nih16m6gqi6qm7pv0zyj8zs") (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.3 (c (n "cargo-cabal") (v "0.8.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1f47jg72jgblcx8zjm4jpjxlnzkr81b18rrg9zwkxmcb3vlvqkwy") (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.4 (c (n "cargo-cabal") (v "0.8.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0qxnph24mq0bw17ivhnm7501281x2f9b8qj7r56fqv35g8dfma12") (r "1.62.1")))

(define-public crate-cargo-cabal-0.8.5 (c (n "cargo-cabal") (v "0.8.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jzc14nh08acqmpglv072s3wv7zdkfvx4yz2y1h1krriq951xamr") (r "1.62.1")))

(define-public crate-cargo-cabal-0.9.0 (c (n "cargo-cabal") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0j8lcg7z91aj2zs0kb33d1dd1bcda4krrw056ih18lmgn020bwxd") (r "1.62.1")))

