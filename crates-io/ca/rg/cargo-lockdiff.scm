(define-module (crates-io ca rg cargo-lockdiff) #:use-module (crates-io))

(define-public crate-cargo-lockdiff-0.1.0 (c (n "cargo-lockdiff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-lock") (r "^4") (k 0)) (d (n "paw") (r "^1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "124q8y9vz00h9gabkrlwan8hk6p8g0w6rnvdjf49qwsccxynk1lm")))

(define-public crate-cargo-lockdiff-0.2.0 (c (n "cargo-lockdiff") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-lock") (r "^4") (k 0)) (d (n "paw") (r "^1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ldpfghhcx4hsisia1aspx7ql7lchcqcwgrynan7fc4zcc0b1b6w")))

(define-public crate-cargo-lockdiff-0.3.0 (c (n "cargo-lockdiff") (v "0.3.0") (d (list (d (n "cargo-lock") (r "^7") (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "env"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cdx7hv9b781g61sfndnaah2a8sg69drd9lry28ncrzc565c1a6m")))

(define-public crate-cargo-lockdiff-0.3.1 (c (n "cargo-lockdiff") (v "0.3.1") (d (list (d (n "cargo-lock") (r "^7") (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "env"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k2x9sxrbji4m5ma44flyp74qj1pb5k2dbkp2gxb7r4ci1l8n4fx")))

