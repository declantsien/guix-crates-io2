(define-module (crates-io ca rg cargo-rune) #:use-module (crates-io))

(define-public crate-cargo-rune-0.1.0 (c (n "cargo-rune") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 1)) (d (n "rune") (r "^0.6.16") (d #t) (k 0)) (d (n "runestick") (r "^0.6.16") (d #t) (k 0)))) (h "0s5db9nkldwv3dvl7jy2kn25lgwl66nmwn9bdncayw6zq65smcwi")))

(define-public crate-cargo-rune-0.1.1 (c (n "cargo-rune") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 1)) (d (n "rune") (r "^0.6.16") (d #t) (k 0)) (d (n "runestick") (r "^0.6.16") (d #t) (k 0)))) (h "15b812l0gha66akwqjwvrv8n9pd9szqvmxi0pkhn0lggxkmy897q")))

(define-public crate-cargo-rune-0.1.2 (c (n "cargo-rune") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 1)) (d (n "rune") (r "^0.6.16") (d #t) (k 0)) (d (n "runestick") (r "^0.6.16") (d #t) (k 0)))) (h "0w2ply36jl2294qahpv95kx6wl4a7kf00q0kddap9rnizbrc4l8q")))

(define-public crate-cargo-rune-0.1.3 (c (n "cargo-rune") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 1)) (d (n "rune") (r "^0.6.16") (d #t) (k 0)) (d (n "runestick") (r "^0.6.16") (d #t) (k 0)))) (h "11njr80rag4vdcvm5cizym3sm81mgwah0qr1yf35l49y2klxcg44")))

(define-public crate-cargo-rune-0.1.4 (c (n "cargo-rune") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 1)) (d (n "rune") (r "^0.6.16") (d #t) (k 0)) (d (n "runestick") (r "^0.6.16") (d #t) (k 0)))) (h "1ixpkr48zayx4c1lb0b3bqazlyzl3fj8r4qc4w9ji51gvxm4szc9")))

