(define-module (crates-io ca rg cargo-files) #:use-module (crates-io))

(define-public crate-cargo-files-0.1.0 (c (n "cargo-files") (v "0.1.0") (d (list (d (n "cargo-files-core") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a2gkjml2bvj34qahnfh9hnnpd3vsmi19kwpc3l89lk63msblcrp")))

(define-public crate-cargo-files-0.2.0 (c (n "cargo-files") (v "0.2.0") (d (list (d (n "cargo-files-core") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g5c3c5s538bvl8r7mb2b3qzn9w572wk7s0im67nx19cglmd2pmy")))

(define-public crate-cargo-files-0.2.1 (c (n "cargo-files") (v "0.2.1") (d (list (d (n "cargo-files-core") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xcmi5j4hkjy818pwkd5jzcplw3lh37ya0s2ffdlcp4ip9hv3rzn")))

(define-public crate-cargo-files-0.3.0 (c (n "cargo-files") (v "0.3.0") (d (list (d (n "cargo-files-core") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mhkvsqb3j7k3nzp70rb398vbfxd429q95bkf9qrxfgv4nhm9ikh")))

(define-public crate-cargo-files-0.3.1 (c (n "cargo-files") (v "0.3.1") (d (list (d (n "cargo-files-core") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fpcslll02dxv9akr3i3y47zy67hvm2r1mf5wwgvr13ngf5bhipq")))

