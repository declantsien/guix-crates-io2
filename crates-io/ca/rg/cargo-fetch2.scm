(define-module (crates-io ca rg cargo-fetch2) #:use-module (crates-io))

(define-public crate-cargo-fetch2-0.1.0 (c (n "cargo-fetch2") (v "0.1.0") (h "102qv1fxd7l2gnaxn9sclqqsk15wv8kvfskmylbqav84wz8mippj")))

(define-public crate-cargo-fetch2-0.1.1 (c (n "cargo-fetch2") (v "0.1.1") (h "10m7ahbs7cqfmwcpyw9nvdclmjsk88ysafznjcshk4l5n15mlrsp")))

