(define-module (crates-io ca rg cargo-name) #:use-module (crates-io))

(define-public crate-cargo-name-0.1.0 (c (n "cargo-name") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "1xmrg2n2s9ykijj5bxd4i92l9f35qwlzvyngf7w76kar1rlhw6gn")))

(define-public crate-cargo-name-1.0.0 (c (n "cargo-name") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "16ygbr94kpb6vafvbaflqhv29ig0c4x3agacvhbip535cjxk33q1")))

(define-public crate-cargo-name-1.0.1 (c (n "cargo-name") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "0gd937aazyjzsdpvbgnr37bfn1r20s16b9byq8v36kq8ckqvl54p")))

