(define-module (crates-io ca rg cargo-ruukh) #:use-module (crates-io))

(define-public crate-cargo-ruukh-0.0.2 (c (n "cargo-ruukh") (v "0.0.2") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "warp") (r "^0.1.4") (d #t) (k 0)))) (h "09w5i7907jkl14sq6isxa0v3ky773bya2k9qbl66zh3jvgy9jxgp")))

(define-public crate-cargo-ruukh-0.0.3 (c (n "cargo-ruukh") (v "0.0.3") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "warp") (r "^0.1.6") (d #t) (k 0)))) (h "0d2fb11bna1sva8wlvmn6cf7l1ks8386pckabidga3gn81bb9jvf")))

