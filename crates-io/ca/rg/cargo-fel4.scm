(define-module (crates-io ca rg cargo-fel4) #:use-module (crates-io))

(define-public crate-cargo-fel4-0.7.1 (c (n "cargo-fel4") (v "0.7.1") (d (list (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "cmake_config") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fel4-config") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vx0nqd8mrn5zddid7l395mf0xa9qlsgpwr18n6xhpancghf4m7y")))

(define-public crate-cargo-fel4-0.7.2 (c (n "cargo-fel4") (v "0.7.2") (d (list (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "cmake_config") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fel4-config") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gsfffhsq8iass1d59parl2i51xn9rx8lsr6zcsl5gb15xywy2mv")))

