(define-module (crates-io ca rg cargo-foobar) #:use-module (crates-io))

(define-public crate-cargo-foobar-1.0.0 (c (n "cargo-foobar") (v "1.0.0") (h "1aczqysy72pfcshfwf2jralzps9l0f7hbhdl7qil5k79qyim2q8b")))

(define-public crate-cargo-foobar-1.0.1 (c (n "cargo-foobar") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)))) (h "1g75420lcrbrfp3b0vm7j1fp6w6ndcjb0dd3n4r0gkms5831pajx") (f (quote (("foo" "cli") ("cli" "atty" "structopt") ("bar" "cli"))))))

(define-public crate-cargo-foobar-1.0.2 (c (n "cargo-foobar") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)))) (h "02cim12s8chdrrnxgqvlql3yk6m4i4pdm3nfx1r2d55dz00yym93") (f (quote (("foo" "cli") ("default" "foo" "bar") ("cli" "atty" "structopt") ("bar" "cli"))))))

