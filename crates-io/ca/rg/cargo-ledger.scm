(define-module (crates-io ca rg cargo-ledger) #:use-module (crates-io))

(define-public crate-cargo-ledger-1.1.0 (c (n "cargo-ledger") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xjsd1367mswmai381msxv05cj3asma5m9a1ki4a9hdgmfjspdmz")))

(define-public crate-cargo-ledger-1.2.0 (c (n "cargo-ledger") (v "1.2.0") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qdfmgvv4yzp2ydyjy3yv2hcwr3c2vkg2j9m3s80ra6f1nzbdx14")))

(define-public crate-cargo-ledger-1.2.1 (c (n "cargo-ledger") (v "1.2.1") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gg8xjpyq3b7byfhla7q1wpr8gsj8rgzky21ygmbmp7cc9d8l4a1")))

(define-public crate-cargo-ledger-1.2.2 (c (n "cargo-ledger") (v "1.2.2") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vdlz7fygh50d33rpzfhj3wwvnk7y7l8za0341gzkmp77kxnzn03")))

(define-public crate-cargo-ledger-1.2.3 (c (n "cargo-ledger") (v "1.2.3") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xidfsjn2f47qsbpbk5bpd5zalqzrfl2m8m47vjy18dyzlhg2093")))

(define-public crate-cargo-ledger-1.2.4 (c (n "cargo-ledger") (v "1.2.4") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m5c75qkz4qf150kwxsyad76wzvjim0diylb99cl0rps3hs5yjb5")))

(define-public crate-cargo-ledger-1.3.0 (c (n "cargo-ledger") (v "1.3.0") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yjriz98yamj9zpaadyfkpzid6vwlra2v2798vcyl9fv2cdbarq8")))

(define-public crate-cargo-ledger-1.4.0 (c (n "cargo-ledger") (v "1.4.0") (d (list (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03wkhg7byq1bwkg903kgv616z1m9bs12lmy69np8q552y717lvf5")))

