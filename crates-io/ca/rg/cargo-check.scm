(define-module (crates-io ca rg cargo-check) #:use-module (crates-io))

(define-public crate-cargo-check-0.1.0 (c (n "cargo-check") (v "0.1.0") (h "0c7p0a490j8slwjqrmhy559j1x53h8jvkvnnl4gvli7ays9hbv0d")))

(define-public crate-cargo-check-0.2.0 (c (n "cargo-check") (v "0.2.0") (d (list (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1gfa3b0v63vb35xwpsvjapp725wvc3ckq4zbl20fdr71kv9dpifs")))

(define-public crate-cargo-check-0.2.1 (c (n "cargo-check") (v "0.2.1") (d (list (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1zf6aqgvidw9p82nrm3mkjakzcirkd7vpcklv3l2xfh7j76iqwpk")))

(define-public crate-cargo-check-0.2.2 (c (n "cargo-check") (v "0.2.2") (d (list (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "10yrvm4mfgn5j84svrfqazr7qwan6d0jxaqmjfvf5m2s6fbs8pg8")))

