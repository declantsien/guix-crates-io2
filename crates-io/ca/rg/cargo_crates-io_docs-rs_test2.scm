(define-module (crates-io ca rg cargo_crates-io_docs-rs_test2) #:use-module (crates-io))

(define-public crate-cargo_crates-io_docs-rs_test2-0.1.0 (c (n "cargo_crates-io_docs-rs_test2") (v "0.1.0") (d (list (d (n "cargo_crates-io_docs-rs_test") (r "^0.1.0") (d #t) (k 0)))) (h "1rzkrzy3krn0hpyc5bvgd7dmpj49mwbvrbilss5f0mrs85z3niis")))

(define-public crate-cargo_crates-io_docs-rs_test2-0.1.1 (c (n "cargo_crates-io_docs-rs_test2") (v "0.1.1") (d (list (d (n "cargo_crates-io_docs-rs_test") (r "^0.1.0") (d #t) (k 0)))) (h "0yix53y6cn7llz4cs099d83156bhc3mxmyxq7r5nhm0hhjxnswfv")))

