(define-module (crates-io ca rg cargo-heaptrack) #:use-module (crates-io))

(define-public crate-cargo-heaptrack-0.1.0 (c (n "cargo-heaptrack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yziymq8bjbk1rjc95xd0fvjhp5bjp8vczs6rf8mmb0lcsbrq3lp")))

