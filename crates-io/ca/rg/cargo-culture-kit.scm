(define-module (crates-io ca rg cargo-culture-kit) #:use-module (crates-io))

(define-public crate-cargo-culture-kit-0.1.0 (c (n "cargo-culture-kit") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1mks1afc3yx2ax2kyd3m4zn7n37pg2cibhxs83l9n2ylrx38sb27") (f (quote (("default"))))))

(define-public crate-cargo-culture-kit-1.0.0 (c (n "cargo-culture-kit") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0hsbrs467xba4pj23iaw958aigk2m35zha6r3iicmjnm8mjr2p4f") (f (quote (("default"))))))

