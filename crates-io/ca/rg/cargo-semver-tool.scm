(define-module (crates-io ca rg cargo-semver-tool) #:use-module (crates-io))

(define-public crate-cargo-semver-tool-0.1.1 (c (n "cargo-semver-tool") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.11") (d #t) (k 0)))) (h "1x1pyjwv67i5x8bqcby8n6l8lr8m1xbn14nvc3zqb5rgzy6xpqgj")))

(define-public crate-cargo-semver-tool-0.1.2 (c (n "cargo-semver-tool") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.11") (d #t) (k 0)))) (h "0r689kz0b318y7wd723yd1y4v91dnnwj4hnqajk566rwi9b7fz55")))

(define-public crate-cargo-semver-tool-0.1.3 (c (n "cargo-semver-tool") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.11") (d #t) (k 0)))) (h "0y2y02zkyq5bpiiawhn4hh8lzphzccarxr17h5y8yvi021sg607f")))

(define-public crate-cargo-semver-tool-0.1.5 (c (n "cargo-semver-tool") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22") (f (quote ("parse" "display"))) (k 0)))) (h "1zqhax15j9w1wm6w2bky3n55nkh28521dm3s6qlsjxl15z4hgllv")))

