(define-module (crates-io ca rg cargo-workspace-version) #:use-module (crates-io))

(define-public crate-cargo-workspace-version-1.0.0 (c (n "cargo-workspace-version") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "0crd9d34iqaijdjvlykmqxssdd3gmaqgx9bkwbqk8z0l9vih2afw")))

