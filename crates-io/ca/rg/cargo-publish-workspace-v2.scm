(define-module (crates-io ca rg cargo-publish-workspace-v2) #:use-module (crates-io))

(define-public crate-cargo-publish-workspace-v2-0.1.0 (c (n "cargo-publish-workspace-v2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1m3r7ldkvzca1gmjyq335xh589ld5bg775y3k5np3dic4c4hnkiq")))

(define-public crate-cargo-publish-workspace-v2-0.2.0 (c (n "cargo-publish-workspace-v2") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0d71px0zh3jxza8nxz0cvpq10f064siswka24f0ycz11b3rhxwzj")))

(define-public crate-cargo-publish-workspace-v2-0.2.1 (c (n "cargo-publish-workspace-v2") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1svhcghlgq92llf1x0c57v7zjmk246rbkg5gin668m0gq12qawzz")))

