(define-module (crates-io ca rg cargo-renamepkg) #:use-module (crates-io))

(define-public crate-cargo-renamepkg-0.1.0 (c (n "cargo-renamepkg") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0sf8c8yg5b88xqgg5x00m7z2gwba8vvg393ghra17fiqdax8f563")))

(define-public crate-cargo-renamepkg-0.1.1 (c (n "cargo-renamepkg") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0qhd5hypyn1c1bmyazd8d2njnhspb0wj45nacvmfa9b7dfdkkv51")))

(define-public crate-cargo-renamepkg-1.0.0 (c (n "cargo-renamepkg") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0pggyk8lvsdk7csncs1n9zblhnshmwp2mliqw9x12jr9b6xnys8g")))

(define-public crate-cargo-renamepkg-1.1.0 (c (n "cargo-renamepkg") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1l8iwi7b552ls94y1fmdh1vdfgr8nqkyqyxggkvg599jfhh464c8")))

