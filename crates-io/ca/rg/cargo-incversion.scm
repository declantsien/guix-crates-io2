(define-module (crates-io ca rg cargo-incversion) #:use-module (crates-io))

(define-public crate-cargo-incversion-0.1.0 (c (n "cargo-incversion") (v "0.1.0") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0a3vqrmr088p64nnk34gly6lc6dpm3kc3dy234fkvxqpygligi99")))

(define-public crate-cargo-incversion-0.1.1 (c (n "cargo-incversion") (v "0.1.1") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0y02555rcyqsp13zfjjg3lwykls38md6j87wvd5zdkrs90yb9cf0")))

(define-public crate-cargo-incversion-0.1.2 (c (n "cargo-incversion") (v "0.1.2") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1svnlk858s4c9zfj0vyxwcc9kgwv9768cr09cfz7vmf830zsykk0")))

