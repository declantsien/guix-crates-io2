(define-module (crates-io ca rg cargo-workflows) #:use-module (crates-io))

(define-public crate-cargo-workflows-0.1.0 (c (n "cargo-workflows") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1s9xzig71bs6m71cw47dqlqxn6svpq9x3p63hs01hjyh1vp8lxvn")))

(define-public crate-cargo-workflows-0.2.0 (c (n "cargo-workflows") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1abqlhcy4qihmw2kzhpwyxpxldgv4ap599wi8rsbfc3ywhpdpmql")))

