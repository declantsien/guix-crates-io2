(define-module (crates-io ca rg cargo-options) #:use-module (crates-io))

(define-public crate-cargo-options-0.1.0 (c (n "cargo-options") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "07fb7x215sy9gqrnrm3n2a4a913mgq7s6pz8p340jsn4fqmkb8lh")))

(define-public crate-cargo-options-0.1.1 (c (n "cargo-options") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "05qz0b9z9ydqx8f2wd6267gaw8yjz94pfmffw8h5j8grk65ld30h")))

(define-public crate-cargo-options-0.1.2 (c (n "cargo-options") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "0kfdbxvagdwd68lm57py9pxbnz467v8grrxf7ggb0ays28vycvam")))

(define-public crate-cargo-options-0.1.3 (c (n "cargo-options") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "0mv5i0hzfz23nlq1bmhfm0mpkwl2wkq910zzxvvr9c1f720k26sv")))

(define-public crate-cargo-options-0.1.4 (c (n "cargo-options") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "1fj5zmd3065mar74h7vqrgwdlrh7xznsajmrvhnl6ns05gqwvnk8")))

(define-public crate-cargo-options-0.2.0 (c (n "cargo-options") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "15c9ydidib42l3lwdx0h6g0n56pn13imq4h126jqbd64fj71bki3") (f (quote (("maturin"))))))

(define-public crate-cargo-options-0.2.1 (c (n "cargo-options") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "1g3x4qh0a6pl43haz6rspfhhm054mqa71bqsrrdq23v8id029jnw")))

(define-public crate-cargo-options-0.3.0 (c (n "cargo-options") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "0rsb0g82njjlx3ynfg7idbabwlaiz2i64kv9xs3zwqvb7vcm1k05")))

(define-public crate-cargo-options-0.3.1 (c (n "cargo-options") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)))) (h "0ks2ikinfwsh8a5fchcsapwz3z9yxxyavhnsj8v586l78ypaxkcq")))

(define-public crate-cargo-options-0.3.2 (c (n "cargo-options") (v "0.3.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "19mb4j5q5bc7k1482rngs8zdl20ychc0gggk4qfa716g0ays6y8i") (y #t)))

(define-public crate-cargo-options-0.4.0 (c (n "cargo-options") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "0gdyvbxpcs8l6cbg7m2pvcfjrdhmb460prsbgsnlcgdxm4rl258y")))

(define-public crate-cargo-options-0.5.0 (c (n "cargo-options") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "1nc8xcdnryz28z03blhjp4y8v54jlncpafd9arpw0y6gg34mpcz0") (y #t)))

(define-public crate-cargo-options-0.5.1 (c (n "cargo-options") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "13n2ggnlai8yxczjbc2dqp5q3svaqsddbzak89fryl15h6q84219") (y #t)))

(define-public crate-cargo-options-0.5.2 (c (n "cargo-options") (v "0.5.2") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "1khspmgz5n6f7rgjxcmwirp8drfwswndh47759p4q8p0wpgvlmb7")))

(define-public crate-cargo-options-0.5.3 (c (n "cargo-options") (v "0.5.3") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "1p0wwzlv6ir83iv0s4yk3g3jzd2ram0c45pz1986ij0v2x1xc346")))

(define-public crate-cargo-options-0.6.0 (c (n "cargo-options") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "0m99dz9vpsplf4s955dvfnfrcvdkm7cifwymriyam11bdfm8v3lv")))

(define-public crate-cargo-options-0.7.0 (c (n "cargo-options") (v "0.7.0") (d (list (d (n "anstyle") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "1ng1nmwqi6dz3nmscg6ppp8hhm2b6ly2yipjkprq7mc0wqa7dc0i") (y #t)))

(define-public crate-cargo-options-0.7.1 (c (n "cargo-options") (v "0.7.1") (d (list (d (n "anstyle") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "0620ajp9qvg4wngfg109h4mf6270lk763gk3slwpq9qahc9n7q28")))

(define-public crate-cargo-options-0.7.2 (c (n "cargo-options") (v "0.7.2") (d (list (d (n "anstyle") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help" "unstable-styles"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "1sknbp83h15da5zmkk9q9vgpw9qzwiz2sizkiv9bkrf8jvwipmya")))

(define-public crate-cargo-options-0.7.3 (c (n "cargo-options") (v "0.7.3") (d (list (d (n "anstyle") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help" "unstable-styles"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "0n3k0bhn5bw962qy90r63la7ybyzp92hxms196sy94clrw1diqax")))

(define-public crate-cargo-options-0.7.4 (c (n "cargo-options") (v "0.7.4") (d (list (d (n "anstyle") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive" "env" "wrap_help" "unstable-styles"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.0") (f (quote ("examples"))) (d #t) (k 2)))) (h "07rj2hkqyan8c2jmvfa35ji6wy0pqs6j7k2a6bmpcym3q13h4m7k")))

