(define-module (crates-io ca rg cargo-leap) #:use-module (crates-io))

(define-public crate-cargo-leap-0.0.0 (c (n "cargo-leap") (v "0.0.0") (h "0bslwa1wx2dwa3fi84d6c067hdirjinyd4mj8xc20h82376jc6wp")))

(define-public crate-cargo-leap-0.1.0 (c (n "cargo-leap") (v "0.1.0") (d (list (d (n "archiver-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.6.0") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zsaj0rzh6r48fp0zxh8xshkfs7siy7a0div8l4whzsqa3yb0np6")))

