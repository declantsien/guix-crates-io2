(define-module (crates-io ca rg cargo-plugin) #:use-module (crates-io))

(define-public crate-cargo-plugin-0.1.0 (c (n "cargo-plugin") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "11vapc38jmg8p6pvm85h7ikppmp9qmy48aqki2rnp6gfsmij1l02")))

