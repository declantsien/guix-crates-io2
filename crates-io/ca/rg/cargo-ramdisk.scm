(define-module (crates-io ca rg cargo-ramdisk) #:use-module (crates-io))

(define-public crate-cargo-ramdisk-0.1.0 (c (n "cargo-ramdisk") (v "0.1.0") (d (list (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^1.3.0") (d #t) (k 0)))) (h "0fa5i7vb7iy6y58xjf10lli00bahfja1ixy77zzk0ygl9wdzzn5v")))

(define-public crate-cargo-ramdisk-0.1.1 (c (n "cargo-ramdisk") (v "0.1.1") (d (list (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^1.3.0") (d #t) (k 0)))) (h "0snzynla5nqs0r73x3f3vn2dglsxppvl0c7sq7gj9ji99g2dp1bj")))

(define-public crate-cargo-ramdisk-0.2.0 (c (n "cargo-ramdisk") (v "0.2.0") (d (list (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^2") (d #t) (k 0)))) (h "06jfsxkpqc0j885sh8l6lh1zg0q2f7jxkyfmmr6hjxns27hjajsc")))

(define-public crate-cargo-ramdisk-0.2.1 (c (n "cargo-ramdisk") (v "0.2.1") (d (list (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^2") (d #t) (k 0)))) (h "06l6ip6dlfl4dbm3fpjfc63mi3k9328kyrplwd9bz2mz48vpmpiv")))

(define-public crate-cargo-ramdisk-0.2.2 (c (n "cargo-ramdisk") (v "0.2.2") (d (list (d (n "carlog") (r "^0.1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-mount") (r "^2") (k 0)))) (h "1hvbivsvfkvlm10sah0hacka6nxk47mv9rl42sghcv43qjayj6br")))

