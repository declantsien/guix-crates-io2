(define-module (crates-io ca rg cargo-buttplug) #:use-module (crates-io))

(define-public crate-cargo-buttplug-0.1.0 (c (n "cargo-buttplug") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "buttplug") (r "^7.1.16") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "0rnnq5k3bbvpv7l4h6bdd6f92h9k8ssnlw8xv06dcr613aiz1qcz")))

