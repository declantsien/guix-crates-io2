(define-module (crates-io ca rg cargo-fixture-macros) #:use-module (crates-io))

(define-public crate-cargo-fixture-macros-0.9.0 (c (n "cargo-fixture-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mg9x88ipsb0wbrkzqwv5n28ir6f2b02hfg7llf3iwm1l3yaqk43")))

(define-public crate-cargo-fixture-macros-1.0.0 (c (n "cargo-fixture-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cmli00d5afinflrv543l4xw04mryz9nwp0rw9l1vn8n5baj4w8s")))

(define-public crate-cargo-fixture-macros-1.1.0 (c (n "cargo-fixture-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09jipfcmg4hh4qq5w3kyw474l7za0lxcazjx2gp51h4f4a8fd6rw")))

(define-public crate-cargo-fixture-macros-1.2.0 (c (n "cargo-fixture-macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0djymgc5851g6rmgmpdzr9v2sj992kp36cqmaq8xrbn81inq0j62")))

(define-public crate-cargo-fixture-macros-1.2.1 (c (n "cargo-fixture-macros") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dd6mgp55ni1wda64844abfywh71qd541rnd6xlpbgdlzykq05y8") (y #t)))

(define-public crate-cargo-fixture-macros-1.2.2 (c (n "cargo-fixture-macros") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yhsf6ajk70q2qzznll2sjvvgf97737bgnzvbxk5p86z3rdk9d8k")))

