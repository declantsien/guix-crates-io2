(define-module (crates-io ca rg cargo-archtest) #:use-module (crates-io))

(define-public crate-cargo-archtest-0.1.3 (c (n "cargo-archtest") (v "0.1.3") (d (list (d (n "arch_test_core") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0g3iik19gfxqhl71cm02q32vv718wniw5gffyr8s2l8jj96iz7ss")))

(define-public crate-cargo-archtest-0.1.4 (c (n "cargo-archtest") (v "0.1.4") (d (list (d (n "arch_test_core") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1cbw59mf8bd10s3h5n503zb5mwnd8lk15x43mpmyvjkhs1pmzfzn")))

(define-public crate-cargo-archtest-0.1.5 (c (n "cargo-archtest") (v "0.1.5") (d (list (d (n "arch_test_core") (r "^0.1.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0d9g6wm7pqdidjk09xmd8464i3kf94f3vg6ql1hn13qsvx3gd5kr")))

(define-public crate-cargo-archtest-0.1.6 (c (n "cargo-archtest") (v "0.1.6") (d (list (d (n "arch_test_core") (r "^0.1.3") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0pd8096ph4m7dm26l18b8ag0ddmicijsmyxxhz9cp7k62l9v5kgx")))

(define-public crate-cargo-archtest-0.1.7 (c (n "cargo-archtest") (v "0.1.7") (d (list (d (n "arch_test_core") (r "^0.1.4") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dhz7akfnss7zqvmq25ydvzwywyk76i1mkdfip3mnlzm6rnw4w99")))

(define-public crate-cargo-archtest-0.1.8 (c (n "cargo-archtest") (v "0.1.8") (d (list (d (n "arch_test_core") (r "^0.1.5") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0yfr2fjgl5bl5c3na4i7wabdrwchbabqs5809b848x0rqh5jrsnb")))

