(define-module (crates-io ca rg cargo-run-wasm) #:use-module (crates-io))

(define-public crate-cargo-run-wasm-0.1.0 (c (n "cargo-run-wasm") (v "0.1.0") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "1d4a1j4vx6b9n14lq7vcxf4n39849a2d6i0ss2jwp92cfy66q7k0")))

(define-public crate-cargo-run-wasm-0.1.1 (c (n "cargo-run-wasm") (v "0.1.1") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "1apxd66k6v1zmrjq57h4a13wjfnckaxdvyml4i3pc81klh2q6ksh")))

(define-public crate-cargo-run-wasm-0.2.0 (c (n "cargo-run-wasm") (v "0.2.0") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "09yg12z0dms8fi9ismxw12b29lc2bi11xbj7rz6vzvl3mlgq26v1")))

(define-public crate-cargo-run-wasm-0.3.0 (c (n "cargo-run-wasm") (v "0.3.0") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "0a0q1b37r16nws9alyfb38s2fzmv15m2y7x4zv0rjdgk109fpc0k")))

(define-public crate-cargo-run-wasm-0.3.1 (c (n "cargo-run-wasm") (v "0.3.1") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "1pshphng3z8sx79609js5ypi9bz4gqca5h48djswgl2s21fya60q")))

(define-public crate-cargo-run-wasm-0.3.2 (c (n "cargo-run-wasm") (v "0.3.2") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "0c32fsgj1sxs7qilzlkndz7knqmqa5gbka629vbhwizg2k7kf7nc")))

(define-public crate-cargo-run-wasm-0.4.0 (c (n "cargo-run-wasm") (v "0.4.0") (d (list (d (n "devserver_lib") (r "^0.4.1") (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.78") (d #t) (k 0)))) (h "0hzd44j0k69h6hsg170yg8b38nw9i2wjjxy63shbs5mizaxk777s")))

