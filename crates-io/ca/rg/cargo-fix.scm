(define-module (crates-io ca rg cargo-fix) #:use-module (crates-io))

(define-public crate-cargo-fix-0.1.0 (c (n "cargo-fix") (v "0.1.0") (h "05pjhr5b18lyb1qyys2c88iz46sjrfy3fp6rxkd1zhggxp5q0nw4")))

(define-public crate-cargo-fix-0.2.0 (c (n "cargo-fix") (v "0.2.0") (d (list (d (n "clap") (r "^2.9.2") (d #t) (k 0)) (d (n "colored") (r "^1.2.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "rustfix") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s0kgqjcsznrl4vc25615xb3z91qja5m44im8373s02yva2w1xin")))

(define-public crate-cargo-fix-0.4.0 (c (n "cargo-fix") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.10") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.5") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfix") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.45") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.45") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 2)))) (h "105w1y0jkfk6ynwih7kn0xadwjz6c8bjsqsbi0aba14mbxgq9k6b")))

(define-public crate-cargo-fix-0.4.1 (c (n "cargo-fix") (v "0.4.1") (d (list (d (n "atty") (r "^0.2.10") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.5") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfix") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.45") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.45") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 2)))) (h "080fiy2qs81bj1vbl1lsrcak2g8ghn0kip73bi79gvx7qfb1va6b")))

