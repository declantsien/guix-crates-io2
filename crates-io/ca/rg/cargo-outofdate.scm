(define-module (crates-io ca rg cargo-outofdate) #:use-module (crates-io))

(define-public crate-cargo-outofdate-0.1.0 (c (n "cargo-outofdate") (v "0.1.0") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.0.5") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0") (d #t) (k 0)))) (h "1xviy5ds21lm14w9g0zqx993i2dp6hrhqsy6j55hh1w5bvnpz88n")))

(define-public crate-cargo-outofdate-0.2.0 (c (n "cargo-outofdate") (v "0.2.0") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.0.5") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0") (d #t) (k 0)))) (h "1cwn67i4bwsmgdgc8mhsh6zlhh5zhzqhrjrsw3yzhh9fka8kf49q")))

(define-public crate-cargo-outofdate-0.2.1 (c (n "cargo-outofdate") (v "0.2.1") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.0.5") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0") (d #t) (k 0)))) (h "0414g1wws8kf4v8v4rchh8xc25divccvizg7lr9g0v8p0xdnbms0")))

