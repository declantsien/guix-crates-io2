(define-module (crates-io ca rg cargo-depdiff) #:use-module (crates-io))

(define-public crate-cargo-depdiff-0.1.0 (c (n "cargo-depdiff") (v "0.1.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "cargo") (r "~0.49") (d #t) (k 0)) (d (n "cargo-lock") (r "~6") (d #t) (k 0)) (d (n "difference") (r "~2") (d #t) (k 0)) (d (n "either") (r "~1") (d #t) (k 0)) (d (n "git2") (r "~0.13") (d #t) (k 0)) (d (n "itertools") (r "~0.9") (d #t) (k 0)) (d (n "semver") (r "~0.10") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0bchsd93z6vf8pjjsj0fgh2r0m3nl9li0m6p3lf50gn5qldhv9ik")))

