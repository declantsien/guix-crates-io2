(define-module (crates-io ca rg cargo-clean) #:use-module (crates-io))

(define-public crate-cargo-clean-0.1.0 (c (n "cargo-clean") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "1rdyf3bd1429nf0rbiapr8y9wma3mbgj4i4l333kkzx00jzb5snk")))

(define-public crate-cargo-clean-0.1.1 (c (n "cargo-clean") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)))) (h "1kawc8jb8wgrhlwdbmm8pjdqlj9bwly7jxpjbf5j9ik2aca0wn0m")))

