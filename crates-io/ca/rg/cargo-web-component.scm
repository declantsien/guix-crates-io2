(define-module (crates-io ca rg cargo-web-component) #:use-module (crates-io))

(define-public crate-cargo-web-component-0.1.0 (c (n "cargo-web-component") (v "0.1.0") (d (list (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0c8g9j313smp33qc722c7ad690rfjw544p66b8fq7rx56g7xcn84")))

(define-public crate-cargo-web-component-0.1.1 (c (n "cargo-web-component") (v "0.1.1") (d (list (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0hbxlx12yr92g849p08c6ppljq41h9iw85k0x640h1mnwpkjlmk5")))

(define-public crate-cargo-web-component-0.1.2 (c (n "cargo-web-component") (v "0.1.2") (d (list (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0l3zjsxp11y3673nqrqccnrx3mq70g4pnirb1rwlj1f3i60s8ssa")))

(define-public crate-cargo-web-component-0.1.3 (c (n "cargo-web-component") (v "0.1.3") (d (list (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1v6i4i1szzn89d53lmf3a3dcvf1k9y87jq54z2skwgcmp14d42cd")))

(define-public crate-cargo-web-component-0.1.4 (c (n "cargo-web-component") (v "0.1.4") (d (list (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0xwybjc8g1zsp8510428w6swpnvqlfwwwfxq99r7ka45wplrp1b2")))

(define-public crate-cargo-web-component-0.1.5 (c (n "cargo-web-component") (v "0.1.5") (d (list (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "11iy8y9sml565zsn2wm2scwyzdbd36dlzaksz2aci2w5q4nldn0y")))

(define-public crate-cargo-web-component-0.1.6 (c (n "cargo-web-component") (v "0.1.6") (d (list (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "19z2h48wm1r6lpjisnip654fadfp7x7r4sppp0wsgcc6g923g11y")))

