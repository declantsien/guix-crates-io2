(define-module (crates-io ca rg cargo-pack) #:use-module (crates-io))

(define-public crate-cargo-pack-0.1.0 (c (n "cargo-pack") (v "0.1.0") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "toml") (r "^0.2.0") (d #t) (k 0)))) (h "0hfkdbbwgknlannc7kqmi8xiimfvln39bb3nqhfhwlqdvi5s28bk")))

(define-public crate-cargo-pack-0.2.0 (c (n "cargo-pack") (v "0.2.0") (d (list (d (n "cargo") (r "^0.24.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.28") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1j6vhk1yapxbxmw2rrimyif4mjilj7knxyp4amgwimhcbbdgshjh")))

(define-public crate-cargo-pack-0.3.0 (c (n "cargo-pack") (v "0.3.0") (d (list (d (n "cargo") (r "^0.26.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.28") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1d0mm1chw54x1ksq66d2lwk8j9qfg6v4sscyc2jb4xswr121fx3q")))

(define-public crate-cargo-pack-0.4.0 (c (n "cargo-pack") (v "0.4.0") (d (list (d (n "cargo") (r "^0.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.28") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "15mfxn96x7aal652k2pwqihn6b9rh22h8dh7m7wkwkmc5rpzhyxh")))

(define-public crate-cargo-pack-0.5.0 (c (n "cargo-pack") (v "0.5.0") (d (list (d (n "cargo") (r "^0.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.28") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1wx1n1w399hs83jkkaxil2z7qbhzn1yxqrc657szycgpy5vnhy8v")))

(define-public crate-cargo-pack-0.6.0 (c (n "cargo-pack") (v "0.6.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1na9r67fz5k2734rjjphwsf5n391li96hsw9032g5p46lw2phkgi")))

