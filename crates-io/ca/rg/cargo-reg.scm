(define-module (crates-io ca rg cargo-reg) #:use-module (crates-io))

(define-public crate-cargo-reg-0.1.0 (c (n "cargo-reg") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.2.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "1ypb6gn2d0fpgzhfw4vbxnc4m02bx7zf069fwciq8wxb6jqhssn6")))

(define-public crate-cargo-reg-0.1.1 (c (n "cargo-reg") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.2.1") (d #t) (k 2)) (d (n "toml_edit") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "0vgpkjhax3bbwgi0k2xrf18nkcnmays15w7gaqb982mcm5lmsg4r")))

