(define-module (crates-io ca rg cargo-new-deps) #:use-module (crates-io))

(define-public crate-cargo-new-deps-0.0.0 (c (n "cargo-new-deps") (v "0.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.0") (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1k292q5154bv39afhcjvxpzxdm45ji4ckq2zkx7mp7hjgj1fz956")))

(define-public crate-cargo-new-deps-0.1.0 (c (n "cargo-new-deps") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.0") (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18432z97irfs1fnav21wgh7lm7qnfxiqzh8z4p9i4h516ppd1wgy")))

(define-public crate-cargo-new-deps-0.2.0 (c (n "cargo-new-deps") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.0") (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0qh6dg76dahgjbhbn51vr19d6830nkvfdmdgwb9jfjsqb4cxzapm")))

