(define-module (crates-io ca rg cargo-user) #:use-module (crates-io))

(define-public crate-cargo-user-0.1.0 (c (n "cargo-user") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)))) (h "07dxk18llnb6m1316bhrgq9xlyj63np7zsd7ixww5p8jx7lz9jsf")))

(define-public crate-cargo-user-0.2.0 (c (n "cargo-user") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)))) (h "0dyg5cav89f5wbc9ka5h850qqvmw92lykmsgcncfn8fvsq4ji2v1")))

(define-public crate-cargo-user-0.2.1 (c (n "cargo-user") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)))) (h "0qr15cpzi7k91m1c74j9p43ba887vh3328pr3ddpzsfw32vlms1f")))

