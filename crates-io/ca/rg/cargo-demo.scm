(define-module (crates-io ca rg cargo-demo) #:use-module (crates-io))

(define-public crate-cargo-demo-0.1.0 (c (n "cargo-demo") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j7nv5fh52jf87kj9dwxl5369gw10babscja241ii48aqz83vp9k")))

