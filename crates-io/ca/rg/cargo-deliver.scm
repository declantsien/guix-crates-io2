(define-module (crates-io ca rg cargo-deliver) #:use-module (crates-io))

(define-public crate-cargo-deliver-0.2.0 (c (n "cargo-deliver") (v "0.2.0") (d (list (d (n "duct") (r "^0.9.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "rprompt") (r "^1.0.3") (d #t) (k 0)) (d (n "tera") (r "^0.10.10") (d #t) (k 0)))) (h "1b5niw0534d6ir0yzqhfr80hpnvv31gl05403npkig2qycy2v8a2")))

