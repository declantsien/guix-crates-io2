(define-module (crates-io ca rg cargo-date) #:use-module (crates-io))

(define-public crate-cargo-date-0.1.0 (c (n "cargo-date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1wd9l9a8sbn3sh32jzvxf1vzv5az9gpmnnc6syrdl9nyp3baq6nk")))

(define-public crate-cargo-date-0.2.0 (c (n "cargo-date") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0r0fnm3hkkz1g0c9byzbxmjpl813445qsydf0hkhnp7v17nw569k")))

