(define-module (crates-io ca rg cargo-create-protocols-plugin) #:use-module (crates-io))

(define-public crate-cargo-create-protocols-plugin-0.0.1 (c (n "cargo-create-protocols-plugin") (v "0.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "194zzz1ak07bn11z86g3y7jkh05w77x4zwl4lyakijan06y7hyw3")))

(define-public crate-cargo-create-protocols-plugin-0.0.2 (c (n "cargo-create-protocols-plugin") (v "0.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1wzj2garfffmzc9nd7g12z8v2k1bbq2lw99r1b0fgr2rv6c1mll3")))

