(define-module (crates-io ca rg cargo-cala) #:use-module (crates-io))

(define-public crate-cargo-cala-0.0.1 (c (n "cargo-cala") (v "0.0.1") (d (list (d (n "actix-files") (r "^0.1") (d #t) (k 0)) (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "inotify") (r "^0.7") (d #t) (k 0)) (d (n "walrus") (r "^0.14") (d #t) (k 0)))) (h "15dijvkk77mmgb6m9yli46kwbd9rsf0gk684ngdzy9dx47pgql99")))

