(define-module (crates-io ca rg cargo-makepad) #:use-module (crates-io))

(define-public crate-cargo-makepad-0.3.0 (c (n "cargo-makepad") (v "0.3.0") (d (list (d (n "makepad-miniz") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-shell") (r "^0.4.0") (d #t) (k 0)))) (h "0jc0fzryd8h6k4k3kan02mxlqgls5vxf1949rxr2a69xk6s4ws1m") (f (quote (("nightly"))))))

(define-public crate-cargo-makepad-0.4.0 (c (n "cargo-makepad") (v "0.4.0") (d (list (d (n "makepad-miniz") (r "^0.4.0") (d #t) (k 0)) (d (n "makepad-shell") (r "^0.4.0") (d #t) (k 0)))) (h "1a6lr2g3qy3p3nlm8fpifygxiw7bb3g5pldgfybk7sh2bkmlw1h6") (f (quote (("nightly"))))))

