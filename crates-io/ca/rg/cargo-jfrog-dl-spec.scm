(define-module (crates-io ca rg cargo-jfrog-dl-spec) #:use-module (crates-io))

(define-public crate-cargo-jfrog-dl-spec-0.1.0 (c (n "cargo-jfrog-dl-spec") (v "0.1.0") (d (list (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1k6v8majrl8cv1n49xs8vz7nph3gzfcrri83dw9zz3k73sbq2zys")))

(define-public crate-cargo-jfrog-dl-spec-1.0.0 (c (n "cargo-jfrog-dl-spec") (v "1.0.0") (d (list (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0j9qqdv2q4qs3a0l1sk28phnk5vfwbxbkv98r0cdy3mrx2zglqrz")))

(define-public crate-cargo-jfrog-dl-spec-1.1.0 (c (n "cargo-jfrog-dl-spec") (v "1.1.0") (d (list (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0wml5ygii4p3w75znc9fn45jr4ykgahfhprgj74jksnijv6zcysv")))

(define-public crate-cargo-jfrog-dl-spec-1.2.0 (c (n "cargo-jfrog-dl-spec") (v "1.2.0") (d (list (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1m9gd938zwx08rmzzjjib12bpm7j0nydc4cn1qs4hnwy41hinabc")))

(define-public crate-cargo-jfrog-dl-spec-1.2.1 (c (n "cargo-jfrog-dl-spec") (v "1.2.1") (d (list (d (n "cargo") (r "^0.59.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1njy8mi47nmy8bjzvkjb0wbx8a2mrd5fxch56ipg2ffdk3z2gp2a")))

