(define-module (crates-io ca rg cargo-r18) #:use-module (crates-io))

(define-public crate-cargo-r18-0.1.0 (c (n "cargo-r18") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "130sd216y1xxrd6qx0jw8zxzf3b3i8fqjqdbqhavrklhca8f7lm5")))

(define-public crate-cargo-r18-0.1.1 (c (n "cargo-r18") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0x20d992ch0l1qs8n1gvdwjyh4lq82wjnyzg9j9mi7ch4zlslgcr")))

(define-public crate-cargo-r18-0.2.0 (c (n "cargo-r18") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "066215i0m9519c488a4l9wbsbcgg2j755926pxmzkjryxcjxi09w")))

(define-public crate-cargo-r18-0.2.1 (c (n "cargo-r18") (v "0.2.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0layiw5lnxr6b27jc54y7kia93w8x7nlcj8f8mv4j15ha8irwd9a")))

(define-public crate-cargo-r18-0.2.2 (c (n "cargo-r18") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.4.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1jwjnr5gv295lsaydi5n5avh2r77fp8aqzrb9m6cx883py8v848h")))

(define-public crate-cargo-r18-0.2.3 (c (n "cargo-r18") (v "0.2.3") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1fzjm2zj8ypzzhrxmha6pgp29m6iphnqn6qzaq3bs8b0qgfz1spr")))

(define-public crate-cargo-r18-0.3.0 (c (n "cargo-r18") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "r18-trans-support") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1fpw1rkk5skbjjfl11qny32lijx8asg8y5isf2va71485rqgxy2x")))

