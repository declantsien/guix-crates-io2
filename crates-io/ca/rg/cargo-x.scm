(define-module (crates-io ca rg cargo-x) #:use-module (crates-io))

(define-public crate-cargo-x-0.1.0 (c (n "cargo-x") (v "0.1.0") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0k00hbsivarvv2rvpck69rdkgg6ll0jaf9xav09mfxnqm9fqzfhi")))

(define-public crate-cargo-x-0.1.1 (c (n "cargo-x") (v "0.1.1") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0zs1y9d1c2lp20vvaba3jbvdjj2h490n6r7dybc6pc3hpm52imrs")))

(define-public crate-cargo-x-0.2.0 (c (n "cargo-x") (v "0.2.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1dwq142pb1c21r0mp32rxa4dvhzxh26v6zzn56jz1ykpz87gx1q2")))

(define-public crate-cargo-x-0.2.1 (c (n "cargo-x") (v "0.2.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0g0phiv2w8mlncgv8p31z994k2h86l18s4kblk6gbf5d0lgzr3mf")))

(define-public crate-cargo-x-0.2.2 (c (n "cargo-x") (v "0.2.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0pb7gzm7m6fms9r57mik6vj66kds8xi9qvwl6rahil34byyfik9i")))

(define-public crate-cargo-x-0.3.0 (c (n "cargo-x") (v "0.3.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1ya6lrlf041bipr0qps7c0za6pgzrcp2jfv87r6ccfkwv7fgk9js")))

(define-public crate-cargo-x-0.3.2 (c (n "cargo-x") (v "0.3.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1afca3aar6v3gk6lgh03p35mpnvjyrm3r70igkzbzqa9sv6fphan")))

(define-public crate-cargo-x-0.3.3 (c (n "cargo-x") (v "0.3.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "00g7jn2344gwam0ay6ydfbm8l6vzs1nbga87w4f8mx1f6sbznxsm")))

(define-public crate-cargo-x-0.3.4 (c (n "cargo-x") (v "0.3.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1qk9abwi1ys9cbbl96vxq5liy68wjsj340c54r0q02qv4g6s9d9w")))

(define-public crate-cargo-x-0.3.5 (c (n "cargo-x") (v "0.3.5") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1sy0qbw6ma8a3b22xdmchgld0xmi795xzz2y86x4s1xg6g6na3vr")))

(define-public crate-cargo-x-0.3.6 (c (n "cargo-x") (v "0.3.6") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0y2qiig7wfix27h1yddbq3lhag9i5h0f5hvqz97sjfaj3rsx2qm6")))

