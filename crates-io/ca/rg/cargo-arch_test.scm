(define-module (crates-io ca rg cargo-arch_test) #:use-module (crates-io))

(define-public crate-cargo-arch_test-0.1.0 (c (n "cargo-arch_test") (v "0.1.0") (d (list (d (n "arch_test_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dw2xpyxzzd76y5r1am439339yngw08xibps9n569xs66b6427r0") (y #t)))

(define-public crate-cargo-arch_test-0.1.1 (c (n "cargo-arch_test") (v "0.1.1") (d (list (d (n "arch_test_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1xr3cl96z4pxiwjccakwcpd9w3shgjm0f8dclxvqxhny5cnk4m84") (y #t)))

(define-public crate-cargo-arch_test-0.1.2 (c (n "cargo-arch_test") (v "0.1.2") (d (list (d (n "arch_test_core") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0k0vam3lwl5j20pk0rm0lp5plfymy38ywj930cid6gh5fncjdvbn") (y #t)))

