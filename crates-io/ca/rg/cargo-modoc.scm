(define-module (crates-io ca rg cargo-modoc) #:use-module (crates-io))

(define-public crate-cargo-modoc-1.0.0 (c (n "cargo-modoc") (v "1.0.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "cratesiover") (r "^2.0") (d #t) (k 0)))) (h "1b22x1dw5ql78xi87iz608rcfvqm7ysy61mm6xy7xjw16s3b9842")))

(define-public crate-cargo-modoc-1.0.1 (c (n "cargo-modoc") (v "1.0.1") (d (list (d (n "cratesiover") (r "^2") (k 0)))) (h "07a7pqwpz9f6dx7z351dwqffq4mpwjqbkqhclny1h2fz9jns1aid")))

(define-public crate-cargo-modoc-1.1.0 (c (n "cargo-modoc") (v "1.1.0") (h "1a7p85qfp3m2ag3jl0l4n8wx0lan303841h5zgzc75xbn5754v22")))

(define-public crate-cargo-modoc-1.2.0 (c (n "cargo-modoc") (v "1.2.0") (h "1gx48n0hszlksnpsm6shcclwb3ni4dhkwp64zm30pbqzlx1hnvhp")))

