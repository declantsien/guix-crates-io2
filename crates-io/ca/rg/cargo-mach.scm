(define-module (crates-io ca rg cargo-mach) #:use-module (crates-io))

(define-public crate-cargo-mach-0.1.0 (c (n "cargo-mach") (v "0.1.0") (h "18rfmq1jxwdxddlgy95rwlnxgl89y46nrsl1ihjxincd8yascg74")))

(define-public crate-cargo-mach-0.1.1 (c (n "cargo-mach") (v "0.1.1") (h "10nc05kgaza71ndqpib84p1i7zb8qkwfyh3h6wsrqsrybmisjls3")))

(define-public crate-cargo-mach-0.1.2 (c (n "cargo-mach") (v "0.1.2") (h "1j7w34rylqinp131chr768q4arkvc786kgd6816hvm3v6rsay3wy")))

