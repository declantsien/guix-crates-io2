(define-module (crates-io ca rg cargo-qaf) #:use-module (crates-io))

(define-public crate-cargo-qaf-0.2.4 (c (n "cargo-qaf") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.1.12") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1bs9bmrgzy2gl5lfm3gxfh64dy2cbg8wx7xrlpizlqam07a4277y")))

