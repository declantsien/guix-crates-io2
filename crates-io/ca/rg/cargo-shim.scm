(define-module (crates-io ca rg cargo-shim) #:use-module (crates-io))

(define-public crate-cargo-shim-0.1.0 (c (n "cargo-shim") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1xra6p5j25lqy5xbvdiqja42wnicyi7iralsph3f03cflzbg91k8")))

(define-public crate-cargo-shim-0.1.1 (c (n "cargo-shim") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0191i0lik4spaxqwckw3l3q98qzzqni42zfmk6lhqs2w44psx5xw")))

(define-public crate-cargo-shim-0.1.2 (c (n "cargo-shim") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1c7bb05rx3sb63dyg6aj7n5ib9pqvf43ak4z0z9b18szgw1lkpjp")))

(define-public crate-cargo-shim-0.1.3 (c (n "cargo-shim") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "002534pgqikydpjsmi3mfwnw3rwkhg2z52dz21zi3qxycw362w1l")))

(define-public crate-cargo-shim-0.1.4 (c (n "cargo-shim") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0xl8b82syhlfcnb4vl5ls482z1hpyz0c2hbn9dhg2srvaqlg5r61")))

(define-public crate-cargo-shim-0.1.5 (c (n "cargo-shim") (v "0.1.5") (d (list (d (n "cargo_metadata") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "111ah45w0k2cyqg7785n3yb1gjzdcldnrzgs6ayvlkh9zgidbx8b")))

(define-public crate-cargo-shim-0.2.0 (c (n "cargo-shim") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17s79gyg8z26wb8h3w440dnj09g8l62swz2rixhbf14yzyyl7p8r")))

