(define-module (crates-io ca rg cargo-cln) #:use-module (crates-io))

(define-public crate-cargo-cln-0.1.0 (c (n "cargo-cln") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "toml") (r "~0.1.30") (d #t) (k 0)))) (h "16yv99p9hjwlmbb0pbxiwq4s102p6xv3qsjchw7pv4kk9gj321v8")))

