(define-module (crates-io ca rg cargo-brew) #:use-module (crates-io))

(define-public crate-cargo-brew-0.1.2 (c (n "cargo-brew") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1cfi8fmgirqhjyqgiz4k3nibwwk3jwqjq2njvpsw266ljr252b00")))

(define-public crate-cargo-brew-0.1.3 (c (n "cargo-brew") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "userror") (r "^0.1") (d #t) (k 0)))) (h "0whx6mvdpijnalz1y9l30hvrbivjmb12clnrzf6h2kh1h9c68gbb")))

(define-public crate-cargo-brew-0.1.4 (c (n "cargo-brew") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "userror") (r "^0.1") (d #t) (k 0)))) (h "0gb4xjalman61b440x9g2j6iyicfh90yqry6h7r92abahc2rna02")))

