(define-module (crates-io ca rg cargo-ctags) #:use-module (crates-io))

(define-public crate-cargo-ctags-0.2.2 (c (n "cargo-ctags") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k3hplhsmlz1aqrd19prczg2kxnn0ls3cp10c9msfgv0sjaaaarq")))

(define-public crate-cargo-ctags-0.2.4 (c (n "cargo-ctags") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11jfjqz1f1ishyl7cqlhdg6xfmz9cy8pshdpblcbxg4pbz681sgz")))

(define-public crate-cargo-ctags-0.2.5 (c (n "cargo-ctags") (v "0.2.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "171rjpgc9r56908mq6cp8bjxvj3vv7rvvh1z06y0h1d6mlyqhskp")))

