(define-module (crates-io ca rg cargo-credential-libsecret) #:use-module (crates-io))

(define-public crate-cargo-credential-libsecret-0.3.1 (c (n "cargo-credential-libsecret") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1r1fahmdf1ihp7mfql443mwsa1byiyksfcm5pdh90rjynir97fzv")))

(define-public crate-cargo-credential-libsecret-0.3.2 (c (n "cargo-credential-libsecret") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "100z39wrv2zydl6yz46bzb7rknik4p2bqvxk73kj1g65dvil7lks") (y #t)))

(define-public crate-cargo-credential-libsecret-0.4.0 (c (n "cargo-credential-libsecret") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "017rlg18v8i20rz5iqd39fc0c4inawigzfwcvgjixd1vs8qknhx1")))

(define-public crate-cargo-credential-libsecret-0.4.1 (c (n "cargo-credential-libsecret") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "02dwjklxslbyp7y0kw4la05wn39l2pyim4vkqxid5kjzhlkzarhy") (r "1.73")))

(define-public crate-cargo-credential-libsecret-0.4.2 (c (n "cargo-credential-libsecret") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "03zhlxds583fg80ss2m1rd3jhrgpsykb36yhv3yg02nx8ixgzc16") (r "1.73")))

(define-public crate-cargo-credential-libsecret-0.4.3 (c (n "cargo-credential-libsecret") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1mbqys6gv4my81zwxcf12mj5xkpvykd4254hfns1npzqwnmy3lv2") (r "1.75.0")))

(define-public crate-cargo-credential-libsecret-0.4.4 (c (n "cargo-credential-libsecret") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1w57r7yk2gy0ix04nhikmg5rzy0krwj3ywmsygi1q7c7422i0wvx") (r "1.76.0")))

