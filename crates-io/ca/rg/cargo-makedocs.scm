(define-module (crates-io ca rg cargo-makedocs) #:use-module (crates-io))

(define-public crate-cargo-makedocs-0.1.1 (c (n "cargo-makedocs") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "013j6z1dvvz6app54bk24222q5dqhkicg56aj2wma7kyfbh71nas")))

(define-public crate-cargo-makedocs-0.1.2 (c (n "cargo-makedocs") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0g6acfhf02zyx2n0m78avnk23xprb68c2a7c6vsnpk2gs22d82m3")))

(define-public crate-cargo-makedocs-0.1.3 (c (n "cargo-makedocs") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "18k7l96cv80r79zxc86kgmdjifj22lp0gbvp4d1qfj025dqbfz6s")))

(define-public crate-cargo-makedocs-0.2.0 (c (n "cargo-makedocs") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0ha72xvyyp6r39182qkg7vwixv6dbjvak97sg37833r9hjmakw21")))

(define-public crate-cargo-makedocs-0.2.1 (c (n "cargo-makedocs") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "1dqzi6q0rsikl9rnzikxqmmfss4m98qn5h95q7xwc7j1gwq82bax")))

(define-public crate-cargo-makedocs-0.2.2 (c (n "cargo-makedocs") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0xfanmyjiqj7ffksjjgsyxrk5gqw9lsakkgmkldsnjp8v66nnlkz")))

(define-public crate-cargo-makedocs-0.3.0 (c (n "cargo-makedocs") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "080dak5bzsaqsf4s8pivmrvm7xmgkicbg58gfyxclzsgvba1af33")))

(define-public crate-cargo-makedocs-0.3.1 (c (n "cargo-makedocs") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0xhd481rc3v92hv3vwmjfklrkf3n0b3ba15s6vwibl9jhadr51ar")))

(define-public crate-cargo-makedocs-0.3.2 (c (n "cargo-makedocs") (v "0.3.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0smyvvd5wvnglf93jisfr0vb7qbfx6s1fr5ydip49axxcl0yfq7g")))

(define-public crate-cargo-makedocs-1.0.0 (c (n "cargo-makedocs") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1779zjy0k0jya8apsl5hb4pfzkfx3rvw0a48ldhmd8dzih5ijqjw")))

(define-public crate-cargo-makedocs-1.1.0 (c (n "cargo-makedocs") (v "1.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1cgb98ica0xh8zn9d2xrscjc0gxrp9fa16379pi7fn7agh2hyzlq")))

(define-public crate-cargo-makedocs-1.2.0 (c (n "cargo-makedocs") (v "1.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0xs4rxfrig5s8zfvk8z2pgq7wm45qh88f170jzqaa2wh8jag9429")))

