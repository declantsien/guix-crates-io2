(define-module (crates-io ca rg cargo-roast) #:use-module (crates-io))

(define-public crate-cargo-roast-0.1.0 (c (n "cargo-roast") (v "0.1.0") (h "01ay2bpwb170s46qy9y92cd41nwbqrip330gv3ss0vwmqab01q5z")))

(define-public crate-cargo-roast-0.2.0 (c (n "cargo-roast") (v "0.2.0") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "native-tls" "gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bawk9d6ibpb77y230d4jbvijb7zbdgb5wjiky883l3nxranyrn8")))

(define-public crate-cargo-roast-0.2.1 (c (n "cargo-roast") (v "0.2.1") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "native-tls" "gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "083j4m8jr4cqnl0f1a0ldhm260lrk1m239blf2dhyz8lfvhnkp21")))

