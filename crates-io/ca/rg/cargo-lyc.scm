(define-module (crates-io ca rg cargo-lyc) #:use-module (crates-io))

(define-public crate-cargo-lyc-1.0.0 (c (n "cargo-lyc") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "cargo") (r "^0.22.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "hmac") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1l5cdqgbycqk2i43cb635n85rqr5k0yyziwkahavni03np3ifc0p")))

(define-public crate-cargo-lyc-1.0.1 (c (n "cargo-lyc") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "cargo") (r "^0.22.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "hmac") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0vv18a5j49xcibfxgl2b48s2rvq15z0grjbrzk90x6icrcjw9nvy")))

(define-public crate-cargo-lyc-1.0.2 (c (n "cargo-lyc") (v "1.0.2") (h "0rbpp4wnr5p3n97qv21cfg9h4sjyyglzli6wjlvda20p82k8cnph")))

