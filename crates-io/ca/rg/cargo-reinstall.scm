(define-module (crates-io ca rg cargo-reinstall) #:use-module (crates-io))

(define-public crate-cargo-reinstall-0.1.0 (c (n "cargo-reinstall") (v "0.1.0") (h "0w80ywa7r3nbpzkdr6fndzjpr91xcs44sncygs9jcv5l97125wfk")))

(define-public crate-cargo-reinstall-0.2.0 (c (n "cargo-reinstall") (v "0.2.0") (h "15v3q30k22z72p7vnwfd8lr1x3jakb5gf2jkv4qip1r0zz996bm6")))

