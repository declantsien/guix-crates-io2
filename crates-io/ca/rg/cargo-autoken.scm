(define-module (crates-io ca rg cargo-autoken) #:use-module (crates-io))

(define-public crate-cargo-autoken-0.1.0 (c (n "cargo-autoken") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "autoken-rustc") (r "=0.1.0") (d #t) (k 1)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.13.0") (f (quote ("cargo_metadata"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.2") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (k 1)))) (h "18g4cspk6zvq04mn8wf6z2vqvhvhrfjimcwy5jp8grncm31icjws")))

