(define-module (crates-io ca rg cargo-vika) #:use-module (crates-io))

(define-public crate-cargo-vika-0.0.0 (c (n "cargo-vika") (v "0.0.0") (d (list (d (n "cargo_metadata") (r "0.14.*") (d #t) (k 0)) (d (n "cfg-if") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "termcolor") (r "1.1.*") (d #t) (k 0)))) (h "1n1jpw61pmhxcfxqibnyd402w6wi5j463lpnlpvl6gjpbb683yga")))

