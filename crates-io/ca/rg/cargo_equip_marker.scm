(define-module (crates-io ca rg cargo_equip_marker) #:use-module (crates-io))

(define-public crate-cargo_equip_marker-0.1.0 (c (n "cargo_equip_marker") (v "0.1.0") (h "04208lbdzvfyfkdg6qscq888wfz4pnsv1x9xpi92cy72l8a5c2rm")))

(define-public crate-cargo_equip_marker-0.1.1 (c (n "cargo_equip_marker") (v "0.1.1") (h "1090mg5mf8k5imhi0gswxq9m2bw3nk36xfd1dfhh0pb1lgg20mi5")))

(define-public crate-cargo_equip_marker-0.1.2 (c (n "cargo_equip_marker") (v "0.1.2") (h "0ybpw7ayhcn23ccy7frarqp1qzqbmn025r9km3dcd8wf8yjj19bf")))

