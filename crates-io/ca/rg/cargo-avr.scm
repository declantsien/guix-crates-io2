(define-module (crates-io ca rg cargo-avr) #:use-module (crates-io))

(define-public crate-cargo-avr-0.1.0 (c (n "cargo-avr") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p9xyibz6saq6dwhmq8di08sfvm2h1h5lh84wp4029sp79393dmz") (y #t)))

