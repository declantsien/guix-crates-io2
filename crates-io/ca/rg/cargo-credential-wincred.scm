(define-module (crates-io ca rg cargo-credential-wincred) #:use-module (crates-io))

(define-public crate-cargo-credential-wincred-0.1.0 (c (n "cargo-credential-wincred") (v "0.1.0") (d (list (d (n "cargo-credential") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("wincred" "winerror" "impl-default"))) (d #t) (k 0)))) (h "02pcqpb1421487ffg2jp814804lq9f8p4lv5lpspvdpjmrhx94ph")))

(define-public crate-cargo-credential-wincred-0.3.0 (c (n "cargo-credential-wincred") (v "0.3.0") (d (list (d (n "cargo-credential") (r "^0.3.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0w8ciyyrq0vp25bdpsmj8221xh09x4np80wrhc53br8gkldljdv6")))

(define-public crate-cargo-credential-wincred-0.3.1 (c (n "cargo-credential-wincred") (v "0.3.1") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dsyhbj2cwpcy35fn27wssg9x4905pjk2ifl4bc07qqd9wqxp8b6") (y #t)))

(define-public crate-cargo-credential-wincred-0.4.0 (c (n "cargo-credential-wincred") (v "0.4.0") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jvc5f9nkh0hcfw8ham9chfxfbx9iab7gwk6hkd2sb0gglcr0kar")))

(define-public crate-cargo-credential-wincred-0.4.1 (c (n "cargo-credential-wincred") (v "0.4.1") (d (list (d (n "cargo-credential") (r "^0.4.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d3h1y0f42xv2sjvn2bmyib4pdimpxjmsi17npx0bd6wlsn568xs") (r "1.73")))

(define-public crate-cargo-credential-wincred-0.4.2 (c (n "cargo-credential-wincred") (v "0.4.2") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nyznrax07w0mj400k3z9v3waf0bm4yvwz1gdsizzgckr1fz879l") (r "1.73")))

(define-public crate-cargo-credential-wincred-0.4.3 (c (n "cargo-credential-wincred") (v "0.4.3") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bb9yczmk3ral2r20v5c4jzf3l9qp0nmm11i20s0w5inprp9b228") (r "1.75.0")))

(define-public crate-cargo-credential-wincred-0.4.4 (c (n "cargo-credential-wincred") (v "0.4.4") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security_Credentials"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wzrxj5y4g96nv9350m3vnj714fpszz93asraimipas88pp5ypm1") (r "1.76.0")))

