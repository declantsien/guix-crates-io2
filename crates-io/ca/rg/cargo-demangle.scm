(define-module (crates-io ca rg cargo-demangle) #:use-module (crates-io))

(define-public crate-cargo-demangle-0.1.0 (c (n "cargo-demangle") (v "0.1.0") (d (list (d (n "clap") (r "^2.22.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.4") (d #t) (k 0)))) (h "0qx5syc2b2m23r6zw4ii7pb3h4pz4lxm5v51wrbaji75zkbrqvi5")))

(define-public crate-cargo-demangle-0.1.1 (c (n "cargo-demangle") (v "0.1.1") (d (list (d (n "clap") (r "^2.22.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.4") (d #t) (k 0)))) (h "1rh0c91jgbq7x923sx9a6yk7lkj0m6zh0rdrkyb12xngvvbdm8x7")))

(define-public crate-cargo-demangle-0.1.2 (c (n "cargo-demangle") (v "0.1.2") (d (list (d (n "clap") (r "^2.22.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.4") (d #t) (k 0)))) (h "13346izpg29d5gfa4780x2cpzhcyxjd4kl386ni4kx0w1hlajfak")))

(define-public crate-cargo-demangle-0.1.3 (c (n "cargo-demangle") (v "0.1.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)))) (h "0qac10y81rrjy32xnvm4jyzn7gi4s64i916qn561nnvfnch216d9")))

