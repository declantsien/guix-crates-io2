(define-module (crates-io ca rg cargo-cntrlr) #:use-module (crates-io))

(define-public crate-cargo-cntrlr-0.1.0 (c (n "cargo-cntrlr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "cargo") (r "^0.49.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cntrlr-build") (r "^0.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "11bd30d0alhydz1s2s6gcxjsxfvj5gh35cbxdricr704vmlfl4bl")))

