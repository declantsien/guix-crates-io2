(define-module (crates-io ca rg cargo-kconfig) #:use-module (crates-io))

(define-public crate-cargo-kconfig-0.0.1 (c (n "cargo-kconfig") (v "0.0.1") (d (list (d (n "kconfig-parser") (r ">=0.0.1") (d #t) (k 0)) (d (n "kconfig-represent") (r ">=0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncurses") (r ">=5.101.0") (f (quote ("wide"))) (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1.9") (d #t) (k 0)))) (h "18kiyc4yws93nqfysm76qlk3bdlhfpkwg9xba9v7yg6qz0k4rirh")))

(define-public crate-cargo-kconfig-0.0.2 (c (n "cargo-kconfig") (v "0.0.2") (d (list (d (n "kconfig-parser") (r ">=0.0.2") (d #t) (k 0)) (d (n "kconfig-represent") (r ">=0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1damvgazqnlmqgzxmap4dny9x1vkc42gfidnwfdy79575h8wvp11")))

(define-public crate-cargo-kconfig-0.0.4 (c (n "cargo-kconfig") (v "0.0.4") (d (list (d (n "kconfig-parser") (r ">=0.0.3") (d #t) (k 0)) (d (n "kconfig-represent") (r ">=0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c9jn1sqa12x3cg4p1065pa786xm3k0wxmphirpkhbj41c3xnhbf")))

(define-public crate-cargo-kconfig-0.1.1 (c (n "cargo-kconfig") (v "0.1.1") (d (list (d (n "kconfig-parser") (r ">=0.1.1") (d #t) (k 0)) (d (n "kconfig-represent") (r ">=0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "108kmrn9mpw82z2z4inf65grjxg7y8xc9dincb6a3rn3bvhzqy49")))

