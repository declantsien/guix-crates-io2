(define-module (crates-io ca rg cargo-sdl-apk) #:use-module (crates-io))

(define-public crate-cargo-sdl-apk-0.1.0 (c (n "cargo-sdl-apk") (v "0.1.0") (d (list (d (n "cargo") (r "^0.70.1") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0800i9cqk6lgxr30p0c3ixqswap9x8m4ry68ss5vqlwdfh9dsi42")))

(define-public crate-cargo-sdl-apk-0.1.1 (c (n "cargo-sdl-apk") (v "0.1.1") (d (list (d (n "cargo") (r "^0.70.1") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0x2g5fgy43s9yzy7r8s7k1lc0j9wy383pdccslfqwci2mb5n4w71")))

