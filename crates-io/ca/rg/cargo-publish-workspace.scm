(define-module (crates-io ca rg cargo-publish-workspace) #:use-module (crates-io))

(define-public crate-cargo-publish-workspace-0.0.0 (c (n "cargo-publish-workspace") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "17b14wkb0lkkh2wnb7d8dv9sm1lshqzqwpncq0gmwrlsy86cpvry")))

(define-public crate-cargo-publish-workspace-0.1.0 (c (n "cargo-publish-workspace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.5.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1yr73kvphn8c766fxryplzhqwwc6z2izf9kqpmja5ilzb4d0kvpp")))

(define-public crate-cargo-publish-workspace-0.16.0 (c (n "cargo-publish-workspace") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.7.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1i60038y76mvhsqwrgla3w582nvb6nq4mkgvzjsbn89zgqigvs1s")))

(define-public crate-cargo-publish-workspace-0.16.1 (c (n "cargo-publish-workspace") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.7.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0jbc506a3mxrflznp6lz3m6lg3v79q3nffr1ibzg1i3ah0f141c4")))

(define-public crate-cargo-publish-workspace-0.17.0 (c (n "cargo-publish-workspace") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.7.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "07garf0wjm7kqmawssd4pmj9sgw8jvbijvyw6w1id65a0j1ghvsr")))

(define-public crate-cargo-publish-workspace-0.18.0 (c (n "cargo-publish-workspace") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.8.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0hbd52h0yvpdf8pndyb6l2z919yxl4p7ykw5gj57dn2gmiz0ccjv")))

(define-public crate-cargo-publish-workspace-0.18.1 (c (n "cargo-publish-workspace") (v "0.18.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "guppy") (r "^0.8.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "104war85zcvrsypkw7wdbpdkxnh1vs0di1vkiqgn2cikv138d5sb")))

(define-public crate-cargo-publish-workspace-0.19.0 (c (n "cargo-publish-workspace") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)) (d (n "guppy") (r "^0.10.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "0qrifqbrawz21wh2lymdwab7169ysdrl30df9m9qzl0w8d1lxbz0")))

(define-public crate-cargo-publish-workspace-0.20.0 (c (n "cargo-publish-workspace") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "guppy") (r "^0.11.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.9.1") (d #t) (k 0)))) (h "18cbmi289gjdipv5lwafnkjg1s6h42yicrljnsigbkd1a34c64c2") (y #t)))

(define-public crate-cargo-publish-workspace-0.21.0 (c (n "cargo-publish-workspace") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "guppy") (r "^0.11.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.9.1") (d #t) (k 0)))) (h "1d02rf9qz65rh2d2dscs5nx2m9s3pz86zhs1gc4il7mkxkacm23g") (y #t)))

