(define-module (crates-io ca rg cargo-depgraph) #:use-module (crates-io))

(define-public crate-cargo-depgraph-0.1.0 (c (n "cargo-depgraph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0h4mdzq8wg3bp4d132011vah0w9qd5pk5ff74azfdc6zvvzjhbnd")))

(define-public crate-cargo-depgraph-1.0.0 (c (n "cargo-depgraph") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "00vp4aj13ll5y791n17lriknhd5z1nzfxlpf6szwv41xr4wmpv52")))

(define-public crate-cargo-depgraph-1.1.0 (c (n "cargo-depgraph") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "01cwar111gxvf7q90rgxgak8cq9a4fz2sibigqzhqqigiif1hz9d")))

(define-public crate-cargo-depgraph-1.1.1 (c (n "cargo-depgraph") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "16br9swsbwjdf5yvcfhdws9w2b9mmh3x9wdizaipvs1apvk87pvb")))

(define-public crate-cargo-depgraph-1.1.2 (c (n "cargo-depgraph") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0ac95x7z3b6gr99lbdp1nymzvdcxjvf3fjgf8w7f6w5ilnaha2lh")))

(define-public crate-cargo-depgraph-1.2.0 (c (n "cargo-depgraph") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "1aqkhb911l2xr3vyi4fpfv3x8m0wrjzayqcvpp3hkm49b27dqbhh")))

(define-public crate-cargo-depgraph-1.2.1 (c (n "cargo-depgraph") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)))) (h "1lyka3nbx9sv8cwhvgkax2x2jl595ni25drlwpml769h0k3n5m82")))

(define-public crate-cargo-depgraph-1.2.2 (c (n "cargo-depgraph") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (f (quote ("stable_graph"))) (k 0)))) (h "0idd418gn2k8ij1wjlx182f8r33r53hpl3gz61mlhfmxrdj7gnqb")))

(define-public crate-cargo-depgraph-1.2.3 (c (n "cargo-depgraph") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "0brz1ggiymvgnzr1ymr1mp2kv542k94j8vbsgx45k90jyzknm2iw")))

(define-public crate-cargo-depgraph-1.2.4 (c (n "cargo-depgraph") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "04vpk42skbrm05wa5439qxdmvmkddbb5wa7yl13pi3qyw0n68xss")))

(define-public crate-cargo-depgraph-1.2.5 (c (n "cargo-depgraph") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "0rrg3pbmza1zj0phh80nn96mkingpbcqdb2362c1dz64pzb6zwj7")))

(define-public crate-cargo-depgraph-1.3.0 (c (n "cargo-depgraph") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "=4.0.0-rc.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "1k5pnisxh6h0r0nn9w51x6i60b84nplv07qdkzka1n277n7sawvx")))

(define-public crate-cargo-depgraph-1.3.1 (c (n "cargo-depgraph") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "177kc88ypwh3si8r282vxgynr3m26sf64krlzbz8pz9d32r5glyv")))

(define-public crate-cargo-depgraph-1.4.0 (c (n "cargo-depgraph") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "1mj7skrrpih6crbqr7m9xaj5b1hj81wvn8p9474mqy7msna6i7bb")))

(define-public crate-cargo-depgraph-1.5.0 (c (n "cargo-depgraph") (v "1.5.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "17qv75nhv3dh9rsdzkhda3pgyxwy7mr1nb55zhy0796kdy7p6969")))

(define-public crate-cargo-depgraph-1.6.0 (c (n "cargo-depgraph") (v "1.6.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (f (quote ("stable_graph"))) (k 0)))) (h "1dv53gk6jhwivcv24gi3bv0dvdq7rj80l8n39pvkbjc85llraa5n")))

