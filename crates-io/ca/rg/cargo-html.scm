(define-module (crates-io ca rg cargo-html) #:use-module (crates-io))

(define-public crate-cargo-html-0.1.0 (c (n "cargo-html") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.14") (f (quote ("serde" "toml"))) (d #t) (k 0)))) (h "0j7cs8g7k4n4vzh26rdkxw2wxcpwhwcvvhdic8h5y1wlkf5ksm24")))

(define-public crate-cargo-html-0.1.1 (c (n "cargo-html") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.14") (f (quote ("serde" "toml"))) (d #t) (k 0)))) (h "0zbaw6gah7mvnvh5zq774aac38yv6nacn1df1pblrrgv09n3pjnn")))

(define-public crate-cargo-html-0.1.2 (c (n "cargo-html") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.15") (f (quote ("serde" "toml"))) (d #t) (k 0)))) (h "0myf1r202q0160wsn2l4gqnp75b72zyv62q1qhcf5nkm398hvjfw")))

(define-public crate-cargo-html-0.1.3 (c (n "cargo-html") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.15") (f (quote ("serde" "toml"))) (d #t) (k 0)))) (h "0y95pwy0w3a7swqq0hizhw6jfrbjrwcf4mnaq1y38f4fbxj7l9h9")))

(define-public crate-cargo-html-0.1.4 (c (n "cargo-html") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.15") (f (quote ("serde" "toml"))) (d #t) (k 0)) (d (n "wasm-pack") (r "^0.9.1") (d #t) (k 0)))) (h "1mhryvbg4wk05rncs74ixk3nyx40d9r0dl0lgdp3klnwrj4fa9is")))

(define-public crate-cargo-html-0.1.5 (c (n "cargo-html") (v "0.1.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "binary-install") (r "^0.0.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.16") (f (quote ("serde" "toml" "version"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p605jk12d8zx6xbavdb4wg64vk5rry6mc0y0pi0sny1n1v6fy43")))

(define-public crate-cargo-html-0.2.0 (c (n "cargo-html") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "binary-install") (r "^0.0.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.17") (f (quote ("serde" "toml" "version"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ya1dshs0wmx8gr33hz062qs78izkqhc5qlcbasv0ygvypa9cksl")))

(define-public crate-cargo-html-0.2.1 (c (n "cargo-html") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "binary-install") (r "^0.0.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mmrbi") (r "^0.0.17") (f (quote ("serde" "toml" "version"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02kacvnl82ksblvpnz151yx698qzvjklzs4mnkgqc77b4cc8f1v0")))

