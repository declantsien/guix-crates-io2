(define-module (crates-io ca rg cargo-ohrs) #:use-module (crates-io))

(define-public crate-cargo-ohrs-0.0.1 (c (n "cargo-ohrs") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p93413mx8a9d0j499h5wrgjjgqn3xiqpmgkclr98xgsyjnx2fam")))

(define-public crate-cargo-ohrs-0.0.2 (c (n "cargo-ohrs") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "048kv91iwpbc9xyfi2n8nl2266jrf4wpzn7zhn8cpxhlxj10rbsw")))

(define-public crate-cargo-ohrs-0.0.3 (c (n "cargo-ohrs") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cxkg9zik5dpf4aapzkvp1dks84gpk6zx9x28fkxwk904vmbl5l3")))

(define-public crate-cargo-ohrs-0.0.4 (c (n "cargo-ohrs") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0brlswixnrknahi2x2vi0ankkpp09k4f8v48kx62nl6ricgwjm7y")))

(define-public crate-cargo-ohrs-0.0.5 (c (n "cargo-ohrs") (v "0.0.5") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g66rnsjg3ljq8dibjvbppirmi11db7pd10ad14n3yq4n0rj3gzj")))

