(define-module (crates-io ca rg cargo-serve-doc) #:use-module (crates-io))

(define-public crate-cargo-serve-doc-0.1.0 (c (n "cargo-serve-doc") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "cargo") (r "^0.35") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)) (d (n "replacing-buf-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)) (d (n "tower-web") (r "^0.3.7") (d #t) (k 0)))) (h "1c4rg4p24r9jpvlifclapy4b1y5mpqc74pp0k04b1h28xba4isyz")))

