(define-module (crates-io ca rg cargo-upstall) #:use-module (crates-io))

(define-public crate-cargo-upstall-0.1.0 (c (n "cargo-upstall") (v "0.1.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "197zwsnz0yn9wd4ljkgyppqgfsvdrmsds601gk9f07983g5mlz6f")))

(define-public crate-cargo-upstall-0.1.1 (c (n "cargo-upstall") (v "0.1.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1398zdzl2ipfcsr5f26vl5k4yg1xafr1ckrcdn665alq6k91b1jc")))

(define-public crate-cargo-upstall-0.1.2 (c (n "cargo-upstall") (v "0.1.2") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1sw6zpfk2jl1dqg7ij3kx90lp5a5yzybcv97x00am21jp21ra6vl")))

(define-public crate-cargo-upstall-0.1.3 (c (n "cargo-upstall") (v "0.1.3") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1r099ahjz9nq662jiq82n8g94glgj7prbq9fqz0cr8a3zbv6f0nw")))

(define-public crate-cargo-upstall-0.1.4 (c (n "cargo-upstall") (v "0.1.4") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0f9skbwiyakacyj7fc3pf7crkjh8lahnx72mj43g8d026a1zlc12")))

