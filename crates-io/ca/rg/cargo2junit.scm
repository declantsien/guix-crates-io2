(define-module (crates-io ca rg cargo2junit) #:use-module (crates-io))

(define-public crate-cargo2junit-0.1.0 (c (n "cargo2junit") (v "0.1.0") (d (list (d (n "junit-report") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "099pwazkk2f1jq2kialqs7frsnnmi8sg277md2f00ss4m76z936i")))

(define-public crate-cargo2junit-0.1.1 (c (n "cargo2junit") (v "0.1.1") (d (list (d (n "junit-report") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1fvd0s5cbjhbxk39wjn221789mf898fry06d23bj7aqkffb9w6v1")))

(define-public crate-cargo2junit-0.1.2 (c (n "cargo2junit") (v "0.1.2") (d (list (d (n "junit-report") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1gzz58skx928jpzkywmgpbj7k61cixna2c4gy7vpg21i8039i6mp")))

(define-public crate-cargo2junit-0.1.3 (c (n "cargo2junit") (v "0.1.3") (d (list (d (n "junit-report") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "03lry6ci0bjfg000gc0452gd55m7la0xdg01w232zpvf3m4zjc7w")))

(define-public crate-cargo2junit-0.1.4 (c (n "cargo2junit") (v "0.1.4") (d (list (d (n "junit-report") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "104jm3zj5dz2bsd37n33dpvfj7m421za172bnlc8cf723fcjdh48")))

(define-public crate-cargo2junit-0.1.5 (c (n "cargo2junit") (v "0.1.5") (d (list (d (n "junit-report") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1ragiv1l4p0g4xrq3rrp0c733qnvzjbz8q10ihphdc5pr9zd5pcl")))

(define-public crate-cargo2junit-0.1.6 (c (n "cargo2junit") (v "0.1.6") (d (list (d (n "junit-report") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "076ccai34219p41spdpq247r1w4hvrqbawq122i89ihfq3aidq2v")))

(define-public crate-cargo2junit-0.1.7 (c (n "cargo2junit") (v "0.1.7") (d (list (d (n "junit-report") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1np9irzilzpkj81wc6biwcmdwhrcxb5wxv606q59p70ah9vs9jm8")))

(define-public crate-cargo2junit-0.1.8 (c (n "cargo2junit") (v "0.1.8") (d (list (d (n "junit-report") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "1ymwahgm1gcc52k6z08njkhqxhh5y1vsxv29i0rd25ilxm8vm25c")))

(define-public crate-cargo2junit-0.1.9 (c (n "cargo2junit") (v "0.1.9") (d (list (d (n "junit-report") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "06nvqbvfwy7dj92hp4gx24lvx6c1am6p1mk6lcvhnnsxk4dii0n8")))

(define-public crate-cargo2junit-0.1.10 (c (n "cargo2junit") (v "0.1.10") (d (list (d (n "junit-report") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "1xjg436w27yr78w0x6pag3v6mczx633j05vff66w1pcik3gm5qxm")))

(define-public crate-cargo2junit-0.1.11 (c (n "cargo2junit") (v "0.1.11") (d (list (d (n "junit-report") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "092yw11cqzk193af8hmhaig7igpada2cndllxvkcbfhjqvz7hj6h")))

(define-public crate-cargo2junit-0.1.12 (c (n "cargo2junit") (v "0.1.12") (d (list (d (n "junit-report") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)))) (h "0a213j418sxq0j8v8s36r6hl22vi5vd3bs8nhydnnwwpfmazbxa8")))

(define-public crate-cargo2junit-0.1.13 (c (n "cargo2junit") (v "0.1.13") (d (list (d (n "junit-report") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1rczcc20751shcd4yn2lj4girwrpk45av28j8848jpqx0xsy296x")))

