(define-module (crates-io ca rg cargo-strip) #:use-module (crates-io))

(define-public crate-cargo-strip-0.2.0 (c (n "cargo-strip") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)))) (h "0mc69jjgrxjla17pv4v2h1qcv449ps3p9y0rqmvx56n32822d0c8")))

(define-public crate-cargo-strip-0.2.1 (c (n "cargo-strip") (v "0.2.1") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)))) (h "0hfr7v59rl3wq1165iip8xn42149r3z876g3v6q5ccih26nny9fs")))

(define-public crate-cargo-strip-0.2.2 (c (n "cargo-strip") (v "0.2.2") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "007n1fczgs9q0m3ljn4fv3zg0kv9105w1wy5an5v2bwxkimlvkfy")))

(define-public crate-cargo-strip-0.2.3 (c (n "cargo-strip") (v "0.2.3") (d (list (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0vf0akkir48dj3h6z7lfrb0xnqin2n6ra4j6dcmvni9xrxv25m3q")))

