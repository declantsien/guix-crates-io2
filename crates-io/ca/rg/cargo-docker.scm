(define-module (crates-io ca rg cargo-docker) #:use-module (crates-io))

(define-public crate-cargo-docker-0.1.1 (c (n "cargo-docker") (v "0.1.1") (d (list (d (n "clap") (r "^2.13.0") (d #t) (k 0)))) (h "1dhhhdska55w726jcmwyws1hnsjcx2nqj7898pjdia96m15p3l3k")))

(define-public crate-cargo-docker-0.2.0 (c (n "cargo-docker") (v "0.2.0") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0g7zxq77h6qs1a0fmrc37s68i725vprfzddcmnin8riq953qr1ra")))

