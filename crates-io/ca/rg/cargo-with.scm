(define-module (crates-io ca rg cargo-with) #:use-module (crates-io))

(define-public crate-cargo-with-0.1.0 (c (n "cargo-with") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1isvis1an7nwqyy5yidsn8r7g7vx0qaygqfibkns71h2i8vpxcw6")))

(define-public crate-cargo-with-0.2.0 (c (n "cargo-with") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01x45mp53qx6nyg79s58h84648vrsa1j86mxpky812l4g8v9rx80")))

(define-public crate-cargo-with-0.3.0 (c (n "cargo-with") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k5v47wiwrbib4g1n0g628fgm1hccbm38659gg6jmhgn5jwcj7pf")))

(define-public crate-cargo-with-0.3.1 (c (n "cargo-with") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0a5w464cbs40lplyln26sxx8dc08w8f3w4652v2hy8xxc6siac62") (y #t)))

(define-public crate-cargo-with-0.3.2 (c (n "cargo-with") (v "0.3.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0fw6v5wymjdhvpxbbssc35g8zz9dk96nd3pr0cq57qd8iij5hamp")))

