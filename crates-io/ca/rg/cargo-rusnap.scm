(define-module (crates-io ca rg cargo-rusnap) #:use-module (crates-io))

(define-public crate-cargo-rusnap-0.1.0 (c (n "cargo-rusnap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.2") (d #t) (k 0)))) (h "1qpgdipvlxpfwiq0cszg6if3s5pypfh95zjbz554a558mnnh93qg")))

(define-public crate-cargo-rusnap-0.1.1 (c (n "cargo-rusnap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.2") (d #t) (k 0)))) (h "0w45xm2hsf2k84fk7md2fvl9wn82gfa91ingk8zx6cpnnz73s6ml")))

