(define-module (crates-io ca rg cargo-miri) #:use-module (crates-io))

(define-public crate-cargo-miri-0.0.0 (c (n "cargo-miri") (v "0.0.0") (h "1mjxhw9vd2wyl1bhm4z5gbyf23r4xbhgx2habkbn2y085ni71ri4")))

(define-public crate-cargo-miri-0.0.1 (c (n "cargo-miri") (v "0.0.1") (h "0ghw22rqxnmdfryn1897p5c6fc5sj2zjfb47y669yzh3m25y2m91")))

