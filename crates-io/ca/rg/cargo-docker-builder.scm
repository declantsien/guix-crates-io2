(define-module (crates-io ca rg cargo-docker-builder) #:use-module (crates-io))

(define-public crate-cargo-docker-builder-0.0.2 (c (n "cargo-docker-builder") (v "0.0.2") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "0lh8c6xjvaz4khl8mcraqsn54m5vl2ah3av4cb7jqz39ym2hf7mv") (y #t)))

(define-public crate-cargo-docker-builder-0.0.4 (c (n "cargo-docker-builder") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "01jb0xwris03v64wlabg1bgicwnnfvfkcnkr2r3r5df89hh6riha") (y #t)))

(define-public crate-cargo-docker-builder-0.0.5 (c (n "cargo-docker-builder") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1fbzlllqvykhzq6pc4s23z2a254ry4cbdsx96cn8dgxbzjkprr71") (y #t)))

