(define-module (crates-io ca rg cargo-wipe) #:use-module (crates-io))

(define-public crate-cargo-wipe-0.1.0 (c (n "cargo-wipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0q2gwln52427ymcq8fv9gm1jlscvgmfnp90zyrb9kasxbhraapdq")))

(define-public crate-cargo-wipe-0.1.1 (c (n "cargo-wipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "19225jzbd6hq6lvv6qxbjcbnkwczjay2z1c1d6a0rl277039ayk7")))

(define-public crate-cargo-wipe-0.1.2 (c (n "cargo-wipe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0zif8d8116d5xc3485b97cw94ndp9jkn5s8jvqmpbqj4hl7cgxyl")))

(define-public crate-cargo-wipe-0.1.3 (c (n "cargo-wipe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1zyq31x60qc5r6v6lplkpb03qvpssjj7rh4sp85fzvi1qyg21354")))

(define-public crate-cargo-wipe-0.2.0 (c (n "cargo-wipe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "05h68h6sffw333i6xf8s549d8gm7f7q2w2hz9whb5g8aa77lprn4")))

(define-public crate-cargo-wipe-0.3.0 (c (n "cargo-wipe") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "12hjvi7c42wwz3l0ph96xg3is95ia6434iyaafi63ck8axmk18zr")))

(define-public crate-cargo-wipe-0.3.1 (c (n "cargo-wipe") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0v90lzbc672kylcz5zzbgq0ysr338lax59cghs36h6hn61y3blr9")))

(define-public crate-cargo-wipe-0.3.2 (c (n "cargo-wipe") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4") (d #t) (k 0)) (d (n "parameterized") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "10l3w39kb4pvcdmlaspi27d6vchg2rjixxqs80g784a4v94860r5")))

(define-public crate-cargo-wipe-0.3.3 (c (n "cargo-wipe") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4") (d #t) (k 0)) (d (n "parameterized") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "03i5f72qr0dfpgb6ljjrs6l06fvxp19xfx11zra1v2kxis0mlzx9")))

