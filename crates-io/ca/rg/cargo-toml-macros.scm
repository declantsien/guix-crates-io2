(define-module (crates-io ca rg cargo-toml-macros) #:use-module (crates-io))

(define-public crate-cargo-toml-macros-1.0.0 (c (n "cargo-toml-macros") (v "1.0.0") (h "1ghwszg93lhkkdiykvjskspq58w8r3n4kpzm9v6b8jpwk6l2kpff")))

(define-public crate-cargo-toml-macros-1.0.1 (c (n "cargo-toml-macros") (v "1.0.1") (h "159nnjpn2kk6bd1zvw9rhl2qkrd0wv1prbqb80nhql4icnsds21n")))

