(define-module (crates-io ca rg cargo-glab) #:use-module (crates-io))

(define-public crate-cargo-glab-0.1.0-rc.2 (c (n "cargo-glab") (v "0.1.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "gitlab") (r "^0.1605.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "115ykaxxqcvgppi9l3blmp6jvh87xp1ffal0nr9gjj2hdsax62h8")))

