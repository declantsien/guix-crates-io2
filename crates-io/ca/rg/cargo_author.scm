(define-module (crates-io ca rg cargo_author) #:use-module (crates-io))

(define-public crate-cargo_author-1.0.0 (c (n "cargo_author") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "1p4180r9a04grdww1xl8pbx5dhs5bkppmgjqj1xc16304pyhaamv")))

(define-public crate-cargo_author-1.0.1 (c (n "cargo_author") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "1jf0mlbwhbf2jpz27j66q08ws3b3vf4ibqx44cj4k6dki3rvgxqm")))

(define-public crate-cargo_author-1.0.2 (c (n "cargo_author") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.7") (d #t) (k 0)))) (h "1245j0wdnzf34i7jvkv22r76cpjfjwd1xfghajzlshikcx625c5s")))

(define-public crate-cargo_author-1.0.3 (c (n "cargo_author") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (f (quote ("std" "unicode-gencat"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.16") (d #t) (k 0)))) (h "07nq4rsw7np659hi43p712xagabfzwch1md75iysbjaqa46v2kz2") (y #t)))

(define-public crate-cargo_author-1.0.4 (c (n "cargo_author") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (f (quote ("std" "unicode-gencat" "unicode-perl"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.16") (d #t) (k 0)))) (h "1s7ryvkhls9zkz6zqlp36rcnhmcdslb7xsbq5ccv3vlyd424al6y")))

(define-public crate-cargo_author-1.0.6 (c (n "cargo_author") (v "1.0.6") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (f (quote ("std" "unicode-gencat" "unicode-perl"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "1zlyrmrl9k7jzyrw0498k2qy031q2fd11xifzlzblhh3c65xix0z")))

(define-public crate-cargo_author-1.0.7 (c (n "cargo_author") (v "1.0.7") (d (list (d (n "regex") (r "^1.6.0") (f (quote ("std" "unicode-gencat" "unicode-perl"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "012xh17ljciqrggq1a71giiqf5bnp4534dxfr81n51hy73j75npm") (r "1.70")))

