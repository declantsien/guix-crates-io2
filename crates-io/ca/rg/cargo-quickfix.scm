(define-module (crates-io ca rg cargo-quickfix) #:use-module (crates-io))

(define-public crate-cargo-quickfix-0.0.0 (c (n "cargo-quickfix") (v "0.0.0") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0y4x424ni5y62qy57z3m72fircpy3ni2mqa2iia1x3rbgi8j0bgh")))

