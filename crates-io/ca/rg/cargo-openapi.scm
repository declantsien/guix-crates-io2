(define-module (crates-io ca rg cargo-openapi) #:use-module (crates-io))

(define-public crate-cargo-openapi-0.2.0 (c (n "cargo-openapi") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "sw4rm-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "sw4rm-rs-generation") (r "^0.2.0") (d #t) (k 0)))) (h "1vibdnnsafr0kciyrx4j8i8hyyxsiwwmz871r5kah88pyyd3b5nb")))

