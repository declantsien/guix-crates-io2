(define-module (crates-io ca rg cargo-duplicates) #:use-module (crates-io))

(define-public crate-cargo-duplicates-0.1.0 (c (n "cargo-duplicates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0xkcmnj5w1svkasjngsv7xkxchbnhd24fksc08l3qlmdb7r9jzpm")))

(define-public crate-cargo-duplicates-0.2.0 (c (n "cargo-duplicates") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "09yli3gb8q2y8iv6pl56m6prmgil27491n4qpzm42pr1syw8pk7d")))

(define-public crate-cargo-duplicates-0.2.1 (c (n "cargo-duplicates") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0dijdccjxn0zw74635a3pqjn5h3w9ghzybzlcw3xq18wi6a6ghwm")))

(define-public crate-cargo-duplicates-0.3.0 (c (n "cargo-duplicates") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "1dcfvc5i3vng4j9j4jdfhll0rd59d5cb6q3fssmwdsv4a9ablqlp")))

(define-public crate-cargo-duplicates-0.3.1 (c (n "cargo-duplicates") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.49") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "16745vcskw52y4m0g19dg9a67j4rn88gf37jpawpqb5gqkk4y2wy")))

(define-public crate-cargo-duplicates-0.3.2 (c (n "cargo-duplicates") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.50") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "1hhf8crfccj90y5byscrihr3ijrfn4xs9c93spprzp6iwljw0b7s")))

(define-public crate-cargo-duplicates-0.4.0 (c (n "cargo-duplicates") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.57") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "1z5kd4lkvi2wfy1kdf9jj689blqh94f6gcdj077hw3c36nsmp9qs")))

(define-public crate-cargo-duplicates-0.5.0 (c (n "cargo-duplicates") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.60") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "047abjl40fmbiy694wvabmfj1idv73ckpf0n2yqz0b77lz92wnai") (r "1.56.0")))

(define-public crate-cargo-duplicates-0.5.1 (c (n "cargo-duplicates") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.61") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0rpmci6aa2808fr58ci4z7rmnk7rcy6rd4kaqahvrw3ffprp1337") (r "1.56.0")))

(define-public crate-cargo-duplicates-0.6.0 (c (n "cargo-duplicates") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.79") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0smihf6h4bq08kgjvyr38427y13n8zgx124hpzlg693qb6vc9hf3") (r "1.58.0")))

