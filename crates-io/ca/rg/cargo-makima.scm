(define-module (crates-io ca rg cargo-makima) #:use-module (crates-io))

(define-public crate-cargo-makima-0.1.0 (c (n "cargo-makima") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1baqjnqy2ylld94a660kmii0gjw9dz3vfzjm6ybw79hg3k625qgl")))

(define-public crate-cargo-makima-0.1.1 (c (n "cargo-makima") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1zx2ihrs2l879679bas6q7cn16xacixrhp9pyrjaqk1fvali4af7")))

