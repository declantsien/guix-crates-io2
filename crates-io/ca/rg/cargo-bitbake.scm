(define-module (crates-io ca rg cargo-bitbake) #:use-module (crates-io))

(define-public crate-cargo-bitbake-0.1.0 (c (n "cargo-bitbake") (v "0.1.0") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustache") (r "~0.0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mpk6bxvf4sfg8w2z9b665mv8p9klcpdjw0s1a9l8w6qrd4n9w4f")))

(define-public crate-cargo-bitbake-0.2.0 (c (n "cargo-bitbake") (v "0.2.0") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustache") (r "~0.0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dy0x6mngmb2m7dfzdnxrkzfnrrl8iyhd1gb0jfzf5hf532yqs2z")))

(define-public crate-cargo-bitbake-0.2.1 (c (n "cargo-bitbake") (v "0.2.1") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qfp21wfc8vrwri7hdjyyizcriv4h5ck4ay2rsbkjsadazsybic5")))

(define-public crate-cargo-bitbake-0.2.2 (c (n "cargo-bitbake") (v "0.2.2") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0q47i509qrpyzqkx49sdbhqjg4wsjmifpsvzb9rxfb7wy9z73fb4")))

(define-public crate-cargo-bitbake-0.3.0 (c (n "cargo-bitbake") (v "0.3.0") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01ab7w83dq3vr87vl2bbg38nsvmvfrhi47lwv3a7vm3y4440sll1")))

(define-public crate-cargo-bitbake-0.3.1 (c (n "cargo-bitbake") (v "0.3.1") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13h9iv85y0bqshzv9pz578kgqzxv0k16yl44zqm11l3w3yprpgad")))

(define-public crate-cargo-bitbake-0.3.2 (c (n "cargo-bitbake") (v "0.3.2") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jf2yb6m8y8dsdg6cylmflghprahd5ywp1k4bgi6g214mfii48s7")))

(define-public crate-cargo-bitbake-0.3.3 (c (n "cargo-bitbake") (v "0.3.3") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "114xyvnvzsl259k18w9gzg1kyvhw1my8ci2s1nwm8rcjzh29gcfc")))

(define-public crate-cargo-bitbake-0.3.4 (c (n "cargo-bitbake") (v "0.3.4") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "10grxbazif5ijay9fvh6r3j584zf4s0q8hzxsja42grs3nf80q2r")))

(define-public crate-cargo-bitbake-0.3.5 (c (n "cargo-bitbake") (v "0.3.5") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1nksqwgk0z39i10d8as3sqpj67wh41fz7wcssq2f4yw56fz7kv1k")))

(define-public crate-cargo-bitbake-0.3.6 (c (n "cargo-bitbake") (v "0.3.6") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qzap8awmm2lqmcl59lnvzrg7zc6mhpiy1xkiz8fbl1x5bn45pbi")))

(define-public crate-cargo-bitbake-0.3.7 (c (n "cargo-bitbake") (v "0.3.7") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jzy4w6fd3xfzamxdvf66cmsxwy6nnnxqhwasd6ym4i7fd946g7r")))

(define-public crate-cargo-bitbake-0.3.8 (c (n "cargo-bitbake") (v "0.3.8") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ij7cxw4anbjiv8vvvq3qxkdmdm2mplskr5zp0jqrqfi2h1cq3ri")))

(define-public crate-cargo-bitbake-0.3.9 (c (n "cargo-bitbake") (v "0.3.9") (d (list (d (n "cargo") (r "^0.21") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "15bx09pzl5vs18ra3plrydi9g5wv7fvcxyigxwn8n68bap1x30s3")))

(define-public crate-cargo-bitbake-0.3.10 (c (n "cargo-bitbake") (v "0.3.10") (d (list (d (n "cargo") (r "^0.24") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "12cj4qswbmynbiwwrwbgqxr7xsig7mqvz1cr7fl0saicx2xvcw7w")))

(define-public crate-cargo-bitbake-0.3.11 (c (n "cargo-bitbake") (v "0.3.11") (d (list (d (n "cargo") (r "^0.24") (d #t) (k 0)) (d (n "git2") (r "^0.6.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1dgjv1slchmfpp7925548fk22cb7wy9amx85xrvab7p4kjvmgjrp")))

(define-public crate-cargo-bitbake-0.3.12 (c (n "cargo-bitbake") (v "0.3.12") (d (list (d (n "cargo") (r "^0.37") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ivlz2p18wgd8ivixfshpim055dj3636rhs338b09jwj397spy3q")))

(define-public crate-cargo-bitbake-0.3.13 (c (n "cargo-bitbake") (v "0.3.13") (d (list (d (n "cargo") (r "^0.41") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rivl67vixys5n51dabmam8kx2fzj18b7jxvcaag25gknmhi8vqa")))

(define-public crate-cargo-bitbake-0.3.14 (c (n "cargo-bitbake") (v "0.3.14") (d (list (d (n "cargo") (r "^0.41") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bgj4l0nh9w9wa3nvnbrgyawjmsi286yxhjplki9as0iblacvfmm")))

(define-public crate-cargo-bitbake-0.3.15 (c (n "cargo-bitbake") (v "0.3.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.49") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01kk6lf4bgc6kvnzbdx65xybhvbyrrdhygba2rpwk1yk9zgdxd3c")))

(define-public crate-cargo-bitbake-0.3.16 (c (n "cargo-bitbake") (v "0.3.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.61") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rmbvhpghkirjn5flylk43dknq8f4419j5zq2zfjir67h3gq9vaw")))

