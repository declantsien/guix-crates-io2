(define-module (crates-io ca rg cargo-loc) #:use-module (crates-io))

(define-public crate-cargo-loc-0.1.0 (c (n "cargo-loc") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "19l0hrbfc0vihxxbhh5lk1jnw0nyikg7zyhkz44czlsbbaaldlww")))

(define-public crate-cargo-loc-0.1.1 (c (n "cargo-loc") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1f0yasj5hmr8bmi7rin6hac1vzb981pk6ymjzzyar8h6kb0nlf5k")))

(define-public crate-cargo-loc-0.1.2 (c (n "cargo-loc") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "17l3rp44pn0wfa3q010qqd4lnsm1ayhqviaypj1vhnkqnaswiq8w")))

