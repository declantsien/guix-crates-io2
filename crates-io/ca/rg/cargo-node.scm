(define-module (crates-io ca rg cargo-node) #:use-module (crates-io))

(define-public crate-cargo-node-0.1.0 (c (n "cargo-node") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "sigma") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0iav63kidglcnm6kmlc2fcj83f8dbjmdk8fjbhgcr7n1y0sdxv16")))

