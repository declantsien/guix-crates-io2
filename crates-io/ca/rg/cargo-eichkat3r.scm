(define-module (crates-io ca rg cargo-eichkat3r) #:use-module (crates-io))

(define-public crate-cargo-eichkat3r-0.1.0 (c (n "cargo-eichkat3r") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "05jl4847kbd29svvijrqisy8ax97vawnd6wv38gjm9g81khzdavv") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

(define-public crate-cargo-eichkat3r-0.2.0 (c (n "cargo-eichkat3r") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "1hpn8ivim6bs29qvaj38p8x9p9ghvfbbzbh5lchmipy7rm0bv828") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

