(define-module (crates-io ca rg cargo_fetch) #:use-module (crates-io))

(define-public crate-cargo_fetch-0.1.0 (c (n "cargo_fetch") (v "0.1.0") (d (list (d (n "cargo") (r "^0.68.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "10n90gyqkkcqnxjia7d83js71wnhl63xqgy8gfd9sdpzig34vxlr")))

(define-public crate-cargo_fetch-0.1.1 (c (n "cargo_fetch") (v "0.1.1") (d (list (d (n "cargo") (r "^0.68.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "184z559g9pgykw5msmwdlmssj6ih5a7689hknpp8dv3zcgwzyk03")))

(define-public crate-cargo_fetch-0.1.2 (c (n "cargo_fetch") (v "0.1.2") (d (list (d (n "cargo") (r "^0.68.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1rjikqrq72h8qhbmzjmji3pzng9afyp2i553m407v8cispihk5yf")))

