(define-module (crates-io ca rg cargo-rclean) #:use-module (crates-io))

(define-public crate-cargo-rclean-1.0.1 (c (n "cargo-rclean") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "02rqi6nvipyybsg4gf3g2bgsfrv0ns3g3vws2yk7hqyy54cyl8z3")))

(define-public crate-cargo-rclean-1.0.2 (c (n "cargo-rclean") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "053nwrmb8mmrdc0m4mv35m75bq0100n942pnxdr8sv7a44cbsa03")))

(define-public crate-cargo-rclean-1.0.3 (c (n "cargo-rclean") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1y0yldrab3pgdsib36kfzbk1g21nzych52myvdlr062mxmdnf6r6")))

(define-public crate-cargo-rclean-1.1.0 (c (n "cargo-rclean") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "15ki25rjll9hrkci1scvxskls99kwv5l3mi0pzbc8s5d9svi8bg0")))

(define-public crate-cargo-rclean-1.2.0 (c (n "cargo-rclean") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0bjm5mrsv773xmyl592rpvz6vcjqjslhjpmraanhljd15s72dv70")))

