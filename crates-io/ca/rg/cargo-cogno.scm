(define-module (crates-io ca rg cargo-cogno) #:use-module (crates-io))

(define-public crate-cargo-cogno-0.1.0 (c (n "cargo-cogno") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.68.0") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("color" "derive" "cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zwjax2mgjjl8dym5vxx8f5s0fhprzgljg06z6p8qmc554jf003c")))

