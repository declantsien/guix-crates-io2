(define-module (crates-io ca rg cargo-docgen) #:use-module (crates-io))

(define-public crate-cargo-docgen-0.1.1 (c (n "cargo-docgen") (v "0.1.1") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2.2") (d #t) (k 0)))) (h "0w34dnb862r96d3p3a16axxhp3w1l58qbv8imh4hdcmmvlyi7i7l")))

(define-public crate-cargo-docgen-0.1.2 (c (n "cargo-docgen") (v "0.1.2") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)))) (h "0y51rjaaslksn4i2qih19hz3y4yhvl3q8n09isf0x67iz0fc23js")))

(define-public crate-cargo-docgen-0.1.3 (c (n "cargo-docgen") (v "0.1.3") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)))) (h "1kl6vkib4gaqlmrar755n0i0qxlgkid9l5m58ddz2m518lyvhvi0")))

