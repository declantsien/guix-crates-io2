(define-module (crates-io ca rg cargo-tag) #:use-module (crates-io))

(define-public crate-cargo-tag-0.1.0 (c (n "cargo-tag") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)))) (h "0d0a0fchh39al85ify07pckmmk96dia7wj3m3wifm96rjb880qsm") (y #t)))

(define-public crate-cargo-tag-0.1.1 (c (n "cargo-tag") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.10") (d #t) (k 0)))) (h "1j5ryqcab3bbvndml5p2il373g0dm4kqik4fkav1bkgyq5ccxlj9")))

