(define-module (crates-io ca rg cargo_toml_validate) #:use-module (crates-io))

(define-public crate-cargo_toml_validate-1.0.0 (c (n "cargo_toml_validate") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.6") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0r5y3j0122zknkfm7xzv4r6pqbhwims775lq10ayc7y380alq31l")))

