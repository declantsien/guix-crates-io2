(define-module (crates-io ca rg cargo-switch) #:use-module (crates-io))

(define-public crate-cargo-switch-0.1.0 (c (n "cargo-switch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1b0lrsbgpf14xk8ffyvvibsp02y303yzqssh65bbxz6bwa6y0hjg")))

(define-public crate-cargo-switch-0.1.1 (c (n "cargo-switch") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x8ip478wb2ljvvpskhqh8jsrwvnjn0lv4spi6hcpih16rjg2zfc")))

