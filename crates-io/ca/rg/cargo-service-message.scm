(define-module (crates-io ca rg cargo-service-message) #:use-module (crates-io))

(define-public crate-cargo-service-message-0.1.4 (c (n "cargo-service-message") (v "0.1.4") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ccy5wpds7yb8in3n2nrscjlgwy2y02s6pm803203z3adjihjkmz")))

(define-public crate-cargo-service-message-0.1.5 (c (n "cargo-service-message") (v "0.1.5") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xwx8j8i7f7i4bdzr2sq8z6by48cri6jj6r1yy6g67zcs6nr43cn")))

(define-public crate-cargo-service-message-0.1.8 (c (n "cargo-service-message") (v "0.1.8") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)))) (h "1sdnkfsy188kah51iydpyh26iw68cmpl5qkbp7657vs2kv01qcfq")))

