(define-module (crates-io ca rg cargo-manifest) #:use-module (crates-io))

(define-public crate-cargo-manifest-0.1.0 (c (n "cargo-manifest") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "08l3iqs0yq6s2y4jxq6mk0yz3hjbkmgcg1mn1iipssda2qvgylqf")))

(define-public crate-cargo-manifest-0.2.0 (c (n "cargo-manifest") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1rzf0nk1mr99sfx10lishzw9ki04dswwj56a99b91i6qcmy1sak6")))

(define-public crate-cargo-manifest-0.2.1 (c (n "cargo-manifest") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0f7df9yc6p1gp9jzsixhrg93d4035kai7qy0dxcns2rj9y74w97b")))

(define-public crate-cargo-manifest-0.2.2 (c (n "cargo-manifest") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "08zz4rpwyz1b5ljacgyiqwlsg1fbqkjn7cwys8djglygs482g6s4")))

(define-public crate-cargo-manifest-0.2.3 (c (n "cargo-manifest") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0vc2zin0cs7p5fjxvhq3qwknb3j81md888494xn2yrf2kqapxn9n")))

(define-public crate-cargo-manifest-0.2.4 (c (n "cargo-manifest") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1q627k3r2l1fc8d46yssjlmz1xspvnzpxi1wzy2fv3pygn96p4i7")))

(define-public crate-cargo-manifest-0.2.5 (c (n "cargo-manifest") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "15psgpkv55r30cv8mpj824f2klibsnvrmfa0gcs4a6kwdmadga0j")))

(define-public crate-cargo-manifest-0.2.6 (c (n "cargo-manifest") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "165v8qh4ih33spzq76hp01pwg1r6qrqwfzllcy69ji17b73navdg")))

(define-public crate-cargo-manifest-0.2.7 (c (n "cargo-manifest") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "02h9v9np2mcysnbwk2ykcizs259041zyq30ahd8vnnspb2p92g32")))

(define-public crate-cargo-manifest-0.2.8 (c (n "cargo-manifest") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "00c5pnn3cbl9azdk744syv0l0dpxppkgzlnprxy1cq0y92416pag")))

(define-public crate-cargo-manifest-0.2.9 (c (n "cargo-manifest") (v "0.2.9") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "19kdqzmaxnbnyp9qssnavf1f6l76p4pvqb8pb6kxwvs666iwsnng")))

(define-public crate-cargo-manifest-0.3.0 (c (n "cargo-manifest") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1x2rccqk86cwz6vg7fjppwcj2bbia00fqkp34377wv8lzy2r11gk")))

(define-public crate-cargo-manifest-0.4.0 (c (n "cargo-manifest") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0zflynzq7bwq0f0q9fisvlid10h488nyxbqb4x4pivv7ysdpq5pp")))

(define-public crate-cargo-manifest-0.5.0 (c (n "cargo-manifest") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0davakn8qjiin575ci1k0ihs82bnvg78kcjqiydy5wrkzvigdkmi")))

(define-public crate-cargo-manifest-0.6.0 (c (n "cargo-manifest") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1zd20xzdg690j6w47w26dvhhxmc17n5bg82kskmfwjw3fhvkdcjl")))

(define-public crate-cargo-manifest-0.7.0 (c (n "cargo-manifest") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0z7bal3i1dkhy9zdff7acp280l6irvn4vbd9v9j87lm0faf5767f")))

(define-public crate-cargo-manifest-0.7.1 (c (n "cargo-manifest") (v "0.7.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1zhjk6lazm0622q17rs2191xaz601dbp5a360wcfgq7v3qnqvqrw")))

(define-public crate-cargo-manifest-0.8.0 (c (n "cargo-manifest") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0dc792akqc6iijpb7rmmmw0jk73icnc3hi2gfy4pfgffqw43fmcv")))

(define-public crate-cargo-manifest-0.9.0 (c (n "cargo-manifest") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1af04lyfij0vylgrww1xcjmf3pl2knpr3ifrjs8pv71fnlgmw5m4")))

(define-public crate-cargo-manifest-0.10.0 (c (n "cargo-manifest") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1cx0blimzlgs9bb5dmrsdhdblh4lxlkhkl53lsh6bm82x54v6dhp")))

(define-public crate-cargo-manifest-0.11.0 (c (n "cargo-manifest") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "17arzchj3mi56zdib4p7p41w6r1dwc9fny147xivnvhh9cj582in")))

(define-public crate-cargo-manifest-0.11.1 (c (n "cargo-manifest") (v "0.11.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "04fhv95h67zabyblaf0gj5l8m0919s643p3vhmnpzpa96zvk6cbm")))

(define-public crate-cargo-manifest-0.12.0 (c (n "cargo-manifest") (v "0.12.0") (d (list (d (n "insta") (r "^1.33.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0hv2mfrgpdb6cj1qc3ca8a06v35d9y4j91wnr314xr6r4k0hqmpb")))

(define-public crate-cargo-manifest-0.12.1 (c (n "cargo-manifest") (v "0.12.1") (d (list (d (n "insta") (r "^1.33.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1ffb83cj1znmk14s0sd1nxc3wn3wmw96y2dm4jl3ly6py7ki7l3q")))

(define-public crate-cargo-manifest-0.13.0 (c (n "cargo-manifest") (v "0.13.0") (d (list (d (n "insta") (r "^1.33.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "13a0dgqchxjmhr4idswpri2l3lwv7fxga69yj5hvylla0adg8vxz")))

(define-public crate-cargo-manifest-0.14.0 (c (n "cargo-manifest") (v "0.14.0") (d (list (d (n "insta") (r "^1.33.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "02hd8lzqfxq9q0n8kw8n1b1sp7d2nqwax6v6s5qcm7kjs8ravdvx")))

