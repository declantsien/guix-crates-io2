(define-module (crates-io ca rg cargo-utests) #:use-module (crates-io))

(define-public crate-cargo-utests-0.1.0 (c (n "cargo-utests") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "nextest-metadata") (r "^0.9.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0vrj0dy9zrha2hs1mij6lgpp7l0h9c46akhz424vy9wb6w4b72an")))

