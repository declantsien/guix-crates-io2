(define-module (crates-io ca rg cargo-go) #:use-module (crates-io))

(define-public crate-cargo-go-0.0.1 (c (n "cargo-go") (v "0.0.1") (h "0vkjnp0h67ckdlr6c2zzm6y8adnjnd3mpgi3l33hf2n5n8bghh3c")))

(define-public crate-cargo-go-0.0.2 (c (n "cargo-go") (v "0.0.2") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (k 0)))) (h "1pq8h4j1x3iawyzzjqzm2ymyvw55man0ffz7msr8xpayfi7y1178")))

(define-public crate-cargo-go-0.0.3 (c (n "cargo-go") (v "0.0.3") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (k 0)))) (h "01gac1092bnwwgwz7yn5n9025xm4185j9q4ssnijrlpysz6ffnhn")))

(define-public crate-cargo-go-0.0.4 (c (n "cargo-go") (v "0.0.4") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (k 0)))) (h "12d6maiylv3xj7yam18y613n62rrd8w5g2slim2vvm8adqigc9kw")))

(define-public crate-cargo-go-0.1.0 (c (n "cargo-go") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (k 0)))) (h "0xw1xif7wqygmy96jqy4xxkcq83ch7zacr8km69i8srh2jd2qpdw")))

(define-public crate-cargo-go-0.2.0 (c (n "cargo-go") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0v5s2p8v4nwfx2hw2yg4lqmx345356srp50vnl2g70112ic49xr1")))

(define-public crate-cargo-go-0.2.1 (c (n "cargo-go") (v "0.2.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "1gm1ccl556wak0q2i0yplxywkiwd8ci22wpwnk4szmy10c98ywn5")))

(define-public crate-cargo-go-0.2.2 (c (n "cargo-go") (v "0.2.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0wlzrprpb6vhr9w2blaig52092zfj9yg9c22gmsas6qi35nnix6a")))

(define-public crate-cargo-go-0.3.0 (c (n "cargo-go") (v "0.3.0") (d (list (d (n "curl") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (k 0)))) (h "04ibskf54b14z3fdk19md6wzbdxxgz6as85s4ajp1a8wscc7n4s5")))

(define-public crate-cargo-go-0.4.0 (c (n "cargo-go") (v "0.4.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (k 0)))) (h "1bdhm9dvxmjb6qwbai01dw9k2im8d44bid09q5qnv1acd5zjsdcl") (y #t)))

(define-public crate-cargo-go-0.4.1 (c (n "cargo-go") (v "0.4.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (k 0)))) (h "1fzsmmnm87xkfd6ivzh3zz5db9zab8qf6mqbyv332kmlzrlxp7rn") (y #t)))

(define-public crate-cargo-go-0.4.2 (c (n "cargo-go") (v "0.4.2") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (k 0)))) (h "1rgw7mghkmzqknp1cd2s0j8z2y1dm09q2y1ja3nw9pryk6mm57m0")))

(define-public crate-cargo-go-0.4.3 (c (n "cargo-go") (v "0.4.3") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (k 0)))) (h "0bh2inx1prgp2h4fv650pax2081i0f3lbj31aib024j639nnca07")))

(define-public crate-cargo-go-0.4.4 (c (n "cargo-go") (v "0.4.4") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (k 0)))) (h "1rg9f2iqf583hmhyvgilpw6ljp6wn9wvn63f2z34qgai5fs595y6")))

