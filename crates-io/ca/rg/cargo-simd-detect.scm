(define-module (crates-io ca rg cargo-simd-detect) #:use-module (crates-io))

(define-public crate-cargo-simd-detect-0.1.0-beta.1 (c (n "cargo-simd-detect") (v "0.1.0-beta.1") (h "08c274991g5v8gncn4c47mx4qwmr9yi4wq7sn7nl2vnapkag28qd")))

(define-public crate-cargo-simd-detect-0.1.0-beta.2 (c (n "cargo-simd-detect") (v "0.1.0-beta.2") (h "128za1dr3v57pksmk4k6ra6zspilh1kwhlkab5lcnm1p0iv2ci8y")))

(define-public crate-cargo-simd-detect-0.1.0 (c (n "cargo-simd-detect") (v "0.1.0") (h "192qfxa1ljrv4l9jp7q1w11csg1haayfcllidvjnsmyap941kmz5")))

(define-public crate-cargo-simd-detect-0.1.1 (c (n "cargo-simd-detect") (v "0.1.1") (h "1w2qzicy2xpmmpi36hbndz0krs1iy9krb8g24svn4vqain1jvm3y")))

