(define-module (crates-io ca rg cargo-scout) #:use-module (crates-io))

(define-public crate-cargo-scout-0.0.1 (c (n "cargo-scout") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "03swlvhcspznfabxqa5642yb7cf8s3jkdyag4gkxg3d09kwlm07c")))

(define-public crate-cargo-scout-0.1.0 (c (n "cargo-scout") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0dil5mydgrzqbv5ydis8fp9zkx371ayg1wgqfa0ybdiymir07ffs")))

(define-public crate-cargo-scout-0.2.0 (c (n "cargo-scout") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0gixya2fyrmrw85ncv5gaj3g1z34z906ijqivgwgmk53pwzssjmi")))

(define-public crate-cargo-scout-0.3.0 (c (n "cargo-scout") (v "0.3.0") (d (list (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.10.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0qhnbcxg4rf9f8zfzf35jf5sph3496nf8q5l6pc1vf5gbk8nllx9")))

(define-public crate-cargo-scout-0.4.0 (c (n "cargo-scout") (v "0.4.0") (d (list (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1fv6kxq7vx0w081m59km4djqq21l56bv1z7nxgpp7m8bmsg7n35l")))

(define-public crate-cargo-scout-0.6.0 (c (n "cargo-scout") (v "0.6.0") (d (list (d (n "cargo-scout-lib") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "1yn7x292v7b8xj6sfszlp1p9m7ac1ibn4xanw5pnmaixh6zzqyj5")))

