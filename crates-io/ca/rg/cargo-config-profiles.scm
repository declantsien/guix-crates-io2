(define-module (crates-io ca rg cargo-config-profiles) #:use-module (crates-io))

(define-public crate-cargo-config-profiles-0.1.0 (c (n "cargo-config-profiles") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.3.2") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "11n6xarl7k3hk0nlp2rlr47kiyk01zmrvhr83jd28h5lzl2r183g")))

(define-public crate-cargo-config-profiles-0.1.1 (c (n "cargo-config-profiles") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.3.2") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0jyk6zx22x4qw1kxwadpiccyi0wwfiw7pdi57c56jc6spzh2ci1s")))

(define-public crate-cargo-config-profiles-0.1.2 (c (n "cargo-config-profiles") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.3.2") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "10xgkrd74732dhbilf0b59mxcr2qdc964754mv76yys8xfk2ivfr")))

