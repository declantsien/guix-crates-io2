(define-module (crates-io ca rg cargo-try) #:use-module (crates-io))

(define-public crate-cargo-try-0.1.0 (c (n "cargo-try") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1zzld9r7b5w93ky1mzf3fcd8jg5xlv4xxn24bb1mi8dqr2igdbi1")))

