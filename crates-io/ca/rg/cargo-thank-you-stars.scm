(define-module (crates-io ca rg cargo-thank-you-stars) #:use-module (crates-io))

(define-public crate-cargo-thank-you-stars-0.1.0 (c (n "cargo-thank-you-stars") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "03nmhbagj81zjgw2r36vg8c90md9lghfs5gj8jw7b5k723bg11rb")))

(define-public crate-cargo-thank-you-stars-0.1.1 (c (n "cargo-thank-you-stars") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "189kv34wymzj0l6xcaarr2856x8yibp62mb7p81yip0lq6fiai5v")))

