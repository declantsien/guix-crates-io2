(define-module (crates-io ca rg cargo-rustc-cfg) #:use-module (crates-io))

(define-public crate-cargo-rustc-cfg-0.1.0 (c (n "cargo-rustc-cfg") (v "0.1.0") (h "0kzv7i5h8hca94if7zxbjbsv4nbfci1nwy5zakhxgzkm0np9knza")))

(define-public crate-cargo-rustc-cfg-0.2.0 (c (n "cargo-rustc-cfg") (v "0.2.0") (h "1r7l3ibq6rv2akk5rlpqrqhrqfqz561hrapm3c4lxs4r1n1q7rw7")))

