(define-module (crates-io ca rg cargo-proto-raze) #:use-module (crates-io))

(define-public crate-cargo-proto-raze-0.1.0 (c (n "cargo-proto-raze") (v "0.1.0") (d (list (d (n "cargo") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0v3m1xifxhc7vvmlwhgbp9wk7as75kdylw4iszlw8vk589lzb77r")))

(define-public crate-cargo-proto-raze-0.1.1 (c (n "cargo-proto-raze") (v "0.1.1") (d (list (d (n "cargo") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (k 0)) (d (n "nom") (r "^2.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hxixy5cl6lc059ny576rdrk3a12s8xrr6rf20pq84n944zh7ncd")))

