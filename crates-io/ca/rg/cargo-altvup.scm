(define-module (crates-io ca rg cargo-altvup) #:use-module (crates-io))

(define-public crate-cargo-altvup-0.1.0 (c (n "cargo-altvup") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jdqfc4lkqc2xx3vjpsn43krrc17r8j37nifhv41v23grf7nvqmx") (r "1.69.0")))

(define-public crate-cargo-altvup-0.1.1 (c (n "cargo-altvup") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "125yvhz0gsd4yzsanfvjj6bp822y2mszvgjrwsqsc7xw22vr5ilg") (r "1.69.0")))

(define-public crate-cargo-altvup-0.2.0 (c (n "cargo-altvup") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zwalkayvvddcz8k185h89z6hg7s1s46ncsrmvdspwvz7r371jxs") (r "1.69.0")))

