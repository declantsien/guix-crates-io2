(define-module (crates-io ca rg cargo-swim) #:use-module (crates-io))

(define-public crate-cargo-swim-0.2.0 (c (n "cargo-swim") (v "0.2.0") (h "1akrvgizvxrdjdp7l27bpkpc4yz1b6sz83zs57ihk2xgz7yfmy58")))

(define-public crate-cargo-swim-0.2.1 (c (n "cargo-swim") (v "0.2.1") (h "1yifw0djl23mp6nd6y4i05s5c7c1lc6pjv71sfy31m5cx19j7nlk")))

