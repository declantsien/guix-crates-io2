(define-module (crates-io ca rg cargo-pkgconfig) #:use-module (crates-io))

(define-public crate-cargo-pkgconfig-0.1.1 (c (n "cargo-pkgconfig") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)))) (h "037801hbjb2y7bpfvq5hd433knph1prvac50p1zpc1pkw35q2ii3")))

(define-public crate-cargo-pkgconfig-0.1.2 (c (n "cargo-pkgconfig") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)))) (h "0r47xpddqgh6yvf0w71bxnm40yq6wyncz5vwydyyw63ijzxc1f4j")))

(define-public crate-cargo-pkgconfig-0.1.3 (c (n "cargo-pkgconfig") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)))) (h "1j0k4b7b33n63v2a8z13vgcrpjnh9sma5jdv7r7wbjybwjabb47d")))

(define-public crate-cargo-pkgconfig-0.1.4 (c (n "cargo-pkgconfig") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)))) (h "1gijr7y91l5v47wbh4nk3rjwazqn8rz2f27cphk6di1izwpi3lfn")))

