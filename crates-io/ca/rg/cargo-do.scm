(define-module (crates-io ca rg cargo-do) #:use-module (crates-io))

(define-public crate-cargo-do-0.2.0 (c (n "cargo-do") (v "0.2.0") (h "0wn02srn77fqxqzrmbd2jpqbpl6fggdlqgy12kpg30kksxzd5x29")))

(define-public crate-cargo-do-0.2.1 (c (n "cargo-do") (v "0.2.1") (h "19jm094ijjadhwackk624w8ar6w3pzjak5wp29681gwvsnikzicy")))

(define-public crate-cargo-do-0.2.2 (c (n "cargo-do") (v "0.2.2") (h "03p7p3w6r66csar0s18j15if925pmasmq6fqr5q54gcb5326m4ya")))

(define-public crate-cargo-do-0.2.3 (c (n "cargo-do") (v "0.2.3") (h "18mcj6aqarv42gqkdp0hvkz8iw81a6m9ww4kidgpjlg08603ybi3")))

(define-public crate-cargo-do-0.2.4 (c (n "cargo-do") (v "0.2.4") (h "1ml2fsimq8d7rd2wb2a4hhm5czzvnx6048biwqlrh3i7x4wgnkjb")))

(define-public crate-cargo-do-0.2.5 (c (n "cargo-do") (v "0.2.5") (h "0gxfalcs83rba8bf84z6542yabrk3r9c32nz4aidh0qkrr47wnh5")))

(define-public crate-cargo-do-0.3.0 (c (n "cargo-do") (v "0.3.0") (h "1f1sj685psycd3n0bdxyaqfsyr8n6m5ijl4b5pijzg4hi4x8sxck")))

(define-public crate-cargo-do-0.3.1 (c (n "cargo-do") (v "0.3.1") (h "1x2m3p5iw66aay36y4i1xf5mv2zl2yhgvdajkvnlgzfkx3v72bhq")))

(define-public crate-cargo-do-0.4.0 (c (n "cargo-do") (v "0.4.0") (h "0w0q1rv8zyq7b98r6zy8ny0m41a2p457qysmk10p7y9bmdhaa2kv")))

