(define-module (crates-io ca rg cargo-whichp) #:use-module (crates-io))

(define-public crate-cargo-whichp-0.1.0 (c (n "cargo-whichp") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "error-context" "help" "std" "usage"))) (k 0)) (d (n "which_problem") (r "^0.1.0") (d #t) (k 0)))) (h "0j4g4a703g5ng5bdsbqwf9djyga3pn0ibbagjrfzs3h6pr1ya7lw") (r "1.64")))

