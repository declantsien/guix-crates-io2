(define-module (crates-io ca rg cargo-disassemble) #:use-module (crates-io))

(define-public crate-cargo-disassemble-0.1.0 (c (n "cargo-disassemble") (v "0.1.0") (d (list (d (n "cargo") (r "^0.25.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1f2iwa7mnjavamwrcv1y2s2mk6l5wi3lahq78qjsb065fvcakmra")))

(define-public crate-cargo-disassemble-0.1.1 (c (n "cargo-disassemble") (v "0.1.1") (d (list (d (n "cargo") (r "^0.25.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1x0sk3ivjj45m0fzannhd6bymjlmczrw796scwcshnq2ssnda7zw")))

