(define-module (crates-io ca rg cargo-fixeq) #:use-module (crates-io))

(define-public crate-cargo-fixeq-0.1.0 (c (n "cargo-fixeq") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full" "visit"))) (k 0)))) (h "1cfz5794aw80lfr3czcsbh569xiyn9kg3306lknzm41gagjf9g4d")))

(define-public crate-cargo-fixeq-0.2.0 (c (n "cargo-fixeq") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full" "visit"))) (k 0)))) (h "15nl124avrk2kvniwg2q6r03ln8g1x01w8vjmlqq5c9a0clgkphv")))

(define-public crate-cargo-fixeq-0.3.0 (c (n "cargo-fixeq") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full" "visit"))) (k 0)))) (h "1d0iyrj6j3yx2zmr8yffq0x8iq19j9b15v1cagqr84761frwh9rn")))

(define-public crate-cargo-fixeq-0.4.0 (c (n "cargo-fixeq") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full" "visit"))) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "035vcaqhcqhlzxlkjj2bkvmdq77hlcfb76g9n7xr931h44bclcsj")))

(define-public crate-cargo-fixeq-0.5.0 (c (n "cargo-fixeq") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "full" "visit"))) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "10p6v2l017p44q1yc6fr0adwdnlwymzqpidl3w9p8c1bwgiiai8x") (r "1.73")))

