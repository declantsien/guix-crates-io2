(define-module (crates-io ca rg cargo-coverage-annotations) #:use-module (crates-io))

(define-public crate-cargo-coverage-annotations-0.1.0 (c (n "cargo-coverage-annotations") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "10n7k2vhvbw7p22qb86p54n06yl64a6szcklyysd619jkxj0qjc2")))

(define-public crate-cargo-coverage-annotations-0.1.2 (c (n "cargo-coverage-annotations") (v "0.1.2") (d (list (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1njf9jnqc3zsbzmbn805wv85xzgvd1yc9g5ih98pjy7sjh7piglh")))

(define-public crate-cargo-coverage-annotations-0.1.3 (c (n "cargo-coverage-annotations") (v "0.1.3") (d (list (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1bn9nrr8iqfrgg5g0q9sc5wwcnbfrfiqhrqa4pqr1vjyxadyacf7")))

(define-public crate-cargo-coverage-annotations-0.1.4 (c (n "cargo-coverage-annotations") (v "0.1.4") (d (list (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "0dsv565k3inacmha9p477gj417yl7y45qkp856zyzzcq93nmvh06")))

(define-public crate-cargo-coverage-annotations-0.1.5 (c (n "cargo-coverage-annotations") (v "0.1.5") (d (list (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1gg9xdshgg0gmbhkpy0d9mfm954y1r59r24rmpcsgmd8k6xnhx4h")))

(define-public crate-cargo-coverage-annotations-0.2.0 (c (n "cargo-coverage-annotations") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "00w3p57j7v7rp5f93cxi831b86j5zmz4r2cc05ql8l9sk061jiqi")))

(define-public crate-cargo-coverage-annotations-0.2.1 (c (n "cargo-coverage-annotations") (v "0.2.1") (d (list (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "17c7dl4rr42rbd711q1y930ayc0acvpwcippz621nnzq42nn38gf")))

(define-public crate-cargo-coverage-annotations-0.2.2 (c (n "cargo-coverage-annotations") (v "0.2.2") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "06jxzylba3s2fs0xkg7c8l0qdya2xj5c3zklckdvhzjz7wjzw7d9")))

(define-public crate-cargo-coverage-annotations-0.2.3 (c (n "cargo-coverage-annotations") (v "0.2.3") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "179x2x8japvi0s08nm78cm0hiq40p8lj5jzdx16n61lmzj1z86w4")))

(define-public crate-cargo-coverage-annotations-0.2.4 (c (n "cargo-coverage-annotations") (v "0.2.4") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0128ykwdlylcxqh68l7qkppx52ksh37pb976pyzgxnlprhn363ww")))

(define-public crate-cargo-coverage-annotations-0.2.5 (c (n "cargo-coverage-annotations") (v "0.2.5") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0lym6zvfhzxdfyyk89963w0jdbv0mjvl2c3di1pzii1q7m169iwp")))

(define-public crate-cargo-coverage-annotations-0.2.6 (c (n "cargo-coverage-annotations") (v "0.2.6") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1c9sxh3r5kvl1mcg3kwl6vq9x070x031m76i7inprfb8j1304acd")))

(define-public crate-cargo-coverage-annotations-0.2.7 (c (n "cargo-coverage-annotations") (v "0.2.7") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "045c0arjc23i3wi05hy91r465q5bd3yqkx83b3cqg1cqni9d3sh8")))

(define-public crate-cargo-coverage-annotations-0.2.8 (c (n "cargo-coverage-annotations") (v "0.2.8") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0v50h6wpbmg38jsp1xl9mxgrfxb4sr7vjs1hzqjybclj23g0zarh")))

(define-public crate-cargo-coverage-annotations-0.3.0 (c (n "cargo-coverage-annotations") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0fcl8mbbyxa8fnvz4r6z68ir48panj1147xlv11ickx8ksyr2h8d")))

(define-public crate-cargo-coverage-annotations-0.4.0 (c (n "cargo-coverage-annotations") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "17j1iihyq5zm9297fpqd52ab5s96fxmaldllp8rmhyy5a5qh3dql")))

(define-public crate-cargo-coverage-annotations-0.4.1 (c (n "cargo-coverage-annotations") (v "0.4.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1bmmv0771ylsjl3c0xykbl35yq3hgdfzidjcv88n5cad3rf5nqdx")))

(define-public crate-cargo-coverage-annotations-0.4.2 (c (n "cargo-coverage-annotations") (v "0.4.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "087n5bqzy62x74dxqsxsz5zg4yw54kcli8q1kplywqacc9d38rbg")))

(define-public crate-cargo-coverage-annotations-0.4.3-dev (c (n "cargo-coverage-annotations") (v "0.4.3-dev") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1dj5gdspriifc8syblhq5d8ilpm4zdswfsr14rcnlrcngr571d0d")))

(define-public crate-cargo-coverage-annotations-0.4.3 (c (n "cargo-coverage-annotations") (v "0.4.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "19xl13p4b2x284lgpd1mwgzdqj121207q7v352cpigvw88aa0i8k")))

