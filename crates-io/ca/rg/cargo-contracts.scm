(define-module (crates-io ca rg cargo-contracts) #:use-module (crates-io))

(define-public crate-cargo-contracts-0.0.1 (c (n "cargo-contracts") (v "0.0.1") (h "00a3lpiv2y89yil3dspsmz30ix56h0hvbc3pk8kls7h47yvdnx72") (y #t)))

(define-public crate-cargo-contracts-0.0.2 (c (n "cargo-contracts") (v "0.0.2") (h "1g2s294wiyhxz7jpaamy6vvylihc4bfjkvkc76q0wg9qsi632knx")))

