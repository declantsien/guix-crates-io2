(define-module (crates-io ca rg cargo-build-git) #:use-module (crates-io))

(define-public crate-cargo-build-git-0.1.0 (c (n "cargo-build-git") (v "0.1.0") (h "1gs3rydq3yp6a2vfzwwmikwqf7qkp6hlw2y11gnj6zv7di4slf4c") (y #t)))

(define-public crate-cargo-build-git-0.1.1 (c (n "cargo-build-git") (v "0.1.1") (h "0jymqgjsb9igf234wfvv5lrgka03804rr1xqkj7hhmhlh9wjqk9c") (y #t)))

(define-public crate-cargo-build-git-0.1.2 (c (n "cargo-build-git") (v "0.1.2") (h "0k77z08jqgzp4rsjsnfayy16wg1p5bm3jglwc0idyx5yzj20rhci") (y #t)))

(define-public crate-cargo-build-git-0.1.3 (c (n "cargo-build-git") (v "0.1.3") (h "1hxhj2nz60c7dhvwk5gyqcs72gfr178jx3kjsifcw21xmmkpk86x") (y #t)))

(define-public crate-cargo-build-git-0.2.0 (c (n "cargo-build-git") (v "0.2.0") (d (list (d (n "force_remove") (r "^0.1.1") (d #t) (k 0)))) (h "1l0biscps3i60zjmkga3nav7f6r255cdw20wxj9pkkxa9zr3qbv2") (y #t)))

