(define-module (crates-io ca rg cargo-v) #:use-module (crates-io))

(define-public crate-cargo-v-0.0.0 (c (n "cargo-v") (v "0.0.0") (h "0j2iizqk1h9m1vjj3fggaz1p4ybz46m3z5bblm86na9kn8a7lhfx")))

(define-public crate-cargo-v-0.0.1 (c (n "cargo-v") (v "0.0.1") (h "0a1g2qihi6rvc1ysvqpbdycbfv54qw2vydjl0rmxxd734wdmcif8")))

(define-public crate-cargo-v-0.1.0 (c (n "cargo-v") (v "0.1.0") (h "14ppsk061hv1fkjgidrq8sa5hh47bbpx5j0ga3rz1ifk2d7cgi4k")))

(define-public crate-cargo-v-0.1.1 (c (n "cargo-v") (v "0.1.1") (h "1c7wsa3gyirpnznnpid82514mgr40q0m4l8z6ylbb7khg0rb7h6h")))

(define-public crate-cargo-v-0.1.2 (c (n "cargo-v") (v "0.1.2") (h "1hs1l5fwlw5zlzn00fm4ls93x7bnsxasnr2kc0rfmai8jvd4fvm8")))

(define-public crate-cargo-v-0.1.5 (c (n "cargo-v") (v "0.1.5") (h "0a6xnvyssslbxqh9fn4c068dqarpdb76ikhfj0j48a7j767sjb4c")))

(define-public crate-cargo-v-0.1.6 (c (n "cargo-v") (v "0.1.6") (h "0l2xr5cvcfzkc5y5yk8xbvah5pgyjl4rw8gvx1v5g6xqsab46f3a")))

(define-public crate-cargo-v-0.1.8 (c (n "cargo-v") (v "0.1.8") (h "0w9jpsqbrq0s693ajzjlkv9fh0ll8wvkvjxq41fd1vl95db19vs8")))

(define-public crate-cargo-v-1.0.0 (c (n "cargo-v") (v "1.0.0") (h "1l2zibsqyjqjny2i3qckqn6zynzz7imd05dkih94phn1j45zjryk")))

(define-public crate-cargo-v-1.0.1 (c (n "cargo-v") (v "1.0.1") (h "0bvkmrql1gcidxjfb7xy1a3nqsik02wams41cpd644jd0272ql55")))

