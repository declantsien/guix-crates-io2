(define-module (crates-io ca rg cargo-cuddle) #:use-module (crates-io))

(define-public crate-cargo-cuddle-0.0.0 (c (n "cargo-cuddle") (v "0.0.0") (h "1qav2cdi6fjk80xbc3vjbsrggny97lxwvam3cwccib882g7cz6yw") (y #t)))

(define-public crate-cargo-cuddle-0.0.0-w-0 (c (n "cargo-cuddle") (v "0.0.0-w-0") (h "1f2ikkc5hks78y0wd9rsrrjwk8dfdvjgc222qxxrkwxx4pzkkmij")))

(define-public crate-cargo-cuddle-0.0.0-w-0-hey-niels-look-its-a-new-version (c (n "cargo-cuddle") (v "0.0.0-w-0-hey-niels-look-its-a-new-version") (h "0jfnzbgz0fccb2iw20nx8gb65kjqfxpsq1x6hhmxixak31nrb244") (y #t)))

(define-public crate-cargo-cuddle-0.1.0-w-0 (c (n "cargo-cuddle") (v "0.1.0-w-0") (h "1aclh3d45x3z4dly9n1ing0f6q97vbw8p6bpqhaqlnyp1wvcyg8n")))

(define-public crate-cargo-cuddle-0.2.0-w-0 (c (n "cargo-cuddle") (v "0.2.0-w-0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "extension-traits") (r "^1.0.1") (d #t) (k 0)) (d (n "uwuify") (r "^0.2.2") (d #t) (k 0)))) (h "14cl9qnkj56v62314wi7qgqascgwq01d11mrh21dl8cy6zd9cq9l") (r "1.60.0")))

