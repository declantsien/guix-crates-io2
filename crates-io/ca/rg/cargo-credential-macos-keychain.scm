(define-module (crates-io ca rg cargo-credential-macos-keychain) #:use-module (crates-io))

(define-public crate-cargo-credential-macos-keychain-0.1.0 (c (n "cargo-credential-macos-keychain") (v "0.1.0") (d (list (d (n "cargo-credential") (r "^0.1.0") (d #t) (k 0)) (d (n "security-framework") (r "^2.0.0") (d #t) (k 0)))) (h "1la4gfnxllvg6w9ynxcy8aa0y8asnq2r79ksv1cn9fv0id7x60z6")))

(define-public crate-cargo-credential-macos-keychain-0.3.0 (c (n "cargo-credential-macos-keychain") (v "0.3.0") (d (list (d (n "cargo-credential") (r "^0.3.0") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "15i7gq5z6a3896aq2bci9mc9h77g91ziij87c2zhhd91g1pf41rs")))

(define-public crate-cargo-credential-macos-keychain-0.3.1 (c (n "cargo-credential-macos-keychain") (v "0.3.1") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0qssm74skk552wlhc0i3a7sypsgjkm9rgsp3f507dqf2vfk7l7cc") (y #t)))

(define-public crate-cargo-credential-macos-keychain-0.4.0 (c (n "cargo-credential-macos-keychain") (v "0.4.0") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "04gjhm93alri361l4zqdpkv1izjx5yrw96sd4f9f8zb0pxijmmgy")))

(define-public crate-cargo-credential-macos-keychain-0.4.1 (c (n "cargo-credential-macos-keychain") (v "0.4.1") (d (list (d (n "cargo-credential") (r "^0.4.1") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1df098axn8pab6l2mljbkxg3bw1v4mcr4k42x6xy6j15854nsz70") (r "1.73")))

(define-public crate-cargo-credential-macos-keychain-0.4.2 (c (n "cargo-credential-macos-keychain") (v "0.4.2") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1rcdh6n5s7yvxw6pkhjwz0h2rqqv2mg3753w7ap20b6w8ydghz2b") (r "1.73")))

(define-public crate-cargo-credential-macos-keychain-0.4.3 (c (n "cargo-credential-macos-keychain") (v "0.4.3") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1ls1ak7xmjw5h04h1sqxz8fyiq7w6xva5kavfkrs7rgplgh0049n") (r "1.75.0")))

(define-public crate-cargo-credential-macos-keychain-0.4.4 (c (n "cargo-credential-macos-keychain") (v "0.4.4") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "security-framework") (r "^2.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0s08brq2rqnr1g3klij84rywz4mr4mfs1hw7jcq519riq4l3m2m5") (r "1.76.0")))

