(define-module (crates-io ca rg cargho) #:use-module (crates-io))

(define-public crate-cargho-0.1.4 (c (n "cargho") (v "0.1.4") (d (list (d (n "argh_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.4") (d #t) (k 0)))) (h "05bynhmpbyipmzbmrs9p008kvxr6rs895wribgxq5ihm88m3sp84")))

(define-public crate-cargho-0.1.5 (c (n "cargho") (v "0.1.5") (d (list (d (n "argh_derive") (r "^0.1.5") (d #t) (k 0) (p "cargho_derive")) (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0) (p "cargho_shared")))) (h "0adga7fb1j9fqx4chi264n06406zfq7a4frfw77la3j97h4gqlc4")))

(define-public crate-cargho-0.1.6 (c (n "cargho") (v "0.1.6") (d (list (d (n "argh_derive") (r "^0.1.5") (d #t) (k 0) (p "cargho_derive")) (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0) (p "cargho_shared")))) (h "1p8g5znrc4d2y6rad4zjrgpz8z9kqghxcbqsmv88s2vf5m9nlcrb")))

(define-public crate-cargho-0.1.7 (c (n "cargho") (v "0.1.7") (d (list (d (n "cargho_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "cargho_shared") (r "^0.1.5") (d #t) (k 0)))) (h "0fqyrh2lxrrjphi8rjkgy15yyk49mjaxl1vd9kl6ya06xhwq99w8")))

