(define-module (crates-io ca rg cargo-board) #:use-module (crates-io))

(define-public crate-cargo-board-0.1.0 (c (n "cargo-board") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "14vjcwlybgl77fg1l19vhxa2pi5as7d1irrmr3hdl8vrjfgrjqbx")))

