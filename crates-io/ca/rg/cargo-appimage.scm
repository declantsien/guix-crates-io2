(define-module (crates-io ca rg cargo-appimage) #:use-module (crates-io))

(define-public crate-cargo-appimage-0.1.0 (c (n "cargo-appimage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1g02yivgsjda3ss1c3779l4h6s5idf3akyzk2jma723mpcwm4jy7")))

(define-public crate-cargo-appimage-0.1.1 (c (n "cargo-appimage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1cwnfavk2ahmmypxc73wi41pyyq2indgxvbvl3rn7k6ypzpgrwnv")))

(define-public crate-cargo-appimage-0.1.2 (c (n "cargo-appimage") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1214can6bi5344nx68lf1y7dvfhr3f2lifmrbaqiw3zbwwpa75ds")))

(define-public crate-cargo-appimage-0.2.0 (c (n "cargo-appimage") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "0lp8f1frb4pgjb6x83rkhlyypm5qi8hhkzzdkhd03k997scqh59d")))

(define-public crate-cargo-appimage-0.2.1 (c (n "cargo-appimage") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "0ci9ydhj62997p2kkc70w47dc2im7z0ldja1rzd5bliknpqcchiw")))

(define-public crate-cargo-appimage-1.0.0 (c (n "cargo-appimage") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "093n7rb2ig31ixsh4kd941lsxmgv00k1d01di56cg6rgid4iq6jc")))

(define-public crate-cargo-appimage-1.1.0 (c (n "cargo-appimage") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "0basxlsbxj90lskb553lcrp5lzm29hjhy0gc3ya6srns1kypqgv7")))

(define-public crate-cargo-appimage-1.2.0 (c (n "cargo-appimage") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1a3h76kzb07za0h8dfzshv94ai37y0mg9qm2hw95rf97mnjjvg86")))

(define-public crate-cargo-appimage-1.3.0 (c (n "cargo-appimage") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "0axh1h1pqm7rx18w4v88i625sywiyd8m4ka288ysn8mvv91axn4g")))

(define-public crate-cargo-appimage-1.3.1 (c (n "cargo-appimage") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1s92n5hkphacy08iaz0b1fyx1xx3xfj39kj0slfvfhvwy80jhfxp")))

(define-public crate-cargo-appimage-1.4.0 (c (n "cargo-appimage") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "18kd4wd0hmf54392r00dcbfwj2nlv1p28pybbbgrpwkxyyfhxsa8")))

(define-public crate-cargo-appimage-1.4.1 (c (n "cargo-appimage") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "1h2g90q0xncsafw3dl267km36y9g7xxi7dfb2spz7yaqifmzr9zl")))

(define-public crate-cargo-appimage-1.4.2 (c (n "cargo-appimage") (v "1.4.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)))) (h "0mm0wwawr4hj7ll7z3l2fjhxdpbc00v38hmfw50j49my09qlndrw")))

(define-public crate-cargo-appimage-2.0.0 (c (n "cargo-appimage") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)))) (h "1ia8wgnbk8hw3rjml4bcj49jckf300fpl3sisbzwn4aw08zhi7s1")))

(define-public crate-cargo-appimage-2.0.1 (c (n "cargo-appimage") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)))) (h "19jphpwcsccgq99l6x14dnw29d6fap2a1yxcg7q7fgsx6ilmdzh9")))

(define-public crate-cargo-appimage-2.2.0 (c (n "cargo-appimage") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.17.1") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (d #t) (k 0)))) (h "08v0rsr01vsnd31j4d5s7mm3q76qjz4yfbxc09vys0az0na4viab")))

