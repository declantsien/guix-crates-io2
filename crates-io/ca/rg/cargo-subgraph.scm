(define-module (crates-io ca rg cargo-subgraph) #:use-module (crates-io))

(define-public crate-cargo-subgraph-0.1.0 (c (n "cargo-subgraph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "walrus") (r "^0.19") (d #t) (k 0)))) (h "0lfwzkz4n3bg13clljnd8qyikkwr1gkzn9bldpwkhpvnnkjy2wpx")))

