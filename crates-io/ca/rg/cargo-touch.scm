(define-module (crates-io ca rg cargo-touch) #:use-module (crates-io))

(define-public crate-cargo-touch-0.1.0 (c (n "cargo-touch") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1d3s00wfs46ybzzp8w3bvv4f3v4723kdgdmx2lxnywv0cnq41a45")))

(define-public crate-cargo-touch-0.2.0 (c (n "cargo-touch") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.17") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "05hnwccjivihanyx8lnmp5lpxbcalqr98k8k9abar7xvkg6kc68h")))

