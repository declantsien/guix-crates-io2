(define-module (crates-io ca rg cargo-pkg) #:use-module (crates-io))

(define-public crate-cargo-pkg-0.1.0 (c (n "cargo-pkg") (v "0.1.0") (h "07f12sv20nv1y89nc4qnlabzvk0kzwfg7mqmikjfj0ghmnks68g5")))

(define-public crate-cargo-pkg-0.1.1 (c (n "cargo-pkg") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0y1a9z484pzzv6vlhyk87nj4dkzjiw1c5dimxnhfg8wfi0q1nplr")))

(define-public crate-cargo-pkg-0.1.2 (c (n "cargo-pkg") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1m2lxcshvj91ha8a8na18smb1xb50akiwnyjd51f6k6c9msnq6mw")))

(define-public crate-cargo-pkg-0.1.3 (c (n "cargo-pkg") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0jw35y68j22ixmac9pz2vi199fd7xa2rwqaymfczypvmlg8ngsj3")))

(define-public crate-cargo-pkg-0.1.4 (c (n "cargo-pkg") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0f63xlqrhm2p9cnamv0plyrqbp3wic3x0hc6rr5j0lhnj1m5mskz")))

(define-public crate-cargo-pkg-0.1.5 (c (n "cargo-pkg") (v "0.1.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1irbf0mxb9qarnqb4fhzxxm6c1dsyiw78rxczd6a7993fhwp297w")))

(define-public crate-cargo-pkg-0.1.6 (c (n "cargo-pkg") (v "0.1.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0z0k59qgfb8b95wlhpmnm1g23lhmlcxnky2w4r8m71mv4mhgyq3c")))

(define-public crate-cargo-pkg-0.1.7 (c (n "cargo-pkg") (v "0.1.7") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13v91922h74vxmjm6a8xbiacsapw3g6m6ik5vxbnz1na1cydh79d")))

(define-public crate-cargo-pkg-0.1.8 (c (n "cargo-pkg") (v "0.1.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0i92pn3grkc3gppzslx7nvlnr959xdszlgcwp08mnnfl750m7f4s")))

(define-public crate-cargo-pkg-0.1.9 (c (n "cargo-pkg") (v "0.1.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "08bp7arfxhbz555ay30ypy8swjiml624ih33663vi1y18jbzdqxm")))

(define-public crate-cargo-pkg-0.2.0 (c (n "cargo-pkg") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "029dycmn9fpy3qr7wrzznpsrfbb98phdpwndy13va4042h9mf4lk")))

(define-public crate-cargo-pkg-0.2.1 (c (n "cargo-pkg") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0p39y2x3bb5xjaggk17c1267aiji3i9nlqf1py4ih35nhsssn4dd")))

(define-public crate-cargo-pkg-0.2.2 (c (n "cargo-pkg") (v "0.2.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0gwd3xy0p9h3d9jsghgv1sd80bw9vp172f9qah16cn5w4a00cxk9")))

(define-public crate-cargo-pkg-0.2.3 (c (n "cargo-pkg") (v "0.2.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1w9jgr8pbb125flh8fz0s0l2jddhqlrj67xhkb202a1akzkww5p9")))

(define-public crate-cargo-pkg-0.2.4 (c (n "cargo-pkg") (v "0.2.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0nkzxlphxm9g0n5ab609r694x23by8zsiyby5ffcq442pc9lfda1")))

(define-public crate-cargo-pkg-0.2.5 (c (n "cargo-pkg") (v "0.2.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0q531ac3pk5sf2knq3jpwijpkcnbbfzq325vrjsp4pqlv13wccx8")))

(define-public crate-cargo-pkg-0.2.6 (c (n "cargo-pkg") (v "0.2.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0bl3d35agkd3hqiyhn68a0flww3is14sx7qk3r5bqfvha80bl41x")))

(define-public crate-cargo-pkg-0.2.7 (c (n "cargo-pkg") (v "0.2.7") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0w1qnv9nv3vi5iian20bl4bmq5yycdlg5m45ag3zrfbiyblz0hny")))

(define-public crate-cargo-pkg-0.2.8 (c (n "cargo-pkg") (v "0.2.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1lvm7a47fihv9as40fl1y6nb5dpcmhw0pm0fi04vfh9d9rsnc649")))

(define-public crate-cargo-pkg-0.2.9 (c (n "cargo-pkg") (v "0.2.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1lai8sgmwmpr7g30hy4qwhyghh7918q09wcm0rb35srk8cp0n31f")))

(define-public crate-cargo-pkg-0.2.12 (c (n "cargo-pkg") (v "0.2.12") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "00h9w9wxnb7nxvnm8hixx6x904svm8pq7p0k9c79ch9r2sfbbq89")))

(define-public crate-cargo-pkg-0.2.13 (c (n "cargo-pkg") (v "0.2.13") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0rkfpbalw8ai8c6dfn959i66rs5nhq982bj1dzwsxg6gi928p86x")))

(define-public crate-cargo-pkg-0.2.14 (c (n "cargo-pkg") (v "0.2.14") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1gxiq1q2ril6c583y2j6qj65aq1bsdwh191mcbi9qqshci096dmk")))

(define-public crate-cargo-pkg-0.2.15 (c (n "cargo-pkg") (v "0.2.15") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1277anpaw5y4fa4fc82lrvf1sk04q1k644khklgwn9c7x6lvkicg")))

(define-public crate-cargo-pkg-0.2.16 (c (n "cargo-pkg") (v "0.2.16") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16vqxkzac89bcad8i4wr9hhjb7215gbkc1fk4rjjcjdfsjdga29m")))

(define-public crate-cargo-pkg-0.2.17 (c (n "cargo-pkg") (v "0.2.17") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "05b7wbfp9ypqji6wqq5hp3bkgz16f3qwdq1gpx025ghbw2cgaa8j")))

(define-public crate-cargo-pkg-0.2.18 (c (n "cargo-pkg") (v "0.2.18") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0lsl805ckv7llkl3f99l7qmhnfrqvz2rx1236qx1dqqwzmiz13b6")))

(define-public crate-cargo-pkg-0.2.19 (c (n "cargo-pkg") (v "0.2.19") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "05akpxzsmyk8hslvqd9kzjxaxqvrvxbs9p2jj2vqnkfd94zsbkxh")))

(define-public crate-cargo-pkg-0.2.20 (c (n "cargo-pkg") (v "0.2.20") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1h0q1rvhfjxblmbsnripqpc0p1sm4ka778z25j2cf8iq04kf2bqy")))

(define-public crate-cargo-pkg-0.2.21 (c (n "cargo-pkg") (v "0.2.21") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1kxb3bamxyl40w2axyqamsarxprg955h8gn7c7jamxmbm80jsj6l")))

(define-public crate-cargo-pkg-0.2.22 (c (n "cargo-pkg") (v "0.2.22") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "09m9whdy6a4v771wybrv6zgqrrr2xaddq846c9mjgc1jirqcsqkq")))

(define-public crate-cargo-pkg-0.2.23 (c (n "cargo-pkg") (v "0.2.23") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "17cpv1wl4rij1x0r4f30izp9xh6409yrxfks5508ldayl66qviin")))

(define-public crate-cargo-pkg-0.2.25 (c (n "cargo-pkg") (v "0.2.25") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "08dfphklg5l3x1a3g955d8crlglvkxzykqpgb6aqc58f54kvj9ca")))

(define-public crate-cargo-pkg-0.2.26 (c (n "cargo-pkg") (v "0.2.26") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1wqkcnz25l59hbcpfa33x1y7p5ypisl1ym0vzjffjl6cjipjrc57")))

(define-public crate-cargo-pkg-0.2.27 (c (n "cargo-pkg") (v "0.2.27") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "10ihlvvsrhvv953aqgnlbailvgq46qyymr0krd6k578y11f6kc38")))

(define-public crate-cargo-pkg-0.2.28 (c (n "cargo-pkg") (v "0.2.28") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0y6302bgki4nly9aa234p4xlgd0mcl3qnlin0ij64rc73wnbdkxc")))

(define-public crate-cargo-pkg-0.2.29 (c (n "cargo-pkg") (v "0.2.29") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1s3jgkk989bvvf41ibqn00qa4fapzgdhvvr0z4wj0ms6lyvj6lng")))

(define-public crate-cargo-pkg-0.2.30 (c (n "cargo-pkg") (v "0.2.30") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1fwxc105aiych6pzqa6srb80q7vkcicgzds16ywxgd5g02qgdcb5")))

(define-public crate-cargo-pkg-0.2.31 (c (n "cargo-pkg") (v "0.2.31") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0k2cc8ff70d9dqy6f9qklf0zm838bmsi068rhx0v8b5dza28xb61")))

(define-public crate-cargo-pkg-0.2.32 (c (n "cargo-pkg") (v "0.2.32") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1gd77ibk9wh77g4y88390fyjg1kqg2w18910n3i6i1cja70sd2s9")))

(define-public crate-cargo-pkg-0.2.33 (c (n "cargo-pkg") (v "0.2.33") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1f09sb3wr8kjw1h28sllrcr2xkhb2zc9m23p5q8nglras5vx7rm3")))

(define-public crate-cargo-pkg-0.2.34 (c (n "cargo-pkg") (v "0.2.34") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1rgsg55w2y58drwpiaw5kc783amb3y1ns8vy2sg1cv8iyhz3p4bk")))

(define-public crate-cargo-pkg-0.2.35 (c (n "cargo-pkg") (v "0.2.35") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "04m5jzniq26znz2rl57ccd97qpm0k2fk8iqg8z8w2ilykqckjq65")))

(define-public crate-cargo-pkg-0.2.36 (c (n "cargo-pkg") (v "0.2.36") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0qi414s9fx0dmgsg4jg4ai07waxf9lax4xqsdipv4ri8vwvq965z")))

(define-public crate-cargo-pkg-0.2.37 (c (n "cargo-pkg") (v "0.2.37") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1i665yi9cdgahhpj7hnk3h0z0q06qpsh2aq6a4kz8cfbb1wdq5zk")))

(define-public crate-cargo-pkg-0.2.38 (c (n "cargo-pkg") (v "0.2.38") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "04hl4xcmdklyn7b2m74n5rad6xjg05h8n35wk5ldziskvf5pmm1w")))

(define-public crate-cargo-pkg-0.2.39 (c (n "cargo-pkg") (v "0.2.39") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0f6ig1vqyjkx3fvqw3aw4c5726zza273x0fgrsjbn0zxs380d182")))

(define-public crate-cargo-pkg-0.2.40 (c (n "cargo-pkg") (v "0.2.40") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0vk5a5zk78mf3mnhfwcxc6lgc98vwcns9qanwrb7zavbjphgi810")))

(define-public crate-cargo-pkg-0.2.41 (c (n "cargo-pkg") (v "0.2.41") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "181c3v5q4pfx93dm90kcj73368j2a23zlfk2r6ggfh60yzyjf9ka")))

(define-public crate-cargo-pkg-0.2.42 (c (n "cargo-pkg") (v "0.2.42") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "18zpbpp2clbkagz8iqpbgzmcddmwanb0pbfsq5r9mq0qlfjp6w1n")))

(define-public crate-cargo-pkg-0.2.43 (c (n "cargo-pkg") (v "0.2.43") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1k877bhp6r7kpnvgpjmlw8byq63vnh9znmj4hjji8rmmlxwhnwzr")))

(define-public crate-cargo-pkg-0.2.44 (c (n "cargo-pkg") (v "0.2.44") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "199y7jxqbhx350h0fqqa6j72badkhbpj77pbi6sbkkznyk75f25c")))

(define-public crate-cargo-pkg-0.2.45 (c (n "cargo-pkg") (v "0.2.45") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0nyx4z8a5aa138c9magyg4l8w54sl7divhjrwwl66a8f4snvf6sg")))

(define-public crate-cargo-pkg-0.2.46 (c (n "cargo-pkg") (v "0.2.46") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1qmm34hv9pl2mjggxri5a1da861374w63168zl156bbfbrir8kzy")))

(define-public crate-cargo-pkg-0.2.47 (c (n "cargo-pkg") (v "0.2.47") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "001x3vwr5xnnsjkalxl7ma7cz94802jma5syalwp8hvaphsks4bf")))

(define-public crate-cargo-pkg-0.2.48 (c (n "cargo-pkg") (v "0.2.48") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1hgmc0g00rpdw2yq1ib4zg0cwci5x4882x4ybmpbjlwkwmayz82i")))

(define-public crate-cargo-pkg-0.2.49 (c (n "cargo-pkg") (v "0.2.49") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "067a72mzp9wj7rq6k0rp75z6hclv70m6q13fkl0ifcxcmcxysrh4")))

(define-public crate-cargo-pkg-0.2.50 (c (n "cargo-pkg") (v "0.2.50") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0kslrrvcrk96wx2xdglhz53mgakvvm7fappq0rp2fxcwn5y4yyx4")))

(define-public crate-cargo-pkg-0.2.51 (c (n "cargo-pkg") (v "0.2.51") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0sc3sbdcwkg62pyn15q0q5l98rw5w4f3jng6liis1qkh9ibxm60d")))

(define-public crate-cargo-pkg-0.2.52 (c (n "cargo-pkg") (v "0.2.52") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0jvm8ls6myng597r87hcv4z848ykr33ylfnpwpa1kyk283n3gi4j")))

(define-public crate-cargo-pkg-0.2.53 (c (n "cargo-pkg") (v "0.2.53") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0qh2fv6yzfzkz73zad5z7yavz48m6ly0180a70725c5aqcd2h34y")))

(define-public crate-cargo-pkg-0.2.54 (c (n "cargo-pkg") (v "0.2.54") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "06vmwhib45rwvbjax3vvzy656m48ahxwja1s0ccq96f0ih5ksz52")))

(define-public crate-cargo-pkg-0.2.55 (c (n "cargo-pkg") (v "0.2.55") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1mdcyzsjfsl727bc261nbldh7c53a0ai6ln07rm1bmf26fvs53a2")))

(define-public crate-cargo-pkg-0.2.56 (c (n "cargo-pkg") (v "0.2.56") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "07iby2nr4fqg13wyvzq17fb9m9djyd8p56dkp961gngl8l8b3zpy")))

(define-public crate-cargo-pkg-0.2.57 (c (n "cargo-pkg") (v "0.2.57") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "15xvlpin7wip0k8bhkngdhn1nn9ya122wp4ybld4jxmkmv1f7c25")))

(define-public crate-cargo-pkg-0.2.58 (c (n "cargo-pkg") (v "0.2.58") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0gk4nld2z1yqb2slcfwxkyn22pj1907j5yqdlfrm7b2ldchair5r")))

(define-public crate-cargo-pkg-0.2.59 (c (n "cargo-pkg") (v "0.2.59") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "06wb84ng18bxm1vq5bb9bzlk6pbanbcggfkz82h3yfh2089dyjxq")))

(define-public crate-cargo-pkg-0.2.60 (c (n "cargo-pkg") (v "0.2.60") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13f6vyj4id9zf1xirywrj0j492l9p0zlgvbmh9rapgfnsw2b9nfx")))

(define-public crate-cargo-pkg-0.2.61 (c (n "cargo-pkg") (v "0.2.61") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0a9a6irdpzjk7nqp0zd5424lz34587l0mw1iwv7lsf2pdwsm8qra")))

(define-public crate-cargo-pkg-0.2.62 (c (n "cargo-pkg") (v "0.2.62") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bvfl3m8s8088f8ai3nkj1n4hbsl7y94bvwxkp3mavbaxn2xi1q5")))

(define-public crate-cargo-pkg-0.2.63 (c (n "cargo-pkg") (v "0.2.63") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1mlpzwqryx6gillm356c7cfpv8abn8g0n9c69fni26qapnji24pq")))

(define-public crate-cargo-pkg-0.2.64 (c (n "cargo-pkg") (v "0.2.64") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "106ln8rjz6v47g7s1lvavxgym5326a8q7pm645h534ywab9cs9yw")))

(define-public crate-cargo-pkg-0.2.65 (c (n "cargo-pkg") (v "0.2.65") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1x3gwgfz15r28l9qgnpnjvvxsv5nx2qic0jzfvbqdjgcra6y0qxx")))

(define-public crate-cargo-pkg-0.2.66 (c (n "cargo-pkg") (v "0.2.66") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kij9d0npwifphyf08c1fn038b7sr2q5a0hqyqf139pcnclirl7l")))

(define-public crate-cargo-pkg-0.2.67 (c (n "cargo-pkg") (v "0.2.67") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "04rpsnq05mca9z3hjjrsq6im5illc0lyys0b6wxjsaszqhdy1kwy")))

(define-public crate-cargo-pkg-0.2.68 (c (n "cargo-pkg") (v "0.2.68") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "10q76f6hrxbspbmh4l7dywmszkss94a6brply9x859y1psjv33gc")))

(define-public crate-cargo-pkg-0.2.69 (c (n "cargo-pkg") (v "0.2.69") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "16aw9fyixq8wisj2qpcrqzd00i7pivr9y8v20y7h5w1srjwb8b9q")))

(define-public crate-cargo-pkg-0.2.70 (c (n "cargo-pkg") (v "0.2.70") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "119mbimd58s8srdn13yn2ijy6ib5m49lbhhzj710c2ymrpjfcfmx")))

(define-public crate-cargo-pkg-0.2.71 (c (n "cargo-pkg") (v "0.2.71") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ncmrgdksx4fnsfpgqmi8jhyhh05chrv19vpzd7i2hzrkl61vzpz")))

(define-public crate-cargo-pkg-0.2.72 (c (n "cargo-pkg") (v "0.2.72") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0myps6qhgdasprgy6apl85lvm80gcd6a13zbwb4ps2z2lqp1a3mc")))

(define-public crate-cargo-pkg-0.2.73 (c (n "cargo-pkg") (v "0.2.73") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zwfwwy7l80aghfkwmp7bhg07ag424hqrpzj8miq7i8l5xwdbngx")))

(define-public crate-cargo-pkg-0.2.74 (c (n "cargo-pkg") (v "0.2.74") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gmlzvbcz4y5qamm1wqv78zhriwxbb077145562hab6q37r4s7zf")))

(define-public crate-cargo-pkg-0.2.75 (c (n "cargo-pkg") (v "0.2.75") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jqvp9n2m7hj5lj8xic2q2v5cygd1gklrcgg6l1ijxr1s3h7k0yj")))

(define-public crate-cargo-pkg-0.2.76 (c (n "cargo-pkg") (v "0.2.76") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ghi2jyb7lz32c7x2hbr3l8wlfqj9cqzwbz9dxvn01i0k9i0fvfl")))

(define-public crate-cargo-pkg-0.2.77 (c (n "cargo-pkg") (v "0.2.77") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0v0i5z5ina0kwc8yf8vp2h2ybzi0ba8s8micapb7l73c68h7w4v6")))

(define-public crate-cargo-pkg-0.2.78 (c (n "cargo-pkg") (v "0.2.78") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zk1jz1c74rv96jv745zira1k5k2wm5496vz5s4d783kkgj529c5")))

(define-public crate-cargo-pkg-0.2.79 (c (n "cargo-pkg") (v "0.2.79") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0avhnkspkqb30cy2dln25sn8xb929wncxmbz02sfg0f70jbghykw")))

(define-public crate-cargo-pkg-0.2.80 (c (n "cargo-pkg") (v "0.2.80") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1n3l23z22ifq77fvfr7fwjplpgfgnxzrsvh9lip40ijrjh2rj240")))

(define-public crate-cargo-pkg-0.2.81 (c (n "cargo-pkg") (v "0.2.81") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0qrnkryfhirb6m8jam6a8s72ar4ff23fb4rbgmimhfhj2b0wdqk9")))

(define-public crate-cargo-pkg-0.2.82 (c (n "cargo-pkg") (v "0.2.82") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1caz33d39jvgw4kikc9mxn50scnyds5qpqi4rwbr4n6hhjxaliij")))

(define-public crate-cargo-pkg-0.2.83 (c (n "cargo-pkg") (v "0.2.83") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0z1la9n2xa9imlvx9yi25gnknaakr9x5657gyv6fxm01j0damsma")))

(define-public crate-cargo-pkg-0.2.84 (c (n "cargo-pkg") (v "0.2.84") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vp75dfv49mhrdy300kqyc5xny9c1k7p360322bqa85i008yxra7")))

