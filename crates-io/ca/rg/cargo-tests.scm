(define-module (crates-io ca rg cargo-tests) #:use-module (crates-io))

(define-public crate-cargo-tests-0.1.0 (c (n "cargo-tests") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jgsl1ymssxry1bxh5s7r263h88aw4icxdz4zac8wv2k0na5lfql")))

(define-public crate-cargo-tests-0.2.0 (c (n "cargo-tests") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0badf13ra3rs3c9x2crswcf7q9rvkp1665flkchd65f47754rlfy")))

(define-public crate-cargo-tests-0.2.1 (c (n "cargo-tests") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0260ypx7biqr916nvv52g89wlqlhw93rydw05454k9zrdkjxlkrv")))

(define-public crate-cargo-tests-0.2.2 (c (n "cargo-tests") (v "0.2.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1n72x7jhwms8askas5gmlwksq6hd4f2ib5ixb86hfpmdwf7p2xyz")))

