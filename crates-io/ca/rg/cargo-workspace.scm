(define-module (crates-io ca rg cargo-workspace) #:use-module (crates-io))

(define-public crate-cargo-workspace-0.1.0 (c (n "cargo-workspace") (v "0.1.0") (d (list (d (n "rayon") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "168k8ggg95rlyyqkav6mp849wkgyqq4z8avndxrxkcz9lkiyjcgi")))

