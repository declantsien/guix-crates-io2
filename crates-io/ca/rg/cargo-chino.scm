(define-module (crates-io ca rg cargo-chino) #:use-module (crates-io))

(define-public crate-cargo-chino-0.1.0 (c (n "cargo-chino") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "viuer") (r "^0.4.0") (d #t) (k 0)))) (h "0yn4yv6ah016040ngq9lldfi898xlh2ixakdqmjr86j1xmry2h9b")))

(define-public crate-cargo-chino-0.1.1 (c (n "cargo-chino") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "viuer") (r "^0.4.0") (d #t) (k 0)))) (h "13x74p8n3ncb0x1yscb9jbrc57xg5fxmaisv8jrnqxmcqxjnlcqy")))

(define-public crate-cargo-chino-0.1.2 (c (n "cargo-chino") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "viuer") (r "^0.4.0") (d #t) (k 0)))) (h "0v30snf301cd4dwrg814n5hanrscik7plqc0ggsq37f0v241saw5")))

(define-public crate-cargo-chino-0.1.3 (c (n "cargo-chino") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "viuer") (r "^0.4.0") (d #t) (k 0)))) (h "1wgmp6yysb58ajilaww9gd662r8ic8jfxq6g4mj4ci7kzlkchx0p")))

