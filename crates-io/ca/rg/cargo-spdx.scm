(define-module (crates-io ca rg cargo-spdx) #:use-module (crates-io))

(define-public crate-cargo-spdx-0.1.0 (c (n "cargo-spdx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)))) (h "1z47vgfcgsd4jbpnr95r4lmarfyak2s6l8my4yxhxkpfd3nl4ivs") (r "1.56")))

