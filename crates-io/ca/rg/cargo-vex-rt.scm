(define-module (crates-io ca rg cargo-vex-rt) #:use-module (crates-io))

(define-public crate-cargo-vex-rt-0.1.0 (c (n "cargo-vex-rt") (v "0.1.0") (d (list (d (n "cargo-subcommand-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0nnkkjc1y5n0714gxsi5yiy9psprgqvz06b8qq14b0w6g0nmnv74")))

(define-public crate-cargo-vex-rt-0.2.0 (c (n "cargo-vex-rt") (v "0.2.0") (d (list (d (n "cargo-subcommand-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1q8lpx8azklha6kbf80m9mlq2z3smgjcwsqn49w1lk1mfqimvpnb")))

