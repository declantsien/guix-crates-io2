(define-module (crates-io ca rg cargo-stabilize) #:use-module (crates-io))

(define-public crate-cargo-stabilize-0.1.0 (c (n "cargo-stabilize") (v "0.1.0") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1ji4ipg7m0v5q839vbbglkbpflp38c4rlcgns2bgw5lmfwpahib2")))

(define-public crate-cargo-stabilize-0.2.0 (c (n "cargo-stabilize") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1s4j8q6zra1ipzxpisgggqaw51iqdwi3hxffjcsz5ah5nzgpkqly")))

