(define-module (crates-io ca rg cargo-qtest) #:use-module (crates-io))

(define-public crate-cargo-qtest-0.1.0 (c (n "cargo-qtest") (v "0.1.0") (d (list (d (n "inquire") (r "^0.6.2") (f (quote ("crossterm"))) (k 0)) (d (n "regex") (r "^1.10.2") (k 0)))) (h "005na1nb6pd60wp35zqr40li5p6ans6avc0cx3prfhk3mlkmfvai") (y #t)))

(define-public crate-cargo-qtest-0.1.1 (c (n "cargo-qtest") (v "0.1.1") (d (list (d (n "inquire") (r "^0.6.2") (f (quote ("crossterm"))) (k 0)) (d (n "regex") (r "^1.10.2") (k 0)))) (h "0zpn88zrdz9b5dg71x3p0fcsm4qwcj9f5c7f3n9krb3lcrr7qy86")))

(define-public crate-cargo-qtest-0.1.2 (c (n "cargo-qtest") (v "0.1.2") (d (list (d (n "inquire") (r "^0.6.2") (f (quote ("crossterm"))) (k 0)) (d (n "regex") (r "^1.10.2") (k 0)))) (h "183zl565np1kcg1skar875kryayigcfldr86vk6cg6xfc27a5x77")))

(define-public crate-cargo-qtest-0.1.3 (c (n "cargo-qtest") (v "0.1.3") (d (list (d (n "inquire") (r "^0.6.2") (f (quote ("crossterm"))) (k 0)) (d (n "regex") (r "^1.10.2") (k 0)))) (h "0km3zv096c5b6mvnzssvi5qk9xyv9j9x3w79mfbzgwagkapsgclj")))

