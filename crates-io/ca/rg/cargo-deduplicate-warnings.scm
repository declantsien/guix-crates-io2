(define-module (crates-io ca rg cargo-deduplicate-warnings) #:use-module (crates-io))

(define-public crate-cargo-deduplicate-warnings-0.1.0 (c (n "cargo-deduplicate-warnings") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w8m192nrs25nhyjzqnnwm3m9iwysip4f63a3jx9k7ffhkrq975w")))

