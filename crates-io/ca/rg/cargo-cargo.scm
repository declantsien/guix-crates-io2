(define-module (crates-io ca rg cargo-cargo) #:use-module (crates-io))

(define-public crate-cargo-cargo-0.1.0 (c (n "cargo-cargo") (v "0.1.0") (h "0lrw833p6nx8fgzcykd88zax8r94da0qby0hrfggynj785rfn9xj")))

(define-public crate-cargo-cargo-1.0.0 (c (n "cargo-cargo") (v "1.0.0") (h "0m04zj07k0csf191z954vxclga85yaaznk51lmrw46cb07428i58")))

