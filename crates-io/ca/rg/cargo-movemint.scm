(define-module (crates-io ca rg cargo-movemint) #:use-module (crates-io))

(define-public crate-cargo-movemint-0.0.0 (c (n "cargo-movemint") (v "0.0.0") (d (list (d (n "abscissa_core") (r "^0.3.0") (d #t) (k 0)) (d (n "abscissa_core") (r "^0.3.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1591v4rwpy1hf5m3cqaiwxqrnzgwxhwmcyq4jgg8b1ih64irz9qf") (y #t)))

