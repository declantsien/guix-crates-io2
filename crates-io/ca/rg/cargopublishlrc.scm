(define-module (crates-io ca rg cargopublishlrc) #:use-module (crates-io))

(define-public crate-cargoPublishLRC-0.0.1 (c (n "cargoPublishLRC") (v "0.0.1") (h "0k45gw1wy6pra1dys5l0b3mws5rqqqwlas20r9dhd3jhpisdpil3")))

(define-public crate-cargoPublishLRC-0.0.2 (c (n "cargoPublishLRC") (v "0.0.2") (h "1fhzq5l4mhvv47mn7c1smivylqg0l3dvm8d0qwmin7197v8ihils")))

