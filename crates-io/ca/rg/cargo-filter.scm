(define-module (crates-io ca rg cargo-filter) #:use-module (crates-io))

(define-public crate-cargo-filter-0.1.0 (c (n "cargo-filter") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "012icyqpk7ai89a3avp8m8lpwfgisxqhjca0h2rgskiivhw94alz")))

