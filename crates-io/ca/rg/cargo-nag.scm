(define-module (crates-io ca rg cargo-nag) #:use-module (crates-io))

(define-public crate-cargo-nag-0.1.0 (c (n "cargo-nag") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1vzxcds6yd6nqg19xlhpd2qzcva1lb5f3j29pj6lnkpcmsrylmlk")))

