(define-module (crates-io ca rg cargo-outdir) #:use-module (crates-io))

(define-public crate-cargo-outdir-0.1.0 (c (n "cargo-outdir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.8") (f (quote ("cargo_metadata"))) (d #t) (k 0)) (d (n "indexmap") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jl2ya3c0pl3kyl2b6gvv06n2h3158c5y261ikmbzkn1f194wi0k")))

