(define-module (crates-io ca rg cargo-report) #:use-module (crates-io))

(define-public crate-cargo-report-0.1.0 (c (n "cargo-report") (v "0.1.0") (d (list (d (n "cargo-lock") (r "^6.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rustsec") (r "^0.23.3") (f (quote ("git"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zj3zlw872isb26wi1m11dz13svf12c494hbfk6d7sn6m67yrpv1")))

(define-public crate-cargo-report-0.2.0 (c (n "cargo-report") (v "0.2.0") (d (list (d (n "cargo-lock") (r "^6.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rustsec") (r "^0.23.3") (f (quote ("git"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0dwvxnszh9xrsni7j7vqs8mq6ja5bgfv963xxz3pny3k4va30kzw")))

