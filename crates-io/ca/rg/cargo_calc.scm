(define-module (crates-io ca rg cargo_calc) #:use-module (crates-io))

(define-public crate-cargo_calc-0.1.1-alpha (c (n "cargo_calc") (v "0.1.1-alpha") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w4kjfvjbm8fhl166nh9yhbad166h6yym590g7nwxqxm1zjkwskh")))

(define-public crate-cargo_calc-0.1.3-alpha (c (n "cargo_calc") (v "0.1.3-alpha") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pn920sk65mx6wggj1lbnbshnawqlbbicd0iasrv0fbx51pmgawi") (r "1.56")))

(define-public crate-cargo_calc-0.1.5-alpha (c (n "cargo_calc") (v "0.1.5-alpha") (h "1zr6a1pdlngywrkfphl9ipd144bgip6pnhfqxmxzlkyjzbhmslyx") (r "1.56")))

(define-public crate-cargo_calc-0.2.0-alpha (c (n "cargo_calc") (v "0.2.0-alpha") (h "18hhk15rww1bapw4c0x00cfzwazhsqw8lf30myksw45qrrah87hb") (r "1.56")))

