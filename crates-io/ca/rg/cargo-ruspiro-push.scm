(define-module (crates-io ca rg cargo-ruspiro-push) #:use-module (crates-io))

(define-public crate-cargo-ruspiro-push-0.1.0 (c (n "cargo-ruspiro-push") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "1b38kl07zzyi482ix38rk1jgwb82c84d5998rm6jia5hndcvhr6w")))

(define-public crate-cargo-ruspiro-push-0.1.1 (c (n "cargo-ruspiro-push") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0g29qz6qyvc8i4yarafpiy0rh9f31ijr9wpgmadfmn5r9yxhxjj0")))

