(define-module (crates-io ca rg cargo-rio) #:use-module (crates-io))

(define-public crate-cargo-rio-0.1.0 (c (n "cargo-rio") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1zglxy25sbmjs4qi2bmfg5rhmqqgj89870dirnzpr0mnw2i73wig")))

