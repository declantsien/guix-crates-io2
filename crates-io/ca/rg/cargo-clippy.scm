(define-module (crates-io ca rg cargo-clippy) #:use-module (crates-io))

(define-public crate-cargo-clippy-0.1.0 (c (n "cargo-clippy") (v "0.1.0") (h "050gh9hi5wlbx20kd1l89nrn567isyc3zwyskfz7qfxd7l064zni") (y #t)))

(define-public crate-cargo-clippy-0.1.1 (c (n "cargo-clippy") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)))) (h "15yq5nzwyg6w5qlmdyidr4csvp1fllfv905ddh5mikas516ssky1") (y #t)))

(define-public crate-cargo-clippy-0.2.0 (c (n "cargo-clippy") (v "0.2.0") (h "0vhn0fw7k96l8y1nzn5w8a38z81ah4af2wprw2dj2q27n0b8v48h") (y #t)))

(define-public crate-cargo-clippy-0.2.1 (c (n "cargo-clippy") (v "0.2.1") (h "1ghbsgwl92d187vq28krflkcgf0i6vm6r19cmc6r5681k7sh9fr6") (y #t)))

(define-public crate-cargo-clippy-0.2.2 (c (n "cargo-clippy") (v "0.2.2") (h "0r7mn935x018zl3na5yzkpi793ggqc4x39qbxvi154fgwl1hcpwy") (y #t)))

