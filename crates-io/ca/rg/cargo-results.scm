(define-module (crates-io ca rg cargo-results) #:use-module (crates-io))

(define-public crate-cargo-results-0.5.0 (c (n "cargo-results") (v "0.5.0") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1nx8w6qid8xk6hcmksviiiwkrc4f2vkbq0q5l70i7c6l9s5v1cfw")))

(define-public crate-cargo-results-0.6.0 (c (n "cargo-results") (v "0.6.0") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0c39qqbvyq99kjsqqxzcc42ysa5hhfs9bd73pgqanr0izj506402")))

(define-public crate-cargo-results-0.6.2 (c (n "cargo-results") (v "0.6.2") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1cmdrndqb0d9x5pknhwxi7zgfvs2ip3hgx7hrd3r64a3lss08pdv")))

