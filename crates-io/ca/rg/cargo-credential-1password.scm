(define-module (crates-io ca rg cargo-credential-1password) #:use-module (crates-io))

(define-public crate-cargo-credential-1password-0.1.0 (c (n "cargo-credential-1password") (v "0.1.0") (d (list (d (n "cargo-credential") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "16z7856zrxx78648if07zw4vagi18sdfkpg0ilja5kiwmaqr7rbc")))

(define-public crate-cargo-credential-1password-0.3.0 (c (n "cargo-credential-1password") (v "0.3.0") (d (list (d (n "cargo-credential") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "091flai45rs809a4d1zgcx9p6w7myzjjk47037bc95d32gx8phiw")))

(define-public crate-cargo-credential-1password-0.4.0 (c (n "cargo-credential-1password") (v "0.4.0") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1qrvn5n6spsmim84cgygi4sqqi60ccygl6xyqxvwpb194qf2l160")))

(define-public crate-cargo-credential-1password-0.4.2 (c (n "cargo-credential-1password") (v "0.4.2") (d (list (d (n "cargo-credential") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "01zbxgrqnn3az8vm7pp7h0c9h9lwgk22iyydrjii35an8wf8m2dh") (r "1.70.0")))

(define-public crate-cargo-credential-1password-0.4.4 (c (n "cargo-credential-1password") (v "0.4.4") (d (list (d (n "cargo-credential") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "0nzn97bxv94jbwx1h5vyv7spznjv710vqqh495h3qgvclbdw83s6") (r "1.73")))

