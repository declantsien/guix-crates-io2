(define-module (crates-io ca rg cargo-modify) #:use-module (crates-io))

(define-public crate-cargo-modify-0.1.0 (c (n "cargo-modify") (v "0.1.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05h9b94dq3bnvdqnbglgiy35gjn9sca4zrdfmcbrcrx26wv0gfpj")))

(define-public crate-cargo-modify-0.2.0 (c (n "cargo-modify") (v "0.2.0") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "12m9pc2d9a85bmj8dakaa6ifbhfwph9sj5ggnabgrp4dl9spclyb")))

(define-public crate-cargo-modify-0.3.0 (c (n "cargo-modify") (v "0.3.0") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0m1q2h0qd3a3pkra985xppbhrn05bwwla4ypayj3r6rzzl6a1ppc")))

(define-public crate-cargo-modify-0.3.1 (c (n "cargo-modify") (v "0.3.1") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03d7caswn411wx6wny5n928fw2h9g8nydh33rpfs845y304d8c6i")))

