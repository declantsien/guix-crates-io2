(define-module (crates-io ca rg cargo-vendor-add) #:use-module (crates-io))

(define-public crate-cargo-vendor-add-0.0.1 (c (n "cargo-vendor-add") (v "0.0.1") (h "09ar05zkq2lbf48wh1fw6qfxjl1fz2qrcnjnv4w8dizbar1sf3qg") (y #t)))

(define-public crate-cargo-vendor-add-0.1.0 (c (n "cargo-vendor-add") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.2.16") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0l2a1kw0g7xxg8jvbig69v1jysz2py7vfzb9hxrdn9fiqy6aafdv") (r "1.63")))

