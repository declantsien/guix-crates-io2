(define-module (crates-io ca rg cargo-gui) #:use-module (crates-io))

(define-public crate-cargo-gui-0.1.0 (c (n "cargo-gui") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.1.7") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "05786bmnb2ph3lcz48vkpfywc1ai55q6wx2d4c76c8hk3b8vm14s")))

(define-public crate-cargo-gui-0.1.1 (c (n "cargo-gui") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.1.7") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1a13a352bf4r24qw0qz8mw6hnm8bxxb4pn70w86ay6al89zlifgd")))

(define-public crate-cargo-gui-0.1.2 (c (n "cargo-gui") (v "0.1.2") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tide") (r "^0.4.0") (d #t) (k 0)) (d (n "tide-naive-static-files") (r "^1.0.0") (d #t) (k 0)))) (h "1bafd1qnadpi186nca5wqvb032gm2z6347nkxa4acqv3pws1xn6f")))

(define-public crate-cargo-gui-0.3.0 (c (n "cargo-gui") (v "0.3.0") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tide") (r "^0.4.0") (d #t) (k 0)) (d (n "tide-naive-static-files") (r "^1.0.0") (d #t) (k 0)))) (h "17cyv26l6i5wgp64yvrbp715l8dz53byrbcwpl3dfwja6r1cv61k")))

(define-public crate-cargo-gui-0.3.1 (c (n "cargo-gui") (v "0.3.1") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tide") (r "^0.4.0") (d #t) (k 0)) (d (n "tide-naive-static-files") (r "^2.0.1") (d #t) (k 0)))) (h "1rlfidjkdaala2dmprgjcp4p6dn4srxqmnkrklv106wxfvx9dwa9")))

