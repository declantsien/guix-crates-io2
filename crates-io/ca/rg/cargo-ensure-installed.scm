(define-module (crates-io ca rg cargo-ensure-installed) #:use-module (crates-io))

(define-public crate-cargo-ensure-installed-0.1.0 (c (n "cargo-ensure-installed") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1d4zvh7885i0d4ay1r36dia7n2f3pmk9dpcsaycryfr9pry6l7jz")))

(define-public crate-cargo-ensure-installed-0.2.0 (c (n "cargo-ensure-installed") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0vmwsq20cbj9f7byzhp3v4399nmfb4bcrnbf3ln3ahyw9clzynsl") (y #t)))

(define-public crate-cargo-ensure-installed-0.2.1 (c (n "cargo-ensure-installed") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1g9mcg3gayha1qi56wbq7rvs2i19k69vbyqy7831sbim8nbhsyn9")))

