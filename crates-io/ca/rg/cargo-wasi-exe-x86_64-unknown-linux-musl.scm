(define-module (crates-io ca rg cargo-wasi-exe-x86_64-unknown-linux-musl) #:use-module (crates-io))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.1 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.1") (h "02k4wd5nadldb79hd1wgqhx57bpkg6k4nsr4lkiqsglqcl5g6m24")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.2 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.2") (h "0m38lssnaj8q8mygrz5w5bfb9m3y3qiz2p1lwljim7jqmr519qxj")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.3 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.3") (h "1xz6nxgb7rrrh0w5amfrpxhhb7lxm23cn87na0c7sgcll4b2rs99")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.4 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.4") (h "1k7bzbmzc10k32wjsmqrddp0yb7ckh6q30dya9w8gwk81i7p9hy5")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.5 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.5") (h "1f3hldizj2x4l1x1khhrdwh4192s51fsxkza6dxs9nmy6pkxnibv")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.6 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.6") (h "00dmdjjsgbxqqvn0lqs9v9kvg6p146mm20mafq05i77nywq0p8jv")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.8 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.8") (h "10b0v7jha84gbzxbz9kh6zv6byzgbpbfnknwqcxh2z7zybn69807")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.9 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.9") (h "0mciw7pdjvzap1c53qhdb7682cwrhs2a1i92jcirvzvyn6bc2b7g")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.10 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.10") (h "01sn91b4j0aj4c6m3qsf0yc80l1jc6dwv0jsdc0df07fw6bj693r")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.11 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.11") (h "0j2fgpyha2crmgm9f077nrhf0gl2fy9ap7zag3m3dv4g9k15d5dd")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.12 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.12") (h "0gv4mgzvw9r4ngml3b2hhvmc2f01gck83rag1ii3zzqcix6ab7kx")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.13 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.13") (h "1mz6avdbj4n1na4927wskfvwn7ndv2f362vcb4ixwi6y83jiy7kj")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.14 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.14") (h "073ncx28pcw13nx361v2vvjlr812srhhblxjlc5nqb62jzfb9hcp")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.15 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.15") (h "0qhf9gzvw56r79kl4f22wjvinz99pf1snv55apqwafimwq5a9bip")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.16 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.16") (h "1ry2s45gxz8nnrblqxjysl61w76ngbjh81nq498pr682pjdyv38l")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.17 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.17") (h "12yx8xwz4gampjh7iy5i24bhg491g3q8ll7f9h4dy8s6aqvng99d")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.18 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.18") (h "1l6r9g43cs0grwybml55nacfpkfbbvks8vgmfxf0rjrshd4j5mq1")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.19 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.19") (h "1vi7iq5y44mwznws9chl1nnmpkb619qpny5cf3w90fw9x3zz6kf4")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.20 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.20") (h "1n8q08y9a0znd9z1qxwvqlj2m09gm5bcg24z911bcvvip8q8rh1f")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.21 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.21") (h "0li1902a7afj65wcvhppw7hd9dd39hw1pcbk3z5bg0x2a5bkypf6")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.22 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.22") (h "0xrlc5hc9kajgqnrf5gknm0ylbajc3inm5jnviydkngfs1la6wir")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.23 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.23") (h "0qzbgaw1rxx2bclp9nbk5nfkj0lk6dh8qfxnl89g4dgl966jkw28")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.24 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.24") (h "0wqv1m6azmd8qmk1kzsibky41ba8hpngfk6618zfl2744yv2l477")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.25 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.25") (h "1i3f6r493vxkj7hgadnbzz5mxlm5y99nvlh8y1sysv43fr9dnvg4")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.26 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.26") (h "1fbllqgfk16yd9gili0j9808dcs2v2b8imrwmz8jyb7n30qvg5da")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.27 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.27") (h "11yl8kjyd1mpn1k581qlrhmvnx9x7sfw7vpyawybl7mz8fkqbyy7")))

(define-public crate-cargo-wasi-exe-x86_64-unknown-linux-musl-0.1.28 (c (n "cargo-wasi-exe-x86_64-unknown-linux-musl") (v "0.1.28") (h "1zg5i158i09q2l6gkajkb6k59wc2p0cc10cz4hmp67s4rpnz20ii")))

