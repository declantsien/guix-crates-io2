(define-module (crates-io ca rg cargo-workspace-lints) #:use-module (crates-io))

(define-public crate-cargo-workspace-lints-0.1.0 (c (n "cargo-workspace-lints") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0gsl8swnwwx2g3lcfjb5hgzx2ggzv4ac6r2l49qxs8ximl1cxsyz")))

