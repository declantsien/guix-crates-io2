(define-module (crates-io ca rg cargo-platform) #:use-module (crates-io))

(define-public crate-cargo-platform-0.1.0 (c (n "cargo-platform") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b26rmmvmpsv4kjakfxmshqaj8qvwc9wspa025x8v6f0hdvcx377")))

(define-public crate-cargo-platform-0.1.1 (c (n "cargo-platform") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzi60pf0z83qkzqp7jwd61xnqz2b5ydsj7rnnikbgyicd5989h2")))

(define-public crate-cargo-platform-0.1.2 (c (n "cargo-platform") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)))) (h "09zsf76b9yr02jh17xq925xp1w824w2bwvb78fd0gpx5m1fq5nyb")))

(define-public crate-cargo-platform-0.1.3 (c (n "cargo-platform") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0ya4gw9xmk8w3yrzavwwfn9sm7vl2s426kqjw73pwx7a1bk2byic")))

(define-public crate-cargo-platform-0.1.4 (c (n "cargo-platform") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.171") (d #t) (k 0)))) (h "0dkdk4sf0brbd2zgqc6gmv4sh5aqbpv09hi9f5b6cxf98m34q0hj")))

(define-public crate-cargo-platform-0.1.5 (c (n "cargo-platform") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1zsr2jn1daih745hicgpxd8pdbgblj59whzv57wvshh12jrkfip3")))

(define-public crate-cargo-platform-0.1.6 (c (n "cargo-platform") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "0ga4qa3fx4bidnmix5gl8qclx2mma1a441swlpfsa645kpv8xvff") (r "1.70.0")))

(define-public crate-cargo-platform-0.1.7 (c (n "cargo-platform") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0gq5hzh08blsndjfpl4zg212817bnds7vh9xqkngl5mfy83qhk39") (r "1.70.0")))

(define-public crate-cargo-platform-0.1.8 (c (n "cargo-platform") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.194") (d #t) (k 0)))) (h "1z5b7ivbj508wkqdg2vb0hw4vi1k1pyhcn6h1h1b8svcb8vg1c94") (r "1.73")))

