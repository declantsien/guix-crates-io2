(define-module (crates-io ca rg cargo-profiler) #:use-module (crates-io))

(define-public crate-cargo-profiler-0.1.0 (c (n "cargo-profiler") (v "0.1.0") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "02v4xwi1rkqsrgszk70k5v29j05ymff8jfifnx4q968d9rqszhm5")))

(define-public crate-cargo-profiler-0.1.1 (c (n "cargo-profiler") (v "0.1.1") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "083pxdx94vxlx69y75m5ag22hppp1dcmvmdffghygkl9ayblmz7s")))

(define-public crate-cargo-profiler-0.1.2 (c (n "cargo-profiler") (v "0.1.2") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "08gvri96h79dbg6b9z40jrrfa7mh672z8fwwd089n3kgssbv2j4z")))

(define-public crate-cargo-profiler-0.1.3 (c (n "cargo-profiler") (v "0.1.3") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03a660wnhhq2bdkc560q3l84vspkrkpc84g4bdj4p33h408p8yzd")))

(define-public crate-cargo-profiler-0.1.4 (c (n "cargo-profiler") (v "0.1.4") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bac85b4sspqly1yfazhxfchxhk8jm5yqfrqlj383ggznkvvn4c0")))

(define-public crate-cargo-profiler-0.1.5 (c (n "cargo-profiler") (v "0.1.5") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19b44pyjpqvz8spwdr0waqnqam14p5axl4zl27yafv3jqkm6hkrz")))

(define-public crate-cargo-profiler-0.1.6 (c (n "cargo-profiler") (v "0.1.6") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jq7mvlsdp6nqwg5d0617shlw03xcw9q59fv0f6y40ahgg01789m")))

