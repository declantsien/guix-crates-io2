(define-module (crates-io ca rg cargo-casperlabs) #:use-module (crates-io))

(define-public crate-cargo-casperlabs-0.1.0 (c (n "cargo-casperlabs") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0lqldi7nmgwn88r9h1la9l6yglbrgm8nji38cc411vql773l44bn") (y #t)))

(define-public crate-cargo-casperlabs-0.2.0 (c (n "cargo-casperlabs") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "1j8bzvsbbfcbldqkdw81b8bb13w63d990c4jmd5ahf9p23vif3n4") (y #t)))

(define-public crate-cargo-casperlabs-0.3.0 (c (n "cargo-casperlabs") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "1z86na5pqqxpzq90d1m8k7whj7d9mgjg8xjr4kya9nwmc7iwgid2") (y #t)))

(define-public crate-cargo-casperlabs-0.4.0 (c (n "cargo-casperlabs") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "102vf83jlbvpyzf5s5bn7ih64s5i31n8783amahf23yh5xrczhbi") (y #t)))

(define-public crate-cargo-casperlabs-0.5.0 (c (n "cargo-casperlabs") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0a8gvl90hxvzx23n5y3mmx1qdywzmacmym20n7lkddc8cfb9x0nc") (y #t)))

(define-public crate-cargo-casperlabs-0.6.0 (c (n "cargo-casperlabs") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0cxz32fw5dx9hs9qm966fcjgjisxi3fdq0wp0xindmmnz1jwyn9v") (y #t)))

(define-public crate-cargo-casperlabs-0.7.0 (c (n "cargo-casperlabs") (v "0.7.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "06hp001dyylgkslmr91rasjd5vcfalc4j0g1q5w0sqjiqahxpfsd") (y #t)))

(define-public crate-cargo-casperlabs-0.7.1 (c (n "cargo-casperlabs") (v "0.7.1") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "1dzdi4cvzaajnjly8lhjm1mcpssa8plm0qyj0szyg6bscclcrz8q") (y #t)))

(define-public crate-cargo-casperlabs-0.7.2 (c (n "cargo-casperlabs") (v "0.7.2") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0yj98dxrcii0v1ydvn9n7wadkyxbf0dx8flpjpx5jyhnxl2v8qp3") (y #t)))

(define-public crate-cargo-casperlabs-0.7.3 (c (n "cargo-casperlabs") (v "0.7.3") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "1xa1s8c3dfq19w5610yvwgjrlsg834ndk4ib2d8gakx7z1pgwpz3") (y #t)))

(define-public crate-cargo-casperlabs-0.7.4 (c (n "cargo-casperlabs") (v "0.7.4") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "18n7abv5rizpsr239p0pjg32gfvgzzpgq573ycii9jp0iiwc0wca") (y #t)))

(define-public crate-cargo-casperlabs-0.8.0 (c (n "cargo-casperlabs") (v "0.8.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0cj3i6w09hn5nfb4fqq0rldxpvcgdd7dlnmkhj9fxnp1my1pcgz1") (y #t)))

(define-public crate-cargo-casperlabs-0.8.1 (c (n "cargo-casperlabs") (v "0.8.1") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0xz90b6gzh2aymccrpckxfcdy9ryslnbvy70wmvpm6mdh367izxw") (y #t)))

(define-public crate-cargo-casperlabs-0.9.0 (c (n "cargo-casperlabs") (v "0.9.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "1rwqc47q6asswa7n20qr0v125yim84mdbwnzlj37siywipa8sw0i") (y #t)))

(define-public crate-cargo-casperlabs-0.9.1 (c (n "cargo-casperlabs") (v "0.9.1") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0dxfqg3x1k105jqmij0mhc3632w1b4n3lz7ihm4xghbrfcqk9n2q") (y #t)))

