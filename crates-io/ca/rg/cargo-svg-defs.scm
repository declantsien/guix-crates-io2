(define-module (crates-io ca rg cargo-svg-defs) #:use-module (crates-io))

(define-public crate-cargo-svg-defs-0.1.0 (c (n "cargo-svg-defs") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "0zxnc19l9hhrnghblf6kmxkhx9gkkj353w8qz55w7ihfdrmiygaz")))

(define-public crate-cargo-svg-defs-0.1.1 (c (n "cargo-svg-defs") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1paf7nraazbgmqzl127j6r0f9xl12dkvigj39bi58a6fr34ab937")))

