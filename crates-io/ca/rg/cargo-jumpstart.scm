(define-module (crates-io ca rg cargo-jumpstart) #:use-module (crates-io))

(define-public crate-cargo-jumpstart-0.1.0 (c (n "cargo-jumpstart") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "19s0yy57dkjwqfmjvm4sj6i9xipax7bzx6sdx40h3m3jsjjkk4d5")))

(define-public crate-cargo-jumpstart-0.1.1 (c (n "cargo-jumpstart") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "195fz85rli0x27x4n5lif3pg4ni3smgs04m6rh6bvj97r1ixbwp5")))

