(define-module (crates-io ca rg cargo-difftests-core) #:use-module (crates-io))

(define-public crate-cargo-difftests-core-0.1.0-alpha.1 (c (n "cargo-difftests-core") (v "0.1.0-alpha.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "197hl37n13sj30b7lh76f9yjw7phi39m6g3j6p58y966mainmyq8")))

(define-public crate-cargo-difftests-core-0.1.0-alpha.2 (c (n "cargo-difftests-core") (v "0.1.0-alpha.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jmyzhn373i1ymkm6x3adm7hmq1gsbqsxprafsdyaqjh5zc5acv4")))

(define-public crate-cargo-difftests-core-0.1.0-alpha.3 (c (n "cargo-difftests-core") (v "0.1.0-alpha.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hig47hsavvbhin8dj9v63d8rpcawy13syq97z6q6l22fz7hzry3")))

(define-public crate-cargo-difftests-core-0.1.0 (c (n "cargo-difftests-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nrfyrww3ks1y028gpvm6b0afrz5vnyy64k21zqdf6zplzql3my4")))

(define-public crate-cargo-difftests-core-0.2.0 (c (n "cargo-difftests-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x60d3vvp22mjy98nk0g12461jfxx14n1im5ixjdcvszj7awvgsw")))

(define-public crate-cargo-difftests-core-0.3.0 (c (n "cargo-difftests-core") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bvzzqmwnsgrhk98f9vf1qigs6pgkdj9mw1rcj7hn1swc5190h3i")))

(define-public crate-cargo-difftests-core-0.4.0 (c (n "cargo-difftests-core") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lw2g5i09ph81gsk2hpg7ypnpylslgqkpiagkx259zigj2w8042f")))

(define-public crate-cargo-difftests-core-0.5.0 (c (n "cargo-difftests-core") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q0ryf067jgxc06xh746k7lnyhggcxfk6dqjxiln1fl5a4dbq1g5") (f (quote (("groups") ("default"))))))

(define-public crate-cargo-difftests-core-0.6.0 (c (n "cargo-difftests-core") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13ym0hrlsl82mbl86zdfq93ai81a0cqnw3bngk7hh1qpism45zq6")))

(define-public crate-cargo-difftests-core-0.6.1 (c (n "cargo-difftests-core") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1imvwbcyvv8vb9wa6df5qsixlf52kfp1sbxyhlscg75lnyiz9l1i")))

