(define-module (crates-io ca rg cargo-neko) #:use-module (crates-io))

(define-public crate-cargo-neko-0.1.0 (c (n "cargo-neko") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "viuer") (r "^0.4.0") (d #t) (k 0)))) (h "0vrf4d4cbjhr84vjz05c06m828qhrqkk57k3v2wgmw1n12knb7i6")))

