(define-module (crates-io ca rg cargo-book) #:use-module (crates-io))

(define-public crate-cargo-book-0.1.2 (c (n "cargo-book") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-staticfile") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ml34qjw4xipzkkcn8p8b07m6l556h4plh14w5cr2gp62p00mkmf")))

(define-public crate-cargo-book-0.1.3 (c (n "cargo-book") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-staticfile") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xbls7nh8mdkrxyirc74y7y0gmn30skjpg1l5xf3xiqzfpl9wm74")))

(define-public crate-cargo-book-0.1.4 (c (n "cargo-book") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-staticfile") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rpzw067gfmnb92gnrbc5dshmn8ap1wpz0lbqidfsaphapbv6z3y")))

