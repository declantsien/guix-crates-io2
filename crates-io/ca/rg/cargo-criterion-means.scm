(define-module (crates-io ca rg cargo-criterion-means) #:use-module (crates-io))

(define-public crate-cargo-criterion-means-0.1.0-beta.1 (c (n "cargo-criterion-means") (v "0.1.0-beta.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0i56d38r89zgh2kqch6f8g4zns7ggnsvgc93if083scqlqzrwxj8")))

(define-public crate-cargo-criterion-means-0.1.0-beta.2 (c (n "cargo-criterion-means") (v "0.1.0-beta.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1macg2202dw2vmv9jw8mgz9h66wcpvg1a7qmfqfw5v7dvnls7f7l")))

(define-public crate-cargo-criterion-means-0.1.0 (c (n "cargo-criterion-means") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "178kl1i3jvr13bz7zw7x7n8brrn92g6dfc7jgkjb041c4sbpahbc")))

