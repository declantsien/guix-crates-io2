(define-module (crates-io ca rg cargo_issue_6504) #:use-module (crates-io))

(define-public crate-cargo_issue_6504-1.1.1 (c (n "cargo_issue_6504") (v "1.1.1") (h "0gwc54r1mgr7m9f8rb6kgwc0aibbnz9y4ahkk4lx17x1w1188hbx")))

(define-public crate-cargo_issue_6504-1.1.1+1 (c (n "cargo_issue_6504") (v "1.1.1+1") (h "1c8iks3nb9pfchrd7hkmy9bl3aw0anbrbrln0p6v03img406vqg6")))

(define-public crate-cargo_issue_6504-2.2.2 (c (n "cargo_issue_6504") (v "2.2.2") (h "1xn51z0czh7vmndgg5as7i396p65hxdxkvb4ysnzdvygqba3cakq") (y #t)))

(define-public crate-cargo_issue_6504-2.2.2+1 (c (n "cargo_issue_6504") (v "2.2.2+1") (h "0zh86gv07inddjxgw26q9z1crfkq96bckizbsssvyq2w1m9i7dl6")))

