(define-module (crates-io ca rg cargo-firstpage) #:use-module (crates-io))

(define-public crate-cargo-firstpage-0.1.0 (c (n "cargo-firstpage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0j3c714jm58ghkc527i4kdn3n4ryhf2sixyb37ign5cpl56jrk07")))

(define-public crate-cargo-firstpage-0.1.1 (c (n "cargo-firstpage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0pv8zmi089r56b8w89vxaslvf93mqc9kp4bnkb80wi2nzfcrsb7x")))

(define-public crate-cargo-firstpage-0.1.2 (c (n "cargo-firstpage") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0h3ii1wq5hkwhkbnkacxm9h0qvs32rr8alq0f1jl4s8pr6p90zrh")))

