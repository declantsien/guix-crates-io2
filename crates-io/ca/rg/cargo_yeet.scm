(define-module (crates-io ca rg cargo_yeet) #:use-module (crates-io))

(define-public crate-cargo_yeet-0.1.0 (c (n "cargo_yeet") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)))) (h "0dv9vvivv7pqk7ph0n97wxc2ll6ll3qb7la556pm965q275zxn6w")))

(define-public crate-cargo_yeet-0.1.1 (c (n "cargo_yeet") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)))) (h "09f0n631va4ajahvicpbab7a0h8ak7lhv79b2aq6crz3afjlhhps")))

(define-public crate-cargo_yeet-0.1.2 (c (n "cargo_yeet") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)))) (h "1fd066pmm1awjr9ahfya2h4sq18mfpzdqwijmk53g0k02n765ks0")))

(define-public crate-cargo_yeet-0.0.0 (c (n "cargo_yeet") (v "0.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)))) (h "1qpky0s3mrs26p813h5c402qdn7mlrabp2s031i8gviyrkh6zmv8")))

(define-public crate-cargo_yeet-0.1.3 (c (n "cargo_yeet") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)))) (h "10787q9w70f2vc0dj8g9jfb5qz9fdbmmjahfh2rzccyclr7dyk0x")))

