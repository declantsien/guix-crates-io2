(define-module (crates-io ca rg cargo-self) #:use-module (crates-io))

(define-public crate-cargo-self-0.1.0 (c (n "cargo-self") (v "0.1.0") (h "1glsapyi3xkvk0797rpvaynl69zx54s4j9asvqy56iyy422lld82")))

(define-public crate-cargo-self-0.1.1 (c (n "cargo-self") (v "0.1.1") (d (list (d (n "cargo") (r "^0.73.1") (d #t) (k 0)))) (h "0aspay8hhkbz6dwiilprfyrg504h3axlsgc37i3zlhwjdp2n10cc")))

(define-public crate-cargo-self-0.1.2 (c (n "cargo-self") (v "0.1.2") (d (list (d (n "cargo") (r "^0.73.1") (d #t) (k 0)))) (h "0ha0krm253y4z1j9ilmzcsf5m0wn3g5wahrba7ja5gsm4yypjlc9")))

(define-public crate-cargo-self-0.1.3 (c (n "cargo-self") (v "0.1.3") (d (list (d (n "async-openai") (r "^0.14.3") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxvw3aviggfjmkisa0ns6k593f92zr8qxh55c03ibxkzkj6dch1")))

(define-public crate-cargo-self-0.1.4 (c (n "cargo-self") (v "0.1.4") (d (list (d (n "async-openai") (r "^0.14.3") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0dr3jwaarq3xv72an04lbwngykw67j29xx8jhinwwc8bv9k5aijx")))

(define-public crate-cargo-self-0.1.5 (c (n "cargo-self") (v "0.1.5") (d (list (d (n "async-openai") (r "^0.14.3") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0hn4vy355khzj86g7plvvp93m70s0x0argzzi6k3xi43ck3a1f39")))

(define-public crate-cargo-self-0.1.6 (c (n "cargo-self") (v "0.1.6") (d (list (d (n "async-openai") (r "^0.14.3") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.4.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "polodb_core") (r "^4.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0zz33vz0p23qkwd4zbrbrnbx2s8jv0z7akn77zarjap227sb8kdi")))

(define-public crate-cargo-self-0.1.7 (c (n "cargo-self") (v "0.1.7") (d (list (d (n "async-openai") (r "^0.14.3") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.4.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "polodb_core") (r "^4.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "09a948grba652w5n8f6ac0wqwa5h6c4fcnkv3wfq2q9r1y1934m0")))

