(define-module (crates-io ca rg cargo-gba) #:use-module (crates-io))

(define-public crate-cargo-gba-0.0.0 (c (n "cargo-gba") (v "0.0.0") (h "1imgf3n3f3l2rcj61jj7qj6sq05mxq97x1v18lz16zvhh5rqixhc")))

(define-public crate-cargo-gba-0.0.1 (c (n "cargo-gba") (v "0.0.1") (h "03m0n7z2k84q72sj25zfglygz13m810xmzygwvm2hmhqd7amxrn3")))

(define-public crate-cargo-gba-0.0.2 (c (n "cargo-gba") (v "0.0.2") (h "15jq2jsyjh6za1igpwacbc8z0qy1djgsf6h7qnb02xc0p1fi46ir")))

(define-public crate-cargo-gba-0.0.3 (c (n "cargo-gba") (v "0.0.3") (h "0xcb82yr0bgizpy2jkqglhqawfnixsa30k5919z1464sbra83dnf")))

(define-public crate-cargo-gba-0.0.4 (c (n "cargo-gba") (v "0.0.4") (h "01qmjr4z6q8kz7zyc1z62vcipkhgll15vizhb0ljf10qr9bajcas")))

