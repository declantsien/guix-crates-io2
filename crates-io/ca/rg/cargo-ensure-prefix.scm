(define-module (crates-io ca rg cargo-ensure-prefix) #:use-module (crates-io))

(define-public crate-cargo-ensure-prefix-0.1.0 (c (n "cargo-ensure-prefix") (v "0.1.0") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0b30j02p13fd3p9nwik8pl67asmi6yd441iy57bn29pr1np0pzn0") (y #t)))

(define-public crate-cargo-ensure-prefix-0.1.1 (c (n "cargo-ensure-prefix") (v "0.1.1") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1xsajb432347d8kcdl8qdvp1dvlrxg2l15slyhwcxkq6k52ilhqb")))

(define-public crate-cargo-ensure-prefix-0.1.3 (c (n "cargo-ensure-prefix") (v "0.1.3") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1qdfchid52ign6kz0rvsrdxgnjv28xqfy2628ncgs59kv69qwshl")))

(define-public crate-cargo-ensure-prefix-0.1.4 (c (n "cargo-ensure-prefix") (v "0.1.4") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09drwvm987w37kymg4fcbvb2q3wh0711n61znd4k58n5wbb5krky")))

(define-public crate-cargo-ensure-prefix-0.1.5 (c (n "cargo-ensure-prefix") (v "0.1.5") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "quote") (r "= 1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15fr2kcnhydkjlgrqwyxcdlk7hrrdyhyxarld69mwpqvh9bm5yqg") (y #t)))

(define-public crate-cargo-ensure-prefix-0.1.6 (c (n "cargo-ensure-prefix") (v "0.1.6") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p7g7ashqrcjhm3mii425yrj98rl0d7g6zrpdy52sfjvzgxxfg4r")))

(define-public crate-cargo-ensure-prefix-0.1.7 (c (n "cargo-ensure-prefix") (v "0.1.7") (d (list (d (n "cargo") (r "^0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11b8xjm6lqnx2zywwc0dbc1s1lmiz1s9lwawl85px59j65r86nbf")))

