(define-module (crates-io ca rg cargo-cross-test) #:use-module (crates-io))

(define-public crate-cargo-cross-test-0.1.0 (c (n "cargo-cross-test") (v "0.1.0") (d (list (d (n "cross-test") (r "^0.1.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "12qaks79skdxah7mvsf87psvfxfpr8lzmkdhqz2gmfswkf7d5a1g")))

(define-public crate-cargo-cross-test-0.1.1 (c (n "cargo-cross-test") (v "0.1.1") (d (list (d (n "cross-test") (r "^0.1.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "15l5x1lq649zbbn4jgdjbwxw7rsac1n68p6cvrs83sx6qii13nzk")))

