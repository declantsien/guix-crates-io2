(define-module (crates-io ca rg cargo-make-rpm) #:use-module (crates-io))

(define-public crate-cargo-make-rpm-1.0.0 (c (n "cargo-make-rpm") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rpm") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0466wdy78a3hf9m24rxbjyx32mngh2ci041bjim7qy2nvlh9ndiz")))

(define-public crate-cargo-make-rpm-1.0.1 (c (n "cargo-make-rpm") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rpm") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1i5njwx5l2sfn7qb9608hi8wd6jhwrkhkqvs3l2n5w3w0syrami0")))

