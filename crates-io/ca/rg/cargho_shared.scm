(define-module (crates-io ca rg cargho_shared) #:use-module (crates-io))

(define-public crate-cargho_shared-0.1.5 (c (n "cargho_shared") (v "0.1.5") (h "0b4ns1lgwrnj6rykfhfarzwzixb5x00lkjkarmbg0dvamfcxzbsy")))

(define-public crate-cargho_shared-0.1.6 (c (n "cargho_shared") (v "0.1.6") (h "1qlvpzy690x8mbi1lx80k46ykgj46fwmi6wzj0xfarn59a9dphwh")))

