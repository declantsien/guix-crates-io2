(define-module (crates-io ca rg cargo-aragonite) #:use-module (crates-io))

(define-public crate-cargo-aragonite-0.1.0 (c (n "cargo-aragonite") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "elf") (r "^0.7.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1a3rirha3fmhy5i3b805xv6m22k0lf96xissx59krkyw3ga0w057")))

