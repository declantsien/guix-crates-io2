(define-module (crates-io ca rg cargo-cargo-derive-ipc-schema-for-swift) #:use-module (crates-io))

(define-public crate-cargo-cargo-derive-ipc-schema-for-swift-0.1.0 (c (n "cargo-cargo-derive-ipc-schema-for-swift") (v "0.1.0") (d (list (d (n "syntex_errors") (r "^0.59.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1rdp29x8241fzkgajrkpsm7v1jllznlw2gjdh3gl54ac2b3g15ih")))

