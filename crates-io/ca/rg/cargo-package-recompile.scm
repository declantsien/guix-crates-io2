(define-module (crates-io ca rg cargo-package-recompile) #:use-module (crates-io))

(define-public crate-cargo-package-recompile-0.1.0 (c (n "cargo-package-recompile") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vjpsbij5kyjzyaxghrbgamxn9yyg700ichil88hq2ng97j4djn2")))

(define-public crate-cargo-package-recompile-0.1.1 (c (n "cargo-package-recompile") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gcg9pbswbq2s1glbkz3fmhc1panc5hjbsmi6v09m7rlic2356c2")))

