(define-module (crates-io ca rg cargo2) #:use-module (crates-io))

(define-public crate-cargo2-0.1.0 (c (n "cargo2") (v "0.1.0") (h "05g24c51i4xwzqdw40h2zm6nhgq3r5zaqgnpv71k5s5djim07dai")))

(define-public crate-cargo2-0.2.0 (c (n "cargo2") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1gn304f1w0iqppzll3lkxjiq7l2z9p5znk8n0j3b885h81ali73d")))

(define-public crate-cargo2-0.2.1 (c (n "cargo2") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "11w0nyvy29i1h0nam2kki2jpw9vbmx05y4xqnfb3idqfx9kwhsyl")))

(define-public crate-cargo2-0.2.2 (c (n "cargo2") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "10mam5yf891d745c6mxpmg8mpzpzxfz41y2iplnpqjfhhf20gfqn")))

