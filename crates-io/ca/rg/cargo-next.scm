(define-module (crates-io ca rg cargo-next) #:use-module (crates-io))

(define-public crate-cargo-next-0.0.0 (c (n "cargo-next") (v "0.0.0") (h "0qv8sc87fyqp7zp1dyy7684rcglsx4vqmm3dy3alldkm9n3a1a71")))

(define-public crate-cargo-next-0.1.1 (c (n "cargo-next") (v "0.1.1") (d (list (d (n "pico-args") (r "^0.3.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1virhmswxw23yf6lqjjpjs9p4xz20mkcsp1p0klix0ji2j8b5lpf")))

(define-public crate-cargo-next-0.1.2 (c (n "cargo-next") (v "0.1.2") (d (list (d (n "pico-args") (r "^0.3.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0ck7w2q79b667k3qp85jvrnx8dgj7iwgdq16kridmndr1x8pig68")))

(define-public crate-cargo-next-0.1.3 (c (n "cargo-next") (v "0.1.3") (d (list (d (n "pico-args") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0c5qz6pph8iyi2ac28hqnyf2bcximm0yijs4wyr7qjfgqvmlf8sd")))

(define-public crate-cargo-next-0.1.4 (c (n "cargo-next") (v "0.1.4") (d (list (d (n "pico-args") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "05m09w56spi6w8amqndy4ksvna1lvij27is86qigr2yh9809d74v")))

(define-public crate-cargo-next-0.2.0 (c (n "cargo-next") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0fbv9k5v8cw9ch18l66ilxff5rylibwiwdk35jvh4df67rf6kdfn")))

(define-public crate-cargo-next-0.2.1 (c (n "cargo-next") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0p8y3d7fw576a358id6qdmlr8nn2byc97bhz9nc1rf3jf10wilmb")))

(define-public crate-cargo-next-0.2.2 (c (n "cargo-next") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0nx3pi0zr2v45lva3llais31afp9all5wgfbq350f1lh46aadg49")))

(define-public crate-cargo-next-0.2.3 (c (n "cargo-next") (v "0.2.3") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "0bw9dvz9rfl8jnyvi4d5y4bhbmcyvsabq7rjm9964aq42j40zcwa")))

