(define-module (crates-io ca rg cargo-test) #:use-module (crates-io))

(define-public crate-cargo-test-0.1.0 (c (n "cargo-test") (v "0.1.0") (h "151ggbbnjqgjqvp8b1d534ph5psypnk93zl63jxzswga02byjwk1")))

(define-public crate-cargo-test-0.1.1 (c (n "cargo-test") (v "0.1.1") (h "1wbqf8mb96aw99bv7irkm1rzsyy82mvnrq72fy7k7znhzlmnamhs")))

(define-public crate-cargo-test-0.2.0 (c (n "cargo-test") (v "0.2.0") (h "1dh74m6xvwns5vihfigvp3msywwrg0xyc89dbd6zk0hrll5k0p3a")))

(define-public crate-cargo-test-0.2.1 (c (n "cargo-test") (v "0.2.1") (h "129h7l4fgbbiy6d8bh3sgw4802xc9i8x25b3g8jqa6wkwq3jmb06")))

(define-public crate-cargo-test-0.3.0 (c (n "cargo-test") (v "0.3.0") (h "0k0d39rsgj3h97sjy838qwvrzs7am064l35dwcmapqi496sr6a6v")))

(define-public crate-cargo-test-0.3.1 (c (n "cargo-test") (v "0.3.1") (h "00dpnqlqy64q0hpjxx4i5l4n2iqm786sck3nmyidisv1dwwnj3lq")))

