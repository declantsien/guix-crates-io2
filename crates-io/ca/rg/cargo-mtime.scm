(define-module (crates-io ca rg cargo-mtime) #:use-module (crates-io))

(define-public crate-cargo-mtime-0.1.0 (c (n "cargo-mtime") (v "0.1.0") (d (list (d (n "async-walkdir") (r "^1.0.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "fs" "io-std" "io-util" "sync" "macros" "signal"))) (d #t) (k 0)))) (h "0z0bqijkd9bknf8d5wpz970v5v58lbxfk5ls0b9arfx25crr5b3h")))

(define-public crate-cargo-mtime-0.1.1 (c (n "cargo-mtime") (v "0.1.1") (d (list (d (n "async-walkdir") (r "^1.0.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "fs" "io-std" "io-util" "sync" "macros" "signal"))) (d #t) (k 0)))) (h "1wifq5i6qq7h8yaldh0ih8s48j55g81vp9n9j2a4059k5v5xrw12")))

