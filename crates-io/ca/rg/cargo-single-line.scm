(define-module (crates-io ca rg cargo-single-line) #:use-module (crates-io))

(define-public crate-cargo-single-line-0.1.0 (c (n "cargo-single-line") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0jqzy18l45zw93y0jwhb62kcl6m66zd5qk0ffhwmcd031mkddv4p")))

(define-public crate-cargo-single-line-0.1.1 (c (n "cargo-single-line") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0a37wg6s11np0gk3c1ii6h4qx7v6nyzg2bqfh19zvjqfrjc1sx96")))

(define-public crate-cargo-single-line-0.1.2 (c (n "cargo-single-line") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1wwbisikxhq3m5di9nc2fq8jkbfr3zz610fh0z57k226y9w951q2")))

(define-public crate-cargo-single-line-0.1.3 (c (n "cargo-single-line") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "039zqx0lakqbykpl9j5v6jhpzchp90i0mr76w10ai46sli84rq5l")))

(define-public crate-cargo-single-line-0.1.4 (c (n "cargo-single-line") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0syjsp71bpvjdpmxrg49x0xisw2k949a8bwhjmi29c3siykp1jz4")))

(define-public crate-cargo-single-line-0.1.6 (c (n "cargo-single-line") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07ir4qsgkxby7kml5zq938kcacf97i1pddk0hid7k28swb3gmlx4")))

