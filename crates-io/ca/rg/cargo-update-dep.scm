(define-module (crates-io ca rg cargo-update-dep) #:use-module (crates-io))

(define-public crate-cargo-update-dep-0.1.0 (c (n "cargo-update-dep") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1kq1gcnfs27mc5s50d855k4qkwwhxd0dxlnis19z26j220dypw7w")))

(define-public crate-cargo-update-dep-1.0.0 (c (n "cargo-update-dep") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1zmah560g7imv2l2xi6r93izv5rb2b6y9p3jm9xds70nyxgwsh5x")))

