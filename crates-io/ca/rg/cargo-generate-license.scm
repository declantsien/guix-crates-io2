(define-module (crates-io ca rg cargo-generate-license) #:use-module (crates-io))

(define-public crate-cargo-generate-license-1.0.0 (c (n "cargo-generate-license") (v "1.0.0") (d (list (d (n "cargo-generate-license_impl-license") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0981fgg45fqsbwgf4v6drj86lhnl62kbsr6pvw54izwa6drhr4hx")))

