(define-module (crates-io ca rg cargo-libyear) #:use-module (crates-io))

(define-public crate-cargo-libyear-0.1.0 (c (n "cargo-libyear") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.11.0") (f (quote ("rustls"))) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "tqdm") (r "^0.7.0") (d #t) (k 0)))) (h "0xh80r3k70x80v3jx3nc4bmw019vfkqi00sf9xki5nrvp4jzjw4s")))

