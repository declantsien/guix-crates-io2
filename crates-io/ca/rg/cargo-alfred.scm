(define-module (crates-io ca rg cargo-alfred) #:use-module (crates-io))

(define-public crate-cargo-alfred-0.0.1 (c (n "cargo-alfred") (v "0.0.1") (d (list (d (n "handlebars") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "13b0vr89q6jlcdhkmlkinx2wxs4i9ch1jdljhxczcgh7pplpsgkg")))

(define-public crate-cargo-alfred-0.0.2 (c (n "cargo-alfred") (v "0.0.2") (d (list (d (n "handlebars") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "1p5642p7w5j66qcwsp7grv3i77qd8kjfxnvw58n3fjh3g882nmm4")))

