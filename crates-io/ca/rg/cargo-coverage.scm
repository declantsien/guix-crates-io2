(define-module (crates-io ca rg cargo-coverage) #:use-module (crates-io))

(define-public crate-cargo-coverage-0.1.0 (c (n "cargo-coverage") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "04aznf4w3jvcplalyfznqrrlc5086mimk1gxlf2p1a2lw7x22kvi")))

(define-public crate-cargo-coverage-0.1.1 (c (n "cargo-coverage") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "05wg23jjc3h8h218bmbsyznwbyq3r5ahax49mb60alsx36lwzxba")))

