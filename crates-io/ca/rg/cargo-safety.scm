(define-module (crates-io ca rg cargo-safety) #:use-module (crates-io))

(define-public crate-cargo-safety-0.1.0 (c (n "cargo-safety") (v "0.1.0") (d (list (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syntex") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (d #t) (k 0)))) (h "0wpx5cgrynfsggwp4fchfy0mbii1mywapcshma8giq5mnabicmzg")))

(define-public crate-cargo-safety-0.1.1 (c (n "cargo-safety") (v "0.1.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syntex") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (d #t) (k 0)))) (h "070b4zh11xy6ymgasf3dqppyb8g6j993ibh3pbw05c3pn708nlsr")))

