(define-module (crates-io ca rg cargo-urlcrate) #:use-module (crates-io))

(define-public crate-cargo-urlcrate-1.0.1 (c (n "cargo-urlcrate") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0c3pnbw82kmav4mkpgwx8d5mrmvpy8xg38rrkl5xwrcip4ayhpig") (f (quote (("default"))))))

