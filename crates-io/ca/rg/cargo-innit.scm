(define-module (crates-io ca rg cargo-innit) #:use-module (crates-io))

(define-public crate-cargo-innit-0.1.0 (c (n "cargo-innit") (v "0.1.0") (h "0wjfx4h0bs9z2v5yzqy09bq2wardnq7ljwy6pcw4n25wn59lp7hd")))

(define-public crate-cargo-innit-1.0.0 (c (n "cargo-innit") (v "1.0.0") (h "1zhbkq01bjr36by5pzmvc824bvxpb9alrlp5278q3m90zqn5dmlf")))

