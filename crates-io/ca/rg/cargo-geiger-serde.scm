(define-module (crates-io ca rg cargo-geiger-serde) #:use-module (crates-io))

(define-public crate-cargo-geiger-serde-0.1.0 (c (n "cargo-geiger-serde") (v "0.1.0") (h "1zf0svy607m1425a9rfrmx4sajhddxl9n5kclmnszxg6x8v2ssl1")))

(define-public crate-cargo-geiger-serde-0.2.0 (c (n "cargo-geiger-serde") (v "0.2.0") (d (list (d (n "semver") (r "^0.11.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0kj3zdf2143j576n60i8ghxfkv9xr521x3rjvr8wx1vxj3hqwc04")))

(define-public crate-cargo-geiger-serde-0.2.1 (c (n "cargo-geiger-serde") (v "0.2.1") (d (list (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jsqdy451yimd7gwn79qhd5qlsksw9c9gwcdslz2kd1q4na7pi2c")))

(define-public crate-cargo-geiger-serde-0.2.2 (c (n "cargo-geiger-serde") (v "0.2.2") (d (list (d (n "semver") (r "^1.0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1m5qrh1idr2kdzadj7ncrkv8ipgjlimhw4yma53hvdixx03qi4gj")))

(define-public crate-cargo-geiger-serde-0.2.3 (c (n "cargo-geiger-serde") (v "0.2.3") (d (list (d (n "semver") (r "^1.0.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "05rarxqjaalrl85pjdd30yjcziscrr46nj2j21mknh8ayhjz9z33")))

(define-public crate-cargo-geiger-serde-0.2.4 (c (n "cargo-geiger-serde") (v "0.2.4") (d (list (d (n "semver") (r "^1.0.16") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1d8wl8abzassv1c3pg0bc6bgvkn4qkw28g7vvc0jgn5f2ygpiyf1")))

(define-public crate-cargo-geiger-serde-0.2.5 (c (n "cargo-geiger-serde") (v "0.2.5") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ffbp0z85rzbcz653kwdg5hajj9v0a9mmpcjpl61ffvk48q63ppb")))

