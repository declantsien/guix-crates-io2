(define-module (crates-io ca rg cargo-wrflib) #:use-module (crates-io))

(define-public crate-cargo-wrflib-0.0.1 (c (n "cargo-wrflib") (v "0.0.1") (h "0aw5kkgfx0i102l2m40a606bvcxscwhdj8dnr5pfd0y8d9wq393f")))

(define-public crate-cargo-wrflib-0.0.2 (c (n "cargo-wrflib") (v "0.0.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11vg9lc874s5j1v2ra9shch0h95viz6b3jm9kaiglyxdlc2ysjkp")))

(define-public crate-cargo-wrflib-0.0.3 (c (n "cargo-wrflib") (v "0.0.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m8k778haa6kah6gcvhqr08qanfb4r3arvmr99766kxvafhbcn46")))

(define-public crate-cargo-wrflib-0.0.5 (c (n "cargo-wrflib") (v "0.0.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01iyb18q4g8g4sp0rbxpswyp1hrm2qqnxafqhhixqkzw8q3svzgi")))

(define-public crate-cargo-wrflib-0.0.6 (c (n "cargo-wrflib") (v "0.0.6") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1myh1ilddk8iz42b74gcpp88rh15jmbaixps5skhgykhip3ansyb")))

(define-public crate-cargo-wrflib-0.0.7 (c (n "cargo-wrflib") (v "0.0.7") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19m57rn2f5w8wvlqv7cx62w71bsdc2j7vcja3b3xd1rgpc0v0s5s")))

