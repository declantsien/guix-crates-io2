(define-module (crates-io ca rg cargo-fill) #:use-module (crates-io))

(define-public crate-cargo-fill-0.1.0 (c (n "cargo-fill") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.16") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "promptly") (r "^0.3") (d #t) (k 0)) (d (n "smallstr") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "19r63vgr5y9dnrjb4w3qwl0750ka5ckgqanlgfmw7cn05jidvk46")))

(define-public crate-cargo-fill-0.2.0 (c (n "cargo-fill") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.16") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "promptly") (r "^0.3") (d #t) (k 0)) (d (n "smallstr") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "17k68vx6da50v5y1bccyppk6q8hkm42yb818w9fg5sjjk1gsn8gg")))

