(define-module (crates-io ca rg cargo-all-features) #:use-module (crates-io))

(define-public crate-cargo-all-features-1.1.0 (c (n "cargo-all-features") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1017ff2mwydb0gxdnm4jfnckm6m21mdw8is79iiihcj8f85bf21z")))

(define-public crate-cargo-all-features-1.1.1 (c (n "cargo-all-features") (v "1.1.1") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0358vw3zcxc5hxad5c0vk17ggk8n38f87sgm53k67lqvj7h3prw2")))

(define-public crate-cargo-all-features-1.2.0 (c (n "cargo-all-features") (v "1.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "13g1zpj5vc4nnaibr57lv3ipnn4fnji3vakbancv0qh6ndxx6ccp")))

(define-public crate-cargo-all-features-1.2.1 (c (n "cargo-all-features") (v "1.2.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1b9zvw7n1wvricy1xn01zq0icdidzcdci82n1w4ag4v9ks3cncx3")))

(define-public crate-cargo-all-features-1.3.0 (c (n "cargo-all-features") (v "1.3.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "00316i763wpgyvxz62i3372h63n65b9b8837qg1ihzak9aidvapb")))

(define-public crate-cargo-all-features-1.4.0 (c (n "cargo-all-features") (v "1.4.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0r46ql6v3yyj7r1lc48wybw3m09i4ax0a94639a14431pa4yycq3")))

(define-public crate-cargo-all-features-1.5.0 (c (n "cargo-all-features") (v "1.5.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1g78d9iby985c435fhsky9dhv76vpg1wqcy4jw5g0m0hsz1yvlbm")))

(define-public crate-cargo-all-features-1.6.0 (c (n "cargo-all-features") (v "1.6.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "15xw4islb92422v37pg266nbg57a7666z7vz7hsiss7mikdn4835")))

(define-public crate-cargo-all-features-1.7.0 (c (n "cargo-all-features") (v "1.7.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0nz7ki6bw9n3iyyw55m3gw3w1z6z83qzznsic60ammgdyfr3yzki")))

(define-public crate-cargo-all-features-1.8.0 (c (n "cargo-all-features") (v "1.8.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0n1bi7lgq7r92fsxysa285a34sj6vilnxdv0cbsrr3y65wwry5p8")))

(define-public crate-cargo-all-features-1.9.0 (c (n "cargo-all-features") (v "1.9.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0cr5iz8zqq1ysvz6a44qh4d736sb6j8n189f5q81sch2kyij3kyv")))

(define-public crate-cargo-all-features-1.10.0 (c (n "cargo-all-features") (v "1.10.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "13ysjag02bkqgd56wn5yilzz6gai4s3la77smh6hy68jm9fxgxza")))

