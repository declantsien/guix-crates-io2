(define-module (crates-io ca rg cargo-fuzzcheck) #:use-module (crates-io))

(define-public crate-cargo-fuzzcheck-0.1.0 (c (n "cargo-fuzzcheck") (v "0.1.0") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1042rvfraym2igjnx635qbxlzykx7n3bmvfdildch997vrl57xri") (y #t)))

(define-public crate-cargo-fuzzcheck-0.1.1 (c (n "cargo-fuzzcheck") (v "0.1.1") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "14gvxgpabffdc5jxqwl023kldj74anlfviri37vbff5z4nbpq8a1")))

(define-public crate-cargo-fuzzcheck-0.2.0 (c (n "cargo-fuzzcheck") (v "0.2.0") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1braxnvwx1irdh7r9dk9f0rdvdvsqn3cm486zjzy8d6257z331nv") (y #t)))

(define-public crate-cargo-fuzzcheck-0.2.1 (c (n "cargo-fuzzcheck") (v "0.2.1") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0c2x3x1rd38cijfc6nw5bbzzmz0cxl3pkfzp6ybmg83gqs7yd45g")))

(define-public crate-cargo-fuzzcheck-0.3.0 (c (n "cargo-fuzzcheck") (v "0.3.0") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "03z532fdgqlg39xb7wz7mkw5d3cpkhd63dmqcn9ha04kn5dq0wvl")))

(define-public crate-cargo-fuzzcheck-0.4.0 (c (n "cargo-fuzzcheck") (v "0.4.0") (d (list (d (n "fuzzcheck_arg_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1xmqaqm1sva2hppwy9ml5p3lcbzbcr5233aywf95z6cwz8yrzr7n")))

(define-public crate-cargo-fuzzcheck-0.5.0 (c (n "cargo-fuzzcheck") (v "0.5.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "decent-toml-rs-alternative") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzcheck_common") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19krdlr2zbkzckg03l52xnflkw1h9kyi43dvx3pvm8n75isgbp4c") (f (quote (("ui" "tui" "termion" "unicode-width" "json" "decent-serde-json-alternative" "fuzzcheck_common/ui"))))))

(define-public crate-cargo-fuzzcheck-0.6.0 (c (n "cargo-fuzzcheck") (v "0.6.0") (d (list (d (n "decent-serde-json-alternative") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "decent-toml-rs-alternative") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzcheck_common") (r "^0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "04qjp5s83rp0zi62cjdqprjfb6x6hk8l9lv22n8b7d6l8a12vhnc") (f (quote (("ui" "tui" "termion" "unicode-width" "json" "decent-serde-json-alternative" "fuzzcheck_common/ui"))))))

(define-public crate-cargo-fuzzcheck-0.7.0 (c (n "cargo-fuzzcheck") (v "0.7.0") (d (list (d (n "fuzzcheck_common") (r "^0.7.0") (d #t) (k 0)))) (h "1pdprbfmbw2rj4mr7h48qrxm6mii14jnzmkfgr0ashganpn6ryjm")))

(define-public crate-cargo-fuzzcheck-0.7.1 (c (n "cargo-fuzzcheck") (v "0.7.1") (d (list (d (n "fuzzcheck_common") (r "^0.7.0") (d #t) (k 0)))) (h "1qgyjs16bp5yq1in4xal8ydkas83nz041hpzsb1vkmx3cc9jla7n")))

(define-public crate-cargo-fuzzcheck-0.8.0 (c (n "cargo-fuzzcheck") (v "0.8.0") (d (list (d (n "fuzzcheck_common") (r "^0.8.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "09sw8kylfij9yk2nwf1g34isj933r3qxx6fxpfxzf8x84bldgmxp")))

(define-public crate-cargo-fuzzcheck-0.9.0 (c (n "cargo-fuzzcheck") (v "0.9.0") (d (list (d (n "fuzzcheck_common") (r "^0.9.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "06j8kqllzshmqaj7v9brlky6sqj5wnb69m05iin04m88angnsqfv")))

(define-public crate-cargo-fuzzcheck-0.10.0 (c (n "cargo-fuzzcheck") (v "0.10.0") (d (list (d (n "fuzzcheck_common") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1x1f6kkc22xzabk0a4phqsh1c6ar1nyh4wvrs345m63fc0amyw6k")))

(define-public crate-cargo-fuzzcheck-0.11.0 (c (n "cargo-fuzzcheck") (v "0.11.0") (d (list (d (n "fuzzcheck_common") (r "^0.11") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0d6cxz8pn6kagxqlsb0xl58chq9fk834rcz70hbmyx0wyq08h5ih")))

(define-public crate-cargo-fuzzcheck-0.12.0 (c (n "cargo-fuzzcheck") (v "0.12.0") (d (list (d (n "fuzzcheck_common") (r "^0.12") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0aihhmj8wbssllg5fxpqky51w8qhrb9k195ndp1bz28sj017pd88")))

