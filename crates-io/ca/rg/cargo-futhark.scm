(define-module (crates-io ca rg cargo-futhark) #:use-module (crates-io))

(define-public crate-cargo-futhark-0.1.0 (c (n "cargo-futhark") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rerun_except") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "07whgfv7a7isi8vbialmj1pyh8y0mr93xhv5hldajjs4g2sml4nj")))

