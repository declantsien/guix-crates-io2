(define-module (crates-io ca rg cargo-mommy-ce) #:use-module (crates-io))

(define-public crate-cargo-mommy-ce-0.2.0 (c (n "cargo-mommy-ce") (v "0.2.0") (d (list (d (n "confique") (r "^0.2") (f (quote ("toml"))) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1lp7wb6w6kxrwp3lg36q2pm917gaq71d51lcycpyihffp01aiaaf")))

(define-public crate-cargo-mommy-ce-0.2.1 (c (n "cargo-mommy-ce") (v "0.2.1") (d (list (d (n "confique") (r "^0.2") (f (quote ("toml"))) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0c90irxd42g722z5cq1rzbzgzrf9ly3wykg6mz59a1szv0308kdq")))

