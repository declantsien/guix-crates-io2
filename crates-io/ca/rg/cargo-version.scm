(define-module (crates-io ca rg cargo-version) #:use-module (crates-io))

(define-public crate-cargo-version-0.1.0 (c (n "cargo-version") (v "0.1.0") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1mcmairx0p86jf2v9g089lzvp64hak7px5z1ijr6iadf2pf3qzbv")))

(define-public crate-cargo-version-0.1.1 (c (n "cargo-version") (v "0.1.1") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0vb67iw5xw2jnm2yaaj3lq5p6hcdzqj2j5c25d8b533rrbrnimfi")))

(define-public crate-cargo-version-0.1.2 (c (n "cargo-version") (v "0.1.2") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "05qk270xisicsamwlibhpprfcqwnxa5fp8yxlg6kgy0x0k4mcr6p")))

