(define-module (crates-io ca rg cargo-recursive) #:use-module (crates-io))

(define-public crate-cargo-recursive-0.1.0 (c (n "cargo-recursive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0dh1xx9fpqz0w036v7cj6x89jw0s4drcnjbvv3pd78q9jqjh65m5")))

(define-public crate-cargo-recursive-0.1.1 (c (n "cargo-recursive") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "099gads0vq241jcc4ildf3l31x1li442x9b8dpclyk2hg3gqwkrw")))

