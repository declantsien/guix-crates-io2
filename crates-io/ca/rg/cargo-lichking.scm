(define-module (crates-io ca rg cargo-lichking) #:use-module (crates-io))

(define-public crate-cargo-lichking-0.3.1 (c (n "cargo-lichking") (v "0.3.1") (d (list (d (n "cargo") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "void") (r "^1.0.1") (d #t) (k 0)))) (h "16vnm2csv0nnkjaaihawa54qmqyxwn2s1p52jwh54g543yb2q172")))

(define-public crate-cargo-lichking-0.3.2 (c (n "cargo-lichking") (v "0.3.2") (d (list (d (n "cargo") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "void") (r "^1.0.1") (d #t) (k 0)))) (h "1kb0cfq3q6rds9p0r7zz3yvr78shj95lxg184fz5r8phph1h4r6l")))

(define-public crate-cargo-lichking-0.4.0 (c (n "cargo-lichking") (v "0.4.0") (d (list (d (n "cargo") (r "^0.15") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "19hf91v3vmx44hbq96nqpx59x61aqvqqgcd8v4mn4cnala48d1mv")))

(define-public crate-cargo-lichking-0.4.1 (c (n "cargo-lichking") (v "0.4.1") (d (list (d (n "cargo") (r "^0.15") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0k2vn1g9jawv22a7h07fvfsdfrfc24mmfbwn11h1n7cimfabh87f")))

(define-public crate-cargo-lichking-0.4.2 (c (n "cargo-lichking") (v "0.4.2") (d (list (d (n "cargo") (r "^0.15") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1rd0gql3gwbj9cyahq2xhg0ffkcy9kyvkkj2mmfmqqs0p7aswcm5")))

(define-public crate-cargo-lichking-0.5.0 (c (n "cargo-lichking") (v "0.5.0") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0vz0p09w88i796nbq64jndnp8i7fph06nkpi6nq0gwbcj5lrggq2")))

(define-public crate-cargo-lichking-0.5.1 (c (n "cargo-lichking") (v "0.5.1") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0nsmndnsqkxq958x2gfv2fmjjj847l82vg7rqxkpqjvwq5413n5w")))

(define-public crate-cargo-lichking-0.5.2 (c (n "cargo-lichking") (v "0.5.2") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1g6iznxg2rq4sihy7zx336zpz96jbiavvs3zhrx47dy2zi4rwma2")))

(define-public crate-cargo-lichking-0.5.3 (c (n "cargo-lichking") (v "0.5.3") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "114kx025aklnbnamz3wkmlzp14r58m7hbpqpih9bvzbhcb6y8bf6")))

(define-public crate-cargo-lichking-0.5.4 (c (n "cargo-lichking") (v "0.5.4") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1gyr06dhsbjm4npk7gzsrvhpl25rvamzw5xz6rb818jfq4ivj8z8")))

(define-public crate-cargo-lichking-0.7.0 (c (n "cargo-lichking") (v "0.7.0") (d (list (d (n "cargo") (r "^0.31") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0yw3xy33az3j7sffir65brfa7f9y60f90dcrgsngnq7m7lkwp1gi")))

(define-public crate-cargo-lichking-0.8.0 (c (n "cargo-lichking") (v "0.8.0") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "01i6cyhg8qkcl22clvpfi30kfbzj5vfy4cdy5ibrmfsfbwxx0vfx")))

(define-public crate-cargo-lichking-0.9.0 (c (n "cargo-lichking") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.26") (f (quote ("std"))) (k 0)) (d (n "cargo_metadata") (r "^0.9.1") (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("color" "suggestions"))) (k 0)) (d (n "itertools") (r "^0.8.2") (f (quote ("use_std"))) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 0)) (d (n "regex") (r "^1.3.4") (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.6") (k 0)))) (h "015wj9fbj1ghcj1vrlpb2676hhi37wx8hqg30i9zq6jk4dpvi75k")))

