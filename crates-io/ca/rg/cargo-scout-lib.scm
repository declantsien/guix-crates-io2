(define-module (crates-io ca rg cargo-scout-lib) #:use-module (crates-io))

(define-public crate-cargo-scout-lib-0.6.0 (c (n "cargo-scout-lib") (v "0.6.0") (d (list (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "0.11.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0rjadh8ia476mmxjvl3v92677a2lq9r3qg76iqjihac17lmarrm9")))

