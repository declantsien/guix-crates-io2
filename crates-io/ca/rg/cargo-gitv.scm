(define-module (crates-io ca rg cargo-gitv) #:use-module (crates-io))

(define-public crate-cargo-gitv-0.1.1 (c (n "cargo-gitv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.13") (d #t) (k 0)))) (h "1bwk3yf4dcxbs9dcaafrz0bzyzikslh4fd1k7b740n90aqi1s3jr")))

(define-public crate-cargo-gitv-0.1.2 (c (n "cargo-gitv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (f (quote ("vendored-libgit2"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.13") (d #t) (k 0)))) (h "05pri7bpkfrwl92xm9w89knlylj74a3kiqr16mlra2czmm8h3z9p")))

