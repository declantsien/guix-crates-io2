(define-module (crates-io ca rg cargo-lock2rpmprovides) #:use-module (crates-io))

(define-public crate-cargo-lock2rpmprovides-0.1.0 (c (n "cargo-lock2rpmprovides") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "12gq1lrcwz4bd7bvm63zacjv0jz1p9jl7n1pixm5gavh9mjfxin1")))

(define-public crate-cargo-lock2rpmprovides-0.1.1 (c (n "cargo-lock2rpmprovides") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06dkbfrs01w31cyqv8h0hbqkx9ja77i46jxv3xzp797575is4kj2")))

(define-public crate-cargo-lock2rpmprovides-0.1.2 (c (n "cargo-lock2rpmprovides") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p449kd8gvcl41m2w30ph6g6n845plcwpj2gh4janicbf5kyxa0i")))

