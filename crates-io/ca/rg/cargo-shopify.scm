(define-module (crates-io ca rg cargo-shopify) #:use-module (crates-io))

(define-public crate-cargo-shopify-0.0.1 (c (n "cargo-shopify") (v "0.0.1") (h "17n7hz3gg94ij5qj5cj7kdklps6am5jlr6mcpixzhh3ib2myqszq")))

(define-public crate-cargo-shopify-0.0.2 (c (n "cargo-shopify") (v "0.0.2") (h "1ps72lzj1jgya1fpapk3j43hpy2xaxb5kjm3rnjiyj5vhlq9q3rj")))

(define-public crate-cargo-shopify-0.0.3 (c (n "cargo-shopify") (v "0.0.3") (h "0wv4qh0z0ivfl25l5l8534qm4295h74swv2jczlsyxl5qv6y0jcj")))

(define-public crate-cargo-shopify-0.0.4 (c (n "cargo-shopify") (v "0.0.4") (h "1n49w7304gr13vq38qq62d6rj88g1n7yzkh0x4dcli95hqdxx02q")))

