(define-module (crates-io ca rg cargo-group) #:use-module (crates-io))

(define-public crate-cargo-group-0.1.0 (c (n "cargo-group") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0vrxh59m32y4xdzqlq4xiq6rf691f1pmfnkqp9c3ai3hcx8jihbd")))

(define-public crate-cargo-group-0.1.1 (c (n "cargo-group") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "02k926zbf2ngnhj62pj6c2h58fcp62h26jw5bcyxgfhd5xf32lxl")))

(define-public crate-cargo-group-0.1.2 (c (n "cargo-group") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0ydwcbrdwyhypciiisw76lrfhah9w2q9ni0nk6w2c13xaaz2c2m9")))

