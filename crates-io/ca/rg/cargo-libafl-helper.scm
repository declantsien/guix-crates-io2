(define-module (crates-io ca rg cargo-libafl-helper) #:use-module (crates-io))

(define-public crate-cargo-libafl-helper-0.1.0 (c (n "cargo-libafl-helper") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1y4gf7vw5xv0q420g0rc2vc6jk343fvz123isw1ydwbf547l7vgp") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

