(define-module (crates-io ca rg cargo-packager-config) #:use-module (crates-io))

(define-public crate-cargo-packager-config-0.0.0 (c (n "cargo-packager-config") (v "0.0.0") (h "090dljpwvx37alzl3fyccihdflfk4xdrdp4jp8dz6gw77q1jcl3f") (y #t)))

(define-public crate-cargo-packager-config-0.1.0 (c (n "cargo-packager-config") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("url" "preserve_order" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "180i759a145j0107bs0ryyfvmg4w5c2h07a7r76dcgjq9mzv8ypi") (y #t) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-cargo-packager-config-0.2.0 (c (n "cargo-packager-config") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("url" "preserve_order" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1fpyynng91hirn4mr4wpvc4lv2fk5f8hafkq3r38f5jrn6qb7c5k") (y #t) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-cargo-packager-config-0.2.1 (c (n "cargo-packager-config") (v "0.2.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("url" "preserve_order" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1vim5sa3i87m8935wfajy53g3cwbwnngwq1ijqcbr6ndrav00vnz") (y #t) (s 2) (e (quote (("clap" "dep:clap"))))))

