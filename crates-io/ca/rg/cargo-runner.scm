(define-module (crates-io ca rg cargo-runner) #:use-module (crates-io))

(define-public crate-cargo-runner-0.1.0 (c (n "cargo-runner") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1x77gfd7aycmrwc98l4vccnisszw578m8n8sy82ci0f65vjja534")))

(define-public crate-cargo-runner-0.1.1 (c (n "cargo-runner") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "14g049rrjam5s9q0g930dvwp7p5mvbjf1zbixck0gcpg0zbhl8rs")))

