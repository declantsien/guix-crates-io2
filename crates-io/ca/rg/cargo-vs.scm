(define-module (crates-io ca rg cargo-vs) #:use-module (crates-io))

(define-public crate-cargo-vs-0.1.0 (c (n "cargo-vs") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v5"))) (d #t) (k 0)))) (h "1banaskllz44ijxjy0py10ilz0f8xykja9h25cx4bgz4ay4yna7d")))

(define-public crate-cargo-vs-0.1.1 (c (n "cargo-vs") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v5"))) (d #t) (k 0)))) (h "1vf2x94j2bzck652jn7imj8d5yawc6lasf4qknji5zidpdjl4qcj")))

(define-public crate-cargo-vs-0.1.2 (c (n "cargo-vs") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v5"))) (d #t) (k 0)))) (h "152f0b6984mxph3z5sxqg2b8cr4aq4wf3g9zl4n5zkbp949wdys6")))

