(define-module (crates-io ca rg cargo-cats) #:use-module (crates-io))

(define-public crate-cargo-cats-0.1.0 (c (n "cargo-cats") (v "0.1.0") (h "0s3hrcm6wg7xndxn5jgic99xdb6634vbj6fkj6p8263mgfzyyrn4") (y #t)))

(define-public crate-cargo-cats-0.1.1 (c (n "cargo-cats") (v "0.1.1") (h "0a7iqpdj57q4zhrryyl9vb0vvn376811vlw95p9h98g983i5nvpx")))

(define-public crate-cargo-cats-0.1.2 (c (n "cargo-cats") (v "0.1.2") (h "1jbdsvf9sar1d35s27xy1c1z93f1v367sm7z1cx7hnrdmkah54g8")))

(define-public crate-cargo-cats-0.1.3 (c (n "cargo-cats") (v "0.1.3") (h "11kzh5p5c59668250v6by4j8v7x7jjg9valmlxjjajrhdm8jr667")))

(define-public crate-cargo-cats-172.0.0 (c (n "cargo-cats") (v "172.0.0") (h "1ggn0qpgkjh17iarslm0fqv8mbfygd175zrnb1l4r3b8x344nglq") (y #t)))

(define-public crate-cargo-cats-0.1.4 (c (n "cargo-cats") (v "0.1.4") (h "05d7z6829lfrjbcw0cq7kmv49imqm2gggn43icz2nbxq9x290byn")))

(define-public crate-cargo-cats-0.1.5 (c (n "cargo-cats") (v "0.1.5") (h "07qsgk2mg76k5rbr7smx1zmd6nqxswr3f87mljgc4a1yifbxciam")))

(define-public crate-cargo-cats-0.1.6 (c (n "cargo-cats") (v "0.1.6") (h "160mckhq2fs0fs05nqm5i0s1z50lmkiwwqs9d7ckimny53xc1s8h")))

(define-public crate-cargo-cats-0.1.7 (c (n "cargo-cats") (v "0.1.7") (h "1x1icvm7w4bg2jysdcgqbbzp5l189ph2y0jbllhm6qgnb4b03qsv")))

(define-public crate-cargo-cats-0.1.8 (c (n "cargo-cats") (v "0.1.8") (h "1ypf12yc5hl8nw4rwxbrrv7vgvvzscv5i2dr4snknr35570pa3wx")))

(define-public crate-cargo-cats-0.1.9 (c (n "cargo-cats") (v "0.1.9") (h "16bznr7cv60frfjfvz5dp6ndpvb5r56w5hry0ribn006jzlrj6la")))

(define-public crate-cargo-cats-0.1.10 (c (n "cargo-cats") (v "0.1.10") (h "0d38bff59wd7h4468zbaip6pxfcrs3zdn7m5mq4wqjq3mhcc9vkz")))

(define-public crate-cargo-cats-0.1.11 (c (n "cargo-cats") (v "0.1.11") (h "0fxmb00yp4qim8zh16sxka1yiarfk4kk8ycxps6d24shvdwa52x8") (f (quote (("env-var") ("default" "env-var" "build") ("build"))))))

(define-public crate-cargo-cats-0.1.12 (c (n "cargo-cats") (v "0.1.12") (h "0wvl9gd03f7dr0yxnafiw7n4a7msqjmja2qg4vk5fdahwhgqgl4f") (f (quote (("env-var") ("default" "env-var" "build") ("build"))))))

(define-public crate-cargo-cats-0.1.13 (c (n "cargo-cats") (v "0.1.13") (h "1a05wn2ivq9gh57mqdpy4yha25259fn1wgpnfmjx1rxm971k6bma") (f (quote (("nightly-features") ("env-var") ("default" "env-var" "build") ("build"))))))

