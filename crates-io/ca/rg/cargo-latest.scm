(define-module (crates-io ca rg cargo-latest) #:use-module (crates-io))

(define-public crate-cargo-latest-0.1.0 (c (n "cargo-latest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.7.0") (d #t) (k 0)))) (h "1shr1zblcmgdwp8p7b70d3vkk068h3rcmy8sw3r69w35i5k40r2d")))

