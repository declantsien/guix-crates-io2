(define-module (crates-io ca rg cargo-referendum) #:use-module (crates-io))

(define-public crate-cargo-referendum-0.1.0 (c (n "cargo-referendum") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l9qbhxcikyk22wyhl0pniyy7bcd5c95liva3yw04ymsc7bcl2y4") (y #t)))

(define-public crate-cargo-referendum-0.1.1 (c (n "cargo-referendum") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w3vj5phskqf14nbl76n2jy5n11iiy32dyxpdgvi70jdwal2809b")))

