(define-module (crates-io ca rg cargo-export) #:use-module (crates-io))

(define-public crate-cargo-export-0.1.0 (c (n "cargo-export") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0whpc3i9d3xakz95j37r5bxsypamrg6qcwi6003wqdchlk08pc7a")))

(define-public crate-cargo-export-0.2.0 (c (n "cargo-export") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04mddzis87c98whrsxi94y1h09z07n84iybiwcz49yns7p27af1i")))

(define-public crate-cargo-export-0.2.1 (c (n "cargo-export") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0vlbs8cq2dm7p3nxlwl8xpc76cvdz9hy4vjbj4kja6lijxlprgzc") (y #t)))

(define-public crate-cargo-export-0.2.2 (c (n "cargo-export") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1kam80gb2kvj4wzxmy014ry1l1grm32k85qzx6wwgn0cmygx2zqw")))

(define-public crate-cargo-export-0.2.3 (c (n "cargo-export") (v "0.2.3") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "09r65n1750l1r31disckphd4qwq4v4kfl5jr39sr4r250ni5zj4l")))

(define-public crate-cargo-export-0.2.4 (c (n "cargo-export") (v "0.2.4") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0049an1rdrpj9l40lfv1x8hfbc99k5dmwd6728rs2fmqw28bsxkd")))

(define-public crate-cargo-export-0.2.5 (c (n "cargo-export") (v "0.2.5") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "05wcjphjr4xbpcqmhbd3xczxi63sp3s4hqwh23zvh09czhrr3b0a")))

