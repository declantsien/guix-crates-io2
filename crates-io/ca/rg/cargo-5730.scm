(define-module (crates-io ca rg cargo-5730) #:use-module (crates-io))

(define-public crate-cargo-5730-0.1.0 (c (n "cargo-5730") (v "0.1.0") (h "1dk1wim909wg0ng4a0dmf425ciylcqh3zp2vm2z8c2w4fam6a7h6")))

(define-public crate-cargo-5730-0.2.0 (c (n "cargo-5730") (v "0.2.0") (h "1lfx2f82hxzg2bb05rs8icc9xkv0hb42lifyq120y4y0gpaxscrv") (y #t)))

(define-public crate-cargo-5730-0.2.1 (c (n "cargo-5730") (v "0.2.1") (h "0i28nczkqq33ybwz9x3isydpfn3pd6gm62ghvw5vbg71gbdj5b39")))

