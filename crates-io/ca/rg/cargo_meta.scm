(define-module (crates-io ca rg cargo_meta) #:use-module (crates-io))

(define-public crate-cargo_meta-0.1.0 (c (n "cargo_meta") (v "0.1.0") (d (list (d (n "cargo_meta_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "11hhpkkkg6ks1a77zbvdiddl156fjmblhd0d8ragm4mcsxc3lad7")))

(define-public crate-cargo_meta-0.1.1 (c (n "cargo_meta") (v "0.1.1") (d (list (d (n "cargo_meta_proc") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0bi9iij29fmyvwk78dbzcx8bdq6djljsy884w38b51a8rgqfrjkv")))

(define-public crate-cargo_meta-0.1.2 (c (n "cargo_meta") (v "0.1.2") (d (list (d (n "cargo_meta_proc") (r "^0.1.2") (d #t) (k 0)))) (h "0qc4glil4ljajl1fz96pqz70hgxqzpnb0772vxgwx3f5lj3llqnj")))

(define-public crate-cargo_meta-0.1.3 (c (n "cargo_meta") (v "0.1.3") (d (list (d (n "cargo_meta_proc") (r "^0.1.3") (d #t) (k 0)))) (h "009w1y5y86jn0hjlgn5qp342wws7qnvxh6vvg46jd25bwwlqzbhz")))

(define-public crate-cargo_meta-0.1.4 (c (n "cargo_meta") (v "0.1.4") (d (list (d (n "cargo_meta_proc") (r "^0.1.4") (d #t) (k 0)))) (h "0r54lw6qcrdkzrmq2z097d1m41yw7awl24xwf1h8p7zjr56mb7kq")))

