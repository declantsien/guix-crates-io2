(define-module (crates-io ca rg cargo-prune) #:use-module (crates-io))

(define-public crate-cargo-prune-0.1.0 (c (n "cargo-prune") (v "0.1.0") (d (list (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1p05i7wv3crqxfz44fm3kgrgk581z6fdqflipzjdqqw8k92i8hjj")))

(define-public crate-cargo-prune-0.1.1 (c (n "cargo-prune") (v "0.1.1") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "10zcr70wazqmjpiwqrawlcshsr4dixaw3l1nw5cwy2h7g170phjc")))

(define-public crate-cargo-prune-0.1.2 (c (n "cargo-prune") (v "0.1.2") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1lql00abgk4ykv98wmkykkra69fxd0cbs772gxm0f8b5462pjjk6")))

(define-public crate-cargo-prune-0.1.3 (c (n "cargo-prune") (v "0.1.3") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0s29wrs4kkfqblbmhrkcbyw9v7dmlilwcrx1p3cihwr60g001iw0")))

(define-public crate-cargo-prune-0.1.4 (c (n "cargo-prune") (v "0.1.4") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0ryq0a010dsvgj29j4r5rvq0sdnwpyd1q2sw9yzj3f9m5h7sqyg0")))

(define-public crate-cargo-prune-0.1.5 (c (n "cargo-prune") (v "0.1.5") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1193jg8m49b96aj1fd6nsz475kmlnhb0dzc1a6x8jn3agymxbhyw")))

(define-public crate-cargo-prune-0.1.6 (c (n "cargo-prune") (v "0.1.6") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "12c34xf0agyw4yi03pp3irkrljhbn2z8bbi9xdqabdarh2rpr7ra")))

(define-public crate-cargo-prune-0.1.7 (c (n "cargo-prune") (v "0.1.7") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1fiv08r1l2gnnhdvh2ay1czz3z744hzka663k0x2ps0riwa2dfi4")))

(define-public crate-cargo-prune-0.1.8 (c (n "cargo-prune") (v "0.1.8") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0y3ycvrancrq5339cznax39kaildbb20db7sgnhf77wz2iv072kq")))

(define-public crate-cargo-prune-0.2.0 (c (n "cargo-prune") (v "0.2.0") (d (list (d (n "docopt") (r "~0.6.86") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.21") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "08gy1x1k7v7xcihnpikwgii4ng3pfyfp9npshbaxrrfspfpxbdn7")))

