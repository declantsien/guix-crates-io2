(define-module (crates-io ca rg cargo-psx) #:use-module (crates-io))

(define-public crate-cargo-psx-0.1.0 (c (n "cargo-psx") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sws8jzrz6f1ggaqlwas4b3qdzb8vvhgfhkkil9p26fzqx5l1695")))

(define-public crate-cargo-psx-0.1.1 (c (n "cargo-psx") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "12av7bmfz9cjnnrc1p7x4fjldm4m4gcx2k42g5zf5xgqy2y4jgxr")))

(define-public crate-cargo-psx-0.1.2 (c (n "cargo-psx") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "01jf8bacn3z2rm52j71jyy43jzws3x2m2c70a09cxmaza65jbb6i")))

(define-public crate-cargo-psx-0.1.3 (c (n "cargo-psx") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x8w6ibi1a56h175mv5i2hp351qcwmq0c9alya752qhwh5wlyflc")))

