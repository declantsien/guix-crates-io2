(define-module (crates-io ca rg cargo-readme) #:use-module (crates-io))

(define-public crate-cargo-readme-0.2.2 (c (n "cargo-readme") (v "0.2.2") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "17h1309hjbp6lfyff1nxfb4wdwsslcp5w0wyczs73qi5wxx8z585")))

(define-public crate-cargo-readme-0.2.3 (c (n "cargo-readme") (v "0.2.3") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "13wc75gf3d1cds8gc9qhmalll1lmpin0wp40hmb0k5nf5hv2grnk")))

(define-public crate-cargo-readme-1.0.0 (c (n "cargo-readme") (v "1.0.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "16vy9y4a7nj6b09ag1j405ahlzzvcyygiyhb10sw8qib3s14012a")))

(define-public crate-cargo-readme-1.1.0 (c (n "cargo-readme") (v "1.1.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "087zlmgm6hsj9kqccp719sfs8w7vv54nlyk39s7s2rw2zbzxfgvw")))

(define-public crate-cargo-readme-1.2.0 (c (n "cargo-readme") (v "1.2.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "1d86sr69838vs9bfh7z2mnlza3fmvqk5grcv5qyyqxq989g7hpwi")))

(define-public crate-cargo-readme-2.0.0 (c (n "cargo-readme") (v "2.0.0") (d (list (d (n "assert_cli") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1xz2c9makzmnspzp3kpa9fk1w3d77gkddghznf92wr0lkvq6d4s2")))

(define-public crate-cargo-readme-2.0.1 (c (n "cargo-readme") (v "2.0.1") (d (list (d (n "assert_cli") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1rcch52kqk6l3zb407zmgpsdc565y87h3d5c8jppfx38falhylpy")))

(define-public crate-cargo-readme-3.0.0-rc (c (n "cargo-readme") (v "3.0.0-rc") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0ki1iqv12aap18hzdwmjrnmshwy62k0my35sj4mlg3i90h8abzv9")))

(define-public crate-cargo-readme-3.0.0 (c (n "cargo-readme") (v "3.0.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "07q3bvqpkhl81w7496lxkc5c9lxbpdyg9p8apx0cppgfp057daqr")))

(define-public crate-cargo-readme-3.1.0 (c (n "cargo-readme") (v "3.1.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0mihf0rahr1qybj3mj2byabywrm41wavsch90mi30753h5xmalrv")))

(define-public crate-cargo-readme-3.1.1 (c (n "cargo-readme") (v "3.1.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "15pyb7dhgxf1n9c4wc4daaqs7qwym73gxrhscysfz8kssws3n30a")))

(define-public crate-cargo-readme-3.1.2 (c (n "cargo-readme") (v "3.1.2") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0l1s83xqmz8d4wnb5168d38vsla4clik7lqzcpgynjy1zjl05y56")))

(define-public crate-cargo-readme-3.2.0 (c (n "cargo-readme") (v "3.2.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0n0x308jgai2pqrzqmr8zl1niwnk7ihk6xg0asb45cpm0y9zrnv6")))

(define-public crate-cargo-readme-3.3.0 (c (n "cargo-readme") (v "3.3.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "02kgizmih51kdf886ymkxxycrav9kf1y5wjfrg507bdjds67l88h")))

(define-public crate-cargo-readme-3.3.1 (c (n "cargo-readme") (v "3.3.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "09f72nzvyrfd712hjd30lwivf0pcrgmn5w59pxfshbmlw6wplc2r")))

