(define-module (crates-io ca rg cargotool) #:use-module (crates-io))

(define-public crate-cargotool-0.0.1 (c (n "cargotool") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rust_util") (r "^0.6.15") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "06mj27xrnvcv5s1dgr09x7hbr8m98bk6p48657dzkzbhavlk75an")))

