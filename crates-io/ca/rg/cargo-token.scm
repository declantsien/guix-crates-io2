(define-module (crates-io ca rg cargo-token) #:use-module (crates-io))

(define-public crate-cargo-token-0.1.0 (c (n "cargo-token") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "074w6d4vmcnsbyyidskw318pdgcsp0jfnbrm74r8qdkskv4cp2zy")))

