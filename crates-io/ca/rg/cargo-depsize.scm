(define-module (crates-io ca rg cargo-depsize) #:use-module (crates-io))

(define-public crate-cargo-depsize-1.0.0 (c (n "cargo-depsize") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.69") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aq57pj9kdykxm8ssd0xz8v2k84j0g3iz9vh3fxz41x8rfw0pqrm")))

(define-public crate-cargo-depsize-1.0.1 (c (n "cargo-depsize") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.69") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s7qvqnmskq84si7f4g481kc8zlvk80fn3njvhlfzmbzmv0dg6s9")))

(define-public crate-cargo-depsize-1.0.2 (c (n "cargo-depsize") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo") (r "^0.78") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02667xl7acvnflmk8y99lvmm1zs01s3900jakhydr2z0pp2zzi96")))

