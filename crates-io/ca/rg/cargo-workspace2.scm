(define-module (crates-io ca rg cargo-workspace2) #:use-module (crates-io))

(define-public crate-cargo-workspace2-0.0.0 (c (n "cargo-workspace2") (v "0.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1d31dyzj93p73gzprnwl79qpbxpj4rkb09lm45w3bb0xx3xyn39q")))

(define-public crate-cargo-workspace2-0.1.0 (c (n "cargo-workspace2") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1z6hnc8gzfxii9ilzlk8w8sw4lki6azn4f3m1j6riyh5kl4a0wfa")))

(define-public crate-cargo-workspace2-0.1.1 (c (n "cargo-workspace2") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1zr9wn9slag39v0wpij7py714h7rx7v2nm1ma88cl7cc18ih8a9c")))

(define-public crate-cargo-workspace2-0.2.0 (c (n "cargo-workspace2") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (d #t) (k 0)) (d (n "toml") (r "^0.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1n20mbqh2a2w8pgg3lp3dfcl3rmji2n9dli1mbrlzpcpsynpc9g9")))

(define-public crate-cargo-workspace2-0.2.1 (c (n "cargo-workspace2") (v "0.2.1") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1b67jiwdrkabxbykyxayd6dn2y04x991k4x3rfp91hvrsppajs1b")))

(define-public crate-cargo-workspace2-0.2.2 (c (n "cargo-workspace2") (v "0.2.2") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1hnmfc4izbnhw7nyw67mhm4357g1qv7z75pf9hgkszqcapl5f7mm")))

