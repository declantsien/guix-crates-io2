(define-module (crates-io ca rg cargo-test-all-features) #:use-module (crates-io))

(define-public crate-cargo-test-all-features-0.1.0 (c (n "cargo-test-all-features") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "120m8pcydzc9p1j3fvb9qh29ny63lgsqfgy1p5h7250dhbyr3p4b")))

(define-public crate-cargo-test-all-features-1.0.0 (c (n "cargo-test-all-features") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "05lj0mx06ihd13cxmja47dq2kh38dly9l7b46n9hhmi7ap5bkpaa")))

(define-public crate-cargo-test-all-features-1.0.1 (c (n "cargo-test-all-features") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0i2cqz0l6c9pj96sghd061rsqvyfhsw14v1w7dx50gxw5bwg029v")))

(define-public crate-cargo-test-all-features-2.0.0 (c (n "cargo-test-all-features") (v "2.0.0") (h "142jq053zzi504zvm94f4frnaa6rnv7lgsm84202wd7wizgbw1cg")))

(define-public crate-cargo-test-all-features-3.0.0 (c (n "cargo-test-all-features") (v "3.0.0") (h "1val1dhas1x9anm4z43q8s2k7rcp6nk7vfq4835jv2kk7ybxw8pi")))

