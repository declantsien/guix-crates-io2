(define-module (crates-io ca rg cargo-mtime-travel) #:use-module (crates-io))

(define-public crate-cargo-mtime-travel-0.0.1 (c (n "cargo-mtime-travel") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "10aja8gf99x9si85n9g2jkvx8qq1akqgjrc0p90vwji6dkb803hc")))

