(define-module (crates-io ca rg cargo-yaml) #:use-module (crates-io))

(define-public crate-cargo-yaml-2.0.0 (c (n "cargo-yaml") (v "2.0.0") (d (list (d (n "toml") (r "^0.1.24") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "1jqynpgi9pk3z382ff9w8718lvi7ljg6z3b2f4rs204mr4l5ql66")))

(define-public crate-cargo-yaml-2.0.1 (c (n "cargo-yaml") (v "2.0.1") (d (list (d (n "toml") (r "^0.1.24") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0lsgswmkw9hf14w434q7kcmj1ayllfq3zbwa7mxqrmh0gl25l237")))

(define-public crate-cargo-yaml-3.0.0 (c (n "cargo-yaml") (v "3.0.0") (d (list (d (n "docopt") (r "^0.6.83") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "toml") (r "^0.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.3") (d #t) (k 0)))) (h "09xi6r5gdsx41a0a8ki4jgdprh7k5by0gy2hc8i79qmh94kk55bf")))

