(define-module (crates-io ca rg cargo-flippy) #:use-module (crates-io))

(define-public crate-cargo-flippy-1.0.0 (c (n "cargo-flippy") (v "1.0.0") (h "06j07cw4ikilqpbjlp7536w9dfkcfgczcw6lz1mbksimv65296qa")))

(define-public crate-cargo-flippy-1.0.1 (c (n "cargo-flippy") (v "1.0.1") (h "06lqc15mcplbkmpcw9q3pam4nxn1y4pcc5mh4dfn5p0brn975yi3")))

