(define-module (crates-io ca rg cargo-brief) #:use-module (crates-io))

(define-public crate-cargo-brief-0.1.0 (c (n "cargo-brief") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wildmatch") (r "^1.0") (d #t) (k 0)))) (h "0vnck52kdwmbfv515h61d77gsb964zv9f0ab8x2y4fwg07s5120x")))

(define-public crate-cargo-brief-0.2.0 (c (n "cargo-brief") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.0") (d #t) (k 0)))) (h "08a3x9ksm5j3ipq5nq26xmlvaxxljw0y84xxlbd51xgrspn65ga7")))

