(define-module (crates-io ca rg cargo-version-cli) #:use-module (crates-io))

(define-public crate-cargo-version-cli-0.1.0 (c (n "cargo-version-cli") (v "0.1.0") (d (list (d (n "cargo-version") (r "^0.1.1") (d #t) (k 0)))) (h "14ixsirpdgb0c3ghhdsqpdp73vd0ldaggy334j4akfl95p08mhgk")))

(define-public crate-cargo-version-cli-0.1.1 (c (n "cargo-version-cli") (v "0.1.1") (d (list (d (n "cargo-version") (r "^0.1.2") (d #t) (k 0)))) (h "1ipwjqi49m40mljqx8rszkykbhf1glsva20y82v8fm5sfir7v3qy")))

