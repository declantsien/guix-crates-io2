(define-module (crates-io ca rg cargo-whatis) #:use-module (crates-io))

(define-public crate-cargo-whatis-0.0.0 (c (n "cargo-whatis") (v "0.0.0") (h "1x22ajvvaqs81qhjihm9l3ii4kx91f3d6969bagdyyghxirggrln") (y #t)))

(define-public crate-cargo-whatis-0.1.0 (c (n "cargo-whatis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo") (r "^0.57.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)))) (h "1d63xxbpghywyqv7aqdgpbjsf9i9f51206zb3gjmkwn717j8dapv")))

(define-public crate-cargo-whatis-0.1.1 (c (n "cargo-whatis") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo") (r "^0.57.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)))) (h "0lcy7iha7c2qajd905yzvyk1aqsjq9n1r94g6acv61cdpr9k25s7")))

(define-public crate-cargo-whatis-0.1.2 (c (n "cargo-whatis") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo") (r "^0.57.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)))) (h "1aysjadjrknqhpr4q026m87z4jgv5ak6mn1igg8cqgg24p089vq6")))

