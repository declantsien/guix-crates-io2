(define-module (crates-io ca rg cargo-vcs) #:use-module (crates-io))

(define-public crate-cargo-vcs-0.1.0 (c (n "cargo-vcs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "machine-uid") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "10q42i4ysjvz4hgz0g69zx9kzjyv342jxv1vxhl2m422y0nd69h1") (r "1.59")))

(define-public crate-cargo-vcs-0.2.0 (c (n "cargo-vcs") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "machine-uid") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ksnjli61igx01yp0nn5kyf6h99l5p5kqxpv09d0v71ynb5w4bwc") (r "1.59")))

