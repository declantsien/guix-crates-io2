(define-module (crates-io ca rg cargo-navigate) #:use-module (crates-io))

(define-public crate-cargo-navigate-0.1.0 (c (n "cargo-navigate") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.7") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "open") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3.2") (d #t) (k 0)))) (h "1qwgkfsx5zfcad4nn3l5xd3zqmv0qwgzdi0bxb2mldaa2xn0nqy5")))

(define-public crate-cargo-navigate-0.1.1 (c (n "cargo-navigate") (v "0.1.1") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.7") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "open") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3.2") (d #t) (k 0)))) (h "0dx7wp60p37p3jrpdvcrzyrpgvkff3x77cngrhlh1059d4p8ji11")))

