(define-module (crates-io ca rg cargo-ghdoc) #:use-module (crates-io))

(define-public crate-cargo-ghdoc-0.1.1-alpha.1 (c (n "cargo-ghdoc") (v "0.1.1-alpha.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mmv2z0lhc6v5167lsd4sq2zk7ljwkvadbv6rdmh4bkpqkfggk7a")))

(define-public crate-cargo-ghdoc-0.1.1-alpha.2 (c (n "cargo-ghdoc") (v "0.1.1-alpha.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cpr3c2svzxlkrdjhb9zqlxzjyc9s14fvwia95sd0i6nnar04sdh")))

(define-public crate-cargo-ghdoc-0.2.0 (c (n "cargo-ghdoc") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ynm42h7lpgy9zs8z53rg8y0vk4sw852g8gf1namp7r908nm2cdw")))

(define-public crate-cargo-ghdoc-0.3.0 (c (n "cargo-ghdoc") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0fghlrh2p1dd9xr980dpvv20h7i3j959m3x691avvsq071v8hry1")))

(define-public crate-cargo-ghdoc-0.3.1 (c (n "cargo-ghdoc") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q467wbnpahnhnylk0pymffb40ypw01c07jarbc6x1lv8x9lp14x")))

(define-public crate-cargo-ghdoc-0.4.0 (c (n "cargo-ghdoc") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "19splarj77dlbn1d59yz6h23gkbk1xqqdp76k2k0166pmnqqg20d")))

