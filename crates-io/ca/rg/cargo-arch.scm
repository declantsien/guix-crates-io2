(define-module (crates-io ca rg cargo-arch) #:use-module (crates-io))

(define-public crate-cargo-arch-0.1.0 (c (n "cargo-arch") (v "0.1.0") (d (list (d (n "clap") (r "^2.9.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "toml") (r "^0.1.30") (d #t) (k 0)))) (h "0yiyrjnkqcgk4x5ya7p981w0cdh77m0hl94jwlvvfai1zy8168fr")))

(define-public crate-cargo-arch-0.1.1 (c (n "cargo-arch") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0yq9faa83rnfzk5x6c2bpbsv8dzfi4nqgj6cfrs4vnrdymcj6smp")))

(define-public crate-cargo-arch-0.1.2 (c (n "cargo-arch") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0lmmgxl0x62mr7rsk2ywnk80wqn9ddb5ymd03bw5vm76pm85pyy9")))

(define-public crate-cargo-arch-0.1.3 (c (n "cargo-arch") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "00xx29gr45mhqgzn21g4gvhvyjd600pql7av33wzz502gq5cr75a")))

(define-public crate-cargo-arch-0.1.4 (c (n "cargo-arch") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "11lmaphc63jgf3swsxp6l056y6sghiqx1gys8zn1321d8cdh10nl")))

(define-public crate-cargo-arch-0.1.5 (c (n "cargo-arch") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0p567yzk3c2hmymyqsvlsaj90pn7mp4z0zxa2vhk0v8ka1c2a166")))

