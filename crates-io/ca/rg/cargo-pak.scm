(define-module (crates-io ca rg cargo-pak) #:use-module (crates-io))

(define-public crate-cargo-pak-0.1.1 (c (n "cargo-pak") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0qfvicilpmclxh84vbzcjy4mnyqfg16j7ysqa5d812hn10qw70jq")))

(define-public crate-cargo-pak-0.1.2 (c (n "cargo-pak") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0x5x61w49qjd1iylqm6bnvwgc97qqn2ai91vmq812p16kypf3mk4")))

