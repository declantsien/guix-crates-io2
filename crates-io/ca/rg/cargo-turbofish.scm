(define-module (crates-io ca rg cargo-turbofish) #:use-module (crates-io))

(define-public crate-cargo-turbofish-0.0.1 (c (n "cargo-turbofish") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0pr5bh8mnk7pklcibj7dpch3500klym3gdgq67i07y9kxx058in6")))

(define-public crate-cargo-turbofish-0.0.2 (c (n "cargo-turbofish") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1yng0fdp681ygvhf89iz75ag5ayrwky7x8qmjaznrl6c3lsv1pv1")))

