(define-module (crates-io ca rg cargo-clean-recursive) #:use-module (crates-io))

(define-public crate-cargo-clean-recursive-0.9.0 (c (n "cargo-clean-recursive") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "18a5ip82vali907nbg4sg3vllkd260x57d0jn6fad90lij05pym5")))

(define-public crate-cargo-clean-recursive-0.9.1 (c (n "cargo-clean-recursive") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1l560bv6c25n6v02ikklzv4rswgi6fx379z44rxqj433ak52i9h8")))

(define-public crate-cargo-clean-recursive-0.9.2 (c (n "cargo-clean-recursive") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("fs" "macros" "process" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (f (quote ("fs"))) (d #t) (k 0)))) (h "10kqmdjsbkvwf418c181pim9q3whbl7yx01vjwrcq5yri368hc7i")))

(define-public crate-cargo-clean-recursive-0.9.3 (c (n "cargo-clean-recursive") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c0w2pwvdbpm6zm7r0ivlmqnd5yglp45ylynv1vsr7gqncj0azg6") (y #t)))

(define-public crate-cargo-clean-recursive-0.9.4 (c (n "cargo-clean-recursive") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rvga8slm4952zlyilgxzl3fz4d42cq30adb31zkwc29pz4hpr2")))

(define-public crate-cargo-clean-recursive-0.9.5 (c (n "cargo-clean-recursive") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sidn0bz7lj756lgssdam1mz9l51w5yayxlmslz3f828gyy5iv5c")))

(define-public crate-cargo-clean-recursive-0.9.6 (c (n "cargo-clean-recursive") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "13hsmsfv71rpybz4rx56gg68i6rh6v727qdhpnp05jv28xc5vsff")))

