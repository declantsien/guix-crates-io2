(define-module (crates-io ca rg cargo-build-docker) #:use-module (crates-io))

(define-public crate-cargo-build-docker-0.0.5 (c (n "cargo-build-docker") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1bljj8x3hij48xi0il007100r4ryclqd6bap38hi4msqryszjxzm")))

(define-public crate-cargo-build-docker-0.1.0 (c (n "cargo-build-docker") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "14p1jix34h92brxb6igbzvbbxgapfdb1ljg0msjzgrrb7v8j2r48")))

(define-public crate-cargo-build-docker-0.1.1 (c (n "cargo-build-docker") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0g9xg5r5n6h22si1kngww6v0dw282ysjkf9zfybbjm59fzxdwhsp")))

