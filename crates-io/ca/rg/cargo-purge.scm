(define-module (crates-io ca rg cargo-purge) #:use-module (crates-io))

(define-public crate-cargo-purge-0.1.0 (c (n "cargo-purge") (v "0.1.0") (d (list (d (n "whoami") (r "^1.1.2") (d #t) (k 0)))) (h "1y1y0d804l6yzx16i6hzrzj4gdcrjl2spcwrsx4aqqlb3zvflnkd")))

(define-public crate-cargo-purge-0.1.1 (c (n "cargo-purge") (v "0.1.1") (d (list (d (n "whoami") (r "^1.1.2") (d #t) (k 0)))) (h "1icayvvpbm91xmnzw75fc0054sy4384rvax1dc6f7xq9l5ww0ih2")))

