(define-module (crates-io ca rg cargo-remote) #:use-module (crates-io))

(define-public crate-cargo-remote-0.1.0 (c (n "cargo-remote") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1mlzi13w58jaw4mnjb7n8l4iz1mn1xqb1d1rfjw6j2cabyn533if")))

(define-public crate-cargo-remote-0.1.1 (c (n "cargo-remote") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "1hr15c8f7j2rryb3bgvgg1iajk7p7vajf7qpp4ba38bwmlbghi88")))

(define-public crate-cargo-remote-0.1.2 (c (n "cargo-remote") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "1xv9x7y5qq4pyv90jph5q1pjxs6v5faxwkk4fhhrj1adzxcc41k5")))

(define-public crate-cargo-remote-0.2.0 (c (n "cargo-remote") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.8.0") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "1v2kcbvx05vr7vc9an2936lfv141shrh9aik1lysv5qbbxb0i8zb")))

