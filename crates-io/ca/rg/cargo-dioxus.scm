(define-module (crates-io ca rg cargo-dioxus) #:use-module (crates-io))

(define-public crate-cargo-dioxus-0.1.0 (c (n "cargo-dioxus") (v "0.1.0") (h "1qpvc2nrzja9l3273rffh0byzbg8aa2sry4ais96aq68qy3pi9yb")))

(define-public crate-cargo-dioxus-0.1.1 (c (n "cargo-dioxus") (v "0.1.1") (h "1ca4bh71l82p262kn8qk6lpbj9rxkprspb9qv16sinrh5z8qlasy")))

