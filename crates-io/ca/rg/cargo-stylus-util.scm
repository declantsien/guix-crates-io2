(define-module (crates-io ca rg cargo-stylus-util) #:use-module (crates-io))

(define-public crate-cargo-stylus-util-0.3.0 (c (n "cargo-stylus-util") (v "0.3.0") (d (list (d (n "ethers") (r "^2.0.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rustc-host") (r "^0.1.7") (d #t) (k 0)))) (h "0z3z4s68qfj0lskaz8m3h0r6mnxyhjqzg9fabq4g7sk8hxdmpzf5")))

