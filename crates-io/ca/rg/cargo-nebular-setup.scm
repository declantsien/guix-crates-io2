(define-module (crates-io ca rg cargo-nebular-setup) #:use-module (crates-io))

(define-public crate-cargo-nebular-setup-0.1.0 (c (n "cargo-nebular-setup") (v "0.1.0") (h "08fmsc7s1w7aq93zy14md6lb5669yx6060ylqfy82k39lml9vic2")))

(define-public crate-cargo-nebular-setup-0.1.1 (c (n "cargo-nebular-setup") (v "0.1.1") (d (list (d (n "public-ip-addr") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1yp0szcv7645bd6wj5yn06hkcc1dc2gvff5843qsnqghbj5a1kan")))

(define-public crate-cargo-nebular-setup-0.1.2 (c (n "cargo-nebular-setup") (v "0.1.2") (d (list (d (n "public-ip-addr") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "063zx16chb75922cbh6571lzdpmdz33720p38xjbpaqlwrpakfzm")))

(define-public crate-cargo-nebular-setup-0.1.3 (c (n "cargo-nebular-setup") (v "0.1.3") (d (list (d (n "public-ip-addr") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1ixzsncgdjdzskbw9azl3g7p4mdj8b0q5y2hgkyip40ym0brfkd4")))

(define-public crate-cargo-nebular-setup-0.1.4 (c (n "cargo-nebular-setup") (v "0.1.4") (d (list (d (n "public-ip-addr") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1mjxbn0iic71aas5s3ayk8g9032b6nkkc8d41igwk33s29q9n1fx")))

