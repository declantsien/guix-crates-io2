(define-module (crates-io ca rg cargo-examples) #:use-module (crates-io))

(define-public crate-cargo-examples-0.1.0 (c (n "cargo-examples") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "15s80plhqqlvsnvpxz14cna0zq6vvgpzm9jigvmzz88jzpjp3mad")))

(define-public crate-cargo-examples-0.1.1 (c (n "cargo-examples") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0bx7nz96kf0szhn6rqsw1n43264yda0bzp1aa3mr87nm2xd4jc6f")))

(define-public crate-cargo-examples-0.2.0 (c (n "cargo-examples") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "07yl1pldr4kywrq1mrazzmjl6jxvaymqfzcbpw63zfxx208v3xnl")))

(define-public crate-cargo-examples-0.2.1 (c (n "cargo-examples") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1mrhj56dcs7smpiqz4ji9k80xdbhhryvwmz651728f03dmk781nq")))

(define-public crate-cargo-examples-0.3.0 (c (n "cargo-examples") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1pw00n0adrp4k70146h9ja44vsr0yzaxd0678wfd4yn7k9448vyd")))

(define-public crate-cargo-examples-0.4.0 (c (n "cargo-examples") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "13p16bmxyq98c9pzp1v1fd3njhky4ffrn2qwdqcx17iigh7ll8qf")))

(define-public crate-cargo-examples-0.5.0 (c (n "cargo-examples") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "16kfijsjyczhxmhr75xvfbi9kb74wxkq9jz08d6q9daagh0zmn4i")))

