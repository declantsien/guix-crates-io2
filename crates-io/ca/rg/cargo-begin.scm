(define-module (crates-io ca rg cargo-begin) #:use-module (crates-io))

(define-public crate-cargo-begin-0.1.0 (c (n "cargo-begin") (v "0.1.0") (h "13fxizk3l0w8bsh90hvvsqvq2wfw0q5rzpqx6v0l5b028n70hrxn") (y #t)))

(define-public crate-cargo-begin-0.1.1 (c (n "cargo-begin") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1rwq14rjb0s26h2xl3l9a1lhn0hdk7zlfhw5rp3wbvmwgdd29d64") (y #t)))

(define-public crate-cargo-begin-0.1.2 (c (n "cargo-begin") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0n469ih2s2qbs6si6xjcizm6j56ljzgx6kwf8yy495yqrzjsw6ga") (y #t)))

(define-public crate-cargo-begin-0.1.3 (c (n "cargo-begin") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1cd709i2s75hyk5wjz55601ivrgbgga5ms2wc88pxzwjan55d2ik") (y #t)))

(define-public crate-cargo-begin-0.1.4 (c (n "cargo-begin") (v "0.1.4") (h "1ivnnr8pd0bi8irvwh9s7h0gvd97xccaqq4cghv70yf0pfzp0myn") (y #t)))

(define-public crate-cargo-begin-0.1.5 (c (n "cargo-begin") (v "0.1.5") (h "089g6m27m89781lgdkblq0sy86d8n7vd85dwv9a9jzshwb24g178")))

(define-public crate-cargo-begin-1.0.0 (c (n "cargo-begin") (v "1.0.0") (h "00cii2v3wbw2435d2bfqvjml4k6vbiisa9r4idl8a7klqfc1ib75")))

(define-public crate-cargo-begin-1.0.1 (c (n "cargo-begin") (v "1.0.1") (h "0hm42xpjpa9gy9nwyr7dq2kw78nh7ickxhnmh0ji5fd15vp55jd7")))

(define-public crate-cargo-begin-1.1.0 (c (n "cargo-begin") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0wgi0djddcckcinhkwx5jwkmmqa9j5mmx628ifyzz8ln4dxm821j")))

(define-public crate-cargo-begin-1.1.1 (c (n "cargo-begin") (v "1.1.1") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0d0zxb0n9bp706jzgchzqmmddcxi4m7rmv91f85j6ghllv92f69p")))

