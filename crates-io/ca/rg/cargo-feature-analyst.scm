(define-module (crates-io ca rg cargo-feature-analyst) #:use-module (crates-io))

(define-public crate-cargo-feature-analyst-0.1.0 (c (n "cargo-feature-analyst") (v "0.1.0") (d (list (d (n "cargo") (r "^0.34") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "05xqs5vcfq2z70azksl4jdghlcxbg4dgdf1avdqx38xlvimxnkd3")))

