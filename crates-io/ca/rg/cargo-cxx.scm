(define-module (crates-io ca rg cargo-cxx) #:use-module (crates-io))

(define-public crate-cargo-cxx-0.1.0 (c (n "cargo-cxx") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0n8x3gdjk3yfcpy3dg7msrcwax4fwrmhljfq8rknclbshaxv2aj6")))

(define-public crate-cargo-cxx-0.1.1 (c (n "cargo-cxx") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0q6a5473jly7xq23r4wl9gk3r510vdsbmf5b94p23rfbfi9k7r3q")))

(define-public crate-cargo-cxx-0.1.2 (c (n "cargo-cxx") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "03kpnmn9kvv2ajqaaf3gf7379n414nl5shk6cxjkp6rnclhiwfpx")))

