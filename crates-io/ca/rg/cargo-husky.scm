(define-module (crates-io ca rg cargo-husky) #:use-module (crates-io))

(define-public crate-cargo-husky-0.0.0 (c (n "cargo-husky") (v "0.0.0") (h "1dyrg8snina2p6qq9pn731vs0xzndb2ff237ncdwd59g5sw93n1k")))

(define-public crate-cargo-husky-0.0.1 (c (n "cargo-husky") (v "0.0.1") (h "1gpv4gxj3xcf73sqswlpqm5nsnmrcjkk0pg2jf0s5yxz144582xh")))

(define-public crate-cargo-husky-0.0.2 (c (n "cargo-husky") (v "0.0.2") (h "19y723jfa4nrvr06qggv9srszz87b4w6pd1d78qz6xzpdwys8lzz")))

(define-public crate-cargo-husky-0.0.3 (c (n "cargo-husky") (v "0.0.3") (h "1x8iphm6j0bqfpha0sgxv0afdncr8wr23grhn215zk6c2yqadbcs")))

(define-public crate-cargo-husky-0.0.4 (c (n "cargo-husky") (v "0.0.4") (h "1vx2n9y33yqs5rmfkv9cfgr6nfm2qgn2fl971gf18452ha34zs5a")))

(define-public crate-cargo-husky-0.0.5 (c (n "cargo-husky") (v "0.0.5") (h "1nq51mhnrakfqfifpzpgri4jqkj88z5g1pmzvnc6fizza6vp97k9")))

(define-public crate-cargo-husky-0.0.6 (c (n "cargo-husky") (v "0.0.6") (h "1vnm08ydmbimb8r07k4f9vn9fvj5vfzldfmd58c2283fy2g5qmri") (f (quote (("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-0.1.0 (c (n "cargo-husky") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)))) (h "0dxz0d1fdq6if0h76hwv14gimjpgm9fk9vzly4img1d8fm46a9ia") (f (quote (("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-0.2.0 (c (n "cargo-husky") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "0fjgl28r6lw23pw3p8dvlcw0k9cqxzffiaxz769znxza0fxnn9fw") (f (quote (("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-0.3.0 (c (n "cargo-husky") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1ni3yxr3gnr6f5lhwafm04v6wjpr347hsbyy6p7jx9f01gksh9nd") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-0.3.1 (c (n "cargo-husky") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1rdwik853sk5mh0n1zymgw7q6c30hm0bh91vjjb0gjhd71v1pwp3") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.0.0 (c (n "cargo-husky") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "05gjz1fwg1h4x7igzc89if9lcd6cmfps9wzyn2d6dq4dvav8n47s") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.0.1 (c (n "cargo-husky") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "0lfcg2mh7yqwci3p63r07pm4a5gwmvgvwwd6y2zca2mnhajwy1n3") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-clippy") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.1.0 (c (n "cargo-husky") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1id5qq1jdr7pcrh8bywrywwqs7n2m2hfjw7mijywcbwp764ydw2v") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.2.0 (c (n "cargo-husky") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "18yzq59cqc5mcnkgi7rj95drcm93az18cpxbzjflcjwyygw37vli") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-fmt") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.2.1 (c (n "cargo-husky") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "0w3kc59h8mdzj6qv12mz8wc3s5n6pqppf0b51vq59gdfa0ghkz7v") (f (quote (("user-hooks") ("run-cargo-test") ("run-cargo-fmt") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test"))))))

(define-public crate-cargo-husky-1.3.0 (c (n "cargo-husky") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "0lkng61f4dv7vdyb8l7cw1pfwcvh29xdnb6pwhz5vqihmyrdixi2") (f (quote (("user-hooks") ("run-for-all") ("run-cargo-test") ("run-cargo-fmt") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test" "run-for-all"))))))

(define-public crate-cargo-husky-1.4.0 (c (n "cargo-husky") (v "1.4.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1gvva158wskmqi380pb8v9ifnfx589bjfn2d77zsxl69pi0nl7zf") (f (quote (("user-hooks") ("run-for-all") ("run-cargo-test") ("run-cargo-fmt") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test" "run-for-all"))))))

(define-public crate-cargo-husky-1.5.0 (c (n "cargo-husky") (v "1.5.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1b9jx720dzw9s7rl82bywz4d089c9rb0j526c1jfzs1g4llvc0kv") (f (quote (("user-hooks") ("run-for-all") ("run-cargo-test") ("run-cargo-fmt") ("run-cargo-clippy") ("run-cargo-check") ("prepush-hook") ("precommit-hook") ("postmerge-hook") ("default" "prepush-hook" "run-cargo-test" "run-for-all"))))))

