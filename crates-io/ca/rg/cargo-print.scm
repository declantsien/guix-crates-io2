(define-module (crates-io ca rg cargo-print) #:use-module (crates-io))

(define-public crate-cargo-print-0.1.0 (c (n "cargo-print") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "0afd4anhhmm472g3kv3pyfb6147lw3j0qwvy2iniy5pkvh2rlbdy")))

(define-public crate-cargo-print-0.1.1 (c (n "cargo-print") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "0qyil872yq74rw2v6byn12x8a2q88kh7xxjfyqh360w31c2n3pvx")))

(define-public crate-cargo-print-0.1.2 (c (n "cargo-print") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "0r36q369chcn9f5yj93mbl81ny571c5c61ijbs40mg4cz7assc69")))

(define-public crate-cargo-print-0.1.3 (c (n "cargo-print") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "0zfk023is2fzghi5p57sjk594g781j0ng8w35dy9xx5rdr6qy5g5")))

(define-public crate-cargo-print-0.1.4 (c (n "cargo-print") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.10") (d #t) (k 0)))) (h "05h1w0wz6f6a14r1693v9pw1n65sd1v65f4gxmnqr96cndjnvj34")))

(define-public crate-cargo-print-0.1.5 (c (n "cargo-print") (v "0.1.5") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)))) (h "163fcblgqja9s4iifmy2bnch8476ph2w0b38lwadkf7mgw2nqk51")))

(define-public crate-cargo-print-0.1.6 (c (n "cargo-print") (v "0.1.6") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15wcnr2jwdih06nf6zr8kkdidnhfq9pa6m3khc95b9bq91h81s9n")))

