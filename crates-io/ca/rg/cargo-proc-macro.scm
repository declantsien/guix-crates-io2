(define-module (crates-io ca rg cargo-proc-macro) #:use-module (crates-io))

(define-public crate-cargo-proc-macro-0.1.0 (c (n "cargo-proc-macro") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "153k9v9gl9i431rv5d0ai1pnrfig2kkwxhbqnq44fdlhxwi1drnk") (y #t)))

(define-public crate-cargo-proc-macro-0.1.1 (c (n "cargo-proc-macro") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "14hga4iqnc9yxa731n1mx7h294xp80w8l4znxjxf8k9ni1nn8ys0") (y #t)))

(define-public crate-cargo-proc-macro-0.1.2 (c (n "cargo-proc-macro") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0y54f2kpa7qwg9qb1l1rachjhdhgi7wwklnmcz0bafy8wx43rsd6") (y #t)))

(define-public crate-cargo-proc-macro-0.1.3 (c (n "cargo-proc-macro") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0jzrirq2ggd334w875c0d99kwgf9c02fnnag9g5by14z9awxrxa8")))

(define-public crate-cargo-proc-macro-0.2.0 (c (n "cargo-proc-macro") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1m3qanjcv75bd6y9n6sfa9kc2slvan88yn5g702hf9sc3ama2528")))

