(define-module (crates-io ca rg cargo-use) #:use-module (crates-io))

(define-public crate-cargo-use-0.1.0 (c (n "cargo-use") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "057wwnjsyf2ly79swx218y6acny4h5g5fyk9560wdxyqrs1ss4gr")))

(define-public crate-cargo-use-0.1.1 (c (n "cargo-use") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ksmyyf143casjh4zgsimf44s6wq00zq8l5k5bc6hxxq4d97c8hm")))

