(define-module (crates-io ca rg cargo-bdeps) #:use-module (crates-io))

(define-public crate-cargo-bdeps-0.1.5 (c (n "cargo-bdeps") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vjahzqz3r1k0ja75f99b136awzavx7s2r1m6906s1jhcaz89i6h")))

(define-public crate-cargo-bdeps-0.1.6 (c (n "cargo-bdeps") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0m4hn7xd0x94kvc2mzscbnw5lbw98qzpil9fl1gvfya30ycjz2zy")))

(define-public crate-cargo-bdeps-0.1.7 (c (n "cargo-bdeps") (v "0.1.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ia6r19d10j62pf4hbv7s9711y805sxa2zqhiz0cdgkrczn66yk4")))

