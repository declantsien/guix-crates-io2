(define-module (crates-io ca rg cargo-dep) #:use-module (crates-io))

(define-public crate-cargo-dep-0.1.0 (c (n "cargo-dep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xil5fkqscv5bp0ncsrxr9sv5n1h0fsmrg0430r546qsjs3zmd4n")))

