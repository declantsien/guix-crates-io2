(define-module (crates-io ca rg cargo-add) #:use-module (crates-io))

(define-public crate-cargo-add-0.1.0 (c (n "cargo-add") (v "0.1.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "0qir79aqs8z1xcx5709621vvd71lwkppa03rq7z78p32c799hqga")))

(define-public crate-cargo-add-0.2.0 (c (n "cargo-add") (v "0.2.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "= 0.3.22") (d #t) (k 0)) (d (n "semver") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1r5r2w0w7fhl6dhnvxwq9sl69c6asd24qckvri39skbhki9jvks9")))

