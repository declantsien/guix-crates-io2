(define-module (crates-io ca rg cargo-rbrew) #:use-module (crates-io))

(define-public crate-cargo-rbrew-0.0.3 (c (n "cargo-rbrew") (v "0.0.3") (d (list (d (n "argp") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "02vqv8wkj2hspb6pkn6kg0rq1pg9lh354czb0f8pp4nlks39cwwz")))

(define-public crate-cargo-rbrew-0.0.4 (c (n "cargo-rbrew") (v "0.0.4") (d (list (d (n "argp") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1p6fjgh9pb8ly9d5wiy5xxjp9f5pybln4j1zm25yjmk1zyv6m2hv")))

(define-public crate-cargo-rbrew-0.0.5 (c (n "cargo-rbrew") (v "0.0.5") (d (list (d (n "argp") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "03clh41x9r0vp7accfaqy23ys6fkwq1md1m2kgp6c2jx3car3zl5")))

