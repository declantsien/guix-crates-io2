(define-module (crates-io ca rg cargo-squatty) #:use-module (crates-io))

(define-public crate-cargo-squatty-0.0.0-reserved (c (n "cargo-squatty") (v "0.0.0-reserved") (h "0vxlb4030jad0723iv6n9mkpzmxm45s722lmmw2ycavricchlchi")))

(define-public crate-cargo-squatty-0.1.0 (c (n "cargo-squatty") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 0)))) (h "1345p3lbzivqlwd1zxg06q23d7my1zky1vis0xmcbqz8ij5avpm8")))

(define-public crate-cargo-squatty-1.0.0 (c (n "cargo-squatty") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 0)))) (h "1vz8fh5qxya9wkk9sc5213rgfjjsmc5qi7ls4d9wzibbr8xz02gg")))

