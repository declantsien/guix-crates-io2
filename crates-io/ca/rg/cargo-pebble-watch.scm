(define-module (crates-io ca rg cargo-pebble-watch) #:use-module (crates-io))

(define-public crate-cargo-pebble-watch-0.1.0 (c (n "cargo-pebble-watch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jdb68xcg35nz96shp8qhhwqpjkfj0a6xkialp6xgd9arh1y5raj")))

