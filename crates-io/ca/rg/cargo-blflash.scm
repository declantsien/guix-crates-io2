(define-module (crates-io ca rg cargo-blflash) #:use-module (crates-io))

(define-public crate-cargo-blflash-0.1.0 (c (n "cargo-blflash") (v "0.1.0") (d (list (d (n "blflash") (r "^0.1.0") (d #t) (k 0)) (d (n "cargo-project") (r "^0.2.4") (d #t) (k 0)) (d (n "main_error") (r "^0.1.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.4") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "05hhij7ng0pi5jm4fskwqgvm9kqniq6yif06yx9d4hfi85rk10d6")))

(define-public crate-cargo-blflash-0.3.0 (c (n "cargo-blflash") (v "0.3.0") (d (list (d (n "blflash") (r "^0.3") (d #t) (k 0)) (d (n "cargo-project") (r "^0.2.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "main_error") (r "^0.1.1") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "09962gs6b6hn4xg2y2qlm0yhhzq586z26dvpfhs2dl5y7c5pfswy")))

(define-public crate-cargo-blflash-0.3.5 (c (n "cargo-blflash") (v "0.3.5") (d (list (d (n "blflash") (r "=0.3.5") (d #t) (k 0)) (d (n "cargo-project") (r "^0.3.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "main_error") (r "^0.1.1") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0m0ll2ckb1fkg8433i148qljsx2nl6bg5mrx6nfk3hdkg98wv1jx")))

