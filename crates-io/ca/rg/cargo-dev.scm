(define-module (crates-io ca rg cargo-dev) #:use-module (crates-io))

(define-public crate-cargo-dev-0.0.1 (c (n "cargo-dev") (v "0.0.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1gqdfj0hkav71cqg84l4sx3zp6gyvm3xd3zr56s1wz6p85ydx9pq")))

(define-public crate-cargo-dev-0.0.2 (c (n "cargo-dev") (v "0.0.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "00r2idmvk4ngd4j3r4h2fm2hanqwrnqx0wppc3ir5jbg0vspgf68")))

