(define-module (crates-io ca rg cargo-selector) #:use-module (crates-io))

(define-public crate-cargo-selector-0.1.0 (c (n "cargo-selector") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "03qxgq9n2p5vqi1jyccsgkc1z81k1hyzwghsgzxmw3p88g47c9b6")))

(define-public crate-cargo-selector-0.2.0 (c (n "cargo-selector") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "1axdi0g6iwx0vwyl9m35jyv3x5w36qmkk1szjxm8rw814g536rz4")))

(define-public crate-cargo-selector-0.3.0 (c (n "cargo-selector") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "1l3gggqqhmanp0i7k0v6c6hg4ypiiki7cd2n9x4cmpmbc6q9y7l4")))

(define-public crate-cargo-selector-0.4.0 (c (n "cargo-selector") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "1sv0rj1pkhrkc5lx67qbf34pgkv36hsqp33izh2kyja0cqbvkc4f")))

