(define-module (crates-io ca rg cargo-3pl) #:use-module (crates-io))

(define-public crate-cargo-3pl-0.1.0 (c (n "cargo-3pl") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0xzqr4arp54h3nsr95ivpsgdbfaccr031ipl10linr3gsnlflhyf")))

(define-public crate-cargo-3pl-0.1.1 (c (n "cargo-3pl") (v "0.1.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1lbjpkgkvjj64yilk4678lk7wyslmx03mbhfk8cfvrj1kcqc1v2j")))

(define-public crate-cargo-3pl-0.1.2 (c (n "cargo-3pl") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bidzpx2vlhcww380h8hhzx82ijcz17l25pjl6p39db41a9qlqcd")))

(define-public crate-cargo-3pl-0.1.3 (c (n "cargo-3pl") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "error-context" "help" "std" "suggestions" "usage"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nhs58s08fx9j404fnk44gnqk7yq2rspn91ziz8536v84x0g0d27")))

