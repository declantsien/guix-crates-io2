(define-module (crates-io ca rg cargo-pkgbuild) #:use-module (crates-io))

(define-public crate-cargo-pkgbuild-0.1.0 (c (n "cargo-pkgbuild") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "toml") (r "^0.1.30") (d #t) (k 0)))) (h "0998dqawqp6sfvjindrmhshp17gcxzff5xf9pfvxjb4dbgc0wgpi")))

(define-public crate-cargo-pkgbuild-0.1.1 (c (n "cargo-pkgbuild") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "toml") (r "^0.1.30") (d #t) (k 0)))) (h "07g0554lzfv8d9fy7bcvqbdp586504wns4dvwmn5y1pjjwcf2mm2")))

