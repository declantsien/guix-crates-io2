(define-module (crates-io ca rg cargo-exe) #:use-module (crates-io))

(define-public crate-cargo-exe-1.0.0 (c (n "cargo-exe") (v "1.0.0") (d (list (d (n "cargo_toml") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "01bxxrvncl6qyhzh1wdisdhj49dfrzffwnf8bp4j0h8cjgxr0ykf")))

(define-public crate-cargo-exe-1.0.1 (c (n "cargo-exe") (v "1.0.1") (d (list (d (n "cargo_toml") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "1sw2xa933z7gkcd3hsd5bdf2jgcpj4hrg6b81241a8jncl1a18ns")))

