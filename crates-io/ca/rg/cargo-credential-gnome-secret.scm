(define-module (crates-io ca rg cargo-credential-gnome-secret) #:use-module (crates-io))

(define-public crate-cargo-credential-gnome-secret-0.1.0 (c (n "cargo-credential-gnome-secret") (v "0.1.0") (d (list (d (n "cargo-credential") (r "^0.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1g7af1i8kp2ll32ac0zbmbd7lpd27inx9748b0qddcgnl14c0d3q")))

(define-public crate-cargo-credential-gnome-secret-0.3.0 (c (n "cargo-credential-gnome-secret") (v "0.3.0") (d (list (d (n "cargo-credential") (r "^0.3.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1r1apjl6vl81k01p4gb8d8hx55bfihj2rsrf7ppbhbrzwx2qasry")))

