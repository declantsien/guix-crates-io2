(define-module (crates-io ca rg cargonauts) #:use-module (crates-io))

(define-public crate-cargonauts-0.1.0 (c (n "cargonauts") (v "0.1.0") (d (list (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "0fv159qa3nknsryhjmyh6l3s1fxrrhwv4lx6060kss41pl26l11r") (y #t)))

(define-public crate-cargonauts-0.1.1 (c (n "cargonauts") (v "0.1.1") (d (list (d (n "itertools") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "0kkf8rf2ycw325g2k4x8vjcnh9dq4wz1jw7c5a93p21hxkvj3nr0") (y #t)))

(define-public crate-cargonauts-0.2.0 (c (n "cargonauts") (v "0.2.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1nggrrn9s3ywhmfynjk11njb6ikkbxh0q9ndz7yw4q28bc0lpwfx") (y #t)))

