(define-module (crates-io ca rg cargo-testjs) #:use-module (crates-io))

(define-public crate-cargo-testjs-0.1.0 (c (n "cargo-testjs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "toml") (r "^0.3.1") (d #t) (k 0)))) (h "0wb351s8pbia6cdm6hcxwi3fdik8dbqi1nixghvmc4h45ifwp73d")))

(define-public crate-cargo-testjs-0.1.1 (c (n "cargo-testjs") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "toml") (r "^0.3.1") (d #t) (k 0)))) (h "10glgk9s50id744y5sy0nqp8chdd5532qvqy3nhsca4lh15dxfgl")))

(define-public crate-cargo-testjs-0.1.2 (c (n "cargo-testjs") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "toml") (r "^0.3.1") (d #t) (k 0)))) (h "0szfjk2zr17ida2jhpxknbbk1kwrzl84cyw55zbysxmi0l5a4mwz")))

