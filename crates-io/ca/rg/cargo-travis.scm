(define-module (crates-io ca rg cargo-travis) #:use-module (crates-io))

(define-public crate-cargo-travis-0.0.0 (c (n "cargo-travis") (v "0.0.0") (h "1l03mbj5m3p7hwbzgxqr96l5idv4qsx7yryj11ww8d9nkjanfh59")))

(define-public crate-cargo-travis-0.0.1-pre0 (c (n "cargo-travis") (v "0.0.1-pre0") (d (list (d (n "cargo") (r "^0.13.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hjyvn8rgbcifilqpxdbqi99p8q09mlfvwkqaa74m01a4sdblkbc")))

(define-public crate-cargo-travis-0.0.1-pre1 (c (n "cargo-travis") (v "0.0.1-pre1") (d (list (d (n "cargo") (r "^0.13.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0nn806dgxngqfjjyyw08xmnwn023y6scdpzzn23sz429q7k19qnq")))

(define-public crate-cargo-travis-0.0.2 (c (n "cargo-travis") (v "0.0.2") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1j3h77iiyxwqmgqhh7j3305sa4kd1z8rj9i5r236ci1f8z4qli1q")))

(define-public crate-cargo-travis-0.0.3 (c (n "cargo-travis") (v "0.0.3") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1p5gwn4qhfbc8k1wkq23mgz0f1h8gn9mgihx6y87a5yf4n05n69f")))

(define-public crate-cargo-travis-0.0.4 (c (n "cargo-travis") (v "0.0.4") (d (list (d (n "cargo") (r "^0.16.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02ywzys05cp32aficwcnncsy80nndkz6cszd7xp79sw8n5xqj6id")))

(define-public crate-cargo-travis-0.0.5 (c (n "cargo-travis") (v "0.0.5") (d (list (d (n "cargo") (r "^0.19.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0hfrr3p320d1p308k5q141ihpjsljghpsmbx4dc3knc28w5n2x12")))

(define-public crate-cargo-travis-0.0.6 (c (n "cargo-travis") (v "0.0.6") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "088gbif8dnnbmrrvzbk1s5v1n88wbm7kmi2zqbm2w1graanjz4a1")))

(define-public crate-cargo-travis-0.0.7 (c (n "cargo-travis") (v "0.0.7") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1mdq05ircrmy7daycjr6idv9mkggbww379d9931x46lpkav7af35")))

(define-public crate-cargo-travis-0.0.8 (c (n "cargo-travis") (v "0.0.8") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "docopt") (r "= 0.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rwm13nzigvcb4m150p05klihr3yi5fyasi53b45hw7i49r1bkni")))

(define-public crate-cargo-travis-0.0.9 (c (n "cargo-travis") (v "0.0.9") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "docopt") (r "= 0.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ib4s6a8jfrbm5073dlrvpn7l6anqnlcsyvkn1wifd0bhply729r")))

(define-public crate-cargo-travis-0.0.10 (c (n "cargo-travis") (v "0.0.10") (d (list (d (n "badge") (r "^0.2") (d #t) (k 0)) (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "04n6296209pf2bmzm5grxnygnpi09fain2h93x70ychyap8dwywb")))

(define-public crate-cargo-travis-0.0.11 (c (n "cargo-travis") (v "0.0.11") (d (list (d (n "badge") (r "^0.2") (d #t) (k 0)) (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0624917xf9b6xkmb8smk0x0vw4gx2dh8p2yq3shjj2w8nnqf2p45")))

