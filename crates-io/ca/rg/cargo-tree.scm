(define-module (crates-io ca rg cargo-tree) #:use-module (crates-io))

(define-public crate-cargo-tree-0.1.0 (c (n "cargo-tree") (v "0.1.0") (d (list (d (n "cargo") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02qc9c1fwkrsfmiw3lkc6ayag4lwixdhah6zvgyybqyv4i6hr473")))

(define-public crate-cargo-tree-0.2.0 (c (n "cargo-tree") (v "0.2.0") (d (list (d (n "cargo") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bhqayyw2cdy2xr0rypnhcj77110r2fm9xr45jrmgv9jli19hxil")))

(define-public crate-cargo-tree-0.3.0 (c (n "cargo-tree") (v "0.3.0") (d (list (d (n "cargo") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0fs1y2lhkbdb0hjs8cr8ajwbmh5z0d0d800hk9a2lmhp5wihgywq")))

(define-public crate-cargo-tree-0.3.1 (c (n "cargo-tree") (v "0.3.1") (d (list (d (n "cargo") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1s0rkca1qfjdz0q8ar6xg296fljghdgpl7sgi7rvdmzy9s646vda")))

(define-public crate-cargo-tree-0.3.2 (c (n "cargo-tree") (v "0.3.2") (d (list (d (n "cargo") (r "^0.7") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "16jrgchabn9d6mcqan8hg6cpfb0380cj1a5in6imla7gvy2nrn44")))

(define-public crate-cargo-tree-0.3.3 (c (n "cargo-tree") (v "0.3.3") (d (list (d (n "cargo") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mcpwhg2fsfxdhm5mmy0i6k499j251k7paygms36r5kd5h7d0crb")))

(define-public crate-cargo-tree-0.4.0 (c (n "cargo-tree") (v "0.4.0") (d (list (d (n "cargo") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1pmi1bxvakh8r8hb4xhyxl2f7vj7bxz32x51pk1jx706q5j4wxyk")))

(define-public crate-cargo-tree-0.4.1 (c (n "cargo-tree") (v "0.4.1") (d (list (d (n "cargo") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06vsxbpgndi3lix453gckccp3imgnnmvar2g3gf9gm98xvk6y4dv")))

(define-public crate-cargo-tree-0.4.2 (c (n "cargo-tree") (v "0.4.2") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0hs2p5iaz4xgmd3fp9zmf26bh3p6mxdr1azl2362anv2cyr7m2c2")))

(define-public crate-cargo-tree-0.4.3 (c (n "cargo-tree") (v "0.4.3") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ksn7qf96wv4s65l6w3i1fbffpb5vyjjqixd8hsxrni4wkrrxhsb")))

(define-public crate-cargo-tree-0.5.0 (c (n "cargo-tree") (v "0.5.0") (d (list (d (n "cargo") (r "^0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f6ih868x2yv56vdhmipkf7ksyrk3fz2fr8lckj943hqkdiwby47")))

(define-public crate-cargo-tree-0.5.1 (c (n "cargo-tree") (v "0.5.1") (d (list (d (n "cargo") (r "^0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1p5r0rjl2gwvnfp8q73id4ngfhhk786ynhqx9hbglxic2rp16q2b")))

(define-public crate-cargo-tree-0.6.0 (c (n "cargo-tree") (v "0.6.0") (d (list (d (n "cargo") (r "^0.12") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1aqwkzkzyzgvgg25552c6q9n9x255bplh23y45lf32dgbf2yx4y7")))

(define-public crate-cargo-tree-0.7.0 (c (n "cargo-tree") (v "0.7.0") (d (list (d (n "cargo") (r "^0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v8jdmj808arrjyr0acslga2q22f6mp7hcgmv9rcgmgy1hs89kn6")))

(define-public crate-cargo-tree-0.8.0 (c (n "cargo-tree") (v "0.8.0") (d (list (d (n "cargo") (r "^0.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19g00ggy9mcj18gl74zqndb0kiqgnlqk2qqycmxzswhjl7lcipba")))

(define-public crate-cargo-tree-0.9.0 (c (n "cargo-tree") (v "0.9.0") (d (list (d (n "cargo") (r "^0.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "125cjhf798wq4nl2k1k5l1cgj12ms9xvm2v1k5rh7wm5x60ajjw1")))

(define-public crate-cargo-tree-0.9.1 (c (n "cargo-tree") (v "0.9.1") (d (list (d (n "cargo") (r "^0.15") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cvj9q9bw6scpar4xdsaiank931fm21a4z0982c3007ld7bwvfvk")))

(define-public crate-cargo-tree-0.10.0 (c (n "cargo-tree") (v "0.10.0") (d (list (d (n "cargo") (r "^0.16") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ra6pv3hkzcqjg5qp0ijq689jj3mj5z7lqbqrxzh1f9imifvv427")))

(define-public crate-cargo-tree-0.10.1 (c (n "cargo-tree") (v "0.10.1") (d (list (d (n "cargo") (r "^0.16") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0h1p44p76z2s907amvk3la8ajfni3g5irl8xzqdxc49qcdhvi6yi")))

(define-public crate-cargo-tree-0.11.0 (c (n "cargo-tree") (v "0.11.0") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ydvn0wbrhhzrqnrvz927kbvz97m3b5anqyhc4p5ndfkij0z94fv")))

(define-public crate-cargo-tree-0.12.0 (c (n "cargo-tree") (v "0.12.0") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jfh1nhjshiabbc1bqdv6k18084515m63zc5hwrrljh04bl443i2")))

(define-public crate-cargo-tree-0.13.0 (c (n "cargo-tree") (v "0.13.0") (d (list (d (n "cargo") (r "^0.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ici28vwkx2chpzx16m5v4wzxb0f2qjg2qgg0rhq484m48bnnb9k")))

(define-public crate-cargo-tree-0.14.0 (c (n "cargo-tree") (v "0.14.0") (d (list (d (n "cargo") (r "^0.20") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kwznw86g5gqmj3k7m3870siwq612ijyfm4f96z78b8712gy7883")))

(define-public crate-cargo-tree-0.15.0 (c (n "cargo-tree") (v "0.15.0") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p66f0mnsy3y4g3igipgqc6dds1nniyf1lxxnxpvdkyxg14y5crp")))

(define-public crate-cargo-tree-0.16.0 (c (n "cargo-tree") (v "0.16.0") (d (list (d (n "cargo") (r "^0.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16srr3jm0nsrzi87i85286cv1rvlliznxnm5kswywhkzsn51ijd3")))

(define-public crate-cargo-tree-0.17.0 (c (n "cargo-tree") (v "0.17.0") (d (list (d (n "cargo") (r "^0.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1phcbhzj4lwgmxar0h37vm7f3sn4slprqbcd6i6r1lxn2v6ss2cy")))

(define-public crate-cargo-tree-0.18.0 (c (n "cargo-tree") (v "0.18.0") (d (list (d (n "cargo") (r "^0.27") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1qhibv84a9anhhqpn8dpji46sc1r30y5jjf194ahsdrgqgbwjx5a")))

(define-public crate-cargo-tree-0.19.0 (c (n "cargo-tree") (v "0.19.0") (d (list (d (n "cargo") (r "^0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1i4b7jnw4p1xb9g7f6jpwn0fbjkhgliajy4lrpmmg6y26x3xp54z")))

(define-public crate-cargo-tree-0.20.0 (c (n "cargo-tree") (v "0.20.0") (d (list (d (n "cargo") (r "^0.29") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0m5vma9iajcl2l6g2pasczdvlnda1915wlydh6jyappagrc75jjm")))

(define-public crate-cargo-tree-0.21.0 (c (n "cargo-tree") (v "0.21.0") (d (list (d (n "cargo") (r "^0.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1w890dvz2363zh1h6x2rifmq9a96qzw47b6rsf0182jcxl9rqvxv")))

(define-public crate-cargo-tree-0.22.0 (c (n "cargo-tree") (v "0.22.0") (d (list (d (n "cargo") (r "^0.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1h0mcqcqsf91n9knk85aq3wsckkgyiiccd74cdy519hirwkixrs3")))

(define-public crate-cargo-tree-0.23.0 (c (n "cargo-tree") (v "0.23.0") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "14bc4pd8ngljas3wbk65w7lgp2js755cdwklk197rawh7f9pmvhs")))

(define-public crate-cargo-tree-0.24.0 (c (n "cargo-tree") (v "0.24.0") (d (list (d (n "cargo") (r "^0.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "15nz7g796x9z1jy7h1z9wwv7f2iyk82asqw8j8pjaj7v1vvdxp0q")))

(define-public crate-cargo-tree-0.25.0 (c (n "cargo-tree") (v "0.25.0") (d (list (d (n "cargo") (r "^0.34") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "01glrf131j189r4v5mf8l5ddqxbpm035n1ylp21q2r5i1n78xaih")))

(define-public crate-cargo-tree-0.26.0 (c (n "cargo-tree") (v "0.26.0") (d (list (d (n "cargo") (r "^0.35") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1abqbh09gadmilwh56a6vs292g3693fv6x28wydz4k6hwqsv0brl")))

(define-public crate-cargo-tree-0.27.0 (c (n "cargo-tree") (v "0.27.0") (d (list (d (n "cargo") (r "^0.39") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a063x0w9cy5ysi0mgz6xc9yivqvdvb2n6ssv3iixqbzmijsxdky")))

(define-public crate-cargo-tree-0.28.0 (c (n "cargo-tree") (v "0.28.0") (d (list (d (n "cargo") (r "^0.41") (d #t) (k 0)) (d (n "cargo-platform") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0plrgrg5k9h1m5hibxqxrr74qd4ci4f7wh8qaarl5sw3fldfc04x")))

(define-public crate-cargo-tree-0.29.0 (c (n "cargo-tree") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s12p3c529damn2bb2ljd8fp3vifig9pkwhflnx8ycqak68hwdzk")))

