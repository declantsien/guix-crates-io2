(define-module (crates-io ca rg cargo-deploy) #:use-module (crates-io))

(define-public crate-cargo-deploy-0.1.0 (c (n "cargo-deploy") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qhq9fzqiaiq093lvfha33vn2sb3vqhghyqzjans1fg5jkswjdia")))

(define-public crate-cargo-deploy-0.1.1 (c (n "cargo-deploy") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ix20p6ql5id0zgwm0vfi2kp46pd4gsdcd9532pxvvm71bh9zzga")))

