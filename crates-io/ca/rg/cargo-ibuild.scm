(define-module (crates-io ca rg cargo-ibuild) #:use-module (crates-io))

(define-public crate-cargo-ibuild-1.0.0 (c (n "cargo-ibuild") (v "1.0.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "tinyget") (r "^1.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "02akmdn2pgdx9liinxhxvj01k458lx4ic06qd5pnqlbd5ycnq5g1")))

(define-public crate-cargo-ibuild-1.1.0 (c (n "cargo-ibuild") (v "1.1.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "tinyget") (r "^1.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "18fdc2yfz6fv1783x2wv29bkdpsdj1krsgscnfxw5zqgn6pgsv9p")))

(define-public crate-cargo-ibuild-1.1.1 (c (n "cargo-ibuild") (v "1.1.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "tinyget") (r "^1.0.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "0gpikxhcngnpv9mkwflq4a45k4g76hiyv4zbfngjx9ry1cnzlnrc")))

