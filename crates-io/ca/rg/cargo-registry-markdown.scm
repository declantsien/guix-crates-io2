(define-module (crates-io ca rg cargo-registry-markdown) #:use-module (crates-io))

(define-public crate-cargo-registry-markdown-0.1.0 (c (n "cargo-registry-markdown") (v "0.1.0") (d (list (d (n "ammonia") (r "=3.1.2") (d #t) (k 0)) (d (n "comrak") (r "=0.12.1") (k 0)) (d (n "htmlescape") (r "=0.3.1") (d #t) (k 0)) (d (n "url") (r "=2.2.2") (d #t) (k 0)))) (h "1b2z3hzqldzi5zx3n1ssjx2brfhzryr8ri57yyaylm70spy4lpgk")))

(define-public crate-cargo-registry-markdown-0.1.1 (c (n "cargo-registry-markdown") (v "0.1.1") (d (list (d (n "ammonia") (r "^3.1.2") (d #t) (k 0)) (d (n "comrak") (r "=0.12.1") (k 0)) (d (n "htmlescape") (r "=0.3.1") (d #t) (k 0)) (d (n "url") (r "=2.2.2") (d #t) (k 0)))) (h "01ll6l70s0hbkwzpsb1n7107qnn9faa374wkhkw7cirsza628sx0")))

