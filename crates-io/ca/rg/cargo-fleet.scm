(define-module (crates-io ca rg cargo-fleet) #:use-module (crates-io))

(define-public crate-cargo-fleet-0.0.1 (c (n "cargo-fleet") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive" "cargo" "std" "color"))) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "wsl") (r "^0.1.0") (d #t) (k 0)))) (h "1zfp4il48i2xx6ibsh2f49c21i85z64819wwywyhyg4qcf4qi95p")))

