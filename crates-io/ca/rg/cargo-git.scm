(define-module (crates-io ca rg cargo-git) #:use-module (crates-io))

(define-public crate-cargo-git-0.1.0 (c (n "cargo-git") (v "0.1.0") (h "1jzb27kr7blyx7na6x2w127vgnmps6lcv1wbm0al7vg4xw7l636f") (y #t)))

(define-public crate-cargo-git-0.2.0 (c (n "cargo-git") (v "0.2.0") (d (list (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "040hirckf9fw61cwh65lp0pm7ndbqv1z6fzxxix50cln7n0mn11r")))

(define-public crate-cargo-git-0.2.1 (c (n "cargo-git") (v "0.2.1") (d (list (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "04h1pc4h163a682hvjmhzx851dfcvbl6lmaah5a5zqi2846gxv7q")))

(define-public crate-cargo-git-0.3.0 (c (n "cargo-git") (v "0.3.0") (d (list (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1326xschd3apbzd77mpqlvrn463qm4ligx5ck9z9y57ihrsh4513")))

(define-public crate-cargo-git-0.3.1 (c (n "cargo-git") (v "0.3.1") (d (list (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "01lnysbvcm92iv7hf46pn5jz6yyd0p6p1529635599qh77shzqnw")))

(define-public crate-cargo-git-0.4.0 (c (n "cargo-git") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.17.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "02lwyvvxi9d8msc5mqwzgnl2z6z6c1s8vwh92agcshzhgcsarg0h")))

(define-public crate-cargo-git-0.5.0 (c (n "cargo-git") (v "0.5.0") (d (list (d (n "bitvec") (r "^0.17.2") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "06hwaj592b0c6q110vvzjm4y807xiim0qd67l6ampz64jcnz3fsh")))

(define-public crate-cargo-git-0.5.1 (c (n "cargo-git") (v "0.5.1") (d (list (d (n "bitvec") (r "^0.17.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "049lxv42irgji7y1v4clphzs2ss2c2zlwfc4j0wpcmzia4x0n2jl")))

(define-public crate-cargo-git-0.6.0 (c (n "cargo-git") (v "0.6.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "19mmz2k2clslxkwr7ha6nchylbyx7db8b05q2a4iyy8863w82w8d")))

(define-public crate-cargo-git-0.7.0 (c (n "cargo-git") (v "0.7.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "13326maqdjna0y4p24cb8mrf7rsw0bmwyk3r8xg70gsr90nxl87f")))

