(define-module (crates-io ca rg cargo-junit) #:use-module (crates-io))

(define-public crate-cargo-junit-0.7.0 (c (n "cargo-junit") (v "0.7.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "12jl1ckvqkisaillsd09a2psv07afrzjmjh73z6x6083grh0lh0f")))

(define-public crate-cargo-junit-0.7.1 (c (n "cargo-junit") (v "0.7.1") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "12i35qk9917s2zlx0mwrsnh7518gvdrr99wy4ky7bmzkb47ffn0l")))

(define-public crate-cargo-junit-0.7.2 (c (n "cargo-junit") (v "0.7.2") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1riz6b65xr0jl4h8jkrgyps5334syv95nxvw3cwyk42f6l2r6cay")))

(define-public crate-cargo-junit-0.7.3 (c (n "cargo-junit") (v "0.7.3") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1cdwndsakkxq817y0y2yynwp6dy2pj4nc1a5y82mwg1d9xyy5753")))

(define-public crate-cargo-junit-0.7.4 (c (n "cargo-junit") (v "0.7.4") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1kyzyv0nmr0n2d8l80iwiisvf4061dm57d0jifd5n9vq7p2f8cqi")))

(define-public crate-cargo-junit-0.8.0 (c (n "cargo-junit") (v "0.8.0") (d (list (d (n "cargo-results") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0plm25d6xb3lws65na5gpxkxfkj89dc16jk12cyrpshknswwxn1a")))

