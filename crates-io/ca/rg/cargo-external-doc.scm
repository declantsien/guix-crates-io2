(define-module (crates-io ca rg cargo-external-doc) #:use-module (crates-io))

(define-public crate-cargo-external-doc-0.1.0 (c (n "cargo-external-doc") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.22") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "0mia7fh562fbd75ldr9xpijp54rq4ccb0dxzwb2hn5mhkw5lsyjb")))

(define-public crate-cargo-external-doc-0.2.0 (c (n "cargo-external-doc") (v "0.2.0") (d (list (d (n "handlebars") (r "^0.22") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "12da9j149hb54gljbxxbmbjcp7ml3didgghagbv6x6ms43y5ych4")))

