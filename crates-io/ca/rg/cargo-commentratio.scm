(define-module (crates-io ca rg cargo-commentratio) #:use-module (crates-io))

(define-public crate-cargo-commentratio-0.0.1 (c (n "cargo-commentratio") (v "0.0.1") (d (list (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.9.0") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0w0k0r23srvqbz13dqf1qxnqm6k5lza01zlr6xnx8fy4sd3r9wzy")))

