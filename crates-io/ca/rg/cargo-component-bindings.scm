(define-module (crates-io ca rg cargo-component-bindings) #:use-module (crates-io))

(define-public crate-cargo-component-bindings-0.0.0 (c (n "cargo-component-bindings") (v "0.0.0") (h "1l83gmmjr5gh59xn3ijfr83b36ircnkrr6il35dgfbh1zpsc3y3i")))

(define-public crate-cargo-component-bindings-0.2.0 (c (n "cargo-component-bindings") (v "0.2.0") (d (list (d (n "cargo-component-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.12.0") (d #t) (k 0)))) (h "18rc1i1rlg5g2ldwkxlax2jvl7d8wrg52anvm98mgzflwplpvmq6")))

(define-public crate-cargo-component-bindings-0.3.0 (c (n "cargo-component-bindings") (v "0.3.0") (d (list (d (n "cargo-component-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.13.0") (d #t) (k 0)))) (h "0i0s6b43csg463xdpd4qbbb6nzfqmf17ihlpv501mvh64m7pc7g8")))

(define-public crate-cargo-component-bindings-0.3.1 (c (n "cargo-component-bindings") (v "0.3.1") (d (list (d (n "cargo-component-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.13.0") (d #t) (k 0)))) (h "1vwpy63gbkj554n4mx7ki4agpmsrd8zldpa1xkm30b76s6v0fgh2")))

(define-public crate-cargo-component-bindings-0.4.0 (c (n "cargo-component-bindings") (v "0.4.0") (d (list (d (n "cargo-component-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.13.0") (f (quote ("realloc"))) (k 0)))) (h "1faapssvbwb4ky4imxvw9ai5hv3plmafdvah2avd3z0awn7gr4rc")))

(define-public crate-cargo-component-bindings-0.4.1 (c (n "cargo-component-bindings") (v "0.4.1") (d (list (d (n "cargo-component-macro") (r "^0.4.1") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.13.0") (f (quote ("realloc"))) (k 0)))) (h "126k7jb1z0yh3dpjxgk2c5znxy5hmrmwb5ln80j1j4ni1sfa2y1d")))

(define-public crate-cargo-component-bindings-0.5.0 (c (n "cargo-component-bindings") (v "0.5.0") (d (list (d (n "cargo-component-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.14.0") (f (quote ("realloc"))) (k 0)))) (h "1ymbijig11wyw0zpz8vdnwvdk0ah2gmm4j8i27vsmqg41hsz4g5b")))

(define-public crate-cargo-component-bindings-0.6.0 (c (n "cargo-component-bindings") (v "0.6.0") (d (list (d (n "cargo-component-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.16.0") (f (quote ("realloc"))) (k 0)))) (h "0167n9w3s7qkc10r2zwlb16xw1ylwsz9ga5fjwygj1qyhax4hpjl")))

