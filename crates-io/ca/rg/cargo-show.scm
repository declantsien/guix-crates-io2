(define-module (crates-io ca rg cargo-show) #:use-module (crates-io))

(define-public crate-cargo-show-0.1.0 (c (n "cargo-show") (v "0.1.0") (d (list (d (n "cargo") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1200q1ffpjyf5k3nw4bxpljgpfrq7fczhh5la9c3zhvpv3all6y0")))

(define-public crate-cargo-show-0.2.0 (c (n "cargo-show") (v "0.2.0") (d (list (d (n "cargo") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1pw2rwzz2cnimiqhy6c1p8dp8dhidp0zzir3k15fb7f8vmjvbr8b")))

(define-public crate-cargo-show-0.2.2 (c (n "cargo-show") (v "0.2.2") (d (list (d (n "cargo") (r "^0.12.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.82") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1laqn0gxjh1l0lmmy91i8mn7j1lijdq9d4zcq2rbkb5v95kdkymj")))

(define-public crate-cargo-show-0.3.0 (c (n "cargo-show") (v "0.3.0") (d (list (d (n "cargo") (r "^0.17.0") (d #t) (k 0)) (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jh1bpwgrdadgjz5jyrqps7g0qk14pa5xr5rgfjc8liswpqbpv3v")))

(define-public crate-cargo-show-0.4.0 (c (n "cargo-show") (v "0.4.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.8.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0s5y3b6a7kj0pki810y3xg4xnwx8hslvqgnbzb43zi0b7nlasd33")))

(define-public crate-cargo-show-0.5.0 (c (n "cargo-show") (v "0.5.0") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f28iqilk2r85xz3dmqqv913chy23x355x5938n0bnwhq2i14a02")))

(define-public crate-cargo-show-0.5.1 (c (n "cargo-show") (v "0.5.1") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bm7mvq6mv3d0f31mldg22qphkzfp6hizh16clnzk78pi52c7hs7") (y #t)))

(define-public crate-cargo-show-0.5.2 (c (n "cargo-show") (v "0.5.2") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xs5lacrzl0n0p6fswzr0zp4hsf6z22v2k7lhlpa1wgc0qgn8c3q")))

(define-public crate-cargo-show-0.5.3 (c (n "cargo-show") (v "0.5.3") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mjj3khci835k8r6j70bibblgjiyi2544zccwa6spjlz7xnny8l8")))

(define-public crate-cargo-show-0.5.4 (c (n "cargo-show") (v "0.5.4") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q3zvzv6skl6cz0dplgqh7rjhqs9zfdlh713r9drd6yq75v8d9h6")))

(define-public crate-cargo-show-0.5.5 (c (n "cargo-show") (v "0.5.5") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09pillirm50md6y7d650rg1kr8l6i13mm0ss10jlxczqfk4ypipi")))

(define-public crate-cargo-show-0.5.6 (c (n "cargo-show") (v "0.5.6") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p89d8img6rw9zvxafk0bwkwmsya22zjn2cq1b2mcsr4bz5n3ijk")))

(define-public crate-cargo-show-0.5.7 (c (n "cargo-show") (v "0.5.7") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05ybavkxcnx4v22vln8771bp9vl6wmz4dksarwh1yyakqzc9hzzi")))

(define-public crate-cargo-show-0.5.8 (c (n "cargo-show") (v "0.5.8") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1shh404jpr14nad66mgdq2vl7ym6r5wf4pgy9rjcv774fjn7rrnl")))

(define-public crate-cargo-show-0.5.9 (c (n "cargo-show") (v "0.5.9") (d (list (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lbj2dihqpsw9qgfjz7r6cfsnrvzsiz4lwc4mkpyi4bspd2ihhin")))

(define-public crate-cargo-show-0.6.0 (c (n "cargo-show") (v "0.6.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.40") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "072wwn371vs91val1av9bpba3409a3vwfwnsfmdjlpcajmvmcv16")))

