(define-module (crates-io ca rg cargo-build-deps) #:use-module (crates-io))

(define-public crate-cargo-build-deps-0.1.0 (c (n "cargo-build-deps") (v "0.1.0") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "04czis9k8qyq14v8fi9fpyiznbknm0j3zlcs8ndaqsibrq6s3dbl")))

(define-public crate-cargo-build-deps-0.1.1 (c (n "cargo-build-deps") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1az8g00psx8lrp0f4w5r00jvk2wz4fxhxhdfv9ha9c3ld66wlcph") (y #t)))

(define-public crate-cargo-build-deps-0.1.2 (c (n "cargo-build-deps") (v "0.1.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "049diwx86wyw8dpsa5sw8g6v9jjbfwak9c6rrxxcfs9187pn7h09") (y #t)))

(define-public crate-cargo-build-deps-0.1.3 (c (n "cargo-build-deps") (v "0.1.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0p5wvkzlbazqi7j1vsq88zpyckrl8wv51zp53kq60s08ih2qy6n9")))

(define-public crate-cargo-build-deps-0.1.4 (c (n "cargo-build-deps") (v "0.1.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1199fdvim1xyzzwfgd65rlx82sp5b15pxq052q4jmn38s58ajwpc")))

(define-public crate-cargo-build-deps-0.1.5 (c (n "cargo-build-deps") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q80j6p7v2wnsm99gansavkbvwnc00ik71k4aiwja5v0gp2i1xm2")))

(define-public crate-cargo-build-deps-0.2.0 (c (n "cargo-build-deps") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02nfbwhlq8kr0pvwghrq2byyhl2n1njrj00y2iv7050h5c4v932h")))

