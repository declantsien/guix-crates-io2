(define-module (crates-io ca rg cargo-find) #:use-module (crates-io))

(define-public crate-cargo-find-0.1.1 (c (n "cargo-find") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "1wgs0cqg4iy2x6iqhi20fvs3za4yl1z3yi7hvy1vm0z8p142k69d")))

(define-public crate-cargo-find-0.1.2 (c (n "cargo-find") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "1vn9fvhfyvmgyg0mjlx2wvh2c1hvws8d365w8j2yci41lifygsxn")))

(define-public crate-cargo-find-0.1.3 (c (n "cargo-find") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "1cnr6kz41z3z6kfhshrjc3n19nw9nalqgal5557zjzsf4dppfj8q")))

(define-public crate-cargo-find-0.1.4 (c (n "cargo-find") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "17ax51zzly3css40691gwbfhnxpmpf4bcbjfavx8314y9x97zfh2")))

(define-public crate-cargo-find-0.1.5 (c (n "cargo-find") (v "0.1.5") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0rh1jbz0zzmq83x1kgb38111kcxrvhi078b1qng28dmr92xglw5v")))

(define-public crate-cargo-find-0.1.6 (c (n "cargo-find") (v "0.1.6") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "04pn10r19v255x8babnl1q6hi53klnias30khmqswv8lv0ffr77p")))

