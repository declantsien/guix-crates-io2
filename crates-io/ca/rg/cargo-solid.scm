(define-module (crates-io ca rg cargo-solid) #:use-module (crates-io))

(define-public crate-cargo-solid-0.1.0 (c (n "cargo-solid") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "0l3rkyhf4xxwxwm2lmmg83y1cbxxdrf5d5v0i2dp8dyjqq7d9g1w")))

(define-public crate-cargo-solid-0.1.1 (c (n "cargo-solid") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "0nn4fji7xs95r4pwc24qrzfd42wsxm4y1wn345m9mnbbb4y99viz")))

(define-public crate-cargo-solid-0.1.2 (c (n "cargo-solid") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "1fs6dakbgja8y2m7wfccghdp255yf6b3jdfz602ynyv5zaggwqln")))

(define-public crate-cargo-solid-0.1.3 (c (n "cargo-solid") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "18h1jw46gqbq3ccdbj06vghdjssgnkjiqwbsg6p1p4yb07x0bdqn")))

(define-public crate-cargo-solid-0.1.4 (c (n "cargo-solid") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "1j8i87pvji145szhqw992pcxbxi63387x4vldw3aik28zshj3hyd")))

(define-public crate-cargo-solid-0.1.5 (c (n "cargo-solid") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)))) (h "11dw0lzdjlnzgy8jas6vsznnfr1l0a648ya039g3dk3jyfvla13g")))

