(define-module (crates-io ca rg cargo-extract) #:use-module (crates-io))

(define-public crate-cargo-extract-0.1.0 (c (n "cargo-extract") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "10rb80wshlpbn7dp6wzzxpmz71s69nly36fk81jws9wciidcc4mv")))

(define-public crate-cargo-extract-0.2.0 (c (n "cargo-extract") (v "0.2.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0bzpjcxyhd3xqmsj23hzvywjkibnm250xkzhm0nqkrhpzxd1myhc")))

(define-public crate-cargo-extract-0.3.0 (c (n "cargo-extract") (v "0.3.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0ldrfhi94hj87c896kl465yz0whi67w0g0x1ciwm1vjwba02vdfr")))

