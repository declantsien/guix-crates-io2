(define-module (crates-io ca rg cargo-difftests-testclient) #:use-module (crates-io))

(define-public crate-cargo-difftests-testclient-0.1.0-alpha.1 (c (n "cargo-difftests-testclient") (v "0.1.0-alpha.1") (d (list (d (n "cargo-difftests-core") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sycb9skvkzr2xx8z3c36m9ajd92s2f464h2vxj7m31783wnc3hc")))

(define-public crate-cargo-difftests-testclient-0.1.0-alpha.2 (c (n "cargo-difftests-testclient") (v "0.1.0-alpha.2") (d (list (d (n "cargo-difftests-core") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j5vpygk9nhkn4j0qhw6h2vyzm9xgc0j6w1m79hf796cm9rsbxgj")))

(define-public crate-cargo-difftests-testclient-0.1.0-alpha.3 (c (n "cargo-difftests-testclient") (v "0.1.0-alpha.3") (d (list (d (n "cargo-difftests-core") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16a10l8md5dchkwhx9nqsgayr6slavylkr2qmxij19f6wjmgylig")))

(define-public crate-cargo-difftests-testclient-0.1.0 (c (n "cargo-difftests-testclient") (v "0.1.0") (d (list (d (n "cargo-difftests-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09sa11hmg85cpabwfiy2q65w9nablivpck2gm64a32zkj8ssli4w")))

(define-public crate-cargo-difftests-testclient-0.2.0 (c (n "cargo-difftests-testclient") (v "0.2.0") (d (list (d (n "cargo-difftests-core") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1025xgmwbg27phyq0ygjmsw4lva7kkni1zrblv1vlvnxlrp6dpw7") (f (quote (("default"))))))

(define-public crate-cargo-difftests-testclient-0.3.0 (c (n "cargo-difftests-testclient") (v "0.3.0") (d (list (d (n "cargo-difftests-core") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fqlyfdc72bhbz2j8kdk659by6jkkrxhq98y5hm3a9g8d04v8j2l") (f (quote (("default"))))))

(define-public crate-cargo-difftests-testclient-0.4.0 (c (n "cargo-difftests-testclient") (v "0.4.0") (d (list (d (n "cargo-difftests-core") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mf15c3ypmv8f06krl5d45ynlvl5ppk8snm80dfkhq7jkx9x1k61") (f (quote (("default"))))))

(define-public crate-cargo-difftests-testclient-0.5.0 (c (n "cargo-difftests-testclient") (v "0.5.0") (d (list (d (n "cargo-difftests-core") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cq4xw4ali9yvp2kfbqnhwz5601p02478drm2ivxcdxd42ic42d6") (f (quote (("parallel-groups" "groups") ("groups" "enforce-single-running-test" "cargo-difftests-core/groups") ("enforce-single-running-test") ("default" "enforce-single-running-test" "groups" "parallel-groups"))))))

(define-public crate-cargo-difftests-testclient-0.6.0 (c (n "cargo-difftests-testclient") (v "0.6.0") (d (list (d (n "cargo-difftests-core") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yb3ph8bkggbq1917wmlh6v7wyghqkha1v1zr6x6a52l461cwgds") (f (quote (("default") ("compile-index-and-clean"))))))

(define-public crate-cargo-difftests-testclient-0.6.1 (c (n "cargo-difftests-testclient") (v "0.6.1") (d (list (d (n "cargo-difftests-core") (r "=0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1smkrcm6hl4ysnppq7p45wcd7m5y1damyyidq6zbrn90j1485p9q") (f (quote (("default") ("compile-index-and-clean"))))))

