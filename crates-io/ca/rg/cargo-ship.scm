(define-module (crates-io ca rg cargo-ship) #:use-module (crates-io))

(define-public crate-cargo-ship-0.1.0 (c (n "cargo-ship") (v "0.1.0") (d (list (d (n "toml") (r "*") (d #t) (k 0)))) (h "1p7qn5nva8f1lk2r4dymxkpmw9pdbq4mjf41g4c2w0z0d7d77ln5")))

(define-public crate-cargo-ship-0.1.1 (c (n "cargo-ship") (v "0.1.1") (d (list (d (n "toml") (r "*") (d #t) (k 0)))) (h "128aj50gs65c5q2bggd2a4i3mh6m4m5jx06jsdagdpp1sy4ads0k")))

