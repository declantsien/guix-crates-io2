(define-module (crates-io ca rg cargo-nix) #:use-module (crates-io))

(define-public crate-cargo-nix-0.1.0-mvp (c (n "cargo-nix") (v "0.1.0-mvp") (d (list (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)) (d (n "tracing-error") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.15") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1zk43cv040gs8v39xnh56rwm127lm894pxp3awpwbbrld1vy87nv")))

