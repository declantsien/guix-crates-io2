(define-module (crates-io ca rg cargo-cranky) #:use-module (crates-io))

(define-public crate-cargo-cranky-0.1.0 (c (n "cargo-cranky") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0sbxr442r2pmm2jn6gxcd5kbr0mhv3qdzg3i0flc2yk3v9cnp5as") (r "1.56")))

(define-public crate-cargo-cranky-0.1.1 (c (n "cargo-cranky") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0f9qbr86397rxkwbkq2frd11k9kn8glmcrkjicl3gpblz0q849cm") (r "1.56")))

(define-public crate-cargo-cranky-0.2.0 (c (n "cargo-cranky") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0lxzdryga46c2v1ww9dmn63vfazgndqvn1pgggw17lcp3awpspy8") (r "1.56")))

(define-public crate-cargo-cranky-0.3.0 (c (n "cargo-cranky") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1nyj7yfcknb1a50cin8ra5l30nw7wvdimsz04nlsmmydcxf8rvmm") (r "1.56")))

