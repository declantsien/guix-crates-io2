(define-module (crates-io ca rg cargo-squat) #:use-module (crates-io))

(define-public crate-cargo-squat-0.0.0 (c (n "cargo-squat") (v "0.0.0") (h "0id537qz31b600xhg51m7y0vxawc86mk90lhayf89xd64bafx32x")))

(define-public crate-cargo-squat-0.1.0 (c (n "cargo-squat") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1w1ra3x3iqpzyjm9l6v382qdkqqlhvdr0qmj7y1s0qg04yhnkp3g")))

