(define-module (crates-io ca rg cargo-watch-plus) #:use-module (crates-io))

(define-public crate-cargo-watch-plus-0.1.0 (c (n "cargo-watch-plus") (v "0.1.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "home") (r "^0.5.4") (d #t) (k 1)) (d (n "which") (r "^4.4.0") (d #t) (k 1)))) (h "070qh3rqna3brhlcvsl29slzqprak4qyfnaip637hmw3vf6lpxgk")))

(define-public crate-cargo-watch-plus-0.2.0 (c (n "cargo-watch-plus") (v "0.2.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "home") (r "^0.5.4") (d #t) (k 1)) (d (n "which") (r "^4.4.0") (d #t) (k 1)))) (h "0ax3yhydh2vjjvmg31nyi27gz4rdwjhks9zll3d7bkcdh92z6dhr")))

