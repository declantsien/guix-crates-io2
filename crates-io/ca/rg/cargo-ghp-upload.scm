(define-module (crates-io ca rg cargo-ghp-upload) #:use-module (crates-io))

(define-public crate-cargo-ghp-upload-0.1.0 (c (n "cargo-ghp-upload") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "1yk5b706fn69bglh4fg9hynx82gi79ns96c1xbh5rh573yzgfq42") (y #t)))

(define-public crate-cargo-ghp-upload-0.1.1 (c (n "cargo-ghp-upload") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "1wrcpy41h239jv6laxasxibl2gpbsj8n6mq7km1v807f4k9jxx97") (y #t)))

(define-public crate-cargo-ghp-upload-0.2.0 (c (n "cargo-ghp-upload") (v "0.2.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "0lxijmd7p23v089a52q5d16j3vw5w03m8wmqknmidsmjll5g4lmi") (y #t)))

(define-public crate-cargo-ghp-upload-0.3.0 (c (n "cargo-ghp-upload") (v "0.3.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "1cnwa8n25h1cfqdywl6x039mwhiia5g0dcjvj1jfl014hbz0slfq") (y #t)))

(define-public crate-cargo-ghp-upload-0.3.1 (c (n "cargo-ghp-upload") (v "0.3.1") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "1i96rwi7jg8xaafjfjjr7afyagikdqh83kw60l401dkdbd935pbh")))

(define-public crate-cargo-ghp-upload-0.3.2 (c (n "cargo-ghp-upload") (v "0.3.2") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "0lidlva8fgx32nlickynfg51m31rcfyaw0f9fg0f76qd5jvy9qvx")))

(define-public crate-cargo-ghp-upload-0.3.3 (c (n "cargo-ghp-upload") (v "0.3.3") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "0ckpg408k6slap4fr7szfjz3pfg2yb5nfwz5f46ya6avwk4s7mjl") (y #t)))

(define-public crate-cargo-ghp-upload-0.3.4 (c (n "cargo-ghp-upload") (v "0.3.4") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2") (d #t) (k 0)))) (h "0k6cl6gjpny93cw40hlgwanqb47yggxg4hfi8g8ig9z8fhv1hnma")))

