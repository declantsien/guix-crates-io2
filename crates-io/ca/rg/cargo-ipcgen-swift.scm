(define-module (crates-io ca rg cargo-ipcgen-swift) #:use-module (crates-io))

(define-public crate-cargo-ipcgen-swift-0.0.0 (c (n "cargo-ipcgen-swift") (v "0.0.0") (d (list (d (n "syntex_errors") (r "^0.59.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1mp1cpsb8zdhpc3b2dirc862jjipy48iq12dv2dr1ksqpaw66vb1")))

(define-public crate-cargo-ipcgen-swift-0.0.1 (c (n "cargo-ipcgen-swift") (v "0.0.1") (d (list (d (n "rls-analysis") (r "^0.13.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.59.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "17fg1h3v9zp3c2ifyz3gz0mkwzqfpsdd1aqq1iq6kdhpp47bb2i5")))

