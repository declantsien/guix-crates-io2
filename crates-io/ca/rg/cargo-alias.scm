(define-module (crates-io ca rg cargo-alias) #:use-module (crates-io))

(define-public crate-cargo-alias-0.1.0 (c (n "cargo-alias") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1bp1pvn5jscp56dmzj0iq6m7hvn5fjaysf8qq8mwrsfmdbmkvgax") (y #t)))

(define-public crate-cargo-alias-0.2.0 (c (n "cargo-alias") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "16vzk70x9l347m0w0mhvvg2xdcqkcyabsv683p3kil0x5c92g90y") (y #t)))

(define-public crate-cargo-alias-0.2.1 (c (n "cargo-alias") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "0nxgx3jrqxlpbs96s1klx2accwqplfmp1vmq0vwmxrfzs5q8ngg7") (y #t)))

(define-public crate-cargo-alias-0.2.2 (c (n "cargo-alias") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "1fgqjv95kf091a9lzwm08nk2n229hk29mn44pm2238dai6fs1398")))

