(define-module (crates-io ca rg cargo-freeze) #:use-module (crates-io))

(define-public crate-cargo-freeze-0.0.1 (c (n "cargo-freeze") (v "0.0.1") (h "1z4yqd570a6xk9s497048dvdipcpgficfn3k73q7d81icnamfrfl")))

(define-public crate-cargo-freeze-0.0.1-alpha0 (c (n "cargo-freeze") (v "0.0.1-alpha0") (h "1j5mfgwwk015wkfpdmgm57p31fhvkd8dc95fs0zm13jmz514xm85")))

(define-public crate-cargo-freeze-0.0.2 (c (n "cargo-freeze") (v "0.0.2") (h "04alpqa8i637p9shjc81ww5slbwkfs616ck0p5xg6i8g5bhhm1ly")))

(define-public crate-cargo-freeze-0.0.3 (c (n "cargo-freeze") (v "0.0.3") (h "1g2nhgdb7yc9yln813k2j5w8nkczilmz7m7g4awvr965vxcdmvfc")))

