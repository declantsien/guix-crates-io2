(define-module (crates-io ca rg cargo-suicide) #:use-module (crates-io))

(define-public crate-cargo-suicide-0.1.1 (c (n "cargo-suicide") (v "0.1.1") (h "0mni18hlp7qad65vfkhbah72fj59id4pm6wi0v8qjjr74kfn48b8")))

(define-public crate-cargo-suicide-0.1.2 (c (n "cargo-suicide") (v "0.1.2") (h "0qvm5kfivqgavv2rawzvfi0a0cr012m77aqpp4qaibp9gssnir3n")))

