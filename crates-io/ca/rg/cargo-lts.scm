(define-module (crates-io ca rg cargo-lts) #:use-module (crates-io))

(define-public crate-cargo-lts-0.3.0-alpha.1 (c (n "cargo-lts") (v "0.3.0-alpha.1") (d (list (d (n "lts") (r "=0.3.0-alpha.1") (d #t) (k 0)))) (h "1rb8ybf37fppw3zslzjpm811k32gp5i7wkdzs092j80dxnq49s1f") (y #t)))

(define-public crate-cargo-lts-0.3.0 (c (n "cargo-lts") (v "0.3.0") (d (list (d (n "lts") (r "^0.3.0") (d #t) (k 0)))) (h "18v86av59xa3m8yrl0bhf2rs75zjy96w6bjggmg3m345nrqg2d7c")))

