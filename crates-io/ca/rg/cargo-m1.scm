(define-module (crates-io ca rg cargo-m1) #:use-module (crates-io))

(define-public crate-cargo-m1-0.1.0 (c (n "cargo-m1") (v "0.1.0") (d (list (d (n "cargo") (r "~0.56.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.3") (f (quote ("suggestions" "color"))) (k 0)))) (h "0fz40w3bv5bvama8q7m31ghmy6yzgddavl62dg72l7zdy334zxvq")))

(define-public crate-cargo-m1-0.1.1 (c (n "cargo-m1") (v "0.1.1") (d (list (d (n "cargo") (r "~0.56.0") (f (quote ("vendored-openssl"))) (k 0)) (d (n "clap") (r "~2.33.3") (f (quote ("suggestions" "color"))) (k 0)))) (h "1a47yjz9qbkg75p5a2x9sg36m48kq49izhlcd14xvvzrfv5p2am1")))

