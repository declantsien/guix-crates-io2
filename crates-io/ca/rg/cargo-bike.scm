(define-module (crates-io ca rg cargo-bike) #:use-module (crates-io))

(define-public crate-cargo-bike-0.0.0 (c (n "cargo-bike") (v "0.0.0") (h "07iivzj5cv9cz7n9cm7nm7d8x3z16ai7fz00mn44yq2xsjfagd7x")))

(define-public crate-cargo-bike-0.0.1 (c (n "cargo-bike") (v "0.0.1") (h "1w3ywscq40x9dh2j0bwmd3vpy9qz87abvkrxyj6r2fl9gynzswdl")))

