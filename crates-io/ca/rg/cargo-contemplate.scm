(define-module (crates-io ca rg cargo-contemplate) #:use-module (crates-io))

(define-public crate-cargo-contemplate-0.1.0 (c (n "cargo-contemplate") (v "0.1.0") (h "05dimz1wdlz90r4v8rd24bv64750yllrlzqllxwnwrrqhcsskpqm")))

(define-public crate-cargo-contemplate-0.1.1 (c (n "cargo-contemplate") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "19y57fd68b0df4a780fqp579k2zay7hz09b4q8fsvmlqgqrg3102")))

(define-public crate-cargo-contemplate-0.2.1 (c (n "cargo-contemplate") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.0") (f (quote ("derive" "unstable-doc"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0b3qapz5nfgrwnf39q916dvfw0pgmnpyf7wf4v7bi4ksg4873bgg")))

(define-public crate-cargo-contemplate-0.2.2 (c (n "cargo-contemplate") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.0") (f (quote ("derive" "unstable-doc"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0appa0pj7g9lh5grdrs8a7l1nkdc1y4fccv6j39a8h48zalg49yl")))

