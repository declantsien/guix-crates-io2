(define-module (crates-io ca rg cargo_advanced) #:use-module (crates-io))

(define-public crate-cargo_advanced-0.1.0 (c (n "cargo_advanced") (v "0.1.0") (h "1ffzr9wzwf6xwpajjnyavkaqxjvgam1bdkkk8b8npgzq5df4xa81")))

(define-public crate-cargo_advanced-0.1.1 (c (n "cargo_advanced") (v "0.1.1") (h "05sgqgk5vln3ajdvjbbwcwkjvxnc41zdfyza1kcs72bkg1a7z3ma") (y #t)))

(define-public crate-cargo_advanced-0.1.2 (c (n "cargo_advanced") (v "0.1.2") (h "093d37piyw4pa3yh7x64pbyiqfx7c4h67l7wskvbgcdahk5kphis")))

