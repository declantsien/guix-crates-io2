(define-module (crates-io ca rg cargo-wa) #:use-module (crates-io))

(define-public crate-cargo-wa-0.1.0 (c (n "cargo-wa") (v "0.1.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "1pyvy89d40mglsjk1h2r06d34zqsmrydv2yhrw9ygs5my9miqmz7") (y #t)))

(define-public crate-cargo-wa-0.1.1 (c (n "cargo-wa") (v "0.1.1") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "0j51gbq01z1mkipax8lzk7kawwjg0c9lk8cip95yawfd4mharh60") (y #t)))

