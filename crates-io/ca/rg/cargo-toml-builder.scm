(define-module (crates-io ca rg cargo-toml-builder) #:use-module (crates-io))

(define-public crate-cargo-toml-builder-0.1.0 (c (n "cargo-toml-builder") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0dn0y8hhk2sr4vycldnx990l56yinzx4m8afwxz51ap0xvxwkmqx")))

(define-public crate-cargo-toml-builder-0.2.0 (c (n "cargo-toml-builder") (v "0.2.0") (d (list (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "1z2dcayf1mkk1s8xmr4z1g1a2rqivgzcy8rfwkygfrb5mfnn0vbr")))

(define-public crate-cargo-toml-builder-0.3.0 (c (n "cargo-toml-builder") (v "0.3.0") (d (list (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "0kwiw29i9fp1zv3ncc1wr4a290aq2i4yj1g7vcd9czc86x11x0hl")))

