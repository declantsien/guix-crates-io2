(define-module (crates-io ca rg cargo-testf) #:use-module (crates-io))

(define-public crate-cargo-testf-0.1.0 (c (n "cargo-testf") (v "0.1.0") (h "173qbwl9s2c93gavlzs041nlalq21wp2qpp7av0nv0s2s45rw15y") (y #t)))

(define-public crate-cargo-testf-0.1.1 (c (n "cargo-testf") (v "0.1.1") (h "0zy61qr0qrscjvrwsra0yxg7i7zzbbh8c3sny4gxkkbwirmzshi7") (y #t)))

(define-public crate-cargo-testf-0.1.2 (c (n "cargo-testf") (v "0.1.2") (h "1fbafb6l3yzimqh8ir3jrya7aqns832dhc8w7h9nhi8fd2374a0d") (y #t)))

(define-public crate-cargo-testf-0.1.3 (c (n "cargo-testf") (v "0.1.3") (h "12pwzp3qwrnqijsmcz7qyrcxdyfq81rj4mqpag066cn910q1zbg2")))

