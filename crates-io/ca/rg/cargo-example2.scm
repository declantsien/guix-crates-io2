(define-module (crates-io ca rg cargo-example2) #:use-module (crates-io))

(define-public crate-cargo-example2-0.1.0 (c (n "cargo-example2") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "035pz7751wn9nj6q0wjgplx9f8kyh6j7qiappmfgl3d4l08gfvzk")))

