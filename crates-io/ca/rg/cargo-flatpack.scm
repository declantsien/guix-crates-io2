(define-module (crates-io ca rg cargo-flatpack) #:use-module (crates-io))

(define-public crate-cargo-flatpack-0.0.0 (c (n "cargo-flatpack") (v "0.0.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1jx4if5fpvz608aw0k5qzxmpashjk9vi3q9kqbjsskl4gb3rfgry")))

