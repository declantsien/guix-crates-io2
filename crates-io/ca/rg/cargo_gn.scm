(define-module (crates-io ca rg cargo_gn) #:use-module (crates-io))

(define-public crate-cargo_gn-0.0.1 (c (n "cargo_gn") (v "0.0.1") (h "0ws5sdc5bhv4pmf6r69wm4033ppda3jfln2lrqq1c4gyqijmcag9")))

(define-public crate-cargo_gn-0.0.2 (c (n "cargo_gn") (v "0.0.2") (h "01l2yi88ss527c9vk94xhv9wphfr0r3amvqz0n86cmczmvkv0apw")))

(define-public crate-cargo_gn-0.0.3 (c (n "cargo_gn") (v "0.0.3") (h "023kjkq3590lawsby5p0s8gsf40p78mpdhfrwn5xnpv2gz8mg7sa")))

(define-public crate-cargo_gn-0.0.4 (c (n "cargo_gn") (v "0.0.4") (h "1bb4kjd4s80r55d7hv424h0a4s52y9kjavb1xb91f1amlr9jm11k")))

(define-public crate-cargo_gn-0.0.5 (c (n "cargo_gn") (v "0.0.5") (h "0n21wd120r433k7c686ysg995m6fx736r1vqp945pl7ipch5cnmx")))

(define-public crate-cargo_gn-0.0.6 (c (n "cargo_gn") (v "0.0.6") (d (list (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "0jvlmw21d529l7sdhpl3a520nxzmy1hzc5qg3w69yhznq14kdzzg")))

(define-public crate-cargo_gn-0.0.7 (c (n "cargo_gn") (v "0.0.7") (d (list (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "078agyhq4b2a98xx3w2zf2lsiypnvwix1nmmnbmcqpsphpy8vkj2")))

(define-public crate-cargo_gn-0.0.8 (c (n "cargo_gn") (v "0.0.8") (d (list (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "1m0l5yzadn2f07vabc0gmf2hj2xm2d6qb11sf9kwhkq4sql8vgxp")))

(define-public crate-cargo_gn-0.0.9 (c (n "cargo_gn") (v "0.0.9") (d (list (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "0nj7f66kjvrsxssvmf4ax951cskzdamvrgj0fglymxnpacj1qybd")))

(define-public crate-cargo_gn-0.0.10 (c (n "cargo_gn") (v "0.0.10") (d (list (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "15f4pyhk5i55bh3s3ib41k7n7qqghgdhlbxnsgx7f4483dwvllzd")))

(define-public crate-cargo_gn-0.0.12 (c (n "cargo_gn") (v "0.0.12") (h "0nkcc0j611qr8q6gc1rlpr0ldzw4689c24vic4rbhvx25kxm0k53")))

(define-public crate-cargo_gn-0.0.13 (c (n "cargo_gn") (v "0.0.13") (h "0dmqmsgick0b0qnixyh7ww1z7b8z9qdsznzl4ndf2vmdih89qrqa")))

(define-public crate-cargo_gn-0.0.14 (c (n "cargo_gn") (v "0.0.14") (h "05k60v4q0pjkbki09x1hwkqq1jz6gnir5kp0jq89z51sg7sa4x89")))

(define-public crate-cargo_gn-0.0.15 (c (n "cargo_gn") (v "0.0.15") (h "106dsxgd5zkg887b8f5mv8hsb2rgr5v22kxi2k1wppq1nbvxg9sv")))

