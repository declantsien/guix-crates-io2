(define-module (crates-io ca rg cargo-armory) #:use-module (crates-io))

(define-public crate-cargo-armory-0.3.5 (c (n "cargo-armory") (v "0.3.5") (d (list (d (n "armory_lib") (r "^0.3.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)))) (h "1j022kd2alldvjzbjjwb1x82g1ysg1yq8976pvqls8dhb443xl1c")))

(define-public crate-cargo-armory-0.4.1 (c (n "cargo-armory") (v "0.4.1") (d (list (d (n "armory_lib") (r "^0.4.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)))) (h "1wg93035rmcjw0z6p7ncddzz12v52yd50c88vcawwbm8lxcfh39f")))

