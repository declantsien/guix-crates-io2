(define-module (crates-io ca rg cargo-lookup) #:use-module (crates-io))

(define-public crate-cargo-lookup-0.1.0 (c (n "cargo-lookup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "0i1krsympmzg7kpg50kb35cxw3q0s5lwwf98wq4l91k341pjcsm6") (f (quote (("default" "cli") ("cli" "clap")))) (r "1.76")))

