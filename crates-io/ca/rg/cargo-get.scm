(define-module (crates-io ca rg cargo-get) #:use-module (crates-io))

(define-public crate-cargo-get-0.1.1 (c (n "cargo-get") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0llrs75nvxgl57l1wwd0dz3v3ag9hfqw4cihwy4hqsl0gj3vpk9r")))

(define-public crate-cargo-get-0.1.2 (c (n "cargo-get") (v "0.1.2") (d (list (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1s53ax491fm6avxqx19naw0yqr8z8knc8ycmvcrsxy1w8qsjyi57")))

(define-public crate-cargo-get-0.2.0 (c (n "cargo-get") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0j3b4i6fn1ilyw1jr74myp8mlyxwswarjr4zn65h8wmv68v5isfd")))

(define-public crate-cargo-get-0.2.1 (c (n "cargo-get") (v "0.2.1") (d (list (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0qb47qpvw550qsnw1ywvz06hh84mg3z3db8m1svwqd6sll6x40rm")))

(define-public crate-cargo-get-0.2.2 (c (n "cargo-get") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1dk9hgc936aka3dfhjgjjxph4gps7vcilf4ics7jvby7s83s70l1")))

(define-public crate-cargo-get-0.3.1 (c (n "cargo-get") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "19cp0mfnpiv404d8rfvgbak5ps11iighqkf64rkqxskdp9h68rk1")))

(define-public crate-cargo-get-0.3.2 (c (n "cargo-get") (v "0.3.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "10hpj8n1iy82m90ik9jqpcbvfdwkv5n72yzpw8vnr8jlb7zmj92k")))

(define-public crate-cargo-get-0.3.3 (c (n "cargo-get") (v "0.3.3") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "0qqnxqp5ykaz2hyajxcqa7m5iashsz7zwnj0w2dpiyx4gizbp6rm")))

(define-public crate-cargo-get-1.0.0 (c (n "cargo-get") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)))) (h "0pf40fk0v7p0j7p408d408w9wp8sdayva59swa0i4dvc1dyniy2z")))

(define-public crate-cargo-get-1.1.0 (c (n "cargo-get") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "17n4gi3afwa5jixqxm4kcy00cz7a1akis0fdpjgvmrqlkq4hm1g4")))

(define-public crate-cargo-get-1.1.1 (c (n "cargo-get") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "cargo_toml") (r "^0.19.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "semver") (r "^1.0.21") (d #t) (k 0)))) (h "1r9c1iln07whm4mh1ycd96067g8qlhr06py7cadmdvcfjrkm39a8")))

