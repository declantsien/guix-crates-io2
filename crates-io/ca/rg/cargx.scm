(define-module (crates-io ca rg cargx) #:use-module (crates-io))

(define-public crate-cargx-0.1.0 (c (n "cargx") (v "0.1.0") (h "06vba8fqndrpjcxmv821kxwi02cdgmjxshrabp3b8ca8xjibvyy7")))

(define-public crate-cargx-0.1.1 (c (n "cargx") (v "0.1.1") (h "0k75i8jwzfn1mm48gvc4vpj2492wabrzb7p6dhql91r9nndp9b6b")))

(define-public crate-cargx-0.1.2 (c (n "cargx") (v "0.1.2") (h "10ycsq01iynvpndmn45r28733nsh0h1nk6q08jb5pwhp6pm6px49")))

(define-public crate-cargx-0.1.6 (c (n "cargx") (v "0.1.6") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "0lsg42iwi84am935kabkaxyh53l8wp4rdz6dkrxpvhfl4928r2gq")))

(define-public crate-cargx-0.1.7 (c (n "cargx") (v "0.1.7") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "0n8lzkmbnssghy587sisrgp1q3csidgqpvm8kjz1by33mppi6dhs")))

(define-public crate-cargx-0.1.8 (c (n "cargx") (v "0.1.8") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "1373h4q7jfa4phx84gvmn04pzgib1wwm204k4glxa4pqqjjqp3y5")))

(define-public crate-cargx-0.1.9 (c (n "cargx") (v "0.1.9") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "057f66ziwx54sqbpngja8lylmf2zdk5bxq2c1zmxkxdlmnckg3wb")))

(define-public crate-cargx-0.2.0 (c (n "cargx") (v "0.2.0") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "1wbifyk0xs75mmv6d02xyjjwkiqs9d9lw4lhap4fj91zlai0vwd9")))

(define-public crate-cargx-0.2.1 (c (n "cargx") (v "0.2.1") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "0vahmambvr52nd4wfpmi6zjq3jia2lscnc2m0sj1xs98ik8s92xg")))

(define-public crate-cargx-0.2.2 (c (n "cargx") (v "0.2.2") (d (list (d (n "yamlette") (r "^0.0.8") (d #t) (k 0)))) (h "0zn30zy0s7ydd5zfv5lcgcp25zh03izcphw9lwz74c6ladh0par8")))

