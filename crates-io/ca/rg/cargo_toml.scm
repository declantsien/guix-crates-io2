(define-module (crates-io ca rg cargo_toml) #:use-module (crates-io))

(define-public crate-cargo_toml-0.2.0 (c (n "cargo_toml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1pw6376jm7mbvxs6lj5ikq2x13v0rvcdzvk5lq77m4hm29iz6mfb") (y #t)))

(define-public crate-cargo_toml-0.3.0 (c (n "cargo_toml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0hi8fqrp0lhg3n19r76whs7gsariv0d7s1jky23akg6hl9qnh8pi") (y #t)))

(define-public crate-cargo_toml-0.4.0 (c (n "cargo_toml") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1z7kj8z98yaidp48fmi13s9cbj6nk7ycp7xfxh9manjcpw48qqip") (y #t)))

(define-public crate-cargo_toml-0.5.0 (c (n "cargo_toml") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1y6y9m9v4yhnvhdvfxzfq1gkkagyg6q48wpif15ap1i4jagbc3sh") (y #t)))

(define-public crate-cargo_toml-0.6.0 (c (n "cargo_toml") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0sd8b4qgv18rvl9qy0xkj99zsv948g0dv48rc4figi7cvz20h39p") (y #t)))

(define-public crate-cargo_toml-0.6.1 (c (n "cargo_toml") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "04l2adkiny1g838kgx2w8d904i0h1fs6gz7pfp8699hhsdrndzxy") (y #t)))

(define-public crate-cargo_toml-0.6.2 (c (n "cargo_toml") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0kgl5c56jlwrw5dxs3917vbhwdbqdp9kd5zxd0shmxziikvsxih1") (y #t)))

(define-public crate-cargo_toml-0.6.3 (c (n "cargo_toml") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "120x63wbshjc95mpc54n3cqa8v2d83lpgj59d7575amq4ld19321") (y #t)))

(define-public crate-cargo_toml-0.6.4 (c (n "cargo_toml") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1abmbnal0c2rlycmv33xspykfn8yvq2x0kwiklyshrm59gk5qzq9") (y #t)))

(define-public crate-cargo_toml-0.7.0 (c (n "cargo_toml") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "17apqzawz4bxzjivmww0maq6fa2avnwyzbx49xgrcfkcj7hzlkag") (y #t)))

(define-public crate-cargo_toml-0.7.1 (c (n "cargo_toml") (v "0.7.1") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0am1g1x2ab4i3w4jnr7xpq70987cfk0khw70mywnkk3rpb8mjn4f") (y #t)))

(define-public crate-cargo_toml-0.7.2 (c (n "cargo_toml") (v "0.7.2") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0fn717zm5fb8knzazghh10bbkp87dvxrmagqd0d777l10jq95l5y") (y #t)))

(define-public crate-cargo_toml-0.8.0 (c (n "cargo_toml") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1wx0g6ck91drhlkvf1fzc59xmydrm39q24m8cvnxg5zrm807p1z7") (y #t)))

(define-public crate-cargo_toml-0.8.1 (c (n "cargo_toml") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1sbgjvbzb67mg7dzlpdndnzlr7ca1ldiqan078lbiiw8d0i1fgai") (y #t)))

(define-public crate-cargo_toml-0.9.0 (c (n "cargo_toml") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wxnmck8kfbbrvlwvfs4637am6ck5acwf3lqfhmsbhr9fxwncknr") (y #t)))

(define-public crate-cargo_toml-0.9.1 (c (n "cargo_toml") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0vixk2shyrx0bj6hx7ia01k3aj9qgw9x2b8k4lmafagwqs6zaqv3") (y #t)))

(define-public crate-cargo_toml-0.9.2 (c (n "cargo_toml") (v "0.9.2") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14md0px40lk4hi1gn9sq1wizk19pkzddsljjdz0csbdhvynrcdcw") (y #t)))

(define-public crate-cargo_toml-0.9.3 (c (n "cargo_toml") (v "0.9.3") (d (list (d (n "serde") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.129") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mzl26jbv1a3cihw6kgl5pkmjb5pjr2pv2g0dvvxxbh71k7wmy9n") (y #t)))

(define-public crate-cargo_toml-0.10.0 (c (n "cargo_toml") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jiwgr6sin5mnm8dqrp56cbzqhfpcnh7c7kc7gvd2b98x6d0nyh6") (y #t)))

(define-public crate-cargo_toml-0.10.1 (c (n "cargo_toml") (v "0.10.1") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "03s21lil5ypv2hh5w33vjwpnv58yrr0njli8gyq7sjli3ihi7mn6") (y #t)))

(define-public crate-cargo_toml-0.10.2 (c (n "cargo_toml") (v "0.10.2") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0fjxw0dl2rxb85mcp8wgdh6paqf966ksbp7l2bd4snmp8iw0l4b7") (y #t)))

(define-public crate-cargo_toml-0.10.3 (c (n "cargo_toml") (v "0.10.3") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0qlfqql9vrcn7yrjha4kfdvjg8m4diq6i7mcqhai842zl7x7qg1n") (y #t)))

(define-public crate-cargo_toml-0.11.1 (c (n "cargo_toml") (v "0.11.1") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "022j8ayvvlppk0cg342mcdw57d2wwibi3sfycmw9szlvixbamisw")))

(define-public crate-cargo_toml-0.11.2 (c (n "cargo_toml") (v "0.11.2") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0b0f94b9pc14nxx76b468wbhz2h1cq836ab8mgbi3i2970xdn46p") (y #t)))

(define-public crate-cargo_toml-0.11.3 (c (n "cargo_toml") (v "0.11.3") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "06g2d7hqj4sha5rarb6fn60mc7g04arddap41gxnvfd66y21zxii")))

(define-public crate-cargo_toml-0.11.4 (c (n "cargo_toml") (v "0.11.4") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1vqkg5hm8vzn1izzd2i4pgad1658193wxxw2i63lb1w6rpq0w9sf")))

(define-public crate-cargo_toml-0.11.5 (c (n "cargo_toml") (v "0.11.5") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1181wnps0kd1fhb0ij7wl0ww8f54xiqyrnykrp8iyra4chzds2aq")))

(define-public crate-cargo_toml-0.11.6 (c (n "cargo_toml") (v "0.11.6") (d (list (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "172cqwjyjfmjhd3davpgvqfiiy0v91kh9mb03cipxzg9vad9whd4")))

(define-public crate-cargo_toml-0.11.7 (c (n "cargo_toml") (v "0.11.7") (d (list (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "112394jq7aw3y3h0by0yh1bg9a039bbih1l5hai035bih1v44ihr")))

(define-public crate-cargo_toml-0.11.8 (c (n "cargo_toml") (v "0.11.8") (d (list (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03m65gp9zyzs9pl6xxzibpns8pgn7akn7fq609ij8z9vkvskyb77")))

(define-public crate-cargo_toml-0.12.0 (c (n "cargo_toml") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1kqnd9xy2y6jljjdgcx9nmbny7vm92csaqqifj89149yhi7xgv0s")))

(define-public crate-cargo_toml-0.12.1 (c (n "cargo_toml") (v "0.12.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0fkzic4d5ydb1z3cnmgw8qb4cvydni0x9c816i6hbfgriliyl868")))

(define-public crate-cargo_toml-0.12.2 (c (n "cargo_toml") (v "0.12.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1z1hqbw56869m4z4l09l5dzy5d8kfir6apdrkv5v6apys7p5ns7f")))

(define-public crate-cargo_toml-0.12.3 (c (n "cargo_toml") (v "0.12.3") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0k0hrk9cgavszv1bn4zfpbppnrkifm76kn40zyqkjn6mkjbfaqyx")))

(define-public crate-cargo_toml-0.12.4 (c (n "cargo_toml") (v "0.12.4") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jdxsg395dfvnhqbmnvb7jva2hfsh6cnbzpip9nhi3bcdmfisqka")))

(define-public crate-cargo_toml-0.13.0-alpha.0 (c (n "cargo_toml") (v "0.13.0-alpha.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0r99ln60x7hi6v90mcjfkisyp46mgqnry875rb6jj1gqh5gw9ilr") (y #t)))

(define-public crate-cargo_toml-0.13.0-alpha.1 (c (n "cargo_toml") (v "0.13.0-alpha.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1520nr03j6rs35fzin14z58y72knhcdga98z1frpj15y9asqqi7m") (y #t)))

(define-public crate-cargo_toml-0.13.0-beta.1 (c (n "cargo_toml") (v "0.13.0-beta.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "06zk9argrqi6q6jm7xyns91iff3vs8d7blg81ssl47wq16pny4zs") (y #t)))

(define-public crate-cargo_toml-0.13.0-beta.2 (c (n "cargo_toml") (v "0.13.0-beta.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1n29fasir5zk1pzscvl0nddngizxlga7zyyii6kyah7w849gc3rc") (y #t)))

(define-public crate-cargo_toml-0.13.0 (c (n "cargo_toml") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1f3yaddnjnz3wjvz1jw1imfvvfwddvajnifa3yjvzcsnmy33a3ma")))

(define-public crate-cargo_toml-0.13.2 (c (n "cargo_toml") (v "0.13.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "19cad8v72z7yr9hsraj4ma290aja60547nk7ag616n3z9dh36h46")))

(define-public crate-cargo_toml-0.13.3 (c (n "cargo_toml") (v "0.13.3") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "06ly7ai6zsinz34jr30y3p88fx0i5vj2x5srlpvghabk8zlljw29")))

(define-public crate-cargo_toml-0.14.0 (c (n "cargo_toml") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0hy8x6dvv6nvv9qg8rkbb67x3v27fm0dccgcp1161rd1a7z084hg")))

(define-public crate-cargo_toml-0.14.1 (c (n "cargo_toml") (v "0.14.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hmc4bzr1hkaanss0wr7yiv4p7g8nybndw6mrpi42h2929iw7yrb")))

(define-public crate-cargo_toml-0.15.0 (c (n "cargo_toml") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "0f2l41hkybhrzh20pakf2mrkyj35x7jckq5yxa2f0cmliaaca88m")))

(define-public crate-cargo_toml-0.15.1 (c (n "cargo_toml") (v "0.15.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "1vbgvwxgsdb0y3x8v3saks4cdpav581brgqqyk8hisw868s2k68q")))

(define-public crate-cargo_toml-0.15.2 (c (n "cargo_toml") (v "0.15.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "02q9s1c09lq4f3742m8x6jhfy1f0i32fnibk0nvl3l0y80pbr0vz")))

(define-public crate-cargo_toml-0.15.3 (c (n "cargo_toml") (v "0.15.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "0f08wqkyb00a44pg97ipfj4f18igr6na2nljq428zzzz019a76jr") (r "1.64")))

(define-public crate-cargo_toml-0.16.0 (c (n "cargo_toml") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1f101h4al2r81a6az5k1j55g8fd7yla0n2077yfsq3mzmwwr3y5d") (r "1.66")))

(define-public crate-cargo_toml-0.16.1 (c (n "cargo_toml") (v "0.16.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1i7kiapwwx1nzah4ga39w84ixxrz02vghm3z7yg1q31gxw8a8n4f") (y #t) (r "1.66")))

(define-public crate-cargo_toml-0.16.2 (c (n "cargo_toml") (v "0.16.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0jksm52abcc2xahwx43w238xi4n3fgs91nlmf9ag7wlgg88z38bh") (r "1.66")))

(define-public crate-cargo_toml-0.16.3 (c (n "cargo_toml") (v "0.16.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1lxgiwha2kqbk60iq5cvczbnd5xrqa4cj7bqk6k8wf64qsdn5yg3") (r "1.66")))

(define-public crate-cargo_toml-0.17.0 (c (n "cargo_toml") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0xxx34avzgagzp6sxl1wrmnc4z4ric9hcd0mjm5zv876k6nr59bc") (r "1.66")))

(define-public crate-cargo_toml-0.17.1 (c (n "cargo_toml") (v "0.17.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0mr93hwnrxh6lgc2bc6hhhbs5s6v1bm3l9b7niknax0fi5cww7jd") (r "1.66")))

(define-public crate-cargo_toml-0.17.2 (c (n "cargo_toml") (v "0.17.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "06bpsp1kxci39556id1gcasy5b94wsx57q8788z9x7jqlw9rx5la") (r "1.66")))

(define-public crate-cargo_toml-0.18.0-beta.1 (c (n "cargo_toml") (v "0.18.0-beta.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1fd8chagq7vq1k91c268i628y1r073y75zs2ppcan68iyd9yy1yc") (r "1.66")))

(define-public crate-cargo_toml-0.18.0-beta.2 (c (n "cargo_toml") (v "0.18.0-beta.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1m9ll0hpigbvd4qjy3w6j3q9r7qgnbw3hlvpkjsd30fdd7k1wmxl") (r "1.66")))

(define-public crate-cargo_toml-0.18.0 (c (n "cargo_toml") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "133y3j1gf5p647dn6zrkidd4kxqdxsxv03s4n2j3b673j187aaw0") (f (quote (("features")))) (r "1.66")))

(define-public crate-cargo_toml-0.19.0-beta.1 (c (n "cargo_toml") (v "0.19.0-beta.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0ff9iaqciwljc69pqxcm2brvnp8hfpl6fk3q06kd87slxb14jszx") (f (quote (("features")))) (r "1.66")))

(define-public crate-cargo_toml-0.19.0 (c (n "cargo_toml") (v "0.19.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0asqzp5gi1z12z4lsc2ad90bbya7zx5j0mwmw3ivjs0x12inwbcj") (f (quote (("features")))) (r "1.66")))

(define-public crate-cargo_toml-0.19.1 (c (n "cargo_toml") (v "0.19.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "054dsflcgaxv2w9043pcvf270mbvxih4rw10h02vanj1cyhggj9x") (f (quote (("features")))) (r "1.66")))

(define-public crate-cargo_toml-0.19.2 (c (n "cargo_toml") (v "0.19.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1gljfkdjx07fisn5xkklv56ki3p49ppf8fkry7c1psx28bgmd0x9") (f (quote (("features")))) (r "1.66")))

(define-public crate-cargo_toml-0.20.0 (c (n "cargo_toml") (v "0.20.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0pj998v7ymas3di65mif3bad1rpqvp6n3dy056grk59ggln9pkp1") (f (quote (("features")))) (r "1.70")))

(define-public crate-cargo_toml-0.20.1 (c (n "cargo_toml") (v "0.20.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1csv7n87adxk9rccvsig7ki2a5mchw09k602ci009p1bm28h99im") (f (quote (("features")))) (r "1.70")))

(define-public crate-cargo_toml-0.20.2 (c (n "cargo_toml") (v "0.20.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1lqw57i9l974fw28mdmmvksh5h9spq4814vlrbjkd3wbddaivjy8") (f (quote (("features")))) (r "1.70")))

