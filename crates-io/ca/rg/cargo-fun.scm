(define-module (crates-io ca rg cargo-fun) #:use-module (crates-io))

(define-public crate-cargo-fun-0.1.0 (c (n "cargo-fun") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sg9933nx985qbczmwpmmjbdgb0b1h1j88g8hf4k7ha9shymf9c3")))

(define-public crate-cargo-fun-0.1.1 (c (n "cargo-fun") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xqx2xs5iysiiv78p0m9hw5br2p6cxq846wgm30i9w41gf4hfzpl")))

(define-public crate-cargo-fun-0.2.0 (c (n "cargo-fun") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "12c034grjdziv2q4s1xdns1bz4273b7gqdz48591mqnc7xq2pmc3")))

