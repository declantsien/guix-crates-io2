(define-module (crates-io ca rg cargo-wop) #:use-module (crates-io))

(define-public crate-cargo-wop-0.1.0 (c (n "cargo-wop") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1im8xxbdx2l9592ayvcbssk6rngh8ply5yji3niax4jq02kll3qi")))

(define-public crate-cargo-wop-0.1.1 (c (n "cargo-wop") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0cl0b7j8qzzpiv4g2jb0rknjqz4gi6bdjr66w5fc48p3cjzdfc39")))

(define-public crate-cargo-wop-0.1.2 (c (n "cargo-wop") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1z0dhi24my0cj0rnqkr30bx63d46fns1i6axv343zv47azb4qgwj")))

(define-public crate-cargo-wop-0.1.3 (c (n "cargo-wop") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06xskryy1kkcng33mni47nzxr7k13yahnh2f7yk64rfqp2sp43cv")))

(define-public crate-cargo-wop-0.1.4 (c (n "cargo-wop") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0l1f2bdbjdak7lbcbnvdab5jjkcpwmdzf8w4xmgcb23n8pqadsc1")))

(define-public crate-cargo-wop-0.1.5 (c (n "cargo-wop") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "09b5j24wck38msxs89brykril0qy7cfxp9fv492k4and0g5694nb")))

(define-public crate-cargo-wop-0.1.6 (c (n "cargo-wop") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1rigrcvkpw8slrzl016hy0dahf9vh2qhpm6iff9vfz35qv7d493h")))

