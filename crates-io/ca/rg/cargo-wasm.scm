(define-module (crates-io ca rg cargo-wasm) #:use-module (crates-io))

(define-public crate-cargo-wasm-0.1.0 (c (n "cargo-wasm") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1wy8gv564p5qbiyzfb9rpgk1v0yygf8wz3g63zsf1k2yv006dzkb") (y #t)))

(define-public crate-cargo-wasm-0.2.0 (c (n "cargo-wasm") (v "0.2.0") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)))) (h "18acfz5fpxzshc486plh18jp3apvscn51vvlm23y9jiwrh8pd4g1") (y #t)))

(define-public crate-cargo-wasm-0.3.0 (c (n "cargo-wasm") (v "0.3.0") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "fantoccini-stable") (r "^0.7.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)) (d (n "tiny_http") (r "^0.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "0qq7bz1kklcf8w3a0d9433klnv68crgdd6z1gf60hsy9680bm3i7") (y #t)))

(define-public crate-cargo-wasm-0.4.0 (c (n "cargo-wasm") (v "0.4.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "0z27am7hlzd2y8l7vgxsjqdx82kg2yvvd1a3c9z4g6zrpibbwkms")))

(define-public crate-cargo-wasm-0.4.1 (c (n "cargo-wasm") (v "0.4.1") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "1m5zvxacziwwpn77mbwa1hklg08fmq2xwmq9vkmb6lsrrynjmmj0")))

