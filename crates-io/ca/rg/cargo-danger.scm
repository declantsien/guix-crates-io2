(define-module (crates-io ca rg cargo-danger) #:use-module (crates-io))

(define-public crate-cargo-danger-0.1.0 (c (n "cargo-danger") (v "0.1.0") (h "0gh0py1j97dj18xxsi20myrx8cxx15h07qrkil0mdnwwra172vhs")))

(define-public crate-cargo-danger-0.2.0 (c (n "cargo-danger") (v "0.2.0") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0p5zwvwk7w0kwn4k2dxzjp6li1kn4d25jfkh0djgdcjcm7fz7lk4")))

(define-public crate-cargo-danger-0.2.1 (c (n "cargo-danger") (v "0.2.1") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1j9lfs0rph78xmxqvhvcllila3ip7jm4rgw1d7isldz6qx270pji")))

(define-public crate-cargo-danger-0.2.2 (c (n "cargo-danger") (v "0.2.2") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0lg8qkgb6105537cj8pnh1yvrpll04jsinj38zm2blkjay5a3r42")))

