(define-module (crates-io ca rg cargo-mod) #:use-module (crates-io))

(define-public crate-cargo-mod-0.1.0 (c (n "cargo-mod") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1bsrz7m1irhp4vpa36wh0cx00y8pbnlny9jlx5y8rh6z4c9f33bz")))

(define-public crate-cargo-mod-0.1.1 (c (n "cargo-mod") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0fjzkl0n9hd11ksg83c19qwdb0xpia1x91psvkbfh5lww2j87bnb")))

(define-public crate-cargo-mod-0.1.2 (c (n "cargo-mod") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "09y4clga3nzflnb9nbr08hssjnvg6hxwl2y0bwi6alvg6nvcyx1s")))

(define-public crate-cargo-mod-0.1.3 (c (n "cargo-mod") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1231f47d5xx3zc152j6j4x7n5y3f2v5m5hcj2401c50zh090cnd9")))

(define-public crate-cargo-mod-0.1.4 (c (n "cargo-mod") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "08wli40zxaz33817nx6g5xal6fhvmbqvmz29id3fj7pii3w59h3v")))

(define-public crate-cargo-mod-0.1.5 (c (n "cargo-mod") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1sx52rn631rs21qfl4swagjn32hx9jk9pm7ipmqv3zkqcpvlc6x8")))

