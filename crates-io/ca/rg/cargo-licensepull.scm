(define-module (crates-io ca rg cargo-licensepull) #:use-module (crates-io))

(define-public crate-cargo-licensepull-0.1.0 (c (n "cargo-licensepull") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1hx34sankp9a37f99dcik9zxcqjw3pdm1g0bxq7w77qkyz96kv04")))

(define-public crate-cargo-licensepull-0.1.1 (c (n "cargo-licensepull") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0h8g0014sn3h1x2739jwm8qjh3cgys8f98aimcfknimzn95if0qk")))

