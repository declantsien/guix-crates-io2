(define-module (crates-io ca rg cargo-credits) #:use-module (crates-io))

(define-public crate-cargo-credits-0.1.0 (c (n "cargo-credits") (v "0.1.0") (d (list (d (n "cargo") (r "^0.47") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1s7d8g490zscvxm13qfcgd2fs1smy08b7c7i5fn1k24viyw53z71")))

(define-public crate-cargo-credits-0.2.0 (c (n "cargo-credits") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1sybpxz6s702xv5y270z129r03j51hz9s3y8b0nwfadira3w85r6")))

(define-public crate-cargo-credits-0.2.1 (c (n "cargo-credits") (v "0.2.1") (d (list (d (n "cargo_metadata") (r "^0.15.0") (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "01yz00c6afxais7qn92c3zqqznbg0bmaihhbv6fic13vhcich52w")))

