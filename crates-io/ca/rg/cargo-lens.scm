(define-module (crates-io ca rg cargo-lens) #:use-module (crates-io))

(define-public crate-cargo-lens-0.1.0 (c (n "cargo-lens") (v "0.1.0") (h "1nyckcmag1c8v5w40sx8v4pjmiha93pvbn282v4m0r7vdy5il7k4") (y #t)))

(define-public crate-cargo-lens-0.1.1 (c (n "cargo-lens") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "00vpi67x913f0kcvykbds0q6cmjq1w2p91dsrqxcm9g7a149cmhm") (f (quote (("debug_socket" "libc"))))))

