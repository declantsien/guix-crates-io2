(define-module (crates-io ca rg cargo-chadr) #:use-module (crates-io))

(define-public crate-cargo-chadr-0.1.0 (c (n "cargo-chadr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "17ybwqdgnscci5yz8iv56rdnqavbwnfbd6p0pms3847dchnqf6s4")))

(define-public crate-cargo-chadr-1.0.0 (c (n "cargo-chadr") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0jzb2574zj0n3fpyrzrn6mbw895bg2na163nan1sq5fi73f8dil9")))

(define-public crate-cargo-chadr-1.0.1 (c (n "cargo-chadr") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0psrgzxr6yhh55y2f8gr23sp0wh7pzvb489mz2nrd5pzn1mhlsdj")))

(define-public crate-cargo-chadr-1.0.2 (c (n "cargo-chadr") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "148s3yixfc28mgizcbkd5mymldnwmyq7l1h5w233c97hylyl2isv")))

(define-public crate-cargo-chadr-1.0.3 (c (n "cargo-chadr") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1gy6fmzyrnax2mvg0204vc9dp748m1m4wrnadgrw93rcp6dqnl0d")))

(define-public crate-cargo-chadr-1.0.4 (c (n "cargo-chadr") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0j40z18mq5iyp5vnywycfahb8xjxdiqgnyanjhpfq8nfrdcr9w7j")))

(define-public crate-cargo-chadr-1.0.5 (c (n "cargo-chadr") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "00iwqngak0czs9r3rana1jv9n879v2s99rf5axqm95syfg98ysmq")))

(define-public crate-cargo-chadr-1.0.6 (c (n "cargo-chadr") (v "1.0.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jk9ijjj9m0kh1005jq3xwpy9vql1599afl85mh7zma94dya0ji0")))

(define-public crate-cargo-chadr-1.0.7 (c (n "cargo-chadr") (v "1.0.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "029aq1ac2z77mcmcha44s45ng3xwav938qd4y7xgcydm00gk0gjq")))

(define-public crate-cargo-chadr-1.0.8 (c (n "cargo-chadr") (v "1.0.8") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "02vqbdkikan52gdm094np84pqxrgaq0vdb0hg7h7rpmx0phq00ix")))

(define-public crate-cargo-chadr-1.0.9 (c (n "cargo-chadr") (v "1.0.9") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0jqcmb72dy2j1x566pykw0gz6277scrzh0z2ipva0yg4d96yx3i3")))

(define-public crate-cargo-chadr-1.0.10 (c (n "cargo-chadr") (v "1.0.10") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1s91aakwi9haj0n6pnhyyhmrrm4azxw89gcj6mlqqxnkdmadjdq5")))

(define-public crate-cargo-chadr-1.0.11 (c (n "cargo-chadr") (v "1.0.11") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "12v4dw9ywr7ahf8fn6mz993kjv7awbfrvmfm1lfpm10ad5qfpmv8")))

(define-public crate-cargo-chadr-1.0.12 (c (n "cargo-chadr") (v "1.0.12") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1cnr59nscb9bhhxhiw0ba4rh1n2ayz24pgr3hqrby11ddkrrpbib")))

(define-public crate-cargo-chadr-1.0.13 (c (n "cargo-chadr") (v "1.0.13") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0b8l6iri29kvzr99ls5hqnfh38036j37ar7a5snhvpfcyqgwxz8a")))

(define-public crate-cargo-chadr-1.0.14 (c (n "cargo-chadr") (v "1.0.14") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1pihm0k3jk2jspf7xjs9hl4wdbz7y0khs4f26wnd45bs25srcf9h")))

(define-public crate-cargo-chadr-1.0.15 (c (n "cargo-chadr") (v "1.0.15") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1894j8d1npf5rjzk9ykvdywcah8vycfppa3fxmjiw02954r2s2af")))

(define-public crate-cargo-chadr-1.0.16 (c (n "cargo-chadr") (v "1.0.16") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "11g45vff2hckmxxlkfb4jk6011im91br95klv270dp0w5lwpm40l")))

(define-public crate-cargo-chadr-1.0.17 (c (n "cargo-chadr") (v "1.0.17") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0iv61rpsqipf8nvv2ff6r9gr2k6nzgvfynpds121nxmlhaayyav4")))

(define-public crate-cargo-chadr-1.0.19 (c (n "cargo-chadr") (v "1.0.19") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1arxqpfblh7m8w6cabccp9lmzp8jvwlmrsm0w65bn7pg80mmimjh")))

(define-public crate-cargo-chadr-420.69.0 (c (n "cargo-chadr") (v "420.69.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1020nkf6q0jclmhwgqxnnxawia6sfkmplwaki63br8cdxgh4dgbj")))

