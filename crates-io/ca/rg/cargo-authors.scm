(define-module (crates-io ca rg cargo-authors) #:use-module (crates-io))

(define-public crate-cargo-authors-0.0.0 (c (n "cargo-authors") (v "0.0.0") (h "0akkhz0mz574xwbk2cklbm418j2pj46vbvgg60y926k67rcyqvqq")))

(define-public crate-cargo-authors-0.0.1 (c (n "cargo-authors") (v "0.0.1") (d (list (d (n "cargo") (r "^0.16") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kshn3sv7nmb7hpvidjgj98nmznnhpsg2id3wp9ikv14m6m1wl7w")))

(define-public crate-cargo-authors-0.1.0 (c (n "cargo-authors") (v "0.1.0") (d (list (d (n "cargo") (r "^0.16") (d #t) (k 0)) (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01s8hgbwg2id9npc47sjjschxk8122q3365g9l1rc6kg0mcxdxj1")))

(define-public crate-cargo-authors-0.1.1 (c (n "cargo-authors") (v "0.1.1") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03vzdhvlizvfca1h6ih1fnz1ixxlx3ns1crrw6klcj2kvlwh9wdw")))

(define-public crate-cargo-authors-0.2.0 (c (n "cargo-authors") (v "0.2.0") (d (list (d (n "cargo") (r "^0.18") (d #t) (k 0)) (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06h1srbxqpvjv1i6rdkdv5l5j027mg89ydjkz7z0qc381spwgfmw")))

(define-public crate-cargo-authors-0.3.0 (c (n "cargo-authors") (v "0.3.0") (d (list (d (n "cargo") (r "^0.20") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "065hbaj58p26aswcl0sh1x26az3qjghza5532qk6fr9fxmr04ypl")))

(define-public crate-cargo-authors-0.3.1 (c (n "cargo-authors") (v "0.3.1") (d (list (d (n "cargo") (r "^0.20") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x13wbs124qcs17qrq1y6k514ibam3big2n098a5gbna9a7rarw7")))

(define-public crate-cargo-authors-0.4.0 (c (n "cargo-authors") (v "0.4.0") (d (list (d (n "cargo") (r "^0.22") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xdns8nhccra2wr3c464iw3f1a7h10la3nacdrhbh1nqqd67pykh")))

(define-public crate-cargo-authors-0.5.0 (c (n "cargo-authors") (v "0.5.0") (d (list (d (n "cargo") (r "^0.39") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17xg35kj9gzw5dgjp83d9ns8kynkzqnmjcjmcj9scfgwp7dc627y")))

(define-public crate-cargo-authors-0.5.1 (c (n "cargo-authors") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.51") (d #t) (k 0)) (d (n "cargo_author") (r "^1.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "009ha9n2j91b8vx2ynlg54lacd2yy22wjcapky6h8irlvgrw95fj")))

(define-public crate-cargo-authors-0.5.2 (c (n "cargo-authors") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.60") (d #t) (k 0)) (d (n "cargo_author") (r "^1.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ss4sxijmwl5yischbz6ywnbaw2wlzd0hiic5xh5saairffszcm4")))

(define-public crate-cargo-authors-0.5.3 (c (n "cargo-authors") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "cargo_author") (r "^1.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07106cfba223h3b5k5xg4sf256slm6l5wc20wf2sa8c4z7nipdj9")))

(define-public crate-cargo-authors-0.5.4 (c (n "cargo-authors") (v "0.5.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.79") (d #t) (k 0)) (d (n "cargo_author") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03viaanplpnb7q252jkqjvypa754va1sxl4pvvh7rniqh43qnvc2")))

