(define-module (crates-io ca rg cargo_will) #:use-module (crates-io))

(define-public crate-cargo_will-0.2.0 (c (n "cargo_will") (v "0.2.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1lhn0gd1864q2ij96jmri6r4rzwyvg5hqvj1g4vzk1rdlb2lkycb") (f (quote (("use_alloc") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-cargo_will-0.3.0 (c (n "cargo_will") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "~0.14") (d #t) (k 2)) (d (n "petgraph") (r "~0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)) (d (n "willbe") (r "~0.8.0") (d #t) (k 0)))) (h "1jzamz75pp1g1lg0apwir9hjmr68yy9pimgrxvqdp2f6rccnw120") (f (quote (("use_alloc") ("full" "enabled") ("enabled") ("default" "enabled"))))))

