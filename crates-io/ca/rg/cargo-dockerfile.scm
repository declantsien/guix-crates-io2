(define-module (crates-io ca rg cargo-dockerfile) #:use-module (crates-io))

(define-public crate-cargo-dockerfile-0.1.0 (c (n "cargo-dockerfile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0j16qrrcvq91c6qaxv6r4rlh67jm7sqs5l4b74ssf1mpskp9wf9n")))

