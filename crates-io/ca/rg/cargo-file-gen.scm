(define-module (crates-io ca rg cargo-file-gen) #:use-module (crates-io))

(define-public crate-cargo-file-gen-1.0.0 (c (n "cargo-file-gen") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kn5d4rwcpf884zhga83qp6f5s2x9r0xagvl0l2ymmbd993ibj1l") (y #t)))

(define-public crate-cargo-file-gen-1.0.1 (c (n "cargo-file-gen") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s8nnz6xnrxh1vgm0020cbx1kqmindvc507n4i8f802f4y0l5aqc")))

