(define-module (crates-io ca rg cargo-tauri-version) #:use-module (crates-io))

(define-public crate-cargo-tauri-version-1.0.0 (c (n "cargo-tauri-version") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1a9r21xaq7k8gdg1cyd5fhrp3d4nx151mz1km6n2xy946r7k3s94") (y #t)))

(define-public crate-cargo-tauri-version-1.0.1 (c (n "cargo-tauri-version") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "15j295r14b688dh4yaa5h38mip6spsi781qmk4s83vb6ifd23ps5")))

