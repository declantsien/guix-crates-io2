(define-module (crates-io ca rg cargo-glab-release) #:use-module (crates-io))

(define-public crate-cargo-glab-release-0.1.0-rc.0 (c (n "cargo-glab-release") (v "0.1.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "gitlab") (r "^0.1605.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "009mj7sk3fhkn7xhwnwm05ry87jv524yj8vp0wlzlks8449d83bc") (y #t)))

(define-public crate-cargo-glab-release-0.1.0-rc.1 (c (n "cargo-glab-release") (v "0.1.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "gitlab") (r "^0.1605.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1lhq5xfwrhjsj3546vimq0clrp80iw0xjhasnamirs1pmxxpx3z4") (y #t)))

