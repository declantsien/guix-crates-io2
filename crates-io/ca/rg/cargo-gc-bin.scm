(define-module (crates-io ca rg cargo-gc-bin) #:use-module (crates-io))

(define-public crate-cargo-gc-bin-0.1.0 (c (n "cargo-gc-bin") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0sz1xkxqd7nya4hi1y90aymfnlw7f4hm3kfdx6zz1r5hbm266l9m")))

(define-public crate-cargo-gc-bin-0.1.1 (c (n "cargo-gc-bin") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0ai6cyz5grqnmippqjjz794h3g2xvvn5dk597ga1v46v2vakh3rb")))

(define-public crate-cargo-gc-bin-0.1.2 (c (n "cargo-gc-bin") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1cdjrr601qya9xpyd8b5a9hfs3izl28aac7r9sjnrrxm07rphwnr")))

(define-public crate-cargo-gc-bin-0.1.3 (c (n "cargo-gc-bin") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1alyq1z30z81pk4r16j242c42jw806nl0ymy5ylgzc10mc0zm83w")))

(define-public crate-cargo-gc-bin-0.1.4 (c (n "cargo-gc-bin") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "09qk40yq0w3hc7k0g4w1hib2mvanvdcin4g42js8kyzymqq1c1q1")))

