(define-module (crates-io ca rg cargo-context) #:use-module (crates-io))

(define-public crate-cargo-context-0.1.0 (c (n "cargo-context") (v "0.1.0") (h "1mbqd1svd1p5pgjhlcaj1dvblh7z79q7axva2yjk1h67g3abl715")))

(define-public crate-cargo-context-0.1.1 (c (n "cargo-context") (v "0.1.1") (h "18ij7843mwslyqms6ddj04j391p9jxv8zjc1smcg58sccqgb7ifw")))

(define-public crate-cargo-context-0.1.2 (c (n "cargo-context") (v "0.1.2") (h "02acqy4kidqb705fhaiyc35v68arx6g08jd55pwd4l43xpx4ylx0")))

