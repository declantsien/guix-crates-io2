(define-module (crates-io ca rg cargo-deplint) #:use-module (crates-io))

(define-public crate-cargo-deplint-0.1.0 (c (n "cargo-deplint") (v "0.1.0") (d (list (d (n "datadriven") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.7") (d #t) (k 0)))) (h "1arwlc8b1f4cf7d1s3z135lkdnj9zpsx93h416v90ijzvpms6yz3")))

