(define-module (crates-io ca rg cargo-dependencies) #:use-module (crates-io))

(define-public crate-cargo-dependencies-0.1.0 (c (n "cargo-dependencies") (v "0.1.0") (h "1ls83jpg3xfbwk7zyasjg8457y3p6lmvk7kkmcamdrc8xpcwxxgy") (y #t)))

(define-public crate-cargo-dependencies-0.1.1 (c (n "cargo-dependencies") (v "0.1.1") (h "0n79cas9f6dmykzpwj43jnzk3kl8n1zkaypghchhp5pqsh9l09qc")))

