(define-module (crates-io ca rg cargo-zclean) #:use-module (crates-io))

(define-public crate-cargo-zclean-1.0.0 (c (n "cargo-zclean") (v "1.0.0") (d (list (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0v29xas3r1n00dkhfapkncjl2hz628jpivixyfd571pa48vbnq85")))

