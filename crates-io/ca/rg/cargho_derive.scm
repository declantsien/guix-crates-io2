(define-module (crates-io ca rg cargho_derive) #:use-module (crates-io))

(define-public crate-cargho_derive-0.1.5 (c (n "cargho_derive") (v "0.1.5") (d (list (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0) (p "cargho_shared")) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lflwn21hw5ig9526s7dnissd02gqsfjkc885jvlkm9l98sgnb3p")))

(define-public crate-cargho_derive-0.1.6 (c (n "cargho_derive") (v "0.1.6") (d (list (d (n "cargho_shared") (r "^0.1.5") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14g1l1qizwdkcnv2lb5cs4wa44xiwxi9bsdx5pijvc8lnv5rkvgf")))

