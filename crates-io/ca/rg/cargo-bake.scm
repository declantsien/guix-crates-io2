(define-module (crates-io ca rg cargo-bake) #:use-module (crates-io))

(define-public crate-cargo-bake-0.1.0 (c (n "cargo-bake") (v "0.1.0") (h "1fvvk6mnaf1jl12hf8acizdwi42ipbdxrfb7pal077r9mdl06smk")))

(define-public crate-cargo-bake-0.2.0 (c (n "cargo-bake") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.6") (d #t) (k 0)))) (h "15qmkmjnzckbzi14q4j764564kda5hsl727hyrpv8rsly85k3i0v")))

