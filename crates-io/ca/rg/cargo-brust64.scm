(define-module (crates-io ca rg cargo-brust64) #:use-module (crates-io))

(define-public crate-cargo-brust64-0.1.4 (c (n "cargo-brust64") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "image-base64-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1rhn16jzj5y2c57d032778x0xv2q4fkh0qsb5x8hb4j95np6h78l")))

(define-public crate-cargo-brust64-0.1.5 (c (n "cargo-brust64") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "image-base64-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rhn9kklndlypc3ckgq7l0f8gn4jf7pyvv49xxijnbx7pgan7n1v")))

(define-public crate-cargo-brust64-0.1.6 (c (n "cargo-brust64") (v "0.1.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "image-base64-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0f0bajfm7zy08rq0ipfzwd348azgjf4f3jd074l25p43ak60n22h")))

(define-public crate-cargo-brust64-0.1.7 (c (n "cargo-brust64") (v "0.1.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "image-base64-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0b874yqcyvwxdkqqmkzq9y9wfj0gh3q0k3aq8g2av7a0d65yqcqd")))

