(define-module (crates-io ca rg cargo-image) #:use-module (crates-io))

(define-public crate-cargo-image-0.1.0 (c (n "cargo-image") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "cargo-toml2") (r "^1.3.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "0xd0yvkhq79jdvwv3z36y8l8b4c88mbc0h91cami6iznmjjda4qh") (y #t)))

(define-public crate-cargo-image-0.2.0 (c (n "cargo-image") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "cargo-toml2") (r "^1.3.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "0qdyid6j59kwy2d9n8fgcwmz5c01fyfzvx0b407n1zf5fjr1q5j5")))

(define-public crate-cargo-image-0.2.1 (c (n "cargo-image") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "cargo-toml2") (r "^1.3.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7.0") (d #t) (k 0)))) (h "0vnqg9ri773a7dsj5xmbab7j340qlsjmcrp0ijmbvpiq50vcn4qd")))

(define-public crate-cargo-image-0.3.0 (c (n "cargo-image") (v "0.3.0") (d (list (d (n "cargo-toml2") (r "^1.3.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "llvm-tools") (r "^0.1.1") (d #t) (k 0)))) (h "0cxrnn8qgk82jah32504672pvwpbdx47pryj2nj7hrw2qmc715g5")))

(define-public crate-cargo-image-0.4.0 (c (n "cargo-image") (v "0.4.0") (d (list (d (n "cargo-toml2") (r "^1.3.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)) (d (n "llvm-tools") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1y6ppm1q56lflcbkjqqibqgzl0g50y5f3zsdphliax098i4aq325")))

