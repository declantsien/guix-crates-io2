(define-module (crates-io ca rg cargo-avrdude) #:use-module (crates-io))

(define-public crate-cargo-avrdude-0.1.0 (c (n "cargo-avrdude") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0khbhyyfy05gf38r9nnnqlqrpgqacw73xl3lahkdznmp1b3sk2gs")))

