(define-module (crates-io ca rg cargo-mutagen) #:use-module (crates-io))

(define-public crate-cargo-mutagen-0.1.0 (c (n "cargo-mutagen") (v "0.1.0") (d (list (d (n "json") (r "^0.11.12") (d #t) (k 0)))) (h "0c09iqq36ldw46xrjg6f86pm57qzk4s7cqjxka7g8yxj3c64wlc0") (y #t)))

(define-public crate-cargo-mutagen-0.1.1 (c (n "cargo-mutagen") (v "0.1.1") (d (list (d (n "json") (r "^0.11.12") (d #t) (k 0)))) (h "1fdrr18y3491c0kqlq92447dh6hn8pc4l7d5gghqf5mffyf40jyl")))

(define-public crate-cargo-mutagen-0.1.2 (c (n "cargo-mutagen") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "json") (r "^0.11.12") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1.5") (d #t) (k 0)))) (h "14hv8wyya4d4m4vcrfggmv5ll3ajqc2vkrs2ffg8pjk2z7c57s12")))

