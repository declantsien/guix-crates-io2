(define-module (crates-io ca rg cargo-forkdep) #:use-module (crates-io))

(define-public crate-cargo-forkdep-0.1.0 (c (n "cargo-forkdep") (v "0.1.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "cargo") (r "0.64.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "0.14.*") (d #t) (k 0)) (d (n "octocrab") (r "0.16.*") (d #t) (k 0)) (d (n "toml_edit") (r "0.14.*") (d #t) (k 0)) (d (n "webbrowser") (r "0.7.*") (d #t) (k 0)))) (h "10v3kshzybnqcm6mqkm85yw6hy4z9wn034cfg4n5qynqri5l2ym7") (r "1.63.0")))

(define-public crate-cargo-forkdep-0.1.1 (c (n "cargo-forkdep") (v "0.1.1") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "cargo") (r "0.64.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "0.14.*") (d #t) (k 0)) (d (n "octocrab") (r "0.16.*") (d #t) (k 0)) (d (n "toml_edit") (r "0.14.*") (d #t) (k 0)) (d (n "webbrowser") (r "0.7.*") (d #t) (k 0)))) (h "0krd5sy6jmqga5050d0l4fx5kvmd6ays3pc15g0wx3zrkwxahg7z") (r "1.63.0")))

