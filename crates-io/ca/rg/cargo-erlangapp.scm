(define-module (crates-io ca rg cargo-erlangapp) #:use-module (crates-io))

(define-public crate-cargo-erlangapp-0.1.0 (c (n "cargo-erlangapp") (v "0.1.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0hgh2dha4qir0g93569av0kaprxy7q21lc5zhxd5jz0g6wmsx31l")))

(define-public crate-cargo-erlangapp-0.1.1 (c (n "cargo-erlangapp") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0hdm65qw9z89b1w2sqnw6ks0m2z50hcydf1rj9w34zh2fj3r2faj")))

(define-public crate-cargo-erlangapp-0.1.2 (c (n "cargo-erlangapp") (v "0.1.2") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "06fg9ssjrh785g70k832qzbxxpbf72x8py465abhpf7y0r1kzdps")))

(define-public crate-cargo-erlangapp-0.1.3 (c (n "cargo-erlangapp") (v "0.1.3") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "137b0bh817s6121v8126qw41kapv505ysw43gfb4jdcy5cc728ys")))

(define-public crate-cargo-erlangapp-0.1.4 (c (n "cargo-erlangapp") (v "0.1.4") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0xgq2i72xizkg0w631zx6qc01w6gy08qkivd4rniw0kimlff0g4d")))

