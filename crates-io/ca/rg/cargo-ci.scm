(define-module (crates-io ca rg cargo-ci) #:use-module (crates-io))

(define-public crate-cargo-ci-0.1.0 (c (n "cargo-ci") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1ac92ndw0l15a3nr3n42x2m2niv8xkfy23dac8qd2c5962g4fb8i") (y #t)))

(define-public crate-cargo-ci-0.1.1 (c (n "cargo-ci") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1sdhaj6knnbzr258w20y77jgr64dh1z72bndbibdc20395q8zall")))

