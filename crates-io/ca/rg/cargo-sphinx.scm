(define-module (crates-io ca rg cargo-sphinx) #:use-module (crates-io))

(define-public crate-cargo-sphinx-0.1.0 (c (n "cargo-sphinx") (v "0.1.0") (d (list (d (n "cargo") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.10.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "toml") (r "^0.2.0") (d #t) (k 0)))) (h "1m2kxhmis85fihy7abgcva1v7k2vjpwy2vawnxss4lv6wmlhxpmk")))

(define-public crate-cargo-sphinx-1.0.0 (c (n "cargo-sphinx") (v "1.0.0") (d (list (d (n "cargo") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.10.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "toml") (r "^0.2.0") (d #t) (k 0)))) (h "03hqvicia3grnbd3ffr9k4ci33cvfkxaqcc3b6jcmi9xnq467gjy")))

(define-public crate-cargo-sphinx-1.1.0 (c (n "cargo-sphinx") (v "1.1.0") (d (list (d (n "cargo") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.4") (d #t) (k 0)))) (h "1g7pziw07h3viyf0ik9mzdixyxa48pany1ai2hzyijq9bqzxf1z6")))

(define-public crate-cargo-sphinx-1.2.0 (c (n "cargo-sphinx") (v "1.2.0") (d (list (d (n "cargo") (r "^0.30.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4.7") (d #t) (k 0)))) (h "085p5qqjiwb11sgb9f4ipjx3jv60sa8fvvjpry07018v8r4aza8s")))

(define-public crate-cargo-sphinx-1.3.0 (c (n "cargo-sphinx") (v "1.3.0") (d (list (d (n "cargo") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1gkcpwpg92q6ln06nr4cm6h11lb4m02ypmgz6pfv2481pq15xjw1")))

(define-public crate-cargo-sphinx-1.3.1 (c (n "cargo-sphinx") (v "1.3.1") (d (list (d (n "cargo") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "143vq691124rzj0a8r1631vw7ygmhd26587qwmsdbdf6jpdz7wbj")))

