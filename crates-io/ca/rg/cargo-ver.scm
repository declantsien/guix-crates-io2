(define-module (crates-io ca rg cargo-ver) #:use-module (crates-io))

(define-public crate-cargo-ver-0.2.1 (c (n "cargo-ver") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1vysm1qgdk8w71v9c3n1zy9n1fzrv48yy3xp6v4219da6xf03j91")))

(define-public crate-cargo-ver-0.2.2 (c (n "cargo-ver") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1y52ac1sddxnw90bl1x9170im2wny51z4m68x4jq2as7yfrapvsp")))

(define-public crate-cargo-ver-0.2.3 (c (n "cargo-ver") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "16a76hv5427aqf553ksjhbz11m318k82y7yd9n1fzhn4z05f3wcw")))

