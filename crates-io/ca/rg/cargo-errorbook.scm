(define-module (crates-io ca rg cargo-errorbook) #:use-module (crates-io))

(define-public crate-cargo-errorbook-0.0.1 (c (n "cargo-errorbook") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1ybb40yblmpbcgsh6sif3pm5xg7mbq2fd8569di87p0zidp19vqd")))

(define-public crate-cargo-errorbook-0.0.2 (c (n "cargo-errorbook") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "05f1204i3mpnfxb6iyymjawyhjlspg6fccjn2nn7yszwni8midmn")))

