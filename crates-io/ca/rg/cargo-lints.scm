(define-module (crates-io ca rg cargo-lints) #:use-module (crates-io))

(define-public crate-cargo-lints-0.1.0 (c (n "cargo-lints") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "031v1q5nnxxhgsb48c11q8ciilzp25k2s4j3xbxmk6pinc0rvvbh")))

