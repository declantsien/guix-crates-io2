(define-module (crates-io ca rg cargo-reserve) #:use-module (crates-io))

(define-public crate-cargo-reserve-0.1.0 (c (n "cargo-reserve") (v "0.1.0") (h "09mhcw9mg6a6v4c4n45831igvchjjjlnmnzy4jl3r0xzpgvdv13h") (y #t)))

(define-public crate-cargo-reserve-0.1.1 (c (n "cargo-reserve") (v "0.1.1") (h "1fc50cl4vdbc02h1qsmb5n1j83x5nxgjll11wqj6bm29l18wkl8n") (y #t)))

