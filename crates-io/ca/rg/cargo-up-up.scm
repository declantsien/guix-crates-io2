(define-module (crates-io ca rg cargo-up-up) #:use-module (crates-io))

(define-public crate-cargo-up-up-0.0.1 (c (n "cargo-up-up") (v "0.0.1") (h "1f3iyw0i6akcir6z5ss93xw8jz9vg4rmqmp6x4qsyvaxxz6y3pd9")))

(define-public crate-cargo-up-up-0.0.2 (c (n "cargo-up-up") (v "0.0.2") (d (list (d (n "cargo-up") (r "^0.0.2") (d #t) (k 0)))) (h "1j4kp4azrhiax7ywbg7yrdwr3nnjxn35892ziziw4asvjz9pi6bn")))

(define-public crate-cargo-up-up-0.0.3 (c (n "cargo-up-up") (v "0.0.3") (d (list (d (n "cargo-up") (r "^0.0.3") (d #t) (k 0)))) (h "1a9x7j6k38h6jq8ip7acm2yk03w8c8q8yjf8y7m9p0bz4w7rqxjb")))

(define-public crate-cargo-up-up-0.0.4 (c (n "cargo-up-up") (v "0.0.4") (d (list (d (n "cargo-up") (r "^0.0.4") (d #t) (k 0)))) (h "15smlk1dpmskslmpmak06qj56vkir9zngz99srhg4d5qz0cki6s6")))

(define-public crate-cargo-up-up-0.0.6 (c (n "cargo-up-up") (v "0.0.6") (d (list (d (n "cargo-up") (r "^0.0.6") (d #t) (k 0)))) (h "07gzikx88d6q4876mm73ngla19ivhdx0bsy3943sa6pr0ig6gxlr")))

