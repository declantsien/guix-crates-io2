(define-module (crates-io ca rg cargo-local) #:use-module (crates-io))

(define-public crate-cargo-local-0.1.0 (c (n "cargo-local") (v "0.1.0") (d (list (d (n "cargo") (r "^0.32.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)))) (h "13qsx3ygcqdfbpb6pknjsdaqn11jrv1pl2wji6zsdrwqhl0zab2y")))

(define-public crate-cargo-local-0.1.1 (c (n "cargo-local") (v "0.1.1") (d (list (d (n "cargo") (r "^0.40.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "085i3rfkgsmymh4hm76zlwpmazd3q9q2gjbr9l84b0cjx8v4sz6m")))

(define-public crate-cargo-local-0.2.0 (c (n "cargo-local") (v "0.2.0") (d (list (d (n "cargo") (r "^0.49.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "1qsw2c9w5ran4yp7ichwhqcygr2yxvp81q7i9r8cacsjflzxgsnc")))

(define-public crate-cargo-local-0.2.1 (c (n "cargo-local") (v "0.2.1") (d (list (d (n "cargo") (r "^0.50.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)))) (h "0qg9w0360x58rnh304jpqszl4gw3vmvxcf2pmrqnkgz126i2a0ah")))

(define-public crate-cargo-local-0.3.0 (c (n "cargo-local") (v "0.3.0") (d (list (d (n "cargo") (r "^0.58.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.1") (f (quote ("std" "cargo"))) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0wx8gib47i22scc7xdch3qlpzh2m67fsfcj0a78ivahypmslary2")))

(define-public crate-cargo-local-0.3.1 (c (n "cargo-local") (v "0.3.1") (d (list (d (n "cargo") (r "^0.71.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("std" "cargo"))) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0ns8rxhkar012dkrckinx9vywkm5i6g9qgp2pgqkkrwlhpgkynn9")))

