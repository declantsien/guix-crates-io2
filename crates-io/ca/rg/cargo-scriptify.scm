(define-module (crates-io ca rg cargo-scriptify) #:use-module (crates-io))

(define-public crate-cargo-scriptify-0.1.0 (c (n "cargo-scriptify") (v "0.1.0") (d (list (d (n "commandspec") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "15amcibahibc5lfaxvqjbw83bjgcwiwh9mkvimafzyfv2nlfs05r")))

