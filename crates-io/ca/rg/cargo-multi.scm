(define-module (crates-io ca rg cargo-multi) #:use-module (crates-io))

(define-public crate-cargo-multi-0.0.0 (c (n "cargo-multi") (v "0.0.0") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0l5v98l32ngvzd6p3v3a0ymnv6k3z63agzpkfp1yla8cjii1s0a9")))

(define-public crate-cargo-multi-0.1.0 (c (n "cargo-multi") (v "0.1.0") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1s5x4fpn6cjr26nn3gk5frdc6pwnqji3pda8dngic4ip0rcij4xs")))

(define-public crate-cargo-multi-0.1.1 (c (n "cargo-multi") (v "0.1.1") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "12rq93dk42bfy7i2djcazbj31j6mdk5s7lmgx17kylbw1mzqyawc")))

(define-public crate-cargo-multi-0.1.2 (c (n "cargo-multi") (v "0.1.2") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1n40j886vgx9w76c2mih2j49mgbsd1rp6bmsx562y16yvg73jib0")))

(define-public crate-cargo-multi-0.2.0 (c (n "cargo-multi") (v "0.2.0") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0jp68j6xgbm2k0avv1xnhqmlh1jrlhr8w94dvz3ygj41xz9r8qld")))

(define-public crate-cargo-multi-0.2.1 (c (n "cargo-multi") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.42") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0lgda5vm0zzk2d76g906a6pmw4ykasxqa52fzgsvgrcxgksabfbw") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.3.0 (c (n "cargo-multi") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.43") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1x2xa4wh1l7pyngbygq3a4h5n4rafm9k92vr3byayik6c478wi4w") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.3.1 (c (n "cargo-multi") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.44") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1x2kyp40xi9l964ajf154lp6swfpqc8d9yvffx2q3xyxzsskvn6b") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.0 (c (n "cargo-multi") (v "0.4.0") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.44") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0la6k4kl4kl0pyxc1znb3f0lx4ybb1il4mslg07wwkml795r878n") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.1 (c (n "cargo-multi") (v "0.4.1") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.44") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1gfzx9m8pvcf6vd2djc3lkl1a8w6z4bqv9jfnq9cp5a50m3h1z9x") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.2 (c (n "cargo-multi") (v "0.4.2") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.61") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0qkp76nwfl90198w9ch3q3qmdkfwgh4v1hqlq034a6l0p3rfizaz") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.3 (c (n "cargo-multi") (v "0.4.3") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.12") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "16kl16fkysx0rxwa2j7xhmvx8lr64hhbrz7y9q3ccci8k9p095df") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.4 (c (n "cargo-multi") (v "0.4.4") (d (list (d (n "clap") (r "^2.4.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.13") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1xrdhn4ydragxipn3k92w5ib70z1ixjkwpv9yfm15pqfrg35yjzf") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.4.5 (c (n "cargo-multi") (v "0.4.5") (d (list (d (n "clap") (r "^2.6") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1zhfqmjb35ww1283vmxh6y3yc8x05ndhp85vgv99s9wc6c17d0cf") (f (quote (("default"))))))

(define-public crate-cargo-multi-0.5.0 (c (n "cargo-multi") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "14dkv31mva89qgqqnahlfvvxzk8z163alc42010pg1pvplnfixcp")))

