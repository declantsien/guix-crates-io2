(define-module (crates-io ca rg cargo-version-util) #:use-module (crates-io))

(define-public crate-cargo-version-util-1.0.0 (c (n "cargo-version-util") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "=0.14.4") (d #t) (k 0)))) (h "0h20mzby4zfvlp4sqw2iwnm956p91p52dh0ynxzv6199r7k47dvv")))

(define-public crate-cargo-version-util-1.1.0 (c (n "cargo-version-util") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "=0.14.4") (d #t) (k 0)))) (h "1jfbak25mrwjccnwmkdjm5hp8m7f673rr2jdlzzxlsz669pm4nq6")))

(define-public crate-cargo-version-util-1.1.1 (c (n "cargo-version-util") (v "1.1.1") (d (list (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "=0.20.2") (d #t) (k 0)))) (h "04pirib70p0ndby9iwxcafhwarxs0fmbvvv6y3ggjn7hdhx643wh")))

(define-public crate-cargo-version-util-1.1.2 (c (n "cargo-version-util") (v "1.1.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "=0.21.0") (d #t) (k 0)))) (h "1qfn95gkxkrkqmjrivcaag3mrsszpz75yr610dmw5gmamvx8sgf3")))

(define-public crate-cargo-version-util-1.1.3 (c (n "cargo-version-util") (v "1.1.3") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "=0.21.0") (d #t) (k 0)))) (h "0q239584plmn4jzs7w7zgmv0jj4mq4n05hpp4kzqprghjq0by0dm")))

