(define-module (crates-io ca rg cargo-why) #:use-module (crates-io))

(define-public crate-cargo-why-0.1.0 (c (n "cargo-why") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1197sqqgpbmnaw9wy9vd0yk2f9lk0sww619pcbp2y0ri6w7iy5rn")))

(define-public crate-cargo-why-0.2.0 (c (n "cargo-why") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "0z5whpnlafh7xm7i76shymcijjrdykznrjglzwccva8wmpahaxaa")))

