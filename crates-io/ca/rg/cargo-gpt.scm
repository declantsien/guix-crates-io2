(define-module (crates-io ca rg cargo-gpt) #:use-module (crates-io))

(define-public crate-cargo-gpt-0.1.0 (c (n "cargo-gpt") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "08aihr41dgwrwvf80fcas7jq0cscb816m44dg8zfs97xslwzpzid")))

(define-public crate-cargo-gpt-0.1.1 (c (n "cargo-gpt") (v "0.1.1") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)))) (h "13138ly6rkmc8wibca4pw4b1k80i09w85llmxw8sgan4b0rvs3d4")))

