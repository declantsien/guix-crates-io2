(define-module (crates-io ca rg cargo-fast) #:use-module (crates-io))

(define-public crate-cargo-fast-0.1.0 (c (n "cargo-fast") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1xihnaimhgkwqfdrkyhzv4qv207lxfmxk5p69q982lvw8172dzda") (y #t)))

(define-public crate-cargo-fast-0.1.1 (c (n "cargo-fast") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0d8pbwp7d1q812xh9iajrb8s5nh4ykisk8209nwgdl6b8p1841s3")))

