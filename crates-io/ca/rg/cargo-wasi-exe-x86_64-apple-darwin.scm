(define-module (crates-io ca rg cargo-wasi-exe-x86_64-apple-darwin) #:use-module (crates-io))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.1 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.1") (h "1k6ggsgmfm16ikci2shnci8gq1nlilg8x5c6sm457xyr5ps5f9v3")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.2 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.2") (h "0ignk31b170zriisgq0ihiqnyhglwss6vc9bcm31nrsmlzs4qkvr")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.3 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.3") (h "0a6m6asd92i4mvvbai9bmabzp1ryfs3bffx8mqsl15z14hr8h4pc")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.4 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.4") (h "1mrlyv4w4m1x7x3ayjws2m1a1rysg0l01r4a2vm21bhl04yqfafw")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.5 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.5") (h "17k7j7pjy70zjz79bl03pqdipxypb9wr4hzwhmb7qnhsfyvxz44f")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.6 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.6") (h "15y4hyqz2f8a9dk65ncfvn7c6dk74h0zf5gf8im6q9965vv0wy35")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.8 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.8") (h "18g993zadkynqxk57xqhlkdrpr0nwivmhxpqd8911gi6wf6ryvzr")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.9 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.9") (h "0xfvpwp4ld07xmk4bwsbc4g9ss6il3cccn42di2kdaw2gj89705d")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.10 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.10") (h "1svkk4h1aqzryb50mkqvvz8vkdz5gzm12wbmykymz9y2hyd2sns3")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.11 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.11") (h "13xf9wz93fljnnm8lri4clk1vf1bcpbfl80aqnqy6rbwmsb69w7f")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.12 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.12") (h "0wl0dkc3jjkdgq8dk0d4pq1vx6vjhmim298147w5qznknrjnvx08")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.13 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.13") (h "1734ja4p4d5wnq6fn4xw94qmhciz26kxqivl8s1qyn44cpr8gmqm")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.14 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.14") (h "0s8lzmbfnzds10l8qiqpaayz6wpraiihlcxl3qrkjvm2wcp9n01m")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.15 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.15") (h "143hqr1kbm3jag2ranm0dycpd7ql1vfr2zy78q0x823k5c6jdnrn")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.16 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.16") (h "1h41x0zb6mp5rwnc9xq3hchykgzs210kqmqz5bb2z2313znf3fag")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.17 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.17") (h "0gsf3mppxfgg23k57kqd4y27j4s9xz3rsk0mh0sgywp3wckazxq5")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.18 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.18") (h "1y6sqmaqqq602wdy8s23p8ar0nd2r1hxfwg3zrlqjfki2z5k5ip7")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.19 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.19") (h "03l03win94v2j6bf3wi8sgc698aqhwcb569cx682k931aymw54d4")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.20 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.20") (h "19lr1jhg4jsy21asyrjj7wlrpiy42xmrz58794zzsbayqdl3kix1")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.21 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.21") (h "0cj0paxb9vl20b088w92hpy046fjc8ikc2hsj7ky51830k5qg1b8")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.22 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.22") (h "14xgz9d3v29ksc4qdiyi6a5d6ijjb7gamd73y5klmp3wvwp1pfgn")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.23 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.23") (h "0pyiy78z77w81d9hgya58cm4lisjaxqq0dqcfx1rkg7wjl12g18i")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.24 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.24") (h "1l3n5x6bzrffw6xymi76lyd1sjxfhp0ir4ar106xjci26iqajz3l")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.25 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.25") (h "03ygqf3sbgfnigrmhrk2qbqjqj5xl7jfxl0ibldai20i74ly2d14")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.26 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.26") (h "1ly6dci9p6krids74l0xmkzsqkw4l5i6xkgp6ijljsbhp4b1df6h")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.27 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.27") (h "08zr9y4cxxrifcs6d30jdkghy9341ia128l8f9bzsgamfxcdghq7")))

(define-public crate-cargo-wasi-exe-x86_64-apple-darwin-0.1.28 (c (n "cargo-wasi-exe-x86_64-apple-darwin") (v "0.1.28") (h "04c6cfqwpc3pmlby18p1y3wvvr8dy7j5ys09s2mmwqgd6iisq5nk")))

