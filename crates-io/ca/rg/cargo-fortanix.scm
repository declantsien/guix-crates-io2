(define-module (crates-io ca rg cargo-fortanix) #:use-module (crates-io))

(define-public crate-cargo-fortanix-0.0.1 (c (n "cargo-fortanix") (v "0.0.1") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "1qwy2wf1d8k1dfkfaiij5dh40vrbbgi7l24jj1lvhgwsndb6azbf")))

(define-public crate-cargo-fortanix-0.0.2 (c (n "cargo-fortanix") (v "0.0.2") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)))) (h "1yvv5rsa6s13maydb6b25qbjslypnckh3lgzmz8jrrsglb8gzdd9")))

