(define-module (crates-io ca rg cargo-simple-todo) #:use-module (crates-io))

(define-public crate-cargo-simple-todo-0.1.0 (c (n "cargo-simple-todo") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "06a49kn58grkplaijcpvxi8fj32a34i44af86sr6kdmw1bkii7n3")))

(define-public crate-cargo-simple-todo-0.1.1 (c (n "cargo-simple-todo") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0v7g4xwajbkriy2b05rff19jabp0i2iljqbi2yx0ab9mlii98sk5")))

(define-public crate-cargo-simple-todo-0.1.2 (c (n "cargo-simple-todo") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "07miq0sqxhj2c23gvk6sz8r7w0l2ka4szcn2f32l96jplrflil9a")))

(define-public crate-cargo-simple-todo-0.2.0 (c (n "cargo-simple-todo") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "19kh0fhr17s50mlqsbb2c9bvz8xiwm86hbicsv805lypqggqlyal")))

(define-public crate-cargo-simple-todo-0.2.1 (c (n "cargo-simple-todo") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "1mbqs5nlq34vpvg2a6a5vwl5qpdwp9j0lnwrpmbv42ih2zaqgihi")))

