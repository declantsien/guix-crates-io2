(define-module (crates-io ca rg cargo-bom) #:use-module (crates-io))

(define-public crate-cargo-bom-0.2.2 (c (n "cargo-bom") (v "0.2.2") (d (list (d (n "cargo") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.17") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0.3") (d #t) (k 0)))) (h "04pg5kcfdwm5g00rwcgn877r0d4mflkjix7i1vcskphp2p88q78z")))

(define-public crate-cargo-bom-0.2.3 (c (n "cargo-bom") (v "0.2.3") (d (list (d (n "cargo") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.17") (d #t) (k 0)) (d (n "tabwriter") (r "^1.0.3") (d #t) (k 0)))) (h "0p35d011zvgs5y06mpqy21hjqsh058mkzif82gmbd3dwdqpkylgd")))

(define-public crate-cargo-bom-0.3.0 (c (n "cargo-bom") (v "0.3.0") (d (list (d (n "cargo") (r "^0.30") (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1cbr5rc1p8pfd83rrphyhj1qnaq732sbkqjg4grq5vvqdh7ssrdx")))

(define-public crate-cargo-bom-0.3.1 (c (n "cargo-bom") (v "0.3.1") (d (list (d (n "cargo") (r "^0.32") (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1pk2yj643h3vl0dgp4mwyq1q5nkghslzqlgncbw6yabj4pifwlb9")))

(define-public crate-cargo-bom-0.4.0 (c (n "cargo-bom") (v "0.4.0") (d (list (d (n "cargo") (r "^0.38") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0xr3rkikya86czrzbang76fa2r2r58a2439aakawkl7z5rimf9in")))

(define-public crate-cargo-bom-0.4.1 (c (n "cargo-bom") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "09n1q8h4cmd7nmmznz597clvwrdyf7mvvrzy1pjwmafbfxmyv82i")))

(define-public crate-cargo-bom-0.4.2 (c (n "cargo-bom") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.48") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1d74grcigns14jnvl3w1721fxprbdyy46jp45vj7k2scvhd2az0z")))

(define-public crate-cargo-bom-0.4.3 (c (n "cargo-bom") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.50") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1g8q2ymlzf8i0k5j91lf42amqs6ssm0x1k1bn0604r29xp1zxrwg")))

(define-public crate-cargo-bom-0.5.0 (c (n "cargo-bom") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.50") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.1") (d #t) (k 0)))) (h "08jhm0xsx5zg72f3yda9qcgnvs486qjwn7bcjs1j9i90xhijanw3")))

(define-public crate-cargo-bom-0.5.1 (c (n "cargo-bom") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.50") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.2") (d #t) (k 0)))) (h "06dvjsylkgyz8xrraiv5y5sl0zlpjzcxsbhyn29sfr3zjs8rjjzy") (y #t)))

(define-public crate-cargo-bom-0.5.2 (c (n "cargo-bom") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.50") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.2") (d #t) (k 0)))) (h "05msdpqsn08rphiqz2whmz1m2pswxxgvf4iy7ggcvfypiwjysk9h")))

(define-public crate-cargo-bom-0.5.3 (c (n "cargo-bom") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.56") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.3") (d #t) (k 0)))) (h "18fw0l05mk3x3fmknyakqvvsa3h4xv2hg5hiya0ds66a56bw8gdg")))

(define-public crate-cargo-bom-0.5.4 (c (n "cargo-bom") (v "0.5.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.56") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.3") (d #t) (k 0)))) (h "0g7qh9dxsqswyfgrz83p27npsyb1yf3wr0k9ixsl37lm4npj8bhs")))

(define-public crate-cargo-bom-0.6.0 (c (n "cargo-bom") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.58") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.3") (d #t) (k 0)))) (h "1gprv48pzvgmr1jmg10n0q1fvf1yrs0i9wcdgdr94swsnwavmkb0")))

(define-public crate-cargo-bom-0.6.1 (c (n "cargo-bom") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.59") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.4") (d #t) (k 0)))) (h "1g17ix1lyvxrghksfdsfxl1lsm35lwwaabypbd9kbx6acwys29f6")))

(define-public crate-cargo-bom-0.6.2 (c (n "cargo-bom") (v "0.6.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.61") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.6") (d #t) (k 0)))) (h "15gdxi9185jc59lwwwxc3fqy9fa9mkccvfr4j6b2zyspqhh5301f")))

(define-public crate-cargo-bom-0.6.3 (c (n "cargo-bom") (v "0.6.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.61") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0rpyb4kmin0g8lgg9lvp2zall2sn94vmjr2z28g5nh86vl6kiznm")))

(define-public crate-cargo-bom-0.6.4 (c (n "cargo-bom") (v "0.6.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.63") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.8") (d #t) (k 0)))) (h "0lnr8mx0bwpvvf7wyj9zjd95vpzp2rap65b31lriqfb2bymp5rlj")))

(define-public crate-cargo-bom-0.6.5 (c (n "cargo-bom") (v "0.6.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.67") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.10") (d #t) (k 0)))) (h "0iqg3my9mc7prqjzhwjiwk784z1zdnr743zy0fbdkijvmp0k1lgm")))

(define-public crate-cargo-bom-0.6.6 (c (n "cargo-bom") (v "0.6.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.14") (d #t) (k 0)))) (h "1xvpc2sjrvka0r1ls7a863533s4ksrq8a2mbpxz4jq0i8nikr4d4")))

(define-public crate-cargo-bom-0.6.7 (c (n "cargo-bom") (v "0.6.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.72") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.14") (d #t) (k 0)))) (h "1npi05xawlsdfd0hcrpb9j8kdvddhfm4snna8jjqldhdy10714n1")))

(define-public crate-cargo-bom-0.6.8 (c (n "cargo-bom") (v "0.6.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo") (r "^0.75") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tabled") (r "^0.15") (d #t) (k 0)))) (h "06mms81g40xvxhix69zn4gh3bj67y1wmjy1drkqmzpc14ng72ihs")))

