(define-module (crates-io ca rg cargo_learn) #:use-module (crates-io))

(define-public crate-cargo_learn-0.1.0 (c (n "cargo_learn") (v "0.1.0") (h "1ylzg0njlg116pi5w13ndcmrv6ywc0sm3w8nblfs6sc6mlcd7m91")))

(define-public crate-cargo_learn-0.1.1 (c (n "cargo_learn") (v "0.1.1") (h "17s8s1n54i7gh86zk82a11qxvwviwjsdq2clv64mmbfq200xcc56") (y #t)))

