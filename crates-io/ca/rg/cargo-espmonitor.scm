(define-module (crates-io ca rg cargo-espmonitor) #:use-module (crates-io))

(define-public crate-cargo-espmonitor-0.1.0 (c (n "cargo-espmonitor") (v "0.1.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.1.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1jz5hvrb80icwaam5czlgc9cb0mx0gq2ga72rhyin2iswpgj630x")))

(define-public crate-cargo-espmonitor-0.2.0 (c (n "cargo-espmonitor") (v "0.2.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0k6xi4dsd3g5gcjz6740xhbv9zaz9k5vhv94lqxrkqi60i0x8m25")))

(define-public crate-cargo-espmonitor-0.3.0 (c (n "cargo-espmonitor") (v "0.3.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "00siswnn41sgplp4pwv29wf3qrpyc754y887f89kjibdi26g4nnn")))

(define-public crate-cargo-espmonitor-0.4.0 (c (n "cargo-espmonitor") (v "0.4.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0lahqray0vpmwjhlp0f8zkykgaxaqrpi2nm5dq4wy9nc79g6dhcp")))

(define-public crate-cargo-espmonitor-0.4.1 (c (n "cargo-espmonitor") (v "0.4.1") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.4.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1sm3ljz2rmb3sjan98zq63aj6hmb3m6kxlnqp1msq9x3wn9rb89p")))

(define-public crate-cargo-espmonitor-0.4.2 (c (n "cargo-espmonitor") (v "0.4.2") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.4.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0nm58lhvrvn59xv5sgk52q78jj1b65lsc5k8xnzkxbxapsjfc4j0")))

(define-public crate-cargo-espmonitor-0.5.0 (c (n "cargo-espmonitor") (v "0.5.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.5.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "11s65dd6hg1dznrsk0ff6b1nbln2z0nzial4mimp40f3cbvjvgh2")))

(define-public crate-cargo-espmonitor-0.5.1 (c (n "cargo-espmonitor") (v "0.5.1") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.5.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0dibiwwvn0jpb0d778h0qksklzac82j3bsng0hxab78cpfglpl1s")))

(define-public crate-cargo-espmonitor-0.5.2 (c (n "cargo-espmonitor") (v "0.5.2") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.5.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1ixcprn1bi5hfjkpjrjh0yvk3dk0vq1l7x3rlja7vm8nb3pcn0ii")))

(define-public crate-cargo-espmonitor-0.6.0 (c (n "cargo-espmonitor") (v "0.6.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.6.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "0ffv66418hdw1yhlc88q7hcxyvwghhkkgmp5ndvnznr30gx1df8a")))

(define-public crate-cargo-espmonitor-0.6.1 (c (n "cargo-espmonitor") (v "0.6.1") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.6.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1hwc4xjljk0lwhyc041cw617g56xhxaw1dh0z7j7nqxy31xpz8lq")))

(define-public crate-cargo-espmonitor-0.6.2 (c (n "cargo-espmonitor") (v "0.6.2") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.6.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "02vf48h5v26m7wily1m2n4z4s7nx06xihy1ak331w083j8jvhf5y")))

(define-public crate-cargo-espmonitor-0.7.0 (c (n "cargo-espmonitor") (v "0.7.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "espmonitor") (r "^0.7.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)))) (h "1966jbqpfd34194l0fw5nc7ym988lgxvj7hf4g9vv862gic0fknl")))

(define-public crate-cargo-espmonitor-0.8.0 (c (n "cargo-espmonitor") (v "0.8.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "espmonitor") (r "^0.8.0") (d #t) (k 0)))) (h "0rn80d8yiy14iasiwyzwgr4s87ijqvm7hvyhi9y035zbabz8rdb7")))

(define-public crate-cargo-espmonitor-0.8.1 (c (n "cargo-espmonitor") (v "0.8.1") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "espmonitor") (r "^0.8.1") (d #t) (k 0)))) (h "158s0m7n5a3cizcy291j3dvs1gzgfwxszdnc8i3kaqpvr8qw4jca")))

(define-public crate-cargo-espmonitor-0.9.0 (c (n "cargo-espmonitor") (v "0.9.0") (d (list (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "espmonitor") (r "^0.9.0") (d #t) (k 0)))) (h "0lx7g9r45i86k0lk65qm3278bvirw5s0a411b3jgr78fplairbnz")))

(define-public crate-cargo-espmonitor-0.10.0 (c (n "cargo-espmonitor") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-project") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "espmonitor") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jxjj5j9zb923pxzwdidbyld4rgkj9bwwn06ljy2lx77rwjvzjhd")))

