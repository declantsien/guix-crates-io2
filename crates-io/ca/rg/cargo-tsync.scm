(define-module (crates-io ca rg cargo-tsync) #:use-module (crates-io))

(define-public crate-cargo-tsync-0.0.1 (c (n "cargo-tsync") (v "0.0.1") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v33y74wndyfgism050agy8194ijdwnmgh0mc60gksnyzlsy2qri") (y #t)))

(define-public crate-cargo-tsync-0.1.0 (c (n "cargo-tsync") (v "0.1.0") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "136vlagdvxb6z0vwp3v14j42z5f4bchpzn45h9dnlxg0kar1x7xb") (y #t)))

(define-public crate-cargo-tsync-0.1.1 (c (n "cargo-tsync") (v "0.1.1") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i50q0vfa130vhmp36gqahgy5asdhsyaijf7ziai8h8klrys8di6") (y #t)))

(define-public crate-cargo-tsync-0.1.2 (c (n "cargo-tsync") (v "0.1.2") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "087yqkhsbm7g89pm5qm3nvsp318fcinsr0nbahrzw4q731f8m23s") (y #t)))

(define-public crate-cargo-tsync-0.1.3 (c (n "cargo-tsync") (v "0.1.3") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00lqxrksc0pramfzxgvnzr4hhyr7zgfpiy1gs075cb16s79198hn") (y #t)))

(define-public crate-cargo-tsync-999.9.9 (c (n "cargo-tsync") (v "999.9.9") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "102brhg94aimcgz9rvvwgr4cba4ajnfb2jh4nvml8ajf3fs9kc8j") (y #t)))

