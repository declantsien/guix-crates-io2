(define-module (crates-io ca rg cargo-rost) #:use-module (crates-io))

(define-public crate-cargo-rost-0.0.1 (c (n "cargo-rost") (v "0.0.1") (h "0l66435qg8mqbp91aaschsd6y36kd7hbgw304abcaw2wy91mz1yy")))

(define-public crate-cargo-rost-0.1.0 (c (n "cargo-rost") (v "0.1.0") (h "1s9cmnfli3yqixlkk9hrhqxjq3p2c37axrnd1pgzzxhggy3r022z")))

(define-public crate-cargo-rost-0.1.1 (c (n "cargo-rost") (v "0.1.1") (h "1bmblhka8dpr77ab6facnxj287w3a32h83q1mpvr4mphi2agicdh")))

(define-public crate-cargo-rost-0.1.2 (c (n "cargo-rost") (v "0.1.2") (h "0g5190izk6vd0m6mzif21bg21yd9x8zlmlnbglmqwqbcfk1sgjiq")))

(define-public crate-cargo-rost-0.1.3 (c (n "cargo-rost") (v "0.1.3") (h "1zg4r6p8j3yiwsnb4w41svbfz3x6cn1670fmgpyb5xybpssrv6sn")))

