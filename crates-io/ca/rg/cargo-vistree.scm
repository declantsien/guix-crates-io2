(define-module (crates-io ca rg cargo-vistree) #:use-module (crates-io))

(define-public crate-cargo-vistree-0.1.0 (c (n "cargo-vistree") (v "0.1.0") (d (list (d (n "cargo-lock") (r "^9.0.0") (f (quote ("dependency-tree"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "1gbnyxx4qyi35r64814xn9z7lwfwv75rdmkr09s31dbrnfp4c5wi")))

(define-public crate-cargo-vistree-0.1.2 (c (n "cargo-vistree") (v "0.1.2") (d (list (d (n "cargo-lock") (r "^9.0.0") (f (quote ("dependency-tree"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "0vb304p4jmja8q4chhd5axkki78sy9h2mi15kkwy3p0mfmz4vxl1")))

