(define-module (crates-io ca rg cargo-cd) #:use-module (crates-io))

(define-public crate-cargo-cd-0.0.0 (c (n "cargo-cd") (v "0.0.0") (h "0yngjwvb5cp21jpvip066wg6csygm1z5s1iy7sm5wp6xh8arkd3c")))

(define-public crate-cargo-cd-0.0.1 (c (n "cargo-cd") (v "0.0.1") (h "0256g0ck38jd30jn7mhvfz77azpy864ql57ri9m9ab9jwzm675fh")))

(define-public crate-cargo-cd-0.0.2 (c (n "cargo-cd") (v "0.0.2") (h "1jyqpiyyvwff63vf1b37gbhp5kz9a6h0v7ninvnppbw4zkk1x4py")))

