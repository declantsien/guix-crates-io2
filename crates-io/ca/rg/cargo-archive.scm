(define-module (crates-io ca rg cargo-archive) #:use-module (crates-io))

(define-public crate-cargo-archive-0.1.0 (c (n "cargo-archive") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "125zh6bh62j8bh3dyl3aqv4dksrrm60a78n0z884m7a3q9v6gwlp")))

