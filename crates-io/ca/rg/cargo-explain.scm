(define-module (crates-io ca rg cargo-explain) #:use-module (crates-io))

(define-public crate-cargo-explain-1.0.0 (c (n "cargo-explain") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "syntect") (r "^4.4.0") (f (quote ("parsing" "assets" "dump-load" "regex-onig"))) (k 0)) (d (n "textwrap") (r "^0.12.1") (d #t) (k 0)))) (h "1smzk3yih2hsbs3ddqfh01vsc822c8mbm17g94p8rk3s1l62yks9")))

