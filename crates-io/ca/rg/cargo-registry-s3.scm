(define-module (crates-io ca rg cargo-registry-s3) #:use-module (crates-io))

(define-public crate-cargo-registry-s3-0.1.0 (c (n "cargo-registry-s3") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1xwlj078x28caygkx3hdl05qgrg141xghn4mwph3i0gy48q1fn0l")))

(define-public crate-cargo-registry-s3-0.2.0 (c (n "cargo-registry-s3") (v "0.2.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "18lnq268ini9vlna7ak661bwbwjx8xrxz98hls0im2s6pw72h9sj")))

