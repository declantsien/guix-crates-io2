(define-module (crates-io ca rg cargo-embassy) #:use-module (crates-io))

(define-public crate-cargo-embassy-0.1.0 (c (n "cargo-embassy") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "open") (r "^5.0.1") (d #t) (k 0)) (d (n "probe-rs") (r "^0.21.1") (d #t) (k 0)))) (h "0dw0y11jp4rls39swapv83ny7yxaxvvrdn3604iwaj9rccqvqk9w")))

(define-public crate-cargo-embassy-0.2.0 (c (n "cargo-embassy") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "open") (r "^5.0.1") (d #t) (k 0)) (d (n "probe-rs") (r "^0.21.1") (d #t) (k 0)))) (h "0hn4c9a6b8axw1m7adi331pkj0bxma7ya7vkkzlj7ivgrm8fzych")))

(define-public crate-cargo-embassy-0.2.1 (c (n "cargo-embassy") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "open") (r "^5.0.1") (d #t) (k 0)) (d (n "probe-rs") (r "^0.21.1") (d #t) (k 0)) (d (n "termimad") (r "^0.29.1") (d #t) (k 0)))) (h "1z1jpp31vlhgfi4sgyscx7g1db52dh7cy01zldrz82hkdcwsb6nc")))

