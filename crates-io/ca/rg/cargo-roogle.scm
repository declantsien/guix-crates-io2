(define-module (crates-io ca rg cargo-roogle) #:use-module (crates-io))

(define-public crate-cargo-roogle-0.1.0 (c (n "cargo-roogle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "0cxly1qmk0kx89jwk0pq6ws61wdlck3x6va94a6yrmj9af2bk6fm")))

