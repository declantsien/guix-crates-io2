(define-module (crates-io ca rg cargo-docco) #:use-module (crates-io))

(define-public crate-cargo-docco-0.1.0 (c (n "cargo-docco") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "rocco") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0ja04dbqdcimwzmh4fylf8ys7qglfl21qg8c3ypp71ba5n69cycq")))

