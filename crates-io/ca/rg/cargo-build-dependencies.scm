(define-module (crates-io ca rg cargo-build-dependencies) #:use-module (crates-io))

(define-public crate-cargo-build-dependencies-0.1.0 (c (n "cargo-build-dependencies") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0v5wrr8fm2yp2hp7az7bhdal7mxx7j8nhsk9sj3ghdak8q0p3qhq")))

(define-public crate-cargo-build-dependencies-0.1.1 (c (n "cargo-build-dependencies") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0m5liibxjwmsijnrd4f4si369268r4cscgl217lngzmywf01l4mn")))

(define-public crate-cargo-build-dependencies-0.1.2-rc.0 (c (n "cargo-build-dependencies") (v "0.1.2-rc.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1i993gbrn5hk9kc4n3k30mr5avxhydma51y4b2iky5m5ypifhk0q")))

(define-public crate-cargo-build-dependencies-0.1.2 (c (n "cargo-build-dependencies") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0lb598ayn72qaahyp06cg4g4bypjd7xqjxq4162bxnhbsx0kx1wl")))

(define-public crate-cargo-build-dependencies-0.1.3 (c (n "cargo-build-dependencies") (v "0.1.3") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "postmerge-hook" "run-cargo-fmt" "run-cargo-check" "run-cargo-clippy" "run-cargo-test"))) (k 2)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0lf21mkvg3wl1fhdd264831nvlffzpnwqqzw0pjhvy7i893dgp5y")))

