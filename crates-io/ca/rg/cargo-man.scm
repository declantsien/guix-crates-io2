(define-module (crates-io ca rg cargo-man) #:use-module (crates-io))

(define-public crate-cargo-man-0.0.1 (c (n "cargo-man") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1fcqicv7r038ahhdlwqjrgmbxabmm4bjqsjj4w04ilmys30nvbxb")))

(define-public crate-cargo-man-0.0.2 (c (n "cargo-man") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "07gyi59dvcij797lmdyfnkbbkkxs7ri79c4d0ffai0p58g6pi4cc")))

(define-public crate-cargo-man-0.0.3 (c (n "cargo-man") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1kh568c57spma4w8mv7n7jw3ahbs6g72mhix3sd9v3h5m7s4mhkr")))

(define-public crate-cargo-man-0.0.4 (c (n "cargo-man") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1c8qjhppbwvzvcagdd75visis75g5dhkr7jpgqr1zswzah8dq3i9")))

(define-public crate-cargo-man-0.1.0 (c (n "cargo-man") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1qw97abddxalkcs1dzchfyksw4f326xj7hch2kwp7h7f2q6jjbmf")))

(define-public crate-cargo-man-0.1.1 (c (n "cargo-man") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1xsqp89gmgb096i65xxxkfr0i8q16fzh7zr13mmlsyd6f6chcf8k")))

(define-public crate-cargo-man-0.1.2 (c (n "cargo-man") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0fwbqary7jcv4mapcfv5lf3ppm4k0hmr3a4k3ak7dsh70hba1ii2")))

