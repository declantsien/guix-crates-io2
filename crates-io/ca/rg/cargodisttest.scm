(define-module (crates-io ca rg cargodisttest) #:use-module (crates-io))

(define-public crate-cargodisttest-0.2.0 (c (n "cargodisttest") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0cahg936msf34v2aibapzk62jr3253yhablm83a84v0yf2cm6jw1")))

(define-public crate-cargodisttest-0.3.0 (c (n "cargodisttest") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "01lfianq2pla06l2daxzawivqhnc1z99vmgd1v4ia6yfph4jxhdi")))

(define-public crate-cargodisttest-0.3.1 (c (n "cargodisttest") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "13l9pyv1l3bcqnbcj8j66k1bddwn2q1pgqlz9pjcigqn82g655j7")))

(define-public crate-cargodisttest-0.3.2 (c (n "cargodisttest") (v "0.3.2") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0k7dk1j1nqzdv5xrsarxgf09sgj6v5lns81v05797hglb4kjs91d")))

(define-public crate-cargodisttest-0.3.3 (c (n "cargodisttest") (v "0.3.3") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1bq5rw8r9v4448iiph86k33hwp19qqmzw56y8dxffbx8gwra9gpd")))

(define-public crate-cargodisttest-0.3.4 (c (n "cargodisttest") (v "0.3.4") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "07cjpxn4dl8qh222lrzdw5rcp3dpyg4fxxyjvz64b0dg9964l9dx")))

(define-public crate-cargodisttest-0.3.5 (c (n "cargodisttest") (v "0.3.5") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1lsq4cairls2i5zpw7pxclrwwcmrvg5q3cjazmnc01c6x7kjahgi")))

(define-public crate-cargodisttest-0.3.6 (c (n "cargodisttest") (v "0.3.6") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0phaqyrlaa6nkkm6j4s8y00lvs7g71skg17a3dx77w660b3ggzy9")))

(define-public crate-cargodisttest-0.3.7 (c (n "cargodisttest") (v "0.3.7") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "075l5zi1xkn7s2vdrkk07rgwkmd07yv8bzsh4ddc00hp9kjzaz4p")))

(define-public crate-cargodisttest-0.3.8 (c (n "cargodisttest") (v "0.3.8") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0hv1k5h02r91czxqjr17xamaprxkk5h3pyfbrncifzv7q35mlipr")))

(define-public crate-cargodisttest-0.3.9 (c (n "cargodisttest") (v "0.3.9") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0738rnl4nl94fc5z2fk37l3yglkwqqy4ns2v4mqh1lfrc60mml0q")))

(define-public crate-cargodisttest-0.3.10 (c (n "cargodisttest") (v "0.3.10") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1qgmx47gx9iha74k2pwpx62nyxplig2iyhwyagdq52bl95xahk5g")))

(define-public crate-cargodisttest-0.3.12 (c (n "cargodisttest") (v "0.3.12") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1fcjwlg5ai10j5nf9hllb30nz211pxb99gcdqlv396mcsbylinhi")))

(define-public crate-cargodisttest-0.3.15 (c (n "cargodisttest") (v "0.3.15") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1xb563b8xi2khbsw918x0g0xxaa6jcacs13ryhb97h5jqlxaaim9")))

(define-public crate-cargodisttest-0.3.17 (c (n "cargodisttest") (v "0.3.17") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "14xs6j1mxghpax72xixksqd12m9hw60iiia8xnwinwfpq43dfzsi")))

(define-public crate-cargodisttest-0.3.18 (c (n "cargodisttest") (v "0.3.18") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1dl24jm0brijy3x04b3xpvqs9w9xh46rfzz7h1vv7bbkpz4gq3qz")))

(define-public crate-cargodisttest-0.4.0 (c (n "cargodisttest") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "18p1qzpvsrmjbamh2pjcd2km98kbd3v4g09db77f5kwk26dj8w37")))

(define-public crate-cargodisttest-0.4.1 (c (n "cargodisttest") (v "0.4.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1j4yin4fp8p8prsdjw0wz13k4bjp4wi3qf16i2q9hy0psz9fdbv0")))

(define-public crate-cargodisttest-0.5.0 (c (n "cargodisttest") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0gn2713hxjgfmx407ihzmxrkhvrhsd7yxrn1yl37qhbfyj1rh8kj")))

(define-public crate-cargodisttest-0.6.4 (c (n "cargodisttest") (v "0.6.4") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "19rcc3qxb4j9w9fxysgayva5i55fxn11gq78y54xc6scmp8xg16x")))

(define-public crate-cargodisttest-0.7.0 (c (n "cargodisttest") (v "0.7.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0r4jqcjxna6c8mbb5p8ll6z60qk5nixd0hqxhz01sn2n2ikfy4cf")))

(define-public crate-cargodisttest-0.7.1 (c (n "cargodisttest") (v "0.7.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1rh0vmsl9gmdc8vz262h2vj0diw2n2s1rpm3s2raydzs2d2nhcyk")))

(define-public crate-cargodisttest-0.7.2 (c (n "cargodisttest") (v "0.7.2") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0ij6ckm4s5zwy7z2n70rppbc2l89lp17kfix94bnckr57vr4pjhv")))

(define-public crate-cargodisttest-0.7.3 (c (n "cargodisttest") (v "0.7.3") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0qhm3zwvdzsl35cz05p1hi2pfyqb9lnm7l0c7xd6azzrq2nknhdk")))

(define-public crate-cargodisttest-0.20.0 (c (n "cargodisttest") (v "0.20.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "13p2qpqigggdqqr4nxvs0942q9zzdsxbpqn1b442flq92125lwzf")))

