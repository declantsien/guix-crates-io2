(define-module (crates-io ca rg cargo-fox) #:use-module (crates-io))

(define-public crate-cargo-fox-0.0.0 (c (n "cargo-fox") (v "0.0.0") (h "18vy4793inih0kx7n1622lhpflkfm6qi24w09049s7s4ybgiaqwg") (y #t)))

(define-public crate-cargo-fox-0.0.1 (c (n "cargo-fox") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0vcllqlpcx58gysh4091vlcx6yr5kcailns8qh3jv24wl9d6h6az")))

