(define-module (crates-io ca rg cargo-ws-version) #:use-module (crates-io))

(define-public crate-cargo-ws-version-1.0.5 (c (n "cargo-ws-version") (v "1.0.5") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kg4bffnfr3bzl0yiigdc6i34hsr8ym4bsg0rxi9b51ivpirg91r") (y #t)))

(define-public crate-cargo-ws-version-1.0.6 (c (n "cargo-ws-version") (v "1.0.6") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0awqw3hmnncpjir4gk5gjysyx6mh4lpd3z6gs8s3dynmhg1y7a9r") (y #t)))

(define-public crate-cargo-ws-version-1.0.7 (c (n "cargo-ws-version") (v "1.0.7") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ajqi5677f6mwv200gd3ikvg9gg16k5qqmic9xypyq946w13fsdw")))

