(define-module (crates-io ca rg cargo-dock) #:use-module (crates-io))

(define-public crate-cargo-dock-0.1.0 (c (n "cargo-dock") (v "0.1.0") (d (list (d (n "clap") (r "^2.25.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "unindent") (r "^0.1.0") (d #t) (k 0)))) (h "0kmmshrybd4j01i3fdyfjqs49cbp6w36v6il61v2py73kxbwapl2")))

