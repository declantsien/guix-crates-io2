(define-module (crates-io ca rg cargo-vibe) #:use-module (crates-io))

(define-public crate-cargo-vibe-0.0.1 (c (n "cargo-vibe") (v "0.0.1") (d (list (d (n "buttplug") (r "^6.1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgiqv9gijpxyrvxk5ysjimvaanaspsgj5hkz2d57y16sqk8rabf")))

(define-public crate-cargo-vibe-0.0.2 (c (n "cargo-vibe") (v "0.0.2") (d (list (d (n "buttplug") (r "^6.1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18nfaka8a0h4zjdn06a17snp16prdbb8vlas2kkhbkby2r6zs190")))

(define-public crate-cargo-vibe-0.0.3 (c (n "cargo-vibe") (v "0.0.3") (d (list (d (n "buttplug") (r "^6.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bbvb36km7zbrs3lc3001yp2g5hhn0cv1x5g95x26araj79gakh7")))

(define-public crate-cargo-vibe-0.0.4 (c (n "cargo-vibe") (v "0.0.4") (d (list (d (n "buttplug") (r "^6.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zmgsaz47s57xl9rg44nh3fmxjnb6wbq7mv5g363vapmwv7z6m0x")))

