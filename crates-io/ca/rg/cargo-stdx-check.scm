(define-module (crates-io ca rg cargo-stdx-check) #:use-module (crates-io))

(define-public crate-cargo-stdx-check-0.1.0 (c (n "cargo-stdx-check") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "19nr6c6xqs9pwkb24y61x8217sd9r7vyz0wyy8r9lr5v5z27ax2n")))

(define-public crate-cargo-stdx-check-0.1.1 (c (n "cargo-stdx-check") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "1j0899wva1ya9zpwsy328sykkf52knmgvgnhzmr829y54yx7y09s")))

(define-public crate-cargo-stdx-check-0.1.2 (c (n "cargo-stdx-check") (v "0.1.2") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "0fnb64dzn2w9xf36njibbw605a8b9343npx60yf109xmizskz2g8")))

