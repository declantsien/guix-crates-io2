(define-module (crates-io ca rg cargo-owo) #:use-module (crates-io))

(define-public crate-cargo-owo-0.1.0 (c (n "cargo-owo") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1l1n9kz2krv9hx5mcj50rpcfa99nbiqbxkq90dvh94lpf6cr2b4x")))

(define-public crate-cargo-owo-0.1.1 (c (n "cargo-owo") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jsd5q0mfcy6haqzlcskd8vihjb95x6imm2hg9l51p4jb7h8rkbq")))

(define-public crate-cargo-owo-0.1.2 (c (n "cargo-owo") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1b6da2cxpg3rlwzipcwzszxlpyqw1clfvb2aglp5nmb8lmmy9nrh")))

