(define-module (crates-io ca rg cargo-smol) #:use-module (crates-io))

(define-public crate-cargo-smol-0.1.0 (c (n "cargo-smol") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "16dfcjqm00r12zbfnxk4sfpix58sa792fd1hzvvy5z35j4665yb8")))

(define-public crate-cargo-smol-0.1.1 (c (n "cargo-smol") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0iwxgglwdni2lchbc0r8vgamz26l9jap3y1al597mq3nwrmf3xmx")))

(define-public crate-cargo-smol-0.1.2 (c (n "cargo-smol") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1qx0qac7kqc64pplg5iw6yr9vb6qi2mx74mszcw7a1m4qd898aa7")))

(define-public crate-cargo-smol-0.2.0 (c (n "cargo-smol") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "15nlbgk51myghcssvamy6kqrxxm60s8dwxfkqdr67981wwk6fykc")))

