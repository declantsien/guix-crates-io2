(define-module (crates-io ca rg cargo-upgrade-command) #:use-module (crates-io))

(define-public crate-cargo-upgrade-command-0.1.0 (c (n "cargo-upgrade-command") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "1vy096d5ixa6ar086whsdmzkaw2sczh1l2c2h1jhn4w5nzfiwx9a")))

(define-public crate-cargo-upgrade-command-0.1.1 (c (n "cargo-upgrade-command") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "00f6q9kxishxi8v0fqs6cv5fp87ljjxlkxy537a1kwzz51z1hlaj")))

(define-public crate-cargo-upgrade-command-0.2.0 (c (n "cargo-upgrade-command") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "0vmvsr33khdcccnm36g6ibkw9ifah5ds5ssbj0pxannc29xxbwfq")))

(define-public crate-cargo-upgrade-command-0.3.0 (c (n "cargo-upgrade-command") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "1nknj42qwfml0qvslv9fb5015spgcfmx01zcafsif6v8wajzfcsq")))

