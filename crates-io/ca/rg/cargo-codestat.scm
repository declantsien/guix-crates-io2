(define-module (crates-io ca rg cargo-codestat) #:use-module (crates-io))

(define-public crate-cargo-codestat-0.1.0 (c (n "cargo-codestat") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "15mgz1vcaaailksspsslninlzicyd6yn2h76a3qs0ff1azdlczgf")))

