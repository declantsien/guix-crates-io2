(define-module (crates-io ca rg cargo-release-script) #:use-module (crates-io))

(define-public crate-cargo-release-script-0.1.2 (c (n "cargo-release-script") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.6") (d #t) (k 0)))) (h "0y5ljiyyicknn71bh3cjan5b705bifh2jc1anp6awvsci4m9bia6")))

(define-public crate-cargo-release-script-0.1.3 (c (n "cargo-release-script") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.6") (d #t) (k 0)))) (h "1qs6q7w5rxs9b65xv1jbspqgwc4c4krzrp8x2hwfdcvai53zs8jd")))

