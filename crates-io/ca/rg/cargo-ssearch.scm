(define-module (crates-io ca rg cargo-ssearch) #:use-module (crates-io))

(define-public crate-cargo-ssearch-0.1.0 (c (n "cargo-ssearch") (v "0.1.0") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "1jay7av4irifcixrvbhkj0y0cddrvlsyzfbmnb4459v96srhdd7k")))

(define-public crate-cargo-ssearch-0.1.1 (c (n "cargo-ssearch") (v "0.1.1") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0vads8izn1xz5mvzqm6n1248xkbvnil9ncv7zpxfqs1p1ixn1wnn")))

(define-public crate-cargo-ssearch-0.1.2 (c (n "cargo-ssearch") (v "0.1.2") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "1y8qlzjv3l9fhskwbfzkkyw84g3ahk5azrfnc6bazpjpwl18jxrk")))

(define-public crate-cargo-ssearch-0.1.3 (c (n "cargo-ssearch") (v "0.1.3") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "1zybdl03di8yjngkvqgsxkn7y4k3f4l4c9vwraf64yn3z1h9kk1i")))

