(define-module (crates-io ca rg cargo-install-latest) #:use-module (crates-io))

(define-public crate-cargo-install-latest-0.1.0 (c (n "cargo-install-latest") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "17nn68m8bhhyqxhivxjphfrz9d61ag2g85db4vv262jvafrj9r4v")))

