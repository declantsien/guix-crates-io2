(define-module (crates-io ca rg cargo-local-install) #:use-module (crates-io))

(define-public crate-cargo-local-install-0.1.0 (c (n "cargo-local-install") (v "0.1.0") (h "06d82y6z7drqb0l922nyqnz6zrbxn3nfhgpqbxyv7fiyy508m4xc")))

(define-public crate-cargo-local-install-0.1.1 (c (n "cargo-local-install") (v "0.1.1") (h "16bam04cm2c6f5jb02zjz3a8rgiiwz6fg660mj3ypzbrx19l7j0g")))

(define-public crate-cargo-local-install-0.1.2 (c (n "cargo-local-install") (v "0.1.2") (h "0al0rxc93x9wbsg8lx0jqgii4r4a50mnjb80ra0jr246q182sb9g")))

(define-public crate-cargo-local-install-0.1.3 (c (n "cargo-local-install") (v "0.1.3") (h "10g4v0qr0f1i5f57b404844vbwgb1yif7djiq5ql9ay3s99mr7xr")))

(define-public crate-cargo-local-install-0.1.4 (c (n "cargo-local-install") (v "0.1.4") (h "1ws99krbd57lvx4ffd97g5jfn724wqdiqckad7cfa2g4c06qrxpr")))

(define-public crate-cargo-local-install-0.1.5 (c (n "cargo-local-install") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1cwcdlmxshdp4hz9g175amlsqb5fx195lmf3qkdjw0lly2zv0cjn") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.6 (c (n "cargo-local-install") (v "0.1.6") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "10v03mwvv75bpl2wqbdjfgn3fqa2mhakr0lng1bg82iybqw3f8rh") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.7 (c (n "cargo-local-install") (v "0.1.7") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0aqk34v0w0fdnnm6q8aqpymi740ki4088adrvkf5l60b231l76bm") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.8 (c (n "cargo-local-install") (v "0.1.8") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0ia8ig7ngpb4mkphhr46aznzvpr6k93lpb7k41l2501zx88zbv68") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.9 (c (n "cargo-local-install") (v "0.1.9") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0hn65invx7cd1h1vkhq6l6hrv70psrr1n88ypwq9cflvjf6w5rcj") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.10 (c (n "cargo-local-install") (v "0.1.10") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0zzsgymdygy4blpk70l8hx2wijxb4lq5nnsrkiid35ai2chq9hv0") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.11 (c (n "cargo-local-install") (v "0.1.11") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "19c8ps1rai3wrkjxc011qxx882h6dss5i2dcjij1h3s12jgmcg0q") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

(define-public crate-cargo-local-install-0.1.12 (c (n "cargo-local-install") (v "0.1.12") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0kjbbxklnayrhvb0i4v2820n85cqkysbc9lc5f4zfhz35vw7s6zz") (f (quote (("manifest" "serde" "toml") ("default" "manifest"))))))

