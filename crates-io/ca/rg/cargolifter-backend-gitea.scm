(define-module (crates-io ca rg cargolifter-backend-gitea) #:use-module (crates-io))

(define-public crate-cargolifter-backend-gitea-0.1.0 (c (n "cargolifter-backend-gitea") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cargolifter-core") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01992wkfvzcir13zn6bs18sbyyrqlyhr3h31skjx317h8912nmmn")))

