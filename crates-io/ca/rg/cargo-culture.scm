(define-module (crates-io ca rg cargo-culture) #:use-module (crates-io))

(define-public crate-cargo-culture-0.1.0 (c (n "cargo-culture") (v "0.1.0") (d (list (d (n "cargo-culture-kit") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1fndzjf2qbh0qinn451p7a0akfgndaajr4pnwrc8wxgdmjxq8wlf")))

(define-public crate-cargo-culture-1.0.0 (c (n "cargo-culture") (v "1.0.0") (d (list (d (n "cargo-culture-kit") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1fy7125y7ff8nlf0y7n3lsi49mbdy2iyx0xb6g616f7lif55rxdb")))

