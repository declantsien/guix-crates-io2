(define-module (crates-io ca rg cargo-pretag-version-issue-test) #:use-module (crates-io))

(define-public crate-cargo-pretag-version-issue-test-0.1.0 (c (n "cargo-pretag-version-issue-test") (v "0.1.0") (h "1kkmbqa1fx3zdksdy2b6kldackzz29gffi34mkx63f69di7bll8q")))

(define-public crate-cargo-pretag-version-issue-test-0.1.1-pre0 (c (n "cargo-pretag-version-issue-test") (v "0.1.1-pre0") (h "1dyi1jj50rfmsyf1hirp8pgbrmf784n9fqgq2ja91c3x46hpbc61")))

(define-public crate-cargo-pretag-version-issue-test-0.1.1 (c (n "cargo-pretag-version-issue-test") (v "0.1.1") (h "11c8m5n7q5whky5666iy9h7pqxzpbk12ngs2l97vppfaasfbw1wd") (y #t)))

