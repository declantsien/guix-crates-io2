(define-module (crates-io ca rg cargo-intraconv) #:use-module (crates-io))

(define-public crate-cargo-intraconv-1.0.0 (c (n "cargo-intraconv") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04iq8z6xr6hj787sf1mspqzh3cbbhfc7c557jh32n10pb3ia0cgw")))

(define-public crate-cargo-intraconv-1.0.1 (c (n "cargo-intraconv") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1h53v9h45valywq7gwaads4hdm1543x4pwdaw7nsjkrcp0b5qz0i")))

(define-public crate-cargo-intraconv-1.1.0 (c (n "cargo-intraconv") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05nnrdi8hfb1ky300x8bgjhwysglk7yg3980ym147n8sgbbyyqc4")))

(define-public crate-cargo-intraconv-1.2.0 (c (n "cargo-intraconv") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1am9v6zfv1dhyskzh56cp4n2bz567vb4fpphf08q6b0rd31jg5nb")))

(define-public crate-cargo-intraconv-1.3.0 (c (n "cargo-intraconv") (v "1.3.0") (d (list (d (n "ansi_term") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "argh") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "cargo_metadata") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "glob") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1fa5fsq0d4va54lbva4npmv2464vnnp21c2i7sg15hjwac8f69a5")))

(define-public crate-cargo-intraconv-1.4.0 (c (n "cargo-intraconv") (v "1.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03s1kakka0gjfndknkbw182895prs8m5vfjnrz5i5ba0zksf8ab7")))

