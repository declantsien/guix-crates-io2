(define-module (crates-io ca rg cargo-mommy) #:use-module (crates-io))

(define-public crate-cargo-mommy-0.1.0 (c (n "cargo-mommy") (v "0.1.0") (h "1al0aqvg6znswjja1l2l680sn1896rzny3r0r58qis9vhp5bq7dy")))

(define-public crate-cargo-mommy-0.1.1 (c (n "cargo-mommy") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "01jflrdcnb5ya4c9jh04wni7a73qfb4p1i8pf76d5f5y0410v7bh")))

(define-public crate-cargo-mommy-0.2.0 (c (n "cargo-mommy") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "0jmfmddgnaz9isqq315pk1m1mb12165gzcq3v318vyn7j1jf81yb")))

(define-public crate-cargo-mommy-0.2.1 (c (n "cargo-mommy") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "0xsz1xrbixqbmzw1gk0xyjr7pb3hglf7p3jb57dywxsf964wwxdi") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

(define-public crate-cargo-mommy-0.3.0-prerelease.1 (c (n "cargo-mommy") (v "0.3.0-prerelease.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "0ygpbggx82az5n968ckq0qr5vgf5b0y269rw7c9crqb0rxk1yd3y") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

(define-public crate-cargo-mommy-0.3.0-prerelease.2 (c (n "cargo-mommy") (v "0.3.0-prerelease.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "1jdafx1y3zq6sv32bc3h214lz2r73c7pb04rlf7wgmb8bl5fhrid") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

(define-public crate-cargo-mommy-0.3.0 (c (n "cargo-mommy") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "03dfyrwlg4q6biclgrli3z631m9fgphi91l93j5pc93ffnh9vi9v") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

(define-public crate-cargo-mommy-0.3.1 (c (n "cargo-mommy") (v "0.3.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 1)))) (h "0y0zaq389vfqabncydxhd56dqdf0pdc7mj0pjbqw3vfibpl1zmwy") (f (quote (("yikes") ("thirsty") ("default" "thirsty" "yikes"))))))

