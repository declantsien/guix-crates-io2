(define-module (crates-io ca rg cargo-dao) #:use-module (crates-io))

(define-public crate-cargo-dao-0.1.0 (c (n "cargo-dao") (v "0.1.0") (h "0mk0ja1j0mc4bgviaglmx6zaarapwjyxgqdvv9cjcrhsryx4q6pb")))

(define-public crate-cargo-dao-0.1.1 (c (n "cargo-dao") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-pack") (r "^0.9") (d #t) (k 0)))) (h "0wv4l8k9m6nl0ngd792h511n73hfpadmk2allkvhz27mvkqxmg1s")))

(define-public crate-cargo-dao-0.2.0 (c (n "cargo-dao") (v "0.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-pack") (r "^0.9") (d #t) (k 0)))) (h "06adwwyqxjqsvxlfi8rl9dbff1qk658728g0fhy1s6g6raa7s12w")))

