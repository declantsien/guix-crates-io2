(define-module (crates-io ca rg cargo_meta_proc) #:use-module (crates-io))

(define-public crate-cargo_meta_proc-0.1.0 (c (n "cargo_meta_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "153h96xl8cl1y304aly4rzcbvchywh8h6z75y3s8wgz1x3s27xxv")))

(define-public crate-cargo_meta_proc-0.1.1 (c (n "cargo_meta_proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "046vc4j67akfaj736lf5rirrb1f39w0mhvsd7md6wisk95a7k2a5")))

(define-public crate-cargo_meta_proc-0.1.2 (c (n "cargo_meta_proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1lz0kywc1ccrg0mihca02bhnga418wspfny2s8y510iqgh3n144g")))

(define-public crate-cargo_meta_proc-0.1.3 (c (n "cargo_meta_proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0my0icmxdcb83dw04pcp7wcba70nzdzkcrxq5vqbdb604pwwp5xp")))

(define-public crate-cargo_meta_proc-0.1.4 (c (n "cargo_meta_proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0m47p5apk1xlmw7xws5l044k0kj1bsccrswl01qaq3dhrm7315va")))

