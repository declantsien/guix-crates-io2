(define-module (crates-io ca rg cargo-aur) #:use-module (crates-io))

(define-public crate-cargo-aur-1.0.0 (c (n "cargo-aur") (v "1.0.0") (d (list (d (n "auto_from") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0s6kswsmfll3ar6739vhsk2dyp47lg3rvaivv43jzl7g1ydiagb9")))

(define-public crate-cargo-aur-1.0.1 (c (n "cargo-aur") (v "1.0.1") (d (list (d (n "auto_from") (r "^0.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1l5qmah12zw4l2757khfw9wbq3jwnlk41bfdv1amn7xkpwfdimgl")))

(define-public crate-cargo-aur-1.0.2 (c (n "cargo-aur") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nv5zay8pzxlkacvf9jdvih4nxa355zslw6xzzcs5rqspdh38cvd")))

(define-public crate-cargo-aur-1.0.3 (c (n "cargo-aur") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "04qshlhnj7ialf1vkg143nd0ygjljmizkxczqqc2yai1vck2mr26")))

(define-public crate-cargo-aur-1.1.0 (c (n "cargo-aur") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "159xpsvlqidzh81f8zl78fhyyrnwn7isiilvbxxr51kgaizkvqrg") (y #t)))

(define-public crate-cargo-aur-1.1.1 (c (n "cargo-aur") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1zap3sg8pa1gdnh8h2c1sss66my62qsvwwfr00hk8ymbai4svdys")))

(define-public crate-cargo-aur-1.1.2 (c (n "cargo-aur") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xmxh1sl8hs7a6rba9j14j3cbsdcgxx19a70x3qk61q95h0q3prq")))

(define-public crate-cargo-aur-1.2.0 (c (n "cargo-aur") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19fpb7xrfwnh0hcpyjmhz8l5a9j90wvw61vsp42hvd7g1yr18k34")))

(define-public crate-cargo-aur-1.3.0 (c (n "cargo-aur") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0g9jz11lvy9gx6a98bwjjdj90hyfyb8n6jscnhvk84kn5vjjd9iz")))

(define-public crate-cargo-aur-1.4.0 (c (n "cargo-aur") (v "1.4.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1yg1mi64smyw891dyif7dqccqbyjcxkg81bnnsv5y5har8fjq3py")))

(define-public crate-cargo-aur-1.4.1 (c (n "cargo-aur") (v "1.4.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08fphqhzsgms4245m88nf77alxqyzm3llkj9aj2z7nwrdw3m4kpp")))

(define-public crate-cargo-aur-1.5.0 (c (n "cargo-aur") (v "1.5.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1rc7a4m3ch8sy939xhn9vfppkmad79nm0f5rq1mzl4mnnkh42f9a")))

(define-public crate-cargo-aur-1.6.0 (c (n "cargo-aur") (v "1.6.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1wvrdcqbp2zzb7bh50dkzng18p9zha7z5n4w086jkbqr7mwl2rd6")))

(define-public crate-cargo-aur-1.7.0 (c (n "cargo-aur") (v "1.7.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1r7g4ym49nl8vsg3k7grwa8lz8v6rszyg89w5yqgkhgg91cnh294") (y #t)))

(define-public crate-cargo-aur-1.7.1 (c (n "cargo-aur") (v "1.7.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "075kx84ffyky1sv731r9r6mjsp7s62275wgdyy12fnzvh82dsxd4")))

