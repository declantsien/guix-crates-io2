(define-module (crates-io ca rg cargo-gist) #:use-module (crates-io))

(define-public crate-cargo-gist-0.0.0 (c (n "cargo-gist") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0wxyg9rw4cibmrayg4hk732mvcpw9iydnpx75yi2lf5vghaibd33")))

(define-public crate-cargo-gist-0.0.1 (c (n "cargo-gist") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.3") (d #t) (k 0)))) (h "1asxwyj434kp4f6xxb7kja9ay4l725l9qcn604jrvpcahrw57y5p")))

