(define-module (crates-io ca rg cargo_new_aoc) #:use-module (crates-io))

(define-public crate-cargo_new_aoc-0.1.0 (c (n "cargo_new_aoc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "06xsh37rjd36d52iyjgcgxdnwapympzx66dsh6ddmw51xwf9izdr")))

(define-public crate-cargo_new_aoc-0.1.1 (c (n "cargo_new_aoc") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "13c76psidr4y6gwb72g7jbbzzh9qp3jczq5s1iysi95nsj94hwz6")))

