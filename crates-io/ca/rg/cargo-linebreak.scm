(define-module (crates-io ca rg cargo-linebreak) #:use-module (crates-io))

(define-public crate-cargo-linebreak-1.0.0 (c (n "cargo-linebreak") (v "1.0.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)))) (h "02r7896iahis0nydlg4qj3aipwmqfc28jrqiwgy5yk8xzdgd5cid")))

(define-public crate-cargo-linebreak-1.0.1 (c (n "cargo-linebreak") (v "1.0.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)))) (h "0x0fcc3hqxvhqr8bsfs0fjyn1hs4cz85bkvhd7bpgqswccn8nxmc")))

(define-public crate-cargo-linebreak-2.0.0 (c (n "cargo-linebreak") (v "2.0.0") (d (list (d (n "argonaut") (r "^0.7.0") (d #t) (k 0)))) (h "0m3c3v15053pl2m1na0xf5xhdwyi8l1b5r1vff05n2zvbx09h3zr")))

