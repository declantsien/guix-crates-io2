(define-module (crates-io ca rg cargo-minicov) #:use-module (crates-io))

(define-public crate-cargo-minicov-0.1.0 (c (n "cargo-minicov") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "postcard") (r "^0.5.0") (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "17ggi9k3g7ihah54d9n93vkdc88j9s4rmw31bhirsmnvbwc00hyg")))

(define-public crate-cargo-minicov-0.1.1 (c (n "cargo-minicov") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "postcard") (r "^0.5.0") (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "13jsqq7ryqviwk23prdrsz3wyzqyj3fwn3xx85ijvnf7as20hzvy")))

(define-public crate-cargo-minicov-0.1.2 (c (n "cargo-minicov") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.62") (d #t) (k 1)) (d (n "postcard") (r "^0.5.1") (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "12pi1l5ly3byf601v2wbf4k9s97s8ih4fkgahbimv9jn10a80x5g")))

