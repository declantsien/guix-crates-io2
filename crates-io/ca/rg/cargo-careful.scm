(define-module (crates-io ca rg cargo-careful) #:use-module (crates-io))

(define-public crate-cargo-careful-0.1.0 (c (n "cargo-careful") (v "0.1.0") (d (list (d (n "directories") (r "^3") (d #t) (k 0)))) (h "0050711amn6g5zlnii928nhxdc7py4a48nyzbzp2rs60gb71izx6")))

(define-public crate-cargo-careful-0.2.0 (c (n "cargo-careful") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "08npmqbxnx49cjmcm89jsldv54q6ls1gir9xcvll3sr91dlrid0w")))

(define-public crate-cargo-careful-0.2.1 (c (n "cargo-careful") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1wqsbbd1vvpxbwzs5hzmggk50a3apgliv14kp7z3g94cr4r3rknq")))

(define-public crate-cargo-careful-0.2.2 (c (n "cargo-careful") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "directories") (r "^3") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0wrdiymk7bd7z0k1knv0pjw92lw1pmxvhfy7asgsz0gmzf9p4y3g")))

(define-public crate-cargo-careful-0.2.3 (c (n "cargo-careful") (v "0.2.3") (d (list (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1xjd2bv2g2pf6fyj4hdr9mg6kljnxp7l18nx5lxskrq0g8c3qh4g")))

(define-public crate-cargo-careful-0.2.4 (c (n "cargo-careful") (v "0.2.4") (d (list (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.3.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "0xa5xw6l0dk3ipkcyjb7mdzcpcqq0vpf0a6kr5kzbxxhajfhq1b3")))

(define-public crate-cargo-careful-0.3.0 (c (n "cargo-careful") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0whnrq3whh5069k8a3x8vmy2m5fzirymk8w6n9s204ki9bb3ca4l")))

(define-public crate-cargo-careful-0.3.1 (c (n "cargo-careful") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0qfjl39hrv17k262ai452prvcf1dmmj8jk8p7mwrv8xsylk7h1wx")))

(define-public crate-cargo-careful-0.3.2 (c (n "cargo-careful") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0ckb7ybldicbhcdi0c8j7bb5skyb1sk30s42gppcpd99j0v6b6iw")))

(define-public crate-cargo-careful-0.3.4 (c (n "cargo-careful") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1x1lh0jgdgz5m060pi81rm4r33km9zlkcp269cvjk50pibmhv4il")))

(define-public crate-cargo-careful-0.4.0 (c (n "cargo-careful") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1395iz029cn7g41hhj0m8bdm7l9my6x1amzml6xh2hxix2l7khm6")))

(define-public crate-cargo-careful-0.4.1 (c (n "cargo-careful") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1yr2mn48f361xjsn45cf360nl4d1zi2y49phakfyqbsy2mqpmdw5")))

(define-public crate-cargo-careful-0.4.2 (c (n "cargo-careful") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "rustc-build-sysroot") (r "^0.5.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1wi71a2j5p53avf3317az5a96cacrg6xbsv4lsksfd10af1vwndb")))

