(define-module (crates-io ca rg cargo_crates-io_docs-rs_test) #:use-module (crates-io))

(define-public crate-cargo_crates-io_docs-rs_test-0.1.0+1 (c (n "cargo_crates-io_docs-rs_test") (v "0.1.0+1") (h "107f9awm7zigjy34i38ss2lcra2s97ic07c4gxnc6amgg5k8ybxw") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.1.0+2 (c (n "cargo_crates-io_docs-rs_test") (v "0.1.0+2") (h "17fb3vhnrssn3hmnm7xc54rm61arcz0h7d0397h1pyirc8040vkg") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.1.0+3 (c (n "cargo_crates-io_docs-rs_test") (v "0.1.0+3") (h "0m2mhbl8nli1xm9m0bm5qw2p6250n7b4c0l1q7nbv4akapn4g1aq") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.1.0 (c (n "cargo_crates-io_docs-rs_test") (v "0.1.0") (h "1kp59xs276v49igswfb4nl7qapz3w14y9wpxziaw53q3lg40m09d") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.2.0 (c (n "cargo_crates-io_docs-rs_test") (v "0.2.0") (h "0g3mlhrin310lrxrsw1kpflp7jkrj4625g1kxhry61mfp1mizh13") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.2.0+1 (c (n "cargo_crates-io_docs-rs_test") (v "0.2.0+1") (h "0h59q6dq8lkfmarmbh3gfcxg4n53ak0x0dc9kpgyrbcv8478213w") (y #t)))

(define-public crate-cargo_crates-io_docs-rs_test-0.2.0-1 (c (n "cargo_crates-io_docs-rs_test") (v "0.2.0-1") (h "19avd0lw4fvv9zs3g4yipbw2k1q4r1ksgas7nxcy6c1pnzx9xdfg")))

(define-public crate-cargo_crates-io_docs-rs_test-0.3.0 (c (n "cargo_crates-io_docs-rs_test") (v "0.3.0") (h "1gmk4w99iff1wfd566a4cf04486bcfbrdmz0w1q2pz78gxz4r04j")))

(define-public crate-cargo_crates-io_docs-rs_test-0.3.0-1 (c (n "cargo_crates-io_docs-rs_test") (v "0.3.0-1") (h "1kvvl4arbxchfn6xxphaavcdisz45vypp73nakzv99pyagl280m3")))

(define-public crate-cargo_crates-io_docs-rs_test-0.3.0-rc.1 (c (n "cargo_crates-io_docs-rs_test") (v "0.3.0-rc.1") (h "144kji4a0aj5lfd0qy60dhdnklyww6chjkrw4rp5p1pyvs0hv2gy")))

(define-public crate-cargo_crates-io_docs-rs_test-0.4.0-1 (c (n "cargo_crates-io_docs-rs_test") (v "0.4.0-1") (h "0xxsax7gs99p8hxn76gmc9a0yvhd443ixyhqpm60i51hx9spncka")))

(define-public crate-cargo_crates-io_docs-rs_test-0.4.0-rc.1 (c (n "cargo_crates-io_docs-rs_test") (v "0.4.0-rc.1") (h "0pqxbzkvndbj74izsf0fafrqpi8fnfvcdlrnv0flvsdx4v9r1c0z")))

(define-public crate-cargo_crates-io_docs-rs_test-0.4.0-rc.2 (c (n "cargo_crates-io_docs-rs_test") (v "0.4.0-rc.2") (h "0bwhh7i9x9lg4q90hyjip9c76dwf7sgw07fkq44h6f2mza8ad6f6")))

(define-public crate-cargo_crates-io_docs-rs_test-0.4.0-rc.3 (c (n "cargo_crates-io_docs-rs_test") (v "0.4.0-rc.3") (h "10knygfivrlccbjyjwhmbibrg9qa5gjam1vxzixrssii8rlwhh2v")))

(define-public crate-cargo_crates-io_docs-rs_test-0.1.0-rc.1 (c (n "cargo_crates-io_docs-rs_test") (v "0.1.0-rc.1") (h "1dy49hz06n49wp5xg87vx727y547i2jg278jqr8wy5bhya4ba8fl")))

(define-public crate-cargo_crates-io_docs-rs_test-0.5.0 (c (n "cargo_crates-io_docs-rs_test") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.1") (d #t) (k 0)))) (h "09slkpc9kzz1a5fa2flzfhg6c9ndimc00gnj2vnbq1077ai2zd9x") (f (quote (("nng-tls") ("nng-stats") ("legacy-111-rc4") ("default" "nng-stats") ("cmake-vs2017-win64") ("cmake-vs2017") ("cmake-ninja"))))))

