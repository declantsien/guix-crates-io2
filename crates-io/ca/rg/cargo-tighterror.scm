(define-module (crates-io ca rg cargo-tighterror) #:use-module (crates-io))

(define-public crate-cargo-tighterror-0.0.2 (c (n "cargo-tighterror") (v "0.0.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.2") (d #t) (k 0)))) (h "0gy2h3n3qbf7abn2bp5l784lb2wqhfvp4dzk2k7nf8sb563y9s1q")))

(define-public crate-cargo-tighterror-0.0.3 (c (n "cargo-tighterror") (v "0.0.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.3") (d #t) (k 0)))) (h "0awghj89kr192yp46hafa04s9gb8wj6m87nnw2ras199m84mn28a")))

(define-public crate-cargo-tighterror-0.0.4 (c (n "cargo-tighterror") (v "0.0.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.4") (d #t) (k 0)))) (h "0j6h8yfwadwb3srf7sgs5gi8vk928jqjqx3i09n06c4xi142nz1z")))

(define-public crate-cargo-tighterror-0.0.5 (c (n "cargo-tighterror") (v "0.0.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.5") (d #t) (k 0)))) (h "0vs8j6agwnzydfldq9npr2bw2gcdbn1vzc44cf01hg1zhm93z0vd")))

(define-public crate-cargo-tighterror-0.0.6 (c (n "cargo-tighterror") (v "0.0.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.6") (d #t) (k 0)))) (h "1pin1x24lf976b59ff998zn60p45j3rahznrrqdj46m51fwxqgfq")))

(define-public crate-cargo-tighterror-0.0.7 (c (n "cargo-tighterror") (v "0.0.7") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.7") (d #t) (k 0)))) (h "1341myaifxkfqgq03nxdvq586daricx4lmch87ba60lr3z993ckl")))

(define-public crate-cargo-tighterror-0.0.8 (c (n "cargo-tighterror") (v "0.0.8") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.8") (d #t) (k 0)))) (h "0rlcq9hf3dyi55rxz8h2k9wchxvx6q7q3g564sgsaciv5z6266g5")))

(define-public crate-cargo-tighterror-0.0.9 (c (n "cargo-tighterror") (v "0.0.9") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.9") (d #t) (k 0)))) (h "0bhmbi2374y6fns1mcx70ca0cc7qm6bngxflpxakh0gx7jlns4l8")))

(define-public crate-cargo-tighterror-0.0.10 (c (n "cargo-tighterror") (v "0.0.10") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.10") (d #t) (k 0)))) (h "1s1pnf6iabq68p50jg1f78xbki1f37l018kgh7329cl23hfscvr1")))

(define-public crate-cargo-tighterror-0.0.11 (c (n "cargo-tighterror") (v "0.0.11") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.11") (d #t) (k 0)))) (h "0fmg2kv3dbxa94kp46ll5qd1wk3rq8qygf020za0ibixakyqsyk5")))

(define-public crate-cargo-tighterror-0.0.12 (c (n "cargo-tighterror") (v "0.0.12") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.12") (d #t) (k 0)))) (h "0mkaq58lw7f5pg1mj7r2g47z0sj34pdih2k3a7j49787fr2rq4gq")))

(define-public crate-cargo-tighterror-0.0.13 (c (n "cargo-tighterror") (v "0.0.13") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.13") (d #t) (k 0)))) (h "09pk8nyfgikvd0ncl3dql28zki31rjw9jfmbq307h67qcm9f3hxw")))

(define-public crate-cargo-tighterror-0.0.14 (c (n "cargo-tighterror") (v "0.0.14") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.14") (d #t) (k 0)))) (h "19zflcy1kdpsqxbrqsr4hxjs47j1y7ll7qbpqxyj73db7vaprcsk")))

(define-public crate-cargo-tighterror-0.0.15 (c (n "cargo-tighterror") (v "0.0.15") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tighterror-build") (r "^0.0.15") (d #t) (k 0)))) (h "0nib4qali26kjigq3ljgqnmq3wwgiw1yik3rim1gswisgj88k84g")))

