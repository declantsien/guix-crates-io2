(define-module (crates-io ca rg cargo-public-items) #:use-module (crates-io))

(define-public crate-cargo-public-items-0.0.1 (c (n "cargo-public-items") (v "0.0.1") (d (list (d (n "public_items") (r "^0.0.3") (d #t) (k 0)))) (h "0znwmnv5arax6acj38fdimnm1ccdj80xvwma6s82385nbxa82z16")))

(define-public crate-cargo-public-items-0.0.2 (c (n "cargo-public-items") (v "0.0.2") (d (list (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.0.4") (d #t) (k 0)))) (h "0f3znjy3w1vcybb2zp6nxyz60nc2yhgfsvjqha1c6aschfi5kiy3")))

(define-public crate-cargo-public-items-0.0.5 (c (n "cargo-public-items") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.0.5") (d #t) (k 0)))) (h "01s4zpwh26yi3jxshrfrxpmgqqm2273is9zrwa7vkzrcw8lpd734")))

(define-public crate-cargo-public-items-0.0.6 (c (n "cargo-public-items") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.0.6") (d #t) (k 0)))) (h "1vl5ggm0v540lsr9vldriaf6p3nwxd60kbml0dwzmmcbs2701w4g")))

(define-public crate-cargo-public-items-0.1.0 (c (n "cargo-public-items") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.1.0") (d #t) (k 0)))) (h "0abs6lwnk71nlv8vz2rx5jwq39cwg35fxk0rkk02f312yzbsling")))

(define-public crate-cargo-public-items-0.2.0 (c (n "cargo-public-items") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.2.0") (d #t) (k 0)))) (h "19sdaqp2cmr0dhsr503m97q4xd21n77vjl4mfz9hw5dylb1j2wdq")))

(define-public crate-cargo-public-items-0.3.0 (c (n "cargo-public-items") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.3.0") (d #t) (k 0)))) (h "076yqbh1zni8d59zf1iklh8icl1qhm2b0alw5zam1vxg1h87h9x0")))

(define-public crate-cargo-public-items-0.4.0 (c (n "cargo-public-items") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.10.3") (d #t) (k 0)) (d (n "public_items") (r "^0.4.0") (d #t) (k 0)))) (h "1vqhrpn2y43pscg0cp6ai46ig4ffv2xjsn981h1piz21rq4lii16")))

(define-public crate-cargo-public-items-0.5.0 (c (n "cargo-public-items") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "public_items") (r "^0.5.0") (d #t) (k 0)))) (h "17npadlvgcv74vibvkmvmaz03zf94a31w0hyx15yjwn2zaisbmy2")))

(define-public crate-cargo-public-items-0.5.2 (c (n "cargo-public-items") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "public_items") (r "^0.5.2") (d #t) (k 0)))) (h "1lg9xr81kdyf3gsxpc1qysr7zv29xfz9326m2wy9h0kbi1h23d2d")))

(define-public crate-cargo-public-items-0.6.0 (c (n "cargo-public-items") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "public_items") (r "^0.6.0") (d #t) (k 0)))) (h "11krq15rv1mqgf4v0q9g2ylypvhjchzqfg7glsb92x4qw4flvfxi")))

(define-public crate-cargo-public-items-0.7.0 (c (n "cargo-public-items") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "public_items") (r "^0.7.0") (d #t) (k 0)))) (h "0amgqb6kgcwslw57xyw4rjgy62dynj49fm7i71v8glmnkq4znwi1")))

(define-public crate-cargo-public-items-0.7.1 (c (n "cargo-public-items") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "public_items") (r "^0.7.1") (d #t) (k 0)))) (h "1bv7j8q85d675farcsv6n533cqds3q93hx1ydcznvdp7n78j7y90")))

(define-public crate-cargo-public-items-0.7.2 (c (n "cargo-public-items") (v "0.7.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "public_items") (r "^0.7.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)))) (h "11b8z0f99nwyb2a18x4lyh94h11c1s1rycj46g477nqg9d1jy7s6")))

(define-public crate-cargo-public-items-0.8.0 (c (n "cargo-public-items") (v "0.8.0") (h "13pfmx2ljcdjpf1fzf7c2nfyg6cmiwj3x1r3nadqd6yl7jxx93zw")))

