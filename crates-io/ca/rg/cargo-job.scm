(define-module (crates-io ca rg cargo-job) #:use-module (crates-io))

(define-public crate-cargo-job-0.1.0 (c (n "cargo-job") (v "0.1.0") (h "1j8d4r456r362lg835k0zqrjjiha334jv3r1s1yqmmxgv60kfyz0")))

(define-public crate-cargo-job-0.1.1 (c (n "cargo-job") (v "0.1.1") (h "1df23536kgq3gj8vi06yg967pc1k9bzd3r5q4zkm7kmz27q68kdd")))

