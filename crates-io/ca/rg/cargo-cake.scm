(define-module (crates-io ca rg cargo-cake) #:use-module (crates-io))

(define-public crate-cargo-cake-1.0.0 (c (n "cargo-cake") (v "1.0.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "0j55s8rnym4qpzjfly626ml2a6r8s2s47w8p7k5dzy99hvl7kn9p")))

(define-public crate-cargo-cake-1.0.1 (c (n "cargo-cake") (v "1.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "01wlywhs9pan7j4nzfl286cibxwk5bxvy71rgzviqdazgxxzbifz")))

(define-public crate-cargo-cake-1.0.2 (c (n "cargo-cake") (v "1.0.2") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "0ysk2v2iljxf3g8khhbg3m914w6kaiki52mlxcs8gshnlr81vcvp")))

(define-public crate-cargo-cake-1.0.3 (c (n "cargo-cake") (v "1.0.3") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "024d97dsb3dnjslbpzac6d1gbxh61awwccg96ycx9ci0fbnyjy9k")))

(define-public crate-cargo-cake-1.0.4 (c (n "cargo-cake") (v "1.0.4") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "1hhz2l7cab4mybr2giaiq519rrjahk3vm6galmk6bp8x1xnfy72l")))

(define-public crate-cargo-cake-1.0.5 (c (n "cargo-cake") (v "1.0.5") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "1kyxhnx41svmqdalnhs5zc17a10wwf7l5483n9birhb8xm1q25cl")))

(define-public crate-cargo-cake-1.0.6 (c (n "cargo-cake") (v "1.0.6") (d (list (d (n "rust_birthday") (r "0.1.*") (d #t) (k 0)))) (h "0rlfalq8fx7yffp6ljan6mc0kggs2mjx6lywgj1vny1mbkq62wyr")))

