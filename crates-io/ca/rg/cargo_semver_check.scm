(define-module (crates-io ca rg cargo_semver_check) #:use-module (crates-io))

(define-public crate-cargo_semver_check-0.1.0 (c (n "cargo_semver_check") (v "0.1.0") (h "0n54vbpzp62mzfmzymfxs1x2ysjkfdab21f1p5ri124hqscbp8jj") (y #t)))

(define-public crate-cargo_semver_check-0.2.0 (c (n "cargo_semver_check") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "ron") (r "^0.7.1") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "trustfall_core") (r "^0.0.2") (d #t) (k 0)))) (h "03yvm05sb77dyxb3sn2rv2nxnb7zcg7zlcsplcvyssd7aw1jnv01") (y #t)))

