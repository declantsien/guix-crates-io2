(define-module (crates-io ca rg cargo_rub) #:use-module (crates-io))

(define-public crate-cargo_rub-0.0.1 (c (n "cargo_rub") (v "0.0.1") (d (list (d (n "buildable") (r "^0.0.1") (d #t) (k 0)) (d (n "docopt") (r "= 0.6.14") (d #t) (k 0)))) (h "1ajyr6d8a17f43d0ih6b5wwzrcb4fg9h49jxmcg9bp9hxrvygmdj")))

(define-public crate-cargo_rub-0.0.2 (c (n "cargo_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0j8fsf92dc0g5li287vhib0vsz2q6ymm1nwy5shs72xa2rw6s36a")))

(define-public crate-cargo_rub-0.0.3 (c (n "cargo_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0hycnsxbb781g0dpvxng7ss38ch9l22i59gmvkrj70hr81w9hghm")))

(define-public crate-cargo_rub-0.0.4 (c (n "cargo_rub") (v "0.0.4") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "13lddf3mxmmsjyzmlmvpr3km3r09ayp9nb9drl1ga81fwyrn35nb")))

