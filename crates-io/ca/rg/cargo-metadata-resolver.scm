(define-module (crates-io ca rg cargo-metadata-resolver) #:use-module (crates-io))

(define-public crate-cargo-metadata-resolver-0.1.1 (c (n "cargo-metadata-resolver") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1ik7aii5krc39qssm0fnqsq7drg1pcbnf61l5a449his5bs62czj")))

