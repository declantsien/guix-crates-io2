(define-module (crates-io ca rg cargo-tidy) #:use-module (crates-io))

(define-public crate-cargo-tidy-0.1.0 (c (n "cargo-tidy") (v "0.1.0") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "toml") (r "^0.2.1") (d #t) (k 0)))) (h "0mdwppznwv70ib411gdnvvcm00bwx58167jpk7hsmxaxcshk1g60")))

