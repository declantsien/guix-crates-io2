(define-module (crates-io ca rg cargo_uploads_test) #:use-module (crates-io))

(define-public crate-cargo_uploads_test-0.1.0 (c (n "cargo_uploads_test") (v "0.1.0") (h "13c6gkm4xr2hkf540vp9xrlszinrvisr29d2h8rnl583fy2h27la")))

(define-public crate-cargo_uploads_test-0.1.1 (c (n "cargo_uploads_test") (v "0.1.1") (h "073b33yq9d3bvcadadpr83337dmrv9k6pznggwsmw2c97qzri801")))

