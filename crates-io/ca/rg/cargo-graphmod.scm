(define-module (crates-io ca rg cargo-graphmod) #:use-module (crates-io))

(define-public crate-cargo-graphmod-1.0.0 (c (n "cargo-graphmod") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1whydasmg8f3zyj06h813hqd2vjisi78mwprmm98wh87j5y35g15") (y #t)))

(define-public crate-cargo-graphmod-1.0.1 (c (n "cargo-graphmod") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k34fv17z5hxfqfdlq2rig3sbwiysahvf1lay5l7zrwwmp2a7rrq") (y #t)))

(define-public crate-cargo-graphmod-1.0.2 (c (n "cargo-graphmod") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "096197qaf6fzda4h28psc659g3iskis97hc89l8b3ldz88fh90l4") (y #t)))

(define-public crate-cargo-graphmod-1.0.3 (c (n "cargo-graphmod") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gnym5ghfj7d1w0vww69zvjlwxb5i5k63x9hhanms85krlnpsmlp") (y #t)))

(define-public crate-cargo-graphmod-1.0.4 (c (n "cargo-graphmod") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l3c56zfym9pdvnvjpqgimzklrnszh0lv4a4zs2g4mfdh8b6g8qj")))

(define-public crate-cargo-graphmod-1.0.5 (c (n "cargo-graphmod") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07hg5nz8qlfjiw0sjn7vnfikhl1zr6wcf5jcyjs6rq86z5nwrm2n") (y #t)))

(define-public crate-cargo-graphmod-1.0.6 (c (n "cargo-graphmod") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nr5l50saindc3arg6h1g8j2ldkjrxjgdcp1d920xiw25drg7m1m")))

(define-public crate-cargo-graphmod-1.1.0 (c (n "cargo-graphmod") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13hl8d12mqic355vlcvswa6z3xdq9q03k600gvrsdk4baq970snw")))

