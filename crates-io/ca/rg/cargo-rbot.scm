(define-module (crates-io ca rg cargo-rbot) #:use-module (crates-io))

(define-public crate-cargo-rbot-0.1.0 (c (n "cargo-rbot") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "033ihm588lr3irxrcsahlb6bdx6r08myjq05ck4hanmrw0jq025n")))

(define-public crate-cargo-rbot-0.1.1 (c (n "cargo-rbot") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ref_slice") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)))) (h "0i02iw34xlrclag5y19nxq1bwqz4dxdbzqsslpzfpnnr8161g2cq")))

