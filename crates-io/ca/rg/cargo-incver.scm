(define-module (crates-io ca rg cargo-incver) #:use-module (crates-io))

(define-public crate-cargo-incver-0.1.0 (c (n "cargo-incver") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tomllib") (r "^0.1.2") (d #t) (k 0)))) (h "1n5iqz5qgm4hzp2rf77173mv9r3asqphg7l5jxkj1bxnwcksphzq")))

(define-public crate-cargo-incver-1.0.0 (c (n "cargo-incver") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tomllib") (r "^0.1.2") (d #t) (k 0)))) (h "1j9kx1ryhbag6am7ndbbksfx4fm0i6k2ki48b1japdw5s2my6ci0")))

