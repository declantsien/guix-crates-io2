(define-module (crates-io ca rg cargo-sync-readme) #:use-module (crates-io))

(define-public crate-cargo-sync-readme-0.1.0 (c (n "cargo-sync-readme") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0nn4k7q3g94lsvn9x6b6fbfmdkj62x3g3qnrw52a6rirrvvk2v22")))

(define-public crate-cargo-sync-readme-0.1.1 (c (n "cargo-sync-readme") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0w9246xgffyxnji5gfw4mfmdnsbrckkv56b5k57r0lbhqp76z0zx")))

(define-public crate-cargo-sync-readme-0.1.2 (c (n "cargo-sync-readme") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0z6ibzf69jvi9mbkjxaxj8gfpvld3krx29jvqwjgxskhmj62v0s2")))

(define-public crate-cargo-sync-readme-0.1.3 (c (n "cargo-sync-readme") (v "0.1.3") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1cs3rphm1sfnnp0wfhvd4xnnpxvn8wjz6hwjf49lr8zz7z0z0x9b")))

(define-public crate-cargo-sync-readme-0.1.4 (c (n "cargo-sync-readme") (v "0.1.4") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1hn30mdjcbq3yllr8hz9iz1zw5wv9p9wpfl2gpr79limf8p2igqy")))

(define-public crate-cargo-sync-readme-0.1.5 (c (n "cargo-sync-readme") (v "0.1.5") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1v8r5a8yfyxzmsccq856gjz2iy3dnrwhqa05rb5xbyg3s9kx48d1")))

(define-public crate-cargo-sync-readme-0.2.0 (c (n "cargo-sync-readme") (v "0.2.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0hn5cngyy23pk3z6q6fj6hx1mnxyppnzn2sbbvnn2mcf2a52729l")))

(define-public crate-cargo-sync-readme-0.2.1 (c (n "cargo-sync-readme") (v "0.2.1") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0cq9n827whxf5ip8d3h0d7aq3mcvmg5icszpwvf3k0zijzl2vv12")))

(define-public crate-cargo-sync-readme-1.0.0 (c (n "cargo-sync-readme") (v "1.0.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0nb74i03vsffp93piihdkp8kyp74gdsr0qgv6vhl1kar5rj1n3v6") (f (quote (("default" "cli") ("cli" "structopt"))))))

(define-public crate-cargo-sync-readme-1.1.0 (c (n "cargo-sync-readme") (v "1.1.0") (d (list (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1l8m2s62ak2qs5fn68mlv72mr08zq96q1xrsg7xzamlw9gzg59cz") (f (quote (("default" "cli") ("cli" "structopt"))))))

