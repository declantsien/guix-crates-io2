(define-module (crates-io ca rg cargo-docserve) #:use-module (crates-io))

(define-public crate-cargo-docserve-0.1.0 (c (n "cargo-docserve") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "staticfile") (r "^0.4") (d #t) (k 0)))) (h "0w9r1dw7ji0k6279xja446k4wamwp25hp6ljk4ryxv2f7i1jd101")))

