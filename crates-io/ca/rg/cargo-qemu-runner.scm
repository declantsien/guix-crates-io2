(define-module (crates-io ca rg cargo-qemu-runner) #:use-module (crates-io))

(define-public crate-cargo-qemu-runner-0.1.0 (c (n "cargo-qemu-runner") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1wbpljgjxwia4njqkzyaybkl75qmk2696s57z6h7vkn7r7ik8dvx")))

(define-public crate-cargo-qemu-runner-0.1.1 (c (n "cargo-qemu-runner") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1prvrk455qvy85xdk9iqc24g8pssfm7lw503msn0ka3kyra0h1i0")))

(define-public crate-cargo-qemu-runner-0.1.2 (c (n "cargo-qemu-runner") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)))) (h "16lskqbw40xmn7902wanvc9zwmc0gfjhjyxpvba6xvbxapnk6pha")))

