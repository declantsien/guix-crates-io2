(define-module (crates-io ca rg cargolifter-storage-filesystem) #:use-module (crates-io))

(define-public crate-cargolifter-storage-filesystem-0.1.0 (c (n "cargolifter-storage-filesystem") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cargolifter-core") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0sfjsn0cq3kv2j6iw2g09048253b7hn3f264iyrc8l9pfzs0i6is")))

(define-public crate-cargolifter-storage-filesystem-0.2.0 (c (n "cargolifter-storage-filesystem") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cargolifter-core") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19sgrwfif03nxhyacpcn7yslxjvcz80x01j86wfx5mhvc8yp6mwy")))

(define-public crate-cargolifter-storage-filesystem-0.3.0 (c (n "cargolifter-storage-filesystem") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cargolifter-core") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "041l8xr92k3nm0m92qrjhhvwhcyz9xj3y5wgs8lx5a04idq5jv7q")))

