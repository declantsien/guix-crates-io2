(define-module (crates-io ca rg cargo-primestack) #:use-module (crates-io))

(define-public crate-cargo-primestack-0.1.0 (c (n "cargo-primestack") (v "0.1.0") (h "0zj6vkyd50d8hi0d0qzpk1xwfib884l8xk2pibb0lkxpp70sciwq")))

(define-public crate-cargo-primestack-0.1.1 (c (n "cargo-primestack") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0l284yj97qd707rzv2x78ksnmclikv9zwmyhivw1mznb25i2ybx2")))

