(define-module (crates-io ca rg cargo-config) #:use-module (crates-io))

(define-public crate-cargo-config-0.1.0 (c (n "cargo-config") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "0xdj8jr7jlwkgybwn8f1l38j2h38p04rsdg0a3psfjkf9cx5a16l")))

(define-public crate-cargo-config-0.1.1 (c (n "cargo-config") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1lzbvbn2x92k8bv67qx08wkc7ppfs8c6f3vw0m1jy5anrl6wnd6c")))

