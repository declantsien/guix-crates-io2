(define-module (crates-io ca rg cargo-emit) #:use-module (crates-io))

(define-public crate-cargo-emit-0.1.0 (c (n "cargo-emit") (v "0.1.0") (h "0w6xr24p46p47hc33jzfn702md2iaw61b56gn9x0fisjpcdphl8x")))

(define-public crate-cargo-emit-0.1.1 (c (n "cargo-emit") (v "0.1.1") (h "1lznkir94hb1zxd11saxlz22vpga2vw1wzal5a146x6jfy06n7ad")))

(define-public crate-cargo-emit-0.2.0 (c (n "cargo-emit") (v "0.2.0") (d (list (d (n "insta") (r "^1.7.2") (d #t) (k 2)))) (h "1ciqw0nsgv9i9c4rf2nryn97dvqlc17ghrmsr7ba57h9350j0q4g")))

(define-public crate-cargo-emit-0.2.1 (c (n "cargo-emit") (v "0.2.1") (d (list (d (n "insta") (r "^1.7.2") (d #t) (k 2)))) (h "15an8al5qlgyi6ym9m388nd3k6fi6nqzzp14nbb6mpamwz4y30hm")))

