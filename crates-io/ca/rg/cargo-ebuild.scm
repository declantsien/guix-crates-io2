(define-module (crates-io ca rg cargo-ebuild) #:use-module (crates-io))

(define-public crate-cargo-ebuild-0.1.2 (c (n "cargo-ebuild") (v "0.1.2") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1xkqxgsdk16j0682pj5y7kvny9bqmkijb6skxp28frnl0jy8cq8h")))

(define-public crate-cargo-ebuild-0.1.3 (c (n "cargo-ebuild") (v "0.1.3") (d (list (d (n "cargo") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cm7r8gjp04f0nx3bf9pq5v6lfdgj5z4l617w8lz4ppi46p2qjc3")))

(define-public crate-cargo-ebuild-0.1.4 (c (n "cargo-ebuild") (v "0.1.4") (d (list (d (n "cargo") (r "^0.17") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0wcp0cgqbf7vzh8mcp965p5g2wffzy0p8k2ri5h76cqx5gdm8811")))

(define-public crate-cargo-ebuild-0.1.5 (c (n "cargo-ebuild") (v "0.1.5") (d (list (d (n "cargo") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "013jdrpwcigx16p7dbqncpsrzl21ck2gw78j48aalxfw1vz2fnji")))

(define-public crate-cargo-ebuild-0.2.0 (c (n "cargo-ebuild") (v "0.2.0") (d (list (d (n "cargo") (r "^0.37") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0661rmb5bdzkfvrrq0zlhb3ciacgpy9gdf15qriw4q636w9msqd9")))

(define-public crate-cargo-ebuild-0.3.0 (c (n "cargo-ebuild") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1j7gc4hyv4yb2mgw4ji2r349mrk95qh3kcfpqgnblk5h0nh7rlqp")))

(define-public crate-cargo-ebuild-0.3.1 (c (n "cargo-ebuild") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gpp1hzg0fzacpx05dw0x3w6vv020n2mbn3csalg85pkl25bfxrd")))

(define-public crate-cargo-ebuild-0.5.4 (c (n "cargo-ebuild") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cargo-lock") (r "^8.0.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rustsec") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "0zm7cq25yappyym66ihibnv74n4hfi2j9ik017slfc096lmgan25") (f (quote (("vendored-libgit2" "rustsec/vendored-libgit2") ("default" "vendored-libgit2"))))))

