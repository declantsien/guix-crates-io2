(define-module (crates-io ca rg cargo-tanker) #:use-module (crates-io))

(define-public crate-cargo-tanker-0.1.0 (c (n "cargo-tanker") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mp14s2kpjjs7zzpw5kw6ssjv429y9n3nrqbfgpqra45fr2x8b79")))

(define-public crate-cargo-tanker-0.1.1 (c (n "cargo-tanker") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "05xp6la8bbrg604lg7hnaxhqsxahla0hcl65vhblvmzqw5gb6b9c")))

