(define-module (crates-io ca rg cargo-osha) #:use-module (crates-io))

(define-public crate-cargo-osha-0.1.0 (c (n "cargo-osha") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1909z5kjzbawbf0iazy749x88c9r5jfkfkn4iypm1jim7ccbsdf3")))

(define-public crate-cargo-osha-0.1.1 (c (n "cargo-osha") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1qkh5ix3xfwj2ix6xd3qb9082a6mjwvl1a6l0q1y9axch3ck60il")))

