(define-module (crates-io ca rg cargo-dockerimage) #:use-module (crates-io))

(define-public crate-cargo-dockerimage-0.1.0 (c (n "cargo-dockerimage") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "018k2m7bcv0qbj4hhsjml2wcrzb7p1a7hkbd8x62vidl67716qws")))

