(define-module (crates-io ca rg cargo-add-dynamic) #:use-module (crates-io))

(define-public crate-cargo-add-dynamic-0.1.0 (c (n "cargo-add-dynamic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1pnl96aw3lkzzzlrvfk64wg7nf0shkxv16n41dhc7y0gvh3j16r3")))

(define-public crate-cargo-add-dynamic-0.1.1 (c (n "cargo-add-dynamic") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1y4vkiaxrad70n8snbizrdqjb7s9rzzrpjy2wq4jjln5xc62q7by")))

(define-public crate-cargo-add-dynamic-0.1.2 (c (n "cargo-add-dynamic") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "05qp5skyhrf5grvx0acgzskgb8z9m0v01fkpglpihhw4yzikqxlf")))

