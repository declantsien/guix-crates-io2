(define-module (crates-io ca rg cargo-expect) #:use-module (crates-io))

(define-public crate-cargo-expect-0.1.0 (c (n "cargo-expect") (v "0.1.0") (d (list (d (n "ipc-channel") (r "0.11.*") (d #t) (k 0)) (d (n "structopt") (r "0.2.*") (d #t) (k 0)))) (h "1p8hplfyqsw7dl7jrm4hff889sw7rxx9p38b7ahaigk2pd0as7s7")))

(define-public crate-cargo-expect-0.1.1 (c (n "cargo-expect") (v "0.1.1") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "crossbeam") (r "0.4.*") (d #t) (k 0)) (d (n "expectation-shared") (r "0.*.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)) (d (n "structopt") (r "0.2.*") (d #t) (k 0)))) (h "1y7h35vgckz7cbpxzzcry5nq4rxnifa9vwxrwv4b9yhicjjya67h")))

