(define-module (crates-io ca rg cargo-publish-crates) #:use-module (crates-io))

(define-public crate-cargo-publish-crates-0.0.13 (c (n "cargo-publish-crates") (v "0.0.13") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "duration-string") (r "^0") (d #t) (k 0)) (d (n "publish-crates") (r "=0.0.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z5810xrk6hm1jfk0bpqy2m06jvp6y8m42jsfnw73xfckqv4w212")))

(define-public crate-cargo-publish-crates-0.0.14 (c (n "cargo-publish-crates") (v "0.0.14") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "duration-string") (r "^0") (d #t) (k 0)) (d (n "publish-crates") (r "=0.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mgyiiwam1430x4qz74rmy6ir3z22cfliripagkibm3i0hz13mf9")))

(define-public crate-cargo-publish-crates-0.0.15 (c (n "cargo-publish-crates") (v "0.0.15") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "duration-string") (r "^0") (d #t) (k 0)) (d (n "publish-crates") (r "=0.0.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h5ghdms600jj8in5lni5ycc1yk5pbwaah5x8gip8v7qjf4c7r1f")))

(define-public crate-cargo-publish-crates-0.0.16 (c (n "cargo-publish-crates") (v "0.0.16") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "duration-string") (r "^0") (d #t) (k 0)) (d (n "publish-crates") (r "=0.0.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a3bgrg22k4d7hzpf4cx7haznxkyhjyyjs4rpzqw3pvb6afr3hl1")))

(define-public crate-cargo-publish-crates-0.0.17 (c (n "cargo-publish-crates") (v "0.0.17") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0") (d #t) (k 0)) (d (n "duration-string") (r "^0") (d #t) (k 0)) (d (n "publish-crates") (r "=0.0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fk0ymxx70gl4vkq6l629j2xamp5zrqqbblsbkbi4ln3m8gnvfk1")))

