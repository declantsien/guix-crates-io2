(define-module (crates-io ca rg cargo-subcommand-metadata) #:use-module (crates-io))

(define-public crate-cargo-subcommand-metadata-0.0.0 (c (n "cargo-subcommand-metadata") (v "0.0.0") (h "16s7mb5lq9jnvrbzvai7v9vbf9c85c8nhdjyxgiqz1549hiypayg") (y #t)))

(define-public crate-cargo-subcommand-metadata-0.1.0 (c (n "cargo-subcommand-metadata") (v "0.1.0") (h "1flirf87nn7zgk5j721w8jamz6z5m1k3frbnfsnw85nvm203ngd3")))

