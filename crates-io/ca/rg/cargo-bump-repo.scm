(define-module (crates-io ca rg cargo-bump-repo) #:use-module (crates-io))

(define-public crate-cargo-bump-repo-0.1.0 (c (n "cargo-bump-repo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.7") (d #t) (k 0)))) (h "0f5kxlasjfb8v5lyv50alsgrx2n0b940c537ffn79y3b1cysyiak")))

