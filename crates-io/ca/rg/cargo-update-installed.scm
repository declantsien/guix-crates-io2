(define-module (crates-io ca rg cargo-update-installed) #:use-module (crates-io))

(define-public crate-cargo-update-installed-0.1.0 (c (n "cargo-update-installed") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0719kn17bkvx4lyb4w3szg8havmahcpzkvh8zc9bh9jbdq6zfb09")))

(define-public crate-cargo-update-installed-0.1.1 (c (n "cargo-update-installed") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "13531lwgxmgi4jxamy2w3sldp56c81q3vzp61g39i35magpz9i0q")))

(define-public crate-cargo-update-installed-0.1.2 (c (n "cargo-update-installed") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "1f7sqbv79la2vig2xx6dd30wz2ja7phzarq308fd9d60iy05k985")))

(define-public crate-cargo-update-installed-0.1.3 (c (n "cargo-update-installed") (v "0.1.3") (d (list (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0frbn4vwgm6zp6x3bsd5iq8q8lgidafga9ydph9vks7kwflvk1ja")))

