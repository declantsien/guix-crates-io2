(define-module (crates-io ca rr carr_madan) #:use-module (crates-io))

(define-public crate-carr_madan-0.1.0 (c (n "carr_madan") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "0p8qy8v5pd6qqbwadbf1sax3yi6ddqlg0qk472bqapn3k3aj45yv")))

(define-public crate-carr_madan-0.2.0 (c (n "carr_madan") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "0w0c47ijm64jkylzjqb0ps2bcp3nczaf5kfsh35ciq2ibg13z50i")))

(define-public crate-carr_madan-0.2.1 (c (n "carr_madan") (v "0.2.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "1d36q4p42c0mjahzspyc4dsiwiyf7yfs29vk8n4drzgyvwxag4qa")))

(define-public crate-carr_madan-0.3.0 (c (n "carr_madan") (v "0.3.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "1gl7bm249q7g9l74izx4szwpq8adlnn8qkyr22m1nxi1qjfirsxq")))

(define-public crate-carr_madan-0.4.0 (c (n "carr_madan") (v "0.4.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost_option") (r "^0.28.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "0fcia5zs0dv4n1wdyla5b1hrhrdrijvdv2ws43ylw0a6wrd47h6p")))

