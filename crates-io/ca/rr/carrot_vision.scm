(define-module (crates-io ca rr carrot_vision) #:use-module (crates-io))

(define-public crate-carrot_vision-0.1.0 (c (n "carrot_vision") (v "0.1.0") (d (list (d (n "carrot_utils") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.195") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1ck1bymvsl9sx76hkihw98b30h3c5lgw340d25slcqsbnfpdhqv0")))

