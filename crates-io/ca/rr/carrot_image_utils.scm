(define-module (crates-io ca rr carrot_image_utils) #:use-module (crates-io))

(define-public crate-carrot_image_utils-0.1.0 (c (n "carrot_image_utils") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "04l7k7mpwiz95pa6ff6zgnwbsbmjzm50zly8zjln0jhw6fdh2rr8")))

(define-public crate-carrot_image_utils-0.1.1 (c (n "carrot_image_utils") (v "0.1.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1mbrbynszi78yihfhkaidcybp3hm1rn278ppkm2h23k5pxv79iz5")))

(define-public crate-carrot_image_utils-0.1.2 (c (n "carrot_image_utils") (v "0.1.2") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1cm1ixk2rhjiipij4j8f5aby4svn2s6lm6ymd0af3bj5yq9zmg0h")))

(define-public crate-carrot_image_utils-0.1.3 (c (n "carrot_image_utils") (v "0.1.3") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0b1xqyn5wg4amjrqch088j908nbn0338a3knd57bvrd499c7p3wy")))

(define-public crate-carrot_image_utils-0.1.4 (c (n "carrot_image_utils") (v "0.1.4") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0dgyfkdcwkfqqma7aac2cmlbg2i0k2c8gxn98nz8nmhzlip735jv")))

(define-public crate-carrot_image_utils-0.1.5 (c (n "carrot_image_utils") (v "0.1.5") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0j45g6y9pv5253yv8glgrkqb081p32ixzxra7ma25b81ppb4r7cs")))

(define-public crate-carrot_image_utils-0.1.6 (c (n "carrot_image_utils") (v "0.1.6") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1aflbrwhw6iyk43qd887n0bvjg42mc58hapz9cwl8yyv3ls8vi29")))

(define-public crate-carrot_image_utils-0.1.7 (c (n "carrot_image_utils") (v "0.1.7") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1sn7xzs0y3w0k8vk8587jvqdn3ykwcl7fy99bbahhy648ksi6cda")))

(define-public crate-carrot_image_utils-0.1.8 (c (n "carrot_image_utils") (v "0.1.8") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0c36qm2zz5yig09v2bp9668516fvg3j74d4yais6skwp077nja15")))

(define-public crate-carrot_image_utils-0.1.9 (c (n "carrot_image_utils") (v "0.1.9") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1vsx75iiswkw5awnjgwiy4g5qp8hw3mw3294nlcjnnaj308csa5b")))

(define-public crate-carrot_image_utils-0.1.10 (c (n "carrot_image_utils") (v "0.1.10") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "16nvqcjzm5mv6s39sswqwq31z40pbzghhawfhplmi1znjd6sj6jy")))

