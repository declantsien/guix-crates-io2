(define-module (crates-io ca rr carrot_utils) #:use-module (crates-io))

(define-public crate-carrot_utils-0.1.0 (c (n "carrot_utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0afligc7dijfy24c1nvyzzq945i8nac7bxqm1w32z76flmgm2dd3")))

(define-public crate-carrot_utils-0.1.1 (c (n "carrot_utils") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "16aj3rdi2s8p4l3wk8z0y7lfhjlm04qvaqn8x01ydjf0nkrk6lc5")))

(define-public crate-carrot_utils-0.1.2 (c (n "carrot_utils") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0pb5f086xwa1c5k6qxhyfn311gb6l9ic882p911vk58cbnamlp08")))

(define-public crate-carrot_utils-0.1.3 (c (n "carrot_utils") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1xkscl354lxfsfjv7pn4z9d94qw661cwdwpgmksl63zh3pcfqz7n")))

(define-public crate-carrot_utils-0.1.4 (c (n "carrot_utils") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1jzb4fq71x7xjw6rd8dfg3cxfyhyfsj10ygim47yiz9gg5w5y2jp")))

(define-public crate-carrot_utils-0.1.5 (c (n "carrot_utils") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0hl2a47wlnmv7c21y08c6bdnh42dadmpcdz9wi8s1qnp7nxzdn0w")))

(define-public crate-carrot_utils-0.1.6 (c (n "carrot_utils") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0w606xcvi0j0wmmh5n3amlnx41fs37c7nrjdh6j2ygb1r6hcflwg")))

(define-public crate-carrot_utils-0.1.7 (c (n "carrot_utils") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0bi54wjz4j47jjj24cl9izmy40xi2d4n1s58g8c07xczc1yjfd1z")))

