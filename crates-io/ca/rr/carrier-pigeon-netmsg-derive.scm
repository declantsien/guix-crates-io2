(define-module (crates-io ca rr carrier-pigeon-netmsg-derive) #:use-module (crates-io))

(define-public crate-carrier-pigeon-netmsg-derive-0.1.0 (c (n "carrier-pigeon-netmsg-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0xrkl0drc7lkqxhhwmpbd6hrcnp22rvdrdfii3n8dvsk4v6nakgy")))

