(define-module (crates-io ca rr carrier-build) #:use-module (crates-io))

(define-public crate-carrier-build-0.2.0 (c (n "carrier-build") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)))) (h "1k7a1sy6fcg38s1qwjr2r7gc6hwrlg2al35k41gnxm183c3wlvzi")))

