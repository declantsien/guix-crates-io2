(define-module (crates-io ca rs cars) #:use-module (crates-io))

(define-public crate-cars-0.1.0 (c (n "cars") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "04hcqdkx5hlyq09056klyvzcrlrcqn15br6bl74bzwvhnwrbg758")))

(define-public crate-cars-0.1.1 (c (n "cars") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0i49f5dbabbhgjixk8h1ska282inr5pfbz8y5msgfn161s0zi38n")))

