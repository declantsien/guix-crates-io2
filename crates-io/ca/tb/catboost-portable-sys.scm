(define-module (crates-io ca tb catboost-portable-sys) #:use-module (crates-io))

(define-public crate-catboost-portable-sys-0.1.1 (c (n "catboost-portable-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "zstd") (r "^0.11.2") (d #t) (k 1)))) (h "0zivpw16qzqzrv6kdabxzs1sik7v6364bm0c7lglh8paixvlmj6n")))

(define-public crate-catboost-portable-sys-0.1.2 (c (n "catboost-portable-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "zstd") (r "^0.11.2") (d #t) (k 1)))) (h "10k3nfb8kkl0lc16bq72i98j8675zhd3x9npk70wg042lp9dl4p8")))

