(define-module (crates-io ca tb catboost-rs) #:use-module (crates-io))

(define-public crate-catboost-rs-0.1.1 (c (n "catboost-rs") (v "0.1.1") (d (list (d (n "catboost-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0sq469cc4cbjan7hy5fqg1ahwnxy4fls3ja6pgajf4vil8cznkpj")))

(define-public crate-catboost-rs-0.1.2 (c (n "catboost-rs") (v "0.1.2") (d (list (d (n "catboost-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1n2gjqyrnm4vnmzlrydlyl8lbazrqkwgyd2w9dhaxjpfpxq3v6xd")))

(define-public crate-catboost-rs-0.1.3 (c (n "catboost-rs") (v "0.1.3") (d (list (d (n "catboost-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0j34n0r0yqb2vblgdw0smdisijwdskbzh6vsq0900cgrihgqcm4m")))

(define-public crate-catboost-rs-0.1.4 (c (n "catboost-rs") (v "0.1.4") (d (list (d (n "catboost-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1hb33jli37yjng4hrpy9xywji2jz7vis61wx29998cnl60x3cqwl")))

(define-public crate-catboost-rs-0.1.5 (c (n "catboost-rs") (v "0.1.5") (d (list (d (n "catboost-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0lwlia55kqyfshnwsiigs9ig86f9vcghya51x1bpv6fpcrg7nx1w")))

(define-public crate-catboost-rs-0.1.6 (c (n "catboost-rs") (v "0.1.6") (d (list (d (n "catboost-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1dqip8qzsdyyyaa7zsvy4b95aaa57klkqlfdqk53djvsqgvfyhfr")))

(define-public crate-catboost-rs-0.1.7 (c (n "catboost-rs") (v "0.1.7") (d (list (d (n "catboost-sys") (r "^0.1.5") (d #t) (k 0)))) (h "012a76x9apmi1ram8g9ydjjxykzcn2q4360zjb4fr39njhz20jl0")))

(define-public crate-catboost-rs-0.1.8 (c (n "catboost-rs") (v "0.1.8") (d (list (d (n "catboost-sys") (r "^0.1.6") (d #t) (k 0)))) (h "148i3zq4i0nvv6rr750gvz50wdlwscwppzv3nr5cmrcsrss4716j")))

