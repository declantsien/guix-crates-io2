(define-module (crates-io ca tb catboost-sys) #:use-module (crates-io))

(define-public crate-catboost-sys-0.1.2 (c (n "catboost-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 1)) (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "ureq") (r "^2.5.0") (d #t) (k 1)))) (h "1imfrnvff6gigjqbjqfs2fr8f52x0qqmlvvy48h1mhn94lc5l6d3")))

(define-public crate-catboost-sys-0.1.3 (c (n "catboost-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 1)) (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "ureq") (r "^2.5.0") (d #t) (k 1)))) (h "14qfln6z3p6jmpgks3wr69zmn3c1vkhlhf1f088f49y5727wh8ma")))

(define-public crate-catboost-sys-0.1.4 (c (n "catboost-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 1)) (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "ureq") (r "^2.5.0") (d #t) (k 1)))) (h "03qn1crzi59g9f8dx36q2dpbvzsc8wf6hv30j9dxw5xy7y2vp9pa")))

(define-public crate-catboost-sys-0.1.5 (c (n "catboost-sys") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 1)) (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "ureq") (r "^2.5.0") (d #t) (k 1)))) (h "08gbzd6ymakn229178rl5vcspjag6ckza46kdhafd8nay7sfd0sh")))

(define-public crate-catboost-sys-0.1.6 (c (n "catboost-sys") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 1)) (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "ureq") (r "^2.5.0") (d #t) (k 1)))) (h "14dbsw30z1wdlfj9yiy72dzs7zsdbryy4y6a9gvyxqs6r6ii0fdi")))

