(define-module (crates-io ca tb catbat) #:use-module (crates-io))

(define-public crate-catbat-0.0.1 (c (n "catbat") (v "0.0.1") (h "1mx16zh7g8v0kjamn65iirq24s8fyjam23lqz2nkkqi9wrbx17q4")))

(define-public crate-catbat-0.0.2 (c (n "catbat") (v "0.0.2") (h "0igjskhjdbg6qrzabybd8ndxysf11fd15yzcd80xd9hd2hvqn9q6")))

(define-public crate-catbat-0.0.3 (c (n "catbat") (v "0.0.3") (h "1wkxrzxcm7r44y7y6r0mjmam1d9lyj8nhchj7scfzfrig2fvzi2x")))

(define-public crate-catbat-0.1.0 (c (n "catbat") (v "0.1.0") (h "0a6vb3mkc8p2c59fl2za200xr8c9va3vv233ky9vvp4w7l3vk396")))

