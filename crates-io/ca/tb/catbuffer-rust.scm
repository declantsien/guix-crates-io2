(define-module (crates-io ca tb catbuffer-rust) #:use-module (crates-io))

(define-public crate-catbuffer-rust-0.1.1-alpha.1 (c (n "catbuffer-rust") (v "0.1.1-alpha.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0w44yrxqhv7b2awfs4qdvsaifab64jwc28vpgfdzn9mgrps6z51m")))

