(define-module (crates-io ca tb catboost-portable) #:use-module (crates-io))

(define-public crate-catboost-portable-0.1.1 (c (n "catboost-portable") (v "0.1.1") (d (list (d (n "catboost-portable-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0gari8lh0fv8qxmm7kydavlj32jjqhnd1hr2qhx1fic7bd3hfpgv")))

(define-public crate-catboost-portable-0.1.2 (c (n "catboost-portable") (v "0.1.2") (d (list (d (n "catboost-portable-sys") (r "^0.1.2") (d #t) (k 0)))) (h "05wa5vhx9lhw65ahdm5lb3pvjanzfg5x4nq739k64bzm45zy4irq")))

