(define-module (crates-io ca py capy-graph) #:use-module (crates-io))

(define-public crate-capy-graph-0.1.0 (c (n "capy-graph") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1draw6hw68a50rqkvq847s3gxlsa8dck291hy5vrr935d85243qg")))

(define-public crate-capy-graph-0.1.1 (c (n "capy-graph") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xg6yzm9bil4l0kmhw25hw8ljyc0i66s7ph0hp90apbm2sz8dpni")))

(define-public crate-capy-graph-0.1.2 (c (n "capy-graph") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d824aashgiqp7amisdabmkcwg7lbza52adfmyac43gs246xh536")))

(define-public crate-capy-graph-0.1.3 (c (n "capy-graph") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y82yyikacxxfipc81g0hscyh60grgh9072v8fslvh8ynsca22p8")))

(define-public crate-capy-graph-0.1.4 (c (n "capy-graph") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s9y8jxpamsj7ljff7fc6hndkanhc578f5w7f5i4rsysamyyix67")))

