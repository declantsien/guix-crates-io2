(define-module (crates-io ca py capykem) #:use-module (crates-io))

(define-public crate-capyKEM-0.1.0 (c (n "capyKEM") (v "0.1.0") (d (list (d (n "hybrid-array") (r "^0.1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (k 0)) (d (n "typenum") (r "^1.17.0") (k 0)))) (h "1gy6zj818rnlrd0iigm73iia0vfqsp3f56iqm7838gyh801zl8ry")))

