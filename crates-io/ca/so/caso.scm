(define-module (crates-io ca so caso) #:use-module (crates-io))

(define-public crate-caso-0.1.0 (c (n "caso") (v "0.1.0") (d (list (d (n "avalog") (r "^0.7.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "0gvxyb66q3dlp3fgv2ikaz72z1vgqsysxyb7m71lk8aihk081sv1")))

(define-public crate-caso-0.2.0 (c (n "caso") (v "0.2.0") (d (list (d (n "avalog") (r "^0.7.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "0n8nv13l0bmyz3jiqwdy3140nz1nywsd8z8hm29hxsh8y7dvb4wi")))

(define-public crate-caso-0.2.1 (c (n "caso") (v "0.2.1") (d (list (d (n "avalog") (r "^0.7.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1vwm1x8wp3qgxkv7cwv6rd316534c2zh5wvv6dzrv38yard3wvsq")))

(define-public crate-caso-0.2.2 (c (n "caso") (v "0.2.2") (d (list (d (n "avalog") (r "^0.7.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "072frags4xbhqywcgncggvjgq13kljj99acdcq8m5f5wqyzksnf6")))

