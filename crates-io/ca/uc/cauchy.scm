(define-module (crates-io ca uc cauchy) #:use-module (crates-io))

(define-public crate-cauchy-0.1.0 (c (n "cauchy") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "00csw5vj1ir1gh7bhlp28mzagfcc1m5gpnajkwkmkyz43j29xq8i")))

(define-public crate-cauchy-0.2.0 (c (n "cauchy") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1grmqzv5i5qf0aha7f9jik8igsijwrm60g0gicrf4bi67ync2m2a")))

(define-public crate-cauchy-0.2.1 (c (n "cauchy") (v "0.2.1") (d (list (d (n "num-complex") (r "^0.2.1") (f (quote ("serde" "rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0mypg4l1yn0ny3jpyz8a1fm2n1if7h8xv7ma4b4rr0ap0hkqkm3w")))

(define-public crate-cauchy-0.2.2 (c (n "cauchy") (v "0.2.2") (d (list (d (n "num-complex") (r "^0.2.1") (f (quote ("serde" "rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1rrd5a1h7lrgjdja58qd77vbbyp13swwri1hjkvprv4ll9sm6l4w")))

(define-public crate-cauchy-0.3.0 (c (n "cauchy") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.3.0") (f (quote ("serde" "rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (d #t) (k 0)))) (h "04vak3a9jjmmv01cq225cx94lcv2al11xxccpskkl97am7hvj1iw")))

(define-public crate-cauchy-0.4.0 (c (n "cauchy") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.4.0") (f (quote ("serde" "rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "0pphwzpx5mhvnp6p75alvjdqs0xcarpfwzr9s06yidgk5bfivwcz")))

