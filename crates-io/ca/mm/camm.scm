(define-module (crates-io ca mm camm) #:use-module (crates-io))

(define-public crate-camm-0.1.0 (c (n "camm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1hr7j3bsyh5nh7da1in5hkjr58cpn8zabxcj51h080zhqfci5046") (f (quote (("default" "byteorder"))))))

