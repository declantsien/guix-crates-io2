(define-module (crates-io ca d9 cad97-publish-test) #:use-module (crates-io))

(define-public crate-cad97-publish-test-0.1.0 (c (n "cad97-publish-test") (v "0.1.0") (h "0csz8xr7h9yvmi4yq40b63x2i8dna0h7pjxv33dx1dq62lb9pbhw")))

(define-public crate-cad97-publish-test-0.1.1 (c (n "cad97-publish-test") (v "0.1.1") (h "1140rqixaisnf6n0jll3wy08097qbmrgx0g9bvv4srmjqfklab3a")))

