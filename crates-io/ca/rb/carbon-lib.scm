(define-module (crates-io ca rb carbon-lib) #:use-module (crates-io))

(define-public crate-carbon-lib-0.1.0 (c (n "carbon-lib") (v "0.1.0") (d (list (d (n "carbon-dump") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "0hjw69l8kkc5ryhcb12lb85knc2kd67aq27dvr6a4hi6qdj5vklp")))

(define-public crate-carbon-lib-0.1.1 (c (n "carbon-lib") (v "0.1.1") (d (list (d (n "carbon-dump") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "042mf8q5r2f31l8nk6h6vkj3vjq35ih5gdf1mqj6xzby637lrsvp")))

(define-public crate-carbon-lib-0.2.0 (c (n "carbon-lib") (v "0.2.0") (d (list (d (n "carbon-dump") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "195s23jq5zsqnhp0xvrw5wz5yq0si75yl6n17fkmfnlp54x8y63h")))

(define-public crate-carbon-lib-0.2.1 (c (n "carbon-lib") (v "0.2.1") (d (list (d (n "carbon-dump") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "10ffn1zirjgc5zd4rs46f7lvzf5l9vp8rx5299z10rss893d6xrx")))

(define-public crate-carbon-lib-0.3.0 (c (n "carbon-lib") (v "0.3.0") (d (list (d (n "carbon-dump") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "syntect") (r "^4.4") (d #t) (k 0)))) (h "17qra5pbl3w3y7pawixh3gvdbq8wfiaywc09ksh54sgnwc4l2dxh")))

(define-public crate-carbon-lib-0.3.1 (c (n "carbon-lib") (v "0.3.1") (d (list (d (n "carbon-dump") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "syntect") (r "^4.4") (d #t) (k 0)))) (h "0m0f6m43viw166wrljscxnsj84w57x6krm1j61yhryc4bn68afps")))

