(define-module (crates-io ca rb carbide) #:use-module (crates-io))

(define-public crate-carbide-0.70.1 (c (n "carbide") (v "0.70.1") (d (list (d (n "carbide_controls") (r "^0.70") (o #t) (d #t) (k 0)) (d (n "carbide_core") (r "^0.70") (d #t) (k 0)) (d (n "carbide_wgpu") (r "^0.70") (o #t) (d #t) (k 0)))) (h "00ii8m0dyw37avlcxac5w31xmr5lm1chq4cj0m3cab6dyrf0wcr3") (f (quote (("default" "carbide_wgpu") ("controls" "carbide_controls"))))))

