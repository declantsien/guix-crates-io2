(define-module (crates-io ca rb carbono) #:use-module (crates-io))

(define-public crate-carbono-0.1.0 (c (n "carbono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.4") (d #t) (k 0)))) (h "0jrddgf88rqi64gz9wh0l4fcgm4qqz4ph6xsihnkn3aynqjgr9bz") (f (quote (("testing"))))))

(define-public crate-carbono-0.1.1 (c (n "carbono") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.4") (d #t) (k 0)))) (h "0h7zdk26mvzhprn1lgbn3qpimwfj6dmylbdc61z9qk7a313kwj5j") (f (quote (("testing"))))))

