(define-module (crates-io ca rb carbon-dump) #:use-module (crates-io))

(define-public crate-carbon-dump-1.0.0 (c (n "carbon-dump") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "0fsk96smwvvdyn8l4jc521w0259as8skaq0h0xcmqdixsfdgyffp")))

(define-public crate-carbon-dump-1.1.0 (c (n "carbon-dump") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "02j3liiwqxnpcxyca2pywqvs36952xkbxln41api53zsfijwmas1")))

(define-public crate-carbon-dump-1.2.0 (c (n "carbon-dump") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "0zqy36y20kra3hv7c6cj4nbv46knbn4wnhkymps4kzvddf07nsnq")))

(define-public crate-carbon-dump-1.2.1 (c (n "carbon-dump") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "syntect") (r "^4.4") (d #t) (k 0)))) (h "1lhqpf0d1caixbmbgqqy8ag1092dq77dk5mm1gn6218hrfk955xf")))

(define-public crate-carbon-dump-1.2.2 (c (n "carbon-dump") (v "1.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "syntect") (r "^4.4") (d #t) (k 0)))) (h "1d322ys9ncxn42padk4i71j05idjq87ki7dichmc02krisqphkzh")))

