(define-module (crates-io ca rb carbide_controls) #:use-module (crates-io))

(define-public crate-carbide_controls-0.70.1 (c (n "carbide_controls") (v "0.70.1") (d (list (d (n "carbide_core") (r "^0.70") (d #t) (k 0)) (d (n "carbide_derive") (r "^0.70") (d #t) (k 0)) (d (n "carbide_wgpu") (r "^0.70") (d #t) (k 2)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1ig6gnaxfzvqcw16bkslwy4mrqshnrf4smvj5am5b1q3058kcq42")))

