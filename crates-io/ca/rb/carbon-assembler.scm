(define-module (crates-io ca rb carbon-assembler) #:use-module (crates-io))

(define-public crate-carbon-assembler-0.1.0 (c (n "carbon-assembler") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "parse_int") (r "^0.6.0") (d #t) (k 0)))) (h "14w560rahf4f2v27nxwxn61hh34xqqpwb708crgx1anfdyqk8rxw")))

(define-public crate-carbon-assembler-1.0.0 (c (n "carbon-assembler") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "parse_int") (r "^0.6.0") (d #t) (k 0)))) (h "0cibpymsadl21zfiqj882kpi2pfmazfwgc4khygs2ddqcy0v6ib5")))

(define-public crate-carbon-assembler-1.0.1 (c (n "carbon-assembler") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "parse_int") (r "^0.6.0") (d #t) (k 0)))) (h "1dx9zmdlpxcsfam4dc5llig8d075lsqg98y7da62wh8gcfyk5xmy")))

(define-public crate-carbon-assembler-1.0.2 (c (n "carbon-assembler") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0rfvfq6gr4bjjja7xni9m4jzwph4fdqvhpr3ca5a6sr7038zifi3")))

(define-public crate-carbon-assembler-1.0.5 (c (n "carbon-assembler") (v "1.0.5") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0v8m79jjx82yrbs73s8l2xismy9aw5g43xnm8m2g3m8rqzvv7r08")))

(define-public crate-carbon-assembler-1.0.6 (c (n "carbon-assembler") (v "1.0.6") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0plyh5kq4fvlyfq3jdmv6x7dfd7lxz8igh8ibi737qpdq8rwxriq")))

(define-public crate-carbon-assembler-1.0.7 (c (n "carbon-assembler") (v "1.0.7") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "17i83p8qscm3xhzm0f1649iaqlb13apq9yi65c33na9cmvl23dnm")))

(define-public crate-carbon-assembler-1.0.8 (c (n "carbon-assembler") (v "1.0.8") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "05j4z8n66x29jzdic0f19gl8l69mbshpmwymg2rhjqdi0811bkzf")))

(define-public crate-carbon-assembler-1.1.0 (c (n "carbon-assembler") (v "1.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1r24chgws9k5xwr2mbkgzz570i72z84jzys25knzkw3ld4f5z8m2")))

(define-public crate-carbon-assembler-1.2.0 (c (n "carbon-assembler") (v "1.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "07drmq43gz5crp7bxxhigrspkddagk3w3bf9yi1pk583m4xy77sd")))

(define-public crate-carbon-assembler-1.3.1 (c (n "carbon-assembler") (v "1.3.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1jwhqgxf5f7s7hv2x623mkz2fn556ywgg2z3dcfn0xwh2dx59hr6")))

(define-public crate-carbon-assembler-1.3.2 (c (n "carbon-assembler") (v "1.3.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "11walgvnyfmb21b5s21ci10w31arq8ls7hsrs96kh9wh2y6ravj5")))

(define-public crate-carbon-assembler-1.5.2 (c (n "carbon-assembler") (v "1.5.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1qr9jgkwfz4j7nak191c5a94156dkapxsgm9261482b0mdnbklq4")))

(define-public crate-carbon-assembler-1.5.3 (c (n "carbon-assembler") (v "1.5.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "10ajwzir7bkkwjbnz2k3czl13knbjqa7qc29axr9n500m56bf5rx")))

(define-public crate-carbon-assembler-1.5.4 (c (n "carbon-assembler") (v "1.5.4") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1x5w9kxadxf7r3xkhs9ly77z2m5mjl3vprr5v8889xziv2vn3qad")))

(define-public crate-carbon-assembler-1.5.5 (c (n "carbon-assembler") (v "1.5.5") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1qz7zii21r9vag2wipbimimxlm5fvbdx3wdlvzayz42m1by0c5mf")))

(define-public crate-carbon-assembler-1.5.6 (c (n "carbon-assembler") (v "1.5.6") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0ivi1v94slgw5mz4bpxmssdmdk5667qa42y542k0893s3b9817ib")))

(define-public crate-carbon-assembler-1.5.7 (c (n "carbon-assembler") (v "1.5.7") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "193gflmk0rkqmz302zrp25mh5gpfbyym4hn7qbd850bbn69bxccb")))

(define-public crate-carbon-assembler-1.5.9 (c (n "carbon-assembler") (v "1.5.9") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "07qgqnm06ibgy4mlzn3fgpbv0p5wzzcijg6a4cz9k94zx13bm6pk")))

(define-public crate-carbon-assembler-1.6.0 (c (n "carbon-assembler") (v "1.6.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1jfrv44v6chjrg15srh3vrpgrlnh28gi3l73n5g5q1g9skkrvp1v")))

(define-public crate-carbon-assembler-1.6.1 (c (n "carbon-assembler") (v "1.6.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1yrs8w5cj84vvwivn1h7drrhp8xw79mnvah2ry1jlhigp3cf1ydm")))

(define-public crate-carbon-assembler-1.6.2 (c (n "carbon-assembler") (v "1.6.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1zf9jwz9qaqnk48v4rn54hhqvbi8wnrhs764xl90grxndbf457g2")))

(define-public crate-carbon-assembler-1.6.4 (c (n "carbon-assembler") (v "1.6.4") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "0gyhiqfyzfd39l7ci8qb19mp5721wmybkfjzpjlxi10zjjzdgj5x")))

(define-public crate-carbon-assembler-1.6.5 (c (n "carbon-assembler") (v "1.6.5") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "0zfq9l03q436q4gdq0xcd9wgr96qs1sf62b07k1d9hz8ckmk07n5")))

(define-public crate-carbon-assembler-1.6.6 (c (n "carbon-assembler") (v "1.6.6") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "19083van270ngsssfh2y0l0cgzw425ak3q44r7sq7rvs0cvczlvc")))

(define-public crate-carbon-assembler-1.6.7 (c (n "carbon-assembler") (v "1.6.7") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "09kss34alnwimir8gsqhvknwc491l153xlpkdxnsk6xcp5vwbbig")))

