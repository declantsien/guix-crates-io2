(define-module (crates-io ca rb carboncopy-tokio) #:use-module (crates-io))

(define-public crate-carboncopy-tokio-0.1.0 (c (n "carboncopy-tokio") (v "0.1.0") (d (list (d (n "carboncopy") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread" "io-std" "io-util" "time" "sync" "macros"))) (d #t) (k 0)))) (h "004zyg56aic2xs1r21v91kjr3552jymfwh4srbwm0lymaw1qr04l")))

(define-public crate-carboncopy-tokio-0.1.1 (c (n "carboncopy-tokio") (v "0.1.1") (d (list (d (n "carboncopy") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread" "io-std" "io-util" "time" "sync" "macros"))) (d #t) (k 0)))) (h "08gpw5pd53f6nfilnqm8xvcg4r8b0a5hjpbajrxyvvmdgflzm0cz")))

(define-public crate-carboncopy-tokio-0.1.2 (c (n "carboncopy-tokio") (v "0.1.2") (d (list (d (n "carboncopy") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread" "io-std" "io-util" "time" "sync" "macros"))) (d #t) (k 0)))) (h "0fbbxc8qd4bn7bsgzi1vg6600z5ljinb3mbf720vla0m3nh0ajz5")))

(define-public crate-carboncopy-tokio-0.1.3 (c (n "carboncopy-tokio") (v "0.1.3") (d (list (d (n "carboncopy") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread" "io-std" "io-util" "time" "sync" "macros"))) (d #t) (k 0)))) (h "19gjwiasb5jzrldqih63gqnswv0wahpvbq4dwsh0z3g8wajzzpid")))

(define-public crate-carboncopy-tokio-0.1.4 (c (n "carboncopy-tokio") (v "0.1.4") (d (list (d (n "carboncopy") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt-multi-thread" "io-std" "io-util" "time" "sync" "macros"))) (d #t) (k 0)))) (h "0vnx55g0rp84q5a02js328rj81z2vf0vfp0vidnw5w095rm3b2xw")))

