(define-module (crates-io ca rb carbide_derive) #:use-module (crates-io))

(define-public crate-carbide_derive-0.70.0 (c (n "carbide_derive") (v "0.70.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wm9xiyqjmgc93b6lbx5r0n7ipfcfnmgmll8rv48h3gawim0lhdh")))

(define-public crate-carbide_derive-0.70.1 (c (n "carbide_derive") (v "0.70.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dxjr6zh5k5v8xvzrfd6rjw5l1df53b114kz0bb21qh4c2sswzb9")))

