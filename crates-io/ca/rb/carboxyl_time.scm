(define-module (crates-io ca rb carboxyl_time) #:use-module (crates-io))

(define-public crate-carboxyl_time-0.0.1 (c (n "carboxyl_time") (v "0.0.1") (d (list (d (n "carboxyl") (r "^0.0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01f5x4c00rz2ymdfcj91as685wzvjn96rx1ldmdlgjhp583dndmd")))

(define-public crate-carboxyl_time-0.0.2 (c (n "carboxyl_time") (v "0.0.2") (d (list (d (n "carboxyl") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ngy5ybr2f1jb0v2fzka3885dybrnnv5mkkdfrgjdim6hyrqngqi")))

(define-public crate-carboxyl_time-0.0.3 (c (n "carboxyl_time") (v "0.0.3") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0kw6lp8vh0nzq69mi29868k1mpk6iirq3l8z9x55b7xq3wk9gqfx")))

