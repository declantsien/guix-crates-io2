(define-module (crates-io ca rb carboxyl) #:use-module (crates-io))

(define-public crate-carboxyl-0.0.1 (c (n "carboxyl") (v "0.0.1") (h "1m9w7cbbp377gkkd8kwrh69vwcvz5y6z9xbrky3vjvsdc9569nq2")))

(define-public crate-carboxyl-0.0.2 (c (n "carboxyl") (v "0.0.2") (h "0brfmw0p57z87bh8cq176gmn4d4gv8y2ml91r90yzin8fk3r1y5h")))

(define-public crate-carboxyl-0.0.3 (c (n "carboxyl") (v "0.0.3") (h "0fc4g9zf6wny3lzdkc1skqw1g8cppjcj8vzr8zs52d06cr3s24zs")))

(define-public crate-carboxyl-0.0.4 (c (n "carboxyl") (v "0.0.4") (h "16wrzc3043jna8k7rbx42x0yh3mr44259lbqx2zxj9aaff5vqfbx")))

(define-public crate-carboxyl-0.0.5 (c (n "carboxyl") (v "0.0.5") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "12k96p31n4zs855q2zfghj6lvxnhlwpvm84xlbm1gqi95dg53a22")))

(define-public crate-carboxyl-0.0.6 (c (n "carboxyl") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0fr5x15vy4zslb1qk2bb0s4glk7g4a8cqn23xam79fm7cqxmilf8")))

(define-public crate-carboxyl-0.0.7 (c (n "carboxyl") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "037pni9kjvv0a6221qc3ljvkpgi7vd2qg1z90r4jv5a69rfj2drg")))

(define-public crate-carboxyl-0.0.8 (c (n "carboxyl") (v "0.0.8") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0k5a5hbsv9jlnn12d61w9isq1mwr6yr39aakrw70pp8ykvagniz3")))

(define-public crate-carboxyl-0.0.9 (c (n "carboxyl") (v "0.0.9") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "05ryg7qavk6i15h618pxajgffzcqp1dmq1cly3vjgbjvnvv27z1b")))

(define-public crate-carboxyl-0.0.10 (c (n "carboxyl") (v "0.0.10") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1hzh3rhm2a8nzyg4vhs1f40xpk4qknjamrjp60wv16yzc188s5fh")))

(define-public crate-carboxyl-0.0.11 (c (n "carboxyl") (v "0.0.11") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "017f3ri8lld544nrzqn8pk4h931l3rixxdxpi6mazgqlhbvr7zis") (f (quote (("unstable"))))))

(define-public crate-carboxyl-0.1.0 (c (n "carboxyl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0rc73pacg4yy1iry2rz5v4hr1hkvd3a090qvhw856afkxq1j0192")))

(define-public crate-carboxyl-0.1.1 (c (n "carboxyl") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rrh6g2n7acdkmfsrywglm19anvamaap8vvdl0w9wlwfidq0na85")))

(define-public crate-carboxyl-0.1.2 (c (n "carboxyl") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vixpl7wmmjq54k0m00lchkaxknxxx1hwcfqh0w7bmryz2fvr1r2")))

(define-public crate-carboxyl-0.2.0 (c (n "carboxyl") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0hmfmzw4vaax82dwa2ljsn9mzzw7nln82mn1ggyn6sm58p56w4ba")))

(define-public crate-carboxyl-0.2.1 (c (n "carboxyl") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)))) (h "03m5nirdsirm9sq43vrhxknp487blnjf6w5xyzr9a2hq897r6r1z")))

(define-public crate-carboxyl-0.2.2 (c (n "carboxyl") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "170wqrabsczf0hnl7c6y0r3a6l1cczavvzxfk7q1yigds8wsmzx0")))

