(define-module (crates-io ca rb carbites) #:use-module (crates-io))

(define-public crate-carbites-0.1.0 (c (n "carbites") (v "0.1.0") (d (list (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "ipld") (r "^0.16") (d #t) (k 0) (p "libipld")) (d (n "ipld-cbor") (r "^0.16") (d #t) (k 0) (p "libipld-cbor")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rvnq4lixl9dqms8an373h2p4m1c14dnkf193gygz391m1zd4ch5")))

(define-public crate-carbites-0.1.1 (c (n "carbites") (v "0.1.1") (d (list (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "ipld") (r "^0.16") (d #t) (k 0) (p "libipld")) (d (n "ipld-cbor") (r "^0.16") (d #t) (k 0) (p "libipld-cbor")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17wc7sf4vs81m4sd02iwaxgmz1q4ypsp8n4pcsrcrzq50mxy1nr4")))

(define-public crate-carbites-0.1.2 (c (n "carbites") (v "0.1.2") (d (list (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "ipld") (r "^0.16") (d #t) (k 0) (p "libipld")) (d (n "ipld-cbor") (r "^0.16") (d #t) (k 0) (p "libipld-cbor")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rfirwz7aw34a6dbx0qzfdnxhl9a5c86zw71y7mjw4cbm0blqwpy")))

