(define-module (crates-io ca rb carboxyl-cli) #:use-module (crates-io))

(define-public crate-carboxyl-cli-0.1.0 (c (n "carboxyl-cli") (v "0.1.0") (d (list (d (n "carboxyl") (r "^0.1") (d #t) (k 0)))) (h "1y54g7r38ccbxq60b5vg0n94pj0x35vm1w0s0biqfs3akm10n9j2")))

(define-public crate-carboxyl-cli-0.1.1 (c (n "carboxyl-cli") (v "0.1.1") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)))) (h "0vq543hg1d8mmav7qmx220744v0d021lhx5ymfjybp3h4gk5i5nm")))

