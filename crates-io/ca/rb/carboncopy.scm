(define-module (crates-io ca rb carboncopy) #:use-module (crates-io))

(define-public crate-carboncopy-0.1.0 (c (n "carboncopy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1giv4kngy6nm5h6pzq972q0kflj6yj5dwl0p7rdkv9lqh0hwjgas")))

(define-public crate-carboncopy-0.1.1 (c (n "carboncopy") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hwa94an9h33hbwgv5h94qky3f0c9pxwsf7zic79xd34sdj6xdb3")))

(define-public crate-carboncopy-0.1.2 (c (n "carboncopy") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1749i73nxjapv535kkih76wkqjk85ys5rwmrh0jm9xp7mmsz8vv0")))

(define-public crate-carboncopy-0.2.0 (c (n "carboncopy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gxx35a1w5lvc6bfxcjmzkliq1g57aqq49iw6jbpkyraqwyfg701")))

(define-public crate-carboncopy-0.2.1 (c (n "carboncopy") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "07dqxaxlv9yvzgjfal3ibg10a0qaya5dj835iggm9v5h4f8xc5hs")))

(define-public crate-carboncopy-0.3.0 (c (n "carboncopy") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fj142f87anrbbk5xqx7paacvhlphfja1rnyc3k30slxpf0pi98q")))

