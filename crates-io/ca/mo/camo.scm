(define-module (crates-io ca mo camo) #:use-module (crates-io))

(define-public crate-camo-0.1.0 (c (n "camo") (v "0.1.0") (h "1762i40an88570gaycr3viprdmknvi4arm09fpm1svf6rp34apds")))

(define-public crate-camo-0.1.1 (c (n "camo") (v "0.1.1") (h "1m4m7zkgp9w4s79vl3pc434nbxl52hjkpd835v16d3al14lqp29z")))

(define-public crate-camo-0.3.0 (c (n "camo") (v "0.3.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "camo-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "camo-typescript") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1q7kqwnhl1f7xhh5l6q13jmix16186z8883afds4dhbrszb9grnp") (f (quote (("typescript" "camo-typescript") ("derive" "camo-derive") ("default" "derive"))))))

(define-public crate-camo-0.4.0 (c (n "camo") (v "0.4.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "camo-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "camo-typescript") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "0rjgnx0j93b1j6vc5h9i5k9b6r3m0y4ra4062lyk5xqm9vw365bp") (f (quote (("typescript" "camo-typescript") ("derive" "camo-derive") ("default" "derive"))))))

(define-public crate-camo-0.5.0 (c (n "camo") (v "0.5.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "camo-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "camo-typescript") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1v72gkprbiaj8zhw0agxzkwhqay1c35dnf7iivyclgl7bi8zx571") (f (quote (("typescript" "camo-typescript") ("derive" "camo-derive") ("default" "derive"))))))

(define-public crate-camo-0.6.0 (c (n "camo") (v "0.6.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "camo-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "camo-typescript") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "14263nyb9pwqc80bddl0ysgbkvhqfvfmarvwpl144p2kpz5ki1sr") (f (quote (("typescript" "camo-typescript") ("derive" "camo-derive") ("default" "derive"))))))

(define-public crate-camo-0.7.0 (c (n "camo") (v "0.7.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "camo-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "camo-typescript") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1l0049201vz1abb6sbhvdg6yfjhlli748q17zkpy2cb10xf30agr") (f (quote (("typescript" "camo-typescript") ("derive" "camo-derive") ("default" "derive"))))))

