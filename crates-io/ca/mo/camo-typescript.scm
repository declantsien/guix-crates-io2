(define-module (crates-io ca mo camo-typescript) #:use-module (crates-io))

(define-public crate-camo-typescript-0.1.0 (c (n "camo-typescript") (v "0.1.0") (d (list (d (n "camo") (r "^0.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1y20k9ahwqffw0lh1mc7swckmym010mvh9h74kjc5zmw526hznwc")))

(define-public crate-camo-typescript-0.1.1 (c (n "camo-typescript") (v "0.1.1") (d (list (d (n "camo") (r "^0.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1ahqajz6bzg6nwmkyy70wir2lpbfp5laxls68w1sivlyg29zzbdh")))

(define-public crate-camo-typescript-0.3.0 (c (n "camo-typescript") (v "0.3.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "0liwk9hjabk5lz542k8xmz8v9cm2ld409rnr8rwm88qsim321lbh")))

(define-public crate-camo-typescript-0.4.0 (c (n "camo-typescript") (v "0.4.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "0f51gv7skm9qzgp0136nx405c20vz1wh8bkbxpq73jp68is03mvh")))

(define-public crate-camo-typescript-0.5.0 (c (n "camo-typescript") (v "0.5.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "185pw12zjq1z1x1wv78zlca523825zr1kpz1s2wzqmsp5g5jdzlf")))

(define-public crate-camo-typescript-0.6.0 (c (n "camo-typescript") (v "0.6.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1in2zf37lgalrfj88icmkl4in2irmr6d7qyw0cg63kpnraljg7vg")))

(define-public crate-camo-typescript-0.7.0 (c (n "camo-typescript") (v "0.7.0") (d (list (d (n "camo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "0ppv6c5km3m4ydnc27jf2arkr9bqn4yrqwgx4dhsbkaa0fcyf0bk")))

