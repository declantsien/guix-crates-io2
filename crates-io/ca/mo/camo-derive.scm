(define-module (crates-io ca mo camo-derive) #:use-module (crates-io))

(define-public crate-camo-derive-0.1.0 (c (n "camo-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qs6bamlcwgq5xdb7cxk04wnq609yrsx583737ign6bj04lg750k")))

(define-public crate-camo-derive-0.1.1 (c (n "camo-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0b8saxdyrbw6f34k01s0xd2svx6hkcscf61lxgsyr6a0ilggrfkn")))

(define-public crate-camo-derive-0.3.0 (c (n "camo-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "0rqjymzpdr2sm85wrirs14sf1p52p8jj70p20fc055gifhd9rmwl")))

(define-public crate-camo-derive-0.4.0 (c (n "camo-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "13jj9abaiyaf2lqy8hxak5ya0vz30h0hxvdvlz57b2zd5k2karmf")))

(define-public crate-camo-derive-0.5.0 (c (n "camo-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "1s5wgpvxy3s14frh40vdxsd3lgz3h1hm7glh6hzagzgp2in1f0j0")))

