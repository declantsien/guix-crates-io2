(define-module (crates-io ca mo camo-url) #:use-module (crates-io))

(define-public crate-camo-url-0.1.2 (c (n "camo-url") (v "0.1.2") (d (list (d (n "crypto-hashes") (r "^0.9.0") (f (quote ("include_weak"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "securefmt") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0las7mpz18pncc12dfn1i7mksmb3c0sla0y850s0lhailb3fnl4p")))

