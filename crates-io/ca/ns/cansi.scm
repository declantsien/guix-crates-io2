(define-module (crates-io ca ns cansi) #:use-module (crates-io))

(define-public crate-cansi-1.0.0 (c (n "cansi") (v "1.0.0") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "parse-ansi") (r "^0.1.6") (d #t) (k 0)))) (h "1ahidq9z0367kq6gpgw5ykxdj5xhwbzmbfv32pwl7r723yzv8xnm")))

(define-public crate-cansi-1.1.0 (c (n "cansi") (v "1.1.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "parse-ansi") (r "^0.1.6") (d #t) (k 0)))) (h "06r3jx3zh54n38ijx03dazxdrvc7sxjwrv9z8a2bwv6f23s407y1")))

(define-public crate-cansi-1.1.1 (c (n "cansi") (v "1.1.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "parse-ansi") (r "^0.1.6") (d #t) (k 0)))) (h "0m7nn2mac5fpnvaw4rv0nxsi8a1b8nnc43kx61l6llh82kck6gj5")))

(define-public crate-cansi-2.0.0 (c (n "cansi") (v "2.0.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "159avyiivyrk85nif2pwydx259zvacqyx2kw10ryky6l972xds2m")))

(define-public crate-cansi-2.1.0 (c (n "cansi") (v "2.1.0") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1w0q959phwy7qgpfwq34i3391byc1fccrc2nzxrmd5shjdyi2pjz") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2.1.1 (c (n "cansi") (v "2.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0kqbp1wk3nmbxmaamrd6j5srnkmcwnczjpn95mk39hjgv20r4nqq") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2.2.0 (c (n "cansi") (v "2.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0av9z6xvsw0qvbqnk3lkjcq6pym1biqxwnhg810y3bnyyczvm44n") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2.2.1 (c (n "cansi") (v "2.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1y3yjr2fkla0cssj23lg0l58m0g6af6f8xyf2ms031in2n3sxp2b") (f (quote (("std") ("default" "std") ("alloc"))))))

