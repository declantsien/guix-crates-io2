(define-module (crates-io ca ly calyx-stdlib) #:use-module (crates-io))

(define-public crate-calyx-stdlib-0.4.0 (c (n "calyx-stdlib") (v "0.4.0") (h "02chfl6m2x6wq1p2b7c24wxning10ni02m2mc8y4p8gk0k08x9y0")))

(define-public crate-calyx-stdlib-0.5.0 (c (n "calyx-stdlib") (v "0.5.0") (h "08ga1nz4c4ilqm210fy4c6ndrj3bkczp6s5mmr51m7z8wmgf8av0")))

(define-public crate-calyx-stdlib-0.6.0 (c (n "calyx-stdlib") (v "0.6.0") (h "0lnj8gy62jjsr0a3wbsy38k4rc4hbsbkyas7qamp32cs47cxd4m3")))

(define-public crate-calyx-stdlib-0.6.1 (c (n "calyx-stdlib") (v "0.6.1") (h "094kkivz95a2wyrcdgj5z0g18icgy3wdlfacq93gv6gcamwplvgk")))

(define-public crate-calyx-stdlib-0.7.0 (c (n "calyx-stdlib") (v "0.7.0") (h "0lp12zwkaaa4as82b3bdi9xvc3lf7x3ifkg67bjsrdmp23nyhcgf") (y #t)))

(define-public crate-calyx-stdlib-0.7.1 (c (n "calyx-stdlib") (v "0.7.1") (h "1ylnh633wsyijllq7n0pfaij4pnlbvxv2ss4rgz21fyalik664gd")))

