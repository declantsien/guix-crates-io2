(define-module (crates-io ca ly calypso_error) #:use-module (crates-io))

(define-public crate-calypso_error-1.0.0 (c (n "calypso_error") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "05b9j8agahlw1fd7v21vsxc9jq26i95blbpl9s7hal6ls402l8xs") (y #t)))

(define-public crate-calypso_error-1.0.1 (c (n "calypso_error") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1f17apgal0pbp0g4biwn76in63bx3yxy4malz25ys2m57p47hdfq") (y #t)))

(define-public crate-calypso_error-1.0.2 (c (n "calypso_error") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1gnji1bdc2m4k15yh8pn3dq4pv0gm69iaad149jp70gipgyvxhnl") (y #t)))

(define-public crate-calypso_error-2.0.0 (c (n "calypso_error") (v "2.0.0") (d (list (d (n "eyre") (r "^0.6.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hzjkkdd94n7frw35mg9ydflilmd0pvc3mkalk9xfpdlwrbq7446") (y #t) (r "1.56")))

