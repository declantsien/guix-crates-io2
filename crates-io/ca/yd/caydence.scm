(define-module (crates-io ca yd caydence) #:use-module (crates-io))

(define-public crate-caydence-0.1.0 (c (n "caydence") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "libnotify") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7vnqbvdvrqmjih2npw7g1sy2kk3v0npmm4rxpbn5jw5bzsd4wq")))

(define-public crate-caydence-0.1.1 (c (n "caydence") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "libnotify") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6gr2n5kn29mqndwh2d7pd90jy1dgaixw8f9iv4y1kf0fzzrmbp")))

(define-public crate-caydence-0.2.0 (c (n "caydence") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "libnotify") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i157ag78yzyw767zmf4hvf7dhnpqjhj4chr9c88vqrri8gfdjhs")))

(define-public crate-caydence-0.2.1 (c (n "caydence") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "libnotify") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d0k4nxabgibx06zpy7lc7mw6d33hd9rh816900ysbb3xc5wyvpm")))

