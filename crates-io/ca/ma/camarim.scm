(define-module (crates-io ca ma camarim) #:use-module (crates-io))

(define-public crate-camarim-0.1.0 (c (n "camarim") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.8") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "syslog") (r "^5.0.0") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "00xlx9p57km6m8kh2m0pn40k5v936avblv6fk9k41c0q9fhzb0v8")))

