(define-module (crates-io ca nv canvy) #:use-module (crates-io))

(define-public crate-canvy-0.1.0 (c (n "canvy") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dnb493lrhg1lykmwha5s098syr9qxfjmglzjxyh7h451gc58aqa")))

(define-public crate-canvy-0.2.0 (c (n "canvy") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q29sddfm0zm74zmh4s3g17n52p8mg41mj1x2bbkbsmqhipgq69h")))

(define-public crate-canvy-0.2.1 (c (n "canvy") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05mx4wwnh532fh119iss8xdwsy4jga779mf8gihz7zrkl3dcvqxz") (y #t)))

(define-public crate-canvy-0.2.2 (c (n "canvy") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vaj0b88mi3pzsriw5zz3rldw63zm6disarsw3gmpdbcy13bqvf3") (y #t)))

(define-public crate-canvy-0.2.3 (c (n "canvy") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qbmixcscadgzjrzlxqqqvxzby7smfkblrfm9w6qxdzjkf6m5zzw")))

(define-public crate-canvy-0.2.4 (c (n "canvy") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fwr71v41f1786zncip9x4via64hzgq6q4m25qi8p8i344bfw8d1")))

(define-public crate-canvy-0.2.5 (c (n "canvy") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08pd79s4fpc37zsvkf1n37jpxjc0fbw14cj1zrs8r1ksjxsmdh2j")))

