(define-module (crates-io ca nv canvas) #:use-module (crates-io))

(define-public crate-canvas-1.0.0 (c (n "canvas") (v "1.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "195ijw8wz8isqvc907w61cplym9bykd0nlw964ba6bkwxj689l1y")))

(define-public crate-canvas-1.0.1 (c (n "canvas") (v "1.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "1mrywmyj74snl5my8qpvxhgx6z2r3pn622m25p4anv0ham4y5ir5")))

