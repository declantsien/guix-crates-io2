(define-module (crates-io ca r_ car_registration) #:use-module (crates-io))

(define-public crate-car_registration-0.1.0 (c (n "car_registration") (v "0.1.0") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m5awbmvzydwc3m1lh2lhhmgmgks5vw6j6q1y5n9mw5n9mlgiii1")))

