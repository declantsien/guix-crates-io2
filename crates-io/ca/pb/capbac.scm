(define-module (crates-io ca pb capbac) #:use-module (crates-io))

(define-public crate-capbac-0.1.0 (c (n "capbac") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc") (r "~2") (d #t) (k 1)) (d (n "protoc-rust") (r "~2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0699h46df38h6wz2hrhka5k9np1lknq2lbp9bb9a9pp9wh409p70")))

(define-public crate-capbac-0.2.0 (c (n "capbac") (v "0.2.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc") (r "~2") (d #t) (k 1)) (d (n "protoc-rust") (r "~2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1yh4nmqv1wdmvfl94gqjagrs5spkmip5hb9s8lx6y224625587x6")))

(define-public crate-capbac-0.2.1 (c (n "capbac") (v "0.2.1") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "^2.16.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1bcv3fppyz9c7kibhfgnnwsq41z1m11gkz17y278p1nwrv8plwms")))

(define-public crate-capbac-0.3.0 (c (n "capbac") (v "0.3.0") (d (list (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "protobuf") (r "^2.16.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "008p4z50yvswkmdm50000r94xw8gqiax1z9ijvylams6chhdi620")))

