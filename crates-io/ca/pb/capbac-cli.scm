(define-module (crates-io ca pb capbac-cli) #:use-module (crates-io))

(define-public crate-capbac-cli-0.3.0 (c (n "capbac-cli") (v "0.3.0") (d (list (d (n "capbac") (r "^0.3.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "protobuf") (r "^2.16.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "12kanlrmag59jp2l8b8myh8mcjcagrqylsbhnmsynlaqmdq5s8pp")))

