(define-module (crates-io ca lm calm_io) #:use-module (crates-io))

(define-public crate-calm_io-0.1.0 (c (n "calm_io") (v "0.1.0") (d (list (d (n "calmio_filters") (r "^0.1") (d #t) (k 0)))) (h "0kc2kqms8gc0ig352kfkq43ga78vsfmymadpwyqw249f8icy1zck")))

(define-public crate-calm_io-0.1.1 (c (n "calm_io") (v "0.1.1") (d (list (d (n "calmio_filters") (r "^0.1") (d #t) (k 0)))) (h "0pzsp3kiq978cgsffd1fvxkrn8ng6mizil3sq47djhpy023n181f")))

