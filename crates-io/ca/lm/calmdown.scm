(define-module (crates-io ca lm calmdown) #:use-module (crates-io))

(define-public crate-calmdown-1.0.0 (c (n "calmdown") (v "1.0.0") (d (list (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1r061pgyz5fqksrr42ma3azrdqjivdmwzqd3lf7x0xnrxpv1y2q2")))

