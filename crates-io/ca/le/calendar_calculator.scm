(define-module (crates-io ca le calendar_calculator) #:use-module (crates-io))

(define-public crate-calendar_calculator-0.1.0 (c (n "calendar_calculator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0pp18lbwhrmyl0wjk66z095mn6dwhm3c45r1z1zpr5bii48sbvcv") (y #t)))

(define-public crate-calendar_calculator-0.2.0 (c (n "calendar_calculator") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "14c2n1dl0qk7dc1vnafqmgfkvbrdh5gdr9baiir33zvr0pmwg3m8") (y #t)))

(define-public crate-calendar_calculator-0.3.0 (c (n "calendar_calculator") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "15wdss5qn28v1m4mq36pzc00p4k0lmjw28jmnky8m65nx89z7l6a")))

