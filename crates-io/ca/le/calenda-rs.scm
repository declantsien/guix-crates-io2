(define-module (crates-io ca le calenda-rs) #:use-module (crates-io))

(define-public crate-calenda-rs-0.1.0 (c (n "calenda-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)))) (h "0fvhqacn2h2qg10csgi05gqc1zyl77gyzwrwfbfahg7zxv3z1fjj")))

(define-public crate-calenda-rs-0.1.1 (c (n "calenda-rs") (v "0.1.1") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "1993q73s6a74bnj7yg9mbg5l157sa174v3rvzx6d56vj9fhyq98z")))

(define-public crate-calenda-rs-0.1.2 (c (n "calenda-rs") (v "0.1.2") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "1gfc2wbmka9vm0c907799s5gyd0qci21s9vifgy930z4vicgqq5a")))

(define-public crate-calenda-rs-0.1.3 (c (n "calenda-rs") (v "0.1.3") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "0przgbq19fmqy4q3y8xamfr2kkziw8na2jlibvz62lh9id9sczgx")))

(define-public crate-calenda-rs-0.1.4 (c (n "calenda-rs") (v "0.1.4") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "1379lgn1kp7kh6givmr5plsykck6zc9fyizvy2a2sr8q3dq7d15d")))

(define-public crate-calenda-rs-0.1.5 (c (n "calenda-rs") (v "0.1.5") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rdbik8pv0n88h2vw9mcsf2fmq74rrhv9aaf7z8465f8cmdyfpa8")))

(define-public crate-calenda-rs-0.1.6 (c (n "calenda-rs") (v "0.1.6") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "0az1chpmypfkxvr94fw79w92j78aibghv0aydq5nqax7yj8fzbcp")))

(define-public crate-calenda-rs-0.1.7 (c (n "calenda-rs") (v "0.1.7") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "00ir5vs4fmyw92h1lmdvpgc5l60g5g12dnf53jak7bjvdq7lns52")))

(define-public crate-calenda-rs-0.1.8 (c (n "calenda-rs") (v "0.1.8") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "1l7dinaidpb39l2jd9ndljkpx3pfhhdjqj44msgl1vyk84yihphq")))

(define-public crate-calenda-rs-0.1.9 (c (n "calenda-rs") (v "0.1.9") (d (list (d (n "time") (r "^0.3.34") (f (quote ("macros"))) (d #t) (k 0)))) (h "16jgg4xcxrvkljnwyb2dqvi7jx73x9zxv9wg4p20by47682140fh")))

