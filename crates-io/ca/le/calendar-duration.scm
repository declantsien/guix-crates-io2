(define-module (crates-io ca le calendar-duration) #:use-module (crates-io))

(define-public crate-calendar-duration-1.0.0 (c (n "calendar-duration") (v "1.0.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "parsing"))) (d #t) (k 2)))) (h "1w4pmcp421idmjn0s3mvrn5as8cnz09x2as8x3j1ccy9siwh2dvn")))

