(define-module (crates-io ca le calendrical_calculations) #:use-module (crates-io))

(define-public crate-calendrical_calculations-0.1.0 (c (n "calendrical_calculations") (v "0.1.0") (d (list (d (n "core_maths") (r "^0.1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (k 0)))) (h "0pcsrzx1884pdhbdh5z3az21hm0wfckczndnzpx6fihblp33pzld") (f (quote (("std")))) (r "1.66")))

(define-public crate-calendrical_calculations-0.1.1 (c (n "calendrical_calculations") (v "0.1.1") (d (list (d (n "core_maths") (r "^0.1.0") (k 0)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "log") (r "^0.4.17") (o #t) (k 0)))) (h "1pxvansdic9jpzns65jzgcf38zal3g5cxmqj4fiizf5116r97i6f") (f (quote (("std")))) (s 2) (e (quote (("logging" "dep:log")))) (r "1.67")))

