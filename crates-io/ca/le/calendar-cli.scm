(define-module (crates-io ca le calendar-cli) #:use-module (crates-io))

(define-public crate-calendar-cli-0.1.0 (c (n "calendar-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "term_grid") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "12g3bcn5y45czlgnl0gnrrqkdna99xkma6a8q5yqk8sv4nl0x1an")))

