(define-module (crates-io ca le calendrier) #:use-module (crates-io))

(define-public crate-calendrier-0.1.0 (c (n "calendrier") (v "0.1.0") (h "046mkyrf3bw7y8iljvcc1dw1fhsy90wvzwr2zdg1jn7avnx1jw3m")))

(define-public crate-calendrier-0.1.1 (c (n "calendrier") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0j41zhix3ayvgpr4w6ayhwdfmvw6lrpci1kkmd3am5jz38i8xva1")))

(define-public crate-calendrier-0.1.2 (c (n "calendrier") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0y8mfbg59fjix1jbv3s98s61n8b3i2id36jjzxq924ap64j3l842") (f (quote (("no-time-offset") ("default" "chrono") ("average-time-offset")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-calendrier-0.1.3 (c (n "calendrier") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0hmx3s3ip7glkcn7qkq1bm2p5dqxsyfzlj6ab8dlzlhwwdv3niad") (f (quote (("no-time-offset") ("default" "chrono") ("average-time-offset")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-calendrier-0.1.4 (c (n "calendrier") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1idrnrchg1gr5h835iivcsam2j2jaf8p9ydq6kchdsn9vymgg9dy") (f (quote (("no-time-offset") ("default" "chrono") ("average-time-offset")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-calendrier-0.1.5 (c (n "calendrier") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0jimamahy0dk4i0ni5zk5l3pi83a9f4z4pszgvii04ivjriyxhlq") (f (quote (("no-time-offset") ("default" "chrono") ("average-time-offset")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-calendrier-0.1.6 (c (n "calendrier") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 1)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)))) (h "0zb5f6k3w6w8hzw4acmv295pd9h0r6dzlzid5p8dwydlw1s01xzb") (f (quote (("no-time-offset") ("default" "chrono") ("average-time-offset")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

