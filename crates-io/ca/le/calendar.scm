(define-module (crates-io ca le calendar) #:use-module (crates-io))

(define-public crate-calendar-0.0.1 (c (n "calendar") (v "0.0.1") (h "09d1jrywxd78cc2xjlqsc5335kmd597l7d919ijyhfa7sz6bqakq") (y #t)))

(define-public crate-calendar-0.1.0 (c (n "calendar") (v "0.1.0") (h "1hhdm7vyw43mfcirmx2kwfdv6bj0iy5l3p1vvkmlg0wy3y6diyfs") (y #t)))

(define-public crate-calendar-0.1.1 (c (n "calendar") (v "0.1.1") (h "1wys9hix88ll0s6hz1065cvafhn0qyhcdrddz3i351r1k08kq5gc") (y #t)))

(define-public crate-calendar-0.1.2 (c (n "calendar") (v "0.1.2") (h "1bdlq4f8ach0w1j3wnv8f6jni7hwmynz6q1vs2hlx0x3fqxzh751") (y #t)))

(define-public crate-calendar-0.1.3 (c (n "calendar") (v "0.1.3") (h "07dwyi67m35n7lacgwsc67k2b4z5l4a3irp38xdxkp7dcnpax12a") (y #t)))

