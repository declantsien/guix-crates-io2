(define-module (crates-io ca do cadom) #:use-module (crates-io))

(define-public crate-cadom-0.1.0 (c (n "cadom") (v "0.1.0") (d (list (d (n "cubob") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "00vakkjpqpf7izhb57b1sxm8cy1ndlk2zjymkjs6x230qd3x9rgk")))

(define-public crate-cadom-0.2.0 (c (n "cadom") (v "0.2.0") (d (list (d (n "cubob") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "15cr6kjm46haxphzl6qbn78ifhdra4jp2v64v9vsaixcs9g071ja")))

(define-public crate-cadom-0.3.0 (c (n "cadom") (v "0.3.0") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "14m06zsgjlz1z2k48gslamijwklbgxchd12n8scc44iqc0m02v2x") (f (quote (("default" "serde"))))))

(define-public crate-cadom-0.4.0 (c (n "cadom") (v "0.4.0") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "=1.8.2") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0ykrppdf81xwbalhiwrgysz7g1bbpqivcc9sr391yfk1b0mgz979") (f (quote (("schema" "serde" "schemars" "indexmap") ("default"))))))

(define-public crate-cadom-0.4.1 (c (n "cadom") (v "0.4.1") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "=1.8.2") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0appvh5ns124ip76r55yn11a3f19046i69h37q3670rkxln71iim") (f (quote (("schema" "serde" "schemars" "indexmap") ("default"))))))

(define-public crate-cadom-0.4.2 (c (n "cadom") (v "0.4.2") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "=1.8.2") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0rw59ayfr57zr7p4j19g3wfrd3nb5dydqkcrq1wwqd1zjx1jq17k") (f (quote (("schema" "serde" "schemars" "indexmap") ("default"))))))

(define-public crate-cadom-0.4.3 (c (n "cadom") (v "0.4.3") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "=1.8.2") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "128kpm24jsfplnk09w5wm4fvzh289yvs002b3dbygismigpc01cj") (f (quote (("schema" "serde" "schemars" "indexmap") ("default"))))))

(define-public crate-cadom-0.4.4 (c (n "cadom") (v "0.4.4") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0axqili7ic7an5b0vc7w0j7jx46196ab6dwz4rzi0nncji3pn2y0") (f (quote (("schema" "serde" "schemars") ("default"))))))

(define-public crate-cadom-0.4.5 (c (n "cadom") (v "0.4.5") (d (list (d (n "cubob") (r "^1.3.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0laja7ka0nlyvyjlx1iygrkjhcdhxra3sv0iz40hx60bw0n8d6c3") (f (quote (("schema" "serde" "schemars") ("default") ("all" "serde" "schema"))))))

