(define-module (crates-io ca ct cacti_weaver_protos_rs) #:use-module (crates-io))

(define-public crate-cacti_weaver_protos_rs-2.0.0-alpha-prerelease (c (n "cacti_weaver_protos_rs") (v "2.0.0-alpha-prerelease") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "0vfjp39a8iqmlzm5gnn9k3p31lnsanasdlsm71h5w9m9lnbaw95n")))

(define-public crate-cacti_weaver_protos_rs-2.0.0-alpha.1 (c (n "cacti_weaver_protos_rs") (v "2.0.0-alpha.1") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "12ww4lf781fl05kxih9yscjgd51kqxnxskm9w39rqxl4ymg6sc02")))

(define-public crate-cacti_weaver_protos_rs-2.0.0-alpha.2 (c (n "cacti_weaver_protos_rs") (v "2.0.0-alpha.2") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "1fs806c92lg5kd4rqfpway9mdryhi9rfqpqdv6nwb3pd2hgx3d0k")))

