(define-module (crates-io ca ct cactus) #:use-module (crates-io))

(define-public crate-cactus-0.1.0 (c (n "cactus") (v "0.1.0") (h "0ya2wg5a90p51b88qv7klz2kl45pqgha4klasrnbvqh60vmc1yaq")))

(define-public crate-cactus-0.1.1 (c (n "cactus") (v "0.1.1") (h "104qkwkjmzim3kqmm102r7yw4npz2bng9i4f58kv71nbmqy7209m")))

(define-public crate-cactus-1.0.0 (c (n "cactus") (v "1.0.0") (h "1pjni13dqnsp7jlbwymmmi2yr3nl1kya3jw55v8hgjii9g17vxwk")))

(define-public crate-cactus-1.0.1 (c (n "cactus") (v "1.0.1") (h "11fad2ch80vycyj2124sqqbh9a3hhmc7r2bv92vwzzy1xdv3aqp7")))

(define-public crate-cactus-1.0.2 (c (n "cactus") (v "1.0.2") (h "123c4100ck6pdc0kn6kgn5f7rwwrmdcxmn7pgyslnjjgl73dswj5")))

(define-public crate-cactus-1.0.3 (c (n "cactus") (v "1.0.3") (h "15skgi5mshry8cqy1ij6vy8iwpc9nswdnmq8hbrcw1kp6ak5iqmv")))

(define-public crate-cactus-1.0.4 (c (n "cactus") (v "1.0.4") (h "10c59ss4abiw3zv486s0sqdnfrai8hf9mb4rz4qyrsrg1z999lp1")))

(define-public crate-cactus-1.0.5 (c (n "cactus") (v "1.0.5") (h "1sv86khqnyix12k18dr6glvvl876xrs7c5ck3b3f8cr7mkncj4q7")))

(define-public crate-cactus-1.0.6 (c (n "cactus") (v "1.0.6") (h "1xfwhzfp562qasmny3jqdccfi5dzh82qi7k1dlf036ninxjlf0yg")))

(define-public crate-cactus-1.0.7 (c (n "cactus") (v "1.0.7") (h "0r05bx1632735gqndgawwpskrgq2jh5g2g9f8jvz87c75lw2dg5c")))

