(define-module (crates-io ca ct cactive_hypixel_api) #:use-module (crates-io))

(define-public crate-cactive_hypixel_api-0.1.0 (c (n "cactive_hypixel_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rf4f898pa8m85gpls58d2gpwfhqpb45bg1bqinsq6m5nn0j6zjf")))

