(define-module (crates-io ca ct cactusref) #:use-module (crates-io))

(define-public crate-cactusref-0.1.0 (c (n "cactusref") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.4") (k 2)) (d (n "hashbrown") (r "^0.11") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (k 0)))) (h "0ipsibwlm1s1c7p6dvla7rgccbn25kj77d19wb2x4pjhxkv367g4")))

(define-public crate-cactusref-0.1.1 (c (n "cactusref") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8.4") (k 2)) (d (n "hashbrown") (r "^0.11") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (k 0)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1yxivqw6g1ks39nw4qiayn3a6i8291g9r2rr7bw94y3z9sqkhkkf")))

(define-public crate-cactusref-0.2.0 (c (n "cactusref") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (k 2)) (d (n "hashbrown") (r "^0.12.0") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0il4d7naavf6609hzd5n298zj18zzhvpqqc0xq0cb8mnm78vhxpj") (f (quote (("std") ("default" "std"))))))

(define-public crate-cactusref-0.3.0 (c (n "cactusref") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "hashbrown") (r "^0.13.1") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0i0lqa66h1zabm5y4v5yz4f38w90n8izx24r5lcmq0q54raf08n0") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-cactusref-0.4.0 (c (n "cactusref") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (k 0)) (d (n "version-sync") (r "^0.9.5") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "07dqnsaj20sp1cf20r3mzmk0qm8njbkks4g93gpawan9585i4y85") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

(define-public crate-cactusref-0.5.0 (c (n "cactusref") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("inline-more"))) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (k 0)) (d (n "version-sync") (r "^0.9.5") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1hsv5jnhx3ga5mp439sr5wk3156bcm797y2if1axg8rd6b260nii") (f (quote (("std") ("default" "std")))) (r "1.77.0")))

