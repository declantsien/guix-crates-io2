(define-module (crates-io ca ja caja) #:use-module (crates-io))

(define-public crate-caja-0.1.0 (c (n "caja") (v "0.1.0") (h "00a51288z220yrjzl822gi0q2a11bld9yhb5xgqfcssl1qrhqyak")))

(define-public crate-caja-0.1.1 (c (n "caja") (v "0.1.1") (h "03cnhlhzdl0b12vlf277spkxmryi3aqd2wi981fc0h43k78hxmiq")))

(define-public crate-caja-0.2.0 (c (n "caja") (v "0.2.0") (h "1vkd7vv43hx94sz7lbam8fv254ajlb09g2qsk6adg3wj5l4qy9b9")))

(define-public crate-caja-0.2.1 (c (n "caja") (v "0.2.1") (h "0jrbhv31a7bgxbg4grfwajh8b09bnh1l0yq3af0mh1x227dcyflf")))

