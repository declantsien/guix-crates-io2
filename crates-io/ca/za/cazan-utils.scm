(define-module (crates-io ca za cazan-utils) #:use-module (crates-io))

(define-public crate-cazan-utils-0.0.0 (c (n "cazan-utils") (v "0.0.0") (h "1nyj9w622lhrgd0apfa5q25ryqji4vk21a3fh0856f3dqd1ghylp") (y #t)))

(define-public crate-cazan-utils-0.0.1 (c (n "cazan-utils") (v "0.0.1") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "03g4l6h7gkxgsh1p8ww243kln6rnakijk42b2iwaswyzv1f33rm1") (f (quote (("points_import" "points") ("points_export" "points") ("points" "mint" "serde_json")))) (y #t)))

(define-public crate-cazan-utils-1.0.0 (c (n "cazan-utils") (v "1.0.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "0r71q8vcavfhr3csb8yi3k6cbf55abz6ly0x93w2n13dbzkyw3kr") (f (quote (("points_import" "points") ("points_export" "points") ("points" "mint" "serde_json")))) (y #t)))

