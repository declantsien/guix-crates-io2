(define-module (crates-io ca va cavalier_contours_ffi) #:use-module (crates-io))

(define-public crate-cavalier_contours_ffi-0.1.0 (c (n "cavalier_contours_ffi") (v "0.1.0") (d (list (d (n "cavalier_contours") (r "^0.1") (d #t) (k 0)))) (h "0w28pbihbbi9kiwnhrmzsiw7rndacp6fsrdkpvh6av1xwln84zxz")))

(define-public crate-cavalier_contours_ffi-0.2.0 (c (n "cavalier_contours_ffi") (v "0.2.0") (d (list (d (n "cavalier_contours") (r "^0.2") (d #t) (k 0)))) (h "1gpl9zywr9fm64a6bwp0bvbdzphy57lrpd0c5wj7dr20djgyhgd1")))

(define-public crate-cavalier_contours_ffi-0.4.0 (c (n "cavalier_contours_ffi") (v "0.4.0") (d (list (d (n "cavalier_contours") (r "^0.4") (d #t) (k 0)))) (h "1wm1f16sq6xwzal66n3a8pfg686kziniv8kpbbm4sycj6liiqs8f")))

