(define-module (crates-io ca va cavalier_contours) #:use-module (crates-io))

(define-public crate-cavalier_contours-0.1.0 (c (n "cavalier_contours") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "static_aabb2d_index") (r "^0.4") (d #t) (k 0)))) (h "02v59aibahq3x7yi5pfvjwrp6faz9iq81h52byd143v48xismhv9") (f (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.2.0 (c (n "cavalier_contours") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_aabb2d_index") (r "^0.6") (d #t) (k 0)))) (h "07y804fbk8p74kmq0f81jlmcjp40gwpzqqwpyhzya18988mcca46") (f (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.3.0 (c (n "cavalier_contours") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_aabb2d_index") (r "^0.7") (d #t) (k 0)))) (h "1n65ppkd3ghifs52ybkqvbs22jh82cyyhfnvz0ldicixxki4hlip") (f (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.4.0 (c (n "cavalier_contours") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_aabb2d_index") (r "^2.0") (d #t) (k 0)))) (h "15wajjf9ss08h7cnvbiykamq0lylpk7syaldbsgca8m57nvi8vfr") (f (quote (("unsafe_optimizations" "static_aabb2d_index/unsafe_optimizations") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

