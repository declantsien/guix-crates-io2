(define-module (crates-io ca pc capcom0) #:use-module (crates-io))

(define-public crate-capcom0-0.1.0 (c (n "capcom0") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "ioapiset" "libloaderapi" "memoryapi" "processenv" "securitybaseapi" "shellapi" "synchapi" "winerror" "winnt" "winreg" "winbase"))) (d #t) (k 0)))) (h "1z497p14jw8qcndadr0qd6caxz7q548gyqmb81fvxzpdz6hzv5iz")))

(define-public crate-capcom0-0.1.1 (c (n "capcom0") (v "0.1.1") (d (list (d (n "ntapi") (r "^0.3") (d #t) (k 0)) (d (n "obfstr") (r "^0.1") (k 2)) (d (n "pelite") (r "^0.7") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "ioapiset" "memoryapi" "processenv" "securitybaseapi" "shellapi" "winerror" "winnt" "winreg" "winbase"))) (d #t) (k 0)))) (h "0v7zz7dwjj6pvc9qvqksp0zv6rc9vyajw1ggl3ndqnyhcgfnfglx")))

