(define-module (crates-io ca pc capctl) #:use-module (crates-io))

(define-public crate-capctl-0.1.0 (c (n "capctl") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "177929v1fc9alw10waxnhv83d1jmwfhhqwa092g1d0jh3m2iyzli")))

(define-public crate-capctl-0.2.0 (c (n "capctl") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "sc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1jlawz9g6jnxv7kkykcwh5rgd9qg0k3jxk71g43hlvy56hddk87f") (f (quote (("std") ("default" "std"))))))

(define-public crate-capctl-0.2.1 (c (n "capctl") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "sc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "00f84kw419hwqxhckizxmxk1j0jw9yf2a82xq59b1kx78s3nlv2j") (f (quote (("std") ("default" "std"))))))

(define-public crate-capctl-0.2.2 (c (n "capctl") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "sc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1b9cc4a0nqx875a6wv60bp66gprx3i7hh4wdliqmscpwiakk5p7v") (f (quote (("std") ("default" "std"))))))

(define-public crate-capctl-0.2.3 (c (n "capctl") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "sc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "11ns6m292lnzadvfqqppriicg6nywrpl2qs5fdglk8vlhyn5vvhv") (f (quote (("std") ("default" "std"))))))

(define-public crate-capctl-0.2.4 (c (n "capctl") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "sc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "19bawpnpzrlzp3s3ph63bxr460zc8xqxdmpy6cm1rxc5fmv72vja") (f (quote (("std") ("default" "std"))))))

