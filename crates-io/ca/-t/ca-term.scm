(define-module (crates-io ca -t ca-term) #:use-module (crates-io))

(define-public crate-ca-term-0.0.1 (c (n "ca-term") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1j88hin1n5zwa5dagxs2c91f3z0b5x54g6b9nm3xv2ip4jjsqncz")))

(define-public crate-ca-term-0.0.2 (c (n "ca-term") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0jqnng879gd9akqlj6mzzg8ppz7qbddp0f8ji7avy9cx5ykcxs3p")))

(define-public crate-ca-term-0.0.3 (c (n "ca-term") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1a11y30qmi57xsndxn6xs8wbkfw3ickj9l27y8xhz1fcvc8pv1pw")))

(define-public crate-ca-term-0.0.4 (c (n "ca-term") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0xxzjwl2hjajinq8lk028dxlrp2rkpxmlc5agyp3ij50xky6mxb1")))

