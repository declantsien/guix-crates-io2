(define-module (crates-io ca ff caffe2op-crossentropy) #:use-module (crates-io))

(define-public crate-caffe2op-crossentropy-0.1.3-alpha.0 (c (n "caffe2op-crossentropy") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1wsla75jllshbalzhl2akqz62zgjq6i6k2cw37ylnjmhy0h5igj1")))

(define-public crate-caffe2op-crossentropy-0.1.4-alpha.0 (c (n "caffe2op-crossentropy") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0yi74w8cwyf9iyisjls0cdc6d8234vpajqsxj35ff0rjq9v4a848")))

(define-public crate-caffe2op-crossentropy-0.1.5-alpha.0 (c (n "caffe2op-crossentropy") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "01vvbhvabaj5q9xylk9prr4wkpiph3kmnhgi71pxm8ph11hwi3ii")))

