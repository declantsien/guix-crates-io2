(define-module (crates-io ca ff caffe2op-stump) #:use-module (crates-io))

(define-public crate-caffe2op-stump-0.1.4-alpha.0 (c (n "caffe2op-stump") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1b57l30fbl8b3qz5ks84m7x7hmkkw40s5rmx86npapws2q01ns5d")))

(define-public crate-caffe2op-stump-0.1.5-alpha.0 (c (n "caffe2op-stump") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0h73q968wnjw8sl5zr2j40xw1hqnj65ssfyzfxfdqasig4v5cvf2")))

