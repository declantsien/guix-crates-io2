(define-module (crates-io ca ff caffe2op-atomics) #:use-module (crates-io))

(define-public crate-caffe2op-atomics-0.1.3-alpha.0 (c (n "caffe2op-atomics") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0ha87ar5b81bn2696pgl07pc6g6ysr9hz7v18s91yfwjc1vx90p8")))

(define-public crate-caffe2op-atomics-0.1.4-alpha.0 (c (n "caffe2op-atomics") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0nlc425snqk2hbvbkqnns9sdczdklfc2as7akymrsbm90116ar65")))

(define-public crate-caffe2op-atomics-0.1.5-alpha.0 (c (n "caffe2op-atomics") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0i54sb54ahfxwj3fkfx94xgs62frbgadavdswmz5x8gl342j7nsy")))

