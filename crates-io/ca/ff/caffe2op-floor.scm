(define-module (crates-io ca ff caffe2op-floor) #:use-module (crates-io))

(define-public crate-caffe2op-floor-0.1.4-alpha.0 (c (n "caffe2op-floor") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "15p5g4f0xq6zg65f460sx72iv453nk49v6yl0kkklcccy7d1py57")))

(define-public crate-caffe2op-floor-0.1.5-alpha.0 (c (n "caffe2op-floor") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1gk773pvc3z861spf309ivdqh8f4hb7qknxrqidrk6mxmgm8nz6p")))

