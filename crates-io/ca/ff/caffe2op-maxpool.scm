(define-module (crates-io ca ff caffe2op-maxpool) #:use-module (crates-io))

(define-public crate-caffe2op-maxpool-0.1.4-alpha.0 (c (n "caffe2op-maxpool") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0kwgdywh2r7d8vcpaazbzkyqcjvjsb8dk0mc7278y16s6iwxr1qa")))

