(define-module (crates-io ca ff caffe2-qtensor) #:use-module (crates-io))

(define-public crate-caffe2-qtensor-0.1.3-alpha.0 (c (n "caffe2-qtensor") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "15aq2017yqdd53biwkcxzgp09r4ppx1p0yn0w09pmpwx3bb5z3r8")))

(define-public crate-caffe2-qtensor-0.1.4-alpha.0 (c (n "caffe2-qtensor") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0hs0ib5f69hcpcdmpmfkk83gx4c63fw47c4wqnd71qni58h8wyzj")))

(define-public crate-caffe2-qtensor-0.1.5-alpha.0 (c (n "caffe2-qtensor") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ss4n9yw72533xbvfalkgj0dfg5h2fd1jskf06dw7hf1rf06rwig")))

