(define-module (crates-io ca ff caffe2op-reduce) #:use-module (crates-io))

(define-public crate-caffe2op-reduce-0.1.4-alpha.0 (c (n "caffe2op-reduce") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1x24jmvbb6wq01xnn3ks4wx8wa0yg6690v485g41fmb5cr5b3zkf")))

(define-public crate-caffe2op-reduce-0.1.5-alpha.0 (c (n "caffe2op-reduce") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1kfkc3grspjbd71f3rbx4fk6cfjr7hvzy33lim0ahki90v2kymr7")))

