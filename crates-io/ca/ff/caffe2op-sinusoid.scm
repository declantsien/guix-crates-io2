(define-module (crates-io ca ff caffe2op-sinusoid) #:use-module (crates-io))

(define-public crate-caffe2op-sinusoid-0.1.4-alpha.0 (c (n "caffe2op-sinusoid") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "08x1c8vs8r1h5acv0qayywsg4h0k6fmcfgclhvf9wbq57hznlffk")))

