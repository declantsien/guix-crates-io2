(define-module (crates-io ca ff caffe2op-enforce) #:use-module (crates-io))

(define-public crate-caffe2op-enforce-0.1.4-alpha.0 (c (n "caffe2op-enforce") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1spldifm7pv6ra6ds52p9qyvm3yv4hwdjfpxpxk41c7d1pakmsfw")))

(define-public crate-caffe2op-enforce-0.1.5-alpha.0 (c (n "caffe2op-enforce") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0ns61fichkwin6i0qww6ph55ylvd8qqifnf97wj91cablifvhy56")))

