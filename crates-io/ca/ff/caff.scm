(define-module (crates-io ca ff caff) #:use-module (crates-io))

(define-public crate-caff-0.1.0 (c (n "caff") (v "0.1.0") (h "054vpmfi103cmh345wqpph5vdmqijys6ba9a8rj7q33q5h7ghc54")))

(define-public crate-caff-0.1.1 (c (n "caff") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "impl-default"))) (k 0)))) (h "1jcaq1m9mh9240b27l8xj6sbig4xmi23wfcl0kfhmalnv4z77kx7")))

(define-public crate-caff-0.1.2 (c (n "caff") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "impl-default"))) (k 0)))) (h "03zz6p3j32287wdlsavhj1yn7kbq2ai4iac8nzxj4b08s7b3lvwj")))

