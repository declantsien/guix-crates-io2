(define-module (crates-io ca ff caffe2op-rsqrt) #:use-module (crates-io))

(define-public crate-caffe2op-rsqrt-0.1.4-alpha.0 (c (n "caffe2op-rsqrt") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "15ykh3hmp53xqzil2ca3fj0j6ji1whawwcmihq9ljm5pn067p7rs")))

(define-public crate-caffe2op-rsqrt-0.1.5-alpha.0 (c (n "caffe2op-rsqrt") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0wsshdckpmd0pidi6216bbcbhgkarnsy3bl019zl5swr7sgnqaa3")))

