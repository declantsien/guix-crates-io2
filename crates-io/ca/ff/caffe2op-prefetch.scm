(define-module (crates-io ca ff caffe2op-prefetch) #:use-module (crates-io))

(define-public crate-caffe2op-prefetch-0.1.3-alpha.0 (c (n "caffe2op-prefetch") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0qa3fdgsccvd8x33gr12rj5537qn7ppq7vgx8jklqymkl1as9ikq")))

(define-public crate-caffe2op-prefetch-0.1.4-alpha.0 (c (n "caffe2op-prefetch") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "072glz4rnsdx67ayv3rrgbrfdzmgx6kzvkjgmd6d6fdmwa562lgc")))

(define-public crate-caffe2op-prefetch-0.1.5-alpha.0 (c (n "caffe2op-prefetch") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1apb9c3r5k47q1myagfsg92kflqm22f7ib1cyf86xqr9irh8mfvb")))

