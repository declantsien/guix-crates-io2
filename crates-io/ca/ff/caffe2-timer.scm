(define-module (crates-io ca ff caffe2-timer) #:use-module (crates-io))

(define-public crate-caffe2-timer-0.1.3-alpha.0 (c (n "caffe2-timer") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0jd6qnp40hh4187nhqhimhpkyn27fkzgknrqpa6blisasvyawzwb")))

(define-public crate-caffe2-timer-0.1.4-alpha.0 (c (n "caffe2-timer") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0lak1j3n6mgrj48dfghcg2j9laazggcaybljdlzmfv4ddb0fhh2r")))

(define-public crate-caffe2-timer-0.1.5-alpha.0 (c (n "caffe2-timer") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "17vj4340kjhiv99k5zld59ygr0367psw8bis8nq76c6k0zmyn4m0")))

