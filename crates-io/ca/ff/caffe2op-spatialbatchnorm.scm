(define-module (crates-io ca ff caffe2op-spatialbatchnorm) #:use-module (crates-io))

(define-public crate-caffe2op-spatialbatchnorm-0.1.3-alpha.0 (c (n "caffe2op-spatialbatchnorm") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1cmm3rdlsr8dmxayi6gjm1sfsp2ckjx6amp3cfynfm58k7vzqh3i")))

(define-public crate-caffe2op-spatialbatchnorm-0.1.4-alpha.0 (c (n "caffe2op-spatialbatchnorm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "114f7b20ahs6midnkcwkf8i7ybhykkv7v7m5dpi5jmrrqvxr36f6")))

(define-public crate-caffe2op-spatialbatchnorm-0.1.5-alpha.0 (c (n "caffe2op-spatialbatchnorm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0j1131g1267sxdbxf7d3lcrqavmxwjk7qm7c3yk9m91m5zk3060r")))

