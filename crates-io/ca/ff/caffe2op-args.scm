(define-module (crates-io ca ff caffe2op-args) #:use-module (crates-io))

(define-public crate-caffe2op-args-0.1.3-alpha.0 (c (n "caffe2op-args") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2op-apmeter") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1sfp2i242s2hcr52awz50n5y6icgwkx76kx43v5wz2x5l11hws58")))

(define-public crate-caffe2op-args-0.1.4-alpha.0 (c (n "caffe2op-args") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-apmeter") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "06i6dsdndwm68q9vy5qha9k4xh1ghch04qgah556cgsiprdmpa8d")))

(define-public crate-caffe2op-args-0.1.5-alpha.0 (c (n "caffe2op-args") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-apmeter") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "091270csbrvwsk6k97kr59yj6ginsbj4dmxj22fb86rh2w6w7wv8")))

