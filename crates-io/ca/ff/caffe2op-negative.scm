(define-module (crates-io ca ff caffe2op-negative) #:use-module (crates-io))

(define-public crate-caffe2op-negative-0.1.4-alpha.0 (c (n "caffe2op-negative") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0izbk7514yd8llsic8gz67bwzm897q1wincpmzg0lczi2w9iik90")))

(define-public crate-caffe2op-negative-0.1.5-alpha.0 (c (n "caffe2op-negative") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "08ma9md7s9g6ggz498b6v9690gj9ywlpd5bbxch3g4xf9k91c1d2")))

