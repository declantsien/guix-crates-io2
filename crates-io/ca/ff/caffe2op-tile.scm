(define-module (crates-io ca ff caffe2op-tile) #:use-module (crates-io))

(define-public crate-caffe2op-tile-0.1.4-alpha.0 (c (n "caffe2op-tile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "04yfdd1q2pkqa9byi2vkla0nbc12r0darw97vhr8748dpwyx4nm6")))

(define-public crate-caffe2op-tile-0.1.5-alpha.0 (c (n "caffe2op-tile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "04rbnvnwh8gw3wd01fw47rq1czz851ps7xi9qp9ffbp0wqishwvc")))

