(define-module (crates-io ca ff caffe2op-bucketize) #:use-module (crates-io))

(define-public crate-caffe2op-bucketize-0.1.3-alpha.0 (c (n "caffe2op-bucketize") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "13p4q6jci8h78pk9nly5q3ili8hszc43bqvwlk2achi0rfm6r2j6")))

(define-public crate-caffe2op-bucketize-0.1.4-alpha.0 (c (n "caffe2op-bucketize") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1vs3ifl0i9b8k2ivajci1ljfhgwdkn0x637jhs5w7x5yfgnypbgq")))

(define-public crate-caffe2op-bucketize-0.1.5-alpha.0 (c (n "caffe2op-bucketize") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "026xbn3kwv1x915rxax38y052lv43amnr9x29snbydw60y8q1ywh")))

