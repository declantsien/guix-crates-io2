(define-module (crates-io ca ff caffe2-nomnigraph) #:use-module (crates-io))

(define-public crate-caffe2-nomnigraph-0.1.3-alpha.0 (c (n "caffe2-nomnigraph") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xa9qn1j9jv0jgbsf19zwi4fibshrw7s62di7dj96krkxdq0cr5s")))

(define-public crate-caffe2-nomnigraph-0.1.4-alpha.0 (c (n "caffe2-nomnigraph") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1141jvmmqvnjscgm5zf0j5ygwnh028zbm1nj57z6ygx67x5zrrm9")))

(define-public crate-caffe2-nomnigraph-0.1.5-alpha.0 (c (n "caffe2-nomnigraph") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "11987mcrfbpmqf7gsxhgab3zh8wrpdlc6fcynfn4hsdxznx4jlxb")))

