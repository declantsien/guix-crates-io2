(define-module (crates-io ca ff caffe2op-multiclass) #:use-module (crates-io))

(define-public crate-caffe2op-multiclass-0.1.4-alpha.0 (c (n "caffe2op-multiclass") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1mcmsj1r04ygi92llmw7mipjvf481fza1qh7pz1xa3ac48gl4rpx")))

(define-public crate-caffe2op-multiclass-0.1.5-alpha.0 (c (n "caffe2op-multiclass") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "13zhww5q9fqjp4jmivf4i7jma5815wlpyfhhfik9d5v8x2jx2h6s")))

