(define-module (crates-io ca ff caffe2-test) #:use-module (crates-io))

(define-public crate-caffe2-test-0.1.3-alpha.0 (c (n "caffe2-test") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "039wrs4c033wnxasxxvrrkc4f948rizg1n8y2f6c3a168z0vhc1q")))

(define-public crate-caffe2-test-0.1.4-alpha.0 (c (n "caffe2-test") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1kzdcklqsbpivbjaxa8zbixj6z1f16v24h18aaz4q1yaaqackzsd")))

