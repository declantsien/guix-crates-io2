(define-module (crates-io ca ff caffe2op-rms) #:use-module (crates-io))

(define-public crate-caffe2op-rms-0.1.4-alpha.0 (c (n "caffe2op-rms") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "052qjirzr9smlh86ngs3rq8ginjsbajvz4sigymayxynd3xngzqg")))

(define-public crate-caffe2op-rms-0.1.5-alpha.0 (c (n "caffe2op-rms") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1kbl04kg02savhjl2narjcay1nkpghw8ga931czkgplnzwd9skgl")))

