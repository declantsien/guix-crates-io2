(define-module (crates-io ca ff caffe2op-tensorprotos) #:use-module (crates-io))

(define-public crate-caffe2op-tensorprotos-0.1.4-alpha.0 (c (n "caffe2op-tensorprotos") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-prefetch") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1nbb78lnj4q7szs8w47f7vbqfk5qybshy42wa6f4zrx3j9gg512q")))

(define-public crate-caffe2op-tensorprotos-0.1.5-alpha.0 (c (n "caffe2op-tensorprotos") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-prefetch") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0jx7a9b8xcmw87y3wm16dzkaqx15ncxv8qs3j7lmc1zxhsaj1zll")))

