(define-module (crates-io ca ff caffe2op-sparselpreg) #:use-module (crates-io))

(define-public crate-caffe2op-sparselpreg-0.1.4-alpha.0 (c (n "caffe2op-sparselpreg") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0lp43jp55kz1sv7gp425m74z5xyxv231yii901iiw71g4cjbxib6")))

(define-public crate-caffe2op-sparselpreg-0.1.5-alpha.0 (c (n "caffe2op-sparselpreg") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "175i4ah4zsik23bnmchvwdlar9mkbjrdllxcbda2hwx2jq59dnzi")))

