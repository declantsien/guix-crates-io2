(define-module (crates-io ca ff caffe2op-moments) #:use-module (crates-io))

(define-public crate-caffe2op-moments-0.1.4-alpha.0 (c (n "caffe2op-moments") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1wvqaxlyhvm4rmn4912iy06d0kbcc7vc08q70ix9hndap8x7i527")))

(define-public crate-caffe2op-moments-0.1.5-alpha.0 (c (n "caffe2op-moments") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1i6c6qr4p3m4xrkpg37dhfqwq6bw3sq6i68b2yzc94bqzpwh76ny")))

