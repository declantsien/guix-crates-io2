(define-module (crates-io ca ff caffe2-random) #:use-module (crates-io))

(define-public crate-caffe2-random-0.1.3-alpha.0 (c (n "caffe2-random") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1jh2hbbq243x1iljlahwi8dkjh23ln0cldc1lnvp1swxld5m4k13")))

(define-public crate-caffe2-random-0.1.4-alpha.0 (c (n "caffe2-random") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0zcsx9296b1hnwvnnljifmhmgicrwl5aay26x52xbxqpdkg94fnv")))

(define-public crate-caffe2-random-0.1.5-alpha.0 (c (n "caffe2-random") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "18sqcd75440lbnnc44crl25zp3gx9lw6k9589dwgvpwmxpfch2w6")))

