(define-module (crates-io ca ff caffe2op-utility) #:use-module (crates-io))

(define-public crate-caffe2op-utility-0.1.3-alpha.0 (c (n "caffe2op-utility") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "12qip0wkjq4ii9fm7virkkvbhj100m5h41c6vq8cbq628h741fii")))

(define-public crate-caffe2op-utility-0.1.4-alpha.0 (c (n "caffe2op-utility") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1h1pivk401dhxcrx14j2nnzj63fzfzldqq0fq33g9qmb932z98bx")))

(define-public crate-caffe2op-utility-0.1.5-alpha.0 (c (n "caffe2op-utility") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0fsb08335lqqldyjs3x4z560bypv5ybq4hzlyf3c9dw95qqsr9ij")))

