(define-module (crates-io ca ff caffe2op-groupnorm) #:use-module (crates-io))

(define-public crate-caffe2op-groupnorm-0.1.3-alpha.0 (c (n "caffe2op-groupnorm") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0bgkdqlsjq1r92lxi0y6q2wlvicsxis067pg91m46nzi3rrbdirw")))

(define-public crate-caffe2op-groupnorm-0.1.4-alpha.0 (c (n "caffe2op-groupnorm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1d62nxi5pn4l9611216y0j0xhjqswwh4ag89a9gr4vdqz8rl8zvl")))

(define-public crate-caffe2op-groupnorm-0.1.5-alpha.0 (c (n "caffe2op-groupnorm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "10vq606fma3k6x9m0v7q515cd7kv240jdnd9qi08fdz4njrnaaq1")))

