(define-module (crates-io ca ff caffe2-txform) #:use-module (crates-io))

(define-public crate-caffe2-txform-0.1.3-alpha.0 (c (n "caffe2-txform") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-transform") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0liinaxkkkkzq1z7amkav75vq69bn0d7bmws2nhc979nqv0jj1vc")))

(define-public crate-caffe2-txform-0.1.4-alpha.0 (c (n "caffe2-txform") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-transform") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1rpn98s92czihqbl14grd5v287c4a5xlh0px118kvw7hdn3fmxz6")))

