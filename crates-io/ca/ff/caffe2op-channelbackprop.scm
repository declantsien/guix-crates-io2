(define-module (crates-io ca ff caffe2op-channelbackprop) #:use-module (crates-io))

(define-public crate-caffe2op-channelbackprop-0.1.3-alpha.0 (c (n "caffe2op-channelbackprop") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0aklgrgd7nr0gcj47nzjycgy3h57rbgdyj6i3w0frbwfrrlr27ih")))

(define-public crate-caffe2op-channelbackprop-0.1.4-alpha.0 (c (n "caffe2op-channelbackprop") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0g15pacfyjk6jpn70rlzcj4vrjqsvxv4zs9m1cjghbsc1g2rcqpz")))

(define-public crate-caffe2op-channelbackprop-0.1.5-alpha.0 (c (n "caffe2op-channelbackprop") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1sn56ly8qhlcxvpwm1bddpmsrpmvsgza8srpjjq1i1jzhji8ij3j")))

