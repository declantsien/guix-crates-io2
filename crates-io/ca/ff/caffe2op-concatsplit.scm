(define-module (crates-io ca ff caffe2op-concatsplit) #:use-module (crates-io))

(define-public crate-caffe2op-concatsplit-0.1.3-alpha.0 (c (n "caffe2op-concatsplit") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1mz532mg2vfscfa9z4hq7p3vn2avf3382923m60hvi13qh006pgq")))

(define-public crate-caffe2op-concatsplit-0.1.4-alpha.0 (c (n "caffe2op-concatsplit") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1lrhrr07n9kg7ps7gwlram9gh62sjyl10vly9n1sc2ydrqxxp23v")))

(define-public crate-caffe2op-concatsplit-0.1.5-alpha.0 (c (n "caffe2op-concatsplit") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "03nlricr9001xr05xzfyggl51dp56hqs0cs4h8davirhms460jkb")))

