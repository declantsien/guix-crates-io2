(define-module (crates-io ca ff caffe2-mem) #:use-module (crates-io))

(define-public crate-caffe2-mem-0.1.3-alpha.0 (c (n "caffe2-mem") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0hi002kppyc1qxywsq6sc6p3f0jiy6bh8i1ispclllrwivj3g2jz")))

(define-public crate-caffe2-mem-0.1.4-alpha.0 (c (n "caffe2-mem") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "08x4afrs56nrdm8h0ybwmvv4yi5ri2hswny2lgkw2ijqapm007ap")))

(define-public crate-caffe2-mem-0.1.5-alpha.0 (c (n "caffe2-mem") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ckdmcnk6z6518v86cl6nl9l7gr6mmzyqdypimi26265pd827r03")))

