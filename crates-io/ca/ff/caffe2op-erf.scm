(define-module (crates-io ca ff caffe2op-erf) #:use-module (crates-io))

(define-public crate-caffe2op-erf-0.1.4-alpha.0 (c (n "caffe2op-erf") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1gmibpyl79ia1gvrxvlwmzwc7flqz3a5zbbgiya5rsi31v5daazx")))

(define-public crate-caffe2op-erf-0.1.5-alpha.0 (c (n "caffe2op-erf") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "17jjyd4ffly2zjvh0k48hdp5z5rkw8pgwra5sjd8paj6ps87b1lm")))

