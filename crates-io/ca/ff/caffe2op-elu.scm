(define-module (crates-io ca ff caffe2op-elu) #:use-module (crates-io))

(define-public crate-caffe2op-elu-0.1.4-alpha.0 (c (n "caffe2op-elu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-activation") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ggyyn455bdmrxf6gkajvhi6dp9xvphlnwv0zjx1jam6grq37xvr")))

(define-public crate-caffe2op-elu-0.1.5-alpha.0 (c (n "caffe2op-elu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-activation") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0v7jwz28a4a13x81zii5wq6fc0pljdw5w8si1l8javnhc371z4li")))

