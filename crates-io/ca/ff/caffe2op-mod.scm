(define-module (crates-io ca ff caffe2op-mod) #:use-module (crates-io))

(define-public crate-caffe2op-mod-0.1.4-alpha.0 (c (n "caffe2op-mod") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1lb547kx7pypcaw0fp0p0qqpvxgy0a7qcg8x49gbbmp8mdqqmr55")))

(define-public crate-caffe2op-mod-0.1.5-alpha.0 (c (n "caffe2op-mod") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0r7y3632h8h0qi5p74mj9yi2jj2yyxcgjpp7bj450arsh6sw0whn")))

