(define-module (crates-io ca ff caffe2-traits) #:use-module (crates-io))

(define-public crate-caffe2-traits-0.1.3-alpha.0 (c (n "caffe2-traits") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1jc412bxs0r3nsxbsv91s5isnchbbk4z423ma9w5jclk08mxahca")))

(define-public crate-caffe2-traits-0.1.4-alpha.0 (c (n "caffe2-traits") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1l6j6fl9cka2w5vf466bmh56ncpg92mpmpwf4prr43nd44kcdk3v")))

