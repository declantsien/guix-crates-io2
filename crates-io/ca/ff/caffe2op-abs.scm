(define-module (crates-io ca ff caffe2op-abs) #:use-module (crates-io))

(define-public crate-caffe2op-abs-0.1.3-alpha.0 (c (n "caffe2op-abs") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1cn6ybv884sfp759maxdfml3v60qfxd9zhfvb8biiwzrsx1yb3cy")))

(define-public crate-caffe2op-abs-0.1.4-alpha.0 (c (n "caffe2op-abs") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "06x39laqx7n751nqz54gi7gz0iqmbk9z6lmbr2g31zv29k4qm9mb")))

(define-public crate-caffe2op-abs-0.1.5-alpha.0 (c (n "caffe2op-abs") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "02zdgjfiyagfcadvzim7bi200ikd7175l5gpklb7a6x7p6snprz6")))

