(define-module (crates-io ca ff caffe2op-softplus) #:use-module (crates-io))

(define-public crate-caffe2op-softplus-0.1.4-alpha.0 (c (n "caffe2op-softplus") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-nomnigraph") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "178b2gsvdgq480x19hh5frs7pid856swzc4ph6pvqk7z4vw25drh")))

(define-public crate-caffe2op-softplus-0.1.5-alpha.0 (c (n "caffe2op-softplus") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-nomnigraph") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "05lxbm90r80ixhp7i5wwgb47k745sp0b2z0d3xg5y0apsnyqrvg4")))

