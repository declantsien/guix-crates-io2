(define-module (crates-io ca ff caffe2op-rowmul) #:use-module (crates-io))

(define-public crate-caffe2op-rowmul-0.1.4-alpha.0 (c (n "caffe2op-rowmul") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0wnk3q1fni2mlwvy8ywjkrimz33cwdvn58s4iqrx1gbkrs6y2lqy")))

(define-public crate-caffe2op-rowmul-0.1.5-alpha.0 (c (n "caffe2op-rowmul") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0zhs5iryxp7cyr6xff88xh8xmv585dl9f7lk8cq30cp7bsmmnp67")))

