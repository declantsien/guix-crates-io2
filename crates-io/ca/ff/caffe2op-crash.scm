(define-module (crates-io ca ff caffe2op-crash) #:use-module (crates-io))

(define-public crate-caffe2op-crash-0.1.3-alpha.0 (c (n "caffe2op-crash") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "00vvvq8fpxf56gkl6ycpkhfbxgr2dpyzva2ii0kpcgwvc329y0rx")))

(define-public crate-caffe2op-crash-0.1.4-alpha.0 (c (n "caffe2op-crash") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "10skkbhbnvz375gcmfly5w8kfhh1c5i977q9k66hy9w1y7nzx702")))

(define-public crate-caffe2op-crash-0.1.5-alpha.0 (c (n "caffe2op-crash") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0fkjy7g8ncpcz3kg82pjd5s5lx38xvg1cm75phkrps7ybrx6kqna")))

