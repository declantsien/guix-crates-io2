(define-module (crates-io ca ff caffe2op-channelstats) #:use-module (crates-io))

(define-public crate-caffe2op-channelstats-0.1.3-alpha.0 (c (n "caffe2op-channelstats") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1vy3l3gxa4mqgzl2n28kw7vld1h4xyhn7ph1scy67vr31wrmihbk")))

(define-public crate-caffe2op-channelstats-0.1.4-alpha.0 (c (n "caffe2op-channelstats") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "07ys9s1dk20dm6bxxmkdv20914qxhyl8dw1hl63q8v4vhf8sqw2l")))

(define-public crate-caffe2op-channelstats-0.1.5-alpha.0 (c (n "caffe2op-channelstats") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1qhwnpwdgk3azhzrc4r8h835dbkh7ncixsdifh0a6hc58h1akgsm")))

