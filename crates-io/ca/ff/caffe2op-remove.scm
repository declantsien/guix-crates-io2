(define-module (crates-io ca ff caffe2op-remove) #:use-module (crates-io))

(define-public crate-caffe2op-remove-0.1.4-alpha.0 (c (n "caffe2op-remove") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0a148vi2iw2m3h6di11xmhsilbhxrswxj80ac5vdh9qy6cgdlm1d")))

(define-public crate-caffe2op-remove-0.1.5-alpha.0 (c (n "caffe2op-remove") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1glrayf7jzmp3kp3qs56q29wba14admd5yx132fpqq4x2srn4lqj")))

