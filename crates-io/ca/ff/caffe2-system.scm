(define-module (crates-io ca ff caffe2-system) #:use-module (crates-io))

(define-public crate-caffe2-system-0.1.3-alpha.0 (c (n "caffe2-system") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-traits") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "048n22ixba8cx7p2dcs7rca2as2v7jbmkc173fwkgkgg47clqymw")))

(define-public crate-caffe2-system-0.1.4-alpha.0 (c (n "caffe2-system") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-traits") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0g8crbsv8rz87f2pn2i9gwx3asr5qamw9b5p951nihyshyyingr8")))

