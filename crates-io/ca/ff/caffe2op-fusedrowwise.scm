(define-module (crates-io ca ff caffe2op-fusedrowwise) #:use-module (crates-io))

(define-public crate-caffe2op-fusedrowwise-0.1.4-alpha.0 (c (n "caffe2op-fusedrowwise") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1pfvkx2b1ais0fk4n13x5yxay1jdi0fqc4dvkrr9hinkj6bn153h")))

(define-public crate-caffe2op-fusedrowwise-0.1.5-alpha.0 (c (n "caffe2op-fusedrowwise") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0hpy15wx23fmrd393rgzfxlbnj9bf80igvshm98ydglnfqyw6i2i")))

