(define-module (crates-io ca ff caffe2op-hardsigmoid) #:use-module (crates-io))

(define-public crate-caffe2op-hardsigmoid-0.1.4-alpha.0 (c (n "caffe2op-hardsigmoid") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "12kkmf74z8n5i8nbdil0nmawvn9a01vjcp3c7jnzx9w051vv76mm")))

(define-public crate-caffe2op-hardsigmoid-0.1.5-alpha.0 (c (n "caffe2op-hardsigmoid") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1lasg332fz11gpbnn0fi1c0gk6x6kd5y0wm1fni3z79wvr4phqq9")))

