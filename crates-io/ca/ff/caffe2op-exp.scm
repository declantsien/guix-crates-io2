(define-module (crates-io ca ff caffe2op-exp) #:use-module (crates-io))

(define-public crate-caffe2op-exp-0.1.4-alpha.0 (c (n "caffe2op-exp") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "13vda3xsc2bwz95sfjgncif5pfafcq8qarqw72b0q00gj5d5724g")))

(define-public crate-caffe2op-exp-0.1.5-alpha.0 (c (n "caffe2op-exp") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0js96c9677kfq67942j1ksngim6hihnfk9rvb2jaj1mg71bnirr0")))

