(define-module (crates-io ca ff caffe2op-softsign) #:use-module (crates-io))

(define-public crate-caffe2op-softsign-0.1.4-alpha.0 (c (n "caffe2op-softsign") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1z47y4iahvxv19fr618pwsr493masqhid5k5694v5sly340pfjq7")))

(define-public crate-caffe2op-softsign-0.1.5-alpha.0 (c (n "caffe2op-softsign") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1hysvfiffszar1gndqyr1ggxwjqr604m9q9p9mr9c1skx5f5mspl")))

