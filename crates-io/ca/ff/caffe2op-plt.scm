(define-module (crates-io ca ff caffe2op-plt) #:use-module (crates-io))

(define-public crate-caffe2op-plt-0.1.4-alpha.0 (c (n "caffe2op-plt") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "11v021n3pqw6wrhwfrji29bvsb0ccypqdqcklffqxn0h8zr3q2sx")))

(define-public crate-caffe2op-plt-0.1.5-alpha.0 (c (n "caffe2op-plt") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0za5q6clx98m4dvjyhh6ncaznjqgxdkfc69p9m6bfapyrjlpllzy")))

