(define-module (crates-io ca ff caffe2op-deform) #:use-module (crates-io))

(define-public crate-caffe2op-deform-0.1.4-alpha.0 (c (n "caffe2op-deform") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "18jqwaqaalg8y2hlk01vg0rzl5c0qlldz050319p12x72rrb87pa")))

(define-public crate-caffe2op-deform-0.1.5-alpha.0 (c (n "caffe2op-deform") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0dxivns0p49ib4s93f5bmg6mzrpqscgxqqx56d4gscrxfvjawmcq")))

