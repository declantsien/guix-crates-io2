(define-module (crates-io ca ff caffe2op-atan) #:use-module (crates-io))

(define-public crate-caffe2op-atan-0.1.3-alpha.0 (c (n "caffe2op-atan") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1nzwxd4hij2c25vrd7hj90m4giyi80ss5m0r96fqbvcwpkysls6i")))

(define-public crate-caffe2op-atan-0.1.4-alpha.0 (c (n "caffe2op-atan") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0mpc4y965vrqx5c390xwx41sx7f23ip4vpdx78aaykihsdc5309p")))

(define-public crate-caffe2op-atan-0.1.5-alpha.0 (c (n "caffe2op-atan") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1dnl519gfjdmw36q287y1r61y7pqhncsc9lxzyrvbmavia40rdri")))

