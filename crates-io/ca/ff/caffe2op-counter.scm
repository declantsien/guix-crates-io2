(define-module (crates-io ca ff caffe2op-counter) #:use-module (crates-io))

(define-public crate-caffe2op-counter-0.1.3-alpha.0 (c (n "caffe2op-counter") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1janwk1qsm8w809g48y6kwjgw1wb832ax6cpv2lpl3dszqbzbxf5")))

(define-public crate-caffe2op-counter-0.1.4-alpha.0 (c (n "caffe2op-counter") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0p7qkz3ljfh9pcb430kfx8ra4c7qx6vlin7w3kv1yhr4irp1s831")))

(define-public crate-caffe2op-counter-0.1.5-alpha.0 (c (n "caffe2op-counter") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "05s79h0vg7wh64y0v1vhhm1n5z1mij3ilrzyjs5z98fbdvbwhlll")))

