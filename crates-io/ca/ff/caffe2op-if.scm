(define-module (crates-io ca ff caffe2op-if) #:use-module (crates-io))

(define-public crate-caffe2op-if-0.1.4-alpha.0 (c (n "caffe2op-if") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1l4lld3yx0zx3k6mwhvvawsyrv4qfrh4hna7g9w7lh580ng60anl")))

(define-public crate-caffe2op-if-0.1.5-alpha.0 (c (n "caffe2op-if") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "048sma7vvlpd3kv4hg2m9dmqh2svxabp5rx85g4x7pgmb4yy0ppl")))

