(define-module (crates-io ca ff caffe2op-cosh) #:use-module (crates-io))

(define-public crate-caffe2op-cosh-0.1.3-alpha.0 (c (n "caffe2op-cosh") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "06df6nc9zgy9aw4jjyz60kfsvfglrhx6lm0iajsw33jc1snhfyx7")))

(define-public crate-caffe2op-cosh-0.1.4-alpha.0 (c (n "caffe2op-cosh") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1v5wkngh833zpdzxl1hg07mn6hr087xq2j21isfmji4cl3kw8j7a")))

(define-public crate-caffe2op-cosh-0.1.5-alpha.0 (c (n "caffe2op-cosh") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0cx8mgqg30p52fvw8phiw2vin22db65r1dxasr0aywqrldxzrwv5")))

