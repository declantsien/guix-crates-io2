(define-module (crates-io ca ff caffe2op-normalize) #:use-module (crates-io))

(define-public crate-caffe2op-normalize-0.1.4-alpha.0 (c (n "caffe2op-normalize") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "16bc3jdn280af43hw2dsmav2ahwbc0xqq7lbvl782axd66pf0f1p")))

(define-public crate-caffe2op-normalize-0.1.5-alpha.0 (c (n "caffe2op-normalize") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1k7z43ka834p3s67sbiql4x0p6pigbk4k3kana5mfshkakdn3hf7")))

