(define-module (crates-io ca ff caffe2op-packsegments) #:use-module (crates-io))

(define-public crate-caffe2op-packsegments-0.1.4-alpha.0 (c (n "caffe2op-packsegments") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "10jggji150f28w28agw38qbmqgf5ki5srrpky6089li188cl9hfm")))

(define-public crate-caffe2op-packsegments-0.1.5-alpha.0 (c (n "caffe2op-packsegments") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0ss7wvb3sp56yq4djzapqcswc27q6rj6ijcrvr6bdf95yg9vxvip")))

