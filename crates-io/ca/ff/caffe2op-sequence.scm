(define-module (crates-io ca ff caffe2op-sequence) #:use-module (crates-io))

(define-public crate-caffe2op-sequence-0.1.4-alpha.0 (c (n "caffe2op-sequence") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "04gbh44w98d8rx86bbkjprm0j83z4z3jqc8a135kyaxnmq5pfiag")))

(define-public crate-caffe2op-sequence-0.1.5-alpha.0 (c (n "caffe2op-sequence") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0giwn5i5546b7dzb4gfxrmz8hqx5426mmp1dyjgkzh9rcxcj7051")))

