(define-module (crates-io ca ff caffe2op-activation) #:use-module (crates-io))

(define-public crate-caffe2op-activation-0.1.3-alpha.0 (c (n "caffe2op-activation") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1v9ld6b3ah9jhrknck41dh62y9c73ad6743wl4gmd595qygk3m47")))

(define-public crate-caffe2op-activation-0.1.4-alpha.0 (c (n "caffe2op-activation") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0zxy60s8w6rvi2l77hjzkvs9vpxqygsqhlz2d9yyx75y28nnhf5a")))

(define-public crate-caffe2op-activation-0.1.5-alpha.0 (c (n "caffe2op-activation") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1avwhd8zpnk11b3awl6pichmyjz97hqsrbmysmipdcadd4yxbyx7")))

