(define-module (crates-io ca ff caffe2op-marginrank) #:use-module (crates-io))

(define-public crate-caffe2op-marginrank-0.1.4-alpha.0 (c (n "caffe2op-marginrank") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0x8gfwn0x8vp8d1bpmdkjw46mh77hl43sws63j60dc4qp8qbg663")))

(define-public crate-caffe2op-marginrank-0.1.5-alpha.0 (c (n "caffe2op-marginrank") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0g0w0f3yr87jlp8kcla1cffjx0ag8lzb7l6c9fv98gjirqri9aza")))

