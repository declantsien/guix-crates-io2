(define-module (crates-io ca ff caffe2op-affine) #:use-module (crates-io))

(define-public crate-caffe2op-affine-0.1.3-alpha.0 (c (n "caffe2op-affine") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1rhlc09a08zqflzgxb1aj3bvwyc0vjjbamv0z5hwmlz51fi07lhw")))

(define-public crate-caffe2op-affine-0.1.4-alpha.0 (c (n "caffe2op-affine") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "175i6h09z9nsy8akz45saggwi05kfkj5v4x1hww86v8l8ic1k9zm")))

(define-public crate-caffe2op-affine-0.1.5-alpha.0 (c (n "caffe2op-affine") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "10rp4d4xp4940p4ivx9gf8dyqdyqaky5gcpcn00bbmxadbn9mcpk")))

