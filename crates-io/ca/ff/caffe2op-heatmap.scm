(define-module (crates-io ca ff caffe2op-heatmap) #:use-module (crates-io))

(define-public crate-caffe2op-heatmap-0.1.4-alpha.0 (c (n "caffe2op-heatmap") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "16qpk5ik31c182n3kyjh3xr9nbgn8b297sizffiqmisvfacxlwcg")))

(define-public crate-caffe2op-heatmap-0.1.5-alpha.0 (c (n "caffe2op-heatmap") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ppbjpxhmf6zkf3d005mc5yq0b46i8nb53ds9sib0njdigq60bd3")))

