(define-module (crates-io ca ff caffe2op-rankloss) #:use-module (crates-io))

(define-public crate-caffe2op-rankloss-0.1.4-alpha.0 (c (n "caffe2op-rankloss") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0b0765vf8r2h9nvz64dwcv17f0cqmsxqfn4vl6isw7zllj5rsaqs")))

(define-public crate-caffe2op-rankloss-0.1.5-alpha.0 (c (n "caffe2op-rankloss") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1j6ianbn6w6f679m39vkkf0019zw7awgksfzwfcmnbn9cl5v6i83")))

