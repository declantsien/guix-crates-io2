(define-module (crates-io ca ff caffe2op-weightedmultisample) #:use-module (crates-io))

(define-public crate-caffe2op-weightedmultisample-0.1.4-alpha.0 (c (n "caffe2op-weightedmultisample") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "00ic298qrygzs30a59jcvmmymsl7np0xv9r7v5kig81yhyzyvyaw")))

(define-public crate-caffe2op-weightedmultisample-0.1.5-alpha.0 (c (n "caffe2op-weightedmultisample") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0wd7ajqb7avllx832k9al90lqh7wim9ndzlkv5ivzzj2hi754xd9")))

