(define-module (crates-io ca ff caffe2op-expand) #:use-module (crates-io))

(define-public crate-caffe2op-expand-0.1.3-alpha.0 (c (n "caffe2op-expand") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1mj1h16w0pprn238nsfrrsza3c9h6kvzixcrd0klknnc50d86621")))

(define-public crate-caffe2op-expand-0.1.4-alpha.0 (c (n "caffe2op-expand") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "12yz6bn7vlmr1xhkixaj9vag0m49sk0kzqq4qf9sh9zijnv4s8mc")))

(define-public crate-caffe2op-expand-0.1.5-alpha.0 (c (n "caffe2op-expand") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "00skgdan7p8qlvcmlq7vymwqd6yr8n3bcwrigzmw5n330d9nbsgm")))

