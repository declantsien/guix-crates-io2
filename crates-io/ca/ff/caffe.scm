(define-module (crates-io ca ff caffe) #:use-module (crates-io))

(define-public crate-caffe-0.1.0 (c (n "caffe") (v "0.1.0") (d (list (d (n "bindgen_plugin") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0xs82fwfv0q50gjpjs8p9fvx20x4l24lpd0ppyfiws8b8r3yv4aw")))

