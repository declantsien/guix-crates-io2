(define-module (crates-io ca ff caffe2op-sqr) #:use-module (crates-io))

(define-public crate-caffe2op-sqr-0.1.4-alpha.0 (c (n "caffe2op-sqr") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "08f4bi19alcqcbdiq7xb9yms1hhfhg2jp70jcvja9ilwdbj62r87")))

(define-public crate-caffe2op-sqr-0.1.5-alpha.0 (c (n "caffe2op-sqr") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0a46xgwmba7a7s96fjz5lfvnsd8ywlp3hr35ikbx7rlzr96spslv")))

