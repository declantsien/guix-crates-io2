(define-module (crates-io ca ff caffe2op-scale) #:use-module (crates-io))

(define-public crate-caffe2op-scale-0.1.4-alpha.0 (c (n "caffe2op-scale") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "08ay6zl4mkrc405n6kkdpwfhwyzrxzryg9jhy23wk4h4d07d534z")))

(define-public crate-caffe2op-scale-0.1.5-alpha.0 (c (n "caffe2op-scale") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1fd5d38r9gsw3h6x5p4fb0x64q0pd7hsk5lr8ssdd76mplih0y4d")))

