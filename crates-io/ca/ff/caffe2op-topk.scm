(define-module (crates-io ca ff caffe2op-topk) #:use-module (crates-io))

(define-public crate-caffe2op-topk-0.1.4-alpha.0 (c (n "caffe2op-topk") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "108g3rhk1z2gk4xryjqq39n2rp60mbyp556iqwcl8r2lckaa0fpn")))

(define-public crate-caffe2op-topk-0.1.5-alpha.0 (c (n "caffe2op-topk") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "078875lqa8hyr108pyhk749288v2y248344r0zdkj04d41fwkmr7")))

