(define-module (crates-io ca ff caffe2op-map) #:use-module (crates-io))

(define-public crate-caffe2op-map-0.1.4-alpha.0 (c (n "caffe2op-map") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "16i6v2j8da3vmphjafxd1imwd0ahbvk0n4r202wwvwv33cspgzbf")))

(define-public crate-caffe2op-map-0.1.5-alpha.0 (c (n "caffe2op-map") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "10yarkb8f4g4232pbq8nqhvmlaxzny7rxqg1nb8g3v1pmkzyjv5k")))

