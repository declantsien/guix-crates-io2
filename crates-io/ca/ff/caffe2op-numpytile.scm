(define-module (crates-io ca ff caffe2op-numpytile) #:use-module (crates-io))

(define-public crate-caffe2op-numpytile-0.1.4-alpha.0 (c (n "caffe2op-numpytile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0x8i5y3vp6l00p5sm12470aibxnbqcz3s2ix74sc20qwiiyd8afk")))

(define-public crate-caffe2op-numpytile-0.1.5-alpha.0 (c (n "caffe2op-numpytile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1nwb7imjfqr8rgybrzslmyqf2si1mhj0rj2jcms4p4bphgvl6wq7")))

