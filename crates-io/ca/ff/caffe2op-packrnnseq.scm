(define-module (crates-io ca ff caffe2op-packrnnseq) #:use-module (crates-io))

(define-public crate-caffe2op-packrnnseq-0.1.4-alpha.0 (c (n "caffe2op-packrnnseq") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "114nrkni1k150hinjzvbqh4if895zc15gy0rmman8gcwaal2rzvj")))

(define-public crate-caffe2op-packrnnseq-0.1.5-alpha.0 (c (n "caffe2op-packrnnseq") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "17i35l2wxwagr2bdppcmy34snck3329w15vcwll4swi3ijj0221c")))

