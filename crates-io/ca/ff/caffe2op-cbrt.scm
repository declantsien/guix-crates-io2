(define-module (crates-io ca ff caffe2op-cbrt) #:use-module (crates-io))

(define-public crate-caffe2op-cbrt-0.1.3-alpha.0 (c (n "caffe2op-cbrt") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0rdnkils235n7vayhx0pxa373p1bfrj0qdyjvpfzhbrm768p2g7k")))

(define-public crate-caffe2op-cbrt-0.1.4-alpha.0 (c (n "caffe2op-cbrt") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1lg1x2nlygwj5hm6y6lpbwybacnki2nww4j55v0jd3lcavlzvak7")))

(define-public crate-caffe2op-cbrt-0.1.5-alpha.0 (c (n "caffe2op-cbrt") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "12n6hn7vzcs67vbdy9wb0cj1p2a35i4h5wbmkrgcpysbb0szjxyl")))

