(define-module (crates-io ca ff caffe2op-halffloat) #:use-module (crates-io))

(define-public crate-caffe2op-halffloat-0.1.4-alpha.0 (c (n "caffe2op-halffloat") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "170jhlcc00r1lvw76q625zdhnd69vag4qjddh5sjqz529f2g7314")))

(define-public crate-caffe2op-halffloat-0.1.5-alpha.0 (c (n "caffe2op-halffloat") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "198pvsm0m7hjms4zd07gzmnhv8w52x2iv6j2h9pcq8n15sy9kcxi")))

