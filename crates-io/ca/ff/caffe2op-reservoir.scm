(define-module (crates-io ca ff caffe2op-reservoir) #:use-module (crates-io))

(define-public crate-caffe2op-reservoir-0.1.4-alpha.0 (c (n "caffe2op-reservoir") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "169wjzwf7yvkqq4bkp2pwjaf25m4sg9x8v4fh9v0w17ddic5kql9")))

(define-public crate-caffe2op-reservoir-0.1.5-alpha.0 (c (n "caffe2op-reservoir") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "13slaa993lw9s3aawk7a2q7a9r062kv8cjxg39awdmw92wx9mrry")))

