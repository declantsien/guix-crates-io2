(define-module (crates-io ca ff caffe2op-bbox) #:use-module (crates-io))

(define-public crate-caffe2op-bbox-0.1.3-alpha.0 (c (n "caffe2op-bbox") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "17xsppgjhp5s7rf7hahbgq4basgqjpzjr86y75gwhj0lizi21vcj")))

(define-public crate-caffe2op-bbox-0.1.4-alpha.0 (c (n "caffe2op-bbox") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0ad82q55z2yy5abjc79645v0i6w47zab47qx00ddya1z1kzxd9n6")))

(define-public crate-caffe2op-bbox-0.1.5-alpha.0 (c (n "caffe2op-bbox") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0gpvhfbmk80kzy55q64b9czdpciryrvzzh3778hb0ia4nr0dnqbg")))

