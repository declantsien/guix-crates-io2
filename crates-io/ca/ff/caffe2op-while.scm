(define-module (crates-io ca ff caffe2op-while) #:use-module (crates-io))

(define-public crate-caffe2op-while-0.1.4-alpha.0 (c (n "caffe2op-while") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0r937b2yzslzpg2i5s0xvfarqrbyyshhcpzs5bz80scrbvfip0yd")))

(define-public crate-caffe2op-while-0.1.5-alpha.0 (c (n "caffe2op-while") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0bi615s3kxqd1842ixc4v1xmdvhmrj3a17fkf93w6acvfs0ywd2p")))

