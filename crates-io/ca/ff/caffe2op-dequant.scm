(define-module (crates-io ca ff caffe2op-dequant) #:use-module (crates-io))

(define-public crate-caffe2op-dequant-0.1.4-alpha.0 (c (n "caffe2op-dequant") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0nx8dhz664vl2axdhnwm7m0jqmf83l64yx2ads9igbkfrpqx3zz3")))

(define-public crate-caffe2op-dequant-0.1.5-alpha.0 (c (n "caffe2op-dequant") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "08d42lvz2i983hvirfq1910wf2xk9hc430yafb90bxa635kgl8vh")))

