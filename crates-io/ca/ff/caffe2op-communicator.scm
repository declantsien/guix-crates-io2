(define-module (crates-io ca ff caffe2op-communicator) #:use-module (crates-io))

(define-public crate-caffe2op-communicator-0.1.3-alpha.0 (c (n "caffe2op-communicator") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1w8qx8ifkmvp5jd4y6mv21cz0nnw9y1hj5mjrpxm93gi5acczqj8")))

(define-public crate-caffe2op-communicator-0.1.4-alpha.0 (c (n "caffe2op-communicator") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1dqwv6m665623pzbkdng3vsjzmrc2y1z092234q5xrkvlcr6p1qs")))

(define-public crate-caffe2op-communicator-0.1.5-alpha.0 (c (n "caffe2op-communicator") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0185gsdmk3fynkp5mp19j7jzyl5z7n8ig0267xiqbq0500hf8rbi")))

