(define-module (crates-io ca ff caffe2op-gru) #:use-module (crates-io))

(define-public crate-caffe2op-gru-0.1.4-alpha.0 (c (n "caffe2op-gru") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "07qdlbnsnzbv7wzyaqdjn2kal3f2icsy6qf6j4wqic5wfn1h4yz9")))

(define-public crate-caffe2op-gru-0.1.5-alpha.0 (c (n "caffe2op-gru") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0bcsmp38rvbw06xvr0h64g8hg97yi4dfkf5i1zrsj8vqds3mk96d")))

