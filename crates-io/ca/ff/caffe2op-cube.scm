(define-module (crates-io ca ff caffe2op-cube) #:use-module (crates-io))

(define-public crate-caffe2op-cube-0.1.3-alpha.0 (c (n "caffe2op-cube") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "011mkwbj6hk72d09zl4b9cww36xnq4rz1zkxg1j9d4glzirxf8gr")))

(define-public crate-caffe2op-cube-0.1.4-alpha.0 (c (n "caffe2op-cube") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ippqlnmc4v842m4n7lqwgvzmcx3bas628rvpyvqscia843wfkwa")))

(define-public crate-caffe2op-cube-0.1.5-alpha.0 (c (n "caffe2op-cube") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "12rqmk90qprwmv21zv4x29a537z721hn9irqi3dqz4rq6wjrpvsi")))

