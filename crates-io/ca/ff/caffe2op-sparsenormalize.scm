(define-module (crates-io ca ff caffe2op-sparsenormalize) #:use-module (crates-io))

(define-public crate-caffe2op-sparsenormalize-0.1.4-alpha.0 (c (n "caffe2op-sparsenormalize") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1w7gy2xb4qgqqiy4vhsj4nr4nmjgxfv09dfxkxbh5icvjl98yl7d")))

(define-public crate-caffe2op-sparsenormalize-0.1.5-alpha.0 (c (n "caffe2op-sparsenormalize") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1s817x7n0p31xwwdx8avzkvgrcqfcqp6ps7n8nnhk7ygkjc2a355")))

