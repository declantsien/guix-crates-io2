(define-module (crates-io ca ff caffe2op-loss) #:use-module (crates-io))

(define-public crate-caffe2op-loss-0.1.4-alpha.0 (c (n "caffe2op-loss") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-reduce") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ah8bgp7dl03iss7558dvpabhbh2sfvfh7cv64vbfshsm3wifnv8")))

(define-public crate-caffe2op-loss-0.1.5-alpha.0 (c (n "caffe2op-loss") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-reduce") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1f3xh5s1qb5nyj20zkl00fc0mx2b4d1gjgy81y88jljx2dyjms1s")))

