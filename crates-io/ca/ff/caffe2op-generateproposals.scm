(define-module (crates-io ca ff caffe2op-generateproposals) #:use-module (crates-io))

(define-public crate-caffe2op-generateproposals-0.1.4-alpha.0 (c (n "caffe2op-generateproposals") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1f6rxfcva6sl9zqdlbdh0l6sikkvjygrvh5zkrv0g8xb0g0jcq63")))

(define-public crate-caffe2op-generateproposals-0.1.5-alpha.0 (c (n "caffe2op-generateproposals") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0b595szcd3j25x45aiwg223x0da4g1hnzq9rqll7arcprw68vi1z")))

