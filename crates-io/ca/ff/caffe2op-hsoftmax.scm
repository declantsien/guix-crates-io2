(define-module (crates-io ca ff caffe2op-hsoftmax) #:use-module (crates-io))

(define-public crate-caffe2op-hsoftmax-0.1.4-alpha.0 (c (n "caffe2op-hsoftmax") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0y34vbiv5vkpyh34msz087i7jjjwr7c08vxsnnza0bwls2qh1l6l")))

(define-public crate-caffe2op-hsoftmax-0.1.5-alpha.0 (c (n "caffe2op-hsoftmax") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0r2zkh6miqycgyvg2l2i8jhcq1y4ix6g9qbax6ilyzdg2fl3248n")))

