(define-module (crates-io ca ff caffe2-prof) #:use-module (crates-io))

(define-public crate-caffe2-prof-0.1.3-alpha.0 (c (n "caffe2-prof") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-timer") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "18wjyl6yi8yzfq6ly9m7i5ij81k2zniflh2adxfaqi4mnygyll2w")))

(define-public crate-caffe2-prof-0.1.4-alpha.0 (c (n "caffe2-prof") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-timer") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "05ssvn49mhx7863z43j0g3gm4g025rfjqy2pv4j0nhp2wk68zi3s")))

(define-public crate-caffe2-prof-0.1.5-alpha.0 (c (n "caffe2-prof") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-timer") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1645xl00vwlvrgilfaqw306mx7xi6mg9jcc33msg8f8xrsxfyy63")))

