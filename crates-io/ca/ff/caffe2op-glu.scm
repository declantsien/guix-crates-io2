(define-module (crates-io ca ff caffe2op-glu) #:use-module (crates-io))

(define-public crate-caffe2op-glu-0.1.4-alpha.0 (c (n "caffe2op-glu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0zwgd5vxi2fdj8nlq8m3yaxk0bhay75lmwkd6mj00izgqpc676q3")))

(define-public crate-caffe2op-glu-0.1.5-alpha.0 (c (n "caffe2op-glu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0vq4g61lvmilbd5aspgcvc6fjl7zbm1mx7m2cs4z5palrn0w40r8")))

