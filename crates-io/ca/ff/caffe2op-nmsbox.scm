(define-module (crates-io ca ff caffe2op-nmsbox) #:use-module (crates-io))

(define-public crate-caffe2op-nmsbox-0.1.4-alpha.0 (c (n "caffe2op-nmsbox") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0f5ldsgvzdms7y8h76qfr568rw0a4dla74qpql488f5phi0sr67l")))

(define-public crate-caffe2op-nmsbox-0.1.5-alpha.0 (c (n "caffe2op-nmsbox") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1yc8za6zd1201brxg48j069fjwassc5ym4iqjpw37bhxp23rrfky")))

