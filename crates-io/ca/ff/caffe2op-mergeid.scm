(define-module (crates-io ca ff caffe2op-mergeid) #:use-module (crates-io))

(define-public crate-caffe2op-mergeid-0.1.4-alpha.0 (c (n "caffe2op-mergeid") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0bjv0bl735f2f1pj5ynrlxkyz2gpr8r4vn1v2d44l2wzly2ga97w")))

(define-public crate-caffe2op-mergeid-0.1.5-alpha.0 (c (n "caffe2op-mergeid") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "13lmxaj0i366prmbv0lkyh6f8f0nclli3rhmqhgsiikransiqbvi")))

