(define-module (crates-io ca ff caffe2op-reversepacked) #:use-module (crates-io))

(define-public crate-caffe2op-reversepacked-0.1.4-alpha.0 (c (n "caffe2op-reversepacked") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1536gzvr8bzw066gb3n91l3xrq7ycvzl8bhciiq0j5yaqsy0284v")))

(define-public crate-caffe2op-reversepacked-0.1.5-alpha.0 (c (n "caffe2op-reversepacked") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "18ffmxpmpw0mksn0m1zahhxmli6qbmyj25x36hxrvwy6fxyasq17")))

