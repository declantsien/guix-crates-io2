(define-module (crates-io ca ff caffe2op-densevec) #:use-module (crates-io))

(define-public crate-caffe2op-densevec-0.1.4-alpha.0 (c (n "caffe2op-densevec") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0kr42bcgx557mvrvg2krwi1bzl7iiccchmyqfpny2brmz45ysl3y")))

(define-public crate-caffe2op-densevec-0.1.5-alpha.0 (c (n "caffe2op-densevec") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0v3fihg3s9ryfzr06xwlxh38sk1h1hjd18ribjm9c0z5w80zrj8w")))

