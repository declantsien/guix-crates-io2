(define-module (crates-io ca ff caffe2-plan) #:use-module (crates-io))

(define-public crate-caffe2-plan-0.1.3-alpha.0 (c (n "caffe2-plan") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0nsffriy446way0lmadydy1z66sj7nh41nqi872hib44fk60b47g")))

(define-public crate-caffe2-plan-0.1.4-alpha.0 (c (n "caffe2-plan") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0xaclw3sg4knszh2inm11hp4a6phlrwzq68czg8v50mpk1pi8xwh")))

(define-public crate-caffe2-plan-0.1.5-alpha.0 (c (n "caffe2-plan") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0k49j4z4hqk4cjrfviim9cx4bvp7ylaszjpfwcwkpyl0m53w4m5a")))

