(define-module (crates-io ca ff caffe2op-mean) #:use-module (crates-io))

(define-public crate-caffe2op-mean-0.1.4-alpha.0 (c (n "caffe2op-mean") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0mpzcqnkb752c99apys4fq6iwhmawj3511bcw47mr32ci8b4hpcv")))

(define-public crate-caffe2op-mean-0.1.5-alpha.0 (c (n "caffe2op-mean") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1m6lfz84zk3qpmsrdkj8dbiqhsvdd2rmqs2y24wq78xl81z0yyl0")))

