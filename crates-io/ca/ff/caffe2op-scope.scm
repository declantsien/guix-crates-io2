(define-module (crates-io ca ff caffe2op-scope) #:use-module (crates-io))

(define-public crate-caffe2op-scope-0.1.4-alpha.0 (c (n "caffe2op-scope") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "15kw5v852zg59am09bv8223jvdv99rk8n87m9m6gsbqlf20d0ywi")))

(define-public crate-caffe2op-scope-0.1.5-alpha.0 (c (n "caffe2op-scope") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0yp4dq9xymjdchrxvv4s4jydwgyg4fyryw9dfqdlz7lqp612j4zw")))

