(define-module (crates-io ca ff caffe2op-cast) #:use-module (crates-io))

(define-public crate-caffe2op-cast-0.1.3-alpha.0 (c (n "caffe2op-cast") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1mzdlnm7ijvxbpjr4klicjv6rpr7rq0gwrfpxn12z7q2igkai0am")))

(define-public crate-caffe2op-cast-0.1.4-alpha.0 (c (n "caffe2op-cast") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1w4iz1nmdrkkn171bysvxvyhjakcb0qvj58byq03xsxg0h8n0yqk")))

(define-public crate-caffe2op-cast-0.1.5-alpha.0 (c (n "caffe2op-cast") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "10r9lvk8bq9da4wncpry7fhmhsihy09kx77hgnc1f6p0qv479gmq")))

