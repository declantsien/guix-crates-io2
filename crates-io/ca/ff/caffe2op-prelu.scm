(define-module (crates-io ca ff caffe2op-prelu) #:use-module (crates-io))

(define-public crate-caffe2op-prelu-0.1.4-alpha.0 (c (n "caffe2op-prelu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1p1vr8fi215hs9279kzlpfq5739x2ls287yzbrasvmasr8i79w4q")))

(define-public crate-caffe2op-prelu-0.1.5-alpha.0 (c (n "caffe2op-prelu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0c8kmh5kzd56dpbs89p34n53pwdbqblq1qvc031d06kp4yxqjgbh")))

