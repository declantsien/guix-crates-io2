(define-module (crates-io ca ff caffe2op-ceil) #:use-module (crates-io))

(define-public crate-caffe2op-ceil-0.1.3-alpha.0 (c (n "caffe2op-ceil") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0i4djwjybpmiz8rpmmnk8cf3hlqkapink7ppblxqx6d7bhjpmip7")))

(define-public crate-caffe2op-ceil-0.1.4-alpha.0 (c (n "caffe2op-ceil") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "12y29d85rpy5yhsplrpgr4sv6vjbjxdhfm1cpnv0w5g773mp6z3z")))

(define-public crate-caffe2op-ceil-0.1.5-alpha.0 (c (n "caffe2op-ceil") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0si4hvm0a2aqahb4h23gxd72l2ipvnawc6c42c219wmp26z2529k")))

