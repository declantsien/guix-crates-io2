(define-module (crates-io ca ff caffe2op-rnn) #:use-module (crates-io))

(define-public crate-caffe2op-rnn-0.1.4-alpha.0 (c (n "caffe2op-rnn") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1f09igjcpciq5cv9iqpb461hgjk94wvr10wz87r6hj53h5x7ab2y")))

(define-public crate-caffe2op-rnn-0.1.5-alpha.0 (c (n "caffe2op-rnn") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "13bv3720f3y1zdrk0gsl8rdw0phwx3wh3mbs7l2w9wnh2qvdv64r")))

