(define-module (crates-io ca ff caffe2op-resize) #:use-module (crates-io))

(define-public crate-caffe2op-resize-0.1.3-alpha.0 (c (n "caffe2op-resize") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0qs2v3h42k3kn806yfffmnzwhfxqjd35yyygvp546s42svswny3w")))

(define-public crate-caffe2op-resize-0.1.4-alpha.0 (c (n "caffe2op-resize") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "13akpgpg43jgz5bkb7x434dhf48rav12baxndx2cvfq1br8p0857")))

(define-public crate-caffe2op-resize-0.1.5-alpha.0 (c (n "caffe2op-resize") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1sgab766148zwzkjxlsacj2afrj2n7d32203zwgsm1knh7ijgc60")))

