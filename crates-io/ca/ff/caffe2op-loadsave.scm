(define-module (crates-io ca ff caffe2op-loadsave) #:use-module (crates-io))

(define-public crate-caffe2op-loadsave-0.1.4-alpha.0 (c (n "caffe2op-loadsave") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-db") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1f3rqhjj7mwajvqg1l223h9hsbwfnp08bp1wy7zvmwd5bxsk0n5f")))

(define-public crate-caffe2op-loadsave-0.1.5-alpha.0 (c (n "caffe2op-loadsave") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-db") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0sk67vg9gsrhckfw2iygwq2vyb8zgig82v4cqllpwc0qd3h1l9pm")))

