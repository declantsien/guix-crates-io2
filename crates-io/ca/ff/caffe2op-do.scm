(define-module (crates-io ca ff caffe2op-do) #:use-module (crates-io))

(define-public crate-caffe2op-do-0.1.4-alpha.0 (c (n "caffe2op-do") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "15ca4ljjhdkk6xcwwnjvf27swb5zvllxx8wk8niq2m219b3r9vdp")))

(define-public crate-caffe2op-do-0.1.5-alpha.0 (c (n "caffe2op-do") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0dymr6zg8ilp9zzqhwgnafx1ca7amckgjkxyflsbd2p7fk5yfd77")))

