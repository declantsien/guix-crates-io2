(define-module (crates-io ca ff caffe2op-indexhash) #:use-module (crates-io))

(define-public crate-caffe2op-indexhash-0.1.4-alpha.0 (c (n "caffe2op-indexhash") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "12z2dn3jz9cr8i5nrjsx6g3mcdvnfq7k522zyc7glv7n0frn9npb")))

(define-public crate-caffe2op-indexhash-0.1.5-alpha.0 (c (n "caffe2op-indexhash") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "100j7hrxxikzkq6amjn49pwc91c662ilhifg5jxk0mj0hxxfwvh0")))

