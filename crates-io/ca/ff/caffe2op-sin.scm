(define-module (crates-io ca ff caffe2op-sin) #:use-module (crates-io))

(define-public crate-caffe2op-sin-0.1.4-alpha.0 (c (n "caffe2op-sin") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1aw6r4b699nbg87fa1fx61664gppiljpc2jnvvpszswf5xhvlffl")))

(define-public crate-caffe2op-sin-0.1.5-alpha.0 (c (n "caffe2op-sin") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0zwplg3y2kkfyiyv0y6kcnnpdayv04kw0wxbfdrwq1w78iwd5bz8")))

