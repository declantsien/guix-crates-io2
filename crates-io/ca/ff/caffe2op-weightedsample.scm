(define-module (crates-io ca ff caffe2op-weightedsample) #:use-module (crates-io))

(define-public crate-caffe2op-weightedsample-0.1.4-alpha.0 (c (n "caffe2op-weightedsample") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "05lv4h5d3czm7mgxjr34683j3vl4fkf29krslj5p12j6f7j93abk")))

(define-public crate-caffe2op-weightedsample-0.1.5-alpha.0 (c (n "caffe2op-weightedsample") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0wlprrl9hv1lc77kqqvvil1s9chqpll6gh0g1b8w5pl4v2r3dcvh")))

