(define-module (crates-io ca ff caffe2op-leakyrelu) #:use-module (crates-io))

(define-public crate-caffe2op-leakyrelu-0.1.4-alpha.0 (c (n "caffe2op-leakyrelu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0653cczw2vfxzs3pig2myc97w4zbxhhvwlbjvl0lmrbn0si43i7v")))

(define-public crate-caffe2op-leakyrelu-0.1.5-alpha.0 (c (n "caffe2op-leakyrelu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0rsli407dada9cph0g5in0q5gp6h46k3r332pjjxmylv44ipjvwh")))

