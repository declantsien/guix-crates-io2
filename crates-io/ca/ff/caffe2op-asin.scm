(define-module (crates-io ca ff caffe2op-asin) #:use-module (crates-io))

(define-public crate-caffe2op-asin-0.1.3-alpha.0 (c (n "caffe2op-asin") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1b1p4zmj0pvxdyxkg8ypqbrh2xf271x28f6am4c6cz6y5zgsfh5b")))

(define-public crate-caffe2op-asin-0.1.4-alpha.0 (c (n "caffe2op-asin") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1b0rpsnaz2df7cn3mzx3ifqmwa1hn7ag6f5l1w0x9mylysdd4f0f")))

(define-public crate-caffe2op-asin-0.1.5-alpha.0 (c (n "caffe2op-asin") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0sx90m4agzham53wj6hh83cns3qywgsl3niwd6gn0dfxm2gxfmjd")))

