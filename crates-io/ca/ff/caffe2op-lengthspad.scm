(define-module (crates-io ca ff caffe2op-lengthspad) #:use-module (crates-io))

(define-public crate-caffe2op-lengthspad-0.1.4-alpha.0 (c (n "caffe2op-lengthspad") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1l6110hj1c1ipxq8im4pnhdqinp7rb9mxsxvglq33xpg9gfkz0n0")))

(define-public crate-caffe2op-lengthspad-0.1.5-alpha.0 (c (n "caffe2op-lengthspad") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0m6sdzzd7n9cbf0ji4b05fyz1yi13ig0hl4y1n4dga24460wqdc3")))

