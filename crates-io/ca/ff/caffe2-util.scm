(define-module (crates-io ca ff caffe2-util) #:use-module (crates-io))

(define-public crate-caffe2-util-0.1.3-alpha.0 (c (n "caffe2-util") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-init") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1rbpaajancly35b2dc9psnb2z5sgh6fqb54bnbk8r80ibfyqna30")))

(define-public crate-caffe2-util-0.1.4-alpha.0 (c (n "caffe2-util") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-init") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0hhaizb806rc0m21kvz85nmq8vc9mql0pc1kx7nxgv55vncyay6x")))

(define-public crate-caffe2-util-0.1.5-alpha.0 (c (n "caffe2-util") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-init") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "054mpv4k0zrb3xi62n8w99v7lvp5ap1b05f4zv5wjjqharhqk5g5")))

