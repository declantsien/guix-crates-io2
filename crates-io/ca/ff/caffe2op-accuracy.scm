(define-module (crates-io ca ff caffe2op-accuracy) #:use-module (crates-io))

(define-public crate-caffe2op-accuracy-0.1.3-alpha.0 (c (n "caffe2op-accuracy") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0vxh9csxb4l3n6536i5w8dx0959jvmpdlf9zg112qh868l5g7xkr")))

(define-public crate-caffe2op-accuracy-0.1.4-alpha.0 (c (n "caffe2op-accuracy") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0x587wpldfhnwg3bixnrgp5pjm9z16bj858n4xmfix4csh0r0n0s")))

(define-public crate-caffe2op-accuracy-0.1.5-alpha.0 (c (n "caffe2op-accuracy") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0m0x6vvg3wx4b5019fmyn4savi5riw6bki4xhj6m5ddfc0hgwhsv")))

