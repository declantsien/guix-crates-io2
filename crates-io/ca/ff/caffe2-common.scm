(define-module (crates-io ca ff caffe2-common) #:use-module (crates-io))

(define-public crate-caffe2-common-0.1.3-alpha.0 (c (n "caffe2-common") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "18vv4lrl2rmbp2xy2j9mml8pm0qaf56cs65i88z8x9jmr9bp3lnm")))

(define-public crate-caffe2-common-0.1.4-alpha.0 (c (n "caffe2-common") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0apwf3g4k7rqbfx938p8k89ch292li6paw4wrj6x6xfmi3mpm4y5")))

(define-public crate-caffe2-common-0.1.5-alpha.0 (c (n "caffe2-common") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "12xc7by4rxcm28k7flkm9dcbz76m05p7429x59hyfdxv988dkayh")))

