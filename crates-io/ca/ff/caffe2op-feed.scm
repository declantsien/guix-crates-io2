(define-module (crates-io ca ff caffe2op-feed) #:use-module (crates-io))

(define-public crate-caffe2op-feed-0.1.4-alpha.0 (c (n "caffe2op-feed") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "01h5awxpw86gp4jyj7vzhh73869vl5kzj5lpjkp7w16pi6bi7fkx")))

(define-public crate-caffe2op-feed-0.1.5-alpha.0 (c (n "caffe2op-feed") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1z2x321bpj4a91pgs163a4h47glnpk9w4g32picds4qz5jxzik3q")))

