(define-module (crates-io ca ff caffe2op-tanh) #:use-module (crates-io))

(define-public crate-caffe2op-tanh-0.1.4-alpha.0 (c (n "caffe2op-tanh") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1k5ica09jvdm8bmqwj8ds5pcp80ff8fhlrs0pih96xsnd18h0rca")))

