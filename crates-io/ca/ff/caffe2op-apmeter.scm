(define-module (crates-io ca ff caffe2op-apmeter) #:use-module (crates-io))

(define-public crate-caffe2op-apmeter-0.1.3-alpha.0 (c (n "caffe2op-apmeter") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1jwvxhvwg83wgklzjgzzdb6fv423331szb87islzjw1z52zbhdyg")))

(define-public crate-caffe2op-apmeter-0.1.4-alpha.0 (c (n "caffe2op-apmeter") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1wnc4j3im5fw13sl9h0dw91r0dzf6ikw96h87f7rbf7sf9w2j3r4")))

(define-public crate-caffe2op-apmeter-0.1.5-alpha.0 (c (n "caffe2op-apmeter") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0h2b2g6q8m82h15aiyhj3q4n1rqdg1rq7zii1nm64kyqhp1f40mj")))

