(define-module (crates-io ca ff caffe2op-im2col) #:use-module (crates-io))

(define-public crate-caffe2op-im2col-0.1.4-alpha.0 (c (n "caffe2op-im2col") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "17j3n7lc2df1rwxmwy0awfa3wc4p1m695yjhx5h6jghz81sl78xq")))

(define-public crate-caffe2op-im2col-0.1.5-alpha.0 (c (n "caffe2op-im2col") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0irwksm317n97iwqplwpp9075vh28y0pqq3d6grysz081cpd3nxy")))

