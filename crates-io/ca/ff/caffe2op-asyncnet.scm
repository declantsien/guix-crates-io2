(define-module (crates-io ca ff caffe2op-asyncnet) #:use-module (crates-io))

(define-public crate-caffe2op-asyncnet-0.1.3-alpha.0 (c (n "caffe2op-asyncnet") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1s5s2jijj0mp64qd31kzmblbybykbr6amc1i780qsyh50flg2qvv")))

(define-public crate-caffe2op-asyncnet-0.1.4-alpha.0 (c (n "caffe2op-asyncnet") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "14mxr9iydpl6a4xcqc9jlsxifxmri9615wxzkj9a3qw5gjzx7saj")))

(define-public crate-caffe2op-asyncnet-0.1.5-alpha.0 (c (n "caffe2op-asyncnet") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "04rlc4hvk85gp3jdga0j5yfm5754vs0h6c2sypxc1v38s1j1pi6l")))

