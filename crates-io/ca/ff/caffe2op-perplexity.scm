(define-module (crates-io ca ff caffe2op-perplexity) #:use-module (crates-io))

(define-public crate-caffe2op-perplexity-0.1.4-alpha.0 (c (n "caffe2op-perplexity") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1h8xkna64ax6q1lafrwyc98vidyx6958j49r4h7d6xfa7vzb0n3c")))

(define-public crate-caffe2op-perplexity-0.1.5-alpha.0 (c (n "caffe2op-perplexity") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0zcqi88if0agdf190pm0a09rirgd9p2wam360zafl8abq7qz6kap")))

