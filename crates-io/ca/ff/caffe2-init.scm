(define-module (crates-io ca ff caffe2-init) #:use-module (crates-io))

(define-public crate-caffe2-init-0.1.3-alpha.0 (c (n "caffe2-init") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xx9z7xf6s6zlzah4gxib3ca5j6jvjkdy8smzrsciii902c4wlci")))

(define-public crate-caffe2-init-0.1.4-alpha.0 (c (n "caffe2-init") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "03w0ck2rq1ciaz95kx8ywnwqnxyf98n2lxc1zm1hshhqkpzz7fgi")))

(define-public crate-caffe2-init-0.1.5-alpha.0 (c (n "caffe2-init") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "106bf3zdq8wxw6sb5qkbrf3zfsvd135svsjszz521r3x5snxkd29")))

