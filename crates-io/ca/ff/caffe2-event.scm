(define-module (crates-io ca ff caffe2-event) #:use-module (crates-io))

(define-public crate-caffe2-event-0.1.3-alpha.0 (c (n "caffe2-event") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1bdqvk0q66xasin4fpdwqh08qjw2cg5cgs5ggfay360zb02ijgzf")))

(define-public crate-caffe2-event-0.1.4-alpha.0 (c (n "caffe2-event") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0b1qmr2jkwpghl27mghlc6k5qz60snk45sy6v45hf4gw6skk4h32")))

(define-public crate-caffe2-event-0.1.5-alpha.0 (c (n "caffe2-event") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0jgi19k42dc4qiinp9y5jswm5pin27c876rzgy1g31pncwxx05vl")))

