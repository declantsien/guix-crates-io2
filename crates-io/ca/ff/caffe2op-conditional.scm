(define-module (crates-io ca ff caffe2op-conditional) #:use-module (crates-io))

(define-public crate-caffe2op-conditional-0.1.3-alpha.0 (c (n "caffe2op-conditional") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "02f6z4cdpiy5cmgszgf3zn4kbh48dxxs1c75h233shr7jd42nava")))

(define-public crate-caffe2op-conditional-0.1.4-alpha.0 (c (n "caffe2op-conditional") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0975bshi9ns6y1bmqsqcfndz7bmx3y1s71xk029mdzr7614hq0gx")))

(define-public crate-caffe2op-conditional-0.1.5-alpha.0 (c (n "caffe2op-conditional") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1aaw1rdrqh2iij40j9ax66zis80yjjfca4q7mnrf2hclz8pqj6cr")))

