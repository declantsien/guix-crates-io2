(define-module (crates-io ca ff caffe2op-sigmoid) #:use-module (crates-io))

(define-public crate-caffe2op-sigmoid-0.1.4-alpha.0 (c (n "caffe2op-sigmoid") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1hhncfsmd57q5zsl8mxdxb5yxx0gi0raa6c6m0pdqc4v5ck2kr16")))

(define-public crate-caffe2op-sigmoid-0.1.5-alpha.0 (c (n "caffe2op-sigmoid") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1lcgsjw4ywdzda9sxcz2jnf2x2piihfm9y3k6iwwb55phlxjgpa4")))

