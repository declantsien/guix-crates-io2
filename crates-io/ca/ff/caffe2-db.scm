(define-module (crates-io ca ff caffe2-db) #:use-module (crates-io))

(define-public crate-caffe2-db-0.1.3-alpha.0 (c (n "caffe2-db") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "10sc0vbigicph0giz2jhx0ya9m4mm8k5fghzm5ybp3k7p6fgag0b")))

(define-public crate-caffe2-db-0.1.4-alpha.0 (c (n "caffe2-db") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1csyr92bhcp4v1f4z8fpqiqsmkhrxvl5mr1fbfyjfwmc5k11zqz2")))

(define-public crate-caffe2-db-0.1.5-alpha.0 (c (n "caffe2-db") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-serde") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "17d5v3qm1k5h2na3kzkdq42nml0js840bszcjhj2awgh13wj6cgv")))

