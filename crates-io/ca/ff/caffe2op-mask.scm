(define-module (crates-io ca ff caffe2op-mask) #:use-module (crates-io))

(define-public crate-caffe2op-mask-0.1.4-alpha.0 (c (n "caffe2op-mask") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1minaplnqvfc74sh2rn3x98qxfbq78bnwxz8n4c6xfxkrg7vmpbx")))

(define-public crate-caffe2op-mask-0.1.5-alpha.0 (c (n "caffe2op-mask") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0casfr1q6kqvhkp56ld8fy42y9qv269hck9phn9flpbswaiy5wjc")))

