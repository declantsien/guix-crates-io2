(define-module (crates-io ca ff caffe2op-logit) #:use-module (crates-io))

(define-public crate-caffe2op-logit-0.1.4-alpha.0 (c (n "caffe2op-logit") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-elementwise") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "05h1lz18h9xax2knds9zbq5ybzcpdrcamgdbkjfqygnlhpjkq373")))

(define-public crate-caffe2op-logit-0.1.5-alpha.0 (c (n "caffe2op-logit") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-elementwise") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ahn6w6dr6q6rv7rcb6rv8pqw8cizl7kqkjxr8qd83fbyb2wxiva")))

