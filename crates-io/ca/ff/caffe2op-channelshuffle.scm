(define-module (crates-io ca ff caffe2op-channelshuffle) #:use-module (crates-io))

(define-public crate-caffe2op-channelshuffle-0.1.3-alpha.0 (c (n "caffe2op-channelshuffle") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0n75h3x2szvhngqjsfsgfdi22r921i7p02a2y8fc1pd3dsjng81v")))

(define-public crate-caffe2op-channelshuffle-0.1.4-alpha.0 (c (n "caffe2op-channelshuffle") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0s0a92wwkk1nx95f0ccn04q4x3j6fqk2a140wppgdk0683fy194q")))

(define-public crate-caffe2op-channelshuffle-0.1.5-alpha.0 (c (n "caffe2op-channelshuffle") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1zskqv9gm2zbl15kblbapbibblyhwyyj29z6lhd30l31bl7mcd9k")))

