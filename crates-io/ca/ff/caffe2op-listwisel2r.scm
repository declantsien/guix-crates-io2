(define-module (crates-io ca ff caffe2op-listwisel2r) #:use-module (crates-io))

(define-public crate-caffe2op-listwisel2r-0.1.4-alpha.0 (c (n "caffe2op-listwisel2r") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ry8viy6sdl7gxy6ayysk4y6kczfddwf34l4cbj0vj3ng1d991rz")))

(define-public crate-caffe2op-listwisel2r-0.1.5-alpha.0 (c (n "caffe2op-listwisel2r") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0wzjh55qlyhpwv6911hp2xrkxxnyx3jm076s47gww2p4sgry7i9d")))

