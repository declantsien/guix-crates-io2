(define-module (crates-io ca ff caffe2op-reciprocal) #:use-module (crates-io))

(define-public crate-caffe2op-reciprocal-0.1.4-alpha.0 (c (n "caffe2op-reciprocal") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1x8milmgn165w0dl5d4xsdgg5n5pd4g9i3wwkjl2iwc3rzmhcy0j")))

(define-public crate-caffe2op-reciprocal-0.1.5-alpha.0 (c (n "caffe2op-reciprocal") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1s2v3d4jalswnbrpn4csfssgvzjy62lk6vwq4bc0215sq5s6wfvm")))

