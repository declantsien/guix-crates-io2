(define-module (crates-io ca ff caffe2op-lengthsreduce) #:use-module (crates-io))

(define-public crate-caffe2op-lengthsreduce-0.1.4-alpha.0 (c (n "caffe2op-lengthsreduce") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-reduce") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-segmentreduction") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0srv3d5vwd0myhk577cgjpq5nycdhqld1zda95nfzvq0w92c59g1")))

(define-public crate-caffe2op-lengthsreduce-0.1.5-alpha.0 (c (n "caffe2op-lengthsreduce") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-reduce") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-segmentreduction") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "042kca8v377mjij378bzmz6n96ydlc4g0dly0lggfpk8v5ngflmc")))

