(define-module (crates-io ca ff caffe2op-reshape) #:use-module (crates-io))

(define-public crate-caffe2op-reshape-0.1.4-alpha.0 (c (n "caffe2op-reshape") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1nfxyhmcprqgc1ymbpyydha6nkxspx3l1w65a1shclwzbsmvm4cl")))

(define-public crate-caffe2op-reshape-0.1.5-alpha.0 (c (n "caffe2op-reshape") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1szkcrsc70fpwrbl5axj2qscdd65ndvbz2mxcc57m2sbrivd5qg4")))

