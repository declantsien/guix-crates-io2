(define-module (crates-io ca ff caffe2op-sparsedropout) #:use-module (crates-io))

(define-public crate-caffe2op-sparsedropout-0.1.4-alpha.0 (c (n "caffe2op-sparsedropout") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "06mwcpbf2a04grrz4dakqdzz7pjc5cb3pji4832ic1g7ksn20ghp")))

(define-public crate-caffe2op-sparsedropout-0.1.5-alpha.0 (c (n "caffe2op-sparsedropout") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1za9vdlqps05yplwns3chk28j3140yhbpghq7dm5jxkgw0y4q8yp")))

