(define-module (crates-io ca ff caffe2op-stats) #:use-module (crates-io))

(define-public crate-caffe2op-stats-0.1.4-alpha.0 (c (n "caffe2op-stats") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "13frp44j756w6hs4pbp5x88b4zk42gbn3aw1aplymwqxfl79s7xm")))

(define-public crate-caffe2op-stats-0.1.5-alpha.0 (c (n "caffe2op-stats") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ppj0imb0ngii2lm1af3dja0wman0m8lncs4xy5mlksrqfbx3yyr")))

