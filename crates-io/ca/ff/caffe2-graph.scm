(define-module (crates-io ca ff caffe2-graph) #:use-module (crates-io))

(define-public crate-caffe2-graph-0.1.3-alpha.0 (c (n "caffe2-graph") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0v3gkj34xrqi12n5dcys3f2i14va4aixmcpw1xiqh3wsld6a68a6")))

(define-public crate-caffe2-graph-0.1.4-alpha.0 (c (n "caffe2-graph") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "03d65gp2bqvl764zn9b8ghjk8rjcfq8bdah5wjmj4kvqjwfsh7jm")))

(define-public crate-caffe2-graph-0.1.5-alpha.0 (c (n "caffe2-graph") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0qlzwszn5vznhiz7z6n5zqg4jlwzhji6vbfjx6fa21i5hjl7d1my")))

