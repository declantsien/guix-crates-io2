(define-module (crates-io ca ff caffe2op-spacebatch) #:use-module (crates-io))

(define-public crate-caffe2op-spacebatch-0.1.4-alpha.0 (c (n "caffe2op-spacebatch") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0s6gp7ca0lvqyazhci5jm0gk548lcc71hvkqn2v7l9bfqxpnf5g7")))

(define-public crate-caffe2op-spacebatch-0.1.5-alpha.0 (c (n "caffe2op-spacebatch") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0s65mxp69wir8dvla3dbxkfxzmgn2sjzb91033b6za6vba5m897n")))

