(define-module (crates-io ca ff caffe2op-accum) #:use-module (crates-io))

(define-public crate-caffe2op-accum-0.1.3-alpha.0 (c (n "caffe2op-accum") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "02vls57qq5y1fj6dzx2kw7k22vwzb29fc2fq7wsch15wy9yv0gvm")))

(define-public crate-caffe2op-accum-0.1.4-alpha.0 (c (n "caffe2op-accum") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1k78r2bz03qlg7mc5p1var1rzibzb37vxk9cmkbgdwwxm4a1nhir")))

(define-public crate-caffe2op-accum-0.1.5-alpha.0 (c (n "caffe2op-accum") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1mmbf9zifq2v993d9bf6fc8dbdhzqa2q5q38kid5i2kjybqpd12d")))

