(define-module (crates-io ca ff caffe2op-sqrt) #:use-module (crates-io))

(define-public crate-caffe2op-sqrt-0.1.4-alpha.0 (c (n "caffe2op-sqrt") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0rcbqydn0rbwa2il24cv06x9s38jrqy4myzyf5rql9zd9phapvwn")))

(define-public crate-caffe2op-sqrt-0.1.5-alpha.0 (c (n "caffe2op-sqrt") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "073zbcl8g0mk9y5vs0mj24lay5mw3y8fysgwkkhf19fvh497vdgx")))

