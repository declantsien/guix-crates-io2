(define-module (crates-io ca ff caffe2-c10) #:use-module (crates-io))

(define-public crate-caffe2-c10-0.1.3-alpha.0 (c (n "caffe2-c10") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-derive") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0nx351axrsh2xk0wy4pxizmgakbr6fvnfjz4236qvpmh68jrxigr")))

(define-public crate-caffe2-c10-0.1.4-alpha.0 (c (n "caffe2-c10") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-derive") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1r5id3lv6g2840bhd3xsr1dn426y8c7nff9zgya0xli841z9xfz9")))

(define-public crate-caffe2-c10-0.1.5-alpha.0 (c (n "caffe2-c10") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-derive") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "03h4msn11irvsjg3sd3b6qjp24l5w1cvll4vbihiwp989xy2pr5v")))

