(define-module (crates-io ca ff caffe2-export) #:use-module (crates-io))

(define-public crate-caffe2-export-0.1.3-alpha.0 (c (n "caffe2-export") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1yhh1zmyydncpjjz36qam6lj9x8khsvv5hfd038yjyrv1ady3hi5")))

(define-public crate-caffe2-export-0.1.4-alpha.0 (c (n "caffe2-export") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1knparrhy9pgalzdgmzvy7a57dycc4qdp9sq57fd1jqgxfxl7cdl")))

(define-public crate-caffe2-export-0.1.5-alpha.0 (c (n "caffe2-export") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0yms855q35nkn67gjd7912rvb4gv1xph2sfp6bkzl71267sq86r2")))

