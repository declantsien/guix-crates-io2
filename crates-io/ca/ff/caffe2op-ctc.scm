(define-module (crates-io ca ff caffe2op-ctc) #:use-module (crates-io))

(define-public crate-caffe2op-ctc-0.1.3-alpha.0 (c (n "caffe2op-ctc") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1p9ksvc7hvyj7wv792y3xax9mb8kabhgy77x9vfqnahpqrswp2h6")))

(define-public crate-caffe2op-ctc-0.1.4-alpha.0 (c (n "caffe2op-ctc") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "007y33z17akvv2fsaqn9bz3aw5zgan634mvqm3vfrvrvp1a4bffw")))

(define-public crate-caffe2op-ctc-0.1.5-alpha.0 (c (n "caffe2op-ctc") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "140v66h3sri3fhpwwk2151gmmlma9nh6mpl7lrl2mdcmpxaxbjmp")))

