(define-module (crates-io ca ff caffe2op-percentile) #:use-module (crates-io))

(define-public crate-caffe2op-percentile-0.1.4-alpha.0 (c (n "caffe2op-percentile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0dap32pys4rk2j672vcrdrfd3mpkkamr4bn969vjhfviy5p40js9")))

(define-public crate-caffe2op-percentile-0.1.5-alpha.0 (c (n "caffe2op-percentile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "142i4m3h3r88944ch2yl4cww6cj1m5sri6rzl7rjzinrz9civ56i")))

