(define-module (crates-io ca ff caffe2op-dropout) #:use-module (crates-io))

(define-public crate-caffe2op-dropout-0.1.4-alpha.0 (c (n "caffe2op-dropout") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1l0fgi4bs6yl3qyk5z0mcvfd0i979l60aakmgi3wqmgxkjjbb5km")))

(define-public crate-caffe2op-dropout-0.1.5-alpha.0 (c (n "caffe2op-dropout") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "18cz1hp5lrb754lh6vdj46lm11a9b8cfr5kkxjxrdk4ip6k3w053")))

