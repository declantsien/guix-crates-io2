(define-module (crates-io ca ff caffe2op-selu) #:use-module (crates-io))

(define-public crate-caffe2op-selu-0.1.4-alpha.0 (c (n "caffe2op-selu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0cfdxpk7dza7pvkcl2nqni4b0w4pcxw0z8qabzn6yxblnzwcs5sw")))

(define-public crate-caffe2op-selu-0.1.5-alpha.0 (c (n "caffe2op-selu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1f3zxj903sxggx31canvjy89hsfrcvl2gai5ci55dbqmfsac0j2d")))

