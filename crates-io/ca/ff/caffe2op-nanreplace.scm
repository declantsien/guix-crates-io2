(define-module (crates-io ca ff caffe2op-nanreplace) #:use-module (crates-io))

(define-public crate-caffe2op-nanreplace-0.1.4-alpha.0 (c (n "caffe2op-nanreplace") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "134q46khnsn9jqv5ayyqa928g7mad0sgj6n28j8721k095rp1dwr")))

(define-public crate-caffe2op-nanreplace-0.1.5-alpha.0 (c (n "caffe2op-nanreplace") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0sddy8k6px6nbivlcbc66lgbyahvgmw9cc3dvvxmpd5dmyv49mgy")))

