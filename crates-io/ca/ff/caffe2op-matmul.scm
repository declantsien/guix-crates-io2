(define-module (crates-io ca ff caffe2op-matmul) #:use-module (crates-io))

(define-public crate-caffe2op-matmul-0.1.4-alpha.0 (c (n "caffe2op-matmul") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1al0ihkyw5nm9j1127q9idqrq268kv4x95y73z4dr78nmfggylrl")))

(define-public crate-caffe2op-matmul-0.1.5-alpha.0 (c (n "caffe2op-matmul") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "12pkydqrvn5z7hvvxbki72346bcy171r7ykr7drf294azjj83rmf")))

