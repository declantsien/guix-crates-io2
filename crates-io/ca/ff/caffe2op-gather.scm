(define-module (crates-io ca ff caffe2op-gather) #:use-module (crates-io))

(define-public crate-caffe2op-gather-0.1.3-alpha.0 (c (n "caffe2op-gather") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "030lxr1mbq3f9l4a4f45azlam4q6lckcsfdlwkn61cj6vlmxmyvl")))

(define-public crate-caffe2op-gather-0.1.4-alpha.0 (c (n "caffe2op-gather") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0zqwvikad04b5xgg083cjv9rrm6xhz0mqvsaz6vmk3f298h86a34")))

(define-public crate-caffe2op-gather-0.1.5-alpha.0 (c (n "caffe2op-gather") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0mlrkjm0sm06wyc889cqz8i7577wac5fif5ih4wpp1a1jyz2ilm4")))

