(define-module (crates-io ca ff caffe2op-clip) #:use-module (crates-io))

(define-public crate-caffe2op-clip-0.1.3-alpha.0 (c (n "caffe2op-clip") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xj90fj854n00wy5q51x9lz7ig5p7gqxj1wnyg0c1hrbndl00lv4")))

(define-public crate-caffe2op-clip-0.1.4-alpha.0 (c (n "caffe2op-clip") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "12a9wz722mcavpw2blnkkxbp15fnkxj8qjlcjaaad5in905pjrhy")))

(define-public crate-caffe2op-clip-0.1.5-alpha.0 (c (n "caffe2op-clip") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0b3f4sxl4may3ff6m5h607nm9m48pglnr58akg3kzjiszx7mkrs9")))

