(define-module (crates-io ca ff caffe2op-bisect) #:use-module (crates-io))

(define-public crate-caffe2op-bisect-0.1.3-alpha.0 (c (n "caffe2op-bisect") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "10n45f9fj92yqpa8n53jilicr0gadibidnknw7zv20x8xwv7cjjr")))

(define-public crate-caffe2op-bisect-0.1.4-alpha.0 (c (n "caffe2op-bisect") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ppw5xkwp5fzvqyj21gy3pyzms57qf32gzrmbwbcgdl1aglr5nkm")))

(define-public crate-caffe2op-bisect-0.1.5-alpha.0 (c (n "caffe2op-bisect") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "19jngz7l7vk8anx1d3hh6dp1hijgjsr5zg5hm27dh4vcw0c0fh31")))

