(define-module (crates-io ca ff caffe2op-lengthstop) #:use-module (crates-io))

(define-public crate-caffe2op-lengthstop-0.1.4-alpha.0 (c (n "caffe2op-lengthstop") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "17dvj0dlq0h5q3l2dbg69dxdd7sx1lv5zypmz7grab09sp8h10m8")))

(define-public crate-caffe2op-lengthstop-0.1.5-alpha.0 (c (n "caffe2op-lengthstop") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "15ijgqhws28jpp5byfk9dwr2gwawf8z30k96r6sci76kj2qiyw8k")))

