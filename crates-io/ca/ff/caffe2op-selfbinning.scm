(define-module (crates-io ca ff caffe2op-selfbinning) #:use-module (crates-io))

(define-public crate-caffe2op-selfbinning-0.1.4-alpha.0 (c (n "caffe2op-selfbinning") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1lqlbgb9albirqqnnfj3q1g5yf5n728knx0a7825f1hgjragjgpx")))

(define-public crate-caffe2op-selfbinning-0.1.5-alpha.0 (c (n "caffe2op-selfbinning") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "04qkann2csafzvx4p57f7k597ngdr3025qq6x3p3bmm7rmpdk5qi")))

