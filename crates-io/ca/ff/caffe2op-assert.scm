(define-module (crates-io ca ff caffe2op-assert) #:use-module (crates-io))

(define-public crate-caffe2op-assert-0.1.3-alpha.0 (c (n "caffe2op-assert") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0vbgyg5dpa4aqg8xhl99w6kr9h19p2a9l7ipj0pph5w4w4f8qzgk")))

(define-public crate-caffe2op-assert-0.1.4-alpha.0 (c (n "caffe2op-assert") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "001c00dggidb47q9ys37vkh30nshqn68q69idrlwgbzq0y55rl2v")))

(define-public crate-caffe2op-assert-0.1.5-alpha.0 (c (n "caffe2op-assert") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0vmhpb32a99dwv4invf66q5kqmkgygzq8y7afby2rwl4xlgg34k4")))

