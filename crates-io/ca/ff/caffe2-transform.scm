(define-module (crates-io ca ff caffe2-transform) #:use-module (crates-io))

(define-public crate-caffe2-transform-0.1.3-alpha.0 (c (n "caffe2-transform") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0aw8z57gmrknysil5wny766mixgsny0m9g25xc0hhz66q077w19f")))

(define-public crate-caffe2-transform-0.1.4-alpha.0 (c (n "caffe2-transform") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1rmyibr7pyv65zrcpl8xhymx19klf5p33rx1x4h2zw126dzv0fhw")))

(define-public crate-caffe2-transform-0.1.5-alpha.0 (c (n "caffe2-transform") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-graph") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0rs74p2spsf067qgc36vhn0ddlq059w6h9fkmbaf128bmkwrgwpd")))

