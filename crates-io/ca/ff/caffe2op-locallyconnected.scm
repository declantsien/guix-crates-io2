(define-module (crates-io ca ff caffe2op-locallyconnected) #:use-module (crates-io))

(define-public crate-caffe2op-locallyconnected-0.1.4-alpha.0 (c (n "caffe2op-locallyconnected") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0brnhxxgc9kymbw0m7dznq2s9ljnwxwix71mq828d5aphm3lh8qg")))

(define-public crate-caffe2op-locallyconnected-0.1.5-alpha.0 (c (n "caffe2op-locallyconnected") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1kcwrbjrzskz7lbrchx2iixl2dz18jk5fz5yjhfilkwac226vzfz")))

