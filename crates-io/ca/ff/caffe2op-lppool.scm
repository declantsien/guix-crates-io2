(define-module (crates-io ca ff caffe2op-lppool) #:use-module (crates-io))

(define-public crate-caffe2op-lppool-0.1.4-alpha.0 (c (n "caffe2op-lppool") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-pool") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1m8pkwlsp40hp798m8ciddqlrs7v5kv60x4vh776mcnai1n53xmx")))

(define-public crate-caffe2op-lppool-0.1.5-alpha.0 (c (n "caffe2op-lppool") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-pool") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0f0d1hd8y37l2asqaqllnzyspm6jxd5r8z6059zwpla4pplnrlgs")))

