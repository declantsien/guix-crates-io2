(define-module (crates-io ca ff caffe2op-stylizer) #:use-module (crates-io))

(define-public crate-caffe2op-stylizer-0.1.4-alpha.0 (c (n "caffe2op-stylizer") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0jfdp61ap0x95qpxlzykvkx6z8msfkd3fyrmva9x6fa6f07xga1i")))

(define-public crate-caffe2op-stylizer-0.1.5-alpha.0 (c (n "caffe2op-stylizer") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "141dkjv7brp5bym7rr6zy2xx1mszwzvnk211pdbw8sqgvfv56ny7")))

