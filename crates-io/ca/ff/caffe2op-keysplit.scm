(define-module (crates-io ca ff caffe2op-keysplit) #:use-module (crates-io))

(define-public crate-caffe2op-keysplit-0.1.4-alpha.0 (c (n "caffe2op-keysplit") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1b21qaz5xc5id94vlab9mialn03lc6c417yarddwx1ld0ghcdqmn")))

(define-public crate-caffe2op-keysplit-0.1.5-alpha.0 (c (n "caffe2op-keysplit") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1x0dbniwp5rg0jxij4gjmfqc9pcijjwkajw6vaf810alhvacn0v6")))

