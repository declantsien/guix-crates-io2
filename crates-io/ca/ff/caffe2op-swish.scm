(define-module (crates-io ca ff caffe2op-swish) #:use-module (crates-io))

(define-public crate-caffe2op-swish-0.1.4-alpha.0 (c (n "caffe2op-swish") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "025w6parxngcl7kg0lp3qwyfz97xnr6bcfdsw5q810mgd1j4b957")))

(define-public crate-caffe2op-swish-0.1.5-alpha.0 (c (n "caffe2op-swish") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1212lmhsy254cargvm50vmgm2m6c6bw7cq4w6d6b8yc827rz59l7")))

