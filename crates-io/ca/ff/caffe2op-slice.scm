(define-module (crates-io ca ff caffe2op-slice) #:use-module (crates-io))

(define-public crate-caffe2op-slice-0.1.4-alpha.0 (c (n "caffe2op-slice") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1rh95834fk9cxvc5d20ycy4ywy77lcfrwmkhhzx58njlgnwbv05y")))

(define-public crate-caffe2op-slice-0.1.5-alpha.0 (c (n "caffe2op-slice") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "14fm92m2dnh67yqh5wl4syf6l502hpbx0kx0pirdxf90klmndn0r")))

