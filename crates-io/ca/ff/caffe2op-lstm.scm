(define-module (crates-io ca ff caffe2op-lstm) #:use-module (crates-io))

(define-public crate-caffe2op-lstm-0.1.3-alpha.0 (c (n "caffe2op-lstm") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "13q5xbj12jv6nppnzdha9q2b5xxxg79bxj10gg81q6m54qknlyqh")))

(define-public crate-caffe2op-lstm-0.1.4-alpha.0 (c (n "caffe2op-lstm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "160xwb6hknh0mjzg4jxssd526gl106gyay4zijfhcxf2lrhkncw8")))

(define-public crate-caffe2op-lstm-0.1.5-alpha.0 (c (n "caffe2op-lstm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1vif5kdxffzg586zbpbz4a8h4zxrvzh75pz2jak2bxlk18hgjs20")))

