(define-module (crates-io ca ff caffe2op-prepend) #:use-module (crates-io))

(define-public crate-caffe2op-prepend-0.1.4-alpha.0 (c (n "caffe2op-prepend") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1sfzlzlam791j4g99n12dgjz5c1b6xs9r3gig2dhdsphmiac76ry")))

(define-public crate-caffe2op-prepend-0.1.5-alpha.0 (c (n "caffe2op-prepend") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1f9vp1kc3ik7fdn8v562qc5r3xs8hl4s4b00fim3n4lnw7mfsx6r")))

