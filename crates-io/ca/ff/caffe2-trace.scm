(define-module (crates-io ca ff caffe2-trace) #:use-module (crates-io))

(define-public crate-caffe2-trace-0.1.3-alpha.0 (c (n "caffe2-trace") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "17br0pdyw7lsczzpf61l1wj9vl3bz8avnj7wasas68bbfr2nxk9m")))

(define-public crate-caffe2-trace-0.1.4-alpha.0 (c (n "caffe2-trace") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1yh5wrsrcz3bq26ca8clwffjy69byjnfiyz0663mywr8rrzgknrx")))

