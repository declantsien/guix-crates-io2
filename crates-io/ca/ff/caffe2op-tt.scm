(define-module (crates-io ca ff caffe2op-tt) #:use-module (crates-io))

(define-public crate-caffe2op-tt-0.1.4-alpha.0 (c (n "caffe2op-tt") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "106cjs5zgmfd5ldmbh9s27ikbabi0b956p8063y4wqi3b9clx1ry")))

(define-public crate-caffe2op-tt-0.1.5-alpha.0 (c (n "caffe2op-tt") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1a8q5vham9adk2pqpyw1ff1m88phjp5xyvn7k5x0hx25dbxc8d0v")))

