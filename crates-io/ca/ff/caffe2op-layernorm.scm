(define-module (crates-io ca ff caffe2op-layernorm) #:use-module (crates-io))

(define-public crate-caffe2op-layernorm-0.1.4-alpha.0 (c (n "caffe2op-layernorm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1xbgg191ygbw2l3098imvlbxzj70nvgmg7n0xfzkaj0p5x2ggj53")))

(define-public crate-caffe2op-layernorm-0.1.5-alpha.0 (c (n "caffe2op-layernorm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "192ydqk26y95lnm3nxxxw3d9kq17h8rvjvlg5x1lzz3qff7vqcsc")))

