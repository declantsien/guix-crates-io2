(define-module (crates-io ca ff caffe2-output) #:use-module (crates-io))

(define-public crate-caffe2-output-0.1.3-alpha.0 (c (n "caffe2-output") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0aq9gk1xh6y3fzg0dm9wd4ciyi4fimmmnzs6vkkx7d9my643nvb5")))

(define-public crate-caffe2-output-0.1.4-alpha.0 (c (n "caffe2-output") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "03dqwc8v6jfvlk293fiq2sbzissxa5hh095ydmacwzyixb6cp32n")))

(define-public crate-caffe2-output-0.1.5-alpha.0 (c (n "caffe2-output") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0xb6s4zvy062k6jwwgfi87dflilk6bjbr9ca6i056vjix86pidpi")))

