(define-module (crates-io ca ff caffe2op-copy) #:use-module (crates-io))

(define-public crate-caffe2op-copy-0.1.3-alpha.0 (c (n "caffe2op-copy") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1czhyxy2psghl5m1hsya3jklr23bwzdvicn73znjyvgn1rkmflmd")))

(define-public crate-caffe2op-copy-0.1.4-alpha.0 (c (n "caffe2op-copy") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1hlnnhrybnnrk76gk02f2xvgc3k3d613gfkjiv6y2c0grnvghzjr")))

(define-public crate-caffe2op-copy-0.1.5-alpha.0 (c (n "caffe2op-copy") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0j2xrg2d45r7pyvhz6bl3s46x3qsmz1rdrahn4975k4835l6a2gw")))

