(define-module (crates-io ca ff caffe2op-thresholdedrelu) #:use-module (crates-io))

(define-public crate-caffe2op-thresholdedrelu-0.1.4-alpha.0 (c (n "caffe2op-thresholdedrelu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1l04x0q8jm0snzn1nmvgnzbrwcmrvpg95i02b7i5hw3jk649i977")))

(define-public crate-caffe2op-thresholdedrelu-0.1.5-alpha.0 (c (n "caffe2op-thresholdedrelu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "149k3yirqfm9zk98ypr4ain5yjqzfnlda12l0if2hsnhbdnxcl5c")))

