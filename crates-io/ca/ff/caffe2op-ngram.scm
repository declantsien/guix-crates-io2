(define-module (crates-io ca ff caffe2op-ngram) #:use-module (crates-io))

(define-public crate-caffe2op-ngram-0.1.4-alpha.0 (c (n "caffe2op-ngram") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0xd0b258ksa5f1b79ahfixyv8ylx7inw1npdhwlcdwjvygppjc4z")))

(define-public crate-caffe2op-ngram-0.1.5-alpha.0 (c (n "caffe2op-ngram") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1pznxrvw8ms421pb768lkim4dln8h39sm5hnvad6a6i8l6gjxbkw")))

