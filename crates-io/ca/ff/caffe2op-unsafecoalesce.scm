(define-module (crates-io ca ff caffe2op-unsafecoalesce) #:use-module (crates-io))

(define-public crate-caffe2op-unsafecoalesce-0.1.4-alpha.0 (c (n "caffe2op-unsafecoalesce") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1f4fk7xzspwhjakk0z6prfcx3jmg5zm1l2x79lzmxx56ia5bwn4w")))

(define-public crate-caffe2op-unsafecoalesce-0.1.5-alpha.0 (c (n "caffe2op-unsafecoalesce") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0mcvvq4awgbb4s2zm4m3q1gafm4swfh7jsjhx4aqczs8dx5nx228")))

