(define-module (crates-io ca ff caff-archive) #:use-module (crates-io))

(define-public crate-caff-archive-0.1.0 (c (n "caff-archive") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v3vji35jkjc3dkmp8imr90yrk822s32yqn84x9kw1mxvpmnlk8v") (f (quote (("discovery" "logging") ("default")))) (s 2) (e (quote (("logging" "dep:log"))))))

