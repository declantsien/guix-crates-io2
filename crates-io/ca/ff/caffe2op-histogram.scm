(define-module (crates-io ca ff caffe2op-histogram) #:use-module (crates-io))

(define-public crate-caffe2op-histogram-0.1.4-alpha.0 (c (n "caffe2op-histogram") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1qnp0dqnkgnhhwrzb4fbpbklaillfsmzwjb5r57hhblnf1fz3gmy")))

(define-public crate-caffe2op-histogram-0.1.5-alpha.0 (c (n "caffe2op-histogram") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1c1kckbd46719z44x9y0rp5yxq9vv1a8j8sgj939l6gw6vzj7hf9")))

