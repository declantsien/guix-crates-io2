(define-module (crates-io ca ff caffe2op-quantdecode) #:use-module (crates-io))

(define-public crate-caffe2op-quantdecode-0.1.4-alpha.0 (c (n "caffe2op-quantdecode") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1pxswifqml7dqvx2hgrx16cqnhgdj2vy1rqc1rgybq2kfsr2g4gb")))

(define-public crate-caffe2op-quantdecode-0.1.5-alpha.0 (c (n "caffe2op-quantdecode") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "05a4ip1p717j52as229kdy346pmb2isfmk8vp6i7f14vzzi24gs0")))

