(define-module (crates-io ca ff caffe2op-shape) #:use-module (crates-io))

(define-public crate-caffe2op-shape-0.1.4-alpha.0 (c (n "caffe2op-shape") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0dlc74xd5sli5csqxxaj7h7xk1k4y53jb4ppa9ghmgiax53pag43")))

(define-public crate-caffe2op-shape-0.1.5-alpha.0 (c (n "caffe2op-shape") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "047606d2416ckch259ayvrqpygfbyasb932f9yw7yv0684x1naff")))

