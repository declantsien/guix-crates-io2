(define-module (crates-io ca ff caffe2op-inference) #:use-module (crates-io))

(define-public crate-caffe2op-inference-0.1.4-alpha.0 (c (n "caffe2op-inference") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1djxbg9s53szw9ag95f3p9nwaipd83xgqmigbl9vsaxk16z9jn3f")))

