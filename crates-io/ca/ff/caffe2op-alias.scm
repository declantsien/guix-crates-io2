(define-module (crates-io ca ff caffe2op-alias) #:use-module (crates-io))

(define-public crate-caffe2op-alias-0.1.3-alpha.0 (c (n "caffe2op-alias") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1pwl1qj9mqlqalkmpkmzydsryagy40i25bwikd3yi31nz1ghps30")))

(define-public crate-caffe2op-alias-0.1.4-alpha.0 (c (n "caffe2op-alias") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "06j17x71gq66b39rmas67ahc25c85lbvjy3cxmb2wrrjnc94vrkg")))

(define-public crate-caffe2op-alias-0.1.5-alpha.0 (c (n "caffe2op-alias") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1i9f5qbq9m9k6n34xsamx9qkw3pkwg4cwpfwl4lzchfp80x0k33f")))

