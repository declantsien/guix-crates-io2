(define-module (crates-io ca ff caffe2-tensor) #:use-module (crates-io))

(define-public crate-caffe2-tensor-0.1.3-alpha.0 (c (n "caffe2-tensor") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0jrqvcynj021c8dfygy1snmx8z19pb4yin57skhw4i2w1ahjm6hj")))

(define-public crate-caffe2-tensor-0.1.4-alpha.0 (c (n "caffe2-tensor") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0hxx8dancf16lwff06x7cdpddkk0l7gh0adjhrgjkyhdx1vvin5m")))

(define-public crate-caffe2-tensor-0.1.5-alpha.0 (c (n "caffe2-tensor") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1d73l38z6xv7wj8gzknpk3apkhd5f0bkyzcb8dpydzb8cvm9v02a")))

