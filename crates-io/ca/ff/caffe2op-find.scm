(define-module (crates-io ca ff caffe2op-find) #:use-module (crates-io))

(define-public crate-caffe2op-find-0.1.4-alpha.0 (c (n "caffe2op-find") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0vsjzw27x1chg2xylhs699x1kvkrfsrvy932lf3055dgkrazbhw6")))

(define-public crate-caffe2op-find-0.1.5-alpha.0 (c (n "caffe2op-find") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1266k0lh8p51zj2w6qyd2jsr0kkb0g2m1baicm0k70ca7jszdfmn")))

