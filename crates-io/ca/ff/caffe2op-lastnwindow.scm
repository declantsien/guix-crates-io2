(define-module (crates-io ca ff caffe2op-lastnwindow) #:use-module (crates-io))

(define-public crate-caffe2op-lastnwindow-0.1.4-alpha.0 (c (n "caffe2op-lastnwindow") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0c1wk6h09j508z3ca3sm7c59i37klg5y23hckncw1i9zcz4df0zy")))

(define-public crate-caffe2op-lastnwindow-0.1.5-alpha.0 (c (n "caffe2op-lastnwindow") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "15mfhh2vzm8y96mc9yy63wwrzf38qc9s3mn8x3vv314whcl2dwzr")))

