(define-module (crates-io ca ff caffe2-perfkernels) #:use-module (crates-io))

(define-public crate-caffe2-perfkernels-0.1.3-alpha.0 (c (n "caffe2-perfkernels") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1jfqiixx00as8yr0jb562w4n5di9wxzl7d5z38pyw583fd5c4avs")))

(define-public crate-caffe2-perfkernels-0.1.4-alpha.0 (c (n "caffe2-perfkernels") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0jxfbnfp8vjlj773084ph9vb4v7zzbawahdpjivd378ybs1nfnmv")))

(define-public crate-caffe2-perfkernels-0.1.5-alpha.0 (c (n "caffe2-perfkernels") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "02g60w1r5slbb825zx1qkhjw6zzlfcazq1j0dpqnkmyw0jawdgpd")))

