(define-module (crates-io ca ff caffe2op-instancenorm) #:use-module (crates-io))

(define-public crate-caffe2op-instancenorm-0.1.4-alpha.0 (c (n "caffe2op-instancenorm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1jcm1jkh3mvkbn10haxyykxc1jrhx1c8kk4c30jskl2agsn4knzd")))

(define-public crate-caffe2op-instancenorm-0.1.5-alpha.0 (c (n "caffe2op-instancenorm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1wddr3xiw70123s9yg0jl3icih1jdgwb5dgpw0jnl7d80ydd8jp3")))

