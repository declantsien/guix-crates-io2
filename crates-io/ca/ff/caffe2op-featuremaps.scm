(define-module (crates-io ca ff caffe2op-featuremaps) #:use-module (crates-io))

(define-public crate-caffe2op-featuremaps-0.1.4-alpha.0 (c (n "caffe2op-featuremaps") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1h4lq41a4dgdg946303x8m7k1yp9kh1im8i7bg05h71373lf3059")))

(define-public crate-caffe2op-featuremaps-0.1.5-alpha.0 (c (n "caffe2op-featuremaps") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1hg0nbbqpcccmvma76q261yavpghbmbnkpjkh0jndz3bz29rkaqg")))

