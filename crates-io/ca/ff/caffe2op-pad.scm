(define-module (crates-io ca ff caffe2op-pad) #:use-module (crates-io))

(define-public crate-caffe2op-pad-0.1.4-alpha.0 (c (n "caffe2op-pad") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "10lysgvbzv7y7655m4bzhp1jsxxs7rrz58wyyng5bfkz1bhjw1aj")))

(define-public crate-caffe2op-pad-0.1.5-alpha.0 (c (n "caffe2op-pad") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-conv") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0y8vy40kx76h7ksy1gvl6vlda8lq4prqms5b0hzrnaa17hp012bs")))

