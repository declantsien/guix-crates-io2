(define-module (crates-io ca ff caffe2op-onnxwhile) #:use-module (crates-io))

(define-public crate-caffe2op-onnxwhile-0.1.4-alpha.0 (c (n "caffe2op-onnxwhile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-scope") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "00mclfy37cryjxs5lih1nb700pyv1jb50zbbxhwj5d45ibc0r7mx")))

(define-public crate-caffe2op-onnxwhile-0.1.5-alpha.0 (c (n "caffe2op-onnxwhile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-scope") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1pmndd6cgkv2fmipkbz4a72zq8yz6wds1vl0i8s0hxksvrlp40ch")))

