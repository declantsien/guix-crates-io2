(define-module (crates-io ca ff caffe2op-integralimage) #:use-module (crates-io))

(define-public crate-caffe2op-integralimage-0.1.4-alpha.0 (c (n "caffe2op-integralimage") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0ish4brghmjwfraxyk9hzg98i74vdm2df876v2qfk1rgzy025h38")))

(define-public crate-caffe2op-integralimage-0.1.5-alpha.0 (c (n "caffe2op-integralimage") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ilqx8m1b9r68jwjg053rd0ax6lm02rvnxi81iwv6lw7sns76dsf")))

