(define-module (crates-io ca ff caffe2op-mish) #:use-module (crates-io))

(define-public crate-caffe2op-mish-0.1.4-alpha.0 (c (n "caffe2op-mish") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1sj8g1z109f0c1fql27w7dcxpp9l9mjkg4ykyld4n5jh2jk7zxb8")))

(define-public crate-caffe2op-mish-0.1.5-alpha.0 (c (n "caffe2op-mish") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1f6bcgsizg34m3i167rk9arh1qwg32wy4yi95g3rl84jx0kh659a")))

