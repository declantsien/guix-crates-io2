(define-module (crates-io ca ff caffe2-store) #:use-module (crates-io))

(define-public crate-caffe2-store-0.1.3-alpha.0 (c (n "caffe2-store") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xl8wjb92j8ilxl3vfq6zg623qcq8sx2gl07d0qiri9ndbknw97z")))

(define-public crate-caffe2-store-0.1.4-alpha.0 (c (n "caffe2-store") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0hs4w0kgy2biav85igpar6fvds58gk77si5n5c8f5j6fc8a85842")))

(define-public crate-caffe2-store-0.1.5-alpha.0 (c (n "caffe2-store") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "01y84f2m0w760s29xhzsmnygl0r5rd7xdkppanx18jl76bwcrk3i")))

