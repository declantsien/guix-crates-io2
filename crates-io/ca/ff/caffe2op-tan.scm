(define-module (crates-io ca ff caffe2op-tan) #:use-module (crates-io))

(define-public crate-caffe2op-tan-0.1.4-alpha.0 (c (n "caffe2op-tan") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0rqd31lknh35j6h544y4jwf4bb75xqa25l89cw1iy9aqxbvaibmy")))

(define-public crate-caffe2op-tan-0.1.5-alpha.0 (c (n "caffe2op-tan") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "01hkj3z6dbxynzrhzasxvvj3z8ji415kbw58cnah7xh0jwgkq74b")))

