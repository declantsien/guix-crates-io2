(define-module (crates-io ca ff caffe2op-onehot) #:use-module (crates-io))

(define-public crate-caffe2op-onehot-0.1.4-alpha.0 (c (n "caffe2op-onehot") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0jk2kwm16kz5f7dngiw53z4zm9i66p9jc9rhhx9m7mwgxsrdrphd")))

(define-public crate-caffe2op-onehot-0.1.5-alpha.0 (c (n "caffe2op-onehot") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "01pc8r1xnm2sfy6yr48m4z68jmnkpdfags21v4sh368cb69r0rp5")))

