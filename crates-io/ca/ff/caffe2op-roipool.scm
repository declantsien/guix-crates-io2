(define-module (crates-io ca ff caffe2op-roipool) #:use-module (crates-io))

(define-public crate-caffe2op-roipool-0.1.4-alpha.0 (c (n "caffe2op-roipool") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1w5bv403iv2cglf2m94kk3bh9bmk1wp3i9ivipl7sj5sm95vjgqy")))

(define-public crate-caffe2op-roipool-0.1.5-alpha.0 (c (n "caffe2op-roipool") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0np3fj35i4jx566iv9790gwblnz42icy9cay37imf7gfp19psa35")))

