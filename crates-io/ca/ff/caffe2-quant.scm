(define-module (crates-io ca ff caffe2-quant) #:use-module (crates-io))

(define-public crate-caffe2-quant-0.1.3-alpha.0 (c (n "caffe2-quant") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "13nsb050817sxrkzi0mfkx5xr3iz8j1hbp20i41ik6kihsi1cji1")))

(define-public crate-caffe2-quant-0.1.4-alpha.0 (c (n "caffe2-quant") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1dpqxp9yy2ypkgfcxsg9smzhx4ahy0v0v6h8ckx6pdxkmcbjxsc7")))

