(define-module (crates-io ca ff caffe2-types) #:use-module (crates-io))

(define-public crate-caffe2-types-0.1.3-alpha.0 (c (n "caffe2-types") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1nakqcrfg2if7fal9bmqm7azzy99k7r77x5py16n0k9k6wqb5b15")))

(define-public crate-caffe2-types-0.1.4-alpha.0 (c (n "caffe2-types") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "154y00wlrhjd5fj9wiln2zcnd2l8a2v0gb47adhb5qfmipkl4kbn")))

