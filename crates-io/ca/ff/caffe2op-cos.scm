(define-module (crates-io ca ff caffe2op-cos) #:use-module (crates-io))

(define-public crate-caffe2op-cos-0.1.3-alpha.0 (c (n "caffe2op-cos") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1d9syslg4smkhc21k1n3v96srx3ns6vik4ps4sn11cm11q5mp4dl")))

(define-public crate-caffe2op-cos-0.1.4-alpha.0 (c (n "caffe2op-cos") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0ypzzz7m12iik12z3jgi9qrw204dlx1sx6civx8rdhbawcb2g61s")))

(define-public crate-caffe2op-cos-0.1.5-alpha.0 (c (n "caffe2op-cos") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "173m9hbas959q5fvb259g9ivkvnjhmvap2jxb4sddw78lbwjl82g")))

