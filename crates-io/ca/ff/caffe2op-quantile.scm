(define-module (crates-io ca ff caffe2op-quantile) #:use-module (crates-io))

(define-public crate-caffe2op-quantile-0.1.4-alpha.0 (c (n "caffe2op-quantile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1y11kcvnlb0msp1fhiasqwslbrhxyqb0k3gyab5ca4clnpjrpcxh")))

(define-public crate-caffe2op-quantile-0.1.5-alpha.0 (c (n "caffe2op-quantile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "09dxd4rfvqzqw9g4nmrd6yhkk9wpafpflricyc8asbpazmhi4rz9")))

