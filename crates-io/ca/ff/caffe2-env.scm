(define-module (crates-io ca ff caffe2-env) #:use-module (crates-io))

(define-public crate-caffe2-env-0.1.3-alpha.0 (c (n "caffe2-env") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0isl5fg9c05lmb9b21qla1mh1km75ba7snaf2wnlvyykf5gw9jzx")))

(define-public crate-caffe2-env-0.1.4-alpha.0 (c (n "caffe2-env") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "062k2rsqdvf184zzjrr4v92a9qgbwskf6fckpksci2kzxvkz20rr")))

(define-public crate-caffe2-env-0.1.5-alpha.0 (c (n "caffe2-env") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0sbx66x6nk9ccpdj5262bc8696c81lszfn6wxxzjm11b9wwmcz26")))

