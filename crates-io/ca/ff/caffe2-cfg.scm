(define-module (crates-io ca ff caffe2-cfg) #:use-module (crates-io))

(define-public crate-caffe2-cfg-0.1.3-alpha.0 (c (n "caffe2-cfg") (v "0.1.3-alpha.0") (d (list (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "1rjgk26jpiw521gxcy14bv39yz4blc7agmmx4br021mx0wcnd1iq")))

(define-public crate-caffe2-cfg-0.1.4-alpha.0 (c (n "caffe2-cfg") (v "0.1.4-alpha.0") (d (list (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "17i4ccllk8d8f439jw4rkxm6afy3jnxaai7bxyazx942hgdy5y37")))

(define-public crate-caffe2-cfg-0.1.5-alpha.0 (c (n "caffe2-cfg") (v "0.1.5-alpha.0") (d (list (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "082s076cmhxf61zyka51lnawsmnh9xr5hg1daij6w2d436imjgjf")))

