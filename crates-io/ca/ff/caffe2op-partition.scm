(define-module (crates-io ca ff caffe2op-partition) #:use-module (crates-io))

(define-public crate-caffe2op-partition-0.1.4-alpha.0 (c (n "caffe2op-partition") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0rpqmp7k83hdam7j14rxjmkldikgc4r38f2in4i9avdpj37i914h")))

(define-public crate-caffe2op-partition-0.1.5-alpha.0 (c (n "caffe2op-partition") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0bfx24ql1w5mzz014wmx6zcp5zgm768czicnfb422swwyprhrbqh")))

