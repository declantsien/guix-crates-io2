(define-module (crates-io ca ff caffe2op-orderswitch) #:use-module (crates-io))

(define-public crate-caffe2op-orderswitch-0.1.4-alpha.0 (c (n "caffe2op-orderswitch") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-types") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "03bldg1ywd479l4l10zqmk70y5y3jgck5k2kmd1k3m0vh7zhv1d3")))

(define-public crate-caffe2op-orderswitch-0.1.5-alpha.0 (c (n "caffe2op-orderswitch") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1sx352jmshwv6md7wq4l48lp1m0pyrw2y0nwsy7xk431ynn11dw3")))

