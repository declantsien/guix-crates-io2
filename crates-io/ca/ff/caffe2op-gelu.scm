(define-module (crates-io ca ff caffe2op-gelu) #:use-module (crates-io))

(define-public crate-caffe2op-gelu-0.1.4-alpha.0 (c (n "caffe2op-gelu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-elementwise") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2op-pow") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "03nydr3wx8zigv7m4kz0sjfazgvq74yki6rcxp1bfrpgy6p13ay4")))

(define-public crate-caffe2op-gelu-0.1.5-alpha.0 (c (n "caffe2op-gelu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-blob") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-elementwise") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2op-pow") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "09l7i04algagk4574vf3vbx82386v0kbr7vvwnkhm2zsc6r3vgs1")))

