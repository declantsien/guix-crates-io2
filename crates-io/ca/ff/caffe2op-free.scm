(define-module (crates-io ca ff caffe2op-free) #:use-module (crates-io))

(define-public crate-caffe2op-free-0.1.4-alpha.0 (c (n "caffe2op-free") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "02s31213fclgpx2jf67fz0b7v68b7rbj3nvplzklhsipjwcgjm14")))

(define-public crate-caffe2op-free-0.1.5-alpha.0 (c (n "caffe2op-free") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "17hn71jnrbfjxgzlmvsi56b8n2sx05av2lqmk1kaa749p9ldff2w")))

