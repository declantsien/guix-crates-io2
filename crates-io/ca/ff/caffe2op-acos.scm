(define-module (crates-io ca ff caffe2op-acos) #:use-module (crates-io))

(define-public crate-caffe2op-acos-0.1.3-alpha.0 (c (n "caffe2op-acos") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "18i3gyvy55n2khlagvanfyl2cs2r5bsrxq6ggs7v5nn1ir0hvlh9")))

(define-public crate-caffe2op-acos-0.1.4-alpha.0 (c (n "caffe2op-acos") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0nrkfphv6rww2gvbzj62gjvpij6zc829c7chx84vvx077rm8sk4k")))

(define-public crate-caffe2op-acos-0.1.5-alpha.0 (c (n "caffe2op-acos") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1lhinp6v94kya9cvkm1nla0bm09zcgppp4v763yiyd85w34lkw18")))

