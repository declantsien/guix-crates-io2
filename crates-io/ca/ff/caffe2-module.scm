(define-module (crates-io ca ff caffe2-module) #:use-module (crates-io))

(define-public crate-caffe2-module-0.1.3-alpha.0 (c (n "caffe2-module") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xgcfl0lmzqyd8lc82w3x7xa8xm56ivwkfn7sbfgyh9xpgihr1hh")))

(define-public crate-caffe2-module-0.1.4-alpha.0 (c (n "caffe2-module") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0lzz9qciran3vpacl5zf6bhqyxqyj16yrz79x6cbacf36d07m4bn")))

(define-public crate-caffe2-module-0.1.5-alpha.0 (c (n "caffe2-module") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "050lv1b4n3iikq0dz76cnphywj2kifkqlfwzcmhyqhm249dd0m98")))

