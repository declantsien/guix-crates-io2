(define-module (crates-io ca ff caffe2op-minmax) #:use-module (crates-io))

(define-public crate-caffe2op-minmax-0.1.4-alpha.0 (c (n "caffe2op-minmax") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0ncm8vrmg21y87sq245kp00k7dfs7nhxh047952lqf7p9xccf96r")))

(define-public crate-caffe2op-minmax-0.1.5-alpha.0 (c (n "caffe2op-minmax") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1vigqskyyrcpnjn053axkbsyksyfx8y32h5imaqpkhj7lwhjnsr0")))

