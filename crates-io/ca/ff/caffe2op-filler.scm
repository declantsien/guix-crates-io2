(define-module (crates-io ca ff caffe2op-filler) #:use-module (crates-io))

(define-public crate-caffe2op-filler-0.1.4-alpha.0 (c (n "caffe2op-filler") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0yv8qs56k10c6x28mlzfi60nx9z366zz8vkjhs9v9cardjf0vsjl")))

(define-public crate-caffe2op-filler-0.1.5-alpha.0 (c (n "caffe2op-filler") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1nisr25f1s33zbfd2n1q5di6ghgf7vs1n7aj63xkh44vjpx7lm6k")))

