(define-module (crates-io ca ff caffe2op-relu) #:use-module (crates-io))

(define-public crate-caffe2op-relu-0.1.4-alpha.0 (c (n "caffe2op-relu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "084v799nqzng96hn76hair71rqf4569m5zxa6f73dn4m42avz7pj")))

(define-public crate-caffe2op-relu-0.1.5-alpha.0 (c (n "caffe2op-relu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0m34a0dy6g2rskrj4kb3f3vb05i6ycndgg4g988a8vhkmilm13hy")))

