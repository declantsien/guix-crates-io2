(define-module (crates-io ca ff caffe2op-negativegradient) #:use-module (crates-io))

(define-public crate-caffe2op-negativegradient-0.1.4-alpha.0 (c (n "caffe2op-negativegradient") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0ciaxc2a17br0kxxsa02ycvyvh9ar41zv6qyzw79gn6lz70v1j8c")))

