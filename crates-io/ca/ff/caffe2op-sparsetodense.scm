(define-module (crates-io ca ff caffe2op-sparsetodense) #:use-module (crates-io))

(define-public crate-caffe2op-sparsetodense-0.1.4-alpha.0 (c (n "caffe2op-sparsetodense") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0fmxdycpy8awakpnfcvb5imkd872k7h5qfsx6vb46874z6dmxrif")))

(define-public crate-caffe2op-sparsetodense-0.1.5-alpha.0 (c (n "caffe2op-sparsetodense") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0hzvwm46w382851sh5qm3cnpaxvsj157xay3lbzwh8jkr93wqvi2")))

