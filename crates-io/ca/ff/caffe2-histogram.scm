(define-module (crates-io ca ff caffe2-histogram) #:use-module (crates-io))

(define-public crate-caffe2-histogram-0.1.3-alpha.0 (c (n "caffe2-histogram") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-serverquantize") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0a6gr0wpi28kqb86jpywba1bjn2m45lm7q5maa75dkx8mv4s0lbg")))

(define-public crate-caffe2-histogram-0.1.4-alpha.0 (c (n "caffe2-histogram") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-serverquantize") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1dppifj4da9av57kb6g0gl3mla9b1h6p4bmivk6bg61vaabj6j6f")))

(define-public crate-caffe2-histogram-0.1.5-alpha.0 (c (n "caffe2-histogram") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-serverquantize") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1ndzqqv3rkypqamwchjapvyrlyjwzz3v6yyinh2rbl8c4ccaggvg")))

