(define-module (crates-io ca ff caffe2op-stopgradient) #:use-module (crates-io))

(define-public crate-caffe2op-stopgradient-0.1.4-alpha.0 (c (n "caffe2op-stopgradient") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "13dkfpz04vjsxiwiivl2rbliw99n19lyj7id20qpc4hxlz676m5h")))

(define-public crate-caffe2op-stopgradient-0.1.5-alpha.0 (c (n "caffe2op-stopgradient") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "09ynxk38rgaplffbymq2fywnfasmp0rjn9vr1k1hkdifnrq92svm")))

