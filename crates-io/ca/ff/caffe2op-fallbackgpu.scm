(define-module (crates-io ca ff caffe2op-fallbackgpu) #:use-module (crates-io))

(define-public crate-caffe2op-fallbackgpu-0.1.4-alpha.0 (c (n "caffe2op-fallbackgpu") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "040xa505zkiq7xa092mymwfy7h39ik09d50ifvmvbsbxyvhm0947")))

(define-public crate-caffe2op-fallbackgpu-0.1.5-alpha.0 (c (n "caffe2op-fallbackgpu") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-common") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0701gggqcc7kyj8ffpak8indnw8rvl2pjjq9vwl0qbc358sdhivc")))

