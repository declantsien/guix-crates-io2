(define-module (crates-io ca ff caffe2op-distance) #:use-module (crates-io))

(define-public crate-caffe2op-distance-0.1.4-alpha.0 (c (n "caffe2op-distance") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0qidd7wfyjzz5mda3phsi8d4pf6md0w8f3pqaqw8pwwnyxrm4ldp")))

(define-public crate-caffe2op-distance-0.1.5-alpha.0 (c (n "caffe2op-distance") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0jivndhpgkk87b3cbrb8h2vcya66wwh4j7riixs93z8asc8p8zp4")))

