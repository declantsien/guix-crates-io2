(define-module (crates-io ca ff caffe2-version) #:use-module (crates-io))

(define-public crate-caffe2-version-0.1.3-alpha.0 (c (n "caffe2-version") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "16nhjxw8lddim4ldv88s7lqapfmmsq41lrm6bg1v47i9f3kjjcvg")))

(define-public crate-caffe2-version-0.1.4-alpha.0 (c (n "caffe2-version") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0v23vgr72ykqmz3b22h1km84m3y0420dqqqziy25gswp2g6ifrsg")))

