(define-module (crates-io ca ff caffe2-context) #:use-module (crates-io))

(define-public crate-caffe2-context-0.1.3-alpha.0 (c (n "caffe2-context") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-event") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0b6g4w1akcscbr39w51ih8rds62f6hw7czsny1h74c9bbqrx94a8")))

(define-public crate-caffe2-context-0.1.4-alpha.0 (c (n "caffe2-context") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-event") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-version") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "11z224mkpzlrf7g0sihm1y146byvjcfjn4y489djrwd367mr03vw")))

(define-public crate-caffe2-context-0.1.5-alpha.0 (c (n "caffe2-context") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-event") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "02gm6k8v2l18yk33p8cmg9rsg5knxcxpanch1x0mpb6y2h9snqzl")))

