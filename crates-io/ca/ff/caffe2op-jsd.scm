(define-module (crates-io ca ff caffe2op-jsd) #:use-module (crates-io))

(define-public crate-caffe2op-jsd-0.1.4-alpha.0 (c (n "caffe2op-jsd") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "186rdmrgdy500l02nzjwsri3lfnfwb1dkh01xjkpym94hd07q98x")))

(define-public crate-caffe2op-jsd-0.1.5-alpha.0 (c (n "caffe2op-jsd") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0wwzsrx30wx2jkszxfg23zf7sprl9sjfjxgcqr9533pjxg7k12bq")))

