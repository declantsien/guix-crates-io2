(define-module (crates-io ca ff caffe2op-zerog) #:use-module (crates-io))

(define-public crate-caffe2op-zerog-0.1.4-alpha.0 (c (n "caffe2op-zerog") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1z17xjwac07h5j2d0qy2pdpwjwlc0zcvcbvf955cax9qzb43w0pf")))

(define-public crate-caffe2op-zerog-0.1.5-alpha.0 (c (n "caffe2op-zerog") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0fbhd15haigq0mly89lqjjbmw891hhh5bda76j1yfnjqgvrj379x")))

