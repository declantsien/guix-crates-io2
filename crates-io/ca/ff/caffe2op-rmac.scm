(define-module (crates-io ca ff caffe2op-rmac) #:use-module (crates-io))

(define-public crate-caffe2op-rmac-0.1.4-alpha.0 (c (n "caffe2op-rmac") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "124xwvw7lrw4vr56sf8hvvsgmdvg0v162n2m79vc3jjyc71l9sbd")))

(define-public crate-caffe2op-rmac-0.1.5-alpha.0 (c (n "caffe2op-rmac") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "1f3r10iayf4i0xjj89aakrw8hv42hpis333cqcasawjrik8d73pn")))

