(define-module (crates-io ca ff caffe2-serde) #:use-module (crates-io))

(define-public crate-caffe2-serde-0.1.3-alpha.0 (c (n "caffe2-serde") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1vy03n08dw4lb38b3w82a1lyqqwvlszrwd4syjsngw5xxmcg90xv")))

(define-public crate-caffe2-serde-0.1.4-alpha.0 (c (n "caffe2-serde") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1x1g5p9ciciyy18pf6ji5xspxnsm845z13xhdfvrx7j7mxr19kp9")))

(define-public crate-caffe2-serde-0.1.5-alpha.0 (c (n "caffe2-serde") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-c10") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-util") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "14kwpqfqh8jzsbi26ikn1s9b1ld5ly9a14z57k18xzbqp09iw1r3")))

