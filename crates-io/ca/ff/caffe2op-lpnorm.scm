(define-module (crates-io ca ff caffe2op-lpnorm) #:use-module (crates-io))

(define-public crate-caffe2op-lpnorm-0.1.4-alpha.0 (c (n "caffe2op-lpnorm") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1a7hl09ph0xfgiwzschbz08ckx9qwy6qgchmzii0r01ms8xaxca2")))

(define-public crate-caffe2op-lpnorm-0.1.5-alpha.0 (c (n "caffe2op-lpnorm") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0dip2vbgpf7gqpj2c2kv4j23x605h95i75382wv4s9mq4z9i56x9")))

