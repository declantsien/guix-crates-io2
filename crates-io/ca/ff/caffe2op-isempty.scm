(define-module (crates-io ca ff caffe2op-isempty) #:use-module (crates-io))

(define-public crate-caffe2op-isempty-0.1.4-alpha.0 (c (n "caffe2op-isempty") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0qh435wpm3ddnh38f1z67564d6gxvf88f3xp502lf5yymb26xgwf")))

(define-public crate-caffe2op-isempty-0.1.5-alpha.0 (c (n "caffe2op-isempty") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0z4g7mzxgwyqm0mnc89gqlmfqpg08435y9jjv9y2iyjn3il0zm5y")))

