(define-module (crates-io ca ff caffe2op-flatten) #:use-module (crates-io))

(define-public crate-caffe2op-flatten-0.1.4-alpha.0 (c (n "caffe2op-flatten") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1ihn98if7fcwl81kn6ymanh4pvcjnni1xxiz35i5mj670i02kcmp")))

(define-public crate-caffe2op-flatten-0.1.5-alpha.0 (c (n "caffe2op-flatten") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0b9nlqylzszl9vnynbwckkz3zdw68lyl834iyzfsggdwkwjbbjqw")))

