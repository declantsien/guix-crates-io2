(define-module (crates-io ca ff caffe2op-pow) #:use-module (crates-io))

(define-public crate-caffe2op-pow-0.1.3-alpha.0 (c (n "caffe2op-pow") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0h67lm3iwgc8wsxravvny6qa627bkh31w4c4m9a22i7wqy74vi8y")))

(define-public crate-caffe2op-pow-0.1.4-alpha.0 (c (n "caffe2op-pow") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "0fy67hpxb7yq48sn4vgfb9fs5lj8g3f4j6sid3mvpk7n4vg5nv1y")))

(define-public crate-caffe2op-pow-0.1.5-alpha.0 (c (n "caffe2op-pow") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0lpawlg2015spw2mxv70c8ahgl6rz1rg9vvdxr72kk4zwvg16npn")))

