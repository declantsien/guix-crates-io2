(define-module (crates-io ca ff caffe2op-summarize) #:use-module (crates-io))

(define-public crate-caffe2op-summarize-0.1.4-alpha.0 (c (n "caffe2op-summarize") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1sf0a6sxpymsilkxln5lazidvh45sb09krn609c99savnhf9j531")))

(define-public crate-caffe2op-summarize-0.1.5-alpha.0 (c (n "caffe2op-summarize") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0r3caqx6xz4s56kgpqi1c361rs26q75x04a3gwf340imljcyifzn")))

