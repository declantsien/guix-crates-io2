(define-module (crates-io ca ff caffe2op-crfviterbi) #:use-module (crates-io))

(define-public crate-caffe2op-crfviterbi-0.1.3-alpha.0 (c (n "caffe2op-crfviterbi") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "1xia79sf8hvbhwp13cilxgcdq05p9psykzf5hfswqb41inyr4h5j")))

(define-public crate-caffe2op-crfviterbi-0.1.4-alpha.0 (c (n "caffe2op-crfviterbi") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1qzi26qkd8k7qfgkrcplgx2cdvh6fjw8p6g5gk7jfvznq9bp7lr6")))

(define-public crate-caffe2op-crfviterbi-0.1.5-alpha.0 (c (n "caffe2op-crfviterbi") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "148jzf47fjzm3jcbq81lvl5h4psh0jcykqi0rv4qp8h4bfrql2ds")))

