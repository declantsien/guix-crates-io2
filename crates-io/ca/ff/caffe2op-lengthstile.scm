(define-module (crates-io ca ff caffe2op-lengthstile) #:use-module (crates-io))

(define-public crate-caffe2op-lengthstile-0.1.4-alpha.0 (c (n "caffe2op-lengthstile") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1624fd43hiigwi7pwyv1wzdradkh637lqwx53r0s8mc8ws8zrnm5")))

(define-public crate-caffe2op-lengthstile-0.1.5-alpha.0 (c (n "caffe2op-lengthstile") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-context") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-operator") (r "^0.1.5-alpha.0") (d #t) (k 0)) (d (n "caffe2-tensor") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "0zmf42pikp54hdhrxwv91mksbdqcpac622003p7amzbfmxrw2yk3")))

