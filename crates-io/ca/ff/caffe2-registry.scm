(define-module (crates-io ca ff caffe2-registry) #:use-module (crates-io))

(define-public crate-caffe2-registry-0.1.3-alpha.0 (c (n "caffe2-registry") (v "0.1.3-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.3-alpha.0") (d #t) (k 0)))) (h "0vp6585ifjm5lg5irs1prjvhg01mvnn539ki078fgxk8nqdwa24h")))

(define-public crate-caffe2-registry-0.1.4-alpha.0 (c (n "caffe2-registry") (v "0.1.4-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.4-alpha.0") (d #t) (k 0)))) (h "1j449x2h81mw7jcs3impa0pirf2x2k8zbkkm4iqdlnbfbs4vdlm0")))

(define-public crate-caffe2-registry-0.1.5-alpha.0 (c (n "caffe2-registry") (v "0.1.5-alpha.0") (d (list (d (n "caffe2-imports") (r "^0.1.5-alpha.0") (d #t) (k 0)))) (h "039pak4qf7p3axjv0k98fgbn3f0y3rllw2vnnik8x4zjd71d77ny")))

