(define-module (crates-io ca ml camloc-common) #:use-module (crates-io))

(define-public crate-camloc-common-0.1.0 (c (n "camloc-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "opencv") (r "^0.84") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "090jvb6jp8fzr2m7yg9w5p8n8qz3rx85xfyins0b1b0rzhjwmn5f") (f (quote (("default") ("all" "cv" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("cv" "dep:opencv"))))))

(define-public crate-camloc-common-0.2.0 (c (n "camloc-common") (v "0.2.0") (d (list (d (n "opencv") (r "^0.84") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cwq9f6lpgqjhx1lr7gjxd63lb1x3cwkqvpb75vv7hh3fl1dx5y5") (f (quote (("default") ("all" "cv" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("cv" "dep:opencv"))))))

