(define-module (crates-io ca ml caml) #:use-module (crates-io))

(define-public crate-caml-0.1.0 (c (n "caml") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0cvk6ch1b12smc83fahdr8b2b1ibk9mgi4f8cbzzhkdrdbc1ag0z")))

(define-public crate-caml-0.1.1 (c (n "caml") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01rpfwrpv0rg45qpr766kbz5ilc4xk3r2xf29q7jw0hd2mfma5ql")))

