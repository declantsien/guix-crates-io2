(define-module (crates-io ca ph caphindsight_fft) #:use-module (crates-io))

(define-public crate-caphindsight_fft-0.1.0 (c (n "caphindsight_fft") (v "0.1.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "text_io") (r "^0.1.5") (d #t) (k 0)))) (h "1qd1z0n214ziipmz3vgdiblcng2676nmn565p8vv11b7d8n7qwb2")))

