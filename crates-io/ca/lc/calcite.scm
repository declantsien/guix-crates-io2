(define-module (crates-io ca lc calcite) #:use-module (crates-io))

(define-public crate-calcite-0.1.0 (c (n "calcite") (v "0.1.0") (d (list (d (n "calcite_proc_macros") (r "^0") (d #t) (k 0)) (d (n "deno_core") (r ">=0.49.0") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1zxqvikdliy56123wk7m00q838fb3dfh3dyfcb134pdl2mi43v0f") (y #t)))

(define-public crate-calcite-0.1.1 (c (n "calcite") (v "0.1.1") (d (list (d (n "calcite_proc_macros") (r "^0") (d #t) (k 0)) (d (n "deno_core") (r ">=0.49.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1qgbzqp9rxh02rk7wg3xkqj72g47vdnpawz8mmg46md7my50rikw")))

(define-public crate-calcite-0.2.0 (c (n "calcite") (v "0.2.0") (d (list (d (n "calcite_proc_macros") (r "^0") (d #t) (k 0)) (d (n "deno_core") (r ">=0.49.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gkw0s6rs661xm6y3fcxvqjrymwrgpgb2p14nl4bam3fkyx1lwhw")))

(define-public crate-calcite-0.2.1 (c (n "calcite") (v "0.2.1") (d (list (d (n "calcite_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "deno_core") (r "^0.54.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1rvm6f5dv21v4qzhbgs3d0qsl89i3vxdc88hr0a7dlp7fgbz9m3h")))

(define-public crate-calcite-0.2.2 (c (n "calcite") (v "0.2.2") (d (list (d (n "calcite_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "deno_core") (r "^0.55.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1a8ncfdyihiv6sqlr56vznw7xaz00ilajl5hms870raakzs5gjf4")))

(define-public crate-calcite-0.2.3 (c (n "calcite") (v "0.2.3") (d (list (d (n "calcite_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "deno_core") (r "^0.56.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0g5jd1rcadlnyfpxjc5ikn5bnc3y2vh8w40l7l4clz3s1xnnav9d")))

(define-public crate-calcite-0.2.4 (c (n "calcite") (v "0.2.4") (d (list (d (n "calcite_proc_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "deno_core") (r "^0.58.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1ly90wd6xsfn3zmr8jwd3p5qhpab5vwdw0ggkw9p4vb6bjgy5pja")))

(define-public crate-calcite-0.2.5 (c (n "calcite") (v "0.2.5") (d (list (d (n "calcite_proc_macros") (r ">=0.1.3, <0.2.0") (d #t) (k 0)) (d (n "deno_core") (r ">=0.69.0, <0.70.0") (d #t) (k 0)) (d (n "futures") (r ">=0.3.8, <0.4.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.117, <2.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)))) (h "16qx750mfa0dpij52kcv8fzvdwqhfgdi9nigi6hv7kj2sqn03h6p")))

(define-public crate-calcite-0.2.6 (c (n "calcite") (v "0.2.6") (d (list (d (n "calcite_proc_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "deno_core") (r "^0.77.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1rxlmk90ybsp9fimnc8r91n7kbnrb45jp5wq6j7vzanggh8xpljv")))

