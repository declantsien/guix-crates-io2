(define-module (crates-io ca lc calcul8) #:use-module (crates-io))

(define-public crate-calcul8-1.0.0 (c (n "calcul8") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gz98fcd02h6m04f7a1n9gs5582ph01wi1niablahwjh9hbp0d2q") (y #t)))

(define-public crate-calcul8-1.0.1 (c (n "calcul8") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "19912n6wnp33krxadf9i4kj5sjfpkp0ak5z1pyypra4m11ilksz8") (y #t)))

(define-public crate-calcul8-1.0.2 (c (n "calcul8") (v "1.0.2") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0psz68dli9p457w9l3gs6lhrk3v5cp04zq9i9c5li21mwz213y8x")))

