(define-module (crates-io ca lc calculator-rs) #:use-module (crates-io))

(define-public crate-calculator-rs-0.1.0 (c (n "calculator-rs") (v "0.1.0") (h "1kc5f9m91097cx1vic0y25v8ny79d0f0i0dlz1vf7gy2vvs7crph")))

(define-public crate-calculator-rs-0.1.1 (c (n "calculator-rs") (v "0.1.1") (h "02sfqp8xylw73is07b59289r4ia229mhz7m7fxcyr2mridgdv0xx")))

(define-public crate-calculator-rs-0.1.2 (c (n "calculator-rs") (v "0.1.2") (h "1s2bwz1i2knm9qdg4b4fdd7j62rb82y59zf7m4bvvxcvw098wcy8")))

