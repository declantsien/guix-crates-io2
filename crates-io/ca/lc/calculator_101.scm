(define-module (crates-io ca lc calculator_101) #:use-module (crates-io))

(define-public crate-calculator_101-0.1.0 (c (n "calculator_101") (v "0.1.0") (h "0x2aava5df3rvhhl4kw64j1ysjlc9lax3ii54rm82px0bn96f9sx")))

(define-public crate-calculator_101-0.1.1 (c (n "calculator_101") (v "0.1.1") (h "1wak9lzmv8slg6gqacd4rs23rsfy6wnin2aa8k46w8afnlvdwhhd")))

