(define-module (crates-io ca lc calcium-oxide) #:use-module (crates-io))

(define-public crate-calcium-oxide-0.1.7 (c (n "calcium-oxide") (v "0.1.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0ma17qb2k2kpql59jw071kibjf0h509013cqh3mngr9l9vl96k1z")))

(define-public crate-calcium-oxide-0.1.8 (c (n "calcium-oxide") (v "0.1.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "1znzknvf62vls8p4j3fjikkjrjz5qj94hvv4ifplgb6ns6czj4ba")))

(define-public crate-calcium-oxide-0.1.9 (c (n "calcium-oxide") (v "0.1.9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0lgw0hxsj0zxr3a9snvmyw21c7c4ik1653ga2qm1dxfny7vbm2xd")))

(define-public crate-calcium-oxide-0.1.10 (c (n "calcium-oxide") (v "0.1.10") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0njbdr9iyzm63klp42qhmj3swll3i7wvfpwhd67adgzsc47p9s6d")))

(define-public crate-calcium-oxide-0.1.11 (c (n "calcium-oxide") (v "0.1.11") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "1fbmwd443sz7x6rr7p1cnqabi2l38di5mz0i6v4sv04895biqfkx")))

(define-public crate-calcium-oxide-0.1.12 (c (n "calcium-oxide") (v "0.1.12") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0k6xxazvdxr722z7b8s0kg4v1hbxqzcr1slhhkkrdcjq1iaqsff2")))

(define-public crate-calcium-oxide-0.1.13 (c (n "calcium-oxide") (v "0.1.13") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0lyppzv925hadb8sh4a2m9ap3y8yisq76jv2zzj5349can5sqh8x")))

(define-public crate-calcium-oxide-0.1.14 (c (n "calcium-oxide") (v "0.1.14") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "1l8p5sa27lx5dirfnybnjjrk8pk174kzz0zjjfin1j0ksq41lbqr")))

(define-public crate-calcium-oxide-0.1.15 (c (n "calcium-oxide") (v "0.1.15") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "num-prime") (r "^0.2.0") (d #t) (k 0)))) (h "0q1gassm3yyb9gnzlj8nm67xnz5j6xgjldzps6dqxiq3nbxlx16h")))

