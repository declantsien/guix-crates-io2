(define-module (crates-io ca lc calculi) #:use-module (crates-io))

(define-public crate-calculi-0.1.0 (c (n "calculi") (v "0.1.0") (h "06ciqb7p6iivsisxw1qip2zrfri457rdffklax3ffgcsr86syaaj")))

(define-public crate-calculi-0.1.1 (c (n "calculi") (v "0.1.1") (h "04mldyf281vrfj10d07yf00kq9divx01sgd2g6v8jlc647hdf38x")))

(define-public crate-calculi-0.1.2 (c (n "calculi") (v "0.1.2") (h "0dviniikv5x4dwap6gly9h55g6cisb9433wp4x6w489swj46szv4")))

(define-public crate-calculi-0.2.0 (c (n "calculi") (v "0.2.0") (h "0l26ka8703dmzv0wzjqnv2mfprhzd2k3k66gsbv9ix4avibxci16")))

