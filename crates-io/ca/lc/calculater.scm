(define-module (crates-io ca lc calculater) #:use-module (crates-io))

(define-public crate-Calculater-0.1.0 (c (n "Calculater") (v "0.1.0") (h "0r45iz7w5fz9938awcsr07yainbrabgj4mjhdsdappm8gzdpvwg4")))

(define-public crate-Calculater-0.1.1 (c (n "Calculater") (v "0.1.1") (h "0iisbn6yc55j85iql6cb2csbgrmfjwvl87vsgx958vjxxphrgsbs")))

(define-public crate-Calculater-0.0.1 (c (n "Calculater") (v "0.0.1") (h "0qlqw97vvbay243w7sbydsdsm03qqb9al0vkgwl188hamj9h0a3z")))

(define-public crate-Calculater-0.0.2 (c (n "Calculater") (v "0.0.2") (h "0aiyiaaq3n7wz5w2kd3bax7gq3wam4xhy606whixj99sxnxwjfjm")))

(define-public crate-Calculater-0.0.3 (c (n "Calculater") (v "0.0.3") (h "1prw3vrjy64bpl7plml2xxn7wqwh65206s269axd6nfvv1id3ncz")))

(define-public crate-Calculater-0.0.5 (c (n "Calculater") (v "0.0.5") (h "1iz3ny93xbbh1riznc4svlxacvwfdzdspz87q9nvqki95v6ycwjn")))

(define-public crate-Calculater-0.0.6 (c (n "Calculater") (v "0.0.6") (h "0c59pkldfhkcfafn92pl01jswkalaz0940igayww534wwi3knywb")))

