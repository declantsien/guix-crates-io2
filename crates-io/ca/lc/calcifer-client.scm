(define-module (crates-io ca lc calcifer-client) #:use-module (crates-io))

(define-public crate-calcifer-client-0.1.0 (c (n "calcifer-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r36vpwpcv6v9nh013mwglm2bam8bjifcwylmrhjix0pg3q7hl14")))

