(define-module (crates-io ca lc calc_lib) #:use-module (crates-io))

(define-public crate-calc_lib-0.1.0 (c (n "calc_lib") (v "0.1.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "0nnm1lw3ngiwa7rrpbx35s21njbkch10dwvl0x2s6l8bka2xj5m3") (y #t)))

(define-public crate-calc_lib-1.0.0 (c (n "calc_lib") (v "1.0.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "0hywqdbq4hq77fxq240lhx1z12n37q9ri4a29z0y8n5whk3v2jiy")))

(define-public crate-calc_lib-1.0.1 (c (n "calc_lib") (v "1.0.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "0h4djm9gqmfwkl4vq87liq4wj3wbk5hqijanpplbdfhai4nw3nb0")))

(define-public crate-calc_lib-1.0.2 (c (n "calc_lib") (v "1.0.2") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "1cjmlk7frk5d7r9kflc76p4kkkmm5rra2k72bql34zayl1f0dlr9")))

(define-public crate-calc_lib-1.2.0 (c (n "calc_lib") (v "1.2.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "0qvihbr35a7sn59p5dq2wk91fbg3hknpn4970ycnbh3qbfwmnqwz")))

(define-public crate-calc_lib-1.3.0 (c (n "calc_lib") (v "1.3.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "1kfr6qgnhkpxccpi5yllbgd487jjdsccwf8abphwgzhxw7ci93k3")))

(define-public crate-calc_lib-1.4.0 (c (n "calc_lib") (v "1.4.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "1msb95sms9jcm1qa04spkgsc3cp4ck8g361sj95qq9d3pqmsj3pa")))

(define-public crate-calc_lib-1.4.1 (c (n "calc_lib") (v "1.4.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)))) (h "0g5pwvxxgqzx3bx8mwz415brwpch6b0dip2y296sbfn7dcswbvy7")))

(define-public crate-calc_lib-1.5.0 (c (n "calc_lib") (v "1.5.0") (h "1h0gsa7gg5wd8cs4zszklb6aa4pick6n0nrxd641dv404bz2c78h")))

(define-public crate-calc_lib-1.5.1 (c (n "calc_lib") (v "1.5.1") (h "04ah6hmbgym0q3ifa5dl8m6md9gwp0b8x6x5nxigq50vnyd6nf6v")))

(define-public crate-calc_lib-1.5.2 (c (n "calc_lib") (v "1.5.2") (h "1ajx2n64ym5pfgq91zffm8g6bp0p8ikyfiqfzfwiix101rmfidqc")))

(define-public crate-calc_lib-1.5.3 (c (n "calc_lib") (v "1.5.3") (h "1sgqrlvc2p59yp71y1pk84vi1dkkjb7jxqmxc9jwr3hcfwnyy16p")))

(define-public crate-calc_lib-1.5.4 (c (n "calc_lib") (v "1.5.4") (h "0mvhdrgnnj6jl4h1wz3dpzilqsghsgr8snmk5lyscdyc2fjn9g0w")))

(define-public crate-calc_lib-1.5.5 (c (n "calc_lib") (v "1.5.5") (h "112wqpypy5nqnxhyvkn19rw4fh1ckkc21bp5hdmmjr011sacsd4l") (y #t)))

(define-public crate-calc_lib-1.5.6 (c (n "calc_lib") (v "1.5.6") (h "1z80psk3c8k8wmdcxzq91dbaxgg5484kgy00yfp3lkvi80vdqhah")))

(define-public crate-calc_lib-1.6.0 (c (n "calc_lib") (v "1.6.0") (h "14qb6pa69bxd5w0q68kbj4i2bz1536rs7bbsynpc23ylyk9g3aks") (y #t)))

(define-public crate-calc_lib-1.7.0 (c (n "calc_lib") (v "1.7.0") (h "04awbydh4vp67rk9zm4yxkchvc2pcrwniwkxifa2mih4p2csr5mb")))

(define-public crate-calc_lib-2.0.0 (c (n "calc_lib") (v "2.0.0") (h "13pwz3qkgfpqci6llhl82hdhn5vjxjzimpd4vzkidjrlavcx1fjz")))

(define-public crate-calc_lib-2.1.0 (c (n "calc_lib") (v "2.1.0") (h "1b9xni0shpkkm5b1mwmcdgj8xlm6n2gs3ld3xaj6yd19w9lxckkl")))

