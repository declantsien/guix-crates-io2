(define-module (crates-io ca lc calcu) #:use-module (crates-io))

(define-public crate-calcu-0.1.0 (c (n "calcu") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0zba0wighhrbbyz03mpph33577ryb6glf2d03jlkwycgl99p7wib") (y #t)))

(define-public crate-calcu-0.2.0 (c (n "calcu") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1bf9ypsq1jppp3a8iwwykggnq6h209vqzfj6vhq0f7nbsdy2x09m") (y #t)))

(define-public crate-calcu-0.3.0 (c (n "calcu") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0nnyrs0wg8f778051hmlp2d256wzmcds9zkg2xxx9g6z95al2m5c") (y #t)))

(define-public crate-calcu-0.3.1 (c (n "calcu") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0jc1rqyjrkhm4hsdhpnhw11l04pkmsbwkww1kyxrxhfm16x9j3r7") (y #t)))

(define-public crate-calcu-0.4.0 (c (n "calcu") (v "0.4.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0rj5w33w0skvmmwwc20ywqipw441p55lc7nn6grpg3s6lnpmg4w3") (y #t)))

