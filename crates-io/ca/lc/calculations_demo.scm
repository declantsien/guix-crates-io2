(define-module (crates-io ca lc calculations_demo) #:use-module (crates-io))

(define-public crate-calculations_demo-0.1.0 (c (n "calculations_demo") (v "0.1.0") (h "0ngsq99av7cpxv5jxhiagmqabgx9i41b33ngd6l4dqiz0imf4k7p") (y #t)))

(define-public crate-calculations_demo-0.1.1 (c (n "calculations_demo") (v "0.1.1") (h "1inv5vsyjafc0vfs2b68yhnvzk9zqxb1gg4jwsh54xvhvh82p3c0") (y #t)))

(define-public crate-calculations_demo-0.1.2 (c (n "calculations_demo") (v "0.1.2") (h "0wjcm0bg78q3w1iq8fbk53ivchxvcqcxn1mqbmz054g6k9mwqlnc")))

