(define-module (crates-io ca lc calcver_example) #:use-module (crates-io))

(define-public crate-calcver_example-0.1.0-rc.14 (c (n "calcver_example") (v "0.1.0-rc.14") (h "0985g226qgixnpgjyl1vzxrsa1754lfpm1g3ysl6b3bd4zqv29wf") (y #t)))

(define-public crate-calcver_example-0.1.0-rc.15 (c (n "calcver_example") (v "0.1.0-rc.15") (h "0323xxp5pszdm46cfv10caz7z8xsyd00kbzxga21m0d79ymahrkg") (y #t)))

(define-public crate-calcver_example-0.1.0 (c (n "calcver_example") (v "0.1.0") (h "1gsqhgx5jsav3pqcm86j6qqql6fnfngnz0q82yzdb9kb3rn41gzw") (y #t)))

