(define-module (crates-io ca lc calcer) #:use-module (crates-io))

(define-public crate-calcer-0.2.0 (c (n "calcer") (v "0.2.0") (d (list (d (n "clier") (r "^0.7.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "18ih2y8abi75s71xcnai8vgxa8ryp79c25z30havx9qs78snpxs8")))

(define-public crate-calcer-0.2.1 (c (n "calcer") (v "0.2.1") (d (list (d (n "clier") (r "^0.7.3") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "02vgyhpvh8nszzybgh2fky1h8q3azhh08783mb514fg2yb7pznnw")))

