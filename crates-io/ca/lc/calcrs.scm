(define-module (crates-io ca lc calcrs) #:use-module (crates-io))

(define-public crate-calcrs-2.1.0 (c (n "calcrs") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0v60867i0lyb24lm1m19i546cwwy82cacfccmhgaa2p8dqyppms0") (y #t)))

(define-public crate-calcrs-2.1.1 (c (n "calcrs") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "1zjzqfms0w1li67bzsxz40fhc9xfzpg6zqs33b4xd7yqhv2fs9js")))

(define-public crate-calcrs-2.1.2 (c (n "calcrs") (v "2.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0d0j1c0y8bh9amscgw4j5mvncnnsw6jsxhzdb51x9v8f4fpbmqwm")))

(define-public crate-calcrs-2.2.0 (c (n "calcrs") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0lpcdb62hhzcj8bi59jzws030c2zqpfykxk8wxkvcani9hn1al20")))

(define-public crate-calcrs-2.2.1 (c (n "calcrs") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0hdkhxpmcjs8sfmiaqcmi5kafy3xdyj1h4b9331qcaiqpfj8g3la")))

