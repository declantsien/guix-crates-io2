(define-module (crates-io ca lc calc_rs) #:use-module (crates-io))

(define-public crate-calc_rs-0.1.1 (c (n "calc_rs") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0j11rv7y52i40fdcz53sggvjicj1dr0f6gycw2ny8qaw7n2fwljb") (y #t)))

(define-public crate-calc_rs-0.1.2 (c (n "calc_rs") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "051bgp29mri8fd5qqm5plvaznrsz6kfpzb67kv7q77d967lfls1p") (y #t)))

