(define-module (crates-io ca lc calc_mjp) #:use-module (crates-io))

(define-public crate-calc_mjp-0.1.0 (c (n "calc_mjp") (v "0.1.0") (h "02a1d3dbmlz6l2lrvqn0zx6gr2d5dcyrj602w98ji725hsciy9ac")))

(define-public crate-calc_mjp-0.2.0 (c (n "calc_mjp") (v "0.2.0") (h "1b82zl52kba366zgzlc7mkgdk9p8c7vc25l5b8srh0z0jkb2xh5h")))

(define-public crate-calc_mjp-1.0.0 (c (n "calc_mjp") (v "1.0.0") (h "0fj0594p1hiy0b0fwclsqbhkvksjawh7i98y0nxdsjxj087xbn86") (f (quote (("say") ("default" "calc") ("calc") ("all" "calc" "say"))))))

(define-public crate-calc_mjp-1.1.0 (c (n "calc_mjp") (v "1.1.0") (h "1d93d38w4kb4dsaiic9vsalxvwy8c1mprdpjhflcjxsixvffwr13") (f (quote (("say") ("default" "calc") ("calc") ("all" "calc" "say"))))))

