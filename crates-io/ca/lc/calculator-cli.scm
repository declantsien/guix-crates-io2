(define-module (crates-io ca lc calculator-cli) #:use-module (crates-io))

(define-public crate-calculator-cli-0.1.0 (c (n "calculator-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)))) (h "14i4qssgzpd0mcfiq90bba0p4hg28l4af41w1dmrin782994rh4y")))

(define-public crate-calculator-cli-0.1.1 (c (n "calculator-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)))) (h "0c36c7g23rd60n737p41anxkjk623hbh0ysz4rrp0h594an139lx")))

(define-public crate-calculator-cli-0.1.2 (c (n "calculator-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)))) (h "0daandybaxqa84x95693vmn1hb285gl6byw5a8fazd00g76lf6f9")))

