(define-module (crates-io ca lc calcit_fingertrees) #:use-module (crates-io))

(define-public crate-calcit_fingertrees-0.0.1 (c (n "calcit_fingertrees") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "157izvmz7b1ax0i0gc15zjill5607nmv9l6xpbz9y2fp4ykl0mdg")))

(define-public crate-calcit_fingertrees-0.0.2 (c (n "calcit_fingertrees") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1zq988q0qbjl295a43vfx57qg9sa12q5a74l0svs472bgi8rjzsq")))

(define-public crate-calcit_fingertrees-0.0.3 (c (n "calcit_fingertrees") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "05z2cy8hxchgbgxhy6sjfz5k75wx23b74d5i0r5w67f5g4rbqgmx")))

