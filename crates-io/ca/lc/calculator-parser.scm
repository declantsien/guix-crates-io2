(define-module (crates-io ca lc calculator-parser) #:use-module (crates-io))

(define-public crate-calculator-parser-1.0.0 (c (n "calculator-parser") (v "1.0.0") (h "0w647i1bjxchizsnxjmavffcadzmww2nb2i120lcm51kjq50d6n7")))

(define-public crate-calculator-parser-1.0.1 (c (n "calculator-parser") (v "1.0.1") (h "0a0cxg1xkcq54pwfba3agbmql5k06p519hshppybbzxjwk72cpnl")))

