(define-module (crates-io ca lc calculator_util) #:use-module (crates-io))

(define-public crate-calculator_util-0.1.1 (c (n "calculator_util") (v "0.1.1") (h "03dx3sv4369acbcgs1pyd2rvm3cm55jv26y0r8w19159gb610n05") (y #t)))

(define-public crate-calculator_util-0.1.2 (c (n "calculator_util") (v "0.1.2") (h "0qcwzyykr7b2sxw1k3l3v8jqq9z9whjdfi26839ppxmarzc9njn8")))

