(define-module (crates-io ca lc calculatrs) #:use-module (crates-io))

(define-public crate-calculatrs-0.1.0 (c (n "calculatrs") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yzris1j1hwwwrpxfrl7czk2v2mgllca4nypk06jilvwhc964xng")))

(define-public crate-calculatrs-0.2.0 (c (n "calculatrs") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l6mz38c3k3clsiqzh8bpxs68daygxk2420zh8ajrrfyyl5jpldj")))

(define-public crate-calculatrs-0.2.1 (c (n "calculatrs") (v "0.2.1") (d (list (d (n "lalrpop") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dic7dnlqyn0i192ls37mlir6rg192f70xlqhrjvj789nazi37hg")))

(define-public crate-calculatrs-0.2.2 (c (n "calculatrs") (v "0.2.2") (d (list (d (n "lalrpop") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fqpxs7xy2fgyiygyw4ywm27rwjaj5yyz95f25bzyykz95y384pm")))

(define-public crate-calculatrs-0.2.3 (c (n "calculatrs") (v "0.2.3") (d (list (d (n "lalrpop") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q230gmwg2p43hv5qpv4w5n7fcjkglnga8x5q7ian689s5jp3w1x")))

