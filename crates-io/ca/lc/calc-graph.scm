(define-module (crates-io ca lc calc-graph) #:use-module (crates-io))

(define-public crate-calc-graph-0.1.0 (c (n "calc-graph") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "1rddgmkima5m6hdmqjp66gidnm4bs2qqmgskipsrskdhjvayrr6y")))

(define-public crate-calc-graph-0.2.0 (c (n "calc-graph") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "024q93awfkg8kscsbrl9vc74f5m0k78z6wdqwqvg0m51ldvyf0hi")))

(define-public crate-calc-graph-0.3.0 (c (n "calc-graph") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "12lpc9xl1yawcnqpkg7gi1x7910x6iabrbizsl0h6ssi7g8r8x88")))

(define-public crate-calc-graph-0.3.1 (c (n "calc-graph") (v "0.3.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "0m0bf8pmby9nmg8b46wywk7m71h1p7g14prap7v9sd0g5m55cjjs")))

(define-public crate-calc-graph-0.3.2 (c (n "calc-graph") (v "0.3.2") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "10vm2pflgf9lvcvcvplcznx1d4vxqpgc26ybhci0pmcr7wf04jnv")))

(define-public crate-calc-graph-0.3.3 (c (n "calc-graph") (v "0.3.3") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "00lhxgp1s2xlk44x0bbjc3ygq6mfciymp06frqlif275aq1wrpms")))

(define-public crate-calc-graph-0.3.4 (c (n "calc-graph") (v "0.3.4") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "11mb66940hwvhw2nzhh6min8ylmdjmimvsy7f2v6i3h45svfzb7i")))

(define-public crate-calc-graph-0.4.0 (c (n "calc-graph") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "1k0hqz162fa8rligqnwka9wnxiz74fsxrajw44rmrki4vyap424f")))

(define-public crate-calc-graph-0.4.1 (c (n "calc-graph") (v "0.4.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "168015kp3xbyp98yarvxrydf52klwmgz171hbhwrkh138cd23gh5")))

(define-public crate-calc-graph-0.4.2 (c (n "calc-graph") (v "0.4.2") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 1)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "take_mut") (r "^0.2") (d #t) (k 0)))) (h "1w5ny9sd5iwcc2ry7n80p6m4jq3l359ms72pa17la0p5m625k0lb")))

