(define-module (crates-io ca lc calcify) #:use-module (crates-io))

(define-public crate-calcify-0.1.0 (c (n "calcify") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0khqn3sxf4vkysfw00pdh23kgqxvz02r150vyj1lfdz479mmms80")))

(define-public crate-calcify-0.2.0 (c (n "calcify") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0563vz4wnmsnsr8pv5w90han4w095hpmd2digjhwws6mkblja9hh")))

(define-public crate-calcify-0.3.1 (c (n "calcify") (v "0.3.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "186a9azh9fprdkip75jbb4ra4sacmqh904gz2j191m53xhaw7gha")))

(define-public crate-calcify-0.4.0 (c (n "calcify") (v "0.4.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1aiyxw9ww8f3i3zhij15q0sbp9m285355hkb3260ig2pxaidlcx5")))

(define-public crate-calcify-0.5.1 (c (n "calcify") (v "0.5.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0f2f21jx2cys71vfh5s52ky98anl7d3vdr5cfb2qfa14a5fbxci1")))

(define-public crate-calcify-0.5.2 (c (n "calcify") (v "0.5.2") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0yciv7nw99mcakc5xzpdhclsgbx9da4dmgmn4v3gxkpamcyaz3g1")))

(define-public crate-calcify-0.5.3 (c (n "calcify") (v "0.5.3") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0s5s8ajvmmmbjppcnkvm959rqzpwrl6skynmr6xr617c3jidbbiv")))

(define-public crate-calcify-0.5.4 (c (n "calcify") (v "0.5.4") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "1v1n2zj1jwv4cwhhxk0qjnhxpm1xb0zjbv7jiykfphvkjskxg8f8")))

(define-public crate-calcify-0.5.5 (c (n "calcify") (v "0.5.5") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "1bvn46cxmhv82pmw9hfxjimphazzw8r8w3zgijb45q7gmlcvhchb")))

(define-public crate-calcify-0.5.6 (c (n "calcify") (v "0.5.6") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "0x1cimws9dlzd71lfvlpy0nzbjhdx05d91pix2x2gaa9sghpgwx5")))

(define-public crate-calcify-0.5.7 (c (n "calcify") (v "0.5.7") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "0gh00s28hmpdbglb9v1xgdisbk3bvip47x593b4x77qd8ckhk2aq")))

(define-public crate-calcify-0.6.0 (c (n "calcify") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "0sn9z6jbhf69k5kdpn9l51q4a37zj2idcwd5ndaxxcl9p3rkv336")))

(define-public crate-calcify-0.6.1 (c (n "calcify") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "1v2k7klfgyxk993mp3ay9cf6m4g2h2myl9n4d6451qsi0lblaz60") (y #t)))

(define-public crate-calcify-0.6.2 (c (n "calcify") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 2)))) (h "04bwrphqc7xpgw6ysvdlrd8wzxl53w6r1v3nqjlywfrwy1dvacxr")))

(define-public crate-calcify-0.6.3 (c (n "calcify") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)))) (h "1mpg4zqdnl3yfsaqp67mvvga3dmxiy9mwgs0m3y3ik1yg86fqn4f")))

(define-public crate-calcify-0.7.0 (c (n "calcify") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)))) (h "001zkvr91azdkcddv398y0fzfbsc2bwbxx3v067f3m5kvj36lvha")))

(define-public crate-calcify-0.8.0 (c (n "calcify") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)))) (h "13qs70rgmf1akj9vk3cy9sbhrhffzgakqqnmc9ix221z9nhp07yg")))

(define-public crate-calcify-0.8.1 (c (n "calcify") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)))) (h "03lcid9837hhsihccvxrrhav589r7qkkvy1qgl0n09gpl1w9is3h")))

(define-public crate-calcify-0.8.2 (c (n "calcify") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 2)))) (h "0swxf9rvz9y0nncwf5l7jldz8kr5vc5mgry1xk278gykgm45q9nl")))

