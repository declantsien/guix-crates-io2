(define-module (crates-io ca lc calculate) #:use-module (crates-io))

(define-public crate-calculate-0.1.0 (c (n "calculate") (v "0.1.0") (h "1q1kxcb1wkni61hkdi9hzlnf81x553fskzmc3h6psysd8hbk0n8v")))

(define-public crate-calculate-0.2.0 (c (n "calculate") (v "0.2.0") (h "0j6mlhba9hvbabay7rmi18ryf0j1d4wmkjrlpm18fjahvgwrm9ym")))

(define-public crate-calculate-0.2.1 (c (n "calculate") (v "0.2.1") (h "0dcjh9kmgrlf4wvrs7daaahy8ybwifw0106c6q0q6qgw4fp07c0h")))

(define-public crate-calculate-0.3.0 (c (n "calculate") (v "0.3.0") (h "1zh6rb87vhy11l3ri9p836xbfkgk80ayhnzpymjjjvyfvnlhs6xd")))

(define-public crate-calculate-0.4.0 (c (n "calculate") (v "0.4.0") (d (list (d (n "decimal") (r "^2.0.0") (d #t) (k 0)) (d (n "liner") (r "^0.4.2") (d #t) (k 0)))) (h "16x8wkj0mf5bj42v8yq0yapf57r673c03wf96ayb6f4kdn8w5j60")))

(define-public crate-calculate-0.4.1 (c (n "calculate") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "decimal") (r "^2.0.0") (d #t) (k 0)) (d (n "liner") (r "^0.4.2") (d #t) (k 0)))) (h "1vwhq7qs3qr04zrzrp6936782ajfrf0pqb70rak8c0wpf1xwk36r")))

(define-public crate-calculate-0.5.0 (c (n "calculate") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "decimal") (r "^2.0.0") (d #t) (k 0)) (d (n "liner") (r "^0.4.2") (d #t) (k 0)))) (h "105hmwbqvy7nqp5qy7r2b2ygnsnxilbn14f5s8xnxwk0lrfhpxsx")))

(define-public crate-calculate-0.5.1 (c (n "calculate") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "decimal") (r "^2.0.0") (d #t) (k 0)) (d (n "liner") (r "^0.4.2") (d #t) (k 0)))) (h "0z2lb94ppzh6cdikazb7ll7bjfgvqz73li5rpr3vln2q1qqzxqyz")))

