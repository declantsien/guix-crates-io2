(define-module (crates-io ca lc calcite_proc_macros) #:use-module (crates-io))

(define-public crate-calcite_proc_macros-0.1.0 (c (n "calcite_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1blkh83nk31slqrsmckyb97nfajl4r6dlbl5vcyahaa42cjci482") (y #t)))

(define-public crate-calcite_proc_macros-0.1.1 (c (n "calcite_proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0srh584gbnrv7jph3lgyra3fhg4mpxs1rp8rh0mbgfx8p1d4cggq")))

(define-public crate-calcite_proc_macros-0.1.2 (c (n "calcite_proc_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "0r0bk0p447i1azffsmfhn42a07zs85mipawzl9685f08p9inqpy3")))

(define-public crate-calcite_proc_macros-0.1.3 (c (n "calcite_proc_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "176lbzgqiipq7psx8zz7kq4hvqs9mh3yp6s2iapy4949knzvjq97")))

(define-public crate-calcite_proc_macros-0.1.4 (c (n "calcite_proc_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)))) (h "0mqi46wpvb5jhc4xk33srw8ljyy46jw22iz6vympwb1g0grfl8sl")))

