(define-module (crates-io ca lc calc_engine) #:use-module (crates-io))

(define-public crate-calc_engine-0.1.0 (c (n "calc_engine") (v "0.1.0") (h "0a9zwiz54qv5v28qwdxjnavv56c71nih43w7zdic92ijidsm5i4g")))

(define-public crate-calc_engine-0.2.0 (c (n "calc_engine") (v "0.2.0") (h "1cm8awkz0c2h6bfaybmq8lf69qrc4cahzd9fl73i3gx6cnqm2jjr")))

(define-public crate-calc_engine-0.2.1 (c (n "calc_engine") (v "0.2.1") (h "1aw685njmpgkn0s20mxzaf26sldhn6s01amx3bwa39lxacpq7hwr")))

(define-public crate-calc_engine-0.3.5 (c (n "calc_engine") (v "0.3.5") (h "1cg2zjq479x5qp3gh00z366zagc6pm35xlbkzbwbhhcm72xyvkf8")))

(define-public crate-calc_engine-0.4.0 (c (n "calc_engine") (v "0.4.0") (d (list (d (n "auto_correct_n_suggest") (r "^0.1.0") (d #t) (k 0)))) (h "0l6xx6gkffpy204fay2dkb67n4na6n7r6gwnlh2060vz2dd5dlfg") (y #t)))

(define-public crate-calc_engine-0.4.1 (c (n "calc_engine") (v "0.4.1") (d (list (d (n "auto_correct_n_suggest") (r "^0.2.0") (d #t) (k 0)))) (h "0ywhrl74waq48gc2l0yrvmlhwrgy6b7p4r67l7c6ays3k7madjzy")))

