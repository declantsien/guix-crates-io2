(define-module (crates-io ca lc calcmhz) #:use-module (crates-io))

(define-public crate-calcmhz-0.1.0 (c (n "calcmhz") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "034nc39jvi1331n29c11rvl9g7v2cg96h0f0ifppcybvshjvrj30") (r "1.71")))

(define-public crate-calcmhz-0.1.1 (c (n "calcmhz") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x4y7cq533kxan3xz5qjqfv521j04r6445p6ncn5fhcd6l576h9k") (r "1.71")))

(define-public crate-calcmhz-0.1.2 (c (n "calcmhz") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0364i8qiphl76yrxp86pdb4ib5gfdipf2n3wawp0vy9fwp84yliz") (r "1.71")))

(define-public crate-calcmhz-0.1.3 (c (n "calcmhz") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00mhxsq0yqngz6rgx6yh8v4jw8jbndm5cp0kx4ismbdgd93lmnn0") (r "1.71")))

(define-public crate-calcmhz-0.1.4 (c (n "calcmhz") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bf13by0k7fgwrcbxhjzl0wdbwsa7sxn2avpfkzr1v29zgxvv1rr") (r "1.71")))

(define-public crate-calcmhz-0.1.5 (c (n "calcmhz") (v "0.1.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fc1a0fj1fgg12yzzly7f3fdb7i4mdypgb3ir39n062rdpph747p") (r "1.71")))

(define-public crate-calcmhz-0.1.6 (c (n "calcmhz") (v "0.1.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vj8s1w31gvi13jvv7a2wlhvvnd2s86jb6zpxg5pgbhrrr7fkvki") (r "1.71")))

(define-public crate-calcmhz-0.1.7 (c (n "calcmhz") (v "0.1.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0syc0l8chzhygm36irpkzmgq1v4z374z6cra4ji34gklihh43ina") (r "1.71")))

