(define-module (crates-io ca lc calculator_function) #:use-module (crates-io))

(define-public crate-calculator_function-0.1.1 (c (n "calculator_function") (v "0.1.1") (h "1lvrvqhwxg4n9yn315hmnnsx0ja1jb0drcp3xwy8rkjk4sgs1z95")))

(define-public crate-calculator_function-0.1.2 (c (n "calculator_function") (v "0.1.2") (h "0xyml4jvnapy47pf262habrlwp97cny0kvhl9nzc3y7i4r20zq6j")))

