(define-module (crates-io ca pn capnp-rpc) #:use-module (crates-io))

(define-public crate-capnp-rpc-0.0.1 (c (n "capnp-rpc") (v "0.0.1") (d (list (d (n "capnp") (r "^0.0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.1") (d #t) (k 0)))) (h "0cdsym2gzmzcmxsjyndj79bwjan67cg81cc5b5nqavvgqlgczqak")))

(define-public crate-capnp-rpc-0.0.2 (c (n "capnp-rpc") (v "0.0.2") (d (list (d (n "capnp") (r "^0.0.3") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.3") (d #t) (k 0)))) (h "0pnlr62ap5pqpba7abmv342kih176ajl684kl4xs17p40hpfgkgl")))

(define-public crate-capnp-rpc-0.0.3 (c (n "capnp-rpc") (v "0.0.3") (d (list (d (n "capnp") (r "^0.0.5") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.5") (d #t) (k 0)))) (h "0262fyakszn5lqiwsjqzykn614x7ww1jcll2km9kc8m5mn10qnvi")))

(define-public crate-capnp-rpc-0.0.4 (c (n "capnp-rpc") (v "0.0.4") (d (list (d (n "capnp") (r "^0.0.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.10") (d #t) (k 1)))) (h "1i2p9siy4lr8fgyyrbad7jip2pj0s7y33zsnbix7wk21kp7dw4y8")))

(define-public crate-capnp-rpc-0.0.5 (c (n "capnp-rpc") (v "0.0.5") (d (list (d (n "capnp") (r "^0.0.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.11") (d #t) (k 1)))) (h "1q5rz85dijmk1m6094zwfwvcmirzg99syycpj14pkzzkip9827rk")))

(define-public crate-capnp-rpc-0.0.6 (c (n "capnp-rpc") (v "0.0.6") (d (list (d (n "capnp") (r "^0.0.11") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.12") (d #t) (k 1)))) (h "07xmh7j5syd896nn905g0ly02hpzljcjywgv84i1aaj0kb1qc8ka")))

(define-public crate-capnp-rpc-0.0.7 (c (n "capnp-rpc") (v "0.0.7") (d (list (d (n "capnp") (r "^0.0.13") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.14") (d #t) (k 1)))) (h "0lxlmn19m6n49kk4ns4hpklsgzxzmv604k7h2fr8laa301rh1x5x")))

(define-public crate-capnp-rpc-0.0.8 (c (n "capnp-rpc") (v "0.0.8") (d (list (d (n "capnp") (r "^0.0.15") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.16") (d #t) (k 1)))) (h "1v7v6x1v2mxxy98ar49x76j7bkhlsmsd09694d7qxm4kfmh3qf95")))

(define-public crate-capnp-rpc-0.0.9 (c (n "capnp-rpc") (v "0.0.9") (d (list (d (n "capnp") (r "^0.0.16") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.17") (d #t) (k 1)))) (h "03z5vbhs14ax6qh36ws9r0bjawpmsmsgcq4jc75m7wl6a8g7i7nb")))

(define-public crate-capnp-rpc-0.0.10 (c (n "capnp-rpc") (v "0.0.10") (d (list (d (n "capnp") (r "^0.0.17") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.18") (d #t) (k 1)))) (h "05ji7jmdyp53ykv59q8kccm4j2xziv0qa198gywryhlk9v588hj3")))

(define-public crate-capnp-rpc-0.0.11 (c (n "capnp-rpc") (v "0.0.11") (d (list (d (n "capnp") (r "^0.0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.19") (d #t) (k 1)))) (h "1awbhmcjv5phclbmbzzafz9myqcafw9jwqw82dxjyxjlki1n3p8x")))

(define-public crate-capnp-rpc-0.0.12 (c (n "capnp-rpc") (v "0.0.12") (d (list (d (n "capnp") (r "^0.0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.19") (d #t) (k 1)))) (h "0r4yv73l54bykr7wxhz4pl4vshcy22l4phsmznqmfhdv4nalyc71")))

(define-public crate-capnp-rpc-0.0.13 (c (n "capnp-rpc") (v "0.0.13") (d (list (d (n "capnp") (r "^0.0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.20") (d #t) (k 1)))) (h "1177wp5hbgjkx23ckpw7mp0g28c1z1rkpksfl3q8p34kh10f84l0")))

(define-public crate-capnp-rpc-0.0.14 (c (n "capnp-rpc") (v "0.0.14") (d (list (d (n "capnp") (r "^0.0.20") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.21") (d #t) (k 1)))) (h "0kpfymsfqznj0hqaprrqvlnf6xld0cmwqgp7gjg54cdjbinmwpym")))

(define-public crate-capnp-rpc-0.0.15 (c (n "capnp-rpc") (v "0.0.15") (d (list (d (n "capnp") (r "^0.0.21") (d #t) (k 0)) (d (n "capnpc") (r "^0.0.22") (d #t) (k 1)))) (h "0gcg2xy8i1yj8g7f0y05wvxxaaxbx6s1z5br216nnnra5bldfvhb")))

(define-public crate-capnp-rpc-0.1.0 (c (n "capnp-rpc") (v "0.1.0") (d (list (d (n "capnp") (r "^0.1.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.0") (d #t) (k 1)))) (h "0sym5l49br50rpffywy3w8p5g8byfr6402220vih8h9i88gbfvdf")))

(define-public crate-capnp-rpc-0.1.1 (c (n "capnp-rpc") (v "0.1.1") (d (list (d (n "capnp") (r "^0.1.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.1") (d #t) (k 1)))) (h "1zjpi3qk0jw9ghyfq8qzfwncqnxchjfmvy82nnflm8zj0wja5xw6")))

(define-public crate-capnp-rpc-0.1.2 (c (n "capnp-rpc") (v "0.1.2") (d (list (d (n "capnp") (r "^0.1.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.2") (d #t) (k 1)))) (h "0115rsga2ssw133068jjf6s3nhzn06fr56fk1p2hg74yvygbmvvh")))

(define-public crate-capnp-rpc-0.1.3 (c (n "capnp-rpc") (v "0.1.3") (d (list (d (n "capnp") (r "^0.1.3") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.3") (d #t) (k 1)))) (h "1y2ypqpgzf37djm1a09bdn3fn6wdjwmrmk6r34c16spkhi7vcz7b")))

(define-public crate-capnp-rpc-0.1.4 (c (n "capnp-rpc") (v "0.1.4") (d (list (d (n "capnp") (r "^0.1.4") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.4") (d #t) (k 1)))) (h "0wxqllxwlncnbkkj223i88hfrfvzl535y93lq47n2iq8r81nhxgm")))

(define-public crate-capnp-rpc-0.1.5 (c (n "capnp-rpc") (v "0.1.5") (d (list (d (n "capnp") (r "^0.1.4") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.5") (d #t) (k 1)))) (h "0wyyr90n5qyjnpwr8lah60rj82r645ndpcdfl5if6ykk4ql47r0s")))

(define-public crate-capnp-rpc-0.1.6 (c (n "capnp-rpc") (v "0.1.6") (d (list (d (n "capnp") (r "^0.1.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.7") (d #t) (k 1)))) (h "1130gz0xikcblfhfrd3y435i0kgsjwrwzr8p6paa0526rq55yvfp")))

(define-public crate-capnp-rpc-0.1.7 (c (n "capnp-rpc") (v "0.1.7") (d (list (d (n "capnp") (r "^0.1.9") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.9") (d #t) (k 1)))) (h "0yr4ssdcdz8cwxrhlli6x7qjkzk98i6vk54ngkrl8zkykqy3i45k")))

(define-public crate-capnp-rpc-0.1.8 (c (n "capnp-rpc") (v "0.1.8") (d (list (d (n "capnp") (r "^0.1.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.10") (d #t) (k 1)))) (h "1x96mzrpkikjhjs7l6q6i2b924m33scpm1shvmwh1vzxgl4fmrkb")))

(define-public crate-capnp-rpc-0.1.9 (c (n "capnp-rpc") (v "0.1.9") (d (list (d (n "capnp") (r "^0.1.11") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.11") (d #t) (k 1)))) (h "10w7ds05b01clscvb7mgkys0m24hf0h8cgcmcg495f0l8f1qw32m")))

(define-public crate-capnp-rpc-0.1.10 (c (n "capnp-rpc") (v "0.1.10") (d (list (d (n "capnp") (r "^0.1.12") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.12") (d #t) (k 1)))) (h "0b9nikqsmq10brbxjqli8yin9szps4j39455x0h1hcwhrd78bz4a")))

(define-public crate-capnp-rpc-0.1.11 (c (n "capnp-rpc") (v "0.1.11") (d (list (d (n "capnp") (r "^0.1.12") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.13") (d #t) (k 1)))) (h "0aycyyyxcndpwlkzqms7c3bs2kmd3lcnsabqg28wg7i3ai408jlk")))

(define-public crate-capnp-rpc-0.1.12 (c (n "capnp-rpc") (v "0.1.12") (d (list (d (n "capnp") (r "^0.1.13") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.15") (d #t) (k 1)))) (h "0vrxc5yr9hi1wy1k0nv9r1xby09r2qr7jsh40vsjg8d7qcj7qwiv")))

(define-public crate-capnp-rpc-0.1.13 (c (n "capnp-rpc") (v "0.1.13") (d (list (d (n "capnp") (r "^0.1.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.18") (d #t) (k 1)))) (h "0dgg47h9yw5c7902qdj0qkslrl0aj8xk82slb8l62382srvmw5dd")))

(define-public crate-capnp-rpc-0.1.14 (c (n "capnp-rpc") (v "0.1.14") (d (list (d (n "capnp") (r "^0.1.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.19") (d #t) (k 1)))) (h "1rdn2yvrar6v22brxrm2sndv809i3y0f4z50fh7a1csdbz68czkq")))

(define-public crate-capnp-rpc-0.1.15 (c (n "capnp-rpc") (v "0.1.15") (d (list (d (n "capnp") (r "^0.1.20") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.20") (d #t) (k 1)))) (h "1ncfs9v8p8c4sblyqa49asqqnwvkh6yikg7812rxc0xfy5fd2kg3")))

(define-public crate-capnp-rpc-0.1.16 (c (n "capnp-rpc") (v "0.1.16") (d (list (d (n "capnp") (r "^0.1.23") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.22") (d #t) (k 1)))) (h "1m6qgiari8qq66ij9r9b2szq40bbj3bnpvckjhy4yxh5yiv6ri1z")))

(define-public crate-capnp-rpc-0.1.17 (c (n "capnp-rpc") (v "0.1.17") (d (list (d (n "capnp") (r "^0.1.24") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.24") (d #t) (k 1)))) (h "0is3gz2i5l0kih5n31q0zg9k71194v1z7dm38dxs3fhbmyipz7cw")))

(define-public crate-capnp-rpc-0.1.18 (c (n "capnp-rpc") (v "0.1.18") (d (list (d (n "capnp") (r "^0.1.26") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.26") (d #t) (k 1)))) (h "0zd3gxj3abcipwfm6c4p13bw6n1fz23wjq57p887jcn932x05i7i")))

(define-public crate-capnp-rpc-0.1.19 (c (n "capnp-rpc") (v "0.1.19") (d (list (d (n "capnp") (r "^0.1.26") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.26") (d #t) (k 1)))) (h "0n0csdff3l17kq3dqlk1bdxadzi24i0w6klv6g0df6s8as526w0m")))

(define-public crate-capnp-rpc-0.1.20 (c (n "capnp-rpc") (v "0.1.20") (d (list (d (n "capnp") (r "^0.1.27") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.27") (d #t) (k 1)))) (h "0aqry0zxcn3cavzk2ljdi9jg0y19sh0v20byn875nzk1n1z1qgvs")))

(define-public crate-capnp-rpc-0.1.21 (c (n "capnp-rpc") (v "0.1.21") (d (list (d (n "capnp") (r "^0.1.29") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.28") (d #t) (k 1)))) (h "1h2z3qnwl9fpj8ihd87nwjv5vfk8kd7g5xg4836zbkb1bj2x0n2s")))

(define-public crate-capnp-rpc-0.1.22 (c (n "capnp-rpc") (v "0.1.22") (d (list (d (n "capnp") (r "^0.1.33") (d #t) (k 0)) (d (n "capnpc") (r "^0.1.29") (d #t) (k 1)))) (h "02kwp39xj089lvqzjqmr14b01ka1dx2nmmanq5awg2jas8jgbmrz")))

(define-public crate-capnp-rpc-0.2.0 (c (n "capnp-rpc") (v "0.2.0") (d (list (d (n "capnp") (r "^0.2.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.2.0") (d #t) (k 1)))) (h "0by6bhj0ciy4s9p59h0sgjkhi4sbq0y30g9k9pqq7ahhcbfayg2x")))

(define-public crate-capnp-rpc-0.2.1 (c (n "capnp-rpc") (v "0.2.1") (d (list (d (n "capnp") (r "^0.2.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.2.0") (d #t) (k 1)))) (h "14xn6aaryz26pa6aigdzy18dv1v0cl51mhdqb3ql3m5fqdpb85md")))

(define-public crate-capnp-rpc-0.2.2 (c (n "capnp-rpc") (v "0.2.2") (d (list (d (n "capnp") (r "^0.2.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.2.2") (d #t) (k 1)))) (h "0snx9d7cy5dqa2k3g8bhblmjl57fjmdv5fwljylzg5qbg34jmc3w")))

(define-public crate-capnp-rpc-0.2.3 (c (n "capnp-rpc") (v "0.2.3") (d (list (d (n "capnp") (r "^0.2.4") (d #t) (k 0)) (d (n "capnpc") (r "^0.2.3") (d #t) (k 1)))) (h "17j9ji6z2l26lzypc7vlm3sbd5avi8fixnh3564y6n2ymlgvx3w6")))

(define-public crate-capnp-rpc-0.3.0 (c (n "capnp-rpc") (v "0.3.0") (d (list (d (n "capnp") (r "^0.3.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.3.0") (d #t) (k 1)))) (h "1xv9w60mkh36q5yqfb1ic94r9x820giyagss80j05b6qisk2wdqy")))

(define-public crate-capnp-rpc-0.3.1 (c (n "capnp-rpc") (v "0.3.1") (d (list (d (n "capnp") (r "^0.3.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.3.0") (d #t) (k 1)))) (h "1xjsx50lwsbf4f40smcz6m5l2jsw5wrqinsbbysnfnh950m1jycd")))

(define-public crate-capnp-rpc-0.3.2 (c (n "capnp-rpc") (v "0.3.2") (d (list (d (n "capnp") (r "^0.3.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.3.1") (d #t) (k 1)))) (h "1prvq3q9jp6bcr1pndizr9ww70gd1v2ww1m8nf6dcw1qq2w0pdd9")))

(define-public crate-capnp-rpc-0.4.0 (c (n "capnp-rpc") (v "0.4.0") (d (list (d (n "capnp") (r "^0.4.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.4.0") (d #t) (k 1)))) (h "1r6d24ib5mwbh0gpx1rjp65w4zya074m29aha72m4lk5g83sk0a6")))

(define-public crate-capnp-rpc-0.5.0 (c (n "capnp-rpc") (v "0.5.0") (d (list (d (n "capnp") (r "^0.5.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.5.0") (d #t) (k 1)))) (h "1lkildlj9cv97cz3g56c9lb1j1cc09nwxa5q4l9c5mb034na272p")))

(define-public crate-capnp-rpc-0.6.0 (c (n "capnp-rpc") (v "0.6.0") (d (list (d (n "capnp") (r "^0.6") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.6") (d #t) (k 1)) (d (n "gj") (r "^0.1") (d #t) (k 0)))) (h "0r81qcs28mkkkqx5d46f1qrg5nakc0l4iw1zwv8gbnvg2nk1a529")))

(define-public crate-capnp-rpc-0.6.1 (c (n "capnp-rpc") (v "0.6.1") (d (list (d (n "capnp") (r "^0.6.1") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.6") (d #t) (k 1)) (d (n "gj") (r "^0.1") (d #t) (k 0)))) (h "0w0zkv12w53aifv4gnbs9hs2kvlm7rxc9mjgyysbhwdq50b7nn3d")))

(define-public crate-capnp-rpc-0.6.2 (c (n "capnp-rpc") (v "0.6.2") (d (list (d (n "capnp") (r "^0.6.1") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.6") (d #t) (k 1)) (d (n "gj") (r "^0.1") (d #t) (k 0)))) (h "0j8amwka627vjj06rjg7hanjm859gnc66a50s5gvwk57blvjr6k3")))

(define-public crate-capnp-rpc-0.6.3 (c (n "capnp-rpc") (v "0.6.3") (d (list (d (n "capnp") (r "^0.6.2") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.6") (d #t) (k 1)) (d (n "gj") (r "^0.1") (d #t) (k 0)))) (h "1kaa8k3aacfza6ypls50d3f88y9mnwrjyg5g0db8gghaqakarfqg")))

(define-public crate-capnp-rpc-0.7.0 (c (n "capnp-rpc") (v "0.7.0") (d (list (d (n "capnp") (r "^0.7") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "1w3ykr950nkwv61ndj7fx1qwrzvfzacd0188icckn8fbvfba8fqk")))

(define-public crate-capnp-rpc-0.7.1 (c (n "capnp-rpc") (v "0.7.1") (d (list (d (n "capnp") (r "^0.7") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "099zw7ccxyaqik232q0qynz3aii200jhmcpgxjv3fhf8afq0gic3")))

(define-public crate-capnp-rpc-0.7.2 (c (n "capnp-rpc") (v "0.7.2") (d (list (d (n "capnp") (r "^0.7") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "01hyk35g7b667n0i7ibc9y4ylpd2592w23sgahwhffa5k800lih8")))

(define-public crate-capnp-rpc-0.7.3 (c (n "capnp-rpc") (v "0.7.3") (d (list (d (n "capnp") (r "^0.7") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "1721rjprb9lqc5v3pxls06mdzgsz8vbxiyjnm7024s3sd91pa81m")))

(define-public crate-capnp-rpc-0.7.4 (c (n "capnp-rpc") (v "0.7.4") (d (list (d (n "capnp") (r "^0.7") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-gj") (r "^0.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "0mf9mgc5dfb252mz018ri7b69jnx63y674imw7mapi8hav2c0aib")))

(define-public crate-capnp-rpc-0.8.0 (c (n "capnp-rpc") (v "0.8.0") (d (list (d (n "capnp") (r "^0.8") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.8") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "00x9c3gjziv9h5wd0fg0xmz4s9h081pd18794yd93yvi0ynbp43r")))

(define-public crate-capnp-rpc-0.8.1 (c (n "capnp-rpc") (v "0.8.1") (d (list (d (n "capnp") (r "^0.8") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.8") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0265ar9m89zsmcrn30gp5c8s4y30n397jm44c39vvvs69wiisrnk")))

(define-public crate-capnp-rpc-0.8.2 (c (n "capnp-rpc") (v "0.8.2") (d (list (d (n "capnp") (r "^0.8") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.8") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1bg77ilj329gzbpq00n4lhfbx3p6wqcazjrc4w01ah40r8dq4661")))

(define-public crate-capnp-rpc-0.8.3 (c (n "capnp-rpc") (v "0.8.3") (d (list (d (n "capnp") (r "^0.8.13") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.8.8") (d #t) (k 1)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)))) (h "03d05dc9i9y3z5cvyp03dpfy8b40q2fqpv5cs6cp0b826irlmwbh")))

(define-public crate-capnp-rpc-0.9.0 (c (n "capnp-rpc") (v "0.9.0") (d (list (d (n "capnp") (r "^0.9.0") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.9.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.9.0") (d #t) (k 1)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)))) (h "0ih0awlnz2n395zadv1gc4spzcdzii90jx6w7vmy0fmvj8dmydyf")))

(define-public crate-capnp-rpc-0.10.0 (c (n "capnp-rpc") (v "0.10.0") (d (list (d (n "capnp") (r "^0.10") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "capnp-futures") (r "^0.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.10") (d #t) (k 1)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)))) (h "1j6xg7yays1hlm1045wviyn1642yvvi2p4kba26yk07a0kafr3jn")))

(define-public crate-capnp-rpc-0.11.0 (c (n "capnp-rpc") (v "0.11.0") (d (list (d (n "capnp") (r "^0.11.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.11.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.11.0") (d #t) (k 1)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "1lsvz2h366vkgkyfhs5k6kwc7zv3sb1fxq8hcym2iyk5rax9hx29")))

(define-public crate-capnp-rpc-0.12.0 (c (n "capnp-rpc") (v "0.12.0") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.12.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.12.0") (d #t) (k 1)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "0bblh6nq75pkppd4ljk8f6dcblxqq48n96x7ac9in4p41jdy2rh9")))

(define-public crate-capnp-rpc-0.12.1 (c (n "capnp-rpc") (v "0.12.1") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "1cav4hyggvaa6shl74hlckmc0zc5yd9bvi0kiw9inp5676cpnyw9")))

(define-public crate-capnp-rpc-0.12.2 (c (n "capnp-rpc") (v "0.12.2") (d (list (d (n "capnp") (r "^0.12.2") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "16d446hhivsnl5v56qvzdfpp5vdg7p5wynh9jfwaianiyxrk0v98")))

(define-public crate-capnp-rpc-0.12.3 (c (n "capnp-rpc") (v "0.12.3") (d (list (d (n "capnp") (r "^0.12.2") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "06sfy78n6c15xppsn00xlgda56rlw4bx2mskbsjvrizvizlbpgdi")))

(define-public crate-capnp-rpc-0.13.0 (c (n "capnp-rpc") (v "0.13.0") (d (list (d (n "capnp") (r "^0.13.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "0kgyl4pllfwm1ss1va12x2skr0kq7sx267gws1znrmzxpbw17m79")))

(define-public crate-capnp-rpc-0.13.1 (c (n "capnp-rpc") (v "0.13.1") (d (list (d (n "capnp") (r "^0.13.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "17p0y0yk68pzsnpmaklhiqrrlrrv0ld8nhbg4qflmgibshi8b69p")))

(define-public crate-capnp-rpc-0.14.0 (c (n "capnp-rpc") (v "0.14.0") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.14.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "147sbdydl79f4igk4acygahcr8my9lxjhnl10i4h1d1cfvp97yki")))

(define-public crate-capnp-rpc-0.14.1 (c (n "capnp-rpc") (v "0.14.1") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.14.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "0pm9bjw481lw1zp8lmzkpsjrb85lbjg5s46piqbc3wk8dzwifksc")))

(define-public crate-capnp-rpc-0.15.0 (c (n "capnp-rpc") (v "0.15.0") (d (list (d (n "capnp") (r "^0.15.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "0cdwsylxibwn20gibmfrr52aab4jdzdwspd4rjhzphs83wqv1fcn")))

(define-public crate-capnp-rpc-0.15.1 (c (n "capnp-rpc") (v "0.15.1") (d (list (d (n "capnp") (r "^0.15.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "1ixx493z2p6gi32hyfv0zw0f3lgrxrnhx4cvfrrrr12gy4jv2x02")))

(define-public crate-capnp-rpc-0.16.0 (c (n "capnp-rpc") (v "0.16.0") (d (list (d (n "capnp") (r "^0.16.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.16.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "04dsyxjnwkn590d289z2hzk2sdbkmc78pyp436x36rdy2wmhkwl6")))

(define-public crate-capnp-rpc-0.16.1 (c (n "capnp-rpc") (v "0.16.1") (d (list (d (n "capnp") (r "^0.16.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.16.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "1k8azw6xcim3xv25dklr0mwjrlhgvbbxv4bysxkxa7fsn407f2b2")))

(define-public crate-capnp-rpc-0.16.2 (c (n "capnp-rpc") (v "0.16.2") (d (list (d (n "capnp") (r "^0.16.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.16.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "1rilhmk4rm03wqi0h8b43pjfi0dgva0cqj23pfww07fx7d1c16k1")))

(define-public crate-capnp-rpc-0.17.0 (c (n "capnp-rpc") (v "0.17.0") (d (list (d (n "capnp") (r "^0.17.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.17.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "0y878gx7r59j8gbrhldavshxi92sfxg7lhjhqfy1qj9yg1lyif4s")))

(define-public crate-capnp-rpc-0.18.0 (c (n "capnp-rpc") (v "0.18.0") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.18.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "0400mpdijax2dg29m688liccall6xxwrplr7w8l0cq3l1yg7mf4l")))

(define-public crate-capnp-rpc-0.19.0 (c (n "capnp-rpc") (v "0.19.0") (d (list (d (n "capnp") (r "^0.19.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.19.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "05hahpmfjnn49mzv4b2zp4y6si9g6yvjy6kgm2ia9apndjcbl78p")))

(define-public crate-capnp-rpc-0.19.1 (c (n "capnp-rpc") (v "0.19.1") (d (list (d (n "capnp") (r "^0.19.0") (d #t) (k 0)) (d (n "capnp-futures") (r "^0.19.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)))) (h "1qpnh33441vhpp625ih43dvv7bpqm7ifigw65qw6pp7vg2vglz18")))

