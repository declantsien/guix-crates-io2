(define-module (crates-io ca pn capnp_conv) #:use-module (crates-io))

(define-public crate-capnp_conv-0.1.0 (c (n "capnp_conv") (v "0.1.0") (d (list (d (n "capnp") (r "^0.14") (d #t) (k 0)) (d (n "capnp_conv_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0jyddnjfyahbvh5ccykcsjml632r95vmcxhxwjsn0d2qkrlcbay4")))

(define-public crate-capnp_conv-0.2.0 (c (n "capnp_conv") (v "0.2.0") (d (list (d (n "capnp") (r "^0.17") (d #t) (k 0)) (d (n "capnp_conv_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0ryby1c33w68k1kj92dawbslyrxxv5hy58qcmv4d45pxydh3ki5l")))

(define-public crate-capnp_conv-0.3.0 (c (n "capnp_conv") (v "0.3.0") (d (list (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnp_conv_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0qpypn2jw20p1vmz293hjhzg0ikl2lhfvlbrx99ljy4aqriycma8")))

