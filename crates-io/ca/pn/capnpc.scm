(define-module (crates-io ca pn capnpc) #:use-module (crates-io))

(define-public crate-capnpc-0.0.1 (c (n "capnpc") (v "0.0.1") (d (list (d (n "capnp") (r "^0.0.1") (d #t) (k 0)))) (h "1b0krkmb8j6gw8vapmnlrzkdb0jy8l84mwxjcsq7c8d64fpylh9w")))

(define-public crate-capnpc-0.0.2 (c (n "capnpc") (v "0.0.2") (d (list (d (n "capnp") (r "~0.0.2") (d #t) (k 0)))) (h "1pr1rq7gs8ahs343vgchfqgnzj4s7q03qb1hw4pj0w54sh127nxh")))

(define-public crate-capnpc-0.0.3 (c (n "capnpc") (v "0.0.3") (d (list (d (n "capnp") (r "^0.0.3") (d #t) (k 0)))) (h "1f5ag8vh5bv3fwjbkpz2czh45jc13i19dgc31wzw7lvn1w1dqdps")))

(define-public crate-capnpc-0.0.4 (c (n "capnpc") (v "0.0.4") (d (list (d (n "capnp") (r "^0.0.4") (d #t) (k 0)))) (h "1nb563nc8k976lqwyq74757h3i0jb9h12bjcwkp7g173argwb1iy")))

(define-public crate-capnpc-0.0.5 (c (n "capnpc") (v "0.0.5") (d (list (d (n "capnp") (r "^0.0.5") (d #t) (k 0)))) (h "1c3xfdqily0042lc0ay5di04pfk7ljba4cgvni110a3r5s7z2n11")))

(define-public crate-capnpc-0.0.6 (c (n "capnpc") (v "0.0.6") (d (list (d (n "capnp") (r "^0.0.6") (d #t) (k 0)))) (h "1s7vzk9kfj0z4jl7v2fxbmrqa95r4q9kmjh2fy7igr16g2c02hlr")))

(define-public crate-capnpc-0.0.7 (c (n "capnpc") (v "0.0.7") (d (list (d (n "capnp") (r "^0.0.6") (d #t) (k 0)))) (h "1i08cy78yabnpvnxrymg3sl5bxdhbx2jdg3s6y2b3v86q9ix79fd")))

(define-public crate-capnpc-0.0.8 (c (n "capnpc") (v "0.0.8") (d (list (d (n "capnp") (r "^0.0.8") (d #t) (k 0)))) (h "19hslflzlzx4qr13af7ikz0fa92w4vz8bw60q1hiadp0f43jvkmg")))

(define-public crate-capnpc-0.0.9 (c (n "capnpc") (v "0.0.9") (d (list (d (n "capnp") (r "^0.0.9") (d #t) (k 0)))) (h "1lgyklhc80p9dwlf00cmhfgfq2w7cw2hz9jgf87nplj6gwvwl0pg")))

(define-public crate-capnpc-0.0.10 (c (n "capnpc") (v "0.0.10") (d (list (d (n "capnp") (r "^0.0.10") (d #t) (k 0)))) (h "0r3834yn8kvl1w5wfv3c1d5v6w97mr090nkj8hvylhyfx2b360pj")))

(define-public crate-capnpc-0.0.11 (c (n "capnpc") (v "0.0.11") (d (list (d (n "capnp") (r "^0.0.10") (d #t) (k 0)))) (h "1374dyjw7h22mxvz198rnkxv4agv6q8sa2qyam1j4f8q0n0d1rmf")))

(define-public crate-capnpc-0.0.12 (c (n "capnpc") (v "0.0.12") (d (list (d (n "capnp") (r "^0.0.11") (d #t) (k 0)))) (h "06wxchy2ila5fnp8zdxa1dkiafm46kffz4j2rwg4mn65zmv6d5i4")))

(define-public crate-capnpc-0.0.14 (c (n "capnpc") (v "0.0.14") (d (list (d (n "capnp") (r "^0.0.13") (d #t) (k 0)))) (h "083vs4kma3jm6ydbn8w9lhn6r71f6dbzgjz8d9sp8sbbvffqlidk")))

(define-public crate-capnpc-0.0.15 (c (n "capnpc") (v "0.0.15") (d (list (d (n "capnp") (r "^0.0.14") (d #t) (k 0)))) (h "09279rra5sa309xpxihmyr9srf3k6g1pbh370d2asdsxyc45nnm1")))

(define-public crate-capnpc-0.0.16 (c (n "capnpc") (v "0.0.16") (d (list (d (n "capnp") (r "^0.0.15") (d #t) (k 0)))) (h "1kcjc86j8i7rrdsh9n5nij7c5y9jw1b99zbxi42k2vhggk0hd3zz")))

(define-public crate-capnpc-0.0.17 (c (n "capnpc") (v "0.0.17") (d (list (d (n "capnp") (r "^0.0.16") (d #t) (k 0)))) (h "121bilarpghwk8wf9c2628y969hnjpdjvl8r8080z8kmrfpq3gkp")))

(define-public crate-capnpc-0.0.18 (c (n "capnpc") (v "0.0.18") (d (list (d (n "capnp") (r "^0.0.17") (d #t) (k 0)))) (h "1ipzq09qhiv4qfi5xsblvh0wd1y2360rcf1x4vn95fcqyf94kjci")))

(define-public crate-capnpc-0.0.19 (c (n "capnpc") (v "0.0.19") (d (list (d (n "capnp") (r "^0.0.18") (d #t) (k 0)))) (h "1lphb3j8fvs4qsys8yr6ifzyp98vgxcc811f51swdkvqpa4apzyy")))

(define-public crate-capnpc-0.0.20 (c (n "capnpc") (v "0.0.20") (d (list (d (n "capnp") (r "^0.0.19") (d #t) (k 0)))) (h "152vasxvlqk662xa6674jwwsmrv9p60yg996y27q9pc0b1zgxy0g")))

(define-public crate-capnpc-0.0.21 (c (n "capnpc") (v "0.0.21") (d (list (d (n "capnp") (r "^0.0.20") (d #t) (k 0)))) (h "1x1l3xldy34kazj06mamb0lg619i6cpn20p2vzw0ng38b0kyzfq4")))

(define-public crate-capnpc-0.0.22 (c (n "capnpc") (v "0.0.22") (d (list (d (n "capnp") (r "^0.0.21") (d #t) (k 0)))) (h "0xb33wwvsb4v484xwqrkw5pbbv0101izhgbnchnhd0h24y11q00p")))

(define-public crate-capnpc-0.1.0 (c (n "capnpc") (v "0.1.0") (d (list (d (n "capnp") (r "^0.1.0") (d #t) (k 0)))) (h "17nza6z939an1v2r0qj2fmifzh0pfxxnf48br10rr07i5zk2ph4n")))

(define-public crate-capnpc-0.1.1 (c (n "capnpc") (v "0.1.1") (d (list (d (n "capnp") (r "^0.1.1") (d #t) (k 0)))) (h "0mz516i2w6j50dckz601c64cc1dl166cy4m4chsak3f4k0fqqnh0")))

(define-public crate-capnpc-0.1.2 (c (n "capnpc") (v "0.1.2") (d (list (d (n "capnp") (r "^0.1.2") (d #t) (k 0)))) (h "0xsdcbyjk9yc113fpfr2rn163xv9p1bw0chv7d1bf3dv2nghxsd1")))

(define-public crate-capnpc-0.1.3 (c (n "capnpc") (v "0.1.3") (d (list (d (n "capnp") (r "^0.1.3") (d #t) (k 0)))) (h "15821yvv6jhayfrs5yjpdpr8zagndl057kwrj8p0ffrcfqh70aj8")))

(define-public crate-capnpc-0.1.4 (c (n "capnpc") (v "0.1.4") (d (list (d (n "capnp") (r "^0.1.4") (d #t) (k 0)))) (h "0ahypywflsfm0x8ql4909b27x1kkj8cv254ijk09ksi3lavc86kj")))

(define-public crate-capnpc-0.1.5 (c (n "capnpc") (v "0.1.5") (d (list (d (n "capnp") (r "^0.1.4") (d #t) (k 0)))) (h "0r5w1bp94lf27mhwnwcrxrv9159sn36gm3j5vlpakhs0yq8xyk4p")))

(define-public crate-capnpc-0.1.6 (c (n "capnpc") (v "0.1.6") (d (list (d (n "capnp") (r "^0.1.4") (d #t) (k 0)))) (h "1wj2ss0amzbjqjhjmb42nyy9assfbdf0d4kvsg572jbwyk451k63")))

(define-public crate-capnpc-0.1.7 (c (n "capnpc") (v "0.1.7") (d (list (d (n "capnp") (r "^0.1.7") (d #t) (k 0)))) (h "148gpsab5garsjlbj4366awnz9j1l0jq95gmdz9bi12lyc7jjqfv")))

(define-public crate-capnpc-0.1.8 (c (n "capnpc") (v "0.1.8") (d (list (d (n "capnp") (r "^0.1.8") (d #t) (k 0)))) (h "17da2hmdqz121j1nfa58v6xw54sjfmwbf422c0p6zz29hi5489bg")))

(define-public crate-capnpc-0.1.9 (c (n "capnpc") (v "0.1.9") (d (list (d (n "capnp") (r "^0.1.9") (d #t) (k 0)))) (h "1ybdxiwdmc6wxkwpcwfs2dkg11vl6inx01xfhw18i5ss3641ylq9")))

(define-public crate-capnpc-0.1.10 (c (n "capnpc") (v "0.1.10") (d (list (d (n "capnp") (r "^0.1.10") (d #t) (k 0)))) (h "1356g84wfzn9m1kcrc0y2mlad5d4ig3vqxqp60nwpk4f4d02n5rw")))

(define-public crate-capnpc-0.1.11 (c (n "capnpc") (v "0.1.11") (d (list (d (n "capnp") (r "^0.1.11") (d #t) (k 0)))) (h "0dy4zfglf30h2afc5s9b74ddn798vadmlvv8wjwvs6c421pvsg10")))

(define-public crate-capnpc-0.1.12 (c (n "capnpc") (v "0.1.12") (d (list (d (n "capnp") (r "^0.1.12") (d #t) (k 0)))) (h "15pbsif6gadw3wi1qal6jqdxngyshf0n899i2vb0pd1wp8f3asnv")))

(define-public crate-capnpc-0.1.13 (c (n "capnpc") (v "0.1.13") (d (list (d (n "capnp") (r "^0.1.12") (d #t) (k 0)))) (h "0r4ix03wr4zhw382wjg28408ps7z86x1jxkp391b9wrv0jy68shw")))

(define-public crate-capnpc-0.1.14 (c (n "capnpc") (v "0.1.14") (d (list (d (n "capnp") (r "^0.1.12") (d #t) (k 0)))) (h "1vxfgfbffvd5qfz6sla9rg7gqzh2h7l41449ps8h89m1nr95282z")))

(define-public crate-capnpc-0.1.15 (c (n "capnpc") (v "0.1.15") (d (list (d (n "capnp") (r "^0.1.13") (d #t) (k 0)))) (h "0gdh0zg99q93k7x8adwzglisvpaq5v4raf7561p4xf70qdbnhp7c")))

(define-public crate-capnpc-0.1.16 (c (n "capnpc") (v "0.1.16") (d (list (d (n "capnp") (r "^0.1.15") (d #t) (k 0)))) (h "1i7jkrv05y4m1y89wi9q4lsar8yiq82jizv9jshpvvl9dmlcvkn7")))

(define-public crate-capnpc-0.1.17 (c (n "capnpc") (v "0.1.17") (d (list (d (n "capnp") (r "^0.1.16") (d #t) (k 0)))) (h "1zzf73w467gwn5fzzkp7azrg77rajpx3ixyrlvj4p82kw0l863as")))

(define-public crate-capnpc-0.1.18 (c (n "capnpc") (v "0.1.18") (d (list (d (n "capnp") (r "^0.1.18") (d #t) (k 0)))) (h "0w7c1xlyvdb6k29rb12vpzwd3w0mqjgskl3j4x5nh70w6pw7pfic")))

(define-public crate-capnpc-0.1.19 (c (n "capnpc") (v "0.1.19") (d (list (d (n "capnp") (r "^0.1.19") (d #t) (k 0)))) (h "12mndvpcaqj63i4592xhbf3pxpb3sjjmxri287r8ibq84mp8rbzs")))

(define-public crate-capnpc-0.1.20 (c (n "capnpc") (v "0.1.20") (d (list (d (n "capnp") (r "^0.1.20") (d #t) (k 0)))) (h "1csnkbp3dgx747273xxqas53i2k83ngfzgsxvfvagsqi0vjlbnjc")))

(define-public crate-capnpc-0.1.21 (c (n "capnpc") (v "0.1.21") (d (list (d (n "capnp") (r "^0.1.21") (d #t) (k 0)))) (h "0pjn669nsv0wljmbjff998xrw0s0agq1wg0pdlalh3g7xpj1nls0")))

(define-public crate-capnpc-0.1.22 (c (n "capnpc") (v "0.1.22") (d (list (d (n "capnp") (r "^0.1.23") (d #t) (k 0)))) (h "04ydx9kwqqdksyyf9sj8hhhvfw4xlaz2w2fpxb1k9nkr0qxbiirz")))

(define-public crate-capnpc-0.1.23 (c (n "capnpc") (v "0.1.23") (d (list (d (n "capnp") (r "^0.1.23") (d #t) (k 0)))) (h "11211v9m106lgfridp1178f4x6kg4qrs8hkhn0n0l3qjrck1dijl")))

(define-public crate-capnpc-0.1.24 (c (n "capnpc") (v "0.1.24") (d (list (d (n "capnp") (r "^0.1.24") (d #t) (k 0)))) (h "1z5dsf2mgaq9x1cf0p5h2l6rgijl155sz6a2ssp2g9754fp08ann")))

(define-public crate-capnpc-0.1.25 (c (n "capnpc") (v "0.1.25") (d (list (d (n "capnp") (r "^0.1.25") (d #t) (k 0)))) (h "0xcpx9xz6lx8ryzhz8dsbgnd5gyj24np7qmmd71i100bc9h5w3gg")))

(define-public crate-capnpc-0.1.26 (c (n "capnpc") (v "0.1.26") (d (list (d (n "capnp") (r "^0.1.26") (d #t) (k 0)))) (h "1ddyng5ah2508m4qcgdlraps9i1s81yqcdyb05l1y0bxhmry9ff1")))

(define-public crate-capnpc-0.1.27 (c (n "capnpc") (v "0.1.27") (d (list (d (n "capnp") (r "^0.1.27") (d #t) (k 0)))) (h "0xjk4cgybzsjmvlfhhhc167mal8wgcrgm01qxyjmyqxmiajr90iz")))

(define-public crate-capnpc-0.1.28 (c (n "capnpc") (v "0.1.28") (d (list (d (n "capnp") (r "^0.1.28") (d #t) (k 0)))) (h "0lx66lyda6w2adl77rpn524lkvn3l073zdzh2154fxp26abs59yl")))

(define-public crate-capnpc-0.1.29 (c (n "capnpc") (v "0.1.29") (d (list (d (n "capnp") (r "^0.1.33") (d #t) (k 0)))) (h "1iiina2wk5il99lbwa8srl58z28qp4kmw3zn7h47cjx113vl7757")))

(define-public crate-capnpc-0.2.0 (c (n "capnpc") (v "0.2.0") (d (list (d (n "capnp") (r "^0.2.0") (d #t) (k 0)))) (h "1ijdjrn3in96nrsfzjiwi3ly59avy66s3ra5pslsxhkisqn11hzv")))

(define-public crate-capnpc-0.2.1 (c (n "capnpc") (v "0.2.1") (d (list (d (n "capnp") (r "^0.2.1") (d #t) (k 0)))) (h "0wd4gxjggy9imp6i7gnjj6jd01sdk4c0yjw5314zdv8gsx880r34")))

(define-public crate-capnpc-0.2.2 (c (n "capnpc") (v "0.2.2") (d (list (d (n "capnp") (r "^0.2.2") (d #t) (k 0)))) (h "0nbs2yhpv72dxr483ykbl282fiyx4pp2193nidlywh6r22phdd4k")))

(define-public crate-capnpc-0.2.3 (c (n "capnpc") (v "0.2.3") (d (list (d (n "capnp") (r "^0.2.4") (d #t) (k 0)))) (h "0yya026qz5v0gf9kzmwlfyijlcflvffy09ymjk88jfj8klc4bb0m")))

(define-public crate-capnpc-0.2.4 (c (n "capnpc") (v "0.2.4") (d (list (d (n "capnp") (r "^0.2.4") (d #t) (k 0)))) (h "0ybjb17q1p27w3wl3f7vflivxqgawx33a13c3rrla8lv75vc83hp")))

(define-public crate-capnpc-0.2.5 (c (n "capnpc") (v "0.2.5") (d (list (d (n "capnp") (r "^0.2.5") (d #t) (k 0)))) (h "0z2i6v79ibl1wzr51xc0j1ixwpah6j35ga5pmvl9h5llqczs8g8j")))

(define-public crate-capnpc-0.3.0 (c (n "capnpc") (v "0.3.0") (d (list (d (n "capnp") (r "^0.3.0") (d #t) (k 0)))) (h "0fha7a297nf7hycmpwryj7fsmjv905lsk57rrgkiz39kp0hxzp3x")))

(define-public crate-capnpc-0.3.1 (c (n "capnpc") (v "0.3.1") (d (list (d (n "capnp") (r "^0.3.2") (d #t) (k 0)))) (h "0yppiwk8h05ngbfhhs8c1vby1zpmgiqrfzranfhmgw0qb9zj6hm7")))

(define-public crate-capnpc-0.4.0 (c (n "capnpc") (v "0.4.0") (d (list (d (n "capnp") (r "^0.4.0") (d #t) (k 0)))) (h "00yki7sqnask9z28rzrqhp8qfqkzg7zqyab2vpcrg3imfhvb64ba")))

(define-public crate-capnpc-0.4.1 (c (n "capnpc") (v "0.4.1") (d (list (d (n "capnp") (r "^0.4.1") (d #t) (k 0)))) (h "1zrmdk5qzsfbqk9n9lm1nv1qdyxjmh888v9ni42ymhzak25nrcf8")))

(define-public crate-capnpc-0.4.2 (c (n "capnpc") (v "0.4.2") (d (list (d (n "capnp") (r "^0.4.2") (d #t) (k 0)))) (h "1yxxj98y50xm2lj145flyiqp9wl6vy48a87ldqi7pz5q517skipq")))

(define-public crate-capnpc-0.4.3 (c (n "capnpc") (v "0.4.3") (d (list (d (n "capnp") (r "^0.4.3") (d #t) (k 0)))) (h "1fyw42j92sgc2pf074ydjxp22v13kn9j08gqca56pgy56b150vs9")))

(define-public crate-capnpc-0.4.4 (c (n "capnpc") (v "0.4.4") (d (list (d (n "capnp") (r "^0.4.4") (d #t) (k 0)))) (h "1h6jwckq1amjv5d6m427j54w4567ykv7x0dkvl6rg6cp9mi7bakm")))

(define-public crate-capnpc-0.5.0 (c (n "capnpc") (v "0.5.0") (d (list (d (n "capnp") (r "^0.5.0") (d #t) (k 0)))) (h "09pnai6hgddig793p39m7xhygaqs6nk8ss6p1ad00k8y7hv07lv4")))

(define-public crate-capnpc-0.5.1 (c (n "capnpc") (v "0.5.1") (d (list (d (n "capnp") (r "^0.5.1") (d #t) (k 0)))) (h "11xf4ldy2qijx1mygik2a1fbz575qrvsfj00k3zj3rk0c9vgc0v0")))

(define-public crate-capnpc-0.6.0 (c (n "capnpc") (v "0.6.0") (d (list (d (n "capnp") (r "^0.6") (d #t) (k 0)))) (h "0xgfgb446lz9cz9vlp28irr7kq5bw7h5j26yahrvqy3px5cjqjxc")))

(define-public crate-capnpc-0.6.1 (c (n "capnpc") (v "0.6.1") (d (list (d (n "capnp") (r "^0.6") (d #t) (k 0)))) (h "09zwm5hjpjcnq1lmv83nk6mhpv697bai8is5wwk2dpic3iwffgfy")))

(define-public crate-capnpc-0.6.2 (c (n "capnpc") (v "0.6.2") (d (list (d (n "capnp") (r "^0.6") (d #t) (k 0)))) (h "10mrqbbndb07qyvbp7f6khc333q9rd0cmgx56bqrps0m79swblk6")))

(define-public crate-capnpc-0.7.0 (c (n "capnpc") (v "0.7.0") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "1f57z1azcxsr9xzx1c033n33gmsczazchfqqaww17iahvsyw3mmw")))

(define-public crate-capnpc-0.7.1 (c (n "capnpc") (v "0.7.1") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "0mw603ik7va2dm047i6iz2p9zacsb2pzdw1ay6crs2r75qjmcgla")))

(define-public crate-capnpc-0.7.2 (c (n "capnpc") (v "0.7.2") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "1dk23s8h6kifqal3a2z3lph416g4vd7naicj715d1cxyc8lxd240")))

(define-public crate-capnpc-0.7.3 (c (n "capnpc") (v "0.7.3") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "0hzp79796936sg1gpii993w8zw6rbv4l3zq2w2ndvzp3znlgk35p")))

(define-public crate-capnpc-0.7.4 (c (n "capnpc") (v "0.7.4") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "0g8xf5bcdxqcnd9ipxjlwy4g2q2p2x7qmq8xwqbbnal7x14jn8h6")))

(define-public crate-capnpc-0.7.5 (c (n "capnpc") (v "0.7.5") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)))) (h "0wbb6izh8fpmy5sv065v2d8i36gj32lrxb0bynql8ar707m0hg9l")))

(define-public crate-capnpc-0.8.0 (c (n "capnpc") (v "0.8.0") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "0npqi41arxwa5yizpy191byb59g2mfqsr5xfb3w20v00vgjawjc1")))

(define-public crate-capnpc-0.8.1 (c (n "capnpc") (v "0.8.1") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "0i6ndgkck0id0jd1ypqsq4l7bis3wzyvj3fyp0qhnszl8skbzys6")))

(define-public crate-capnpc-0.8.2 (c (n "capnpc") (v "0.8.2") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "17zmmw5cyj58mf1n64rv12vyd4qpa6mv063xa7c8hhd5vbbr0y4c")))

(define-public crate-capnpc-0.8.3 (c (n "capnpc") (v "0.8.3") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "036fyc7gvfmw694g4s65xa5pv7481vh9q9nmiv235f4xdcxai05k")))

(define-public crate-capnpc-0.8.4 (c (n "capnpc") (v "0.8.4") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "191wfzyaxf4nsrfjdz9rqbvwnlcpjn965ki4f6zws966v311vg8i")))

(define-public crate-capnpc-0.8.5 (c (n "capnpc") (v "0.8.5") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "0hfgziii3dk920jq0x8bn6hsp2zdfzqgbmkq3bip7dk89m7p27zx")))

(define-public crate-capnpc-0.8.6 (c (n "capnpc") (v "0.8.6") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "1mqz8iz757li7g22llbijb2rnxcmql3a2713229wxpi4c21hbzkp")))

(define-public crate-capnpc-0.8.7 (c (n "capnpc") (v "0.8.7") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "1qhqk4i1paf1c3hdpsf3i7ycdabv0hvvxn5si1nk3w93drjk1djy")))

(define-public crate-capnpc-0.8.8 (c (n "capnpc") (v "0.8.8") (d (list (d (n "capnp") (r "^0.8.13") (d #t) (k 0)))) (h "1kbyrlxl0rff1car11simjdv34wfhi9r4mb8bpyrm9w48kcycalk")))

(define-public crate-capnpc-0.8.9 (c (n "capnpc") (v "0.8.9") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)))) (h "0w98pgjx4fh392h31s1bhffvkkfmwplv3abvh607ywqmzyi5np90")))

(define-public crate-capnpc-0.9.0 (c (n "capnpc") (v "0.9.0") (d (list (d (n "capnp") (r "^0.9") (d #t) (k 0)))) (h "0xafxc2qxpmlli7v9bf6xvz4ri7253zh0sxrjjkkrmnrswcbi1bg")))

(define-public crate-capnpc-0.9.1 (c (n "capnpc") (v "0.9.1") (d (list (d (n "capnp") (r "^0.9") (d #t) (k 0)))) (h "1bhwsb2v8ni82h0364m57c3322jicpcj4s8pbhx16m8p9dccrxx2")))

(define-public crate-capnpc-0.9.2 (c (n "capnpc") (v "0.9.2") (d (list (d (n "capnp") (r "^0.9") (d #t) (k 0)))) (h "1h5zpyvs85pb0ic1zdvnhr1f5j0i6n9xy08nd8pcfm0ynxi5dw4r")))

(define-public crate-capnpc-0.9.3 (c (n "capnpc") (v "0.9.3") (d (list (d (n "capnp") (r "^0.9.3") (d #t) (k 0)))) (h "08gdn6b4473pg6gxpbaxvp3qmff9yx0y250nznv8zs8alsmxrxyy")))

(define-public crate-capnpc-0.9.4 (c (n "capnpc") (v "0.9.4") (d (list (d (n "capnp") (r "^0.9.5") (d #t) (k 0)))) (h "0cbg7zdq7bnvq6nkagzrafx09bm12ip7y2kyx02ms6yga1464j32")))

(define-public crate-capnpc-0.9.5 (c (n "capnpc") (v "0.9.5") (d (list (d (n "capnp") (r "^0.9.5") (d #t) (k 0)))) (h "0mh0yvbf1yy3wp3jfjpfss5sgpnqbaz2d5wfr1067jfraaxljx09")))

(define-public crate-capnpc-0.10.0 (c (n "capnpc") (v "0.10.0") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)))) (h "0am2jbs09lcrnlj6yhy7789r9018s1yla00ngf02p0il97873wnz")))

(define-public crate-capnpc-0.10.1 (c (n "capnpc") (v "0.10.1") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)))) (h "00gbnni3wm8781zcq9xyilmv8fd1anpddv9hvf54w2gfspm2w6kc")))

(define-public crate-capnpc-0.10.2 (c (n "capnpc") (v "0.10.2") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)))) (h "1zxbmdkr0xfzkfq9p8zn7pp9jjq275qhr8fh9a0cc0ab37yfvbyj")))

(define-public crate-capnpc-0.11.0 (c (n "capnpc") (v "0.11.0") (d (list (d (n "capnp") (r "^0.11.0") (d #t) (k 0)))) (h "0d4fc7cxj2s5z6py8axgpinfgxsb6nahay3yrrf1h6k6vc34wkkc")))

(define-public crate-capnpc-0.11.1 (c (n "capnpc") (v "0.11.1") (d (list (d (n "capnp") (r "^0.11.0") (d #t) (k 0)))) (h "08zvj1m9sf96x35wrh8n14095sjxvdv9z86za0jj22xah84y0klh")))

(define-public crate-capnpc-0.12.0 (c (n "capnpc") (v "0.12.0") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)))) (h "1vwf8d1yn727vxxmq232kxsjddsxwlagyzbgf4pk3yb8xf588pl0")))

(define-public crate-capnpc-0.12.1 (c (n "capnpc") (v "0.12.1") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)))) (h "1wy37wgmw54w6b2ysg1k7qgf2mf6aivrmdfd4qplddahpn0cfhz7")))

(define-public crate-capnpc-0.12.2 (c (n "capnpc") (v "0.12.2") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)))) (h "0j3b9hkld4x93khhvvmgf2iklxjzk7a1i3bcmvv8ip5z1ck0kfx6")))

(define-public crate-capnpc-0.12.3 (c (n "capnpc") (v "0.12.3") (d (list (d (n "capnp") (r "^0.12.2") (d #t) (k 0)))) (h "1sahpbg32f487gyb17pa3wagydn5c192qfmi1haf346k71j4ccda")))

(define-public crate-capnpc-0.12.4 (c (n "capnpc") (v "0.12.4") (d (list (d (n "capnp") (r "^0.12.2") (d #t) (k 0)))) (h "1hd8131asf8zck9dpcb53l49phqcag67p4fkylbbixw3dc4y35qh")))

(define-public crate-capnpc-0.13.0 (c (n "capnpc") (v "0.13.0") (d (list (d (n "capnp") (r "^0.13.0") (k 0)))) (h "1h4ipzq7mxrc9qjsxqilpsvz3a391wkf18z3qds7nf4jkilpzcrm")))

(define-public crate-capnpc-0.13.1 (c (n "capnpc") (v "0.13.1") (d (list (d (n "capnp") (r "^0.13.0") (k 0)))) (h "1hbm5xqpam3f0ha5ds39wjmpqpzdalpbrls9wlp7m3slh3p5r1c1")))

(define-public crate-capnpc-0.14.0 (c (n "capnpc") (v "0.14.0") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "1lsarw7dzihzcqq9mm4nr5mwd5i5hxl45gshdjwqkvgyy4lrhy79")))

(define-public crate-capnpc-0.14.1 (c (n "capnpc") (v "0.14.1") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "0pbiidgng5wn0gq7x3v6crmkj262chii5swmczwy7ab98igilrfp")))

(define-public crate-capnpc-0.14.2 (c (n "capnpc") (v "0.14.2") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "07jjf01hhjprr3ifi3m339pm2dj1x92c48lspxf4xdjzc2qhxibn")))

(define-public crate-capnpc-0.14.3 (c (n "capnpc") (v "0.14.3") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "0jgmcgr7s42z6pq0cahcpl6is5pf11z25137d10qd0dl2z9skj2h")))

(define-public crate-capnpc-0.14.4 (c (n "capnpc") (v "0.14.4") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "1zhis7h4wbhd4d7zc3jnpgkjm4nj9dcfsipp71f8nlb2260wwyxl")))

(define-public crate-capnpc-0.14.5 (c (n "capnpc") (v "0.14.5") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "0bwm6lnk39d0wh2p7bfz53snbpp54g7h3qn5zh3ivh0ad1x2lbv8")))

(define-public crate-capnpc-0.14.6 (c (n "capnpc") (v "0.14.6") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "0169wj67r9s9lv3a1xcqj81mmbaixniwasgzkk0cpxbn4a5iwh4y")))

(define-public crate-capnpc-0.14.7 (c (n "capnpc") (v "0.14.7") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "1df04gg3v5p7cc0sy67cbpxmdmrbki8brk18nfl03b4jyy09pvf7")))

(define-public crate-capnpc-0.14.8 (c (n "capnpc") (v "0.14.8") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "0s086yix4fccf4816vsgr2d5n5ilm9zxsy8y12spslhyllg8ws0m")))

(define-public crate-capnpc-0.14.9 (c (n "capnpc") (v "0.14.9") (d (list (d (n "capnp") (r "^0.14.0") (k 0)))) (h "1aisyk7rp1jxw2drf9a6c957d06vjy7nr8di0zq4yvb6hkfg3jdx")))

(define-public crate-capnpc-0.15.0 (c (n "capnpc") (v "0.15.0") (d (list (d (n "capnp") (r "^0.15.0") (k 0)))) (h "1y93b611mcxcc2jw87mkbvdgh5ishkxr0v94xdjyv5qwkq2y1n8f")))

(define-public crate-capnpc-0.15.1 (c (n "capnpc") (v "0.15.1") (d (list (d (n "capnp") (r "^0.15.0") (k 0)))) (h "041p17f564yinwkvmm0ykbz0byf7kqavd72lijq50xg1dr5rann7")))

(define-public crate-capnpc-0.15.2 (c (n "capnpc") (v "0.15.2") (d (list (d (n "capnp") (r "^0.15.0") (k 0)))) (h "1s9y154pmdydwkj46gdwfbjjb1wdv8akmdbjkm7lbrcqha734ss7")))

(define-public crate-capnpc-0.16.0 (c (n "capnpc") (v "0.16.0") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "1cajx8203wawa604hmbfvj5s12rry8is4wyma0y6s9gg5plvzv31")))

(define-public crate-capnpc-0.16.1 (c (n "capnpc") (v "0.16.1") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "074457698fxghb5a35rm38iynhznqsscfjbgcxfzn3ljn0sps53l")))

(define-public crate-capnpc-0.16.2 (c (n "capnpc") (v "0.16.2") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "080qcf07ill4dpw4z67nr3np4kd98dnwh27sh9b10313jcgs4sbj")))

(define-public crate-capnpc-0.16.3 (c (n "capnpc") (v "0.16.3") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "1l2a1rk81fysw5xs1yli0vi48r6b6jx91fy35yjrpigyng0a0icb")))

(define-public crate-capnpc-0.16.4 (c (n "capnpc") (v "0.16.4") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "1i7cwkid5hc1dp8jdjfdc188lfs8i0jd19kb78s07lac0l41dvhv")))

(define-public crate-capnpc-0.16.5 (c (n "capnpc") (v "0.16.5") (d (list (d (n "capnp") (r "^0.16.0") (k 0)))) (h "031nm05wrn3qnnvwi9jvd5yjk9g02myp76d7402gva5h3b1jzdsm")))

(define-public crate-capnpc-0.17.0 (c (n "capnpc") (v "0.17.0") (d (list (d (n "capnp") (r "^0.17.0") (k 0)))) (h "1k084c2cfhlk32mrgazky001cdnmchi2jsppxsg7gb14q8ajhvyr")))

(define-public crate-capnpc-0.17.1 (c (n "capnpc") (v "0.17.1") (d (list (d (n "capnp") (r "^0.17.0") (k 0)))) (h "1myvyzc62brrjlrbrnraqvvc200kmg28x0axas8ahs34ymkavack")))

(define-public crate-capnpc-0.17.2 (c (n "capnpc") (v "0.17.2") (d (list (d (n "capnp") (r "^0.17.0") (k 0)))) (h "0aq4ysmfpnqq111pg8rwamy7gy163bhixicwiqc3ariyzdikgg7v")))

(define-public crate-capnpc-0.18.0 (c (n "capnpc") (v "0.18.0") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)))) (h "0bcwyz7f2lah8xvavhdi4w0k7zx6ayd3w5ah67897nclxv4g6rsh")))

(define-public crate-capnpc-0.18.1 (c (n "capnpc") (v "0.18.1") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)))) (h "0cdi91h5j3bd89a51cd3xp7m6d917iclq0f0vh5yg1w1lymglhm6")))

(define-public crate-capnpc-0.19.0 (c (n "capnpc") (v "0.19.0") (d (list (d (n "capnp") (r "^0.19.0") (d #t) (k 0)))) (h "1v49w7zsv4bkdn88dfmi2hk5dzv5pgs0qwgkq99jsn081w7a6ny7")))

