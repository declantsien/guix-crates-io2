(define-module (crates-io ca pn capng) #:use-module (crates-io))

(define-public crate-capng-0.1.0 (c (n "capng") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0jfh1lxkbvanvjbcdc4mpkpf1v23m3qhfi5lqcplq0py9sa4xrn1")))

(define-public crate-capng-0.2.0 (c (n "capng") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "132ac9daps6f8sjby2rp7ijadhzvyk73aki41n8imwgli00z1h8n")))

(define-public crate-capng-0.2.1 (c (n "capng") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "1f00ragbgdn43dgy2kfi3hdv702s1yh8bqs8x8sgkyprj2piriiy")))

(define-public crate-capng-0.2.2 (c (n "capng") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0x95j3fn8f2ra6d63rxk2phd9y7mjlfi41i6wi1kcq1kh92fky7n")))

(define-public crate-capng-0.2.3 (c (n "capng") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "11cyx3xyk226w9ayi7915p8zas1az8j2iv89hf5pwzzhjdppc9ks")))

