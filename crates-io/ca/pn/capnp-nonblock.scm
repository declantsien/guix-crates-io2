(define-module (crates-io ca pn capnp-nonblock) #:use-module (crates-io))

(define-public crate-capnp-nonblock-0.1.0 (c (n "capnp-nonblock") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1j7m7b11k5w6a5c10hg5w4qpmn69d2h99p9fxr6bnrjpkafc72z5")))

(define-public crate-capnp-nonblock-0.2.0 (c (n "capnp-nonblock") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1i8klh5hm6rns79pbyang7699n4f0dadvzzyvzw8rvll0kcyf03y")))

(define-public crate-capnp-nonblock-0.2.1 (c (n "capnp-nonblock") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.5") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0jf9yyfq6k2vqwayi80jxb4r2qpq0r1mllz37bqcw99fq55zhyvi")))

(define-public crate-capnp-nonblock-0.3.0 (c (n "capnp-nonblock") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.6") (d #t) (k 0)) (d (n "capnp") (r "^0.6") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1gimf0sxhhvzrnpack3ac2l9plbl5qw6fgz5azj10zr1ib5sp38x")))

(define-public crate-capnp-nonblock-0.4.0 (c (n "capnp-nonblock") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "capnp") (r "^0.6") (d #t) (k 0)) (d (n "capnp") (r "^0.6") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "13cn1bggsrcfmk9vc4xkp2v6c89n5fpr70c0v4a63nw621jld0fi")))

