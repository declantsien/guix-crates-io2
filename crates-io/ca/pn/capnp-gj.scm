(define-module (crates-io ca pn capnp-gj) #:use-module (crates-io))

(define-public crate-capnp-gj-0.1.0 (c (n "capnp-gj") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.6") (d #t) (k 0)) (d (n "gj") (r "^0.1") (d #t) (k 0)))) (h "1srhnfbxmz9x9v7n3qxha3ay19ri8fglc3xavrihm903crr6ljsq")))

(define-public crate-capnp-gj-0.2.0 (c (n "capnp-gj") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "0zpvgxl7bhakbg94cy6jdyj2ylmm0hmqvmrdb6i367zd51qqzs6a")))

(define-public crate-capnp-gj-0.2.1 (c (n "capnp-gj") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "gjio") (r "^0.1") (d #t) (k 0)))) (h "18kg5l1qna7yf13gqgfabyxkpv97dgk19dp8f4g1x7qv87lqbh51")))

