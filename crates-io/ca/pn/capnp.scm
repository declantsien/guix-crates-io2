(define-module (crates-io ca pn capnp) #:use-module (crates-io))

(define-public crate-capnp-0.0.1 (c (n "capnp") (v "0.0.1") (h "1gsvw0s5hmjs223ya4r8qgl5yjsgjai7kxh8gwd3312i7znlgcwc")))

(define-public crate-capnp-0.0.2 (c (n "capnp") (v "0.0.2") (h "1nw8kkf7dnf96nh301kwgni30l21g1qb69w4jcd9vgrcg81i0c4h")))

(define-public crate-capnp-0.0.3 (c (n "capnp") (v "0.0.3") (h "1dvhjznhkbj0z6alxmv38imwj2s2m6fn9y62qn1kv8r52z2gz3qn")))

(define-public crate-capnp-0.0.4 (c (n "capnp") (v "0.0.4") (h "1pm03rpxk4dakm1iilsxsqy0x0d01knarm1zcayvmpb3rc8ilvnv")))

(define-public crate-capnp-0.0.5 (c (n "capnp") (v "0.0.5") (h "07a7nz8zrls41vnny9nyd2ggj83xh7ncnhbfzc0mpadz0l0nnpwj")))

(define-public crate-capnp-0.0.6 (c (n "capnp") (v "0.0.6") (h "08j479r7sp1n2ghdcmfxbjrvdqnqkll3brg56l30vp5shs2z8lnm")))

(define-public crate-capnp-0.0.7 (c (n "capnp") (v "0.0.7") (h "1jm6k5qhr28c1bi3brbb9hq48h8kcikqz080vcsb1ga00pydp6nv")))

(define-public crate-capnp-0.0.8 (c (n "capnp") (v "0.0.8") (h "157xx696k8i5psjfbmswn8fvr0vn3465cf38ld9ppy30k5576gh6")))

(define-public crate-capnp-0.0.9 (c (n "capnp") (v "0.0.9") (h "07gxggc6n7an9vsdy0i1yvyfz1mpihxag442yw1mriani22jvaw9")))

(define-public crate-capnp-0.0.10 (c (n "capnp") (v "0.0.10") (h "13s56axb4vz7xq2697as3q9mlyigb7msjsqdlxmwcgqrk6ackbpp")))

(define-public crate-capnp-0.0.11 (c (n "capnp") (v "0.0.11") (h "1vb8kakqmb71q4jlbca86dadhg1zawkadlaf72mlsjb6nxb1h6j2")))

(define-public crate-capnp-0.0.12 (c (n "capnp") (v "0.0.12") (h "0486gbs45n961f7ig9zzkdg5d8ld5bywz8mg0n2n3ylx8r1q1sq7")))

(define-public crate-capnp-0.0.13 (c (n "capnp") (v "0.0.13") (h "1flcs3zvifayxkh8j6yvxdvliixrkr6acl44vlcm6sgnr1ap8qhk")))

(define-public crate-capnp-0.0.14 (c (n "capnp") (v "0.0.14") (h "1mnn27m7h7i0xwsq6kdhhp79dvl52c6zs6r9ik46ja8as0wmcicn")))

(define-public crate-capnp-0.0.15 (c (n "capnp") (v "0.0.15") (h "0gjc4f6sjjhjgxnmas4fxvmgpgsqshbdakqb4pa1mxy3s035vgdr")))

(define-public crate-capnp-0.0.16 (c (n "capnp") (v "0.0.16") (h "0v6j6pfdgslrdi9s1079pnk7vb298x8hbkpzwhibq7ssp0l9b2xa")))

(define-public crate-capnp-0.0.17 (c (n "capnp") (v "0.0.17") (h "08jj00aq9crsz5f85xc7hbp3hmrlywzl20bqpxz1j58rlhn09v23")))

(define-public crate-capnp-0.0.18 (c (n "capnp") (v "0.0.18") (h "00hkvc4nff7swvkxyl06i9h9w18daswyram627g9gzspgwmk65s4")))

(define-public crate-capnp-0.0.19 (c (n "capnp") (v "0.0.19") (h "0scwrw9d6bqyszwh2pg7cl7bhr450myyfqxnx72nkw5c9g9rlwxp")))

(define-public crate-capnp-0.0.20 (c (n "capnp") (v "0.0.20") (h "13k7bhf8rwswd1g4bljxhcnw4y8awih0mnhz835p10gc2sdawyvk")))

(define-public crate-capnp-0.0.21 (c (n "capnp") (v "0.0.21") (h "1dc3alw8lj4f6ngfrc3m0mxsi01hj9qsa9dlkq3kmja5dwisfqh5")))

(define-public crate-capnp-0.0.22 (c (n "capnp") (v "0.0.22") (h "1yy9nsk1x9ba0p533h2pyclfjkmxbsb9dwmpv2qazp9bllpahnp9")))

(define-public crate-capnp-0.1.0 (c (n "capnp") (v "0.1.0") (h "0afa1crlzy3lmnkd02zvs4wnlrmxkw76qz48rjzirzvvchnhfpj3")))

(define-public crate-capnp-0.1.1 (c (n "capnp") (v "0.1.1") (h "1pknxs6xb1fba0gbn2br5m35072m0nwf4b46jbnkwyaabf5vf0dq")))

(define-public crate-capnp-0.1.2 (c (n "capnp") (v "0.1.2") (h "1mc0kpv5vc9s0r59sxnar9fibbsasn6d3p282b9rk87w7iignvxd")))

(define-public crate-capnp-0.1.3 (c (n "capnp") (v "0.1.3") (h "1gsy16v9qvcfsgziak5yb1rr69yasmrfhzj0vfylifdqg5fn3zyh")))

(define-public crate-capnp-0.1.4 (c (n "capnp") (v "0.1.4") (h "0wiw0v5lh7hyf9c66pgv9fhqdgbh81dl1ikxf0flzj2yix8rla55")))

(define-public crate-capnp-0.1.5 (c (n "capnp") (v "0.1.5") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "19p4wxiwd1r0a3vzm8kc8hndhl1x8bj86mqy185vawbpqkl8m0g9")))

(define-public crate-capnp-0.1.6 (c (n "capnp") (v "0.1.6") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1mbzvnca1yqj1v4j7zsg0gls7p6j9qh3jfac2p0cgrjidhhzas5i")))

(define-public crate-capnp-0.1.7 (c (n "capnp") (v "0.1.7") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "11vdzp80xadr1mvg371ww7qmfx33dp75sjjhvm1ds9fcj6r2cl06")))

(define-public crate-capnp-0.1.8 (c (n "capnp") (v "0.1.8") (h "0c556q6kcg39dhkjazsg7s7qjmh2f8hp1la88hgyrpnyzs47mb7x")))

(define-public crate-capnp-0.1.9 (c (n "capnp") (v "0.1.9") (h "0kvz5ja29w7zrc4xl3glwk4vla1fg0h3pbzfcvkjfi3c9a5k06mv")))

(define-public crate-capnp-0.1.10 (c (n "capnp") (v "0.1.10") (h "1kprdnbi5rbr59dga0ax6d22zaaa0vjids4abcm4avby2jj27nnp")))

(define-public crate-capnp-0.1.11 (c (n "capnp") (v "0.1.11") (h "0z6byvvwfis5whqyf608h2yyyzl6nwwl33sydln6zfya3j3bz8wk")))

(define-public crate-capnp-0.1.12 (c (n "capnp") (v "0.1.12") (h "1aw4vc05wxg013akabmxyb674mgi8w7apkmqm0xp72g9g9b8ripg")))

(define-public crate-capnp-0.1.13 (c (n "capnp") (v "0.1.13") (h "0976w2w4kj2nsqi5x9g98swhrikjkcc6d257jkajfvy9r5f5k04x")))

(define-public crate-capnp-0.1.14 (c (n "capnp") (v "0.1.14") (h "1n9qfd2sw49aav1bx3jm49wk3rxp5pmaxqyp4jqclrxv890bfavr")))

(define-public crate-capnp-0.1.15 (c (n "capnp") (v "0.1.15") (h "1ax4l6440wdc0lykq4k8nlnkh03q14ynmzqvlw4pqfspnnqm4s6f")))

(define-public crate-capnp-0.1.16 (c (n "capnp") (v "0.1.16") (h "011amqp2ab3l6m38pkq16x24yw4mg4ijh2sszp6fdqicxa2bvbzd")))

(define-public crate-capnp-0.1.17 (c (n "capnp") (v "0.1.17") (h "0y73h39rna8frzbhcms07i6qkriabshd4wsn6pn9ivjyv53fmziy")))

(define-public crate-capnp-0.1.18 (c (n "capnp") (v "0.1.18") (h "0n6jfja1drgfpi0ikknxnfb44b5schb4m5xgf2xhah4j2w4vsra7")))

(define-public crate-capnp-0.1.19 (c (n "capnp") (v "0.1.19") (h "0if15ngx4qd07wvvrcjf28q377p008yxkzlhydsr7kh1nig1jag4")))

(define-public crate-capnp-0.1.20 (c (n "capnp") (v "0.1.20") (h "1gr8gg5p0f8md04kz91wwzq1iyiaby56psjjd8gyxy0w4kr3x7as")))

(define-public crate-capnp-0.1.21 (c (n "capnp") (v "0.1.21") (h "1rcd57snfp00iycxm97l8xk2p5ip2v2xzcjb5s3dinpdhscc6b7d")))

(define-public crate-capnp-0.1.22 (c (n "capnp") (v "0.1.22") (h "08b905am9pylczcapwsai3vx6mw6jbal65whm42533rh49905pcg")))

(define-public crate-capnp-0.1.23 (c (n "capnp") (v "0.1.23") (h "10ad63slrs1iqcbh4fqyq4bi1k8pgh2bhnvbxj3c1jj4c4jwinqw")))

(define-public crate-capnp-0.1.24 (c (n "capnp") (v "0.1.24") (h "01qibk9qaf49r5nl6xq9mlq3jvyb7pvd5djc4sjc4ai7xhn6n6mj")))

(define-public crate-capnp-0.1.25 (c (n "capnp") (v "0.1.25") (h "0ssyfv85w2h8vya8ckgnpfy7zdsqcg9s7fh6ka7a8gv9s4qwj209")))

(define-public crate-capnp-0.1.26 (c (n "capnp") (v "0.1.26") (h "0anczb59h1zbyzpm1253pc4bzw9b7qpb2g63kbnl2c0gf6brz094")))

(define-public crate-capnp-0.1.27 (c (n "capnp") (v "0.1.27") (h "07byb0a6i3zygp51pbv2yiwpsdx56rwkzwrfi4ijbniyd79yn7kg")))

(define-public crate-capnp-0.1.28 (c (n "capnp") (v "0.1.28") (h "16pk8cyjg749ji8clf6v2nzbj17nicq2g49gxhjf8r4f77sg75qq")))

(define-public crate-capnp-0.1.29 (c (n "capnp") (v "0.1.29") (h "0xxyzwakn381hls1pf1dxvrgild0s2v7kkrp5izag3yhpjn61hhh")))

(define-public crate-capnp-0.1.30 (c (n "capnp") (v "0.1.30") (h "11l6xaix3y8r76wn0v0khxm5933wfp8g2vzh3l2bfjsspdfyirk8")))

(define-public crate-capnp-0.1.31 (c (n "capnp") (v "0.1.31") (h "0x7zjc6z2ni3gvzsm5pdl039cg890bd13l2ssvw6900za0gm16ra")))

(define-public crate-capnp-0.1.32 (c (n "capnp") (v "0.1.32") (h "0453imx2pq8wrjc145wvgc604hh6yhribxm6fjia5n2lhrz48ash")))

(define-public crate-capnp-0.1.33 (c (n "capnp") (v "0.1.33") (h "1czicxqym2yc09zxf0b8bnfx9min8d3qpn8wdpx21d5mn71xdmvh")))

(define-public crate-capnp-0.1.34 (c (n "capnp") (v "0.1.34") (h "0rfrqzr52x8a13535kbmrvf5am47p8zhm1m9xzb5l7y4w853mky2")))

(define-public crate-capnp-0.2.0 (c (n "capnp") (v "0.2.0") (h "13bv3nvflz55cxzmvangdrcpskl91kjr8s6x2zmqzmz8dns5470j")))

(define-public crate-capnp-0.2.1 (c (n "capnp") (v "0.2.1") (h "06jabpvhnlad21zhv4w77gkq2fj9b9x6c0db9jhhvfc5lmi4hw28")))

(define-public crate-capnp-0.2.2 (c (n "capnp") (v "0.2.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "18q2hzvmzsmibqrmv117lpzh592lm9bqapv2ssvmkbdvq0fr07yk")))

(define-public crate-capnp-0.2.3 (c (n "capnp") (v "0.2.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "04pscpq12f6bjdj65f16rqcwkpgcn0n6557hhn15dhgz6lnp5yxc")))

(define-public crate-capnp-0.2.4 (c (n "capnp") (v "0.2.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1nb62ff494xa1dnydkm6c91jmp8l0qxa8wqc610h55l31aqnxhch")))

(define-public crate-capnp-0.2.5 (c (n "capnp") (v "0.2.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1wky6j9dbf86mg3gfvrcfwi209c9f1s1inisf5n2lsm01xbbp0la")))

(define-public crate-capnp-0.3.0 (c (n "capnp") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0v3xcrvbhw800k8vps8jpw4ng8z95vg93ahi7ag7djnf2vfnc0vw")))

(define-public crate-capnp-0.3.1 (c (n "capnp") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "04cc1pxk7k7z9s856ck8hpjmi4yb24d1hlll0zn59cgy38scv7j5")))

(define-public crate-capnp-0.3.2 (c (n "capnp") (v "0.3.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1mfdrnw7r8v1nh93mgp83n6qf3y31r4sgm2glaxz2kn8in69q933")))

(define-public crate-capnp-0.4.0 (c (n "capnp") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "00wccm51f8gf671r1cvlkp0h958qjcpzpyzqz48fw0p8qr7hwy6s")))

(define-public crate-capnp-0.4.1 (c (n "capnp") (v "0.4.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1j0yimpfxha4629xzc9c5xsncakg9j9a3awcxwvdhjwxrg73rfw7")))

(define-public crate-capnp-0.4.2 (c (n "capnp") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0wwd9x3hd8wszcbzj3k3wb68bwww7vfv5v88in8a9vpi1wnl7zcd")))

(define-public crate-capnp-0.4.3 (c (n "capnp") (v "0.4.3") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0xdp3winq2kwxy5sdz7y1z14ppxijd5n2xv50xgxxyx1dcnx7y2n")))

(define-public crate-capnp-0.4.4 (c (n "capnp") (v "0.4.4") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "08zq9mnz2sb6mw1ix71k0dy8snf53d4bqqmqab8z54sjxvybdcmb")))

(define-public crate-capnp-0.5.0 (c (n "capnp") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1vpy1qkglxwl7i09svh9973szbm5mz4qqp81c3jfcpzzppz2fvgc")))

(define-public crate-capnp-0.5.1 (c (n "capnp") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0123frfbx5dxcl226chy2kbidadlpq44sqz4wd93r4wv7wxjcrs9")))

(define-public crate-capnp-0.5.2 (c (n "capnp") (v "0.5.2") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1pw6yvr1gdi39xz2ybzwvkhbk7gzca7dfvisqg57dsvgqb3iblzd")))

(define-public crate-capnp-0.5.3 (c (n "capnp") (v "0.5.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1qmxbxickbp08x7794p8pn2p8jpyy1ykh1fkrkbabni492z6wrmp")))

(define-public crate-capnp-0.6.0 (c (n "capnp") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "054frx8h8vb7l1kpwizaj13awcav9h9ybwjg5j3mnkwgrk42sv7i") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.5.4 (c (n "capnp") (v "0.5.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1dx2d5fal2zx1mmws6kr9v64sfg3qms9ri204amh0v7984kp0fsc")))

(define-public crate-capnp-0.6.1 (c (n "capnp") (v "0.6.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "141ak24wdp5rjl6snswva3fqqrdpidrpniym8y3gwd7fipcrpmjc") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.6.2 (c (n "capnp") (v "0.6.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "09d64dkl8dyy6blckj1b1dm4ycpxis6h8nrwznsn6n6izrdz04r4") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.0 (c (n "capnp") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "13qyhb8gxkf9mp37x5i6sf22a5xxjclpzwkvyxkia0k3v4rh485l") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.1 (c (n "capnp") (v "0.7.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1r10cxpwrwpd1j4wmyy3ma72l6hzkkjbb7mc56fsbahg0xw3xzn2") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.2 (c (n "capnp") (v "0.7.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1mx7aklzy1j3hwamsy4z5jq1a7zcci61z24s3gdb25z3nij1c3in") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.3 (c (n "capnp") (v "0.7.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1mh7x8pwh2hq2y9x1diq19jx5b4qccl6c2n2ipwr3zv9h91i8wax") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.4 (c (n "capnp") (v "0.7.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1gf1p9qaxjzymj2i60xyim3lhg0ia3n4vrcjh0cklxirp8dnlv27") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7.5 (c (n "capnp") (v "0.7.5") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "gj") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0zjb0hx7fp06hpkbig5wdwcj0yjvd3kp609bpn46h0hi974n8lmm") (f (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.8.0 (c (n "capnp") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "074bnhwzwq4qgl0i8jjxkc8p5n03w848kkwzsiz6yhxw12p6w7x2") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.1 (c (n "capnp") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "14x7cvgljhgl0milcx9r41wlsgjlkvgymv0w0cq3scf81l23x4lx") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.2 (c (n "capnp") (v "0.8.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1cy0ada1p9m40pw5fmlffvrh9k9xia85iwxlp809kp9v92digixl") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.3 (c (n "capnp") (v "0.8.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1w8db8p524022ammvdzb7xpi5xcq6ygagddycyk5wjj7cnkiv0dw") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.4 (c (n "capnp") (v "0.8.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1zbimyc73k1lv4jj4l6wi9hp8px4nb1nqfj1mid793qjnw5i14nl") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.5 (c (n "capnp") (v "0.8.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0b2v5c985v8vgmzm63ah3iy28g019aksgpl8kzdh20mbkvvh4w9v") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.6 (c (n "capnp") (v "0.8.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1b7zcxfjv8qrhz64c75mp7hhj8l5p8d5nz54dndl2gk73y5221b0") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.7 (c (n "capnp") (v "0.8.7") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1gly9yyb8drarnw2340bc86vjab8s6y9biqlbw4x16gidl9649sx") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.8 (c (n "capnp") (v "0.8.8") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "180zlnj84anmp556ygsxk2jk5zdsww3s9gfq431bh9vcn3f4rd3w") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.9 (c (n "capnp") (v "0.8.9") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1xakjbx99jkjb9n9f4sy9aq55vy582snc85v007ym9ly14khp85c") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.10 (c (n "capnp") (v "0.8.10") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0ibsaallfnzywk9364bn3bnw0ikbbcj53fih1ycqlgd9c7a2c9f4") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.11 (c (n "capnp") (v "0.8.11") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1qbhsjskqgz7vygwx9qiwx5b8lzvbx4g0f47kdzkk7r52jyyyjbc") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.12 (c (n "capnp") (v "0.8.12") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "01bzk6810016i55y1rschn1y9h8w1j7pvqpy1yrf2k25r5k35bvs") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.13 (c (n "capnp") (v "0.8.13") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "08shfan4kicvjmdqg1j98qgd17q0g9p9lxj57f4rhb1ki8ic94hl") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.14 (c (n "capnp") (v "0.8.14") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "00jx57qvgm4x3v48cxvl1gmq86lhd73mi715qakh4nrig59kbwz5") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.15 (c (n "capnp") (v "0.8.15") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0vqgj8hrh7pj1mq3hx420xh75a5xgd8icx60v5g78in8cax16jbs") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.16 (c (n "capnp") (v "0.8.16") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "06cs53sabmfm9n4ac3kinbymxmggcnyks05lq8s378wxhlnp7wa5") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8.17 (c (n "capnp") (v "0.8.17") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0qhimabww2pqcp4g89m4j34mbzg1kmvav67d6v6mxlan2vbw6rb5") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9.0 (c (n "capnp") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "106d3rlg0rkrrnaiqr3wj6zs17icr0ks8sjq2dvldaxfjmvsbkxk") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9.1 (c (n "capnp") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "09921jvh8snfjhii0721zag1nrqdrk3vb96gz96a5b53xb8lia6q") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9.2 (c (n "capnp") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1fdwaknpsgb3hkqah5n5gby19x6nsn5w34fpzxm4ld34mbvbkbrh") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9.3 (c (n "capnp") (v "0.9.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1m6wmyh0awqrdkjjr1zzal4n1256r2qhcbbgpq8d3zsgqz788rfy") (f (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9.4 (c (n "capnp") (v "0.9.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1j7ab3zxfxj4lv11n673l1gljyl8v6qrs1rgld9n5sfsm7r0jf2z") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.9.5 (c (n "capnp") (v "0.9.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1iqnhzsxj954zx4wkim2h4amri6j28fbcwrns6gnfkvnibhl1g2h") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10.0 (c (n "capnp") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0kvgvp1j38vskg04binjf7g4r9sz40wisa99sqr241rl24fyxpzs") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10.1 (c (n "capnp") (v "0.10.1") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0fhj9vspmw1dljfvx45gbc4h8scjbfdwdqdp3i9y7g4182cdqc9s") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10.2 (c (n "capnp") (v "0.10.2") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0530cbhfs9sv8pap78ihc11gvmcwq963hsskqq0v7d9r52g68wrh") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10.3 (c (n "capnp") (v "0.10.3") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "17hsnmlcrzksjjpwpz51y8g36xzq8042i2cwns0lsg7rixfw2rxq") (f (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.11.0 (c (n "capnp") (v "0.11.0") (d (list (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1hjrfg2lnmk6ysqssj5nqmgrycpw2awhbswslb8wbrrdwg39xx2r") (f (quote (("rpc_try"))))))

(define-public crate-capnp-0.11.1 (c (n "capnp") (v "0.11.1") (d (list (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0wk1446g5yv4ik9ykcs2c34p6zls9b6l9xp2daf5dvcf5c9s6iwd") (f (quote (("rpc_try"))))))

(define-public crate-capnp-0.11.2 (c (n "capnp") (v "0.11.2") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0aj7zvdsrdpk8bwmlj09a8ns40y36112jk7grg0in80afybglnik") (f (quote (("rpc_try"))))))

(define-public crate-capnp-0.12.0 (c (n "capnp") (v "0.12.0") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0xgdy0jp50aiinylal321x8ayv30wcdm5r9v9b753q8i4qj0shj0") (f (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12.1 (c (n "capnp") (v "0.12.1") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0s53f90sbnnr59h56n864vxkn2kllqpr2r9kdhi4pnhbhpq8hb71") (f (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12.2 (c (n "capnp") (v "0.12.2") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1jdyr73qpb2jpdh412kj13bra5qm636qj3j36g3yhx8gk9xz48gm") (f (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12.3 (c (n "capnp") (v "0.12.3") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0vnly50b8l80ywvlxqjh0aqblxs123prk1k2cbf5r4327n065z5r") (f (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.13.0 (c (n "capnp") (v "0.13.0") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1m6h79jhqmqgzsxi37231wysdra72zlnv1vnlyj4803rgri9ipvn") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.1 (c (n "capnp") (v "0.13.1") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0ahif19nf6ja8jbchrwykxhmzwglp212vqcdyif5zw3ijy1dqz95") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.2 (c (n "capnp") (v "0.13.2") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "139slq5zqwma5ics7d44rpg8v4x977cc37vabmxgq7cdkrqbvn9i") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.3 (c (n "capnp") (v "0.13.3") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "18d668hysl099mkmfhvirrha7b8iywdxlg3a25jdyjcf8l3ad7vk") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.4 (c (n "capnp") (v "0.13.4") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0rygm9zl6fv101girxx7g2l1wk82i8wlc4avybzsam5jg9594w0r") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.5 (c (n "capnp") (v "0.13.5") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0p8hja2xh0czdjlavwqia12jzvrxs07jgsnkr10kvm34fzc9lkrr") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.6 (c (n "capnp") (v "0.13.6") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1bx2pj0ysmyd1ryd5s43m3c7n5k067x1aciqiiyg4kjswlcs6xjf") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.12.4 (c (n "capnp") (v "0.12.4") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1gx64cd2zwr46mbzgcvp8d35bby7r1w6r6p012b1izv9bzhwfvbh") (f (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.14.0 (c (n "capnp") (v "0.14.0") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1jmcx7gmgbyxi1ma3zd21m6rl1dxyrg2xvp0wsd5zlqrjhxz2czf") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.1 (c (n "capnp") (v "0.14.1") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0kyiyxg202ya98yw3rg74biw5x7cxfbf2kncg80p0rmqxvmcr77d") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.2 (c (n "capnp") (v "0.14.2") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1mldpiv3mqqznwvrh0mprwidg6xwvacy5vvxhg60k46ljscwd82p") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.3 (c (n "capnp") (v "0.14.3") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0z79pr9y3zq3dyi3aqfz0m3cynz17rayxd62vmwm42s235qqm6xf") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.4 (c (n "capnp") (v "0.14.4") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1lfy5ymj6barnw6p69zsdrhdcbamfrg3b244p27xwz6qs8mbb52h") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.5 (c (n "capnp") (v "0.14.5") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0rd0y5mi00mmaw4kyg6bwp7ivxds8raplgws4s9864b8dxr65hhn") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.6 (c (n "capnp") (v "0.14.6") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0s6y6j4bririm6hhq69n6s98zk132m7s9ykj0rrg2iiijzddgm91") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.7 (c (n "capnp") (v "0.14.7") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1x7240vww8c8zl2b1w7b5mg966fl83i21kmchllsfwndzij16kp9") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.8 (c (n "capnp") (v "0.14.8") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0shsygnbjl8n93bpaschbw9r807cc0jhajrkhsvk4zjymfqa7vw2") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.9 (c (n "capnp") (v "0.14.9") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "05b0pbk3zblcflv4dr3j01g4vkraaxmvhvz8x6whwdxfzfxxpddi") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14.10 (c (n "capnp") (v "0.14.10") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1wxajl7xsdhwxpwcb509ak8q47cgingwqknk6hn2ib36vhxx7wnx") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15.0 (c (n "capnp") (v "0.15.0") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0j60l41rjkk7wbi8nal70nxn43rbcfly13msw82nhc23qlshg18f") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15.1 (c (n "capnp") (v "0.15.1") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "13dcjgsz0wcq7r78h8lx84sb0szsrlz8agbrb4ks8nn5xxqrv4pl") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.15.2 (c (n "capnp") (v "0.15.2") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1kn6nv6yssyxysgvjqh48xrzck0zrbihw74cc00ffls5rzfi9amg") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.14.11 (c (n "capnp") (v "0.14.11") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1wwp619sib07raykk93n6j7amvsinlchnicxfjnnb7bx5if0ijid") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13.7 (c (n "capnp") (v "0.13.7") (d (list (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1xkfv6mm38y4d6ykxmvs1pdfywnn3n5bklsqcsz9xh72i9ffd3s5") (f (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15.3 (c (n "capnp") (v "0.15.3") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0m0vld69w45f2dgcmvx2d99z2bz9g2yv5l0zsxifsa6jg2mzfikq") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.16.0 (c (n "capnp") (v "0.16.0") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "08m87lf5mzl809y5mbhzq79wqsxsk8ccmgj3d0gakwamrdm0qh1m") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.16.1 (c (n "capnp") (v "0.16.1") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1zxvccbqmllydfnhxgzvjd15q7lwnyqi70hmv8yn5qx7mjws3v0q") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.17.0 (c (n "capnp") (v "0.17.0") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "17f237m6xn634dji18bg67vmr6r3km7d0h0iw78n27b0s4rd9qhk") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.17.1 (c (n "capnp") (v "0.17.1") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0pdzzcvn9j626bmj3036pgzp5pd8f7z6l58i78dcg3jiyvpklm6f") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.17.2 (c (n "capnp") (v "0.17.2") (d (list (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0y6nbxd19miz9z35kfz8n0hxqjrcxnf7i497gkzbnl4jv0hm1rlm") (f (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (r "1.65.0")))

(define-public crate-capnp-0.18.0 (c (n "capnp") (v "0.18.0") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0kjbqklcqh5kp97y29r9gxdvbhj1ppz413dlvmg5mj1gg4jbjjx0") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.1 (c (n "capnp") (v "0.18.1") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0xkz5vpm10d8kvqdmgyxlhbg59zy5yj73s195ni2lx6rkdrbvpcy") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.2 (c (n "capnp") (v "0.18.2") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1hw6c9wmqi9n80fa3nax2c8ggc02rkj9gx531h20fxj93i2aj4n1") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.3 (c (n "capnp") (v "0.18.3") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1r2arc5z1382fxa2rpn7ihw7n1q00wrpcj7s4flbf69cn8fym729") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.4 (c (n "capnp") (v "0.18.4") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1q2ag519i130lrmih49b6icwmvcdfaqns1bnjvxl0vsg8ac8avlx") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.5 (c (n "capnp") (v "0.18.5") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1xibw9vnjm7silis9666c3axd4c35xnqm8995mhvk4c84crd1x8b") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.6 (c (n "capnp") (v "0.18.6") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "16fnicp6yjvmb20l2lqyib8bfmi61ndzniyxn33541x4d03bpf6g") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.7 (c (n "capnp") (v "0.18.7") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "14a0w63gr4bq8fyn0wyhl4xv0ca3ab0rnnll1vm8vlva3cxklfnf") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.8 (c (n "capnp") (v "0.18.8") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0qkp40kxcfbg1ci3whil79qb81rg3da6gzkp1f9qqn3pnma85bjw") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.9 (c (n "capnp") (v "0.18.9") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0vqx6nq6p32a9shjj5min80s1ask5w4dvbpwiic0r7ipr8ifid6z") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.10 (c (n "capnp") (v "0.18.10") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1w73akdaqxlsb1i3xvdvklp0ajfqwr513dd3jpicim6ncx4l4y0f") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.11 (c (n "capnp") (v "0.18.11") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1g359r16q3wd8r9n3afhc1jpmvx1ywz3sbxjz26bhv0vnfrrm5gj") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.12 (c (n "capnp") (v "0.18.12") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1hbdd5v29m1xri32m4ax959y6dq7wa1gd480damvfqlxvpwcjid5") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.18.13 (c (n "capnp") (v "0.18.13") (d (list (d (n "embedded-io") (r "^0.5.0") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "19246srp8rpbx55yclg64rjl83a05b982aqckf8dpsirbcd6fjrq") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.0 (c (n "capnp") (v "0.19.0") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1csaxfqd1s8kc4zysrbf6jk94537v8f7bn9ijlrki7nijb1ssmam") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.1 (c (n "capnp") (v "0.19.1") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0whwpigh2my89jsam841q2jc3zn5p6skqkcn6pvx4cxfjqgxsmsz") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.2 (c (n "capnp") (v "0.19.2") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "004fx630950msmn3q9adz290gajhwzd4bx1mm2lplna0hl4bggxl") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.3 (c (n "capnp") (v "0.19.3") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1v90fxhhwgcszxday345rmjp3nicpnbvshnccd55qsbszgk3465i") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.4 (c (n "capnp") (v "0.19.4") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "05wpj2mlxnmvapsp9zc0m37gh491v46m9zvq67xg4mrq0q5h0z2y") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.5 (c (n "capnp") (v "0.19.5") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "05ydz1vi0a8jllwzcsv9i6yczb8kbwwl0g47vazw1csl44kqbv9s") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

(define-public crate-capnp-0.19.6 (c (n "capnp") (v "0.19.6") (d (list (d (n "embedded-io") (r "^0.6.1") (o #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0spngfp2cg25f66abfv1h2hj05j152bf06f2nwydviya29wkhwfy") (f (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (r "1.65.0")))

