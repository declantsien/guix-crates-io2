(define-module (crates-io ca pn capnp-futures) #:use-module (crates-io))

(define-public crate-capnp-futures-0.0.1 (c (n "capnp-futures") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1gilrnvfpymzd8azsazilm0cyibr2ch42k4mp0zy3czvjaav5gic")))

(define-public crate-capnp-futures-0.0.2 (c (n "capnp-futures") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnp") (r "^0.7") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "16xlnvijbncfx0y47snrv4h0vcjik0sqiqami621nr10mk0dnh70")))

(define-public crate-capnp-futures-0.1.0 (c (n "capnp-futures") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "capnp") (r "^0.8") (d #t) (k 0)) (d (n "capnp") (r "^0.8") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0zan0sfpg7c1ar7sms6phvb3p5x8cvrrb7bw5kbb417n01dai4pc")))

(define-public crate-capnp-futures-0.1.1 (c (n "capnp-futures") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "capnp") (r "^0.8") (d #t) (k 0)) (d (n "capnp") (r "^0.8") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0ypd3fhz5hpr162q2xakwp2s8imfs5wgfsqpb6pjj91j7g9gj4hv")))

(define-public crate-capnp-futures-0.9.0 (c (n "capnp-futures") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "capnp") (r "^0.9") (d #t) (k 0)) (d (n "capnp") (r "^0.9") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1406i6x4x45yxkp9a12b8628xm49dgrc33gwwfnwwyr4j2g7czr5")))

(define-public crate-capnp-futures-0.9.1 (c (n "capnp-futures") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "capnp") (r "^0.9") (d #t) (k 0)) (d (n "capnp") (r "^0.9") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0mcjzi0zk6bqlhlb8m51f2h2xzpgc095sw54dwfkskhqqismq640")))

(define-public crate-capnp-futures-0.10.0 (c (n "capnp-futures") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "capnp") (r "^0.10.0") (d #t) (k 0)) (d (n "capnp") (r "^0.10.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0m6fhhkv6bqr40fn5hnnh1k0rx7dxv424ihc1y6zfkd11lrb5df6")))

(define-public crate-capnp-futures-0.10.1 (c (n "capnp-futures") (v "0.10.1") (d (list (d (n "capnp") (r "^0.10.0") (d #t) (k 0)) (d (n "capnp") (r "^0.10.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0qdiqkp9mh4acpa0dqhpzv2gwf949rj3m85mgwl1rih6gvgbh1zs")))

(define-public crate-capnp-futures-0.11.0 (c (n "capnp-futures") (v "0.11.0") (d (list (d (n "capnp") (r "^0.11.0") (d #t) (k 0)) (d (n "capnp") (r "^0.11.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1zfrzjc908xl697j32dglyp6wyp36hwmla421n82jb57jzjjbpsz")))

(define-public crate-capnp-futures-0.12.0 (c (n "capnp-futures") (v "0.12.0") (d (list (d (n "capnp") (r "^0.12.0") (d #t) (k 0)) (d (n "capnp") (r "^0.12.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0dgd709ck6hh9gf88izibcdi039qqygzq6c092y6r23pd0gpknz8")))

(define-public crate-capnp-futures-0.13.0 (c (n "capnp-futures") (v "0.13.0") (d (list (d (n "capnp") (r "^0.13.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std" "executor"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1hl0x954imbqiv1ka7bj2syayfqmkfsnnqvypijdhvkna0ax5sqj")))

(define-public crate-capnp-futures-0.13.1 (c (n "capnp-futures") (v "0.13.1") (d (list (d (n "capnp") (r "^0.13.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0nc4ikxhdnlq3jchz3s6cjnv7n5f47p7y55wxkby0v88mqfzzyg9")))

(define-public crate-capnp-futures-0.13.2 (c (n "capnp-futures") (v "0.13.2") (d (list (d (n "capnp") (r "^0.13.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "18q93ncbfcnjq7zhvy9idnifypmi2qcp775q7454y3r4lvvdcyyw")))

(define-public crate-capnp-futures-0.14.0 (c (n "capnp-futures") (v "0.14.0") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp") (r "^0.14.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1y7f8sf638q8w04806nwvhhrb91s5hzk0ax8r4nrgx7hj94h2j2k")))

(define-public crate-capnp-futures-0.14.1 (c (n "capnp-futures") (v "0.14.1") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp") (r "^0.14.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "10zrpagd3iw97dkdl9qx4qsc3bgk3yzlzrzyqniqkcdciy6285aa")))

(define-public crate-capnp-futures-0.14.2 (c (n "capnp-futures") (v "0.14.2") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp") (r "^0.14.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0fp6lr04w50mzfpxvvrbdm9pny8ch17514y7qgcsk6giqqf808cq")))

(define-public crate-capnp-futures-0.15.0 (c (n "capnp-futures") (v "0.15.0") (d (list (d (n "capnp") (r "^0.15.0-alpha") (d #t) (k 0)) (d (n "capnp") (r "^0.15.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1l6c6x307ldb7aygpqamvp2ndd05jvnmyvsvv8j2f8phd4acjq26")))

(define-public crate-capnp-futures-0.15.1 (c (n "capnp-futures") (v "0.15.1") (d (list (d (n "capnp") (r "^0.15.0-alpha") (d #t) (k 0)) (d (n "capnp") (r "^0.15.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0dwiryfrv274wcw4q9bznvl0vpmm74dcqq5pc10chlfscigmvpdd")))

(define-public crate-capnp-futures-0.15.2 (c (n "capnp-futures") (v "0.15.2") (d (list (d (n "capnp") (r "^0.15.0") (d #t) (k 0)) (d (n "capnp") (r "^0.15.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "022zzda8394sasdlpnljzlsa2dgcl9w7grirqnk9ryrdmvyc8p37")))

(define-public crate-capnp-futures-0.16.0 (c (n "capnp-futures") (v "0.16.0") (d (list (d (n "capnp") (r "^0.16.0") (d #t) (k 0)) (d (n "capnp") (r "^0.16.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "05vsm5yvx5a6k4cv6hvkx8hl77ipf6lkszd0351d9ik7srwj9gkc")))

(define-public crate-capnp-futures-0.17.0 (c (n "capnp-futures") (v "0.17.0") (d (list (d (n "capnp") (r "^0.17.0") (d #t) (k 0)) (d (n "capnp") (r "^0.17.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "163iq48mngz5rywj9amqya39fkzfhx4hy8vz6pg952r2mzh21mbi")))

(define-public crate-capnp-futures-0.18.0 (c (n "capnp-futures") (v "0.18.0") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)) (d (n "capnp") (r "^0.18.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1bx827s5na5yqpkl78y31wspvyn70m44a6yqrly1qka5x071jcfi")))

(define-public crate-capnp-futures-0.18.1 (c (n "capnp-futures") (v "0.18.1") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)) (d (n "capnp") (r "^0.18.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0qm7w8i594lk5gkz6wqg6542mjkqw5dcqv3kn3r6p883vh2sbry6")))

(define-public crate-capnp-futures-0.14.3 (c (n "capnp-futures") (v "0.14.3") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "capnp") (r "^0.14.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "16q04fxbikxgla9nd8v2fzl6x7cz0lbr9shssb38bk1ikif13mq6")))

(define-public crate-capnp-futures-0.15.3 (c (n "capnp-futures") (v "0.15.3") (d (list (d (n "capnp") (r "^0.15.0") (d #t) (k 0)) (d (n "capnp") (r "^0.15.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1gvfb0nimyqav12z7d6llrri40b9b005rrcslvbby4abcrpgbnzy")))

(define-public crate-capnp-futures-0.16.1 (c (n "capnp-futures") (v "0.16.1") (d (list (d (n "capnp") (r "^0.16.0") (d #t) (k 0)) (d (n "capnp") (r "^0.16.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "004rik3cinxf03h7whab07n4hdssad9xgvm4riv4ps26ghd0wn8l")))

(define-public crate-capnp-futures-0.17.1 (c (n "capnp-futures") (v "0.17.1") (d (list (d (n "capnp") (r "^0.17.0") (d #t) (k 0)) (d (n "capnp") (r "^0.17.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0l67dch5lzgwmbkckvgkslv2qahaijw263frki7d7dcqn5xnc056")))

(define-public crate-capnp-futures-0.18.2 (c (n "capnp-futures") (v "0.18.2") (d (list (d (n "capnp") (r "^0.18.0") (d #t) (k 0)) (d (n "capnp") (r "^0.18.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1wgq1khlzwhhw07k6bpkd45bjvrvdm11fa7hg3rly0avgy2pnsdq")))

(define-public crate-capnp-futures-0.19.0 (c (n "capnp-futures") (v "0.19.0") (d (list (d (n "capnp") (r "^0.19.0") (d #t) (k 0)) (d (n "capnp") (r "^0.19.0") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("executor"))) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "06zvxipmy0xdsxxqr54gs4yi8rgl33iprfhx4nzc0fsfncy4ib2z")))

