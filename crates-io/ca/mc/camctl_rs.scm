(define-module (crates-io ca mc camctl_rs) #:use-module (crates-io))

(define-public crate-camctl_rs-0.1.0 (c (n "camctl_rs") (v "0.1.0") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.0") (d #t) (k 0)))) (h "0pp093p4nrf1dyfmi43wlp49xlw8cvz7gmm1mahq88kc9gpki4f4")))

(define-public crate-camctl_rs-0.1.1 (c (n "camctl_rs") (v "0.1.1") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.0") (d #t) (k 0)))) (h "0ssmhh2n2pk3py8w3bnmdslk32mqsbahm2c9fp5schllfjnf5m2s")))

(define-public crate-camctl_rs-0.1.2 (c (n "camctl_rs") (v "0.1.2") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.0") (d #t) (k 0)))) (h "06w1zy7wnv6699sv8irdq2cfxckkh92qgcgn2xfjj1hpq47d9ghw")))

