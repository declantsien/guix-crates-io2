(define-module (crates-io ca mc camctrl) #:use-module (crates-io))

(define-public crate-camctrl-0.1.0 (c (n "camctrl") (v "0.1.0") (d (list (d (n "glam") (r "^0") (d #t) (k 0)) (d (n "rbt3") (r "^0") (d #t) (k 0)))) (h "0si4ca756w76m0g1sfjs7xwwx8kdfrnm4ax59q1i6w5ysxbcq7n4")))

(define-public crate-camctrl-0.2.0 (c (n "camctrl") (v "0.2.0") (d (list (d (n "glam") (r "^0") (d #t) (k 0)) (d (n "rbt3") (r "^0") (d #t) (k 0)))) (h "0wv2nz1mvw4hxhs2qix2cwncsqbxw4r5j9j6zi424f6c8wqnf5bj")))

