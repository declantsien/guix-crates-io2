(define-module (crates-io ca nr canrust) #:use-module (crates-io))

(define-public crate-canrust-1.0.0 (c (n "canrust") (v "1.0.0") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "0.15.*") (d #t) (k 0)))) (h "0mzd45a51nifak9lqxd1q1pvbf40gbsshm2zc5hi04ckfzxxfdga")))

(define-public crate-canrust-1.0.1 (c (n "canrust") (v "1.0.1") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "0.15.*") (d #t) (k 0)))) (h "0wnbr1rv6fxdnqm5gv3wd0ssgnkw0nppr3wy1qlynf9w32dd5kj6")))

(define-public crate-canrust-1.0.2 (c (n "canrust") (v "1.0.2") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "0.15.*") (d #t) (k 0)))) (h "19fxbz34z88vhv61sj462k8d8lvxw32awfy185faz3k0wq9z0k8y")))

(define-public crate-canrust-1.1.0 (c (n "canrust") (v "1.1.0") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "0.15.*") (d #t) (k 0)))) (h "13y7nzv5r6n1g7hicypa4sn9ssrlzy4vwi361mhdy64q6fpr6gb1")))

(define-public crate-canrust-1.2.1 (c (n "canrust") (v "1.2.1") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15") (d #t) (k 0)))) (h "0bnwmvgp33wh06qjwgzbrmw9ijh4vl3v1ia9z9x41x2klh2x847j")))

(define-public crate-canrust-1.3.1 (c (n "canrust") (v "1.3.1") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15") (d #t) (k 0)))) (h "1yq5ckjxhvbc9qxbb0421lglmjhq8n9azghg3f1lk550dbznljgs")))

(define-public crate-canrust-1.3.2 (c (n "canrust") (v "1.3.2") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15") (d #t) (k 0)))) (h "0bc4n8djayvw31606zjfdgymw7b53c8654b0d1v0jrwyxg0d3a4n")))

(define-public crate-canrust-1.3.3 (c (n "canrust") (v "1.3.3") (d (list (d (n "fontconfig") (r "^0.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15") (d #t) (k 0)))) (h "0aalsrrdjz6dfgxqizx1p1w88m8hnfq93drp8lisg67scrqsm0m6")))

