(define-module (crates-io ca nr canrun) #:use-module (crates-io))

(define-public crate-canrun-0.1.0 (c (n "canrun") (v "0.1.0") (d (list (d (n "canrun_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ishw9ck91zsd4v9klc2h3aai0bpxyrw3rrnhvq16glzsmb5w86n")))

(define-public crate-canrun-0.2.0 (c (n "canrun") (v "0.2.0") (d (list (d (n "canrun_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1aijlda5waza6j24cli35l6w15jww5pmdsb1ba3n4yslrnwbkg4i")))

(define-public crate-canrun-0.3.0 (c (n "canrun") (v "0.3.0") (d (list (d (n "canrun_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1lvqi057z8lcp4jiza07zc9adwv5m8b1vmjzbhc6rbwm65jma6k6")))

(define-public crate-canrun-0.4.0 (c (n "canrun") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)))) (h "06vralmgxl5pf76i9f469ggr6fg8qz44sl8hg4jp2yqbvcncd3g6")))

(define-public crate-canrun-0.5.0 (c (n "canrun") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)))) (h "0947qa52vsjx3hs321631pav9l9sriqk3vmmvvkgbh8kk4sgc677") (r "1.64.0")))

