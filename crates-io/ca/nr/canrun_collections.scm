(define-module (crates-io ca nr canrun_collections) #:use-module (crates-io))

(define-public crate-canrun_collections-0.1.0 (c (n "canrun_collections") (v "0.1.0") (d (list (d (n "canrun") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1d978y0y7zr859j0lgxww4vs5zkk7xgl31qs27yzffa6lihb1xyh")))

