(define-module (crates-io ca nr canrun_codegen) #:use-module (crates-io))

(define-public crate-canrun_codegen-0.1.0 (c (n "canrun_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "02n9aafm4hghg7r51iqy6w3cv9qlmfnn3fyjnf5h0kp44vhsyjlv")))

(define-public crate-canrun_codegen-0.2.0 (c (n "canrun_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1nx1vmw31xldvf2j8lrj4bvz4x8c9jqzzrp9b61xd3h4308xvj25")))

