(define-module (crates-io ca bl cable) #:use-module (crates-io))

(define-public crate-cable-0.1.0 (c (n "cable") (v "0.1.0") (h "08qdpwqjvq6jpbgcqgpxzkzsvh3gv9109vwq9d8i21hs8pj6msws")))

(define-public crate-cable-0.1.1 (c (n "cable") (v "0.1.1") (h "1bvdmszphjz07slxazih9kl013plzgrjhcphm2jfvimhlckrn8an")))

