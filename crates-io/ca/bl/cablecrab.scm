(define-module (crates-io ca bl cablecrab) #:use-module (crates-io))

(define-public crate-cablecrab-0.1.0 (c (n "cablecrab") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "pcap") (r "^0.10.1") (d #t) (k 0)) (d (n "pktparse") (r "^0.7.1") (d #t) (k 0)))) (h "0a32my7px4h6dasrw1qhvmwkbhkm6wga1zgxnngxz0prl4xjbrjv")))

(define-public crate-cablecrab-0.1.1 (c (n "cablecrab") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "pcap") (r "^0.10.1") (d #t) (k 0)) (d (n "pktparse") (r "^0.7.1") (d #t) (k 0)))) (h "086gssbmv75np72bkivsj13y7vrq3laxzwiarqfi8732m2gfa8yz")))

