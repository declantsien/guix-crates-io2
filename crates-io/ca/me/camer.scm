(define-module (crates-io ca me camer) #:use-module (crates-io))

(define-public crate-camer-1.0.0 (c (n "camer") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0h1dg7rp3rf42w8didr3d13zrzb9ghkcqc24jyfs4b6gc4flmb61")))

(define-public crate-camer-1.0.1 (c (n "camer") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1w21mcvl92ww9v6w27zdm9wrmarwgjw0mjlgssndli5ma48mnbdi")))

(define-public crate-camer-1.0.2 (c (n "camer") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1a2hxkvgmgiva1kfl3fmbg4y9plc8ilyp6h0s89584rppl5ywcjq")))

(define-public crate-camer-1.0.3 (c (n "camer") (v "1.0.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0r7qfas1b2dq5ma63xmiapglv1jrys876mfy2pma7xzy8nrci814")))

(define-public crate-camer-1.0.4 (c (n "camer") (v "1.0.4") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1lbid1z21w88968x0s4xhjdyv2gkliay1zzzd8r9ivh0qq5w7bn5")))

(define-public crate-camer-1.0.5 (c (n "camer") (v "1.0.5") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0z5bg9i6sr3apfsjkqz8m1b8axcn995mabrr8icwmq5c7y6rg9b0")))

(define-public crate-camer-1.0.6 (c (n "camer") (v "1.0.6") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "102f152n2shvfaxdpwa2h5kj4anl48aniv0p0n5zhid2bzcy6blj")))

(define-public crate-camer-1.0.7 (c (n "camer") (v "1.0.7") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0s94bdn3p2rw911nwqyk46n083w00m3l3ap6rkzjvi6wkwnqkqpd")))

(define-public crate-camer-1.0.8 (c (n "camer") (v "1.0.8") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0xbjprqbiffi1irqgy77c499ws1k9lpm6x286lsqad93plw3w94r")))

