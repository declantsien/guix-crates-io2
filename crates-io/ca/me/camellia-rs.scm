(define-module (crates-io ca me camellia-rs) #:use-module (crates-io))

(define-public crate-camellia-rs-0.1.0 (c (n "camellia-rs") (v "0.1.0") (h "0phngqbp6vifk430gsrzkkqiqjiwwzcwhfaf4q5p7wg2cb2vf51d")))

(define-public crate-camellia-rs-0.2.0 (c (n "camellia-rs") (v "0.2.0") (h "0czma457af4cpy3hsrcd4x9fnkari61j2bw59fmg4nfwkiz2jdcy")))

