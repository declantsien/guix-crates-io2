(define-module (crates-io ca me cameleon-impl) #:use-module (crates-io))

(define-public crate-cameleon-impl-0.1.0 (c (n "cameleon-impl") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cameleon-impl-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "08airs9154riiianlxf2i4a4zfisj0xd35yqscbn2wzwal8ifi5y")))

(define-public crate-cameleon-impl-0.1.4 (c (n "cameleon-impl") (v "0.1.4") (d (list (d (n "cameleon-impl-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "10h7n8hc8y5y0rf74bsiix6y7iws3dcn5bmbxinfjlnfdd8pdnad")))

(define-public crate-cameleon-impl-0.1.5 (c (n "cameleon-impl") (v "0.1.5") (d (list (d (n "cameleon-impl-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "1r7i3ldb6vwl6cwkq70pv3bxn6y261vbw1c0rmkhdq51z7p00g6s")))

(define-public crate-cameleon-impl-0.1.6 (c (n "cameleon-impl") (v "0.1.6") (d (list (d (n "cameleon-impl-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "04w1bybsvjawiz41c2k7mfh5b7372f90wpb6vqidp3b31qp628bz")))

(define-public crate-cameleon-impl-0.1.7 (c (n "cameleon-impl") (v "0.1.7") (d (list (d (n "cameleon-impl-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "19bb4l60np1x7d2q89mq9ywj98ajh5mq1640ndwnp3cqi467xcvf")))

(define-public crate-cameleon-impl-0.1.8 (c (n "cameleon-impl") (v "0.1.8") (d (list (d (n "cameleon-impl-macros") (r "^0.1.8") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lcw3v0lw5qysfg9d544cmj34yrbk298dnsnp0m09r2f1cl6f6i5")))

(define-public crate-cameleon-impl-0.1.9 (c (n "cameleon-impl") (v "0.1.9") (d (list (d (n "cameleon-impl-macros") (r "^0.1.9") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "1rqj9h3naakzv6yg596498pyziqlrm8aycncck1fr5752wdyn6hs")))

(define-public crate-cameleon-impl-0.1.10 (c (n "cameleon-impl") (v "0.1.10") (d (list (d (n "cameleon-impl-macros") (r "^0.1.10") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "18gxyadsidh2k02ffz979lc4d3xxidiffci6a8pyns58djva6wl0")))

(define-public crate-cameleon-impl-0.1.11 (c (n "cameleon-impl") (v "0.1.11") (d (list (d (n "cameleon-impl-macros") (r "^0.1.11") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fcx6g2b340rnab38l0rnjgvmv4xpkw19lwcahvpg2vr30d0bj7b")))

(define-public crate-cameleon-impl-0.1.12 (c (n "cameleon-impl") (v "0.1.12") (d (list (d (n "cameleon-impl-macros") (r "^0.1.12") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zfrrr78nl9z0a3fx18wavq9c3wbr2bdx7ky1f0h28kq9gvccn8a")))

