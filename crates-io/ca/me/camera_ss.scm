(define-module (crates-io ca me camera_ss) #:use-module (crates-io))

(define-public crate-camera_ss-0.1.0 (c (n "camera_ss") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "getset") (r "^0.0.6") (d #t) (k 0)))) (h "1vphsq54vidsqd70b6kawyygsg6sgrins7nsc5kwfsjwi79wws2m")))

(define-public crate-camera_ss-0.1.1 (c (n "camera_ss") (v "0.1.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)))) (h "05m0z3bi3ksz04niw4dvv4wrigqg51qsnq9pday8ks00hdzcpbmn")))

