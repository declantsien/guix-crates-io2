(define-module (crates-io ca me camelpaste) #:use-module (crates-io))

(define-public crate-camelpaste-0.1.0 (c (n "camelpaste") (v "0.1.0") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jc4hm383chw5cgdlan1z6m1bhxmv1sg76dysxslp8ba1rw8mn02") (r "1.31")))

