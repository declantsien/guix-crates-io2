(define-module (crates-io ca me cameleon-impl-macros) #:use-module (crates-io))

(define-public crate-cameleon-impl-macros-0.1.0 (c (n "cameleon-impl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxns2yhqh20jrpy61ybigdbdwsxyd57x5kly8a8hkmipf9r47ny")))

(define-public crate-cameleon-impl-macros-0.1.4 (c (n "cameleon-impl-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgp8jd6bgv0dmw9w2yazargfagi9lrr9i4xmrji35xa2mk98f8j")))

(define-public crate-cameleon-impl-macros-0.1.5 (c (n "cameleon-impl-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1p48zg9nyp9grv8ddnn9r3p8iqwn6f9x4gmfrvcr51rxpdq25qiy")))

(define-public crate-cameleon-impl-macros-0.1.6 (c (n "cameleon-impl-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7s9jilnbnw9ghhxxcw57kccskl1prqbxva4m7q9y481j4x86bs")))

(define-public crate-cameleon-impl-macros-0.1.7 (c (n "cameleon-impl-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1p0nyqfbky2v820pzdz4360m8hk1ai512pilkiqzbjyw74a286gn")))

(define-public crate-cameleon-impl-macros-0.1.8 (c (n "cameleon-impl-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "04gvjc2r32lmr4dr3kjr7pjd5pqx1ci33cc46zm55scm6znsz6yj")))

(define-public crate-cameleon-impl-macros-0.1.9 (c (n "cameleon-impl-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "07sk4h1wfi2zgd5qydl21qv1y5g97nv1r3z38fzsy50rky67hmki")))

(define-public crate-cameleon-impl-macros-0.1.10 (c (n "cameleon-impl-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1ndhjgbflz1amca6686pw8jyx4y5cppgwmqd01k9l9zvxkj4xwv2")))

(define-public crate-cameleon-impl-macros-0.1.11 (c (n "cameleon-impl-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "08gc76dbx0pcajay3lpbwax23avm207dmb3swscsrkjw9fqc7m5r")))

(define-public crate-cameleon-impl-macros-0.1.12 (c (n "cameleon-impl-macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1ll5pglq9m2fv7xc50gkhsfkhqwqlf21wglkshvyn438nk9sl2yb")))

