(define-module (crates-io ca me camera) #:use-module (crates-io))

(define-public crate-camera-0.1.0 (c (n "camera") (v "0.1.0") (d (list (d (n "camera_capture") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)))) (h "04qbakvw8j6fk0rchslyq0zh71i1x464lm0i7pckwv53vkbyrd7p")))

