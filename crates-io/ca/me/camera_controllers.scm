(define-module (crates-io ca me camera_controllers) #:use-module (crates-io))

(define-public crate-camera_controllers-0.0.2 (c (n "camera_controllers") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-event") (r "^0.0.12") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.7") (d #t) (k 0)) (d (n "quaternion") (r "^0.0.5") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.5") (d #t) (k 0)))) (h "1gf46xjxdlmdzl1njv9fskysv90ca1aqmzryn5wd22g4smb1vjrm")))

(define-public crate-camera_controllers-0.0.3 (c (n "camera_controllers") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "piston") (r "^0.1.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.0.7") (d #t) (k 0)) (d (n "quaternion") (r "^0.0.6") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.22") (d #t) (k 0)))) (h "02m28sw2a0zv1xan0nsxf6gh9bf0l1zq8j7aya18b9db48siq3vz")))

(define-public crate-camera_controllers-0.0.4 (c (n "camera_controllers") (v "0.0.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "piston") (r "^0.1.3") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.0.8") (d #t) (k 0)) (d (n "quaternion") (r "^0.0.7") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.23") (d #t) (k 0)))) (h "0rrqx9h9dwz545nhr6rdl3lal2dgx2mjxj64jh60v9g1bqbapzky")))

(define-public crate-camera_controllers-0.1.0 (c (n "camera_controllers") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "piston") (r "^0.1.4") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.0") (d #t) (k 0)))) (h "02fm4vlkl0dzm0ab44z66i0ddcwp3mfp8ii1hxxg4yywca4l807b")))

(define-public crate-camera_controllers-0.2.0 (c (n "camera_controllers") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "piston") (r "^0.2.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.0") (d #t) (k 0)))) (h "1ax03gpqlcnsjhm5q9r07azydm802wjry68r7nnc196xsr3awq2g")))

(define-public crate-camera_controllers-0.3.0 (c (n "camera_controllers") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston") (r "^0.3.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "1a9qd4w7crh6yzzb6lc4k1gfj11cfb0anqabvfrn1wlh9b7r9c5n")))

(define-public crate-camera_controllers-0.4.0 (c (n "camera_controllers") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston") (r "^0.4.1") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "12bf0mw8x220lx0jnqamswcgsypbqcnb4fj26x6yznh73sndar26")))

(define-public crate-camera_controllers-0.5.0 (c (n "camera_controllers") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "10m3jhp9gnlbxsb1ccfs565qf7x61vj587358wck4yjbphh2m383")))

(define-public crate-camera_controllers-0.6.0 (c (n "camera_controllers") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "1cw30z5n14ip79fnk90d6989pcpkh6n528vd5qbi0v1062vl3bll")))

(define-public crate-camera_controllers-0.7.0 (c (n "camera_controllers") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "1n91fkq4i6s0hmxvanpm2562gzj3nfi316yqlfp4r5wj0a6nx8kl")))

(define-public crate-camera_controllers-0.8.0 (c (n "camera_controllers") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1l74d4cm6qqnq1997crdkff1017m576yv01ic4yibhfkn2qipd4r")))

(define-public crate-camera_controllers-0.9.0 (c (n "camera_controllers") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1qvn2gkdn5pl4p0n5gj1yn0p8n2zci0iys5g4cycd6hhky8cdmf0")))

(define-public crate-camera_controllers-0.10.0 (c (n "camera_controllers") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1bhh3lgh1dxvpwj6acnn2g8bk9hfm4cblys92hh0n33inkqhflsv")))

(define-public crate-camera_controllers-0.11.0 (c (n "camera_controllers") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "066pslhxjckk4d7id6vz8bysflzy6r4chbzwahmpirvrgycwpa7p")))

(define-public crate-camera_controllers-0.12.0 (c (n "camera_controllers") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.9.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "0nb1s41c9vb1dznqydp6kri9q79fz9kn2f36grrpdxg94r8299zy")))

(define-public crate-camera_controllers-0.13.0 (c (n "camera_controllers") (v "0.13.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "05xdyc28czbfqmdjd2b8f7gfb6jzv5c0qdsc51rc6f5afliwgb52")))

(define-public crate-camera_controllers-0.14.0 (c (n "camera_controllers") (v "0.14.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "15rjmwirs8wx8aqyfw741agalh8dcamk6knr53xbfq8nrf15ss47")))

(define-public crate-camera_controllers-0.15.0 (c (n "camera_controllers") (v "0.15.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1kmq2h2ckwdx9504swrapyvdbggcmj4066j38gc8n74ab0vqyy5f")))

(define-public crate-camera_controllers-0.16.0 (c (n "camera_controllers") (v "0.16.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.13.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1d5k3vbkxgsvp2n73dj59q05fl73q7dv7b9hhb83lb4ykhk2g2mj")))

(define-public crate-camera_controllers-0.17.0 (c (n "camera_controllers") (v "0.17.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "0rrxmn390h5aip948wrvai7hjfvjqbj9mlrh24ghjilm7fv9sjpn")))

(define-public crate-camera_controllers-0.18.0 (c (n "camera_controllers") (v "0.18.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "1ca8sq5j39aih93asbinbwi740p3bsllprr10rm4xqh0drzf4mwf")))

(define-public crate-camera_controllers-0.19.0 (c (n "camera_controllers") (v "0.19.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0mcd9f9rynvk05x3g8z1ym8yq22pm8vsdkvrngbmpy3dlz6qlhx0")))

(define-public crate-camera_controllers-0.20.0 (c (n "camera_controllers") (v "0.20.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0nc2i0p4k9n7pc371ffn7if6wnsy6pmrbm88dd3y6cjfvncwa1bd")))

(define-public crate-camera_controllers-0.21.0 (c (n "camera_controllers") (v "0.21.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0hjwrdmjs210kfr42gkp62l3vz0x3302zy9phlj60ikyj4z75a9b")))

(define-public crate-camera_controllers-0.22.0 (c (n "camera_controllers") (v "0.22.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "00dbjpx6zpi9i5br0035m1zgpg7v8jq2plja57spgmpcvw1f6xks")))

(define-public crate-camera_controllers-0.23.0 (c (n "camera_controllers") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "11fs4jb961qpbnm6agcgl4x4xyj83zp3p05ggdgis4jmnlq7nx62")))

(define-public crate-camera_controllers-0.24.0 (c (n "camera_controllers") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0qk5dab9hf9k4whjwrasny2w4fl399q0xd192ck30f831vpw28vj")))

(define-public crate-camera_controllers-0.25.0 (c (n "camera_controllers") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1d70rjp829y261z04jwggq0wzh92vgvj8gxyp0q8sxcz3wgsalcg")))

(define-public crate-camera_controllers-0.26.0 (c (n "camera_controllers") (v "0.26.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0nmg9symrvv8bwdgzd8jhzbjlcgza22gxxhinyk3y3yyrfhs0w0p")))

(define-public crate-camera_controllers-0.27.0 (c (n "camera_controllers") (v "0.27.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1fpi9bc229lqhgrmqa9nanydz1x25w27py36xvww7r1i9wzln81x")))

(define-public crate-camera_controllers-0.28.0 (c (n "camera_controllers") (v "0.28.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "17zhb61ffmakdpa1hbmczhd1j6dhfp4nx95ilswiabdl4vij5xyq")))

(define-public crate-camera_controllers-0.29.0 (c (n "camera_controllers") (v "0.29.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0x3ima2bwi65f2c2hyzsm8wa41j8m0w24i03qh6ma0k0h7mnz00j")))

(define-public crate-camera_controllers-0.30.0 (c (n "camera_controllers") (v "0.30.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1ny7g2i8in2gzfwj465c3m7xc333j55px4l4cb7kdhpa2f5lnqrz")))

(define-public crate-camera_controllers-0.31.0 (c (n "camera_controllers") (v "0.31.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0n9ncdv8rix8isfpzirnxrnynjpj3b1w6h05bz15711zps6qlj55")))

(define-public crate-camera_controllers-0.32.0 (c (n "camera_controllers") (v "0.32.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "09pd3x5gqqjvyyf85g2hyhk86a5icha6prsgkz9dfb0sp4v5wb43")))

(define-public crate-camera_controllers-0.33.0 (c (n "camera_controllers") (v "0.33.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.6.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "quaternion") (r "^1.0.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "11fdhcw92whng2b8cmljvbzwgnik1f9p50pzhgfzv6r7mk5h8sjb")))

(define-public crate-camera_controllers-0.34.0 (c (n "camera_controllers") (v "0.34.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.6.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "quaternion") (r "^1.0.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1l4xdsrpnh48i4kig335a5blslr0k7pvqjhpknxsf0hcqn1yk3gy")))

