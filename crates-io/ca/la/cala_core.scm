(define-module (crates-io ca la cala_core) #:use-module (crates-io))

(define-public crate-cala_core-0.1.0 (c (n "cala_core") (v "0.1.0") (d (list (d (n "pasts") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "01v7wcgcd9jx99zc946p3sp55cpp4sjj9843jbq27zky4knd37g5") (f (quote (("docs-rs") ("default") ("cala"))))))

(define-public crate-cala_core-0.1.1 (c (n "cala_core") (v "0.1.1") (d (list (d (n "pasts") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17939zm80lxi0mqsvi98wv2hjasbbh132j5i2m201x30j8dkx4wx") (f (quote (("docs-rs") ("default") ("cala"))))))

