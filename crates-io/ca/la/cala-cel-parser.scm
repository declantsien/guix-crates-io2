(define-module (crates-io ca la cala-cel-parser) #:use-module (crates-io))

(define-public crate-cala-cel-parser-0.1.0-dev (c (n "cala-cel-parser") (v "0.1.0-dev") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1jj77qz69f3wk9nawr4pdizhvpgxlsx7i3p56vlj6hr7zgqmnwwg") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.0 (c (n "cala-cel-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0fypq40l94kkichapcdjlc7d8hfaxsgnbvpx5crkf4n3bmq23ah7") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.1 (c (n "cala-cel-parser") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "01qc13hsn8fh6yvs5g2p964a7g4i06phwnw5svqkz3cs6l0h0sgl") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.2 (c (n "cala-cel-parser") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1fj4n7dajsgk8z5kmhz2bijrnrf8qpmiij2g552m1p7wh6dpjlqp") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.3 (c (n "cala-cel-parser") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0c75mgi2jyym6vfpx302aj4l4cnnr4d6fpp6zvr94x8143vkd1yh") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.4 (c (n "cala-cel-parser") (v "0.1.4") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1bmix7i3ri82mpkdid563wskwnh9gr0hbqcr3fgggz3hdwypfr15") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.5 (c (n "cala-cel-parser") (v "0.1.5") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "15z8hmjkyvj03nha0znahlss96crzhlq7cvbrknqsw6g6hymz88q") (f (quote (("fail-on-warnings"))))))

(define-public crate-cala-cel-parser-0.1.6 (c (n "cala-cel-parser") (v "0.1.6") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1c94j86s5p90jbq44ys5vplg2kp1pxnsm79cc91z6hjiicxhig2h") (f (quote (("fail-on-warnings"))))))

