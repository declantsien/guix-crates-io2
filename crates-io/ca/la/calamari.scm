(define-module (crates-io ca la calamari) #:use-module (crates-io))

(define-public crate-calamari-0.1.0 (c (n "calamari") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "035p7amzc8h84m5b7zib5gdyv4zlkl12z55h4dk7f6rj4m3nfwzx")))

