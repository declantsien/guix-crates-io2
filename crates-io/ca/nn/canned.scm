(define-module (crates-io ca nn canned) #:use-module (crates-io))

(define-public crate-canned-0.0.1 (c (n "canned") (v "0.0.1") (h "1g02i02mixqk9llckicga4557siik7hamla50p91ldsrfmgr5bbw") (y #t)))

(define-public crate-canned-0.0.2 (c (n "canned") (v "0.0.2") (h "1zlxgy1qfnq8bsizmp2y0fp3dgjcbjkakcwzbixvqhpmryi7r2cd") (y #t)))

(define-public crate-canned-0.0.3 (c (n "canned") (v "0.0.3") (h "003b9qfyl1ydivnd998rs5pagk4zp2bvsqgdnp0fsrwb2kl3pj5z")))

