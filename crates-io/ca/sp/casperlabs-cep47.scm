(define-module (crates-io ca sp casperlabs-cep47) #:use-module (crates-io))

(define-public crate-casperlabs-cep47-0.2.0 (c (n "casperlabs-cep47") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "renvm-sig") (r "^0.1.1") (d #t) (k 0)))) (h "0gyr36d00k6akl1ai8dbvn3iyiswr5j99wgfny84h2f2iraa61yn") (f (quote (("default" "casper-contract/std" "casper-types/std" "casper-contract/test-support"))))))

(define-public crate-casperlabs-cep47-0.2.1 (c (n "casperlabs-cep47") (v "0.2.1") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1dvznivp63f6a5slaq9rq4ikhakpv7n6xsnmhyj60jy69lkxq1dk")))

