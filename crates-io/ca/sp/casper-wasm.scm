(define-module (crates-io ca sp casper-wasm) #:use-module (crates-io))

(define-public crate-casper-wasm-0.46.0 (c (n "casper-wasm") (v "0.46.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "162z2nlpkr186m476690cp2c6bxgn4q4190fmmpgdgwzg173rxa8") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

