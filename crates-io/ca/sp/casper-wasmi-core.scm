(define-module (crates-io ca sp casper-wasmi-core) #:use-module (crates-io))

(define-public crate-casper-wasmi-core-0.2.1 (c (n "casper-wasmi-core") (v "0.2.1") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "memory_units") (r "^0.4.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("num-bigint"))) (k 0)) (d (n "num-traits") (r "^0.2.8") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "region") (r "^3.0") (o #t) (d #t) (k 0)))) (h "131qv30wqfba3hhg290jp0a52g9p643rpmciry2g2909aqjrc230") (f (quote (("virtual_memory" "region" "std") ("std" "num-rational/std" "num-rational/num-bigint-std" "num-traits/std" "downcast-rs/std") ("default" "std"))))))

