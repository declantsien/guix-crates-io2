(define-module (crates-io ca sp casper-event-standard-macro) #:use-module (crates-io))

(define-public crate-casper-event-standard-macro-0.1.0 (c (n "casper-event-standard-macro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0fzyq34cdz2z8gqdrgwal09i6q8mpvmrcr7rz8h65bsxbbk2zmg2")))

(define-public crate-casper-event-standard-macro-0.1.1 (c (n "casper-event-standard-macro") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1xdar5jk4r216wp7lnryimjqlagba9249dfvmzriyyh2d1i7vgi2")))

(define-public crate-casper-event-standard-macro-0.2.0 (c (n "casper-event-standard-macro") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0piramwkdycmijp6qw17gy7vidl09givv45fbf3iykrzacwfy81m")))

(define-public crate-casper-event-standard-macro-0.3.0 (c (n "casper-event-standard-macro") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0325f9lq5idy8rrv05xnjs9jmqq94s54rnc5706i8025jbinbanr")))

(define-public crate-casper-event-standard-macro-0.4.0 (c (n "casper-event-standard-macro") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0hv2css85pdvflpkgjdzg13w2x4yk1sh7kd4wndlww82dks29kiz")))

(define-public crate-casper-event-standard-macro-0.4.1 (c (n "casper-event-standard-macro") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0c1pyn5jy9mwnpjglybrni593w2bffqd59vzjvknpfigdh7dwxdf")))

(define-public crate-casper-event-standard-macro-0.5.0 (c (n "casper-event-standard-macro") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "03lhi2m2hlg9a91yrn0adk4y5472dkkf90cv5sln6y1qr3k10n28")))

