(define-module (crates-io ca sp casper-wasmi-validation) #:use-module (crates-io))

(define-public crate-casper-wasmi-validation-0.5.0 (c (n "casper-wasmi-validation") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "casper-wasm") (r "^0.46.0") (k 0)))) (h "1fw0mnm3lair48yyw6wrfm76a0cwss43aifzgyjj3qrc2f2x6sgn") (f (quote (("std" "casper-wasm/std") ("default" "std"))))))

