(define-module (crates-io ca sp casperlabs-engine-wasm-prep) #:use-module (crates-io))

(define-public crate-casperlabs-engine-wasm-prep-0.1.0 (c (n "casperlabs-engine-wasm-prep") (v "0.1.0") (d (list (d (n "parity-wasm") (r "^0.31.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "1in4krmh4jgpph7983v08yy2nym0k9bfj50k67dnqv67p98mdzgv") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.2.0 (c (n "casperlabs-engine-wasm-prep") (v "0.2.0") (d (list (d (n "parity-wasm") (r "^0.31.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "types") (r "^0.2.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "0dxdr466pbb539xqnccyp4d7cfm1pvni4s3v5s8mxhgz2zfr2vfq") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.3.0 (c (n "casperlabs-engine-wasm-prep") (v "0.3.0") (d (list (d (n "parity-wasm") (r "^0.31.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "types") (r "^0.3.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "0dm2j8hlql4jhk909p888gb2h3lc76q801pvmw2wnni67ks45wir") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.4.0 (c (n "casperlabs-engine-wasm-prep") (v "0.4.0") (d (list (d (n "parity-wasm") (r "^0.31.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "types") (r "^0.4.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "148knbdcwyfkvqhlbq963gj7adxdkbng2ymcxyxygbcl34p9qvlj") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.4.1 (c (n "casperlabs-engine-wasm-prep") (v "0.4.1") (d (list (d (n "parity-wasm") (r "^0.31.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "types") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "0pxz6adv83kvcp807a1wnhx6wgakzc4wv1bfi1rgn1460wg0j0c6") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.5.0 (c (n "casperlabs-engine-wasm-prep") (v "0.5.0") (d (list (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "types") (r "^0.5.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "06bjdq6is57xprlhzzxk5a1kwhpfms8kx49nxi8fcsz65bigx9ri") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.5.1 (c (n "casperlabs-engine-wasm-prep") (v "0.5.1") (d (list (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "types") (r "^0.5.1") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "1wl58hb0ybpmps6pdq164z19viglqs2h2ha2xw4yfjl4q6w7ldvj") (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.6.0 (c (n "casperlabs-engine-wasm-prep") (v "0.6.0") (d (list (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "types") (r "^0.6.0") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "0qljxwfjlaz66jiczx4imad2c447lbyh2rj78glvp2drzdajvz6m") (f (quote (("no-unstable-features" "types/no-unstable-features")))) (y #t)))

(define-public crate-casperlabs-engine-wasm-prep-0.6.1 (c (n "casperlabs-engine-wasm-prep") (v "0.6.1") (d (list (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)) (d (n "pwasm-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "types") (r "^0.6.1") (f (quote ("std"))) (d #t) (k 0) (p "casperlabs-types")))) (h "15sahr9j1gpdm6698ylqhv5csjmq29qz8x16ai2rb0d1v7plj030") (f (quote (("no-unstable-features" "types/no-unstable-features")))) (y #t)))

