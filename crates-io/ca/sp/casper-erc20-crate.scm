(define-module (crates-io ca sp casper-erc20-crate) #:use-module (crates-io))

(define-public crate-casper-erc20-crate-0.1.0 (c (n "casper-erc20-crate") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "1w92iiip51fd1f5zyb0dkrvl3sys3p3v0anws5iqs881bb3ba2wj") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std"))))))

(define-public crate-casper-erc20-crate-0.1.1 (c (n "casper-erc20-crate") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "0hdpfq9gvw59lwydld6mzxz5ihnq5xqjvai1xc1gywaparwmwdpx") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std"))))))

(define-public crate-casper-erc20-crate-0.1.2 (c (n "casper-erc20-crate") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "012xhs52h69c84694s5jr6k7w47yhzyzzki8n2sw5b4v9mcwq97k") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std"))))))

(define-public crate-casper-erc20-crate-0.1.3 (c (n "casper-erc20-crate") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "1h9vmb47ziviphax6pi58w3xp8766ls36h30ipn3ip23nkpp7913") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std"))))))

