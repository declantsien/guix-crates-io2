(define-module (crates-io ca sp casperlabs-pausable) #:use-module (crates-io))

(define-public crate-casperlabs-pausable-0.1.0 (c (n "casperlabs-pausable") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "casperlabs-owned") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0jf2mm45426p4v4jqyc2yhvhl19mfjpqh0w7bhcx79shk5p36c09")))

