(define-module (crates-io ca sp casperlabs-ownable) #:use-module (crates-io))

(define-public crate-casperlabs-ownable-0.1.0 (c (n "casperlabs-ownable") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "=1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "02mr431cc8cd6hmymv3861z8c5s1fn529jfhlc7fq75gsk79qqa3")))

(define-public crate-casperlabs-ownable-0.2.0 (c (n "casperlabs-ownable") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "=1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "118hdy2cr5gby436ym1fqws0b79qcrrn5kbvlpks4nnqhahgrwwy")))

