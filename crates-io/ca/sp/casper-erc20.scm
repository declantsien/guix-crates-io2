(define-module (crates-io ca sp casper-erc20) #:use-module (crates-io))

(define-public crate-casper-erc20-0.1.0 (c (n "casper-erc20") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.3.2") (d #t) (k 0)) (d (n "casper-types") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "07yzmmm2zw13z1qzp6jwd82ai3y3w0h4nnx4g3iwf01wxk66ywpf") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std")))) (y #t)))

(define-public crate-casper-erc20-0.2.0 (c (n "casper-erc20") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.3.2") (d #t) (k 0)) (d (n "casper-types") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "005y0ji0rayf9sm9gkxkblhkdrjjsnxp90rd7sxaf1kihlfzj43w") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std")))) (y #t)))

(define-public crate-casper-erc20-0.2.1 (c (n "casper-erc20") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.3.2") (d #t) (k 0)) (d (n "casper-types") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "1dv5xrajh451z5r0s910jz910pqpnc3zidf2imclpsrwfayqli91") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std")))) (y #t)))

