(define-module (crates-io ca sp casperlabs-owned) #:use-module (crates-io))

(define-public crate-casperlabs-owned-0.1.0 (c (n "casperlabs-owned") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "17v0mmdjwp7amygcxxfn635mqqglhmisffihpvjnp4210hrfp27v")))

