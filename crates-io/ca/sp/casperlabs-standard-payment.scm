(define-module (crates-io ca sp casperlabs-standard-payment) #:use-module (crates-io))

(define-public crate-casperlabs-standard-payment-0.1.0 (c (n "casperlabs-standard-payment") (v "0.1.0") (d (list (d (n "types") (r "^0.3.0") (d #t) (k 0) (p "casperlabs-types")))) (h "0y8q9y9k0nh5sb6vipq3s5arjyfmzbnrvm02nyval83g96kzkwx5") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.2.0 (c (n "casperlabs-standard-payment") (v "0.2.0") (d (list (d (n "types") (r "^0.4.0") (d #t) (k 0) (p "casperlabs-types")))) (h "1zmfbdcldbwz468j943dg4aingcc3r6f7ww74qkppkcfqaxrkin9") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.2.1 (c (n "casperlabs-standard-payment") (v "0.2.1") (d (list (d (n "types") (r "^0.4.1") (d #t) (k 0) (p "casperlabs-types")))) (h "1hagrjy07sn05k2j6wc8ji4q32s4aafajjvr6g1hvghvfszh7yi1") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.3.0 (c (n "casperlabs-standard-payment") (v "0.3.0") (d (list (d (n "types") (r "^0.5.0") (d #t) (k 0) (p "casperlabs-types")))) (h "0y1yfxdk5yqmga2hg9hpmksfkcx2fcizlqiq3xv5q1jg3sygdrwm") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.3.1 (c (n "casperlabs-standard-payment") (v "0.3.1") (d (list (d (n "types") (r "^0.5.1") (d #t) (k 0) (p "casperlabs-types")))) (h "1b6n3qzkisldls5mffmxx2635v3xvy4rkir7q3yrnz7qbg2x1i32") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.4.0 (c (n "casperlabs-standard-payment") (v "0.4.0") (d (list (d (n "types") (r "^0.6.0") (d #t) (k 0) (p "casperlabs-types")))) (h "0al93zxnv43jhyrc984ch52vww0j01ficwg7j1p3rxcr1q40zz2x") (f (quote (("std" "types/std") ("no-unstable-features" "types/no-unstable-features")))) (y #t)))

(define-public crate-casperlabs-standard-payment-0.4.1 (c (n "casperlabs-standard-payment") (v "0.4.1") (d (list (d (n "types") (r "^0.6.1") (d #t) (k 0) (p "casperlabs-types")))) (h "0jdmysvbvvas8caphh4icbnbyhbn0s57mkiiffmvszx1645w0m2a") (f (quote (("std" "types/std") ("no-unstable-features" "types/no-unstable-features")))) (y #t)))

