(define-module (crates-io ca sp casperlabs-contract-utils) #:use-module (crates-io))

(define-public crate-casperlabs-contract-utils-0.1.0 (c (n "casperlabs-contract-utils") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0kbrr70lrgv3bghnkyy0g0kp25bki7x8k5kmvy9bncyxak24mln8")))

(define-public crate-casperlabs-contract-utils-0.1.1 (c (n "casperlabs-contract-utils") (v "0.1.1") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1wyx4y4x75p0083dm3qa8vca4nq6qs6n9z791j7qdpsf068swl6r")))

(define-public crate-casperlabs-contract-utils-0.1.2 (c (n "casperlabs-contract-utils") (v "0.1.2") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "12lbrsgrfp7f9sh6cjpwqh355fbxxagyx46xsfvmba7378zppj9j")))

(define-public crate-casperlabs-contract-utils-0.1.3 (c (n "casperlabs-contract-utils") (v "0.1.3") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0dbvfrkwbcmxyifs3irln9s3f22df0025zbkn1lbqv9zv26qq2pc")))

(define-public crate-casperlabs-contract-utils-0.1.4 (c (n "casperlabs-contract-utils") (v "0.1.4") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1f8k39xcd8g5akzdf8mk91ryva5f505l8rj0ld70rksl9v7yws38")))

(define-public crate-casperlabs-contract-utils-0.2.0 (c (n "casperlabs-contract-utils") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1489pdyhwvmp96x6d81b26gss31zmkphfynxj2h7883lkfh9a1f5") (f (quote (("default" "casper-contract/std" "casper-types/std" "casper-contract/test-support"))))))

(define-public crate-casperlabs-contract-utils-0.2.1 (c (n "casperlabs-contract-utils") (v "0.2.1") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0y31pa86dx8px6pm06wx3dn7wg938bg97li1n1x6l3vzazzd5kj5") (f (quote (("default" "casper-contract/std" "casper-types/std" "casper-contract/test-support"))))))

(define-public crate-casperlabs-contract-utils-0.2.2 (c (n "casperlabs-contract-utils") (v "0.2.2") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "19nzia9b5337vnagjcipp64bkgm4kd452113l8fgcjpxr632y3hz") (f (quote (("default" "casper-contract/std" "casper-types/std" "casper-contract/test-support"))))))

