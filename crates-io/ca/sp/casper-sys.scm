(define-module (crates-io ca sp casper-sys) #:use-module (crates-io))

(define-public crate-casper-sys-0.1.0 (c (n "casper-sys") (v "0.1.0") (d (list (d (n "libnv-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0gfc1hwfc8xvnl021s2lv4bpc9j38ng1hvg18ir4jzfizjliyc4s") (r "1.62")))

(define-public crate-casper-sys-0.1.1 (c (n "casper-sys") (v "0.1.1") (d (list (d (n "libnv-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0rf3m4vww42q314ni2dmgajn56j1v6bppzpyi2983g5g9825wx68") (r "1.62")))

