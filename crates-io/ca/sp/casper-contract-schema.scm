(define-module (crates-io ca sp casper-contract-schema) #:use-module (crates-io))

(define-public crate-casper-contract-schema-0.1.0 (c (n "casper-contract-schema") (v "0.1.0") (d (list (d (n "casper-types") (r "^3.0.0") (f (quote ("json-schema"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "04lahjhm1fq0i087c8rsgczh5h5myldndlplv4xg14wj596dbiab")))

(define-public crate-casper-contract-schema-0.2.0 (c (n "casper-contract-schema") (v "0.2.0") (d (list (d (n "casper-types") (r "^4.0.1") (f (quote ("json-schema"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0mbxvr0f3bik6wz34cm61dnxbsqn1vhd6cmbw96j1ia6p7yzwbgi")))

