(define-module (crates-io ca sp casperlabs-proof-of-stake) #:use-module (crates-io))

(define-public crate-casperlabs-proof-of-stake-0.1.0 (c (n "casperlabs-proof-of-stake") (v "0.1.0") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.3.0") (d #t) (k 0) (p "casperlabs-types")))) (h "0zhmjjvn0004d5w8jwbs19k3i12dfvkbiy5pvqag7k8xzgspsayp") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.2.0 (c (n "casperlabs-proof-of-stake") (v "0.2.0") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.4.0") (d #t) (k 0) (p "casperlabs-types")))) (h "13xjpy68cr1j7g4k8c8p2b3x5m1y2mnx3bb3nbxnsmbwzg23icjz") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.2.1 (c (n "casperlabs-proof-of-stake") (v "0.2.1") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.4.1") (d #t) (k 0) (p "casperlabs-types")))) (h "0n6sz51qcj38ln48g98chnalnpv55bp621hjzv261c83ngrp2sc3") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.3.0 (c (n "casperlabs-proof-of-stake") (v "0.3.0") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.5.0") (d #t) (k 0) (p "casperlabs-types")))) (h "14hyrkfx3fpnb72p8a4fwlaaa3rmjqjnsgy9aj2n1ss9rpxb86cd") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.3.1 (c (n "casperlabs-proof-of-stake") (v "0.3.1") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.5.1") (d #t) (k 0) (p "casperlabs-types")))) (h "1m3nn4vav5ywgskf5kmf4kkh0djn2hkylqyb1jws4zis7xfs7mzb") (f (quote (("std" "types/std")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.4.0 (c (n "casperlabs-proof-of-stake") (v "0.4.0") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.6.0") (d #t) (k 0) (p "casperlabs-types")))) (h "15hiwskxbskxclcrisdwldn0w43y322d8rzpkfaj7b4vm5iim316") (f (quote (("std" "types/std") ("no-unstable-features" "types/no-unstable-features")))) (y #t)))

(define-public crate-casperlabs-proof-of-stake-0.4.1 (c (n "casperlabs-proof-of-stake") (v "0.4.1") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "types") (r "^0.6.1") (d #t) (k 0) (p "casperlabs-types")))) (h "14h8qrb56p3p2svvr0r6365jmi8xy9nn14dl857841162xazjbzs") (f (quote (("std" "types/std") ("no-unstable-features" "types/no-unstable-features")))) (y #t)))

