(define-module (crates-io ca sp casper_utils) #:use-module (crates-io))

(define-public crate-casper_utils-0.1.0 (c (n "casper_utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1722wxp2akp06ragd8kv673zr5wd497pbpppg37a4xw6cw41hxc9")))

(define-public crate-casper_utils-0.2.1 (c (n "casper_utils") (v "0.2.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p4g7k87i3xlm6xg4p150kklflmszf72zlanj1kdqwfvgd92w86p")))

