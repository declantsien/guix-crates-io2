(define-module (crates-io ca sp casper_types_derive) #:use-module (crates-io))

(define-public crate-casper_types_derive-0.1.0 (c (n "casper_types_derive") (v "0.1.0") (d (list (d (n "casper-types") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "0a7san8742rpg1gn765ji1491ls61s0i5qy16vlqaybsb5cdb18i")))

