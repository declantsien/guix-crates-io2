(define-module (crates-io ca sp casperfpga_derive) #:use-module (crates-io))

(define-public crate-casperfpga_derive-0.1.0 (c (n "casperfpga_derive") (v "0.1.0") (d (list (d (n "casper_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nwxbgvyjb0k3pajhpw5hyiivm83widv2kp3sszxb88q2hnq9k12")))

(define-public crate-casperfpga_derive-0.1.1 (c (n "casperfpga_derive") (v "0.1.1") (d (list (d (n "casper_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "040hw50xs737al6i8r2sn378hnsa4ax3qj8g0mnb2m00hsf8yppm")))

(define-public crate-casperfpga_derive-0.2.0 (c (n "casperfpga_derive") (v "0.2.0") (d (list (d (n "casper_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l11x30iwgb7n10n30a1dd9w4vz94k7ij7av42zywc8kmwdg5v44")))

