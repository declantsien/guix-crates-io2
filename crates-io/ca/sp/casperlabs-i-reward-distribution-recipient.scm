(define-module (crates-io ca sp casperlabs-i-reward-distribution-recipient) #:use-module (crates-io))

(define-public crate-casperlabs-i-reward-distribution-recipient-0.1.0 (c (n "casperlabs-i-reward-distribution-recipient") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "casperlabs-ownable") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1f187pn8m2v0w8iasxk0jrw8i0d20f10xn3nxbyzcq08glyapir6")))

