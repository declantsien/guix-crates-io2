(define-module (crates-io ca sp casperlabs-lp-token-wrapper) #:use-module (crates-io))

(define-public crate-casperlabs-lp-token-wrapper-0.2.0 (c (n "casperlabs-lp-token-wrapper") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1dh05bp9kcnsbf96v493vkjyz51rgj737p1prf339xngwq25qmk9")))

(define-public crate-casperlabs-lp-token-wrapper-0.1.0 (c (n "casperlabs-lp-token-wrapper") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0224yaaq9r29jgf02pzh75c4pz1bp6kx8wcv0vsk6v3bpn0j8rf1")))

