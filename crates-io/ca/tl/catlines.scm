(define-module (crates-io ca tl catlines) #:use-module (crates-io))

(define-public crate-catlines-1.0.3 (c (n "catlines") (v "1.0.3") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "087k5bf9c8nhp455nk99z18ijaxhivwnl0jczr48lhgk4sj3hwa0")))

(define-public crate-catlines-1.0.4 (c (n "catlines") (v "1.0.4") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "18vin9nc1iqn9i3dj6lkp9k68pna7qnxni5hqckrm5y1q34yp0q1")))

