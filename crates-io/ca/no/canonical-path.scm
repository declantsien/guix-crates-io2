(define-module (crates-io ca no canonical-path) #:use-module (crates-io))

(define-public crate-canonical-path-0.0.0 (c (n "canonical-path") (v "0.0.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a7gvals7rn0bsqbfb82zkw94a0af3xajwb9jcfajv15y2sflv6a")))

(define-public crate-canonical-path-0.1.0 (c (n "canonical-path") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ga230nlnbsxf73c07irkwq582y39q32384f6x2ka8vr1d003n4x")))

(define-public crate-canonical-path-0.1.1 (c (n "canonical-path") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1p55jmrbiv4hbfap2rq2g3gncrbmqy4q9pjpcwxr9lwba6gkpa4s")))

(define-public crate-canonical-path-1.0.0 (c (n "canonical-path") (v "1.0.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1m1vkfq2z61hf6ynpzhq2p81wjzhd6h8rdav5gi3cllxic4xiqpg")))

(define-public crate-canonical-path-2.0.0 (c (n "canonical-path") (v "2.0.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04mi8szaszrh9awvn8k7cp9fj04id4mi8pyr08kgjqirhqvm6fg1") (y #t)))

(define-public crate-canonical-path-2.0.1 (c (n "canonical-path") (v "2.0.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12q9i8y7qybynvqk8pa98zva1r6frzzkh7laivzfz4wi9jkzhlaa") (y #t)))

(define-public crate-canonical-path-2.0.2 (c (n "canonical-path") (v "2.0.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vvsjda6ka5nz8zvx6r08zqi0j59sjccgcbjxj96xj764w9y1sg6")))

