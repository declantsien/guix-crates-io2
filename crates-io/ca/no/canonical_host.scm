(define-module (crates-io ca no canonical_host) #:use-module (crates-io))

(define-public crate-canonical_host-0.1.0 (c (n "canonical_host") (v "0.1.0") (d (list (d (n "canonical") (r "^0.1.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "18qpp73kd5pgchrv8sfncnldkmx9g7dzy92c022gni03jlnnwsma")))

(define-public crate-canonical_host-0.2.0 (c (n "canonical_host") (v "0.2.0") (d (list (d (n "canonical") (r "^0.2.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "07jsgfacafj1m8w1j6c7avd2hm9dhrgad4kph1d25sl0d9dyr31j")))

(define-public crate-canonical_host-0.2.1 (c (n "canonical_host") (v "0.2.1") (d (list (d (n "canonical") (r "^0.2.1") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "1r6zf64y86km52wq0093j0z1iax9an1g5cs7hbbq7bl0mmwyrmf4")))

(define-public crate-canonical_host-0.3.0 (c (n "canonical_host") (v "0.3.0") (d (list (d (n "canonical") (r "^0.3.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "14261fs2vmismybr8xy9s9azdng2wzfqva0pnwls6dslh18zqy8p")))

(define-public crate-canonical_host-0.4.0 (c (n "canonical_host") (v "0.4.0") (d (list (d (n "canonical") (r "^0.4.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "0zgk4g9hvcj9cvckzql8ynjmf9dr3p7rkybj874394mr6nz0faq0")))

(define-public crate-canonical_host-0.4.1 (c (n "canonical_host") (v "0.4.1") (d (list (d (n "canonical") (r "^0.4") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "16l592aqnfbczhimxl0y10p2x6pa4d7r3ys930pqk15wgscd7fkr")))

(define-public crate-canonical_host-0.4.2 (c (n "canonical_host") (v "0.4.2") (d (list (d (n "canonical") (r "^0.4") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "175r0h8g07w0p06knxlrkk2r2lam6pkyqzyjkn5a9fl9hxmmw8c7")))

(define-public crate-canonical_host-0.4.3 (c (n "canonical_host") (v "0.4.3") (d (list (d (n "appendix") (r "^0.2") (d #t) (k 0)) (d (n "canonical") (r "^0.4") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "wasmi") (r "^0.6.0") (d #t) (k 0)))) (h "12kdjk7kkfgmz9mqw2pzqaxvh76p87dy591yncqnpa8zxds3m90a")))

(define-public crate-canonical_host-0.5.0 (c (n "canonical_host") (v "0.5.0") (d (list (d (n "appendix") (r "^0.2") (d #t) (k 0)) (d (n "canonical") (r "^0.5") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "12afcqijq8irs1rqk5cls5sizjbp9anb3pwlfl3qva8pzs59v04s")))

