(define-module (crates-io ca no canopen-tokio) #:use-module (crates-io))

(define-public crate-canopen-tokio-0.0.1-alpha1 (c (n "canopen-tokio") (v "0.0.1-alpha1") (d (list (d (n "can-socket") (r "^0.0.1-alpha2") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1mv7sag5mkzm278j8c5i22w02m5jsdpfbi980clxr7r8pj3ls1lm")))

