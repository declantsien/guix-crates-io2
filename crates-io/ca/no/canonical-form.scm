(define-module (crates-io ca no canonical-form) #:use-module (crates-io))

(define-public crate-canonical-form-0.1.0 (c (n "canonical-form") (v "0.1.0") (h "0rg4gswcp1szwzjfqnhwwakdcj3inw7ibi8p6lhha86f9vdzfpai")))

(define-public crate-canonical-form-0.2.0 (c (n "canonical-form") (v "0.2.0") (h "0jqx09j3wih7y4bywjlk7za7ghcyxvzmygmr4sghbbwa60vgl579")))

(define-public crate-canonical-form-0.3.0 (c (n "canonical-form") (v "0.3.0") (h "1r5702f44v5qbyyrpkp3mmhvihih37kligka9m8dx8iis60vxapy")))

(define-public crate-canonical-form-0.4.0 (c (n "canonical-form") (v "0.4.0") (h "0c5fwhin4p22kffl3w7j5d4isrf75grfrm52shqlync2kr80bhaz")))

(define-public crate-canonical-form-0.5.0 (c (n "canonical-form") (v "0.5.0") (h "1mgvkjrx8q0hbnl9bnpiv3zk057cv2i7kpbwcv5dgr7a45d7c4zc")))

(define-public crate-canonical-form-0.6.0 (c (n "canonical-form") (v "0.6.0") (h "015x2kdyby6lpacl3h3q197sfq7kx42cb405ahvvbhkdi9hviqc3")))

(define-public crate-canonical-form-0.7.0 (c (n "canonical-form") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "13mwwwdmhyc6da5c478gali3bmbhkph73r7grn6aqmbdx7z7hbz5")))

(define-public crate-canonical-form-0.8.0 (c (n "canonical-form") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "08z0b2q6zafy67cvxikadfnbv7aybniy9xvjlhhk0c7ad8f5s3j4")))

(define-public crate-canonical-form-0.9.0 (c (n "canonical-form") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "14fd6k7ss6q8n7xznv1xy86c8a6mfbkr5f6hbrm7irqsfk5v744s")))

(define-public crate-canonical-form-0.9.1 (c (n "canonical-form") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1cq1wzjqhk7lr4zvv5mld9pwkdi3z30rhg41gjqwhyr2kwrn583a")))

(define-public crate-canonical-form-0.9.2 (c (n "canonical-form") (v "0.9.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "15cz1gn85kslvmqlsx86zwvccj69fiz35s61frpkka3z0fp2jakf")))

(define-public crate-canonical-form-0.9.4 (c (n "canonical-form") (v "0.9.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0rw0wkmk35ycl0jznbs3av5sijpjpl6sbx1gxk0d8k98l1zjs9jg")))

(define-public crate-canonical-form-0.10.0 (c (n "canonical-form") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0j8kgkhqdq0w5kpckyswwvis80bbxnji0inb8vaqclyi1lzns1v5")))

