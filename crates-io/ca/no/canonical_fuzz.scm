(define-module (crates-io ca no canonical_fuzz) #:use-module (crates-io))

(define-public crate-canonical_fuzz-0.2.0 (c (n "canonical_fuzz") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 0)) (d (n "canonical") (r "^0.2.0") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_host") (r "^0.2.0") (d #t) (k 0)))) (h "00k1gp29z6klrjvgayjmvdc7jl25m0vdbm8jm8510hg9d2qgn8hd")))

(define-public crate-canonical_fuzz-0.2.1 (c (n "canonical_fuzz") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 0)) (d (n "canonical") (r "^0.2.1") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_host") (r "^0.2.1") (d #t) (k 0)))) (h "0wxl216g5p9dvsws1xqp3pnkfz569krvnb5q3dicg6h1171h2p2n")))

(define-public crate-canonical_fuzz-0.4.0 (c (n "canonical_fuzz") (v "0.4.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 0)) (d (n "canonical") (r "^0.4.0") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_host") (r "^0.4.0") (d #t) (k 0)))) (h "1scr6wbi953anxlkcv5xv19h1icv1ix0makqn6l0pb9mx5jp9sc2")))

(define-public crate-canonical_fuzz-0.5.0 (c (n "canonical_fuzz") (v "0.5.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 0)) (d (n "canonical") (r "^0.5") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_host") (r "^0.5") (d #t) (k 0)))) (h "0sg5ci17xny9f52jg6aj0iazmmmqf425fcq3q3blmciyh573pmgi")))

(define-public crate-canonical_fuzz-0.6.0 (c (n "canonical_fuzz") (v "0.6.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 0)) (d (n "canonical") (r "^0.6.0") (d #t) (k 0)))) (h "0h0in6gh39sg6avx16mnrnpz24j43kd28nj2iyxh0372xf6m2w92")))

(define-public crate-canonical_fuzz-0.6.2 (c (n "canonical_fuzz") (v "0.6.2") (d (list (d (n "arbitrary") (r "^1.0") (d #t) (k 0)) (d (n "canonical") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0arls5sig87gvvfiadd6cdz71m6f5f91qh7nw5xd21hn76agbspa")))

(define-public crate-canonical_fuzz-0.7.0 (c (n "canonical_fuzz") (v "0.7.0") (d (list (d (n "arbitrary") (r "^1.0") (d #t) (k 0)) (d (n "canonical") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "15jr8am92rw1l95ddrb8y9572qzs8axc5msix4r647i28c8jiifd")))

