(define-module (crates-io ca no canonical_derive) #:use-module (crates-io))

(define-public crate-canonical_derive-0.1.0 (c (n "canonical_derive") (v "0.1.0") (d (list (d (n "canonical") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1if5zlv76faa0v5yxbkpdg0l89kk31ac3zi99zbkmhi558v08m8z")))

(define-public crate-canonical_derive-0.2.0 (c (n "canonical_derive") (v "0.2.0") (d (list (d (n "canonical") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "13fbw6k3cwz8pdm5yvyk9h7acs3gcrqs6yhqjy0d10znsyfc90yv")))

(define-public crate-canonical_derive-0.2.1 (c (n "canonical_derive") (v "0.2.1") (d (list (d (n "canonical") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0h6giia181c0xcc33j8n2spi64l39pi36x0gf42mi7dn648mh2ch")))

(define-public crate-canonical_derive-0.3.0 (c (n "canonical_derive") (v "0.3.0") (d (list (d (n "canonical") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "00s1lflqp9mgsmfbn05rckwa7p4m22vvyd8phji5a8k91128lh6a")))

(define-public crate-canonical_derive-0.4.0 (c (n "canonical_derive") (v "0.4.0") (d (list (d (n "canonical") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1jgl6vijjp5ir2q0jrb2lkdbi03zbmdhbipcilnqzg6jlsd193wi")))

(define-public crate-canonical_derive-0.4.1 (c (n "canonical_derive") (v "0.4.1") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0clfgr7dzar4bpfisym5mqixb47rgcrhcyy7r93zh2cw2z6q45jn")))

(define-public crate-canonical_derive-0.5.0 (c (n "canonical_derive") (v "0.5.0") (d (list (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r8v5f34z3jzw734b7hr2x4l1hgw8xmhj26wqd3lr05rymmbpvpi")))

(define-public crate-canonical_derive-0.6.0 (c (n "canonical_derive") (v "0.6.0") (d (list (d (n "arbitrary") (r "^0.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07fqwps2g0cfs2qzidbf6bd0285pc5h70lsi2vm25k3wvdqph52w")))

(define-public crate-canonical_derive-0.6.2 (c (n "canonical_derive") (v "0.6.2") (d (list (d (n "arbitrary") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z9ak5rsy2vdzhzj93anlc19xf9diaz0p5dbhxfm78b37hrgb2by") (y #t)))

(define-public crate-canonical_derive-0.6.3 (c (n "canonical_derive") (v "0.6.3") (d (list (d (n "arbitrary") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "094qpynbdrzhnykk8l8kxrkw008wgb0hkvxs0wcl1qvv9h2wq7bc") (y #t)))

(define-public crate-canonical_derive-0.7.0 (c (n "canonical_derive") (v "0.7.0") (d (list (d (n "arbitrary") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "canonical") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i913lkass2dbj72a93jnij93xx2qscmkczys60fqf7f84ss5gfg")))

