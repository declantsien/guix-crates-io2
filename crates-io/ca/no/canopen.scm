(define-module (crates-io ca no canopen) #:use-module (crates-io))

(define-public crate-canopen-0.1.0 (c (n "canopen") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.11.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "0yz3an19iw6ymb3wz6qv2iwys1rlsd0s86548sah88jcih0c0fg9")))

