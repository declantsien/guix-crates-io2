(define-module (crates-io ca no canonical_json) #:use-module (crates-io))

(define-public crate-canonical_json-0.1.0 (c (n "canonical_json") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j1fjls6hw1q6a48fhczmcahhq2l7y1sk2gk2b2z7kxs4bhxj236")))

(define-public crate-canonical_json-0.2.0 (c (n "canonical_json") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15abj5yn43sn6sqwgrr86hlq1ha0xa33ff1c0br187vrmvpksdhm")))

(define-public crate-canonical_json-0.3.0 (c (n "canonical_json") (v "0.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ddhi3cybh4r3qh73kpjkbzn8i8sx9js4cdq7sa0pgma1fh1yrh7")))

(define-public crate-canonical_json-0.4.0 (c (n "canonical_json") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gxwlkkcwr16027n48b6yrlilzz9gyqlnx9ic2rrzsangnr69x34")))

(define-public crate-canonical_json-0.5.0 (c (n "canonical_json") (v "0.5.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "042p9h363qprpa8bx3b6hy2sqkc6a304lzwdf5xc8wad07yq747q")))

