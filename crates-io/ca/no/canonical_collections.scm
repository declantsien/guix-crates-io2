(define-module (crates-io ca no canonical_collections) #:use-module (crates-io))

(define-public crate-canonical_collections-0.1.0 (c (n "canonical_collections") (v "0.1.0") (d (list (d (n "canonical") (r "^0.1.0") (f (quote ("host"))) (d #t) (k 0)) (d (n "canonical_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "canonical_host") (r "^0.1.0") (d #t) (k 2)))) (h "08ispsn2c1k4pkf0589w16nc4yb5qi3vxv1wkyi07hspryhacb36") (f (quote (("hosted" "canonical/hosted") ("host" "canonical/host"))))))

(define-public crate-canonical_collections-0.2.0 (c (n "canonical_collections") (v "0.2.0") (d (list (d (n "canonical") (r "^0.2.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1qisxdh83idgz93i497sq2a0hg72736na4a1z7j003n5sv4qzvkp") (f (quote (("host" "canonical/host"))))))

(define-public crate-canonical_collections-0.2.1 (c (n "canonical_collections") (v "0.2.1") (d (list (d (n "canonical") (r "^0.2.1") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "canonical_host") (r "^0.2.1") (d #t) (k 2)))) (h "10cx8avqssrhql8mq0cdw9k0f8jcqylf28hnl2i3xphl9ib9c1mq") (f (quote (("host" "canonical/host"))))))

(define-public crate-canonical_collections-0.3.0 (c (n "canonical_collections") (v "0.3.0") (d (list (d (n "canonical") (r "^0.3.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "canonical_host") (r "^0.3.0") (d #t) (k 2)))) (h "1drmnkms2q553ab8vpvywbav7vm20wwalw6zl6frnrhzjr98szkw") (f (quote (("host" "canonical/host"))))))

(define-public crate-canonical_collections-0.4.0 (c (n "canonical_collections") (v "0.4.0") (d (list (d (n "canonical") (r "^0.4.0") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4.0") (d #t) (k 2)))) (h "0mzpmj90ba7zq4nbmy1dz4vb1bn6p3a3gpb36grv03h70xzmg3vb") (f (quote (("host" "canonical/host"))))))

