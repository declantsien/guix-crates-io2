(define-module (crates-io ca no canonicalize) #:use-module (crates-io))

(define-public crate-canonicalize-0.1.0 (c (n "canonicalize") (v "0.1.0") (h "1rxvdhbrmzj57z2rwdcz9f4d8xsjax89wk5jxxh7az5rxb1hxjm4")))

(define-public crate-canonicalize-0.1.1 (c (n "canonicalize") (v "0.1.1") (h "0i3cbc7791h99d0gnwh4bnccqzn9zgmwyqggykzhbbxw59fwrw56")))

(define-public crate-canonicalize-0.1.2 (c (n "canonicalize") (v "0.1.2") (h "0di2yzzh8giv147ak2ahjyl89608p6iihcw859m78qznsdx4613r")))

(define-public crate-canonicalize-0.1.3 (c (n "canonicalize") (v "0.1.3") (h "05hq62jm3ciy4yjcxshr0y62hfywhzjv362sv065k90yd1ncyj55")))

(define-public crate-canonicalize-0.1.4 (c (n "canonicalize") (v "0.1.4") (h "15g6mydia8q2mqh1i435h9jhk1fsyr1g1i9iv6jay7nixpz8ddf4")))

(define-public crate-canonicalize-0.1.5 (c (n "canonicalize") (v "0.1.5") (h "0w7rag46431a0hzhci7pqhb055zbcma3kwbn5g3pwzp6a6b0z772") (f (quote (("std") ("default" "std"))))))

