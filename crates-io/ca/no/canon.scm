(define-module (crates-io ca no canon) #:use-module (crates-io))

(define-public crate-canon-0.1.0 (c (n "canon") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0baz5833yhrvk387dsh2hza3w1d2xrnnjas7ggb8w4y8s9k5qzqm")))

(define-public crate-canon-0.1.1 (c (n "canon") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "10pf6shjcybqakva4sc9fap61fk8373vnmjsx315hj01zdw2jzyj")))

