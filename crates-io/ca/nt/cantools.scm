(define-module (crates-io ca nt cantools) #:use-module (crates-io))

(define-public crate-cantools-0.1.0 (c (n "cantools") (v "0.1.0") (h "02kizqy3fsc15rdmmcsdvmd0vj4sdb9dfv42n8vhkvy750a2gc73") (y #t)))

(define-public crate-cantools-0.2.0 (c (n "cantools") (v "0.2.0") (h "1wjb7rwvxbm27k7xawhvaw5y8b90fayi50fkbdsfc5js13blqz58")))

(define-public crate-cantools-0.2.1 (c (n "cantools") (v "0.2.1") (h "0s0bkc7iaiacvidfcrn6ghyl96dc6z99dafasbajfhq8gk9swkzq")))

