(define-module (crates-io ca nt cantor_macros) #:use-module (crates-io))

(define-public crate-cantor_macros-0.1.0 (c (n "cantor_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0xjpngmzbivsijwfz0jrn9rad58ikpq2pb0lghcb21hair25g03y")))

(define-public crate-cantor_macros-0.1.1 (c (n "cantor_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1lagw4m6zsz7xdisfcwh0dzwjnsdb4x1dz0xqncv8lw80hxa47c1")))

(define-public crate-cantor_macros-0.1.2 (c (n "cantor_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0p119l7hv482g4inivpsjbz56w2hdwy10459g3f4n2dc05j42qrx")))

