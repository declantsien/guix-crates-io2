(define-module (crates-io ca nt canteen) #:use-module (crates-io))

(define-public crate-canteen-0.1.1 (c (n "canteen") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "08qwg33yg3jya28ljqms5aa37sk4a3ybrz59sdynyhpx1f84xb8j")))

(define-public crate-canteen-0.1.2 (c (n "canteen") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1sd2mwjxy6hmjal960vvff3mjg0pfqgwalqrv32j9rgg56wp5fbf")))

(define-public crate-canteen-0.1.3 (c (n "canteen") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0z8gddyf7y4hrzns1b2sck8q2860f2r7y603sbpp250ljrinarz5")))

(define-public crate-canteen-0.2.1 (c (n "canteen") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "106f34hjci2s1graixg0yk2jy7yl97bn87g3lxhqkxnsvmyk24zv")))

(define-public crate-canteen-0.2.2 (c (n "canteen") (v "0.2.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1c0zgan8br3a4vkf5gdii0dc7w850dgaihrhl273y9bgd0sryrkl")))

(define-public crate-canteen-0.2.3 (c (n "canteen") (v "0.2.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "191q73afrvgr0w6wlv0hmykn4kvjz40i18b2gnnffm5nxn0wg0l9")))

(define-public crate-canteen-0.2.4 (c (n "canteen") (v "0.2.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1cw2xls7mp33rlxvf5lqjc9iq6a0hnwk3ndz5cjl8mgkk2ns2rg5")))

(define-public crate-canteen-0.2.5 (c (n "canteen") (v "0.2.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "083pgg6dgkw3h4yqi20bychsvhdmal9a8j5b1ail195mnn39hcq9")))

(define-public crate-canteen-0.2.6 (c (n "canteen") (v "0.2.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dp1spa6nc4r2fagq5zmqpm3lhmnznnm4mn6sh0xvxkj3k7sw5lq")))

(define-public crate-canteen-0.3.1 (c (n "canteen") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1ya1pl0f0sxq406g7kgmvxrgvknh5506fxpj2d2p0scxgix0zybm")))

(define-public crate-canteen-0.3.2 (c (n "canteen") (v "0.3.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1wvvvfi2cpm103ssyrsavw4vcw9r5wk63g3q39q52c28p3qsx9f5")))

(define-public crate-canteen-0.3.3 (c (n "canteen") (v "0.3.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0pi4vrqc2gmqfaaa0q6wp4dqqiqz2s3x01yxfsl1j07sl968pwlr")))

(define-public crate-canteen-0.3.4 (c (n "canteen") (v "0.3.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1x6yw6f79dda0b7lm9pmqpn6cy9k0yijgn8ml2zh71l6112l8wnd")))

(define-public crate-canteen-0.3.5 (c (n "canteen") (v "0.3.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0n1m83rbz1ljyds1bv6zw6ifqbxwyi1r9ziac5gvkx1gc5xrb17v")))

(define-public crate-canteen-0.3.6 (c (n "canteen") (v "0.3.6") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "00rbyna9rx1mpzdic8pdi5prdgqrv13ac0sax6fibpmmh88sc5m0")))

(define-public crate-canteen-0.3.7 (c (n "canteen") (v "0.3.7") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0cg71lmr7psk03zd5r21m8ms5pvllg020fc0lvms0w9aa744j9rf")))

(define-public crate-canteen-0.4.0 (c (n "canteen") (v "0.4.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0y8xgwqx7qrfclnh0s2n35bvpghdjd3zrgrr5igpza4rnc65fdk6")))

(define-public crate-canteen-0.4.1 (c (n "canteen") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0g1dzkcgyxlxgyl63g6z9away82qxggisx7043zc1mbdjailh5s6")))

(define-public crate-canteen-0.5.0 (c (n "canteen") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0ix4yjd85c5y7fy5p3k0ibb4dzshrgaflqhj5xdyh2ib2c9rkzbm")))

(define-public crate-canteen-0.5.1 (c (n "canteen") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "08cf3h7646s1jsxnhiqrzz9854ck6vm7agbm4w5rwy4frbhxd7n3")))

(define-public crate-canteen-0.5.2 (c (n "canteen") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0zad2m20d0dv3r7znzjc1nib0n57g1y790fsg30nlz8svhbip21l")))

(define-public crate-canteen-0.5.3 (c (n "canteen") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1ss5h2kphh6xq5cmk476lspnhmmxy78ymj9c14gjgxd3dczx9qhi")))

(define-public crate-canteen-0.5.4 (c (n "canteen") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1g40i5544bj374akfw6kdrij9i9xv7n2myys92xj5wd7l4p7lxhc")))

(define-public crate-canteen-0.5.5 (c (n "canteen") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "07fs1svz4w4s3smz4aaxcl29d9gb88sw1z9bvnwbcqxwrg31mij0")))

(define-public crate-canteen-0.5.6 (c (n "canteen") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "00p22ql3njydcszsnx7z1g1v22c9234pgs9kf4as1g24fwh60nap")))

