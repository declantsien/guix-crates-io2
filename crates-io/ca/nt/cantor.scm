(define-module (crates-io ca nt cantor) #:use-module (crates-io))

(define-public crate-cantor-0.1.0 (c (n "cantor") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "cantor_macros") (r "^0.1.0") (d #t) (k 0)))) (h "049c4lh9ri3bq2yv4mmlnqzc4ysy09kdw40jkrn8gyxqpijqkcl8")))

(define-public crate-cantor-0.1.1 (c (n "cantor") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "cantor_macros") (r "^0.1.0") (d #t) (k 0)))) (h "02ri4j9h7f0s109nc0gfsxrnvij6dx3d9gr305r9wz3s0872vq8c")))

(define-public crate-cantor-0.1.2 (c (n "cantor") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "cantor_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0wzcy0369gsx9zq9kfy7kwv6f8igy07cny2cpy3vb0rm4xs6ndx0")))

(define-public crate-cantor-0.1.3 (c (n "cantor") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "cantor_macros") (r "^0.1.2") (d #t) (k 0)))) (h "01hnxzqy6q804akpmnjs2j4i534k03niwy5101napzwyshxqhgip")))

