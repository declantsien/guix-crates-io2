(define-module (crates-io ca nt canticle) #:use-module (crates-io))

(define-public crate-canticle-0.0.1 (c (n "canticle") (v "0.0.1") (h "1p5kc6vzd8665ab5qy0np9ax9cid60k9d1smf8w48xsa5cdrqpr1")))

(define-public crate-canticle-0.0.2 (c (n "canticle") (v "0.0.2") (h "0n1spik6i4lv05q98590ggynwyr00md9iyg1mlfhm5id91f1b9wa")))

(define-public crate-canticle-0.0.2-wip (c (n "canticle") (v "0.0.2-wip") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1w8gchyjgsw80pqz604m3jkyp2plarn1vvxh77zlr2ss98qlnc9n")))

(define-public crate-canticle-0.0.3-wip (c (n "canticle") (v "0.0.3-wip") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "0yl699ryck36ha0p3zwsajfxi0jyxqkymh8549i3qm1z6x50alfq")))

(define-public crate-canticle-0.0.3 (c (n "canticle") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1dr8mr4qgpdcs1rgn4d6sxjxx9vk00yfiqcwsqzwlc35ni769pgf")))

(define-public crate-canticle-0.0.4 (c (n "canticle") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1fzm06m8w1nywdgv4vzrxiniq777k2c2z6j5szy9478djlgn8q93")))

(define-public crate-canticle-0.0.5 (c (n "canticle") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1g53vbhwn5r14a0blgq8vwq4n9hx4ha2bpcb3rv5h9kpccd21cd3")))

(define-public crate-canticle-0.0.6 (c (n "canticle") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1gp82br4zpcrxgxsqg8x78xrirjhi5sl4za9mj5daadkrzsgv75z")))

