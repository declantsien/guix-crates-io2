(define-module (crates-io ca pw capwriter) #:use-module (crates-io))

(define-public crate-capwriter-0.1.0 (c (n "capwriter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "1h5hzlbiy9wji97y7i5rwcg5cl2p0rym6ib1x8p5gx592gam6wm6")))

(define-public crate-capwriter-0.1.1 (c (n "capwriter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "1dxcs8gvvada28201drvybkvpcwq35c2pn4g52ip486l3x8zw69a")))

(define-public crate-capwriter-0.1.2 (c (n "capwriter") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "0infyvk7jdajdwcf9xxr4zicf4cl6pbykcg12yvrkmr52plk57ag")))

(define-public crate-capwriter-0.1.3 (c (n "capwriter") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "0mh151f3n1xqvaz1w9j411lg42cyl27mzhjavlxw0467b2y59fwm")))

(define-public crate-capwriter-0.1.4 (c (n "capwriter") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "15rcyz8sl2a21isf1v0vkh3gzlmcdn5l2h7al2a7cb0qyc5h0k6a")))

(define-public crate-capwriter-0.2.0 (c (n "capwriter") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "0b0wf1yjfflgcpqswczi9svwry9bh4a8p60kn4b0yjkqjy0l3g93")))

