(define-module (crates-io ca nb canbench-rs-macros) #:use-module (crates-io))

(define-public crate-canbench-rs-macros-0.1.0 (c (n "canbench-rs-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j28b36aisavgrv4n3psqq9sjmq9x5x2si7p122h3ryj3yjd9b5m")))

(define-public crate-canbench-rs-macros-0.1.4 (c (n "canbench-rs-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "191z55s8nasggxgr19lb4sdnw03wraf2b056lw13i2hkfxn90m6l")))

