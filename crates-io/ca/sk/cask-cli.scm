(define-module (crates-io ca sk cask-cli) #:use-module (crates-io))

(define-public crate-cask-cli-0.1.0 (c (n "cask-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)))) (h "0fsndg2xv9806j2l1mz2bim8b93i1929mb15hd15464vz2ki6z8c")))

(define-public crate-cask-cli-0.2.0 (c (n "cask-cli") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "run_script") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)))) (h "176pwwa191yma037gnm1hy01wrp72rzrckjfgid66433kan043hd")))

