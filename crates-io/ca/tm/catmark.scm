(define-module (crates-io ca tm catmark) #:use-module (crates-io))

(define-public crate-catmark-0.0.0 (c (n "catmark") (v "0.0.0") (h "176d83crq12ia340xxbppv14w6p1fs05knr379b766r7z4xykm3j")))

(define-public crate-catmark-0.1.0 (c (n "catmark") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "syntect") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1sxc98zgbsgg19xvghxss7ysj69xl8xarna4s2xb5ar57k26aaci")))

(define-public crate-catmark-0.2.0 (c (n "catmark") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "syntect") (r "^1.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "04yw4k5kh7sf4q3rhcp9q8svy4a3n724vplxhgfqgimmjywipllx")))

(define-public crate-catmark-0.2.1 (c (n "catmark") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "syntect") (r "^1.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0ns8i4fiags1krhcirs7xiy0av3aipr50ss630l4lgak8dkw92pg")))

(define-public crate-catmark-0.2.2 (c (n "catmark") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1ws90dqqzlvhqi8z4w58s2w7jh8kclmff8b00hx3r0vbnm87gwxk")))

