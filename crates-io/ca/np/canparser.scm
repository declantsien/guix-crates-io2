(define-module (crates-io ca np canparser) #:use-module (crates-io))

(define-public crate-canparser-0.1.0 (c (n "canparser") (v "0.1.0") (d (list (d (n "can-dbc") (r "^5.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1csn8qg0805l7b80zkvny713i5anw3f50gl14mz8z3sn2c5q43v8")))

(define-public crate-canparser-0.1.1 (c (n "canparser") (v "0.1.1") (d (list (d (n "can-dbc") (r "^5.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0rxxvp471dgs48dq50k53yhygjzgpk7aph8lj0ibvmyamp7m2axk")))

(define-public crate-canparser-0.1.2 (c (n "canparser") (v "0.1.2") (d (list (d (n "can-dbc") (r "^5.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1p6p45mcrfdbgbllv6mwf9flnxcnf5jbvb7dqjf3lwzz8pk6lskk")))

