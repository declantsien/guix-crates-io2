(define-module (crates-io ca np canpack-macros) #:use-module (crates-io))

(define-public crate-canpack-macros-0.0.1 (c (n "canpack-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1wk88maxyrpiyp13m3whqla1zkb6mwi0nwg1yiawcs2ylnyc6x")))

(define-public crate-canpack-macros-0.1.0 (c (n "canpack-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wfvf9yqi8jaqj38yi74sgpljyc7p8g8giwrdiahxlalnm7djs41")))

(define-public crate-canpack-macros-0.1.1 (c (n "canpack-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16kngdq3k3ca4h03bjxrrwx501jdhij3d860dv3qpgv2ha6y6pr0")))

