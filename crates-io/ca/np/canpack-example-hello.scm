(define-module (crates-io ca np canpack-example-hello) #:use-module (crates-io))

(define-public crate-canpack-example-hello-0.0.1 (c (n "canpack-example-hello") (v "0.0.1") (h "05zd6d6ggvsnxi9hh0vpbhqa2wp2mrm8npva7qxnylplgv07mkmp")))

(define-public crate-canpack-example-hello-0.0.2 (c (n "canpack-example-hello") (v "0.0.2") (d (list (d (n "canpack") (r "^0.0.1") (d #t) (k 0)))) (h "1vn6pc5lvzlprpdj5935vrgbcpg76dpgfiwm9p534p0id08fpw34")))

(define-public crate-canpack-example-hello-0.1.0 (c (n "canpack-example-hello") (v "0.1.0") (d (list (d (n "canpack") (r "^0.0.1") (d #t) (k 0)))) (h "07zfrcdv2yr3ljvw3mf96b7m26vl2wl967qknpqzn8qb24j7qiyi")))

