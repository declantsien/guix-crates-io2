(define-module (crates-io ca ta catalog-of-markdown) #:use-module (crates-io))

(define-public crate-catalog-of-markdown-0.1.0 (c (n "catalog-of-markdown") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w7q902ama2y38v9isfshga709x07k7m189mjnsy66434hi3mz7h")))

(define-public crate-catalog-of-markdown-0.1.1 (c (n "catalog-of-markdown") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12pgzc2rh799b0w766y6l70m7ifhnb7x8d18jgqm8700m1gffw0v")))

(define-public crate-catalog-of-markdown-0.1.2 (c (n "catalog-of-markdown") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rz92j4r076wq0c6z1n1y9n58m9rnb99k9d407nk0nppv3c74im4")))

(define-public crate-catalog-of-markdown-0.1.3 (c (n "catalog-of-markdown") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l634w004fgmi9vps8p55667qa0vv6w5dl9gq9qvi1mk35kgifw7")))

(define-public crate-catalog-of-markdown-0.1.4 (c (n "catalog-of-markdown") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "175z3ks48wjixl8cigra94m00v1216wwqzv80nv0h5dx1qfd57w0")))

