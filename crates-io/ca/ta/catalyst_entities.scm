(define-module (crates-io ca ta catalyst_entities) #:use-module (crates-io))

(define-public crate-catalyst_entities-0.1.0 (c (n "catalyst_entities") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.5.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "region") (r "^3.0.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (f (quote ("copy"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)))) (h "1caazzjk9w9jk7z6c341jvxwi1q6bz8gb580fdjc6zq34g53899f")))

(define-public crate-catalyst_entities-0.1.1 (c (n "catalyst_entities") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1.5.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "region") (r "^3.0.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (f (quote ("copy"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)))) (h "10r9pdc1njpaninhphhdnrbd4pd5jhihq991093bfzgsaxqs4c3d")))

(define-public crate-catalyst_entities-0.1.2 (c (n "catalyst_entities") (v "0.1.2") (d (list (d (n "arc-swap") (r "^1.5.1") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "region") (r "^3.0.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (f (quote ("copy"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)))) (h "13dq0g6fba1dwsbchz0qlxyp7wpy7z9xwcj9zzcawhb2l3bdhlf0")))

