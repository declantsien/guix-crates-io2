(define-module (crates-io ca ta cataacli) #:use-module (crates-io))

(define-public crate-cataacli-0.0.2 (c (n "cataacli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cataas") (r "^0.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0hqh0jpqcz46919njx0p2nmp8250akhn3h5910rw0dhh5kbnr0my")))

