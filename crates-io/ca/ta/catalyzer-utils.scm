(define-module (crates-io ca ta catalyzer-utils) #:use-module (crates-io))

(define-public crate-catalyzer-utils-0.1.0 (c (n "catalyzer-utils") (v "0.1.0") (h "0yhbm5wpqmv1danyiwadw112m5y0mx4d1vj5r8bb0shmw1ir76z8")))

(define-public crate-catalyzer-utils-0.1.2 (c (n "catalyzer-utils") (v "0.1.2") (h "1msrcc6baq3ky12jd6r7pdcdp88mj85gisyygjq9fn9s2q6k1l7w")))

(define-public crate-catalyzer-utils-0.1.3 (c (n "catalyzer-utils") (v "0.1.3") (h "0cgm9qvf1d62xipjps67sxr3p6gwsinw45ix98agryg49r7b1wan")))

