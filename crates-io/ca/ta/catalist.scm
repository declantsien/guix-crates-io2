(define-module (crates-io ca ta catalist) #:use-module (crates-io))

(define-public crate-catalist-0.6.4 (c (n "catalist") (v "0.6.4") (h "1bss06yagpw0jfrca1gxmvjqvw5wsnnmplmkr6wigvh1s1zhph4r")))

(define-public crate-catalist-0.6.5 (c (n "catalist") (v "0.6.5") (h "1srdw7f58qp968hnsfy5l6s3cm27l6cq0a1v2hjg37k8vgafh3lh")))

(define-public crate-catalist-0.7.0 (c (n "catalist") (v "0.7.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "103amhr9is4phz23vxvf5nikwmpc0qd773yamqpfkr2rvfy8wkz2")))

(define-public crate-catalist-0.8.0 (c (n "catalist") (v "0.8.0") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "0cbwa1kc6gvaidlzfvf269b05z1n0jj7minrd6xizjr4fljhii4z")))

