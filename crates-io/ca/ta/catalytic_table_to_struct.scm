(define-module (crates-io ca ta catalytic_table_to_struct) #:use-module (crates-io))

(define-public crate-catalytic_table_to_struct-0.1.0 (c (n "catalytic_table_to_struct") (v "0.1.0") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scylla") (r "^0.3") (d #t) (k 0)))) (h "0m1lz8i2z02xwpd4g3snsnyi5zccm2fanicibwxkqmr2dbzj25qw")))

(define-public crate-catalytic_table_to_struct-0.1.1 (c (n "catalytic_table_to_struct") (v "0.1.1") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scylla") (r "^0.4.5") (d #t) (k 0)))) (h "1s2390ia1izpq0zgidq5kp44nd4w7i8p0g9yap4plnh6sh9fv6nd")))

(define-public crate-catalytic_table_to_struct-0.1.2 (c (n "catalytic_table_to_struct") (v "0.1.2") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0jlkbp4pgjl44fnc1h4c1hhac36c5vj74n6vlq8ryn4qclcjh865")))

(define-public crate-catalytic_table_to_struct-0.1.3 (c (n "catalytic_table_to_struct") (v "0.1.3") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0ylcdxcb76brd9xac3flrxrw2h92cwj3645a9m3i6mpk0v7wm6hz")))

(define-public crate-catalytic_table_to_struct-0.1.4 (c (n "catalytic_table_to_struct") (v "0.1.4") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1ngff820kpa5d5781sffnzqwcqgk1si570s9m52mcy43ni3xzcm8")))

(define-public crate-catalytic_table_to_struct-0.1.6 (c (n "catalytic_table_to_struct") (v "0.1.6") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1zqrfmrrzwzw9ghb5jd22fg56njwcsl49zpz47f81y20l0sb28vi")))

(define-public crate-catalytic_table_to_struct-0.1.7 (c (n "catalytic_table_to_struct") (v "0.1.7") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0rw0jg0a9s9sxyzmpvaan7qin21qbaiy4yb1q41ganp9vwbmhzhf")))

(define-public crate-catalytic_table_to_struct-0.1.9 (c (n "catalytic_table_to_struct") (v "0.1.9") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0hrzfzmwjzj1i3lqj0r9l3bd63v5w6j72f0dq16cbqak05zqiyq9")))

(define-public crate-catalytic_table_to_struct-0.1.10 (c (n "catalytic_table_to_struct") (v "0.1.10") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0lh9bn8849qv4gph097rgffm4cckmcnxx9q632hcdkr3niwb8x4k")))

(define-public crate-catalytic_table_to_struct-0.1.11 (c (n "catalytic_table_to_struct") (v "0.1.11") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1c4ih7fjl0mcbbdcbkq7zazp00q1nv3r82d748fj154x65r9g3hr")))

(define-public crate-catalytic_table_to_struct-0.1.12 (c (n "catalytic_table_to_struct") (v "0.1.12") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1p7azxkshimcp0930irmd8dwimf42f0kl4s2bckvnnj3ay5jybfg")))

(define-public crate-catalytic_table_to_struct-0.1.13 (c (n "catalytic_table_to_struct") (v "0.1.13") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)))) (h "024krfb6xd7zxhvpl8ndf8q5qb5hfd11q2w1wpn0zvmqqw46w7hj")))

(define-public crate-catalytic_table_to_struct-0.1.14 (c (n "catalytic_table_to_struct") (v "0.1.14") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)))) (h "0yaxgrzhbm3qydsh07nq8hwwq4salhzlnz7071qfkjyv7sw6zx9d")))

(define-public crate-catalytic_table_to_struct-0.1.15 (c (n "catalytic_table_to_struct") (v "0.1.15") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)))) (h "075vgpxp6i1pkvznbrzlnld4xrrapjy8vls21lhx1ad0xxhyn8l9")))

(define-public crate-catalytic_table_to_struct-0.1.16 (c (n "catalytic_table_to_struct") (v "0.1.16") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1w2i71677ifsjhp8zgzw3qvwj5cp09d0iyspr3z1vqaabnbjwppa")))

(define-public crate-catalytic_table_to_struct-0.1.17 (c (n "catalytic_table_to_struct") (v "0.1.17") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1s3mlh57yzd7ki9c7d60sis5n9r9dif71w24vdw3wl3hvqggzpwa")))

(define-public crate-catalytic_table_to_struct-0.1.18 (c (n "catalytic_table_to_struct") (v "0.1.18") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "13zf82flchpi69nzq3lwp4pvx5bg9xvgirjxv5ikl5jav39r8m08")))

(define-public crate-catalytic_table_to_struct-0.1.19 (c (n "catalytic_table_to_struct") (v "0.1.19") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0fwnbma49ckycbmqnmp9y1ml8j6a7r3r4badxrcysq0v2q737f1h")))

(define-public crate-catalytic_table_to_struct-0.1.20 (c (n "catalytic_table_to_struct") (v "0.1.20") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0fyi7v97qk1qnhnz75c6bxqrcmkc6l35fy299rbkhxsijrk1h3cp")))

(define-public crate-catalytic_table_to_struct-0.1.21 (c (n "catalytic_table_to_struct") (v "0.1.21") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1syvzmvl6hrk16y0rnyw5p7r337ykl81g5d8qgvyw1zsjrh1kp6f")))

(define-public crate-catalytic_table_to_struct-0.1.22 (c (n "catalytic_table_to_struct") (v "0.1.22") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "16grgqr2irhjjifwqdmgcb3plyidk1c9ccgm5h1afkigh41gvpli")))

(define-public crate-catalytic_table_to_struct-0.1.23 (c (n "catalytic_table_to_struct") (v "0.1.23") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1pvqgybsswlfs2nwmdzqmkavs67iq9vc14w5bw9zaz0iwzfqr3n1")))

(define-public crate-catalytic_table_to_struct-0.1.24 (c (n "catalytic_table_to_struct") (v "0.1.24") (d (list (d (n "catalytic") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1jniqgnksjp48c6a92gi7cj0hbd6b7rzjp81yd2104px8wh69cc0")))

