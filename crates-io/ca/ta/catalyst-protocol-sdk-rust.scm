(define-module (crates-io ca ta catalyst-protocol-sdk-rust) #:use-module (crates-io))

(define-public crate-catalyst-protocol-sdk-rust-0.1.0 (c (n "catalyst-protocol-sdk-rust") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "1s5rx9brsa9s9aqnianmrm5vip0irsn5h7xyvbj6agwk63xq05hn")))

(define-public crate-catalyst-protocol-sdk-rust-0.1.1 (c (n "catalyst-protocol-sdk-rust") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "1xpd8zmi0j38f05d4yl6r3brnj28fwgipgcxrvzgqs06sik8w4dy")))

(define-public crate-catalyst-protocol-sdk-rust-0.1.2 (c (n "catalyst-protocol-sdk-rust") (v "0.1.2") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "0w7axz0hj4kb55vfgnk79cspa6mq61diibdgnsgx4hgvnvf08lxm")))

(define-public crate-catalyst-protocol-sdk-rust-0.1.3 (c (n "catalyst-protocol-sdk-rust") (v "0.1.3") (d (list (d (n "protobuf") (r "= 2.8.1") (d #t) (k 0)))) (h "1m7xs71clz31zix4l0dwjnhpj5s0rqaxrgz0dgi0nw2rfa53wcqf")))

(define-public crate-catalyst-protocol-sdk-rust-0.1.4 (c (n "catalyst-protocol-sdk-rust") (v "0.1.4") (d (list (d (n "protobuf") (r "= 2.10.0") (d #t) (k 0)))) (h "1a8qfprbh44ylzwvmvdw70av5qj9xawy4xw964yy90fkziazzli9")))

(define-public crate-catalyst-protocol-sdk-rust-0.1.5 (c (n "catalyst-protocol-sdk-rust") (v "0.1.5") (d (list (d (n "protobuf") (r "= 2.10.0") (d #t) (k 0)))) (h "1kzvrkh4gjj2y6llnsvrlx4fnsc7psl936g9bcs9ji47pvw18aaj")))

