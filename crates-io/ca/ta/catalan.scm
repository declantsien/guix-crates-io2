(define-module (crates-io ca ta catalan) #:use-module (crates-io))

(define-public crate-catalan-0.1.0 (c (n "catalan") (v "0.1.0") (h "1pf6vg361xfjkz3da7z38mz96811i8zc5f4msrizygfd9yilgpmh")))

(define-public crate-catalan-0.1.1 (c (n "catalan") (v "0.1.1") (h "0mijgm55azgn19fj7j83jbdwcmrg9kh1akpmis8lljfa9gz2p22g")))

(define-public crate-catalan-0.2.0 (c (n "catalan") (v "0.2.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1qlnpfgkg2qssfndy1j15m6awmrrnjn8hqrbikp3d9v0wb1a2jlm")))

(define-public crate-catalan-0.3.0 (c (n "catalan") (v "0.3.0") (h "1v36bgq479p8qx920lbpaaw48i115a255r54fwp03fpfb1y92maa")))

(define-public crate-catalan-0.3.1 (c (n "catalan") (v "0.3.1") (h "1g5l6kgmwv16iwjgbf3i2cwvy2df0rn08lglkakc1l2mdnd4y9pj")))

(define-public crate-catalan-0.3.2 (c (n "catalan") (v "0.3.2") (h "1d7jf600ml5l4kxqbsgdp4sqca2xivh8vir9lfas8swwggsb9mc2")))

(define-public crate-catalan-0.3.3 (c (n "catalan") (v "0.3.3") (h "0lih5x3gmhimx0qxgcyxwfrx49nx7wgzs9190ajiy7z6kvrvi740")))

