(define-module (crates-io ca ta catapult) #:use-module (crates-io))

(define-public crate-catapult-0.1.1 (c (n "catapult") (v "0.1.1") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "nom") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "00wa16l5gziz86wi5wk95fdv5g0019hxj8rx67bps8cgwchhx6kb")))

(define-public crate-catapult-0.1.2 (c (n "catapult") (v "0.1.2") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "nom") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "18469xxjggc2yrxi4v08h4gg8j5w6hvy517m8vwd7dfwg2g7hnw6")))

