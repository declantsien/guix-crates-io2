(define-module (crates-io ca ta catalysis) #:use-module (crates-io))

(define-public crate-catalysis-0.1.0 (c (n "catalysis") (v "0.1.0") (h "1nv97bhkbs81gxa2v2h3lahaxbksq76hyi9a00gkmm8374p7mvfm")))

(define-public crate-catalysis-0.2.0 (c (n "catalysis") (v "0.2.0") (h "0llfldqfzsaghcv721wnkccwzdkdpiacfs5is2mldfjkn9i42wnx")))

