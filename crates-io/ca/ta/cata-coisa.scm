(define-module (crates-io ca ta cata-coisa) #:use-module (crates-io))

(define-public crate-cata-coisa-0.1.0 (c (n "cata-coisa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 0)))) (h "1asy1l0iy0j3hx00nggwm7x0lrkxdbqg4dnbfx6fl07z6bdkazsk")))

