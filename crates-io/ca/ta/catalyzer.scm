(define-module (crates-io ca ta catalyzer) #:use-module (crates-io))

(define-public crate-catalyzer-0.1.0 (c (n "catalyzer") (v "0.1.0") (d (list (d (n "base") (r "^0.1.0") (k 0) (p "catalyzer-core")) (d (n "macros") (r "^0.1.0") (d #t) (k 0) (p "catalyzer-macros")))) (h "0qrwb5xgf1mcb0xf60r9kr4y6b7q2l6mj8f2qgdpadk1wmykp96v") (f (quote (("default" "builtin-logger") ("builtin-logger" "base/builtin-logger"))))))

(define-public crate-catalyzer-0.1.1 (c (n "catalyzer") (v "0.1.1") (d (list (d (n "base") (r "^0.1.0") (k 0) (p "catalyzer-core")) (d (n "macros") (r "^0.1.0") (d #t) (k 0) (p "catalyzer-macros")))) (h "12k81iyjs19vdsc7xhmg6k12ba4mq7hs4fs9dmdrjcj6yqch1s68") (f (quote (("default" "builtin-logger") ("builtin-logger" "base/builtin-logger"))))))

(define-public crate-catalyzer-0.1.2 (c (n "catalyzer") (v "0.1.2") (d (list (d (n "base") (r "^0.1.0") (k 0) (p "catalyzer-core")) (d (n "macros") (r "^0.1.0") (d #t) (k 0) (p "catalyzer-macros")))) (h "17b88f35lc6bfxjppqqhm26y7x0fmhln4p058al25q18x2av7fp8") (f (quote (("default" "builtin-logger") ("builtin-logger" "base/builtin-logger"))))))

(define-public crate-catalyzer-0.1.3 (c (n "catalyzer") (v "0.1.3") (d (list (d (n "base") (r "^0.1.3") (k 0) (p "catalyzer-core")) (d (n "macros") (r "^0.1.3") (d #t) (k 0) (p "catalyzer-macros")))) (h "1axw53jg5lmsak23jl15zgybxlyz8dyxrqsxxgjqyap4pvq1xvib") (f (quote (("default" "builtin-logger") ("builtin-logger" "base/builtin-logger"))))))

