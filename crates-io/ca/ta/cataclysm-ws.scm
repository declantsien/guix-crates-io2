(define-module (crates-io ca ta cataclysm-ws) #:use-module (crates-io))

(define-public crate-cataclysm-ws-0.1.0 (c (n "cataclysm-ws") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19781nwd1jhvvdwq0vk5ir3f01w8dj4963h5fxflcjbw5g96vz3p")))

(define-public crate-cataclysm-ws-0.1.1 (c (n "cataclysm-ws") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zq5yip3p6xrrwr5amsq3sybakgbdlln5qf87xy0ivi5q350grd5")))

(define-public crate-cataclysm-ws-0.1.2 (c (n "cataclysm-ws") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2dwxkjz8anv4pc08hynhlr4qm2w1lqw68nd5ckxmk21qnynmmz")))

(define-public crate-cataclysm-ws-0.2.0-beta.1 (c (n "cataclysm-ws") (v "0.2.0-beta.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13hnfx9vh482pik0dnnnh6am9gpzh2h1q82qgld55h6h2dzjgbkd")))

(define-public crate-cataclysm-ws-0.2.0-beta.2 (c (n "cataclysm-ws") (v "0.2.0-beta.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0lm928v9dyiznxyyzfg42vh1inprw438jwnkn5ng73kiqjfz9q")))

(define-public crate-cataclysm-ws-0.2.0-beta.3 (c (n "cataclysm-ws") (v "0.2.0-beta.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hqffcph63a654m6aiyq1b8jirlb2im2sn52811r2015zdyd17pc")))

(define-public crate-cataclysm-ws-0.2.0-beta.4 (c (n "cataclysm-ws") (v "0.2.0-beta.4") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h7was8pmgc675p7ks0zi4mz6vi6l0ki6z43n2jsk3r1hylgklpk")))

(define-public crate-cataclysm-ws-0.2.0-beta.5 (c (n "cataclysm-ws") (v "0.2.0-beta.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c5i79nf7pa4mscmgf6aq2qkx0lwb6s3pd7ab4509gl1yi4dn5yi")))

(define-public crate-cataclysm-ws-0.2.0-beta.6 (c (n "cataclysm-ws") (v "0.2.0-beta.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fcgpnh6x06i9i2x9q0hgzdzz7q4wapiyd9jlrksvygw4rvki2ky")))

(define-public crate-cataclysm-ws-0.3.0-beta.1 (c (n "cataclysm-ws") (v "0.3.0-beta.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1prb0k7any6rk4qq330ss4rkah19i1aad79l9zrl6x3qhyjqqapp")))

(define-public crate-cataclysm-ws-0.3.0-beta.2 (c (n "cataclysm-ws") (v "0.3.0-beta.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dk3w0k38zb5mhizzc3xr8mn59cx5kbggm3cnlb6zwj4w4lj0ln8")))

