(define-module (crates-io ca se cases) #:use-module (crates-io))

(define-public crate-cases-0.0.1 (c (n "cases") (v "0.0.1") (h "065bi39h4xy4d4h64wxgha289nlb4wglyhkg2pvzbsga26rj961a")))

(define-public crate-cases-0.1.0 (c (n "cases") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)))) (h "1crl74szg9p56mvhnx4fq4ih7wkwzrvq76w5wn1j9pf35w2m1ac5")))

(define-public crate-cases-0.2.0 (c (n "cases") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "095391lyj5nyvnh6hvri26criv5zhdq0vxz9nh8rz64vsywp5sl4")))

(define-public crate-cases-0.2.1 (c (n "cases") (v "0.2.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1093l8b3xwwm4b4q3wvaqwgn4p4l2hjpvpkrdky01sr8cc5a9f83")))

(define-public crate-cases-0.2.2 (c (n "cases") (v "0.2.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0jfjc2pl5p2nxravymq5hggrgwkdpq91zn6wcrnf82bki6byvz1h")))

(define-public crate-cases-0.2.3 (c (n "cases") (v "0.2.3") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1ycr6zhxd2blqpdhfxg3lc7bsvx8rwgkvlja9xq0j5hipbsiac6z")))

(define-public crate-cases-0.2.4 (c (n "cases") (v "0.2.4") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "006z0r8s6lz4lp3ryz7vfc1k1yiw2pq43j8g2mmwjzdc261flcng")))

