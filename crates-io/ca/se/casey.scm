(define-module (crates-io ca se casey) #:use-module (crates-io))

(define-public crate-casey-0.1.0 (c (n "casey") (v "0.1.0") (h "0cmm401p2x9issa4jx1z3lb4wj8pgr3kfv941a40pkdivsjdm9wj")))

(define-public crate-casey-0.1.1 (c (n "casey") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)))) (h "0z697gq0als7sq21pd2qj88yk3k365nnkqxgmnqlxmzm9krawzls")))

(define-public crate-casey-0.2.0 (c (n "casey") (v "0.2.0") (h "1psvwqb2k2cq2j8nbkcsxcr3ccldf03vx93s4l80p367d87s8cfj")))

(define-public crate-casey-0.3.0 (c (n "casey") (v "0.3.0") (h "12yygps5b4i547q6mppi97kx5sd9n6pvmcmrr40pgrakmwcfxn90")))

(define-public crate-casey-0.3.1 (c (n "casey") (v "0.3.1") (h "02rlb20kc1qihayk8jdrjy273f5zfh1kn1dy4c1dsjgkqwpgq5ss")))

(define-public crate-casey-0.3.2 (c (n "casey") (v "0.3.2") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04xrlh00gl1r886nlr2mnh00pl9sv9xpinfhv47w8c9r6kcrxm1c")))

(define-public crate-casey-0.3.3 (c (n "casey") (v "0.3.3") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "057a7x7g3q7ycmq3zcrwzs6iqn5by5ncx0jmf5kz576s1l9qbgps")))

(define-public crate-casey-0.4.0 (c (n "casey") (v "0.4.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sv5ll6aavwcigmr53b22dg16adlz4pa2pb73367sna974k8cib1")))

