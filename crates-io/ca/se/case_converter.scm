(define-module (crates-io ca se case_converter) #:use-module (crates-io))

(define-public crate-case_converter-0.1.0 (c (n "case_converter") (v "0.1.0") (h "14j6g5ag48qglbnnzsc11cmgk77fkx8ri9dlrg2bwl7prkw35070")))

(define-public crate-case_converter-0.1.1 (c (n "case_converter") (v "0.1.1") (h "1j9gi1x6c6v8rdk967f3mpzfai737f3zqykfagb4i6s7ws6p3vlk")))

