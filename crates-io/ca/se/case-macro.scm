(define-module (crates-io ca se case-macro) #:use-module (crates-io))

(define-public crate-case-macro-0.1.0 (c (n "case-macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "11ya8avlnsf4m9kh2840xywzxq5zxxsxzpp0bi2f2dirlahw5yyd")))

