(define-module (crates-io ca se caser) #:use-module (crates-io))

(define-public crate-caser-1.0.0 (c (n "caser") (v "1.0.0") (h "1bi0b8rxqlizkr1bwmhgnkjj3l7k5a6ap8myfxx0sf9h3wi7pqv9")))

(define-public crate-caser-1.1.0 (c (n "caser") (v "1.1.0") (h "022d9jdhq5i3v8b58y1g09li9vpi2packz46dj9b0hbd10g0a52v") (y #t)))

(define-public crate-caser-1.1.1 (c (n "caser") (v "1.1.1") (h "05bcd7blb77xly672n85z0rqnv807dg2gzli4fjh6f4il8hsyc8g")))

