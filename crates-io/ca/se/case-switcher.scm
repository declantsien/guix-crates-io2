(define-module (crates-io ca se case-switcher) #:use-module (crates-io))

(define-public crate-case-switcher-1.0.0 (c (n "case-switcher") (v "1.0.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0dljp53mv28g38mn11wih4x7vpngl681wp6q0x2d0na4q5dwbbdr")))

(define-public crate-case-switcher-1.0.1 (c (n "case-switcher") (v "1.0.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "16nk37zfr9dz4kbdcjqmcf4z4ld3n54ss1nsa7j1d9xp81w89fp3")))

(define-public crate-case-switcher-1.0.2 (c (n "case-switcher") (v "1.0.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0wzy1p4vkj5q77xv4g68sp5yfnwri79j19s04fja4vs065llqp9i")))

