(define-module (crates-io ca se case01-rs) #:use-module (crates-io))

(define-public crate-case01-rs-0.1.0 (c (n "case01-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1n8qpk2w8dhc7qysg4d96crddrc082kgy1bbfk5p2axdz5kzslqx")))

(define-public crate-case01-rs-0.2.0 (c (n "case01-rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1gp2lb50n9jj4x65m9zcpkp62fqywlqmp8d3xy1vxjhgwwf6jcim")))

