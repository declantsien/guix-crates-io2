(define-module (crates-io ca se caseless) #:use-module (crates-io))

(define-public crate-caseless-0.0.1 (c (n "caseless") (v "0.0.1") (h "154snivwqmiza7fbqcxj19v73hi2lqgmcjnhkklbkhh51r30s1an")))

(define-public crate-caseless-0.1.0 (c (n "caseless") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "17a1lnjd7002v90w3aafll0amgbq51qadpvqh7r5slr3lm3kb8jd")))

(define-public crate-caseless-0.1.1 (c (n "caseless") (v "0.1.1") (d (list (d (n "regex") (r "^0.1") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1yvdvmzw86qfsyxfxqn3si3v880hsz6154msrfspb4hcmj33z2dn")))

(define-public crate-caseless-0.1.2 (c (n "caseless") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "12jg65q349h4cbaj3jbq72770fzh59mfcmdpihl2qx523fiyjhvr")))

(define-public crate-caseless-0.1.3 (c (n "caseless") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1rrgnijdamajbc0acj2r5vv7qv41nnl4hwg9vsxdlp7prxsv0l49")))

(define-public crate-caseless-0.2.0 (c (n "caseless") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "16pk1jc6cvb1q42nqid8wld38ij894bbnvk69bwv97ad0cw1c9p3")))

(define-public crate-caseless-0.2.1 (c (n "caseless") (v "0.2.1") (d (list (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0zrx8rrks6mq08m37y3ng8kq272diphnvlrircif2yvl30rsp3c0")))

