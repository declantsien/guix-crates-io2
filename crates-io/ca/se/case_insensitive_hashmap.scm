(define-module (crates-io ca se case_insensitive_hashmap) #:use-module (crates-io))

(define-public crate-case_insensitive_hashmap-0.9.0 (c (n "case_insensitive_hashmap") (v "0.9.0") (d (list (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0c9f4v5nbxrcbi6a3rbxwibg7f0slrccvq25fkn86xmzxmqqf2bg")))

(define-public crate-case_insensitive_hashmap-0.9.1 (c (n "case_insensitive_hashmap") (v "0.9.1") (d (list (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0zflqrsf30c8r46399d7h7kd33irn2i2p4pm8ppn5223kbb3ggd7")))

(define-public crate-case_insensitive_hashmap-1.0.0 (c (n "case_insensitive_hashmap") (v "1.0.0") (d (list (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "14jms4i46qj10cpxjwxi5wscbnjjb93b1gz7vvg7hbzh7g2kp2nc")))

