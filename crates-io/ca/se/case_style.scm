(define-module (crates-io ca se case_style) #:use-module (crates-io))

(define-public crate-case_style-0.1.0 (c (n "case_style") (v "0.1.0") (h "1b4s92sv5ldk2c4bib98cam3d71b7s72s1cxx2x5gc0k2mb7ba7s")))

(define-public crate-case_style-0.2.0 (c (n "case_style") (v "0.2.0") (h "04b9s4spf2h41djrzarxc9b2gfz5i2vy2xg982kmw0zqjlf6vqkv")))

(define-public crate-case_style-0.2.1 (c (n "case_style") (v "0.2.1") (h "15lqyl018n81skx6ra6wiv8czsz9z4y0b5nb12pkd4libbq03rhb")))

