(define-module (crates-io ca se case_insensitive_string) #:use-module (crates-io))

(define-public crate-case_insensitive_string-0.1.0 (c (n "case_insensitive_string") (v "0.1.0") (h "0gsa5jk59aykdsj40xiic6yalsgikba96s7csndsk4nizh5q19mc")))

(define-public crate-case_insensitive_string-0.1.1 (c (n "case_insensitive_string") (v "0.1.1") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1fmhfyl684bnnzdkpmgxnamzg5f8vs421nbkiw1l6b7ayxgxgvz3") (f (quote (("compact" "compact_str"))))))

(define-public crate-case_insensitive_string-0.1.2 (c (n "case_insensitive_string") (v "0.1.2") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gfmqph255ngpi9vh9350142dfa9zm652qvc37hmhs0wsrj1wa0h") (f (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1.4 (c (n "case_insensitive_string") (v "0.1.4") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vlx2gs9lq8pqczmy1ps4im1nb4l7nnwa6in8p8fpcdwdwm94qar") (f (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1.5 (c (n "case_insensitive_string") (v "0.1.5") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11xb6ng3cn5mfvwd37ilxl58m6mq07fw9dra6z348448ga34x5gc") (f (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1.6 (c (n "case_insensitive_string") (v "0.1.6") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cd025l2ldqh8qr23ghdr1r7yw6nfw7br3s0hndz4l7c05pxjljj") (f (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1.7 (c (n "case_insensitive_string") (v "0.1.7") (d (list (d (n "compact_str") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ib3xg3kghysd0p22mj8a5filkz1zlc4f6919rl3d2xs3ry9fz40") (f (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.2.0 (c (n "case_insensitive_string") (v "0.2.0") (d (list (d (n "compact_str") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13ppdxlj5z6lp9j4di18982fqwr3ickdkdiz2qpqjsx3rqlpj63i") (s 2) (e (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

(define-public crate-case_insensitive_string-0.2.2 (c (n "case_insensitive_string") (v "0.2.2") (d (list (d (n "compact_str") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bk9292vq7525jhswfipdppv3lyzj0998k1mmgdp7jfwb94msfr9") (s 2) (e (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

(define-public crate-case_insensitive_string-0.2.3 (c (n "case_insensitive_string") (v "0.2.3") (d (list (d (n "compact_str") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k8vh8vg3rga6fl478919mrhf103sl09bq49vjmiah9rggi9n8pw") (s 2) (e (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

