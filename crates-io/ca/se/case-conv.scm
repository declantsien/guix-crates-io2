(define-module (crates-io ca se case-conv) #:use-module (crates-io))

(define-public crate-case-conv-0.1.0 (c (n "case-conv") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0zrj0zx5mnq415x16n5zdw1x7cpi2qy3jp2qjh9sfbkgffsxvjhk")))

(define-public crate-case-conv-0.1.1 (c (n "case-conv") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0l0ii1lyakzgx43fj66y3zrvass9q695885qbany9cwhjrbv2blm")))

(define-public crate-case-conv-0.1.2 (c (n "case-conv") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1n66n4qfa6mfl5jkhjl17r10rfkx3zry5gckszyyfh5r0hlrbqww")))

(define-public crate-case-conv-0.1.3 (c (n "case-conv") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0sn40cirybqf9iw93x0a8p4jbvx302b1mljlsr8vpnz7frcfcbfd")))

(define-public crate-case-conv-0.1.4 (c (n "case-conv") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wf6l20l0075rb76dij610q0204sjl97xaxasiw98fa8wskpndn1")))

(define-public crate-case-conv-0.1.5 (c (n "case-conv") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "19z7zfx4f4bayanwszzjp1yc2ldv8rdibzb5f4dhpmqkikwzglm3")))

(define-public crate-case-conv-0.1.6 (c (n "case-conv") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "03sx0z9sxhlb37mdjw13j2bmlxny5yyrasql63zls747iwjpg780")))

