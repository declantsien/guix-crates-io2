(define-module (crates-io ca ba cabac) #:use-module (crates-io))

(define-public crate-cabac-0.1.0 (c (n "cabac") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "04awfbyvdf5p8nl0qnhsf35ghifrjlc1gn60m95wbd654hqri2r8")))

(define-public crate-cabac-0.2.0 (c (n "cabac") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "157lvl471h3ms6cjz35l1dwaz5c3cwc7hq6yyb8fli1mwxkl1805")))

(define-public crate-cabac-0.3.0 (c (n "cabac") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1bhq4h9xwl13wdhyag33rly6015w4hbp0bixx76y9gnpkxxj1gjp")))

(define-public crate-cabac-0.4.0 (c (n "cabac") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "06xrqsankmx9vq7aw88akbhqm2fcil72h53md4w8486i370w3zbg")))

(define-public crate-cabac-0.5.0 (c (n "cabac") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "00f915h6w0rzym8w8fpnw0ffmbknnb7syjbn71np7jv6hj12m6ly")))

(define-public crate-cabac-0.6.0 (c (n "cabac") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1d6irbf4vqllvmp61q7qhdxbikasfyrhg4kg0fd43qxx2iyv72nq")))

(define-public crate-cabac-0.7.0 (c (n "cabac") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1haimcz45byb8yxbysm9zs3877cyh21pn8hpnf7nlav0ad48zv95")))

(define-public crate-cabac-0.8.0 (c (n "cabac") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "05si7vdp1akn3d8491nnmp02zg37vbz9gy5xcmb3k20fim2jf4f7")))

