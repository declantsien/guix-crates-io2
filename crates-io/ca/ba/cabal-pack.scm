(define-module (crates-io ca ba cabal-pack) #:use-module (crates-io))

(define-public crate-cabal-pack-0.3.0 (c (n "cabal-pack") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bax6j809zjwq0xfcm4h077pix3qyd32gax4cii4y93qf3rs3dqk")))

(define-public crate-cabal-pack-0.3.1 (c (n "cabal-pack") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0b47rnkq6hisb2w2bpcrjy1f1b2a6w2q9pjc9nybgm3kjlfrlhjb") (y #t)))

(define-public crate-cabal-pack-0.3.2 (c (n "cabal-pack") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1pm2n854wwnq76pqbvvpj5186d00r5qjjiig0dc10d3jqgbxms8b")))

(define-public crate-cabal-pack-0.3.3 (c (n "cabal-pack") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1d7fg0cjdi0mrx7zk1ryhb95s3hfv1sdny3l9lrb29105ikssvvh")))

(define-public crate-cabal-pack-0.3.4 (c (n "cabal-pack") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "170w5n0v2bry95zg99crcwm8zd3g9zbzgxaaq1dl5hr9xdjwmdsd")))

(define-public crate-cabal-pack-0.4.0 (c (n "cabal-pack") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1962xjc2g829klrpyr8i691sl506xmy71my21l5f11aysjj0hjgb") (y #t)))

(define-public crate-cabal-pack-0.4.1 (c (n "cabal-pack") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0pdzwl3lzwcax6wdizjga965jrk0lm34xc69v6sisr1cmfk5wn82")))

(define-public crate-cabal-pack-0.5.0 (c (n "cabal-pack") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0mw4jhyq3q8fp36ia8cbryf90ygnz3ji33rfif22zj53msn3zv2r")))

(define-public crate-cabal-pack-0.5.1 (c (n "cabal-pack") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1pljzqn5cwan21ascz1jjaw25ak4fhf408nnlkg3v5rhhk08z0zl")))

(define-public crate-cabal-pack-0.6.0 (c (n "cabal-pack") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0f2wvwg3nx6f1l6vl0cbc1sqanpvys1h655pb2w6hjh2q41hp3av")))

(define-public crate-cabal-pack-0.6.1 (c (n "cabal-pack") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kx8g18b95h571n9g8ndg61mv02fz2wpf99sk06swbx5xrhcbzsx") (r "1.62.1")))

(define-public crate-cabal-pack-0.6.2 (c (n "cabal-pack") (v "0.6.2") (h "004ylimafn3zjxj0lclra6vb9d1fnlkwk6skfzrizlm2xwmdrx07") (r "1.62.1")))

