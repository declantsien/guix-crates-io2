(define-module (crates-io ca fs cafs) #:use-module (crates-io))

(define-public crate-cafs-0.0.1 (c (n "cafs") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.1") (d #t) (k 0)) (d (n "capnp") (r "^0.6.2") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "docopt") (r "^0.6.78") (d #t) (k 2)) (d (n "leveldb") (r "^0.8.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)) (d (n "rust-sqlite") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)) (d (n "toml-config") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^0.2.35") (d #t) (k 2)))) (h "0zxi6mfaj7ab1ay8q38gl6v0f9f9y8mvayzy78s8byqq3n8ya4jx")))

