(define-module (crates-io ca fe cafe_core) #:use-module (crates-io))

(define-public crate-cafe_core-0.1.0 (c (n "cafe_core") (v "0.1.0") (d (list (d (n "coffee_lang") (r "^0.0.0") (d #t) (k 0)) (d (n "latte_fs") (r "^0.0.0") (d #t) (k 0)) (d (n "mocha_audio") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (k 0)) (d (n "stb_image") (r "^0.2.5") (d #t) (k 0)) (d (n "tea_render") (r "^0.1.0") (d #t) (k 0)))) (h "0d23kgd2m25y7xsazz085mibqnb9cnqpx2s5x06lmjfy23mj1l4v")))

(define-public crate-cafe_core-0.1.1 (c (n "cafe_core") (v "0.1.1") (d (list (d (n "coffee_lang") (r "^0.0.0") (d #t) (k 0)) (d (n "latte_fs") (r "^0.0.0") (d #t) (k 0)) (d (n "mocha_audio") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (k 0)) (d (n "stb_image") (r "^0.2.5") (d #t) (k 0)) (d (n "tea_render") (r "^0.1.0") (d #t) (k 0)))) (h "0cy92jfw74kpmgpagfw3ya1fnics1am8m4sla47agymv4n04x121")))

(define-public crate-cafe_core-0.1.2 (c (n "cafe_core") (v "0.1.2") (d (list (d (n "coffee_lang") (r "^0.0.0") (d #t) (k 0)) (d (n "latte_fs") (r "^0.0.0") (d #t) (k 0)) (d (n "mocha_audio") (r "^0.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (k 0)) (d (n "stb_image") (r "^0.2.5") (d #t) (k 0)) (d (n "tea_render") (r "^0.1.0") (d #t) (k 0)))) (h "0pick2i96szvx15js9yz3xnq52mhg44cv9lzjj349p3h0hm1jic9")))

