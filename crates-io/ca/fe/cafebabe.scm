(define-module (crates-io ca fe cafebabe) #:use-module (crates-io))

(define-public crate-cafebabe-0.1.0 (c (n "cafebabe") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0b4axrw9xp50q45ik3lsm218a3dfwhsf5c9sb3apdv2z862f3irb")))

(define-public crate-cafebabe-0.2.0 (c (n "cafebabe") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "18l2ybzk7h5v6byh82wgfka467fpikwb2k9zw2llg89a2pd0ysw4")))

(define-public crate-cafebabe-0.3.0 (c (n "cafebabe") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "118qvm7agxjz1as2nsb3yfnqsm357x1vdib8dbs9a7pyd7ka0bb5")))

(define-public crate-cafebabe-0.3.1 (c (n "cafebabe") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0ah6lqh0fbpjyyl5i0hdhw731lwm6bpl5bn89xgvddvqbkph47nk")))

(define-public crate-cafebabe-0.4.0 (c (n "cafebabe") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0v4zspf6r7xz48ysa2bsksji669mi7jnw93wmk071csc57c8hvbx")))

(define-public crate-cafebabe-0.4.1 (c (n "cafebabe") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0q97h1w2p3cwsin8pcgipymwgklqfm7ysrwlql4j4wgzndly9p6f")))

(define-public crate-cafebabe-0.4.2 (c (n "cafebabe") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "133r88dwxpjzaf6kmfclkq3mbgnkhf4v0kbjmg0xg00i6vif8d27")))

(define-public crate-cafebabe-0.5.0 (c (n "cafebabe") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0aiw4fqia5vz9c4b76n01xmhixs5wq9ypshir3srz0zx5qn8mbip")))

(define-public crate-cafebabe-0.6.0 (c (n "cafebabe") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "12c526mm43v42f4icabjpd78kks0h359mxp2gxy7qsbjnzm7wjqk")))

(define-public crate-cafebabe-0.6.1 (c (n "cafebabe") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)))) (h "0860qympjgkyv6jwz7xwlicfrc4j4f56943s4nw8w4nf3njfxd9w")))

