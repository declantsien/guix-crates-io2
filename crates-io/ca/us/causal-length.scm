(define-module (crates-io ca us causal-length) #:use-module (crates-io))

(define-public crate-causal-length-0.2.0 (c (n "causal-length") (v "0.2.0") (d (list (d (n "num-integer") (r "^0") (d #t) (k 0)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1cl6vz9ajnh2xc6rhq463qg538dx688c3q5qwf7z2h7dwbcyxra6") (f (quote (("serialization" "serde" "serde_derive") ("default" "serialization"))))))

