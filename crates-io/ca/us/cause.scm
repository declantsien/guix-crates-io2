(define-module (crates-io ca us cause) #:use-module (crates-io))

(define-public crate-cause-0.1.0 (c (n "cause") (v "0.1.0") (h "01pcl46c7q0b4qk73gssykc6jbqr88ixibh7jswpy34fc4p5zi7c")))

(define-public crate-cause-0.1.1 (c (n "cause") (v "0.1.1") (h "0wjpj9m27dg46nyjc2sj93jgz5q6hzlpjdwn2jxhvlvijkw6ndk9")))

(define-public crate-cause-0.1.2 (c (n "cause") (v "0.1.2") (h "0kyarb1iygf2xb3vcqys2lphpwprmx0yj3bxirw37034mifa0isa")))

