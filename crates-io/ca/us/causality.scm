(define-module (crates-io ca us causality) #:use-module (crates-io))

(define-public crate-causality-0.1.0 (c (n "causality") (v "0.1.0") (h "01dyd74gsdjq9fz8k33697vb83r20bc23xjx0dqyk9lchzhdizv8")))

(define-public crate-causality-0.2.0 (c (n "causality") (v "0.2.0") (d (list (d (n "simple-error") (r "^0.2.3") (d #t) (k 2)))) (h "0nindijcb135b5mpmzk119nkmx1k2c9zyawjx8z6i267ba3klzhf")))

