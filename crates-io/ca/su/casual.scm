(define-module (crates-io ca su casual) #:use-module (crates-io))

(define-public crate-casual-0.1.0 (c (n "casual") (v "0.1.0") (h "1g09vih417krbn8vqz4b7abx813i4fvj83p62xz0s3gnn8wj0w7b")))

(define-public crate-casual-0.1.1 (c (n "casual") (v "0.1.1") (h "14gvbw1nbxxjjp9js27kliib4857dvg2mf19b055nwjqi760wawv")))

(define-public crate-casual-0.1.2 (c (n "casual") (v "0.1.2") (h "047i78v3whw2fmqdvpn5lprwjkk2ck307sbv3y5zmdpnczq75061")))

(define-public crate-casual-0.2.0 (c (n "casual") (v "0.2.0") (h "081l7kxfxpdss7ln0x3c5b71hzv4h5l6c8z33zbvny0y2w6w1fh2")))

