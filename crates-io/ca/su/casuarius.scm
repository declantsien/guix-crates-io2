(define-module (crates-io ca su casuarius) #:use-module (crates-io))

(define-public crate-casuarius-0.1.0 (c (n "casuarius") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0liqffz0fpimwbh40k5ihv1wgblc5ksfy0x39hgqf8p3mi64j0qr")))

(define-public crate-casuarius-0.1.1 (c (n "casuarius") (v "0.1.1") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1nfh53l0dgnsn5q0y8wi5ni192fcpilil29k44mjxkzcdbz3dxqd")))

