(define-module (crates-io ca rd cardinal-configs) #:use-module (crates-io))

(define-public crate-cardinal-configs-1.0.0 (c (n "cardinal-configs") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "cardinal-stake-pool") (r "^1.17.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.10.41") (d #t) (k 0)))) (h "1wj9z2apcykqca1m39pwhmfgd17xsr94xp131xwdp9kaydipks77") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

