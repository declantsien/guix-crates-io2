(define-module (crates-io ca rd card-backend) #:use-module (crates-io))

(define-public crate-card-backend-0.1.0 (c (n "card-backend") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q95f7b7j9g4j9hwm46szqincmnbjzkbb9bvqqfcp4bpxs6613k0")))

(define-public crate-card-backend-0.2.0 (c (n "card-backend") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ra2zfcs0n4msw4j0hmv8f3bpfz49x5c704i93f6a844k2if6gmx")))

