(define-module (crates-io ca rd card_deck) #:use-module (crates-io))

(define-public crate-card_deck-0.1.0 (c (n "card_deck") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0qpill0y36a026hpvbm2jhrxirwayszzhws9qbsxagssj5v6ybk2")))

(define-public crate-card_deck-0.1.1 (c (n "card_deck") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1yqxyx19l085gcxa4x8asxg40gx9y4xmjivxw4qqybj6cbf0fx12")))

(define-public crate-card_deck-0.1.2 (c (n "card_deck") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0sdyynv82vlrsfs3v9cq6a797dj1y8dcaqdpzgn4v3i664vi9zb9")))

(define-public crate-card_deck-0.1.3 (c (n "card_deck") (v "0.1.3") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1jfdn4fwfp1krm5n3w9bh7mamrs3zsxqizxqm7nsi8rlw948sfbi") (y #t)))

(define-public crate-card_deck-0.1.4 (c (n "card_deck") (v "0.1.4") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0c167xblvajdizibhxmsnmbj6p3drxf9s9amjqbv4bp1hvi0dsax")))

(define-public crate-card_deck-0.1.5 (c (n "card_deck") (v "0.1.5") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1g5117h1m7lkn6rq7aan0ss5r451dhpbwss3wbqw7bjqz7cqjfvm")))

(define-public crate-card_deck-0.1.6 (c (n "card_deck") (v "0.1.6") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0slzbv6l45i6qy974q92jv2czq2qw0p104vlq11ziw8jya5lj6yh")))

(define-public crate-card_deck-0.1.7 (c (n "card_deck") (v "0.1.7") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1f091r8h5mzxgxhgyi79kcjp7b62wkdn9qx1yzyi16614k1nciy6")))

(define-public crate-card_deck-0.1.8 (c (n "card_deck") (v "0.1.8") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "19k90xfr5m6qx3jlagqck1xirpinw67bq3rxrd5s6m1gpgg9h2gd")))

(define-public crate-card_deck-0.1.9 (c (n "card_deck") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0xjkcpw8v9hzj6zpl523q0m17nzb4f789sq1hjfclmgw3bbxxhip")))

