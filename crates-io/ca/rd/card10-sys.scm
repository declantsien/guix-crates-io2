(define-module (crates-io ca rd card10-sys) #:use-module (crates-io))

(define-public crate-card10-sys-1.9.0 (c (n "card10-sys") (v "1.9.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "panic-abort") (r "^0.3") (d #t) (k 0)) (d (n "r0") (r "^0.2") (d #t) (k 0)))) (h "0fykdwfrdb0sxi5pvpk26m46zs3klfcj4p5ckh2wm45f60p78m0d")))

(define-public crate-card10-sys-1.10.0 (c (n "card10-sys") (v "1.10.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "panic-abort") (r "^0.3") (d #t) (k 0)) (d (n "r0") (r "^0.2") (d #t) (k 0)))) (h "17q5hkil4kx7wxbn5qjcr1ag4473rd5nfvv7xphg07dxbh6jzsmg")))

