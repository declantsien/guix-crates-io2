(define-module (crates-io ca rd card10-alloc) #:use-module (crates-io))

(define-public crate-card10-alloc-0.1.0 (c (n "card10-alloc") (v "0.1.0") (d (list (d (n "alloc-cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "card10-sys") (r "1.9.*") (d #t) (k 0)))) (h "09xwg4f8iqz1mbq676dpabj4155i3ywr21c0d5ldadkfym6pjfxq")))

(define-public crate-card10-alloc-0.1.1 (c (n "card10-alloc") (v "0.1.1") (d (list (d (n "alloc-cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "card10-sys") (r "^1.10") (d #t) (k 0)))) (h "0yv35kf5a83gdgrv5syhwrsq5j3fgy01jh3x9gmgki5n3khmk1rz")))

