(define-module (crates-io ca rd cardito) #:use-module (crates-io))

(define-public crate-cardito-0.1.0 (c (n "cardito") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "card_format") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "templito") (r "^0.2.0") (d #t) (k 0)))) (h "04s8hgy2imjm4sggw2d8pvjxh752xhgxrx9lb4xqymi2gsagwh0l")))

(define-public crate-cardito-0.2.0 (c (n "cardito") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "card_format") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "templito") (r "^0.4.1") (d #t) (k 0)))) (h "0x3gz7h4nsqkj5cfbnyl7f1jg4ljsffi3mhzrgv64f2mwpg43n0b")))

(define-public crate-cardito-0.2.1 (c (n "cardito") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "card_format") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "templito") (r "^0.4.2") (d #t) (k 0)))) (h "0nhjx1ywnh7df0fyki5ynl7pnr6x31050rq4vx8d1avn6293yz1j")))

(define-public crate-cardito-0.2.2 (c (n "cardito") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "card_format") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "templito") (r "^0.4.3") (d #t) (k 0)))) (h "1qyh12am95vx61jyq7g4pydn3hx8g8zj206ggml1czkg1wsilrpc")))

