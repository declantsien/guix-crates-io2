(define-module (crates-io ca rd cards) #:use-module (crates-io))

(define-public crate-cards-1.1.0 (c (n "cards") (v "1.1.0") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "10wbf2bd5rvhzwvgcfipvdj2vazph7rpgxbm595a1bp0mgqn326p")))

(define-public crate-cards-1.1.1 (c (n "cards") (v "1.1.1") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1ip827zbw2vgsyg4wavayr8isnlxsh65ckqx1f2c7i4b5ibyq8fa")))

(define-public crate-cards-1.1.2 (c (n "cards") (v "1.1.2") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "1r0995shl34ww8vpaci1qrfin9p5r2sgpbylwjkwjpa6hgjg16nd")))

