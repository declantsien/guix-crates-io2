(define-module (crates-io ca rd card-backend-scdc) #:use-module (crates-io))

(define-public crate-card-backend-scdc-0.4.0 (c (n "card-backend-scdc") (v "0.4.0") (d (list (d (n "card-backend") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sequoia-ipc") (r "^0.29") (d #t) (k 0)) (d (n "tokio") (r "^1.13.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0hf88yxiyiln55z0hnsg4j870lal66ji5kxs81fjn277x3p9v28m")))

(define-public crate-card-backend-scdc-0.5.0 (c (n "card-backend-scdc") (v "0.5.0") (d (list (d (n "card-backend") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sequoia-ipc") (r "^0.29") (d #t) (k 0)) (d (n "tokio") (r "^1.13.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1rblmk3nprbijcx1cfvvwn16nrgp5flmp4f090168ig08lwdgcw2")))

