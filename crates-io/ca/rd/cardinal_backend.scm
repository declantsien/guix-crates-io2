(define-module (crates-io ca rd cardinal_backend) #:use-module (crates-io))

(define-public crate-cardinal_backend-0.1.0 (c (n "cardinal_backend") (v "0.1.0") (h "1yq4va9830k29a0znjhksy8bwpmaqxf13zmfscfci90m0qyd9596")))

(define-public crate-cardinal_backend-0.1.1 (c (n "cardinal_backend") (v "0.1.1") (h "11baim2j6vd08dkzcvp817n8blr4zc22awrj1pb9pnkaayrbx3m9")))

