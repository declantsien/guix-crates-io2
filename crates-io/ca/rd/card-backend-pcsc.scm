(define-module (crates-io ca rd card-backend-pcsc) #:use-module (crates-io))

(define-public crate-card-backend-pcsc-0.4.0 (c (n "card-backend-pcsc") (v "0.4.0") (d (list (d (n "card-backend") (r "^0.1") (d #t) (k 0)) (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "1xlm2l8zx9fc692sjdmvpxkxixbb752vdniglrx2vxhdcaydm52k")))

(define-public crate-card-backend-pcsc-0.5.0 (c (n "card-backend-pcsc") (v "0.5.0") (d (list (d (n "card-backend") (r "^0.2") (d #t) (k 0)) (d (n "iso7816-tlv") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (d #t) (k 0)))) (h "0ddv3jkcyy2vfc6jmlsh87yxcgkhcppp1g9sv670asqvgdq0pfv8")))

