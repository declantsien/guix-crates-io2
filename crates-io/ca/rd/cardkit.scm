(define-module (crates-io ca rd cardkit) #:use-module (crates-io))

(define-public crate-cardkit-0.1.0-alpha.1 (c (n "cardkit") (v "0.1.0-alpha.1") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0pyfv6f346smh3vs3gc3fgg8vcq8ppcph80qr8fszgvs6j6b3yxg")))

(define-public crate-cardkit-0.1.0-alpha.2 (c (n "cardkit") (v "0.1.0-alpha.2") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "10x73ysnim4xgvynr51hhcg9mfsm11arazv5ww4hdglscqi55ig6")))

(define-public crate-cardkit-0.1.0-alpha.3 (c (n "cardkit") (v "0.1.0-alpha.3") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0mrp76wglb480bkzyd28yrfqibnad0nry1y5ds69x5xgi3llw766")))

(define-public crate-cardkit-0.1.0-alpha.4 (c (n "cardkit") (v "0.1.0-alpha.4") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0x03k8a6hjlmg9xjnch8xggcyr29z637vadz36s7b22byym7k8pp")))

(define-public crate-cardkit-0.1.0-alpha.5 (c (n "cardkit") (v "0.1.0-alpha.5") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0zszhv5dy4d0s3gyixi7chqis6zsqa555fihsgyq4kh1yqvvm16d")))

(define-public crate-cardkit-0.1.0-alpha.6 (c (n "cardkit") (v "0.1.0-alpha.6") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0vjms7mjqk7mhapx4pcp3p3db4bi4ak2b5rd6ndwzgkyg62bxjwg")))

(define-public crate-cardkit-0.1.0-alpha.7 (c (n "cardkit") (v "0.1.0-alpha.7") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "002vyq530cz6s3ddgrvaiyxgyw83imlaljwgb3jwc4v48jfm5i7p")))

(define-public crate-cardkit-0.1.0-alpha.8 (c (n "cardkit") (v "0.1.0-alpha.8") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "092rxyngjpbibl4wdzlfbzzs3wymmpyyfcl9lq9n71ccf7m9g76c")))

(define-public crate-cardkit-0.1.0-alpha.9 (c (n "cardkit") (v "0.1.0-alpha.9") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "1125yya1jbvcv6q46k8mkycbwh8bvisymh9xrzvlkgf9ids88qmq")))

(define-public crate-cardkit-0.1.0-alpha.10 (c (n "cardkit") (v "0.1.0-alpha.10") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "02kkd99z2ymfq2myhakp4dqnh8wc6xgpk52yizhx45ammm5msabq")))

(define-public crate-cardkit-0.1.0-alpha.11 (c (n "cardkit") (v "0.1.0-alpha.11") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0gz9bwr2m66549zypq7p8ddl10c235xhn5ma7ckdg1bgi6j0iz7s")))

(define-public crate-cardkit-0.1.0-alpha.12 (c (n "cardkit") (v "0.1.0-alpha.12") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "0dnnwyj8kibprdwkr9sv1bhs7nk9wpjg4nfja5fb06kjwxmg6g58")))

(define-public crate-cardkit-0.1.0-alpha.13 (c (n "cardkit") (v "0.1.0-alpha.13") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "19m01nmkma6h75ff5syl1njxqfrw6a9nhxyk3bmx5nkydfs6f2s4")))

(define-public crate-cardkit-0.1.0-alpha.14 (c (n "cardkit") (v "0.1.0-alpha.14") (d (list (d (n "ultraviolet") (r "^0.6") (d #t) (k 0)))) (h "12zkdklsab3rfrwnwgrcb9gj61da4p6207hs901v7xchp1nzs1dc")))

