(define-module (crates-io ca rd cardinal-scanner) #:use-module (crates-io))

(define-public crate-cardinal-scanner-0.1.0 (c (n "cardinal-scanner") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "088wswby8j5qg7fa0d6gdrg20wizyxc9bvgpf7rpj9z7slca1i2q") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cardinal-scanner-0.1.1 (c (n "cardinal-scanner") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0xaz8yzlq046jqrxqvivg9f8x97sqpl7mpiawvzgaa8avprxha6y") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cardinal-scanner-0.1.2 (c (n "cardinal-scanner") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0z1v6kva60f0xvb325swy0m7rfcfrz9g0ads8yvb4nbd84f2h15g") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cardinal-scanner-0.1.3 (c (n "cardinal-scanner") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1jr21s0266pw617z92yq594klgi9sg3xsf77q34lrlh79cjk33vz") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

