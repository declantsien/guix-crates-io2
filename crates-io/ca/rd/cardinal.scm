(define-module (crates-io ca rd cardinal) #:use-module (crates-io))

(define-public crate-cardinal-0.1.0 (c (n "cardinal") (v "0.1.0") (h "0gv67d1h3v6973p1yqn1424i73p4vfqiinsimvkcp8jg43zvqwg5") (y #t)))

(define-public crate-cardinal-0.1.1 (c (n "cardinal") (v "0.1.1") (d (list (d (n "unutils") (r "^0.1") (d #t) (k 0)))) (h "04ykaqdx2g7mfn35y4fm449r8zm5qrvcia7lixi1addk8v69zmra")))

