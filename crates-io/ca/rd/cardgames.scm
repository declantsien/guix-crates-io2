(define-module (crates-io ca rd cardgames) #:use-module (crates-io))

(define-public crate-cardgames-0.1.0 (c (n "cardgames") (v "0.1.0") (d (list (d (n "fraction") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yklrjijgka6l2shvxk2201704qig3cln1sj8ing2bgc38ym359l")))

(define-public crate-cardgames-0.2.0 (c (n "cardgames") (v "0.2.0") (d (list (d (n "fraction") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bbjn41ban530l7gp25gzlcfgmr2v8kl97jlbczp5n5d8fz72daf")))

(define-public crate-cardgames-0.2.1 (c (n "cardgames") (v "0.2.1") (d (list (d (n "fraction") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ikxdc8h0qfr046jvlha4i9llx0cjigcfp4n33g4pjan61q65rl3")))

(define-public crate-cardgames-0.2.2 (c (n "cardgames") (v "0.2.2") (d (list (d (n "fraction") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "069c26b1w3wamk6n9bj1chix9nr06vbh1qnfyzvxfryqg0ighy33")))

(define-public crate-cardgames-0.2.3 (c (n "cardgames") (v "0.2.3") (d (list (d (n "fraction") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03iqk1m2cjwcr7yyyrf3w49783qsbnh5c1idsksasa6dxis8q2fr")))

(define-public crate-cardgames-0.2.4 (c (n "cardgames") (v "0.2.4") (d (list (d (n "fraction") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vkvm0ydqyy5xf2vxi2vj8mv7ns6bqswa6v89z6lbmvgkykjs978")))

(define-public crate-cardgames-0.2.5 (c (n "cardgames") (v "0.2.5") (d (list (d (n "fraction") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12hmrqmc416kvfk5vrq5ipxv7cpxazzygc9r72hick3llyrmr3l5")))

