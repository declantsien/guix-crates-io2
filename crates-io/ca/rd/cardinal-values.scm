(define-module (crates-io ca rd cardinal-values) #:use-module (crates-io))

(define-public crate-cardinal-values-0.1.0 (c (n "cardinal-values") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ka7hr5kfzd33026j9f05cvyyg7lxl6z78x4dmgqjw1wxmhcjcyi")))

(define-public crate-cardinal-values-0.1.1 (c (n "cardinal-values") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ds1zjwxdf741dq80q543k6r0dl4x69bcrmfz7v35qjr02v0357l")))

