(define-module (crates-io ca rd cardinal-codegen) #:use-module (crates-io))

(define-public crate-cardinal-codegen-0.1.0 (c (n "cardinal-codegen") (v "0.1.0") (h "0ny1vyznhgip2likk64sw8kg2q020k4ks6bgjv9m32h54jm6p3ji")))

(define-public crate-cardinal-codegen-0.1.1 (c (n "cardinal-codegen") (v "0.1.1") (h "0a5x2q8vpcbqqf4fd4n4hygwf2glp4fspbf2myn1n5512yli9mkl")))

