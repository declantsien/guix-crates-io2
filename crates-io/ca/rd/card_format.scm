(define-module (crates-io ca rd card_format) #:use-module (crates-io))

(define-public crate-card_format-0.1.0 (c (n "card_format") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "gobble") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "18lsk8jhsbhzfcxw3jpbdqyxvaq90krag3airbpr2wn8pdfmi3ly")))

(define-public crate-card_format-0.2.1 (c (n "card_format") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)) (d (n "tokenate") (r "^0.1.0") (d #t) (k 0)))) (h "1a14w0xk95m5qyy584igrd7p8qb9abq697xbxsrvql600kllhzzm")))

