(define-module (crates-io ca rd cardparse) #:use-module (crates-io))

(define-public crate-cardparse-0.1.0 (c (n "cardparse") (v "0.1.0") (d (list (d (n "cardparse_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0sjcdbgwqvr5v3fkng7pxb4k3cw6gr9a6yargkw83qljns47j592")))

