(define-module (crates-io ca rd card-validate) #:use-module (crates-io))

(define-public crate-card-validate-0.2.0 (c (n "card-validate") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "094qijssz2ma4bn7pivl9nlgc1sj46j4q0lrvzy24zxwdnsqkdn1")))

(define-public crate-card-validate-0.2.1 (c (n "card-validate") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0819bzfvn13shnlfm6h6rm1h2nxpd2j1kh3ib8skkghvlljzdfp7")))

(define-public crate-card-validate-0.2.2 (c (n "card-validate") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0fg54yc6i45w7c1mkks1y11dfw3xh891yjd653x881jb6mg9zc24")))

(define-public crate-card-validate-1.0.0 (c (n "card-validate") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0smbyagysgd8q015am750ylpd2aqh0rdmjg981ya7cb7rk2scg8b")))

(define-public crate-card-validate-1.0.1 (c (n "card-validate") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "14q6y1ffzpa5qwf75f76gfjic7pmgb5vfn60ppyd3r85gwlqjrbq")))

(define-public crate-card-validate-1.0.2 (c (n "card-validate") (v "1.0.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ggp7k375n674phf19p4i92s0i0n59djz43xvqp0xmig7r27lzb4")))

(define-public crate-card-validate-1.0.3 (c (n "card-validate") (v "1.0.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "01cqw0avybjiwci00vq79zp4si9kl1cijm8plx2m3j49nb3n28wa")))

(define-public crate-card-validate-1.0.4 (c (n "card-validate") (v "1.0.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "08w6k28lblik42m7v1bvyp0da2klwpxa9d00ydwzic29rb6392gj") (y #t)))

(define-public crate-card-validate-1.1.0 (c (n "card-validate") (v "1.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1srwb74xci3jzv6mwfk0zz1kkzprjl19f8qpnprpcb3ky34ya9w1") (y #t)))

(define-public crate-card-validate-2.0.0 (c (n "card-validate") (v "2.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0g9axhk6iiq9g1bm9v4cidbvn68p57n9syfg2js6b1n67n0v0r1x")))

(define-public crate-card-validate-2.0.1 (c (n "card-validate") (v "2.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1qyv17p72arapi04d33ddzz318vm4skz7jcdfdbsp3jmv1friyd7")))

(define-public crate-card-validate-2.1.0 (c (n "card-validate") (v "2.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "110pbj2pm2zlvh6m8g8xccki08dy603lgyph83azspp8bbzc30wb")))

(define-public crate-card-validate-2.1.1 (c (n "card-validate") (v "2.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ckfr65mf43f219zzcd1n4f51ksmyypa97la97bfgmg3j3p7ndpi")))

(define-public crate-card-validate-2.1.2 (c (n "card-validate") (v "2.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0q1rd8b90k35mcjpl4pqpqpf7fgg6vi3m5fpp5vs4cqvsv17pxkw")))

(define-public crate-card-validate-2.1.3 (c (n "card-validate") (v "2.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16r9p0y2vi083x04ppv3q35z2j5j1dkifn8901hjqr0imw14fnh2")))

(define-public crate-card-validate-2.1.4 (c (n "card-validate") (v "2.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1wsna57m15fh8nhlr83d1z0hk5m4adl815pfyfdvxskcl91dq97n")))

(define-public crate-card-validate-2.1.5 (c (n "card-validate") (v "2.1.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "08nxh5aim5p3nsrm96lsvkrjw2a52g471na0slclfi18ma9yh5nd")))

(define-public crate-card-validate-2.1.6 (c (n "card-validate") (v "2.1.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "luhnmod10") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1nnq4g6cxswdgm3khy88b27z1x8h59q59di7nd766mqa19k5lnqr")))

(define-public crate-card-validate-2.2.0 (c (n "card-validate") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1j9pi4kc89gwq1jb8r6i43xy1xgn08k2c90scr1r3w3rnsx2qzip")))

(define-public crate-card-validate-2.2.1 (c (n "card-validate") (v "2.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0wfq1zg9nxfxw5y8jgpxa4wbbl87q0xi7k41n79angz1z7akds22")))

(define-public crate-card-validate-2.2.2 (c (n "card-validate") (v "2.2.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1bgay22f2ir2q05izv8509ndxznhfj80gcsxrjfrw949472i9b1w")))

(define-public crate-card-validate-2.2.3 (c (n "card-validate") (v "2.2.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0zkvrk4ya5zxsldn7k8sw3x1ssgd3f6yggj0jdk2n07d0mcczcxy")))

(define-public crate-card-validate-2.3.0 (c (n "card-validate") (v "2.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "075kamy7wcmsn5fshs6b78lqvihk3bm0i9awjrlci60wnrp71538")))

(define-public crate-card-validate-2.4.0 (c (n "card-validate") (v "2.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "14gbz65z79n1xjxmh5fr8g6djshnq2g49i60q1r878z2jq2sfpv5")))

