(define-module (crates-io ca rd cardano-message-signing) #:use-module (crates-io))

(define-public crate-cardano-message-signing-1.0.1 (c (n "cardano-message-signing") (v "1.0.1") (d (list (d (n "base64-url") (r "^1.4.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cbor_event") (r "^2.1.3") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "pruefung") (r "^0.2.1") (d #t) (k 0)))) (h "0fibksh1f67m7acjsvcq1n7wnrysm547b711bf0j1s98nm172yf5")))

