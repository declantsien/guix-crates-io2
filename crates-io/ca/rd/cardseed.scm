(define-module (crates-io ca rd cardseed) #:use-module (crates-io))

(define-public crate-cardseed-0.0.1 (c (n "cardseed") (v "0.0.1") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1z663j8dfcjp5swc28jnsqhxgnbq0al4x5d6ac2228bdizhx8cqb")))

(define-public crate-cardseed-0.0.2 (c (n "cardseed") (v "0.0.2") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1pwmrkxbrk0j6m03sj9jcgjn7pcqy81b10xx5khl6n8l786l78bh")))

