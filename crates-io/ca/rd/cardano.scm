(define-module (crates-io ca rd cardano) #:use-module (crates-io))

(define-public crate-cardano-0.1.0 (c (n "cardano") (v "0.1.0") (h "0r0m5c6bq59w0ca3bx9i6p2y58c20k92dq8ayssypj49cpab2i3i")))

(define-public crate-cardano-0.0.0 (c (n "cardano") (v "0.0.0") (h "1hw22smwyrbyvgy3yq9hdy7yay2zaa5dyndwyngbs4k5c9jgn0w5")))

(define-public crate-cardano-0.0.1 (c (n "cardano") (v "0.0.1") (h "0minmpb2x8jxc7v52hxd4h976bmyn7c4315g7mz8hvw30nxwqmzn")))

(define-public crate-cardano-0.1.1 (c (n "cardano") (v "0.1.1") (h "1a94v14cahkn5h41m2akbfarmxk1hpdpn5g7b0pbhisdiz9liq2h")))

(define-public crate-cardano-0.1.2 (c (n "cardano") (v "0.1.2") (h "1bnwcpvil1drpi6f2jzy8hyb4yc5gr4ni1zd9bnmkcf3c2j27ynx")))

(define-public crate-cardano-0.1.3 (c (n "cardano") (v "0.1.3") (h "0dpj7xwvnbl0dzf7slwmm8gsig2fqz4a3a4v3j763kpvrlmmb2jm")))

