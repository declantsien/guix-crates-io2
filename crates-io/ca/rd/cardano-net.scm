(define-module (crates-io ca rd cardano-net) #:use-module (crates-io))

(define-public crate-cardano-net-0.1.0 (c (n "cardano-net") (v "0.1.0") (d (list (d (n "cardano-sdk") (r "^0.1") (d #t) (k 0)) (d (n "cbored") (r "^0.1") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (d #t) (k 0)))) (h "06mi229n8drrrv6dib4y15sbcfqvcfkv0qps5qnavlkn71bzn63r")))

(define-public crate-cardano-net-0.2.0 (c (n "cardano-net") (v "0.2.0") (d (list (d (n "cardano-sdk") (r "^0.2") (d #t) (k 0)) (d (n "cbored") (r "^0.3") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (d #t) (k 0)))) (h "0xrh4yxwl3np94cfgjmk0pbr4iyda59s4xwkxc2z5zsczc1zl8d4")))

(define-public crate-cardano-net-0.2.1 (c (n "cardano-net") (v "0.2.1") (d (list (d (n "cardano-sdk") (r "^0.2") (d #t) (k 0)) (d (n "cbored") (r "^0.3") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (d #t) (k 0)))) (h "096h3fdm5gas78q4wpfisk5q94rwhdcwzh84lagm0ph3hgnp5qmr")))

