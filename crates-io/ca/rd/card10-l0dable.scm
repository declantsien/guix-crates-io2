(define-module (crates-io ca rd card10-l0dable) #:use-module (crates-io))

(define-public crate-card10-l0dable-0.1.0 (c (n "card10-l0dable") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "panic-abort") (r "^0.3") (d #t) (k 0)) (d (n "r0") (r "^0.2") (d #t) (k 0)))) (h "1mz6qaa8csxga43azm9l7f8a509gl1jf046ix19cv632b39spyrc") (y #t)))

(define-public crate-card10-l0dable-0.1.1 (c (n "card10-l0dable") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "panic-abort") (r "^0.3") (d #t) (k 0)) (d (n "r0") (r "^0.2") (d #t) (k 0)))) (h "0rsp5nqkx20p4smc9qhxwz7540l5ydzwp86xiyn3ckpx9j0biskj")))

(define-public crate-card10-l0dable-0.2.0 (c (n "card10-l0dable") (v "0.2.0") (d (list (d (n "card10-sys") (r "1.9.*") (d #t) (k 0)))) (h "0d998gqqp1rxk03h3jz0jz26xcnghgrics1rlm2sdnq7867mfpcm")))

(define-public crate-card10-l0dable-0.3.0 (c (n "card10-l0dable") (v "0.3.0") (d (list (d (n "card10-sys") (r "^1.10") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.5") (d #t) (k 0)))) (h "1z52wsn68c2a4wdd09j9rdklh2snwf81mdhmiw04ka0cgav24dym")))

