(define-module (crates-io ca rd cardparse_derive) #:use-module (crates-io))

(define-public crate-cardparse_derive-0.1.0 (c (n "cardparse_derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "19lr4m7zazlhs25vxkxm1ijwbymlqj84z019lfcc4zi0h6mds5p3")))

