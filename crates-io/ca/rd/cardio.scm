(define-module (crates-io ca rd cardio) #:use-module (crates-io))

(define-public crate-cardio-0.1.1 (c (n "cardio") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "1a7r3vwc0mx6qkdz5a1ypfv6qv5ghb772pkz7lqz62z2qqrqnxlh")))

(define-public crate-cardio-0.1.2 (c (n "cardio") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "0b12bng4k47l5xz4l2ycjnv99n7rm08llplk6mzhzyyzcqa040ic")))

