(define-module (crates-io ca de cadence-with-flush) #:use-module (crates-io))

(define-public crate-cadence-with-flush-0.29.0 (c (n "cadence-with-flush") (v "0.29.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0lszgxl5y0fm7m0d6mkfw3caz26nssz88cqklxkjmmdmkj4mg7rs")))

(define-public crate-cadence-with-flush-0.29.1 (c (n "cadence-with-flush") (v "0.29.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "01m9svigpajxh4lvnnz8gvs4wxsc5g409633r2hidvdf5cscfz1b")))

