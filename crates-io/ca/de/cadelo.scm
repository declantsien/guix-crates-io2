(define-module (crates-io ca de cadelo) #:use-module (crates-io))

(define-public crate-cadelo-0.0.1 (c (n "cadelo") (v "0.0.1") (d (list (d (n "ascii") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "kerbeiros") (r "^0.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "1zja2hffsgkkr1x3cqzplpx1lgfzpciixb7538njfnrcg1cpdsfc")))

