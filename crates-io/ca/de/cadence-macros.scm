(define-module (crates-io ca de cadence-macros) #:use-module (crates-io))

(define-public crate-cadence-macros-0.24.0 (c (n "cadence-macros") (v "0.24.0") (d (list (d (n "cadence") (r "^0.24") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0kfp55ml49yyv3ssz6gjf916q597xdw3m01cdxxxvlr0n7pmzxgx") (y #t)))

(define-public crate-cadence-macros-0.25.0 (c (n "cadence-macros") (v "0.25.0") (d (list (d (n "cadence") (r "^0.25") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 2)))) (h "064snyarln5g7hc8gywg4mj3skg2p1vz9gj93m59z0bhxqrmvh49")))

(define-public crate-cadence-macros-0.26.0 (c (n "cadence-macros") (v "0.26.0") (d (list (d (n "cadence") (r "^0.26") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 2)))) (h "0rzfjs5r72zjlmbzff91rnjnkvlb1znngws50sfvn3lx1p9nj89h")))

(define-public crate-cadence-macros-0.27.0 (c (n "cadence-macros") (v "0.27.0") (d (list (d (n "cadence") (r "^0.27") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "10fx0ir50nz7lbzyknwagl1a5mmyx365km5ilxg1fzzbfwl32ixm")))

(define-public crate-cadence-macros-0.28.0 (c (n "cadence-macros") (v "0.28.0") (d (list (d (n "cadence") (r "^0.28") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "0jxxcg5q38jaars9w3ckblylvy23q6k8ckh41xb0h2s3lw214jz9")))

(define-public crate-cadence-macros-0.29.0 (c (n "cadence-macros") (v "0.29.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "1n6ha475r78g00hbqgxy85bq4wq871nzglykpbpjxlg7133v8way")))

(define-public crate-cadence-macros-0.29.1 (c (n "cadence-macros") (v "0.29.1") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "1087qx7msmcjdqymn7fhg8vzcp13d5wnzd0029hxyad3r2ihg7qs")))

(define-public crate-cadence-macros-1.0.0 (c (n "cadence-macros") (v "1.0.0") (d (list (d (n "cadence") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "190y704xfc64lv5xc9sy0mz5ds0vybr2hva5dplz7ci27czb8lln")))

(define-public crate-cadence-macros-1.1.0 (c (n "cadence-macros") (v "1.1.0") (d (list (d (n "cadence") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "096jmj8k79w4x138vsyc1zkghhaalhiifz9kkrkdr16wr34cr2ji")))

(define-public crate-cadence-macros-1.2.0 (c (n "cadence-macros") (v "1.2.0") (d (list (d (n "cadence") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "0arbqypb8dmr75zdjvb96gl0vrwr5ywxiq687g1i94dpyswsdjik")))

(define-public crate-cadence-macros-1.3.0 (c (n "cadence-macros") (v "1.3.0") (d (list (d (n "cadence") (r "^1.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "1i16s2163ggsgv43bq94ywhh84ldlhy4vrkfxgk31rbw67vxchni")))

(define-public crate-cadence-macros-1.4.0 (c (n "cadence-macros") (v "1.4.0") (d (list (d (n "cadence") (r "^1.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 2)))) (h "0kzc3jaxknx3nvq0chg6qzpwn5y5a1z6pxjk9fqxpggvsw7kc0k7")))

