(define-module (crates-io ca de cadence_json) #:use-module (crates-io))

(define-public crate-cadence_json-0.1.0 (c (n "cadence_json") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_with") (r "^1.10.0") (d #t) (k 0)))) (h "01yam1ky11kq104dr288qr0fhszmra40ki830da3zqp7pzd29qpa")))

(define-public crate-cadence_json-0.1.2 (c (n "cadence_json") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.10.0") (d #t) (k 0)))) (h "1vxssanfpzvg8xh6ha3wj4a1yvhwgi1972cnbqd2gfgvv3dc81kq")))

