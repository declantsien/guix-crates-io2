(define-module (crates-io ca ss casserole) #:use-module (crates-io))

(define-public crate-casserole-0.1.0 (c (n "casserole") (v "0.1.0") (d (list (d (n "ascii85") (r "^0.2") (d #t) (k 0)) (d (n "casserole-derive") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jlwxfm6hkc04yvs79axi7z44aqvkg8gk24jj6xynr18jd38jr0z")))

(define-public crate-casserole-0.2.0 (c (n "casserole") (v "0.2.0") (d (list (d (n "ascii85") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "casserole-derive") (r "^0.2") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c0k8gkjwhf5z4f8rbsfhl160dbhk3pgprdnb8d87qy0bnd6hl3h") (f (quote (("rc" "serde/rc") ("json-store" "ascii85" "rust-crypto" "serde_json") ("default"))))))

