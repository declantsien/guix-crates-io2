(define-module (crates-io ca ss cassandra-sigv4) #:use-module (crates-io))

(define-public crate-cassandra-sigv4-0.1.0 (c (n "cassandra-sigv4") (v "0.1.0") (d (list (d (n "aws-sigv4") (r "^0.55.2") (f (quote ("bytes"))) (k 0)) (d (n "cassandra-cpp-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha256") (r "^1.1.3") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)))) (h "04dcdwk456vi0c4pccq5zf6djkdjlhmmmdyzw0cq1lk8dsrbybp2")))

(define-public crate-cassandra-sigv4-0.1.1 (c (n "cassandra-sigv4") (v "0.1.1") (d (list (d (n "aws-sigv4") (r "^0.55.2") (f (quote ("bytes"))) (k 0)) (d (n "cassandra-cpp-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha256") (r "^1.1.3") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)))) (h "030n65lhamhcaf0w22is0h6ds3f1s9ck9iyx8zn1k9bdgcsgglar")))

(define-public crate-cassandra-sigv4-0.1.2 (c (n "cassandra-sigv4") (v "0.1.2") (d (list (d (n "aws-sigv4") (r "^0.55.2") (f (quote ("bytes"))) (k 0)) (d (n "cassandra-cpp-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha256") (r "^1.1.3") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)))) (h "1hk54fyrxvs759l2nlknf9y9whjq6ayr18jpcwsl6m0inf37mmfz")))

(define-public crate-cassandra-sigv4-0.1.3 (c (n "cassandra-sigv4") (v "0.1.3") (d (list (d (n "aws-sigv4") (r "^0.55.2") (f (quote ("bytes"))) (k 0)) (d (n "cassandra-cpp-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha256") (r "^1.1.3") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)))) (h "0rdsg9x4disx17sr2p9pyjkxf0p2spi3ijfiw3lgnp0rkxmxiyi2")))

