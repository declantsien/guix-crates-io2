(define-module (crates-io ca ss cassowary) #:use-module (crates-io))

(define-public crate-cassowary-0.1.0 (c (n "cassowary") (v "0.1.0") (h "1ri4wlgnkmsamp27600qrhhv74pvkqgrwv90q3qyfrkjxhalak4k")))

(define-public crate-cassowary-0.2.0 (c (n "cassowary") (v "0.2.0") (h "0wrdvxvhjcwb2fn8wc0cs81d3f1phbxdh5my64lya05rlpf4yhkh")))

(define-public crate-cassowary-0.2.1 (c (n "cassowary") (v "0.2.1") (h "10rc8c0fpqp7qbkszkv7p72fy4q7pkbf10w3yydnrwdb3878s0mi")))

(define-public crate-cassowary-0.3.0 (c (n "cassowary") (v "0.3.0") (h "0lvanj0gsk6pc1chqrh4k5k0vi1rfbgzmsk46dwy3nmrqyw711nz")))

