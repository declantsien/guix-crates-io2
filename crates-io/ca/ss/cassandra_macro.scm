(define-module (crates-io ca ss cassandra_macro) #:use-module (crates-io))

(define-public crate-cassandra_macro-0.1.0 (c (n "cassandra_macro") (v "0.1.0") (h "1mq0imbbdxn3cfjmpliyq958ahcnhdj91mjjsgmfgi2c7n447kmm")))

(define-public crate-cassandra_macro-0.1.1 (c (n "cassandra_macro") (v "0.1.1") (d (list (d (n "cdrs") (r "^2") (d #t) (k 0)))) (h "09h50k8zhjcbc8gy338y87hx6i581w1am25n0nk1jmba05xrlw91")))

(define-public crate-cassandra_macro-0.1.2 (c (n "cassandra_macro") (v "0.1.2") (d (list (d (n "cdrs") (r "^2") (d #t) (k 0)))) (h "019nnszsh6svgwpk3by6rlkj8k0diyw9b2x70m2vd3y0sfy7fpf8")))

(define-public crate-cassandra_macro-0.1.3 (c (n "cassandra_macro") (v "0.1.3") (d (list (d (n "cdrs") (r "^2") (d #t) (k 0)))) (h "1qmhc3kp4fzwa8ilcbqgbw9ygwm8wsm5n9600cz7hv72l5vzhvkr")))

