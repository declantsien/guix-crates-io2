(define-module (crates-io ca ss cassie) #:use-module (crates-io))

(define-public crate-cassie-0.1.0 (c (n "cassie") (v "0.1.0") (h "1hbc78s3l5qqmll7jxwj57imx6jyzycd4jgyk481i6yc7bg5713n")))

(define-public crate-cassie-0.1.1 (c (n "cassie") (v "0.1.1") (h "09is65bf823rb9gzbnikqx3kb9zr5bai4gajzd4w68ym54zsjamj")))

(define-public crate-cassie-0.1.2 (c (n "cassie") (v "0.1.2") (h "1ckszwfa2nfv2xf1qs97ay7ddmhs07929ra2y1xbi5i1b2lgpl6q")))

