(define-module (crates-io ca ss cassandra-cpp-sys) #:use-module (crates-io))

(define-public crate-cassandra-cpp-sys-0.8.8 (c (n "cassandra-cpp-sys") (v "0.8.8") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0iz4dmcslikdgb0nx4vp6admr09aal92xr25g9f371jj4d7xappx") (f (quote (("default"))))))

(define-public crate-cassandra-cpp-sys-0.10.0 (c (n "cassandra-cpp-sys") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1fq8xjpbv1bdc80dlvvjygadd1zqrbd0wv4yalc34krsmhlag9wk") (f (quote (("default"))))))

(define-public crate-cassandra-cpp-sys-0.11.0 (c (n "cassandra-cpp-sys") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1aribag2qf0723szy2nv45fdfhlgfv72bxay1q7v95xjcfgihqyd") (f (quote (("default"))))))

(define-public crate-cassandra-cpp-sys-0.12.0 (c (n "cassandra-cpp-sys") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "1jr0zn704hpm8zkfvsy634wkdnppgfy47j1g4bb0s0px5fdh9f0q") (f (quote (("default"))))))

(define-public crate-cassandra-cpp-sys-0.12.1 (c (n "cassandra-cpp-sys") (v "0.12.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "1872m0lcavp1hnmwjzj89l7q57admas0sfwr1gc8pmvdaqarcs0s") (f (quote (("default"))))))

(define-public crate-cassandra-cpp-sys-0.12.2 (c (n "cassandra-cpp-sys") (v "0.12.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0ajiv8n0l2ic9hhjf96f0pk3nxr28rw6dnvd2pb4m2nr9pdcd094") (f (quote (("default")))) (l "cassandra")))

(define-public crate-cassandra-cpp-sys-0.12.3 (c (n "cassandra-cpp-sys") (v "0.12.3") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0mm1fdlrkr9vkb96xxbx2ff7d8mp3xm99ala5xcxhm1js42f9pga") (f (quote (("default")))) (l "cassandra")))

(define-public crate-cassandra-cpp-sys-1.0.0 (c (n "cassandra-cpp-sys") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0ci6i0igavxmxj85gvkq9q5x7jjs96yf5rzfw209r9ckpbqj0g3a") (f (quote (("default")))) (l "cassandra")))

(define-public crate-cassandra-cpp-sys-1.1.0 (c (n "cassandra-cpp-sys") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0h0fdm5hgc5k4na5ggm53vxsxjrybm8vhmrm3sjbafl762fx4d42") (f (quote (("early_access_min_tls_version") ("default")))) (l "cassandra")))

