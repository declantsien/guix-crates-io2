(define-module (crates-io ca ss cassandra-sys) #:use-module (crates-io))

(define-public crate-cassandra-sys-0.2.1 (c (n "cassandra-sys") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "17vlqywvsvp2m9744ajb7dj1jr6r0q0d9c8r18rjf26n1gm25lvr")))

(define-public crate-cassandra-sys-0.2.3 (c (n "cassandra-sys") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0xdpl7z5808ac2q6dz293f7axwfvi1khbv2ywc5cg2kl24q8gn5y")))

(define-public crate-cassandra-sys-0.2.4 (c (n "cassandra-sys") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1sk6zfl5qm3g39i6v0salnpr9piq1myg67gbva8fcyhda89vq9kp")))

(define-public crate-cassandra-sys-0.3.2 (c (n "cassandra-sys") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0snidsgrwbl8fb34lxzgyws0m186ck92md9q4n4kqm94a30r9fj4")))

(define-public crate-cassandra-sys-0.4.1 (c (n "cassandra-sys") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1xw51prh4118gs94jjbzc8sws7mm2dhjncn0qz4a1sxy1vx81m0x")))

(define-public crate-cassandra-sys-0.5.0 (c (n "cassandra-sys") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1rmm8pc5ib0c6n4nhkxwpg0pkcyq6wzii6rq0b0gr9h9zlqjymsh") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.5.1 (c (n "cassandra-sys") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "17124150mx6n3x4xvi0x6nndzzbiyzhlwrmaj31pvpbjjw3v6k4h") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.6.0 (c (n "cassandra-sys") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0cydy6vwhr3plcymx1v3j67zd217bjp72z3d4jwp574a6q2gajwf") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.7.0 (c (n "cassandra-sys") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "08g830zmyqpbm2hj9ssv73x70b5vfirv4fah8cz8q45x8jkva667") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.0 (c (n "cassandra-sys") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0gziclgf2ahfg39s7qgq19apl9bn4a5xikn009ays4c49ydd2n6s") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.1 (c (n "cassandra-sys") (v "0.8.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "171bv186633id9s8ilwfdhfn8zlnyai01c73xrxzf67jmf8yffd1") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.2 (c (n "cassandra-sys") (v "0.8.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1cljy5aniik7ryz1wkaaz34420jp5l18g3xb5y3q31gpmc827lik") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.4 (c (n "cassandra-sys") (v "0.8.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0r8qgxdmzhcxc5ap2n48ak3iz23669bcxjh4pclsr53gadn37yzf") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.5 (c (n "cassandra-sys") (v "0.8.5") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0bn845wynvnswqx5cm30v1p78zl6prmnj9g59rqavirr9dcpha1h") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.6 (c (n "cassandra-sys") (v "0.8.6") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0bvj0f2g3w9i00zndrfib74pv1zx3p0i4i2qabwqlw2lxrjdyg0y") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.8.7 (c (n "cassandra-sys") (v "0.8.7") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "01zkj6k2566kb3m8zk9023jcmjw78ajakrrn09c4vgfi27hg57b9") (f (quote (("default"))))))

(define-public crate-cassandra-sys-0.9.0 (c (n "cassandra-sys") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libbindgen") (r "^0.1") (f (quote ("llvm_stable"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1kr24l831k8kr1l32qizws6vdiywrrq4avgyg5ywmqd10qksjhrx") (f (quote (("default"))))))

