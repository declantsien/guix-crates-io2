(define-module (crates-io ca ss cassiopeia) #:use-module (crates-io))

(define-public crate-cassiopeia-0.3.0 (c (n "cassiopeia") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.0") (f (quote ("wrap_help" "suggestions" "color"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)))) (h "1xxbxv0zaafdbm5sh51gc71k767icnf402m90ai2w44g39c7lzhg")))

