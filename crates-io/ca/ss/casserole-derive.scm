(define-module (crates-io ca ss casserole-derive) #:use-module (crates-io))

(define-public crate-casserole-derive-0.1.0 (c (n "casserole-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zh0ybabjz4amd9rkxij6j4cra3kbl2kqki511gmxszp4vk66zb6")))

(define-public crate-casserole-derive-0.2.0 (c (n "casserole-derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "01al8rf5lg2m8ajvvwd3dar2c05bqlwxqa5kij53a8hxr15lnvbb")))

