(define-module (crates-io ca ss cassandra_macro_derive) #:use-module (crates-io))

(define-public crate-cassandra_macro_derive-0.1.0 (c (n "cassandra_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07pa02f98vbf3cgxb3q34gjr6jgqnx2mxn8ldhny6nnbkf1q5wj9")))

(define-public crate-cassandra_macro_derive-0.1.1 (c (n "cassandra_macro_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09v68psamb5j9minh6vignciwzi1rkmkxzkg4l4ssg6vbqbgswix")))

(define-public crate-cassandra_macro_derive-0.1.2 (c (n "cassandra_macro_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dvm7dp4846mlwxvi58c45crimjncbznbmjc9mkmi8ajg3sv7i2g")))

(define-public crate-cassandra_macro_derive-0.1.3 (c (n "cassandra_macro_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kxs6l6lgg25nn0j6hc3gll4ihiisnvc9yk2sjgma7sdqb11rmkg")))

