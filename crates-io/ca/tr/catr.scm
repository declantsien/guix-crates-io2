(define-module (crates-io ca tr catr) #:use-module (crates-io))

(define-public crate-catr-0.1.0 (c (n "catr") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "06xg4l182q8fbx7h3hzprnqm7sn91znan5s4bkd2jvjh6ndmnzx4") (y #t)))

(define-public crate-catr-0.1.1 (c (n "catr") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0nkbsbf3lmpfzc0qpq799kvpa0ihzbf5divpbicc56vfdm3mbza9") (y #t)))

(define-public crate-catr-0.1.2 (c (n "catr") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1qywqqc99hl5dn0gs6n3sbnd6a1qxnzpksndr1rj8qy2ygsaf4wy")))

