(define-module (crates-io ca tr catread) #:use-module (crates-io))

(define-public crate-catread-0.1.0 (c (n "catread") (v "0.1.0") (h "04nkvn69xvmy2d8nb86y899501ph1mpz7rzq23ai2myvf31nnn0y")))

(define-public crate-catread-0.2.0 (c (n "catread") (v "0.2.0") (h "1ygrgdjv3j6sv87by79ajd8405jcmzblman1w5pg82619jnzcvq5")))

