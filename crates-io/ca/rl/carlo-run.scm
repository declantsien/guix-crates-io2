(define-module (crates-io ca rl carlo-run) #:use-module (crates-io))

(define-public crate-carlo-run-0.1.0 (c (n "carlo-run") (v "0.1.0") (d (list (d (n "carlotk") (r "^0.1.0") (d #t) (k 0)))) (h "1i05m2gqfwg54zdcb6as2psjdkw7cqf3a2d04khjxpab94s8ifqp")))

(define-public crate-carlo-run-0.2.0 (c (n "carlo-run") (v "0.2.0") (d (list (d (n "carlotk") (r "^0.12.0") (d #t) (k 0)))) (h "04zx1gx6s9lia3nqgvy49nfkf69qpvg755dgmhddcqjj8d2y48h1")))

(define-public crate-carlo-run-1.0.0 (c (n "carlo-run") (v "1.0.0") (d (list (d (n "carlotk") (r "^1.0.0") (d #t) (k 0)))) (h "0f7fp5a65qb9hwblagl5789cgvxqr0x309swxzjzpxv23r1nrj2f")))

