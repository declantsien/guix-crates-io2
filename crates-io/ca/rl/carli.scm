(define-module (crates-io ca rl carli) #:use-module (crates-io))

(define-public crate-carli-0.1.0 (c (n "carli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1xxrr3bzy2yya8bbzcysckmflrzpnc34w9shi2dmf9pjaj4gbmnb") (r "1.57")))

(define-public crate-carli-0.2.0 (c (n "carli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qgzivzl9afr69qgw9d854fg35didh50ckpkcgizlvl6vsy6mikr") (r "1.57")))

(define-public crate-carli-0.2.1 (c (n "carli") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0nv519my0hbns9y7zxanfwlyhr4dk5mbqrmcl6gb0a79b43i4m2n") (r "1.57")))

