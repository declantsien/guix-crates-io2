(define-module (crates-io ca rl carlo) #:use-module (crates-io))

(define-public crate-carlo-0.1.0 (c (n "carlo") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1j03m7fg2dxw9a6dn562c5cwl0iwf30lxjz35rlq47m6v2ndnk91")))

(define-public crate-carlo-0.2.0 (c (n "carlo") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0kqkkgh8lilz1vkzz706iacdamrdqsp11skbqlnnmy2x1y79gmmc")))

(define-public crate-carlo-0.2.1 (c (n "carlo") (v "0.2.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1511hz27nraj6njcmnp63scsdjmlik6m6zfx8frav5ri8s0jhrk4")))

(define-public crate-carlo-0.3.0 (c (n "carlo") (v "0.3.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1l2cqdr144y5873xsdckj2la9bdflbp7zj0gynkdwymasjkwva59")))

(define-public crate-carlo-0.4.0 (c (n "carlo") (v "0.4.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "03dc1fjqnqxzpv5is8yfzx34h98i7ppzxrwwzg40711i4r7dyap2")))

(define-public crate-carlo-0.5.0 (c (n "carlo") (v "0.5.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1lyk4pab6ckvs5m5f6nzbiqvbhz310z10kqvmijlpz6p3v9wmzmi")))

(define-public crate-carlo-0.5.1 (c (n "carlo") (v "0.5.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "153bxf740a5vfdx7b62ipcfwj93macliwqv73pxgppq366qx44ms")))

(define-public crate-carlo-0.6.0 (c (n "carlo") (v "0.6.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0abg59hr339jd7bm46wd3jja276a3c4h2izvsffk181jvhqlc8ii")))

(define-public crate-carlo-0.7.0 (c (n "carlo") (v "0.7.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0187fvq01gn15b60vq1gcwkxv1q3rvvmq9y3kl8bl63h1fngm7p0")))

(define-public crate-carlo-0.8.0 (c (n "carlo") (v "0.8.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "015a3k2sbv0xyb3cj1sc17xriy26xpyq30c9npjdi188di2hjvqj")))

(define-public crate-carlo-0.9.0 (c (n "carlo") (v "0.9.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1wr4dk272gmwd45bbx6d9qjdhxxlfjvw0wi1gbqz4ndz47bpzg8d")))

(define-public crate-carlo-0.10.0 (c (n "carlo") (v "0.10.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "17612j1qflm3xan9chmhaq9ihc1l4pfrc76287dz9gywykj0ls0r")))

(define-public crate-carlo-0.11.0 (c (n "carlo") (v "0.11.0") (d (list (d (n "carlotk") (r "^0.1.0") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^0.1.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^0.1.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^0.1.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^0.1.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^0.1.0") (d #t) (k 0) (p "carlo-version")))) (h "0wndmpyjvwbykk35lf60dpp87fa2bw83y548k3cqj8vwca8cjns8")))

(define-public crate-carlo-0.12.0 (c (n "carlo") (v "0.12.0") (d (list (d (n "carlotk") (r "^0.12.0") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^0.2.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^0.2.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^0.2.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^0.2.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^0.2.0") (d #t) (k 0) (p "carlo-version")))) (h "1dqd17xj0xmwypq5ypv6l7y8az5hax7j4i8ql6dp6zsw8mrkhpdh")))

(define-public crate-carlo-0.12.1 (c (n "carlo") (v "0.12.1") (d (list (d (n "carlotk") (r "^0.12.1") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^0.2.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^0.2.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^0.2.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^0.2.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^0.2.0") (d #t) (k 0) (p "carlo-version")))) (h "0cncgs7hdib51k84bah2v4hsi40pl2cxp95chz0q804s62fzaxlp")))

(define-public crate-carlo-1.0.0 (c (n "carlo") (v "1.0.0") (d (list (d (n "carlotk") (r "^1.0.0") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^1.0.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^1.0.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^1.0.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^1.0.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^1.0.0") (d #t) (k 0) (p "carlo-version")))) (h "056cvvbmx6v4891wj91dkfipj3ndlvr3b304cnk2j39zkd2f6zyh")))

(define-public crate-carlo-1.0.1 (c (n "carlo") (v "1.0.1") (d (list (d (n "carlotk") (r "^1.0.1") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^1.0.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^1.0.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^1.0.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^1.0.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^1.0.0") (d #t) (k 0) (p "carlo-version")))) (h "1fhswdgcdq7jn5wwpg2by1szxvxp1nzbqamwcicr62zvb6gxjy0l")))

(define-public crate-carlo-1.1.0 (c (n "carlo") (v "1.1.0") (d (list (d (n "carlotk") (r "^1.0.1") (d #t) (k 0) (p "carlotk")) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "help") (r "^1.0.0") (d #t) (k 0) (p "carlo-help")) (d (n "latex") (r "^1.0.0") (d #t) (k 0) (p "carlo-latex")) (d (n "repl") (r "^1.0.0") (d #t) (k 0) (p "carlo-repl")) (d (n "run") (r "^1.0.0") (d #t) (k 0) (p "carlo-run")) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "version") (r "^1.0.0") (d #t) (k 0) (p "carlo-version")))) (h "0p4lv8bp17jrv39vigzp7mc4v65s0b723dfyc31j5ym4zihdd6p2")))

