(define-module (crates-io ca rl carlotk) #:use-module (crates-io))

(define-public crate-carlotk-0.1.0 (c (n "carlotk") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "06g5cs9y3lmw842rf5c6iaiq42l9wpkz9n2qs7gwx9p5jddg365j")))

(define-public crate-carlotk-0.12.0 (c (n "carlotk") (v "0.12.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1g18paip4qa1l8jsbf2l676s607w9if3wx1jsy95fnwlfkbbkffv")))

(define-public crate-carlotk-0.12.1 (c (n "carlotk") (v "0.12.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0q5cjyvfa11w5w78z6h2csja9kd9yqclpvv7yx06881fb9gnkk2p")))

(define-public crate-carlotk-1.0.0 (c (n "carlotk") (v "1.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "043n8q4qya03jga238fgys9bl8gxqwj2gx44nkkrak7rd88zgwyg")))

(define-public crate-carlotk-1.0.1 (c (n "carlotk") (v "1.0.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1q9zmhr087f6rfzhrqwb5072spygpscqaph2wrg3qgi71lh4hdyn")))

(define-public crate-carlotk-1.1.0 (c (n "carlotk") (v "1.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1ws500liv31rzaj2q4gbk67mplklgfc0n6rjaa642y6l061jgj8g")))

