(define-module (crates-io ca rl carla-ackermann) #:use-module (crates-io))

(define-public crate-carla-ackermann-0.1.0 (c (n "carla-ackermann") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "carla") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 2)) (d (n "noisy_float") (r "^0.2.0") (d #t) (k 0)) (d (n "pid") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hra15rg238z53xp06fw84z8vxmdlfa2j902v85bv38c20csqbay") (f (quote (("docs-only" "carla/docs-only"))))))

