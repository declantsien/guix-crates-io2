(define-module (crates-io ca rl carlo-help) #:use-module (crates-io))

(define-public crate-carlo-help-0.1.0 (c (n "carlo-help") (v "0.1.0") (d (list (d (n "carlotk") (r "^0.1.0") (d #t) (k 0)))) (h "01cf3iih0iq1aja4zcrk2sdgj632qv7a00n793n9fczbrbri8vih")))

(define-public crate-carlo-help-0.2.0 (c (n "carlo-help") (v "0.2.0") (d (list (d (n "carlotk") (r "^0.12.0") (d #t) (k 0)))) (h "0iq91rqyll0sn1azpwyx10af0afbyr1gdiv6iky1s4xny347hz5p")))

(define-public crate-carlo-help-1.0.0 (c (n "carlo-help") (v "1.0.0") (d (list (d (n "carlotk") (r "^1.0.0") (d #t) (k 0)))) (h "14gdmspfhrdbk0h46dj2xamav79iv5i1mpbh9hfzbm41kcb0z32f")))

