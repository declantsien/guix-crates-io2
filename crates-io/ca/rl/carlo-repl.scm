(define-module (crates-io ca rl carlo-repl) #:use-module (crates-io))

(define-public crate-carlo-repl-0.1.0 (c (n "carlo-repl") (v "0.1.0") (d (list (d (n "carlotk") (r "^0.1.0") (d #t) (k 0)))) (h "1vi4ar6s1w9x457zgg3psy3x0ldxhgq5rmy116i8w1adn4pb0jwn")))

(define-public crate-carlo-repl-0.2.0 (c (n "carlo-repl") (v "0.2.0") (d (list (d (n "carlotk") (r "^0.12.0") (d #t) (k 0)))) (h "15yd7y059b0rp99lmf05lm1g9zz2kc3d4lnsl14h6ssp692a6bba")))

(define-public crate-carlo-repl-1.0.0 (c (n "carlo-repl") (v "1.0.0") (d (list (d (n "carlotk") (r "^1.0.0") (d #t) (k 0)))) (h "0276ksjmnywfvm4gmp1nbnqc8y4viyg9c5l92bhw5g7bhs2kx93p")))

