(define-module (crates-io ca rl carlo-version) #:use-module (crates-io))

(define-public crate-carlo-version-0.1.0 (c (n "carlo-version") (v "0.1.0") (d (list (d (n "carlotk") (r "^0.1.0") (d #t) (k 0)))) (h "0z2i0nr3rf1kz6gqwax87rk6hs5a0hhwp743g3as26whcifim6ba")))

(define-public crate-carlo-version-0.2.0 (c (n "carlo-version") (v "0.2.0") (d (list (d (n "carlotk") (r "^0.12.0") (d #t) (k 0)))) (h "1xii2bhalv79i0sa3vw11x04gd7qily0yfxmd401jqyd7b1bx1px")))

(define-public crate-carlo-version-1.0.0 (c (n "carlo-version") (v "1.0.0") (d (list (d (n "carlotk") (r "^1.0.0") (d #t) (k 0)))) (h "00g9m6j7bpjx3am73srmjdp33xy0x79fk49gvzajx67jjms74mzn")))

