(define-module (crates-io ca sc casco) #:use-module (crates-io))

(define-public crate-casco-0.1.0 (c (n "casco") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)))) (h "1f3wcw9jz7iwg298z5ba1ii1nzza3mj2dp8g29hpcrl56v3vwhf9")))

(define-public crate-casco-0.2.0 (c (n "casco") (v "0.2.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "1p9wbg6mbgmgja3n14942f7k8k8kqxrnpdr817g02rj98d9mpysx")))

(define-public crate-casco-0.3.0 (c (n "casco") (v "0.3.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "0hcl4nhp4z28zhvfi4dhdq1hbin7rkkykdr1awgi3607mywlwm27")))

(define-public crate-casco-0.3.1 (c (n "casco") (v "0.3.1") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "1rh6dzfk1smlf6f4fa06c9j4bxgjp0jp4a2slvhz1c2xwhppl6mq")))

(define-public crate-casco-0.3.2 (c (n "casco") (v "0.3.2") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "1gp9shfv79za0irkf5413ys9q4295fglvdppf5xxc66c7njl55i2")))

(define-public crate-casco-0.4.0 (c (n "casco") (v "0.4.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "0gfaackfx668blwxkk5mkp83zgi91ymci74wabrvv0zixxwr8m1l") (f (quote (("lexer" "logos"))))))

