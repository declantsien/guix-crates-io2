(define-module (crates-io ca sc cascade) #:use-module (crates-io))

(define-public crate-cascade-0.1.0 (c (n "cascade") (v "0.1.0") (h "0dqig722mh9v7di154nlnnr2rmr3gjyrjqx389dnkzjv96y537k1")))

(define-public crate-cascade-0.1.1 (c (n "cascade") (v "0.1.1") (h "06fagv6d0b794fh3m7lk2dgai36s6jdlvq66q7jayd7whcypwnhs")))

(define-public crate-cascade-0.1.2 (c (n "cascade") (v "0.1.2") (h "08z6d0c8kb5ccnafy7hdncf9fpldkdy1v6hg2i3dnvcb82jblp2b")))

(define-public crate-cascade-0.1.3 (c (n "cascade") (v "0.1.3") (h "0v61y0g3b30rrxzj6jv335yhizdzjajspaa0kklwq3qm3jq0jq43")))

(define-public crate-cascade-0.1.4 (c (n "cascade") (v "0.1.4") (h "04r6rvx6aamr3shspjq0c10vabr91hdzh5q12cpginx9l7sdvj9i")))

(define-public crate-cascade-1.0.0 (c (n "cascade") (v "1.0.0") (h "08894qrlzkngqpsvvvg8d82v6x2p3gpzd5ngwi3xkqma3f96m37i")))

(define-public crate-cascade-1.0.1 (c (n "cascade") (v "1.0.1") (h "0lnb27ydwyh8a510g2nsfys3a2imnksra0vgw60xv17pvczb96fl")))

