(define-module (crates-io ca sc cascara) #:use-module (crates-io))

(define-public crate-cascara-0.1.0 (c (n "cascara") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0lk1xaigmmbqkvzisnr00i94r510cx0cwzvjz65inq4vgr0asw9d")))

(define-public crate-cascara-0.1.1 (c (n "cascara") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11df6fx8xkyrdvgrhdz78qkahycan2llkad7qqjhll8zrbwv57x1")))

