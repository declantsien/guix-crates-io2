(define-module (crates-io ca -r ca-rules) #:use-module (crates-io))

(define-public crate-ca-rules-0.1.0 (c (n "ca-rules") (v "0.1.0") (h "0bg9jskcv4fwyklfp4xjs9nv5padw5bd98yqgffz9fyzz7y4n444")))

(define-public crate-ca-rules-0.2.0 (c (n "ca-rules") (v "0.2.0") (h "09f91swvsmyq55sxfwf2g1wrdd9y9w3s59cana6fbmzqqskk6ikd")))

(define-public crate-ca-rules-0.3.0 (c (n "ca-rules") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)))) (h "1l5m328vyn0kiyiywpj4wy7i7w55qnhaq4sgqgjfgrbwarzxz6m3")))

(define-public crate-ca-rules-0.3.1 (c (n "ca-rules") (v "0.3.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)))) (h "0ixdb26apn559yv044j97948qz36jc2i90rdj68zb2pdpv9q58mj")))

(define-public crate-ca-rules-0.3.2 (c (n "ca-rules") (v "0.3.2") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)))) (h "12iyj2ivwrzm650gpksjhvinspiy7317spsy96zw7llsrrsfqihy")))

(define-public crate-ca-rules-0.3.3 (c (n "ca-rules") (v "0.3.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1sc2hsxhbp6iv0x201n606crimc2sdrag60l916309bv7skg4a97")))

(define-public crate-ca-rules-0.3.4 (c (n "ca-rules") (v "0.3.4") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17xqidfxqc4mnnpjb7jh4knjkc2ys0ilhm5igsmz175gx0ril2ki")))

(define-public crate-ca-rules-0.3.5 (c (n "ca-rules") (v "0.3.5") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0np5gfbwmw54birxrmk9zz2m7592zjnr4m0x0ddv8dx2rx0n85kd")))

