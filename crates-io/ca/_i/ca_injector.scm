(define-module (crates-io ca _i ca_injector) #:use-module (crates-io))

(define-public crate-ca_injector-0.1.0 (c (n "ca_injector") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "coyote") (r ">=0") (d #t) (k 2)) (d (n "env_logger") (r ">=0") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r ">=0") (d #t) (k 2)) (d (n "tempdir") (r ">=0") (d #t) (k 2)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0ww7ak4n357ncjk0010kd5p29aak7pgls8wmkf6hbhmqlpshm3f0")))

(define-public crate-ca_injector-0.1.1 (c (n "ca_injector") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "coyote") (r ">=0") (d #t) (k 2)) (d (n "env_logger") (r ">=0") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r ">=0") (d #t) (k 2)) (d (n "tempdir") (r ">=0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1v407wrnb67dv243aby4k48cw9k074f65h9xcrijpizpnz08ai7p")))

(define-public crate-ca_injector-0.1.2 (c (n "ca_injector") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "coyote") (r ">=0") (d #t) (k 2)) (d (n "env_logger") (r ">=0") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r ">=0") (d #t) (k 2)) (d (n "tempdir") (r ">=0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0z4l2wvbz0icpcbld9ay71r6ya9lbhhl1r1zzhg3vn5h61vpndd0")))

