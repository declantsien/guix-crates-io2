(define-module (crates-io ca pa capabilities) #:use-module (crates-io))

(define-public crate-capabilities-0.1.0 (c (n "capabilities") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "18j9ivpyc8ypgbg0nxlbnxmcilkdmf09fqzz4qjymqn2saw9zdhm") (y #t)))

(define-public crate-capabilities-0.2.0 (c (n "capabilities") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0vkl0p6s8szhszj3q3vhrkqbvv65li5yxp52wigkm6s1l0yvfxr0") (y #t)))

(define-public crate-capabilities-0.2.1 (c (n "capabilities") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0i57a35h5iycnjksdqddqxf6avadkyy3mnhrqxkynwsya8d4s50q") (y #t)))

(define-public crate-capabilities-0.3.0 (c (n "capabilities") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "13q2l48hgynpqk35g8ilgjb64ldajs6paq9353hqbjx2jwnhivla") (y #t)))

