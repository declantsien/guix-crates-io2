(define-module (crates-io ca mu camunda-worker) #:use-module (crates-io))

(define-public crate-camunda-worker-0.1.0 (c (n "camunda-worker") (v "0.1.0") (d (list (d (n "camunda-client") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qhp4lkqh234k5235v9j11r0zjv75lqky6clrsbr156vjwmnr0nd")))

