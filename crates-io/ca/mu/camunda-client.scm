(define-module (crates-io ca mu camunda-client) #:use-module (crates-io))

(define-public crate-camunda-client-0.1.0 (c (n "camunda-client") (v "0.1.0") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1077vcgan5vx8hqgapipi41c7sggik8dh4vav1xdrvwjynfl9acw")))

(define-public crate-camunda-client-0.1.1 (c (n "camunda-client") (v "0.1.1") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1fsmicw4fw7i6bx5khch6yww0yakbwyzrirgggx7bjxf12sv0r1f")))

(define-public crate-camunda-client-0.1.2 (c (n "camunda-client") (v "0.1.2") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1kayzmgiq0sg8ag96pjbbg45jkqs2hv9cs0p0w347rx201g64pcg")))

