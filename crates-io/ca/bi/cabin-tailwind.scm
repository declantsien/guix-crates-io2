(define-module (crates-io ca bi cabin-tailwind) #:use-module (crates-io))

(define-public crate-cabin-tailwind-0.2.0 (c (n "cabin-tailwind") (v "0.2.0") (d (list (d (n "cabin") (r "^0.2") (k 0)) (d (n "cabin-macros") (r "^0.2") (d #t) (k 0)) (d (n "insta") (r "^1.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (d #t) (k 0)))) (h "15019wl98cilzbz6m0hg16ah1dl6ifx20vifw6d9m7v587fp26nk") (f (quote (("preflight") ("forms") ("default" "preflight"))))))

