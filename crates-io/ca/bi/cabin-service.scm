(define-module (crates-io ca bi cabin-service) #:use-module (crates-io))

(define-public crate-cabin-service-0.2.0 (c (n "cabin-service") (v "0.2.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cabin") (r "^0.2") (k 0)) (d (n "cabin-tailwind") (r "^0.2") (o #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("time"))) (o #t) (k 0)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "15npsibs9v3pdh8qyj35nykayp1g6r0d31fda7g7v4n3rcx17x0c") (f (quote (("livereload" "tokio") ("default" "livereload" "cabin-tailwind"))))))

