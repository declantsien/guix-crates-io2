(define-module (crates-io ca bi cabinet) #:use-module (crates-io))

(define-public crate-cabinet-0.1.0 (c (n "cabinet") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "092v1d08a8g6dnl62a8b8z1xlvbjm2w5gwph3dwnn62gz4vld094") (y #t)))

(define-public crate-cabinet-0.2.0 (c (n "cabinet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "14kcxpw7hjx8n40dgh4scwg73n2h7afkmk8bglbakd83qm6b8s6z") (y #t)))

(define-public crate-cabinet-0.2.1 (c (n "cabinet") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1ahmxgi3bi5xrbjkp5bxqvdzdwzgwrkfqrzfqivs2bxi2h2plm0k") (y #t)))

(define-public crate-cabinet-0.3.0 (c (n "cabinet") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0hacvfwvfjw3py6370dfraj8hi6hh1qifwzbqwxdqrfzs5rz0ky8")))

(define-public crate-cabinet-0.4.0 (c (n "cabinet") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0y9847ilml6v75r7n5jvsdhbgv22sxm3ndvggyqg8vcf077dx5y9")))

(define-public crate-cabinet-0.5.0 (c (n "cabinet") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1ynifs85x944ch2jc8jvsc8mfcr6wj3gm61syhvpvnr26rjzc3s6") (y #t)))

(define-public crate-cabinet-0.5.1 (c (n "cabinet") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0iajnzzyn14sbwj2mvdmrw4mwfyffn2vk2kprka0nqx9qjd3wa94")))

(define-public crate-cabinet-0.6.0 (c (n "cabinet") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0srqi8ljdg4viji5fdyk4spd4lan9vkkakmapcairra4cad6sxz8") (y #t)))

(define-public crate-cabinet-0.6.1 (c (n "cabinet") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1x786d4wnra3xzl03g86fzczmflbaif4dq9340hi4zksz70ncwm1")))

