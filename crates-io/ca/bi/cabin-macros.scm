(define-module (crates-io ca bi cabin-macros) #:use-module (crates-io))

(define-public crate-cabin-macros-0.2.0 (c (n "cabin-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pk1qcia5vcb9s9yvk93q12a4b7aqlhnf9r7a2lakvbxc2slx3vz")))

