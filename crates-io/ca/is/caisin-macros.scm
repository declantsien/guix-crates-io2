(define-module (crates-io ca is caisin-macros) #:use-module (crates-io))

(define-public crate-caisin-macros-0.1.0 (c (n "caisin-macros") (v "0.1.0") (d (list (d (n "caisin") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "clone-impls" "parsing" "proc-macro" "printing" "extra-traits"))) (k 0)))) (h "1xk137vq0bmlzjk8yfwr37nghgr7nq3xnrj3ib3r3hf0bwcj6qwk")))

(define-public crate-caisin-macros-0.1.1 (c (n "caisin-macros") (v "0.1.1") (d (list (d (n "caisin") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "clone-impls" "parsing" "proc-macro" "printing" "extra-traits"))) (k 0)))) (h "0pdv5yq7zq080vjl4wlpqnybba3qh653x5wyrvznin8yy37wm3rw")))

