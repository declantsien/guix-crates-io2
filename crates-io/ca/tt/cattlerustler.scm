(define-module (crates-io ca tt cattlerustler) #:use-module (crates-io))

(define-public crate-cattlerustler-0.1.0 (c (n "cattlerustler") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.4") (d #t) (k 0)) (d (n "recurdates") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1y2hayh647ak1rk640m8xpdbbp20y462wlp9r1hnis4qqx8nnyys")))

(define-public crate-cattlerustler-0.1.1 (c (n "cattlerustler") (v "0.1.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.4") (d #t) (k 0)) (d (n "recurdates") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0mw5j5lg0w4pak5ipx45an0vvbrhciqn1j8xp9mx02l0cq2xn9i6")))

(define-public crate-cattlerustler-0.2.0 (c (n "cattlerustler") (v "0.2.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.4") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0k60hj8sbxfibwjrjr34wmq4wwj8bnrvia94cwlcbpx49n4bph7i")))

(define-public crate-cattlerustler-0.2.1 (c (n "cattlerustler") (v "0.2.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.4") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0630p6w6zw1ixfv0bm2jfdl092pyqdr2d299pl7svljjqx0pywnm")))

(define-public crate-cattlerustler-0.2.2 (c (n "cattlerustler") (v "0.2.2") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.4") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1qkpy0lydm9g5wig2s9iqvgyyx2g3wvdg3a7hm5x1mm0np8wyr2f")))

