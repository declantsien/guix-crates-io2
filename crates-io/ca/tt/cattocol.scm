(define-module (crates-io ca tt cattocol) #:use-module (crates-io))

(define-public crate-cattocol-0.1.0 (c (n "cattocol") (v "0.1.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "1bpk7524svagmwxxn67vnsyysyzrf887haz4nlbcp4lrshz40pn8")))

(define-public crate-cattocol-0.2.0 (c (n "cattocol") (v "0.2.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "1cay811k3xqz13rdas35cjdwrw59gj85fb271sx2xybjls0swgzh")))

(define-public crate-cattocol-0.3.0 (c (n "cattocol") (v "0.3.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "15ajd2bhl6n1j1cfzv6vx408jkn4ms979mz26x3qjimig4g42kzg")))

(define-public crate-cattocol-0.3.1 (c (n "cattocol") (v "0.3.1") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "1z42mvx36nyyfm4pjrcd7w32b5akamc8bgkby8nma7qbfkhv1zsc")))

