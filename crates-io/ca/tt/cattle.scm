(define-module (crates-io ca tt cattle) #:use-module (crates-io))

(define-public crate-cattle-0.1.0 (c (n "cattle") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive" "chrono"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdbiw023yy92jm8nrldcz3fmkfnqnzrsik62m2i71lbyjgbpsdw")))

