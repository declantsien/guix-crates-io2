(define-module (crates-io ca tt catt-core) #:use-module (crates-io))

(define-public crate-catt-core-0.1.0 (c (n "catt-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1fkfwdaqb3nmsx1nih3q258j6qxdm4fh9axfpabf8yws4ycb1pi5")))

