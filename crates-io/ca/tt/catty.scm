(define-module (crates-io ca tt catty) #:use-module (crates-io))

(define-public crate-catty-0.1.0 (c (n "catty") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spinning_top") (r "^0.2.2") (d #t) (k 0)))) (h "0mi44891qpcnbm6i8fkvzy6qhkf9h2b2z498znhif3wi0hhd2fmp") (f (quote (("nightly" "spinning_top/nightly")))) (y #t)))

(define-public crate-catty-0.1.1 (c (n "catty") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spinning_top") (r "^0.2.2") (d #t) (k 0)))) (h "18jh1gp84fi4rw8aj2zyjj9ljm3bcs51wgy8jzybvk33xgj0wn1f") (f (quote (("nightly" "spinning_top/nightly")))) (y #t)))

(define-public crate-catty-0.1.2 (c (n "catty") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spinning_top") (r "^0.2.2") (d #t) (k 0)))) (h "04hcx0l0hlj3s5020ilx8p2w9y5lxvhmql1w62hrg3h7170mv2wc") (f (quote (("nightly" "spinning_top/nightly")))) (y #t)))

(define-public crate-catty-0.1.3 (c (n "catty") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0gnss32yr44g7mw2gxs740wddr1nymxsg2wgx6wrqcv6d8jl5755") (y #t)))

(define-public crate-catty-0.1.4 (c (n "catty") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "06z6jdwxhrsi0pc82hj8jka8bkjgqiw135nc9rhlqba4x5cij8vx")))

(define-public crate-catty-0.1.5 (c (n "catty") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.3") (f (quote ("spin_mutex"))) (k 0)))) (h "1g2w5aw350cixicz8jgz19nsr5r4whkwip7qf9b981hwrjrsvw6w")))

