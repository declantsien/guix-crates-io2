(define-module (crates-io ca mp campmaster-constantine) #:use-module (crates-io))

(define-public crate-campmaster-constantine-0.1.0 (c (n "campmaster-constantine") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serenity") (r "^0.8.6") (d #t) (k 0)))) (h "07l5iv7xcg9fvfjpdssz261ndbr1ndfsk0kwvfq19ba4c7bg74km")))

(define-public crate-campmaster-constantine-0.1.1 (c (n "campmaster-constantine") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "serenity") (r "^0.8.6") (d #t) (k 0)))) (h "1d5iyh6kika9s6jgx7m7arqvjkgjj93wkvh37p9myw6dagqvavzf")))

(define-public crate-campmaster-constantine-0.2.1 (c (n "campmaster-constantine") (v "0.2.1") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serenity") (r "^0.8.6") (d #t) (k 0)))) (h "0cb51j35xggpahr51chvxrg7q9sfhz0haxlbrhdz8splh5m8hahl")))

