(define-module (crates-io ca lf calfsay) #:use-module (crates-io))

(define-public crate-calfsay-0.1.0 (c (n "calfsay") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1559appg10dsb633iwlczpr1wiwzilcq1b30ax7b3ifh79dvlf05")))

(define-public crate-calfsay-0.1.1 (c (n "calfsay") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0lsq8w7hxapah3ckh8faf7y576b069bsxnglx2gjpxalzaqfdlag")))

(define-public crate-calfsay-0.1.2 (c (n "calfsay") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0bfsxxkkk2ngzh6frkdpf7aqlzjadq4qnmpbxys98q1ldap9fwkp")))

(define-public crate-calfsay-0.1.3 (c (n "calfsay") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0zhafqsbh1s7cyv7naq019ff19i0rs36vn4wmfawvh39qqjpbpaz")))

