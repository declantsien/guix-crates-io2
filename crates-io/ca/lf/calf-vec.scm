(define-module (crates-io ca lf calf-vec) #:use-module (crates-io))

(define-public crate-calf-vec-0.1.0-alpha (c (n "calf-vec") (v "0.1.0-alpha") (h "0q13crsklpg0vpz8f7dpg00na4gvqgf072dsnyd9xi58ivqx0wah")))

(define-public crate-calf-vec-0.2.0-alpha (c (n "calf-vec") (v "0.2.0-alpha") (h "0rbiwaqgznjdf3pjlbmcsnak9byyf314nj5q70ccallq4nlnyr2d")))

(define-public crate-calf-vec-0.3.0-beta (c (n "calf-vec") (v "0.3.0-beta") (h "10nwh3bqzvpcm7b5b3ly57byizw7phywl1y3p6j2q5lyba20gixa")))

(define-public crate-calf-vec-0.3.1-beta (c (n "calf-vec") (v "0.3.1-beta") (h "1bp924977j3k8852w4an6p6vgmd6xzcjic9pamdqcjm3z4b67r31")))

