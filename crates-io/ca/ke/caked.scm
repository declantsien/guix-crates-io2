(define-module (crates-io ca ke caked) #:use-module (crates-io))

(define-public crate-caked-0.0.1 (c (n "caked") (v "0.0.1") (h "1k89nszmnbibinvfww8ipgc8gb1vf7xg0gbzmpsdziigrbrjfjh2")))

(define-public crate-caked-0.1.0 (c (n "caked") (v "0.1.0") (d (list (d (n "observitor") (r "~1") (d #t) (k 0)))) (h "1z49y3vxsa1py3p6r13ssgfz00vykzi4cx7014v0a9327c962cj5")))

