(define-module (crates-io ca ke cakecutter) #:use-module (crates-io))

(define-public crate-cakecutter-0.1.0 (c (n "cakecutter") (v "0.1.0") (d (list (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "ureq") (r "^0.1.1") (d #t) (k 0)))) (h "1831g6831xp9v23ksrq846iv5fibq6q9s3b2cq616kp7mriq70yq")))

