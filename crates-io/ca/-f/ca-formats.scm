(define-module (crates-io ca -f ca-formats) #:use-module (crates-io))

(define-public crate-ca-formats-0.1.0 (c (n "ca-formats") (v "0.1.0") (h "0jmg09mb2wgcma2bq07qrng1v9rj6h2yvvhiwff5flr69gg933np")))

(define-public crate-ca-formats-0.1.1 (c (n "ca-formats") (v "0.1.1") (h "0vfb3pp7mqj92rwapp9145jjnadr43fizxds9rm1mi8wjzqlgcdi")))

(define-public crate-ca-formats-0.1.2 (c (n "ca-formats") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)))) (h "15gii5qqqzfl2rh8as99d4n33ij47rr48myi9if1jg6s6jhmdg62")))

(define-public crate-ca-formats-0.1.3 (c (n "ca-formats") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1nhraanhd07d77pf1yrs3dcci719j1k1r0qm9l2yrkvj65g053h2")))

(define-public crate-ca-formats-0.2.0 (c (n "ca-formats") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0c07r9km2nxvsrq6vpfgiba8jnhn8m8alc7czf1kmvf5lh8ivx68")))

(define-public crate-ca-formats-0.2.1 (c (n "ca-formats") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0n0nhjv55xdsrqny05m6mgmip2fa71fckk7qs7bgdvh4m2ip91xb")))

(define-public crate-ca-formats-0.2.2 (c (n "ca-formats") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "04f67whbiln69qvxhqjhr75wlnjj8ihlf4k7f9vjn13dqqrpx0jw")))

(define-public crate-ca-formats-0.2.3 (c (n "ca-formats") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1rlzy91bbhh105pl2lmgq123zx9jyggj8v0kb7mvxf82m3r4lqpz")))

(define-public crate-ca-formats-0.2.4 (c (n "ca-formats") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hks5an11064gpjfdzldvbk6gzpqx4pgxvx3siy24q7k9c3mjkpl") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.0 (c (n "ca-formats") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0d5rd340i1zbl7a2j13bzxiln2csdhgvmvd4rqrqarc9qy6l8qlb") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.1 (c (n "ca-formats") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "07ws7qphrrcjgji4xfl3w9m8di29498rnzkhwafgmaj8xx7nd58q") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.2 (c (n "ca-formats") (v "0.3.2") (d (list (d (n "lazy-regex") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1fnqix02zqvr814m56sbgxyiv3a9hww82wviwwcj91427dgl4lv4") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.3 (c (n "ca-formats") (v "0.3.3") (d (list (d (n "displaydoc") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0hgfpzcfagp5bjyi80c6cn2qg7vm6f2gadd5i7fl48152hj65ysc") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.4 (c (n "ca-formats") (v "0.3.4") (d (list (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0rbhx6g6nw9xg8fhryd1wml8b593qjrr836k0z81xh8dvxsbmy23") (f (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3.5 (c (n "ca-formats") (v "0.3.5") (d (list (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rz43sx4yc61d23rrlqvgmmyixkn3pfmr4p1lm6d0w8bwww94gpx") (f (quote (("unknown") ("default"))))))

