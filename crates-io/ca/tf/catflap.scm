(define-module (crates-io ca tf catflap) #:use-module (crates-io))

(define-public crate-catflap-1.0.0 (c (n "catflap") (v "1.0.0") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)))) (h "1l9wzizad5h0v26npkjzv0kg3wakfdsl0cmmk6wj2a9b59vr7lw4")))

(define-public crate-catflap-1.1.0 (c (n "catflap") (v "1.1.0") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)))) (h "0ns47qsyp62kc2h6c4458qbjjmx5fwic9n7pwmlkzw78lvac7vf4")))

(define-public crate-catflap-1.2.0 (c (n "catflap") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "112f3gg40v50jwjwx7q3lfbgz3wq2z27rr477y16smzkp6vhgcqf")))

