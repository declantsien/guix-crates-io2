(define-module (crates-io ca tf catfact-rs) #:use-module (crates-io))

(define-public crate-catfact-rs-1.0.0 (c (n "catfact-rs") (v "1.0.0") (d (list (d (n "minreq") (r "^2.2") (f (quote ("https"))) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "1dd552pkxppip0v9bx9jr6zxlm76ibppzhscr5sgrjqdbfvwln85") (f (quote (("default" "catfact_ninja") ("catfact_ninja") ("catfact_heroku")))) (y #t)))

