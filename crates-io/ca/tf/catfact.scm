(define-module (crates-io ca tf catfact) #:use-module (crates-io))

(define-public crate-catfact-1.0.0 (c (n "catfact") (v "1.0.0") (d (list (d (n "minreq") (r "^2.2") (f (quote ("https"))) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "0dyzrx91aany17bpaivaq1dl46ilmd81fwxn7rkni1hx6q4vh4nx") (f (quote (("default" "catfact_ninja") ("catfact_ninja") ("catfact_heroku"))))))

