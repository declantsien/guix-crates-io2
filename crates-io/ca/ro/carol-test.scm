(define-module (crates-io ca ro carol-test) #:use-module (crates-io))

(define-public crate-carol-test-0.1.0 (c (n "carol-test") (v "0.1.0") (h "11zl1lc5l64n8rd35si71iv8dcljncri8qqmcry43zdx2k7lvjci")))

(define-public crate-carol-test-0.1.2 (c (n "carol-test") (v "0.1.2") (h "0zhcyyx8syqamh4y0pi2mc40f8wd7rpdi40zi7kxj1fb01xjdqmg")))

(define-public crate-carol-test-0.1.3 (c (n "carol-test") (v "0.1.3") (h "1s17jjks6kixq6d5673ycxwyx649a0rd0sgizxgvn1xlc6m4mgf1")))

(define-public crate-carol-test-0.6.8 (c (n "carol-test") (v "0.6.8") (h "1m5mq863qgy00wb0wswh6mayi35k58dfsn49bha0ck4357qm2c4a") (f (quote (("32-column-tables"))))))

(define-public crate-carol-test-0.8.0 (c (n "carol-test") (v "0.8.0") (h "18cv8dz7622hg4xpyk4211dqm5wk6mbnm9wir6mdz7fp7m29zmsz") (f (quote (("32-column-tables")))) (l "a")))

(define-public crate-carol-test-0.1.4 (c (n "carol-test") (v "0.1.4") (h "0f5w4sxgwnimp5gn7axmb970i13gdx0fm5isbiq4i2g15c7dn09g")))

(define-public crate-carol-test-1.0.1 (c (n "carol-test") (v "1.0.1") (h "0acs7zc6jy2q325cjh9vfz2bs66zjs7qnxz805aw6yyr3jdlxgw8")))

(define-public crate-carol-test-1.0.2 (c (n "carol-test") (v "1.0.2") (h "087kdxamvvvqnkvdb3rhxh5mmvklv3vr0raf4fmbym48zdrd023g")))

(define-public crate-carol-test-1.0.3 (c (n "carol-test") (v "1.0.3") (h "1i95l8kq1ihwqzcgygh36lm0s5wi9468q1zk5mp11ia6842cya79")))

(define-public crate-carol-test-1.0.4 (c (n "carol-test") (v "1.0.4") (h "006jbgiv97kml02iyjpbd4aqx00ln7vkf9qjkwc2s4m0m07d0k5l")))

(define-public crate-carol-test-1.0.5 (c (n "carol-test") (v "1.0.5") (h "166q8ffb28vi62yi6ckk6i7zv49pkv23fbxjgjhyan00wzix0w94")))

(define-public crate-carol-test-1.0.6 (c (n "carol-test") (v "1.0.6") (h "0k25hay5fpdnyj0dpxkh20f2kqrdrfssbjyjsbrf6ayzc3a15pa7")))

(define-public crate-carol-test-0.0.1 (c (n "carol-test") (v "0.0.1") (h "1vhwv9rzfcnm5v2m1zgw74jhb9yqji00yrzwll6f8wmcspx7yihr")))

(define-public crate-carol-test-1.0.7 (c (n "carol-test") (v "1.0.7") (h "1i7xap4kww5z0iq95f3zcxg87bnm2v38k3cgbkzz3i0khkzahnhp")))

(define-public crate-carol-test-1.0.35 (c (n "carol-test") (v "1.0.35") (h "0yc6wdbvxk68diw37f6bkxn4vavybyhy191vnv6dqki71sq2q452")))

(define-public crate-carol-test-0.0.0 (c (n "carol-test") (v "0.0.0") (h "0mm8jhfm85qy2hg4v0f3aa511kbcah65plkfln895zhlnlan6if8")))

(define-public crate-carol-test-1.1.0 (c (n "carol-test") (v "1.1.0") (h "16zkchkq5aap9rz8sm52lv2633v5f0fab2g2g2azlzd3dbf8lqc6")))

(define-public crate-carol-test-1.1.1 (c (n "carol-test") (v "1.1.1") (h "0faw6v4mjihnwwc571hg8iqqa8cwj6vd3i7whqz35cgx0nwb31ms")))

(define-public crate-carol-test-2.0.0 (c (n "carol-test") (v "2.0.0") (h "1ks525ij94fgqh0av4ghlryzy82d75pyzpg58n02kqh67mhm9dnh")))

(define-public crate-carol-test-1.1.2 (c (n "carol-test") (v "1.1.2") (d (list (d (n "carol-test") (r "^2") (d #t) (k 0)))) (h "11nk24hrkappfhb49r729xifi5bml05mbm4lw6szs9204xa8gkm3")))

(define-public crate-carol-test-2.1.0 (c (n "carol-test") (v "2.1.0") (h "15454rp6n9yn13n36gr7pv38z881vka47jxnb095lhb97bhffirj")))

(define-public crate-carol-test-2.2.0 (c (n "carol-test") (v "2.2.0") (h "1frf56klmvjypn31fzk2xr5299mwj3m2256lkqmsckk9i3qj1nvy")))

(define-public crate-carol-test-2.2.1 (c (n "carol-test") (v "2.2.1") (h "03sjq2ir6c4k2bwv2m53zajj5g250wyqx4rcm52hdqdw79dn9n21")))

(define-public crate-carol-test-2.2.2 (c (n "carol-test") (v "2.2.2") (h "1rb0k39n984amvc7rr8f6lx1yrdjfqv6vw1il33ciba7ls5znx8l")))

(define-public crate-carol-test-2.3.0 (c (n "carol-test") (v "2.3.0") (h "1i05bbihlsdx58afghi91q77glbjvwlbdkcxcmpn33mq59xl778b")))

