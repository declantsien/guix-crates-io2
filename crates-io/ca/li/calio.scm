(define-module (crates-io ca li calio) #:use-module (crates-io))

(define-public crate-calio-1.0.0 (c (n "calio") (v "1.0.0") (d (list (d (n "almanac") (r "^0.4.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "ical") (r "^0.8.0") (f (quote ("ical"))) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)))) (h "1v94im8wv5mhfhj7j3xniy1vf5xy1z7abb4n2ia7gkvixaqwisjc")))

(define-public crate-calio-1.1.0 (c (n "calio") (v "1.1.0") (d (list (d (n "almanac") (r "^0.4.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "ical") (r "^0.8.0") (f (quote ("ical"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "10jv3j2ixflnrbrzrrrdw4l1id6fsrfjy9mnfh140zph54r8d0c6")))

