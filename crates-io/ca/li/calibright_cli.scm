(define-module (crates-io ca li calibright_cli) #:use-module (crates-io))

(define-public crate-calibright_cli-0.1.0 (c (n "calibright_cli") (v "0.1.0") (d (list (d (n "calibright") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "03z6hnbsrfnzw694qdgskysmgbfh0v0jnbdcf5lvp1znb13748hb")))

(define-public crate-calibright_cli-0.1.1 (c (n "calibright_cli") (v "0.1.1") (d (list (d (n "calibright") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "1iwadhl99n043k6mhdng72j8zrf0zmwv6w7ciky77qw9qf1qmdmr")))

(define-public crate-calibright_cli-0.1.2 (c (n "calibright_cli") (v "0.1.2") (d (list (d (n "calibright") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "06dkk3gs7jq06byph36hxg6bdaixw72ykz41gl9iw76qfpn34690")))

