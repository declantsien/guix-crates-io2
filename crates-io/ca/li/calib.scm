(define-module (crates-io ca li calib) #:use-module (crates-io))

(define-public crate-calib-0.1.0 (c (n "calib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)))) (h "04c0j9qf8nj77igf9lm88yfbhsqwll1g90aqn24y7c8ffdwmnc9v")))

(define-public crate-calib-0.1.1 (c (n "calib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)))) (h "1bid4ll50dpg262wi4lf7ikv55var2rygk12870p423pmisxn7qs")))

