(define-module (crates-io ca li calibre-db) #:use-module (crates-io))

(define-public crate-calibre-db-0.1.0 (c (n "calibre-db") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)))) (h "0l1d0226ms01y099xg72sv51m3p29ricak15gpv9r75i9znr6xi5")))

(define-public crate-calibre-db-0.1.1 (c (n "calibre-db") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)))) (h "11lappdxkqxpcbm0qn1pkakrk0a491wcakgbqa97kjjyqyjr5j3l")))

