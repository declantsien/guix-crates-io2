(define-module (crates-io ca li calisp) #:use-module (crates-io))

(define-public crate-calisp-0.1.0 (c (n "calisp") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "1996y6j90kklal76lwxvayq2bqmss5ny21r3k42kna6m632mmfpk")))

(define-public crate-calisp-0.2.1 (c (n "calisp") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "0abqayw4pl11lksx288y3hvdrzkkbcsdigw1qv7zrpjabww32k74")))

(define-public crate-calisp-0.2.2 (c (n "calisp") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "02aqnndcacf0n0z25hclxbm2fal0qrciakkz5bdp87xqw57y87jp")))

