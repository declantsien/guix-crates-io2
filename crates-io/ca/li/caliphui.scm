(define-module (crates-io ca li caliphui) #:use-module (crates-io))

(define-public crate-caliphui-0.1.0 (c (n "caliphui") (v "0.1.0") (d (list (d (n "caliph") (r "^0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (f (quote ("persistence"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q53adb76hzjf2rsi35vvzqfnpyzd85h60k2hsw5fslfc3a6j169") (f (quote (("persistence" "eframe/persistence" "serde") ("default" "persistence"))))))

(define-public crate-caliphui-0.1.1 (c (n "caliphui") (v "0.1.1") (d (list (d (n "caliph") (r "^0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (f (quote ("persistence"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z1n6422yk24515is53j9wmf7asz47h1wpdfkay06v33jis2mk9f") (f (quote (("persistence" "eframe/persistence" "serde") ("default" "persistence"))))))

