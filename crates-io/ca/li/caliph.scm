(define-module (crates-io ca li caliph) #:use-module (crates-io))

(define-public crate-caliph-0.1.0 (c (n "caliph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "splines") (r "^4.0") (d #t) (k 0)))) (h "00vsjik0k32vm2ymzfx2q040mpcvsz4v7sab69fjs1fkljfwdirs")))

(define-public crate-caliph-0.1.1 (c (n "caliph") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "splines") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "022941136gy6j3yk5imbq8qc22sk0v0p3fafanr7haiidf844lyy")))

(define-public crate-caliph-0.1.2 (c (n "caliph") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "splines") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1iiaxfi3bmsm2bwm6sdpmwjqlfwibpschvgiwmn63mscinzdfawm")))

(define-public crate-caliph-0.1.3 (c (n "caliph") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "splines") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0nds273l6v1ral7z6l71hz6nish2b2332dv74b4pvgvg1k55994n")))

(define-public crate-caliph-0.1.4 (c (n "caliph") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "splines") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0qpxcv6qsbr86hghgzgn277if1v3msl72jn22q9lsf26c7599l23")))

