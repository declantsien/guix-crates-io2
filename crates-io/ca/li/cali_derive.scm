(define-module (crates-io ca li cali_derive) #:use-module (crates-io))

(define-public crate-cali_derive-0.2.0 (c (n "cali_derive") (v "0.2.0") (d (list (d (n "cali_core") (r "^0.2.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (f (quote ("default" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1w3lrllg5xx414alm55cb2c3ckacqinhnjk92a6ynwah4df5624d")))

