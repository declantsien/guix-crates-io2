(define-module (crates-io ca li calimero-sdk-macros) #:use-module (crates-io))

(define-public crate-calimero-sdk-macros-0.0.1 (c (n "calimero-sdk-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "152m0vi33n5mm5ss17fxb95w150s7h0mw15cvsyrcx08qkxqqqqk")))

