(define-module (crates-io ca l- cal-tea) #:use-module (crates-io))

(define-public crate-cal-tea-0.1.0 (c (n "cal-tea") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1xgbqihb18cfw7sj89j81wm70368m5qpngihlznhrz5m0gswh2qn")))

(define-public crate-cal-tea-0.1.1 (c (n "cal-tea") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0b50krhdkvsbw40i7bl1fs31i8h7aqq3kwnanbw9q8anap74wp6b")))

