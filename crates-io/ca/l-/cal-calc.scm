(define-module (crates-io ca l- cal-calc) #:use-module (crates-io))

(define-public crate-cal-calc-0.1.0 (c (n "cal-calc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)))) (h "1y3bp65cnal3s5dd0b5bvy1wvax2c6bbpfkndd1z2n4xfjk9rm25")))

(define-public crate-cal-calc-0.1.1 (c (n "cal-calc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)))) (h "1z7q6fq26zfkadr52yyp2i5k9f5w8b9lciiccxfgn3dp0p7sh5s5")))

(define-public crate-cal-calc-0.1.2 (c (n "cal-calc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)))) (h "0iiph0gwyjpkyk0rkpphhhf8vg9ayj8vnnixw3c40yp2y90q680i")))

(define-public crate-cal-calc-0.1.3 (c (n "cal-calc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)))) (h "0sz7qfkzbzx7c1pmgjw8r23ajis92l1ysi6nakbpb40l3x62sxvg")))

(define-public crate-cal-calc-0.2.0 (c (n "cal-calc") (v "0.2.0") (d (list (d (n "computus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "serde"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)))) (h "16820ixnq4dsbabw1vpg6ji462qdwgsfp56s1z87430hma4h8ifd")))

