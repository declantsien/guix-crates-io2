(define-module (crates-io ca in cain) #:use-module (crates-io))

(define-public crate-cain-0.0.1 (c (n "cain") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0yyy9aqs23rra9ndlmfvnrhdgvwxlgz62hn1pmbjx8wv2kcfg2g3")))

(define-public crate-cain-0.1.0 (c (n "cain") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14nxv30jm97lv5ga9fy13xa4dpbfxw06gwjm4ypzlf6j6knaiy7a")))

(define-public crate-cain-0.1.1 (c (n "cain") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ca5973ik5vpdgyavlbk6dfhxkxy0ab6shbv53irqcbsg6hf6s1a")))

(define-public crate-cain-0.1.2 (c (n "cain") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0n499jbiwly2m04q3jj6526z3il87xnddcqnwcjvv9ppal19j7k6")))

