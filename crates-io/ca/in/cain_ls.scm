(define-module (crates-io ca in cain_ls) #:use-module (crates-io))

(define-public crate-cain_ls-0.1.0 (c (n "cain_ls") (v "0.1.0") (h "1h42h2dcm187bnipq2326g0m96q7alxn92gy4rs0grp1k44vayg6")))

(define-public crate-cain_ls-0.1.1 (c (n "cain_ls") (v "0.1.1") (h "0jp0jsax54paifsjhjxbd03m7i6pccbh0mk5864grrmys4qq8va7")))

(define-public crate-cain_ls-0.1.2 (c (n "cain_ls") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0cqswfcbjcxp7iprj4jp0mlflb6abxflrxj2dzcy8rsq67pdnalj")))

(define-public crate-cain_ls-0.1.3 (c (n "cain_ls") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1lfsr82lr1fkwsb9qv3zac4y1wznd01p5b9ad7yylzzq7r8pairj")))

