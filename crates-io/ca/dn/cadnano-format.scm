(define-module (crates-io ca dn cadnano-format) #:use-module (crates-io))

(define-public crate-cadnano-format-0.1.0 (c (n "cadnano-format") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14l3pgrgb6hwik44dcv5nyi8cnhq7y1rm06mpw2isb68h4qc6bpa")))

