(define-module (crates-io ca ps capsule-ffi) #:use-module (crates-io))

(define-public crate-capsule-ffi-18.11.6 (c (n "capsule-ffi") (v "18.11.6") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0zfzfz9mldf6vsnj8sm2c37rq9nxwvkpvn6pi547yn3ak6s87rlx") (y #t) (l "dpdk")))

(define-public crate-capsule-ffi-0.1.1 (c (n "capsule-ffi") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "1pqnhkmzy3nz4ckd7ldrzjy1mgcfp6f56cby0aqvk7bb3yncn93f") (f (quote (("gen" "bindgen")))) (y #t) (l "dpdk")))

(define-public crate-capsule-ffi-0.1.2 (c (n "capsule-ffi") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0wm0jv5hd1c8ya26pijaajv8x6fckg2zzn81a0122k0qrzqnx51v") (f (quote (("rustdoc")))) (y #t) (l "dpdk")))

(define-public crate-capsule-ffi-0.1.3 (c (n "capsule-ffi") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0gzzb5vrxqz8kl3wb7rc4x1sxnh7z4dvah3qwmivqxsr6hp0x9dr") (f (quote (("rustdoc")))) (l "dpdk")))

(define-public crate-capsule-ffi-0.1.4 (c (n "capsule-ffi") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "1mn22pdiaw5dfq2y83sc1qhjnifhf9vlmbpzqshafzflh979l665") (f (quote (("rustdoc")))) (l "dpdk")))

(define-public crate-capsule-ffi-0.1.5 (c (n "capsule-ffi") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0abj195f9l01dzbai2dm6wgnysxzpvpkayncz0rffxyvmpzps4ag") (f (quote (("rustdoc")))) (l "dpdk")))

