(define-module (crates-io ca ps capstone-sys) #:use-module (crates-io))

(define-public crate-capstone-sys-0.1.0 (c (n "capstone-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06f01g6qs60y3k9qmiyraw5rcyiig036yg374yp1x9qn05l0dq9g") (f (quote (("default") ("build_src_cmake" "cmake" "build_src") ("build_src"))))))

(define-public crate-capstone-sys-0.2.0 (c (n "capstone-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0h0vba0mpigb0h6b9z7ybgska48lyyi0wm9grb9zziaywmm7rhyd") (f (quote (("default") ("build_src_cmake" "cmake" "build_src") ("build_src"))))))

(define-public crate-capstone-sys-0.3.0 (c (n "capstone-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fdgpv1g9nh5wl0vgxa6aainza5vqfvfjag6fy4hbv4lkixsja25") (f (quote (("use_system_capstone") ("use_bundled_capstone_bindings") ("default") ("build_capstone_cmake" "cmake")))) (y #t)))

(define-public crate-capstone-sys-0.2.1 (c (n "capstone-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ixpjgwgax0jlg8l8hyrwvzq06y4bpwn73vmsbfm0xsj96qjb0zz") (f (quote (("default") ("build_src_cmake" "cmake" "build_src") ("build_src")))) (y #t)))

(define-public crate-capstone-sys-0.4.0 (c (n "capstone-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rwlibcqvz1cklh2xq970g7xzmxirybs8g13gq1v3vvd4w62xx4k") (f (quote (("default") ("build_src_cmake" "cmake" "build_src") ("build_src"))))))

(define-public crate-capstone-sys-0.5.0 (c (n "capstone-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0ha7l1l7pr6xkwii7b3l90z8f3bc21vrfh02ppyn07w8w9vb541r") (f (quote (("use_system_capstone" "pkg-config") ("use_bindgen" "bindgen") ("default") ("build_capstone_cmake" "cmake"))))))

(define-public crate-capstone-sys-0.6.0 (c (n "capstone-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.30.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1vpnhwzyrr2qr4g78l5mscqizi94wyi83642xzm44qg3hprccfax") (f (quote (("use_system_capstone" "pkg-config") ("use_bindgen" "bindgen") ("default") ("build_capstone_cmake" "cmake"))))))

(define-public crate-capstone-sys-0.7.0 (c (n "capstone-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.35.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.28") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1dyc12dlfamhmn0psls8x3n527xid764541hh0gnv3n4kbnk14bm") (f (quote (("use_system_capstone" "pkg-config") ("use_bindgen" "bindgen") ("default") ("build_capstone_cmake" "cmake") ("build_capstone_cc" "cc"))))))

(define-public crate-capstone-sys-0.8.0 (c (n "capstone-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.35.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.28") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "12dfqh30a9wcfmqglk8kfip8x35m12yv5wgykv9by2p3hr1d6bxp") (f (quote (("use_system_capstone" "pkg-config") ("use_bindgen" "bindgen") ("default") ("build_capstone_cmake" "cmake") ("build_capstone_cc" "cc")))) (l "capstone")))

(define-public crate-capstone-sys-0.9.0 (c (n "capstone-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.37.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05xkpj9kzcx5d7cwazldcrn90synwccvb37jsy4zlxdwvmfk0zmi") (f (quote (("use_bindgen" "bindgen") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.9.1 (c (n "capstone-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.37.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "19nnmji8zw2phxklyraiim4r6zdhk0f43bqhvvyd1rn1qlmx7j1d") (f (quote (("use_bindgen" "bindgen") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.10.0 (c (n "capstone-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mxqcv2mr39yfswszgfvc07qszzzj5xc6ljrqfc4zql0rgfmxqps") (f (quote (("use_bindgen" "bindgen") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.11.0 (c (n "capstone-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.53.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "0ffpgs2lvl2m34y330bccd8zmawa371vyqinji3ki628gf4hggmf") (f (quote (("use_bindgen" "bindgen" "regex") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.12.0 (c (n "capstone-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.53.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "1i12p26ym9c5aq62f659ynsnssx19pi7f7bgvg1556c6690n5hl1") (f (quote (("use_bindgen" "bindgen" "regex") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.13.0 (c (n "capstone-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.53.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "1xpia1gs2b0zl7n521ldq6lax2jqqjw0hz2c8skak94gp2bksbyg") (f (quote (("use_bindgen" "bindgen" "regex") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.14.0 (c (n "capstone-sys") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "0kq8j55frbn9jjh50qm9r70sli3fv64n4an93kcv0d5ds0i3lrfz") (f (quote (("use_bindgen" "bindgen" "regex") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.15.0 (c (n "capstone-sys") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "0njvwa86jqcnjwz6d1gc53bkq02m4q1wiwh8qpka5hn4bqfnazrf") (f (quote (("use_bindgen" "bindgen" "regex") ("default")))) (l "capstone")))

(define-public crate-capstone-sys-0.16.0 (c (n "capstone-sys") (v "0.16.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.59") (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 1)))) (h "1qshi53z72yciyqskswyll6i9q40yjxf90347b3bgzqi2wkq6wgy") (f (quote (("use_bindgen" "bindgen" "regex") ("full") ("default" "full")))) (l "capstone")))

