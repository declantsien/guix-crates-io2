(define-module (crates-io ca ps capstone3) #:use-module (crates-io))

(define-public crate-capstone3-0.1.0 (c (n "capstone3") (v "0.1.0") (d (list (d (n "capstone-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hyrk5vzas8w74jdl5ig53dr3bxvr0m7afgwqh24l0ql5zhp2zsp") (f (quote (("default") ("build_capstone_src_cmake" "capstone-sys/build_src_cmake") ("build_capstone_src" "capstone-sys/build_src"))))))

