(define-module (crates-io ca ps capstan) #:use-module (crates-io))

(define-public crate-capstan-0.0.1 (c (n "capstan") (v "0.0.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "svg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17609dz9zvybjpj42zdfwf9rkwdh0yhk1m75lg6j0s9sccnk9d0g")))

(define-public crate-capstan-0.0.2 (c (n "capstan") (v "0.0.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "svg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qvrbcfbsw8in7v672nl0zqq5xk06fqxmwvdcj3n9phrhfgw9ah7")))

(define-public crate-capstan-0.0.3 (c (n "capstan") (v "0.0.3") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "svg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nyw07zq60jmbz9fs5k5bwr9yzrq8nfxnd0q5ffgcj79fnsab5fk")))

