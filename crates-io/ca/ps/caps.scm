(define-module (crates-io ca ps caps) #:use-module (crates-io))

(define-public crate-caps-0.0.1 (c (n "caps") (v "0.0.1") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z8wsk0xb4r9jdrgjypxkqvswijhb08828knhm63a7cd3sj8n8rc")))

(define-public crate-caps-0.0.2 (c (n "caps") (v "0.0.2") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.9") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1paardqy1swdayspqq10c6kwlrgwnl6qlli6y64lx29pac8k2rx9")))

(define-public crate-caps-0.1.0 (c (n "caps") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xilj5z085vp2hqjy98zg68k4fji871lr0inmmd26n8rw5vf4ffi")))

(define-public crate-caps-0.2.0 (c (n "caps") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hfs57zj13rdrccds6k6kf5vi0b4dx7d667mpwgpsqc5xliwg3v3")))

(define-public crate-caps-0.3.0 (c (n "caps") (v "0.3.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08ajs2y5nwlfls9ixzzpanrsjrly2ajw8rpp7dvb66l5926ajwcw")))

(define-public crate-caps-0.3.1 (c (n "caps") (v "0.3.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0crh3rr0zvrfzij91ajpmhrmw80v0xz3yk8ds3xcmhr95mfp4qgm")))

(define-public crate-caps-0.3.2 (c (n "caps") (v "0.3.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17y92kjvj6nldxjy5y0j6ay4fn2skjm1310208qq6x8wps1sy3fv")))

(define-public crate-caps-0.3.3 (c (n "caps") (v "0.3.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vplgzx8nifzr3f0l8ca77jqnz3fdymdg0ickacgdvawc44a3n90")))

(define-public crate-caps-0.3.4 (c (n "caps") (v "0.3.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "071xv18dxfvmifs3dpbmqy91cm55i9mh7w2zahg9yh3z3y566smz")))

(define-public crate-caps-0.4.0-alpha.1 (c (n "caps") (v "0.4.0-alpha.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ck290vs6n3w7bksvp0fcrlx1bx045g9m7r5qxwn6njvs3clqrln")))

(define-public crate-caps-0.4.0-alpha.2 (c (n "caps") (v "0.4.0-alpha.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kzn2yl9l5bs3glppxs503sa6s4w7qynvv4l46483icbxr99d8dy")))

(define-public crate-caps-0.4.0 (c (n "caps") (v "0.4.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m673q1306fwgkl00lr7kswdwa402kap74j468rlym1i03b45bbw")))

(define-public crate-caps-0.5.0 (c (n "caps") (v "0.5.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k7p63s6197m3hr5ps4ppl0vkxp50y7mrlbw9af120cyg4rkpc96")))

(define-public crate-caps-0.5.1 (c (n "caps") (v "0.5.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lffmsks66dz9dniba71fya5lzn9b8ri8357gn5gkcbzcnvgp4nh")))

(define-public crate-caps-0.5.2 (c (n "caps") (v "0.5.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h4m9s09plj85y5lb5j9hck36r8w63ifpc9s09mzi0zjvvfz5260")))

(define-public crate-caps-0.5.3 (c (n "caps") (v "0.5.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k75s2nqpkr25wa1siljmj2fx1dzzb6yy1gcd4kww16im88p5gv1") (f (quote (("serde_support" "serde"))))))

(define-public crate-caps-0.5.4 (c (n "caps") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hy1m58kz3yxh7bn8ymcz3f6ij2pc14lm3rvxwiadkpa1wc5134k") (f (quote (("serde_support" "serde"))))))

(define-public crate-caps-0.5.5 (c (n "caps") (v "0.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02vk0w48rncgvfmj2mz2kpzvdgc14z225451w7lvvkwvaansl2qr") (f (quote (("serde_support" "serde"))))))

