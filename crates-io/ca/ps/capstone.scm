(define-module (crates-io ca ps capstone) #:use-module (crates-io))

(define-public crate-capstone-0.0.1 (c (n "capstone") (v "0.0.1") (h "1ky66nv0v72zbsgablqfhqialr02ws6489g1ag7ff87rrcf8ipav")))

(define-public crate-capstone-0.0.3 (c (n "capstone") (v "0.0.3") (h "014mxhn9rvwb8v6ir62xza4l7gbfaif32v11sfda131pp2yilz22")))

(define-public crate-capstone-0.0.4 (c (n "capstone") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ixfj089d57c4wwmiwjqbz1mll1kn8k22p524ak6cqkv33cgjryz")))

(define-public crate-capstone-0.0.5 (c (n "capstone") (v "0.0.5") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)))) (h "1ad796qd6iys5mhgmg5s8r24g8fqsmfk26wjs3vq6c0w8d6vm4ll")))

(define-public crate-capstone-0.0.6 (c (n "capstone") (v "0.0.6") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "num") (r "^0.1.37") (d #t) (k 0)))) (h "01kgg39s0bbkqvslrsqkdf80bszbncdm5xn5i36mw9k87rryhv0k")))

(define-public crate-capstone-0.1.0 (c (n "capstone") (v "0.1.0") (d (list (d (n "capstone-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)))) (h "0pjlfxa50bkzb2d7paadr4cjm38qmx1rbdq89hcyq42zn47cah65") (f (quote (("use_system_capstone" "capstone-sys/use_system_capstone") ("use_bindgen" "capstone-sys/use_bindgen") ("default") ("build_capstone_cmake" "capstone-sys/build_capstone_cmake"))))))

(define-public crate-capstone-0.2.0 (c (n "capstone") (v "0.2.0") (d (list (d (n "capstone-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)))) (h "0255adr7d6x3rd9a26rqprfhpc282fx63mzsqq9xnlwaksnidv5s") (f (quote (("use_system_capstone" "capstone-sys/use_system_capstone") ("use_bindgen" "capstone-sys/use_bindgen") ("default") ("build_capstone_cmake" "capstone-sys/build_capstone_cmake"))))))

(define-public crate-capstone-0.3.0 (c (n "capstone") (v "0.3.0") (d (list (d (n "capstone-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)))) (h "07s2ph1mmffzp2kf6sgs2hqsq135sfcyh680q316n4yz7r55k1z6") (f (quote (("use_system_capstone" "capstone-sys/use_system_capstone") ("use_bindgen" "capstone-sys/use_bindgen") ("default") ("build_capstone_cmake" "capstone-sys/build_capstone_cmake") ("build_capstone_cc" "capstone-sys/build_capstone_cc"))))))

(define-public crate-capstone-0.3.1 (c (n "capstone") (v "0.3.1") (d (list (d (n "capstone-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)))) (h "07aqgg2dimkqlsc7vmpankqyx3ylbm34jyqpjphlby2dn4gdza41") (f (quote (("use_system_capstone" "capstone-sys/use_system_capstone") ("use_bindgen" "capstone-sys/use_bindgen") ("default") ("build_capstone_cmake" "capstone-sys/build_capstone_cmake") ("build_capstone_cc" "capstone-sys/build_capstone_cc"))))))

(define-public crate-capstone-0.4.0 (c (n "capstone") (v "0.4.0") (d (list (d (n "capstone-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "stderrlog") (r "^0.3.0") (d #t) (k 2)))) (h "13jhy56hbim4gbazw8fq9cf3pjqsawcrc230id0dj4nw8lnv7abc") (f (quote (("use_system_capstone" "capstone-sys/use_system_capstone") ("use_bindgen" "capstone-sys/use_bindgen") ("default") ("build_capstone_cmake" "capstone-sys/build_capstone_cmake") ("build_capstone_cc" "capstone-sys/build_capstone_cc") ("alloc_system"))))))

(define-public crate-capstone-0.5.0 (c (n "capstone") (v "0.5.0") (d (list (d (n "capstone-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.31") (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "stderrlog") (r "^0.4") (d #t) (k 2)))) (h "0zhm8xx5hlhz8dj66261w3gm2608zcrkcq147fwpiq507wh9vgh0") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default") ("alloc_system"))))))

(define-public crate-capstone-0.6.0 (c (n "capstone") (v "0.6.0") (d (list (d (n "capstone-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "~2.32") (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "stderrlog") (r "^0.4") (d #t) (k 2)))) (h "1cyvp3qkdridzvvb7is7055j11ri2m39d1gc6riis6hm74faa6q3") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default") ("alloc_system"))))))

(define-public crate-capstone-0.7.0 (c (n "capstone") (v "0.7.0") (d (list (d (n "capstone-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "1jcd7cmnq22xrs6cisp3q6gnycpblnsi8qlccydrm847g44py3pn") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default"))))))

(define-public crate-capstone-0.8.0 (c (n "capstone") (v "0.8.0") (d (list (d (n "capstone-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "1dindnf462z8bai2p1hv0b7dxc76891fbg59l9l9dq9qml6qkr2i") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default"))))))

(define-public crate-capstone-0.9.0 (c (n "capstone") (v "0.9.0") (d (list (d (n "capstone-sys") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "1v19wszrrhidh6z1ms0hda0dl4p0fl2n1mhx5mwkjfffnj03r2qp") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default"))))))

(define-public crate-capstone-0.10.0 (c (n "capstone") (v "0.10.0") (d (list (d (n "capstone-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "1ddpd6hhm1kl5rd9ddy891hl3i7ilpz05dpw4bzvcf9m9kqx3db6") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default"))))))

(define-public crate-capstone-0.11.0 (c (n "capstone") (v "0.11.0") (d (list (d (n "capstone-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "0.*") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "1n1hjy20nx59yzx51qbgc21vhhkpfmzmd5c2c2nkpbadb44fd5qh") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("default"))))))

(define-public crate-capstone-0.12.0 (c (n "capstone") (v "0.12.0") (d (list (d (n "capstone-sys") (r "^0.16.0") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "macho") (r "^0.4.1") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 2)))) (h "0v2vfzpibdbbabi7nzqrbxn2i5p0a7m8hbhcdchjnnjqv4wa935h") (f (quote (("use_bindgen" "capstone-sys/use_bindgen") ("std") ("full" "capstone-sys/full") ("default" "full" "std"))))))

