(define-module (crates-io ca ps capsule-macros) #:use-module (crates-io))

(define-public crate-capsule-macros-0.1.0 (c (n "capsule-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07bjhnhj7830vzjcq3wsf9fjjrsclwpnq4m8bnj623gfxx1z2fil") (y #t)))

(define-public crate-capsule-macros-0.1.1 (c (n "capsule-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gwvgyqs41glc34piygy99lrfqnc1h8g7j29y2idkcnrwzly7z6j") (y #t)))

(define-public crate-capsule-macros-0.1.2 (c (n "capsule-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1asiriif95fr84q2bziw6ibcqbn5x6w2z683n93l0znc00a9qs0c") (y #t)))

(define-public crate-capsule-macros-0.1.3 (c (n "capsule-macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "159d1j7rybsbj890vq8brxy7anbhvsx3z7mvhisz9qcv3ixly6r6")))

(define-public crate-capsule-macros-0.1.4 (c (n "capsule-macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0giikgryv680m2fiy3jhmmgwr5bdn8pc5lwsv6mzpzh27p6qabjj")))

(define-public crate-capsule-macros-0.1.5 (c (n "capsule-macros") (v "0.1.5") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gdiy16an9dhk2yln9sw70zn67cyqkc5dbspcfiagixbbfb0jibl")))

