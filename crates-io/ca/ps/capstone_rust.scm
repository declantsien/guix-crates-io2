(define-module (crates-io ca ps capstone_rust) #:use-module (crates-io))

(define-public crate-capstone_rust-0.1.0 (c (n "capstone_rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "139v2mk04hb2j9dr307waccwdxm51pzs0fn2c038hbfz776dw6zf")))

(define-public crate-capstone_rust-0.2.0 (c (n "capstone_rust") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zhdwh4xc057skl7bj2dbmwif5x6gz6ymraigl4j6fph5k29z9my")))

(define-public crate-capstone_rust-0.2.1 (c (n "capstone_rust") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17nxcn0b5lhjzwis36ikgih89328apc4p948vqq8j5642dpy129x")))

(define-public crate-capstone_rust-0.2.2 (c (n "capstone_rust") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.25.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0831x5rddcycxxad6yfvhy4dx5fwgpfq2a5grdbf4mrcr4k6kknh")))

(define-public crate-capstone_rust-0.2.3 (c (n "capstone_rust") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.25.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04wi41z8c3gj45sr701brgs1lg80w823gn8wra1i773q1nznsw58")))

