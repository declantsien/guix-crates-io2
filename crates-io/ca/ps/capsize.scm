(define-module (crates-io ca ps capsize) #:use-module (crates-io))

(define-public crate-capsize-0.1.0 (c (n "capsize") (v "0.1.0") (h "1kx0fasdf0bfj5jg6z7wkinrlc8y62zwgijncdlw6pbhik8x4cv3")))

(define-public crate-capsize-0.1.1 (c (n "capsize") (v "0.1.1") (h "0bl4p2klimz2fxq2pqzjgymgbqczgbfrip85zl5469hbwpk8692r")))

