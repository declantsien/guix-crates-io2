(define-module (crates-io ca st cast_trait) #:use-module (crates-io))

(define-public crate-cast_trait-0.1.0 (c (n "cast_trait") (v "0.1.0") (h "0s9ks4f3arkayffkkaz19hj6s0qm1gac0b2yx9k0gc76y7f1d4nv")))

(define-public crate-cast_trait-0.1.1 (c (n "cast_trait") (v "0.1.1") (h "03213g61z1nphh53zg90nsgz2f7mbxa6nb6f587iv1s36w35bn8w") (f (quote (("nightly"))))))

(define-public crate-cast_trait-0.1.2 (c (n "cast_trait") (v "0.1.2") (h "0a31mvh10lh4mzcy5mg22hz9s9qxda39fa6m9kvsvfknqj0xky64")))

