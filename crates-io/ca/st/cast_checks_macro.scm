(define-module (crates-io ca st cast_checks_macro) #:use-module (crates-io))

(define-public crate-cast_checks_macro-0.1.0 (c (n "cast_checks_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0s1r0blqajw3c1af81xnfa0aais8bbz7wwh1rcf5vbcwzixp643b") (f (quote (("release") ("default")))) (r "1.65")))

(define-public crate-cast_checks_macro-0.1.1 (c (n "cast_checks_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ffibr17b1l5mkc2rvh2zfxm1qd4s4v5vq8g779nga5jkhn8252c") (f (quote (("release") ("default")))) (r "1.65")))

(define-public crate-cast_checks_macro-0.1.2 (c (n "cast_checks_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0nji726ll3m6hwws72ly6k1cy3lksvny74cxz8j2r57wpxyz8aa8") (f (quote (("release") ("default")))) (r "1.65")))

(define-public crate-cast_checks_macro-0.1.3 (c (n "cast_checks_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1iamlcfs38xqmv7s0yv2y6wppch7660cz7rd0ikn3ij6yqfb713i") (f (quote (("release") ("default")))) (r "1.65")))

(define-public crate-cast_checks_macro-0.1.4 (c (n "cast_checks_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1zd86lar9rh310pz45bbwn80m8zxfqvfcy3fgzz79nh7hgbnxvx8") (f (quote (("release") ("default")))) (r "1.65")))

(define-public crate-cast_checks_macro-0.1.5 (c (n "cast_checks_macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1k14r8flxag66f8yfmfw0lbl2987hlja3fmjapg3v29h6wkgzr3n") (f (quote (("release") ("default")))) (r "1.65")))

