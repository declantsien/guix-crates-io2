(define-module (crates-io ca st cast_wd) #:use-module (crates-io))

(define-public crate-cast_wd-0.0.1 (c (n "cast_wd") (v "0.0.1") (h "1l665riny6lc68xf99z9fr38r3k4qv4li42b8k7y8hicnxy1lhwv")))

(define-public crate-cast_wd-0.0.2 (c (n "cast_wd") (v "0.0.2") (h "1c7bbnfd1g609rys8zlxmgr3p93rsjzmrcl1y501ly745hyrgb9b")))

(define-public crate-cast_wd-0.0.3 (c (n "cast_wd") (v "0.0.3") (h "0i0b381dq1ypcmnrgflx0zmq0cgn3gsdri077l1r2302vl9k8h04")))

(define-public crate-cast_wd-0.0.4 (c (n "cast_wd") (v "0.0.4") (h "0iy7sfl7hmb0b4h5blij0ba9dzrn5p5sqwbksw1fq4akwfjclpyn")))

(define-public crate-cast_wd-0.0.5 (c (n "cast_wd") (v "0.0.5") (h "042d22h2f8zpld7mhfww7q7s8n9m4gzjcy8mkg31pkb19yb9nav1")))

