(define-module (crates-io ca st cast_trait_object) #:use-module (crates-io))

(define-public crate-cast_trait_object-0.1.0 (c (n "cast_trait_object") (v "0.1.0") (d (list (d (n "cast_trait_object_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1w95p7qp06k2qmpa0j1vkqlxf8w3xjypppf9vlfpl0c5ai29cf9g") (f (quote (("proc-macros" "cast_trait_object_macros") ("full" "alloc" "proc-macros") ("docs" "full") ("default" "full") ("alloc"))))))

(define-public crate-cast_trait_object-0.1.1 (c (n "cast_trait_object") (v "0.1.1") (d (list (d (n "cast_trait_object_macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0wk9ypb8spysmrgazdblkzj8dgv10vs0h5rgjngid7b3z7bg0r6y") (f (quote (("proc-macros" "cast_trait_object_macros") ("full" "alloc" "proc-macros") ("docs" "full") ("default" "full") ("alloc"))))))

(define-public crate-cast_trait_object-0.1.2 (c (n "cast_trait_object") (v "0.1.2") (d (list (d (n "cast_trait_object_macros") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "169rdln9a16jkc2241v9y4cz56gc3ymkgb3l9w569wq7wm4yjqhk") (f (quote (("proc-macros" "cast_trait_object_macros") ("full" "alloc" "proc-macros") ("docs" "full") ("default" "full") ("alloc"))))))

(define-public crate-cast_trait_object-0.1.3 (c (n "cast_trait_object") (v "0.1.3") (d (list (d (n "cast_trait_object_macros") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "11cj5wq8dgjjrhkc7hhfyvkc2bzx63f87bzzydpbpc17zm52gdy7") (f (quote (("proc-macros" "cast_trait_object_macros") ("full" "alloc" "proc-macros") ("docs" "full") ("default" "full") ("alloc"))))))

