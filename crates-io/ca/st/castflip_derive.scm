(define-module (crates-io ca st castflip_derive) #:use-module (crates-io))

(define-public crate-castflip_derive-0.1.0 (c (n "castflip_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00hiwk4adsqyndrsb1yhlw73p4l9f3vy615kvix0pr7wz4bbg3wb")))

(define-public crate-castflip_derive-0.1.1 (c (n "castflip_derive") (v "0.1.1") (d (list (d (n "castflip") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cxz1wm44rk4xf9a0np8mi8bcddy4c3m20dd9zrawpyf3w5q30gi")))

(define-public crate-castflip_derive-0.1.3 (c (n "castflip_derive") (v "0.1.3") (d (list (d (n "castflip") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pfhplb84pr7752via3c0cbp1b1zpvb7jrlcyafgym6kr3xcladx")))

(define-public crate-castflip_derive-0.1.5 (c (n "castflip_derive") (v "0.1.5") (d (list (d (n "castflip") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l8hbmnjvvbwc86v81zfw9qvbjkszjyw55ksx6717m574ld1pq05")))

(define-public crate-castflip_derive-0.1.6 (c (n "castflip_derive") (v "0.1.6") (d (list (d (n "castflip") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "021zzk0lrxnw1h1rgbh6g4h35z91sjmfhgmngw8r40bcjpi6l9bg")))

(define-public crate-castflip_derive-0.1.10 (c (n "castflip_derive") (v "0.1.10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zlnx66fhibi98ccf7fbd70jn3vh71azx32qxwvy0cxblnkc6v4h")))

