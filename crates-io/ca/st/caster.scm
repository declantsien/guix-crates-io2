(define-module (crates-io ca st caster) #:use-module (crates-io))

(define-public crate-caster-0.0.1 (c (n "caster") (v "0.0.1") (h "14qg3n371b4qpr7lljkp9d86l87w8kxyrj837ircg10z85sa2c7g") (y #t)))

(define-public crate-caster-0.1.0 (c (n "caster") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3kg05lxfqg8z3528cmd1d433qhsbrl6rkdxibbbi4mrmd1g07r") (y #t)))

(define-public crate-caster-0.2.0 (c (n "caster") (v "0.2.0") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvs8andgvnx9x3hh0ls34zdhkq1ccbb4rxn43h7qgn38s83kzs8") (y #t)))

(define-public crate-caster-0.2.1 (c (n "caster") (v "0.2.1") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yww42ns462hbp2fvwbw12mq76kcc3z060jxsyrihi9pggqhg8pw") (y #t)))

(define-public crate-caster-0.2.2 (c (n "caster") (v "0.2.2") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vz3nh4vg5d4lh6nw6k921vn2ql5a0c2385vmk317d64758zwx2f") (y #t)))

(define-public crate-caster-0.2.3 (c (n "caster") (v "0.2.3") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.11") (f (quote ("util"))) (d #t) (k 0)))) (h "070l9s50ymjwfdz9qzrihj80k2mannahn00rlx14j3siwbp3bva2") (y #t)))

(define-public crate-caster-0.3.0 (c (n "caster") (v "0.3.0") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.11") (f (quote ("util"))) (d #t) (k 0)))) (h "0i0qlz1ksmyfhf3lih6d6xxw4fhd2f5yl5jffp8gs17lscqb7vbn")))

