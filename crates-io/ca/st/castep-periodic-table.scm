(define-module (crates-io ca st castep-periodic-table) #:use-module (crates-io))

(define-public crate-castep-periodic-table-0.1.0 (c (n "castep-periodic-table") (v "0.1.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)))) (h "18cvxj7vhpcr52kcjl2jrxh18h9k4rpzg3617jgq24ny6a1zq3wb")))

(define-public crate-castep-periodic-table-0.2.0 (c (n "castep-periodic-table") (v "0.2.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "06axpc63033h0kh6gpydwlglk79gsfajv4d6r70by43fi1mdfxs4")))

(define-public crate-castep-periodic-table-0.2.1 (c (n "castep-periodic-table") (v "0.2.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "12y3i8dpwfj7rgmdmjdbncdaimi5cmcr4lcqwq1956cmcsdhqqvg")))

(define-public crate-castep-periodic-table-0.2.2 (c (n "castep-periodic-table") (v "0.2.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0v2gflk76wd6d1wh2qijjf5qcpbz4pn6pp25vynxdl3f4pn9vj8y")))

(define-public crate-castep-periodic-table-0.3.0 (c (n "castep-periodic-table") (v "0.3.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "19slbsm4h1k8b2gvsh7ylc01dkyc8n56dmqh6rqilvvpkz0n61xi")))

(define-public crate-castep-periodic-table-0.3.1 (c (n "castep-periodic-table") (v "0.3.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "14l9b23cafn1x97a9a7wl9mh30g0hdaggvn23xn5wq6qcfmmjlca")))

(define-public crate-castep-periodic-table-0.3.2 (c (n "castep-periodic-table") (v "0.3.2") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "15khskpizb0bhx8ykffx88fq7x9cf34pf1mkvc6mbvlx67r65jn7")))

(define-public crate-castep-periodic-table-0.4.0 (c (n "castep-periodic-table") (v "0.4.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "17zc8zdx0d747lza4azgkxsy4mhq2dxz55za7yi04i1v7isxgqi4")))

