(define-module (crates-io ca st castle_shared_parser) #:use-module (crates-io))

(define-public crate-castle_shared_parser-0.3.2 (c (n "castle_shared_parser") (v "0.3.2") (d (list (d (n "castle_error") (r "^0.3.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.3.2") (d #t) (k 0)))) (h "0fik0fizbhl3bp5v4hakilipl96cl1m25xragsd5h1mair63shx2")))

(define-public crate-castle_shared_parser-0.4.0 (c (n "castle_shared_parser") (v "0.4.0") (d (list (d (n "castle_error") (r "^0.4.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.0") (d #t) (k 0)))) (h "1yav8n69zp3dw379x6kca2lczj31v1skl8pjahdapjjnfslc3s56")))

(define-public crate-castle_shared_parser-0.4.1 (c (n "castle_shared_parser") (v "0.4.1") (d (list (d (n "castle_error") (r "^0.4.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.1") (d #t) (k 0)))) (h "1ymk71h7a3krp51n727c0gqwgfshmfqicf1pxfz267xnxkwa964n")))

(define-public crate-castle_shared_parser-0.4.2 (c (n "castle_shared_parser") (v "0.4.2") (d (list (d (n "castle_error") (r "^0.4.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.2") (d #t) (k 0)))) (h "09n4jkwksqgfnlbrshf4703hmpgc3b4nlg2qg4zmdwsyaf6vc8xn")))

(define-public crate-castle_shared_parser-0.4.3 (c (n "castle_shared_parser") (v "0.4.3") (d (list (d (n "castle_error") (r "^0.4.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.3") (d #t) (k 0)))) (h "0jlajpi0plkqlxdx4qc1spa5i21xddzbihq8r5h86km6jza9aadc")))

(define-public crate-castle_shared_parser-0.4.4 (c (n "castle_shared_parser") (v "0.4.4") (d (list (d (n "castle_error") (r "^0.4.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.4") (d #t) (k 0)))) (h "0j52a72jbq0ch9980w83dq3vzgcc0hgb3hnh08s4kk0lqqyzzm4c")))

(define-public crate-castle_shared_parser-0.4.5 (c (n "castle_shared_parser") (v "0.4.5") (d (list (d (n "castle_error") (r "^0.4.5") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.5") (d #t) (k 0)))) (h "1x1gjhzp6619nd9fic7yaqzdzic2vwy5qphqdzk8gj7wknbczm19")))

(define-public crate-castle_shared_parser-0.4.6 (c (n "castle_shared_parser") (v "0.4.6") (d (list (d (n "castle_error") (r "^0.4.6") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.6") (d #t) (k 0)))) (h "0b4jbk61sirm243fjrcaga3nk37pgj7pnaz6zz0fmax1y809i3y9")))

(define-public crate-castle_shared_parser-0.5.0 (c (n "castle_shared_parser") (v "0.5.0") (d (list (d (n "castle_error") (r "^0.5.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.0") (d #t) (k 0)))) (h "1n9mk20wx1vhjjkxbjwxk1hkw2r7s5b4il1swcyvrm2jmvnxjh7s")))

(define-public crate-castle_shared_parser-0.5.1 (c (n "castle_shared_parser") (v "0.5.1") (d (list (d (n "castle_error") (r "^0.5.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.1") (d #t) (k 0)))) (h "1aw88aq1sz9m4chkr0hg6cmr9q8rk45s7qbmlwy6k5ldg831dgby")))

(define-public crate-castle_shared_parser-0.5.2 (c (n "castle_shared_parser") (v "0.5.2") (d (list (d (n "castle_error") (r "^0.5.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.2") (d #t) (k 0)))) (h "02xkl6068ilal5sivk2plz4n3ycfkkp3walk2bav04nfv1nflz7y")))

(define-public crate-castle_shared_parser-0.5.3 (c (n "castle_shared_parser") (v "0.5.3") (d (list (d (n "castle_error") (r "^0.5.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.3") (d #t) (k 0)))) (h "1mlw97654zjr89raj544d0pzfs5p41xvng2azrdxbkg8y4nkhaz2")))

(define-public crate-castle_shared_parser-0.5.4 (c (n "castle_shared_parser") (v "0.5.4") (d (list (d (n "castle_error") (r "^0.5.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.4") (d #t) (k 0)))) (h "1z6wxpjs3pg05f3216fjxvdxxq3bzriqd5wj4mkfyw5fvqf3lzjh")))

(define-public crate-castle_shared_parser-0.5.5 (c (n "castle_shared_parser") (v "0.5.5") (d (list (d (n "castle_error") (r "^0.5.5") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.5") (d #t) (k 0)))) (h "0sc0vlb97rcnhxi66mj6s3d6wrpcixm72s0f651s6ba8fgy4wfg5")))

(define-public crate-castle_shared_parser-0.5.6 (c (n "castle_shared_parser") (v "0.5.6") (d (list (d (n "castle_error") (r "^0.5.6") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.6") (d #t) (k 0)))) (h "07g3whnsg2i4ay7zxwl0lm37gcs61m2ry82d8dd6kz00gxj12cqn")))

(define-public crate-castle_shared_parser-0.5.7 (c (n "castle_shared_parser") (v "0.5.7") (d (list (d (n "castle_error") (r "^0.5.7") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.7") (d #t) (k 0)))) (h "0w8k6v145iqvcdckf69vkflp21qi4189wjk3rjz0pbnsvghh7gn8")))

(define-public crate-castle_shared_parser-0.5.8 (c (n "castle_shared_parser") (v "0.5.8") (d (list (d (n "castle_error") (r "^0.5.8") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.8") (d #t) (k 0)))) (h "0mz58h2wx6jza343lsgh5l9v5dhq1h77kirvhrdsqx19vw51yinm")))

(define-public crate-castle_shared_parser-0.5.9 (c (n "castle_shared_parser") (v "0.5.9") (d (list (d (n "castle_error") (r "^0.5.9") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.9") (d #t) (k 0)))) (h "05zs6x7a45l5lvc6yshfigac1aac458ysq2d9nzrs1vsq8libqaj")))

(define-public crate-castle_shared_parser-0.6.1 (c (n "castle_shared_parser") (v "0.6.1") (d (list (d (n "castle_tokenizer") (r "^0.6.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.1") (d #t) (k 0)))) (h "07a1i5dm5h0n6pllslv8pq6yv8mpbk0p9vyqvk3a0003dkkd0qq9")))

(define-public crate-castle_shared_parser-0.6.2 (c (n "castle_shared_parser") (v "0.6.2") (d (list (d (n "castle_tokenizer") (r "^0.6.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.2") (d #t) (k 0)))) (h "04y6dflvz0wphf5rcarpvxvhd050wjw41i67a8x6rix56vgnwz72")))

(define-public crate-castle_shared_parser-0.6.3 (c (n "castle_shared_parser") (v "0.6.3") (d (list (d (n "castle_tokenizer") (r "^0.6.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.3") (d #t) (k 0)))) (h "1fy9pqg7fm2j1licwz5wrhyglqmgh5cxwkq15zi728vnr7rnwp7d")))

(define-public crate-castle_shared_parser-0.6.4 (c (n "castle_shared_parser") (v "0.6.4") (d (list (d (n "castle_tokenizer") (r "^0.6.4") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.4") (d #t) (k 0)))) (h "0s2081vr2c9xpmwdr3m125hlc6jgdcbk0p2cqpj8qbl3a79cv02i")))

(define-public crate-castle_shared_parser-0.6.5 (c (n "castle_shared_parser") (v "0.6.5") (d (list (d (n "castle_tokenizer") (r "^0.6.5") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.5") (d #t) (k 0)))) (h "0796dkk1jz82zslxv9crd1jwpbzqn8bw6hjd15yi0322630xshzx")))

(define-public crate-castle_shared_parser-0.6.6 (c (n "castle_shared_parser") (v "0.6.6") (d (list (d (n "castle_tokenizer") (r "^0.6.6") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.6") (d #t) (k 0)))) (h "0vpzp8j2gm58qsg6npfby39f4wgibbr8scdq7z8858jqsj8jkcpr")))

(define-public crate-castle_shared_parser-0.7.0 (c (n "castle_shared_parser") (v "0.7.0") (d (list (d (n "castle_tokenizer") (r "^0.7.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.0") (d #t) (k 0)))) (h "06qrm8pj0nafd1g7hylvxh8wqbsqqmnnybn134x8s7q6nzlyjxm0")))

(define-public crate-castle_shared_parser-0.7.1 (c (n "castle_shared_parser") (v "0.7.1") (d (list (d (n "castle_tokenizer") (r "^0.7.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.1") (d #t) (k 0)))) (h "12py493bcf7c62sgp9dw4cmksa3qbg5cc4zlc8mjdsyw26rpmb13")))

(define-public crate-castle_shared_parser-0.7.2 (c (n "castle_shared_parser") (v "0.7.2") (d (list (d (n "castle_tokenizer") (r "^0.7.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.2") (d #t) (k 0)))) (h "1xwa6w0fqa3cpdv143mqmmfxcd9mlfkjc011qmg0i1ls2f7pjgam")))

(define-public crate-castle_shared_parser-0.8.0 (c (n "castle_shared_parser") (v "0.8.0") (d (list (d (n "castle_tokenizer") (r "^0.8.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.0") (d #t) (k 0)))) (h "0n1v9bw55wirxwzj821gkjvaj2a0w04223ixhzpiczb638wia4f8")))

(define-public crate-castle_shared_parser-0.8.1 (c (n "castle_shared_parser") (v "0.8.1") (d (list (d (n "castle_tokenizer") (r "^0.8.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.1") (d #t) (k 0)))) (h "0hfi5i92nciaxm6i18hxiq5q6nn97r7cnpifrj2vijfrjdhd1x42")))

(define-public crate-castle_shared_parser-0.8.2 (c (n "castle_shared_parser") (v "0.8.2") (d (list (d (n "castle_tokenizer") (r "^0.8.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.2") (d #t) (k 0)))) (h "0n7q7m9yzmbxph06an26kj7ryj6yxvap6pnai12i02ndvcfm1yzm")))

(define-public crate-castle_shared_parser-0.8.3 (c (n "castle_shared_parser") (v "0.8.3") (d (list (d (n "castle_tokenizer") (r "^0.8.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.3") (d #t) (k 0)))) (h "0x4b9sqqldns5jwnw3l18psa68jlhhhxiqdgnbz5pnljs3lvp0q6")))

(define-public crate-castle_shared_parser-0.9.0 (c (n "castle_shared_parser") (v "0.9.0") (d (list (d (n "castle_tokenizer") (r "^0.9.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.0") (d #t) (k 0)))) (h "0jg9fxq9cg98s9xzi0k6amrmjgixrq2wbil0ka9z038px3qdsfx1")))

(define-public crate-castle_shared_parser-0.9.1 (c (n "castle_shared_parser") (v "0.9.1") (d (list (d (n "castle_tokenizer") (r "^0.9.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.1") (d #t) (k 0)))) (h "1zx2lhd7vmhvjnkfbhzn6z3cinl7bfrvmgzwc90r296y9j3mpkwx")))

(define-public crate-castle_shared_parser-0.9.2 (c (n "castle_shared_parser") (v "0.9.2") (d (list (d (n "castle_tokenizer") (r "^0.9.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.2") (d #t) (k 0)))) (h "1v7sjxxxnyh35bcpv1p14m9h45xjnc1w4p6andki0w4n8wbr1ccq")))

(define-public crate-castle_shared_parser-0.9.3 (c (n "castle_shared_parser") (v "0.9.3") (d (list (d (n "castle_tokenizer") (r "^0.9.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.3") (d #t) (k 0)))) (h "1pphk0a1bzhlfw1v0k97bh4nywj4lgbfww3l3mjl2f606jdzsnlh")))

(define-public crate-castle_shared_parser-0.9.4 (c (n "castle_shared_parser") (v "0.9.4") (d (list (d (n "castle_tokenizer") (r "^0.9.4") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.4") (d #t) (k 0)))) (h "0q5bvh3s53q5fz0iapzy76jn2fkfs5y4qdy0y1ncslyg4r9dz2g3")))

(define-public crate-castle_shared_parser-0.10.0 (c (n "castle_shared_parser") (v "0.10.0") (d (list (d (n "castle_tokenizer") (r "^0.10.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.10.0") (d #t) (k 0)))) (h "0a0jbjm3sr0i5vihpslshj6bajnqpzb4lf0x4r5v8j3yd7kmzysg")))

(define-public crate-castle_shared_parser-0.10.1 (c (n "castle_shared_parser") (v "0.10.1") (d (list (d (n "castle_tokenizer") (r "^0.10.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.10.1") (d #t) (k 0)))) (h "0gk2i1vapq878l896a9ssmqnwly2hmjp2475abqf4wla1y4j12k2")))

(define-public crate-castle_shared_parser-0.12.0 (c (n "castle_shared_parser") (v "0.12.0") (d (list (d (n "castle_tokenizer") (r "^0.12.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.12.0") (d #t) (k 0)))) (h "1xgjpcygsk8fscaxgvkdr1vixbahjvhylan734c9slzfj3s1ckls")))

(define-public crate-castle_shared_parser-0.13.0 (c (n "castle_shared_parser") (v "0.13.0") (d (list (d (n "castle_tokenizer") (r "^0.13.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.13.0") (d #t) (k 0)))) (h "1w0vxxcid87nmjdayydlsm5biwjaykqcv655x0m3103ar4vi5y2d")))

(define-public crate-castle_shared_parser-0.13.1 (c (n "castle_shared_parser") (v "0.13.1") (d (list (d (n "castle_tokenizer") (r "^0.13.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.13.1") (d #t) (k 0)))) (h "07ij22zj5f4b1q7iqa13c2qq5fm0av2j6iynxpjlpch1pznk4m0b")))

(define-public crate-castle_shared_parser-0.14.0 (c (n "castle_shared_parser") (v "0.14.0") (d (list (d (n "castle_tokenizer") (r "^0.14.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.14.0") (d #t) (k 0)))) (h "05wm1w1wff0vhn2g1131fdw2gax7j5bkhg20883xzsh1a1acyzz7")))

(define-public crate-castle_shared_parser-0.15.0 (c (n "castle_shared_parser") (v "0.15.0") (d (list (d (n "castle_tokenizer") (r "^0.15.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.15.0") (d #t) (k 0)))) (h "0613p1j1blh8kjn2iws34p4kwn4z1n4994nq4bk89kz2n12sbzym")))

(define-public crate-castle_shared_parser-0.16.0 (c (n "castle_shared_parser") (v "0.16.0") (d (list (d (n "castle_tokenizer") (r "^0.16.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.16.0") (d #t) (k 0)))) (h "0j4rlbq1lwxhp2rr4fsvxr5g6x4dzwzx4xf3awiywnlxxpfhb3gk")))

(define-public crate-castle_shared_parser-0.17.0 (c (n "castle_shared_parser") (v "0.17.0") (d (list (d (n "castle_tokenizer") (r "^0.17.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.17.0") (d #t) (k 0)))) (h "1wk0chinz2r467qyyvgybg68lzj1mq937lxjwhzdmkdrrm4razdl")))

(define-public crate-castle_shared_parser-0.18.0 (c (n "castle_shared_parser") (v "0.18.0") (d (list (d (n "castle_tokenizer") (r "^0.18.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.18.0") (d #t) (k 0)))) (h "1hqg562y5hfr2m4sj5gzwk0pb0vxxv322ya0nxwh2nh8130w79bq")))

(define-public crate-castle_shared_parser-0.19.0 (c (n "castle_shared_parser") (v "0.19.0") (d (list (d (n "castle_tokenizer") (r "^0.19.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.19.0") (d #t) (k 0)))) (h "0nr706g5qvnlhknnb0dyc5m7h34pd0biivcdx4zfdgwgg31zmb90")))

(define-public crate-castle_shared_parser-0.20.0 (c (n "castle_shared_parser") (v "0.20.0") (d (list (d (n "castle_tokenizer") (r "^0.20.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.0") (d #t) (k 0)))) (h "1msl0wlgi5vgi3i4049dw6d0s213599agx5x65sasm9dazkr0w00")))

(define-public crate-castle_shared_parser-0.20.1 (c (n "castle_shared_parser") (v "0.20.1") (d (list (d (n "castle_tokenizer") (r "^0.20.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.1") (d #t) (k 0)))) (h "1wx7p1nlqbhk8ivimg0hiz75n1wj63wc72h2ag10r5w3xv29bmsb")))

(define-public crate-castle_shared_parser-0.20.2 (c (n "castle_shared_parser") (v "0.20.2") (d (list (d (n "castle_tokenizer") (r "^0.20.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.2") (d #t) (k 0)))) (h "16dnav80897w18qw9p8kzmlpvidwc4np4zcczzm9sslig3zgapp0")))

