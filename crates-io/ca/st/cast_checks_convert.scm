(define-module (crates-io ca st cast_checks_convert) #:use-module (crates-io))

(define-public crate-cast_checks_convert-0.1.0 (c (n "cast_checks_convert") (v "0.1.0") (h "00q73acqb7s228881m1ryy5immwhl0ifsjyk0wlw3p1ld5jqp6f6")))

(define-public crate-cast_checks_convert-0.1.1 (c (n "cast_checks_convert") (v "0.1.1") (h "0yqk98k20qag9r7g74wbwdz6yjd382l03jmxxh0jdxb2yw55pxiv")))

(define-public crate-cast_checks_convert-0.1.2 (c (n "cast_checks_convert") (v "0.1.2") (h "1r4dsahcpfgyhydai925rdk7qdyplnlsdhlyjpl9rnrppcvz02wk")))

(define-public crate-cast_checks_convert-0.1.3 (c (n "cast_checks_convert") (v "0.1.3") (h "152d9mfxhwfpg38085hpfj5wfigl09ximyry2wkm57qz5wxvv83f")))

(define-public crate-cast_checks_convert-0.1.4 (c (n "cast_checks_convert") (v "0.1.4") (h "1wy1j5wf29zz7qkmxh1wxj9i1a1lf0l6vnm18xx7w4f2mi781yy1")))

(define-public crate-cast_checks_convert-0.1.5 (c (n "cast_checks_convert") (v "0.1.5") (h "16skpbi0nfmrfa2602pvcmd60ynk1dqjli7bfk3h675bxy3si517")))

