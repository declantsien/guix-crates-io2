(define-module (crates-io ca st castle-core) #:use-module (crates-io))

(define-public crate-castle-core-0.0.1 (c (n "castle-core") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1x7zg3v9qq61s7mqi85gjf4szb61qxj4wsap1sgz9rpf6hkxyssa")))

(define-public crate-castle-core-0.0.2 (c (n "castle-core") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0pv603z39b6bx7fz1dwnf41dmgnipp7banfz4yfmmssv989yfh6r")))

(define-public crate-castle-core-0.0.3 (c (n "castle-core") (v "0.0.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0jq5bqv3qqhj46l1izc3n17b0k3acd1rzhqdn0545b36yxs7574s")))

(define-public crate-castle-core-0.0.4 (c (n "castle-core") (v "0.0.4") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "186m9xg7v4lcyz8mq0ah0j4i84jm777wpcdg5prb4bpa7nqs7c5s")))

(define-public crate-castle-core-0.0.5 (c (n "castle-core") (v "0.0.5") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "04wn5i03ba9y1j720d2cwidk4vahwyawxhsm21gqxxp87pm6d53d")))

(define-public crate-castle-core-0.0.6 (c (n "castle-core") (v "0.0.6") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1s7q5mpky9x2yc6z3pbi9wn7m95qj6pj4p9af9yg21ajx1l6fw4c")))

(define-public crate-castle-core-0.0.7 (c (n "castle-core") (v "0.0.7") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "01yasl2cipyiy7jr9il2rv7dabnacwgxss9ncya23ibplx6gag1b")))

(define-public crate-castle-core-0.0.8 (c (n "castle-core") (v "0.0.8") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0f63v1a428sd20g6h51nxdwk0g8pnr0f03x8fyhil5dg1ch8lm73")))

(define-public crate-castle-core-0.0.9 (c (n "castle-core") (v "0.0.9") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0jr23j2phmphhbh8xjdkjrk71q8r4wp9j9801sg8sjqhbpl8wa8a")))

(define-public crate-castle-core-0.0.10 (c (n "castle-core") (v "0.0.10") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0jxiqx7s67zp3759axvqbfhl73cnnq5g471dm6nhwbd4x4qd1zjq")))

(define-public crate-castle-core-0.0.11 (c (n "castle-core") (v "0.0.11") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "071x5b0d4c6z60mnw1rhhwd9dfd2pnj58rk4dwpccf409ws6aldw")))

(define-public crate-castle-core-0.0.12 (c (n "castle-core") (v "0.0.12") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1cmxavqvs9w5h4zc6wxysbjk59jnqbiwf9pppmwbfgm03hq46vfj")))

