(define-module (crates-io ca st cast) #:use-module (crates-io))

(define-public crate-cast-0.1.0 (c (n "cast") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2.25") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.25") (o #t) (d #t) (k 0)))) (h "1dkc3k4nd31x2krnj1v37zhlzi37qgn1nahkwk1ql6nsagxl2681") (f (quote (("unstable" "quickcheck" "quickcheck_macros"))))))

(define-public crate-cast-0.2.0 (c (n "cast") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "175bijmdi25sdl7iy10xg2dnisbmdm4726xbhcb8nw6847nmnrpi") (f (quote (("std") ("default" "std"))))))

(define-public crate-cast-0.2.1 (c (n "cast") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "0nv79c3i0n6j6lh24pw7wkn6xp053v7qkj7661klz54mhg4cwgvr") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.2.2 (c (n "cast") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "09yl2700crxa4n860b080msij25klvs1kfzazhp2aihchvr16q4j") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.2.3 (c (n "cast") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1c5z7zryj0zwnhdgs6rw5dfvnlwc1vm19jzrlgx5055alnwk952b") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.2.4 (c (n "cast") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "04bwldr2pmxp07nk1aagji7fpybgyj1rjkilg26cf3mlpqgnsmlg") (f (quote (("x128") ("std") ("default" "std")))) (y #t)))

(define-public crate-cast-0.2.5 (c (n "cast") (v "0.2.5") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0xdbmxf2wz6b263bg8fzpiv1rpa01y129fqi81349r6ppy2w6f6c") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.2.6 (c (n "cast") (v "0.2.6") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3.3") (d #t) (k 1)))) (h "0x2khql6m1d970ps8b6qb6yjb6gplw0n3ayw8i6wpmmaa1fzmkap") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.2.7 (c (n "cast") (v "0.2.7") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "16p3bqi3qad1qdjgjc1r0x72iinj1aw2k8fw5zx2l51s52sdl92c") (f (quote (("x128") ("std") ("default" "std"))))))

(define-public crate-cast-0.3.0 (c (n "cast") (v "0.3.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1dbyngbyz2qkk0jn2sxil8vrz3rnpcj142y184p9l4nbl9radcip") (f (quote (("std"))))))

