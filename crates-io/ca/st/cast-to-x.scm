(define-module (crates-io ca st cast-to-x) #:use-module (crates-io))

(define-public crate-cast-to-x-0.1.0 (c (n "cast-to-x") (v "0.1.0") (d (list (d (n "ashpd") (r "^0.3.0") (d #t) (k 0)) (d (n "gstreamer") (r "^0.18.8") (d #t) (k 0)) (d (n "gstreamer-video") (r "^0.18.7") (d #t) (k 0)) (d (n "pipewire") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (f (quote ("x11"))) (k 0)) (d (n "zbus") (r "^2.1.1") (d #t) (k 0)))) (h "1qimiiakxl94vvzr9j2jd1i4fdr4dg5qfbh19hsawm8br101lkzj")))

