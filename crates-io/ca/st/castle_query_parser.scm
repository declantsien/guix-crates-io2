(define-module (crates-io ca st castle_query_parser) #:use-module (crates-io))

(define-public crate-castle_query_parser-0.3.2 (c (n "castle_query_parser") (v "0.3.2") (d (list (d (n "castle_error") (r "^0.3.2") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.3.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.3.2") (d #t) (k 0)))) (h "1rs62dgsaqqrkax19qzxnfagj3a768wq363yys440s5a8vqr393s")))

(define-public crate-castle_query_parser-0.4.0 (c (n "castle_query_parser") (v "0.4.0") (d (list (d (n "castle_error") (r "^0.4.0") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.0") (d #t) (k 0)))) (h "0n637jxl6j2d8nzpflwc96pjbmiii6yzjzf5y3wm1an9i0n7x066")))

(define-public crate-castle_query_parser-0.4.1 (c (n "castle_query_parser") (v "0.4.1") (d (list (d (n "castle_error") (r "^0.4.1") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.1") (d #t) (k 0)))) (h "0znfb9xvxg73z522jhwgyng3qhir8b4nmaiiar2z169znz2prvji")))

(define-public crate-castle_query_parser-0.4.2 (c (n "castle_query_parser") (v "0.4.2") (d (list (d (n "castle_error") (r "^0.4.2") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.2") (d #t) (k 0)))) (h "059lkbl0ii3frl9hhipq93d8hkhpmd7bh5dqfbw86dik2si92vzc")))

(define-public crate-castle_query_parser-0.4.3 (c (n "castle_query_parser") (v "0.4.3") (d (list (d (n "castle_error") (r "^0.4.3") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.3") (d #t) (k 0)))) (h "1n59nggc3hlfhhisjc6gxmg3kr2wwgvi1z3my87xqj3j5i55g4al")))

(define-public crate-castle_query_parser-0.4.4 (c (n "castle_query_parser") (v "0.4.4") (d (list (d (n "castle_error") (r "^0.4.4") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.4") (d #t) (k 0)))) (h "02laflibbgmbd8w75bni7xiri36sl0p19v5lkdi4fjqxfvmqjzlk")))

(define-public crate-castle_query_parser-0.4.5 (c (n "castle_query_parser") (v "0.4.5") (d (list (d (n "castle_error") (r "^0.4.5") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.5") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.5") (d #t) (k 0)))) (h "0d0y3xirmlmw08dbvs597hbmmjl9sc77kzby5lkvk0acwh92n2yz")))

(define-public crate-castle_query_parser-0.4.6 (c (n "castle_query_parser") (v "0.4.6") (d (list (d (n "castle_error") (r "^0.4.6") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.4.6") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.4.6") (d #t) (k 0)))) (h "06girf9zi8jdk0lnzw56czsq4c22d5l5mfjb5wybgvxkahabb06p")))

(define-public crate-castle_query_parser-0.5.0 (c (n "castle_query_parser") (v "0.5.0") (d (list (d (n "castle_error") (r "^0.5.0") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.0") (d #t) (k 0)))) (h "03xk9kdajflxzh209xnq8iwyqvqcpypy2dvnyhv34vwwyz0xm4nz")))

(define-public crate-castle_query_parser-0.5.1 (c (n "castle_query_parser") (v "0.5.1") (d (list (d (n "castle_error") (r "^0.5.1") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.1") (d #t) (k 0)))) (h "04al7swgppi6h7vn52gm0bndaz599pi9pq4l55pnjwqmnscy77in")))

(define-public crate-castle_query_parser-0.5.2 (c (n "castle_query_parser") (v "0.5.2") (d (list (d (n "castle_error") (r "^0.5.2") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.2") (d #t) (k 0)))) (h "0455gb3zmh8qpwx3nbjznavjbda1svqi3g9gx8alagswyfh88grm")))

(define-public crate-castle_query_parser-0.5.3 (c (n "castle_query_parser") (v "0.5.3") (d (list (d (n "castle_error") (r "^0.5.3") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.3") (d #t) (k 0)))) (h "1n054cgraxh417xzibww4rmpkipgx1swi5xr9bn2hcpyx2wcsbxm")))

(define-public crate-castle_query_parser-0.5.4 (c (n "castle_query_parser") (v "0.5.4") (d (list (d (n "castle_error") (r "^0.5.4") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.4") (d #t) (k 0)))) (h "1zs8kq11r381vp3ja57qqpjnv24lbvnnfyqwdsg3p4cjp3f55k4c")))

(define-public crate-castle_query_parser-0.5.5 (c (n "castle_query_parser") (v "0.5.5") (d (list (d (n "castle_error") (r "^0.5.5") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.5") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.5") (d #t) (k 0)))) (h "1qjb0a3rjz7p4mzjyb0lii8jgq6ysa28a8kz51ww7l7l1rp03h0y")))

(define-public crate-castle_query_parser-0.5.6 (c (n "castle_query_parser") (v "0.5.6") (d (list (d (n "castle_error") (r "^0.5.6") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.6") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.6") (d #t) (k 0)))) (h "007sxrd0w8w04h2586lilq50s88cq22sz2g1icsrfk4ajia4rs3z")))

(define-public crate-castle_query_parser-0.5.7 (c (n "castle_query_parser") (v "0.5.7") (d (list (d (n "castle_error") (r "^0.5.7") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.7") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.7") (d #t) (k 0)))) (h "07r9x20cazxqksnyjlwriynd2qgbf6581linwn0qy1zl8aiy3ygi")))

(define-public crate-castle_query_parser-0.5.8 (c (n "castle_query_parser") (v "0.5.8") (d (list (d (n "castle_error") (r "^0.5.8") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.8") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.8") (d #t) (k 0)))) (h "1gqw7cppk7nl4sdj3xdx3icwmyvz66n2zksrg3627117659pvdsg")))

(define-public crate-castle_query_parser-0.5.9 (c (n "castle_query_parser") (v "0.5.9") (d (list (d (n "castle_error") (r "^0.5.9") (d #t) (k 0)) (d (n "castle_shared_parser") (r "^0.5.9") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.5.9") (d #t) (k 0)))) (h "1hmr8vqlcwj0zz7cirjav2zz8ixwx4bbrkr16czlw9rfk2c8f72k")))

(define-public crate-castle_query_parser-0.6.1 (c (n "castle_query_parser") (v "0.6.1") (d (list (d (n "castle_shared_parser") (r "^0.6.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.1") (d #t) (k 0)))) (h "1p27jajg1231vg0xbx9lwlqh24y8rrhggfsbj3gi493jgs3mpxh4")))

(define-public crate-castle_query_parser-0.6.2 (c (n "castle_query_parser") (v "0.6.2") (d (list (d (n "castle_shared_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.2") (d #t) (k 0)))) (h "0gkpdlhakghq8l76dhsv2alb4n6jxdrdj646g8nah9wvzq4nr5y9")))

(define-public crate-castle_query_parser-0.6.3 (c (n "castle_query_parser") (v "0.6.3") (d (list (d (n "castle_shared_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.3") (d #t) (k 0)))) (h "15zv7yxvrwvvn9wca51gw01jd604b2krq652762a9pwj2q063i04")))

(define-public crate-castle_query_parser-0.6.4 (c (n "castle_query_parser") (v "0.6.4") (d (list (d (n "castle_shared_parser") (r "^0.6.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.4") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.4") (d #t) (k 0)))) (h "1k2gbx65ww0ndh23w094cbvqbrl48ys3ijijkw0dy0bycz49v9i9")))

(define-public crate-castle_query_parser-0.6.5 (c (n "castle_query_parser") (v "0.6.5") (d (list (d (n "castle_shared_parser") (r "^0.6.5") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.5") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.5") (d #t) (k 0)))) (h "17zdjb5ccpnnp02zsvpxm9c4hhgnxx2z9xyrqmsvr345r6qfvf83")))

(define-public crate-castle_query_parser-0.6.6 (c (n "castle_query_parser") (v "0.6.6") (d (list (d (n "castle_shared_parser") (r "^0.6.6") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.6.6") (d #t) (k 0)) (d (n "castle_types") (r "^0.6.6") (d #t) (k 0)))) (h "0q5ijz7wm0b7hsfkfkhhyqvs0hscbc44b0mvkhxxm864wxy35kvm")))

(define-public crate-castle_query_parser-0.7.0 (c (n "castle_query_parser") (v "0.7.0") (d (list (d (n "castle_shared_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.7.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.0") (d #t) (k 0)))) (h "0frbr45q7k1jlxpvw2x277gv6xxrgcsg0s4v4y3r2hp7m0bf1dvw")))

(define-public crate-castle_query_parser-0.7.1 (c (n "castle_query_parser") (v "0.7.1") (d (list (d (n "castle_shared_parser") (r "^0.7.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.7.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.1") (d #t) (k 0)))) (h "19ik6ym9mcn015h1nakaf6sbwqra4bhcapdqzakypb9n7q7zprm9")))

(define-public crate-castle_query_parser-0.7.2 (c (n "castle_query_parser") (v "0.7.2") (d (list (d (n "castle_shared_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.7.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.7.2") (d #t) (k 0)))) (h "06ni2vmk8cqr0p4w0bf75k9gjgghi51kikkg832f34wsgdz071mn")))

(define-public crate-castle_query_parser-0.8.0 (c (n "castle_query_parser") (v "0.8.0") (d (list (d (n "castle_shared_parser") (r "^0.8.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.8.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.0") (d #t) (k 0)))) (h "0b8588i62nkvkc8yzr3ry12jbax5dby5f5y3c5kapa3h5b2q7r2a")))

(define-public crate-castle_query_parser-0.8.1 (c (n "castle_query_parser") (v "0.8.1") (d (list (d (n "castle_shared_parser") (r "^0.8.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.8.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.1") (d #t) (k 0)))) (h "0jssjc97kgmvdhdg5fybsk1v4y6q1nwrb0axs89fjz100mrh174s")))

(define-public crate-castle_query_parser-0.8.2 (c (n "castle_query_parser") (v "0.8.2") (d (list (d (n "castle_shared_parser") (r "^0.8.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.8.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.2") (d #t) (k 0)))) (h "1kc9zm2zyjlblyq32jh3npphdyrvjzxrpjcvh58yfck6npnf9jl6")))

(define-public crate-castle_query_parser-0.8.3 (c (n "castle_query_parser") (v "0.8.3") (d (list (d (n "castle_shared_parser") (r "^0.8.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.8.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.8.3") (d #t) (k 0)))) (h "1qyg7r6vqz7zlwcd3xyia2pgrllp50l1w2r8dnjccy65k3mb8j8x")))

(define-public crate-castle_query_parser-0.9.0 (c (n "castle_query_parser") (v "0.9.0") (d (list (d (n "castle_shared_parser") (r "^0.9.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.9.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.0") (d #t) (k 0)))) (h "1j64hbgmrpf9qxjca7l6g42iys4ngzvd3df6wk1yck88qimvbnsm")))

(define-public crate-castle_query_parser-0.9.1 (c (n "castle_query_parser") (v "0.9.1") (d (list (d (n "castle_shared_parser") (r "^0.9.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.9.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.1") (d #t) (k 0)))) (h "0ry4i3y5514214k8ds8015mhffml1nf9zg95mkaii540j79n3lji")))

(define-public crate-castle_query_parser-0.9.2 (c (n "castle_query_parser") (v "0.9.2") (d (list (d (n "castle_shared_parser") (r "^0.9.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.9.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.2") (d #t) (k 0)))) (h "1cbzmn3n1ds1w6i75caq9xv1r606gbd8rvc4kg2ax6ryap0iddyw")))

(define-public crate-castle_query_parser-0.9.3 (c (n "castle_query_parser") (v "0.9.3") (d (list (d (n "castle_shared_parser") (r "^0.9.3") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.9.3") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.3") (d #t) (k 0)))) (h "128wp1va9bfdwdkkgmvg57c2n9ab0b9298431y1niv0d31fq3c8j")))

(define-public crate-castle_query_parser-0.9.4 (c (n "castle_query_parser") (v "0.9.4") (d (list (d (n "castle_shared_parser") (r "^0.9.4") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.9.4") (d #t) (k 0)) (d (n "castle_types") (r "^0.9.4") (d #t) (k 0)))) (h "1k2g33i5y7f0f4512f8cvcnnwl1jf6ygnpwvz83v80k703y0gy3c")))

(define-public crate-castle_query_parser-0.10.0 (c (n "castle_query_parser") (v "0.10.0") (d (list (d (n "castle_shared_parser") (r "^0.10.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.10.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.10.0") (d #t) (k 0)))) (h "0wdijk8djfw0hmrkvdahgqnbvn3bxcbvdqmxc22lyl3mljxx1var")))

(define-public crate-castle_query_parser-0.10.1 (c (n "castle_query_parser") (v "0.10.1") (d (list (d (n "castle_shared_parser") (r "^0.10.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.10.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.10.1") (d #t) (k 0)))) (h "04ga0gxlh51yn9kg1d48jb5xpvx2kghlpraxg5lbqp3iib73c3lh")))

(define-public crate-castle_query_parser-0.12.0 (c (n "castle_query_parser") (v "0.12.0") (d (list (d (n "castle_shared_parser") (r "^0.12.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.12.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.12.0") (d #t) (k 0)))) (h "1b2xjn90awfsfjkmlmy1g5wbnv05ryfk067d80zzv63vj0va9d3d")))

(define-public crate-castle_query_parser-0.13.0 (c (n "castle_query_parser") (v "0.13.0") (d (list (d (n "castle_shared_parser") (r "^0.13.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.13.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.13.0") (d #t) (k 0)))) (h "1mj01x5lla52bwvgmz9mlqjb2zh7lkbrdr3xh6v9ynayk2dqdmcm")))

(define-public crate-castle_query_parser-0.13.1 (c (n "castle_query_parser") (v "0.13.1") (d (list (d (n "castle_shared_parser") (r "^0.13.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.13.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.13.1") (d #t) (k 0)))) (h "1nvqzj10alwz1vjf8d3zjafhiybv5im0qgrb1jn9vixiapcifm45")))

(define-public crate-castle_query_parser-0.14.0 (c (n "castle_query_parser") (v "0.14.0") (d (list (d (n "castle_shared_parser") (r "^0.14.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.14.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.14.0") (d #t) (k 0)))) (h "073mlx6sk32yd6a4qmcy6a5jwbwjd70yii0jr9c7qm2shw5q36p0")))

(define-public crate-castle_query_parser-0.15.0 (c (n "castle_query_parser") (v "0.15.0") (d (list (d (n "castle_shared_parser") (r "^0.15.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.15.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.15.0") (d #t) (k 0)))) (h "1a9kdghf43gs6iisb5xvi8943hj3vkgg0wxhpvfc59m6avx2h3bc")))

(define-public crate-castle_query_parser-0.16.0 (c (n "castle_query_parser") (v "0.16.0") (d (list (d (n "castle_shared_parser") (r "^0.16.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.16.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.16.0") (d #t) (k 0)))) (h "1i1ypmr98sc7w37z4d94fd5hswcjskrpp52wxpxwqh5x6vi392jk")))

(define-public crate-castle_query_parser-0.17.0 (c (n "castle_query_parser") (v "0.17.0") (d (list (d (n "castle_shared_parser") (r "^0.17.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.17.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.17.0") (d #t) (k 0)))) (h "1wwx00wfasis1p5hjc4gif90w6r7cd42b4wg188405wcrfmflc1l")))

(define-public crate-castle_query_parser-0.18.0 (c (n "castle_query_parser") (v "0.18.0") (d (list (d (n "castle_shared_parser") (r "^0.18.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.18.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.18.0") (d #t) (k 0)))) (h "1m1r99c4ndh5zaz58wg7acdkyh74mh0wl0k5ai81w7vkln5ww5rp")))

(define-public crate-castle_query_parser-0.19.0 (c (n "castle_query_parser") (v "0.19.0") (d (list (d (n "castle_shared_parser") (r "^0.19.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.19.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.19.0") (d #t) (k 0)))) (h "10a317zj8qszj3v7mzw2wjdcgxm0bzh024h2i2h1iica91zh0ajq")))

(define-public crate-castle_query_parser-0.20.0 (c (n "castle_query_parser") (v "0.20.0") (d (list (d (n "castle_shared_parser") (r "^0.20.0") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.20.0") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.0") (d #t) (k 0)))) (h "0zk2ba4f5999ilwd3p04wjw7biyvwwhv6irhygbigw6kri2v5i57")))

(define-public crate-castle_query_parser-0.20.1 (c (n "castle_query_parser") (v "0.20.1") (d (list (d (n "castle_shared_parser") (r "^0.20.1") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.20.1") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.1") (d #t) (k 0)))) (h "1l5qz77pa1q3vaa9ncjb0w4ynp3l19djlylkwlrxvysymd3zpsdx")))

(define-public crate-castle_query_parser-0.20.2 (c (n "castle_query_parser") (v "0.20.2") (d (list (d (n "castle_shared_parser") (r "^0.20.2") (d #t) (k 0)) (d (n "castle_tokenizer") (r "^0.20.2") (d #t) (k 0)) (d (n "castle_types") (r "^0.20.2") (d #t) (k 0)))) (h "01r1grkf328ac8pw1z99xmzhq2apygbb6l43g2v1h04zpvphv4i7")))

