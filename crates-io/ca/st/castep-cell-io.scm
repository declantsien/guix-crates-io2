(define-module (crates-io ca st castep-cell-io) #:use-module (crates-io))

(define-public crate-castep-cell-io-0.1.0 (c (n "castep-cell-io") (v "0.1.0") (d (list (d (n "castep-periodic-table") (r "^0.3.2") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "04h5kixahwrwb8xrd4i423hl92i605blyq8wndz0ygcqq9yk8xcd")))

(define-public crate-castep-cell-io-0.1.1 (c (n "castep-cell-io") (v "0.1.1") (d (list (d (n "castep-periodic-table") (r "^0.3.2") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "13ad4xfmnvsd192qsrfhs9vqc18mnksqii6grhqm7lv6fx9bf9mb")))

(define-public crate-castep-cell-io-0.1.2 (c (n "castep-cell-io") (v "0.1.2") (d (list (d (n "castep-periodic-table") (r "^0.3.2") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "003rr0f80nk5izhk4mqyr86hjx2jxn6sbkzasbn5kjsb7jdrm794")))

(define-public crate-castep-cell-io-0.1.3 (c (n "castep-cell-io") (v "0.1.3") (d (list (d (n "castep-periodic-table") (r "^0.4.0") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "0gkbq98039s1cd664crppv64mi262gx74532vzzaidyy77fxpxi2")))

(define-public crate-castep-cell-io-0.1.4 (c (n "castep-cell-io") (v "0.1.4") (d (list (d (n "castep-periodic-table") (r "^0.4.0") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "0pgxv5rnmzk7wkvaax146ipck0nfd0g37lpd7qhlg9j8s9i3h6nr")))

(define-public crate-castep-cell-io-0.1.5 (c (n "castep-cell-io") (v "0.1.5") (d (list (d (n "castep-periodic-table") (r "^0.4.0") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "1pxqr1d1n5ibhsyyd4i74y7f90j6fznahrh60ns7jap080phf5jy")))

