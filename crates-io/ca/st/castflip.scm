(define-module (crates-io ca st castflip) #:use-module (crates-io))

(define-public crate-castflip-0.1.0 (c (n "castflip") (v "0.1.0") (d (list (d (n "castflip_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0nxqfa0wwz0c5agz3240nljf092fwjyym3wgzj1y1gjdw69mnr5z")))

(define-public crate-castflip-0.1.1 (c (n "castflip") (v "0.1.1") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0wcahk0g164h106fbq064lp1ck5njmml334wsw17qhda1j1a20xd")))

(define-public crate-castflip-0.1.2 (c (n "castflip") (v "0.1.2") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0g7hfw7fc40dsmparg2x0m7f3wbbh4372nk1rf7ljc9wvcmibkpc")))

(define-public crate-castflip-0.1.3 (c (n "castflip") (v "0.1.3") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0an8n4zqm5sld3n7yz1c3pypxqv54h98jxkdjnvd9naqapb3jbvl")))

(define-public crate-castflip-0.1.4 (c (n "castflip") (v "0.1.4") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "071vii9dyyzmm9zp5c65grk7511g8rvpppjxfj3wfgmfazv4lbxr")))

(define-public crate-castflip-0.1.5 (c (n "castflip") (v "0.1.5") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "1l69g0wpflacm97v4245x2hm7g2h88jy8hcvilws1w9sifz3ynaf")))

(define-public crate-castflip-0.1.6 (c (n "castflip") (v "0.1.6") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0lys6sydkhimsg7nyalih3srnck8ik2fzw7j16w49p0q1pjwi5bf")))

(define-public crate-castflip-0.1.7 (c (n "castflip") (v "0.1.7") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "1bw6dwwrywr9krz4g36bzmhdfi0w7v162ba9sg25jiyxdq070056")))

(define-public crate-castflip-0.1.8 (c (n "castflip") (v "0.1.8") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "18d48rwf5mm12id6lvcjhk1ymnga18izzng2p1a15r8gf1i4c7bx") (f (quote (("std") ("default" "std"))))))

(define-public crate-castflip-0.1.9 (c (n "castflip") (v "0.1.9") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0czz7b7rfgas1mj7avfqgzfbgkzcdxxy471g5rvvq822b6wvyw5k") (f (quote (("std") ("default" "std"))))))

(define-public crate-castflip-0.1.10 (c (n "castflip") (v "0.1.10") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "0wwygwa6kb2h2dp92wdjc831sx9zx4sa7wjv251d0qqlg1ln50zd") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-castflip-0.1.11 (c (n "castflip") (v "0.1.11") (d (list (d (n "castflip_derive") (r "^0.1") (d #t) (k 0)))) (h "1pgn3qjx61vwxhb520l2k6pm4pb2c5rik6r8yw3cs7ad13r7sa23") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

