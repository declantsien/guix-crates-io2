(define-module (crates-io ca st castor) #:use-module (crates-io))

(define-public crate-castor-0.1.0 (c (n "castor") (v "0.1.0") (d (list (d (n "kv") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1aw1qqgslndfjm1l38br1kpipikxix4b4s8d8hnwmcdkzdjrllsz")))

(define-public crate-castor-0.1.1 (c (n "castor") (v "0.1.1") (d (list (d (n "kv") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0f8hc3rxivbb97igm8nw99ah1k6yg19bsqnvy1p0vqgiv9nyxgfp")))

(define-public crate-castor-0.2.0 (c (n "castor") (v "0.2.0") (d (list (d (n "kv") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "01ww972v7h31yr2lnnwdrib0vja4a2wqkjznky6w1qhbi0nhy9fg")))

(define-public crate-castor-0.2.1 (c (n "castor") (v "0.2.1") (d (list (d (n "kv") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1kdzbd54mdmww96m6w5rs2nxhrr0zyshjpabsganjhg5f687l2h1")))

(define-public crate-castor-0.3.0 (c (n "castor") (v "0.3.0") (d (list (d (n "kv") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0zvykjn51s1b2h0xb3a2jng8z9i08n198hm2li7a0mz92il7gwwj")))

(define-public crate-castor-0.3.1 (c (n "castor") (v "0.3.1") (d (list (d (n "kv") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1caicmy51xsc4sl4cx95a8sfldlaqvvijrw05q27mf00619r61cz")))

(define-public crate-castor-0.4.0 (c (n "castor") (v "0.4.0") (d (list (d (n "blake2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "kv") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0n0gdk9irmsqww91r52d1w5xsjzya552krpvvzp4g7sn76b3jld1") (f (quote (("sha3_512" "sha3") ("sha3_256" "sha3") ("sha2_512" "sha2") ("sha2_256" "sha2") ("default" "blake2s") ("blake2s" "blake2") ("blake2b" "blake2"))))))

(define-public crate-castor-0.4.1 (c (n "castor") (v "0.4.1") (d (list (d (n "blake2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "kv") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0ailbji1hzlacrihr6b97dgpzlzq2m3x2yzzmphq4x6r9a0y33ah") (f (quote (("sha3_512" "sha3") ("sha3_256" "sha3") ("sha2_512" "sha2") ("sha2_256" "sha2") ("default" "blake2s") ("blake2s" "blake2") ("blake2b" "blake2"))))))

(define-public crate-castor-0.4.2 (c (n "castor") (v "0.4.2") (d (list (d (n "blake2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "kv") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "09i49x4a500yfklbllqvl8swziyv6zcdqxli0cylc2hn10kv92vv") (f (quote (("sha3_512" "sha3") ("sha3_256" "sha3") ("sha2_512" "sha2") ("sha2_256" "sha2") ("default" "blake2s") ("blake2s" "blake2") ("blake2b" "blake2"))))))

(define-public crate-castor-0.4.3 (c (n "castor") (v "0.4.3") (d (list (d (n "blake2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "kv") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1qqh68bxx0vfyhvigwk4875gmvzjz55vrlxn0bnn7gm77r6cw7p9") (f (quote (("sha3_512" "sha3") ("sha3_256" "sha3") ("sha2_512" "sha2") ("sha2_256" "sha2") ("default" "blake2s") ("blake2s" "blake2") ("blake2b" "blake2"))))))

(define-public crate-castor-0.5.0 (c (n "castor") (v "0.5.0") (d (list (d (n "blake2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "kv") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1g95ql2wf21ndx5gw6zna5hd515pkjyi9nvkhvnb8cvj61jp3mid") (f (quote (("sha3_512" "sha3") ("sha3_256" "sha3") ("sha2_512" "sha2") ("sha2_256" "sha2") ("default" "blake2s") ("blake2s" "blake2") ("blake2b" "blake2"))))))

