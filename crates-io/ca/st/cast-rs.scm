(define-module (crates-io ca st cast-rs) #:use-module (crates-io))

(define-public crate-cast-rs-0.1.0 (c (n "cast-rs") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "typename") (r "^0.1.1") (d #t) (k 0)))) (h "1kar9kwc933rd9z3j9v7faycw9rjx081iv6hw487alkgfb495wxg")))

(define-public crate-cast-rs-0.1.1 (c (n "cast-rs") (v "0.1.1") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "typename") (r "^0.1.1") (d #t) (k 0)))) (h "1w3gpcpy3jnpwa7wdrki53dpsk4bgxdp6lr796rc0pz6wx4bk8ry")))

(define-public crate-cast-rs-0.1.2 (c (n "cast-rs") (v "0.1.2") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "slice-cast") (r "^0.1") (d #t) (k 0)))) (h "1v8pzwzr96skqhncjrhmf8qx6s3ank571y3w6cvxmgiis1bv6f82")))

(define-public crate-cast-rs-0.2.0 (c (n "cast-rs") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "slice-cast") (r "^0.1") (d #t) (k 0)))) (h "1hpgn7dklmv0pyy5a9zlyp0pw9b2flvqikb4paiv5n58c23rb6x6")))

(define-public crate-cast-rs-0.2.1 (c (n "cast-rs") (v "0.2.1") (d (list (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "slice-cast") (r "^0.1") (d #t) (k 0)))) (h "0fdwdq8c0rkawcq9m3szdczf01gmdbysm2kllapnq1x2p97xrr9h")))

