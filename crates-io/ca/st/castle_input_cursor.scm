(define-module (crates-io ca st castle_input_cursor) #:use-module (crates-io))

(define-public crate-castle_input_cursor-0.3.2 (c (n "castle_input_cursor") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "11p7nccgmjyxq59lpixwhcz8i1lahgs2h8r5h5q9mlzv8wbx8kvv")))

(define-public crate-castle_input_cursor-0.4.0 (c (n "castle_input_cursor") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w7s7z5p9548ka341m5x33563iwiyqikdsamp3z22kd2drxfpkx9")))

(define-public crate-castle_input_cursor-0.4.1 (c (n "castle_input_cursor") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ms0d2fs8x4mpq3fbpk0ggv35hcfzc625w1dp0r7gxl51wk43x5z")))

(define-public crate-castle_input_cursor-0.4.2 (c (n "castle_input_cursor") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wbyy1x30vzwwz8ngg5pslg5n18362g0v8yvk0w8vxqs6as8yfwd")))

(define-public crate-castle_input_cursor-0.4.3 (c (n "castle_input_cursor") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zj2wmn5c16blrgvgwlqbnzmr809lgacszgbwvyfzykicw4swgd8")))

(define-public crate-castle_input_cursor-0.4.4 (c (n "castle_input_cursor") (v "0.4.4") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h08ll84h69a69dpcnxfx0gbsnpmqggrbgnhk8d1xb1b5yy20m3a")))

(define-public crate-castle_input_cursor-0.4.5 (c (n "castle_input_cursor") (v "0.4.5") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "077dw78y9wf6kza1k1h8z9v5v34bvmvvz08917jqmb5p5mx3m375")))

(define-public crate-castle_input_cursor-0.4.6 (c (n "castle_input_cursor") (v "0.4.6") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cbf9i0yq269qkhyxgbh3z03qxfqkbqynv2v865dsjzbdra563hf")))

(define-public crate-castle_input_cursor-0.5.0 (c (n "castle_input_cursor") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "143p6wqa6cy0lnpx1b03d8dz0fl32c87pk9a7x5qnppayiv5ngvp")))

(define-public crate-castle_input_cursor-0.5.1 (c (n "castle_input_cursor") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yir986h37ny82r23i33zlf30qlh3kc7243rk5sprr46d8xzcqf9")))

(define-public crate-castle_input_cursor-0.5.2 (c (n "castle_input_cursor") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "01vi9z560sk1wflm5gmmgp33ik7rdbbiwacvv4295zwx6kch87c3")))

(define-public crate-castle_input_cursor-0.5.3 (c (n "castle_input_cursor") (v "0.5.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vj0jvll07sdg2nc2ms7gd16qab3divbwnfq6k1hx8rr9yh1ms4c")))

(define-public crate-castle_input_cursor-0.5.4 (c (n "castle_input_cursor") (v "0.5.4") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k2c2mn4d9s9pvdb02lqfhkk4n8m36gfc6j7vhn05bp6cazd3ac2")))

(define-public crate-castle_input_cursor-0.5.5 (c (n "castle_input_cursor") (v "0.5.5") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s0asdc1flz45002s7nxs325zi8z56ci71aq2h25raj5r3452ly8")))

(define-public crate-castle_input_cursor-0.5.6 (c (n "castle_input_cursor") (v "0.5.6") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0krc77jwrc01zrp3qhi8yyaqiz1ab7c0acwwnw9bdkw6isrw7f38")))

(define-public crate-castle_input_cursor-0.5.7 (c (n "castle_input_cursor") (v "0.5.7") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "10gm9h83w2v37nrabb84j2jj71yyb561xs1m5cchlc76pi2cr3rz")))

(define-public crate-castle_input_cursor-0.5.8 (c (n "castle_input_cursor") (v "0.5.8") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zf8n015grnh148z5aimixasn3j2yi6mzzw8fxr878cfjgdbzm3z")))

(define-public crate-castle_input_cursor-0.5.9 (c (n "castle_input_cursor") (v "0.5.9") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nwqlr153rfqsgcv6f0jsh7jrgczj6yhkm6h9ywzq8cv8a3666in")))

(define-public crate-castle_input_cursor-0.6.0 (c (n "castle_input_cursor") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fr9ppgsi4g0mxf3j614xfv0y40cwkv003g0h02g5wpgy0jsx1da")))

(define-public crate-castle_input_cursor-0.6.1 (c (n "castle_input_cursor") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qvffb20r1whzbz7v2gzi7yc4a2fyafip932rkfkc7rysq1f27fj")))

(define-public crate-castle_input_cursor-0.6.2 (c (n "castle_input_cursor") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "135yac0fnqds8fp7gc04mdwg31wlq8f1n5r0xa8fyw4yx07hzzd5")))

(define-public crate-castle_input_cursor-0.6.3 (c (n "castle_input_cursor") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fpmkyi2ivbwy48lgy82b38kk6syiz34vsy3x7ljj274bsr5rl3n")))

(define-public crate-castle_input_cursor-0.6.4 (c (n "castle_input_cursor") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hdvzg9qdj2l0zdyndxxz8x4dgr58nr4grby3hk081sjbs448m7m")))

(define-public crate-castle_input_cursor-0.6.5 (c (n "castle_input_cursor") (v "0.6.5") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h4i7nl4f5awb9a436bid9s2mmsd74xai0n82xf1j0lznkijns0b")))

(define-public crate-castle_input_cursor-0.6.6 (c (n "castle_input_cursor") (v "0.6.6") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1522rf4npnkbf7kf02jblx37i14hv01yazhsa0wpi9n6sk2l4xak")))

(define-public crate-castle_input_cursor-0.7.0 (c (n "castle_input_cursor") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gkf53qcvhlakkhcpw50s9ngyrcizwwlggcw66snzhmjshn141yw")))

(define-public crate-castle_input_cursor-0.7.1 (c (n "castle_input_cursor") (v "0.7.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "11p9rvdai6rs0iwkl7ds9rfd3jfilbw4dnsvrx8p38f01p4ipbja")))

(define-public crate-castle_input_cursor-0.7.2 (c (n "castle_input_cursor") (v "0.7.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "19y7lwx3g1jw8yxw2qgjzhfwx5m0r05516jsn4v12rqvvzjid64q")))

(define-public crate-castle_input_cursor-0.8.0 (c (n "castle_input_cursor") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "12i97mj22wg2x6i1clnbzzmcrr3sh6x7n90kwx17j74g0xva4n3v")))

(define-public crate-castle_input_cursor-0.8.1 (c (n "castle_input_cursor") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dzzxwc7172h9k32pkqsl853qyj2gcvr6gq6ph1vqcgyy7zxdbxl")))

(define-public crate-castle_input_cursor-0.8.2 (c (n "castle_input_cursor") (v "0.8.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mxac17dllpk5bxhyp0zz954p1a17hyjazkis9acinyr4r8w89x6")))

(define-public crate-castle_input_cursor-0.8.3 (c (n "castle_input_cursor") (v "0.8.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a86m7bfs8fgqipfrpphbrsdnd9p8vkvxj567skbr8i5x5cib5cw")))

(define-public crate-castle_input_cursor-0.9.0 (c (n "castle_input_cursor") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m5i57qj8q11gh6bgmnzkbw7wdsivbrym8h09k8zrqaflhzvh18m")))

(define-public crate-castle_input_cursor-0.9.1 (c (n "castle_input_cursor") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "04f4rk8bgq46kj7jms6z8796xk8rp7dm1c69a9g4k6cddchc57m9")))

(define-public crate-castle_input_cursor-0.9.2 (c (n "castle_input_cursor") (v "0.9.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r2zhbib4xjdflvbawaynrqsmlkq9ywdr1fvhc0bnj2g0clin4ax")))

(define-public crate-castle_input_cursor-0.9.3 (c (n "castle_input_cursor") (v "0.9.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "18pfxx43l415db1n4dbdlqs5kakidbg94ldcd27gdi4x2rnshfpx")))

(define-public crate-castle_input_cursor-0.9.4 (c (n "castle_input_cursor") (v "0.9.4") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "119d994gcg1dcyk8d71waw3ff2d3v38xzd1f3jlwyi8ig037p2rk")))

(define-public crate-castle_input_cursor-0.10.0 (c (n "castle_input_cursor") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cc97fmw9sivhmpdkhz270hsd59zxk8lnjglrw2gcihscyabm6qy")))

(define-public crate-castle_input_cursor-0.10.1 (c (n "castle_input_cursor") (v "0.10.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kx0x1rr9lblvlczmn1jljzmx1awggyysb25w6cf09r4gr34l55d")))

(define-public crate-castle_input_cursor-0.11.0 (c (n "castle_input_cursor") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bz7ss6mma8galx3y8ag5f8mpmnlklw2xcs3f5brhygs63a803kc")))

(define-public crate-castle_input_cursor-0.12.0 (c (n "castle_input_cursor") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "10s4xqlhz6x1v0y2r6h4gl10j70ndv96kql46aa4zb37pjfb80dc")))

(define-public crate-castle_input_cursor-0.13.0 (c (n "castle_input_cursor") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0av43ajqhj28520d02w7d5fxax4ifris1lkly1i5a7ajhjl4jmsk")))

(define-public crate-castle_input_cursor-0.13.1 (c (n "castle_input_cursor") (v "0.13.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a8ql2a6d7cyf9wj923lirc413jhbfb4sci9407adkpci60ix5jp")))

(define-public crate-castle_input_cursor-0.14.0 (c (n "castle_input_cursor") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cq65ad1jqx8v0nma7jx65brzzspfs04al6rlabfzmcp0wyiq00p")))

(define-public crate-castle_input_cursor-0.15.0 (c (n "castle_input_cursor") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "19b12gq9csf2dhzv1h0pisz5l623n8vxwbz5gsng6bczdprxid3z")))

(define-public crate-castle_input_cursor-0.16.0 (c (n "castle_input_cursor") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "19zfgz2krs7vi9vanf5v36zvy9w53zfg715ppn9yaqvkzsr4bl9f")))

(define-public crate-castle_input_cursor-0.17.0 (c (n "castle_input_cursor") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jjncdanlz95pcr3jrm9pdni5yhfmawypgwcksk6qqr9d9vml8w1")))

(define-public crate-castle_input_cursor-0.18.0 (c (n "castle_input_cursor") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v8aykv895j5dr4cg3nrzqwzr2hqp1xixa6aifl1664y5d8a6m37")))

(define-public crate-castle_input_cursor-0.19.0 (c (n "castle_input_cursor") (v "0.19.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "106jdvlpvrf4y1kjvirkklw8hvws7wyzk3y6x0k2x0ws4h5yk1vb")))

(define-public crate-castle_input_cursor-0.20.0 (c (n "castle_input_cursor") (v "0.20.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "078lnx37rdg026fssm60blqabrrp7sn5hzkmpbsp71q58484m454")))

(define-public crate-castle_input_cursor-0.20.1 (c (n "castle_input_cursor") (v "0.20.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s49gvn8k90qx7vmmm48x0nx1nihlkg8zrpdkzx1sdslv0xwxdji")))

(define-public crate-castle_input_cursor-0.20.2 (c (n "castle_input_cursor") (v "0.20.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kv37gfvgyb4c6blwf18clr3bbfcvlimcryrqgkf0q09xarfq3qc")))

