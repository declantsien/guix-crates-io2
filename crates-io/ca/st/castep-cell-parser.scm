(define-module (crates-io ca st castep-cell-parser) #:use-module (crates-io))

(define-public crate-castep-cell-parser-0.1.0 (c (n "castep-cell-parser") (v "0.1.0") (d (list (d (n "castep-periodic-table") (r "^0.3.2") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "1vz8ywbps87dpcvq1vy24jc72bcx490v3hlh56872a4ld6mxk2vv")))

