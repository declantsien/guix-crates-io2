(define-module (crates-io ca st castaway) #:use-module (crates-io))

(define-public crate-castaway-0.1.0 (c (n "castaway") (v "0.1.0") (h "0m31z0lw7fc23fikbjrvn12hi11w9pzxq1x347gq61807bayyfpd")))

(define-public crate-castaway-0.1.1 (c (n "castaway") (v "0.1.1") (h "1hrzlzhg5sajcqaky2f1m2sxmf4d9433pwg0pgr4d3lihqaps97d")))

(define-public crate-castaway-0.1.2 (c (n "castaway") (v "0.1.2") (h "1xhspwy477qy5yg9c3jp713asxckjpx0vfrmz5l7r5zg7naqysd2")))

(define-public crate-castaway-0.2.0 (c (n "castaway") (v "0.2.0") (d (list (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1icbw1ial4v7n9m18d2vzx3pnk7c9vcfqpnl419izlwvyr3116lv") (f (quote (("std") ("default" "std"))))))

(define-public crate-castaway-0.2.1 (c (n "castaway") (v "0.2.1") (d (list (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1dsyaxyifp5n8sr5nnipwqmprx7xwq85xvxb2pkwi9lliv865dj6") (f (quote (("std") ("default" "std"))))))

(define-public crate-castaway-0.2.2 (c (n "castaway") (v "0.2.2") (d (list (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1k1z4v61vq7la56js1azkr0k9b415vif2kaxiqk3d1gw6mbfs5wa") (f (quote (("std") ("default" "std"))))))

