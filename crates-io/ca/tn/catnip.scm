(define-module (crates-io ca tn catnip) #:use-module (crates-io))

(define-public crate-catnip-0.1.1 (c (n "catnip") (v "0.1.1") (d (list (d (n "panic-never") (r "^0.1.0") (d #t) (k 0)))) (h "135wqjl0qvpp360pjwrkzywgdj125ypixk1ynmv90kxk6jl7inql")))

(define-public crate-catnip-0.1.2 (c (n "catnip") (v "0.1.2") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (d #t) (k 0)))) (h "18hxmca8jlaxasczl046zdqqsfl7m3ald0jnjvf9mgrrhvqpi90v") (f (quote (("crc" "crc32fast"))))))

(define-public crate-catnip-0.1.3 (c (n "catnip") (v "0.1.3") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "08j98bhbhxq0krr09zh3q6wjny99pymk443abc2l676m69dx15cf") (f (quote (("panic_never" "panic-never") ("default") ("crc" "crc32fast"))))))

(define-public crate-catnip-0.1.4 (c (n "catnip") (v "0.1.4") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "10gabd98b7l0r8ks9fbn1s8r84cn2v0fy233q4cdn0714wlkak4r") (f (quote (("panic_never" "panic-never") ("default" "panic-never") ("crc" "crc32fast"))))))

(define-public crate-catnip-0.2.0 (c (n "catnip") (v "0.2.0") (d (list (d (n "byte_struct") (r "^0.8.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0ckd2qpgzpi1zw8sdzv51vfw8vnh42ywibprs771pasnqb6pl02r") (f (quote (("panic_never" "panic-never") ("no_std") ("default" "no_std"))))))

(define-public crate-catnip-0.3.0 (c (n "catnip") (v "0.3.0") (d (list (d (n "byte_struct") (r "^0.8.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "1h2ms3ji8k2xp83wwllwya80b7bd4z8q73imrffd68yghmwhx0sw") (f (quote (("panic_never" "panic-never") ("no_std") ("default" "no_std"))))))

(define-public crate-catnip-0.3.1 (c (n "catnip") (v "0.3.1") (d (list (d (n "byte_struct") (r "^0.9.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "panic-never") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1vn30n90r1k8lvbhy7ysz2994pdq25b2inz3kblkrc6w5n4zidhr") (f (quote (("panic_never" "panic-never") ("no_std") ("default" "no_std"))))))

