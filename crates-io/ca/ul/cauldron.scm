(define-module (crates-io ca ul cauldron) #:use-module (crates-io))

(define-public crate-cauldron-0.0.1 (c (n "cauldron") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "02s6pw5hlby1pygxws1arjmivfnnkym6p65zl0f9xf6c0kaq1qcy") (y #t)))

(define-public crate-cauldron-0.0.2 (c (n "cauldron") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0yvv6d3xzwqdw3j73arscz2br3w1h3dggr88xqhvya9avr11qlzj") (y #t)))

(define-public crate-cauldron-0.0.3 (c (n "cauldron") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cpal") (r "^0.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "10brr2i17xkarf5pziflv9bcvfsgligw590zk1ccmdqzbbf9ad5i") (y #t)))

