(define-module (crates-io ca ul cauly-rust-leetcode-utils) #:use-module (crates-io))

(define-public crate-cauly-rust-leetcode-utils-0.1.0 (c (n "cauly-rust-leetcode-utils") (v "0.1.0") (h "1i0hnarpdvqjjyis2r2hnfn9kypvhbwfazgsrvalj82xcw1slm0d")))

(define-public crate-cauly-rust-leetcode-utils-0.1.1 (c (n "cauly-rust-leetcode-utils") (v "0.1.1") (h "04yzrbmhlyy7im351ysas01ag2ah0hy7z0zzbq12xwyq1zpy9zxl")))

