(define-module (crates-io ca nd candid_client) #:use-module (crates-io))

(define-public crate-candid_client-0.1.0 (c (n "candid_client") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)))) (h "17gdf22n2qyx95bmm1bc9ngrm2pr44yrhrh8wa5z0nvvj6nfvmr7")))

(define-public crate-candid_client-0.1.1 (c (n "candid_client") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)))) (h "0wfsqaxry8ga74v0k8s0k5q8bcz5vj072hha21vd9svq50s167xy")))

(define-public crate-candid_client-0.2.1 (c (n "candid_client") (v "0.2.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "181lbm79z3m82111dgzqfj9cp5s5ykvn8hrs02smzxcgrygrln2y")))

(define-public crate-candid_client-0.3.0 (c (n "candid_client") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "13nd8gi5w37bag3a9s74736z87qgfhhmfp3dyacgzbicvlyh2p39")))

(define-public crate-candid_client-0.3.1 (c (n "candid_client") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "17qkcw678w49vdwwyhbvcrsyc7gaj1d099my8b5b8jz7lvmj0k4j") (y #t)))

(define-public crate-candid_client-0.3.2 (c (n "candid_client") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1vzgv9ylphwcfivvy10n7lia3wfj9sldrjlh0b78rjq3zcla00si")))

