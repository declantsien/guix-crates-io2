(define-module (crates-io ca nd candid_server) #:use-module (crates-io))

(define-public crate-candid_server-0.1.0 (c (n "candid_server") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "13ivvjfy9aaw68ar4zrxxxv1pp6pch7zvwkwirq0l2ydx8a0kzhg")))

(define-public crate-candid_server-0.1.1 (c (n "candid_server") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "0dpirmdsmxbfnz3zjbkx2rq9ni3h12a19mhwkcxp4vg1q3jwcf0g")))

(define-public crate-candid_server-0.2.0 (c (n "candid_server") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "1gz2l53g99g6iwaq0xmbx5z2805kiw64vx9gwn4hy2q94cgiirar")))

(define-public crate-candid_server-0.2.1 (c (n "candid_server") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "1ajw8pfzp8rrv1wsjld8p8d2ph0cakbg9q0ixyw3ym1xfjky93k5")))

(define-public crate-candid_server-0.2.2 (c (n "candid_server") (v "0.2.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "1hky7sbc8x6z0pf88bz6hm7as272ycv51rd4ac67s3w7wh4bblcq")))

(define-public crate-candid_server-0.2.3 (c (n "candid_server") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "03k4cz1vijvg2766vs0v42b50r03gw6vzlsi9l0xpk4hqnhy4zs9")))

(define-public crate-candid_server-0.3.0 (c (n "candid_server") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (d #t) (k 0)))) (h "082nmgrir0xvc8asm56rzgwg89jqdbp4zwk99hv3ybi13mlc3xl0")))

