(define-module (crates-io ca nd candela) #:use-module (crates-io))

(define-public crate-candela-0.1.0 (c (n "candela") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a3sin11v4hbs6kz2s0km5jqrkrfjzfi84lg32lmd92flizbnwms")))

(define-public crate-candela-0.2.0 (c (n "candela") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9.2") (f (quote ("vendored"))) (o #t) (d #t) (k 0)))) (h "0gn8nvhqqwg6nzdy1r3mdd31ssfpa2w5cb0lq9h30ryv50d6zm6b") (f (quote (("zmq-client" "zmq" "mio") ("default"))))))

(define-public crate-candela-0.2.1 (c (n "candela") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9.2") (f (quote ("vendored"))) (o #t) (d #t) (k 0)))) (h "0j525wkj6qy2disn5wmhr6bvswa9a9ajm9nm3vvzkdnqxli1hqqd") (f (quote (("zmq-client" "zmq" "mio") ("default"))))))

