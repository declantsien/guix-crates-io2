(define-module (crates-io ca nd candle-ext) #:use-module (crates-io))

(define-public crate-candle-ext-0.1.0 (c (n "candle-ext") (v "0.1.0") (d (list (d (n "candle-core") (r "^0.3.0") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.0") (d #t) (k 0)))) (h "1ri9mcqqw8ypggw79r4y9yz3k4z62bn3dhjw4123b0rk8d7jmrk4")))

(define-public crate-candle-ext-0.1.1 (c (n "candle-ext") (v "0.1.1") (d (list (d (n "candle-core") (r "^0.3.0") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.0") (d #t) (k 0)))) (h "01xl4p4yw5mjgxfkpi96mrb6j0r653dz3a53s2c46y31jc1xxqyf")))

(define-public crate-candle-ext-0.1.2 (c (n "candle-ext") (v "0.1.2") (d (list (d (n "candle-core") (r "^0.3.0") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.0") (d #t) (k 0)))) (h "0ylb9k4sasxd876i6yq1l6yzqbndxicf356a0l8wlv9dnbgai6hm")))

(define-public crate-candle-ext-0.1.3 (c (n "candle-ext") (v "0.1.3") (d (list (d (n "candle-core") (r "^0.3.0") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.0") (d #t) (k 0)))) (h "0n2sfqj3an6672spjh76mcacy5b0lbmiad78cad91bbmz4xa7hcn")))

(define-public crate-candle-ext-0.1.4 (c (n "candle-ext") (v "0.1.4") (d (list (d (n "candle-core") (r "^0.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3") (d #t) (k 0)))) (h "00p1hn6li75kbiw8hwpqhrwbiqkm4px7ifpfpc6x8bm9bik1g3na")))

(define-public crate-candle-ext-0.1.5 (c (n "candle-ext") (v "0.1.5") (d (list (d (n "candle-core") (r "^0.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3") (d #t) (k 0)))) (h "1iblhz7ccrjcy05wg02iqbp5a51rjbb9nswl3na1wwkw9yynyz54")))

(define-public crate-candle-ext-0.1.6 (c (n "candle-ext") (v "0.1.6") (d (list (d (n "candle-core") (r "^0.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3") (d #t) (k 0)))) (h "094g8mik1pag1lgqj8nssadd9n34kpsy7qd4xsidrmnsj4nfhbw5")))

(define-public crate-candle-ext-0.1.7 (c (n "candle-ext") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "candle-core") (r "^0.3") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cb34rvxim7507qdkqpkc84f82k027i1yacrgynx9w8xnb5hsyz9") (f (quote (("default") ("cuda" "candle-core/cuda"))))))

