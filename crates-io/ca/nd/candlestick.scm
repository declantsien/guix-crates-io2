(define-module (crates-io ca nd candlestick) #:use-module (crates-io))

(define-public crate-candlestick-0.1.0 (c (n "candlestick") (v "0.1.0") (h "1vrmwcjfbfchpb0x72qjlh16940ij0bcxa5ycbb3qpv14j9lxib8")))

(define-public crate-candlestick-0.2.0 (c (n "candlestick") (v "0.2.0") (d (list (d (n "time-series") (r "^0.1") (o #t) (d #t) (k 0)))) (h "00zzz2b63k646wfqcvw320qplsg2ycip8r67l2vc54bb0bdqqy9a") (f (quote (("time_series" "time-series"))))))

