(define-module (crates-io ca nd candia) #:use-module (crates-io))

(define-public crate-candia-0.1.0 (c (n "candia") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "histogram") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09vrn804ra9xbjv1zsq50rbh7jxl8lawa668rhyxixldz68j06j6")))

