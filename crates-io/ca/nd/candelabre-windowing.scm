(define-module (crates-io ca nd candelabre-windowing) #:use-module (crates-io))

(define-public crate-candelabre-windowing-0.2.0 (c (n "candelabre-windowing") (v "0.2.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.24.0") (d #t) (k 0)))) (h "0vkxqizqgbgbqpkrifz05s8nhsn81fn1wyvxh9gw3xxybc1zs0l2")))

(define-public crate-candelabre-windowing-0.2.1 (c (n "candelabre-windowing") (v "0.2.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.24.0") (d #t) (k 0)))) (h "1l144aapkc93r8lagld12l3kck1ag6x4p9cn7krdkldr6rc9ksl3")))

