(define-module (crates-io ca nd candle-kernels) #:use-module (crates-io))

(define-public crate-candle-kernels-0.1.0 (c (n "candle-kernels") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1gsxd8hjcb383rbh3lm6dvxqfq3rb5xg4q22w0g04d6ndndjkw7q")))

(define-public crate-candle-kernels-0.1.1 (c (n "candle-kernels") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1hplcxnj72r4w1ghgc292ym30zwqqchrvwl2fxvznykn98b8gba8")))

(define-public crate-candle-kernels-0.1.2 (c (n "candle-kernels") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1y6cxzc4dg7wiwr38dpdzldncmis9sgfhq3c58ga22g9d1mczf4c")))

(define-public crate-candle-kernels-0.2.0 (c (n "candle-kernels") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1gghvf6zcr8nbi6a5cgw9srw1w2miycbqd3p7aqpxhmhllc12n48")))

(define-public crate-candle-kernels-0.2.1 (c (n "candle-kernels") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "0mq6db3i609db6ljw2b9hyhfbkwkdfrc224x1amdv71pknc0xy4n")))

(define-public crate-candle-kernels-0.2.2 (c (n "candle-kernels") (v "0.2.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "0l1mcsbhhrwzpwhswpfhzl0lm8v2c84661lw13962rh5gpw7r1ry")))

(define-public crate-candle-kernels-0.3.0 (c (n "candle-kernels") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "11h2shjh0h6afi3mv0vdr7ax4v1spf5zmydrahmssirclwidxzdz")))

(define-public crate-candle-kernels-0.3.1 (c (n "candle-kernels") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1jg6bqp62l8x438xchzazx3swg3ggxkfsclkvc3g3ifppq4izs8w")))

(define-public crate-candle-kernels-0.3.2 (c (n "candle-kernels") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rayon") (r "^1.7.0") (d #t) (k 1)))) (h "1lx80v9wzwg0d7afnyij7d82ci5is7850rnhn5cm3f4f6x2xg9as")))

(define-public crate-candle-kernels-0.3.3 (c (n "candle-kernels") (v "0.3.3") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)))) (h "1snv3a1ajyairdzp1zpj9r6vq3ydmjjxng1mc6x303f63igxs36q")))

(define-public crate-candle-kernels-0.4.0 (c (n "candle-kernels") (v "0.4.0") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)))) (h "1pm4vcxkj6l5my4r7rcx2fyq2rpl0x5i66hks05g7m04rvblhaas")))

(define-public crate-candle-kernels-0.4.1 (c (n "candle-kernels") (v "0.4.1") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)))) (h "1chmb2ybls27zq079bq2s6x87dd17dwj63kanmrvnkhn8hd92iaq")))

(define-public crate-candle-kernels-0.5.0 (c (n "candle-kernels") (v "0.5.0") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)))) (h "0x339r96hxjmls5kyhlc0np30z1nmgsl30np1ch6z6kchg0n152s")))

(define-public crate-candle-kernels-0.5.1 (c (n "candle-kernels") (v "0.5.1") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)))) (h "0qdny9axjqql71pbnlmbavlrvjmxd3zd1x5hj2zbi3r9r94b1d6k")))

