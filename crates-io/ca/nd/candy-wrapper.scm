(define-module (crates-io ca nd candy-wrapper) #:use-module (crates-io))

(define-public crate-candy-wrapper-0.1.0 (c (n "candy-wrapper") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.10.1") (d #t) (k 0)))) (h "1ka2azygmc10z7932vqlih7qxyx8p4ks2daqqdmixgj2pj1ir3sw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-candy-wrapper-0.1.1 (c (n "candy-wrapper") (v "0.1.1") (d (list (d (n "solana-program") (r "^1.9.13") (d #t) (k 0)))) (h "05cq26cm2x4fmsy5wsl3mwj8s17mgsj2ff1bf4s1pf16lwl3knj7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

