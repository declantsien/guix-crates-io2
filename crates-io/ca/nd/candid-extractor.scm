(define-module (crates-io ca nd candid-extractor) #:use-module (crates-io))

(define-public crate-candid-extractor-0.1.0 (c (n "candid-extractor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)) (d (n "wasmtime") (r "^12") (d #t) (k 0)))) (h "1ksy6px0l2bbkh1y978x6w84pc48j10h32i5d93gm9ldmvsr1k10") (r "1.65.0")))

(define-public crate-candid-extractor-0.1.1 (c (n "candid-extractor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)) (d (n "wasmtime") (r "^12") (d #t) (k 0)))) (h "05wcrbjasqa2z2iysqyl2fynqb5zfbvvglwp43pks6jgpzls82n1") (r "1.65.0")))

(define-public crate-candid-extractor-0.1.2 (c (n "candid-extractor") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)) (d (n "wasmtime") (r "^12") (d #t) (k 0)))) (h "0j1zjkh3anj4x9x0j50b0939d45lj94mgkmmyf0z9m3mxc8pwd6w") (r "1.66.0")))

(define-public crate-candid-extractor-0.1.3 (c (n "candid-extractor") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)) (d (n "wasmtime") (r "^19") (d #t) (k 0)))) (h "0kyssxq12r8d16vv5bihad8563cspgwa0d7n2kvy6kyx8r67kjjk") (r "1.75.0")))

(define-public crate-candid-extractor-0.1.4 (c (n "candid-extractor") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)) (d (n "wasmtime") (r "^19") (d #t) (k 0)))) (h "0v0cwmssh352f7m25k2n7c2s4m35wr8vshmlwmzlj4ad7d5wzcly") (r "1.75.0")))

