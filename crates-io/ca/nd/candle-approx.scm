(define-module (crates-io ca nd candle-approx) #:use-module (crates-io))

(define-public crate-candle-approx-0.1.0 (c (n "candle-approx") (v "0.1.0") (d (list (d (n "candle-core") (r "^0.3.2") (d #t) (k 0)))) (h "1mbyk51lfafcmq8n7yxhwnhg899l5gd5gzfh1jid9c6bf2ciqs7s")))

(define-public crate-candle-approx-0.2.0 (c (n "candle-approx") (v "0.2.0") (d (list (d (n "candle-core") (r "^0.4.1") (d #t) (k 0)))) (h "1zpkkj0wswrl0xigra7byn4fvgbb0zkliv1y7k1sx47x92yq8kha")))

(define-public crate-candle-approx-0.5.0 (c (n "candle-approx") (v "0.5.0") (d (list (d (n "candle-core") (r "^0.5.0") (d #t) (k 0)))) (h "0d05fsadbdgb3hkp9aid7aik7iis15p0wy72g2jkaklapapcvs58")))

(define-public crate-candle-approx-0.5.1 (c (n "candle-approx") (v "0.5.1") (d (list (d (n "candle-core") (r "^0.5.0") (d #t) (k 0)))) (h "0w1df627fi03nb99iz8g7vd03dsipc3gpccnq5mpgy2gycawa72j")))

