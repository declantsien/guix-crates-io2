(define-module (crates-io ca nd candidateparser) #:use-module (crates-io))

(define-public crate-candidateparser-0.1.0 (c (n "candidateparser") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.162") (o #t) (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1ml7if4fkgp5bq87ggn0mgr88pnxnflz4fkx683akzp3qjafgqcx") (f (quote (("default"))))))

(define-public crate-candidateparser-0.1.1 (c (n "candidateparser") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.162") (o #t) (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0biaqbjqzhh8xs07m8bsrb609mmf7s1py5vn98gqq4l72a33dqq1") (f (quote (("default"))))))

(define-public crate-candidateparser-0.2.0 (c (n "candidateparser") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.163") (o #t) (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1jjn0s3x443dkpvs0x2jygaz1qcc052ysdmppys7ykgzwa2bgnzm") (f (quote (("default"))))))

