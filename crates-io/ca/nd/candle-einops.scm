(define-module (crates-io ca nd candle-einops) #:use-module (crates-io))

(define-public crate-candle-einops-0.1.0 (c (n "candle-einops") (v "0.1.0") (d (list (d (n "candle-core") (r "^0.4") (d #t) (k 0)) (d (n "candle-einops-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "candle-nn") (r "^0.4") (d #t) (k 0)))) (h "06syqa145bppjnzz6hd3b0iwyw7nvnw0p6amgzi3djdrqivh9kn7")))

(define-public crate-candle-einops-0.1.1 (c (n "candle-einops") (v "0.1.1") (d (list (d (n "candle-core") (r "^0.4") (d #t) (k 0)) (d (n "candle-einops-macros") (r "^0.1.1") (d #t) (k 0)))) (h "01nca389r2zmiyf0czmhj5m9d5wh63fl5gy5f9aazr5kxxxrpad1")))

