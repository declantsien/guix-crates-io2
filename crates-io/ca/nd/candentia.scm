(define-module (crates-io ca nd candentia) #:use-module (crates-io))

(define-public crate-candentia-0.1.0 (c (n "candentia") (v "0.1.0") (d (list (d (n "ansi_term") (r "0.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log4rs") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "walkdir") (r "2.*") (d #t) (k 0)))) (h "1s163xn7s44w2267x21q8xb6nhwxv9sl16ag38hhs8lkwgjg3h7c")))

(define-public crate-candentia-0.2.0 (c (n "candentia") (v "0.2.0") (d (list (d (n "ansi_term") (r "0.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log4rs") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "walkdir") (r "2.*") (d #t) (k 0)))) (h "0xdsvwq9rnkqpc49a7zikrjmg1xfhy31jccm2fxpc6g77adig7kd")))

