(define-module (crates-io ca nd candle-einops-macros) #:use-module (crates-io))

(define-public crate-candle-einops-macros-0.1.0 (c (n "candle-einops-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "040n0zs15a26awvyhzlh05g063vjdrsqd8xcjp7jr0hkhghhss12")))

(define-public crate-candle-einops-macros-0.1.1 (c (n "candle-einops-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fd5vzc0vfjrsq7ahrkcgsz9jvh38r0wwh86b0znchxnl8j5wiwv")))

