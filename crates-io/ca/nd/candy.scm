(define-module (crates-io ca nd candy) #:use-module (crates-io))

(define-public crate-candy-0.1.0 (c (n "candy") (v "0.1.0") (h "0fxyl1lwa5fma4czcpki5bk98m318vh6pzadyfjf0vhkfdzwwdik") (f (quote (("nightly") ("default"))))))

(define-public crate-candy-0.1.1 (c (n "candy") (v "0.1.1") (h "1ldbzixjiw0lgscz75cjz3wbbmx0gn6hdg5s7yyf69m0sy2izry1") (f (quote (("nightly") ("default"))))))

(define-public crate-candy-0.1.2 (c (n "candy") (v "0.1.2") (h "1ci895ykw9rknqjy9ndwy06rl24yqd0gqcs45bj8rl696rrcsdiq") (f (quote (("nightly") ("default"))))))

(define-public crate-candy-0.1.3 (c (n "candy") (v "0.1.3") (h "0ah7a3n5bwr1j6nvqqzj8mw0blk9nrjg4vng057ljpxykiyyyimi") (f (quote (("try-trait" "nightly") ("nightly") ("default"))))))

(define-public crate-candy-0.1.4 (c (n "candy") (v "0.1.4") (h "0rjbwg7zb04hxpcrb4yyim7icr5qzikdigklpr070gsllixczd5z") (f (quote (("try-trait" "nightly") ("nightly") ("default"))))))

(define-public crate-candy-0.1.5 (c (n "candy") (v "0.1.5") (h "05ksc27hv80nqgc9654i2i3307j2j9hlk59lpp48svwy3ylh8scg") (f (quote (("try-trait" "nightly") ("nightly") ("default"))))))

