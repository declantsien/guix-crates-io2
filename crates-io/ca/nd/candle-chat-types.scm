(define-module (crates-io ca nd candle-chat-types) #:use-module (crates-io))

(define-public crate-candle-chat-types-0.1.0 (c (n "candle-chat-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "candle-core") (r "^0.3.1") (d #t) (k 0)) (d (n "candle-examples") (r "^0.3.1") (d #t) (k 0)) (d (n "candle-nn") (r "^0.3.1") (d #t) (k 0)) (d (n "candle-transformers") (r "^0.3.1") (d #t) (k 0)) (d (n "hf-hub") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "tokenizers") (r "^0.13.4") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0mbycmk21wp9jlr51h1x8vqdbx1swwyb3ykhya1ibnhj51h5yqp0") (y #t)))

