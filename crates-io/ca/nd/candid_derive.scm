(define-module (crates-io ca nd candid_derive) #:use-module (crates-io))

(define-public crate-candid_derive-0.1.0 (c (n "candid_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "01q7v6mnmlfjq2yc0c84qbrnjy7vv0piswlps17rhmmampvadx32")))

(define-public crate-candid_derive-0.1.1 (c (n "candid_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1g0vjw0jlzlqinydp6lr3hzk9a5p85yxpzv1hlwp030b4c5n140b")))

(define-public crate-candid_derive-0.2.0 (c (n "candid_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0kj0is0d15r012mf9bks7g0zw2dig1krnmqdbk2p4kgv9ls3f7y4")))

(define-public crate-candid_derive-0.2.1 (c (n "candid_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "026ab06z5grlxvwvfczcbk629qzhpmlvrdazavwyv0y9n27c8pba")))

(define-public crate-candid_derive-0.2.2 (c (n "candid_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0si11a5dnm1x9bbkbkhf12pcsmzijq0xcxhlhkp03fkpwmn3hpkw")))

(define-public crate-candid_derive-0.2.3 (c (n "candid_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0hggdb72hmkfq3g5p7irw0qmjik7xb3md1vh8c2nwxpmab9s1w0m")))

(define-public crate-candid_derive-0.2.4 (c (n "candid_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1jdd7ybv8rys5jia6abhx7y44ddw6s32zf6m75f2ji8f5jcghm1n") (y #t)))

(define-public crate-candid_derive-0.3.0 (c (n "candid_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0c0k4pvhchyxy2p6f6hnccnqhfchyix9d1pwmqj3dc5m4knna7d5")))

(define-public crate-candid_derive-0.4.0 (c (n "candid_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit"))) (d #t) (k 0)))) (h "17v79ynkqrg71l6yz4ja9la59an34c6zp6hk9amln65i7apwqcdb")))

(define-public crate-candid_derive-0.4.1 (c (n "candid_derive") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1cw9330yrqz9w2w7x9dgby2grj2dpvsmi1k16wlqwpbpg98vlkg0") (f (quote (("cdk"))))))

(define-public crate-candid_derive-0.4.2 (c (n "candid_derive") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0m1cx0dgk40zkryb94bc7rjsdpij04s7f0h2fa5g8gsl487460sn") (f (quote (("cdk"))))))

(define-public crate-candid_derive-0.4.3 (c (n "candid_derive") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "04hpxphh6b8hkxcjgpb85i6wvkyxavgwyld511w0b1y5vczi3iz3") (f (quote (("cdk"))))))

(define-public crate-candid_derive-0.4.4 (c (n "candid_derive") (v "0.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1qkad1dmg0iavjkspgqidymsyafp88dhcygxkyp86vc3vnjk4wld") (f (quote (("cdk"))))))

(define-public crate-candid_derive-0.4.5 (c (n "candid_derive") (v "0.4.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0qchrbx64sph36hdl5k2cfz1z1s9zcw9a47kyfip8xjl9lyc00if") (f (quote (("cdk"))))))

(define-public crate-candid_derive-0.5.0 (c (n "candid_derive") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1pahagaa061g1rvgdggqrrrxvjf2qmfmr8xkf1xvh13xgkdz9waq")))

(define-public crate-candid_derive-0.6.0 (c (n "candid_derive") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1hg4m2jdcglr6k4hir0dw8xihf48c0gd579x4jdxwi0aifn4dprl")))

(define-public crate-candid_derive-0.6.1 (c (n "candid_derive") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0n5gi36b5sdcb6yjql6vr1wmn9sgdyd91clrb01h19200w1f2704")))

(define-public crate-candid_derive-0.6.2 (c (n "candid_derive") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1mbx9w26jmg5dvj47r6zf8p8j8wx1b1wgzsj0q4q5wj40bb3n2w1")))

(define-public crate-candid_derive-0.6.3 (c (n "candid_derive") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "12dlzzsc9axwpk6ga1qld28hlra7f3mnfpbswi591dgs73m0710m")))

(define-public crate-candid_derive-0.6.4 (c (n "candid_derive") (v "0.6.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "082hk8y6gdpvvbs76llafnqzk4la1z5r9b2k4s1d1b81v6ca87i0")))

(define-public crate-candid_derive-0.6.5 (c (n "candid_derive") (v "0.6.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "023szfyyxip8bawwy1y17bmmj2k2bgmg7fsxxzvscbxam06j434p")))

(define-public crate-candid_derive-0.6.6 (c (n "candid_derive") (v "0.6.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1agiim19vq52k2wky14sgx3q3i33hyv8g64xlpkjcrrq1ibriqrx") (r "1.70.0")))

