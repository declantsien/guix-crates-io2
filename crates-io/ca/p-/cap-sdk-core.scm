(define-module (crates-io ca p- cap-sdk-core) #:use-module (crates-io))

(define-public crate-cap-sdk-core-0.1.0-alpha1 (c (n "cap-sdk-core") (v "0.1.0-alpha1") (d (list (d (n "cap-common") (r "^0.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.2") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04rkkpdlb1vyygfask5xx5yk9hfkr2gzaccj7k2y349j9qzqza2a")))

(define-public crate-cap-sdk-core-0.2.2 (c (n "cap-sdk-core") (v "0.2.2") (d (list (d (n "cap-common") (r "^0.2.2") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.2") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10f0fi597nfpn10fyny0q4rz0vnx7z3m4pmyawzf0wx27ai84v2q")))

(define-public crate-cap-sdk-core-0.2.1 (c (n "cap-sdk-core") (v "0.2.1") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "cap-common") (r "^0.2") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c12mqnyr7k4q4gs7r1kdhc5536j9gq6w8xz9kcmr553843b2jbm")))

(define-public crate-cap-sdk-core-0.2.3 (c (n "cap-sdk-core") (v "0.2.3") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "cap-common") (r "^0.2") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "124nc178wz8jx6xq81bv89wdyzvigyyibcvrsirwjm7hm46q1niv")))

