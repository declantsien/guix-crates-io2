(define-module (crates-io ca p- cap-standards) #:use-module (crates-io))

(define-public crate-cap-standards-0.1.0 (c (n "cap-standards") (v "0.1.0") (h "09wzw2gbw5j9qf3wjxnbknq89127iwm8pcd2nyskcgs5adgjrxcm")))

(define-public crate-cap-standards-0.1.0-alpha1 (c (n "cap-standards") (v "0.1.0-alpha1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "cap-sdk") (r "^0.1.0-alpha1") (o #t) (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zw3bd4cgh4aw3m14ccmpbrxyj4k8d8x8f55q9paljfzkbxjsq1m") (f (quote (("sdk-impls" "cap-sdk") ("default" "sdk-impls") ("alpha-xtc") ("alpha-dip721") ("alpha-dip20-dank" "alpha-dip20") ("alpha-dip20" "num-bigint"))))))

(define-public crate-cap-standards-0.2.0 (c (n "cap-standards") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "cap-sdk") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ciwvp14386aw7l1mdzpbgw2nyw2r226x0da5h8zanc17wypdpq6") (f (quote (("sdk-impls" "cap-sdk") ("default" "sdk-impls") ("alpha-xtc") ("alpha-dip721") ("alpha-dip20-dank" "alpha-dip20") ("alpha-dip20" "num-bigint"))))))

