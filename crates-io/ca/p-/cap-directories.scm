(define-module (crates-io ca p- cap-directories) #:use-module (crates-io))

(define-public crate-cap-directories-0.1.0 (c (n "cap-directories") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "cap-std") (r "^0.1.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18vmxmabsjacrgr7m03sx733z6s6wvm824zysg997aj65a8h6pmp")))

(define-public crate-cap-directories-0.2.0 (c (n "cap-directories") (v "0.2.0") (d (list (d (n "cap-std") (r "^0.2.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wn9kbxq6c3qygg0w71ag5y27lcbvx1jxjdsfbl97da4pxv2ja1w")))

(define-public crate-cap-directories-0.3.0 (c (n "cap-directories") (v "0.3.0") (d (list (d (n "cap-std") (r "^0.3.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "14djqay7lkqzphff3y57vlyi5gyzr9qfwfb4a2w9fak7cgkkag53")))

(define-public crate-cap-directories-0.4.0 (c (n "cap-directories") (v "0.4.0") (d (list (d (n "cap-std") (r "^0.4.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "048kpnqj9c98adk5g3hz51q0qrp2v6vwh7qa8j2x9w4bmsz4z9y3")))

(define-public crate-cap-directories-0.5.0 (c (n "cap-directories") (v "0.5.0") (d (list (d (n "cap-std") (r "^0.5.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0ak0c6f2d7fin0z3j5jfak3baa39szl8hjlca1n8rsw42wghypa5")))

(define-public crate-cap-directories-0.6.0 (c (n "cap-directories") (v "0.6.0") (d (list (d (n "cap-std") (r "^0.6.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0m6fwgk806rz742m2npyzbr7wdl39xhp9cq2syjhqaxdm7s13nnl")))

(define-public crate-cap-directories-0.7.0 (c (n "cap-directories") (v "0.7.0") (d (list (d (n "cap-std") (r "^0.7.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "19r0babb7whd73kjks7swbxmb7y93bl7fiamk4r01r78yhxg5sjh")))

(define-public crate-cap-directories-0.8.0 (c (n "cap-directories") (v "0.8.0") (d (list (d (n "cap-std") (r "^0.8.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "03k62ix79lck0b1qya16yjsnav1dgv5i09zwzr9z7hbknhj4dlk2")))

(define-public crate-cap-directories-0.9.0 (c (n "cap-directories") (v "0.9.0") (d (list (d (n "cap-std") (r "^0.9.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1rjjib2zygkvqi6qk3mkj5i522nznpi8cqm9pybjj1bnd6qxap3v")))

(define-public crate-cap-directories-0.10.0 (c (n "cap-directories") (v "0.10.0") (d (list (d (n "cap-std") (r "^0.10.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "07bkqagk7spbskzmlvkcplfn1fqav9bsiqgdbf1nfxg4v7akzjc6")))

(define-public crate-cap-directories-0.11.0 (c (n "cap-directories") (v "0.11.0") (d (list (d (n "cap-std") (r "^0.11.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1p19s62fbhwhizrim36iwwk11dgphiiw38v8gcm7qlhg7lfl30i7")))

(define-public crate-cap-directories-0.12.0 (c (n "cap-directories") (v "0.12.0") (d (list (d (n "cap-std") (r "^0.12.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0hjq2q8zlb6nz2lhysg3wln504fqivpl0vg3xaz1ylcp4mbfhcrs")))

(define-public crate-cap-directories-0.13.0 (c (n "cap-directories") (v "0.13.0") (d (list (d (n "cap-std") (r "^0.13.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "04hdq37kf4qf6adn6qfx8wik37gy12qjjjwclwvgxwy5jmnmr6gr")))

(define-public crate-cap-directories-0.13.2 (c (n "cap-directories") (v "0.13.2") (d (list (d (n "cap-std") (r "^0.13.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1w16kay1053ydc36rpx5wl8a6r9k861q1j9zc5jy8cv26iyrviy4")))

(define-public crate-cap-directories-0.13.4 (c (n "cap-directories") (v "0.13.4") (d (list (d (n "cap-std") (r "^0.13.4-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "07wcsm4z9npz4s8z2ns5g54mysryjygz5b26avcx0lnl8rq8z7nh")))

(define-public crate-cap-directories-0.13.6 (c (n "cap-directories") (v "0.13.6") (d (list (d (n "cap-std") (r "^0.13.5") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "18syiry5a9qr1fqd6b6pa7b18x0cirlp84aprqksq167zpl0k3nv")))

(define-public crate-cap-directories-0.13.7 (c (n "cap-directories") (v "0.13.7") (d (list (d (n "cap-std") (r "^0.13.7-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1lgbsmg8ngjw2hf4rn2k7gvsz49b1d2zda6wn3ff06c40l5n58yl")))

(define-public crate-cap-directories-0.13.8 (c (n "cap-directories") (v "0.13.8") (d (list (d (n "cap-std") (r "^0.13.8-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0b3kixp5gxm29fwgsfi2ls57vgjrallrdy15lmr3jh08npjkfl44")))

(define-public crate-cap-directories-0.13.9 (c (n "cap-directories") (v "0.13.9") (d (list (d (n "cap-std") (r "^0.13.8-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "05wf40lkpzwrl5646dnkyfc6b6y5bmmx79rs8c8wkp1rypy4hc9g")))

(define-public crate-cap-directories-0.13.10 (c (n "cap-directories") (v "0.13.10") (d (list (d (n "cap-std") (r "^0.13.10-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "09y0hyj0c1b049gr06jmpb8hm3m8b0wz61nihflrvwklyjx1f0j3")))

(define-public crate-cap-directories-0.14.0 (c (n "cap-directories") (v "0.14.0") (d (list (d (n "cap-std") (r "^0.14.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "126j2nyrq05qzd92ssq6kzlf4v1hpl0jbnxwk4500jy3kzsar7p3")))

(define-public crate-cap-directories-0.14.1 (c (n "cap-directories") (v "0.14.1") (d (list (d (n "cap-std") (r "^0.14.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.8.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0n3iyd4ivqrib4d42cir403ibv85pjhv45krf9y9q5p1p6vvnvvk")))

(define-public crate-cap-directories-0.15.0 (c (n "cap-directories") (v "0.15.0") (d (list (d (n "cap-std") (r "^0.15.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.14.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "18dw139gxb0h8qdmlw24z3vw3ch5f1acjgrsajrnzc9qja9dn1wk")))

(define-public crate-cap-directories-0.15.1 (c (n "cap-directories") (v "0.15.1") (d (list (d (n "cap-std") (r "^0.15.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.15.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1ivc79d71gk5gj4bz606bm1344aznyfdvbsykds9l7cs661y5r7q")))

(define-public crate-cap-directories-0.15.2 (c (n "cap-directories") (v "0.15.2") (d (list (d (n "cap-std") (r "^0.15.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.15.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1g29qf2qf5vlv5l8qz19rwbqgkz4zx3yywfxpwgpdzsi34j1mfx5")))

(define-public crate-cap-directories-0.16.0 (c (n "cap-directories") (v "0.16.0") (d (list (d (n "cap-std") (r "^0.16.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.16.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1ma8j7q2spzyhjhqilhczxix4flqhjr4dccl9l3p72fvs7kwvga1")))

(define-public crate-cap-directories-0.16.1 (c (n "cap-directories") (v "0.16.1") (d (list (d (n "cap-std") (r "^0.16.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.16.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1kpzf3zisikbzk7xk2pyby8gxmlqv4xqrrp5pk4dfijmmnhxvl38")))

(define-public crate-cap-directories-0.16.3 (c (n "cap-directories") (v "0.16.3") (d (list (d (n "cap-std") (r "^0.16.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "posish") (r "^0.16.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0jwqf1gv714x1mb7rj8q2xmbvxkr6pf00clg3d2kaiiq7f0l04pv")))

(define-public crate-cap-directories-0.17.0 (c (n "cap-directories") (v "0.17.0") (d (list (d (n "cap-std") (r "^0.17.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.18.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0qkqzcaalx0z8lvqv8r5d4d2smdddasaz9hb0i685qz36i8dbnqb")))

(define-public crate-cap-directories-0.18.0 (c (n "cap-directories") (v "0.18.0") (d (list (d (n "cap-std") (r "^0.18.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.20.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "17flm8yn6d9gll6pr2ndzdfiy79r4qamhw7k0ncdrxd7qjb80ag1")))

(define-public crate-cap-directories-0.18.1 (c (n "cap-directories") (v "0.18.1") (d (list (d (n "cap-std") (r "^0.18.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.21.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0i9nfx1i98c7szdrgwwxvih409rjckmr821w3ph35kajka1wdrfj")))

(define-public crate-cap-directories-0.19.0 (c (n "cap-directories") (v "0.19.0") (d (list (d (n "cap-std") (r "^0.19.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.22.4") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1y6ripa5d487xfbhx2z6qxd5n18fi9hj855hc1zrrdwjm0q8j0ya")))

(define-public crate-cap-directories-0.19.1 (c (n "cap-directories") (v "0.19.1") (d (list (d (n "cap-std") (r "^0.19.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.23.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0r7sxa4fmyzdzzs21n8m87piabl7c4h28dqv9macs2mxj9j5x1k7")))

(define-public crate-cap-directories-0.20.0 (c (n "cap-directories") (v "0.20.0") (d (list (d (n "cap-std") (r "^0.20.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rsix") (r "^0.23.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0rlgp12fvh4jci943v6jrl465x5xp5cwkybd1xf4z8xxlh97rj4r")))

(define-public crate-cap-directories-0.21.0 (c (n "cap-directories") (v "0.21.0") (d (list (d (n "cap-std") (r "^0.21.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.26.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "061hsry2f442yv6cgq1330f9aw3yafa4j63zhvn21w3qmvmmb4zc")))

(define-public crate-cap-directories-0.21.1 (c (n "cap-directories") (v "0.21.1") (d (list (d (n "cap-std") (r "^0.21.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.26.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "06wvzkmm7w7yk6x6k5nv442vzgykksjjdl4kxm3xv2yaxlfr1yc6")))

(define-public crate-cap-directories-0.22.0 (c (n "cap-directories") (v "0.22.0") (d (list (d (n "cap-std") (r "^0.22.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.31.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0y5vj7ac9kbjn00rsjxpmlam97kywq9bpmyw0v09g7xx0jhk8lap")))

(define-public crate-cap-directories-0.22.1 (c (n "cap-directories") (v "0.22.1") (d (list (d (n "cap-std") (r "^0.22.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.31.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "15haas9dad3ssxigk2q7zg0xdis11ygip0r9s60x1bxdgp1pkfvj")))

(define-public crate-cap-directories-0.23.0 (c (n "cap-directories") (v "0.23.0") (d (list (d (n "cap-std") (r "^0.23.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.32.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0vbr2qin4nb8ih7vzc0jinjmjpjq24xzp9zi0sx25ylpnj1pdlaw")))

(define-public crate-cap-directories-0.23.1 (c (n "cap-directories") (v "0.23.1") (d (list (d (n "cap-std") (r "^0.23.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.32.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0ls8hh2d36l9hss28kf378rj3wp7ivdfq4f7yjlm14ibk6caxg5l")))

(define-public crate-cap-directories-0.24.0 (c (n "cap-directories") (v "0.24.0") (d (list (d (n "cap-std") (r "^0.24.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0q4nxvzzqqjys27iagn1cia3cypvc658kf15ybm7k7bpzkkxaxih")))

(define-public crate-cap-directories-0.24.1 (c (n "cap-directories") (v "0.24.1") (d (list (d (n "cap-std") (r "^0.24.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0srm08svxxw5iqchq2mydzvfwr4m2bfzikyvin8pnq6zyq4w0w2z")))

(define-public crate-cap-directories-0.24.2 (c (n "cap-directories") (v "0.24.2") (d (list (d (n "cap-std") (r "^0.24.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "17aj3717j7vl35vlxxh6qcrzrcm9q7qh5dcc4hd1d51xq2aknmvb")))

(define-public crate-cap-directories-0.24.3 (c (n "cap-directories") (v "0.24.3") (d (list (d (n "cap-std") (r "^0.24.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1b8v5zvxn13yg2sn0hiwzsmf0dqzaydpx8rggcz03cl0qx9pzk64")))

(define-public crate-cap-directories-0.24.4 (c (n "cap-directories") (v "0.24.4") (d (list (d (n "cap-std") (r "^0.24.1-alpha.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1pf46axac4xx6757izryvn6bfip3qsmdd6wf571fsz52scn02cky")))

(define-public crate-cap-directories-0.25.0 (c (n "cap-directories") (v "0.25.0") (d (list (d (n "cap-std") (r "^0.25.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0w0y1pxw053gqkich2h88n063p5r5sw2fi9hwbrhg3an9wmxv4rv")))

(define-public crate-cap-directories-0.25.1 (c (n "cap-directories") (v "0.25.1") (d (list (d (n "cap-std") (r "^0.25.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rzqhp3nxznl1xlwibj06lgpn1837fj85s7bryl82rhr0xifshhs")))

(define-public crate-cap-directories-0.25.2 (c (n "cap-directories") (v "0.25.2") (d (list (d (n "cap-std") (r "^0.25.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1daz5x0c8dgwxx4cx6ra83sa7zxppnlldg71b3q5xjdav3hi0z92")))

(define-public crate-cap-directories-1.0.0-rc1 (c (n "cap-directories") (v "1.0.0-rc1") (d (list (d (n "cap-std") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0-rc1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "071i4pna84kh1ns4ippqvi6wbaqclx65y251pq9r94x7hvcj14x3")))

(define-public crate-cap-directories-0.26.0-patch0 (c (n "cap-directories") (v "0.26.0-patch0") (d (list (d (n "cap-std") (r "^0.26.0-patch0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f6w81xfr5dpgp48jbv5wz72fqqs781zak5nnb4j9jdfszk1scp3")))

(define-public crate-cap-directories-0.26.0-patch1 (c (n "cap-directories") (v "0.26.0-patch1") (d (list (d (n "cap-std") (r "^0.26.0-patch0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n7f0ap5l479f46dk457qm4xjsyvx21msf64bv85q3afc6z3606j")))

(define-public crate-cap-directories-0.25.3 (c (n "cap-directories") (v "0.25.3") (d (list (d (n "cap-std") (r "^0.25.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "058a5byr648jbrshdrcs340qwx7zp7hilmjxz9c4364s4hvw8xyq")))

(define-public crate-cap-directories-0.26.0-patch2 (c (n "cap-directories") (v "0.26.0-patch2") (d (list (d (n "cap-std") (r "^0.26.0-patch0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09bgjcscib4kdc8inq0cfqslcb2sghh32hzfk5amb1bv5lw6ic09")))

(define-public crate-cap-directories-0.26.0 (c (n "cap-directories") (v "0.26.0") (d (list (d (n "cap-std") (r "^0.26.0-patch0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cj3dlngbkiri79s0vl23m1mlfna60vlqw2j6z7cvf4mj47hfn55")))

(define-public crate-cap-directories-1.0.0-rc2 (c (n "cap-directories") (v "1.0.0-rc2") (d (list (d (n "cap-std") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0-rc2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p99dcaqhj85qp8iqlc0p2i814qja4qc7f5kpsad0kqgibxzn136")))

(define-public crate-cap-directories-0.26.1 (c (n "cap-directories") (v "0.26.1") (d (list (d (n "cap-std") (r "^0.26.0-patch0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.35.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "156dmfn8fx0isi9qgghcq20mh9d9lfxy9dpm6sp44s2h9v5g3igm")))

(define-public crate-cap-directories-1.0.0 (c (n "cap-directories") (v "1.0.0") (d (list (d (n "cap-std") (r "^1.0.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10lhf3942zji4b4dh29302k141fn0mcnnwf3mq9xq07sph4hvcqk")))

(define-public crate-cap-directories-1.0.1 (c (n "cap-directories") (v "1.0.1") (d (list (d (n "cap-std") (r "^1.0.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a7kn9iniq6w5yhp3myv7q5kb1l2vg4nqygd1sb8hg3bmrcwi4xr")))

(define-public crate-cap-directories-1.0.2 (c (n "cap-directories") (v "1.0.2") (d (list (d (n "cap-std") (r "^1.0.2") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0377731ii4h17qr0mr46fk8h6gar72jghq4dmmbiqfp30bbb92zm")))

(define-public crate-cap-directories-1.0.3 (c (n "cap-directories") (v "1.0.3") (d (list (d (n "cap-std") (r "^1.0.3") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fjniz69ikw7zrbybgj1nhysamvjzzw3mq3gss5r1r8244if59fa")))

(define-public crate-cap-directories-1.0.4 (c (n "cap-directories") (v "1.0.4") (d (list (d (n "cap-std") (r "^1.0.4") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0g903x6fyfwzib12x7mb922h2bmrpyn7hqg40ilqfvp2zhi17v2c")))

(define-public crate-cap-directories-1.0.5 (c (n "cap-directories") (v "1.0.5") (d (list (d (n "cap-std") (r "^1.0.5") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l4vs6n5kyszj212g2mc82ynz1579fpc7ashc6rbd14n5r678din")))

(define-public crate-cap-directories-1.0.6 (c (n "cap-directories") (v "1.0.6") (d (list (d (n "cap-std") (r "^1.0.6") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10gds9dn31ygi0767xf102l9i0l0b8db0jhs6pzza0z0npcan3jf")))

(define-public crate-cap-directories-1.0.7 (c (n "cap-directories") (v "1.0.7") (d (list (d (n "cap-std") (r "^1.0.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00j2rymb9a9hdcmd4dqrxbxv9ir6rdysf2mlaa416i74n2kxkpbz")))

(define-public crate-cap-directories-1.0.8 (c (n "cap-directories") (v "1.0.8") (d (list (d (n "cap-std") (r "^1.0.8") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cg9a3g0ca3g9c4gn3sg97hh6pd0848z2aaba8qpfac8vm0r8gkn")))

(define-public crate-cap-directories-1.0.9 (c (n "cap-directories") (v "1.0.9") (d (list (d (n "cap-std") (r "^1.0.9") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0k4g6nwg0cm065vc91p5z3raz0mkcgfmxdwx3v1nz2ca309wa3pv")))

(define-public crate-cap-directories-1.0.10 (c (n "cap-directories") (v "1.0.10") (d (list (d (n "cap-std") (r "^1.0.10") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "044ayp8cdc8159fp8jc2cxrafmb8k0vhkqplrs88xjbpj5lc7m5p")))

(define-public crate-cap-directories-1.0.11 (c (n "cap-directories") (v "1.0.11") (d (list (d (n "cap-std") (r "^1.0.11") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "076ic7g9qnpghbq6kjbzy15ka366lv62hr2yg2s9kkvkcqyc99w8")))

(define-public crate-cap-directories-1.0.12 (c (n "cap-directories") (v "1.0.12") (d (list (d (n "cap-std") (r "^1.0.12") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d4zxnqqhp4qn6h7bwlc0mf914csjabr7cyyk4w0yjshf1ni76g5")))

(define-public crate-cap-directories-1.0.13 (c (n "cap-directories") (v "1.0.13") (d (list (d (n "cap-std") (r "^1.0.13") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "029gw3jmppgakhpi4lv8rzww73plgnha8qax90dld7dvjai982zx")))

(define-public crate-cap-directories-1.0.14 (c (n "cap-directories") (v "1.0.14") (d (list (d (n "cap-std") (r "^1.0.14") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "173zvghvbz3f8dyhnwxcxn7av2c3ppzzjbi42c0d6mfrvmhln6cj")))

(define-public crate-cap-directories-1.0.15 (c (n "cap-directories") (v "1.0.15") (d (list (d (n "cap-std") (r "^1.0.15") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.37.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0k18i1q3948c9yidvixciwf4c1z5r9p2xj6l6mhqq2wch6wkbc14")))

(define-public crate-cap-directories-1.0.16 (c (n "cap-directories") (v "1.0.16") (d (list (d (n "cap-std") (r "^1.0.16") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i1f5bvqggr3128fzxqhi3ipbba3b7527avffrzp46iw407114yr") (y #t)))

(define-public crate-cap-directories-2.0.0 (c (n "cap-directories") (v "2.0.0") (d (list (d (n "cap-std") (r "^2.0.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1796vf6ia192ma62q1hnic31sd2hsj3jgspvvfbwma3rfp88h98q")))

(define-public crate-cap-directories-2.0.1 (c (n "cap-directories") (v "2.0.1") (d (list (d (n "cap-std") (r "^2.0.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ad0irddzx5pz3qwvplffm33c31mylrrqs37swn28i3fqyxrkflp")))

(define-public crate-cap-directories-3.0.0 (c (n "cap-directories") (v "3.0.0") (d (list (d (n "cap-std") (r "^3.0.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w8haakbj75xs8pjf5a779xwl047jjficvbiigzyjp2q9ljnsw7b")))

(define-public crate-cap-directories-3.1.0 (c (n "cap-directories") (v "3.1.0") (d (list (d (n "cap-std") (r "^3.1.0") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06jr3i1dy9qi5pb2cwx4gmpav59vdixky73wbqjapgq58441ngzi")))

