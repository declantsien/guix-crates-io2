(define-module (crates-io ca p- cap-std-ext) #:use-module (crates-io))

(define-public crate-cap-std-ext-0.1.0 (c (n "cap-std-ext") (v "0.1.0") (d (list (d (n "rustix") (r "^0.31.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "05bmfrji9f6im3j4qzdzf2khy5m6pxx5di1zxzx6hgj9rkwf2c0x")))

(define-public crate-cap-std-ext-0.1.1 (c (n "cap-std-ext") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.23") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.23") (d #t) (k 2)) (d (n "rustix") (r "^0.31.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1mkn6zpz0xkrhl5xxyw65gjhrh925mgdgnr3fxixp1xprjyhir5r")))

(define-public crate-cap-std-ext-0.1.2 (c (n "cap-std-ext") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.23") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.23") (d #t) (k 2)) (d (n "rustix") (r "^0.32.1") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rfq3hrcy43l7l7acxbrbwcskk3mas38r75s25v1iviqias4ackx") (y #t)))

(define-public crate-cap-std-ext-0.23.0 (c (n "cap-std-ext") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.23") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.23") (d #t) (k 2)) (d (n "rustix") (r "^0.32.1") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "13r9251wl1sbf3x3zi3limg017s1dfahxyrjirck9p5n03zi58al")))

(define-public crate-cap-std-ext-0.23.1 (c (n "cap-std-ext") (v "0.23.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.23") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.23") (d #t) (k 2)) (d (n "rustix") (r "^0.32.1") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04dwdagwmgb9igv0vh2dxwsxj4wqadaar677dxghg8366r9fggkq")))

(define-public crate-cap-std-ext-0.24.0 (c (n "cap-std-ext") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.24") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.24") (d #t) (k 2)) (d (n "rustix") (r "^0.33.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hxx3ilij0ywn87d3h2mjilbfrkizm9lxihmvlvk8iq87llnmbph")))

(define-public crate-cap-std-ext-0.24.1 (c (n "cap-std-ext") (v "0.24.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.24") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.24") (d #t) (k 2)) (d (n "rustix") (r "^0.33.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sri88a641ibg7m26ag25ipkqcp9fmdb2k2yacdxj8zgaisl4kip")))

(define-public crate-cap-std-ext-0.24.2 (c (n "cap-std-ext") (v "0.24.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.24") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.24") (d #t) (k 2)) (d (n "rustix") (r "^0.33.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vydp0hk9ca7rvi7j7cxagk3dxdhvwm598qfyv2jaawgigilrkjz")))

(define-public crate-cap-std-ext-0.24.3 (c (n "cap-std-ext") (v "0.24.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.24") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.24") (d #t) (k 2)) (d (n "rustix") (r "^0.33.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z4cy7w0gpb4dqyrcp7k66p7dmpbnq32fxfyx430d4df74qjqm1m")))

(define-public crate-cap-std-ext-0.25.0 (c (n "cap-std-ext") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.24") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.24.3") (d #t) (k 0)) (d (n "rustix") (r "^0.33.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1rnkj7cadrg7zfmbnk7qrkls8jcawjxpdz4fyjdlmi3dc3dgvp5i")))

(define-public crate-cap-std-ext-0.26.0 (c (n "cap-std-ext") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-std") (r "^0.25") (d #t) (k 0)) (d (n "cap-tempfile") (r "^0.25") (d #t) (k 0)) (d (n "rustix") (r "^0.35.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0hbahidffkr5lnq1a2rq7akgd1nd5qms6gdmlsfa73vfk0dm3vxi") (y #t)))

(define-public crate-cap-std-ext-0.26.1 (c (n "cap-std-ext") (v "0.26.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^0.25.1") (d #t) (k 0)) (d (n "rustix") (r "^0.35.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0vrj734nbdai45iz78xkdd7lhjn76h5v9rrc90n04zfqpza53ldl")))

(define-public crate-cap-std-ext-0.26.2 (c (n "cap-std-ext") (v "0.26.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^0.25.1") (d #t) (k 0)) (d (n "rustix") (r "^0.35.0") (f (quote ("procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0ab576cw59h9hmmwzd6bznfsxxrnzkqdhbllvxd8x0r9jpvdivyj")))

(define-public crate-cap-std-ext-1.0.0 (c (n "cap-std-ext") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^1.0.1") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.1") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0128gfarkd944h917viiww4kd7gnlzikmyyakjk9c1chrfarg3bj") (r "1.58.0")))

(define-public crate-cap-std-ext-1.0.1 (c (n "cap-std-ext") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^1.0.1") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0z9p8mj2gkbayjb0ng9ir5xbg78cmx5k7h9x0m838hcckqsxzfm0") (r "1.58.0")))

(define-public crate-cap-std-ext-1.0.2 (c (n "cap-std-ext") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^1.0.1") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "procfs"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1r61avmifqn1iihd0klpmh5670rvkw1i6gm1q70cp081fippc7fn") (r "1.58.0")))

(define-public crate-cap-std-ext-1.0.3 (c (n "cap-std-ext") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^1.0.1") (d #t) (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "procfs" "process"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1kkk8smndxmh3s484rwp44vfxqh2gz1vckn1w2mvvl3121pfncc0") (r "1.58.0")))

(define-public crate-cap-std-ext-2.0.0 (c (n "cap-std-ext") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^1.0.1") (d #t) (k 0)) (d (n "rustix") (r ">=0.36, <=0.37") (f (quote ("fs" "procfs" "process"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "09wc2lajmrpmciaf7c13fq73wnaf8p5wjb8y2pwcrqz34vkv24k0") (r "1.58.0")))

(define-public crate-cap-std-ext-3.0.0 (c (n "cap-std-ext") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-tempfile") (r "^2") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "procfs" "process" "pipe"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "15q0rls7250b49lyyk0yff789b8qvk4mjwnr4hgaq2k8xj6a8dl9") (r "1.70.0")))

(define-public crate-cap-std-ext-3.0.1 (c (n "cap-std-ext") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-primitives") (r "^2") (d #t) (k 0)) (d (n "cap-tempfile") (r "^2") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "procfs" "process" "pipe"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0zrmvxkqx9n9pp2zmg8y23qxf3hxgj65v4h5svi55fbj75r7jm4a") (r "1.70.0")))

(define-public crate-cap-std-ext-4.0.0 (c (n "cap-std-ext") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cap-primitives") (r "^3") (d #t) (k 0)) (d (n "cap-tempfile") (r "^3") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "procfs" "process" "pipe"))) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0n2f5pk1hnxdmi7d7mpgyz05y4i5ky7w00hcqdzbz37f43s5la61") (r "1.70.0")))

