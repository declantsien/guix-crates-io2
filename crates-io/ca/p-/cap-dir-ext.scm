(define-module (crates-io ca p- cap-dir-ext) #:use-module (crates-io))

(define-public crate-cap-dir-ext-0.3.0 (c (n "cap-dir-ext") (v "0.3.0") (d (list (d (n "arf-strings") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "cap-async-std") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "cap-primitives") (r "^0.3.0") (d #t) (k 0)) (d (n "cap-std") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0iikd5d692fq1iss5klcm7xgm93mq4afhxck8d2kzwv5wjqj6xm7") (f (quote (("std" "cap-std") ("fs_utf8" "arf-strings") ("default" "std") ("async_std" "cap-async-std"))))))

(define-public crate-cap-dir-ext-0.4.0 (c (n "cap-dir-ext") (v "0.4.0") (d (list (d (n "arf-strings") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "cap-async-std") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "cap-primitives") (r "^0.4.0") (d #t) (k 0)) (d (n "cap-std") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "106kv1lvsvprsmby2l8ypplxr3kggbbvf6r5vcdwcxj1dmr8d3x4") (f (quote (("std" "cap-std") ("fs_utf8" "arf-strings") ("default" "std") ("async_std" "cap-async-std"))))))

(define-public crate-cap-dir-ext-0.5.0 (c (n "cap-dir-ext") (v "0.5.0") (d (list (d (n "arf-strings") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "cap-async-std") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "cap-primitives") (r "^0.5.0") (d #t) (k 0)) (d (n "cap-std") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1bbj65p5c5bbf928vay073grj52q2jndqjvkws0l2bcm6vp7bxl5") (f (quote (("std" "cap-std") ("fs_utf8" "arf-strings") ("default" "std") ("async_std" "cap-async-std"))))))

(define-public crate-cap-dir-ext-0.6.0 (c (n "cap-dir-ext") (v "0.6.0") (d (list (d (n "arf-strings") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "cap-async-std") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "cap-primitives") (r "^0.6.0") (d #t) (k 0)) (d (n "cap-std") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1n5jfxrjyfd4q3w5r8simp4f2h15kx8639fda0yvb2483nh36ibm") (f (quote (("std" "cap-std") ("fs_utf8" "arf-strings") ("default" "std") ("async_std" "cap-async-std"))))))

(define-public crate-cap-dir-ext-0.7.0 (c (n "cap-dir-ext") (v "0.7.0") (h "193f3dx2700kpn162092w95404nwkm542vr1vfjndi941jvj77fc")))

