(define-module (crates-io ca p- cap-rand) #:use-module (crates-io))

(define-public crate-cap-rand-0.4.0 (c (n "cap-rand") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ddiha46g1q5sywja2i5b860995wyyiya0650y6fbw3hh1ac9gn1") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.5.0 (c (n "cap-rand") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "042zzjwijijdy6ng5vghwg7rxngdff70ily54q6nhz49qn1yfnmi") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.6.0 (c (n "cap-rand") (v "0.6.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1dra8lvr0s9ck0cb8qyrcwk3lpryrkz4ac23q0m8bkd23468k9sl") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.7.0 (c (n "cap-rand") (v "0.7.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0w7wskrdnznncknk6pfvhf76nz3890jx0fhfbgbyddrq0hq5zmy2") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.8.0 (c (n "cap-rand") (v "0.8.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1339hrlb44bbn651qdvh875mk82w4mcq3ijvh7qj52kl1j6dk2c2") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.9.0 (c (n "cap-rand") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1p932sj69zf9rnv8vfm8r3bcsli6rx1bqf4gas27k55wdjjhf4gg") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.10.0 (c (n "cap-rand") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "12rgy0wjryfznkrjgglwaijkksvi3pcr47xgfjm2gcsw9mr0zdvs") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.11.0 (c (n "cap-rand") (v "0.11.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1libz3rbdc03sgphqcankhi88aifpdfk9rxamg11ibdfbg0h3y9z") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.12.0 (c (n "cap-rand") (v "0.12.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "16j9szw6nc33fsfxmn35dd23xc5rmcdpiq49yvb232phipwfngkh") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.0 (c (n "cap-rand") (v "0.13.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1ydfp33jfl1jga8hsm7599snynhzpibnijyxwa2kirmbp0gh5mbp") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.2 (c (n "cap-rand") (v "0.13.2") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "06a042lgwrhmqi9ijycwff7clxkq1952w9bc8idhx1b3j9l56y6d") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.4 (c (n "cap-rand") (v "0.13.4") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "09mdfd2lmlcdyc47qf1030abfk9rs7w5g9b40qnz5rfnndwzl655") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.6 (c (n "cap-rand") (v "0.13.6") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0w4i27s7h2xw4zxr2bi6a05nlzcmqgdbyndwzad0liw99iv21vdx") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.7 (c (n "cap-rand") (v "0.13.7") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0bsxa3g735vqsgf2vgyplkhmg753wc9f0p0w0b3fzdwdndc80hd0") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.8 (c (n "cap-rand") (v "0.13.8") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1m2asgwcmdki019rk9gi0yg6gqndcym2xl5pi5j7f6c8sd5n6hzf") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.9 (c (n "cap-rand") (v "0.13.9") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1b5i9jvv4adigz9j8w5qpzy12ppni9h0bf5k06wql1rfk9nflhh8") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.13.10 (c (n "cap-rand") (v "0.13.10") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1r9aw2qf5warpsrxpa7glzgpg8ap17c8ia0dqq56s9z403nri3j5") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.14.0 (c (n "cap-rand") (v "0.14.0") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1wlp2qf186z6z15fc03s4mw1vxvmpgfbhi2pdw32fzwgaar2l03r") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.14.1 (c (n "cap-rand") (v "0.14.1") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0qcn23fb73wc2r6w70q55b30q6g4nld6plpwdp56hb6kl3vhj51q") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.15.0 (c (n "cap-rand") (v "0.15.0") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "02p1xb38k3bgyi2r9gb03pz5wp7p8265ik73azdg4csl03ri4sh8") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.15.1 (c (n "cap-rand") (v "0.15.1") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "14am158z58q196iaq4sfs0kmhgdwzxwd62i54791nngxr7i4r8pg") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.15.2 (c (n "cap-rand") (v "0.15.2") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1n1k77hb7cjknl45qhsrcikzm5rsrzdb35b4icc8pncz55l3yrwm") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.16.0 (c (n "cap-rand") (v "0.16.0") (d (list (d (n "ambient-authority") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "06wlwnrc2yhmgc1rjn1kjq4vv6frzv3izjz6jqs298mjj6p4hc05") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.16.1 (c (n "cap-rand") (v "0.16.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0p8hpdklaydrih5q0s1d60czkc8yml526wldx0lmhr58bxw74wgk") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.16.3 (c (n "cap-rand") (v "0.16.3") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "08sa6p2ldgwxdipdsvrszyd6lnzsjdqy52yrjpl7f02j0krqvahb") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.17.0 (c (n "cap-rand") (v "0.17.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0zznrp35zsdmfp1lqi85nz277y7nixyyndg6h0ygsknkp2sjm7an") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.18.0 (c (n "cap-rand") (v "0.18.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1g0yfigv84n2j6ssfh5h5biifqj43y3ji4zjljn20marlvszig32") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.18.1 (c (n "cap-rand") (v "0.18.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1wr83y75v2gpjdkwww8w272gibl9wa8cp19514ykfpic958mdjqa") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.19.0 (c (n "cap-rand") (v "0.19.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1lpg00rwj1ckj3yw8mvsj2c7h9ka5acdhlwdh3ibamwlj58y4f6f") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.19.1 (c (n "cap-rand") (v "0.19.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0i4ki078z6xrivr07zjarj921ks9d8gbhnd4lxnypsqc1g88jvkf") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.20.0 (c (n "cap-rand") (v "0.20.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0hdiiz7v85amcbksb06gvvqqgr6naj2ag3y92glbmk98y4fqzbm7") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.21.0 (c (n "cap-rand") (v "0.21.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1l41xc0b3fkvshy4445ahyhwkj7li9bl1kzlbwxxpq226g8igj3z") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.21.1 (c (n "cap-rand") (v "0.21.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "030mshk7qznsx4d9xfq90spg6xa2a9icyfb420xr11aaxc1skzgs") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.22.0 (c (n "cap-rand") (v "0.22.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1fr3dasillzyx51l38kar7fl69j16d356cvykiil0rvvcnvvwjni") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.22.1 (c (n "cap-rand") (v "0.22.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "11hjcfkqsjr6xdjknknx5dcjpgvgzrhwgx3zy87jxyqsy7zx9xrl") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.23.0 (c (n "cap-rand") (v "0.23.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0qdqxix3880axwlxzz8x3nakzsvbcbs5cbxw903456sl0jxl57jp") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.23.1 (c (n "cap-rand") (v "0.23.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0irlmsg88gf37p9b1msd595dadgnfkshw3750nf692yifbgz51bv") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.24.0 (c (n "cap-rand") (v "0.24.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "15aicq3cc9issl7lv7xc801c82gryq62dh1vn6x151v7g0mvqc1d") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.24.1 (c (n "cap-rand") (v "0.24.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0q8lf1ws5ip41i23j4cgnjgc8m5sclk2qazaq4rgpr5vpn9plqhj") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.24.2 (c (n "cap-rand") (v "0.24.2") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "152b91wv754zvpw7j05cysy7vcs4vs4l3bbarblsp078rdprh951") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.24.3 (c (n "cap-rand") (v "0.24.3") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "11wschspm96icrwcy5nb1zfrlmimgmkyz9naflnlplsqj2j9cpjp") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.24.4 (c (n "cap-rand") (v "0.24.4") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0wgrphbcz5h2gny2h37dvbszk10cs7v6i0acdxq3v60n84ljffya") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.25.0 (c (n "cap-rand") (v "0.25.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1nwvz7xfmgh53wr06sinjiaf8hzd9dyb6vfj3aflxwjspm9a06pz") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.25.1 (c (n "cap-rand") (v "0.25.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1xh3ka8bi8c2z3mlnc8yjkrgc2jiqg3qlc4sa04rjbm5rjmwnd61") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.25.2 (c (n "cap-rand") (v "0.25.2") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0zsfb02zl5r9vwpf9qlfmaxc2f8dm2v22dxvjp1n2w7gxy6kyr7g") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.0-rc1 (c (n "cap-rand") (v "1.0.0-rc1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "03r8g4licww0brahfc24aqcszll2wzqd7ai6hyikrm3fgmg5xk6i") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.26.0-patch0 (c (n "cap-rand") (v "0.26.0-patch0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1bkfqcsjcdd9pcnfyj2v026z2gfybx510j8vrjviir9838x9yx2b") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.26.0-patch1 (c (n "cap-rand") (v "0.26.0-patch1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "163g0dz8ncngbzs70mv45y9slqjjwd9ng5nzzymsmihf41jwfbkg") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.25.3 (c (n "cap-rand") (v "0.25.3") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0idfvjvpihj1q1lzhv02wpj6gsiwxy6f8zv4xi3fh2lz4j780867") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.26.0-patch2 (c (n "cap-rand") (v "0.26.0-patch2") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1m7nzz8nw2zbhb04mf72bpz6fdi4a72kha6fal3iq7db7zm4na84") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.26.0 (c (n "cap-rand") (v "0.26.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "11c4wx04cggrp6kasmlnpza6fz3967w70yi6p57s0bjmn6b290yj") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.0-rc2 (c (n "cap-rand") (v "1.0.0-rc2") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1vp1540sn4b7fh1nqqw2h2062sm2zcxqbsb536njavp6flimab02") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-0.26.1 (c (n "cap-rand") (v "0.26.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1skdxk965yia3lh2v6fvgcii9h1v411i8j5wa53d6l4mm839cjyi") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.0 (c (n "cap-rand") (v "1.0.0") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "00zsc155j8qyg7ki3mdbh56m0byqn4wyp892dx0552ms14kzdi4m") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.1 (c (n "cap-rand") (v "1.0.1") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1zg69q3nz2570fd241z59kz24bysksi1006q21zq6g06rj2m5kbd") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.2 (c (n "cap-rand") (v "1.0.2") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0bmglz8sk9im7npwfdql5fdkv45xyv7rf1pg7cczcmn6ng0313dp") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.3 (c (n "cap-rand") (v "1.0.3") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "09yp1sqrmq2fxpshd135rw2n245b8ari28kikghi31mqvi6h9psk") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.4 (c (n "cap-rand") (v "1.0.4") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1ybvaw4g5wr93jqdxzy9f6m739zpdxskgck1z6z5pqmniwxnr61a") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.5 (c (n "cap-rand") (v "1.0.5") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "096yj71ds0wi47l3gbd4hkzyaz32slsp3d7zlrglql3kgpcr8ghh") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.6 (c (n "cap-rand") (v "1.0.6") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1szdsr4m8cabfs1k4bqfy1fjwrs6yrxxlqf3zb8105y0p6ni30qk") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.7 (c (n "cap-rand") (v "1.0.7") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1jiabqj0h7ss0l7cq2zyhcdhz7lg69mkh30dy95wrrv56mzdqpbb") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.8 (c (n "cap-rand") (v "1.0.8") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1gwflpjc103wnwgmndafdq28v55bpc7352il24qpbb8s953yl2iq") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.9 (c (n "cap-rand") (v "1.0.9") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "03kjd55j6g8730sqc70yv5cwjfii6bbn37zyynaj1rrwglgw955l") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.10 (c (n "cap-rand") (v "1.0.10") (d (list (d (n "ambient-authority") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1y3z3ryfarlijgikf2dwsbcd1vjajwvwpvp0qn77w8pibr3yd6fr") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.11 (c (n "cap-rand") (v "1.0.11") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0rgv37vfy027lz0mm5hg4xf9pi0jx891377fvj0s9m724hsr9bml") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.12 (c (n "cap-rand") (v "1.0.12") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0qhrh20n06pfgv7czxk801vc2015s5ffvblszzpmzm3b213jzki4") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.13 (c (n "cap-rand") (v "1.0.13") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1hbpn7lla9rgqk8cyl87bka1s8i4y1aqj595pn87pfjbwrq3j8g4") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.14 (c (n "cap-rand") (v "1.0.14") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0kldgr7a4n1cq5p2qgkmi2dzzdachmdmd3n045cjmx0kvkg2xgnk") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.15 (c (n "cap-rand") (v "1.0.15") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1axqm4z4bqmqak9al3pzdy8vq6njalyq6d8xrx2542ybz9g5a9ad") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-1.0.16 (c (n "cap-rand") (v "1.0.16") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "18jjx0s42m8bwvjfm6jxvimlmn6wjfyrcld37x37m7ja8gx00f1q") (f (quote (("small_rng" "rand/small_rng") ("default")))) (y #t)))

(define-public crate-cap-rand-2.0.0 (c (n "cap-rand") (v "2.0.0") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "08pm243n9hkwbfllw5nvyzwdfsqipwldvrgg2dmpv962yz7yczap") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-2.0.1 (c (n "cap-rand") (v "2.0.1") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1jm4iw7jlricrpi0ngmp5dvhwm4p6lr70w9wph373kghcmankr90") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-3.0.0 (c (n "cap-rand") (v "3.0.0") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0s6y8d86lv1wd3gkv8r8qwqvmhrjiy6f2jn50fxrjfn3ma6z09s3") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-cap-rand-3.1.0 (c (n "cap-rand") (v "3.1.0") (d (list (d (n "ambient-authority") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0zq7si3znzmasf6lbhblg8mhaka49vp8ns4g9xh65z1a38s1s3r7") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

