(define-module (crates-io ca tp catptcha_oxide_derive) #:use-module (crates-io))

(define-public crate-catptcha_oxide_derive-1.0.0 (c (n "catptcha_oxide_derive") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bfcw89086mdqapf1yh2d31ndl6jgjlrp7ahbpsxqbzwzh4cjs6m") (y #t)))

(define-public crate-catptcha_oxide_derive-1.1.0 (c (n "catptcha_oxide_derive") (v "1.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rbfrcdj6r3g2lzr6hn5rkz5rlhj8fk9d9nipy2r2b2rhxcgji0j") (y #t)))

(define-public crate-catptcha_oxide_derive-2.0.0 (c (n "catptcha_oxide_derive") (v "2.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0n83119mriscxvm8prw9dr46swmyimvdbqdkjjzpqnidhgvj121z") (y #t)))

(define-public crate-catptcha_oxide_derive-3.0.0 (c (n "catptcha_oxide_derive") (v "3.0.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ssrsnx5a6ds6l58snjlp4sinyf6y4jcjafq1hnkmmr3w8daxrnm") (y #t)))

(define-public crate-catptcha_oxide_derive-3.1.0 (c (n "catptcha_oxide_derive") (v "3.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pqc30kpvlm053dz4jjzadx01siwy2wy0gxdl1gw56xw499x5bra") (y #t)))

(define-public crate-catptcha_oxide_derive-3.1.1 (c (n "catptcha_oxide_derive") (v "3.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17f144fzx7c556ra17bcxm72qfhjyhgvlp5rp7qlwxj6byfi8v9b") (y #t)))

(define-public crate-catptcha_oxide_derive-3.1.2 (c (n "catptcha_oxide_derive") (v "3.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vhch2mywjpih98j91ykxc74w059x41c0jf4c8xjmx7s1xmrin3n") (y #t)))

