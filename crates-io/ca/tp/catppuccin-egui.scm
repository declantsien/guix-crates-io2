(define-module (crates-io ca tp catppuccin-egui) #:use-module (crates-io))

(define-public crate-catppuccin-egui-1.0.0 (c (n "catppuccin-egui") (v "1.0.0") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "0spqfv87g775gw1czdqwizgv1hlxfnjl3iz76lizkbkxxibycnxk")))

(define-public crate-catppuccin-egui-1.0.1 (c (n "catppuccin-egui") (v "1.0.1") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "1bksw0f60whnfc11bkicgwb5kzcdim8bmr55pcgxvqsd3girn4za")))

(define-public crate-catppuccin-egui-1.0.2 (c (n "catppuccin-egui") (v "1.0.2") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "021zbkwfdcwx6rhwfd4wqlkjl6j96dvd2qkrgzqzwhm2jj68w4rp")))

(define-public crate-catppuccin-egui-2.0.0 (c (n "catppuccin-egui") (v "2.0.0") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "05qkg4vlylagi47cf718f68ymrr6k209pni4mgjkc4qgg5has275")))

(define-public crate-catppuccin-egui-2.0.1 (c (n "catppuccin-egui") (v "2.0.1") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "0x846m1jhgw8wzw2l9k0djrvmdmlb61j1b8hvs9sifrnbynvqh68")))

(define-public crate-catppuccin-egui-2.0.2 (c (n "catppuccin-egui") (v "2.0.2") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1i1hdix26ywikr823lamclx9bqz1xv208blp366qag86d7xw0mx8")))

(define-public crate-catppuccin-egui-3.0.0 (c (n "catppuccin-egui") (v "3.0.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1y9ircsg01vmlpfn28nmkb7c78fz2lcgv195kcm3j7ydyn03zqmw")))

(define-public crate-catppuccin-egui-3.1.0 (c (n "catppuccin-egui") (v "3.1.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1dci13h0r00h28iwjjbdn5czm6k4bvxs7c190l1dq6bw87258dqx")))

(define-public crate-catppuccin-egui-4.0.0 (c (n "catppuccin-egui") (v "4.0.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "1dhmv6gzdzgdilrpq2nxqy71xi3gzq4pdlk9brrj5n2b5in5mgb4")))

(define-public crate-catppuccin-egui-5.0.0 (c (n "catppuccin-egui") (v "5.0.0") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "1aymq6y0kqpgljrzi1b50ivbvwv9vm0cmpxnmcwrw219dgccz6d0")))

(define-public crate-catppuccin-egui-5.1.0 (c (n "catppuccin-egui") (v "5.1.0") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui26") (r "^0.26") (o #t) (k 0) (p "egui")) (d (n "egui27") (r "^0.27") (o #t) (k 0) (p "egui")))) (h "030lkvazw9j2p6nrpvwwabxfvyww9zmz7747mywsgi2f0blb2m8f") (f (quote (("default" "egui26"))))))

(define-public crate-catppuccin-egui-5.1.1 (c (n "catppuccin-egui") (v "5.1.1") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui26") (r "^0.26") (o #t) (k 0) (p "egui")) (d (n "egui27") (r "^0.27") (o #t) (k 0) (p "egui")))) (h "13scbsaj21190b5m6q1n83ygig1gb0zd5xb8b165cl6z6wbiqlhw") (f (quote (("default" "egui26"))))))

