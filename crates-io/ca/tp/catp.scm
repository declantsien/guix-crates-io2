(define-module (crates-io ca tp catp) #:use-module (crates-io))

(define-public crate-catp-0.1.0 (c (n "catp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("ptrace" "process"))) (d #t) (k 0)))) (h "1f85gdczyhc5g2dfwqppsd6n635bal62215lfwkvz182hh9rjfhl")))

(define-public crate-catp-0.1.1 (c (n "catp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("ptrace" "process"))) (d #t) (k 0)))) (h "1vifn2vxnqn5zmxdvy2anlh9r5j0qw80w0jm192njca8hhfgamyq")))

(define-public crate-catp-0.1.2 (c (n "catp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("ptrace" "process" "uio"))) (d #t) (k 0)))) (h "0h16ngn59csfbzfmm1kg811hx1xg7315gy8cx1nqghy2rifw5ghl")))

(define-public crate-catp-0.1.3 (c (n "catp") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("ptrace" "process" "uio"))) (d #t) (k 0)))) (h "1n9lmah3img336rp24ryfk0kvimy6rybmj11s19xn28wiy180ra5")))

(define-public crate-catp-0.2.0 (c (n "catp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("ptrace" "process" "uio"))) (d #t) (k 0)))) (h "1z5wzxqrbr0v4gp21inva9zdp2q7fq47w2k374g3jid6blyn2b0p")))

