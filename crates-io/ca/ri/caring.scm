(define-module (crates-io ca ri caring) #:use-module (crates-io))

(define-public crate-caring-0.1.0 (c (n "caring") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "interprocess-traits") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "memfd") (r "^0.2") (d #t) (k 0)) (d (n "sendfd") (r "^0.2") (d #t) (k 2)))) (h "0b5zl3az2ixwh6fhzbgrq1nxcn8w55gxpa8fl2a1lldkayxfwwc3")))

(define-public crate-caring-0.1.1 (c (n "caring") (v "0.1.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "interprocess-traits") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "memfd") (r "^0.2") (d #t) (k 0)) (d (n "sendfd") (r "^0.2") (d #t) (k 2)))) (h "0kv96302skgxvzsy665izsxrfzww5m8a5cslava4rpsakh26s88n")))

(define-public crate-caring-0.2.0 (c (n "caring") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "interprocess-traits") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "memfd") (r "^0.3") (d #t) (k 0)) (d (n "sendfd") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1jlys14mx27215mbblqq6jvz9452vgylkhgvq79wz8vfkrqn15p8")))

(define-public crate-caring-0.3.0 (c (n "caring") (v "0.3.0") (d (list (d (n "interprocess-traits") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "memfd") (r "^0.3") (d #t) (k 0)) (d (n "sendfd") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "0kx7r9ggw8q4svp3ws03np9jv53k5l50a8dd1b0b33j6z6s7klcl")))

