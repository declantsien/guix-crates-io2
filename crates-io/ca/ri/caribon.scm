(define-module (crates-io ca ri caribon) #:use-module (crates-io))

(define-public crate-caribon-0.2.1 (c (n "caribon") (v "0.2.1") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "1yrgawnsrzmv9l31gz9wpngjmg4sk2g4abzz11giqxcslyyl2p5h")))

(define-public crate-caribon-0.2.2 (c (n "caribon") (v "0.2.2") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "0h5i36n99cdmwwv7vp0mxp6g81r0nlx0ad118l3z8rqh5fiiag4a")))

(define-public crate-caribon-0.3.0 (c (n "caribon") (v "0.3.0") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "0s4ra3cmr8p6i6564v4r1fzb807wazrvx7z0aygwkgc4x3d1q5q3")))

(define-public crate-caribon-0.4.0 (c (n "caribon") (v "0.4.0") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "1sy2fskci1slc9zm20l4q4bla54f968a3h1s8k7gl5yhwhi8pw2x")))

(define-public crate-caribon-0.5.0 (c (n "caribon") (v "0.5.0") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "0yb3ad5cvy83gf4fx1mjvkz7mci9bipysdv4wwgg5fki7dg4zl2z")))

(define-public crate-caribon-0.5.1 (c (n "caribon") (v "0.5.1") (d (list (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "1rbzx4izqpwkmchf7k5amjaw36ly4hhh2brp24wv60cjcywafyij")))

(define-public crate-caribon-0.5.2 (c (n "caribon") (v "0.5.2") (d (list (d (n "edit-distance") (r "^1") (d #t) (k 0)) (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "07pggrlw6bfjz513ix7apxps3lr6mm525vgnz8xvw2cxzavfasi4")))

(define-public crate-caribon-0.6.0 (c (n "caribon") (v "0.6.0") (d (list (d (n "edit-distance") (r "*") (d #t) (k 0)) (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "1iag7bzmsy334pj3glxxjh16p3rnp7d61cwzf235zkrhbjz3pkkq")))

(define-public crate-caribon-0.6.1 (c (n "caribon") (v "0.6.1") (d (list (d (n "edit-distance") (r "*") (d #t) (k 0)) (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "09qp8aa34a8mlvv7874kikz2w7vnykr1wagjylfx2k44hyclni5q")))

(define-public crate-caribon-0.6.2 (c (n "caribon") (v "0.6.2") (d (list (d (n "edit-distance") (r "*") (d #t) (k 0)) (d (n "stemmer") (r "*") (d #t) (k 0)))) (h "13lcj15wgvx4p7c105rl3bs2jjkzgjj3hcqfgv699a4805yc4k31")))

(define-public crate-caribon-0.7.0 (c (n "caribon") (v "0.7.0") (d (list (d (n "edit-distance") (r "^1") (d #t) (k 0)) (d (n "stemmer") (r "^0.3") (d #t) (k 0)))) (h "0nzjpkm2h9qzflcw9ldjkhkdwa7d74qcfxjighss4q66basn4ryi")))

(define-public crate-caribon-0.7.1 (c (n "caribon") (v "0.7.1") (d (list (d (n "edit-distance") (r "^1") (d #t) (k 0)) (d (n "stemmer") (r "^0.3") (d #t) (k 0)))) (h "1mgl6pq99k5h3c9fcjzb8fycgciv3f6yknxaj4admyysynga8wdb")))

(define-public crate-caribon-0.7.2 (c (n "caribon") (v "0.7.2") (d (list (d (n "edit-distance") (r "^1") (d #t) (k 0)) (d (n "stemmer") (r "^0.3") (d #t) (k 0)))) (h "0gwz51mi77b52hgj37cbvvcq5kysd732niq4pw50i5f9mhcimrnc")))

(define-public crate-caribon-0.7.3 (c (n "caribon") (v "0.7.3") (d (list (d (n "edit-distance") (r "^1") (d #t) (k 0)) (d (n "stemmer") (r "^0.3") (d #t) (k 0)))) (h "19irnvi00j3i1w9k6nnbrbg74slq3ndbrlq42jbfk3w8dmhnjya9")))

(define-public crate-caribon-0.7.4 (c (n "caribon") (v "0.7.4") (d (list (d (n "stemmer") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "10fwfmi34dfab4gmkkg1i5qfvyg1v1j6zr4xq2xc4lmz38id8qml")))

(define-public crate-caribon-0.8.0 (c (n "caribon") (v "0.8.0") (d (list (d (n "stemmer") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "1jw8mrlyd5i3dq4sgs5889f5zj3wmc541afl424xdlm0kdrszv15")))

(define-public crate-caribon-0.8.1 (c (n "caribon") (v "0.8.1") (d (list (d (n "stemmer") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "1bj78bs11r8lsxy20qbhymkkfarixqynqm6rdhx4qizs37qmlifr")))

