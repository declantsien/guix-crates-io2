(define-module (crates-io ca ny canyon-sys) #:use-module (crates-io))

(define-public crate-canyon-sys-0.1.1 (c (n "canyon-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1jw7kjghng6pxb8fmzd5p6c92w3c05xhw1faxqrwhab5dzzjmgaz")))

(define-public crate-canyon-sys-0.1.2 (c (n "canyon-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "006ivpd1q4ncl4fynxs8rj0c67qfbs0rhycbx22mycy8ns9x4kwd")))

(define-public crate-canyon-sys-0.1.3 (c (n "canyon-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1ynbrdhm30mds5xmkzn5yvk6wa0n86c7jz1zhr5d9k8z97vj9xrd")))

