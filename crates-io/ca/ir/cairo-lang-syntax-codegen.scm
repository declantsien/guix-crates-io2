(define-module (crates-io ca ir cairo-lang-syntax-codegen) #:use-module (crates-io))

(define-public crate-cairo-lang-syntax-codegen-0.1.0 (c (n "cairo-lang-syntax-codegen") (v "0.1.0") (d (list (d (n "cairo-lang-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0lzygmlbi9bl07wbcn9v7bpf21nyxhzqxnkijqpfj42lrwflrd4i")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-alpha.2 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-alpha.2") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1xygpqg5bv7h2v1yjgwfy4jkgxsdmz6746sww1icxsa47dllwcgn")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-alpha.3 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-alpha.3") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1kvap6aj3gglpl320r7wxds2zsc3ymvhpj2ikxrz7i8s7wyrfffz")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-alpha.4 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-alpha.4") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1kh9v90q1sblwqixc5r0m430c13qbbzc3wxs5kzq7gzazxh4vk0f")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-alpha.6 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-alpha.6") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1w042jc0b2sjni2m51jdm45z07y10ivxrllr7rc15n8qaha3dcyj")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-alpha.7 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-alpha.7") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0psqdrla1anyi42kmcdj3p7z3pcx7mp8k00k3zay6p92whpy91g8")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "1.0.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^1.0.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1lz2yh6lhx96kh6ly483mzhf76hl9ch64rgjmnx9mwz8jfcj0mkk")))

(define-public crate-cairo-lang-syntax-codegen-1.0.0 (c (n "cairo-lang-syntax-codegen") (v "1.0.0") (d (list (d (n "cairo-lang-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0r0lv7zfxmnzyhfcxlb272gxvfxf726ji6fcgkzwp10mdnrwsr37")))

(define-public crate-cairo-lang-syntax-codegen-1.1.0-alpha0 (c (n "cairo-lang-syntax-codegen") (v "1.1.0-alpha0") (d (list (d (n "cairo-lang-utils") (r "^1.1.0-alpha0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "153qf1v0bj7apyfkj1iz7pkx11x55p6l20cvgb1k7a0nsy9k6piq")))

(define-public crate-cairo-lang-syntax-codegen-1.1.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "1.1.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^1.1.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0q60r0ym635dgr5fshxqy7d20ik5gwg21mbyfyw6bf35b6s1f0jp")))

(define-public crate-cairo-lang-syntax-codegen-1.1.0 (c (n "cairo-lang-syntax-codegen") (v "1.1.0") (d (list (d (n "cairo-lang-utils") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0j6d9idrpd8rwkklr2459dj9423yb5wyf1sdvqz4gac6k2rcmis0")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "008n085fwkz4pil74s8qmg6q43c5bcls09myaj426ylp3vg606rd")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc1 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0jir28hl2lay0ij4hyji66bs5bczi30qksz4w4x7vyivdwnk9w5g")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc2 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1scrw9ry3gzpln0337z9x9jskyh1hdf0y2fb983x3qmid11psipq")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc3 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0j0rc9hmchb55nsv83zjyv0wkah7sra4x89r3n1z4p7p90cllji3")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc4 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "06c9ngk3gcrs5fggm3lq4bcb13p7czvmglf5bzzd7zbfzqniyxi7")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc5 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0z9bcpajyy5kw00696nq510j7hyzlwlcdbwg91simqbryy3qcyy2")))

(define-public crate-cairo-lang-syntax-codegen-1.1.1 (c (n "cairo-lang-syntax-codegen") (v "1.1.1") (d (list (d (n "cairo-lang-utils") (r "^1.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "07xrpclg3sddas1nrbysiaqs10z9pa5zgjw7wjj7aj0wlslzvfy9")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc6 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1f19qbm1zl1hdfhlnzrwax6943h3j17gp0mmmk71n8babam5m38r")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0-rc7 (c (n "cairo-lang-syntax-codegen") (v "2.0.0-rc7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "049z64n1vxhiqphxy82zq5msdrbdli620nvxgyca8z5r1hdc5mwm")))

(define-public crate-cairo-lang-syntax-codegen-2.0.0 (c (n "cairo-lang-syntax-codegen") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0l5xshharz17gf2bl4yy0xs083frk9b9mryhll7va7f37pyvad16")))

(define-public crate-cairo-lang-syntax-codegen-2.0.1 (c (n "cairo-lang-syntax-codegen") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0i58qd3ynkk8j9s3zr9d0injk2jpa69kffgv3s7n79qr32wh48cb")))

(define-public crate-cairo-lang-syntax-codegen-2.0.2 (c (n "cairo-lang-syntax-codegen") (v "2.0.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "05mnvjhh536lr7rhjp1limz6ayxkd90v9zh8swb9xw0lih1nhmlm")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "2.1.0-rc0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1c11760vr3ks7zls4mz8bnh86yay7san4bxmpl9cklw35d6j4261")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0-rc1 (c (n "cairo-lang-syntax-codegen") (v "2.1.0-rc1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0ara2nik7c8mmphkpaz9s0vrph3jr5sm9f0rih3mwknk2826fmmb")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0-rc2 (c (n "cairo-lang-syntax-codegen") (v "2.1.0-rc2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0bw9vnscvp6prsm747qw81xnpxx58wmm0q3i1v4glcf17a7l8s3s")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0-rc3 (c (n "cairo-lang-syntax-codegen") (v "2.1.0-rc3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0qcq6bf8qkcb1wscgs9zpwb8qazm4plh1nfz2qlmsamki3dwbphh")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0-rc4 (c (n "cairo-lang-syntax-codegen") (v "2.1.0-rc4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0v4f4cwp95jj7wlw9nickz1j88ih0c2bj7a1nnj4sqn21d272wm8")))

(define-public crate-cairo-lang-syntax-codegen-2.1.0 (c (n "cairo-lang-syntax-codegen") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "118642y5zlwcfpizhsddmqg7fdq80kza37v8dg6wdnbyhpf3921l")))

(define-public crate-cairo-lang-syntax-codegen-2.1.1 (c (n "cairo-lang-syntax-codegen") (v "2.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1zknj3kjqppfdh4n988dzcr933ybk6l6w8df9x1bq8wmy2sy8982")))

(define-public crate-cairo-lang-syntax-codegen-2.2.0 (c (n "cairo-lang-syntax-codegen") (v "2.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1l1saradd9lihxafdax3zffjva30a8qwxkpdc9i519gn4jzzlbw2")))

(define-public crate-cairo-lang-syntax-codegen-2.3.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "2.3.0-rc0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0wjxv3nw4alwc53bzifh2ivc6an9hly67cc959sm8wisr6wcghs0")))

(define-public crate-cairo-lang-syntax-codegen-2.3.0 (c (n "cairo-lang-syntax-codegen") (v "2.3.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0sif1vqdn750pnaj06x8b17dp3698pd5yi6d5gvgzxcbfqxykwh4")))

(define-public crate-cairo-lang-syntax-codegen-2.3.1 (c (n "cairo-lang-syntax-codegen") (v "2.3.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0drnwpas5kfshm6h31a2qmix3qcal5jjdb62anid7h60h99785cn")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc0 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0z8911m43vzg1x9p1n54jqx3pv476gkii46f80bn6a62s43hx9dg")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc1 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1lwckfaimnslpf1p072w9lam5xi9hqdx4yfa6vim4z2crqkrdr4d")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc2 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "14v231gdgxsxjys80vzzqhq6xxa6mnhn7166z59ap12s43fsjfq1")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc3 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "13p9mp7zg60fw5gs8r5fkgnp16s7vn5wnw8zwy07zsi8cdhfnmaa")))

(define-public crate-cairo-lang-syntax-codegen-2.1.2 (c (n "cairo-lang-syntax-codegen") (v "2.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "11k230pxzmgnx1y9x3s0fipyk04kldl0vdyv6jdclfakbkwqn7k5")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc4 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1dbcw5afwsqklqhixgysmy3n70m8svyf07jlz7b0rhfhg31q07gq")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc5 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0mbb51q6myn3ywz991dqp0ygx79xlv8yp1dn6x6w3ar4mmldizbk")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0-rc6 (c (n "cairo-lang-syntax-codegen") (v "2.4.0-rc6") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "13vlr0lr76f8f1gr85mjxdry9axh4yryjjp7nws2bhhd85ad15gv")))

(define-public crate-cairo-lang-syntax-codegen-2.4.0 (c (n "cairo-lang-syntax-codegen") (v "2.4.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0jghcpc6l9nx29rzwwzc5jak197kb6n0vr126590m2axvjjw6l3s")))

(define-public crate-cairo-lang-syntax-codegen-2.4.1 (c (n "cairo-lang-syntax-codegen") (v "2.4.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0z499gw8a6mc0r4nr4ar7i482fjzssgm40gqm647wh9264ysxl2b")))

(define-public crate-cairo-lang-syntax-codegen-2.5.0-dev.0 (c (n "cairo-lang-syntax-codegen") (v "2.5.0-dev.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0lkmn2iry7k497x91hhk900zw3v8flba50i17pjj1kv5ljyyx86y")))

(define-public crate-cairo-lang-syntax-codegen-2.4.2 (c (n "cairo-lang-syntax-codegen") (v "2.4.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "11hjcw9w98z0rx470vl6byag0v45xnyiqb74ksllv0am81pzwifp")))

(define-public crate-cairo-lang-syntax-codegen-2.4.3 (c (n "cairo-lang-syntax-codegen") (v "2.4.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0fw1cx9zbgfirvhmiif12rhp12rpq47plpij319nnwsv30cz3jgp")))

(define-public crate-cairo-lang-syntax-codegen-2.5.0-dev.1 (c (n "cairo-lang-syntax-codegen") (v "2.5.0-dev.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1p1p45yvccsswb1xmjbbgkfs2hkgvk4gazv5v55zyyvk9xkw7irj")))

(define-public crate-cairo-lang-syntax-codegen-2.4.4 (c (n "cairo-lang-syntax-codegen") (v "2.4.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "02ymgca76kq4fgh2fwbqc5h7vzn5lhyw7va9fn7002xj9dkm4cqc")))

(define-public crate-cairo-lang-syntax-codegen-2.5.0 (c (n "cairo-lang-syntax-codegen") (v "2.5.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1njd29x7y2vk60ngy786dq4kkrl0ggaq2cg6yw6k6b6sa9wyb9fa")))

(define-public crate-cairo-lang-syntax-codegen-2.5.1 (c (n "cairo-lang-syntax-codegen") (v "2.5.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0ixyzi5fdnhn6lhda0cb3lc9hpjhkrshkc05b01qm45fh103k1qg")))

(define-public crate-cairo-lang-syntax-codegen-2.5.2 (c (n "cairo-lang-syntax-codegen") (v "2.5.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0xdazs7zphb6d65rl40k4l25hqskxbqz7binpj2q0l1xvzfgjqda")))

(define-public crate-cairo-lang-syntax-codegen-2.5.3 (c (n "cairo-lang-syntax-codegen") (v "2.5.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0wrsxnsyqnhf4qpwpyfwaif6g6bzgsvgpyvisgwz343i8nin7ibd")))

(define-public crate-cairo-lang-syntax-codegen-2.6.0-rc.0 (c (n "cairo-lang-syntax-codegen") (v "2.6.0-rc.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "088326k7hdyh2yb2f848bdx4qqq45mkz1plmx13bynh2g6jbv12p")))

(define-public crate-cairo-lang-syntax-codegen-2.5.4 (c (n "cairo-lang-syntax-codegen") (v "2.5.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1nh9g8xx0pasx7n75z58v2ynwgmw6q90zlizvh7qyrbqpl3lvawx")))

(define-public crate-cairo-lang-syntax-codegen-2.6.0-rc.1 (c (n "cairo-lang-syntax-codegen") (v "2.6.0-rc.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0i0p5llbhp5bjh8i40l6ln023f9s4y2wfmbbz4b38m4lvhh1s8g6")))

(define-public crate-cairo-lang-syntax-codegen-2.6.0 (c (n "cairo-lang-syntax-codegen") (v "2.6.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "160nv5ihcbm67j8f3n9zj885gfsvs8s8ag4zcz8wlhkqp2g1p106")))

(define-public crate-cairo-lang-syntax-codegen-2.6.1 (c (n "cairo-lang-syntax-codegen") (v "2.6.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1r54abs4sviwjc8vn69441wwjd13cibni8yg1wpvy3yd4rn0a4z8")))

(define-public crate-cairo-lang-syntax-codegen-2.6.2 (c (n "cairo-lang-syntax-codegen") (v "2.6.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "015ddpndrpk8z8nk1njdynjdmbypprf5g78r5f7f4n1ydkm8j4hi")))

(define-public crate-cairo-lang-syntax-codegen-2.6.3 (c (n "cairo-lang-syntax-codegen") (v "2.6.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)) (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "15zsk55avzsq6s0hqxc67mjr32kkxgaqy551liwl3fz4xa9ddqkh")))

(define-public crate-cairo-lang-syntax-codegen-2.7.0-dev.0 (c (n "cairo-lang-syntax-codegen") (v "2.7.0-dev.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "genco") (r "^0.17.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2.16") (d #t) (k 2)) (d (n "xshell") (r "^0.2.6") (d #t) (k 0)))) (h "1v4i19x8zc0w65pasycx5kwz16wqrdhy842frncfvc6vf056hjm8")))

