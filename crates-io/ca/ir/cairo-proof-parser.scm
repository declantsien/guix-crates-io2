(define-module (crates-io ca ir cairo-proof-parser) #:use-module (crates-io))

(define-public crate-cairo-proof-parser-0.3.0 (c (n "cairo-proof-parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "starknet-crypto") (r "^0.6.2") (d #t) (k 0)))) (h "16q6lgz5cn3bzj9l19vhcl8mayn51l75x82nmmk6d0a8am7kni9f")))

