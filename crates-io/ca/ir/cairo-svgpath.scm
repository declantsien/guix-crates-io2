(define-module (crates-io ca ir cairo-svgpath) #:use-module (crates-io))

(define-public crate-cairo-svgpath-0.1.0 (c (n "cairo-svgpath") (v "0.1.0") (d (list (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "svgtypes") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1q0fpi8r0cw67g8w90g14y566vl1k0ivj9rwf0cxhkh2r6bmzqnd")))

(define-public crate-cairo-svgpath-0.1.1 (c (n "cairo-svgpath") (v "0.1.1") (d (list (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "svgtypes") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1mc07m37hc69g50jmwj6spz30dmf9lzkgn8m67zcv9wal5mbvwqg")))

