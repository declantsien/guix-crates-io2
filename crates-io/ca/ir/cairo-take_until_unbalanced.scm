(define-module (crates-io ca ir cairo-take_until_unbalanced) #:use-module (crates-io))

(define-public crate-cairo-take_until_unbalanced-0.24.0-rc1 (c (n "cairo-take_until_unbalanced") (v "0.24.0-rc1") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0p7im2zcibgvi55w19vi664bpxzpwg4a6z1lvckyiphz7ddaqps3") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.24.0 (c (n "cairo-take_until_unbalanced") (v "0.24.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0ch4gy63c78mglg6ahlib9y2xkmkryx9cgppqqypkrhww2vk23fs") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.24.1 (c (n "cairo-take_until_unbalanced") (v "0.24.1") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1fp81z0r4cagd2bmwa120fmfdh1ax80jydgl9din3mw3kvlz72qj") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.24.2-rc1 (c (n "cairo-take_until_unbalanced") (v "0.24.2-rc1") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0bbp65wrxkdh1dab5pkl1h4y28jp83g34vrpg6sykkzpdl2p9qd4") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.25.0 (c (n "cairo-take_until_unbalanced") (v "0.25.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0bn3i2dbwvb969w5mn53m2bykc8pqiisvh5sn5j62l7l3ndb5w6w") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.26.0 (c (n "cairo-take_until_unbalanced") (v "0.26.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1ailp9xi7aqmb5zgkl51yl7jpsm3yfzd5w3zxqg7hd2khaz2xvd6") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.27.0 (c (n "cairo-take_until_unbalanced") (v "0.27.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "02a1lqiv2yjpv6b0d2ir97p7rir00pv2anj5n3a3wzrpkcmmr4ha") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.28.0 (c (n "cairo-take_until_unbalanced") (v "0.28.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0c3ab4mjgk2q2lzxzc26xycclfgx6n8l4zajsbngvr8icj26zily") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.29.0 (c (n "cairo-take_until_unbalanced") (v "0.29.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "07nhz4391jbabr0ca5jnrswcfhwzygi53kl1r90wzz4bhpdcayl4") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.30.0 (c (n "cairo-take_until_unbalanced") (v "0.30.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0b6z9zpr0fcf8mn028qprvyzbabslcg7c2n2zhcxnqrblnr6dhrj") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.31.0 (c (n "cairo-take_until_unbalanced") (v "0.31.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1f6z4j97q8n6v9jy1jm1n1k8yp2nbr1kfh52hvvij56ic79kr0pa") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-cairo-take_until_unbalanced-0.32.0 (c (n "cairo-take_until_unbalanced") (v "0.32.0") (d (list (d (n "nom") (r "^7.1.1") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "12mzz7lvy7pcyvdhy52ri0fv22b2czas55hbrb7z2ynjy8dydgh8") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

