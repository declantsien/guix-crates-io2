(define-module (crates-io ca ir cairo-lang-debug) #:use-module (crates-io))

(define-public crate-cairo-lang-debug-0.1.0 (c (n "cairo-lang-debug") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1bbk2jrbk83ln1wazvp3pvc8bsw4iw1jlrdhk6ck087qigyxidc3")))

(define-public crate-cairo-lang-debug-1.0.0-alpha.2 (c (n "cairo-lang-debug") (v "1.0.0-alpha.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0ghk0absw28hr3w20kdjpx1yxnmsrm4lfv2017mqbvnir54h8nc2")))

(define-public crate-cairo-lang-debug-1.0.0-alpha.3 (c (n "cairo-lang-debug") (v "1.0.0-alpha.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1kzlqr4dk8lhxccl8g7i2yij23q5hw9nm53jhlkb0lrnyxdwhbl1")))

(define-public crate-cairo-lang-debug-1.0.0-alpha.4 (c (n "cairo-lang-debug") (v "1.0.0-alpha.4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "03dhjdzkl9glx216kxxvhqqp5jhnja877rf4dcsizyhybmm7iqsj")))

(define-public crate-cairo-lang-debug-1.0.0-alpha.5 (c (n "cairo-lang-debug") (v "1.0.0-alpha.5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "09zq4m6yck9izsm6dvxswg7ri44y2gas3yhjlm6v00h6hwl15qxn") (y #t)))

(define-public crate-cairo-lang-debug-1.0.0-alpha.6 (c (n "cairo-lang-debug") (v "1.0.0-alpha.6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1a30gwdgld2gp0aj82plmlpq54a5gi9xynrqvzpfk0k9xr8v281b")))

(define-public crate-cairo-lang-debug-1.0.0-alpha.7 (c (n "cairo-lang-debug") (v "1.0.0-alpha.7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "04pmwjz5pra0r4wp4a9hq5g9m7qmy9kk1qij3vxf3909xlqq1cca")))

(define-public crate-cairo-lang-debug-1.0.0-rc0 (c (n "cairo-lang-debug") (v "1.0.0-rc0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1wyagi56dca8agcsqhs5f37b34s8zdm7bxdqkzwx3v2d4444aqzk")))

(define-public crate-cairo-lang-debug-1.0.0 (c (n "cairo-lang-debug") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1hz38gk5gjqql65sdq7c5m3hyr27yqknap79i5xyxvsy5hx27a4r")))

(define-public crate-cairo-lang-debug-1.1.0-alpha0 (c (n "cairo-lang-debug") (v "1.1.0-alpha0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1bs1h5w71gh2v9whqjcr5qcvij0xmw2f8bl1ngva0xrnpww2hvdz")))

(define-public crate-cairo-lang-debug-1.1.0-rc0 (c (n "cairo-lang-debug") (v "1.1.0-rc0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "19bjmmmdiysvk2wf3wm32zh17dghc97py0103a8gvbp6l22cpnn5")))

(define-public crate-cairo-lang-debug-1.1.0 (c (n "cairo-lang-debug") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "10iwc2wz6k4lvhr52dlcls2w1g0n72zlzrds0lv9nkki3rs9z6b5")))

(define-public crate-cairo-lang-debug-2.0.0-rc0 (c (n "cairo-lang-debug") (v "2.0.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.17.0-pre.2") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "19xcwnkzx11w8bpshgfn267byqz70v54way3sdh8fb2lbaxx52ng")))

(define-public crate-cairo-lang-debug-2.0.0-rc1 (c (n "cairo-lang-debug") (v "2.0.0-rc1") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.17.0-pre.2") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0ly69rw4xh4pgrykzf9b4zw3n2kisbjh4yqvmgqipjsfm6his8w4")))

(define-public crate-cairo-lang-debug-2.0.0-rc2 (c (n "cairo-lang-debug") (v "2.0.0-rc2") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0apdizyb35q95kqx86kxbifzwlwy9f67aznpw4w854bs1i7x7dya")))

(define-public crate-cairo-lang-debug-2.0.0-rc3 (c (n "cairo-lang-debug") (v "2.0.0-rc3") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0vrxzc6l36r933havrvyvv2wihslknnywb11qydqk8d8bsqp59ky")))

(define-public crate-cairo-lang-debug-2.0.0-rc4 (c (n "cairo-lang-debug") (v "2.0.0-rc4") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "024h09hkk89l3qwsz9s6kxjiz43brz4s7733610q457qmhbb27hj")))

(define-public crate-cairo-lang-debug-2.0.0-rc5 (c (n "cairo-lang-debug") (v "2.0.0-rc5") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0c4n160kjakp4qsi28zv7pkaav0dx99hdb3v9zggippz5p695byr")))

(define-public crate-cairo-lang-debug-1.1.1 (c (n "cairo-lang-debug") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "19s06f0s348j7rja81i1sclh1l0zlkxa0wq6gihiqllq9yhl37f9")))

(define-public crate-cairo-lang-debug-2.0.0-rc6 (c (n "cairo-lang-debug") (v "2.0.0-rc6") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1p486l73zw6ihxqhw9l41cb335v6g2zhl58yq7vcyq9mnazys2ay")))

(define-public crate-cairo-lang-debug-2.0.0-rc7 (c (n "cairo-lang-debug") (v "2.0.0-rc7") (d (list (d (n "cairo-lang-utils") (r "^2.0.0-rc7") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "056y88g0wcgg6h9dxpvqzm52ax5z9y5s639ii0zzgx13wzd1nk8m")))

(define-public crate-cairo-lang-debug-2.0.0 (c (n "cairo-lang-debug") (v "2.0.0") (d (list (d (n "cairo-lang-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1vcqdswsjsj8kn2jc6b4a3hwf7z0hjig3hmqvandnzvhzx0cgrb5")))

(define-public crate-cairo-lang-debug-2.0.1 (c (n "cairo-lang-debug") (v "2.0.1") (d (list (d (n "cairo-lang-utils") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1db6da8ymykmfpf7zf8cks5hgnxdbdhx1gaglrkg8lmcyncnml2b")))

(define-public crate-cairo-lang-debug-2.0.2 (c (n "cairo-lang-debug") (v "2.0.2") (d (list (d (n "cairo-lang-utils") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1ndrrsk7k3smqb171123gzih7f9j2g4ad237v2bhi15fnqwgvh8y")))

(define-public crate-cairo-lang-debug-2.1.0-rc0 (c (n "cairo-lang-debug") (v "2.1.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^2.1.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1ldgfd1c3mkjblgv2hfkg3gmp508p5838sdx5zclfs77jblndpfb")))

(define-public crate-cairo-lang-debug-2.1.0-rc1 (c (n "cairo-lang-debug") (v "2.1.0-rc1") (d (list (d (n "cairo-lang-utils") (r "^2.1.0-rc1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1wf2wik6v42fx2sqs5j3ggycgcwh2h42cznkm150awvhyz0rj8k0")))

(define-public crate-cairo-lang-debug-2.1.0-rc2 (c (n "cairo-lang-debug") (v "2.1.0-rc2") (d (list (d (n "cairo-lang-utils") (r "^2.1.0-rc2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "007xld3pp15ijbh1bzglg45pjlckxrg87r29cax8xlxhx7azw6ca")))

(define-public crate-cairo-lang-debug-2.1.0-rc3 (c (n "cairo-lang-debug") (v "2.1.0-rc3") (d (list (d (n "cairo-lang-utils") (r "^2.1.0-rc3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1m3v45srnp0xha7xihyygfpg0252zdgx1xsnhnbnk2jz651gidip")))

(define-public crate-cairo-lang-debug-2.1.0-rc4 (c (n "cairo-lang-debug") (v "2.1.0-rc4") (d (list (d (n "cairo-lang-utils") (r "^2.1.0-rc4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1adi3is0iix3lvpr0m0dxqv2wb423glvdaqr8s1qa0z73dk1k2b8")))

(define-public crate-cairo-lang-debug-2.1.0 (c (n "cairo-lang-debug") (v "2.1.0") (d (list (d (n "cairo-lang-utils") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0dg2xqfja8nn3zpbhv6jwvzc3hgs8llz0md3drzz1bnp7074kqjv")))

(define-public crate-cairo-lang-debug-2.1.1 (c (n "cairo-lang-debug") (v "2.1.1") (d (list (d (n "cairo-lang-utils") (r "^2.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0b7f6s638qnxkrqrfpzsi58rafs6ys0ahh01dxyp05fmkg4d01mg")))

(define-public crate-cairo-lang-debug-2.2.0 (c (n "cairo-lang-debug") (v "2.2.0") (d (list (d (n "cairo-lang-utils") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0zl8hyh9fkz1vx30bqr381nkyayjvs676v3xgir0qy1z9iysffw7")))

(define-public crate-cairo-lang-debug-2.3.0-rc0 (c (n "cairo-lang-debug") (v "2.3.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^2.3.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1j223qkwrbvpj1n3yx8qigsxqrz25habjfc64y81b8blwjsyyv3g")))

(define-public crate-cairo-lang-debug-2.3.0 (c (n "cairo-lang-debug") (v "2.3.0") (d (list (d (n "cairo-lang-utils") (r "^2.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "17afl7ffz5j2yyhkplzxgihz9ykh0903qkbrcdhhclrvary9ci1m")))

(define-public crate-cairo-lang-debug-2.3.1 (c (n "cairo-lang-debug") (v "2.3.1") (d (list (d (n "cairo-lang-utils") (r "^2.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1m0b62hy8id75gwrlvwsfsw9zwsi7gxcvd06pn4z0kwi1js2jicm")))

(define-public crate-cairo-lang-debug-2.4.0-rc0 (c (n "cairo-lang-debug") (v "2.4.0-rc0") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0p06kck9d9g0k6dvh2w9vj6v0vshi9waffwiifa7vnp9a089g3m2")))

(define-public crate-cairo-lang-debug-2.4.0-rc1 (c (n "cairo-lang-debug") (v "2.4.0-rc1") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1s2rys13gbpa16gih5xqdild07g7bhjnnl9zf3g6qkrglrag98b6")))

(define-public crate-cairo-lang-debug-2.4.0-rc2 (c (n "cairo-lang-debug") (v "2.4.0-rc2") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1nns78ala4lp0dagqrj9s6ih4kf2hrwadm2zf524czp7vprhi957")))

(define-public crate-cairo-lang-debug-2.4.0-rc3 (c (n "cairo-lang-debug") (v "2.4.0-rc3") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1q1vsggkbvlq30iylcq85i0bw1642xi2gxl1szpdh7dabqhzd8dh")))

(define-public crate-cairo-lang-debug-2.1.2 (c (n "cairo-lang-debug") (v "2.1.2") (d (list (d (n "cairo-lang-utils") (r "^2.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "199har5j5yqbnk4pmh2szi8v7vdcm10dk782g8hkwqmfbygahb1x")))

(define-public crate-cairo-lang-debug-2.4.0-rc4 (c (n "cairo-lang-debug") (v "2.4.0-rc4") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1iaw11cy5md0y2fcl90zpdhg6g7pmp0dzaawkj3krcsyyyxq9j2a")))

(define-public crate-cairo-lang-debug-2.4.0-rc5 (c (n "cairo-lang-debug") (v "2.4.0-rc5") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "11aqypwxbzmhj69ibl4ngcrlrfknzxkxichk0lhvbva1brv41cpj")))

(define-public crate-cairo-lang-debug-2.4.0-rc6 (c (n "cairo-lang-debug") (v "2.4.0-rc6") (d (list (d (n "cairo-lang-utils") (r "^2.4.0-rc6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1b9jwxnklxzvaq7fv46baj57vbwvf2g54vbnj1klhs733ws029ym")))

(define-public crate-cairo-lang-debug-2.4.0 (c (n "cairo-lang-debug") (v "2.4.0") (d (list (d (n "cairo-lang-utils") (r "^2.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1b5m6gh54xszn644jps3579pipz2sn1f8fbkz9ia89m8ggmhs68c")))

(define-public crate-cairo-lang-debug-2.4.1 (c (n "cairo-lang-debug") (v "2.4.1") (d (list (d (n "cairo-lang-utils") (r "^2.4.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "18x1h9n6cqmhh303vnj8qrn58sj4fnar0k6bgpa2kmn941glmza6")))

(define-public crate-cairo-lang-debug-2.5.0-dev.0 (c (n "cairo-lang-debug") (v "2.5.0-dev.0") (d (list (d (n "cairo-lang-utils") (r "^2.5.0-dev.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1df0i5nnpyg4v3mdcsddqs0pnialjnw8qmsqcwp1qai1h4hzjr4x")))

(define-public crate-cairo-lang-debug-2.4.2 (c (n "cairo-lang-debug") (v "2.4.2") (d (list (d (n "cairo-lang-utils") (r "^2.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1dvj61iyj6jv3y695h96zaj2d8cffypabz0zv8brbv88nx2sg0vj")))

(define-public crate-cairo-lang-debug-2.4.3 (c (n "cairo-lang-debug") (v "2.4.3") (d (list (d (n "cairo-lang-utils") (r "^2.4.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1a0n7wjz35l9ds2vvj54g454kqf2mwxajbv930w74axjza0v6lha")))

(define-public crate-cairo-lang-debug-2.5.0-dev.1 (c (n "cairo-lang-debug") (v "2.5.0-dev.1") (d (list (d (n "cairo-lang-utils") (r "^2.5.0-dev.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "19600ngnv2rg46rxchdwyb7spmw03p66s8zzclaxyf1f2h5cy3ky")))

(define-public crate-cairo-lang-debug-2.4.4 (c (n "cairo-lang-debug") (v "2.4.4") (d (list (d (n "cairo-lang-utils") (r "^2.4.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1qf0kcp179fknravrzykyapjh3s5mch51nyi2xlclxzhbbdngsi2")))

(define-public crate-cairo-lang-debug-2.5.0 (c (n "cairo-lang-debug") (v "2.5.0") (d (list (d (n "cairo-lang-utils") (r "^2.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0d48h48vgb9jzcy7yw4ykkldp1068invb6axqjkc8bqhfbhlidms")))

(define-public crate-cairo-lang-debug-2.5.1 (c (n "cairo-lang-debug") (v "2.5.1") (d (list (d (n "cairo-lang-utils") (r "^2.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0kqmaxpxax086mfy4jdr41b7rjj587n4qa8ni3dl8h0s0g883gd6")))

(define-public crate-cairo-lang-debug-2.5.2 (c (n "cairo-lang-debug") (v "2.5.2") (d (list (d (n "cairo-lang-utils") (r "^2.5.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1j0zfpl28b8p6na4zaw2xwjnmjx8mdd2b310sn7p4fjdn90qpfby")))

(define-public crate-cairo-lang-debug-2.5.3 (c (n "cairo-lang-debug") (v "2.5.3") (d (list (d (n "cairo-lang-utils") (r "^2.5.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0rskgwn32g1r51ikmwif5mz0bqngjwnmawfallr2k0a0aqljkqxy")))

(define-public crate-cairo-lang-debug-2.6.0-rc.0 (c (n "cairo-lang-debug") (v "2.6.0-rc.0") (d (list (d (n "cairo-lang-utils") (r "^2.6.0-rc.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0qvid9147xyz8lzl9vqpzxqwszl3vz67wx4rf3z1rhb9giwm53x2")))

(define-public crate-cairo-lang-debug-2.5.4 (c (n "cairo-lang-debug") (v "2.5.4") (d (list (d (n "cairo-lang-utils") (r "^2.5.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "10x6sl0aqr950bxv3kffdcyq7zqbmba61cpsaly6n310jal3nqh3")))

(define-public crate-cairo-lang-debug-2.6.0-rc.1 (c (n "cairo-lang-debug") (v "2.6.0-rc.1") (d (list (d (n "cairo-lang-utils") (r "^2.6.0-rc.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0qaa6dk6drly0mwxvjnn1f8y50d8b2ri1d4d5mdcb83f11p36bvc")))

(define-public crate-cairo-lang-debug-2.6.0 (c (n "cairo-lang-debug") (v "2.6.0") (d (list (d (n "cairo-lang-utils") (r "~2.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1q1b0dba766mkw59cnnys2widzmx09s7xvgk6sd5vjq3gk07jccx")))

(define-public crate-cairo-lang-debug-2.6.1 (c (n "cairo-lang-debug") (v "2.6.1") (d (list (d (n "cairo-lang-utils") (r "~2.6.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0v11yaazdxjyqxf10ls2fimnw8hf0img5b5sf4k1ajb6gsfl5alv")))

(define-public crate-cairo-lang-debug-2.6.2 (c (n "cairo-lang-debug") (v "2.6.2") (d (list (d (n "cairo-lang-utils") (r "~2.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "1pvlhpy5x14crh17316r1z8b5rf0vfx37apxd3x90sg90396xsw7")))

(define-public crate-cairo-lang-debug-2.6.3 (c (n "cairo-lang-debug") (v "2.6.3") (d (list (d (n "cairo-lang-utils") (r "~2.6.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.13") (d #t) (k 2)))) (h "0sqc8q6s2lv83wyxkni4f7wia0vpy5s9qwjzshnmm9hywfw1wxjs")))

(define-public crate-cairo-lang-debug-2.7.0-dev.0 (c (n "cairo-lang-debug") (v "2.7.0-dev.0") (d (list (d (n "cairo-lang-utils") (r "~2.7.0-dev.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 2)) (d (n "test-log") (r "^0.2.16") (d #t) (k 2)))) (h "1zf4sh88jxp5iziyqkfzia43wmysll6pykdr8mfbxmnnav2cr8k9")))

