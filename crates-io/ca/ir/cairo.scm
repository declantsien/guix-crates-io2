(define-module (crates-io ca ir cairo) #:use-module (crates-io))

(define-public crate-cairo-0.0.3 (c (n "cairo") (v "0.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0i14ihy9s5fcxig1kyy4djprq614hwxq066kyaam25k20aq1zc97")))

(define-public crate-cairo-0.0.4 (c (n "cairo") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0n46vhvfd1x0f1k6g54nfpdjqyriv7wlihd8hxhlirbpbp4inqck")))

