(define-module (crates-io ca ir cairo-blur) #:use-module (crates-io))

(define-public crate-cairo-blur-0.1.0 (c (n "cairo-blur") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.18") (d #t) (k 0)))) (h "0bidmjsx2ka8zb5slgv673yjdj7mb91w798b2akdaw5mjf6grmdr")))

(define-public crate-cairo-blur-0.1.1 (c (n "cairo-blur") (v "0.1.1") (d (list (d (n "cairo-rs") (r "^0.18") (d #t) (k 0)))) (h "0p4b5lwa7xfn0h1pbhrwhkqwk32g5psmgqkc2wpwp96h4612f0vw")))

