(define-module (crates-io ca ir cairo-sys) #:use-module (crates-io))

(define-public crate-cairo-sys-0.0.1 (c (n "cairo-sys") (v "0.0.1") (h "0gk349282nmi9nkavs1hg5s0wrblyqm3fwfr3ylk4bl7vkv7vmw0")))

(define-public crate-cairo-sys-0.0.2 (c (n "cairo-sys") (v "0.0.2") (h "1j9ay51qzmw2pvz4wy30lhjjdj2sl1mcldlqfgfyj2qq980qvpk7")))

