(define-module (crates-io ca ir cairo-language-server) #:use-module (crates-io))

(define-public crate-cairo-language-server-2.0.0-rc1 (c (n "cairo-language-server") (v "2.0.0-rc1") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0911cxw04mcwz5pcf4fp0cylnfx6kriy013lmb0cl3f7d10j2k3m")))

(define-public crate-cairo-language-server-2.0.0-rc2 (c (n "cairo-language-server") (v "2.0.0-rc2") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "18kj37vx8588wnmm2wmfrvsvq04g7ws1dcw8ryvzazh5cqsr7476")))

(define-public crate-cairo-language-server-2.0.0-rc3 (c (n "cairo-language-server") (v "2.0.0-rc3") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc3") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "19z6kq1i772lcjkww35vizzzahlcbas284jwvrjjh56yqmx4i6qg")))

(define-public crate-cairo-language-server-2.0.0-rc4 (c (n "cairo-language-server") (v "2.0.0-rc4") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc4") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0frizym3i7ws87qkklysskdsz9chk3f1j09fnabwsqq7rw95p4zn")))

(define-public crate-cairo-language-server-2.0.0-rc5 (c (n "cairo-language-server") (v "2.0.0-rc5") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc5") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "02fj388wl8kzc3fzlvg0fqvbgr6nr1a6w37yi7s05h497939a9ip")))

(define-public crate-cairo-language-server-2.0.0-rc6 (c (n "cairo-language-server") (v "2.0.0-rc6") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc6") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1i55ydlx4q8msinaqg0f27f7zbhzxc90jfsmws73ppbidj0fr1nc")))

(define-public crate-cairo-language-server-2.0.0-rc7 (c (n "cairo-language-server") (v "2.0.0-rc7") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0-rc7") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0-rc7") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0b36jf8l4v10rjjzl12klf0in4a6n84ymd7r1wgvmhf2rq078lws")))

(define-public crate-cairo-language-server-2.0.0 (c (n "cairo-language-server") (v "2.0.0") (d (list (d (n "cairo-lang-language-server") (r "^2.0.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0hj1a4ql9p0mdikbvpv5bjc20qxnr9xgm31w5vnabd46vcxw71x4")))

(define-public crate-cairo-language-server-2.0.1 (c (n "cairo-language-server") (v "2.0.1") (d (list (d (n "cairo-lang-language-server") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0zgdydm5aclaxxldqzckj8chl2vp2aa84a7an0x2hy31irbn4nj4")))

(define-public crate-cairo-language-server-2.0.2 (c (n "cairo-language-server") (v "2.0.2") (d (list (d (n "cairo-lang-language-server") (r "^2.0.2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.0.2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1cng04ip6sfzlmcx6hz1zf13vd4zm36pp4rv9iim3c1l8r0hy5na")))

(define-public crate-cairo-language-server-2.1.0-rc0 (c (n "cairo-language-server") (v "2.1.0-rc0") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0-rc0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0sw7hlfpw0dg7xrih4b1835140bx46ydyjppahrw3swl3izm3zn5")))

(define-public crate-cairo-language-server-2.1.0-rc1 (c (n "cairo-language-server") (v "2.1.0-rc1") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0-rc1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1m6s50ffcm9wns6vzgxbksz7c3bvn3pi9z7z1z31icygaqdya3i5")))

(define-public crate-cairo-language-server-2.1.0-rc2 (c (n "cairo-language-server") (v "2.1.0-rc2") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0-rc2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "18qj02s1v2vyp8mb2vncngjhl0zcqgvmq1lf3s33z98mr86wbr5k")))

(define-public crate-cairo-language-server-2.1.0-rc3 (c (n "cairo-language-server") (v "2.1.0-rc3") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0-rc3") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "17i474qiy66w86j5gai9c0mjxpzb5d37c9yslsf34ma0r0ik27rv")))

(define-public crate-cairo-language-server-2.1.0-rc4 (c (n "cairo-language-server") (v "2.1.0-rc4") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0-rc4") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1pyd2bv6wrm28bb7hjgv19l19n5qmrzi1nb43sj382avbixwq2bm")))

(define-public crate-cairo-language-server-2.1.0 (c (n "cairo-language-server") (v "2.1.0") (d (list (d (n "cairo-lang-language-server") (r "^2.1.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1vsh3jnqdrlars7d316libqjsqgqklmg44vqrv00b5v1x56cxlmd")))

(define-public crate-cairo-language-server-2.1.1 (c (n "cairo-language-server") (v "2.1.1") (d (list (d (n "cairo-lang-language-server") (r "^2.1.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.1.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1g5q1wwkrp78nldrhsc64xag8my6978qhgjjcm0rig3xrgl5qnrp")))

(define-public crate-cairo-language-server-2.2.0 (c (n "cairo-language-server") (v "2.2.0") (d (list (d (n "cairo-lang-language-server") (r "^2.2.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.2.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "069sh3f4q5mc3a6v0rw66da61xx3b9cpyzr0xva0k1asr1pjdx1k")))

(define-public crate-cairo-language-server-2.3.0-rc0 (c (n "cairo-language-server") (v "2.3.0-rc0") (d (list (d (n "cairo-lang-language-server") (r "^2.3.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.3.0-rc0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0n1gqyic3lv3jqkmsva8gb7i7j9pphpz2jjvlr26ldnn1zk4cnqv")))

(define-public crate-cairo-language-server-2.3.0 (c (n "cairo-language-server") (v "2.3.0") (d (list (d (n "cairo-lang-language-server") (r "^2.3.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.3.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0px4wbrk3677cj4snsqcffcmxi193iwx81qzhhvsfxid326ww985")))

(define-public crate-cairo-language-server-2.3.1 (c (n "cairo-language-server") (v "2.3.1") (d (list (d (n "cairo-lang-language-server") (r "^2.3.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.3.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "14xsr23ih1r54m8q9572wvlvcsa6v318qc6lafqm44b0hqj9m7x1")))

(define-public crate-cairo-language-server-2.4.0-rc0 (c (n "cairo-language-server") (v "2.4.0-rc0") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1cpadcghsxk5hmf1x7sll7kx4bq1i0fr1g7rqn49a8z9bajlj9qm")))

(define-public crate-cairo-language-server-2.4.0-rc1 (c (n "cairo-language-server") (v "2.4.0-rc1") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1mp0r7872vwg1c687lqimcv7jid4ilsz4kfsspi56mi77a0kw6wx")))

(define-public crate-cairo-language-server-2.4.0-rc2 (c (n "cairo-language-server") (v "2.4.0-rc2") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "030zf5h4nbf6b97qsbsxcmdh1n2c1nmpw6ab73z81pqxwi93gism")))

(define-public crate-cairo-language-server-2.4.0-rc3 (c (n "cairo-language-server") (v "2.4.0-rc3") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc3") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc3") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "08srpvbw3yz6i5dkxv5h8b6wajf6mdm20rcb10rddqmg8frlnc7y")))

(define-public crate-cairo-language-server-2.1.2 (c (n "cairo-language-server") (v "2.1.2") (d (list (d (n "cairo-lang-language-server") (r "=2.1.2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "=2.1.2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "19q9k3fjwczwv6zlnfbrzpncdivchxqx394ic5fdqycp9s52h33b")))

(define-public crate-cairo-language-server-2.4.0-rc4 (c (n "cairo-language-server") (v "2.4.0-rc4") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc4") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc4") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "06k3zbhm0bvl3r5khkqyp3dmsj99gb4hhj9vz5g5bmcvyylm6za2")))

(define-public crate-cairo-language-server-2.4.0-rc5 (c (n "cairo-language-server") (v "2.4.0-rc5") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc5") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc5") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "02cn8cmah4yjbsk07fncjp8npdmm3z9ddk9h4m0waqd55f40g377")))

(define-public crate-cairo-language-server-2.4.0-rc6 (c (n "cairo-language-server") (v "2.4.0-rc6") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0-rc6") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0-rc6") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1y7bgjba36ssb154rs66fxjzxa3v1046gvwlkk4xxqr7ich0d1zj")))

(define-public crate-cairo-language-server-2.4.0 (c (n "cairo-language-server") (v "2.4.0") (d (list (d (n "cairo-lang-language-server") (r "^2.4.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1kv6xfwjig23nfzahgyfzikci8mc9z9nqg9rr4abpnaz5i867ipy")))

(define-public crate-cairo-language-server-2.4.1 (c (n "cairo-language-server") (v "2.4.1") (d (list (d (n "cairo-lang-language-server") (r "^2.4.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1map6h1wx4aaqd5h705n5s8z9wjzm065sqkzxiyzjfi2lxydyhcp")))

(define-public crate-cairo-language-server-2.5.0-dev.0 (c (n "cairo-language-server") (v "2.5.0-dev.0") (d (list (d (n "cairo-lang-language-server") (r "^2.5.0-dev.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.0-dev.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0s4pwp48n4am6314w5gjwdj212ai82ynnjnrhx6gcj7llrsb5pid")))

(define-public crate-cairo-language-server-2.4.2 (c (n "cairo-language-server") (v "2.4.2") (d (list (d (n "cairo-lang-language-server") (r "^2.4.2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1kyqyrh0mrxdw0m3z8ds7hdpsnr2n8d3cq1y0lbdid8ykv8kndgr")))

(define-public crate-cairo-language-server-2.4.3 (c (n "cairo-language-server") (v "2.4.3") (d (list (d (n "cairo-lang-language-server") (r "^2.4.3") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.3") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "101srxggg5n6lwfak1qc0xp0lw7dfccd54qqzmq47lfisljaji1a")))

(define-public crate-cairo-language-server-2.5.0-dev.1 (c (n "cairo-language-server") (v "2.5.0-dev.1") (d (list (d (n "cairo-lang-language-server") (r "^2.5.0-dev.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.0-dev.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1ih28fljbrs97dbb0dhk7hgwhy0plbj31v62wq5w16qirsfx8jxb")))

(define-public crate-cairo-language-server-2.4.4 (c (n "cairo-language-server") (v "2.4.4") (d (list (d (n "cairo-lang-language-server") (r "^2.4.4") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.4.4") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0v79k3zxz4r0gm9yrxf8y94j8wdl4qi70dryd9m6c3f47bc4z4yk")))

(define-public crate-cairo-language-server-2.5.0 (c (n "cairo-language-server") (v "2.5.0") (d (list (d (n "cairo-lang-language-server") (r "^2.5.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "18riip9g2ki85jhqk9n7p172zlkizr7fijgj3zz0ijcgx7p82di7")))

(define-public crate-cairo-language-server-2.5.1 (c (n "cairo-language-server") (v "2.5.1") (d (list (d (n "cairo-lang-language-server") (r "^2.5.1") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.1") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "06i65pdc0m2cgxjcbarwcx8fcgvn4kgxyg1larrmj37d7lk31l8y")))

(define-public crate-cairo-language-server-2.5.2 (c (n "cairo-language-server") (v "2.5.2") (d (list (d (n "cairo-lang-language-server") (r "^2.5.2") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.2") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "0n9im4mw5rwgcynr9fnhqyh9qxafdxgmkkkmiy4vbmvhchsqvwr7")))

(define-public crate-cairo-language-server-2.5.3 (c (n "cairo-language-server") (v "2.5.3") (d (list (d (n "cairo-lang-language-server") (r "^2.5.3") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.3") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1w1g8aiqbq1fkln4kwf0h86acmgdvh0x9qwn8zxlxgd58jvw722h")))

(define-public crate-cairo-language-server-2.6.0-rc.0 (c (n "cairo-language-server") (v "2.6.0-rc.0") (d (list (d (n "cairo-lang-language-server") (r "^2.6.0-rc.0") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.6.0-rc.0") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "1jy1pnc37yda73nkcgap8jnmja4c652rw64kpq296x5a6jvbgi72")))

(define-public crate-cairo-language-server-2.5.4 (c (n "cairo-language-server") (v "2.5.4") (d (list (d (n "cairo-lang-language-server") (r "^2.5.4") (d #t) (k 0)) (d (n "cairo-lang-utils") (r "^2.5.4") (f (quote ("env_logger"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full" "sync"))) (d #t) (k 0)))) (h "139hrc12cfk6afja13qkchv2l0s4ckikdbb0f8x4ipaakd7qgqfn")))

(define-public crate-cairo-language-server-2.6.0-rc.1 (c (n "cairo-language-server") (v "2.6.0-rc.1") (d (list (d (n "cairo-lang-language-server") (r "^2.6.0-rc.1") (d #t) (k 0)))) (h "110a023wpy66m3kk2p8hf2g2f4h7qdgin7xq1d10j3z5211q1975")))

(define-public crate-cairo-language-server-2.6.0 (c (n "cairo-language-server") (v "2.6.0") (d (list (d (n "cairo-lang-language-server") (r "~2.6.0") (d #t) (k 0)))) (h "1mk7ksj8qfc6j8szysqsyvvq4bfhnd9xmydspvjmrvq840ay9n5n")))

(define-public crate-cairo-language-server-2.6.1 (c (n "cairo-language-server") (v "2.6.1") (d (list (d (n "cairo-lang-language-server") (r "~2.6.1") (d #t) (k 0)))) (h "1x8bfqi3gcbk2w3q2dx5xz09y5697cszlr0qh49w9smimglsf1vw")))

(define-public crate-cairo-language-server-2.6.2 (c (n "cairo-language-server") (v "2.6.2") (d (list (d (n "cairo-lang-language-server") (r "~2.6.2") (d #t) (k 0)))) (h "0r7s4dam4nzxjxxbfasf2k4hsvgycvqfcs941g2mqr2vpjfjqsj6")))

(define-public crate-cairo-language-server-2.6.3 (c (n "cairo-language-server") (v "2.6.3") (d (list (d (n "cairo-lang-language-server") (r "~2.6.3") (d #t) (k 0)))) (h "15j4hvlnl1g5ayaca4f6v5m3iwa2hdp5bg9hsji3h32jzv38xscd")))

(define-public crate-cairo-language-server-2.7.0-dev.0 (c (n "cairo-language-server") (v "2.7.0-dev.0") (d (list (d (n "cairo-lang-language-server") (r "~2.7.0-dev.0") (d #t) (k 0)))) (h "1hdp3x56mz47605lxbq2bas42d24xgrd59rxmc0bn74k91qlwgdm")))

