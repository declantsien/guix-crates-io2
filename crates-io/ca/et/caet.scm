(define-module (crates-io ca et caet) #:use-module (crates-io))

(define-public crate-caet-0.1.0 (c (n "caet") (v "0.1.0") (h "1v4q8ii0vpvaf1mrwphikh42dy6hgdvv01rhx8n6m1wrkrcf5h09")))

(define-public crate-caet-0.1.1 (c (n "caet") (v "0.1.1") (h "1bgapkwgzqspl6gmy8wpvzjdyj2iqdwi4zwry1z4ylc057w2zcj6")))

