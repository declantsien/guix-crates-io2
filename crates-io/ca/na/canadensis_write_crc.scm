(define-module (crates-io ca na canadensis_write_crc) #:use-module (crates-io))

(define-public crate-canadensis_write_crc-0.1.0 (c (n "canadensis_write_crc") (v "0.1.0") (d (list (d (n "object") (r "^0.24.0") (f (quote ("read" "std"))) (k 0)))) (h "1y8b3mdsaz71akcri648yibjw1g5dvmzxngl7vjjsql9xwxhxn9z")))

(define-public crate-canadensis_write_crc-0.1.1 (c (n "canadensis_write_crc") (v "0.1.1") (d (list (d (n "crc-any") (r "^2.4.1") (d #t) (k 0)) (d (n "object") (r "^0.24.0") (f (quote ("read" "std"))) (k 0)))) (h "02awhnrq5pll975fd3w60l9df2zjs3hp1jsjj9z9ms2nnh6lxbkh")))

(define-public crate-canadensis_write_crc-0.1.2 (c (n "canadensis_write_crc") (v "0.1.2") (d (list (d (n "crc-any") (r "^2.4.1") (d #t) (k 0)) (d (n "object") (r "^0.24.0") (f (quote ("read" "std"))) (k 0)))) (h "1jq83116m0j4pdwm850djzjbzklyhz0zrh6x0z2h6rfx2bd7abb3")))

(define-public crate-canadensis_write_crc-0.1.3 (c (n "canadensis_write_crc") (v "0.1.3") (d (list (d (n "crc-any") (r "^2.4.1") (d #t) (k 0)) (d (n "object") (r "^0.24.0") (f (quote ("read" "std"))) (k 0)))) (h "083ykjrn8i6ncvfx4zzic0pirp763jhdajdl03mf1rq3bkz246dn")))

(define-public crate-canadensis_write_crc-0.1.4 (c (n "canadensis_write_crc") (v "0.1.4") (d (list (d (n "crc-any") (r "^2.4.1") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("read" "std"))) (k 0)))) (h "11jds899kbvc91k4pdqi886hdy89dgfpg0hdv0vj7p6nifmvh25p")))

