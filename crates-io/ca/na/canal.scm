(define-module (crates-io ca na canal) #:use-module (crates-io))

(define-public crate-canal-0.1.0 (c (n "canal") (v "0.1.0") (h "0v1q0c2cpmz27lhbiylwkpbbjyizrxd9w03bkdzq5fd7qlil5h1b") (f (quote (("nightly"))))))

(define-public crate-canal-0.1.1 (c (n "canal") (v "0.1.1") (h "10nih4400vczsmmx08mawjm0ndgna59vfplyvybvbr10h6kxjih6") (f (quote (("nightly"))))))

(define-public crate-canal-0.1.2 (c (n "canal") (v "0.1.2") (h "1vvixgzdpbmhjgrh57bslmqwarhp20v3kwsqraskhq68r5kszi87") (f (quote (("nightly"))))))

(define-public crate-canal-0.1.3 (c (n "canal") (v "0.1.3") (h "0cr6aj8lyqypvxaf8l0xzixiznbldiwh1z342pph56mmz06874ff")))

