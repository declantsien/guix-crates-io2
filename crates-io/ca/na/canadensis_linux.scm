(define-module (crates-io ca na canadensis_linux) #:use-module (crates-io))

(define-public crate-canadensis_linux-0.1.0 (c (n "canadensis_linux") (v "0.1.0") (d (list (d (n "canadensis_can") (r "^0.1.0") (d #t) (k 0)) (d (n "canadensis_core") (r "^0.1.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "1iqypl05jd8m9scaj1dshwiv0hi7mj0xa924yymzvsdb8ha8vlwd")))

(define-public crate-canadensis_linux-0.2.0 (c (n "canadensis_linux") (v "0.2.0") (d (list (d (n "canadensis_can") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_core") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "0ghhxp6wsjbxjlz5bifg71195z1ay0npaachnsqy6iajq3sywcd7")))

(define-public crate-canadensis_linux-0.2.1 (c (n "canadensis_linux") (v "0.2.1") (d (list (d (n "canadensis_can") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_core") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "1lizpgy626lnwrmmpbpmhhpf9khbayzd6jsm76gsgcpnsigbvncl")))

(define-public crate-canadensis_linux-0.2.2 (c (n "canadensis_linux") (v "0.2.2") (d (list (d (n "canadensis_can") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_core") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "0wd272sigdjj3xzl64jsa49rb8jvs497aci1453g4ja71giiwbij")))

(define-public crate-canadensis_linux-0.3.0 (c (n "canadensis_linux") (v "0.3.0") (d (list (d (n "canadensis_can") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_core") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "0a5570z24f5wcm0irn8khbzq0q8kbmgcwwyl9sdyg1ajrq52iwlg")))

