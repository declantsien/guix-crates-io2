(define-module (crates-io ca na canadensis_encoding) #:use-module (crates-io))

(define-public crate-canadensis_encoding-0.1.0 (c (n "canadensis_encoding") (v "0.1.0") (d (list (d (n "half") (r "^1.7.1") (d #t) (k 0)))) (h "0zm56l1jh977pjmqp73rr5w3nw5hg5s4w1l3v99848syl7djbg9b")))

(define-public crate-canadensis_encoding-0.2.0 (c (n "canadensis_encoding") (v "0.2.0") (d (list (d (n "half") (r "^1.8") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.0") (d #t) (k 0)))) (h "1h2lbx2pjpwnhikvlsbpvxlwkq3nxccx7154dwzrd9k5s3rjv4g7")))

(define-public crate-canadensis_encoding-0.2.1 (c (n "canadensis_encoding") (v "0.2.1") (d (list (d (n "half") (r "^1.8") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.0") (d #t) (k 0)))) (h "0wdhgy43pdk8lr5s93wrcmryv2zmdzvgx855vizvhpbxby5qn4sz")))

(define-public crate-canadensis_encoding-0.2.2 (c (n "canadensis_encoding") (v "0.2.2") (d (list (d (n "half") (r "^1.8") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.0") (d #t) (k 0)))) (h "0xgnj0jx4hrcdj19mhqkhp6lzgp0wjwdkxa57i164wi68imqs7sa")))

(define-public crate-canadensis_encoding-0.3.0 (c (n "canadensis_encoding") (v "0.3.0") (d (list (d (n "half") (r "^2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.0") (d #t) (k 0)))) (h "15f8k5x791fi4fvgk24y9qiqgcm6c2jzkg13mirzv6ybp2jq83r2")))

(define-public crate-canadensis_encoding-0.3.1 (c (n "canadensis_encoding") (v "0.3.1") (d (list (d (n "half") (r "^2.2") (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.0") (d #t) (k 0)))) (h "1498angb48g5zzn1ysl5g6k3z78gj1a4v4rkpllvvhnb0pb2xjls")))

