(define-module (crates-io ca na canaryspy) #:use-module (crates-io))

(define-public crate-canaryspy-0.1.0 (c (n "canaryspy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "august") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)))) (h "1shrm4c8xv9p31d671hbi0kp34mh0j890yrnxkd6xhihjdi6bja9") (y #t)))

(define-public crate-canaryspy-0.1.1 (c (n "canaryspy") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "august") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)))) (h "0l506rf7pqclgml50dhd5rab2ha56rd7kar4yiwnrvbq8pha0gja") (y #t)))

(define-public crate-canaryspy-0.1.2 (c (n "canaryspy") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "august") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)))) (h "0lmhqip42mw339gri2vdk372asibvs1pakn081yg24r4r3hx11v0") (y #t)))

(define-public crate-canaryspy-0.1.3 (c (n "canaryspy") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "august") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)))) (h "1vs31f6jz54nf4kqanbzndaxncnc32xl081sk263iwgsy3yd6yy4")))

