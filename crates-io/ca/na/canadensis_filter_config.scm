(define-module (crates-io ca na canadensis_filter_config) #:use-module (crates-io))

(define-public crate-canadensis_filter_config-0.1.0 (c (n "canadensis_filter_config") (v "0.1.0") (h "11l2zhlmzs49jqww0hr7m0s1kpzaqnsv9yiv6i5iwavj8mvdrgr8")))

(define-public crate-canadensis_filter_config-0.2.0 (c (n "canadensis_filter_config") (v "0.2.0") (h "1314vla47fril8x9hqnnyhkpbqwsr3cc3795p29r55ij4ylp8fir")))

(define-public crate-canadensis_filter_config-0.2.1 (c (n "canadensis_filter_config") (v "0.2.1") (h "07ml73jc686qa60gf7f9c27wikjicm5rzqn04bj8ddq82k7qiyri")))

(define-public crate-canadensis_filter_config-0.2.2 (c (n "canadensis_filter_config") (v "0.2.2") (h "1h8if5ri3ndgsnxp58c8yj516srrflg1ps94grmbfyv9m5i6pwcz")))

