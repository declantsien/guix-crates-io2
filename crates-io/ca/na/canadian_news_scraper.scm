(define-module (crates-io ca na canadian_news_scraper) #:use-module (crates-io))

(define-public crate-canadian_news_scraper-0.1.0 (c (n "canadian_news_scraper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzirxaibhh86pn00nw0a1yn2mg72ndx0a07aj1chb88zq1a4aiw")))

(define-public crate-canadian_news_scraper-0.1.1 (c (n "canadian_news_scraper") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8c3r4rrlbhhbszs1s6vndn1vmyzh1m6vxgpvsayl0lff2lprv9")))

(define-public crate-canadian_news_scraper-0.1.2 (c (n "canadian_news_scraper") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pl7gpx6lgidvkjsl4hcpcyj7hsxmnfnbj8g8nizr9jj6klain88")))

(define-public crate-canadian_news_scraper-0.1.3 (c (n "canadian_news_scraper") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1x8kyifcm1yl0vkvkyni6hawb256qwkggzpj6hyx3qd0b8li02ya") (f (quote (("sync" "tokio"))))))

(define-public crate-canadian_news_scraper-0.1.4 (c (n "canadian_news_scraper") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0sc9bicbsph467l6z5n9syfjpr3i7081ppqml6wxdswa9z72wyc9")))

(define-public crate-canadian_news_scraper-0.1.5 (c (n "canadian_news_scraper") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8zn1yv0jg4zwhyysggclqcfm2gyh9lvs4f79i84n4ffdc4ncdf")))

(define-public crate-canadian_news_scraper-0.1.6 (c (n "canadian_news_scraper") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z4fapbzfb8hb48n9ahh54l3zk0qsmqqqishbnnazmg42agchwgb")))

