(define-module (crates-io ca na canaryvalidationlib) #:use-module (crates-io))

(define-public crate-CanaryValidationLib-0.1.0 (c (n "CanaryValidationLib") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0rhzpfb74z2dyalcfys7cvpxib1c0zimcxfjk4rls7f0vbw750hq")))

(define-public crate-CanaryValidationLib-0.1.1 (c (n "CanaryValidationLib") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1p58n0v1d3im8aplr0x5k5inj96wbsshmam4x6vzwrrkf52c1q34")))

