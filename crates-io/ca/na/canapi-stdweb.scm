(define-module (crates-io ca na canapi-stdweb) #:use-module (crates-io))

(define-public crate-canapi-stdweb-0.1.0 (c (n "canapi-stdweb") (v "0.1.0") (d (list (d (n "canapi") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)))) (h "0hbpj8j1z2cj727axy352nr81rwdk8ifk7b89h2vxxnrqgjck7pj")))

