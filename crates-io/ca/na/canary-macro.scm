(define-module (crates-io ca na canary-macro) #:use-module (crates-io))

(define-public crate-canary-macro-0.0.6 (c (n "canary-macro") (v "0.0.6") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1n0mp29svml55hjrc5xas6m9fqhk8c5hl62c2nzjchbxwp7sql")))

(define-public crate-canary-macro-0.1.1 (c (n "canary-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r286qnmlb09zagh53mda0vlwihz12wk0fdx9srs44m8grjkj9wi")))

(define-public crate-canary-macro-0.1.3 (c (n "canary-macro") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d87q2gmww3fjc34i7447ib065j34s4zsd6zn7bpxnbqp59dlygk")))

