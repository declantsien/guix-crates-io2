(define-module (crates-io ca na canadensis_codegen_rust) #:use-module (crates-io))

(define-public crate-canadensis_codegen_rust-0.2.0 (c (n "canadensis_codegen_rust") (v "0.2.0") (d (list (d (n "canadensis_bit_length_set") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "15dpcb3bcclnxyvsyw73nq6ws1mywvnajc4f11aq11cg43cfk7n0")))

(define-public crate-canadensis_codegen_rust-0.2.1 (c (n "canadensis_codegen_rust") (v "0.2.1") (d (list (d (n "canadensis_bit_length_set") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "1ng67gb8l9mxn88l4d2r5lfn1l41vy31qg14z8xs66zjhas9scy3")))

(define-public crate-canadensis_codegen_rust-0.3.0 (c (n "canadensis_codegen_rust") (v "0.3.0") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "0gs361bd0gnrpg27qlbjk1gnw70ivw1ggkwcjvpqnz5dmdgfapay")))

(define-public crate-canadensis_codegen_rust-0.3.1 (c (n "canadensis_codegen_rust") (v "0.3.1") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)))) (h "19gszmwyn9fkl6lc8g5kdgmk0slq809m9ad2w12f63lrva6qcm3k")))

(define-public crate-canadensis_codegen_rust-0.3.2 (c (n "canadensis_codegen_rust") (v "0.3.2") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)))) (h "1zzb7vkg1hf10f7cssaf43yinp4zhjbxakz6rr24l35a6zjfvpn0")))

(define-public crate-canadensis_codegen_rust-0.4.0 (c (n "canadensis_codegen_rust") (v "0.4.0") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1syv4lvl09jf6qdbcs1b757akf6ji3ph6fk1i23jnj0nnbp7yxmw")))

(define-public crate-canadensis_codegen_rust-0.4.1 (c (n "canadensis_codegen_rust") (v "0.4.1") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1ys234ykw964c6llhjxpvhlcg87zz3c9ylzj2vmhxiz9dw83xlyf")))

(define-public crate-canadensis_codegen_rust-0.4.2 (c (n "canadensis_codegen_rust") (v "0.4.2") (d (list (d (n "canadensis_bit_length_set") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_dsdl_frontend") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1rdm94k56nibsqq5pdalz5rwiy3n71yikm7z7gznr0lr2p7irars")))

