(define-module (crates-io ca na canapi) #:use-module (crates-io))

(define-public crate-canapi-0.1.0 (c (n "canapi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1i4v7rb355zd9dx7c937781mvxmbbbwn5nyja7c33da49yh04gpz")))

(define-public crate-canapi-0.2.0 (c (n "canapi") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b4nkjcf1sxshjllj6ij0qy3g40s7l6p7dsip0cvzy6fxp8xdd5a")))

