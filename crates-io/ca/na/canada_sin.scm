(define-module (crates-io ca na canada_sin) #:use-module (crates-io))

(define-public crate-canada_sin-1.0.0 (c (n "canada_sin") (v "1.0.0") (h "0b3276506xva9cfz3kqbnsmxrww14bhvx7sy9wizsa00wr4bl7c2") (y #t)))

(define-public crate-canada_sin-1.0.1 (c (n "canada_sin") (v "1.0.1") (h "0sj6rcdzici7l54kprfacxi9cqmpxn436yij1fsz6wa26vyvpkqg")))

(define-public crate-canada_sin-1.0.2 (c (n "canada_sin") (v "1.0.2") (h "0qcpgnw51aa9498dr6nrb7i7kqyzw1amiv5yvf2wgrvqnzkz7zpg")))

(define-public crate-canada_sin-1.1.0 (c (n "canada_sin") (v "1.1.0") (h "0fka3dl63h276l7h08h5ncyqq2vi8q0pqr7zc6jbzmd8jqkyg6xx")))

