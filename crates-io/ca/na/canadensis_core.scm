(define-module (crates-io ca na canadensis_core) #:use-module (crates-io))

(define-public crate-canadensis_core-0.1.0 (c (n "canadensis_core") (v "0.1.0") (d (list (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)))) (h "012j24gfnidc09pdgvnhlfz6vfnjz15dhd119wwlbvqhvfkh3sf8")))

(define-public crate-canadensis_core-0.2.0 (c (n "canadensis_core") (v "0.2.0") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0mzb0zh0ggsj53qbfgprf6zpjrxsf2ywyj18bg1811c4k3llb39f")))

(define-public crate-canadensis_core-0.2.1 (c (n "canadensis_core") (v "0.2.1") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1ijrph1abzr3w86wz161c92ff7k28lc0j22lq9bcfjbdhy06czpj")))

(define-public crate-canadensis_core-0.2.2 (c (n "canadensis_core") (v "0.2.2") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0hgri6rp49rd8qgyxfqiiaviy7sl04fgksm8vhsapgp78z36b238")))

(define-public crate-canadensis_core-0.2.3 (c (n "canadensis_core") (v "0.2.3") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0a5cf6l8fkg5vdz2djv6mh504sshp48xrhhlynbzp8lsg82kgxpw")))

(define-public crate-canadensis_core-0.3.0 (c (n "canadensis_core") (v "0.3.0") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1h5zp7qp99qqacjy8zslsljv6kj61npf6l5gxism6fqm7z2003qb")))

