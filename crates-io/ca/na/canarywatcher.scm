(define-module (crates-io ca na canarywatcher) #:use-module (crates-io))

(define-public crate-canarywatcher-0.1.0 (c (n "canarywatcher") (v "0.1.0") (d (list (d (n "inotify") (r "^0.4") (d #t) (k 0)))) (h "1v25s2wlaqbssxl1njm5fpx51fnnw878yv9yqpkr9jcfsflz9h4x")))

(define-public crate-canarywatcher-0.1.1 (c (n "canarywatcher") (v "0.1.1") (d (list (d (n "inotify") (r "^0.4") (d #t) (k 0)))) (h "0sh4r22arj9wizfjbfrwlpkrc87ms23gk6mpcbzbj7q9h35bxmrx")))

(define-public crate-canarywatcher-0.1.2 (c (n "canarywatcher") (v "0.1.2") (d (list (d (n "inotify") (r "^0.5.1") (d #t) (k 0)))) (h "06bjb19nda33lsjncc29v59mak18ywrv5y6vmks6vdrzn1lpwj7w")))

(define-public crate-canarywatcher-0.2.0 (c (n "canarywatcher") (v "0.2.0") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "inotify") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "procinfo") (r "^0.4.2") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1kc11fchwgcp01viwj7k88ddr47hjvg7ha30a7q7ynjny5yp1h8m")))

(define-public crate-canarywatcher-0.2.1 (c (n "canarywatcher") (v "0.2.1") (d (list (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "inotify") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "procinfo") (r "^0.4.2") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "005lyjram3dx92bqs3s5vracbj653q883h18gnh4x1lbf5d82wn1")))

