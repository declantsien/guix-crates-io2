(define-module (crates-io ca na canadensis_header) #:use-module (crates-io))

(define-public crate-canadensis_header-0.1.0 (c (n "canadensis_header") (v "0.1.0") (d (list (d (n "canadensis_core") (r "^0.3.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.4.0") (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "17f2qhz143dm3kqqdrqrahfvynhiksrjqayagxqzy92y1xhjbxmz")))

