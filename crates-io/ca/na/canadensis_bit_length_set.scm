(define-module (crates-io ca na canadensis_bit_length_set) #:use-module (crates-io))

(define-public crate-canadensis_bit_length_set-0.2.0 (c (n "canadensis_bit_length_set") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)))) (h "09bbngfxfys0zh7sr0s5gx54i5b1kswmgldfxazl0rw2xwwzvhlc")))

(define-public crate-canadensis_bit_length_set-0.2.1 (c (n "canadensis_bit_length_set") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)))) (h "0xwv6q47690z79g21z7x5l6731gyw3daava78353lb5pmcgabraq")))

(define-public crate-canadensis_bit_length_set-0.3.0 (c (n "canadensis_bit_length_set") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)))) (h "1nb4z3pyzqzg879h4jxpx40ba6a2vck43npcasr876sl88ij2qpa")))

(define-public crate-canadensis_bit_length_set-0.3.1 (c (n "canadensis_bit_length_set") (v "0.3.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)))) (h "1752p6kfzfk28fhjp93q3jbz0pdhmvbi349vq5rs81cswwiki4ln")))

