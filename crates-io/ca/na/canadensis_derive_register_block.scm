(define-module (crates-io ca na canadensis_derive_register_block) #:use-module (crates-io))

(define-public crate-canadensis_derive_register_block-0.1.0 (c (n "canadensis_derive_register_block") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v72qpga1d41vk7plffw1bw44ir9vj4bdnl6rjq4c4y5idr0v05k")))

(define-public crate-canadensis_derive_register_block-0.2.0 (c (n "canadensis_derive_register_block") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cbi1lsiz3yj2d8fgq010c2vb66dl831qhbmck21qk6lkn2j78y5")))

(define-public crate-canadensis_derive_register_block-0.2.1 (c (n "canadensis_derive_register_block") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lxvsiva6ars95isk6rb7zqrm8df8xhb8pznlaiy6ccwxlmwdgm8")))

(define-public crate-canadensis_derive_register_block-0.2.2 (c (n "canadensis_derive_register_block") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dx1lvw2b40ik6gwfdk7jsarwf3chaa5y7i3jv0l71ncz2075471")))

