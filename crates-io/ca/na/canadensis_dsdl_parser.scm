(define-module (crates-io ca na canadensis_dsdl_parser) #:use-module (crates-io))

(define-public crate-canadensis_dsdl_parser-0.2.0 (c (n "canadensis_dsdl_parser") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "03pasbjgjqimx825hlf9mm93f7wwv0wd9abcmvb4ghq55kycz4nb")))

(define-public crate-canadensis_dsdl_parser-0.2.1 (c (n "canadensis_dsdl_parser") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1k2pqmydm4xzszlaq7phjzxyqc9hikb50s1ybkq2091hsy4ifxss")))

(define-public crate-canadensis_dsdl_parser-0.2.2 (c (n "canadensis_dsdl_parser") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "09rck0plf5gg9x6il3bbvq32yi9zxyzbq5pmr0wdf7jbgsqif0ix")))

(define-public crate-canadensis_dsdl_parser-0.2.3 (c (n "canadensis_dsdl_parser") (v "0.2.3") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0mk7vm8sh3a1wizfrs9y6lvz6ydn65i620ayidx4r03qaq0gp5c2")))

(define-public crate-canadensis_dsdl_parser-0.3.0 (c (n "canadensis_dsdl_parser") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "19k4ph9nmf3zn9krr1q3k1pjybrphfq8qgj35zdvpr1fdldbg202")))

