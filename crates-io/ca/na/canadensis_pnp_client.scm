(define-module (crates-io ca na canadensis_pnp_client) #:use-module (crates-io))

(define-public crate-canadensis_pnp_client-0.1.0 (c (n "canadensis_pnp_client") (v "0.1.0") (d (list (d (n "canadensis") (r "^0.1.0") (d #t) (k 0)) (d (n "canadensis_data_types") (r "^0.1.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.1.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.3.9") (k 0)))) (h "0ziargzim28a3i46fdlj36qhhr39nb5sj6zai4rgl99mvcpy6j36")))

(define-public crate-canadensis_pnp_client-0.2.0 (c (n "canadensis_pnp_client") (v "0.2.0") (d (list (d (n "canadensis") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_data_types") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.3.9") (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "03i41qwj6g0wgrlv3702p61m3qa4rf6npi6l4lhdqbgj5y9mdkvs")))

(define-public crate-canadensis_pnp_client-0.2.1 (c (n "canadensis_pnp_client") (v "0.2.1") (d (list (d (n "canadensis") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_data_types") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.3.9") (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "1mzpkxgxpmxyjl53ayzd85f2ry7s8pr62d7mk7hr41n0x99cpjgs")))

(define-public crate-canadensis_pnp_client-0.2.2 (c (n "canadensis_pnp_client") (v "0.2.2") (d (list (d (n "canadensis") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_data_types") (r "^0.2.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.3.9") (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "1mdx6fgcjkkcxfdxgazp2cipiq5ngbr2gz4k3i0b8aj62pcpn51f")))

(define-public crate-canadensis_pnp_client-0.3.0 (c (n "canadensis_pnp_client") (v "0.3.0") (d (list (d (n "canadensis") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_data_types") (r "^0.3.0") (d #t) (k 0)) (d (n "canadensis_filter_config") (r "^0.2.0") (d #t) (k 0)) (d (n "crc-any") (r "^2.4.0") (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)))) (h "0lwjc7m7xzkvmmqlwqh70wam035z8991rnj89gpszygq3zw2aky4")))

