(define-module (crates-io ca tw catwalk-s-protobuf) #:use-module (crates-io))

(define-public crate-catwalk-s-protobuf-0.1.0 (c (n "catwalk-s-protobuf") (v "0.1.0") (d (list (d (n "catwalk") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "protobuf") (r ">=2.18.0, <3.0.0") (d #t) (k 0)) (d (n "zoet") (r ">=0.1.6, <0.2.0") (d #t) (k 0)))) (h "1nnzkz6l7vvmb6x1rzx42lk51szqqpn8vzayr0izjns2h20i3pf7")))

