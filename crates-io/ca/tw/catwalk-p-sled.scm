(define-module (crates-io ca tw catwalk-p-sled) #:use-module (crates-io))

(define-public crate-catwalk-p-sled-0.1.0 (c (n "catwalk-p-sled") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "catwalk") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "color-backtrace") (r ">=0.5.0, <0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "sled") (r ">=0.34.6, <0.35.0") (f (quote ("compression"))) (d #t) (k 0)) (d (n "sled") (r ">=0.34.6, <0.35.0") (f (quote ("compression" "event_log" "pretty_backtrace" "docs" "color-backtrace"))) (d #t) (k 2)))) (h "1mmla66s6x9f4hpx6yaq4z6d423vi77hv95hcny1hp72gvh1y49v")))

