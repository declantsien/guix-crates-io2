(define-module (crates-io ca tw catwalk) #:use-module (crates-io))

(define-public crate-catwalk-0.1.0 (c (n "catwalk") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "color-backtrace") (r ">=0.5.0, <0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "semver") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "0gr8zrmfyaspvc9cngf709sgnkqr37hq8diy5scvxknrna9vgh0p")))

