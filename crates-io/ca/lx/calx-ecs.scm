(define-module (crates-io ca lx calx-ecs) #:use-module (crates-io))

(define-public crate-calx-ecs-0.3.0 (c (n "calx-ecs") (v "0.3.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1chx7hmfkn3pal32zb0my6shpq0lfy88c84w4b2bjlicbxzq12ck")))

(define-public crate-calx-ecs-0.4.0 (c (n "calx-ecs") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0fwwcjjfdh33amihh8hc81m1bghm6pwnjp6lizvpwwqmmysyz3rc")))

(define-public crate-calx-ecs-0.5.0 (c (n "calx-ecs") (v "0.5.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0d2l36gnrcg6xbdnmp83q1bx9gc6x2gnxjs1qwdlpd3n9mj3kd69")))

(define-public crate-calx-ecs-0.6.0 (c (n "calx-ecs") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hb3f17j2dz6rrasaj2qjqyvx1xck641ic6cpvpja5j1pdamgk1m")))

(define-public crate-calx-ecs-0.6.1 (c (n "calx-ecs") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q9cckw1xrlqn7pgjbnwcvxv8ww8phi847hbpbwwnrc4hvvzxblr")))

