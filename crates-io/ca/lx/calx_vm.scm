(define-module (crates-io ca lx calx_vm) #:use-module (crates-io))

(define-public crate-calx_vm-0.1.0 (c (n "calx_vm") (v "0.1.0") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z2l6asqsfr69mljcz82333m2b31dvjyhk0vnj93lhjbb72yz7fd")))

(define-public crate-calx_vm-0.1.1 (c (n "calx_vm") (v "0.1.1") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0583wazzlp30s7g3m7837gy2kdz3gfbaqzlfdfkrqdpgjxcy18zh")))

(define-public crate-calx_vm-0.1.2 (c (n "calx_vm") (v "0.1.2") (d (list (d (n "cirru_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sxknk8l1wypi9k424iidcgb46k08acgxik773p8nwxvjdbjcybj")))

(define-public crate-calx_vm-0.1.3 (c (n "calx_vm") (v "0.1.3") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.23") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0ha7k5y4srbv0fpa6562701w7hqwf1wzzfjcpc2v1nd1xhjkb2yv")))

(define-public crate-calx_vm-0.1.4 (c (n "calx_vm") (v "0.1.4") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.23") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0za0kk55nqn52bszzbhs1x3dcb2rx7xiz12723nnp905vyklgrmh")))

(define-public crate-calx_vm-0.1.5 (c (n "calx_vm") (v "0.1.5") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.25") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1m4i1j7pf3fgpij1zyflrwi7h759hgpax3mdivr2dcicxci3rm8i")))

(define-public crate-calx_vm-0.2.0-a1 (c (n "calx_vm") (v "0.2.0-a1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.26") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1vjv6c60l353wlkif5ifzvhynhjciyvh301rrnbvkmpvndwvzljx")))

(define-public crate-calx_vm-0.2.0 (c (n "calx_vm") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.26") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)))) (h "0mfsn6xq13lxkwm3cvigkylmazl41h83a2bb79ybrahymlb7ha1r")))

