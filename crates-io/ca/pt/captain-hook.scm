(define-module (crates-io ca pt captain-hook) #:use-module (crates-io))

(define-public crate-captain-hook-0.1.2 (c (n "captain-hook") (v "0.1.2") (h "0avd3rlbd6q65wpbf1gxcgj9kk2056hb35bxn5j2qnzdnihnlapp")))

(define-public crate-captain-hook-0.2.0 (c (n "captain-hook") (v "0.2.0") (h "0zrx5jjzyln4pmhr9rs7sjb75ynkp8ly4qk5aj4pgsrfqj994nlp")))

(define-public crate-captain-hook-0.2.1 (c (n "captain-hook") (v "0.2.1") (h "0jy7k9gpbl16dmacl6fad252dx61s5m45572gqzxxvnd6hphg2hw")))

(define-public crate-captain-hook-0.2.2 (c (n "captain-hook") (v "0.2.2") (h "09wv5234cmmb7qb1z6irh04sb1s1dv30k1c2izbvm246ilvfprpd")))

