(define-module (crates-io ca pt capture-logger) #:use-module (crates-io))

(define-public crate-capture-logger-0.1.0 (c (n "capture-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "191xdg09knh715c6rh9g1b8pkb3gsq5pczvhippi5v17m2sanqwh")))

(define-public crate-capture-logger-0.1.1 (c (n "capture-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10pz2d5y5gszqkjb21njvdg9smq49k61n999sdyfisac0p2mjnk0")))

