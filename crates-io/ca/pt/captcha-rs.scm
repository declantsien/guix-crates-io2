(define-module (crates-io ca pt captcha-rs) #:use-module (crates-io))

(define-public crate-captcha-rs-0.1.0 (c (n "captcha-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0jxd4pcw54l63i8dhwiysk6rijrb8fvgik24ccpl66icdww3s1f0")))

(define-public crate-captcha-rs-0.1.1 (c (n "captcha-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1hxzlzncvlycvi10wjni0h9xqwh024156yvj1n80zc669xk0bwhi")))

(define-public crate-captcha-rs-0.1.2 (c (n "captcha-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "12jsbfnsc9fcvj15904pzvd59hjkw2pcv9cqpn1k34nb5kfdma3h")))

(define-public crate-captcha-rs-0.1.3 (c (n "captcha-rs") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "09gaf9z249w26z0nh984mkwb6pzc8cmbnblgl262hjwc36b51508")))

(define-public crate-captcha-rs-0.1.4 (c (n "captcha-rs") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1ffm4g1ks6zb079dvj31rslsh25k87pd0h331id4s70yylifna2g")))

(define-public crate-captcha-rs-0.1.5 (c (n "captcha-rs") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0zr6p1s8fvgm8ri8ci1xxgq6jkvaq2l3wfa6z3wpxlzjzxgdfh2j")))

(define-public crate-captcha-rs-0.1.6 (c (n "captcha-rs") (v "0.1.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0fwrw8b0imwf4ryjfdh5hv918nrbi9z4vhnvs4wmyg8ir0r683zy")))

(define-public crate-captcha-rs-0.1.7 (c (n "captcha-rs") (v "0.1.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1wm4bw8ianngjkpkwqi1ld6gz7b9gxkbk4f7r80vdjvkcppqll9s")))

(define-public crate-captcha-rs-0.1.8 (c (n "captcha-rs") (v "0.1.8") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1px2jy5gh8hv3cg98jhhlyqq0plnqz1a4xbvd7xqa6jr0mnvhzkz")))

(define-public crate-captcha-rs-0.1.9 (c (n "captcha-rs") (v "0.1.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1xy2qinccs2x0mgj2jif1h0mzvzigjl27g4s8xw6dkciq0ln292i")))

(define-public crate-captcha-rs-0.2.0 (c (n "captcha-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0x792vs46z8kl3d8wk4rp38jb2i9v64wf8paczbd8szlibbq0hj3")))

(define-public crate-captcha-rs-0.2.1 (c (n "captcha-rs") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0f7l61h2gzhbybmdqhgs21d7xwin82g2vip8ja67yycz8w8dswfw")))

(define-public crate-captcha-rs-0.2.2 (c (n "captcha-rs") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1h4b8v6gbm5zravhb8p07yl6li5kab1z5wqlk9ffa2gj8j4mjwxn")))

(define-public crate-captcha-rs-0.2.3 (c (n "captcha-rs") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "13jz3qjhamdsiijw0njac5skxaax2f87fj077v8wwsnj0yvp820w")))

(define-public crate-captcha-rs-0.2.4 (c (n "captcha-rs") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "11xb81kpz7ffchsmlz3jnjs7srmxmb0b0rih1d9632ggcji9ymwp")))

(define-public crate-captcha-rs-0.2.5 (c (n "captcha-rs") (v "0.2.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0ws4pq2pmmk1cfvlahai0yllvma6k28czfab28nwasy7b78w09qn")))

(define-public crate-captcha-rs-0.2.6 (c (n "captcha-rs") (v "0.2.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0l1an81aa0d0kw6v5whyb0cbzy2nwpxhva0s9x4ghvpznf275bn1")))

(define-public crate-captcha-rs-0.2.7 (c (n "captcha-rs") (v "0.2.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1v4v60n07q976dy42qskgy8p4ik5fsyr8s6xg1gslh03hw9a9j27")))

(define-public crate-captcha-rs-0.2.8 (c (n "captcha-rs") (v "0.2.8") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "101gbdya82q3f3ghmpbfbq4z0d5k8hp8f2p6gizgi2n6azdbcb4a")))

(define-public crate-captcha-rs-0.2.9 (c (n "captcha-rs") (v "0.2.9") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "17qz7gc509cz4lfihp4mbwni5xbzx01xwq01njdra0kgzcsz7952")))

(define-public crate-captcha-rs-0.2.10 (c (n "captcha-rs") (v "0.2.10") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("jpeg"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "111brixdgjl99vzsx3g0dfzlpn0jai0cpb6hiwrx4gwz2hvkzml4")))

