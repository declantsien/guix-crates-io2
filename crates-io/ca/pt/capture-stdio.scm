(define-module (crates-io ca pt capture-stdio) #:use-module (crates-io))

(define-public crate-capture-stdio-0.1.0 (c (n "capture-stdio") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "os_pipe") (r "^1.0.1") (d #t) (k 0)))) (h "1v9r0m4gy1y68hbj01hkr9cs6c9h5wdx3d02lfmmcpkwm3ypz5pg")))

(define-public crate-capture-stdio-0.1.1 (c (n "capture-stdio") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "os_pipe") (r "^1.0.1") (d #t) (k 0)))) (h "04ba4xmz1x77wysmac09vgnz85himgrpndvw75cz7lckgirn6h9w")))

