(define-module (crates-io ca pt captain-workflow-manager) #:use-module (crates-io))

(define-public crate-captain-workflow-manager-0.1.0 (c (n "captain-workflow-manager") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "fence") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i1fn24kp5yq1318pj3ni6kii250gg5y0y2zfc71gwpda49wcg1b")))

