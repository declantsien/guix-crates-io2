(define-module (crates-io ca pt captcha_rust) #:use-module (crates-io))

(define-public crate-captcha_rust-0.1.0 (c (n "captcha_rust") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0yr7266v1kw6y5i6zv170kr2s4gdvkv570sl3502rb8wzcw3id9g")))

(define-public crate-captcha_rust-0.1.1 (c (n "captcha_rust") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1q7biax6if63grd8sbjmfr9fbdzmiqbhwnigsfd4f8vv0cr9d205")))

(define-public crate-captcha_rust-0.1.2 (c (n "captcha_rust") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1kb9f07glkskwknzwgc3j2yvjnakyw99qqxj9qii5gzvbzm91a39")))

(define-public crate-captcha_rust-0.1.3 (c (n "captcha_rust") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1dk58y1wxa0l2pvf2q64x51j6zgjr274gxqfh0pa2yh1s0bs6p3n")))

