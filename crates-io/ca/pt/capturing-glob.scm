(define-module (crates-io ca pt capturing-glob) #:use-module (crates-io))

(define-public crate-capturing-glob-0.1.0 (c (n "capturing-glob") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hx2gnd7756g6w11zq3syxf20vh5bmv5a2ww39x5n7zq2ibg19pm")))

(define-public crate-capturing-glob-0.1.1 (c (n "capturing-glob") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qyjp90y10nyk7s8pzxr2wanb63xsy7wd9y5wi6bxyqgqcflg959")))

