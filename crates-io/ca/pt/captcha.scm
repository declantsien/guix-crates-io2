(define-module (crates-io ca pt captcha) #:use-module (crates-io))

(define-public crate-captcha-0.0.1 (c (n "captcha") (v "0.0.1") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1h7ijxyzsfmm4l5isvhacg0s60ghi9arwqpw88hkkh6w1ilhpijl")))

(define-public crate-captcha-0.0.2 (c (n "captcha") (v "0.0.2") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0yjih5hr12d0ajxmzwznqynff32hrjrsd6ar0499gwgq9rcrk4n3")))

(define-public crate-captcha-0.0.3 (c (n "captcha") (v "0.0.3") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1zvfj2phicp0a50jxrrq36p8bpnadl0080bnp6x641v78bygvk6a")))

(define-public crate-captcha-0.0.4 (c (n "captcha") (v "0.0.4") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1wc1ln2wg99j66xdn9gmyc5m3070cxns3gsd7idrbjbx0c4akzjm")))

(define-public crate-captcha-0.0.5 (c (n "captcha") (v "0.0.5") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0b56b9qjhsvwm0m9vpx1qxx9jda71sv68ld722swim5sn082rjin")))

(define-public crate-captcha-0.0.6 (c (n "captcha") (v "0.0.6") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1l4as7jcs1rrianl1r2ba4h44g7fmiaqcp3r8dh2vhd2i1x80mk5")))

(define-public crate-captcha-0.0.7 (c (n "captcha") (v "0.0.7") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1v84mypbgjjzhs0r5s8wkc9hd60ljfhyjj1lkpl2znrswhxhl1jd")))

(define-public crate-captcha-0.0.8 (c (n "captcha") (v "0.0.8") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "lodepng") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "1g557clsr0clkrdhdzv2hpyaajpia287r39jw78gcd24fhw60999")))

(define-public crate-captcha-0.0.9 (c (n "captcha") (v "0.0.9") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hound") (r "^3.4") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (f (quote ("png"))) (k 0)) (d (n "lodepng") (r "^3.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 2)))) (h "1axh5y30likp51yv47nb5cyyakxaxr1aivy3l9lgj9dl6w1ph8fv") (f (quote (("default" "audio") ("audio" "hound"))))))

