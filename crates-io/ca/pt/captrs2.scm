(define-module (crates-io ca pt captrs2) #:use-module (crates-io))

(define-public crate-captrs2-0.1.0 (c (n "captrs2") (v "0.1.0") (d (list (d (n "dxgcap") (r "^0.2.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0dadqg3gj9czbw4gql01x8khhami7w578k4r4b354y1si0iznx88") (y #t)))

(define-public crate-captrs2-0.1.1 (c (n "captrs2") (v "0.1.1") (d (list (d (n "dxgcap2") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1xdwv63b0hl4lywzd019s1dfx3ia0b6g40zr5klhld55y5vnzbi2")))

(define-public crate-captrs2-0.1.2 (c (n "captrs2") (v "0.1.2") (d (list (d (n "dxgcap2") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1js9yynf0lhdga414c7m065h7l6zv14kb2aiq0ma2fxlvnwyz5pd")))

(define-public crate-captrs2-0.2.0 (c (n "captrs2") (v "0.2.0") (d (list (d (n "dxgcap2") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0xn5bwl8is2dvwg9aind29pfaaba3dwykqryfy373nnfhir8m4dr")))

(define-public crate-captrs2-0.2.1 (c (n "captrs2") (v "0.2.1") (d (list (d (n "dxgcap2") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0xgbzrvfijx6pnapal7xbc3r43qxw80hpmq84f3lp64i2jpiz3p1")))

(define-public crate-captrs2-0.2.2 (c (n "captrs2") (v "0.2.2") (d (list (d (n "dxgcap2") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "qoi") (r "^0.4.1") (d #t) (k 2)) (d (n "shuteye") (r "^0.3.3") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1klgvgqbfh7wpvrd4y9qfiwwibywlxg6ilivppnsvka4s3qd7h4r")))

