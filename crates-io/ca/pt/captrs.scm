(define-module (crates-io ca pt captrs) #:use-module (crates-io))

(define-public crate-captrs-0.1.0 (c (n "captrs") (v "0.1.0") (d (list (d (n "dxgcap") (r "^0.0.8") (d #t) (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.3.1") (d #t) (k 0)))) (h "1x290k3rdz4s1r93zjsmbsf07xs4z7xa8pbjqmfamzvnkcvn67sd")))

(define-public crate-captrs-0.2.0 (c (n "captrs") (v "0.2.0") (d (list (d (n "dxgcap") (r "^0.0.8") (d #t) (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4") (d #t) (k 0)))) (h "0m8f6h0qkjrw29a4zfhyc399zb000miylwh18zhdzwjhz05lpqfc")))

(define-public crate-captrs-0.2.1 (c (n "captrs") (v "0.2.1") (d (list (d (n "dxgcap") (r "^0.0.8") (d #t) (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (k 0)))) (h "0n7x3gn8nnj1jwa6p7azf449dxgif2gs8clkha9jiafcqj8mrpdb")))

(define-public crate-captrs-0.2.2 (c (n "captrs") (v "0.2.2") (d (list (d (n "dxgcap") (r "^0.0.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1n4sj2riyvv4nyajkr84szr6mn24jqxyl96m7w1sah7fym4slmzp")))

(define-public crate-captrs-0.2.3 (c (n "captrs") (v "0.2.3") (d (list (d (n "dxgcap") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1dw5a0z9n7v0cx3anrk4lj9jb47a8hmkxkdd09dnjpx2ac6kvc49")))

(define-public crate-captrs-0.3.0 (c (n "captrs") (v "0.3.0") (d (list (d (n "dxgcap") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1i43slp8srl28dw6qgmxslkmd8jhv5bfqfzvdqrmr6gqqfa82k1r")))

(define-public crate-captrs-0.3.1 (c (n "captrs") (v "0.3.1") (d (list (d (n "dxgcap") (r "^0.2.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "shuteye") (r "^0.2") (d #t) (k 2)) (d (n "x11cap") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "06yvhfmg078rzsdv9mrqy60hgsjh6gisrnqcmplwr8qfpvqi5lys")))

