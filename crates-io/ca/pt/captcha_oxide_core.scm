(define-module (crates-io ca pt captcha_oxide_core) #:use-module (crates-io))

(define-public crate-captcha_oxide_core-0.1.0 (c (n "captcha_oxide_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "full" "extra-traits" "clone-impls"))) (k 0)))) (h "13jy03rdmhmvjavwvnfsmvbw74cd3z0pnc8v9nlg7j5farj31500")))

(define-public crate-captcha_oxide_core-0.1.1 (c (n "captcha_oxide_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "full" "extra-traits" "clone-impls"))) (k 0)))) (h "1r35xfy60g25r9rg6zx7saw8n7spfilmca2a7kjaxvnvvp0qcsmw")))

