(define-module (crates-io ca pt captcha_oxide_derive) #:use-module (crates-io))

(define-public crate-captcha_oxide_derive-3.1.2 (c (n "captcha_oxide_derive") (v "3.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rs5g22qk3igpvq7g73p9wm188107ksp4ywxszzvfj7j99sfmjx8")))

(define-public crate-captcha_oxide_derive-3.4.0 (c (n "captcha_oxide_derive") (v "3.4.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "deluxe") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05dn3zj8yfsfzwinhzqkrb2idv08qn0rgrs7ixrzmf4q8zpqip9n")))

(define-public crate-captcha_oxide_derive-4.0.0-rc.1 (c (n "captcha_oxide_derive") (v "4.0.0-rc.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "deluxe") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00pyad943j6ghyyjkdcz06m31gzlwaxn4d50bv6bq3qrszkgmnk2")))

(define-public crate-captcha_oxide_derive-4.0.0-rc.2 (c (n "captcha_oxide_derive") (v "4.0.0-rc.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "deluxe") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1575p56y54lribq0jqskxmg6qggim7c2cd4zwjzaqgxkl4554w5s")))

(define-public crate-captcha_oxide_derive-4.0.0 (c (n "captcha_oxide_derive") (v "4.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "deluxe") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0x89ckc542p6v27m655dswswx3nxc6c0ldzmqg93r73kjgwx18c0")))

(define-public crate-captcha_oxide_derive-5.0.0 (c (n "captcha_oxide_derive") (v "5.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "deluxe") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08hl863b2w9whin4ym5l4m3r61f6rrdh9r335kk6jk8iiyx9lzal")))

