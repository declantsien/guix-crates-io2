(define-module (crates-io ca pt captchacam) #:use-module (crates-io))

(define-public crate-captchacam-1.1.2 (c (n "captchacam") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nokhwa") (r "^0.10.4") (f (quote ("input-native"))) (d #t) (k 0)) (d (n "self_update") (r "^0.37.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "11lp7jhyhbfk96xxdh9lpr42jxajz9fvfdynhx0wphz870vlf9mq")))

