(define-module (crates-io ca pt capture-it) #:use-module (crates-io))

(define-public crate-capture-it-0.1.0 (c (n "capture-it") (v "0.1.0") (h "1bby9li946yakla5sz9qk4fbfdblbc1h4ildrwkww5rif69adfwa")))

(define-public crate-capture-it-0.1.1 (c (n "capture-it") (v "0.1.1") (h "1b7ax9sbjivvz6c67h1zqc1vv00a9895nq4rz6lj935ycbi4xb81")))

(define-public crate-capture-it-0.2.0 (c (n "capture-it") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "10cv9md76irzxfs3v122iv41wjjp702179d86niczyzil0cjgh6b")))

(define-public crate-capture-it-0.3.0 (c (n "capture-it") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0xiyvm0v8a8h7az9mrnvkvwjxvi2awm6599l7q3kyj4vflsww0kf")))

(define-public crate-capture-it-0.3.1 (c (n "capture-it") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0gv4kr4vdq90qi7ac7p44p50wdjsfi1f9ny2bcz6q9xvidf87r8v")))

(define-public crate-capture-it-0.4.0 (c (n "capture-it") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "13lpw33z8rsd53lng4azky67q17jjiqdpk4b11hvmsl1nhp68n55") (f (quote (("no-std"))))))

(define-public crate-capture-it-0.4.1 (c (n "capture-it") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1piyqqgj2zrn9agf4rhdz3mf18c4ci359p88i9chnkah9i1smk5i") (f (quote (("no-std"))))))

(define-public crate-capture-it-0.4.2 (c (n "capture-it") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0snqgzavvqh6qsyqp972vbvv18qmxyjvcgnn32h9sfbaybb88hkk") (f (quote (("no-std"))))))

(define-public crate-capture-it-0.4.3 (c (n "capture-it") (v "0.4.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1ckai3ncicmgfjmkv3a9hm4v5xq0jcllzvlwniapl8k9zn0pjnvl") (f (quote (("no-std"))))))

