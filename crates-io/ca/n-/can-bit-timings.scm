(define-module (crates-io ca n- can-bit-timings) #:use-module (crates-io))

(define-public crate-can-bit-timings-1.0.0 (c (n "can-bit-timings") (v "1.0.0") (d (list (d (n "assert_hex") (r "^0.2.2") (d #t) (k 2)) (d (n "can-bit-timings-core") (r "^1.0.0") (d #t) (k 0)) (d (n "can-bit-timings-proc-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1whm8frv8wxm9w80ajq20l4qsimr7xqws5j55ri6800l6gfabd2w")))

(define-public crate-can-bit-timings-1.0.1 (c (n "can-bit-timings") (v "1.0.1") (d (list (d (n "assert_hex") (r "^0.2.2") (d #t) (k 2)) (d (n "can-bit-timings-core") (r "^1.0.0") (d #t) (k 0)) (d (n "can-bit-timings-proc-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0yacvzl4pgjkw3z0sskjr6ji3h67fi3h2c6ssac7dngbdqp7jfv5")))

(define-public crate-can-bit-timings-1.1.0 (c (n "can-bit-timings") (v "1.1.0") (d (list (d (n "assert_hex") (r "^0.2.2") (d #t) (k 2)) (d (n "can-bit-timings-core") (r "^1.1.0") (d #t) (k 0)) (d (n "can-bit-timings-proc-macro") (r "^1.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0b1rws0aqhaf2ck0q95prb9wg3q3gcd9j610f17wvj08wviiq6rf")))

