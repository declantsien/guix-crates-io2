(define-module (crates-io ca n- can-utils) #:use-module (crates-io))

(define-public crate-can-utils-0.1.0 (c (n "can-utils") (v "0.1.0") (h "017f1slkj29x25zn3vckvig57sigmlxxmwz1ckl429qavyk5492z")))

(define-public crate-can-utils-0.1.1 (c (n "can-utils") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1hcj72kzvg99dz1yvl41ibcnxfc7zbp0j30j7ic5yl97ckc5lyh5")))

(define-public crate-can-utils-0.1.2 (c (n "can-utils") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1af1c07r1l9iss0x414jpammfcvj8hqa9hz30893kyaaqr9yzlff")))

(define-public crate-can-utils-0.1.3 (c (n "can-utils") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "16lhqjzm0ng2hlxc7d6405fbqiyyw0p2jr5y1znzs0gaas02sj3g")))

(define-public crate-can-utils-0.1.4 (c (n "can-utils") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "0s4wxs2ffpnk9vm0kmvykwr8y3y5bhbdm43wklzz5ijr0zvbgvqw")))

