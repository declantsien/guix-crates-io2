(define-module (crates-io ca n- can-bit-timings-proc-macro) #:use-module (crates-io))

(define-public crate-can-bit-timings-proc-macro-1.0.0 (c (n "can-bit-timings-proc-macro") (v "1.0.0") (d (list (d (n "can-bit-timings-core") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03jm35w8nihp8y6xh8f3mp57fw83mhmrs9bzs25xqibh06d9khk1")))

(define-public crate-can-bit-timings-proc-macro-1.0.1 (c (n "can-bit-timings-proc-macro") (v "1.0.1") (d (list (d (n "can-bit-timings-core") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06frhq7fmx6wijygcdf4jj2iz5xqz946qn2jlaq0m4lh8sfwc26j")))

(define-public crate-can-bit-timings-proc-macro-1.1.0 (c (n "can-bit-timings-proc-macro") (v "1.1.0") (d (list (d (n "can-bit-timings-core") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13qnqyw2ld4hybvxj4kadqn7389a5x7bagx671rvmgp5371cykyx")))

