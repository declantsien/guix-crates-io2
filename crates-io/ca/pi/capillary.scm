(define-module (crates-io ca pi capillary) #:use-module (crates-io))

(define-public crate-capillary-0.1.0 (c (n "capillary") (v "0.1.0") (h "0r2fkx04fynyzs4hixp2xh5lbgkbqbzifay52x15ag2pqgp6jhh3")))

(define-public crate-capillary-0.2.0 (c (n "capillary") (v "0.2.0") (h "0g031dn7vxzi75rg5hk8ncnr21g4i3k7vv8ms3grrmzhfqrfkif3")))

(define-public crate-capillary-0.3.0 (c (n "capillary") (v "0.3.0") (h "0k9nlqjz8x20qjl40v9b4z4pd82i59s6c1w9v6l6d6lffv7qbrl6")))

(define-public crate-capillary-0.4.0 (c (n "capillary") (v "0.4.0") (d (list (d (n "petgraph") (r "^0.6.2") (f (quote ("stable_graph"))) (k 0)))) (h "1f8lqv79v4754qp268vb8f0ffwj38jmh2xvkk38fpmh91c34vcd8")))

