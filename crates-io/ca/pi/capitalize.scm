(define-module (crates-io ca pi capitalize) #:use-module (crates-io))

(define-public crate-capitalize-0.1.0 (c (n "capitalize") (v "0.1.0") (h "1j0i9vd4f3zv4jl1bwy4ihb5qcfxcgpyy94rbbal4r9c0lpzwb3h")))

(define-public crate-capitalize-0.2.0 (c (n "capitalize") (v "0.2.0") (h "1d404fbmdhrbx870xnivq0zfz93m6bx1z01d30f169rgw3ni92md")))

(define-public crate-capitalize-0.3.0 (c (n "capitalize") (v "0.3.0") (d (list (d (n "data-test") (r "^0.1") (d #t) (k 2)))) (h "0a758pvh9swx550fvighrl8h03y78ac4vxywwcv3547mm88hci0h") (f (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3.1 (c (n "capitalize") (v "0.3.1") (d (list (d (n "data-test") (r "^0.1") (d #t) (k 2)))) (h "0y4rhfwgvkscgvh7xczvr74xvmg1nrfa5k6bry4l13brgmkgdhmi") (f (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3.2 (c (n "capitalize") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "data-test") (r "^0.1") (d #t) (k 2)))) (h "0bgqfbagw14jakh9cvn5d8czxgpqsjpa68z8fas48ypbvzqnn589") (f (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3.3 (c (n "capitalize") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "data-test") (r "^0.1") (d #t) (k 2)))) (h "19xiap5yrg93pkwj9ifgjyyb5cylnqc8ia0xr4v7paalkm512m6d") (f (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3.4 (c (n "capitalize") (v "0.3.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "data-test") (r "^0.1") (d #t) (k 2)))) (h "1gxmp8s740i1g6pqfmq9cbcv6v5x0dsfcbsqqzl5x0r2201p2lkb") (f (quote (("nightly") ("default"))))))

