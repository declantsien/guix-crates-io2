(define-module (crates-io ca pi capi) #:use-module (crates-io))

(define-public crate-capi-0.0.1 (c (n "capi") (v "0.0.1") (h "1h4j446v2r34zrz6zji1dr6088sk2qi18bfp5spfxlvmwasd2zhz") (y #t)))

(define-public crate-capi-0.0.2 (c (n "capi") (v "0.0.2") (h "0jqqrc0v5mb8myxf5dib8bi1l1zwc653i7q812acxcp9rhn2mqwm") (y #t)))

(define-public crate-capi-0.0.3 (c (n "capi") (v "0.0.3") (h "06y5hnvxrm16whrd2y7hwr5184abvv6zdygk1nwfq8k5q11a4d9b") (y #t)))

