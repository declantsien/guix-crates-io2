(define-module (crates-io tp om tpom) #:use-module (crates-io))

(define-public crate-tpom-0.1.0 (c (n "tpom") (v "0.1.0") (d (list (d (n "goblin") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "06y1q2937widds64mfngvbhz308pbc5bkz9d2qcsw84pxrkvqack")))

