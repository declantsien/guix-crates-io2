(define-module (crates-io tp -f tp-finality-grandpa) #:use-module (crates-io))

(define-public crate-tp-finality-grandpa-0.0.0 (c (n "tp-finality-grandpa") (v "0.0.0") (h "1bp0ly5743fb9868196spk4q6qvrags7rivxn91lig3mlxydm5r1")))

(define-public crate-tp-finality-grandpa-2.0.2 (c (n "tp-finality-grandpa") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "grandpa") (r "^0.13.0") (f (quote ("derive-codec"))) (k 0) (p "tetsy-finality-grandpa")) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tet-application-crypto") (r "^2.0.2") (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-keystore") (r "^0.8.1") (o #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "0jmk4047syi2h5r0538nkqqxbgv9k5ni59gv73calrzjx3gsvnj9") (f (quote (("std" "log" "serde" "codec/std" "grandpa/std" "tp-api/std" "tet-application-crypto/std" "tet-core/std" "tp-keystore" "tp-runtime/std" "tetcore-std/std") ("default" "std"))))))

