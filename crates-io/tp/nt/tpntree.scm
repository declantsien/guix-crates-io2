(define-module (crates-io tp nt tpntree) #:use-module (crates-io))

(define-public crate-tpntree-0.1.0 (c (n "tpntree") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "0ybgppn12lh9l6mwn4qqs099nh9hrqkq05hqj3fjglavbbdlq17v") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.2.0 (c (n "tpntree") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "0290cisbgxrqsr5xgfxivz0svzcgfkywnj6khk8nl66j3qzd6jfc") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.3.0 (c (n "tpntree") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1cqhgswqhczwwq0kzg5l98v5phaxy0fpim9gih87mszkxgckgzib") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.4.0 (c (n "tpntree") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "130if36hcmzaazcshhr602lw7y66n34ilnxbp4zlkagf1678jdcj") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5.0 (c (n "tpntree") (v "0.5.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "01p57h397kylnfgy53fz4vh2mc317b8afk843ds1v115im12mygx") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5.1 (c (n "tpntree") (v "0.5.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1pj4j99c8lz7jniz0njypyaz4y2dz603jlf9wr5jm2dcb7i4lsns") (f (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5.2 (c (n "tpntree") (v "0.5.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1aks6lfhx7gjgjchdrkbsimmrx49zjdyyq6q2dmhyy3m3ll2zbrk") (f (quote (("default" "nalgebra"))))))

