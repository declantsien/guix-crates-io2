(define-module (crates-io tp -n tp-npos-elections-compact) #:use-module (crates-io))

(define-public crate-tp-npos-elections-compact-2.0.1 (c (n "tp-npos-elections-compact") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0yadwa1bvwi163zr9w2pr0iy0mlrbnqlkfaadkkli13klv99phws")))

(define-public crate-tp-npos-elections-compact-2.0.2 (c (n "tp-npos-elections-compact") (v "2.0.2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1qiy554iidfpnn31ny99rzpz8zmjfsgb8inil4d0akjh3884r866")))

