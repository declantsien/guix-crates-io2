(define-module (crates-io tp s6 tps6507x) #:use-module (crates-io))

(define-public crate-tps6507x-0.1.0-alpha.0 (c (n "tps6507x") (v "0.1.0-alpha.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "00drdw1sdwmxarfvpb2lw2ahmr988nms2hw6wnq2b9m35rpyz2fg") (y #t)))

(define-public crate-tps6507x-0.1.0-alpha.1 (c (n "tps6507x") (v "0.1.0-alpha.1") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0kfvfv38w2k6x9hgfl3vdyyn5c5sqz06za2nv9vylbphl93a3cdz") (y #t)))

(define-public crate-tps6507x-0.1.0 (c (n "tps6507x") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1bassda28aw0h4wz1xkpzsjfayd6py8ssnn1sdrjwzmpjrlalapi")))

