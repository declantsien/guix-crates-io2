(define-module (crates-io tp a2 tpa2016d2) #:use-module (crates-io))

(define-public crate-tpa2016d2-0.1.0 (c (n "tpa2016d2") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "02pzc0hqb0n5rhifl04yasgvg5z7yyvzmysk0zljw17azz03yq43")))

(define-public crate-tpa2016d2-0.1.1 (c (n "tpa2016d2") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "1c1ip9f73ddcxwf5xfhag6z5ahqzzdarjyjndh4kbcwj03h3zzfj")))

(define-public crate-tpa2016d2-0.2.0 (c (n "tpa2016d2") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "08dqhbkwl1v8yd6ynmxxkz7jaq650h34f1dc8ln2l6x4lw4bpwj1")))

