(define-module (crates-io tp -o tp-offchain) #:use-module (crates-io))

(define-public crate-tp-offchain-2.0.1 (c (n "tp-offchain") (v "2.0.1") (d (list (d (n "tet-core") (r "^2.0.1") (k 0)) (d (n "tp-api") (r "^2.0.1") (k 0)) (d (n "tp-runtime") (r "^2.0.1") (k 0)) (d (n "tp-state-machine") (r "^0.8.1") (k 2)))) (h "13rvpkrsalycwr40zjr8b50k8bzpk7zj570nk2ygavlnpf3ph4w8") (f (quote (("std" "tet-core/std" "tp-api/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-offchain-2.0.2 (c (n "tp-offchain") (v "2.0.2") (d (list (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)) (d (n "tp-state-machine") (r "^0.8.2") (k 2)))) (h "0wmgmasvw5qfww6ahffvhryhjimsa9jx454y21y2h6nk20qns948") (f (quote (("std" "tet-core/std" "tp-api/std" "tp-runtime/std") ("default" "std"))))))

