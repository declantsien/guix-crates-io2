(define-module (crates-io tp m2 tpm2-policy) #:use-module (crates-io))

(define-public crate-tpm2-policy-0.1.0 (c (n "tpm2-policy") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tss-esapi") (r "^4.0.5-alpha.1") (d #t) (k 0)))) (h "0i2z6pac6ksnf74l28b9gjddkf21ijcisrins2icpy7hjb72sla6")))

(define-public crate-tpm2-policy-0.2.0 (c (n "tpm2-policy") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tss-esapi") (r "^4.0.6-alpha.1") (d #t) (k 0)))) (h "008z8wx4yagzz7cp6fchvczsqw1b75icdy9n05pvl7q0223g1dxz")))

(define-public crate-tpm2-policy-0.3.0 (c (n "tpm2-policy") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^4.0.10-alpha.1") (d #t) (k 0)))) (h "1k800v0saxzbzjqhrkbnkqrjlkjm9cc9klqhlkw7xgrbrlgrzk8w")))

(define-public crate-tpm2-policy-0.3.1 (c (n "tpm2-policy") (v "0.3.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^4.0.10-alpha.1") (d #t) (k 0)))) (h "0f03da27rgdp5in3yzmql2ya7x3zva9sbyw3l40fbfcn1hyxzn5x")))

(define-public crate-tpm2-policy-0.4.0 (c (n "tpm2-policy") (v "0.4.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^5.0") (d #t) (k 0)))) (h "0wwrbd9rkczigmbi9kd77f7aq701yp7zpp21gfad2ynxib9y3wxs")))

(define-public crate-tpm2-policy-0.5.1 (c (n "tpm2-policy") (v "0.5.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^6.1.1") (d #t) (k 0)))) (h "085885j1lhrs733xviq74488qk1yb79457xkfdv3v2casc3w6ac1")))

(define-public crate-tpm2-policy-0.5.2 (c (n "tpm2-policy") (v "0.5.2") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^6.1.1") (d #t) (k 0)))) (h "1y805c7zhddsbrslyjrq5xj34hiarakm9y7jgh6ppapsk3qzibp3")))

(define-public crate-tpm2-policy-0.5.3 (c (n "tpm2-policy") (v "0.5.3") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^6.1.1") (d #t) (k 0)))) (h "04qz87m9fibjcpqszmvjx2vvk0vi0yqkrxxr012dk7gprf3lpvkr")))

(define-public crate-tpm2-policy-0.6.0 (c (n "tpm2-policy") (v "0.6.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^7.0.0") (d #t) (k 0)))) (h "0ais0fg1spn0iaq0lfx8zs46w4rixcs16jnjwy3isjzxhh017g87")))

