(define-module (crates-io tp m2 tpm2) #:use-module (crates-io))

(define-public crate-tpm2-0.0.1 (c (n "tpm2") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1jf1r34hn5h34k96vgm234ylgj1qgbsw9acgksa41fs8h9n0w3b9") (f (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.2 (c (n "tpm2") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0jm7fp4q0aw7qi1fgqrwnp5b64x208s4ggv02d1d8kilk4zjvvjg") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.3 (c (n "tpm2") (v "0.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0fh2w5hndfs3bkx1z2m3yyjbxf04p3j58zllkm97fwlqa3mcmcgy") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.4 (c (n "tpm2") (v "0.0.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0f9vyx29lkp59vrbv2asir40pc38q8r7bp75g8nhxj37sjm7i2rv") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

