(define-module (crates-io tp m2 tpm2_ek_cert_gen) #:use-module (crates-io))

(define-public crate-tpm2_ek_cert_gen-0.1.0 (c (n "tpm2_ek_cert_gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "tss-esapi") (r "^4.0.10-alpha.2") (d #t) (k 0)))) (h "00m4rs735vphiz8s0w2595cjh9m09kabnj2wqzca5vsr5hkfnmn2")))

(define-public crate-tpm2_ek_cert_gen-0.1.1 (c (n "tpm2_ek_cert_gen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "tss-esapi") (r "^4.0.10-alpha.2") (d #t) (k 0)))) (h "0r4s0wnibzwz5m05cm6v1kyq666nznygmm99sv476c59awr3rl5w")))

