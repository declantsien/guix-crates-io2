(define-module (crates-io tp kt tpkt) #:use-module (crates-io))

(define-public crate-tpkt-0.1.0 (c (n "tpkt") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "0gqh05h0kiypghwh0xh6slxnrkr2f015fji03vs7s4wmn4qnx6ff")))

