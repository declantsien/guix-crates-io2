(define-module (crates-io tp -c tp-consensus-pow) #:use-module (crates-io))

(define-public crate-tp-consensus-pow-0.8.1 (c (n "tp-consensus-pow") (v "0.8.1") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.0.1") (k 0)) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tp-api") (r "^2.0.1") (k 0)) (d (n "tp-runtime") (r "^2.0.1") (k 0)))) (h "0vhrfny7i4irivjxl1wc56whl6177zhwcmdw4505s916kpri5xkk") (f (quote (("std" "tetcore-std/std" "tp-api/std" "tp-runtime/std" "tet-core/std" "codec/std") ("default" "std"))))))

(define-public crate-tp-consensus-pow-0.8.2 (c (n "tp-consensus-pow") (v "0.8.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1dnbdfr5r6f8387lklpv9khxn8m5cl1i4jyyghwssbvhc5wj3m5k") (f (quote (("std" "tetcore-std/std" "tp-api/std" "tp-runtime/std" "tet-core/std" "codec/std") ("default" "std"))))))

(define-public crate-tp-consensus-pow-2.1.2 (c (n "tp-consensus-pow") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.1.2") (k 0)) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tp-api") (r "^2.1.2") (k 0)) (d (n "tp-runtime") (r "^2.1.2") (k 0)))) (h "0dz852jvag4vzk8d7yzydxps4j0mqd5p4f8zswiigf67k4s59h2w") (f (quote (("std" "tetcore-std/std" "tp-api/std" "tp-runtime/std" "tet-core/std" "codec/std") ("default" "std"))))))

