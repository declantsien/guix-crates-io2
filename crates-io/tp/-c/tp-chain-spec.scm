(define-module (crates-io tp -c tp-chain-spec) #:use-module (crates-io))

(define-public crate-tp-chain-spec-2.0.1 (c (n "tp-chain-spec") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "01k2cjd5zn66d26d4l34j16y8kl87yrlyw8rd3fpmlcd21a2gfch")))

(define-public crate-tp-chain-spec-2.0.2 (c (n "tp-chain-spec") (v "2.0.2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0d8yz7nlbwfx42pycyq2w1kzcrq71ak0gp17j9y4s6hqix3rwk9i")))

(define-public crate-tp-chain-spec-2.1.2 (c (n "tp-chain-spec") (v "2.1.2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1iwp49aiw39wivd2jnpppkqychl71cz9dlz2bbpl5c4kcdn5npc8")))

