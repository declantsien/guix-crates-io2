(define-module (crates-io tp -c tp-cli) #:use-module (crates-io))

(define-public crate-tp-cli-0.1.0 (c (n "tp-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j4dv6bwpzrb6d8nlcw0ald8plsjgyjxz339id5v98756gjqay1m")))

