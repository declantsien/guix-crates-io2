(define-module (crates-io tp -a tp-allocator) #:use-module (crates-io))

(define-public crate-tp-allocator-0.1.0 (c (n "tp-allocator") (v "0.1.0") (h "1xmh8g4darch3p0sijxf9pxgr8cq6cvjn8wkz2x9kn96z3al954f")))

(define-public crate-tp-allocator-2.0.1 (c (n "tp-allocator") (v "2.0.1") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.1") (k 0)) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.1") (k 0)) (d (n "thiserror") (r "^1.0.21") (o #t) (d #t) (k 0)))) (h "0gd9j6dsynpdmz9casqdkyi9q07z9scshd3fydh95698h22yzs7l") (f (quote (("std" "tetcore-std/std" "tet-core/std" "tetcore-wasm-interface/std" "log" "thiserror") ("default" "std"))))))

(define-public crate-tp-allocator-2.0.2 (c (n "tp-allocator") (v "2.0.2") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.2") (k 0)) (d (n "thiserror") (r "^1.0.21") (o #t) (d #t) (k 0)))) (h "0gvwz69d3r9lg1a2akr3jz3xl5krhjn88skj6spz97pxacs5k1p7") (f (quote (("std" "tetcore-std/std" "tet-core/std" "tetcore-wasm-interface/std" "log" "thiserror") ("default" "std"))))))

(define-public crate-tp-allocator-2.1.2 (c (n "tp-allocator") (v "2.1.2") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.1.2") (k 0)) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tetcore-wasm-interface") (r "^2.1.2") (k 0)) (d (n "thiserror") (r "^1.0.21") (o #t) (d #t) (k 0)))) (h "0l9755l8wzs6r7pjxizaxa9a3rsjwi1yv3pg1k56m9cwm5m6f2b2") (f (quote (("std" "tetcore-std/std" "tet-core/std" "tetcore-wasm-interface/std" "log" "thiserror") ("default" "std"))))))

