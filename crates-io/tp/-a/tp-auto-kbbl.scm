(define-module (crates-io tp -a tp-auto-kbbl) #:use-module (crates-io))

(define-public crate-tp-auto-kbbl-0.1.4 (c (n "tp-auto-kbbl") (v "0.1.4") (d (list (d (n "dbus") (r "^0.8.1") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.13.1") (d #t) (k 0)))) (h "02kga57xcq8g7qbabgwzh5gp8dvv9qlwzawv5m08jf34vmps9qf6")))

(define-public crate-tp-auto-kbbl-0.1.5 (c (n "tp-auto-kbbl") (v "0.1.5") (d (list (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1a5q9jf0v3l8w07wq6l2iq7wlw004zwhwv7bbwkrh5vymkrcf7lj")))

