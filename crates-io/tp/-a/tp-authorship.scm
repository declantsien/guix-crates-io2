(define-module (crates-io tp -a tp-authorship) #:use-module (crates-io))

(define-public crate-tp-authorship-0.0.0 (c (n "tp-authorship") (v "0.0.0") (h "1bmqkvjmd3iidix85vd1yi54hr2857plg1qgqlmx1hca2wf75cwg") (y #t)))

(define-public crate-tp-authorship-2.0.1 (c (n "tp-authorship") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tp-inherents") (r "^2.0.1") (k 0)) (d (n "tp-runtime") (r "^2.0.1") (k 0)))) (h "1kgd04q5g2f3z03n1zr201afdz539ai3g56wr6m0z6nbv6iwra62") (f (quote (("std" "codec/std" "tetcore-std/std" "tp-inherents/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-authorship-2.0.2 (c (n "tp-authorship") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-inherents") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "07d67nggmz2lbfxx15cyimifi4hkfskami9c0gj9brygn9aybimj") (f (quote (("std" "codec/std" "tetcore-std/std" "tp-inherents/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-authorship-2.1.2 (c (n "tp-authorship") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tp-inherents") (r "^2.1.2") (k 0)) (d (n "tp-runtime") (r "^2.1.2") (k 0)))) (h "0mipgfixiapmisrlzz9zmw5d8vywjnpzfv4fha85j0lwfx83ahwi") (f (quote (("std" "codec/std" "tetcore-std/std" "tp-inherents/std" "tp-runtime/std") ("default" "std"))))))

