(define-module (crates-io tp -a tp-api-proc-macro) #:use-module (crates-io))

(define-public crate-tp-api-proc-macro-2.0.1 (c (n "tp-api-proc-macro") (v "2.0.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1ynvyjk6ygqmv0kfv75s5ghpjr3krkzs1fqan7qkwn1x06c1q4yc") (f (quote (("std") ("default" "std"))))))

(define-public crate-tp-api-proc-macro-2.0.2 (c (n "tp-api-proc-macro") (v "2.0.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1xzchzf5dj00j33p6f3j87fhvz4x9b2b6372yvskgmdiiw6r8grj") (f (quote (("std") ("default" "std"))))))

(define-public crate-tp-api-proc-macro-2.1.2 (c (n "tp-api-proc-macro") (v "2.1.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0c1y47f9nmdw3ggqi9zdniy3isddpa3sqmb56hq6i557wi0srg6f") (f (quote (("std") ("default" "std"))))))

