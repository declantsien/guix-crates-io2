(define-module (crates-io tp -r tp-rpc) #:use-module (crates-io))

(define-public crate-tp-rpc-2.0.1 (c (n "tp-rpc") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "tet-core") (r "^2.0.1") (d #t) (k 0)))) (h "07wpz1ykcr1k426bp60mdcli0g8219vnvcz3hhj92j4amyqdp0k3")))

(define-public crate-tp-rpc-2.0.2 (c (n "tp-rpc") (v "2.0.2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)))) (h "0x4dmgbbgzwk2bjdajhkpzcagx7xqspl70dfhsav19snyv0pg1ji")))

