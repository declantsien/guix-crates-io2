(define-module (crates-io tp -v tp-version) #:use-module (crates-io))

(define-public crate-tp-version-0.0.0 (c (n "tp-version") (v "0.0.0") (h "01k0s455mds19a3zi966x0vvpn1linsn6kqdcwa0vbnfv2haqny7") (y #t)))

(define-public crate-tp-version-2.0.1 (c (n "tp-version") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "impl-serde") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tetcore-std") (r "^2.0.0") (k 0)) (d (n "tp-runtime") (r "^2.0.0") (k 0)))) (h "0vv7vcz3x15xlr3xj2mwkyhsr1lcas19hzbgxkd6swf97qpdp85g") (f (quote (("std" "impl-serde" "serde" "codec/std" "tetcore-std/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-version-2.0.2 (c (n "tp-version") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "impl-serde") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1jxsd4jp8wmbq62yhismmnf2y8ksna482wgicgm62dz79s0dihgl") (f (quote (("std" "impl-serde" "serde" "codec/std" "tetcore-std/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-version-2.1.2 (c (n "tp-version") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "impl-serde") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tp-runtime") (r "^2.1.2") (k 0)))) (h "0yvh1510zxmh4vl5dbgdb1pcifm0h532ydxv5ikd4ivlraf7d7sb") (f (quote (("std" "impl-serde" "serde" "codec/std" "tetcore-std/std" "tp-runtime/std") ("default" "std"))))))

