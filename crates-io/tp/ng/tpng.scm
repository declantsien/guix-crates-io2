(define-module (crates-io tp ng tpng) #:use-module (crates-io))

(define-public crate-tpng-0.1.0 (c (n "tpng") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1r5v9s52a2ip283jylkxpwzridwjdgd08z6hpg4hsfk26a9b5mj5")))

(define-public crate-tpng-0.1.1 (c (n "tpng") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1ssliaw2bpfmmmzv02qbdlirgkfq7zx1dc8712hdc25b737605x7")))

(define-public crate-tpng-0.1.2 (c (n "tpng") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1jh7adv2mvkqs1dyb9p3hb938kgxszbwg16kahhaws0jhf66v8cv")))

(define-public crate-tpng-0.1.3 (c (n "tpng") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1mmkvv7zdafcvcr1sagh05w1i56pnys4k13h51nwjs2pxk3dz5hi")))

(define-public crate-tpng-0.1.4 (c (n "tpng") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "08h58jm52vb7z0qyxm7p40wm2makkrik5jcq1f3k129xz5cgwl6r")))

(define-public crate-tpng-0.1.5 (c (n "tpng") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "07wpnsljaf5cghfhjhsf2g8gv7xn32gdkh8x1blgja2qvyh4194i")))

(define-public crate-tpng-0.1.6 (c (n "tpng") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "0h879gzikn1qa4b29fldiixb004ygvpiing3ns0p50cw2mv9bzld") (r "1.48")))

