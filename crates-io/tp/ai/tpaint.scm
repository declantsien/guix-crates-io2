(define-module (crates-io tp ai tpaint) #:use-module (crates-io))

(define-public crate-tpaint-0.1.0 (c (n "tpaint") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0d3cc6147pc5965piq6cp7amlj9s7ii2bia2mblfv7sw3fyd04xx")))

(define-public crate-tpaint-0.1.1 (c (n "tpaint") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "06k9zmmk777i3zvap5szyz0syblckz8ji8qnhlz4m4zpn4q7pnh7")))

(define-public crate-tpaint-0.2.0 (c (n "tpaint") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1dx4h3szhfz8nnvzz3s6bwgp7srl6ps5k0cvznbyr3xih1314wvy")))

(define-public crate-tpaint-0.2.1 (c (n "tpaint") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0hqyhdjx2yjis83v9daps60a2y8p98alrqqqcs1kiqvbfdf56q2m")))

(define-public crate-tpaint-0.2.2 (c (n "tpaint") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1yb03w93kzkgzlfqc0f4z0b9kqp6nyjq6w377272qa048ji1h7hf")))

(define-public crate-tpaint-0.2.3 (c (n "tpaint") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "06frf2w778cs90mxl6ij1cgf0sgn4qi1sfgfjl8ckb3jjr5qa7kx")))

(define-public crate-tpaint-0.2.4 (c (n "tpaint") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "020pdf0x7hmhz1pn4ixmgjax92mm8ncpqinjy45lr22p22jmzlqw")))

(define-public crate-tpaint-0.2.5 (c (n "tpaint") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0hbmb8qyck187m2pvm160sc0026rdah51bcdlxkldwqs75xq973n")))

(define-public crate-tpaint-0.2.6 (c (n "tpaint") (v "0.2.6") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "imgref") (r "^1.8.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1wl1n7a3pydw0y9zakkpm9a294yjbhrr7g2qxzrbzpyl41r0k2yz")))

(define-public crate-tpaint-0.2.8 (c (n "tpaint") (v "0.2.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0g05zpb492bhqi25vvrvf25pbv8diivvqrfvmmn4c1c7pdbgw5m5")))

(define-public crate-tpaint-0.2.9 (c (n "tpaint") (v "0.2.9") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00hip700x3wskmnk244s5cn0wgg8n2ypfbiqr787d46w6ix20y0j")))

(define-public crate-tpaint-0.2.10 (c (n "tpaint") (v "0.2.10") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1gm4cy6h76p2qppm0s17lgb8r7aqd7yz28v50x91xmcpam6sq471")))

(define-public crate-tpaint-0.2.11 (c (n "tpaint") (v "0.2.11") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0cffrfghrmxqqasj1rnaz86lraqq4yqzcpjw3lg4qhh0znd545vj")))

(define-public crate-tpaint-0.2.12 (c (n "tpaint") (v "0.2.12") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1422gnq9x2dbsk78spi7jn2h5bm9j9a8k9rjpyckrphw5ksx7r8a")))

