(define-module (crates-io tp -k tp-keyring) #:use-module (crates-io))

(define-public crate-tp-keyring-2.0.1 (c (n "tp-keyring") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.1") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.1") (d #t) (k 0)))) (h "1hgqkwr5b0njdcsm3akwx16xxk4pb8zlhfp1ngm91bivid6j8w1i")))

(define-public crate-tp-keyring-2.0.2 (c (n "tp-keyring") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "1mm2yp12l76dh6p18dy0mvyvabz8zbn67fwvsz4dy0xw7227m7mf")))

