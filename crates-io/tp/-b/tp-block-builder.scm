(define-module (crates-io tp -b tp-block-builder) #:use-module (crates-io))

(define-public crate-tp-block-builder-0.0.0 (c (n "tp-block-builder") (v "0.0.0") (h "0hsqkgwhsi0rrnfy6cqzi80xns4zxykh7rdsl5mgqiz3dvrkqbsk") (y #t)))

(define-public crate-tp-block-builder-2.0.1 (c (n "tp-block-builder") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tp-api") (r "^2.0.1") (k 0)) (d (n "tp-inherents") (r "^2.0.1") (k 0)) (d (n "tp-runtime") (r "^2.0.1") (k 0)))) (h "16rw05myv8ras3zpc7qljggclr4f2dqjq7cji0zxz0iw3r1bgz16") (f (quote (("std" "tp-runtime/std" "codec/std" "tp-inherents/std" "tp-api/std" "tetcore-std/std") ("default" "std"))))))

(define-public crate-tp-block-builder-2.0.2 (c (n "tp-block-builder") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-inherents") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "1yi7q27080g6czq5d015374sywmfiskd09rjdmmz81n6hbfhawdx") (f (quote (("std" "tp-runtime/std" "codec/std" "tp-inherents/std" "tp-api/std" "tetcore-std/std") ("default" "std"))))))

(define-public crate-tp-block-builder-2.1.2 (c (n "tp-block-builder") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.1.2") (k 0)) (d (n "tp-api") (r "^2.1.2") (k 0)) (d (n "tp-inherents") (r "^2.1.2") (k 0)) (d (n "tp-runtime") (r "^2.1.2") (k 0)))) (h "0kd6bsyj7kgi2cp8ha06qg212vgjvxcqav85cklxz6jsn1lkcwf6") (f (quote (("std" "tp-runtime/std" "codec/std" "tp-inherents/std" "tp-api/std" "tetcore-std/std") ("default" "std"))))))

