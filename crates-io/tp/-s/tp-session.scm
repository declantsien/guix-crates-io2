(define-module (crates-io tp -s tp-session) #:use-module (crates-io))

(define-public crate-tp-session-2.0.2 (c (n "tp-session") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "tp-staking") (r "^2.0.2") (k 0)))) (h "1x834im53d49fq8kwkhj4f91bfsb279x4kpwk5g3qzr2d162rc53") (f (quote (("std" "codec/std" "tp-api/std" "tet-core/std" "tetcore-std/std" "tp-staking/std" "tp-runtime/std") ("default" "std"))))))

