(define-module (crates-io tp -s tp-sandbox) #:use-module (crates-io))

(define-public crate-tp-sandbox-0.0.0 (c (n "tp-sandbox") (v "0.0.0") (h "1ndy75kx66zk09j1miyw5w2lmby6d18m3nh66b4yg5avp51mmg12") (y #t)))

(define-public crate-tp-sandbox-0.8.1 (c (n "tp-sandbox") (v "0.8.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.0.1") (k 0)) (d (n "tet-io") (r "^2.0.1") (k 0)) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.1") (k 0)) (d (n "twasmi") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "18972mhfizvvc4f18aab16iqzbg8hfhwaz4d7lc78397d9dhx7gx") (f (quote (("strict") ("std" "twasmi" "tet-core/std" "tetcore-std/std" "codec/std" "tet-io/std" "tetcore-wasm-interface/std") ("default" "std"))))))

(define-public crate-tp-sandbox-0.8.2 (c (n "tp-sandbox") (v "0.8.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tetcore-wasm-interface") (r "^2.0.2") (k 0)) (d (n "twasmi") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1148sbxg12iqaqspjn483cgxplaxiclwcmrf4l600izncsixvw78") (f (quote (("strict") ("std" "twasmi" "tet-core/std" "tetcore-std/std" "codec/std" "tet-io/std" "tetcore-wasm-interface/std") ("default" "std"))))))

