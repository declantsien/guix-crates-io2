(define-module (crates-io tp -s tp-staking) #:use-module (crates-io))

(define-public crate-tp-staking-2.0.1 (c (n "tp-staking") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.1") (k 0)) (d (n "tp-runtime") (r "^2.0.1") (k 0)))) (h "0sl09nc65bhjjdwkslyn0510y5k1mn21g2gppswmgylyx9hi3cxg") (f (quote (("std" "codec/std" "tp-runtime/std" "tetcore-std/std") ("default" "std"))))))

(define-public crate-tp-staking-2.0.2 (c (n "tp-staking") (v "2.0.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)))) (h "0bzqiz02irfwzprckfhja8v8hz8ccf4c7dhm4bph8l4baxkq6kd5") (f (quote (("std" "codec/std" "tp-runtime/std" "tetcore-std/std") ("default" "std"))))))

