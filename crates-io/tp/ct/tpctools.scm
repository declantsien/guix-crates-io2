(define-module (crates-io tp ct tpctools) #:use-module (crates-io))

(define-public crate-tpctools-0.1.0 (c (n "tpctools") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (k 0)))) (h "1ym9f6xy5d1qjprvfm3fhaa805g9rvymhk036x4y0cfc8hw5r387")))

(define-public crate-tpctools-0.1.1 (c (n "tpctools") (v "0.1.1") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^4.0.0") (d #t) (k 0)) (d (n "parquet") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0cmq1r1di5m8caj7nfzpk0nqg856zk93mz4wcxjkr6l03yby6369")))

(define-public crate-tpctools-0.1.2 (c (n "tpctools") (v "0.1.2") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^4.0.0") (d #t) (k 0)) (d (n "parquet") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1qhc5gl01c0hjqdk99xqmz8pdy90qf3ka603aiafii9jw74hz8fl")))

(define-public crate-tpctools-0.1.3 (c (n "tpctools") (v "0.1.3") (d (list (d (n "arrow") (r "^6.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^6.0.0") (d #t) (k 0)) (d (n "parquet") (r "^6.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0x6z0ddzjl74pirh9vxnx03whfh6515pb8viz7b5q1p7l449c384")))

(define-public crate-tpctools-0.1.4 (c (n "tpctools") (v "0.1.4") (d (list (d (n "arrow") (r "^6.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^6.0.0") (d #t) (k 0)) (d (n "parquet") (r "^6.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1hfm828gqyh1r7dmpjkafc90msggdcmh17flhmkqqdkfwlyfds7d")))

(define-public crate-tpctools-0.1.5 (c (n "tpctools") (v "0.1.5") (d (list (d (n "arrow") (r "^6.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^6.0.0") (d #t) (k 0)) (d (n "parquet") (r "^6.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0y1qb9l42fk5r2khb82kl1yisrjvqgd370g0bw7l5m7m8hz9bbxq")))

(define-public crate-tpctools-0.2.0 (c (n "tpctools") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^15.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1wr8f5789rf93h74795zw1rv9r3bp8zx2nqyc0hisblz4n028i3i")))

(define-public crate-tpctools-0.3.0 (c (n "tpctools") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^15.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0m4w9rg14spvs3f5x1ykiawqddp88yrshzvnj9r7wqrr96spcnkx")))

(define-public crate-tpctools-0.4.0 (c (n "tpctools") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^17.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1xmay6sa79lxlglxxgxllcgf1c4js1hqvpfmiv54jnz1sz7j3a40") (y #t)))

(define-public crate-tpctools-0.5.0 (c (n "tpctools") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^23.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1s7w0qv205k8j7cw35fxidwpb9mzk9rjdzar9b4ss7530w8g4b57")))

(define-public crate-tpctools-0.6.0 (c (n "tpctools") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^23.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0jqw36l4fjfsk0lm54z4474hmxvkfba2f84c48k17dvswc40jdc5")))

(define-public crate-tpctools-0.7.0 (c (n "tpctools") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "datafusion") (r "^23.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1rclszsm9di5bahsg4cq5ny9b99i2anr787v2cakapp3825knxjq")))

