(define-module (crates-io tp fs tpfs_logger_log4rs_adapter) #:use-module (crates-io))

(define-public crate-tpfs_logger_log4rs_adapter-0.2.2 (c (n "tpfs_logger_log4rs_adapter") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (f (quote ("console_appender" "console_writer"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tpfs_logger_port") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1icy107r419nvhqsbvh3r6c5866r8s7fr458qpzgbhg0j77iiyjw")))

