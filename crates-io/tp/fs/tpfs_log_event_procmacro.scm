(define-module (crates-io tp fs tpfs_log_event_procmacro) #:use-module (crates-io))

(define-public crate-tpfs_log_event_procmacro-0.1.19-beta.35 (c (n "tpfs_log_event_procmacro") (v "0.1.19-beta.35") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0c1n38yphfx1ksaxvpmsq8avmrx6nn82lpv9l01l2qpr56bb8bbb")))

(define-public crate-tpfs_log_event_procmacro-0.1.19-beta.40 (c (n "tpfs_log_event_procmacro") (v "0.1.19-beta.40") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "170r098ggb67bmv6i9gq8j5b16y3q7dxggxcxz3xy03aimybz873")))

(define-public crate-tpfs_log_event_procmacro-0.1.19 (c (n "tpfs_log_event_procmacro") (v "0.1.19") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1731f63kb6pgz5fw3687cz8236nibkck922cfkbj8d8pmsdc1ynb")))

