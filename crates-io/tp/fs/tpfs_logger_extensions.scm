(define-module (crates-io tp fs tpfs_logger_extensions) #:use-module (crates-io))

(define-public crate-tpfs_logger_extensions-0.1.16 (c (n "tpfs_logger_extensions") (v "0.1.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.1") (d #t) (k 0)))) (h "03bs15djxyr9yh1k9p0pzlzlj31yn718di4lp3k3nvykf29l04r9")))

