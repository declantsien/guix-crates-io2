(define-module (crates-io tp fs tpfs_logger_port) #:use-module (crates-io))

(define-public crate-tpfs_logger_port-0.3.2 (c (n "tpfs_logger_port") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.1") (d #t) (k 0)) (d (n "tpfs_log_event_procmacro") (r "^0.1.19") (d #t) (k 0)))) (h "095b6bn3xs9i2nvvx7by8w98w8qdpgdzmh70xkkyx10d4137cmqm")))

