(define-module (crates-io tp li tplinker) #:use-module (crates-io))

(define-public crate-tplinker-0.1.0 (c (n "tplinker") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b6wrfrkvqyw7f1hswygw5j87m5wv00kcv9agrfl55mbqxylnqmg")))

(define-public crate-tplinker-0.2.0 (c (n "tplinker") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g7sqpvs69pfza1nszfjlz26m14fnpxvbjh9mlwzfiqrgr53lizn")))

(define-public crate-tplinker-0.3.0 (c (n "tplinker") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06nqfryg8hyc1kn7q62xbfjwnndl63il9j43aw5j0hqdg810cj1q")))

(define-public crate-tplinker-0.3.1 (c (n "tplinker") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0izpz5mqqs418rvjf6kmdl9jdif2clb7bzksv8d59my1v4dx341l")))

(define-public crate-tplinker-0.4.0 (c (n "tplinker") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pjdjr77499jfsjh806mly64y7mi9xafl6q75jxiqwqknah810v2")))

(define-public crate-tplinker-0.4.1 (c (n "tplinker") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02d5vgd8gp812kkgh0vazwzg5lmhr16fln087rj8ghd596vamb9x")))

(define-public crate-tplinker-0.4.2 (c (n "tplinker") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jdpn3ria3lk8yga9hy6cw3kwhqhazmq8nzsp6p8jgxk90y3z3h3")))

(define-public crate-tplinker-0.4.3 (c (n "tplinker") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02fham8010nvlf3k3x8kcnc8wcfxwg0lw2751js1j83k3g2lc91y")))

(define-public crate-tplinker-0.4.4 (c (n "tplinker") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sg7h89irg2hla4g9ag72bacbkc6q70k57z9q0d3mc79hnb338zy")))

