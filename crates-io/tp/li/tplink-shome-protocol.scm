(define-module (crates-io tp li tplink-shome-protocol) #:use-module (crates-io))

(define-public crate-tplink-shome-protocol-0.1.0 (c (n "tplink-shome-protocol") (v "0.1.0") (h "0jnip39bzyasn8m762whvw5n9y89ii2cbg7fdw8ww3ksbrlsjghl")))

(define-public crate-tplink-shome-protocol-0.1.1 (c (n "tplink-shome-protocol") (v "0.1.1") (h "0rfxzqy45k67f120cvbafhmfwvrkvsh707xm48kyzh3i6asl2dpk")))

(define-public crate-tplink-shome-protocol-0.1.2 (c (n "tplink-shome-protocol") (v "0.1.2") (h "1hal7gngqxd5xh7xv4wm7g4fcl2bzl8ih38hdw8ac3a18iys2jwf")))

