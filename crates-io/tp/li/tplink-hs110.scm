(define-module (crates-io tp li tplink-hs110) #:use-module (crates-io))

(define-public crate-tplink-hs110-0.1.0 (c (n "tplink-hs110") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "037y1gnr3cl0da33jky7nri8h0kcnwk6g73y486s0m88msngxdga")))

(define-public crate-tplink-hs110-0.2.0 (c (n "tplink-hs110") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1afdqq6m3h7ry7aj2y9bdilld6k4ia5572i4bsprcsh6lylklmgl")))

