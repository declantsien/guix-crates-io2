(define-module (crates-io tp m- tpm-box) #:use-module (crates-io))

(define-public crate-tpm-box-0.1.0 (c (n "tpm-box") (v "0.1.0") (d (list (d (n "testresult") (r "^0.3.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^8.0.0-alpha") (f (quote ("generate-bindings"))) (d #t) (k 0)))) (h "0xjrwrasqv9l4vf0jca9cysi5sr0728p73xzddzks9nd7jx31y6r")))

(define-public crate-tpm-box-0.1.1 (c (n "tpm-box") (v "0.1.1") (d (list (d (n "testresult") (r "^0.3.0") (d #t) (k 2)) (d (n "tss-esapi") (r "^8.0.0-alpha") (f (quote ("generate-bindings"))) (d #t) (k 0)))) (h "0dq6b7iyikpkkj7dw2qwq9xzk6ap4qwhx8zlbn1bcx3zzqp12f3n")))

