(define-module (crates-io tp m- tpm-change-pin) #:use-module (crates-io))

(define-public crate-tpm-change-pin-0.1.0 (c (n "tpm-change-pin") (v "0.1.0") (d (list (d (n "pkcs11") (r "^0.5") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)))) (h "10yns8vsbrhlqscy8ilnbxv0xr8pr7c8aakk60zgl33cb00qjhwb")))

