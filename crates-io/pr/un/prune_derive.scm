(define-module (crates-io pr un prune_derive) #:use-module (crates-io))

(define-public crate-prune_derive-0.1.0 (c (n "prune_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13sg266w618irfq8yy6xzbncifadgwz0958zzj2db81mfa0379vz")))

(define-public crate-prune_derive-0.1.1 (c (n "prune_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b0qgyksw9fpdg1rh6dli60w71fw2ifk8cwb9zl5paqprkg87rvh")))

(define-public crate-prune_derive-0.1.2 (c (n "prune_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0979wsx81p81wny1gz19nchg9fh5bl8mb72lgxgzfvhsff2ldvmg")))

(define-public crate-prune_derive-0.1.3 (c (n "prune_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qdjdg30b825czbihx711vs3x1l69wpq1rr0n3qwa3lk3nikrb9g")))

(define-public crate-prune_derive-0.1.4 (c (n "prune_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0s9zzbqihj9wgc1i131wlmlqij4x6s2byhrdnyg6l7npv2kv16h9")))

(define-public crate-prune_derive-0.1.5 (c (n "prune_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xmjrzqj0rr489a5srg1cnhwzjqh7bhli7m6nzcvf46h1sbi1n8d")))

(define-public crate-prune_derive-0.1.6 (c (n "prune_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prune") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1z56b4vvz7ydhc2gmcqvqyra01d4fmdghphpjlxh8jk8dmqpprl2")))

