(define-module (crates-io pr un prun) #:use-module (crates-io))

(define-public crate-prun-0.0.1 (c (n "prun") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1ni2pfzsnsg62zmdfzc7vnc3x00f74p2qphqqyr2v24ga4h4ia0s") (y #t)))

(define-public crate-prun-0.0.2 (c (n "prun") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0si3q6w74b7yn92gyx3vlyinw41ckb6w1wjb5q3b558hlnm01ks7") (y #t)))

(define-public crate-prun-0.0.3 (c (n "prun") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1qqpaw463db529gzb0fdl0xa623r0x1bp9651lvvwiss7air344x") (y #t)))

(define-public crate-prun-0.0.4 (c (n "prun") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "035lprfdhcl79m0zscd0y1klwv3p5vvh4k0b75r88lngca3jz5ly")))

(define-public crate-prun-0.0.5 (c (n "prun") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1wpxlpj9r5am0ala55ylma6cm83vpixx9mdmxgp5qvzfb14r7h3x")))

