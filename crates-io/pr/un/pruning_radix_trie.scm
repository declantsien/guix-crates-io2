(define-module (crates-io pr un pruning_radix_trie) #:use-module (crates-io))

(define-public crate-pruning_radix_trie-0.2.2 (c (n "pruning_radix_trie") (v "0.2.2") (h "057cqv6adyqi779s5zhqvqwg1v25h3xl2dsagpi6hz3bfqbqsl8m")))

(define-public crate-pruning_radix_trie-0.3.0 (c (n "pruning_radix_trie") (v "0.3.0") (h "0nx26l0pwd051xhzn9w6j2wxaj1g5lagsar672pvmqnrqk3nkpxk")))

(define-public crate-pruning_radix_trie-1.0.0 (c (n "pruning_radix_trie") (v "1.0.0") (d (list (d (n "compact_str") (r "^0.7.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0njxwcxnqndlxwnzm2rlvvcc5cfdhx613x3ymlzcyyif8zv9cx1y")))

