(define-module (crates-io pr un pruner) #:use-module (crates-io))

(define-public crate-pruner-0.4.0 (c (n "pruner") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "144qxrn09slz497a5jwpya5kpzhmpj81s2k9v1g18gvifxbjs6md")))

(define-public crate-pruner-0.5.0 (c (n "pruner") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "13f0zybd029yhzv4imsch3j2r53p5q8q7k1ks7wmync7nd359ihi")))

(define-public crate-pruner-0.6.0 (c (n "pruner") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0p57cmx07v6m01gxfbfcw1kgyqilqkm00sixcflw51fsprfjjwb4")))

(define-public crate-pruner-0.12.0 (c (n "pruner") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0qlfj3k00mddc3gd1xcgwnprn0gyynhwxyq1zgp2nlsxs2z7xgsd")))

(define-public crate-pruner-0.14.0 (c (n "pruner") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "093rajjr3ajcp1q43229rkakkz9203bmzk6swdp1f08d895s7pzq")))

(define-public crate-pruner-0.16.0 (c (n "pruner") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0rv0k76nms2cpq38p3ym632jj5cdfnvgaq90bwjmgjm8yakm7244")))

(define-public crate-pruner-0.17.0 (c (n "pruner") (v "0.17.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1winlxsvbb89c9ajykqlgn1jfg8n23blr630jmhbi9y1c3r28rsf")))

(define-public crate-pruner-0.22.0 (c (n "pruner") (v "0.22.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "00z29h36jgyskb2vblqy56hmgapygl8r29d2pl3y1j3fg7z827hs")))

(define-public crate-pruner-0.24.0 (c (n "pruner") (v "0.24.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1ym035jxg5narkpjq01xpaq7xd9f5j8phwvgk3n1za0idrrvwg2p")))

(define-public crate-pruner-0.26.0 (c (n "pruner") (v "0.26.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1iqxykk2qabyd8s9v61wvyln7idfg4z8ydc5nnahcjy9hwdlhca4")))

(define-public crate-pruner-0.28.0 (c (n "pruner") (v "0.28.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iso8601") (r "^0.5.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0vibm5liysmkc48d01nx38l87dv5kspg8k8s8fgb8kcj8cickldq")))

(define-public crate-pruner-0.30.0 (c (n "pruner") (v "0.30.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iso8601") (r "^0.5.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1f3v7bakdckf480c9q370vggflq77x2lr0c93qf9iqgyj8q25187")))

(define-public crate-pruner-0.32.0 (c (n "pruner") (v "0.32.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1m0yxd8s239jpz4p8r0fs5gp4x568zv3jqjv73ch4cwh79d16jdl")))

(define-public crate-pruner-0.34.0 (c (n "pruner") (v "0.34.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "13kkaqa2wa5gq5pjb0ad1i7l460cj7w60844k4cmrwpra42is2ii")))

(define-public crate-pruner-0.36.0 (c (n "pruner") (v "0.36.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1a2vlqi1gfiw5sksk8v14cbgkdyf5l0qam28mxinavxp3g4shx8j")))

(define-public crate-pruner-0.38.0 (c (n "pruner") (v "0.38.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0mqw78mnbjr0pv508clq5kfxpzwl29q6pnj5rpjcyc65wbarj6aa")))

(define-public crate-pruner-0.40.0 (c (n "pruner") (v "0.40.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0l42njlv41wpvf6shma6yhy3j7g1x2z34yz311hw7zmn0lndb0n1")))

(define-public crate-pruner-0.42.0 (c (n "pruner") (v "0.42.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0y3b2nq7m0hh1xxbc9mf0inxsjf9va5b90c73g41c8bm514py6cg")))

(define-public crate-pruner-0.44.0 (c (n "pruner") (v "0.44.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1cfy0ygj089cxsfwns7r9asg2kcb3v437hxxfrl7qjjfg7ck3llr")))

(define-public crate-pruner-0.46.0 (c (n "pruner") (v "0.46.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0sshidh9phkayixpynfqp0bz43jhw7h163jviqrlim6g6g28bhy9")))

(define-public crate-pruner-0.48.0 (c (n "pruner") (v "0.48.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "152dvbggg49pbgh23h6ya2jnlngafp3dgb8iv2ypq8kj2yy45sf9")))

(define-public crate-pruner-0.50.0 (c (n "pruner") (v "0.50.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "iso8601") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0adzwikd7nb1gpz04a8ysxji0v8gflhdz0987q6fazbjkkc660yi")))

