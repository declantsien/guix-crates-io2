(define-module (crates-io pr of profile_time) #:use-module (crates-io))

(define-public crate-profile_time-0.1.0 (c (n "profile_time") (v "0.1.0") (h "0gh84xmwbiij89qpikjm2j3r12rigz3gk0l9c8fh3wcqnl69z2ac") (y #t)))

(define-public crate-profile_time-0.1.1 (c (n "profile_time") (v "0.1.1") (h "18yjddwqj68d3r0xqs9km0mh82pmqgyqg6z99mlhp5hffzbhlsy1")))

