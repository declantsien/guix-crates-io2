(define-module (crates-io pr of profqu_neat) #:use-module (crates-io))

(define-public crate-profqu_neat-0.1.0 (c (n "profqu_neat") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1di91xi23hldzan9hrcb3vx1brwbfcqbs3znjn5ma3clyq95wfpc")))

(define-public crate-profqu_neat-0.1.1 (c (n "profqu_neat") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0qfny6m8kdc7p4fi6kfplhlwj5v1ipcrkbcb8xjlm9pwzrk807l0")))

