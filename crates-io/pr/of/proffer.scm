(define-module (crates-io pr of proffer) #:use-module (crates-io))

(define-public crate-proffer-0.1.0 (c (n "proffer") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1dvx9kv63y25dl895i6xfxs8hz9wlhin4c7q9nvrllrfbxdik3c4")))

(define-public crate-proffer-0.2.0 (c (n "proffer") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "18ri1wjqfabqgs632nv847x9832qpagsa874alz8g393mii30hdl")))

(define-public crate-proffer-0.3.0 (c (n "proffer") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "122cj0dxpgv3y71vk5xggxjspr5l6fl5ydc3rdrz3awqzsgh28m0")))

(define-public crate-proffer-0.4.0 (c (n "proffer") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "02cyj6ma9ssfczxsggxq25k5nqi1g7rf9657a2n1z3hqmnz1asya")))

(define-public crate-proffer-0.5.0 (c (n "proffer") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1h54djd8nakzrc6l3v1fqzczqnmgp9q7l5crw4zay2qhnybrq104")))

(define-public crate-proffer-1.0.0-rc1 (c (n "proffer") (v "1.0.0-rc1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("parsing" "full"))) (k 2)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0xbcfy6fa09v6sh514djhy8qq8cn44kw0cwlng1ami0pcjysa7aw")))

(define-public crate-proffer-1.0.0-rc2 (c (n "proffer") (v "1.0.0-rc2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("parsing" "full"))) (k 2)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "079pnps4ya07ynjbl43z31z7wzcjrcp03rb7isnv8zk6030h2559")))

