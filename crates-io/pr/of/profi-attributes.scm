(define-module (crates-io pr of profi-attributes) #:use-module (crates-io))

(define-public crate-profi-attributes-0.1.0 (c (n "profi-attributes") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)))) (h "0x42hf20rsyy7nrdqc5kr1p2ssd2gl03isywhrr8iy7z4gmj3pp5")))

(define-public crate-profi-attributes-0.1.1 (c (n "profi-attributes") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)))) (h "09d5fdn3z0ph7hwxp4y5lfcwsgghps4cpnf140h332n94a54b9z7")))

(define-public crate-profi-attributes-0.1.2 (c (n "profi-attributes") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)))) (h "1avc28s7lv047h24bqjnaygjjclii237szxjmx2yn5jmy4n1lp3j")))

