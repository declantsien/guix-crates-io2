(define-module (crates-io pr of profile) #:use-module (crates-io))

(define-public crate-profile-0.1.0 (c (n "profile") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("fs" "time" "tracing" "process" "macros" "rt" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "16lmhw75b418pn4iiflr16yd57dqrpysn1krncrlmn10wc7and8j")))

(define-public crate-profile-0.1.1 (c (n "profile") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("fs" "time" "tracing" "process" "macros" "rt" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "1d8x59qs20pz9vj11c203b5b7kg9sfyry204k292yad0xlqj3ags")))

