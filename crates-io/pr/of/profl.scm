(define-module (crates-io pr of profl) #:use-module (crates-io))

(define-public crate-profl-0.1.0 (c (n "profl") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xsp42psxr20rjk0071cagkijw3yh7ikw3r2hlgwcf5a52mldqrz") (f (quote (("active"))))))

(define-public crate-profl-0.1.1 (c (n "profl") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cmq2zs50rgjabih0s0w2y7n30ljimh09z1vckg4kv0inflwhpsl") (f (quote (("active"))))))

