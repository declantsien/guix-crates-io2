(define-module (crates-io pr of profiling-procmacros) #:use-module (crates-io))

(define-public crate-profiling-procmacros-0.1.0 (c (n "profiling-procmacros") (v "0.1.0") (d (list (d (n "optick") (r ">=1.3.2, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j0ljwvmg2lvh3631zjk33x8mavycw6izyrq3ay0zgcvc6w0il1c")))

(define-public crate-profiling-procmacros-0.1.1 (c (n "profiling-procmacros") (v "0.1.1") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cl2s2dqx04gd98gys09ndrkd9369bgqmjz0q6vwsy45ns3acxa1")))

(define-public crate-profiling-procmacros-0.1.2 (c (n "profiling-procmacros") (v "0.1.2") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xscc131xgbvs240cz20815fcf9d5zbrmgs03w827lq2iwra1x2z")))

(define-public crate-profiling-procmacros-0.1.3 (c (n "profiling-procmacros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0svi80jhk7vwr1rdhfc95ij0vaqvqhxs8pzwll584k2bvbkmr6fk")))

(define-public crate-profiling-procmacros-1.0.0 (c (n "profiling-procmacros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wlv6kl1k4nj8yhhv6al4il1bmcjly2q8vcdzqxabzfxair9hfdr")))

(define-public crate-profiling-procmacros-1.0.1 (c (n "profiling-procmacros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sarn47vxs3abb68ml5b51clyki7knxif9l3w000iim79rmjk0wv") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.2 (c (n "profiling-procmacros") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dqwlfc8if1dw67dnidhsgchh2xfx90ylc4d7hh27w7v7v052sj6") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.3 (c (n "profiling-procmacros") (v "1.0.3") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b40wbqskxqpn3vxmc0r40cby6f2nggiivmhjhv47hacs6z95p5r") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.4 (c (n "profiling-procmacros") (v "1.0.4") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05z59yrxpjmvdabmw0vbw2psr3llq64rh7rc4yvwb59s45vvr6gq") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.5 (c (n "profiling-procmacros") (v "1.0.5") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bq0apl983lvbm9xgwp6crwnbi3rz066vc5bfghalscc2ms4f6qm") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.6 (c (n "profiling-procmacros") (v "1.0.6") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdsf9prgnqz6x4hl70jcvh0crbxzgby2gvins2gg9pj2b0y7vlq") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.7 (c (n "profiling-procmacros") (v "1.0.7") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k37l3l1222k9b58vl8nsh9ms3rdpl3sdy0m8bcqlnwhxwbj87ks") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.8 (c (n "profiling-procmacros") (v "1.0.8") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c6076zv09bm7h9lfjcnasqf2sxjbnp43vcbzc52ic8v2n6xn2m1") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.9 (c (n "profiling-procmacros") (v "1.0.9") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1794g6jd5ahmi8z3bljk5hvkxmdipnyxym7dbv3vipr1j6wzhyq9") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.10 (c (n "profiling-procmacros") (v "1.0.10") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00xrv06x1i4vs50npisf9i431nmj6mj11yp08252kxcscag5xibl") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.11 (c (n "profiling-procmacros") (v "1.0.11") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gylp366pw3v56ccm9q7cz9q9z2snsgif8kl94kq1zmrnr2nl5gb") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.12 (c (n "profiling-procmacros") (v "1.0.12") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13ddnqfnd207c7pvz61zvcm1csd0s5bwrm9yi5a259r1qvikd3wx") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.13 (c (n "profiling-procmacros") (v "1.0.13") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01gbxcn06a6xlbdpjpfh1lnhkqdn1nyzr468wfdl9ay1cmyjscjb") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.14 (c (n "profiling-procmacros") (v "1.0.14") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j2wj5s2mvl6jsy2hqpyspvi8jmm3dd8nl90brp2jjdw4z6zx5yf") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

(define-public crate-profiling-procmacros-1.0.15 (c (n "profiling-procmacros") (v "1.0.15") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zf3a6wc21l43ckmyhfd56pyq255i9msq9i5zhn4777cr1cwy8c0") (f (quote (("profile-with-tracy") ("profile-with-tracing") ("profile-with-superluminal") ("profile-with-puffin") ("profile-with-optick"))))))

