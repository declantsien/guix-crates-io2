(define-module (crates-io pr of profidcp) #:use-module (crates-io))

(define-public crate-profidcp-1.0.2 (c (n "profidcp") (v "1.0.2") (d (list (d (n "etherparse") (r "^0.14.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pcap") (r "^1.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0n3y8y7637yf8idl9ip0aabf0kk5gfvpawwga9w6m62l3bwq9xi5")))

(define-public crate-profidcp-1.0.3 (c (n "profidcp") (v "1.0.3") (d (list (d (n "etherparse") (r "^0.14.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pcap") (r "^1.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1s9wxh5gm66cr6aynifp5521ndp6i5lcwh9s77rgf1kdprzh94hy")))

