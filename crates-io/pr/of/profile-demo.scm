(define-module (crates-io pr of profile-demo) #:use-module (crates-io))

(define-public crate-profile-demo-0.1.0 (c (n "profile-demo") (v "0.1.0") (h "1cbwfd8qzcfvzvkgsx488cyz0inc154glr456xf1ln2hkdblsdqd")))

(define-public crate-profile-demo-0.1.1 (c (n "profile-demo") (v "0.1.1") (h "0fr73ksbk2m10im2p84zl0n482wcgrgr0730577bqhm461yxk96x")))

(define-public crate-profile-demo-0.1.2 (c (n "profile-demo") (v "0.1.2") (h "0zs128az9g8m537y6194yj63dcwii7mrficsndjfznw9crz3wf43")))

(define-public crate-profile-demo-0.1.3 (c (n "profile-demo") (v "0.1.3") (h "0li13xnji7mdlln4l4jr6cyl878zr75bkg9bwpricpb6n3vic6sz")))

