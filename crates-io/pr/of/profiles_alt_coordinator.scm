(define-module (crates-io pr of profiles_alt_coordinator) #:use-module (crates-io))

(define-public crate-profiles_alt_coordinator-0.2.0 (c (n "profiles_alt_coordinator") (v "0.2.0") (d (list (d (n "hc_zome_profiles_integrity") (r "=0.2.0") (d #t) (k 0)) (d (n "hdk") (r "=0.2.6") (k 0)) (d (n "serde") (r "=1.0.163") (k 0)))) (h "08ldd891aqhc6n89jsz6z4b58z9p1ivxszwq7rzhkgs4p99nhzww") (r "1.70.0")))

