(define-module (crates-io pr of profilr-db) #:use-module (crates-io))

(define-public crate-profilr-db-0.1.0 (c (n "profilr-db") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdxjchz8mfk714dancwi1yrrgkxklmg81z92xwqpc5p1crhyl2c")))

