(define-module (crates-io pr of profane-rs) #:use-module (crates-io))

(define-public crate-profane-rs-0.0.1 (c (n "profane-rs") (v "0.0.1") (h "0x78gv9pjx9jaxhq9b2vx650vxrynkyqqdx1dhvsf3j0varqv6yl") (y #t)))

(define-public crate-profane-rs-0.0.2 (c (n "profane-rs") (v "0.0.2") (h "1d8vdkli14yb6pr8bd4q90zxs3491n4k6941hlq39sr60hcgvzam") (y #t)))

(define-public crate-profane-rs-0.0.3 (c (n "profane-rs") (v "0.0.3") (h "11d0rmcl0qk0gfppvkda8azvr07bj710fafisf2gd0lcxbfspr56") (y #t)))

(define-public crate-profane-rs-0.0.4 (c (n "profane-rs") (v "0.0.4") (h "0jhv86hr9z70xkjkkh8pa45hbbhb899f2f04b4s6kx8dmj79s2wg")))

