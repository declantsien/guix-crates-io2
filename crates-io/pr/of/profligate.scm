(define-module (crates-io pr of profligate) #:use-module (crates-io))

(define-public crate-profligate-0.1.0 (c (n "profligate") (v "0.1.0") (h "1f675z96i7cz19w24cknpl0dfg6cbhf9ki5drrf8mghacp5alm5l")))

(define-public crate-profligate-0.2.0 (c (n "profligate") (v "0.2.0") (h "1r6p6gck4pxxdnax1y2lxigl34i7c4yirz7mgw45i7xivwgjnc27")))

