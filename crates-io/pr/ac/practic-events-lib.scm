(define-module (crates-io pr ac practic-events-lib) #:use-module (crates-io))

(define-public crate-practic-events-lib-0.1.0 (c (n "practic-events-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14j9mfx8cpvx1b2pad5cyq72gxp8b5rry7wbmjgs009l11r05m6k")))

(define-public crate-practic-events-lib-0.1.1 (c (n "practic-events-lib") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "119z03wx5x8sglmcmnyhn00cp1i5qvg2lv0lmdlfygb6hwky1s3w")))

(define-public crate-practic-events-lib-0.1.2 (c (n "practic-events-lib") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09kn5g6klyd4fcbgmf446wky37jh9wipzb7dahn71cffj7wwyc9d")))

(define-public crate-practic-events-lib-0.1.3 (c (n "practic-events-lib") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1idb7ynz3ddasp0lh26jhizkk8i1z7028vscfkn073czadj1aryc")))

(define-public crate-practic-events-lib-0.1.4 (c (n "practic-events-lib") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04x1wy1091mvkm61i9pwp47hbyd9lhm90bmlvn4jbdr4s6bs3v6k")))

