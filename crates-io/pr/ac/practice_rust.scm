(define-module (crates-io pr ac practice_rust) #:use-module (crates-io))

(define-public crate-practice_rust-0.1.0 (c (n "practice_rust") (v "0.1.0") (h "0lczh96r0ij5bkxvv74zaibqc9v6gl23w5mn5k0jm0w5khjlmmfv")))

(define-public crate-practice_rust-0.1.1 (c (n "practice_rust") (v "0.1.1") (d (list (d (n "adder") (r "^0.1.0") (d #t) (k 0)))) (h "0064c13hyddzny0c3qfilydll76v3m1lvwgrvj5vaf9m0drmbcli")))

