(define-module (crates-io pr ac practise) #:use-module (crates-io))

(define-public crate-practise-0.1.0 (c (n "practise") (v "0.1.0") (h "0w3zc3sfqzz9v8j483xx55apsyxnmdzj03ymy228z6wi22961j61")))

(define-public crate-practise-0.1.1 (c (n "practise") (v "0.1.1") (h "105ni7jwlf5gy8j1a3k1w8k2mc6scvd5jmargfxcq3v1ivk483yp")))

