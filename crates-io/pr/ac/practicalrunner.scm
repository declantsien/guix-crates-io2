(define-module (crates-io pr ac practicalrunner) #:use-module (crates-io))

(define-public crate-practicalrunner-0.1.0 (c (n "practicalrunner") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fontconfig") (r "^0.8.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (f (quote ("ttf"))) (d #t) (k 0)))) (h "16k6vkd31sp9k03rn2dc9lchiikp5dgag0ygf46yiamxvhri16i2")))

(define-public crate-practicalrunner-0.1.1 (c (n "practicalrunner") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fontconfig") (r "^0.8.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (f (quote ("ttf"))) (d #t) (k 0)))) (h "0sinpjfdp0zx5rn3v53clvjgxdmkg851afdfpb44mb5c6l0v0v5j")))

