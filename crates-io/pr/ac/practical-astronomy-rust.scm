(define-module (crates-io pr ac practical-astronomy-rust) #:use-module (crates-io))

(define-public crate-practical-astronomy-rust-0.2.0 (c (n "practical-astronomy-rust") (v "0.2.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1lbs8cf9d9ab1k9yh5j3pnq81j826pplrycdy2kpbvqvwp3rf6v0")))

(define-public crate-practical-astronomy-rust-0.2.1 (c (n "practical-astronomy-rust") (v "0.2.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1c0n8xrxcyjffghqvghdash2qnv56p74i6gxxvi4sbyp05j0c05l")))

(define-public crate-practical-astronomy-rust-0.2.2 (c (n "practical-astronomy-rust") (v "0.2.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "06h8fm8vgz1d5ihx3zfb1q1vvz3shyqppfr7c07z3giaycfgg6sb")))

(define-public crate-practical-astronomy-rust-0.2.3 (c (n "practical-astronomy-rust") (v "0.2.3") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1ffvxv6bfx9w66vzzslwws7xndpac03gcbmb7p3i04drlk0wrzsd")))

(define-public crate-practical-astronomy-rust-0.2.4 (c (n "practical-astronomy-rust") (v "0.2.4") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "156ipywxm4n1iwn4q68467vnc2vprl1sllppizh1yhr7p07mlf23")))

