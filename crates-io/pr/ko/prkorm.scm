(define-module (crates-io pr ko prkorm) #:use-module (crates-io))

(define-public crate-prkorm-0.1.0 (c (n "prkorm") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0ba44fgyxa3wv2j6v1r7qgqnr8a15s53f2l40xwrxzp3mcdppw1z")))

(define-public crate-prkorm-0.2.0 (c (n "prkorm") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0wiwaq7ixqlawj54pqzskwrsqqaqf8p36md2b56d15s78szdcnlp")))

(define-public crate-prkorm-0.3.0 (c (n "prkorm") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0r1kn1k5h64y10rckhzad8n8m0vg2b609sza6ni84pvzqzx0d6m2")))

(define-public crate-prkorm-0.4.0 (c (n "prkorm") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "1ljzsz4byl1zlsvhsc3z3rg62qv50q866p3mar9ic23zhm4hfdpp")))

(define-public crate-prkorm-0.5.0 (c (n "prkorm") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "1ix3as1axa4zkhmjz54axcy9n9g46x5vl8hxnkavb2ws8a48l1hb")))

(define-public crate-prkorm-0.5.1 (c (n "prkorm") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0awp8f97jkwvzg11qfn4nksv3gfjbaz26fhs3ldch88ggban6kl8")))

(define-public crate-prkorm-0.5.2 (c (n "prkorm") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0ls47hvrkcfj3ggq3ik438sryqrwjwnh223bmay3qsklq6k1n4k0")))

(define-public crate-prkorm-0.5.3 (c (n "prkorm") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0xi31y7wssjx4sbg2z3bsla0pbyk9634rka03id02dfswr5avad6")))

