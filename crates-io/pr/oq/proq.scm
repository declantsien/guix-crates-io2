(define-module (crates-io pr oq proq) #:use-module (crates-io))

(define-public crate-proq-0.0.0 (c (n "proq") (v "0.0.0") (h "1ri9qf340hnvfqvwdyh68zz319xkgp9fk73pyisyyl0baa13mqzz")))

(define-public crate-proq-0.1.0 (c (n "proq") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.6.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)))) (h "1iija6f982qjc0ac3cqp2bcb07gpvn1s0ba4lsgi4yn576y8svz8")))

