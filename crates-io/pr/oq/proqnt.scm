(define-module (crates-io pr oq proqnt) #:use-module (crates-io))

(define-public crate-proqnt-0.1.0 (c (n "proqnt") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)))) (h "1m4r4xqnw5j0dn1gclhi1akrivcv94f2bmryajbrs0qkby6b6c8n") (f (quote (("std") ("default" "std"))))))

