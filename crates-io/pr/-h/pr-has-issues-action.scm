(define-module (crates-io pr -h pr-has-issues-action) #:use-module (crates-io))

(define-public crate-pr-has-issues-action-2.0.0 (c (n "pr-has-issues-action") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "02q7cy4ad5mc89dj0in4vwg2ybpflyhq8yqkfd1l86lif6n83pkn")))

