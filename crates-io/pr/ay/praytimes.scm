(define-module (crates-io pr ay praytimes) #:use-module (crates-io))

(define-public crate-praytimes-1.0.0 (c (n "praytimes") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "1xyc3giljjvjddl223p7mazakmacfvwfca4crndlp9zzkwlnzs0j") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

(define-public crate-praytimes-1.1.0 (c (n "praytimes") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "1b5xvhn3k561xnlczanh8hb3jp2ynnjfab0j1rnd0wmd53gspvp4") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

