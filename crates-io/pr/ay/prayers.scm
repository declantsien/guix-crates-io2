(define-module (crates-io pr ay prayers) #:use-module (crates-io))

(define-public crate-prayers-0.1.0 (c (n "prayers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1by2887lxxz7nd5l78j29pjamzkcz4r13akqcxysdls1kdkdjpdp")))

(define-public crate-prayers-0.2.0 (c (n "prayers") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "03f3d3aj03vf03fyq1izjycn09ddwc3fmfckdfxxb8mspp1xmyk0")))

(define-public crate-prayers-0.2.1 (c (n "prayers") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "19kn1151gzhvsjixw617jc2vyv94sxpsyhzc3xbxydcpvwnz3hw4")))

(define-public crate-prayers-0.2.2 (c (n "prayers") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "15z4z4zlh1d6d1lysjxxn7wy56fjcnfhnjb01wz7aiilxpm0bn8n")))

