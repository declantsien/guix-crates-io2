(define-module (crates-io pr ay prayterm) #:use-module (crates-io))

(define-public crate-prayterm-0.1.0 (c (n "prayterm") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0wzf3vdqsrg7s8a2r1sy8bgqi0hsl24wy5p6fp9clciq4r3dvwyj") (y #t)))

(define-public crate-prayterm-1.0.0 (c (n "prayterm") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0xgzyk1lq39b1i5xq6nk79h07j7i6sy67761pi2vq8p1zklm4kq1") (y #t)))

(define-public crate-prayterm-1.0.1 (c (n "prayterm") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1gxicy7mj1b5h45fc01c607n5fylqa63vd8d3lrmajnmki5s18f4")))

