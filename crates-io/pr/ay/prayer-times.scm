(define-module (crates-io pr ay prayer-times) #:use-module (crates-io))

(define-public crate-prayer-times-0.1.0 (c (n "prayer-times") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.10.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "0zq6q5rbf0ln2q2fvhfxn9qk4v7n32wzcyzncclqz3h63bgllc5b")))

