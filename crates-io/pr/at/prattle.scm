(define-module (crates-io pr at prattle) #:use-module (crates-io))

(define-public crate-prattle-0.1.0 (c (n "prattle") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0myh0vymn4wd779cdy8wfgysygy1ja1bf6f443nivgvqnd60adb4")))

(define-public crate-prattle-0.1.1 (c (n "prattle") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0vzwvdv0a4bcz55fmlpg8xxly8yq0rsfq5219nam2s8smvppy6vs")))

(define-public crate-prattle-0.1.2 (c (n "prattle") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0zlpqvc938hbybrs060pyxysclbkg0ql7s6jpnqdpr3966y3072g")))

(define-public crate-prattle-0.1.3 (c (n "prattle") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0wmyvlh53g7a4vyrmwxcv7gqjhdmp9z5xqbfyxfqd7qsmii37zir")))

