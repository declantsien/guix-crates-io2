(define-module (crates-io pr at pratt) #:use-module (crates-io))

(define-public crate-pratt-0.1.0 (c (n "pratt") (v "0.1.0") (h "056c0dn21dynsivaq5qgz8iawg2wincackb88jjpbqzd1bnp0944")))

(define-public crate-pratt-0.1.1 (c (n "pratt") (v "0.1.1") (h "1093wakvmy7cfc42593m2vm7b0zyzxhwb1w65v6b1s0i7rj8a346")))

(define-public crate-pratt-0.1.2 (c (n "pratt") (v "0.1.2") (h "18wmxxjcvijc0giipgbzd01m6i14pny35lv2qbdrav4bxwa5yyma")))

(define-public crate-pratt-0.1.3 (c (n "pratt") (v "0.1.3") (h "0q2cd7n1bvjqdn9n6wh8xpx2k3swg9kxplhj60awmivi3bp7hbv7")))

(define-public crate-pratt-0.1.4 (c (n "pratt") (v "0.1.4") (h "1ng5y1x32aw1wir41call0w4flr50n0y1xib9pvy6pnm6mjy4f1q")))

(define-public crate-pratt-0.2.0 (c (n "pratt") (v "0.2.0") (h "0lfif3fyjqdzhvdqwnkv9lq8qlhdrj2l7c8drxy3vk8zbfy0sn4p")))

(define-public crate-pratt-0.2.1 (c (n "pratt") (v "0.2.1") (h "1d0isbipdp81g9m1jllvx482z1rpdbhbpmhg6ij2m0024yssc7iv")))

(define-public crate-pratt-0.2.2 (c (n "pratt") (v "0.2.2") (h "06vw8mcxn9667y7nk7a8786gi14jx7y4s4ak126wq2nwwm65d8lk")))

(define-public crate-pratt-0.3.0 (c (n "pratt") (v "0.3.0") (h "1kid62n4ri15rzg6crzz8m7waribk2dnvpchawcpnslkyw9bq6z3")))

(define-public crate-pratt-0.4.0 (c (n "pratt") (v "0.4.0") (h "1f4b5d3pxg3rvnqfw5qhqs070x7kzcw3lrr0p0c0fvq7bm1a9q0p")))

