(define-module (crates-io pr ll prll-ri) #:use-module (crates-io))

(define-public crate-prll-ri-1.0.2 (c (n "prll-ri") (v "1.0.2") (d (list (d (n "bio") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)))) (h "08l07f7y1lx7ppf9ciwb01iyny0kpbfgrjpwggi6z84fx8clfpnd")))

(define-public crate-prll-ri-1.0.3 (c (n "prll-ri") (v "1.0.3") (d (list (d (n "bio") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)))) (h "1frvrl2x94gnibbij9bx9c31mz7bz0dksymr4wg5fqbl2gx6ffwr")))

(define-public crate-prll-ri-1.0.4 (c (n "prll-ri") (v "1.0.4") (d (list (d (n "bio") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0fi9lcz22yilbbyzd4305pbklldydlnq374y5qjxal0xqhqsaj6n")))

(define-public crate-prll-ri-1.0.5 (c (n "prll-ri") (v "1.0.5") (d (list (d (n "bio") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "03krji790fdfy9f0m07svzs2m4sml6kdw1zjmr1avvvsz6sywasb")))

(define-public crate-prll-ri-1.0.6 (c (n "prll-ri") (v "1.0.6") (d (list (d (n "bio") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00m590bxrjwsxr15wal2lzzw3rw3l3wqkyl6chzqy557hzz849mp")))

