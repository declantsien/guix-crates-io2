(define-module (crates-io pr ox proxy-dll) #:use-module (crates-io))

(define-public crate-proxy-dll-0.1.0 (c (n "proxy-dll") (v "0.1.0") (d (list (d (n "proxy-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "08piv9x5zi4k830jpqpsf1ajf593bfsjg0qhi0xh646fkrr85sf0")))

(define-public crate-proxy-dll-0.2.0 (c (n "proxy-dll") (v "0.2.0") (d (list (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)) (d (n "proxy-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yfz2ylvd0hhdjv4d6h1zafqx9ys22bs9qzwl29rm57kh90ga42d")))

(define-public crate-proxy-dll-0.2.1 (c (n "proxy-dll") (v "0.2.1") (d (list (d (n "proxy-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f1ry1ar7cf4kml385r7l6r405ljjjh56vmg78b8sz7x7vmpvknf")))

(define-public crate-proxy-dll-0.2.2 (c (n "proxy-dll") (v "0.2.2") (d (list (d (n "proxy-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sq8ns82j8amrgsp7fkh5m0067l79wjqv3jbj33ns4j3r3y7np67")))

(define-public crate-proxy-dll-0.2.3 (c (n "proxy-dll") (v "0.2.3") (d (list (d (n "proxy-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jkbxp25h3qii70pkm3ihhym6p7ffn431x0b5ahn6dgrzbsqgsac")))

(define-public crate-proxy-dll-0.2.4 (c (n "proxy-dll") (v "0.2.4") (d (list (d (n "ctor") (r "^0.1.26") (d #t) (t "cfg(linux)") (k 0)) (d (n "proxy-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13c5vdzppx8nmd34hsld812f0w24g8i3i9g4xfvkd5044h386k32")))

(define-public crate-proxy-dll-0.2.5 (c (n "proxy-dll") (v "0.2.5") (d (list (d (n "ctor") (r "^0.1.26") (d #t) (t "cfg(linux)") (k 0)) (d (n "proxy-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16ziimv6gn22blqzgkzg0wk539bskqkx3psb51kfk585ln25hn0x")))

