(define-module (crates-io pr ox proxim) #:use-module (crates-io))

(define-public crate-proxim-0.1.0 (c (n "proxim") (v "0.1.0") (d (list (d (n "prettylog") (r "^0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w37bli176fgwbihff2b6kaicwqazxvv7wq4ha2x5bp0zm76zbpn")))

(define-public crate-proxim-0.1.1 (c (n "proxim") (v "0.1.1") (d (list (d (n "prettylog") (r "^0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1075lx13v5yswa788wr89q8mi6fjfxik8pn6q3si1bsba0a95jzl")))

(define-public crate-proxim-0.1.2 (c (n "proxim") (v "0.1.2") (d (list (d (n "prettylog") (r "^0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vscz5ysibm0l5zzn3cj6wjydpxh17sdshh3h7r3dm1wx8gq0czi")))

