(define-module (crates-io pr ox proxyify) #:use-module (crates-io))

(define-public crate-proxyify-0.1.0 (c (n "proxyify") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0sphcaibfxwz8r8yd3lc0pd66xc4zalwwfakndhsdmqw4ark3abm") (y #t)))

(define-public crate-proxyify-0.2.0 (c (n "proxyify") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "1wg8gk79ami0jy1p07j0n6dwr8rywa5hmvx6is0kdq03pfi9ackz") (y #t)))

(define-public crate-proxyify-0.3.0 (c (n "proxyify") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "11475m46zx6lalm427p19hr7v1hffj6fy85r28a96sz3c5a4732k") (y #t)))

(define-public crate-proxyify-0.4.0 (c (n "proxyify") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "04m9h6fivlyrczh3rx8lx72nh9dwcq8hysqfmyqfp2yj0b6gvpb2") (y #t)))

(define-public crate-proxyify-0.4.1 (c (n "proxyify") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "15xg5lqisknkp3lvzbw24ip2p5bwb798c3vzbnhnlq909znal8f0") (y #t)))

