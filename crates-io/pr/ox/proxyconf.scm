(define-module (crates-io pr ox proxyconf) #:use-module (crates-io))

(define-public crate-proxyconf-0.1.0 (c (n "proxyconf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0sra4bsbwwcyqcwqi5iqr06lk8hysy35hv7pmxlgvffvxl58l7aq")))

(define-public crate-proxyconf-0.1.1 (c (n "proxyconf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0dp8r3fq677ps4rzf4kvdnlxfzvf8bdd1xwjww5lzxdhg05y3xq0")))

(define-public crate-proxyconf-0.1.2 (c (n "proxyconf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5.1") (d #t) (k 0)))) (h "0x3xp07v0jn76jy8dx666rqprmdzzi0w1q166cjrn7xr75nc8rl7")))

(define-public crate-proxyconf-0.2.0 (c (n "proxyconf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5.1") (d #t) (k 0)))) (h "07sygvqswwp1i1d6lir49xlj6xsd4bsb3rjgfgrdcf35bkl8k1av")))

(define-public crate-proxyconf-0.2.1 (c (n "proxyconf") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "winreg") (r "^0.6.0") (d #t) (k 0)))) (h "0gg5wwfw5ibq8i8g8gw2122bmg91qh7yimmgs78a867axy468g4k")))

