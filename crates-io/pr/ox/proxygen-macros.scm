(define-module (crates-io pr ox proxygen-macros) #:use-module (crates-io))

(define-public crate-proxygen-macros-0.4.0 (c (n "proxygen-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15x23qawp7x0zklx0m2q6wyx0y3x2xypdrjp88znmhf0r85nbr53")))

(define-public crate-proxygen-macros-0.5.0 (c (n "proxygen-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06habsd5vf2nw2imlafdak3bj9p389221f3ac53g55sgxvjh3c7w")))

(define-public crate-proxygen-macros-0.5.1 (c (n "proxygen-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17mh05lp6rq78sqb8fnm20m7h0n5nn4dxvidfjfqd29p0qjn0lsr")))

