(define-module (crates-io pr ox proxy-x) #:use-module (crates-io))

(define-public crate-proxy-x-0.1.0 (c (n "proxy-x") (v "0.1.0") (d (list (d (n "clap") (r "~4.4.11") (f (quote ("cargo"))) (d #t) (k 0)))) (h "04m6x9dv52r44q9jnricpsddip6v0l6is3s84y64ypyvjcwanzra")))

(define-public crate-proxy-x-0.1.1 (c (n "proxy-x") (v "0.1.1") (d (list (d (n "clap") (r "~4.4.11") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0vllvp1g6i87hbslwv1zvazrhnw4hyf1vkyph4szdqx4ajl9idab")))

(define-public crate-proxy-x-0.2.1 (c (n "proxy-x") (v "0.2.1") (d (list (d (n "clap") (r "~4.4.11") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "095f73qf2a1lrsnfzr48f2l6r1d4wvm3pfxqdmpa417j4slzszy7")))

