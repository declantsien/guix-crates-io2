(define-module (crates-io pr ox proxy-sys) #:use-module (crates-io))

(define-public crate-proxy-sys-0.1.0 (c (n "proxy-sys") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00qms1ing09587baqxbdlc7gm321lav926b7z51jaxvdqzlpgr8a")))

(define-public crate-proxy-sys-0.1.1 (c (n "proxy-sys") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "minwindef" "std" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19z4xjwfsagqi64k48z75c6fvsnd40c2vvmcaya07jq96fi0f1wy")))

(define-public crate-proxy-sys-0.2.0 (c (n "proxy-sys") (v "0.2.0") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0xmw2n1d3vbj3gkhpa0fjhhji1fxx6sqb7xf022vd43m50rjn17v")))

(define-public crate-proxy-sys-0.2.1 (c (n "proxy-sys") (v "0.2.1") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09z886xs864aaa0gn7y5dgaz1rmqncajh2bgbrlx7r08rmc2nysr")))

(define-public crate-proxy-sys-0.2.2 (c (n "proxy-sys") (v "0.2.2") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "160ksqxxybkzqw0x9bq2j7ccxn69gidvdgvxhxmz8k0c3hs2i5pg")))

