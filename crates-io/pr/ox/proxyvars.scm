(define-module (crates-io pr ox proxyvars) #:use-module (crates-io))

(define-public crate-proxyvars-0.1.0 (c (n "proxyvars") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)))) (h "0ykisbn2vfixvymahy288aj9fn6hrfpznznwxibsvb1vlb195472")))

(define-public crate-proxyvars-0.1.1 (c (n "proxyvars") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)))) (h "0zx85hz6j8hjd9sn2z2b9wgqkfcqr1w0gc24nn92s29dk97ysg5r")))

(define-public crate-proxyvars-0.1.3 (c (n "proxyvars") (v "0.1.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)))) (h "0j8clk41jgy5mmj2wp0i39ak3h8xxqixi1xmvxg9gv5nzxr7z08p")))

(define-public crate-proxyvars-0.2.0 (c (n "proxyvars") (v "0.2.0") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)))) (h "018yvk0qyliyb4wx9r24mb03w9jmmcx9l30hn7cqrlplx4dbz1bj")))

