(define-module (crates-io pr ox proxy-scraper) #:use-module (crates-io))

(define-public crate-proxy-scraper-0.1.0 (c (n "proxy-scraper") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1fm2n0wkfgrddqcx077r4pcfg0drg048xnf03v5sq41sfx5gpv55")))

(define-public crate-proxy-scraper-0.2.0 (c (n "proxy-scraper") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0hna93r2x8lya9lr275z9f3xlarjah142jkdp54zf3wcrjq5chza") (f (quote (("scraper")))) (s 2) (e (quote (("default" "scraper" "dep:argh" "dep:reqwest" "dep:tokio"))))))

