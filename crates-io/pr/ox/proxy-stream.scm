(define-module (crates-io pr ox proxy-stream) #:use-module (crates-io))

(define-public crate-proxy-stream-0.0.0 (c (n "proxy-stream") (v "0.0.0") (h "1ydwnhkhfdplwy6rb1s7bxwyw2pjwlsrarz80xzhw5faznfzj5fs")))

(define-public crate-proxy-stream-0.0.1 (c (n "proxy-stream") (v "0.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0j1h94w840r5rzc75qdvknbfmvcrx785n7a0a24mslalxwda710b")))

(define-public crate-proxy-stream-0.0.2 (c (n "proxy-stream") (v "0.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hxm7lz7vwcfcyhai7xjqaalh1jxijwdxa57dxs4wq2ax9gh10hw")))

