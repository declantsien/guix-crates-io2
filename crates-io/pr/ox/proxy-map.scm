(define-module (crates-io pr ox proxy-map) #:use-module (crates-io))

(define-public crate-proxy-map-0.1.0 (c (n "proxy-map") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "pingora") (r "^0.2.0") (f (quote ("proxy"))) (d #t) (k 0)))) (h "1yc3mf3flndap42qxwki3jxm2db3pqaj3flhhnpxrvijy2gf5kpl")))

