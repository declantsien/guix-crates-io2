(define-module (crates-io pr ox proxit) #:use-module (crates-io))

(define-public crate-proxit-1.0.0 (c (n "proxit") (v "1.0.0") (h "0z0jy3x07w8lbdnklh7gdcdl2mf4blmbm9qqvlj94lmqbybv8g2h")))

(define-public crate-proxit-1.0.1 (c (n "proxit") (v "1.0.1") (h "0w1grpd6b22k2qs5yhb0j8xmiv7jfdv6r0715aysm8lwrqr1v7kr")))

