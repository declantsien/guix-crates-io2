(define-module (crates-io pr ox proxytoggle) #:use-module (crates-io))

(define-public crate-proxytoggle-1.0.0 (c (n "proxytoggle") (v "1.0.0") (d (list (d (n "native-windows-derive") (r "^1.0") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0") (f (quote ("tray-notification" "message-window" "menu" "cursor"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1lgykky0ai5y696jj52y4pf6dkfx46gxy1wm6mxv3c1xvlqslfmq")))

