(define-module (crates-io pr ox proxy-protocol) #:use-module (crates-io))

(define-public crate-proxy-protocol-0.1.0 (c (n "proxy-protocol") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19bcacwgk5xzz5vkz9dh3k5bz2vklai6aj1vsghxqkfvbccq817l")))

(define-public crate-proxy-protocol-0.1.1 (c (n "proxy-protocol") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mqvx872n20r9rx4zadnsh63afn1bl06wfqpf6vccirxi2fnv5mp")))

(define-public crate-proxy-protocol-0.2.0 (c (n "proxy-protocol") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z3700x0xkzlx91dnz3kdpq1rymw6wy3zbqfdq34n8s2zl4kdhk6")))

(define-public crate-proxy-protocol-0.3.0 (c (n "proxy-protocol") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00ayax97c18zig3cdcskc9vnvvq711p9j45m84mpwi1q6ipb1xa2")))

(define-public crate-proxy-protocol-0.4.0 (c (n "proxy-protocol") (v "0.4.0") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "~0.8") (d #t) (k 2)) (d (n "snafu") (r "~0.6") (d #t) (k 0)))) (h "1c6d8zp58arcbzzjr63pfs35nsx3gnnz21hq4prbjfkxfaqi1ci6") (f (quote (("default") ("always_exhaustive"))))))

(define-public crate-proxy-protocol-0.4.1 (c (n "proxy-protocol") (v "0.4.1") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "~0.8") (d #t) (k 2)) (d (n "snafu") (r "~0.6") (d #t) (k 0)))) (h "01nhsw1li9mwhbhaynkxiyp3l0559b8alh9r1n3cv82kr7h3v412") (f (quote (("default") ("always_exhaustive"))))))

(define-public crate-proxy-protocol-0.5.0 (c (n "proxy-protocol") (v "0.5.0") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "~0.8") (d #t) (k 2)) (d (n "snafu") (r "~0.6") (d #t) (k 0)))) (h "0cnmf4kcw6yl1si9v78gs9ybyc7f19j37k2hyg2zaf6744ncfl0f") (f (quote (("default") ("always_exhaustive"))))))

