(define-module (crates-io pr ox proxy_config) #:use-module (crates-io))

(define-public crate-proxy_config-0.0.1 (c (n "proxy_config") (v "0.0.1") (d (list (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0b3vgjwv038la9q8227fnk4zsiq4kvr9s8i06ifzsvfywi9zw5lk") (f (quote (("env") ("default" "env"))))))

(define-public crate-proxy_config-0.0.2 (c (n "proxy_config") (v "0.0.2") (d (list (d (n "url") (r "^1.5") (d #t) (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1r13i3jihkiar492pc51pi9lmpfjs3qzrj8hmmgf7x3k2v6q6jpw") (f (quote (("env") ("default" "env"))))))

