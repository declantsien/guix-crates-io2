(define-module (crates-io pr ox proximal-optimize) #:use-module (crates-io))

(define-public crate-proximal-optimize-0.1.0 (c (n "proximal-optimize") (v "0.1.0") (h "1zd9fci2ak1q763p3hbwjhl66f7pgf1sd0sqxz49bh9iy5pdcp7z")))

(define-public crate-proximal-optimize-0.2.0 (c (n "proximal-optimize") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "121zq6cah33j67gka5cjp69fa4ch8q1hp7nkkgmmbriss9aciymy")))

