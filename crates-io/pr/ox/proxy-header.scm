(define-module (crates-io pr ox proxy-header) #:use-module (crates-io))

(define-public crate-proxy-header-0.1.0 (c (n "proxy-header") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1jsyb0cn4pw5hwg30xx5l71ka79dgjdj04k7s1bifh85jfn21qmq") (s 2) (e (quote (("tokio" "dep:tokio" "dep:pin-project-lite"))))))

