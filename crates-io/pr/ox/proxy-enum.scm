(define-module (crates-io pr ox proxy-enum) #:use-module (crates-io))

(define-public crate-proxy-enum-0.1.1 (c (n "proxy-enum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0bkwjpsn3l4ff2g57d6si1mb98jyn365fbkbqx8bszih9jn5skkz")))

(define-public crate-proxy-enum-0.1.2 (c (n "proxy-enum") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1zwbhrgzkq26zqbvmrpwi0valk5k3q3i9a3qr5xkqajng3kkqyl6") (y #t)))

(define-public crate-proxy-enum-0.2.0 (c (n "proxy-enum") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1cxlziiafswp4na241xdcl5n2l7352zhd0dj5pmlibykh8f5lw68")))

(define-public crate-proxy-enum-0.3.0 (c (n "proxy-enum") (v "0.3.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1hk7zc9arzlmqmwqqccndc38pq4k7wp99ycdwi83x6zmdrvhbmg8")))

(define-public crate-proxy-enum-0.3.1 (c (n "proxy-enum") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0wlrwnjs6fyqd45x4xcjgclhf3z3sdbra4ms1r3q553fcw3ndykd")))

