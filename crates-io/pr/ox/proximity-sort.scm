(define-module (crates-io pr ox proximity-sort) #:use-module (crates-io))

(define-public crate-proximity-sort-1.0.0 (c (n "proximity-sort") (v "1.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0j95z0mx2ivx9jlzcsizkx1lwzqdbiirghi0lpbjnzq1smhbrybm")))

(define-public crate-proximity-sort-1.0.1 (c (n "proximity-sort") (v "1.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1gv4384klzni5wxg3z51q8vbcwganm7v8mkfs95v4cm6fik7kkqr")))

(define-public crate-proximity-sort-1.0.2 (c (n "proximity-sort") (v "1.0.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0in37k5gvakn6mbh7qna344q02rz9khk3nc8rn5r7j35ryq7fw4v")))

(define-public crate-proximity-sort-1.0.3 (c (n "proximity-sort") (v "1.0.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0wcasdbzsgvq9jk5lz3bd8gg3bz1qm941qxkqag8jv7y5fvh3vdn")))

(define-public crate-proximity-sort-1.0.4 (c (n "proximity-sort") (v "1.0.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1m5cnm9p94yya82rcjrcbnkg7hkq07arh040vzad23fy47ybg370")))

(define-public crate-proximity-sort-1.0.5 (c (n "proximity-sort") (v "1.0.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0sqnyjlln1wmvyprwrn6jqy8skn5zrjhfz5y117yi6xbbls4nh9j")))

(define-public crate-proximity-sort-1.0.6 (c (n "proximity-sort") (v "1.0.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "03p2lxxgzgrfihbphs5hc76jhx1zpk1b1x4cqyyncm3lhfnvnypj")))

(define-public crate-proximity-sort-1.0.7 (c (n "proximity-sort") (v "1.0.7") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "08a0m6whndnzw5w9aprrzw38nybqdq19cq1hyhf8d8q5j9hl5fzr")))

(define-public crate-proximity-sort-1.1.0 (c (n "proximity-sort") (v "1.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0ndgnfkhxnyqprbamn110b2a3w541nm3igfiaq0xjk07smwlbvh0")))

(define-public crate-proximity-sort-1.2.0 (c (n "proximity-sort") (v "1.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n8g4jjw0bff9vnn4zqh22znvlynp9f84zcc17jli6s8c6zmzcpz")))

(define-public crate-proximity-sort-1.3.0 (c (n "proximity-sort") (v "1.3.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.4") (f (quote ("raw_os_str"))) (k 0)))) (h "1m0ihbfg4gji8d3p39frpfn8c1rrvja5wi2k5b7bihvbgf45w3sa")))

