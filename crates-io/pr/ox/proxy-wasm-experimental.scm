(define-module (crates-io pr ox proxy-wasm-experimental) #:use-module (crates-io))

(define-public crate-proxy-wasm-experimental-0.1.0-alpha.1 (c (n "proxy-wasm-experimental") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1ns54yv54q06451xld3194clq2r92yzlxb2l65lf482113469a4x")))

(define-public crate-proxy-wasm-experimental-0.1.0-alpha.2 (c (n "proxy-wasm-experimental") (v "0.1.0-alpha.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "050fimisg02v2d9ppl674kjgqzp1yywrycj18642q39lgsdjd6lc")))

(define-public crate-proxy-wasm-experimental-0.1.0-alpha.3 (c (n "proxy-wasm-experimental") (v "0.1.0-alpha.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "09blmlgndb6c080979n6y2iw41ds8ixhxlidx68vd154gasl6k8n")))

(define-public crate-proxy-wasm-experimental-0.1.0-alpha.4 (c (n "proxy-wasm-experimental") (v "0.1.0-alpha.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "03jjdpaaf0vl51b5frssyaks5fdcxirpiwl5h8zqy002fmb4f5ql")))

(define-public crate-proxy-wasm-experimental-0.1.0-alpha.5 (c (n "proxy-wasm-experimental") (v "0.1.0-alpha.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0ys7ni70pfqdh6yncm4rkfwcn7vsfjrgx3a47vp9y11lgv78saq9") (y #t)))

(define-public crate-proxy-wasm-experimental-0.0.2 (c (n "proxy-wasm-experimental") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0s89vp1abln6nlw0yhplb86qka9lj2axisknnm5fgcp07mdnlx12")))

(define-public crate-proxy-wasm-experimental-0.0.4 (c (n "proxy-wasm-experimental") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1ghq7jgiwksddgm9bi2x25v068nn3qcsv8wjc47jlf1zc2rwzvpk")))

(define-public crate-proxy-wasm-experimental-0.0.5 (c (n "proxy-wasm-experimental") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1h7jky86vpn9zy5l8imdk6qiqlmq5zffnnq2nnbrcdw35h159r7f")))

(define-public crate-proxy-wasm-experimental-0.0.3 (c (n "proxy-wasm-experimental") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "02llr1ryqqmqxjnqjhm8hc3n93zgdmc1bci6kg34sgkk6ghh9mcq")))

(define-public crate-proxy-wasm-experimental-0.0.6 (c (n "proxy-wasm-experimental") (v "0.0.6") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1qvgqvvqsqyqb8sh7bf0zigg1g4nrnl7x449baw77fg7r1dyj010")))

(define-public crate-proxy-wasm-experimental-0.0.7 (c (n "proxy-wasm-experimental") (v "0.0.7") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1qv0y69bkr4b65alggh9qvnbsrw6bpfc2wmw6jxw46z8vgarr9x6")))

(define-public crate-proxy-wasm-experimental-0.0.8 (c (n "proxy-wasm-experimental") (v "0.0.8") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (t "cfg(not(all(target_arch = \"wasm32\", target_os = \"unknown\")))") (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "01y3pjxg7xl2ylrd0ivh8r96kgm34p1hsikspfxnrwm1bxjj4izs") (f (quote (("wee-alloc" "wee_alloc"))))))

