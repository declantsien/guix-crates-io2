(define-module (crates-io pr ox proxus) #:use-module (crates-io))

(define-public crate-proxus-0.1.1 (c (n "proxus") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-util" "net" "macros" "rt-multi-thread" "time"))) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (k 0)))) (h "1s50mb0rbarkhv6rqb9dshyax3n5w3d45ckjyd99v2ik7352ibzz")))

(define-public crate-proxus-0.1.2 (c (n "proxus") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-util" "net" "macros" "rt-multi-thread" "time"))) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (k 0)))) (h "1b9dkv400i7r6pk2mdycbz57h37wncm1h8np4j65xwsyn01x6im9")))

(define-public crate-proxus-0.1.3 (c (n "proxus") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-util" "net" "macros" "rt-multi-thread" "time"))) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (k 0)))) (h "07y6lqzq2lis5in8i4bbysrp6rj1qvp52jxjk92fxs0k6kzvsxhq")))

