(define-module (crates-io pr ox proxy-llama) #:use-module (crates-io))

(define-public crate-proxy-llama-0.1.0 (c (n "proxy-llama") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "net" "time" "rt-multi-thread" "macros" "tracing"))) (d #t) (k 0)) (d (n "tokio-task-manager") (r "^0.2") (d #t) (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util" "limit"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1hybcvkp8sh5kmn4py45n9yplzzc9fzbff9zj7s4hi0jmpdvpija") (y #t)))

