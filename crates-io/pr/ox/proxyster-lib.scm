(define-module (crates-io pr ox proxyster-lib) #:use-module (crates-io))

(define-public crate-proxyster-lib-0.1.0 (c (n "proxyster-lib") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.149") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1vwkdkbf5zcsy2hyh79f0b11i0569w8g36bvsjw27ja4hw4jbqqg")))

(define-public crate-proxyster-lib-0.1.1 (c (n "proxyster-lib") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.149") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0xhld5q52lmav0jrvnrdk71zpwfjg0jisqmp7w30al4qxrzm7n64")))

