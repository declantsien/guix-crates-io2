(define-module (crates-io pr ox proxy-switcher-tray-applet) #:use-module (crates-io))

(define-public crate-proxy-switcher-tray-applet-0.1.0 (c (n "proxy-switcher-tray-applet") (v "0.1.0") (d (list (d (n "dconf_rs") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libappindicator") (r "^0.4.0") (d #t) (k 0)))) (h "14g5rvvlxiyy5jzppcr1d7bymw99nrr6l85fw3r5v1c1asn23cif")))

