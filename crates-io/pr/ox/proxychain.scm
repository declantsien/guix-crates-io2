(define-module (crates-io pr ox proxychain) #:use-module (crates-io))

(define-public crate-proxychain-0.0.0 (c (n "proxychain") (v "0.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("net" "os-poll"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xmc8x3dyh7m09lv5l6xszhx54yrwz4l38yl1glfylr010d7zaad")))

