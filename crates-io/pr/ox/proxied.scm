(define-module (crates-io pr ox proxied) #:use-module (crates-io))

(define-public crate-proxied-0.1.0 (c (n "proxied") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1x05jdd1igskgbxp9hx8shcglwwi60d3q9cmbigc7270dkjgfah9")))

