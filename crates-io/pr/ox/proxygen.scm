(define-module (crates-io pr ox proxygen) #:use-module (crates-io))

(define-public crate-proxygen-0.1.0 (c (n "proxygen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "1hc2ikylz2gy761cnf6c98zqxvfs8fwdjqjdbj38aswjrxb79had")))

(define-public crate-proxygen-0.2.0 (c (n "proxygen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0k9xaqv5wrfyxh4b008k8r5bjh3yd8y95kvhhbyylljl8v743m4k")))

(define-public crate-proxygen-0.2.1 (c (n "proxygen") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "015qcl97sry99qav4p1b94kbwqaf3pinfc40zim0wfbmjbvp8yq8")))

(define-public crate-proxygen-0.3.0 (c (n "proxygen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0bw43nb2f9ia721lssii0whlz8b2kj16c5xfplkfvz05pdwhq8cm")))

(define-public crate-proxygen-0.3.1 (c (n "proxygen") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0cscmm11q4wz54qxbbmdpf1spm3rparkybx7pml2mcw22mqx9sxa")))

(define-public crate-proxygen-0.4.0 (c (n "proxygen") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "1nphq1lysc6gagir1266j7yr2xci6yss2i17wdkgcjykcrjw544m")))

(define-public crate-proxygen-0.4.1 (c (n "proxygen") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "1rqadnjyfrka92k08j3xvp2v6raq84wik7hq1kg3srz755laybfb")))

(define-public crate-proxygen-0.5.0 (c (n "proxygen") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "1kwlxwcfyn0d6kx97qd6kd1c0m6697a3w8v2sq8kb3rz85l5pdfi")))

(define-public crate-proxygen-0.5.1 (c (n "proxygen") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exe") (r "^0.5.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0xjpi0wq95q4v4df62zwpljm49by032cz0ig9xbb10ayyw8nwvvp")))

