(define-module (crates-io pr av pravega-client-channel) #:use-module (crates-io))

(define-public crate-pravega-client-channel-0.1.0 (c (n "pravega-client-channel") (v "0.1.0") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wa961nh242gw2zxgf0hmqhp99jcpsaclvn9g3zmyya7hd2i947i")))

(define-public crate-pravega-client-channel-0.2.0 (c (n "pravega-client-channel") (v "0.2.0") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmgksvbq55k6r59k1kpma3kfmqaqv3yysdnsr3gibqzzypcf5f9")))

(define-public crate-pravega-client-channel-0.3.1 (c (n "pravega-client-channel") (v "0.3.1") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qf4ick82l9fj8mm7pcayiy6j70851jy0fpag1lcdlwbkgfrw788")))

(define-public crate-pravega-client-channel-0.3.2 (c (n "pravega-client-channel") (v "0.3.2") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v1ydc76416wlhncd4p30skk0w2z475s7965ig2284s01gada984")))

(define-public crate-pravega-client-channel-0.3.5 (c (n "pravega-client-channel") (v "0.3.5") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n006wjw7lmy8vikz97cw0k49ys7zl2frn0fgq58rjziik6dv9mn")))

(define-public crate-pravega-client-channel-0.3.6 (c (n "pravega-client-channel") (v "0.3.6") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xb0cs87377dnwzybgslhm8m9xzph4bw61lw6qqdf88461arqy1h")))

(define-public crate-pravega-client-channel-0.3.7 (c (n "pravega-client-channel") (v "0.3.7") (d (list (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c9dqm9md7nn07lj27yavfw4k9baxgpwizbv9a4nvr1jb9qq8j35")))

