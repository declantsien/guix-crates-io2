(define-module (crates-io pr av pravega-client-retry) #:use-module (crates-io))

(define-public crate-pravega-client-retry-0.1.0 (c (n "pravega-client-retry") (v "0.1.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10ln31dikzgv5xa38dxr2vf6g3gwy98canc0shjnaqrd9cfy4m6m")))

(define-public crate-pravega-client-retry-0.2.0 (c (n "pravega-client-retry") (v "0.2.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "19655lhjlqb911d6pv6cvysrqy5ighycprijix8ps9k48qlzyr5z")))

(define-public crate-pravega-client-retry-0.3.1 (c (n "pravega-client-retry") (v "0.3.1") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xafvva1imzn1mg2k4695l3drl4n1615iasqg3zd8rw2d88ss5ir")))

(define-public crate-pravega-client-retry-0.3.2 (c (n "pravega-client-retry") (v "0.3.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wfmamxr588z7r5v3n0zjy2pk9bracih2sqm05sfblqsl2cw7aip")))

(define-public crate-pravega-client-retry-0.3.4 (c (n "pravega-client-retry") (v "0.3.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nr7dj9nvfy6fav8cfjf8l2ydqhan6s58mh9zqdcqlsyk8iibvzl")))

(define-public crate-pravega-client-retry-0.3.5 (c (n "pravega-client-retry") (v "0.3.5") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i4a0z5ppwb4n5jddy2jlxrnc58lnq83ai84266v8liq35c9hkik")))

(define-public crate-pravega-client-retry-0.3.6 (c (n "pravega-client-retry") (v "0.3.6") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pihz49pnrk9s4ixzr3zyqwxfgix6wyqb9j132mpa6hsflfy7nc2")))

(define-public crate-pravega-client-retry-0.3.7 (c (n "pravega-client-retry") (v "0.3.7") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rkzl2a31rwyxv4sqiccswyxhkjpq54nkbgihcnhkcczn4hjzw3p")))

