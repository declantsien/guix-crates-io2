(define-module (crates-io pr av pravega-client-macros) #:use-module (crates-io))

(define-public crate-pravega-client-macros-0.3.1 (c (n "pravega-client-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i0v399lm6x8l1jpd73zdsyb033mlifrxj5xw21bw76rpfcfndkw")))

(define-public crate-pravega-client-macros-0.3.2 (c (n "pravega-client-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d83cl9bnwxpqax0cl3vg4yw8vq0a47v9bkg2f7md5fzg8zaiv7j")))

(define-public crate-pravega-client-macros-0.3.5 (c (n "pravega-client-macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pwm6fvfdsw40z636a2d8qs16d8vpw1pxkxsxcj5hrqxj18pz6h8")))

(define-public crate-pravega-client-macros-0.3.6 (c (n "pravega-client-macros") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k50azfvbm0v88d600avnnmkaclrk69lm9j0af3mc5x6r02dk9lz")))

(define-public crate-pravega-client-macros-0.3.7 (c (n "pravega-client-macros") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cmvm9y5lhgkxixa6qg9yq97qjlnhgxmpy0h131xm731dn2pimky")))

