(define-module (crates-io pr ec precis-profiles) #:use-module (crates-io))

(define-public crate-precis-profiles-0.1.0 (c (n "precis-profiles") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.0") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.0") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "06rrrd11pdj7ra9i0a3qidiilgcdm3zz8i0kh448ig838v69xyfj")))

(define-public crate-precis-profiles-0.1.1 (c (n "precis-profiles") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.0") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.0") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0ankflw7dd0ai2vsg8z2d5vh6v3z3xc871kn2b1c94lxppi0fcxs") (f (quote (("download_ucd"))))))

(define-public crate-precis-profiles-0.1.2 (c (n "precis-profiles") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.2") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.2") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "17ms96izq7q807dc6qy34pnayz2yag57siv9ivk7zljwg0vdqx47") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.3 (c (n "precis-profiles") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.3") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.3") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1q2dvrydlpyfbhrnhjwyy4fzbsisjxll1fxfr8a76mswnhv3wq50") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.4 (c (n "precis-profiles") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.4") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0rm9w2ag090m71w2kcm53b752m9nllvx5rfw86i3ap1qdv07ywfk") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.5 (c (n "precis-profiles") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.5") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1mglv5aa31b28s9bj5g09x6y1fwp2j4mgw4k0xly78a9p1p5c5g3") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.6 (c (n "precis-profiles") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.6") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "131rya7ycaqsk3w55nd5qrgfx9359kvcjl0k9sqrrx36s2789mnp") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.7 (c (n "precis-profiles") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.7") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.5") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "05sh0r7sk91chna19xg371xxkcbrpywfd4qdljqiqw34jwq6pzpn") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.8 (c (n "precis-profiles") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.8") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.6") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "17yl0wrly03563v9kfk2jn55jn499qq3qbq885yw0r6sl6c4qgq5") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.9 (c (n "precis-profiles") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.9") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.7") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0s0m1vfs6w6p2qw4slayiinwikbmbyxp9dagg5yw64gkdpcj90b8") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

(define-public crate-precis-profiles-0.1.10 (c (n "precis-profiles") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "precis-core") (r "^0.1.9") (d #t) (k 0)) (d (n "precis-tools") (r "^0.1.7") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1m3a0zw9kc5byi6ycq9qhnd64pq8p7dxp8dywamv03664ikbvr5x") (f (quote (("networking" "precis-core/networking" "precis-tools/networking"))))))

