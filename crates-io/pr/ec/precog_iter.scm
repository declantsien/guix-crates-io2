(define-module (crates-io pr ec precog_iter) #:use-module (crates-io))

(define-public crate-precog_iter-0.5.0 (c (n "precog_iter") (v "0.5.0") (h "0mi4gj4q8z7zcmsx4f4hc01w0c5s6j13nkc80hnrr4xvva48d1yj") (y #t)))

(define-public crate-precog_iter-0.5.1 (c (n "precog_iter") (v "0.5.1") (h "0hsj9vm2m467cxpisiw0xlp79iylpyd9dbayqfm4faj4xyv77ii0") (y #t)))

(define-public crate-precog_iter-0.5.2 (c (n "precog_iter") (v "0.5.2") (h "1dfkblyx6dr8l4wp7ah3sas3p0vj8d8k2akdzzzc2y4rnp4zlfp8") (y #t)))

(define-public crate-precog_iter-0.5.3 (c (n "precog_iter") (v "0.5.3") (h "14j766rvsf6w3vnzjymv4hw3r55x8vfidqdmypzn0xcv05ijd137") (y #t)))

