(define-module (crates-io pr ec precrypt) #:use-module (crates-io))

(define-public crate-precrypt-0.1.0 (c (n "precrypt") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "umbral-pre") (r "^0.5.1") (f (quote ("serde-support"))) (d #t) (k 0)))) (h "04vw3srmq20vfqxss43si67ajyrlj1dig5n48bxzhgjrkvhdb8pl")))

(define-public crate-precrypt-0.2.0 (c (n "precrypt") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "umbral-pre") (r "^0.5.1") (f (quote ("serde-support"))) (d #t) (k 0)))) (h "0cb4gn7012f2ffpq8m1qpnndy1yaard0i9n2063gdvrjmx6fg8km")))

(define-public crate-precrypt-0.2.1 (c (n "precrypt") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "umbral-pre") (r "^0.5.1") (f (quote ("serde-support"))) (d #t) (k 0)))) (h "01bxra7inq0zhr7b24cfagxrqfzsr5gvvx52jwlmw7f58k1cxv58")))

(define-public crate-precrypt-0.3.5 (c (n "precrypt") (v "0.3.5") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "umbral-pre") (r "^0.5.1") (f (quote ("serde-support" "bindings-wasm"))) (d #t) (k 0)))) (h "0rap3y3cdnr8n75y0m0ymkyr6m03kzm3f2k7lsnh9wzv50azhgpl")))

