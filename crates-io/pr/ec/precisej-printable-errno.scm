(define-module (crates-io pr ec precisej-printable-errno) #:use-module (crates-io))

(define-public crate-precisej-printable-errno-0.1.0 (c (n "precisej-printable-errno") (v "0.1.0") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "15g2bl1zpbjim5ayc3djdkszflqvs7yv2qrbf9c1gr4jx8azl34h") (y #t)))

(define-public crate-precisej-printable-errno-0.1.1 (c (n "precisej-printable-errno") (v "0.1.1") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1hwdznhp6mzfals0579vjf4ycf0f5xc1zlbbqwl7rfzx2q3n5xgz") (y #t)))

(define-public crate-precisej-printable-errno-0.1.2 (c (n "precisej-printable-errno") (v "0.1.2") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0vwr6h80xlasjm9zc9l5s4xq2nfvvga9r3d1cdwjav1g22ij6kx0") (y #t)))

(define-public crate-precisej-printable-errno-0.2.0 (c (n "precisej-printable-errno") (v "0.2.0") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0x04qp7dzcmsr1hwrkri1jvyl4ir80d0p50i6xdqm43gyh1jdnc4")))

(define-public crate-precisej-printable-errno-0.2.1 (c (n "precisej-printable-errno") (v "0.2.1") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0l5czh1ny202m2nndjdbc72220kphl02s6iz6chgi8mfcfpa2kzq")))

(define-public crate-precisej-printable-errno-0.2.2 (c (n "precisej-printable-errno") (v "0.2.2") (d (list (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0vr1si4pa0af7849py7c7w1x50n2dd2cvzbasmdr3jg3183lh4c8")))

