(define-module (crates-io pr ec precedence-net) #:use-module (crates-io))

(define-public crate-precedence-net-0.8.0 (c (n "precedence-net") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0z1iwkxsfvd7l0rnmxnacn72brpqhvyasxhjb1x9xniqyac2gxcz")))

(define-public crate-precedence-net-1.0.0 (c (n "precedence-net") (v "1.0.0") (d (list (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "04nazb6xcm228vnkxihljm6n4b3w3zs3ywmxg07rd80dqr9fmpqd")))

(define-public crate-precedence-net-1.1.0 (c (n "precedence-net") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0d4y42irh0a8sadlnabcdbgl18i5gllpbl1inf3z7i1dwgppnky1")))

