(define-module (crates-io pr ec precomputed-hash) #:use-module (crates-io))

(define-public crate-precomputed-hash-0.1.0 (c (n "precomputed-hash") (v "0.1.0") (h "0l21314dl14hcb13aj5sy6hzcvk4ihwd4b3zhim75vxk2qvgrwfd")))

(define-public crate-precomputed-hash-0.2.0 (c (n "precomputed-hash") (v "0.2.0") (h "039xa5mr9456bx68v42idlimf2iysslzkcx95gjw9hi5179g74f5") (y #t)))

(define-public crate-precomputed-hash-0.1.1 (c (n "precomputed-hash") (v "0.1.1") (h "075k9bfy39jhs53cb2fpb9klfakx2glxnf28zdw08ws6lgpq6lwj")))

