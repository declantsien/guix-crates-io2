(define-module (crates-io pr ec precise-calc) #:use-module (crates-io))

(define-public crate-precise-calc-0.1.0 (c (n "precise-calc") (v "0.1.0") (d (list (d (n "astro-float") (r "^0.6.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cnwgpvjqlqfn5ravmgj8lj2nzkmdyq8cjmpqqkiargq2q99v86w")))

(define-public crate-precise-calc-0.1.1 (c (n "precise-calc") (v "0.1.1") (d (list (d (n "astro-float") (r "^0.6.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rlq1swysf5k6ifrjwpfajr00yk0kqxj8jl4k29s56vbrkplacm4")))

(define-public crate-precise-calc-0.1.2 (c (n "precise-calc") (v "0.1.2") (d (list (d (n "astro-float") (r "^0.6.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0aww76rjm78p4hf113s6px1q3073y2frssa0rxdg0rv97yprwhfj")))

