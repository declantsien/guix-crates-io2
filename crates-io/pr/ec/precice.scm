(define-module (crates-io pr ec precice) #:use-module (crates-io))

(define-public crate-precice-2.5.0 (c (n "precice") (v "2.5.0") (d (list (d (n "clap") (r "^2.9") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "14ax61irvxvxl444yaxlhwyiwpys50503iw4hmjyrwnzgv7izhyy") (l "precice")))

(define-public crate-precice-3.0.0 (c (n "precice") (v "3.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "10a32vp56ss1bazr4q7vm9ww8lrizm0cvimfks52hbs8pkwy2lry") (l "precice")))

(define-public crate-precice-3.0.1 (c (n "precice") (v "3.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "0plqsn8il7j9jcg3spnpvsp7gc0chl8hx8m3vx5w9yj9vac55mas") (l "precice")))

(define-public crate-precice-3.1.0 (c (n "precice") (v "3.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "048q283hvqbsrydyd9bvjmvwcpx1hjbi91shh9xl52wajmd3rpwj") (l "precice")))

