(define-module (crates-io pr ec precious-exec) #:use-module (crates-io))

(define-public crate-precious-exec-0.3.0 (c (n "precious-exec") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r ">=3.0.0, <5.0.0") (d #t) (k 0)))) (h "1ybq7mf3bj4h2cx0b0d0w36xqwh7zx37yw9mwma314afd1ba0bcx")))

