(define-module (crates-io pr ec precise) #:use-module (crates-io))

(define-public crate-precise-0.1.0 (c (n "precise") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07x6xk4w1pv4z9dh460z8vafifkwx7pqf94kh44p89nllvqiqp2s")))

(define-public crate-precise-0.1.1 (c (n "precise") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bz24d1ka77j2i3lwp04zxh37b73a5b52ji3khcds9cm90v5zd1y")))

(define-public crate-precise-0.1.2 (c (n "precise") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ndsbs8x9srj2g5pcshfyl6yh7jc9n5gkfqjyqrcg10mzzblnwjq") (r "1.31")))

(define-public crate-precise-0.1.3 (c (n "precise") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1s1mdpcpswyypscckng8cq98xshxi5ysw2hr0k2asc1kj5q0fjwd") (r "1.31")))

(define-public crate-precise-0.1.4 (c (n "precise") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vphyr3zayy33gdzr8fa2bw4kbwr8w4qzrlghfvmf1vrnymn5w8h") (r "1.31")))

(define-public crate-precise-0.1.5 (c (n "precise") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lqd49l36smx33srkfwxmw8fn8b9fvh4vsvr52hfa8x7gpwm64r6") (r "1.31")))

(define-public crate-precise-0.1.6 (c (n "precise") (v "0.1.6") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0s82q7rk30s8jhwrrb7fqs8f5icjkssz6v5rg991xx9hi0rxwp1k") (r "1.31")))

(define-public crate-precise-0.1.7 (c (n "precise") (v "0.1.7") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bnax88inmzihynzc697g5pn7wqyx4cvd2fng4lnlajbsyyikcdd") (r "1.31")))

(define-public crate-precise-0.1.8 (c (n "precise") (v "0.1.8") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fcav9viyicszbwcdd0cfixbg7j4s464wksf8sxx8lxjp1kr158v") (r "1.31")))

(define-public crate-precise-0.1.9 (c (n "precise") (v "0.1.9") (d (list (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11878g0g9z8hg5ywy4z2af1r2sjbjsrmzknzqfxjbns62yp7a958") (r "1.31")))

