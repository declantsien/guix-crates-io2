(define-module (crates-io pr ec prec) #:use-module (crates-io))

(define-public crate-prec-0.1.0 (c (n "prec") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0d2jxjiby73ldzai08nc08z4car53b0d8smqkx35qxaw2pp8nibv")))

(define-public crate-prec-0.1.1 (c (n "prec") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "07dd8w84nrf8j0vv2mrfswkwnchjrdllv5ghqn6kvjmhaz28qlb4")))

(define-public crate-prec-1.0.0 (c (n "prec") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "19p9973ddnhhczad93zhxn4z29wjm6y4g36wicbjqm2wi2r3rqb2")))

(define-public crate-prec-1.1.0 (c (n "prec") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "17vda1h2sm37mr8gzhr3gq3sdis08lq25a165s0i947vbri84kcz")))

