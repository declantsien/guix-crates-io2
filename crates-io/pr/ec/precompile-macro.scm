(define-module (crates-io pr ec precompile-macro) #:use-module (crates-io))

(define-public crate-precompile-macro-0.1.0 (c (n "precompile-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9fipqk781asdsqrhgzba1bb5mxjl5dwkmwnqqjvmb1wgby1vid")))

