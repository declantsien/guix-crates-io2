(define-module (crates-io pr ec precis-tools) #:use-module (crates-io))

(define-public crate-precis-tools-0.1.0 (c (n "precis-tools") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "0xxxpy1k6407xkrqk3didb0xgilf8hmsnr1xzl79214p2zn2kn73")))

(define-public crate-precis-tools-0.1.1 (c (n "precis-tools") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "0jixm7pl1vp60z9s7khxyn9zj6dhwni33xzb5y440pl6l03y9rj5")))

(define-public crate-precis-tools-0.1.2 (c (n "precis-tools") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "0xgh1iaamajdcg415f5sqy6sgcc56pbscj8s1jbh98qa1q6pwrm4") (f (quote (("networking" "reqwest"))))))

(define-public crate-precis-tools-0.1.3 (c (n "precis-tools") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "1fjw4dlnrrpg4zfgyca7bcff8jpknrqimj3xrpn583r6yv362hhr") (f (quote (("networking" "reqwest"))))))

(define-public crate-precis-tools-0.1.4 (c (n "precis-tools") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "1dmmgngwv2jvl36lbry9v7wd0ggymh8rn9bs77z7vharjq29bmkc") (f (quote (("networking" "reqwest"))))))

(define-public crate-precis-tools-0.1.5 (c (n "precis-tools") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "008vwm50y0l6fb04px3mpdlpchjc6qjpw0mrsx4zfpksf8v9ldds") (f (quote (("networking" "reqwest"))))))

(define-public crate-precis-tools-0.1.6 (c (n "precis-tools") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "01176ayhclz1bfvb0x4n970zlgszlvjm70a0534yp7s61x90az9i") (f (quote (("networking" "reqwest"))))))

(define-public crate-precis-tools-0.1.7 (c (n "precis-tools") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ucd-parse") (r "^0.1.8") (d #t) (k 0)))) (h "0ljzkxyaqabjp8wcjm13ip9l1ag11sp1by5b147mc3qbqzgclznh") (f (quote (("networking" "reqwest"))))))

