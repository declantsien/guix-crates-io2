(define-module (crates-io pr ec precision) #:use-module (crates-io))

(define-public crate-precision-0.1.0 (c (n "precision") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rs2qlh9l97lwhqvf8dgbr5blzf1qf4bk0pln86snp5fp111an1w")))

(define-public crate-precision-0.1.1 (c (n "precision") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1vrnah8jp03kw332gfcgci1fima6ahy36hc3xsn6mgm1gyq3mii8")))

(define-public crate-precision-0.1.2 (c (n "precision") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "08h7vvk6wcjwzv6w43kfsp2ny07gjx639q8a7ly7p14rmmsdcx93")))

(define-public crate-precision-0.1.3 (c (n "precision") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0y4hwjhg9k9py6497rnjzn4h8x45qqwxghw86lilj9f0zrxad205")))

(define-public crate-precision-0.1.4 (c (n "precision") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0nqky3dizv2d4l0jc82asa21vdkffa58dy98lqykgzs02pl6r9cp")))

(define-public crate-precision-0.1.5 (c (n "precision") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0318gld4g0c9jp36fhm1g4qf0la9a05jnlf8dk6bzfn5bpgggx2y")))

(define-public crate-precision-0.1.6 (c (n "precision") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "11bdz3b7ay9izv77wi7x2af9cr65qdxnbmlf9f5fngp1x2z18chv")))

(define-public crate-precision-0.1.7 (c (n "precision") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0giz33wi22z6qgf23ihkffgis5hzzjh7gg02aiirg1wc3spw22pb")))

(define-public crate-precision-0.1.8 (c (n "precision") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1y3nc9dbhbxgwb2b6cgdvgxvfjkf15qyv3pm3yyravychrckkv33")))

(define-public crate-precision-0.1.9 (c (n "precision") (v "0.1.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0mbxavgbqyn4bw298nq6xcnjf9ff162z5x044qwddvg9j3gb7w43")))

(define-public crate-precision-0.1.10 (c (n "precision") (v "0.1.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1y924hjylwmwbw8ak5x176z5751i2n4z4b520dnj11xxrm4zp3z4")))

(define-public crate-precision-0.1.11 (c (n "precision") (v "0.1.11") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1k4xkbgpgzm9rd387kh7r2cbs25rrxxj2w89qqlprpw5arvhz01x")))

(define-public crate-precision-0.1.12 (c (n "precision") (v "0.1.12") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi") (r "^0.10.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "1achj24sq50n4nmrdpc1n5mkxk8yh0srass1kggbdik0hidifq2f")))

(define-public crate-precision-0.1.13 (c (n "precision") (v "0.1.13") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi") (r "^0.10.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "18dnsvwpxvhadrx94izyy8kbdq1hf6yvmfg0g5nw1s28ly4bivla")))

(define-public crate-precision-0.1.14 (c (n "precision") (v "0.1.14") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "0b8wg6i7mccp1mng202l8ijg1mrxwpr302j3j67s2bmh7zq3pl80")))

(define-public crate-precision-0.1.15 (c (n "precision") (v "0.1.15") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "0qws0bj0zzy1vk01hrvi6q5chl9in34jsagyh8ny0lwl6v44zj5v")))

(define-public crate-precision-0.1.16 (c (n "precision") (v "0.1.16") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "14qrxq4ax07vpnwc9ndc518hv94axy56mrgfn3p5crpfip1prbkq")))

(define-public crate-precision-0.1.17 (c (n "precision") (v "0.1.17") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (t "cfg(not(any(target_arch = \"wasm32\", target_arch = \"wasm64\")))") (k 1)) (d (n "wasi-abi2") (r "^0.12.1") (o #t) (d #t) (t "cfg(target_os = \"wasi\")") (k 0) (p "wasi")) (d (n "wasix") (r "^0.12") (d #t) (t "cfg(any(target_os = \"wasix\", target_os = \"wasi\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "144nnvklq6l5qvci8820zljzk09cywqx3g50jq7jfnvsgk7gwlv7") (s 2) (e (quote (("wasi-abi2" "dep:wasi-abi2"))))))

