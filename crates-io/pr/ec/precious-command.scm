(define-module (crates-io pr ec precious-command) #:use-module (crates-io))

(define-public crate-precious-command-0.2.1 (c (n "precious-command") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r ">=3.0.0, <5.0.0") (d #t) (k 0)))) (h "1s6fbmn1idjsp0a53yn9z23c6lhjcw3k0sjgv7188jfgkqampqyn")))

(define-public crate-precious-command-0.2.2 (c (n "precious-command") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r ">=3.0.0, <5.0.0") (d #t) (k 0)))) (h "1l5fn1a9kd748i7csbibf6gzh5ihjd12h5n8ziy9znl9iyhqal2r")))

(define-public crate-precious-command-0.2.3 (c (n "precious-command") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "which") (r ">=3.0.0, <5.0.0") (d #t) (k 0)))) (h "0bbf0ayf5iw39y87irhrd3m5cwh8sc98rfwr5rss125s87m5m0sr")))

