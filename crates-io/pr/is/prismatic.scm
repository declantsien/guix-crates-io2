(define-module (crates-io pr is prismatic) #:use-module (crates-io))

(define-public crate-prismatic-0.1.0 (c (n "prismatic") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0kk8ijmniklkzasy2bw230w10sbawm41v9km588fn3yk6hvnq5k6")))

(define-public crate-prismatic-0.2.0 (c (n "prismatic") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "08ygv961ks07lahgcrv3d7gzbpcm9mkk8lv5h3mlvp8m3c1acjrh")))

