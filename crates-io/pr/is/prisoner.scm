(define-module (crates-io pr is prisoner) #:use-module (crates-io))

(define-public crate-prisoner-0.3.0 (c (n "prisoner") (v "0.3.0") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "11c2w31nvx0as49v6nxlbdl4maaf0v37mq4cca99fbi1010fsybf")))

(define-public crate-prisoner-0.4.0 (c (n "prisoner") (v "0.4.0") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "02ddf656v4ndf1j6zyby7sllr2wczng2al969d18l4djq8n8w9lf")))

(define-public crate-prisoner-0.5.0 (c (n "prisoner") (v "0.5.0") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "1y3ld03g1qdy2j2sh9shrycpcb2gbc33l7v61fr5myaahwcrqjim")))

(define-public crate-prisoner-0.6.0 (c (n "prisoner") (v "0.6.0") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "1105as16lda9vlbxcnn3nyh7axll0ybs2hh5n6ir2vr5j4aggk1h")))

(define-public crate-prisoner-0.6.1 (c (n "prisoner") (v "0.6.1") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "0lwx7cvsjpm8d2sydkm77cd5kr98hrjhk6zsl2bqlfryrdl6d3pj")))

(define-public crate-prisoner-0.6.2 (c (n "prisoner") (v "0.6.2") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "084h09xb36wxrsdxgw1jfab56bjh9hqf4fn7ypz030blfag4ahfk")))

(define-public crate-prisoner-0.6.3 (c (n "prisoner") (v "0.6.3") (d (list (d (n "block-id") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "04b03w8r0m9jmy70sz8qxprwim7l0l3dzpp5xr8mxpapz6g6vd4p")))

