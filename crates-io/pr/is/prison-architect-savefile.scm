(define-module (crates-io pr is prison-architect-savefile) #:use-module (crates-io))

(define-public crate-prison-architect-savefile-0.1.0 (c (n "prison-architect-savefile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)))) (h "00mqm4bif4scwbybf7maks7x2naaddgr132ds1j9i92a3n0fp1f1")))

(define-public crate-prison-architect-savefile-0.2.0 (c (n "prison-architect-savefile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)))) (h "1hrgdb7f89a9ab7ky12p7q0zsl1h5jw35jp8bpc118afba3w5wkj")))

