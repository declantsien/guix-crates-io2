(define-module (crates-io pr is prisma) #:use-module (crates-io))

(define-public crate-prisma-0.1.0 (c (n "prisma") (v "0.1.0") (d (list (d (n "angular-units") (r "^0.2.4") (d #t) (k 0)) (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rhk5pqfv8yskdz58c1b1r66kc0cvqxp9mpmxd5pm0s2gb5r5nr4") (f (quote (("default" "approx"))))))

(define-public crate-prisma-0.1.1 (c (n "prisma") (v "0.1.1") (d (list (d (n "angular-units") (r "^0.2.4") (d #t) (k 0)) (d (n "approx") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "159l4jz0g8vq4r5c0vpkf6ddp7kqsci5xwbf2l06jrbb24rhar7m") (f (quote (("default" "approx"))))))

