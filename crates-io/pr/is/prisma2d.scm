(define-module (crates-io pr is prisma2d) #:use-module (crates-io))

(define-public crate-prisma2d-0.1.0 (c (n "prisma2d") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pixels") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 2)))) (h "18jijsbsyn841j038f6hzjk5azlmav16f0y49vndjcmcjlr6wi17") (f (quote (("transparency") ("threading" "rayon") ("nostd") ("images") ("default" "nostd" "transparency" "aggr_inline") ("aggr_inline"))))))

