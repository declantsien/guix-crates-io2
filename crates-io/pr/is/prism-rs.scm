(define-module (crates-io pr is prism-rs) #:use-module (crates-io))

(define-public crate-prism-rs-0.1.0 (c (n "prism-rs") (v "0.1.0") (d (list (d (n "ansi-to-tui") (r "^2.0.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "signal-child") (r "^1.0.5") (d #t) (k 0)) (d (n "timeout-readwrite") (r "^0.3.2") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "06i2wnnl3y9ak6nxjgrjk9xm3nsm9wd0cdi11464zxws7412bnyz")))

