(define-module (crates-io pr is prism-wasmbind) #:use-module (crates-io))

(define-public crate-prism-wasmbind-0.1.0 (c (n "prism-wasmbind") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "1myx6famkn4rdjjpg19q8f4mqmc545h16p6z4w3jrcqsbzvzkcjx")))

(define-public crate-prism-wasmbind-0.2.0 (c (n "prism-wasmbind") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "1jqhqv22zfmny4zmd5fqvmvlslwr282hvpmzm005dsbdvj5mg01f")))

(define-public crate-prism-wasmbind-0.2.1 (c (n "prism-wasmbind") (v "0.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "098wjk778gr14daklrack85rr4gmwzjfsymz1hpdnp2apyr6jiac")))

