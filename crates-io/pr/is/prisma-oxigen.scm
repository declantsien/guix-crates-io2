(define-module (crates-io pr is prisma-oxigen) #:use-module (crates-io))

(define-public crate-prisma-oxigen-1.0.0 (c (n "prisma-oxigen") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0jw3lb8k1vg9smv780xq1wpwff5cnsbdpmbdc79ff5ndzai9dhln")))

