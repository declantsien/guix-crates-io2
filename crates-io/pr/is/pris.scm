(define-module (crates-io pr is pris) #:use-module (crates-io))

(define-public crate-pris-0.1.0 (c (n "pris") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros" "signal"))) (d #t) (k 0)))) (h "11kk4axkahk0cdj9y0r3wqqj8n8k47c3vyj57z5jvj46z9a5crxs")))

(define-public crate-pris-0.1.1 (c (n "pris") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros" "signal"))) (d #t) (k 0)))) (h "077zn9fd8i6dhjs9pkdg3p5x5m1gpl5lbg1ql8szbxyvkcy4kksr")))

