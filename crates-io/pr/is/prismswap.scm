(define-module (crates-io pr is prismswap) #:use-module (crates-io))

(define-public crate-prismswap-1.0.1 (c (n "prismswap") (v "1.0.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-asset") (r "^0.3.4") (d #t) (k 0)) (d (n "cw20") (r "^0.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)))) (h "186cfffk8zkjdr1cn3d8zk34r5hnnlnb4vlrl4q4vrdmfsh6ypfq")))

