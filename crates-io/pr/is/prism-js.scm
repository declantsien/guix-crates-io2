(define-module (crates-io pr is prism-js) #:use-module (crates-io))

(define-public crate-prism-js-0.1.0 (c (n "prism-js") (v "0.1.0") (d (list (d (n "quick-js") (r "^0.4.1") (d #t) (k 0)))) (h "0l1b21phx54sz0rm2ws9900khq6wcavf33fqnm3a3gv2ncam4v0w")))

(define-public crate-prism-js-0.1.1 (c (n "prism-js") (v "0.1.1") (d (list (d (n "quick-js") (r "^0.4.1") (d #t) (k 0)))) (h "0xx6f9x3k8y7j4krm7y908mdg9qsqdsj4gb617hz9p275iwy7fpd")))

(define-public crate-prism-js-0.1.2 (c (n "prism-js") (v "0.1.2") (d (list (d (n "quick-js") (r "^0.4.1") (d #t) (k 0)))) (h "1nx79k1jf941wx68wlaqqdmxb90pziyp06d8pw8id7fwjsfkdgv2")))

