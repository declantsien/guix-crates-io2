(define-module (crates-io pr ae prae) #:use-module (crates-io))

(define-public crate-prae-0.2.1 (c (n "prae") (v "0.2.1") (d (list (d (n "prae_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1rx4l0aw0sdvwjkzfp815f5lbvamsmw1d2s34l69dpiqwvcimk8i")))

(define-public crate-prae-0.3.0 (c (n "prae") (v "0.3.0") (d (list (d (n "prae_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "02cxlx42ymnfmvx2iz2mj138wxcxji56gr9d6nc846lndzma2ki2")))

(define-public crate-prae-0.3.1 (c (n "prae") (v "0.3.1") (d (list (d (n "prae_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0j1ycmx6qyb7l40yhqn70yg85l5szyabkvwhmkl1b2a47yy1lng7")))

(define-public crate-prae-0.3.2 (c (n "prae") (v "0.3.2") (d (list (d (n "prae_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1jzl12qgipd3jg68dphx2s1hz6r2c93jnjvqkbrz6wm6py5jz7pa")))

(define-public crate-prae-0.4.0 (c (n "prae") (v "0.4.0") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0m2p517kmrsxrz3wl909rp0lmb4w9lggib2ssbbrjab6fj07xkyf")))

(define-public crate-prae-0.4.1 (c (n "prae") (v "0.4.1") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "11czr9xh0qp0hr0h0rcmcick6lnd4faqwqjj65vsm5fjydkazk3m")))

(define-public crate-prae-0.4.2 (c (n "prae") (v "0.4.2") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1g7sn6hja74l1cb8m4pz0didghp52n4q32mr4al8bwfas89m1wyc")))

(define-public crate-prae-0.4.3 (c (n "prae") (v "0.4.3") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1i2pcc2mlssxzrcql4g34gkygwfp0xw5gy8jcb3gxfkr73mayabv")))

(define-public crate-prae-0.4.4 (c (n "prae") (v "0.4.4") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xxk57ccwzfrvmjdsrmfjqfjgd56k9jmwzl9rf0vq4wybhxq6mba")))

(define-public crate-prae-0.4.5 (c (n "prae") (v "0.4.5") (d (list (d (n "prae_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "16h43ss9l1sb4vbis0kf5fil6ys6s6xyv27xd6xvzmfyp4xyb8h9")))

(define-public crate-prae-0.5.0 (c (n "prae") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zmvakws1imhxa4inrx7qgl8f5liaxi6754pq43zhnzb8rvlfirx")))

(define-public crate-prae-0.5.1 (c (n "prae") (v "0.5.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00c0rdsgwi2la9slsp774k5mg2jzz4klphrmbs5aiw9c8mv3qfq1")))

(define-public crate-prae-0.6.0 (c (n "prae") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gg2y7lwjf5zryfk2z18hrmia2yxw2qavbwbidqzr5svbajlz0wd") (f (quote (("unchecked") ("default"))))))

(define-public crate-prae-0.7.0 (c (n "prae") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11xq4d4nzc0k24kd7jcy5ndanvms54cipw2bvrpv5kkc5w7099qg") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.7.1 (c (n "prae") (v "0.7.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wkkv2c4sp1nngl53b9ppcxrdxar4dgzyfqniy7cwq6m6dqgl5pf") (f (quote (("unprocessed") ("default")))) (y #t)))

(define-public crate-prae-0.7.2 (c (n "prae") (v "0.7.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rqwzmywxh2rhhlh5b257vjgx6djc1fky9nhxakcas8lfm1m0a6a") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.7.3 (c (n "prae") (v "0.7.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m9p2grww8dhvz3rcw5sfxyl2l4h12shqwycm4z25jwd6kcl6qgq") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.7.4 (c (n "prae") (v "0.7.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "prae_macro") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16gp0lrgn591j0n2qlqy3wjphl2msnfkyq9wj288j09lzawywdhr") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.8.0 (c (n "prae") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g3nyhpcpq42aq8xnl48pmcvz39q4vv4pw7ixg9q44j67cmabq2l") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.8.1 (c (n "prae") (v "0.8.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qhr673dmrg8qhk69z6ck8iz0pi76091sv1di4ycafc5a0xgl3g8") (f (quote (("unprocessed") ("default"))))))

(define-public crate-prae-0.8.2 (c (n "prae") (v "0.8.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11d3m7ih34rcldw3kvphzfjv4qspbdlfc2wac37aw43li2rnh1wl") (f (quote (("default"))))))

(define-public crate-prae-0.8.3 (c (n "prae") (v "0.8.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s4xiv29zaraicwf0qcxdrqim85zqkqsqy6ds19rkw1gb4limxy4") (f (quote (("default"))))))

(define-public crate-prae-0.8.4 (c (n "prae") (v "0.8.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0izzfgfi9l05hw59k1n1xp85craqqsj771rqa2mvj4wq43v7yq6p") (f (quote (("default"))))))

