(define-module (crates-io pr ae prae_macro) #:use-module (crates-io))

(define-public crate-prae_macro-0.1.0 (c (n "prae_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09kqwz1fdksagkyq1q7a8jaxlyybzsqdd0qwjh58c3228y4s4q3j")))

(define-public crate-prae_macro-0.2.0 (c (n "prae_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w4gr8qhnay422nbd3svnkjmkwdxrh833qacz3lvbf4i40nhmppd")))

(define-public crate-prae_macro-0.2.1 (c (n "prae_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1czq4bzlhc2k6zfyawaj9wg9ih35izbqpj9bbnizs108nfc409xi")))

(define-public crate-prae_macro-0.3.0 (c (n "prae_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xd49yvx390gh6xmibwixdw6vxn1sflsvvvi6jcw8614fiqzlnz4")))

(define-public crate-prae_macro-0.4.0 (c (n "prae_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05h1vr92z1w57na1kxfgps7lp7qvmnwcy1ln6qx3nrklhms9hmzh")))

(define-public crate-prae_macro-0.5.0 (c (n "prae_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03rb2vigxciph5wykxpbwqs12wgj51c028fri7d5x6mymb4rv3x6")))

(define-public crate-prae_macro-0.5.1 (c (n "prae_macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h9k3337ymnl2x784rwhhdibn530ffj15wxnpihn6grflms66a5j")))

