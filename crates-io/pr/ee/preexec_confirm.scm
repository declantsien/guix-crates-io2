(define-module (crates-io pr ee preexec_confirm) #:use-module (crates-io))

(define-public crate-preexec_confirm-0.1.0 (c (n "preexec_confirm") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05val4pqdldcz1ag41m6ddvz8ws11xl5bcjm992r7zq8wxk1cv40")))

(define-public crate-preexec_confirm-0.1.1 (c (n "preexec_confirm") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10vaqghzbhbr76jmz4q6prmk9x5l7mf6jcd3macfpa31i1rbrf4h")))

(define-public crate-preexec_confirm-0.1.2 (c (n "preexec_confirm") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0m0d55nxnj5771dkzb1w5cbvam42ln7mr9izpnk2qkci83lymgy7")))

