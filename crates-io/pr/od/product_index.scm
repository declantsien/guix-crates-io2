(define-module (crates-io pr od product_index) #:use-module (crates-io))

(define-public crate-product_index-0.1.0 (c (n "product_index") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09y1fi3f7l6yn3h95wgpv8mn1b06y7y2k36ygbib6n745kqwcsgy")))

