(define-module (crates-io pr od product-os-net) #:use-module (crates-io))

(define-public crate-product-os-net-0.0.1 (c (n "product-os-net") (v "0.0.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "no-std-net") (r "^0.6.0") (d #t) (k 0)))) (h "1mks0abjrkzf0i8q5gw0qfrqrpib8is24gvivz96rf4zzd0igqvn") (f (quote (("std" "no-std-compat/std") ("default" "std")))) (r "1.69")))

