(define-module (crates-io pr od product-os-cursor) #:use-module (crates-io))

(define-public crate-product-os-cursor-0.0.1 (c (n "product-os-cursor") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (o #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0zlxi7f56lmf0dkqwkf6zs3p0x86k1sdy1fdn4shy4zsx56afpgx") (f (quote (("stream") ("std") ("default" "std" "byteorder")))) (s 2) (e (quote (("byteorder" "dep:byteorder")))) (r "1.69")))

