(define-module (crates-io pr od product-os-urlencoding) #:use-module (crates-io))

(define-public crate-product-os-urlencoding-0.0.1 (c (n "product-os-urlencoding") (v "0.0.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0aia195j1br0l03n5kscx0r9hs07gjzpsijfas2rb1l25l9g5fk5") (f (quote (("std") ("default" "std")))) (r "1.69")))

