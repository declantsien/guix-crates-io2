(define-module (crates-io pr od product-os-monitoring) #:use-module (crates-io))

(define-public crate-product-os-monitoring-0.0.1 (c (n "product-os-monitoring") (v "0.0.1") (d (list (d (n "psutil") (r "^3.2.1") (f (quote ("process" "memory"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1mqg8y7dailkg5vv946anhv6l4q98bzx72n7c278xx063nj9871v") (r "1.69")))

(define-public crate-product-os-monitoring-0.0.2 (c (n "product-os-monitoring") (v "0.0.2") (d (list (d (n "psutil") (r "^3.2.1") (f (quote ("process" "memory"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "13c46d81g5hxhgmdwsa0kjpxqvbwvl78n4lay1kgsq4n4mgxdvgp") (r "1.69")))

(define-public crate-product-os-monitoring-0.0.3 (c (n "product-os-monitoring") (v "0.0.3") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "psutil") (r "^3.2.1") (f (quote ("process" "memory"))) (k 0)) (d (n "tracing") (r "^0.1.29") (k 0)))) (h "1r33xw6hfbj51wgxyjxkcsklb6yf14frbsd454fci2285iyzqc69") (f (quote (("default" "no-std-compat/std")))) (r "1.69")))

