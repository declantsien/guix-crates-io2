(define-module (crates-io pr od product-os-http) #:use-module (crates-io))

(define-public crate-product-os-http-0.0.1 (c (n "product-os-http") (v "0.0.1") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "05qh841p68kmkhb81nmfwgmx54vrrbpf21bbgw2rr3nn7fh59shr") (f (quote (("std") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-http-0.0.2 (c (n "product-os-http") (v "0.0.2") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1yijwfjh17d5rw9fjhisp8bwqbdmp1gb8gsva1kx3gqmnhlnv4kh") (f (quote (("std") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-http-0.0.3 (c (n "product-os-http") (v "0.0.3") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0isd2jlggsxgsp0g1296whx742xpwnll77kc3hzkrjz62m36j81m") (f (quote (("std") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-http-0.0.5 (c (n "product-os-http") (v "0.0.5") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "http") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0mcama3jgz4a3bc2n821ca61mjc9in8bpxpshpi6m1sx22z802nh") (f (quote (("std" "no-std-compat/std" "http") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-http-0.0.6 (c (n "product-os-http") (v "0.0.6") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "http") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1dpy075dxfmmskh4xxwwk5ymjlr02csvyhg7z7zkw2ywzk5wjsrb") (f (quote (("std" "no-std-compat/std" "http") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-http-0.0.7 (c (n "product-os-http") (v "0.0.7") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "http") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1hh1279lcwc31r69dy3cpzrm5w06hkf2iqa3zd8w1gkryxjlgpkx") (f (quote (("std" "no-std-compat/std" "http") ("default" "std")))) (r "1.69")))

