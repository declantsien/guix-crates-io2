(define-module (crates-io pr od product-os-semaphore) #:use-module (crates-io))

(define-public crate-product-os-semaphore-0.0.1 (c (n "product-os-semaphore") (v "0.0.1") (d (list (d (n "core-error") (r "^0.0.1-rc4") (k 0)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "17vqy47v2bk477lwjdq18yj4q292qnlwlz3i29p0v6xv1siab7mp") (f (quote (("wrapper") ("std") ("nightly") ("default" "std")))) (r "1.69")))

