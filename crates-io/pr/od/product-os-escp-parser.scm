(define-module (crates-io pr od product-os-escp-parser) #:use-module (crates-io))

(define-public crate-product-os-escp-parser-0.0.1 (c (n "product-os-escp-parser") (v "0.0.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (o #t) (k 0)))) (h "018ki1vr1666594qbr7bxzwqi2ip5592lrypwjvy1w3c1rjl2812") (f (quote (("default" "no-std-compat/std")))) (y #t) (r "1.69")))

