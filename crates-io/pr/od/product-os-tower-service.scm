(define-module (crates-io pr od product-os-tower-service) #:use-module (crates-io))

(define-public crate-product-os-tower-service-0.0.1 (c (n "product-os-tower-service") (v "0.0.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0vnrbyckjjhy7fx6grnmf65mvsxn5jk83pd19qvq1lc1yvqlyssn") (f (quote (("std") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-tower-service-0.0.2 (c (n "product-os-tower-service") (v "0.0.2") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (o #t) (k 0)))) (h "0kdh7j1z4i9c6qy8lw1d0gsjs0ksvrrgg4fykmbi038cl87fr7w9") (f (quote (("std" "no-std-compat/std" "tower-service") ("default" "std")))) (r "1.69")))

