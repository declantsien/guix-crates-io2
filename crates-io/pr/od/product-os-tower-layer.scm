(define-module (crates-io pr od product-os-tower-layer) #:use-module (crates-io))

(define-public crate-product-os-tower-layer-0.0.1 (c (n "product-os-tower-layer") (v "0.0.1") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1dj4ypiingg8d2fzbads75c1givi4alrd687vah0mvv8lw1lmas5") (f (quote (("std") ("default" "std")))) (r "1.69")))

(define-public crate-product-os-tower-layer-0.0.2 (c (n "product-os-tower-layer") (v "0.0.2") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.2") (o #t) (k 0)))) (h "0ar78ry1f142wdw5rfka2anrg4sq98bfjzb0gvpnfbxcdndihgrh") (f (quote (("std" "no-std-compat/std" "tower-layer") ("default" "std")))) (r "1.69")))

