(define-module (crates-io pr ei prei) #:use-module (crates-io))

(define-public crate-prei-0.1.0 (c (n "prei") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "13v340m5dyc87ayahaah6nbd7ypqd9l34qxpbbasdc4zngs231qx")))

(define-public crate-prei-0.1.1 (c (n "prei") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0na6rv506ipkq4yb60i1p5f600i7v8jysidbnlsapbzaiyrza5xq")))

(define-public crate-prei-0.1.2 (c (n "prei") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1v2w2wrv0gf6gz6v1lx6zbsva3m8fvzs83kiw9biv9iizgp3kkjc")))

(define-public crate-prei-0.1.3 (c (n "prei") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1a42npva1x4qqafm2l4gxr4ksfg7k0y0136fzsaf9bcq7w7n9y8p")))

(define-public crate-prei-0.1.4 (c (n "prei") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0ar7wk7qbcsh5js7x968vvqpk6r3jdr13wj9r40qbkkxvzcw13lv")))

(define-public crate-prei-0.1.5 (c (n "prei") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1y04nwqbivgmscaz7dn37fnzz6wln101g90cf0b0p5m0l7akz8hh")))

