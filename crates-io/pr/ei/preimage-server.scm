(define-module (crates-io pr ei preimage-server) #:use-module (crates-io))

(define-public crate-preimage-server-0.1.0 (c (n "preimage-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros" "fs" "io-util" "signal" "sync" "time"))) (d #t) (k 0)))) (h "0s11hp795xk862gpwkz4b09k79ipjahafi6pgjfjv155ragwg7l2")))

