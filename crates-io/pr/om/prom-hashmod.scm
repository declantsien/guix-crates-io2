(define-module (crates-io pr om prom-hashmod) #:use-module (crates-io))

(define-public crate-prom-hashmod-0.1.0 (c (n "prom-hashmod") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "19lzhbljnrb8i66981xxlaza3vgzhcwbrna6a19mikn6pbm2dq20")))

(define-public crate-prom-hashmod-0.1.1 (c (n "prom-hashmod") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "1bz77mvvav7k2z691dc3v44azmnxkshddnalyixrfv9qb1kbv223")))

