(define-module (crates-io pr om promissory) #:use-module (crates-io))

(define-public crate-promissory-0.1.2 (c (n "promissory") (v "0.1.2") (h "0i7f927pjx9jx5ispaq0z1z0k12yi8h3n03s76h2w1iqlrlp6bxg")))

(define-public crate-promissory-0.1.3 (c (n "promissory") (v "0.1.3") (h "044a12m1aiqhcs6dk8pjwzadvkamwr1gc085rw669z0f2pikpk5w")))

