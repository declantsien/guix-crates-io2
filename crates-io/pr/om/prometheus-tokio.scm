(define-module (crates-io pr om prometheus-tokio) #:use-module (crates-io))

(define-public crate-prometheus-tokio-0.1.0 (c (n "prometheus-tokio") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)) (d (n "tokio-metrics") (r "^0.2") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt"))) (d #t) (k 2)))) (h "1ls6mcn4h0qj30gvfw6jq7lvr5f33z7f44cyf5x7jfg7x2figlm6")))

(define-public crate-prometheus-tokio-0.2.0 (c (n "prometheus-tokio") (v "0.2.0") (d (list (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "axum") (r "^0.6.12") (d #t) (k 2)) (d (n "tokio") (r "^1.27") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "0l7gr6npdw0w11l9jva79lifry5i2i3bjzhplhmx7rnsnjaj0yww") (f (quote (("io_driver" "tokio/net"))))))

