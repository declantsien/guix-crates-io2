(define-module (crates-io pr om promformat) #:use-module (crates-io))

(define-public crate-promformat-0.1.0 (c (n "promformat") (v "0.1.0") (h "0h8lrhbcq6mv3gdaazxk6j64yvwb2yw5xhckj7zcs791kpfcxvcy")))

(define-public crate-promformat-0.1.1 (c (n "promformat") (v "0.1.1") (h "0qd2q27w0c1z238ajyzby6bp8f23d251qrin1hzc5gfcdi4hmajg")))

(define-public crate-promformat-0.2.0 (c (n "promformat") (v "0.2.0") (h "0d4wdcn2s7qvzl7mkfm2ch9xlmkw8vmmnzcgf4bfw7h6671gva37")))

(define-public crate-promformat-0.3.0 (c (n "promformat") (v "0.3.0") (h "0xb70wqsnc2h4wj7wwq5blqg579dlv57yapyn4adf48f6x8qn86p")))

(define-public crate-promformat-0.4.0 (c (n "promformat") (v "0.4.0") (h "14a0brq6kmd1qx48aq54ynd4xfmf1l7gm60nm9a1fzwk5q5fi7j1")))

(define-public crate-promformat-0.4.1 (c (n "promformat") (v "0.4.1") (h "0v2gmp2mgljv80mifflwlnrxbxvbyjl7ggf9lawapssc83l3zzh3")))

