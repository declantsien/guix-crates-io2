(define-module (crates-io pr om promkit-async) #:use-module (crates-io))

(define-public crate-promkit-async-0.1.0 (c (n "promkit-async") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("use-dev-tty" "event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.3") (d #t) (k 0)) (d (n "promkit") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0sb9v909n498kgnnqp0rqsraxjdix6x42kgabcyq33kw2r1bji")))

(define-public crate-promkit-async-0.1.1 (c (n "promkit-async") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("use-dev-tty" "event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.3") (d #t) (k 0)) (d (n "promkit") (r "=0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0py3cwf3jad2125gzpidb076vksxb6mb0pp8swbah6svgaclpqwl")))

