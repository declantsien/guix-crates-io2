(define-module (crates-io pr om prompt-organizer) #:use-module (crates-io))

(define-public crate-prompt-organizer-0.1.0 (c (n "prompt-organizer") (v "0.1.0") (d (list (d (n "parse-format") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 0)))) (h "1zmc5xvaaq48pp2gsjdbdjrpw7iawh7734lxy1s5ad284w18rdhj")))

