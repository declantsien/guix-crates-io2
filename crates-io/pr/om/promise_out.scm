(define-module (crates-io pr om promise_out) #:use-module (crates-io))

(define-public crate-promise_out-0.1.0 (c (n "promise_out") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0x1zx93c044dk2f0mkgmzix1jxfi7a8z7iisqg7hai7vd71g9yqx")))

(define-public crate-promise_out-1.0.0 (c (n "promise_out") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "04f3rya6rb4kp4nyi1wx7bjj996fh4zf3131ngl5pgzhx5v9hdz2")))

