(define-module (crates-io pr om promptis) #:use-module (crates-io))

(define-public crate-promptis-0.1.0 (c (n "promptis") (v "0.1.0") (h "1bdv1bpa5hrds013q7qkz3652jgwb6xc8nyglak78phizyas9678")))

(define-public crate-promptis-0.1.1 (c (n "promptis") (v "0.1.1") (h "1nqhyb4zp7w1y0hdjzsqhd8g11972p74qy6dnsd3f941533wj7g4")))

(define-public crate-promptis-0.2.0 (c (n "promptis") (v "0.2.0") (h "0x9583w6b0h0wg9xf5yi1bcnr8kl3qdhmqyw8v1aa87rz2whia9c")))

(define-public crate-promptis-0.2.3 (c (n "promptis") (v "0.2.3") (h "1v8zx1156ywhnv16p6fldqfivjw26vv5wd9v0wayxja4a62w70v2")))

(define-public crate-promptis-0.2.4 (c (n "promptis") (v "0.2.4") (h "1a213qjwavklh0vbcf7wbxc88vgk7aa1gpayclpr8b3zfzx57jjl")))

(define-public crate-promptis-0.3.0 (c (n "promptis") (v "0.3.0") (h "00z4aa4r28cvsmn1c6fzbbljxxwpy1ybn5waml1r86a92c14xicc")))

(define-public crate-promptis-0.4.0 (c (n "promptis") (v "0.4.0") (h "0saij1cgxsscjp2sr9ywphzw07ra0dpwp6l1l0w66xw3l46d70ar")))

(define-public crate-promptis-0.5.0 (c (n "promptis") (v "0.5.0") (h "1k7r0z6bs5dq2n5gvvy0myidflk2wvaxg8qkcr2mqxjq1k8jddsb")))

(define-public crate-promptis-0.5.1 (c (n "promptis") (v "0.5.1") (h "0ayaqrl77ahly711cly0l76hpxy50s3kxqw2yf5jzgkwfd304pbp")))

