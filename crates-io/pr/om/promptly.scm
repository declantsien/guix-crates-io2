(define-module (crates-io pr om promptly) #:use-module (crates-io))

(define-public crate-promptly-0.1.0 (c (n "promptly") (v "0.1.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1qxqhfl7ysq8vgm1hh1dm0mvs4cq6ddjfmzf3aglm80sfhc34z66")))

(define-public crate-promptly-0.1.1 (c (n "promptly") (v "0.1.1") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0xd7nwjaviv7gira0dnh5c1xd9ci65yyxyl8hfn08avmk42adzby")))

(define-public crate-promptly-0.1.2 (c (n "promptly") (v "0.1.2") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "05gryfpgzvgf37vxhax4j9qb64ddrc8xpm5ck870gn8clml4v0gg")))

(define-public crate-promptly-0.1.3 (c (n "promptly") (v "0.1.3") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "148zzxr7z6m5y54cnw1s71gq69239qj7hz68mr0m0zhz90hnw8lp") (y #t)))

(define-public crate-promptly-0.1.4 (c (n "promptly") (v "0.1.4") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0nyv4rh0xyicjjhln97kn6x86zb18y70kzld4ds8lfiv7q2hpx0h")))

(define-public crate-promptly-0.1.5 (c (n "promptly") (v "0.1.5") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (o #t) (d #t) (k 0)))) (h "1cj052y9jqfy01fxn3v10nh2xk4z9l10qaz2rp8h2d3ydm985h4h") (f (quote (("nightly") ("default"))))))

(define-public crate-promptly-0.2.0 (c (n "promptly") (v "0.2.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "141n7cwxxj22l02qamfwf9kimj6vziw4nwhlhhfla8hh0lqr9qc0") (f (quote (("nightly") ("default"))))))

(define-public crate-promptly-0.3.0 (c (n "promptly") (v "0.3.0") (d (list (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "02c5clawc9b3axg7q0z5r6dk5115jaycydqnsafrc38ii41gp75r") (f (quote (("nightly") ("default"))))))

(define-public crate-promptly-0.3.1 (c (n "promptly") (v "0.3.1") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1fl0548ww11gpja8hlsfc8jgxk00rdd48n1g6icgwadhlp2wdjws") (f (quote (("nightly") ("default"))))))

