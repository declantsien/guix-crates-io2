(define-module (crates-io pr om prompts) #:use-module (crates-io))

(define-public crate-prompts-0.1.0 (c (n "prompts") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "crossterm") (r "^0.14") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.10") (f (quote ("full"))) (d #t) (k 2)))) (h "1kn6k3gn9pj4lc3flbm9hkvkfi20x6wpkcgk36ixv083c45c2lzc")))

