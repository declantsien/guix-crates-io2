(define-module (crates-io pr om prom-timer) #:use-module (crates-io))

(define-public crate-prom-timer-0.1.0 (c (n "prom-timer") (v "0.1.0") (d (list (d (n "prometheus") (r ">=0.4, <=0.13") (k 0)))) (h "0ll8jqzq1njgk5svky34h6gkc3fpgcc034vi21znjii0q2m3qmq5")))

(define-public crate-prom-timer-0.1.1 (c (n "prom-timer") (v "0.1.1") (d (list (d (n "prometheus") (r ">=0.4, <=0.13") (k 0)))) (h "0y9i7y941k8rz7msi6n3js5i426gl396y4d2lpc18sbz433gzfql")))

