(define-module (crates-io pr om prometheus-client-derive-encode) #:use-module (crates-io))

(define-public crate-prometheus-client-derive-encode-0.4.0 (c (n "prometheus-client-derive-encode") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qp6azzlda4caml8y8m2x84f5d2f2v6bcqr3l71nvhiv2i0gajwm")))

(define-public crate-prometheus-client-derive-encode-0.4.1 (c (n "prometheus-client-derive-encode") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hcclh3333qm4rqr66vjf0yi21f9a7a559vzxq1xa2pbgchsbdkj")))

(define-public crate-prometheus-client-derive-encode-0.4.2 (c (n "prometheus-client-derive-encode") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1f22ckswiqnjlh1xaxkh8pqlfsdhj851ns33bnvrcsczp97743s4")))

