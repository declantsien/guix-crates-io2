(define-module (crates-io pr om prometheus-parse) #:use-module (crates-io))

(define-public crate-prometheus-parse-0.2.0 (c (n "prometheus-parse") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "16m3i0jnmgcdas57vq1l4xrjrgc21kciiba9ssfdmxhag7b85r1f")))

(define-public crate-prometheus-parse-0.2.1 (c (n "prometheus-parse") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0xkwjamq7yf0vrafxj7gpa5rqskcs6khq31hz8grdqfc69k8r49c")))

(define-public crate-prometheus-parse-0.2.2 (c (n "prometheus-parse") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "04p7rk25m1laxxvjdc378mgmab518y2gsb8d9h1sll8wxb5g75n9")))

(define-public crate-prometheus-parse-0.2.3 (c (n "prometheus-parse") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "01pk0i4gpkr0plvnlx84sydbhfrr1pr1x4r80gz5bz6gbg8qwypg")))

(define-public crate-prometheus-parse-0.2.4 (c (n "prometheus-parse") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "perf" "unicode"))) (k 0)))) (h "14nqddxws0qf7hss4fz77lc43ayva4zmdbwyj74b5x1vp3zaaahc")))

(define-public crate-prometheus-parse-0.2.5 (c (n "prometheus-parse") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "itertools") (r "^0.12") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "perf" "unicode"))) (k 0)))) (h "19bq7g8m54qqlc4khimc0ki02rna5j07vwz1n8gl0njylsz32441")))

