(define-module (crates-io pr om prometheus-parser) #:use-module (crates-io))

(define-public crate-prometheus-parser-0.4.0-rc1 (c (n "prometheus-parser") (v "0.4.0-rc1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0bs05q5178r8kpqvjgrfnf97nbwdcfkkk6ww6nzr0k8bmzaiyp3m")))

(define-public crate-prometheus-parser-0.4.0-rc2 (c (n "prometheus-parser") (v "0.4.0-rc2") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1v6lf0l290sq4d9f9766004h4wxazdb0lpfn4v7vqn9my6xykb87")))

(define-public crate-prometheus-parser-0.4.0 (c (n "prometheus-parser") (v "0.4.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0p6fcd2gp45y437c8bc9a3vpmhgy4m6sz8pfrv144casd6wg9gs9")))

(define-public crate-prometheus-parser-0.4.2 (c (n "prometheus-parser") (v "0.4.2") (d (list (d (n "enquote") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume") (r "^1.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0yz2byvlga9ff834sji27lyn389ca20kx152ypjz0f7z12r51jlm")))

