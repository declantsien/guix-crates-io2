(define-module (crates-io pr om prompto) #:use-module (crates-io))

(define-public crate-prompto-0.0.1 (c (n "prompto") (v "0.0.1") (d (list (d (n "image") (r "0.10.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "tempfile") (r "2.1.*") (d #t) (k 2)))) (h "0gm6g1ijz9q6kg11g4d4yjj8pfyspsivcgyzi8xm7pxri9ldsw4l")))

(define-public crate-prompto-0.0.2 (c (n "prompto") (v "0.0.2") (d (list (d (n "image") (r "0.10.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "tempfile") (r "2.1.*") (d #t) (k 2)))) (h "1845v0lkbhaghvrsil827jkgb7zqv7pgi0lg5bissasv9v8alpnn")))

(define-public crate-prompto-0.0.3 (c (n "prompto") (v "0.0.3") (d (list (d (n "image") (r "0.10.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "tempfile") (r "2.1.*") (d #t) (k 2)))) (h "1l98wzw51r2ms0j239ciilbsf7gn7sqabbb93d84ay00ky15lvi9")))

