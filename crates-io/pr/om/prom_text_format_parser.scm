(define-module (crates-io pr om prom_text_format_parser) #:use-module (crates-io))

(define-public crate-prom_text_format_parser-0.1.0 (c (n "prom_text_format_parser") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "winnow") (r "^0.5") (d #t) (k 0)))) (h "1szhvf7xp0aiajhslxmf6qq7glj0nbh06vpzqsmdsjrxnf97x5mp")))

