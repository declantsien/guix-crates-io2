(define-module (crates-io pr om promerge) #:use-module (crates-io))

(define-public crate-promerge-0.1.3 (c (n "promerge") (v "0.1.3") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0qyxfd9hn48k8ak9halqm89z7vgdh8rzhphpp8l0zln65knmfg75")))

(define-public crate-promerge-0.1.4 (c (n "promerge") (v "0.1.4") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "15c0bia1w7na4mwi0p7i3bdprawrbix0x0grlycnjxxq7v9wwm76")))

(define-public crate-promerge-0.1.5 (c (n "promerge") (v "0.1.5") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "16dyjnnx2vwzs5djksz9ciid82br0c01qfq4ks58czd0m8phngm8")))

