(define-module (crates-io pr om prompted) #:use-module (crates-io))

(define-public crate-prompted-0.2.2 (c (n "prompted") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0qwsaby2l0hyrv6d8jqwv58h8wprajw5bsz9als166xfhgyq0ix8")))

(define-public crate-prompted-0.2.3 (c (n "prompted") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1gn91j8jxcglj81crnv3jrqa4qfg0yx0rlp3b0wfii5xh0rxf5gc")))

(define-public crate-prompted-0.2.4 (c (n "prompted") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "16x6rh30j2rvzl5r28fr7wf23rd61ihabsvl7jz7ahkv6j5xnvyi")))

(define-public crate-prompted-0.2.5 (c (n "prompted") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0kww54spcxw7mks63702a9w6yl2597vnbh593kip67hs2pnqnjar")))

(define-public crate-prompted-0.2.6 (c (n "prompted") (v "0.2.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "053bv5p7sqpzjgb9n2kvq85di1flnra54qc43mhzdf7yfi7l8i92")))

(define-public crate-prompted-0.2.7 (c (n "prompted") (v "0.2.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1pd9n8pnar4p0xnkq2q48rybd0bisbhaphi8v12qzccj7izank9y")))

(define-public crate-prompted-0.2.8 (c (n "prompted") (v "0.2.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "16wz6yksyjgaz3jrsw3qybmg2c15jssryqbbzc8nxzp292zzak6k")))

