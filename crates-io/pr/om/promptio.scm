(define-module (crates-io pr om promptio) #:use-module (crates-io))

(define-public crate-promptio-0.1.0 (c (n "promptio") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "00k673z3kyc8wdwkjcxyjg7xv33crawzg6sfv5nngrpf19qmnw5q") (y #t)))

