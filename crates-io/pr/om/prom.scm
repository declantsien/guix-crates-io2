(define-module (crates-io pr om prom) #:use-module (crates-io))

(define-public crate-prom-0.1.0 (c (n "prom") (v "0.1.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 2)) (d (n "protobuf") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^0.2") (d #t) (k 0)))) (h "0x2njw22z405mfvwfybfckm876m05lb5zd18c35xcdlpwzsb772f") (f (quote (("dev" "clippy") ("default"))))))

