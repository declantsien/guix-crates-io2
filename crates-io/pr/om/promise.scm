(define-module (crates-io pr om promise) #:use-module (crates-io))

(define-public crate-promise-0.0.1 (c (n "promise") (v "0.0.1") (h "1vqnkcihxk4wnk0c3dv0yywilmvhrnsvqwhfrlh2w88yv91y8v0i")))

(define-public crate-promise-0.0.2 (c (n "promise") (v "0.0.2") (h "0aypjy9bb34mqir7yqq86lddfjfnj3vxqk0xlzg73i8gds9p9g4m")))

(define-public crate-promise-0.0.3 (c (n "promise") (v "0.0.3") (h "0xsqbxvjvlj2svni87i7v4h99fxqpxr2i833yvspkwgg0slsrcxr")))

(define-public crate-promise-0.0.4 (c (n "promise") (v "0.0.4") (h "1mi56h39gp8k5jxn6ygs3ixph9m2dqjdrdicd3day31f1j3yvf6y")))

