(define-module (crates-io pr om prom-timer-macro) #:use-module (crates-io))

(define-public crate-prom-timer-macro-0.1.1 (c (n "prom-timer-macro") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "prom-timer") (r "^0.1.1") (d #t) (k 0)) (d (n "prometheus") (r ">=0.4, <=0.13") (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ic28il6kpa5k6mdffxxvnr7qpmvgv70nbkw5w14zzwnx1g9p6ag")))

