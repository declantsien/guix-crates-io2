(define-module (crates-io pr om promises) #:use-module (crates-io))

(define-public crate-promises-0.1.0 (c (n "promises") (v "0.1.0") (h "0qgf1krs6wwq4947xwa8l4na01rzrygr04vaf8y4gajlp52j9j06")))

(define-public crate-promises-0.2.0 (c (n "promises") (v "0.2.0") (h "0krd4wa9lxxmvfk1fksv7cky2dmvyyja70xbvaza25kanjk5cnaq")))

