(define-module (crates-io pr om prometheus-exposition-format-rs) #:use-module (crates-io))

(define-public crate-prometheus-exposition-format-rs-0.1.0 (c (n "prometheus-exposition-format-rs") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "04rsxsi5n09k5zhbgcjz2ls148js3p70f77y9qp22cc0xcrdbhj1")))

(define-public crate-prometheus-exposition-format-rs-0.1.1 (c (n "prometheus-exposition-format-rs") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "0pcr9f9k70qrx66yij2gg8yigra5lqd8vm5bqafra3h1wir4zlqh")))

(define-public crate-prometheus-exposition-format-rs-0.1.2 (c (n "prometheus-exposition-format-rs") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "166x58pa5nahzzprq6rswrwjiqyn09xwcgaisk6p879rcvhshi9z")))

