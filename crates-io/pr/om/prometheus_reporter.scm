(define-module (crates-io pr om prometheus_reporter) #:use-module (crates-io))

(define-public crate-prometheus_reporter-0.0.1 (c (n "prometheus_reporter") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "lru-cache") (r "^0.0.7") (d #t) (k 0)) (d (n "persistent") (r "^0.2.0") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "router") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "12riw9vikrxhd21i991jfy3g1469mzr43fx9vnwrwnfx30873zn0")))

(define-public crate-prometheus_reporter-0.0.2 (c (n "prometheus_reporter") (v "0.0.2") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "lru-cache") (r "^0.0.7") (d #t) (k 0)) (d (n "persistent") (r "^0.2.0") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "router") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1jgyhm1akpc55cz9llrpj6n6a9glza02nrh0r2mvasj3f1fs1s2r")))

