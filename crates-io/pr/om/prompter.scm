(define-module (crates-io pr om prompter) #:use-module (crates-io))

(define-public crate-prompter-0.1.0 (c (n "prompter") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "10w565c5q7fy11w7bwh2nr7277i73dchwsf845nyjm0ca9cvy1wv")))

(define-public crate-prompter-0.1.1 (c (n "prompter") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "13h59l5hq8951vfn0cx3jni11l71zk58qkza0gadb1skzlx02ni5")))

(define-public crate-prompter-0.1.2 (c (n "prompter") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)))) (h "0qvp208cyysap9m3wwzhf2pg2s96p4sd84arylhhxz0x0qz6nnhl")))

