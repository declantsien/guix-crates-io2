(define-module (crates-io pr om prom-write) #:use-module (crates-io))

(define-public crate-prom-write-0.1.0 (c (n "prom-write") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "prometheus_remote_write") (r "^0.1.0") (f (quote ("parse" "compression" "http"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("tls" "rustls"))) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "056kbc4lvc4ypr7vg1ml9z79l48qsfp12263q63p2qf8m0qmld2c")))

(define-public crate-prom-write-0.2.0 (c (n "prom-write") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "prometheus_remote_write") (r "^0.2.0") (f (quote ("parse" "compression" "http"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("tls" "rustls"))) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1a0ggm6zd7ccxaln39z4940p5c5i4265zf10qv6jddgpqalnfa41")))

(define-public crate-prom-write-0.2.1 (c (n "prom-write") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "prometheus_remote_write") (r "^0.2.1") (f (quote ("parse" "compression" "http"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("tls" "rustls"))) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "17i4z56fhgcj9mwi09wrfmm02lzik3c9h8zklf2gacv6qv5r7381")))

