(define-module (crates-io pr om prometheus-metric-storage-derive) #:use-module (crates-io))

(define-public crate-prometheus-metric-storage-derive-0.1.0 (c (n "prometheus-metric-storage-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "012hziap6ayg7df54r0grarakg6f545hc5vh7l7r0xw5avnn3926")))

(define-public crate-prometheus-metric-storage-derive-0.1.1 (c (n "prometheus-metric-storage-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gfmnvbdd529j9nzmwd8hm9b5xmfagra23rh9g9cblzdxgh90gz5")))

(define-public crate-prometheus-metric-storage-derive-0.2.0 (c (n "prometheus-metric-storage-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16qj7syc171m8cfzgdqxmnrhaybx2aahsq5y6138fqm383r5hzsj")))

(define-public crate-prometheus-metric-storage-derive-0.3.0 (c (n "prometheus-metric-storage-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05qla2i1iqn4cyvap625ljw0phpn9dr4xxr16zy6jzf50x5cm3si")))

(define-public crate-prometheus-metric-storage-derive-0.4.0 (c (n "prometheus-metric-storage-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hqrxbaw1pnpp6q0srxi8dxqdkpvh2r8ahlp5gckj0b8w5bba7k8")))

(define-public crate-prometheus-metric-storage-derive-0.5.0 (c (n "prometheus-metric-storage-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qv9rfmaclbkg96mhj8bfqhk6qdjh2951xmqb1y2fdfd22m234i3")))

