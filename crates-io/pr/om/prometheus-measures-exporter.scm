(define-module (crates-io pr om prometheus-measures-exporter) #:use-module (crates-io))

(define-public crate-prometheus-measures-exporter-0.0.1 (c (n "prometheus-measures-exporter") (v "0.0.1") (d (list (d (n "hyper") (r "^0.14.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)))) (h "0vkwbhaj04ibhdbcbk08122swx02gjvcpj4qxgydp4kvkz6jia3h")))

