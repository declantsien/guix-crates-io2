(define-module (crates-io pr om prom-attire-bootstrap) #:use-module (crates-io))

(define-public crate-prom-attire-bootstrap-0.1.0 (c (n "prom-attire-bootstrap") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "prom-attire-impl") (r "= 0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1vngps4wv1kq4l39nmbhx878rh8arfp5q1rzpmk5ls22lgrvqyf8") (y #t)))

