(define-module (crates-io pr om prometheum) #:use-module (crates-io))

(define-public crate-prometheum-0.1.0 (c (n "prometheum") (v "0.1.0") (h "1cq6c3gk9k3x8625vdp8kn2wvp3fvb17bmj23980212zi7zvvjh5")))

(define-public crate-prometheum-0.1.1 (c (n "prometheum") (v "0.1.1") (h "1qxjqc28llsxrfkdbghqiq2rvmygxnz94vnijlkk8411cidccz65")))

