(define-module (crates-io pr om prometheus-json-encoder) #:use-module (crates-io))

(define-public crate-prometheus-json-encoder-0.1.0 (c (n "prometheus-json-encoder") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.17.0") (d #t) (k 0)) (d (n "opentelemetry-prometheus") (r "^0.10.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "03k7d9a2rxyrqa325g0x0f1b27zcxsdw6b31jns67985appwns7j") (y #t)))

(define-public crate-prometheus-json-encoder-0.1.1 (c (n "prometheus-json-encoder") (v "0.1.1") (d (list (d (n "opentelemetry") (r "^0.17.0") (d #t) (k 0)) (d (n "opentelemetry-prometheus") (r "^0.10.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1rn0idiscq0sdx5rdk1h87fmfmm6fkxhrq89ckl586qys3bwrnl3")))

