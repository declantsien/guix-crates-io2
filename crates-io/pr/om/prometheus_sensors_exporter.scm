(define-module (crates-io pr om prometheus_sensors_exporter) #:use-module (crates-io))

(define-public crate-prometheus_sensors_exporter-0.1.2 (c (n "prometheus_sensors_exporter") (v "0.1.2") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 0)) (d (n "tmp1x2") (r "^0.2.0") (d #t) (k 0)))) (h "17rjwp59q8g6a7mbskdj3mh46nahx3ykhwdmzkkdq5dxrgrf3k8k")))

