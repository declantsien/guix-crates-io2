(define-module (crates-io pr om prometheus-endpoint) #:use-module (crates-io))

(define-public crate-prometheus-endpoint-0.0.0 (c (n "prometheus-endpoint") (v "0.0.0") (h "0alxzgwdw26khzhn5k427kgdhn69fj8snp0an6k0q5kcvgfgdyqm") (y #t)))

(define-public crate-prometheus-endpoint-0.8.1 (c (n "prometheus-endpoint") (v "0.8.1") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (k 0)) (d (n "hyper") (r "^0.13.9") (f (quote ("stream"))) (t "cfg(not(target_os = \"unknown\"))") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)) (d (n "tokio") (r "^0.2") (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0)))) (h "0jkyzx768w5d3jfrf08x671ff0v5wycxpr9crz19xhr2axp1c7sl")))

(define-public crate-prometheus-endpoint-2.1.2 (c (n "prometheus-endpoint") (v "2.1.2") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (k 0)) (d (n "hyper") (r "^0.13.9") (f (quote ("stream"))) (t "cfg(not(target_os = \"unknown\"))") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)) (d (n "tokio") (r "^0.2") (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 0)))) (h "05yq6103lr8kjv3p8hwnxi2f4yx9kzkfv9k5nkav54jrxyndlnh1")))

