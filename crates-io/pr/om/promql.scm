(define-module (crates-io pr om promql) #:use-module (crates-io))

(define-public crate-promql-0.2.0 (c (n "promql") (v "0.2.0") (d (list (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1y49iw3f6g00bvjbn223xz79i3808q4kfy5vra7k255df3zk95w3")))

(define-public crate-promql-0.3.0 (c (n "promql") (v "0.3.0") (d (list (d (n "nom") (r "^4.0.0-beta1") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "08ma41xhhmq6ggcddr494gcqmffnj1b37nzlwwdfmfp8k0xcjldj")))

(define-public crate-promql-0.4.0 (c (n "promql") (v "0.4.0") (d (list (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1cdpdnl411b5jamrqfl3wwbkiwc6nbs0601n4xlq6ja8q4bf96mf")))

(define-public crate-promql-0.4.1 (c (n "promql") (v "0.4.1") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "15qkpdrjhwf97i127249n6kjqbqkc2r16dpn8l10yp0r2javpybq")))

(define-public crate-promql-0.4.2 (c (n "promql") (v "0.4.2") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1srjs79vy94mcjqpv59jy2kqydj8grgqgq5vvyjpnfywryx2r8v0")))

(define-public crate-promql-0.5.0 (c (n "promql") (v "0.5.0") (d (list (d (n "builder-pattern") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yz3rirydgbfbajxx3xf1gw930cz9krrjwxyykj7g4b229dhvr1w") (f (quote (("serializable" "serde" "serde_derive") ("default"))))))

