(define-module (crates-io pr om prom2jsonrs) #:use-module (crates-io))

(define-public crate-prom2jsonrs-0.1.0 (c (n "prom2jsonrs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0iv8ds7jp9hnglz0lpgbr46iv4drdpfmmrmg8znpfr9g7bdg3s8a")))

