(define-module (crates-io pr om prometheus-metric-storage) #:use-module (crates-io))

(define-public crate-prometheus-metric-storage-0.1.0 (c (n "prometheus-metric-storage") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.1.0") (d #t) (k 0)))) (h "183ymwg5n84y7bf4i2v0qn6j7xk2ksnz2s6gbc22x8j7s4ji4kpq")))

(define-public crate-prometheus-metric-storage-0.1.1 (c (n "prometheus-metric-storage") (v "0.1.1") (d (list (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0kqbix2pz1579sxvnvpc0wzwavlq39r5ldrgvpmyimd91840an7m")))

(define-public crate-prometheus-metric-storage-0.2.0 (c (n "prometheus-metric-storage") (v "0.2.0") (d (list (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.2.0") (d #t) (k 0)))) (h "00nb65wpx5hs727r3604sds80qilbhcma9vfmkc2i9jfnkq7pg78")))

(define-public crate-prometheus-metric-storage-0.3.0 (c (n "prometheus-metric-storage") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1hn69g1k0lpr69isr33bp55w0cqdyzk7gamhjk5bym4hgry3q1j6")))

(define-public crate-prometheus-metric-storage-0.4.0 (c (n "prometheus-metric-storage") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1p9fwkcsg3ja846fa6yfmi0sfpkr5pmz5z4c1lkbbls6cif7d1j3")))

(define-public crate-prometheus-metric-storage-0.5.0 (c (n "prometheus-metric-storage") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)) (d (n "prometheus-metric-storage-derive") (r "^0.5.0") (d #t) (k 0)))) (h "057zcdfp2l3phv90241c2sq3v5n5jikyjpg4268alyxhl1z490ij")))

