(define-module (crates-io pr om prominence) #:use-module (crates-io))

(define-public crate-prominence-0.1.0 (c (n "prominence") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07zhqchkr4smlzccaz91y37w2camqb9xsc71r3wprh85mpd6x8sf") (s 2) (e (quote (("serde" "dep:serde"))))))

