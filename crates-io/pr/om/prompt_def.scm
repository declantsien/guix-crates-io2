(define-module (crates-io pr om prompt_def) #:use-module (crates-io))

(define-public crate-prompt_def-0.1.0 (c (n "prompt_def") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "01ygh4si0plm12pspmv04cjmii8vpf4q67mf7kljmaalivf3367w")))

