(define-module (crates-io pr om prom-attire-impl) #:use-module (crates-io))

(define-public crate-prom-attire-impl-0.1.0 (c (n "prom-attire-impl") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1svqg51hh7ckj58nb3dgbx27sm0dc3h66ab3h1z47j6vnqd05h7f") (y #t)))

