(define-module (crates-io pr om prometheus-client-derive-text-encode) #:use-module (crates-io))

(define-public crate-prometheus-client-derive-text-encode-0.2.0 (c (n "prometheus-client-derive-text-encode") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0li6jgfqyrdd7vc620mk7jcw2gr6nrk7di998pmxjsnnp40jvqg8")))

(define-public crate-prometheus-client-derive-text-encode-0.3.0 (c (n "prometheus-client-derive-text-encode") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kakm6asrnrnwx5id3jbvz6qjy7xc3l8digkxiyilk4mrgxmb936")))

