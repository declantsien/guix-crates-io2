(define-module (crates-io pr om promising-future) #:use-module (crates-io))

(define-public crate-promising-future-0.1.0 (c (n "promising-future") (v "0.1.0") (h "1z773cpgyjc62wvf1h9andcn3z5jm6g3z3ah2wqjgqbizr4xvx2q")))

(define-public crate-promising-future-0.1.1 (c (n "promising-future") (v "0.1.1") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17nifsrwv2fr0jqyzfnricsjw110f92vxhnc1vbmlqy6x49w0pzr") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.2 (c (n "promising-future") (v "0.1.2") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "124yclm60qv2skmmc6i1c9h6zmwxwhib7l4s11snc6xa3nyv9msq") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.3 (c (n "promising-future") (v "0.1.3") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0z1zls0yipjq50d40w5wvywqphvkn5rizhillpi224g6k7wabgda") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.4 (c (n "promising-future") (v "0.1.4") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1r5w64qvw9jbqi2id0wzsnfgahxb40bn8v2y0pj8mklpg8m35mfp") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.5 (c (n "promising-future") (v "0.1.5") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "015whhm6hh55411d7clc21ag4h8zdjg7al6bli8h136swpm73gzr") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.6 (c (n "promising-future") (v "0.1.6") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0zhpyhzzsyn9dr4rxxv056i5x45hc4dnhx5mxk7k1rmxjmfp9dk1") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.1.7 (c (n "promising-future") (v "0.1.7") (d (list (d (n "threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1kd65xvsx78hrj71z7hq8pgz7cvlj3rkjpgj6k9k67mr4xqz3387") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.2.0 (c (n "promising-future") (v "0.2.0") (d (list (d (n "threadpool") (r "^0.2") (o #t) (d #t) (k 0)))) (h "18zblr1vvp3s71pn1wq30c0pxf5qk36s8ah50w0lazipw90m3x0b") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.2.1 (c (n "promising-future") (v "0.2.1") (d (list (d (n "threadpool") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1smvy0rj2n38sa66d6g0zcysjhs9882qsf47fcxxcrqx6fc6c8ha") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.2.2 (c (n "promising-future") (v "0.2.2") (d (list (d (n "threadpool") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1rmi0rh13s3nn4hh49dn8dm3kdp1gh6jv34bwwj5scs56zqfznpw") (f (quote (("default" "threadpool"))))))

(define-public crate-promising-future-0.2.3 (c (n "promising-future") (v "0.2.3") (d (list (d (n "threadpool") (r "^1.2") (o #t) (d #t) (k 0)))) (h "13qyph03rdal9h8skjl6g56m24s646ga69896jf9adhxlgp4yw6p") (f (quote (("unstable") ("default" "threadpool"))))))

(define-public crate-promising-future-0.2.4 (c (n "promising-future") (v "0.2.4") (d (list (d (n "threadpool") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1qjc9c4vk93xmvyrxrdzryn1z02jza9xh9khhq1fb1c73cf4dfj4") (f (quote (("unstable") ("default" "threadpool"))))))

