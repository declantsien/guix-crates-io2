(define-module (crates-io pr om prom-attire) #:use-module (crates-io))

(define-public crate-prom-attire-0.1.0 (c (n "prom-attire") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "prom-attire-bootstrap") (r "= 0.1.0") (d #t) (k 0)) (d (n "prom-attire-impl") (r "= 0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 2)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1ngh4hfrvdr23azfy5l9dwibhrck3lppyi5ks6n2mg95svm5j9lp") (y #t)))

