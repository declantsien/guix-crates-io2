(define-module (crates-io pr om prom-types) #:use-module (crates-io))

(define-public crate-prom-types-0.1.0 (c (n "prom-types") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wx528nz7ps4ms35sphjxnx7zhjhr23afr0dlzy7bm4fpdbnvgyx")))

(define-public crate-prom-types-0.1.1 (c (n "prom-types") (v "0.1.1") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nwdd009vwbmzhkq0xqm6cqld4b103i7sbqvbddwczgmx4ixxzvl")))

