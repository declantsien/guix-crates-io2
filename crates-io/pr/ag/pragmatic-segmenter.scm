(define-module (crates-io pr ag pragmatic-segmenter) #:use-module (crates-io))

(define-public crate-pragmatic-segmenter-0.1.0 (c (n "pragmatic-segmenter") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-ucd-case") (r "^0.9.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 2)))) (h "1vzj6qnilff6jm0kib7vr635n3jai3fnkdg3frlycd0sxp5h4rg9")))

(define-public crate-pragmatic-segmenter-0.1.1 (c (n "pragmatic-segmenter") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-ucd-case") (r "^0.9.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 2)))) (h "1yi7nqsq63rvv8y2sgvhyjjbzavp2x2ysfa05bjrnq4kb78xg0p1")))

(define-public crate-pragmatic-segmenter-0.1.2 (c (n "pragmatic-segmenter") (v "0.1.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-ucd-case") (r "^0.9.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 2)))) (h "1fnjryfijj8ssm516y3ln70l2nfwg7dc2zdnrbm8ysg7i3cvqa2d")))

(define-public crate-pragmatic-segmenter-0.1.3 (c (n "pragmatic-segmenter") (v "0.1.3") (d (list (d (n "aho-corasick") (r "^1") (d #t) (k 0)) (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unic-ucd-case") (r "^0.9.0") (d #t) (k 0)))) (h "0nza65wvywwm0f58ycixd0xgjf4inr1xj820mqi52152q4b0lxww")))

