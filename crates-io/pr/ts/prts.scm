(define-module (crates-io pr ts prts) #:use-module (crates-io))

(define-public crate-prts-0.1.0 (c (n "prts") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0nc3n5h1pw2h43lqn9xrhjw2zmm0ifbgjq5w7l9vqvr2nrq5ia26") (y #t)))

(define-public crate-prts-0.1.1 (c (n "prts") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1pmbfiddhlwrp785qd3pakrrlmgawvb5x4gjwks123kpx6vrk8w6") (y #t)))

(define-public crate-prts-0.1.2 (c (n "prts") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "04vsjpphb5hda79dqbrh1q7ff9k4x9jxj1w0wyr8wkqfrvw9nsd8") (y #t)))

(define-public crate-prts-0.2.0 (c (n "prts") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "09ja86rz68s6z74sg95vf9d4nn4dwgqayjraz30wwqrjspfk7pb7") (y #t)))

(define-public crate-prts-0.2.1 (c (n "prts") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0rwaa5v935lj339k10fl3gb6qj4ckgg432g7g9dvfiahrp2ds01v") (y #t)))

(define-public crate-prts-0.2.2 (c (n "prts") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1jsfnipy1wmizlqzf5qynjiafbdj9qwyz6n3k5m0xljkq14fki0m") (y #t)))

(define-public crate-prts-0.2.3 (c (n "prts") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "13w8fadcjpdhqy4ws7vnpvn1xccndmqdgnd28nh1nmh198d27yva")))

(define-public crate-prts-0.2.4 (c (n "prts") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0ygnshrsnb6ds27rp902ywr7sh5jffsc1vz1lrmqfikykfixzzva")))

