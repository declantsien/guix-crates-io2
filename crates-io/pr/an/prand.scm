(define-module (crates-io pr an prand) #:use-module (crates-io))

(define-public crate-prand-1.0.0 (c (n "prand") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0q9cfczjj3xfxbk3r0pan71qxwp97358ghhy5mhmzxjca0b8dj8b")))

(define-public crate-prand-1.0.1 (c (n "prand") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0bjcm0ylcl4sdfjq33ddfi1zm88vbcfj84gf4ylw3y95k5ci87r7")))

