(define-module (crates-io pr an prange2) #:use-module (crates-io))

(define-public crate-prange2-2.0.0 (c (n "prange2") (v "2.0.0") (h "16a981jaiifnnq00spwra80cwdkwi5r51b7pkzmn0jrm27csmjld")))

(define-public crate-prange2-2.0.1 (c (n "prange2") (v "2.0.1") (h "1gwqh0qwfnzrn9lfqikp6pb22xj850cpyy9v8aqp1ggqp435vz4w")))

