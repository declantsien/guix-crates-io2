(define-module (crates-io pr an pranav_minigrep) #:use-module (crates-io))

(define-public crate-pranav_minigrep-0.1.0 (c (n "pranav_minigrep") (v "0.1.0") (h "0fgpak2pl6crywilg36cp51ppfnj8f53xkrj5y768bdqr5qkgibs") (y #t)))

(define-public crate-pranav_minigrep-0.1.1 (c (n "pranav_minigrep") (v "0.1.1") (h "0saxn4zax4vrr4k5ipyrkypqhrwh02zzjzgnbywv5gb35bz8kjsw") (y #t)))

(define-public crate-pranav_minigrep-0.1.2 (c (n "pranav_minigrep") (v "0.1.2") (h "1a139ac3dadv86v0f8fyxmhdp6ssfr8dabibyvlls03sfd4hvhip") (y #t)))

(define-public crate-pranav_minigrep-0.1.3 (c (n "pranav_minigrep") (v "0.1.3") (h "1jc2h5ww5vmwvnv94qdmnr1ma5lq8j0kphp8awyhardwfms1vz8j") (y #t)))

(define-public crate-pranav_minigrep-0.1.4 (c (n "pranav_minigrep") (v "0.1.4") (h "1q7xh22r8r4cshhrwpbmns01l867ali88aszpyrgg0ibf09ksnkx")))

