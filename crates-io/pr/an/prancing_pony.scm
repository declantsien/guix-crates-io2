(define-module (crates-io pr an prancing_pony) #:use-module (crates-io))

(define-public crate-prancing_pony-0.1.0 (c (n "prancing_pony") (v "0.1.0") (h "17vm9wm0yya194cvcmmss0i423lfjr0mkryyqzpkdvdfblrlvwz9")))

(define-public crate-prancing_pony-1.0.0 (c (n "prancing_pony") (v "1.0.0") (h "1m2a42xlyfp06f1ag79vz45i6a1rfyapcz5kxasn9dyg05y0si14")))

