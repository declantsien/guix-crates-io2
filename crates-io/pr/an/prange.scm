(define-module (crates-io pr an prange) #:use-module (crates-io))

(define-public crate-prange-1.0.0 (c (n "prange") (v "1.0.0") (h "1rd7pvnx0vc45yfd7lr568v7z7ncby4ilsbm22fy57p1ic43m1sq")))

(define-public crate-prange-1.0.1 (c (n "prange") (v "1.0.1") (h "1svc8zpvyjm03bglpzyq9dabf45vnfsgqxpy47zmxnklym1mwk7w")))

