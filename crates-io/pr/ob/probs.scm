(define-module (crates-io pr ob probs) #:use-module (crates-io))

(define-public crate-probs-0.1.0 (c (n "probs") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "18n50s3bhh6w4j7ycih0jv0scjc1x27gidx5wz5kbn8q0fi2pcp4")))

(define-public crate-probs-0.1.1 (c (n "probs") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "1h6qkgddgl015jgki430icsybysym8g8pq4nd6rvyi41d3czc0xd")))

(define-public crate-probs-0.1.2 (c (n "probs") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "10cf7166vkl32fd6fbm25j9m9zjfrscjjfgk5aj3b56yyzgymfas")))

(define-public crate-probs-0.2.0 (c (n "probs") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "nshare") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "0irqzpxp2x8hf0pq115fa364df3n6kr6gklzyl69brs4bbdgi2cy")))

(define-public crate-probs-0.2.1 (c (n "probs") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "nshare") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "079r3z3gmff6d1d07v6qyd88305njsmc765rd59cjpazm5wsx2r6")))

(define-public crate-probs-0.3.0 (c (n "probs") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "nshare") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "1in1h2sqwfk8jcsf30ilpvgxsg4nx1xcbg5kgnyjp6v36g6fmknm")))

(define-public crate-probs-0.3.1 (c (n "probs") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "nshare") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.3") (d #t) (k 0)))) (h "0ykyiw233dabl1c7mfyjlafgfps2n9d19hc9xng4zmkb920nq5cm")))

