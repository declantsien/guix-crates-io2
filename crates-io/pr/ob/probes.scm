(define-module (crates-io pr ob probes) #:use-module (crates-io))

(define-public crate-probes-0.0.1 (c (n "probes") (v "0.0.1") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "1z9d4ghnhiyxn81aphrbd7n3z99c0pvyflg4mrhx0vghslh5m98l")))

(define-public crate-probes-0.1.0 (c (n "probes") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "096sdy21r2kamf46yjjpg12259sxx2dyc17w8p43w8948hb6nfv5")))

(define-public crate-probes-0.1.1 (c (n "probes") (v "0.1.1") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "1mrqp0vyk0sd949kljkzq4b0wyrkq5ia2h7i1lz1gv6vz07xhrh8")))

(define-public crate-probes-0.1.2 (c (n "probes") (v "0.1.2") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "0pckh3al6dmm86p5rlnv1x9sc5aps066q8r6wrcv8jqnxrvgqy8n")))

(define-public crate-probes-0.1.3 (c (n "probes") (v "0.1.3") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "1hghbl5qvw7y0scgzfn82y916cp4ndhq3f0lsk7s3rs0kmmdkqm3")))

(define-public crate-probes-0.2.0 (c (n "probes") (v "0.2.0") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "0kffzmpzs32dbcvxmbx79xp1rrw02nscf9nb373r2cpj2hhb2vda")))

(define-public crate-probes-0.2.1 (c (n "probes") (v "0.2.1") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "126dc7kfz6fq6ha2rbc6bkp9l23xd9v5n7hvam2731i497qj7k54")))

(define-public crate-probes-0.2.2 (c (n "probes") (v "0.2.2") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "0qxpdlvl85p4bswvi9dbqknk3m4a6hmyi4y019yr8adchvgx4p05")))

(define-public crate-probes-0.3.0 (c (n "probes") (v "0.3.0") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "time") (r ">= 0.1.34") (d #t) (k 0)))) (h "05njwshzgd9q5bh5s1c3gh5j3z3wa39qzi099aywp66qsvv6czpp")))

(define-public crate-probes-0.4.0 (c (n "probes") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09j27hsh16084w1zgi8sczbknri44ckgwf56dfwfm20k5rzyqym6")))

(define-public crate-probes-0.4.1 (c (n "probes") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1n02r72zbs8sxdkrw5cn5px4z3pc5g49wlhr5i45y68zccl2mc2b")))

(define-public crate-probes-0.4.2 (c (n "probes") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02l58vhc3dfszwbxfpy277sbwsf34jp903c2i3zx8jbylzv38z08")))

(define-public crate-probes-0.4.3 (c (n "probes") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xch2siqj7iw8yqv6phfljr8ii6f8aazj2kga0c7285wv4pizxcm")))

(define-public crate-probes-0.5.0 (c (n "probes") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fs33winapqvm6ks7rqlh4b8ha2qw0y83rydrq1d6s2qyqqwq31l")))

(define-public crate-probes-0.5.1 (c (n "probes") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a5qc4x5nyvir05xgskq9zh39g30qxv6iraanixpwlrkwfmg3rhv")))

(define-public crate-probes-0.5.2 (c (n "probes") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a1liilwd34rqcsfdi4qs57mvywvnl98llaqza4pvzcdkn55ajy6")))

(define-public crate-probes-0.5.3 (c (n "probes") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p3j09j3wvdpckkmydyxxii6w5jhhpw0lwyn8nggmf7xy0dzx0s2")))

(define-public crate-probes-0.5.4 (c (n "probes") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "178q68n7mlmlh1sy3sa1s655fkbm80hx5qi6g4z5h48ywb1caqwz")))

(define-public crate-probes-0.6.0 (c (n "probes") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dh52zmp909flsyyqng28is4j396aq1dgm7s8paw74qm3ccwpbvz")))

(define-public crate-probes-0.7.0 (c (n "probes") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p8qnqgvp7jxcwssmvcb6h65dcmknspxv53j7l8qa8khiyhfk439")))

(define-public crate-probes-0.7.1 (c (n "probes") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13vxx7inj379svc7jfkyjpa4adix3ka9vdxc25k310zswyn666i2")))

