(define-module (crates-io pr ob probly-search) #:use-module (crates-io))

(define-public crate-probly-search-0.1.0 (c (n "probly-search") (v "0.1.0") (h "1lgp0141idazyg0hf8myp8yzqmq29jyw9i9gm2nxf1smnfnmxh6d")))

(define-public crate-probly-search-0.1.1 (c (n "probly-search") (v "0.1.1") (h "0lplb5f6nzr5bx1x6sq753n29hcdm75nx626q9cblv4087ck14jq")))

(define-public crate-probly-search-1.0.0 (c (n "probly-search") (v "1.0.0") (h "1g0zx86wsjr2gi2v409pp50gmp3hig6j3hgrh8a7si1fx1sq6smn")))

(define-public crate-probly-search-1.1.0 (c (n "probly-search") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0wa3ih82rfxafnmn355ykh9m38466g6wb21w4mhmhqxj8pbiy6y5")))

(define-public crate-probly-search-1.1.1 (c (n "probly-search") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "095l9imad6zga7gn9143jh79fdbibdx6xyyncc23zhzr6fi4fc1q")))

(define-public crate-probly-search-1.1.2 (c (n "probly-search") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0dxf6xp6sgjrrr2qbys7qia27ykvg5mw3r4kw5di7lkgihl5bqc1")))

(define-public crate-probly-search-1.1.3 (c (n "probly-search") (v "1.1.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0nyrxndv81m9gbc5nsrf7iz2m6m53862n88l1x9kxl67b8zbd8ks")))

(define-public crate-probly-search-1.2.3 (c (n "probly-search") (v "1.2.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0xz6q3mych7bmzzrclw55q5kpz2pxhqzrf9wl8p5mqp226l17clp") (y #t)))

(define-public crate-probly-search-1.2.4 (c (n "probly-search") (v "1.2.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0y23iasf70qy83b0drm2b4c80q6982qj2mnskskbffjipm1kvwf3")))

(define-public crate-probly-search-2.0.0-alpha-1 (c (n "probly-search") (v "2.0.0-alpha-1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "1rjk66jv9rlvqkkh27qlj0zwlri4aq3pxp0crywb0la9q5j7ym2d")))

(define-public crate-probly-search-2.0.0-alpha-2 (c (n "probly-search") (v "2.0.0-alpha-2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "1409w54ayzlni0pay1axikajpdyv4ir0lglcqmxanfclcyndgwjn")))

(define-public crate-probly-search-2.0.0 (c (n "probly-search") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "typed-generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0wq91h48xxzd91qgah8rl453nqmd64bic3ww9iim9dhkn7vcma5z")))

