(define-module (crates-io pr ob probables) #:use-module (crates-io))

(define-public crate-probables-0.1.0 (c (n "probables") (v "0.1.0") (h "0j1qqmd13jzj6vyhnyn6ax54aac4hc7f7nmcvz50qb7ffliid3yf")))

(define-public crate-probables-0.1.1 (c (n "probables") (v "0.1.1") (h "1nm8y84kkm5dasnyi36hmfb777mmakda5p4ag28dmlc6wc9yjli6")))

(define-public crate-probables-0.1.2 (c (n "probables") (v "0.1.2") (h "06i0hfl1jc0fsaq3mv9smir090nkkjfhma7322np2b4ghybkiihn")))

(define-public crate-probables-0.1.3 (c (n "probables") (v "0.1.3") (h "1s04rh3znzpkyxq7xb8xpjw9mqn8b7wsxfidykxsn3jggl88byga")))

(define-public crate-probables-0.1.4 (c (n "probables") (v "0.1.4") (h "1z2gx4l8v1m7hwc6q77gc2hhj79xkisg2aybhv3zjn8xs6fgzpj2")))

