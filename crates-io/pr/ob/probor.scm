(define-module (crates-io pr ob probor) #:use-module (crates-io))

(define-public crate-probor-0.1.0 (c (n "probor") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "cbor-codec") (r "*") (d #t) (k 0)))) (h "0p8nz6j036qlj8xnvfgv6f788a8rdafzqbc7viy66r3f7g8v7rhg")))

(define-public crate-probor-0.1.1 (c (n "probor") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "cbor-codec") (r "*") (d #t) (k 0)))) (h "066g0vkf9zj8na9m69giiha4myfb24nbpd2d2x5kpvqyqh1nv3xn")))

(define-public crate-probor-0.1.2 (c (n "probor") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "cbor-codec") (r "*") (d #t) (k 0)))) (h "1r22mb3bgifcdpdq80qai16cks1lc5khmp5w9kchn28xhri26x4a")))

(define-public crate-probor-0.1.3 (c (n "probor") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.44") (o #t) (d #t) (k 0)))) (h "0vp9iaxvzkivi6cibr0gr1zv5zh4xnl7ccbn3j1dgal3vz6giwc4") (f (quote (("regex_serde" "regex"))))))

(define-public crate-probor-0.2.0 (c (n "probor") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.44") (o #t) (d #t) (k 0)))) (h "1khriwyfihrs9p3mrq5mg4cvgsny7k530lv0lghyidzxmpg8h9qf") (f (quote (("regex_serde" "regex"))))))

(define-public crate-probor-0.2.1 (c (n "probor") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.44") (o #t) (d #t) (k 0)))) (h "0psg9fma1asmnrs8pv40p4pcb1h94csjmnzibzx4a82szc55qkr2") (f (quote (("regex_serde" "regex"))))))

(define-public crate-probor-0.3.0 (c (n "probor") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "03a1nw92ry2bv5kxvgd72p8w7z61hcfayyz9gxwyv0kp52dig829") (f (quote (("regex_serde" "regex"))))))

(define-public crate-probor-0.3.1 (c (n "probor") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0k9ii669vwp0n1h7yqjz8mxydq531i4ihl49b14ab8rbz08z8669") (f (quote (("regex_serde" "regex"))))))

