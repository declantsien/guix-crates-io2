(define-module (crates-io pr ob prob-rate-limiter) #:use-module (crates-io))

(define-public crate-prob-rate-limiter-0.1.0 (c (n "prob-rate-limiter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "governor") (r "^0.4.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "r8limit") (r "^0.2.2") (d #t) (k 2)))) (h "1rw5npd33lcy4f4q3iz4lp90llmsm98l6cyi4rhihnh094ji1nvm") (y #t)))

(define-public crate-prob-rate-limiter-0.1.1 (c (n "prob-rate-limiter") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "governor") (r "^0.4.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "r8limit") (r "^0.2.2") (d #t) (k 2)))) (h "0f32rxz3z6ymzikwxbywifqxpln9pppva7ia8pa5597kmjs4lqi2")))

