(define-module (crates-io pr ob problem-list) #:use-module (crates-io))

(define-public crate-problem-list-0.1.0 (c (n "problem-list") (v "0.1.0") (h "18714a2fq5wrjscm5kgy0y4wrw8y9zmbjxl1ghszrzh04fzg1qp1")))

(define-public crate-problem-list-0.1.1 (c (n "problem-list") (v "0.1.1") (h "1yj3xp25r598vznghc1dgx807lvgnridla79yaw15g5lckqwdc2n")))

