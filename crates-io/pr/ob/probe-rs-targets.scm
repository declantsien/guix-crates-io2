(define-module (crates-io pr ob probe-rs-targets) #:use-module (crates-io))

(define-public crate-probe-rs-targets-0.2.0 (c (n "probe-rs-targets") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 1)) (d (n "phf") (r "^0.7.24") (k 0)) (d (n "probe-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.2.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1agnpnwbp8474va7m19i8bypd0zywi1ypfxbhz91p8hmrygal87h")))

