(define-module (crates-io pr ob prob_prime) #:use-module (crates-io))

(define-public crate-prob_prime-0.1.0 (c (n "prob_prime") (v "0.1.0") (d (list (d (n "modular_arithmetic") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "07qy9gd3j439dx0h66macdjyjrba3yxh9qw21q2gkjvjyznfa2bg")))

(define-public crate-prob_prime-0.2.0 (c (n "prob_prime") (v "0.2.0") (d (list (d (n "modular_arithmetic") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "04icnldjbw9mr1lw7c7flcr1k59zj4d3p1pi5xwlvsk8b92ww4bv")))

