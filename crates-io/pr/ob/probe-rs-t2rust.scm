(define-module (crates-io pr ob probe-rs-t2rust) #:use-module (crates-io))

(define-public crate-probe-rs-t2rust-0.4.0 (c (n "probe-rs-t2rust") (v "0.4.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "142a49vnm2l35l7blid00rlqz5liv62ai2jprbn9l518w9gaplr1")))

(define-public crate-probe-rs-t2rust-0.5.0 (c (n "probe-rs-t2rust") (v "0.5.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0jfxm0q5d2jsn6nr4zb6kzka1r4g2mdxpprxgpfwps24crbqr1hg")))

(define-public crate-probe-rs-t2rust-0.6.0 (c (n "probe-rs-t2rust") (v "0.6.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "1q5as1053ws9hzw2b73bh9kc9wbp32q7yxlqdnjcis1kg8bjs3kf")))

(define-public crate-probe-rs-t2rust-0.7.0 (c (n "probe-rs-t2rust") (v "0.7.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0zsvqf6wzkgmsmh0aqzbj0fnx2kacvmqvgnhvd9w5gwf6qjnlsw5")))

