(define-module (crates-io pr ob probability_to_friendly_string) #:use-module (crates-io))

(define-public crate-probability_to_friendly_string-0.1.0 (c (n "probability_to_friendly_string") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18b0fidarl3gqvqah5jaz1q3vvfanjg1y1z5whz5q26sb2r2ab77")))

(define-public crate-probability_to_friendly_string-0.2.0 (c (n "probability_to_friendly_string") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1k61628vkw1kvrhakmizvlcw5ndf7w7s9ncj301lx7661yrwhxay")))

(define-public crate-probability_to_friendly_string-0.3.0 (c (n "probability_to_friendly_string") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p9snf2kkp1ajbwf6ayfppc3w3lwy8cr5jrxnph6b7x977h5aw3y")))

