(define-module (crates-io pr ob probability_cli) #:use-module (crates-io))

(define-public crate-probability_cli-0.0.1 (c (n "probability_cli") (v "0.0.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0352ws4l5gfbhnppc75jirjslkhmrhfy7hv371afw5qb0pv7nc61")))

(define-public crate-probability_cli-0.0.2 (c (n "probability_cli") (v "0.0.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "04rlyqq3f2bcqqr1dmk7dwp8p27jd2s1qsahilbkk7k56giw0kgv")))

