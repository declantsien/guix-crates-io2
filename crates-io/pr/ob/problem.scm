(define-module (crates-io pr ob problem) #:use-module (crates-io))

(define-public crate-problem-0.1.0 (c (n "problem") (v "0.1.0") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "138ipwi3v9pdxvn8g5gl3d64997536b7x5icgfi1zz6b7cf7b9fh")))

(define-public crate-problem-1.0.0 (c (n "problem") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "02bhn4w5p9mhkn3wfccwkwjrrl43fdyvzz2n7m1x4xkfkfda3zzc") (f (quote (("log-panic" "log") ("default" "log-panic" "backtrace"))))))

(define-public crate-problem-1.1.0 (c (n "problem") (v "1.1.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0s1f8zs388ixp4d4wk3ysv53k17y8hr2k60ayqza3s8yivhgnnpj") (f (quote (("log-panic" "log") ("default" "log-panic" "backtrace"))))))

(define-public crate-problem-2.0.0 (c (n "problem") (v "2.0.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0fa1wi34lw0k36i5x34d713s0dv2y27c43vl2p6b3s468swm54c4") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-3.0.0 (c (n "problem") (v "3.0.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "05wa1izvjx9y09nn97vxa9r2khfxvxlyplq29pxmnnqwvb3q0dd0") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-4.0.0 (c (n "problem") (v "4.0.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0dl7knmisvq9qamcfg356h68zfys9af1d3p3yy2r6ny1cljzilw1") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.0.0 (c (n "problem") (v "5.0.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0kz26z2ysg18574n5bcb3gc8hipsxvfhfbqg1jk9qi8p9amffw36") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.1.0 (c (n "problem") (v "5.1.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0gbv67g50chi39xsnzd3w7s0wjyrm5p8d4h4l8jrbc8kmkfwmssy") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.1.1 (c (n "problem") (v "5.1.1") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0wh7mc6n8hkfhfhn07amr3bry90qpx2nh32vmlzdzbrdmjgm1fmw") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.1.2 (c (n "problem") (v "5.1.2") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "03y85vvvbfmzidsa6j3nxbxg6yxdjjw0wdyazx2hwwggb4m82051") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.2.0 (c (n "problem") (v "5.2.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "1qzcm4df662842gxqrv8vlg22hqnsfmaym4d0m3nz407wqgm9ngr") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.2.1 (c (n "problem") (v "5.2.1") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)))) (h "14mjb1ghr5w7hybgwi90xxriv6yvfqd9kk0cv1066vvl9a2cv1l3") (f (quote (("default" "log" "backtrace"))))))

(define-public crate-problem-5.3.0 (c (n "problem") (v "5.3.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 2)))) (h "1vp1qdx3s6wdr3rsn1by327w0i5gwci4nyp3n0k9axd1w4m4scm5") (f (quote (("default" "log" "backtrace"))))))

