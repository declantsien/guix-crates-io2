(define-module (crates-io pr ob probe-rs-rtt) #:use-module (crates-io))

(define-public crate-probe-rs-rtt-0.1.0 (c (n "probe-rs-rtt") (v "0.1.0") (d (list (d (n "probe-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "07d09hsik0zj1c79nr1bbwi0a0rnzrb4g36kf8va9giyap4l6g42")))

(define-public crate-probe-rs-rtt-0.2.0 (c (n "probe-rs-rtt") (v "0.2.0") (d (list (d (n "probe-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1w3z6q0aab9j4dvimcg42yby16ddm2wkcd3pwglql72d2x320ysa")))

(define-public crate-probe-rs-rtt-0.3.0 (c (n "probe-rs-rtt") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "14qiaj1crmsbinippd5j1zkgpz29164z1bc2x6jpa54q1nlfjny4")))

(define-public crate-probe-rs-rtt-0.4.0 (c (n "probe-rs-rtt") (v "0.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "00wbrf9wvha9pivjny00wldxl6jgmc6cv80n1b8lv4sspaw250g0")))

(define-public crate-probe-rs-rtt-0.10.0 (c (n "probe-rs-rtt") (v "0.10.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0c4fr1wjxsq3k24qgay1bmjx5i72vqsv0n3s6lsiz0pkmyzfv898")))

(define-public crate-probe-rs-rtt-0.10.1 (c (n "probe-rs-rtt") (v "0.10.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1lh9i44210kii2pnr3ixx2xkyyvwjdqywf5ycri7rgcm31is64ns")))

(define-public crate-probe-rs-rtt-0.11.0 (c (n "probe-rs-rtt") (v "0.11.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.11.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0118p2c9ij5nl1b8l0i1sbsmcmgh0bdwrzmw4wwszwy8w7l0p56y")))

(define-public crate-probe-rs-rtt-0.12.0 (c (n "probe-rs-rtt") (v "0.12.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.12.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0lcdxl7fd7da0w23l8l9pxlc2kryz6jvxsqxjrwz18jzvnfz9a8p")))

(define-public crate-probe-rs-rtt-0.13.0 (c (n "probe-rs-rtt") (v "0.13.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "probe-rs") (r "^0.13.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1m4f63h91h37wn2qmhb9za95f8hp12qslqqnj6y95i4s4rd7zab8")))

(define-public crate-probe-rs-rtt-0.14.0 (c (n "probe-rs-rtt") (v "0.14.0") (d (list (d (n "probe-rs") (r "^0.14.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("log"))) (d #t) (k 0)))) (h "1hyrc6vq08j7cim6qa4rzx0dn1pajgkkb3nm5l6x60h4rbwf36g8")))

(define-public crate-probe-rs-rtt-0.14.1 (c (n "probe-rs-rtt") (v "0.14.1") (d (list (d (n "probe-rs") (r "^0.14.1") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("log"))) (d #t) (k 0)))) (h "18l5zwpq7blf96hrqbhaj1vm9i0ha02bwdg5xmsay7ajxi1i92nn")))

(define-public crate-probe-rs-rtt-0.14.2 (c (n "probe-rs-rtt") (v "0.14.2") (d (list (d (n "probe-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("log"))) (d #t) (k 0)))) (h "1zb03if9bnxrcng40dly2lkqvx6p5vx3ni1daki629nw7wygss37")))

