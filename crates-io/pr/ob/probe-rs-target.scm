(define-module (crates-io pr ob probe-rs-target) #:use-module (crates-io))

(define-public crate-probe-rs-target-0.11.0-alpha.4 (c (n "probe-rs-target") (v "0.11.0-alpha.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x9i9966wbww5h0njh43m6rwpff62avslzdccpwnqlnggp0pwwz3") (f (quote (("bincode"))))))

(define-public crate-probe-rs-target-0.11.0-alpha.5 (c (n "probe-rs-target") (v "0.11.0-alpha.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rvswqfp39vp7dknglg5y9fgrc54g38g3bmr599m48n3xpra1x88") (f (quote (("bincode"))))))

(define-public crate-probe-rs-target-0.11.0 (c (n "probe-rs-target") (v "0.11.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fsyfsh711fkfs63fsiphbrsfrjayk29s704ynghq0vkr6yaxlkc") (f (quote (("bincode"))))))

(define-public crate-probe-rs-target-0.12.0 (c (n "probe-rs-target") (v "0.12.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzi1rgbcjy4pn8dy4za5zzf5r6lf76v1xzjgna9brlp8qii15hg") (f (quote (("bincode"))))))

(define-public crate-probe-rs-target-0.13.0 (c (n "probe-rs-target") (v "0.13.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "02s3d1m6h4phbnxgsm4l2fzsmcvncfhshxplnzyspnv98d8ls0ja") (f (quote (("bincode"))))))

(define-public crate-probe-rs-target-0.14.0 (c (n "probe-rs-target") (v "0.14.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19qsqgxi4qfa92nx9g58wgf449nm9434f7c3syfgixynzzhxwq2i")))

(define-public crate-probe-rs-target-0.14.1 (c (n "probe-rs-target") (v "0.14.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08kgc5vpq1s6p6fp8wn4i8g9jdrvpxa4rm6ipvhsjrppd3314zj4")))

(define-public crate-probe-rs-target-0.14.2 (c (n "probe-rs-target") (v "0.14.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lx9yxh8103nk2mwi1pwnic6dpk7kc04w8f3n9d031s19gi066gh")))

(define-public crate-probe-rs-target-0.15.0 (c (n "probe-rs-target") (v "0.15.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17rk7akrk82ij5blg3y4264g3frwx5gj949vi87f6vm8939ac1sm")))

(define-public crate-probe-rs-target-0.16.0 (c (n "probe-rs-target") (v "0.16.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vj8sfjjjj80f4yr69dvcb7hhsbnxcngfrmxyvpiqcak9ashd5fw")))

(define-public crate-probe-rs-target-0.17.0 (c (n "probe-rs-target") (v "0.17.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09m9dwmmza3apdp5c7liqrn2nbd8hbjbh3jsi1pg79dy7zkzwya9")))

(define-public crate-probe-rs-target-0.18.0 (c (n "probe-rs-target") (v "0.18.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ka5qanjgf2gvm69j7mg3rysf7xsm6winss1ah5km3qj741xhx2b")))

(define-public crate-probe-rs-target-0.19.0 (c (n "probe-rs-target") (v "0.19.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h8lx8jic28fxnp4rkhwy379xysmnayw2pvncfxp4adyzdjj5ixr")))

(define-public crate-probe-rs-target-0.20.0 (c (n "probe-rs-target") (v "0.20.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ms5i8rynrpk46chggbrhr92l5q374jshh636i37lja3hiy56rnk")))

(define-public crate-probe-rs-target-0.21.0 (c (n "probe-rs-target") (v "0.21.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dbp9ixg05ihkdms9rk8fjcsprny9gmf2zh550rxxmm8wyi2dy3y")))

(define-public crate-probe-rs-target-0.21.1 (c (n "probe-rs-target") (v "0.21.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dpkpjq1c6qz2bbcwx9byx3dcqp3zyr4ym8l9274z007959z69r6")))

(define-public crate-probe-rs-target-0.22.0 (c (n "probe-rs-target") (v "0.22.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04d4rqsm3s2fy27bdagi4ymf6avzp0q22h8a0szwk15s2pbvcmgb")))

(define-public crate-probe-rs-target-0.23.0 (c (n "probe-rs-target") (v "0.23.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wdqg8ysqyzh8km4x7mbw932ddgblkmdpggr7dg87x7ih04h77kw")))

(define-public crate-probe-rs-target-0.24.0 (c (n "probe-rs-target") (v "0.24.0") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "jep106") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (f (quote ("serde"))) (d #t) (k 0)))) (h "157n56bc6j6687b9hc8ik2avj7577lkvpml3qmva8fllng6ajr86")))

