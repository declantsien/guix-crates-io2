(define-module (crates-io pr ob prob) #:use-module (crates-io))

(define-public crate-prob-0.1.0 (c (n "prob") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "083c2qbzh6mn6nwddba6ami1d9zffswka8dg24x4gz3ps1v5wddp")))

(define-public crate-prob-0.1.1 (c (n "prob") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "04gps8djfafg098zm5bf9vc8byxzzmlchwr52f0ha9dcxrjvy498")))

