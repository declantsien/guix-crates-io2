(define-module (crates-io pr ov proverbilo) #:use-module (crates-io))

(define-public crate-proverbilo-0.1.0 (c (n "proverbilo") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "19xv9bcih1iw5njar0wl2mw264c6r58gn6x0xbr6gfjxgn5wxxgy")))

(define-public crate-proverbilo-0.1.1 (c (n "proverbilo") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xmgwscvs6pn87vbwc5zixlmfq04xf9xyv619imwmm08v20x08qc")))

(define-public crate-proverbilo-0.1.2 (c (n "proverbilo") (v "0.1.2") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0q2151wcjbsq38za4k4r46rikv4lqivpibs473gpgrydgkpc2kkm")))

(define-public crate-proverbilo-0.1.3 (c (n "proverbilo") (v "0.1.3") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1dkmyski9qvifydbx8c50qb7wx1if98i3js511xgpnz5l5040szc")))

(define-public crate-proverbilo-0.1.4 (c (n "proverbilo") (v "0.1.4") (d (list (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "06pfl51gvz0kjh8krffj44wnw4vk3818xp30sl7zqvndnl4ff534")))

(define-public crate-proverbilo-0.1.5 (c (n "proverbilo") (v "0.1.5") (d (list (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1cixayql40aa5g1mrgdzpskhpf2m4p34z656bnibai9aqp6l5hxm")))

