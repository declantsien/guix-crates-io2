(define-module (crates-io pr ov prove) #:use-module (crates-io))

(define-public crate-prove-0.1.0 (c (n "prove") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1ndv4jiy81ij9lpvqnwq5vcm7ndlr8p7fyfjz7lclwsqpxkqiapv") (f (quote (("proc" "quote" "proc-macro2"))))))

(define-public crate-prove-0.1.1 (c (n "prove") (v "0.1.1") (d (list (d (n "idna") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "1d67y76swqmp2v5dfdklfc641q0qvl1dm2j0npp8kni2chh8hzz2") (f (quote (("proc" "quote" "proc-macro2"))))))

(define-public crate-prove-0.1.2 (c (n "prove") (v "0.1.2") (d (list (d (n "idna") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "0haxrir9d8s8vdp1bf5aq38cyxbqs83i52fq35ckd49gzqvhnmyr") (f (quote (("proc" "quote" "proc-macro2"))))))

(define-public crate-prove-0.1.3 (c (n "prove") (v "0.1.3") (d (list (d (n "idna") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "0jq6h7dfm93k13n7zyw4ng66jxjl2ww6pfcm0ihfyb74gyni49z7") (f (quote (("proc" "quote" "proc-macro2"))))))

(define-public crate-prove-0.1.4 (c (n "prove") (v "0.1.4") (d (list (d (n "idna") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "0sbdjil284isqf219qzifxmxn06q3wdqrklg3ssaxr8fvc1h7l58") (f (quote (("proc" "quote" "proc-macro2"))))))

(define-public crate-prove-0.1.5 (c (n "prove") (v "0.1.5") (d (list (d (n "idna") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "0ap2mmzn4z4g8fagc32znakza4sakq73wv83aj94lnxy5kfgw7d8") (f (quote (("proc" "quote" "proc-macro2"))))))

