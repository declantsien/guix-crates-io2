(define-module (crates-io pr ov prov-cosmwasm-storage) #:use-module (crates-io))

(define-public crate-prov-cosmwasm-storage-1.0.0-provbeta2 (c (n "prov-cosmwasm-storage") (v "1.0.0-provbeta2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1xgn5w6rsq2x2gkip7imrbhcx7xjncmw2zjs2gpp7bzasvi77gpd") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

