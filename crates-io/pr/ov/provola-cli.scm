(define-module (crates-io pr ov provola-cli) #:use-module (crates-io))

(define-public crate-provola-cli-0.1.0 (c (n "provola-cli") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0mk8paca345v4w4803hzgi8hkabrcbnrwc01vilvyh5wmc1aiq0l") (y #t)))

