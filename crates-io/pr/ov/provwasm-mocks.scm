(define-module (crates-io pr ov provwasm-mocks) #:use-module (crates-io))

(define-public crate-provwasm-mocks-0.13.2 (c (n "provwasm-mocks") (v "0.13.2") (d (list (d (n "cosmwasm-std") (r "^0.13.2") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "12gxsf3q9d1y0m0wqw9zach0xq0fwk5q1hijgkvknmyy8qzylix3")))

(define-public crate-provwasm-mocks-0.13.3 (c (n "provwasm-mocks") (v "0.13.3") (d (list (d (n "cosmwasm-std") (r "^0.13.2") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1vk2ahd9kxd6idlzg7shw3rscyzhxgshlwbycqxk8a5ikbq9rjfj")))

(define-public crate-provwasm-mocks-0.14.0-beta1 (c (n "provwasm-mocks") (v "0.14.0-beta1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1cclqm0s3fxwib70fr9rf7m4bs1blzbnvkya944z1k6qkqf4z5jr")))

(define-public crate-provwasm-mocks-0.14.0 (c (n "provwasm-mocks") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "11209j20b1494wn2mpjd67yd1qc3hs22cvz09invcj6a3m4498c6")))

(define-public crate-provwasm-mocks-0.14.1 (c (n "provwasm-mocks") (v "0.14.1") (d (list (d (n "cosmwasm-std") (r "^0.14.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.14.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hx1a326p6fsmms5bv47aal90xawbn4zvww5ykxh3d1hhlxh5a0i")))

(define-public crate-provwasm-mocks-0.14.2 (c (n "provwasm-mocks") (v "0.14.2") (d (list (d (n "cosmwasm-std") (r "^0.14.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.14.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0wwaqjsaicw6ppkljbaaw94dw6qn1lpv38nb6rar0dfqnpzzwrnp")))

(define-public crate-provwasm-mocks-0.14.3 (c (n "provwasm-mocks") (v "0.14.3") (d (list (d (n "cosmwasm-std") (r "^0.14.1") (f (quote ("staking"))) (d #t) (k 0)) (d (n "provwasm-std") (r "^0.14.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0yirhfakvgcnv67c05xn2kld4llg5pm6kz59645dvcjravqkm20d")))

(define-public crate-provwasm-mocks-0.16.0 (c (n "provwasm-mocks") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "provwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0jzxy9ay51v39vxvgr82rnllqcdwl1qbgcl4n40rlp70mv64v40s")))

(define-public crate-provwasm-mocks-1.0.0-beta (c (n "provwasm-mocks") (v "1.0.0-beta") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "provwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0lzdly1g60j4nlnj6gf3abcyvpy83vddr0ag5rm5hy9w6ihji7fk")))

(define-public crate-provwasm-mocks-1.0.0-beta2 (c (n "provwasm-mocks") (v "1.0.0-beta2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "provwasm-std") (r "^1.0.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0a1ymf62qgy15cc1xyryg07lq6wv46phhimqwid7w74hylml0104")))

(define-public crate-provwasm-mocks-1.0.0-rc.0 (c (n "provwasm-mocks") (v "1.0.0-rc.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-rc.0") (k 0)) (d (n "provwasm-std") (r "^1.0.0-rc.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0gqiahm084q6dwiz7lf799a9vn44b7ryyrr65mifxcc9992arxb9")))

(define-public crate-provwasm-mocks-1.0.0 (c (n "provwasm-mocks") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "provwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "17v9yrh3fmwikclq9vy5n9my99d2s2l60r0vygk9pz8b2sracl1d")))

(define-public crate-provwasm-mocks-1.1.0 (c (n "provwasm-mocks") (v "1.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "provwasm-std") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1wf46vs19813mq6abqd5gczdqixvvnkw8385z2mpqf9qj70hs2xr")))

(define-public crate-provwasm-mocks-1.1.1 (c (n "provwasm-mocks") (v "1.1.1") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (k 0)) (d (n "provwasm-std") (r "^1.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "10s65g3y22nlfilljx0gvr9mwhfd0m7vv00gslla2x11ylnlcza3")))

(define-public crate-provwasm-mocks-1.1.2 (c (n "provwasm-mocks") (v "1.1.2") (d (list (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "provwasm-std") (r "^1.1.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1rfzf8w484jpbmr8wq021shfcfl9x2sbc8w0jdz4bi2nl7p3rkvz")))

(define-public crate-provwasm-mocks-1.2.0 (c (n "provwasm-mocks") (v "1.2.0") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "provwasm-std") (r "^1.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0d1bwd49vjhls9ks51vx5hrmb5ic9cn4hh9q78514q93w937a0hh")))

(define-public crate-provwasm-mocks-2.0.0-rc1 (c (n "provwasm-mocks") (v "2.0.0-rc1") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "provwasm-common") (r "^0.1.0-rc1") (d #t) (k 0)) (d (n "provwasm-std") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (k 0)))) (h "0x5l5mhsfxlr54hxsp9rfhp642x3zhd9q3hcppc7yfbwgm4iq404")))

(define-public crate-provwasm-mocks-2.0.0 (c (n "provwasm-mocks") (v "2.0.0") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "provwasm-common") (r "^0.1.0") (d #t) (k 0)) (d (n "provwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (k 0)))) (h "1gd6sgs6s3w2pxhpjldfvlyq7rglnqx4swn4x9gwn1b2p8kmf5q2")))

(define-public crate-provwasm-mocks-2.1.0-rc1 (c (n "provwasm-mocks") (v "2.1.0-rc1") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "provwasm-common") (r "^0.1.0") (d #t) (k 0)) (d (n "provwasm-std") (r "^2.1.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (k 0)))) (h "0khvmdrrx2qn4f60baqid6vjcn4vaam8pj7ma6ky5grr65cacj86")))

(define-public crate-provwasm-mocks-2.1.0 (c (n "provwasm-mocks") (v "2.1.0") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "provwasm-common") (r "^0.1.0") (d #t) (k 0)) (d (n "provwasm-std") (r "^2.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (k 0)))) (h "0l4r628cmmdcpfjam399zhlzi5m1lfxbrjx362fmadzm86fkn571")))

(define-public crate-provwasm-mocks-2.2.0 (c (n "provwasm-mocks") (v "2.2.0") (d (list (d (n "cosmwasm-std") (r "^1.5") (f (quote ("cosmwasm_1_1"))) (k 0)) (d (n "provwasm-common") (r "^0.1.0") (d #t) (k 0)) (d (n "provwasm-std") (r "^2.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "derive"))) (k 0)))) (h "01i8qwghbqkw89zs3fsis9pdbns984qiwwsdq8dn77134myh3sgj")))

