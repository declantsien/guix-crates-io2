(define-module (crates-io pr ov proverb) #:use-module (crates-io))

(define-public crate-proverb-0.1.0 (c (n "proverb") (v "0.1.0") (h "0w28n9bly55j17ps5m7k4cfbaawi2rry152c18xsicdgfv1pwxvb")))

(define-public crate-proverb-0.1.1 (c (n "proverb") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13pbd27gy3v42mb2nikw9i5lbl1nb1983scjy3zmqcfj8p9d8pr9")))

(define-public crate-proverb-0.1.2 (c (n "proverb") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vbdm8ml8vwb7lgjs32kmvwbch3d0ifm760nrq5dd4aic5jgiz0w")))

(define-public crate-proverb-0.1.3 (c (n "proverb") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0c9zm7i0nzh4rxcqa0pscrdkzv92bnm75lndq8hf5nbx6y8681r7")))

(define-public crate-proverb-0.1.4 (c (n "proverb") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "12q77a0yyc5627by0fjhv02c6kg4g646kvqaak1df4pvdaca3xdy")))

