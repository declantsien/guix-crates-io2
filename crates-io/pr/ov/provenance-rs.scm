(define-module (crates-io pr ov provenance-rs) #:use-module (crates-io))

(define-public crate-provenance-rs-0.1.0 (c (n "provenance-rs") (v "0.1.0") (h "039iryvqw5hcgl9jm73kmizakg606ri6cwwzkf376bia7i3lg362")))

(define-public crate-provenance-rs-0.2.0 (c (n "provenance-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.1.1") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("json" "blocking" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "17dr0ak1abwcqly7plg5xxvdiclhg8gd7dnpw5vhg1cd1m02xahv")))

(define-public crate-provenance-rs-0.3.0 (c (n "provenance-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.1.1") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("json" "blocking" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1qngd3rqsdcs7jsc88xi6gmc4svmdhwncasfv8p3l71nlcgfwy6c")))

