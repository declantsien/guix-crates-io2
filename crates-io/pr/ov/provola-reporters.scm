(define-module (crates-io pr ov provola-reporters) #:use-module (crates-io))

(define-public crate-provola-reporters-0.1.7 (c (n "provola-reporters") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "provola-core") (r "^0.1.7") (d #t) (k 0)) (d (n "provola-terminalreporter") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "087plhq28c3i6dsn3dlpfy7p7vxhqg7lq77n2h0ksgm08nq7r4yk") (f (quote (("terminalreporter" "provola-terminalreporter") ("default" "terminalreporter"))))))

(define-public crate-provola-reporters-0.2.0 (c (n "provola-reporters") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)) (d (n "provola-terminalreporter") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0ws03cr9hmzlkjlhlf375qaaknvkhnxshd6xrzfn1z85ap34cjk8") (f (quote (("terminalreporter" "provola-terminalreporter") ("default" "terminalreporter"))))))

