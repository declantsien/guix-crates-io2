(define-module (crates-io pr ov prov-cosmwasm-derive) #:use-module (crates-io))

(define-public crate-prov-cosmwasm-derive-1.0.0-provbeta3 (c (n "prov-cosmwasm-derive") (v "1.0.0-provbeta3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kj6m4r1gz1ljhkcl7nkwpk8ch3x7wvzd2xxqjd33pas4wmxs6zy") (f (quote (("default"))))))

