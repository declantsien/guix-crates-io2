(define-module (crates-io pr ov provola-googletest) #:use-module (crates-io))

(define-public crate-provola-googletest-0.1.5 (c (n "provola-googletest") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "05s7qyy4k7ymrl737nm1rx6a7mwa78lbz12as6pr27k8mdxxgq3g")))

(define-public crate-provola-googletest-0.1.7 (c (n "provola-googletest") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "12jq911gjwwjc4r4dz3j4j4wp4g3bxl65fmqnciz6j0aqfcwmsr5")))

(define-public crate-provola-googletest-0.2.0 (c (n "provola-googletest") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "08pdwvix2hxy8vyvl776fa9lvhhcsxrzph1kf7n36s31xi29q7zy")))

