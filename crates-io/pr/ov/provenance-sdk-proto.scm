(define-module (crates-io pr ov provenance-sdk-proto) #:use-module (crates-io))

(define-public crate-provenance-sdk-proto-0.1.0 (c (n "provenance-sdk-proto") (v "0.1.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0w0qq202ysmbk5p5xw4yxlkd9pifn3hibdmdrkvwipz9psqxvanh")))

(define-public crate-provenance-sdk-proto-0.1.1 (c (n "provenance-sdk-proto") (v "0.1.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.16") (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0hnc8g9lmmb7mr24j0qrsm915y455g41k6z11x651wm4gfi6vwb0")))

