(define-module (crates-io pr ov provola-catch2) #:use-module (crates-io))

(define-public crate-provola-catch2-0.1.7 (c (n "provola-catch2") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0zbj4vrcm769jahikgw9fm74ksk9sj8a5nl48czq1j9x7g5gxqds")))

(define-public crate-provola-catch2-0.2.0 (c (n "provola-catch2") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "02hc028izc69xa7x48r0hgwjri54jv2zzn6fqy34cq2hpvl8byf0")))

