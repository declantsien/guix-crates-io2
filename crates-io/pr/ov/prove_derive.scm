(define-module (crates-io pr ov prove_derive) #:use-module (crates-io))

(define-public crate-prove_derive-0.1.0 (c (n "prove_derive") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "167vsq5bjfk5qqb82kpml3xbvr6yvnl61p9gsscxdl1ymsndknh0")))

(define-public crate-prove_derive-0.1.1 (c (n "prove_derive") (v "0.1.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.1") (f (quote ("proc"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jrgk8i126q83lp2zi3mg8x82bky5szszdn83nf8pyz4qhqhd3m5")))

(define-public crate-prove_derive-0.1.2 (c (n "prove_derive") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.2") (f (quote ("proc"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1px0s106h1i6cdhyb7crhgab4dw21azl9cki1i1yw4jma45xrgb3")))

(define-public crate-prove_derive-0.1.3 (c (n "prove_derive") (v "0.1.3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.3") (f (quote ("proc"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mqy94yjw7jk33m3znqy6a1g8v725929aim7fbybannfghabr5l1")))

(define-public crate-prove_derive-0.1.4 (c (n "prove_derive") (v "0.1.4") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.4") (f (quote ("proc"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06r4gxms0xvfvg5nqgyy5bxqgj217yvy869n5dzxknxa9zxpwnn5")))

(define-public crate-prove_derive-0.1.5 (c (n "prove_derive") (v "0.1.5") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "prove") (r "^0.1.5") (f (quote ("proc"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05s2wx6b76kvl18098dd3s1kl3xvhjm7xvcnwl2wargl4mnwrlzw")))

