(define-module (crates-io pr ov provwasm-stdv1) #:use-module (crates-io))

(define-public crate-provwasm-stdv1-0.0.1 (c (n "provwasm-stdv1") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "10wff0cnf5g6bpf5qziii021gwdbpfzqxj30g4xnjpqa3hjz9ar2") (f (quote (("backtraces"))))))

(define-public crate-provwasm-stdv1-0.0.2 (c (n "provwasm-stdv1") (v "0.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0pg6cgmqdzganbqq2p41c8hhs7ysqfwkmxj9vj4yjvpg2ak22pfw") (f (quote (("backtraces"))))))

