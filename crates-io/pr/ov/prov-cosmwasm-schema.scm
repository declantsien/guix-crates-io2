(define-module (crates-io pr ov prov-cosmwasm-schema) #:use-module (crates-io))

(define-public crate-prov-cosmwasm-schema-1.0.0-provbeta3 (c (n "prov-cosmwasm-schema") (v "1.0.0-provbeta3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v54q4qxaccxjcrapg2n178x6mn01v5xg4ckgl7yb82wssj62263")))

