(define-module (crates-io pr ov provola-testrunners) #:use-module (crates-io))

(define-public crate-provola-testrunners-0.1.5 (c (n "provola-testrunners") (v "0.1.5") (d (list (d (n "provola-core") (r "^0.1.5") (d #t) (k 0)) (d (n "provola-googletest") (r "^0.1.5") (d #t) (k 0)))) (h "1gbb7y45jzcaqg0s8knz6s9pc1prr6z60wkshdxlw844v786lzhb")))

(define-public crate-provola-testrunners-0.1.7 (c (n "provola-testrunners") (v "0.1.7") (d (list (d (n "provola-catch2") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "provola-core") (r "^0.1.7") (d #t) (k 0)) (d (n "provola-googletest") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0kf33azzyyqbq2fg0djqcsvx9sql27fd9igrzqx2znbgqzji2qk9") (f (quote (("googletest" "provola-googletest") ("default" "googletest" "catch2") ("catch2" "provola-catch2"))))))

(define-public crate-provola-testrunners-0.2.0 (c (n "provola-testrunners") (v "0.2.0") (d (list (d (n "provola-catch2") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)) (d (n "provola-googletest") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "026020l37f50d2bw77ag00r7hjj5bsndh6f71n2bck0f17wdk6hq") (f (quote (("googletest" "provola-googletest") ("default" "googletest" "catch2") ("catch2" "provola-catch2"))))))

