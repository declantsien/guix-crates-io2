(define-module (crates-io pr ov provwasm-common) #:use-module (crates-io))

(define-public crate-provwasm-common-0.1.0-rc1 (c (n "provwasm-common") (v "0.1.0-rc1") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (f (quote ("stargate"))) (d #t) (k 0)))) (h "00g0lwknr5b78rf14zb1mds9axczaa0a9b84mzcdcvf8z654z2gd")))

(define-public crate-provwasm-common-0.1.0 (c (n "provwasm-common") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (f (quote ("stargate"))) (d #t) (k 0)))) (h "0vdrfrrhgd9npay158wf6hp4322kkhnww88j42mbb625j6lf13wk")))

