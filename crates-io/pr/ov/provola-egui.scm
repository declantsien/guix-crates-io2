(define-module (crates-io pr ov provola-egui) #:use-module (crates-io))

(define-public crate-provola-egui-0.2.0 (c (n "provola-egui") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "eframe") (r "^0.16") (f (quote ("persistence"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)) (d (n "provola-reporters") (r "^0.2.0") (d #t) (k 0)) (d (n "provola-testrunners") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13gk2hlhah25cp9c56cq4mbampffrh3qkf70w0mrpjl6h5638cxg")))

