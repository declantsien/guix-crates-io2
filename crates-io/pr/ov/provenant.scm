(define-module (crates-io pr ov provenant) #:use-module (crates-io))

(define-public crate-provenant-0.1.0 (c (n "provenant") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19ya8rwnx3yyak4nb9dz56wx9cd6yd9sdqrqayknbkqzkc195adb")))

(define-public crate-provenant-0.1.1 (c (n "provenant") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0y92w0k4pjycwdrkvi98i5j6a325r53v9m36clkqw2027x7rc901")))

