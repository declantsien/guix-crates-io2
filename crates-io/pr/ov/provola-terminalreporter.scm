(define-module (crates-io pr ov provola-terminalreporter) #:use-module (crates-io))

(define-public crate-provola-terminalreporter-0.1.7 (c (n "provola-terminalreporter") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "provola-core") (r "^0.1.7") (d #t) (k 0)))) (h "1g9mwr0wyl59nwmxvfr38h9hjxzvghjx34ikb40527xglpj3m9l2")))

(define-public crate-provola-terminalreporter-0.2.0 (c (n "provola-terminalreporter") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "provola-core") (r "^0.2.0") (d #t) (k 0)))) (h "1k7h1vi1vhr6zpd5wzacf10m44f9s4akhijbl763m8bfxfvkmqcc")))

