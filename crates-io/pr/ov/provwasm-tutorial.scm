(define-module (crates-io pr ov provwasm-tutorial) #:use-module (crates-io))

(define-public crate-provwasm-tutorial-0.1.2 (c (n "provwasm-tutorial") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0") (d #t) (k 0)) (d (n "cw2") (r "^0.8.1") (d #t) (k 0)) (d (n "provwasm-mocks") (r "^1.0.0-beta2") (d #t) (k 2)) (d (n "provwasm-std") (r "^1.0.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1wv8bl2d00n82pk1n5dais1a56amav10dy4y3pcnw0r0g9qcz4w7") (f (quote (("backtraces"))))))

