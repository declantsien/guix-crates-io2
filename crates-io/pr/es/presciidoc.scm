(define-module (crates-io pr es presciidoc) #:use-module (crates-io))

(define-public crate-presciidoc-0.4.1 (c (n "presciidoc") (v "0.4.1") (d (list (d (n "bpaf") (r "^0.9") (f (quote ("derive" "bright-color"))) (d #t) (k 0)) (d (n "bpaf") (r "^0.9") (f (quote ("derive" "docgen"))) (d #t) (k 1)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 1)))) (h "0080z5cn4g6a82yb7aai2ka27bgapl5ypipxb0drryg4aq1fr426") (r "1.67")))

