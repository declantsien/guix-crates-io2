(define-module (crates-io pr es presage) #:use-module (crates-io))

(define-public crate-presage-0.1.0 (c (n "presage") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "presage-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0va5774bw1a5hy21vay3zzziy6iyb85pyl1xx22fzhcglj76fbk4") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:presage-macros"))))))

(define-public crate-presage-0.2.0 (c (n "presage") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "presage-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mwnfdnkxyvj3cgvs245a7w03gdmj04v8pv4fy2bgh1djnpvvlvx") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:presage-macros"))))))

(define-public crate-presage-0.3.0 (c (n "presage") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "presage-macros") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16nk4yrnbsksyqry8anp4613a1a608ix2xjkpslmigspdinh10c6") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:presage-macros"))))))

