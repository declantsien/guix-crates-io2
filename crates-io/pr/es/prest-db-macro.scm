(define-module (crates-io pr es prest-db-macro) #:use-module (crates-io))

(define-public crate-prest-db-macro-0.1.0 (c (n "prest-db-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "15bazxixdq47f8k78dpsn0ak9vz241kpsgr70s1li5nr5p90iq5q")))

(define-public crate-prest-db-macro-0.2.0 (c (n "prest-db-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0fgpaj1cwq55px0c0am3dlndcr3d6dsal87vm39vcaw6pi8nplg6")))

