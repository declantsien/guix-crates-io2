(define-module (crates-io pr es presto) #:use-module (crates-io))

(define-public crate-presto-0.1.0 (c (n "presto") (v "0.1.0") (d (list (d (n "curl") (r "^0.2.16") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^0.6.13") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6.14") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29.0") (d #t) (k 1)))) (h "0lvvx220f0qnchrr1kfm76fjfrhdhrxbbis8x1cp5nbr7wrf007y") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

