(define-module (crates-io pr es pressent) #:use-module (crates-io))

(define-public crate-pressent-1.0.0 (c (n "pressent") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1riaxlar7q1vpk7nhh0c71mxvgjxccpgjpa4z2l72vdimsqinahj")))

(define-public crate-pressent-1.0.1 (c (n "pressent") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "02gqv7nbqyp06080bp5bvfqdqbsv4hbd7xbchwkxps4zjcwq5qj6")))

(define-public crate-pressent-1.0.2 (c (n "pressent") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "157ldd5f5fdhzgfjdfam653x8bxlkl5ykmrf69nvj2ha0d28l12d")))

(define-public crate-pressent-1.1.0 (c (n "pressent") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "161q8imw99hwzkrfkvqpyfwkkfphm6z90yp5aip51mii8zd9n8da")))

(define-public crate-pressent-1.1.1 (c (n "pressent") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1n8hfp40vc84r626z2vajr9i6yknvlj7d28rainwhsprsyqdx3gq")))

(define-public crate-pressent-1.1.2 (c (n "pressent") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1z382pa2g9wdrvsywyhg7y33wa5k8824dgl1ajsbr0dfkzn86svs")))

