(define-module (crates-io pr es press) #:use-module (crates-io))

(define-public crate-press-0.1.0 (c (n "press") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.13") (f (quote ("serde"))) (d #t) (k 0)))) (h "04vzg7q98rf37z3ch6f48sxxrydmv2a0i8c7lqa4w5wrkc4wlxvp")))

