(define-module (crates-io pr es prest-html-macro) #:use-module (crates-io))

(define-public crate-prest-html-macro-0.1.0 (c (n "prest-html-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bz8idwfvcnmbr6mcaljzbdsz25rmzprvll59bb9dyl0snjvi01p")))

