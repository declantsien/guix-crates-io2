(define-module (crates-io pr es pressurize) #:use-module (crates-io))

(define-public crate-pressurize-0.1.0 (c (n "pressurize") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bcc") (r "^0.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "dogstatsd") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0") (d #t) (k 0)))) (h "198gsdrn1wmzw2dlnp1simqvxxjnym8wvjp7fdk3mcs4wzvyv4zx") (f (quote (("nightly")))) (y #t)))

(define-public crate-pressurize-0.0.1 (c (n "pressurize") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bcc") (r "^0.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "dogstatsd") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0") (d #t) (k 0)))) (h "1gdaiqjgj3gq2wcsgqm1gnr8wk8rmifv0gvqw840cp78nrq79x2q") (f (quote (("nightly"))))))

