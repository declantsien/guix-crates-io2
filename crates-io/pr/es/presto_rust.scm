(define-module (crates-io pr es presto_rust) #:use-module (crates-io))

(define-public crate-presto_rust-0.1.0 (c (n "presto_rust") (v "0.1.0") (h "1vjvhklzm8981wx2lsinb61iz2ng7kja3z4zni5wm1zq4f9xw52v")))

(define-public crate-presto_rust-0.1.1 (c (n "presto_rust") (v "0.1.1") (h "1fk0ippmjzk73xkr7hcdgwwphjcvqhpc4vf2acv3xy8n0di9ls9h") (y #t)))

