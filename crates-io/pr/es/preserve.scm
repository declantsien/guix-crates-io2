(define-module (crates-io pr es preserve) #:use-module (crates-io))

(define-public crate-preserve-0.1.0 (c (n "preserve") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6") (d #t) (k 0)) (d (n "rust-acd") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0djrq5abp7xldlmsvdac33a17j74ca6zwsyhj99w3nadccvjbl61")))

