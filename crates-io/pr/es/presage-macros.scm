(define-module (crates-io pr es presage-macros) #:use-module (crates-io))

(define-public crate-presage-macros-0.1.0 (c (n "presage-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05g8pa1ra0j5ny9jcfbldx3y6x8ihfswkgmq684g36s5kn6wlwq0")))

(define-public crate-presage-macros-0.2.0 (c (n "presage-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdrigdcfws3j5fiha11jmvsdgn7pv84rfga23r8yr2nhqm2rxhr")))

(define-public crate-presage-macros-0.3.0 (c (n "presage-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16dhk8acyzi9vqwg320swif5955m0k0mjj4b36050022l4a7ap7k")))

