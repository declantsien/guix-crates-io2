(define-module (crates-io pr es presser) #:use-module (crates-io))

(define-public crate-presser-0.1.1 (c (n "presser") (v "0.1.1") (d (list (d (n "gpu-allocator") (r "^0.15.1") (f (quote ("vulkan"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ibjg7xvbpqihgrbxlqgb8zh8hfcj7q98w6144mmp58sxy5416qy") (f (quote (("default")))) (r "1.56")))

(define-public crate-presser-0.2.0 (c (n "presser") (v "0.2.0") (h "00gnvvqm01fb60b98fa8f18nb00mnlfqpwjcx2ylyvplmky5176h") (f (quote (("default")))) (r "1.64")))

(define-public crate-presser-0.2.1 (c (n "presser") (v "0.2.1") (h "0mh8286magi2w7dwgrnpjfjjn065jf56dsjqz9pkk5gz37f1r5xn") (f (quote (("default")))) (r "1.64")))

(define-public crate-presser-0.3.0 (c (n "presser") (v "0.3.0") (h "1zz80rc2n9c39xpm8lgd5sxp6jxnvwak93xlang40830nw169fk0") (f (quote (("std") ("default" "std")))) (r "1.64")))

(define-public crate-presser-0.3.1 (c (n "presser") (v "0.3.1") (h "1ykvqx861sjmhkdh540aafqba7i7li7gqgwrcczy6v56i9m8xkz8") (f (quote (("std") ("default" "std")))) (r "1.64")))

