(define-module (crates-io pr es press-btn-continue) #:use-module (crates-io))

(define-public crate-press-btn-continue-0.1.0 (c (n "press-btn-continue") (v "0.1.0") (h "1wr8wxzk81b1immma48bpjs64b9cxhjncx8w0x3673r3xlrp21g2") (y #t)))

(define-public crate-press-btn-continue-0.1.1 (c (n "press-btn-continue") (v "0.1.1") (h "0q7i73zc3dqqw7d1vi49y4d70ryy7sfymdvfk65ba81ygykp2djd")))

(define-public crate-press-btn-continue-0.2.0 (c (n "press-btn-continue") (v "0.2.0") (h "08sfmld4h1y7pgk7p0np7blfb6vzpxn5m3zzkqlzfvv6wwh92fga")))

