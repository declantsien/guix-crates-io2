(define-module (crates-io pr es preserves-schema-macros) #:use-module (crates-io))

(define-public crate-preserves-schema-macros-0.992.1 (c (n "preserves-schema-macros") (v "0.992.1") (d (list (d (n "preserves") (r "^4.992.1") (d #t) (k 0)) (d (n "preserves-schema") (r "^5.992.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "11km97h8n8p946vz043ism9nyva7fq3z45aiz2fsdxmpz8p5s9k7")))

(define-public crate-preserves-schema-macros-0.993.0 (c (n "preserves-schema-macros") (v "0.993.0") (d (list (d (n "preserves") (r "^4.993.0") (d #t) (k 0)) (d (n "preserves-schema") (r "^5.993.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "0vnlbf5scfqs0gbr2y80n76j57k69s36lqbfxzmaxfz3hfrkns0w")))

(define-public crate-preserves-schema-macros-0.994.0 (c (n "preserves-schema-macros") (v "0.994.0") (d (list (d (n "preserves") (r "^4.994.0") (d #t) (k 0)) (d (n "preserves-schema") (r "^5.994.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1nndxx8j96xyycva292p09cd7bvq2slwlcyyak2ppb8jlwwl87mc")))

(define-public crate-preserves-schema-macros-0.995.0 (c (n "preserves-schema-macros") (v "0.995.0") (d (list (d (n "preserves") (r "^4.994.0") (d #t) (k 0)) (d (n "preserves-schema") (r "^5.994.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "04ixqzhdjkh49kha8nnz67a7gl5nf9av6jq9qj6rkg7szma30rjj")))

