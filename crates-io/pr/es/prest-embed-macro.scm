(define-module (crates-io pr es prest-embed-macro) #:use-module (crates-io))

(define-public crate-prest-embed-macro-0.1.0 (c (n "prest-embed-macro") (v "0.1.0") (d (list (d (n "prest-embed-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "015i8ifsr07fi47zwdjrwvpip68n771q8b8rd71viv8f8aj7glfk") (f (quote (("lazy-embed"))))))

