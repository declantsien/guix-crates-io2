(define-module (crates-io pr iq priq) #:use-module (crates-io))

(define-public crate-priq-0.1.0 (c (n "priq") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "032m80al3k6m944dvd6h0dhhpwk16hxhga0fm5kygfbpi5ddgx8m") (y #t)))

(define-public crate-priq-0.1.1 (c (n "priq") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0z7cbwkmd5bqqcrryicl1y62zlrjjirhis5ir67ajv0587jmx0yk")))

(define-public crate-priq-0.1.5 (c (n "priq") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "158hsycv0v3yz8p14yxbs3paqz8s7kyk0x822pqwcigvaynd71k2")))

(define-public crate-priq-0.1.6 (c (n "priq") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0na1rwdqsc77ffa1lnjmhi0wdr0cshg61hb8ncs4zfgkr43j6d1a")))

(define-public crate-priq-0.2.0 (c (n "priq") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1020p52qfd568qd41sy5gnm0h15ggv71iv61y02izbhsq419z3qs")))

