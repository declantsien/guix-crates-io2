(define-module (crates-io pr oo proof) #:use-module (crates-io))

(define-public crate-proof-0.0.0 (c (n "proof") (v "0.0.0") (d (list (d (n "eth2_ssz") (r "^0.1.2") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0s13qi7svvnfjxiaj8k3zw2wc367i4jylcnjnc1k5bah9dg40w1s")))

