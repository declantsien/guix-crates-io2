(define-module (crates-io pr oo proof_gen) #:use-module (crates-io))

(define-public crate-proof_gen-0.1.0 (c (n "proof_gen") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "evm_arithmetization") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plonky2") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "trace_decoder") (r "^0.1.0") (d #t) (k 0)))) (h "1apk79kh47h5bz8p4ww1nqj2j3rcll6qs9a0pjvynhg7qg7h98r4")))

(define-public crate-proof_gen-0.1.1 (c (n "proof_gen") (v "0.1.1") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "evm_arithmetization") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plonky2") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "trace_decoder") (r "^0.1.1") (d #t) (k 0)))) (h "1rxpba40rd28m541jl407x0zibpqdfg8qyw0hz2bgs6fgny06w35")))

(define-public crate-proof_gen-0.1.2 (c (n "proof_gen") (v "0.1.2") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "evm_arithmetization") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plonky2") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "trace_decoder") (r "^0.2.0") (d #t) (k 0)))) (h "0h2l2ak3w229lyj3vzkl6m5saygx1djl9ranjpni5b8k9qwawcmn")))

(define-public crate-proof_gen-0.1.3 (c (n "proof_gen") (v "0.1.3") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 0)) (d (n "evm_arithmetization") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plonky2") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)))) (h "1xq0nr7w4i7q2ksb65aqwiwlwd98b11gxsb9vjg7wqzv1zz8dawv")))

