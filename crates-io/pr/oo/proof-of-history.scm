(define-module (crates-io pr oo proof-of-history) #:use-module (crates-io))

(define-public crate-proof-of-history-0.1.0 (c (n "proof-of-history") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "17z1h7dy0y0j50jiaajzjj9wcrzb4lh0m43f0as7g68qmycs7d4c")))

