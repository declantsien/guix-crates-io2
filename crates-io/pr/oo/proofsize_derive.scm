(define-module (crates-io pr oo proofsize_derive) #:use-module (crates-io))

(define-public crate-proofsize_derive-0.1.0 (c (n "proofsize_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1i8b1fpy2svk0fs6gnm005rz92rx5aivv67iyg9c8kpkc2q8jy6f")))

