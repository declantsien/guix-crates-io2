(define-module (crates-io pr oo proof-fair) #:use-module (crates-io))

(define-public crate-proof-fair-0.1.0 (c (n "proof-fair") (v "0.1.0") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1hiyd25x8v5rryjcvxvrblss9fvfqknli40dyl5qyirfl4h7206z")))

