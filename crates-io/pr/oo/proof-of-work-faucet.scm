(define-module (crates-io pr oo proof-of-work-faucet) #:use-module (crates-io))

(define-public crate-proof-of-work-faucet-0.1.0 (c (n "proof-of-work-faucet") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)))) (h "00jf3m6wvz2gam3xqi1sw0klgmwnp770hxcrqf47lw8h5hpnh4gk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

