(define-module (crates-io pr ou proudcat) #:use-module (crates-io))

(define-public crate-proudcat-0.0.1 (c (n "proudcat") (v "0.0.1") (h "00h3p2qjhyn2xgss20qlhs8mgicx0lq5zrw7nzfanijn34sz99f5")))

(define-public crate-proudcat-0.1.0 (c (n "proudcat") (v "0.1.0") (h "0js1wiyvb5bmybzdzba620afjld35b2sf45kfny93hwc8hvnlg50")))

(define-public crate-proudcat-0.1.1 (c (n "proudcat") (v "0.1.1") (h "0vbrbvz07063lrlg8psqnb51cabd84js8zyl2lcpk451sgk6lm19")))

(define-public crate-proudcat-0.1.2 (c (n "proudcat") (v "0.1.2") (h "16wc0p2f1k6613y44fyn2c12pcnq76basij4ynywrqizmyi6ca2a")))

(define-public crate-proudcat-0.2.0 (c (n "proudcat") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0gaqdjns4m1p20pq8172h7l37fxn05xw1zq2gldq9ahin9rx7ci7")))

