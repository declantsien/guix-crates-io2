(define-module (crates-io pr ou prout) #:use-module (crates-io))

(define-public crate-prout-0.2.0 (c (n "prout") (v "0.2.0") (d (list (d (n "chumsky") (r "^0.8") (d #t) (k 0)))) (h "1zyphsn76drh2z334w855cp4dsancsgqa4dhy1dzvj39ywy0g3fn") (y #t)))

(define-public crate-prout-0.3.0 (c (n "prout") (v "0.3.0") (d (list (d (n "chumsky") (r "^0.8") (d #t) (k 0)))) (h "1dgi6carsw9vp3q29lw3imgqjl0p1p85viwg25g39g0ay3gsf20a") (y #t)))

(define-public crate-prout-0.3.5 (c (n "prout") (v "0.3.5") (h "1p1w5ddmzqdkclw2j0hbciaqlsymwl3b2v4lnc42qbkj37z8klva")))

