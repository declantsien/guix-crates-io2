(define-module (crates-io pr kc prkcst) #:use-module (crates-io))

(define-public crate-prkcst-0.1.0 (c (n "prkcst") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0940mzfpn2c1n6n525sl88xng7k4hfrn91vsjwfniwj73ir727m1")))

(define-public crate-prkcst-0.2.0 (c (n "prkcst") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1bwf6w51bvsvw6l7nmwid2y1v16r2lh0wvf4r16ds1cd87im7vsl")))

(define-public crate-prkcst-0.2.1 (c (n "prkcst") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0xwrawpaq2qiflqg792ydazw4fjpv2wwwi2687v4103gf6cg7wkw")))

(define-public crate-prkcst-0.3.0 (c (n "prkcst") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0nmb8771n3p1bs1zajrv16ccyr2i06xff6glaq7fg4m2hkpw60cz")))

(define-public crate-prkcst-0.4.0 (c (n "prkcst") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "05gpl7w658n948163qbnpb2cz7wj8mdk00byic30qb2jvvbbfgrb")))

(define-public crate-prkcst-0.4.1 (c (n "prkcst") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0ck73bcbv6y9l7ivzzbymmygs0hy5wdbbds0fch3bbds3qcmwxxa")))

