(define-module (crates-io pr c- prc-rs-derive) #:use-module (crates-io))

(define-public crate-prc-rs-derive-0.1.0 (c (n "prc-rs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1nczky94zbsx2h8hsmlx691p3fvvbvs0c0axrpfwp0kg7d0ri9ir")))

(define-public crate-prc-rs-derive-0.1.1 (c (n "prc-rs-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1aa0pb5lf747jz17lfq2zzgcyhsah7y1mcrln27hlqi7c1frpd65")))

(define-public crate-prc-rs-derive-0.1.2 (c (n "prc-rs-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0fg0dwfz767l2cr6bamvav5vj0mbbs61g92fl8ddm944s9myv2kj")))

(define-public crate-prc-rs-derive-0.2.0 (c (n "prc-rs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "048yb7cqdzpcqd9z2g3anhkim7dwy24jclz7x326v8r7k9zimas6")))

