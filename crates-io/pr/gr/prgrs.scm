(define-module (crates-io pr gr prgrs) #:use-module (crates-io))

(define-public crate-prgrs-0.2.0 (c (n "prgrs") (v "0.2.0") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)))) (h "1ixv33dj6jx4qsxx8k6k3y1fhpvw7l8s4ha7yf0isfpfbja9dlcc") (y #t)))

(define-public crate-prgrs-0.3.0 (c (n "prgrs") (v "0.3.0") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)))) (h "0xcbm4lyqimvfyqayxgifq0sjbkwc2vg44ra1pk0527zdic0m7j6") (y #t)))

(define-public crate-prgrs-0.3.1 (c (n "prgrs") (v "0.3.1") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)))) (h "1zicz2199w2gampwi0jgbq1z9pafmxi6c1cyq9g1n63yn1xbpzfh") (y #t)))

(define-public crate-prgrs-0.4.0 (c (n "prgrs") (v "0.4.0") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)))) (h "1afkiypp4agcb01px2snm11n7ky4ws4p7px3074mxv509052xki5") (y #t)))

(define-public crate-prgrs-0.5.0 (c (n "prgrs") (v "0.5.0") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "093fiy5d31iafmysfqgbc30bn74mdhn811jpxfw8cj5093dspqah")))

(define-public crate-prgrs-0.5.1 (c (n "prgrs") (v "0.5.1") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "02923n27p6r96w3pqb75ywvvfhm8prqz755f4rlniqzcvv8lgyji")))

(define-public crate-prgrs-0.5.2 (c (n "prgrs") (v "0.5.2") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "15g02745wcshdi29pfws1ycay62g4nmvh03s2yam7yaj3xdh4jf7")))

(define-public crate-prgrs-0.5.3 (c (n "prgrs") (v "0.5.3") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "1x0zwg17nmz3rzawnr8j6pz264bnq1ip42qszhp9pb9iv98kd5pi")))

(define-public crate-prgrs-0.5.4 (c (n "prgrs") (v "0.5.4") (d (list (d (n "terminal") (r "^0.2") (f (quote ("crossterm-backend"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "1rsmszpxm0yi68by8i20258fya43agawacsikh8zd9c0jxcxzzaw")))

(define-public crate-prgrs-0.6.0 (c (n "prgrs") (v "0.6.0") (d (list (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "12kbjzqcc67yh2cj7hh40y137cc42gqyl0qa5yrkpaiszrmdidj3")))

(define-public crate-prgrs-0.6.1 (c (n "prgrs") (v "0.6.1") (d (list (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "1jj34f796rxc0ykxy4h5mh51f9bljivqw80j90jb95b1b7rvi1dd")))

(define-public crate-prgrs-0.6.2 (c (n "prgrs") (v "0.6.2") (d (list (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "1gm4mky2jrx5iikmik4hapyfysnjkwhb2kvkxyjwdm2gf1i1p754")))

(define-public crate-prgrs-0.6.3 (c (n "prgrs") (v "0.6.3") (d (list (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "1y5rl922hpgs3aa9bk0wzx2nm76qi7y83byywdzaxnxmjibipbg9")))

(define-public crate-prgrs-0.6.4 (c (n "prgrs") (v "0.6.4") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1sb25hm22rbp3m6nbqghz8jdpi6fxg7ykhd7y9v7gdd5lpaj6nk5")))

