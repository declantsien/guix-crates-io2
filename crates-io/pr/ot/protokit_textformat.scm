(define-module (crates-io pr ot protokit_textformat) #:use-module (crates-io))

(define-public crate-protokit_textformat-0.0.1 (c (n "protokit_textformat") (v "0.0.1") (d (list (d (n "binformat") (r "^0.0.1") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qyrkhjff8yp855iv26nmh7cxc1752m7q4f3q45grk5krdav9i54") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.0.2 (c (n "protokit_textformat") (v "0.0.2") (d (list (d (n "binformat") (r "^0.0.2") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s8y5smwnvnc4rjf90n9zh3ffyk7q9wfwl6lirbysh470dzgh7jv") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.0.3 (c (n "protokit_textformat") (v "0.0.3") (d (list (d (n "binformat") (r "^0.0.3") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zy7gpzmxn913c687cy4735rsrr6xykl818jlk6pj5lxvx1zbbn3") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.1.1 (c (n "protokit_textformat") (v "0.1.1") (d (list (d (n "binformat") (r "^0.1.1") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b93wrnvk22jrwcv7873lmix58rx15lybsf48gpx92gwjical3sf") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.1.2 (c (n "protokit_textformat") (v "0.1.2") (d (list (d (n "binformat") (r "^0.1.2") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0aj7yl1fxphag9rwl9sans5nw0kj7z3n33v81s523gn5p2j42scm") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.1.3 (c (n "protokit_textformat") (v "0.1.3") (d (list (d (n "binformat") (r "^0.1.3") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qk9wvs1d37ls89qgi8in9spxkh9gsprcdr0rvi3h0s8qks2wp83") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.1.4 (c (n "protokit_textformat") (v "0.1.4") (d (list (d (n "binformat") (r "^0.1.4") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16x013r6bnaanxbh08d3v3xfsrpcb0fwbz1hp55krsg6m6wdm1kv") (f (quote (("default"))))))

(define-public crate-protokit_textformat-0.2.0 (c (n "protokit_textformat") (v "0.2.0") (d (list (d (n "binformat") (r "^0.2.0") (d #t) (k 0) (p "protokit_binformat")) (d (n "logos") (r "^0.13.0") (f (quote ("export_derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xgl2zgnvwyvi5728l2jxm3kzn8bvvw00fvxbhqxpr8r90rgipca") (f (quote (("default"))))))

