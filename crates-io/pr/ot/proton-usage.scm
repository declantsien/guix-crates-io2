(define-module (crates-io pr ot proton-usage) #:use-module (crates-io))

(define-public crate-proton-usage-0.1.0 (c (n "proton-usage") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yn7mhb7j7gpys5vzjwf2zhl3x2rcki5bkcbxh0c1ad4fnqxapbp") (y #t)))

