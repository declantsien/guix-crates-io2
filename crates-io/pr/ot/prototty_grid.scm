(define-module (crates-io pr ot prototty_grid) #:use-module (crates-io))

(define-public crate-prototty_grid-0.7.1 (c (n "prototty_grid") (v "0.7.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.7.1") (d #t) (k 0)))) (h "06l1j051nc39vc5kd4clmskkqacq71m00mf3mvqw1pximmfxk96g")))

(define-public crate-prototty_grid-0.8.0 (c (n "prototty_grid") (v "0.8.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.0") (d #t) (k 0)))) (h "1xq0aaxxdychncqzw7iqa6gy3gjx974bhyj61zyvjjaf008ap7qp")))

(define-public crate-prototty_grid-0.8.1 (c (n "prototty_grid") (v "0.8.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.1") (d #t) (k 0)))) (h "122py4rhznba7832cdlc4bg48rckginjmpmmwmpigqhkis1knxy6")))

(define-public crate-prototty_grid-0.8.2 (c (n "prototty_grid") (v "0.8.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.2") (d #t) (k 0)))) (h "05nyrq5cnvcvyghlim7imchr9901liyzhccmw8d0si7s9g72dfkg")))

(define-public crate-prototty_grid-0.9.0 (c (n "prototty_grid") (v "0.9.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.9.0") (d #t) (k 0)))) (h "0v3qglrjgdsqi3mr5cny9g4qqkmx9r32gc35bw4zvk3ffibgxnjk")))

(define-public crate-prototty_grid-0.10.0 (c (n "prototty_grid") (v "0.10.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.10.0") (d #t) (k 0)))) (h "08vp6vcxdi4m2ja3v29cgiyv39zxk9y5q4m4bkz37nghaak699cl")))

(define-public crate-prototty_grid-0.11.0 (c (n "prototty_grid") (v "0.11.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.0") (d #t) (k 0)))) (h "0z3inwcqlllahvv8ccvcqsvqxbv7lz9m0f3jh6qn7j2qqbbm87il")))

(define-public crate-prototty_grid-0.11.1 (c (n "prototty_grid") (v "0.11.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.1") (d #t) (k 0)))) (h "1yi788vii7zglinbh0np1896yfdl0pvksm553h2v6xy97haf06cw")))

(define-public crate-prototty_grid-0.11.2 (c (n "prototty_grid") (v "0.11.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.2") (d #t) (k 0)))) (h "1mnkm7lh6cncdrf7zv75w9d6g7cmwqw2dxqngdjmjyn67df18cz3")))

(define-public crate-prototty_grid-0.11.3 (c (n "prototty_grid") (v "0.11.3") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.3") (d #t) (k 0)))) (h "13dpdnzkdh7p4n175bczd05nq3gz0gqqhkfvywnhmf6wn6ar26zw")))

(define-public crate-prototty_grid-0.12.0 (c (n "prototty_grid") (v "0.12.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.12.0") (d #t) (k 0)))) (h "1n25j3ghhc7qdl5kxxs5rzzwqxx9yr3vjcmybylxpjwwp97pxrh0")))

(define-public crate-prototty_grid-0.13.0 (c (n "prototty_grid") (v "0.13.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.13.0") (d #t) (k 0)))) (h "0spzdpcnvhb7hacvjp54i0avppsmf22ls0ngbii3apngxdihj47y")))

(define-public crate-prototty_grid-0.14.0 (c (n "prototty_grid") (v "0.14.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.14.0") (d #t) (k 0)))) (h "0j65m3iq00qwhizrzi6rghqw3g126qg34yh5gf5jvjigblnq4cdx")))

(define-public crate-prototty_grid-0.15.0 (c (n "prototty_grid") (v "0.15.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.15.0") (d #t) (k 0)))) (h "0m9mky0di0ssa9ma2gfiscjqb6h69v55wv195b6s608lgz891av6")))

(define-public crate-prototty_grid-0.16.0 (c (n "prototty_grid") (v "0.16.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.16.0") (d #t) (k 0)))) (h "1vwaz1lkh2sg4jys9vjw8wv2cma3vqsl308q7rxlfi1k68h771m2")))

(define-public crate-prototty_grid-0.17.0 (c (n "prototty_grid") (v "0.17.0") (d (list (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17.0") (d #t) (k 0)))) (h "0qxkb64x1jp9k2w7f7wn9v5jfpw8rfw6jhv5qj8hb6b4h4axxys0")))

(define-public crate-prototty_grid-0.17.1 (c (n "prototty_grid") (v "0.17.1") (d (list (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17") (d #t) (k 0)))) (h "1pcm9iy6gbdkms7pai43wlzkgbh59jpa60disyg1bq5b4rdhblg4")))

(define-public crate-prototty_grid-0.18.0 (c (n "prototty_grid") (v "0.18.0") (d (list (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.18") (d #t) (k 0)))) (h "1lacvfnd0fs3phsmgd4lwdi7jhj1rkhk5gdpgfyxsfqd9cj56cfj")))

(define-public crate-prototty_grid-0.19.0 (c (n "prototty_grid") (v "0.19.0") (d (list (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.19") (d #t) (k 0)))) (h "17l1n022gnb6mjmzq65n71qnp8flvgvqqzqc9pmhk8x3w82gd7ph")))

(define-public crate-prototty_grid-0.20.0 (c (n "prototty_grid") (v "0.20.0") (d (list (d (n "prototty") (r "^0.20") (d #t) (k 0)))) (h "09548frgls491m6mfcdamr7n679qnpwl0vxv4mf7n2gf9qqzhaf9")))

(define-public crate-prototty_grid-0.21.0 (c (n "prototty_grid") (v "0.21.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty") (r "^0.21") (d #t) (k 0)))) (h "0pwprnj76c6hrcfdjhgacp5r0k9qprx81v4qx3c1b6aai4dvp4kq") (f (quote (("serialize" "prototty/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.22.0 (c (n "prototty_grid") (v "0.22.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.22") (d #t) (k 0)))) (h "0li7nqp1c4pyskav06rapb1acqlcdv8fxdz1vq4k6gas9270lybz") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.23.0 (c (n "prototty_grid") (v "0.23.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.23") (d #t) (k 0)))) (h "1d4p7j31ar22g0l28g1xb33679d2ng539hl30dln2xpzg3lk6l9f") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.24.0 (c (n "prototty_grid") (v "0.24.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.24") (d #t) (k 0)))) (h "13yzqg4iidlv4wfc8vnhfwh3nbcbgk46hk43xahqh1f6535gd0rl") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.25.0 (c (n "prototty_grid") (v "0.25.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.25") (d #t) (k 0)))) (h "0mvdpsbfpp7h6fcpq5y7c1s2b7a73rpgw8nzfamjif1w11sf36b0") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.26.0 (c (n "prototty_grid") (v "0.26.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.26") (d #t) (k 0)))) (h "0nj95xq8pk3vq50hlmwnm3z70kfls9dz2hwmd1sbh0cb73wx4np7") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

(define-public crate-prototty_grid-0.27.0 (c (n "prototty_grid") (v "0.27.0") (d (list (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "prototty_render") (r "^0.27") (d #t) (k 0)))) (h "1d1l7m7lsw30prvnn0kadr33qfkvj9715n3kzi8hrall0cv12hmg") (f (quote (("serialize" "prototty_render/serialize" "grid_2d/serialize"))))))

