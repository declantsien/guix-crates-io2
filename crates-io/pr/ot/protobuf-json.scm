(define-module (crates-io pr ot protobuf-json) #:use-module (crates-io))

(define-public crate-protobuf-json-0.1.0 (c (n "protobuf-json") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.3") (d #t) (k 0)))) (h "073ryjpw5rpfhb0yf9hkx7l8pfaq5sw4pqvz65r9c82672jlsmkv")))

(define-public crate-protobuf-json-0.2.0 (c (n "protobuf-json") (v "0.2.0") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1rj2fxpgi8lzvz4ajcywhb8ip337f75khypq2a8wjycbyh7lxp42")))

(define-public crate-protobuf-json-0.3.0 (c (n "protobuf-json") (v "0.3.0") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qsvnysihl2j6g5mcx637nfv4g5rrlfqqqdsw1a8nnxc7hyd3s9f")))

