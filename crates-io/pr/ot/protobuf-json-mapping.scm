(define-module (crates-io pr ot protobuf-json-mapping) #:use-module (crates-io))

(define-public crate-protobuf-json-mapping-3.0.0-alpha.14 (c (n "protobuf-json-mapping") (v "3.0.0-alpha.14") (d (list (d (n "protobuf") (r "=3.0.0-alpha.14") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0-alpha.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yqlxsnjm97cv6df0aky6y1v380b6jawrv8j99ssv84jc5ad6dl7")))

(define-public crate-protobuf-json-mapping-3.0.0 (c (n "protobuf-json-mapping") (v "3.0.0") (d (list (d (n "protobuf") (r "=3.0.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qcfi3k8skhbwbmvzfgn6g8vhqshbh02c64gm7m901nxf5bz9730")))

(define-public crate-protobuf-json-mapping-3.0.1 (c (n "protobuf-json-mapping") (v "3.0.1") (d (list (d (n "protobuf") (r "=3.0.1") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rbil9xsl6gwl416vmhy2rhr36qbvvb51b80j20kmz7pgkvbragf")))

(define-public crate-protobuf-json-mapping-3.0.2 (c (n "protobuf-json-mapping") (v "3.0.2") (d (list (d (n "protobuf") (r "=3.0.2") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ch1vbl508wa4ndh26w3ybkq94ay5pbaavp2cd7ns6181gxjxm7q")))

(define-public crate-protobuf-json-mapping-3.0.3 (c (n "protobuf-json-mapping") (v "3.0.3") (d (list (d (n "protobuf") (r "=3.0.3") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0a8v24jcfh6clj12xgpnjmmlykadj3226abrbz26sg53nsx10x2m")))

(define-public crate-protobuf-json-mapping-3.1.0 (c (n "protobuf-json-mapping") (v "3.1.0") (d (list (d (n "protobuf") (r "=3.1.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06paj8n7yqywzg15k2afgd4qfp9p9fs4qv9waisdm5iymcp10mxy")))

(define-public crate-protobuf-json-mapping-3.2.0 (c (n "protobuf-json-mapping") (v "3.2.0") (d (list (d (n "protobuf") (r "=3.2.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nwam34dyncggn8cls7zingnqii71i22yqlxyxwj3rim1khgw6ff")))

(define-public crate-protobuf-json-mapping-3.3.0 (c (n "protobuf-json-mapping") (v "3.3.0") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0i7cadbqk843nan3rz5is6whkwvfd0h1rf8g4ssjkck60slkjc2j")))

(define-public crate-protobuf-json-mapping-3.4.0 (c (n "protobuf-json-mapping") (v "3.4.0") (d (list (d (n "protobuf") (r "=3.4.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gk9082r2228wh9pc7j6a6p71xq37xqwqrqzx3m9vn3098l4112x")))

