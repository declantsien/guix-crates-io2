(define-module (crates-io pr ot protoql) #:use-module (crates-io))

(define-public crate-protoql-0.1.0 (c (n "protoql") (v "0.1.0") (d (list (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "keyvalint") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "tuple_key") (r "^0.5") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.5") (d #t) (k 0)) (d (n "zerror") (r "^0.4") (d #t) (k 0)) (d (n "zerror_core") (r "^0.5") (d #t) (k 0)) (d (n "zerror_derive") (r "^0.3") (d #t) (k 0)))) (h "1rwdip6yf99z7vp6j34mgamq17q1shwzypskrq0vc31nfqpxazpy") (f (quote (("default" "binaries") ("binaries"))))))

