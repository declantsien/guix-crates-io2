(define-module (crates-io pr ot protobuf-native) #:use-module (crates-io))

(define-public crate-protobuf-native-0.1.0 (c (n "protobuf-native") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.60") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.60") (d #t) (k 1)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)))) (h "1h578vdv4yy9ghc4mmrj24x9fbqndqibq8flz4ihc4k0z5jzymi4")))

(define-public crate-protobuf-native-0.2.0+3.19.1 (c (n "protobuf-native") (v "0.2.0+3.19.1") (d (list (d (n "cxx") (r "^1.0.62") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.62") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "17vhhvzmyvn88sqviik5z4jlkxv4k4m5wypcxzklrzmh9xibx7cs")))

(define-public crate-protobuf-native-0.2.1+3.19.1 (c (n "protobuf-native") (v "0.2.1+3.19.1") (d (list (d (n "cxx") (r "^1.0.62") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.62") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0kkz7xk7bb1hm9za1nw1lqghhj4bvhd8zwm9x0v314x6yb87dpw6")))

(define-public crate-protobuf-native-0.2.2+3.19.1 (c (n "protobuf-native") (v "0.2.2+3.19.1") (d (list (d (n "cxx") (r "^1.0.62") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.62") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "13wf0fmmlbyw1gb0gdz5w0my43jfxgz6mwz6iqsg50r8jl1fnzsp")))

(define-public crate-protobuf-native-0.3.0+26.1 (c (n "protobuf-native") (v "0.3.0+26.1") (d (list (d (n "cxx") (r "^1.0.122") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.122") (d #t) (k 1)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "protobuf-src") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0a639s3xzy35bzhqk07088jc6g1xlzvf9cayvwinrchg8spw2sfg")))

