(define-module (crates-io pr ot prototty_elements) #:use-module (crates-io))

(define-public crate-prototty_elements-0.3.0 (c (n "prototty_elements") (v "0.3.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "003f3avlbh86hhxs87xzpzqph5qvhf5whpn3qar1hq11z3pb7lzm")))

(define-public crate-prototty_elements-0.3.1 (c (n "prototty_elements") (v "0.3.1") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p7vwd6c9939p68y4gl4kfbyzfwkkbdxvzr88jqvn8w44nz8bjj0")))

(define-public crate-prototty_elements-0.5.0 (c (n "prototty_elements") (v "0.5.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15xfj3cpglbnykqbiv5hmxdngyw9lj9x010y8xd5a1a16yqacxpf")))

(define-public crate-prototty_elements-0.6.0 (c (n "prototty_elements") (v "0.6.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xign5mm7n1srgkv687r9si3bjv0sb0vbz831gdn61m9faivjqjr")))

(define-public crate-prototty_elements-0.7.0 (c (n "prototty_elements") (v "0.7.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0v9glpiwm3kb51851a7znjl2jygfcg9qfrmj8iyhha2p7lx375ch")))

