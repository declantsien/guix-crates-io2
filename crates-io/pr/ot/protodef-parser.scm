(define-module (crates-io pr ot protodef-parser) #:use-module (crates-io))

(define-public crate-protodef-parser-0.1.0 (c (n "protodef-parser") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1kc8h065fwfd94laqmc81zq4250wvyhiyqp3b3cljpklp5c8fx48")))

