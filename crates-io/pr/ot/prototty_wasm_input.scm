(define-module (crates-io pr ot prototty_wasm_input) #:use-module (crates-io))

(define-public crate-prototty_wasm_input-0.22.0 (c (n "prototty_wasm_input") (v "0.22.0") (d (list (d (n "prototty_input") (r "^0.22") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "06l4nmwqs8w6ykrf14zvxwcjkvd6kf6z7ck03sa5qp1g3pb8lqfg") (f (quote (("serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_wasm_input-0.23.0 (c (n "prototty_wasm_input") (v "0.23.0") (d (list (d (n "prototty_input") (r "^0.23") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1gvj2rpasdc8yy0lfwsnzlrc5ksnaqfd486c5dh5nc4a3b7jxak0") (f (quote (("serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_wasm_input-0.24.0 (c (n "prototty_wasm_input") (v "0.24.0") (d (list (d (n "prototty_input") (r "^0.24") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1bz26mkfj9vrq2h79zg3aaizrnhx4l29g2b6ipi7k46r5rsypjzp") (f (quote (("serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_wasm_input-0.25.0 (c (n "prototty_wasm_input") (v "0.25.0") (d (list (d (n "prototty_input") (r "^0.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "06xyqwfkk59vamff1lmasdbz4qnzf5crj2ivbsrbqfrr1jjym66y") (f (quote (("serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_wasm_input-0.26.0 (c (n "prototty_wasm_input") (v "0.26.0") (d (list (d (n "prototty_input") (r "^0.26") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "15zqy5sp5l32vg6lx1rfq9acimakblg0c84q7yj957qx5zd1y739") (f (quote (("serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_wasm_input-0.27.0 (c (n "prototty_wasm_input") (v "0.27.0") (d (list (d (n "prototty_input") (r "^0.27") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0wpxlh74hr3lhfgrfvdbwr0wb4wb6168jazgqaldyyvkfp6wwgsw") (f (quote (("serialize" "prototty_input/serialize"))))))

