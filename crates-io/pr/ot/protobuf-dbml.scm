(define-module (crates-io pr ot protobuf-dbml) #:use-module (crates-io))

(define-public crate-protobuf-dbml-0.1.0-beta.2 (c (n "protobuf-dbml") (v "0.1.0-beta.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dbml-rs") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "02vw92c5rk1xigcsh21vhxyi0nz53c8l8s95nvc4rhcgcsza704f") (f (quote (("default"))))))

(define-public crate-protobuf-dbml-0.1.0-beta.3 (c (n "protobuf-dbml") (v "0.1.0-beta.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dbml-rs") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "1ggjf1kayj5zrjnwszslvcrrjl4yg6fgfskig4vzhw3dmyxcrjd1") (f (quote (("default"))))))

