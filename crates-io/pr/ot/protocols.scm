(define-module (crates-io pr ot protocols) #:use-module (crates-io))

(define-public crate-protocols-0.0.1 (c (n "protocols") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "signals") (r "^0.0.2") (d #t) (k 0)))) (h "056wah44xd6zrxm128wbg9bhcggnkq1h4rg7698j3j7vc6azddb1")))

