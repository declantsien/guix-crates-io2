(define-module (crates-io pr ot protobuf-src) #:use-module (crates-io))

(define-public crate-protobuf-src-0.1.0+3.19.1 (c (n "protobuf-src") (v "0.1.0+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "1jlwd5b662wi0c19cw2gx9rczv9g4vsgg5qsx02i4086dcjyd9n5") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.0+3.19.1 (c (n "protobuf-src") (v "1.0.0+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "0gmcrl5wh9k81rzvigwgfj8kzagi8vzcy5fwnx0vh0xjhj3gv9zq") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.1+3.19.1 (c (n "protobuf-src") (v "1.0.1+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "1jkmkrr7ay3bni9x1r0dn6y4rwwy8lrr8drmzd35w7nksbqjz5f0") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.2+3.19.1 (c (n "protobuf-src") (v "1.0.2+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "0irv2dg96kqbn17iwf5q11rcbv7swckx956fpsnah8axz243bry0") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.3+3.19.1 (c (n "protobuf-src") (v "1.0.3+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "0sxf75bcb84c9an06in7rzf0rkh978ng5yda3gm8bbpgs1p0n0hy") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.4+3.19.1 (c (n "protobuf-src") (v "1.0.4+3.19.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "07vglkkklbvd1r6k2gb5bk2sc0yck2big9gdzb26d3xbjsmn288m") (l "protobuf-src")))

(define-public crate-protobuf-src-1.0.5+3.19.3 (c (n "protobuf-src") (v "1.0.5+3.19.3") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)))) (h "0y7h8f07si1ca31c28d8609jnkjj47dbbknbrgw4hzvnz65zcmzy") (l "protobuf-src")))

(define-public crate-protobuf-src-1.1.0+21.5 (c (n "protobuf-src") (v "1.1.0+21.5") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (k 1)))) (h "1hg3w2799fdlrr2wjf7i9g8ybzy0jgxlcdmrhgxwcg7bp998ib67") (l "protobuf-src")))

(define-public crate-protobuf-src-2.0.0+26.1 (c (n "protobuf-src") (v "2.0.0+26.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "05qnivwl7niy1b4snxjqaflvjnxl8kbvxfabh7b34gn1ra6ys5wn") (l "protobuf-src")))

(define-public crate-protobuf-src-2.0.1+26.1 (c (n "protobuf-src") (v "2.0.1+26.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1rryfcyn9srp3a5nxkryvcb3bf9lyj5kiklcdf99ih4x9gx1rfpq") (l "protobuf-src")))

