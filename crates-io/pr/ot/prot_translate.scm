(define-module (crates-io pr ot prot_translate) #:use-module (crates-io))

(define-public crate-prot_translate-0.1.0 (c (n "prot_translate") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 2)))) (h "1cc6f0pjvr7gv3kd23w6mjmx825iypqx4iss3l421bh7csn20pif")))

