(define-module (crates-io pr ot protocol-ftp-client) #:use-module (crates-io))

(define-public crate-protocol-ftp-client-0.1.0 (c (n "protocol-ftp-client") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "041c548ij01rzczni8q00l99x0i05rwnvxv4rsbn7x7k3cawnpfq")))

(define-public crate-protocol-ftp-client-0.1.1 (c (n "protocol-ftp-client") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0pjdcd5q21rdilq01z5x03ck3d3csg5z64jc7hvqhig1490z75kb")))

