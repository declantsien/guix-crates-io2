(define-module (crates-io pr ot protoc-gen-prost-crate) #:use-module (crates-io))

(define-public crate-protoc-gen-prost-crate-0.0.1 (c (n "protoc-gen-prost-crate") (v "0.0.1") (h "053raj6girhqc6icpg9a5ncxza0a1g38pdggczd8xvl2ajx9lzr2")))

(define-public crate-protoc-gen-prost-crate-0.1.0 (c (n "protoc-gen-prost-crate") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0y7sclg9ll8sma0pvrlqnk0mk178r01pqidjr6msddiisxabcj3n")))

(define-public crate-protoc-gen-prost-crate-0.1.1 (c (n "protoc-gen-prost-crate") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0f1mz9x2n6qd90qpq82nrbly7dbjpkl0fia89hhivh2hnx3kf57f")))

(define-public crate-protoc-gen-prost-crate-0.1.2 (c (n "protoc-gen-prost-crate") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "08h1i12jky10rydmbyrg0qlh0w5sz5ib8iy1gnd68kvczw4xxkrv")))

(define-public crate-protoc-gen-prost-crate-0.1.3 (c (n "protoc-gen-prost-crate") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "12jlzqvfwsqkp05dpiwl7635sr6pdc684ldg8cziyfnqvbwcg3sd")))

(define-public crate-protoc-gen-prost-crate-0.1.4 (c (n "protoc-gen-prost-crate") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0pvcr2hnk9r8j7rz52jlq3j21s6k93sgxnnjx98bn01mdyw5a71w")))

(define-public crate-protoc-gen-prost-crate-0.1.5 (c (n "protoc-gen-prost-crate") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "058snzw7wvqawg9lmdfy1l0vji8ydhhsc7r2cfl221j0m913lx1r")))

(define-public crate-protoc-gen-prost-crate-0.1.6 (c (n "protoc-gen-prost-crate") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "09d67szfj4lysq3j96s2bb2nvw25y9hkmknm9c23wzx9mybp3q9j")))

(define-public crate-protoc-gen-prost-crate-0.2.0 (c (n "protoc-gen-prost-crate") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "18993g5jg2hskb3b8km71v2ap1mqyff63zfz5wgx4jzfr6dz787q")))

(define-public crate-protoc-gen-prost-crate-0.3.0 (c (n "protoc-gen-prost-crate") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.0") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "023p4n83c1cpy222r4r4l7x14df6n1rfzh06gkq1rywiq5w0y9l6")))

(define-public crate-protoc-gen-prost-crate-0.3.1 (c (n "protoc-gen-prost-crate") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r ">=0.11.0, <0.11.2") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1vrn9rzsyrx5dcfwnqls0l2hy3d1k5iqd2rsfrcylgrry25yjgh4")))

(define-public crate-protoc-gen-prost-crate-0.4.0 (c (n "protoc-gen-prost-crate") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.3") (k 0)) (d (n "prost-types") (r "^0.12.3") (k 0)) (d (n "protoc-gen-prost") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "01hq0bwxl3waacrzgkrj0mapz1a9xb61is51bj428l81a5ncpck7")))

