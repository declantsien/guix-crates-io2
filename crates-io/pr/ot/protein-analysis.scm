(define-module (crates-io pr ot protein-analysis) #:use-module (crates-io))

(define-public crate-protein-analysis-0.0.1 (c (n "protein-analysis") (v "0.0.1") (d (list (d (n "dihedral") (r "^0.0") (f (quote ("f32"))) (d #t) (k 0)) (d (n "protein-core") (r "^0.1") (d #t) (k 0)))) (h "13ayivkqc18ldddkzi9a908qfjw5sn1axa7qilgmh9z1vgxxqnb7")))

