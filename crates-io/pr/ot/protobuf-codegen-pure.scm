(define-module (crates-io pr ot protobuf-codegen-pure) #:use-module (crates-io))

(define-public crate-protobuf-codegen-pure-1.6.0 (c (n "protobuf-codegen-pure") (v "1.6.0") (d (list (d (n "protobuf") (r "^1.6.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^1.6.0") (d #t) (k 0)))) (h "05jmnkv9kzmy6bk3m0jkppqilmqpwz6sjqscx5fxrcwf2j57f6h1")))

(define-public crate-protobuf-codegen-pure-2.0.0 (c (n "protobuf-codegen-pure") (v "2.0.0") (d (list (d (n "protobuf") (r "^2.0.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0.0") (d #t) (k 0)))) (h "0mz65pbpxq6l7g90wpczrlnhkfa6cpq7vs9niw59xj47wmcrfwm8")))

(define-public crate-protobuf-codegen-pure-2.0.1 (c (n "protobuf-codegen-pure") (v "2.0.1") (d (list (d (n "protobuf") (r "^2.0.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0.1") (d #t) (k 0)))) (h "0rjhbyqiv4pq9721qghwikrf2ygvx9shx0vwjg9vads8vpniz2bf")))

(define-public crate-protobuf-codegen-pure-2.0.2 (c (n "protobuf-codegen-pure") (v "2.0.2") (d (list (d (n "protobuf") (r "= 2.0.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.2") (d #t) (k 0)))) (h "1wc2pq4cc005hh6b5bkz9w0kvp8dlmlz54bahl5y5dz9j49mdc0x")))

(define-public crate-protobuf-codegen-pure-2.0.3 (c (n "protobuf-codegen-pure") (v "2.0.3") (d (list (d (n "protobuf") (r "= 2.0.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.3") (d #t) (k 0)))) (h "06qsb21l86pk7rg135vawmcfa93w4r7ifgx35vbwljv4n22k1k3z")))

(define-public crate-protobuf-codegen-pure-2.0.4 (c (n "protobuf-codegen-pure") (v "2.0.4") (d (list (d (n "protobuf") (r "= 2.0.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.4") (d #t) (k 0)))) (h "1b1qkbhdsr8g5idlkc0l6kzkdbn1vzb72yzvsrflzy3inql08wrh")))

(define-public crate-protobuf-codegen-pure-2.0.5 (c (n "protobuf-codegen-pure") (v "2.0.5") (d (list (d (n "protobuf") (r "= 2.0.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.5") (d #t) (k 0)))) (h "0ddsjlkrj83k3h9griyx939wkdd06kh8x5nn1y4znpq6nnwa02km")))

(define-public crate-protobuf-codegen-pure-2.1.0 (c (n "protobuf-codegen-pure") (v "2.1.0") (d (list (d (n "protobuf") (r "= 2.1.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.0") (d #t) (k 0)))) (h "0vlyrvn941d1i82zxxm944n5fjajj2hjfazm2rfknz0zd0h25v9c")))

(define-public crate-protobuf-codegen-pure-2.1.1 (c (n "protobuf-codegen-pure") (v "2.1.1") (d (list (d (n "protobuf") (r "= 2.1.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.1") (d #t) (k 0)))) (h "1szl9psi7dc9cmfmrj1sqp2l5l3v6i1g3sv96yr7i7kvw2hxp3za")))

(define-public crate-protobuf-codegen-pure-2.1.2 (c (n "protobuf-codegen-pure") (v "2.1.2") (d (list (d (n "protobuf") (r "= 2.1.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.2") (d #t) (k 0)))) (h "1hqyyml6dvjqd7fzdkz2v8akh6ajvlna6fknn9ll9ihz3waxpr94")))

(define-public crate-protobuf-codegen-pure-2.1.3 (c (n "protobuf-codegen-pure") (v "2.1.3") (d (list (d (n "protobuf") (r "= 2.1.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.3") (d #t) (k 0)))) (h "0wd50823nh7bimn33nhr0fk9svwnlsj509y39gaddadxhvb1c2q4")))

(define-public crate-protobuf-codegen-pure-2.1.4 (c (n "protobuf-codegen-pure") (v "2.1.4") (d (list (d (n "protobuf") (r "= 2.1.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.4") (d #t) (k 0)))) (h "1x2x8g25jajdmryfrwbc6kp34jx6491brajxkqhna3zcqbazchrr")))

(define-public crate-protobuf-codegen-pure-2.2.0 (c (n "protobuf-codegen-pure") (v "2.2.0") (d (list (d (n "protobuf") (r "= 2.2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.0") (d #t) (k 0)))) (h "0yd3kwzrpc9b1xizz5xn7lpiia9c8nbmnccv6ykxbfv4xi04j3lv")))

(define-public crate-protobuf-codegen-pure-2.2.1 (c (n "protobuf-codegen-pure") (v "2.2.1") (d (list (d (n "protobuf") (r "= 2.2.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.1") (d #t) (k 0)))) (h "1j6pfjf1sszncaawxcchvik9fflkzg3myxy7l91cqhg1ccih2mnr")))

(define-public crate-protobuf-codegen-pure-2.2.2 (c (n "protobuf-codegen-pure") (v "2.2.2") (d (list (d (n "protobuf") (r "= 2.2.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.2") (d #t) (k 0)))) (h "08f4qmfy982f9ckaq7v906bsw018xiv4zg9iv1mw340yhp7b94qj")))

(define-public crate-protobuf-codegen-pure-2.2.3 (c (n "protobuf-codegen-pure") (v "2.2.3") (d (list (d (n "protobuf") (r "= 2.2.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.3") (d #t) (k 0)))) (h "1yw5md1rmi1fkkbfhsm75mbfkb1xasx0v8bvyc2wx8wbhmp0rhpp")))

(define-public crate-protobuf-codegen-pure-2.2.4 (c (n "protobuf-codegen-pure") (v "2.2.4") (d (list (d (n "protobuf") (r "= 2.2.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.4") (d #t) (k 0)))) (h "1ikvjnv5wybx8vrr9b2ixpb97k8px05r972kx1pmchy02x6mamzw")))

(define-public crate-protobuf-codegen-pure-2.1.5 (c (n "protobuf-codegen-pure") (v "2.1.5") (d (list (d (n "protobuf") (r "= 2.1.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.5") (d #t) (k 0)))) (h "1jl9hq4hjk7vsmypsfaydl9ynxm5x6aysrwmv2m4gkga18a70jzq")))

(define-public crate-protobuf-codegen-pure-2.0.6 (c (n "protobuf-codegen-pure") (v "2.0.6") (d (list (d (n "protobuf") (r "= 2.0.6") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.6") (d #t) (k 0)))) (h "0cr9fgr9ad4aacyhbdglympigxxbgwbyr2icm3hk88477xl7b4ri")))

(define-public crate-protobuf-codegen-pure-2.2.5 (c (n "protobuf-codegen-pure") (v "2.2.5") (d (list (d (n "protobuf") (r "= 2.2.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.5") (d #t) (k 0)))) (h "1hsab00i1i267mcz1d7rxywksxzc0v4vgjpbfz4s0lchc2mxdal9")))

(define-public crate-protobuf-codegen-pure-2.3.0 (c (n "protobuf-codegen-pure") (v "2.3.0") (d (list (d (n "protobuf") (r "= 2.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.3.0") (d #t) (k 0)))) (h "0kf4gwg555fciawpvjfylpqzqqyaqgha8kmsq021wp1kzvx0iyal")))

(define-public crate-protobuf-codegen-pure-2.3.1 (c (n "protobuf-codegen-pure") (v "2.3.1") (d (list (d (n "protobuf") (r "= 2.3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.3.1") (d #t) (k 0)))) (h "15sbypkzchc9l14xbjffvd3bkz5i9xdmy2sn7q20lmk7lj4178rb")))

(define-public crate-protobuf-codegen-pure-2.4.0 (c (n "protobuf-codegen-pure") (v "2.4.0") (d (list (d (n "protobuf") (r "= 2.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.4.0") (d #t) (k 0)))) (h "1fq3420m1khb1x553xpcc8swcwi3y05355rq9abnawp0wzdsbds8")))

(define-public crate-protobuf-codegen-pure-2.4.2 (c (n "protobuf-codegen-pure") (v "2.4.2") (d (list (d (n "protobuf") (r "= 2.4.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.4.2") (d #t) (k 0)))) (h "06fn4zxa0rfd4kdghw62q28xf7fphipfks3y78gmf9mpfmqg2y0i")))

(define-public crate-protobuf-codegen-pure-2.5.0 (c (n "protobuf-codegen-pure") (v "2.5.0") (d (list (d (n "protobuf") (r "= 2.5.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.5.0") (d #t) (k 0)))) (h "1zdmnyi8a280hmly8r2mqqyfxrls8m2zfm7s73a81ilq7kv87z8l")))

(define-public crate-protobuf-codegen-pure-2.6.0 (c (n "protobuf-codegen-pure") (v "2.6.0") (d (list (d (n "protobuf") (r "= 2.6.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.0") (d #t) (k 0)))) (h "1d6jw3p9qd5fb0hw0sl996hypragyllh9m7s9ba2fhcrrqgr15dk")))

(define-public crate-protobuf-codegen-pure-2.6.1 (c (n "protobuf-codegen-pure") (v "2.6.1") (d (list (d (n "protobuf") (r "= 2.6.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.1") (d #t) (k 0)))) (h "0daf3xzrlig69rlpn99vy8brkzl6ra4iyb8j9lj4shlk3pgcx1nh")))

(define-public crate-protobuf-codegen-pure-2.6.2 (c (n "protobuf-codegen-pure") (v "2.6.2") (d (list (d (n "protobuf") (r "= 2.6.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.2") (d #t) (k 0)))) (h "1js8y8kc179d3hcly232p5wb7qd0bqfkqn38y8jh1z4pj0jx34d7")))

(define-public crate-protobuf-codegen-pure-2.7.0 (c (n "protobuf-codegen-pure") (v "2.7.0") (d (list (d (n "protobuf") (r "= 2.7.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.7.0") (d #t) (k 0)))) (h "08abr4qi7fmky3xb6qvfy8f3cmzqwkb4iq61sbs42gyc21bcg9qk")))

(define-public crate-protobuf-codegen-pure-2.8.0 (c (n "protobuf-codegen-pure") (v "2.8.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.0") (d #t) (k 0)))) (h "05j68566436j3zsf6fmsvs36vz99zllnqssz0259vkxzzg2kv680")))

(define-public crate-protobuf-codegen-pure-2.8.1 (c (n "protobuf-codegen-pure") (v "2.8.1") (d (list (d (n "protobuf") (r "= 2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.1") (d #t) (k 0)))) (h "006nbh4jc2rwwvrs7xf0wbd3vx2d6a2a9x5zn0l5p7rilp6nlr61")))

(define-public crate-protobuf-codegen-pure-2.9.0 (c (n "protobuf-codegen-pure") (v "2.9.0") (d (list (d (n "protobuf") (r "= 2.9.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.9.0") (d #t) (k 0)))) (h "04mvbkl0yiw97nlaps8f4c334f0wqnyfligmdwiwqgkdac1xqmgg") (y #t)))

(define-public crate-protobuf-codegen-pure-2.8.2 (c (n "protobuf-codegen-pure") (v "2.8.2") (d (list (d (n "protobuf") (r "= 2.8.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.2") (d #t) (k 0)))) (h "04iwvs1lfcxlsx5akixqhw8gdc2rjrlzlx97nv5679zl313p27pp")))

(define-public crate-protobuf-codegen-pure-2.10.0 (c (n "protobuf-codegen-pure") (v "2.10.0") (d (list (d (n "protobuf") (r "= 2.10.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.0") (d #t) (k 0)))) (h "1l64mwhff0cnv75vcinrm9kijya946ig2r370jws6bixlbzp6m6v")))

(define-public crate-protobuf-codegen-pure-2.10.1 (c (n "protobuf-codegen-pure") (v "2.10.1") (d (list (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.1") (d #t) (k 0)))) (h "1j8mrhj2cmx7fgbmzcq3d27ird3axasql84hpqrk61xnb8nv8z2a")))

(define-public crate-protobuf-codegen-pure-2.10.2 (c (n "protobuf-codegen-pure") (v "2.10.2") (d (list (d (n "protobuf") (r "= 2.10.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.2") (d #t) (k 0)))) (h "07ab7sf9ch36m11i2sxaqrj1pnqbrrgkrm9f4fzb70zf22bs8zq3")))

(define-public crate-protobuf-codegen-pure-2.10.3 (c (n "protobuf-codegen-pure") (v "2.10.3") (d (list (d (n "protobuf") (r "= 2.10.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.3") (d #t) (k 0)))) (h "0imxy1dww83g39dy1x04bh1n4hh2ghay8gll6rrgyqzpi9775079")))

(define-public crate-protobuf-codegen-pure-2.11.0 (c (n "protobuf-codegen-pure") (v "2.11.0") (d (list (d (n "protobuf") (r "= 2.11.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.11.0") (d #t) (k 0)))) (h "174j6zkxxbsdl7gp5pp2sfhfvidrx6ds1cx0y306pfz98k28a05j")))

(define-public crate-protobuf-codegen-pure-2.12.0 (c (n "protobuf-codegen-pure") (v "2.12.0") (d (list (d (n "protobuf") (r "= 2.12.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.12.0") (d #t) (k 0)))) (h "09xajfgms7is91727zxnc7iky9z0fbqalmbvqqxqgm9r9ix17h0j")))

(define-public crate-protobuf-codegen-pure-2.13.0 (c (n "protobuf-codegen-pure") (v "2.13.0") (d (list (d (n "protobuf") (r "= 2.13.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.13.0") (d #t) (k 0)))) (h "0b8kj48gbc4h0l8a15cb104aa6zya3dppd9gw5ph5lr83ybk6vlh")))

(define-public crate-protobuf-codegen-pure-2.14.0 (c (n "protobuf-codegen-pure") (v "2.14.0") (d (list (d (n "protobuf") (r "= 2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.14.0") (d #t) (k 0)))) (h "0h34gfqlb7bqmgqv1mfgy5wk35z5r2h5ki3p3pdcmw1vqzmly6id")))

(define-public crate-protobuf-codegen-pure-2.15.0 (c (n "protobuf-codegen-pure") (v "2.15.0") (d (list (d (n "protobuf") (r "=2.15.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.15.0") (d #t) (k 0)))) (h "0k39nmf4p7rj2q286gq3lq918b6lvqw20ji25gc664g6bdvrqb07")))

(define-public crate-protobuf-codegen-pure-2.15.1 (c (n "protobuf-codegen-pure") (v "2.15.1") (d (list (d (n "protobuf") (r "=2.15.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.15.1") (d #t) (k 0)))) (h "1i5qzjzyjgk153wpvq6qcl730428lv1im8lzl1wdzqycprdph3cn")))

(define-public crate-protobuf-codegen-pure-2.16.0 (c (n "protobuf-codegen-pure") (v "2.16.0") (d (list (d (n "protobuf") (r "=2.16.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.0") (d #t) (k 0)))) (h "1ilmsvi8asxgpk4g9syyrddqj07nirn6i9rl6mvl936qx318p5j3")))

(define-public crate-protobuf-codegen-pure-2.16.1 (c (n "protobuf-codegen-pure") (v "2.16.1") (d (list (d (n "protobuf") (r "=2.16.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.1") (d #t) (k 0)))) (h "0fszf1grm7si4xlq1anxi5cp9mnnxla08wy5yv11jwmm2g7w4ny2")))

(define-public crate-protobuf-codegen-pure-2.16.2 (c (n "protobuf-codegen-pure") (v "2.16.2") (d (list (d (n "protobuf") (r "=2.16.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.2") (d #t) (k 0)))) (h "0pxm939kp9sqq2ds77940xbqq0m04ilqcwa8n3mw0rf0zwdpbl3c")))

(define-public crate-protobuf-codegen-pure-2.17.0 (c (n "protobuf-codegen-pure") (v "2.17.0") (d (list (d (n "protobuf") (r "=2.17.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.17.0") (d #t) (k 0)))) (h "0pnqlqy6jk14shys0irg7ir14p1q0vdmgbwv25qp8qnvsgcn37l9")))

(define-public crate-protobuf-codegen-pure-2.18.0 (c (n "protobuf-codegen-pure") (v "2.18.0") (d (list (d (n "protobuf") (r "=2.18.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.0") (d #t) (k 0)))) (h "1232wj8aywrhkpqwpgyjhdy46lb1748b7mglajll3z8lkqnxgy3a")))

(define-public crate-protobuf-codegen-pure-2.18.1 (c (n "protobuf-codegen-pure") (v "2.18.1") (d (list (d (n "protobuf") (r "=2.18.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.1") (d #t) (k 0)))) (h "1abxzi2c0535fwh2pika3dzm48x3p60l98qpyywl4py8zi1vi902")))

(define-public crate-protobuf-codegen-pure-2.19.0 (c (n "protobuf-codegen-pure") (v "2.19.0") (d (list (d (n "protobuf") (r "=2.19.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.19.0") (d #t) (k 0)))) (h "0ghya1j9a12rws0akh7x2mkxgbf1vdk7y7k2irsqjlkqx9lbj57m")))

(define-public crate-protobuf-codegen-pure-2.20.0 (c (n "protobuf-codegen-pure") (v "2.20.0") (d (list (d (n "protobuf") (r "=2.20.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.20.0") (d #t) (k 0)))) (h "0r6rvy9m6vzs82iwb5pygg1kl4hvmlwllwyacss2gw9vfdj5mk70")))

(define-public crate-protobuf-codegen-pure-2.21.0 (c (n "protobuf-codegen-pure") (v "2.21.0") (d (list (d (n "protobuf") (r "=2.21.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.21.0") (d #t) (k 0)))) (h "0q3vrh2m32w874a4dr1dibpwln6fpn1dm97vpp8wdkgzvr1aip90")))

(define-public crate-protobuf-codegen-pure-2.22.0 (c (n "protobuf-codegen-pure") (v "2.22.0") (d (list (d (n "protobuf") (r "=2.22.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.22.0") (d #t) (k 0)))) (h "1qyndsbd3hzrhm16q6haakjlc4wibwkfnyn0hf6mzp3v85qrs8qz")))

(define-public crate-protobuf-codegen-pure-2.22.1 (c (n "protobuf-codegen-pure") (v "2.22.1") (d (list (d (n "protobuf") (r "=2.22.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.22.1") (d #t) (k 0)))) (h "0aw6ivlpsi6n2qj2fj2hbv0hwfy21mvh6xnp3s3dzc6v0w1m58mk")))

(define-public crate-protobuf-codegen-pure-2.18.2 (c (n "protobuf-codegen-pure") (v "2.18.2") (d (list (d (n "protobuf") (r "=2.18.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.2") (d #t) (k 0)))) (h "0qsmf3qn47vkfcd3z75piabm73m33vsay5j2wj8kky5sydds4gji")))

(define-public crate-protobuf-codegen-pure-2.23.0 (c (n "protobuf-codegen-pure") (v "2.23.0") (d (list (d (n "protobuf") (r "=2.23.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.23.0") (d #t) (k 0)))) (h "1chl0anm346i004bwfaz39y1pxrdkfy0lr98cdm8bxwqz3if19lc")))

(define-public crate-protobuf-codegen-pure-2.24.0 (c (n "protobuf-codegen-pure") (v "2.24.0") (d (list (d (n "protobuf") (r "=2.24.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.0") (d #t) (k 0)))) (h "00shr0ij63fr003vzgcyllkbn50km86af7yab8wigv7d60m909nq")))

(define-public crate-protobuf-codegen-pure-2.24.1 (c (n "protobuf-codegen-pure") (v "2.24.1") (d (list (d (n "protobuf") (r "=2.24.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.1") (d #t) (k 0)))) (h "08qd3flli98f0z6nmyga4pyf0ri9yvq5bkl6vfiismv8syk6iyqs")))

(define-public crate-protobuf-codegen-pure-2.24.2 (c (n "protobuf-codegen-pure") (v "2.24.2") (d (list (d (n "protobuf") (r "=2.24.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.2") (d #t) (k 0)))) (h "0gpqnv2aw43smwan9lipwii7q7wb1l47q2kk6g0s5s7wb6vhc9gy")))

(define-public crate-protobuf-codegen-pure-2.25.0 (c (n "protobuf-codegen-pure") (v "2.25.0") (d (list (d (n "protobuf") (r "=2.25.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.0") (d #t) (k 0)))) (h "1bwal7nzzbrgm79wh5ac88nf4piw9dhcia6wih97lvhxnshxml7n")))

(define-public crate-protobuf-codegen-pure-2.25.1 (c (n "protobuf-codegen-pure") (v "2.25.1") (d (list (d (n "protobuf") (r "=2.25.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.1") (d #t) (k 0)))) (h "1dzj4kcgbj5w2m6cndfbjqsbk9rwbbsrlfbcl2a5f37d2riv3fm2")))

(define-public crate-protobuf-codegen-pure-2.25.2 (c (n "protobuf-codegen-pure") (v "2.25.2") (d (list (d (n "protobuf") (r "=2.25.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.2") (d #t) (k 0)))) (h "1xx85s56zly9gr4p009yfrx76x0i1wn08c4dvxj56h0rm3i76jir")))

(define-public crate-protobuf-codegen-pure-3.0.0-alpha.1 (c (n "protobuf-codegen-pure") (v "3.0.0-alpha.1") (d (list (d (n "protobuf") (r "=3.0.0-alpha.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.0.0-alpha.1") (d #t) (k 0)))) (h "12jkvjc1f19f2hqq4zcf5ydpqfdwlayqbmany78jm69l90gwg7sw")))

(define-public crate-protobuf-codegen-pure-3.0.0-alpha.2 (c (n "protobuf-codegen-pure") (v "3.0.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yb8gzr8lvhf287550vgl8ff5ya7qi1fwcqmh8g5b441s1s3wibr")))

(define-public crate-protobuf-codegen-pure-2.26.0 (c (n "protobuf-codegen-pure") (v "2.26.0") (d (list (d (n "protobuf") (r "=2.26.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.26.0") (d #t) (k 0)))) (h "1z2pakbys9ybixnjyd4z24lj9ii1kljhgfy54vl7k7nh0r5h3si0")))

(define-public crate-protobuf-codegen-pure-2.26.1 (c (n "protobuf-codegen-pure") (v "2.26.1") (d (list (d (n "protobuf") (r "=2.26.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.26.1") (d #t) (k 0)))) (h "05pg0j9vb7j3fdd1n58bplxwjwmq7w4nk37ilnmylbhdcyf33yxw")))

(define-public crate-protobuf-codegen-pure-2.27.0 (c (n "protobuf-codegen-pure") (v "2.27.0") (d (list (d (n "protobuf") (r "=2.27.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.27.0") (d #t) (k 0)))) (h "0bji236qrpj7p4fkg7fds7nfm5qf4vzc42pr65gzr2qr4fyd44zn")))

(define-public crate-protobuf-codegen-pure-2.27.1 (c (n "protobuf-codegen-pure") (v "2.27.1") (d (list (d (n "protobuf") (r "=2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.27.1") (d #t) (k 0)))) (h "1iicacpccn3bg65pm3ylvjndf35pplb8l23bg461jmcfn7yj50cz")))

(define-public crate-protobuf-codegen-pure-2.28.0 (c (n "protobuf-codegen-pure") (v "2.28.0") (d (list (d (n "protobuf") (r "=2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.28.0") (d #t) (k 0)))) (h "0rfqvpbbqh4pa406nda54jdl0sgagdgp274mmbpd7g4lzjcr78lm")))

