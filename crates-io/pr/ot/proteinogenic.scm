(define-module (crates-io pr ot proteinogenic) #:use-module (crates-io))

(define-public crate-proteinogenic-0.1.0 (c (n "proteinogenic") (v "0.1.0") (d (list (d (n "pubchem") (r "^0.1.1") (d #t) (k 2)) (d (n "purr") (r "^0.9.0") (d #t) (k 0)))) (h "0q2w1dqs8ln2yl8ynsf968scrngkxbpsz4zvj5ms4qpqc0zq5g68")))

(define-public crate-proteinogenic-0.2.0 (c (n "proteinogenic") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pubchem") (r "^0.1.1") (d #t) (k 2)) (d (n "purr") (r "^0.9.0") (d #t) (k 0)))) (h "1yc443nm3vq6iiv28z6y656m99v9vivswqw6vcrlxn58zpz5r8rm")))

