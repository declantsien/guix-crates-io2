(define-module (crates-io pr ot protobuf-codegen3) #:use-module (crates-io))

(define-public crate-protobuf-codegen3-2.27.2 (c (n "protobuf-codegen3") (v "2.27.2") (d (list (d (n "protobuf") (r "=2.27.2") (d #t) (k 0) (p "protobuf3")))) (h "1jrxldkd88hnfzbkllhi3iiy7ykzmnh63yzisr82h6i1dqjnq9k9")))

(define-public crate-protobuf-codegen3-2.28.1 (c (n "protobuf-codegen3") (v "2.28.1") (d (list (d (n "protobuf") (r "=2.28.0") (d #t) (k 0)))) (h "1s5zml0jvwigc14x7prr5nqfk1qpbrqcd3aklr86l43jhbmy5wb3")))

(define-public crate-protobuf-codegen3-2.28.2 (c (n "protobuf-codegen3") (v "2.28.2") (d (list (d (n "protobuf") (r "=2.28.0") (d #t) (k 0)))) (h "0bqx5wb86prgk6v4wlfl9sx62rymsai6lb3qc5yvswm81y6qgibk")))

