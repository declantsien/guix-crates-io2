(define-module (crates-io pr ot protobuf) #:use-module (crates-io))

(define-public crate-protobuf-0.0.1 (c (n "protobuf") (v "0.0.1") (h "08k1196qd8wixh1bwfh4mhzmvxam1h1blvlx2mqfqgnda5jvzgxv")))

(define-public crate-protobuf-0.0.2 (c (n "protobuf") (v "0.0.2") (h "13ipzxwbrnbb8kvfg1b5xn6iz5qhc5dsrlzyfn1hx65gdhss25va")))

(define-public crate-protobuf-0.0.3 (c (n "protobuf") (v "0.0.3") (h "13hk0fwyfmgyzaanah6bhqqhxm2zda11vjw6ak452la6gbhcbk76")))

(define-public crate-protobuf-0.0.4 (c (n "protobuf") (v "0.0.4") (h "11hrmgx3ypfqy6w4qdfqp6qc4pz0gz7xp2agg6rbhz4rarax1xnl")))

(define-public crate-protobuf-0.0.5 (c (n "protobuf") (v "0.0.5") (h "19h5xkgd533kspzhcws3aa0c3p7zfrhb2hkw1ippskwbdraq26l1")))

(define-public crate-protobuf-0.0.6 (c (n "protobuf") (v "0.0.6") (h "11j7xh8111pg0gfb8hpnns5fyrjwj903kj3npnwsxcm4k9hfm74b")))

(define-public crate-protobuf-0.0.7 (c (n "protobuf") (v "0.0.7") (h "00yyzv8srlqfw2w22h0qx74hkrbghk4sh8w6f39rldvvzmkjq3pf")))

(define-public crate-protobuf-0.0.8 (c (n "protobuf") (v "0.0.8") (h "0z345nnbphgk48z61n0i7vzfilpkyw7cpyc2hyml3yls2nw8zlkz")))

(define-public crate-protobuf-0.0.9 (c (n "protobuf") (v "0.0.9") (h "1iix6pyw1bbyl5mkn9jnsf7ll61llk1ib5yjgc2fpyqyw0awbpvh")))

(define-public crate-protobuf-0.0.10 (c (n "protobuf") (v "0.0.10") (h "1janf6787hyvfhdmaxs6s6kz5xc2lk9122x8v0cj32yigdcbl7c7")))

(define-public crate-protobuf-1.0.0 (c (n "protobuf") (v "1.0.0") (h "00n902cw9021srwgpchjd46aick48lkpwxrq2h91knb698m2drx1")))

(define-public crate-protobuf-1.0.1 (c (n "protobuf") (v "1.0.1") (h "11hkvawhvp0gj3i2jzqh1qvkmmsw6izzax2f97raygpy5i0fa4b7")))

(define-public crate-protobuf-1.0.2 (c (n "protobuf") (v "1.0.2") (h "1b7m6q594m5parh93ckjfm1v060ddll9g4f6xshjiar67j1481wb")))

(define-public crate-protobuf-1.0.3 (c (n "protobuf") (v "1.0.3") (h "0alf8h2ikri2a0djwjmzhh152nkfjhvz0bgbm0i4x07y0yxhgp97")))

(define-public crate-protobuf-1.0.4 (c (n "protobuf") (v "1.0.4") (h "0zd7liqrl6f50p2n5gw5wq5zlqj5mx5id41l85b18sfmliq41bcf")))

(define-public crate-protobuf-1.0.5 (c (n "protobuf") (v "1.0.5") (h "1rpk9r8lv5scynmry651vll9ij8v1fjk8rz5r9av0wz3khjz0ia5")))

(define-public crate-protobuf-1.0.6 (c (n "protobuf") (v "1.0.6") (h "05nwx20ybvblsrd1inh45knmx8nrzdjwwy2af11nvv48fwbfc95f")))

(define-public crate-protobuf-1.0.7 (c (n "protobuf") (v "1.0.7") (h "0s05i90b8whh0nrwb6dcsr5ihsiiilv32r2grc0fgs5x7dgy5xf0")))

(define-public crate-protobuf-1.0.8 (c (n "protobuf") (v "1.0.8") (h "1xadlihdba1h8gl03rrl3yb2g6ryrahlpfgpv7zfz1485d1p5jgw")))

(define-public crate-protobuf-1.0.9 (c (n "protobuf") (v "1.0.9") (h "18hydyywnnv4v5m5ab1c11kzvs2rzwhd9liwhq8dlsq60dscq04l")))

(define-public crate-protobuf-1.0.10 (c (n "protobuf") (v "1.0.10") (h "1y223vqkjjsr5v80kkqxl2mygfwkhqnlxprm9c18i0icvn21a992")))

(define-public crate-protobuf-1.0.11 (c (n "protobuf") (v "1.0.11") (h "00qvp5icawicjn2qaa1x41sspb7vdhhlk18g24snl9h6xzd3nmkm")))

(define-public crate-protobuf-1.0.12 (c (n "protobuf") (v "1.0.12") (h "1xavfig076rrj5qqlyif4ys6x52y4pmnpfm3imw3y441pghaavjz")))

(define-public crate-protobuf-1.0.13 (c (n "protobuf") (v "1.0.13") (h "04jd0dvrhi4cps4bfmal101a3crwvbyp94lrf7dm6mg401xpz8af")))

(define-public crate-protobuf-1.0.14 (c (n "protobuf") (v "1.0.14") (h "0bj2flldzzqbhqh20i181774ms2mg40q7jm1gav1i25wvrzr6y1f")))

(define-public crate-protobuf-1.0.15 (c (n "protobuf") (v "1.0.15") (h "04f4azrv3iz39hdhfaxyck0ig4lbkwjrdpw53imnakjky7pg7yqn")))

(define-public crate-protobuf-1.0.16 (c (n "protobuf") (v "1.0.16") (h "0a70k9wa3nd6j5hmxz8wnkpapsvz9m0dp42n1wpnpgp207wfxfnc")))

(define-public crate-protobuf-1.0.17 (c (n "protobuf") (v "1.0.17") (h "054mwqjkfpm99ikm2xl99h25jvnlvbr3dfqn3bd3lgg054nqscpv")))

(define-public crate-protobuf-1.0.18 (c (n "protobuf") (v "1.0.18") (h "0szvhd6yvhamr77rx8lcs1jd35bl0jn9gffkz46h3y6n3nbi6n32")))

(define-public crate-protobuf-1.0.19 (c (n "protobuf") (v "1.0.19") (h "0s9cz0vv31lr0h0k9gr471z2h9x6c0i79cv4rv0izbgzhddilnq3")))

(define-public crate-protobuf-1.0.20 (c (n "protobuf") (v "1.0.20") (h "1b1zsralav2d3x4vf7h26iz25wjal0q0pcfmmqav6z7r6x72kahh")))

(define-public crate-protobuf-1.0.21 (c (n "protobuf") (v "1.0.21") (h "1wwr7wa8lxza2ds6q8nx36dghqp0k4rz3qzd6w18g9nlqn1ghv5n")))

(define-public crate-protobuf-1.0.22 (c (n "protobuf") (v "1.0.22") (h "06r8q83r146ywgk666625c0vwxnwszbfpd3kfcpnmg5191d55wmy")))

(define-public crate-protobuf-1.0.23 (c (n "protobuf") (v "1.0.23") (h "0qxmjlsg3vmiqwxrnszqkrfdvzv1q878m0kz18ggya48yf9bpy8c")))

(define-public crate-protobuf-1.0.24 (c (n "protobuf") (v "1.0.24") (h "0pbdj4ydgdl7x56lwmf5c30m9gzy6jjm7aq9i8hrh0ip0kzc5i3f")))

(define-public crate-protobuf-1.1.0 (c (n "protobuf") (v "1.1.0") (h "1xwbk33wimnd2y7s1iqz2qlfjbw7wjk72a3hy1cjmxiv46li5492")))

(define-public crate-protobuf-1.2.0 (c (n "protobuf") (v "1.2.0") (h "1rkdkq2kcp1d50l3aid55p3ab0m85454bfflwi42ai8pqnyywzyz")))

(define-public crate-protobuf-1.2.1 (c (n "protobuf") (v "1.2.1") (h "0mfkj7yhxhafi373dfjima1g5vkrmf26ypw7pnflg6p49dysrsi2")))

(define-public crate-protobuf-1.2.2 (c (n "protobuf") (v "1.2.2") (h "0f7p0wawv7aymvldqldlik1rvj1aw56zbd5l9fs20fzzx1pyvqn3")))

(define-public crate-protobuf-1.3.0 (c (n "protobuf") (v "1.3.0") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0j205xhkfamymfnn8gy1h3riqil0i574wdy886qji3znf3w6jz9f") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.3.1 (c (n "protobuf") (v "1.3.1") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0yagaicidz8qkn9saqf1afpmd41w1vsx4iz9bc4kv46l5mph2770") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.0 (c (n "protobuf") (v "1.4.0") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1ppzas8jqzsp4ldrhgpywy299cw4gw6f0mcb9jpxwhch5mjar8bh") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.1 (c (n "protobuf") (v "1.4.1") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1966p84adijc1fs7kcbxi114i64c6a25z9g37bkabnbjspj1b2jn") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.2 (c (n "protobuf") (v "1.4.2") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "19z46ajd5706mrvxyh3xkl0iny43gfaac85w770gg5kq99lsdiwr") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.3 (c (n "protobuf") (v "1.4.3") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "01lm9f2izlqchsgcnvfr4f2b1s6anwhxypql10crjzab35knxhmy") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.4 (c (n "protobuf") (v "1.4.4") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1ifjzm6p83hffcbmq8cm7pnq3v5lb863anvbdvwgmazn6vyfysm0") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.5.0 (c (n "protobuf") (v "1.5.0") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1pqfd2f89kwafrlm429w4f66kylljrb61lrvzjm3cvnqx5ps6zxb") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.5.1 (c (n "protobuf") (v "1.5.1") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0ss8fq6v5w1qspbm29q9mbjqxmvsrrvav0y4bycf1jlxcd74iqj0") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.4.5 (c (n "protobuf") (v "1.4.5") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0rn9391v8cnzmz7b430ywj2q6zf2wgkbaws6q9fwwmyk0xsgllqk") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.6.0 (c (n "protobuf") (v "1.6.0") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1yrflm7nr13bkmssv9y2ar1h2nsg8jwcj3sxb5jvmb1jx2i8kbv3") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.7.1 (c (n "protobuf") (v "1.7.1") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "06hlgm5gaygngpcdlkv2svw5i97bidk6gir45hlxhjamm0ykv7w0") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.0 (c (n "protobuf") (v "2.0.0") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1l2xk419fbpk4j7piwlp63dr1j7zzky7j0rq70v4pj3ffksqlhm2") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.1 (c (n "protobuf") (v "2.0.1") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0b1ziddjb12k0igzvv52vj4kc39qflm38l8pjhmxhybsmm62w331") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.7.2 (c (n "protobuf") (v "1.7.2") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0h6q18a4a9yxarv4n2hlpjw3bkicmvs6r464alkd7h990x30nk89") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.2 (c (n "protobuf") (v "2.0.2") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1byimp9y6mam3pk7f674yfrni8nk8i1lf6c8laxbfp4ad5lvwqx1") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.7.3 (c (n "protobuf") (v "1.7.3") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "1s1ki4564n83ayfaf3nszn2xlabqfvdns51jkwjbw2yq9k7v4001") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.3 (c (n "protobuf") (v "2.0.3") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0sj3jd19a8zrvpvd9rf5ir2mkn5a4j33xsb6kk954v521y7lpv3z") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.7.4 (c (n "protobuf") (v "1.7.4") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0ax1rhfhgnh49a4fw4gq1b4c7cq7fkmlf67k9vj6b5bhyrdw9ysj") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.4 (c (n "protobuf") (v "2.0.4") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0725vfrik94p3rrvi9d6qk1vj6daaizv31c6g9b9lwyzl8vlz2hj") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.5 (c (n "protobuf") (v "2.0.5") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0qkcq36k6i97j7wjy06h2ymcl560pxmd2y4vdd7n4lh7j1incby7") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.0 (c (n "protobuf") (v "2.1.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qpymvii60wnjnma7hjkl3vpkxxyg3vp6pprm5id1rabg63qb55k") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.1 (c (n "protobuf") (v "2.1.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1szafmb54yybw962n511zh0fs3rzihfsj1gsyibh8ibfqggn7hsn") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.2 (c (n "protobuf") (v "2.1.2") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0d2am389gr3dcyjs5if8s439vhvl2pjvn8rz5jvf28p1l52sr5cf") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.3 (c (n "protobuf") (v "2.1.3") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04n2kwis4ra382448qhg4sgn2hkd4cfly6rjrq262v9p3zsq8v08") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.4 (c (n "protobuf") (v "2.1.4") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17fcci9b8x79yj8khjcck3p5a5s8hcksgc5j81dn7lvbhg79q6k7") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.0 (c (n "protobuf") (v "2.2.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0f5vli3533nqcrbnf4dlc006v3qsglyk8bk6pg3b36diil98vl6b") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.1 (c (n "protobuf") (v "2.2.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gif9900yh5fbv3cs8kvrqhx2n6cj52f877h4ijlx5vqdqb4rpif") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.2 (c (n "protobuf") (v "2.2.2") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qmyqmpdar531ai8sx0lyylgy5cp0lrv0zm43wvpvs0xl6flgq7a") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.3 (c (n "protobuf") (v "2.2.3") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18q77zkrlxxiwa9yyazkcnfk5fm2r81lpabpgiwsyd05yqhn18d3") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.4 (c (n "protobuf") (v "2.2.4") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16k0dk5rr7aqs41xzijabg7m73vaprnq22dqrwaw308lznzwn64y") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.1.5 (c (n "protobuf") (v "2.1.5") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0h5ywwshi435hr7i13p1cbk7davvs2bsnr0cbn4v39d6b1q1kq1v") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.0.6 (c (n "protobuf") (v "2.0.6") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0drmwbbq1ryb6v4aidycy3fyjdhmrqr4im96ag3dh7k4487mgi7v") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.2.5 (c (n "protobuf") (v "2.2.5") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zd7ajch8rpwnyv8r5gsd1phgrc7hsh289i4ri5sz3qlclffdnpi") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.3.0 (c (n "protobuf") (v "2.3.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jz11nbvbb0hdsl2lnlz29xyrvmir58r45ah06zfcp2nqxxi2bfq") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.3.1 (c (n "protobuf") (v "2.3.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0y6cqrzgvf4g0c6p37ah917s3dq5n8l8556mbv05w5y36y0qqfk6") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.4.0 (c (n "protobuf") (v "2.4.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pwjknlvjc1jx7rzii0ajhn7g20djl6p7wj1h65xpzc85cyxgm94") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.4.2 (c (n "protobuf") (v "2.4.2") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0c6qgr5cbhx6n5m7wbw8caxrnhn42s175nv8lyxxwzb2jmfickaj") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.5.0 (c (n "protobuf") (v "2.5.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0x9b7xzh3bd0mgr73zpw0k38rj46fcs8w88wlmxs4bxf8zvasyxw") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.6.0 (c (n "protobuf") (v "2.6.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pw3c6hb2p18ynhfgrh4x4jqmzzam950lv05xy02y6ahca6vwn9r") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-1.7.5 (c (n "protobuf") (v "1.7.5") (d (list (d (n "bytes") (r "0.*") (o #t) (d #t) (k 0)))) (h "0a0glf8dglxfz1ljj1plvrkkldpsh0ddxpzjsh988x7cg5mwsk71") (f (quote (("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.6.1 (c (n "protobuf") (v "2.6.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00fv20mi7s26r7hr68mwsjm104hsnaisyvs4mgb5j06zj8dc2ld1") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.6.2 (c (n "protobuf") (v "2.6.2") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0a5ncb8bfqc16z41sq86y7x344ldcl8s3czsv2s4ln13x357d43y") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.7.0 (c (n "protobuf") (v "2.7.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vqacb95vbanprq3ibdylvbkjd5fd16cfwqa5jngxv34rfiy802z") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.8.0 (c (n "protobuf") (v "2.8.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "199z1d86m9hzn9mbm8c6cpfqkgj3fy10f7f8izcj9da2y74wxvwa") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.8.1 (c (n "protobuf") (v "2.8.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "085agil4gwalkxwg33kz2pygfjj4yv39ch78ywgqgmgxvqv1hdj0") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.9.0 (c (n "protobuf") (v "2.9.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0apsf8h03r1zdihb5fn2l3nl4in02p3brdq65cq2fnqfj81y976p") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes")))) (y #t)))

(define-public crate-protobuf-2.8.2 (c (n "protobuf") (v "2.8.2") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w959x31wdgfyyjpqr3kmcyql1fmdbwsbj162b8mcb67xr91hwvh") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.10.0 (c (n "protobuf") (v "2.10.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00bprknsrcxg4jvwmi7z2nibbyyqi1yl81lzyvkp3dqll1bscqv5") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.10.1 (c (n "protobuf") (v "2.10.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ifkffc7qbn3fhnhyj7c2108a85mryr8g9pjnn3jdglddbcxv1k6") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.10.2 (c (n "protobuf") (v "2.10.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hj9fwg0d7i83rgppxshscrrx58gj8vaiq5x79yq6kcs05fk599p") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.10.3 (c (n "protobuf") (v "2.10.3") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zg72immsqz1vizzhz1igbfgsv7swiv5sdhnadhd9vs0l8rbd099") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.11.0 (c (n "protobuf") (v "2.11.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01hzbam36zia41ghc1946f10xin733v919789401bks2zj74l6zw") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.12.0 (c (n "protobuf") (v "2.12.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09m17gvf92ldyc1h0gd0csnlqkbrl1gk5qvs5n409ksizls4z5ki") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.13.0 (c (n "protobuf") (v "2.13.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mpzkbpc392zrdl6z0ksssqffchl1js8brf98ajh840l36v6kf0j") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.14.0 (c (n "protobuf") (v "2.14.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11bl8hf522s9mbkckivnn9n8s3ss4g41w6jmfdsswmr5adqd71lf") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.15.0 (c (n "protobuf") (v "2.15.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00ija4lyf2p59wn2c8fjch0h0c06ifcabrq14hnmy5vyixmwnb1y") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.15.1 (c (n "protobuf") (v "2.15.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1kd81zzw3vlpsql8231awvyljjgifg7vn873kyz38qy0af11m5g4") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.16.0 (c (n "protobuf") (v "2.16.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v1zzcsxl188269zdrfjkhcywfx7xp8s6z7y8dygabg5bzfxi5rm") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.16.1 (c (n "protobuf") (v "2.16.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1q6gsld8cxfv84awq11dchdsi6xykhkklifd1sim2szbnh8mbhnb") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.16.2 (c (n "protobuf") (v "2.16.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gh0zwn5a89ir5k2bkz7cf9rxpgll4d1h18ksa0p46y28n3gg0yq") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.17.0 (c (n "protobuf") (v "2.17.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qmik0kj0dd38w3zyclllmw78amsmpp1qzh620jfw4zjqwy1h56b") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.18.0 (c (n "protobuf") (v "2.18.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0svplmlxwh7y2msy58awn41y24qcsmxxrzxb3z4bzk5wfzdpw53d") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.18.1 (c (n "protobuf") (v "2.18.1") (d (list (d (n "bytes") (r ">=0.5.0, <0.6.0") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)))) (h "15jnn1wr8qfm2czxgvy4as0hw62b9xg5gipc8ggkc3z4q15y0y6s") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.19.0 (c (n "protobuf") (v "2.19.0") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fmyr633mwm41kar78yrdvc665wzii536mn025nlhj22fhrwqavm") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.20.0 (c (n "protobuf") (v "2.20.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04jk9rlha6r86w7s0csxhg8jkq7qd3xhmgx066qi03sq2rgksiw6") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.21.0 (c (n "protobuf") (v "2.21.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "140710sb25kkl016bfmlvi63j4135nmh6fvza4n67yy68zchj4dq") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.22.0 (c (n "protobuf") (v "2.22.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11mv1m7wvmk8cfmvxxdjqgv93dw0lywnq9jv1vdf08kdi622ixvk") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.22.1 (c (n "protobuf") (v "2.22.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14kgy3kkcjfbqj512xwg2s2qzikk06lk4h70lhjlqxdkkc94lzqv") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.18.2 (c (n "protobuf") (v "2.18.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dm0k3vk0lnksr9ya5r8bcygf7qlfc0n9kqcwp7pk5w8jbgii3py") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.23.0 (c (n "protobuf") (v "2.23.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mjqfkirw0nh9l3r0xhdlzkq2q3lmha2xqnq2kayfn41m33lyq25") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.24.0 (c (n "protobuf") (v "2.24.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "199kj6fl017b2l7szq99qqzfrx6abalg4zwcqi5zmnm8aiiya7kn") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.24.1 (c (n "protobuf") (v "2.24.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rq20xr5vhj4ys4qv4mpn656kc4h3bm332n57p6qqicnw5xffl6v") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.24.2 (c (n "protobuf") (v "2.24.2") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11cps8x8556h4bzqd06x088ah2r1lvam6hb22qwln0bh406pmr0h") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.25.0 (c (n "protobuf") (v "2.25.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16q3gx09d5kpqa6p19yr8wnqkcvn8sn3wwkw3wlw9i92fyq8c3q2") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.25.1 (c (n "protobuf") (v "2.25.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fn6k8vhxknsc6zk6frbrvky4vkhpl48mkjzjgnmqdf9y989s4i3") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.25.2 (c (n "protobuf") (v "2.25.2") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m2pb79b73pkmcsmw9s0x6s4n1z7qbdprycx2mc226k2j7hjghs7") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-3.0.0-alpha.1 (c (n "protobuf") (v "3.0.0-alpha.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1g4hz6srf1bh23xnmf34r477xh1y8hsk06z9xl9xz42xmkazx78p") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.2 (c (n "protobuf") (v "3.0.0-alpha.2") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wb2vz2adfq902p9xwrczk8l6laqhx5r5idnw7jjqiy76nfgaplx") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-2.26.0 (c (n "protobuf") (v "2.26.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "099djb2jvm2jgagq30gppd0ckj6bqn7lyjvkhzhq40bcv57knqcx") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.26.1 (c (n "protobuf") (v "2.26.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1rxkz1gkvnsrj92f2ympyxdmvk46zlqxh2qx7hfi56jj2xs5zs80") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-3.0.0-alpha.3 (c (n "protobuf") (v "3.0.0-alpha.3") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0prcfrbnr2503qbfnfwxbsndhvmif6z705l9qz4j17jidq715pn2") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.4 (c (n "protobuf") (v "3.0.0-alpha.4") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1i3xagkmfqdcvgqg3qyyd552hirswybnak6wsq8a6s1ada2n3fj8") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.5 (c (n "protobuf") (v "3.0.0-alpha.5") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0j9hvsimgm46nnm5gw1m97w11bawgdcz16f1pwa6iz6c7yxgs2sw") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-2.27.0 (c (n "protobuf") (v "2.27.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vgfjy6p9k21zw63dbgzs0q40dawga5ifnn1r5ydm3myvdrlfnri") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-2.27.1 (c (n "protobuf") (v "2.27.1") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15jgq6nkhysicmnb2a998aiqan8jr4rd46hdsc10kkcffcc6szng") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-3.0.0-alpha.6 (c (n "protobuf") (v "3.0.0-alpha.6") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hhp135kk5wlr75871q5sas2rlpx70wd4grdlr1kdxn51hjcqsbj") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.7 (c (n "protobuf") (v "3.0.0-alpha.7") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dpyjb9jyb2fp5539h7lamrsflky44wwsdzk3awbcixz6yrsq8cq") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.8 (c (n "protobuf") (v "3.0.0-alpha.8") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z1qnn98qjdzdd6aqvcsxf9dp9r7g1b7539lqscyr74fsk2xa5m4") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.9 (c (n "protobuf") (v "3.0.0-alpha.9") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pjwis5n30radbw119ql7hrgd32ap3knydbhxr7fgz7kajx26cbh") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.10 (c (n "protobuf") (v "3.0.0-alpha.10") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17cba8ag0fckrlzdsg2kgb8bdv827iasfdv31qf0b6xxm3h55c31") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.11 (c (n "protobuf") (v "3.0.0-alpha.11") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0-alpha.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0y3xvikvwpxw9n6sgm70ky2qqhdp6fisyfvxsfzppm76g3haj5dh") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.12 (c (n "protobuf") (v "3.0.0-alpha.12") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0-alpha.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m7nl9grqwfxp0zvj8kpkxhvrwqh1r5ql08valymjldbf6zs7wk5") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.13 (c (n "protobuf") (v "3.0.0-alpha.13") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0-alpha.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nxj1pjwb3cpc6j4iwk8jnb7jfww6wlqdxzgg8613zhy19hp7mwn") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0-alpha.14 (c (n "protobuf") (v "3.0.0-alpha.14") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0-alpha.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "105yi3zfl0fq094w44y8myf0l07a3k69sgcsp0sych15s4w84krj") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.0 (c (n "protobuf") (v "3.0.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13z0dc55p1y51yc0z0mg5dvaz9cwa0prhc1axxh71x39nbb6pmks") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.1 (c (n "protobuf") (v "3.0.1") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xvflncsvy5hk37syqhffq1pm72cb7cwxv84kqvcj702jc9gj3s3") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.2 (c (n "protobuf") (v "3.0.2") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "071v5b9zl674m6s4aid8whs1pvzkfpjz10rfv9gm6sj65bakfjd7") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.0.3 (c (n "protobuf") (v "3.0.3") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0f2zksxcwmcw9cfv8vh1lbn2y25d8hcyg4pxx261i8sjlkpllr2x") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.1.0 (c (n "protobuf") (v "3.1.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xq2af8fnjh4zsc4xj5dw5l76q1620d8s9m6czqwh00qp7cagr2f") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-2.28.0 (c (n "protobuf") (v "2.28.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "154dfzjvxlpx37ha3cmp7fkhcsnyzbnfv7aisvz34x23k2gdjv8h") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-protobuf-3.2.0 (c (n "protobuf") (v "3.2.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07m6l6j6q231i0hv334zs60ipc5pcdryncaqcm9shy7k4s8ssnxm") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.3.0 (c (n "protobuf") (v "3.3.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1461vasdqvjkbll40lys6826kydbw1rw22fwwlsaf8w7q674lpxn") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf-3.4.0 (c (n "protobuf") (v "3.4.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1h6gckpissns3cyzbqynjbkfz80ncwm0bjkbxjyx5kigvrj8lrsq") (f (quote (("with-bytes" "bytes") ("default"))))))

