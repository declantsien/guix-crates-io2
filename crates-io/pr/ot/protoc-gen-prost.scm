(define-module (crates-io pr ot protoc-gen-prost) #:use-module (crates-io))

(define-public crate-protoc-gen-prost-0.0.1 (c (n "protoc-gen-prost") (v "0.0.1") (h "1lnl3bj35g6la21yx54fi9576ks9rbn9ib3bnipc62w4grvfwj6k")))

(define-public crate-protoc-gen-prost-0.1.0 (c (n "protoc-gen-prost") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0ka1jkl4hrdpn93hslsj3i80s735293xh5mh3myw9zyyx3is65ps")))

(define-public crate-protoc-gen-prost-0.1.1 (c (n "protoc-gen-prost") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "17lmdlk71qblp14jp574jl7c6y02bh5w7ivrkqq6i1dbfp7f8yss")))

(define-public crate-protoc-gen-prost-0.1.2 (c (n "protoc-gen-prost") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1lclrragx76a6lcdy25j165vpf2yaiw6vqpsl4phpghrbikrcl4n")))

(define-public crate-protoc-gen-prost-0.1.3 (c (n "protoc-gen-prost") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "16zw6kl4s0nwf7xhv1ixi7p4n8qdafjhd1nmzwvw9xwsc0syzqw1")))

(define-public crate-protoc-gen-prost-0.1.4 (c (n "protoc-gen-prost") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1y2yjy019ipi3g50amkfvqgagwnicpjv9cmd2hjiz4j2qymy2c4v")))

(define-public crate-protoc-gen-prost-0.2.0 (c (n "protoc-gen-prost") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.0") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1ihy2dk6malb7lmb9a0j00fnadqh1apkfavcw7pkbf0a9sdspbr9")))

(define-public crate-protoc-gen-prost-0.2.1 (c (n "protoc-gen-prost") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r ">=0.11.0, <0.11.2") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "004x9f44k4z1f1aydasqa4z5xbns2diasg8if9960b8y6x5b72vv") (r "1.62")))

(define-public crate-protoc-gen-prost-0.2.2 (c (n "protoc-gen-prost") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.8") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.8") (k 0)) (d (n "prost-types") (r "^0.11.8") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1jnj9pbbygylhnrk6dqwpjy9zfcs1dpqj2dj11qc9zi9njdkl7m8") (r "1.62")))

(define-public crate-protoc-gen-prost-0.2.3 (c (n "protoc-gen-prost") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.8") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.8") (k 0)) (d (n "prost-types") (r "^0.11.8") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "15gv2bv9k1i1p9mss4ra2gqm0qh7xpiyqwyyh2qwzza1mlqs1pqh") (r "1.62")))

(define-public crate-protoc-gen-prost-0.3.0 (c (n "protoc-gen-prost") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.3") (k 0)) (d (n "prost-types") (r "^0.12.3") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0s9m7nhzp2p4i28bad2gji7h2pdqqv4vx02j0q5a5ix00ilfc9yv") (r "1.62")))

(define-public crate-protoc-gen-prost-0.3.1 (c (n "protoc-gen-prost") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.3") (k 0)) (d (n "prost-types") (r "^0.12.3") (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "11dvjia18bhg3kf60zh4mzs6gfna1k4an6pvrr61v6nb4iqq8478") (r "1.62")))

