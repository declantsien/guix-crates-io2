(define-module (crates-io pr ot protoc-grpcio) #:use-module (crates-io))

(define-public crate-protoc-grpcio-0.1.0 (c (n "protoc-grpcio") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.2") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "protoc") (r "^1.4") (d #t) (k 0)))) (h "13ap9z31m2j354sy5939xb2wj1y3c3lqdkkxggy70dyajak2nk9p")))

(define-public crate-protoc-grpcio-0.1.1 (c (n "protoc-grpcio") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.2") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "protoc") (r "^1.4") (d #t) (k 0)))) (h "0f3hmw69szv4si17g2vcfrhvcvk94s24ldn5ipyj6c269585x6kw")))

(define-public crate-protoc-grpcio-0.2.0 (c (n "protoc-grpcio") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)))) (h "0knh0jaif30jiwf1ay56rvsggr3ff23hnxcg1z0qclldppmv0zzx")))

(define-public crate-protoc-grpcio-0.2.1 (c (n "protoc-grpcio") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)))) (h "1jaq4hn4szy0f2bybfj6j41jpsxf6j2mx2zypbv4y5rnln9jsadh")))

(define-public crate-protoc-grpcio-0.3.0 (c (n "protoc-grpcio") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)))) (h "1pj0rf5rv8xixw8ls3xv5n0ww69fwzsk3ck635ghgsp81gmrq436")))

(define-public crate-protoc-grpcio-0.3.1 (c (n "protoc-grpcio") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1xiycsnd6gqz1w4il4fal6sp6q4222251cbq0xw1vdql8r9qnyd9")))

(define-public crate-protoc-grpcio-1.0.0 (c (n "protoc-grpcio") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^2.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.1") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0x5rx4w79jx524fv20vph8vkni3qrfabxyinsi351qcmqxsmq4qb")))

(define-public crate-protoc-grpcio-1.0.1 (c (n "protoc-grpcio") (v "1.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^2.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.1") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 0)))) (h "022nkv079xdvpklm07cmli3csvz40q6wq08j29mbayg6z1ywn0hs")))

(define-public crate-protoc-grpcio-1.0.2 (c (n "protoc-grpcio") (v "1.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "protobuf") (r "^2.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.1") (d #t) (k 0)) (d (n "protoc") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 0)))) (h "1pb2qlfk9z7qn97cpcf1f2ga4zyc2glf3hfyir1nidyliqjqjhys")))

(define-public crate-protoc-grpcio-1.1.0 (c (n "protoc-grpcio") (v "1.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.8") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.8") (d #t) (k 0)) (d (n "protoc") (r "^2.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1pxsp9sssdr0sz4nq0v5amrglmrzf41hq0r8ym9n9na4prv9pfz7")))

(define-public crate-protoc-grpcio-1.2.0 (c (n "protoc-grpcio") (v "1.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.5") (d #t) (k 0)) (d (n "protobuf") (r "^2.13") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.13") (d #t) (k 0)) (d (n "protoc") (r "^2.13") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "08nqblda1m6ac7fc7jcgfng33fq8di8b35wsic6qakwb8c06b06b")))

(define-public crate-protoc-grpcio-1.3.0 (c (n "protoc-grpcio") (v "1.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.13") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.13") (d #t) (k 0)) (d (n "protoc") (r "^2.13") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1lmbzngxp7a1gd64vb3m4pf5cmxbilylfn1cnjfcxcqcbap6l56d") (y #t)))

(define-public crate-protoc-grpcio-2.0.0 (c (n "protoc-grpcio") (v "2.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.13") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.13") (d #t) (k 0)) (d (n "protoc") (r "^2.13") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1dzqldkj94bdx2rm0pdh24vnz7sirha11wj26w84xwdpc524hpdg")))

(define-public crate-protoc-grpcio-3.0.0 (c (n "protoc-grpcio") (v "3.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.7") (d #t) (k 0)) (d (n "protobuf") (r "^2.18") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.18") (d #t) (k 0)) (d (n "protoc") (r "^2.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1xrxhlgydpp7w6sv5sqdkp4p0v3jdnpjzdxyf97zi38k8pc0w3cq")))

