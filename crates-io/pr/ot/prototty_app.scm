(define-module (crates-io pr ot prototty_app) #:use-module (crates-io))

(define-public crate-prototty_app-0.28.0 (c (n "prototty_app") (v "0.28.0") (d (list (d (n "prototty_input") (r "^0.28") (d #t) (k 0)) (d (n "prototty_render") (r "^0.28") (d #t) (k 0)))) (h "1ykw01jafp28ik0hy536nz4kn3brv3k2w6x20wpgxb38ld1hddq9")))

(define-public crate-prototty_app-0.29.0 (c (n "prototty_app") (v "0.29.0") (d (list (d (n "prototty_input") (r "^0.29") (d #t) (k 0)) (d (n "prototty_render") (r "^0.29") (d #t) (k 0)))) (h "0wjay460zdlczg0wgqa8xk8rswbmjk2rsrwcmmwqm719y27s5nn6")))

