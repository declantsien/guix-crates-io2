(define-module (crates-io pr ot protected-id-derive) #:use-module (crates-io))

(define-public crate-protected-id-derive-0.1.0 (c (n "protected-id-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0p2a15n94l3aaijbvi2nkp32nzs19pbgzbygfms5m4is46sbhckb") (y #t)))

(define-public crate-protected-id-derive-0.1.1 (c (n "protected-id-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0cmr80vxflsiqcsg7psiriqscnprvmigzfniy9whr8b6lk6c0gnp") (y #t)))

