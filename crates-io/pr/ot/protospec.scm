(define-module (crates-io pr ot protospec) #:use-module (crates-io))

(define-public crate-protospec-0.1.0 (c (n "protospec") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "00nmqq5bfav9av27iavq2frcalqq7h37485q1dg0i34mm1ky7ac6")))

(define-public crate-protospec-0.1.1 (c (n "protospec") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0262qn80kadjdi1v8hndgn4b64qajzpg8wn6jbr72r2b3bzplplj")))

(define-public crate-protospec-0.2.0 (c (n "protospec") (v "0.2.0") (h "031bqv372ishaaap3xjfk52dypfl9jxqfrsfw1zdhl60di6f36cw")))

(define-public crate-protospec-0.3.0 (c (n "protospec") (v "0.3.0") (h "0q9axvg7d3ag3glz6s0jw9sifnkql9af2vz5ar6xkhcyfqj612ic")))

