(define-module (crates-io pr ot protobufs-rust) #:use-module (crates-io))

(define-public crate-protobufs-rust-0.1.0 (c (n "protobufs-rust") (v "0.1.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1fxgn8ais6i784l7h5my7fhkvi2w346g1g827m7irnbpc91kahrd") (y #t)))

