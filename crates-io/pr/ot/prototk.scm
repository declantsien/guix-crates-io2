(define-module (crates-io pr ot prototk) #:use-module (crates-io))

(define-public crate-prototk-0.1.0 (c (n "prototk") (v "0.1.0") (d (list (d (n "buffertk") (r "^0.1.0") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "zerror") (r "^0.1.0") (d #t) (k 0)))) (h "0sa589hyk3v3kbfqhkxy1rc1j3mqq3qm8gsgr76ilbfgp7qnjlk6")))

(define-public crate-prototk-0.2.0 (c (n "prototk") (v "0.2.0") (d (list (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "12abqvvmczv02bhlqpx374nsa2aax16hna8agwywyrrg30qigw3g")))

(define-public crate-prototk-0.2.1 (c (n "prototk") (v "0.2.1") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "0wgh82jsl6r3hccja48d7825sarnps6b473qrdhka7fr6mqlvhwl")))

(define-public crate-prototk-0.2.2 (c (n "prototk") (v "0.2.2") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.3") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "19gb5wr3zjmvh0n3rjhc34xq2zb35ns2lk4daia0jqcj5wjmzy43")))

(define-public crate-prototk-0.3.0 (c (n "prototk") (v "0.3.0") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.3") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "0pnf1m80a934y25rj31kgf0qcz9ihb1iflahbxwzk9iskj2xj5ny")))

(define-public crate-prototk-0.4.0 (c (n "prototk") (v "0.4.0") (d (list (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.4") (d #t) (k 0)) (d (n "zerror") (r "^0.2") (d #t) (k 0)))) (h "11fk28a53xlnnjsv2fvdniq2k6h6f72g4b5wa4qzg0v4r6fnd1m8")))

(define-public crate-prototk-0.5.0 (c (n "prototk") (v "0.5.0") (d (list (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.5") (d #t) (k 0)) (d (n "zerror") (r "^0.3") (d #t) (k 0)))) (h "0dqpkc8pksg371iyvy8jnblpx99mip0fq3fjch7nggbq8hyyg0n6")))

(define-public crate-prototk-0.6.0 (c (n "prototk") (v "0.6.0") (d (list (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "zerror") (r "^0.4") (d #t) (k 0)))) (h "0ddasxn4p6d3v9pag2m7y77asj5i01caq3s8wz4idb3ybpk6a6hz")))

