(define-module (crates-io pr ot protobuf2) #:use-module (crates-io))

(define-public crate-protobuf2-4.0.0-alpha.0 (c (n "protobuf2") (v "4.0.0-alpha.0") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=4.0.0-alpha.0") (d #t) (k 0) (p "protobuf-support2")) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1da0fc049p8qsq4yfi4gcr7rqmw32ik91p4jrr5ashcq4l5gqw3n") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf2-4.0.0-alpha.1 (c (n "protobuf2") (v "4.0.0-alpha.1") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=4.0.0-alpha.1") (d #t) (k 0) (p "protobuf-support2")) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00qqxm3b032iqs8653qyy4v52cx6h179nv09aa9k2mr64fcxc5bk") (f (quote (("with-bytes" "bytes") ("default"))))))

(define-public crate-protobuf2-4.0.0-alpha.2 (c (n "protobuf2") (v "4.0.0-alpha.2") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "protobuf-support") (r "=4.0.0-alpha.2") (d #t) (k 0) (p "protobuf-support2")) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01ddnidlvzb8bv8ai21l563p5xbk16mib1ydqv7ncz9glqk0w2gw") (f (quote (("with-bytes" "bytes") ("default"))))))

