(define-module (crates-io pr ot protect-endpoints-core) #:use-module (crates-io))

(define-public crate-protect-endpoints-core-0.1.0 (c (n "protect-endpoints-core") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (o #t) (k 0)))) (h "1ahfxzcj0bipm8gajbdkz8acz308whrxrfkm7vcw7jydjiyp0cvk") (s 2) (e (quote (("tower" "dep:tower" "pin-project" "futures-util"))))))

(define-public crate-protect-endpoints-core-0.1.1 (c (n "protect-endpoints-core") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (o #t) (k 0)))) (h "0lssisg25xy8jhj7nl76cqm17a9pq9v953dn4snx6x9msaxfj9kg") (s 2) (e (quote (("tower" "dep:tower" "pin-project" "futures-util"))))))

