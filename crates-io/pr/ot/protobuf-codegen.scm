(define-module (crates-io pr ot protobuf-codegen) #:use-module (crates-io))

(define-public crate-protobuf-codegen-1.6.0 (c (n "protobuf-codegen") (v "1.6.0") (d (list (d (n "protobuf") (r "^1.6.0") (d #t) (k 0)))) (h "1d13d0pmp1xa5zxhy172987fiv2ims5bv76jcjhrcrwcpd5m5xw9")))

(define-public crate-protobuf-codegen-2.0.0 (c (n "protobuf-codegen") (v "2.0.0") (d (list (d (n "protobuf") (r "^2.0.0") (d #t) (k 0)))) (h "0qmywfrn97hhmm5jrrr3rjc9qbcgsipjq5j280898s0nr2inxpq3")))

(define-public crate-protobuf-codegen-2.0.1 (c (n "protobuf-codegen") (v "2.0.1") (d (list (d (n "protobuf") (r "^2.0.1") (d #t) (k 0)))) (h "0rpwv8m8kc4lnchwzl481z8b86cdm3bv6nh3zw9irmcnkq24r7sx")))

(define-public crate-protobuf-codegen-2.0.2 (c (n "protobuf-codegen") (v "2.0.2") (d (list (d (n "protobuf") (r "= 2.0.2") (d #t) (k 0)))) (h "1zwcy86nzlm951wc5pbf4zxz74g39kfmdq065xhcby0nfq4imbx2")))

(define-public crate-protobuf-codegen-2.0.3 (c (n "protobuf-codegen") (v "2.0.3") (d (list (d (n "protobuf") (r "= 2.0.3") (d #t) (k 0)))) (h "1s6zrcbg4sv12fhhyh44ngvs2v9sj6mpi3wx6fjda2ji82v19gcw")))

(define-public crate-protobuf-codegen-2.0.4 (c (n "protobuf-codegen") (v "2.0.4") (d (list (d (n "protobuf") (r "= 2.0.4") (d #t) (k 0)))) (h "1jnswkp1428wjnz7hf1dhyqp6rawwr55g2xz3pi2slbzmkzc091n")))

(define-public crate-protobuf-codegen-2.0.5 (c (n "protobuf-codegen") (v "2.0.5") (d (list (d (n "protobuf") (r "= 2.0.5") (d #t) (k 0)))) (h "19iiz71gr5ipps03mp72d11slnd03x9xp9q5yfyjqrk4wkdsnfiv")))

(define-public crate-protobuf-codegen-2.1.0 (c (n "protobuf-codegen") (v "2.1.0") (d (list (d (n "protobuf") (r "= 2.1.0") (d #t) (k 0)))) (h "0vrh3grdzc2dsimn8wyy2f2sz5si8qwpwr3gcmfxw0jy9iw5jmfn")))

(define-public crate-protobuf-codegen-2.1.1 (c (n "protobuf-codegen") (v "2.1.1") (d (list (d (n "protobuf") (r "= 2.1.1") (d #t) (k 0)))) (h "0s4aaqg6sp4vi2dngf21swcw3l6d6n55z4dn3ajgfm7jvn3pfhc0")))

(define-public crate-protobuf-codegen-2.1.2 (c (n "protobuf-codegen") (v "2.1.2") (d (list (d (n "protobuf") (r "= 2.1.2") (d #t) (k 0)))) (h "05180az6qrd0jmaw0xqnp6cx1hz3b9r4f0xs034y962b4kl4hay3")))

(define-public crate-protobuf-codegen-2.1.3 (c (n "protobuf-codegen") (v "2.1.3") (d (list (d (n "protobuf") (r "= 2.1.3") (d #t) (k 0)))) (h "1q51mwnwvwald9mixyarjzpp82h11px3dkd56s7n8ycja3wgq63c")))

(define-public crate-protobuf-codegen-2.1.4 (c (n "protobuf-codegen") (v "2.1.4") (d (list (d (n "protobuf") (r "= 2.1.4") (d #t) (k 0)))) (h "1ww4pbzlf2pi50pfksb2pjn3f3h78r27pgn7x1r3hw58syicpgix")))

(define-public crate-protobuf-codegen-2.2.0 (c (n "protobuf-codegen") (v "2.2.0") (d (list (d (n "protobuf") (r "= 2.2.0") (d #t) (k 0)))) (h "0qh8drw5ck5rsj7mmaxrjbxpkgd0055ypbxaihq6vs4flx8hwwy5")))

(define-public crate-protobuf-codegen-2.2.1 (c (n "protobuf-codegen") (v "2.2.1") (d (list (d (n "protobuf") (r "= 2.2.1") (d #t) (k 0)))) (h "1zzswfajgjvjkbcvl967fm3v47bp4y2bfn3cww62fbcnsjahxsgr")))

(define-public crate-protobuf-codegen-2.2.2 (c (n "protobuf-codegen") (v "2.2.2") (d (list (d (n "protobuf") (r "= 2.2.2") (d #t) (k 0)))) (h "0jcf5asr3qd6n7dmqajnah29nc4n6aqaqv54qli8rxffdlxw83k4")))

(define-public crate-protobuf-codegen-2.2.3 (c (n "protobuf-codegen") (v "2.2.3") (d (list (d (n "protobuf") (r "= 2.2.3") (d #t) (k 0)))) (h "08cphpdqmhgy6kwszgf98agv4h7vaqc151d3kzlrcarxlvka5qd1")))

(define-public crate-protobuf-codegen-2.2.4 (c (n "protobuf-codegen") (v "2.2.4") (d (list (d (n "protobuf") (r "= 2.2.4") (d #t) (k 0)))) (h "15afj0nq8xyjwzmadd1njxgzq9p33chp3f2ws9xm3jk61sjv2v7b")))

(define-public crate-protobuf-codegen-2.1.5 (c (n "protobuf-codegen") (v "2.1.5") (d (list (d (n "protobuf") (r "= 2.1.5") (d #t) (k 0)))) (h "03z2qsdjj1s3cvlnsspcsk0584kx5fjmws3bn8appxkqac7mgsny")))

(define-public crate-protobuf-codegen-2.0.6 (c (n "protobuf-codegen") (v "2.0.6") (d (list (d (n "protobuf") (r "= 2.0.6") (d #t) (k 0)))) (h "1qa17yszf9zirwzmfdjqlrw5bz1lb0c1z8kgnhy716fw6w8mfan1")))

(define-public crate-protobuf-codegen-2.2.5 (c (n "protobuf-codegen") (v "2.2.5") (d (list (d (n "protobuf") (r "= 2.2.5") (d #t) (k 0)))) (h "1lfdys06iwv4i75f3pfglxj28p0ad2q0kgjwqdxzqil6fi9qgqrr")))

(define-public crate-protobuf-codegen-2.3.0 (c (n "protobuf-codegen") (v "2.3.0") (d (list (d (n "protobuf") (r "= 2.3.0") (d #t) (k 0)))) (h "09fi9anvs0lzv6qnpd9jyza3h47dr80m8a77shld2783399vy9az")))

(define-public crate-protobuf-codegen-2.3.1 (c (n "protobuf-codegen") (v "2.3.1") (d (list (d (n "protobuf") (r "= 2.3.1") (d #t) (k 0)))) (h "19yfpijz3hkj7rfibyan2ad90ss0yh21drl83gjklffxfj0anip3")))

(define-public crate-protobuf-codegen-2.4.0 (c (n "protobuf-codegen") (v "2.4.0") (d (list (d (n "protobuf") (r "= 2.4.0") (d #t) (k 0)))) (h "0rq5z18wraf141cm701ikcyfkha3mhii4ykic6r3rlah2ciyzhbx")))

(define-public crate-protobuf-codegen-2.4.2 (c (n "protobuf-codegen") (v "2.4.2") (d (list (d (n "protobuf") (r "= 2.4.2") (d #t) (k 0)))) (h "1881cjlxif430lrjv4093grsw82g8mxaq53pci6nfdh75abwahp1")))

(define-public crate-protobuf-codegen-2.5.0 (c (n "protobuf-codegen") (v "2.5.0") (d (list (d (n "protobuf") (r "= 2.5.0") (d #t) (k 0)))) (h "11q4x0fixcmmc0a3cdig0hjzv3z5k76h9abiv4xi9dcy0545mrqv")))

(define-public crate-protobuf-codegen-2.6.0 (c (n "protobuf-codegen") (v "2.6.0") (d (list (d (n "protobuf") (r "= 2.6.0") (d #t) (k 0)))) (h "1dq5fpmb2kpih13wqdyjkgvwf71a4i4p13pisvj5r5lymv3s2q85")))

(define-public crate-protobuf-codegen-2.6.1 (c (n "protobuf-codegen") (v "2.6.1") (d (list (d (n "protobuf") (r "= 2.6.1") (d #t) (k 0)))) (h "1f4n47s6013dbjjr7m9gh3hrn20jg8dlig7xq5pb8y6mnakrw9yg")))

(define-public crate-protobuf-codegen-2.6.2 (c (n "protobuf-codegen") (v "2.6.2") (d (list (d (n "protobuf") (r "= 2.6.2") (d #t) (k 0)))) (h "0szsm47ncsmcc3p85p36ki08a6l38jaa5hiwndpd2pyb65lwx5hz")))

(define-public crate-protobuf-codegen-2.7.0 (c (n "protobuf-codegen") (v "2.7.0") (d (list (d (n "protobuf") (r "= 2.7.0") (d #t) (k 0)))) (h "0gmxghsb4v9ch0wzvhkf9ngqwm011q1dm6gm0rin9nvc2raybinj")))

(define-public crate-protobuf-codegen-2.8.0 (c (n "protobuf-codegen") (v "2.8.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)))) (h "1qkmixjbmzz6mvdj1k5j4wp30m9mzbi78z1v3f79wswd0bl9nlri")))

(define-public crate-protobuf-codegen-2.8.1 (c (n "protobuf-codegen") (v "2.8.1") (d (list (d (n "protobuf") (r "= 2.8.1") (d #t) (k 0)))) (h "0jlbymmlw4xp4wsy4wsa7l33i156442vvswqd345yi1mhkbspihj")))

(define-public crate-protobuf-codegen-2.9.0 (c (n "protobuf-codegen") (v "2.9.0") (d (list (d (n "protobuf") (r "= 2.9.0") (d #t) (k 0)))) (h "1qrd3ywqccgxk1cy80swpjh7xinrj2a1ggv1hvjqajsbjzvs9x4v") (y #t)))

(define-public crate-protobuf-codegen-2.8.2 (c (n "protobuf-codegen") (v "2.8.2") (d (list (d (n "protobuf") (r "= 2.8.2") (d #t) (k 0)))) (h "1587fbgsgs97jma13mfgqygfwlbcq57738w5khbagj9apz5vjx1x")))

(define-public crate-protobuf-codegen-2.10.0 (c (n "protobuf-codegen") (v "2.10.0") (d (list (d (n "protobuf") (r "= 2.10.0") (d #t) (k 0)))) (h "09fjg76cmj2d0mppshnnh2fclxaalvljc8vf1drkwp8dndnvq6vg")))

(define-public crate-protobuf-codegen-2.10.1 (c (n "protobuf-codegen") (v "2.10.1") (d (list (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)))) (h "08wdpm6ys9kpwv7q1r2j6pnqv11m7q67dkb8b2875z7pxhg44mk4")))

(define-public crate-protobuf-codegen-2.10.2 (c (n "protobuf-codegen") (v "2.10.2") (d (list (d (n "protobuf") (r "= 2.10.2") (d #t) (k 0)))) (h "1hkxqizsimqpww7dmsw7qjgw60djysvjbwg8vaqc3ip0j9hkmpb4")))

(define-public crate-protobuf-codegen-2.10.3 (c (n "protobuf-codegen") (v "2.10.3") (d (list (d (n "protobuf") (r "= 2.10.3") (d #t) (k 0)))) (h "0hk4wslihnir3vqkxrlnhbdl431a22x2ji7prsbcckhc6pdh6rxl")))

(define-public crate-protobuf-codegen-2.11.0 (c (n "protobuf-codegen") (v "2.11.0") (d (list (d (n "protobuf") (r "= 2.11.0") (d #t) (k 0)))) (h "1h3nc318l5rhkd99ddqdmz09zzmakyq7ycv4m5c0yixzhb1n6jsa")))

(define-public crate-protobuf-codegen-2.12.0 (c (n "protobuf-codegen") (v "2.12.0") (d (list (d (n "protobuf") (r "= 2.12.0") (d #t) (k 0)))) (h "1dpx131jzhyddxh9hykq80gs6647iymr9xbdkd51j5x7826p28pi")))

(define-public crate-protobuf-codegen-2.13.0 (c (n "protobuf-codegen") (v "2.13.0") (d (list (d (n "protobuf") (r "= 2.13.0") (d #t) (k 0)))) (h "1pa0ic47jd5pb18nm7bzylpcz3m2j7nv73b7jmpr7rl9w15081g1")))

(define-public crate-protobuf-codegen-2.14.0 (c (n "protobuf-codegen") (v "2.14.0") (d (list (d (n "protobuf") (r "= 2.14.0") (d #t) (k 0)))) (h "031bx325lsgcx7wc76vc2cqph6q0b34jgc8nz0g2rkwcfnx3n4fy")))

(define-public crate-protobuf-codegen-2.15.0 (c (n "protobuf-codegen") (v "2.15.0") (d (list (d (n "protobuf") (r "=2.15.0") (d #t) (k 0)))) (h "08gw5il691634ax3wndwnf5ms0qzq8khiqqw0h75ig1vi5lip6zb")))

(define-public crate-protobuf-codegen-2.15.1 (c (n "protobuf-codegen") (v "2.15.1") (d (list (d (n "protobuf") (r "=2.15.1") (d #t) (k 0)))) (h "0ssgibw8c43sishgjd285r6ang2gdqy4rkdrphy5q79x15xcv0jn")))

(define-public crate-protobuf-codegen-2.16.0 (c (n "protobuf-codegen") (v "2.16.0") (d (list (d (n "protobuf") (r "=2.16.0") (d #t) (k 0)))) (h "14qwpv8l0rpgz96i52lv0bcqdkca71j55fa9bvllz2rs1wasxfs5")))

(define-public crate-protobuf-codegen-2.16.1 (c (n "protobuf-codegen") (v "2.16.1") (d (list (d (n "protobuf") (r "=2.16.1") (d #t) (k 0)))) (h "11zic3b3b2vkfmn3lck6qlrflzvcssix6jwp7ya1j52dzmnv7gv3")))

(define-public crate-protobuf-codegen-2.16.2 (c (n "protobuf-codegen") (v "2.16.2") (d (list (d (n "protobuf") (r "=2.16.2") (d #t) (k 0)))) (h "1rk96ldhw9lym0sg7xihdqy32kpn8rnhf6srqr923cmb3fdzkhxi")))

(define-public crate-protobuf-codegen-2.17.0 (c (n "protobuf-codegen") (v "2.17.0") (d (list (d (n "protobuf") (r "=2.17.0") (d #t) (k 0)))) (h "187rfd8gzwhfayqagdgqf4lfdwdwz6z2ax2zqsgpsviq2zha2q1k")))

(define-public crate-protobuf-codegen-2.18.0 (c (n "protobuf-codegen") (v "2.18.0") (d (list (d (n "protobuf") (r "=2.18.0") (d #t) (k 0)))) (h "1gdv2712a06jb0lki5smbmiig2fdrpvmag15hxpm1fda4l6gg0cy")))

(define-public crate-protobuf-codegen-2.18.1 (c (n "protobuf-codegen") (v "2.18.1") (d (list (d (n "protobuf") (r "=2.18.1") (d #t) (k 0)))) (h "0vb9sg2vqgkjsd4iaphsj2hsa7kiicg3a93dzqd8hs1j6qlvkcj9")))

(define-public crate-protobuf-codegen-2.19.0 (c (n "protobuf-codegen") (v "2.19.0") (d (list (d (n "protobuf") (r "=2.19.0") (d #t) (k 0)))) (h "0bdimgc9ksi3n48rwkziddy22nylp3nb6wi12hklrx50zikjxgka")))

(define-public crate-protobuf-codegen-2.20.0 (c (n "protobuf-codegen") (v "2.20.0") (d (list (d (n "protobuf") (r "=2.20.0") (d #t) (k 0)))) (h "0pq4b7z8883gvb088a1kdr8x7c37hi95xwkn6p7c7ngwh52vmdn8")))

(define-public crate-protobuf-codegen-2.21.0 (c (n "protobuf-codegen") (v "2.21.0") (d (list (d (n "protobuf") (r "=2.21.0") (d #t) (k 0)))) (h "1xigv41kjchz9ybmcpvbb42ix4y7izagmfvjsb9aa8xvy0w459ca")))

(define-public crate-protobuf-codegen-2.22.0 (c (n "protobuf-codegen") (v "2.22.0") (d (list (d (n "protobuf") (r "=2.22.0") (d #t) (k 0)))) (h "0hymcagqir8lcj4ndacij5rfvyj584imii89qlcivjfva8b7l8g8")))

(define-public crate-protobuf-codegen-2.22.1 (c (n "protobuf-codegen") (v "2.22.1") (d (list (d (n "protobuf") (r "=2.22.1") (d #t) (k 0)))) (h "0sj81iplkhgc2kic4rs9hcd9qj3vvmhdl55r0f0m0mqq8qxgmlp5")))

(define-public crate-protobuf-codegen-2.18.2 (c (n "protobuf-codegen") (v "2.18.2") (d (list (d (n "protobuf") (r "=2.18.2") (d #t) (k 0)))) (h "0a6nw5r3zib9sdg52g36xan67y1zbycfiyyba5fpvzxm53z855zl")))

(define-public crate-protobuf-codegen-2.23.0 (c (n "protobuf-codegen") (v "2.23.0") (d (list (d (n "protobuf") (r "=2.23.0") (d #t) (k 0)))) (h "15f0g895hykdq88j2dlhbvgkxb6wd32dn4v310f8r5c5nm1g71yb")))

(define-public crate-protobuf-codegen-2.24.0 (c (n "protobuf-codegen") (v "2.24.0") (d (list (d (n "protobuf") (r "=2.24.0") (d #t) (k 0)))) (h "1q1i4f6cjmi4ghs2sjkgzb8vspablh2np9f96bxlyxd75jb3pr8g")))

(define-public crate-protobuf-codegen-2.24.1 (c (n "protobuf-codegen") (v "2.24.1") (d (list (d (n "protobuf") (r "=2.24.1") (d #t) (k 0)))) (h "143gkkybardsmsrw81a2qz6qd9cjrkvvg5sgi0vdv7gfkgpiqch9")))

(define-public crate-protobuf-codegen-2.24.2 (c (n "protobuf-codegen") (v "2.24.2") (d (list (d (n "protobuf") (r "=2.24.2") (d #t) (k 0)))) (h "1bwv7scr05hjjq5781hbbaii0d971xhjwfh6cq16lvbmcz8asskh")))

(define-public crate-protobuf-codegen-2.25.0 (c (n "protobuf-codegen") (v "2.25.0") (d (list (d (n "protobuf") (r "=2.25.0") (d #t) (k 0)))) (h "137n4925slm56yl1mnpbibqmgl4fivhsr6sx2kgv06c62b2wg2kv")))

(define-public crate-protobuf-codegen-2.25.1 (c (n "protobuf-codegen") (v "2.25.1") (d (list (d (n "protobuf") (r "=2.25.1") (d #t) (k 0)))) (h "0v02fl46p44xj5y1d6hn970f00rvlyh84v8vgwgdx9nsvbh8raab")))

(define-public crate-protobuf-codegen-2.25.2 (c (n "protobuf-codegen") (v "2.25.2") (d (list (d (n "protobuf") (r "=2.25.2") (d #t) (k 0)))) (h "0lxq7k34hgf7pfqaar826qmdilf1d2yh1bnvq99nckdx126cky1x")))

(define-public crate-protobuf-codegen-3.0.0-alpha.1 (c (n "protobuf-codegen") (v "3.0.0-alpha.1") (d (list (d (n "protobuf") (r "=3.0.0-alpha.1") (d #t) (k 0)))) (h "0c5i4ab22rd8s0mipr2kb7x5vj9zbawypfry8psw8s2fg16hk36b")))

(define-public crate-protobuf-codegen-3.0.0-alpha.2 (c (n "protobuf-codegen") (v "3.0.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vrkyd0r7vy17r0yc5f6yid30nhmvp4zx2dkraj7g6zn37l0w449")))

(define-public crate-protobuf-codegen-2.26.0 (c (n "protobuf-codegen") (v "2.26.0") (d (list (d (n "protobuf") (r "=2.26.0") (d #t) (k 0)))) (h "1afxfx0phakaxzcz762a1i0idb0i25a570k4zmkgyick37bxvp2y")))

(define-public crate-protobuf-codegen-2.26.1 (c (n "protobuf-codegen") (v "2.26.1") (d (list (d (n "protobuf") (r "=2.26.1") (d #t) (k 0)))) (h "0gc6l3h28w4x6gf7wajp36yfrpd7qjf3q31spdmqs03bq70hc5gm")))

(define-public crate-protobuf-codegen-3.0.0-alpha.3 (c (n "protobuf-codegen") (v "3.0.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.3") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.3") (d #t) (k 0)) (d (n "protoc") (r "=3.0.0-alpha.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r3wjld1j4mm58wfbs3d7rg96d2zks0s5qqclab5iv04bfhizc67")))

(define-public crate-protobuf-codegen-3.0.0-alpha.4 (c (n "protobuf-codegen") (v "3.0.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.4") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.4") (d #t) (k 0)) (d (n "protoc") (r "=3.0.0-alpha.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03iw6klsgvqnkc3rg9fhi7y5splfai0i0yp5s15rmd97xwi4zqir")))

(define-public crate-protobuf-codegen-3.0.0-alpha.5 (c (n "protobuf-codegen") (v "3.0.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.5") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.5") (d #t) (k 0)) (d (n "protoc") (r "=3.0.0-alpha.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qyh2kjj71f8hflb405zq4w5d7w0v917kdls0kzbaan9nf6fjxgh")))

(define-public crate-protobuf-codegen-2.27.0 (c (n "protobuf-codegen") (v "2.27.0") (d (list (d (n "protobuf") (r "=2.27.0") (d #t) (k 0)))) (h "0wk0lrr6r59d58rq1n5xy9k80hzd0fn8p81llbfvzdx4g39x6dd7")))

(define-public crate-protobuf-codegen-2.27.1 (c (n "protobuf-codegen") (v "2.27.1") (d (list (d (n "protobuf") (r "=2.27.1") (d #t) (k 0)))) (h "01ypnn1194br42c5ign40s4v2irw3zypv6j38c1n4blgghmn7hdf")))

(define-public crate-protobuf-codegen-3.0.0-alpha.6 (c (n "protobuf-codegen") (v "3.0.0-alpha.6") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.6") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1am86ml9hs73zsn1p6g7fi3vk1k0mqvqiipz7j22ra7vi1lwj4lq")))

(define-public crate-protobuf-codegen-3.0.0-alpha.7 (c (n "protobuf-codegen") (v "3.0.0-alpha.7") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.7") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01smw3vils4nw7gkg35iaw4l9cdqqzkrvrlvwddlahmrlrs3p09g")))

(define-public crate-protobuf-codegen-3.0.0-alpha.8 (c (n "protobuf-codegen") (v "3.0.0-alpha.8") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.8") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1h4vw3xw5wv38dkmb6fpzmiw3fh01z469h3ya8vwmg0dsv89zm24")))

(define-public crate-protobuf-codegen-3.0.0-alpha.9 (c (n "protobuf-codegen") (v "3.0.0-alpha.9") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.9") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1rl0k6wp3mzvclzsx47ff2c8ijy2aybqwp8l8x97nvyqcbqkdgsg")))

(define-public crate-protobuf-codegen-3.0.0-alpha.10 (c (n "protobuf-codegen") (v "3.0.0-alpha.10") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.10") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1z396n93ngz83sgqkjwm9330fx06r1jz34hq91vj45nczxgq6y8r")))

(define-public crate-protobuf-codegen-3.0.0-alpha.11 (c (n "protobuf-codegen") (v "3.0.0-alpha.11") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.11") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.11") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1can8xlmdz4yfhh40y2rlm5zbi968gb8wgsq8vclh36sxcx38f5p")))

(define-public crate-protobuf-codegen-3.0.0-alpha.12 (c (n "protobuf-codegen") (v "3.0.0-alpha.12") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.12") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.12") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14a0w8kn9hngdxymzqcy8qbnmigapiw4vx30aikqjd705vi82h6p")))

(define-public crate-protobuf-codegen-3.0.0-alpha.13 (c (n "protobuf-codegen") (v "3.0.0-alpha.13") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.13") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qyzax2kw1fk9klnfc2fx2m6s4zkfprh3x9dzbzfcr12nxjhrbnk")))

(define-public crate-protobuf-codegen-3.0.0-alpha.14 (c (n "protobuf-codegen") (v "3.0.0-alpha.14") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.14") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1g73bmb0kynk5wnladq45lc05h3g1gldsa3pcnwj1ga3s1f9xi5l")))

(define-public crate-protobuf-codegen-3.0.0 (c (n "protobuf-codegen") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0jijvknfwl1zk70jaxjig01mr1fv1xz7wch5gcn4ds74rbq8q0xk")))

(define-public crate-protobuf-codegen-3.0.1 (c (n "protobuf-codegen") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.1") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kbi7fbpigikxgc3b9zvf6fymqpzil2mcb45a3h1aa9b5wj8y97r")))

(define-public crate-protobuf-codegen-3.0.2 (c (n "protobuf-codegen") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.2") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09gqcjf82aiia1li7gbvrki9hb5kggf4282f2aixpnbnkb39fad0")))

(define-public crate-protobuf-codegen-3.0.3 (c (n "protobuf-codegen") (v "3.0.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.3") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "140h3csscqq4zx1g3a5w30wqzrh7z92vkq3hgxf6k4z1dj77r3d9")))

(define-public crate-protobuf-codegen-3.1.0 (c (n "protobuf-codegen") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.1.0") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pn4vv3856x4zlxw09kbasyjah376c6qqkr4sm2maffkwzjr7f07")))

(define-public crate-protobuf-codegen-2.28.0 (c (n "protobuf-codegen") (v "2.28.0") (d (list (d (n "protobuf") (r "=2.28.0") (d #t) (k 0)))) (h "1mhpl2cs1d2sqddf097ala180il61g9axpqnzky5bxswnypn0d03")))

(define-public crate-protobuf-codegen-3.2.0 (c (n "protobuf-codegen") (v "3.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.2.0") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00a9mk6qkgrlc8xj9yxj6zkfpqj41pzq1jrp5l1s9jli7jn1im0d")))

(define-public crate-protobuf-codegen-3.3.0 (c (n "protobuf-codegen") (v "3.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0y0g0sal6amp3zlkk5ad7nxv3v55qxnf4clh3l8p673b455531bf")))

(define-public crate-protobuf-codegen-3.4.0 (c (n "protobuf-codegen") (v "3.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.4.0") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xvs5dw36hsz86k093cmnm294jvybkcgmcqjw39djf357w5pnxrj")))

