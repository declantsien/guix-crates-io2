(define-module (crates-io pr ot protomask-metrics) #:use-module (crates-io))

(define-public crate-protomask-metrics-0.1.0 (c (n "protomask-metrics") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.27") (f (quote ("server" "http1" "tcp"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)))) (h "0vmasqbnq4j65avq2az3sl1v511qzlrwvksamvh9z0r2qzd8zqmj")))

