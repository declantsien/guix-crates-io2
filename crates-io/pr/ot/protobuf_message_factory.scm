(define-module (crates-io pr ot protobuf_message_factory) #:use-module (crates-io))

(define-public crate-protobuf_message_factory-0.1.0 (c (n "protobuf_message_factory") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0bp7qa0a86pcm2w410idd0fdail403m5sc3v7ppgx5vpsj0ffdsr")))

(define-public crate-protobuf_message_factory-0.1.1 (c (n "protobuf_message_factory") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "04jhbv8nk7hjjkni63pqjapfrfhbgf54qzc1y52hpjivwrw9651i")))

(define-public crate-protobuf_message_factory-0.1.2 (c (n "protobuf_message_factory") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "16wf5a9klcccj6mf5kjpq18pccqxrfb2qk3r8qg4m6pq3pijaqbb")))

(define-public crate-protobuf_message_factory-0.1.3 (c (n "protobuf_message_factory") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1dl8qyxidb8pr6l0dpckdzdshcgd05ysambbp6g3j8fxrgigbg1k")))

