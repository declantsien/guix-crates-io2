(define-module (crates-io pr ot prototty_storage) #:use-module (crates-io))

(define-public crate-prototty_storage-0.22.0 (c (n "prototty_storage") (v "0.22.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06yd0dvchmqvn4xfp9851hswa338df93sjpn2mgv2qx46k9vv0j5")))

(define-public crate-prototty_storage-0.23.0 (c (n "prototty_storage") (v "0.23.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wh8958xgkdpb1qgg6hi3j6h4kn7dmz542a2jn55i44adsvdl5ng")))

(define-public crate-prototty_storage-0.24.0 (c (n "prototty_storage") (v "0.24.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13hmmxdaj0mhqzibnfnsfpa6drdagi71fgzjxigz66l8zxv7c1xi")))

(define-public crate-prototty_storage-0.25.0 (c (n "prototty_storage") (v "0.25.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xjxzw5z982hqw14gxc3n52jznb3c97z89sgmy40v766sw779rda")))

(define-public crate-prototty_storage-0.26.0 (c (n "prototty_storage") (v "0.26.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18q9yrbzkbpv88496j84qnnfkd9fxy9aan8izbjqd6a98yjvnl4f")))

(define-public crate-prototty_storage-0.27.0 (c (n "prototty_storage") (v "0.27.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1x2nyq4n08j3bdqx4kzh98asxiyzrizgvaw4zz6h7scdxs3qpd4m")))

(define-public crate-prototty_storage-0.28.0 (c (n "prototty_storage") (v "0.28.0") (d (list (d (n "bincode") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0pj6d5lkzknfvp0gmqrn7dwpb1vqj7dkzhpw133yssijjf5v48gm") (f (quote (("yaml" "serde_yaml") ("json" "serde_json"))))))

(define-public crate-prototty_storage-0.29.0 (c (n "prototty_storage") (v "0.29.0") (d (list (d (n "bincode") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0jrbp5nypj14pqx4lzagkzcif0kc0426xdrysf22hrbxll7qbbnp") (f (quote (("yaml" "serde_yaml") ("json" "serde_json"))))))

