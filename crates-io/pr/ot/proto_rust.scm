(define-module (crates-io pr ot proto_rust) #:use-module (crates-io))

(define-public crate-proto_rust-0.1.0 (c (n "proto_rust") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "proto_core") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full" "tracing"))) (d #t) (k 0)))) (h "1avnv5fgxc5fqdp2hlwih75gk71frsf40n74jaf0hc6cvyy5q16j")))

(define-public crate-proto_rust-0.1.1 (c (n "proto_rust") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "proto_core") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full" "tracing"))) (d #t) (k 0)))) (h "1k5fwjcjwds8x06akyndc92hciqj1w1kzdm9ji17w0qbazz0v9i5")))

(define-public crate-proto_rust-0.2.0 (c (n "proto_rust") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "proto_core") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)))) (h "01vl7zfwwzrjyxshs8121xidgwa036dcx1iga5p6wmnkjp2a7qjr")))

(define-public crate-proto_rust-0.3.0 (c (n "proto_rust") (v "0.3.0") (d (list (d (n "proto_core") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "17zbacfkjg2q89ywii0hkl177vwakbg0v6ff5yr3phnv5c6awzdy")))

(define-public crate-proto_rust-0.3.1 (c (n "proto_rust") (v "0.3.1") (d (list (d (n "proto_core") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1pvbd0083xs29dvz1ir6nxln8kni0a54kqs733f9nlswilkrfcgs")))

(define-public crate-proto_rust-0.4.0 (c (n "proto_rust") (v "0.4.0") (d (list (d (n "proto_core") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1wyv883r0a9awz1l1ldr0zcx793diwfi2jrwydax0h6g339kwlyq")))

(define-public crate-proto_rust-0.4.1 (c (n "proto_rust") (v "0.4.1") (d (list (d (n "proto_core") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jvpny3jrq2vj4pnsx0y721mzagx9p4mkniyd5zf68l4acg7cy7x")))

(define-public crate-proto_rust-0.4.2 (c (n "proto_rust") (v "0.4.2") (d (list (d (n "proto_core") (r "^0.7.2") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1a42w25nr408hppisgal2v3h3fy8pp0vnvirdhwznbjwbb445015")))

(define-public crate-proto_rust-0.5.0 (c (n "proto_rust") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0fw70wciiqjgb46rz45vjswh157nxajsi7gvvq222icya6i1pxbg")))

(define-public crate-proto_rust-0.5.1 (c (n "proto_rust") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0ckk1jib3g90js2jd69xqv3cw2irhbcn2vj7alsks47k6kjz5l1p")))

(define-public crate-proto_rust-0.5.2 (c (n "proto_rust") (v "0.5.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.8.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jsqimqc1mjm8s0pb1vq04if1rm11dnmlabf951zjnnl1j9za2xx")))

(define-public crate-proto_rust-0.6.0 (c (n "proto_rust") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1cvad5zr7wn9xxj6xdxi2c735168hfdwr6mqnlpvi9f9kphv22j6")))

(define-public crate-proto_rust-0.6.1 (c (n "proto_rust") (v "0.6.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1h0aq8klk1wccs5pph33yq1pjsj55v6na2xzinp93x9mmswdhi2f")))

(define-public crate-proto_rust-0.6.2 (c (n "proto_rust") (v "0.6.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.9.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1qv7i7vw1428h4vfq80ar7bn74hqibgm20w6wh5xbwsfmplrd71i")))

(define-public crate-proto_rust-0.7.0 (c (n "proto_rust") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ymqnsn2qff5zcxs9b5irbs80s4zdbv4i4lljvglapbglgf9mzrf")))

(define-public crate-proto_rust-0.7.1 (c (n "proto_rust") (v "0.7.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proto_core") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pbnrrnzfsq3nrx2g6rcl7qxgyx305ya1jpiln4bynpg0nzp862b")))

(define-public crate-proto_rust-0.7.2 (c (n "proto_rust") (v "0.7.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1g6cvf2mrmw106azrbjgn773lx15miydyypwps7h4d6jchmzq3qc")))

(define-public crate-proto_rust-0.8.0 (c (n "proto_rust") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "02js24dcbqlws06pf58kf34blfkm6frildkrxmf2ayj3mfxszy58")))

(define-public crate-proto_rust-0.9.0 (c (n "proto_rust") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "12w2nd185q4sig4pnkabkgj8wkzzjz523grkaqdjn8gypvjxv81n")))

(define-public crate-proto_rust-0.9.1 (c (n "proto_rust") (v "0.9.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "04iycr6is0qr8x7854vdvb6x4dgibw33hv6jafc6mq9zyacnfxxp")))

(define-public crate-proto_rust-0.9.2 (c (n "proto_rust") (v "0.9.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.12.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1j8dmq379n5ifxgrjj4spl3hwh3yrbfv8i559qibvkw0qm69gc5j")))

(define-public crate-proto_rust-0.10.0 (c (n "proto_rust") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0f323gsbzmkzp8cdi1q4b02hfm5liasdvdrjdb1djfqz2jqpg0pj")))

(define-public crate-proto_rust-0.10.1 (c (n "proto_rust") (v "0.10.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proto_core") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1qzklc1bv6ds9g2qj84kbh2hff1cj1p299ay4jdwqxfn5nl1f2lp")))

