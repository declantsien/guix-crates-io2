(define-module (crates-io pr ot prototty_decorator) #:use-module (crates-io))

(define-public crate-prototty_decorator-0.26.0 (c (n "prototty_decorator") (v "0.26.0") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0pnrh0p6l5j3ccybkd2a46dv5gaajldrj46gc6lqpcfivnnyycwk") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_decorator-0.26.1 (c (n "prototty_decorator") (v "0.26.1") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1kckbqxf1pnn7c83505fql359ph2ljmzqj70qp6xpcv1rb71s9mr") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_decorator-0.26.2 (c (n "prototty_decorator") (v "0.26.2") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0j92gn3qq9h9xwlnggg899x4jfm9dmjm7msy6p0a2gk18bszh23z") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_decorator-0.27.0 (c (n "prototty_decorator") (v "0.27.0") (d (list (d (n "prototty_render") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1gkb9f9y2fvap35aiczac4jc9dp1q1zlk707j6dfm5kmlmapzm4w") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_decorator-0.28.0 (c (n "prototty_decorator") (v "0.28.0") (d (list (d (n "prototty_render") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "049yr0f9k6hrxpf64wp105m8pg0f9zprqq1qrrmixjx352hb13b4") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_decorator-0.29.0 (c (n "prototty_decorator") (v "0.29.0") (d (list (d (n "prototty_render") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "06m8szfhlwsdh1snhbg05ix7rjl7lgjszpj20ybdzzmrab4jck15") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

