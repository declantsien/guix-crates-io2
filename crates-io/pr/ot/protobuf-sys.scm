(define-module (crates-io pr ot protobuf-sys) #:use-module (crates-io))

(define-public crate-protobuf-sys-0.1.0+3.19.1 (c (n "protobuf-sys") (v "0.1.0+3.19.1") (d (list (d (n "autocxx") (r "^0.14.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.14.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)))) (h "0mn03d7cfra09pkvnxwascv964p83zpr0hf6hsani4izh2yc2xcp")))

(define-public crate-protobuf-sys-0.1.1+3.19.1 (c (n "protobuf-sys") (v "0.1.1+3.19.1") (d (list (d (n "autocxx") (r "^0.14.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.14.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)))) (h "1078zakrzwpd1p2glsx29sh0jczp8y5x29c4cc2bqd0mv58y5gzc")))

(define-public crate-protobuf-sys-0.1.2+3.19.1 (c (n "protobuf-sys") (v "0.1.2+3.19.1") (d (list (d (n "autocxx") (r "^0.14.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.14.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.54") (d #t) (k 0)) (d (n "protobuf-src") (r "^1.0.0") (d #t) (k 0)))) (h "15f2bfhl3iz7h28x52fvkkrikskay48cz6vbbcjc5nwv0a3ds79q")))

(define-public crate-protobuf-sys-0.2.0+26.1 (c (n "protobuf-sys") (v "0.2.0+26.1") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0.122") (d #t) (k 0)) (d (n "protobuf-src") (r "^2.0.0") (d #t) (k 0)))) (h "1ba0q2ddgdmbnz8d5r999g3l5cgk53zv87kijf1i7d9yy16sk061")))

