(define-module (crates-io pr ot protobuf-support) #:use-module (crates-io))

(define-public crate-protobuf-support-3.0.0-alpha.11 (c (n "protobuf-support") (v "3.0.0-alpha.11") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kifv9qw404zin9x8xphrlhm1acbw20g1r9nklbylvkzbzcbafvz")))

(define-public crate-protobuf-support-3.0.0-alpha.12 (c (n "protobuf-support") (v "3.0.0-alpha.12") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0js3ih16qxh2wkycw1070kx0lzgkjm2579360g2if8qkj6jilccw")))

(define-public crate-protobuf-support-3.0.0-alpha.13 (c (n "protobuf-support") (v "3.0.0-alpha.13") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0k8hngw27q4pgpfi22h19xc9104ahz3pkbbl77rpwjl1y36qwq92")))

(define-public crate-protobuf-support-3.0.0-alpha.14 (c (n "protobuf-support") (v "3.0.0-alpha.14") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1isx0pdz8qwylkxyxiwiic11c8slfmn4blzlhk6gf9b79z0p2nvq")))

(define-public crate-protobuf-support-3.0.0 (c (n "protobuf-support") (v "3.0.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zwx06qlxg5ahw4xs9ry9pi77spvdaw2h9f4zxdf789mj07apdvn")))

(define-public crate-protobuf-support-3.0.1 (c (n "protobuf-support") (v "3.0.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jz87i8r428ndylz96mfj389f9msjcrk2d87v8rrnfmmzhbcslwm")))

(define-public crate-protobuf-support-3.0.2 (c (n "protobuf-support") (v "3.0.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03iv08zd8bqh6bx75v0ml0c4j2vz5qa8jr9vkhjdlw36sqv4dwzb")))

(define-public crate-protobuf-support-3.0.3 (c (n "protobuf-support") (v "3.0.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1sshr4ig9226cn2gljm10rid6pagj1ny3wndwmhgg55l0gwysdzp")))

(define-public crate-protobuf-support-3.1.0 (c (n "protobuf-support") (v "3.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "077f13xizb2dh89a23nprnbks6zlgpi3bxzj2piy4zpw2bz5g8cc")))

(define-public crate-protobuf-support-3.2.0 (c (n "protobuf-support") (v "3.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wn30pnvncvqhdwgf334jcg5xr311sgvgsxw6vxlr08wc2wdgm55")))

(define-public crate-protobuf-support-3.3.0 (c (n "protobuf-support") (v "3.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1776rzbj4ypkaxwn4bc9zhgv0xxvpgskhn1bk8ih70xrykag8wk8")))

(define-public crate-protobuf-support-3.4.0 (c (n "protobuf-support") (v "3.4.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1rz7qdbd4rp4rw6kp3sax38wchs9scf6nq8k1s0k03svhd52kvg1")))

