(define-module (crates-io pr ot protect) #:use-module (crates-io))

(define-public crate-protect-1.0.0 (c (n "protect") (v "1.0.0") (h "0pyr1a862w3vh0613s52lv1068glx5ylrcdj9yhpblwm2z2w3mmp")))

(define-public crate-protect-1.1.0 (c (n "protect") (v "1.1.0") (h "1m2lb37smd5wj47avqqs497flg7zljpcmawrklrvmpdq1s5rfm4w")))

