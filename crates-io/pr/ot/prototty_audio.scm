(define-module (crates-io pr ot prototty_audio) #:use-module (crates-io))

(define-public crate-prototty_audio-0.28.0 (c (n "prototty_audio") (v "0.28.0") (h "0dccw8qgdlh6rhk72d63ywiqz1srn079if8hvfsamnj2byb7nxsp")))

(define-public crate-prototty_audio-0.29.0 (c (n "prototty_audio") (v "0.29.0") (h "1krw6b6ax9zwswrj9m5hhjs9b424xgqpgac45hbm67xrgyf52pnk")))

