(define-module (crates-io pr ot protobuf-mapper) #:use-module (crates-io))

(define-public crate-protobuf-mapper-0.1.0 (c (n "protobuf-mapper") (v "0.1.0") (d (list (d (n "bigdecimal") (r ">=0.0.10, <0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "protobuf-mapper-codegen") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17sqi88jvixxylajvrpr6pmik0afifh4gdnsc50md1y302i9zwya")))

