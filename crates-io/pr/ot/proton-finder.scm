(define-module (crates-io pr ot proton-finder) #:use-module (crates-io))

(define-public crate-proton-finder-0.1.0 (c (n "proton-finder") (v "0.1.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)))) (h "09sqp3anmbmqrqbnf0z9pi2m9144shqw1j42bwqr8nfwgfpjcyaz") (f (quote (("no_tricks")))) (y #t)))

(define-public crate-proton-finder-1.0.0 (c (n "proton-finder") (v "1.0.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)))) (h "1iz27ipx2zbl61yx4wka7298kw85mlg590hlbar24jb9dqfaps64") (f (quote (("no_tricks"))))))

