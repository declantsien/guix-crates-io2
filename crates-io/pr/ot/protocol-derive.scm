(define-module (crates-io pr ot protocol-derive) #:use-module (crates-io))

(define-public crate-protocol-derive-0.3.3 (c (n "protocol-derive") (v "0.3.3") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.7") (d #t) (k 0)))) (h "15d8m2w5bdaafn078x3p2mj9cbpczak9142798nik2635jvjd5v9")))

(define-public crate-protocol-derive-0.3.4 (c (n "protocol-derive") (v "0.3.4") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0lr4p6sm243i23i9kic8bpy0j0rlm86ibkxdxq1f96rr6idxdm7n")))

(define-public crate-protocol-derive-0.3.5 (c (n "protocol-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "149hwp84jmbfiqc7xn7h2hizkbyw9vkl14nnbvazkfi7wp4s44jx")))

(define-public crate-protocol-derive-0.3.6 (c (n "protocol-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1mw52mxllf6yy017zg9prr35mkh6z8hq43ka4cq476hssxyxnr7l")))

(define-public crate-protocol-derive-0.3.7 (c (n "protocol-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "0d8andyymn606zbx6maxjqnx0f7gjx3zqzdp9710m83lh1arsy04")))

(define-public crate-protocol-derive-0.3.8 (c (n "protocol-derive") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1f5srxfvgwr7j920qg59jkpc51z6akakpdlyqv008f7jh9mnxp5i")))

(define-public crate-protocol-derive-0.4.0 (c (n "protocol-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "04szgqiqgv8gg3cf2sf05aj2awwmx6vh0shxglax0iiiy09ihvky")))

(define-public crate-protocol-derive-0.4.1 (c (n "protocol-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "11a6p6k5awsgr1bvr5qxrhh8sjvg1sy0hx6gyla7p4c70yis9msy")))

(define-public crate-protocol-derive-0.4.2 (c (n "protocol-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "0rkn8rr1j4v8z2w94r297kcpc0ww8zz5jhrgbdpazch842w3nwgy")))

(define-public crate-protocol-derive-0.4.3 (c (n "protocol-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1nd59ansj697vqc57flxnyc0hbskjn3rf3vbj5jvn3gmf767jij4")))

(define-public crate-protocol-derive-0.4.4 (c (n "protocol-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "0ji0xg66gwp50i0j4x85qj6vvhp552fb0hv9786cvngpg8mv5ihq")))

(define-public crate-protocol-derive-0.5.0 (c (n "protocol-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "16cl78saakr0zavznkfy8mdbdpfaargffp1j7l6jhbj02ym0487i")))

(define-public crate-protocol-derive-0.5.1 (c (n "protocol-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "0ww55hy114hqnbvxgsgnvbcj7d23cvabkzj4q8zxdx1gy5bvx19d")))

(define-public crate-protocol-derive-0.5.2 (c (n "protocol-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "05la4qpmpbv4wg2sys64ss8lmds62kn7gczq1dcs77p7r7p313yc")))

(define-public crate-protocol-derive-0.5.3 (c (n "protocol-derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1k7lkgipbiskvg3545gfjdcnr40qi2sv30za7mypn9ap2i156kp1")))

(define-public crate-protocol-derive-0.5.4 (c (n "protocol-derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1yr3dpyqhf1s8kvjkjbnadjynwqiyhmva1a868rk4vv3r19pc5ip")))

(define-public crate-protocol-derive-0.5.5 (c (n "protocol-derive") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1w6acdmn1kw5ci53wy0qwg2cqjh5d5m6wpgagi2i91823xm3iwr6")))

(define-public crate-protocol-derive-0.5.6 (c (n "protocol-derive") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "02rvy3czqjjrdxjg3z2hw4dpfmc9y98qcjvh5f2ljv4dg5grkscz")))

(define-public crate-protocol-derive-0.5.7 (c (n "protocol-derive") (v "0.5.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "15ib8mnr54li7yf77ni59cjspn1smllapncblgh43kqibpixcsff")))

(define-public crate-protocol-derive-0.5.8 (c (n "protocol-derive") (v "0.5.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "065q6hcl1s348wl36wmx984di94bi5yz2nczdzyavz02flc9dfm5")))

(define-public crate-protocol-derive-0.5.9 (c (n "protocol-derive") (v "0.5.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "1shzkbcbrpj80imjw9ff19h7fm2spwj3dlk8vrjnvm8kdh2ab2vq")))

(define-public crate-protocol-derive-0.5.10 (c (n "protocol-derive") (v "0.5.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0mzd8g0wyb5immdhhr6r7w48gr2gnv3274p94kiwk6bzx3nkzama")))

(define-public crate-protocol-derive-0.5.11 (c (n "protocol-derive") (v "0.5.11") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "130hj97pky7cg08bsd8vncdqxr4iqy2wi0k629mljigy66zpkr6l")))

(define-public crate-protocol-derive-0.5.12 (c (n "protocol-derive") (v "0.5.12") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0akh15a4lsg8p0ac2pp0kdfvp23yi6kns0z4vafvh1rp53gz225x")))

(define-public crate-protocol-derive-0.5.13 (c (n "protocol-derive") (v "0.5.13") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0s995jggjmigr6bl0ab6ggjz586ihzwz3i8mbmr1m639qraj6n12")))

(define-public crate-protocol-derive-0.6.0 (c (n "protocol-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0yk3za331z967b68fjjcjsrzjfdm6rya30i2vqcz2j35h2hxgjl9")))

(define-public crate-protocol-derive-0.6.1 (c (n "protocol-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "08rba97h101cxrk01yia2c0lfnzx9nnppm6h0zb7v572bidswma4")))

(define-public crate-protocol-derive-0.6.2 (c (n "protocol-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0l48zdnf4mdcf0iaq0q2dymr2j714qrk2li8k8r8xl591xlkaxfq")))

(define-public crate-protocol-derive-0.7.0 (c (n "protocol-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1j31n9n7k5i4ijymav5mbs4f2gz5yn0xkaw62hh2awkw3yayqk1z")))

(define-public crate-protocol-derive-0.7.1 (c (n "protocol-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "189bf70nlp3irjypl9q7lrr1k6alis8c6w71h01ijjhs73yyi2j7")))

(define-public crate-protocol-derive-0.7.2 (c (n "protocol-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0wym5i2bivjyvfmbgk76gmx6mmkrxcvczig946js0s5h21xg4gzv")))

(define-public crate-protocol-derive-1.0.0 (c (n "protocol-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0az9kvchzcdqm2pp4c3q2zrkbs0l84n69srwbxp2n9hv8gl78p67")))

(define-public crate-protocol-derive-1.0.1 (c (n "protocol-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1sxjq7kx0514svzhk7a2vnasih9c301a04faasp303nkywcj61w7")))

(define-public crate-protocol-derive-1.0.2 (c (n "protocol-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1akdxdg3rn3sxhy1gr9k98y4a6xrig1y74khilzpb17f87kn98l2")))

(define-public crate-protocol-derive-1.0.3 (c (n "protocol-derive") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "00a90f7rv1xbqqg0g5pnbzjsl5xf3szl6dmxmzrv7xqc5k5nxq51")))

(define-public crate-protocol-derive-2.0.0 (c (n "protocol-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "05jvd3k23vdk3djnpp1c35ihv9x3hfmq3686saqm6ppg8q79vfrb")))

(define-public crate-protocol-derive-3.0.0 (c (n "protocol-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1m8yvc7k4xpsd5qlw59k1j83cjv9wkfcm266xj4ibakm2jcmddnp")))

(define-public crate-protocol-derive-3.1.0 (c (n "protocol-derive") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0yhdl1ajzw4irjpbvvvchykab0yhvhji2f0q60mbh04halrjgiar")))

(define-public crate-protocol-derive-3.1.1 (c (n "protocol-derive") (v "3.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0mzllwgrvl0l4k1anw0lq18xphz1pg0ng7sjyzh3j5qirs6gp4p8")))

(define-public crate-protocol-derive-3.1.2 (c (n "protocol-derive") (v "3.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "06xnp9zwmk0jxafd59m6zggqsn9ms1gwz5rjl877bhanin6nnxl5")))

(define-public crate-protocol-derive-3.1.3 (c (n "protocol-derive") (v "3.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "071612x172h4y6jxx9ckvsgr5lnm2b6gqfsnj7ds6f5xp3p4g8g9")))

(define-public crate-protocol-derive-3.1.4 (c (n "protocol-derive") (v "3.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1wngsifzy2qb26ws15rvhi0pcbn44dxwnvspxy9v20k44aj8zgxb") (y #t)))

(define-public crate-protocol-derive-3.1.5 (c (n "protocol-derive") (v "3.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "03xxfgvm6y1698pmb8w6g35vwbfg879ipq475p82lfvmnrn3p6ln")))

(define-public crate-protocol-derive-3.1.6 (c (n "protocol-derive") (v "3.1.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0xhmidbx8f3a881zdbb4jb76rrqzmr4rn7mavyg5jam5yahvshn3")))

(define-public crate-protocol-derive-3.1.7 (c (n "protocol-derive") (v "3.1.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "155fj9yy8ishcjf12fndfc47xjg8g0mg4x2n5pbbjf0f15mq0x0s")))

(define-public crate-protocol-derive-3.1.8 (c (n "protocol-derive") (v "3.1.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0wrvrj6p0i5fvrcr64lbj1ifkxa4ivdr8i6ka6mjixfic3rwdrhd")))

(define-public crate-protocol-derive-3.1.9 (c (n "protocol-derive") (v "3.1.9") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1iqipvhss0lma4i206d5vrbliyaabndbq0637knir79a8nrv643z")))

(define-public crate-protocol-derive-3.2.0 (c (n "protocol-derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0qzqk1av8pqc3a4zrfnm9plckxb72ilk4lkc8m273myfdkhlkhms")))

(define-public crate-protocol-derive-3.2.1 (c (n "protocol-derive") (v "3.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0khsg80ps567j54izlamxqnjp4j3gib3k1zl96kriyy8s1ydlwln")))

(define-public crate-protocol-derive-3.2.2 (c (n "protocol-derive") (v "3.2.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0xj1i53dr4qhllvyllx3n4vmwalclxcs228pdg61n29b0xcbmlqp")))

(define-public crate-protocol-derive-3.2.3 (c (n "protocol-derive") (v "3.2.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0fzky8k697ybjc4saa000yfrbi21is6ib9qwabyzc6l92py0rkyk")))

(define-public crate-protocol-derive-3.2.4 (c (n "protocol-derive") (v "3.2.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1x0qqalq75z5f4ip1ns8qac6bpfq1q0gcq49jpcm026gyykx2g53")))

(define-public crate-protocol-derive-3.2.5 (c (n "protocol-derive") (v "3.2.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0vbq8fq5m1hdd40jhdhpxvgggmj2vwln5d0hlwbgnpij994bidpr")))

(define-public crate-protocol-derive-3.2.6 (c (n "protocol-derive") (v "3.2.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0mhbpxfddvk8ygrz7zbg199wriyp0ca58s11q60yxdf6klrci2ml")))

(define-public crate-protocol-derive-3.2.7 (c (n "protocol-derive") (v "3.2.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "075sa33lrd9gxgclmxxgi3w2jsf0az2v7nx2q3n0pmf806zfhz2x")))

(define-public crate-protocol-derive-3.2.8 (c (n "protocol-derive") (v "3.2.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0m0n5cdsmdngwwm96im3jrsbx68p1xaw284i3hmvw3z0d3nrmf6x")))

(define-public crate-protocol-derive-3.3.0 (c (n "protocol-derive") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "02mhv7khidwi9ai6vm4k789dqs5ynk2m5rixrx44qi47s399a89j")))

(define-public crate-protocol-derive-3.4.0 (c (n "protocol-derive") (v "3.4.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0ivy7zyqgqgxg11mbjp5kiqarr3lbkp9l7mmcvcym64854q7yr18")))

