(define-module (crates-io pr ot protopathy) #:use-module (crates-io))

(define-public crate-protopathy-0.0.0 (c (n "protopathy") (v "0.0.0") (d (list (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vzjmpaywc32mjid9hkbrb5i1z5p7pgwz1i3cj9vhpbh9h8413sz") (f (quote (("default" "tracing")))) (s 2) (e (quote (("tracing" "dep:tracing")))) (r "1.70")))

(define-public crate-protopathy-0.0.1 (c (n "protopathy") (v "0.0.1") (d (list (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "triggered") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "05zrn614ksspb17qxkv64942s5pqazcjvlwcp6ja94z3jc5q19zb") (f (quote (("tcp") ("default" "tcp" "tracing")))) (s 2) (e (quote (("tracing" "dep:tracing")))) (r "1.70")))

