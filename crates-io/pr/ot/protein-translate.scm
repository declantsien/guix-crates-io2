(define-module (crates-io pr ot protein-translate) #:use-module (crates-io))

(define-public crate-protein-translate-0.1.0 (c (n "protein-translate") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 2)))) (h "0f3kvgyzm821z9vkrl7xglvyji15rg0cdrfmpxridjxmj4n4c56v")))

(define-public crate-protein-translate-0.1.1 (c (n "protein-translate") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 2)))) (h "1gx7875p3zzgd148a84yfqakggqixn3wq1bcpmp3f569dabq7xb7")))

(define-public crate-protein-translate-0.1.2 (c (n "protein-translate") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 2)))) (h "0rcr6vai7vzbwpblf6gamzan5g7m6ig0da9wwk65dj931njbklfp")))

(define-public crate-protein-translate-0.2.0 (c (n "protein-translate") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 2)))) (h "120gbbsn1hwd86s89992br8bgnblxdxz2s2wnq9nw03r962hp9xg")))

