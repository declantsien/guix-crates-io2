(define-module (crates-io pr ot prototty) #:use-module (crates-io))

(define-public crate-prototty-0.1.0 (c (n "prototty") (v "0.1.0") (h "1vdv7d22xdyx2rhsy854j4p8c1p8kbg3nw44pfgbc7klxk6hm5xn")))

(define-public crate-prototty-0.2.0 (c (n "prototty") (v "0.2.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0z0i6y3hk7wd3kc2zxf1nw3a08q81281zkbz47yihsxlm8kcf511")))

(define-public crate-prototty-0.3.0 (c (n "prototty") (v "0.3.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0bapfdx0gllhixjd79n05c143rdf1dqgblmj0hk3f2h6n20nq0s0")))

(define-public crate-prototty-0.4.0 (c (n "prototty") (v "0.4.0") (d (list (d (n "ansi_colour") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1dzkbbch3wamzczapjbswxw3vxq11v13pvzb83l75sjvldh4r2w0")))

(define-public crate-prototty-0.6.0 (c (n "prototty") (v "0.6.0") (d (list (d (n "ansi_colour") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19qiaxz39yqq61f1j6fmfkppx0ha6i9rknwm0l6inl5qa8wgmasn")))

(define-public crate-prototty-0.7.0 (c (n "prototty") (v "0.7.0") (d (list (d (n "ansi_colour") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1f4rcz5h6j5pcavb6w5m9nwki0y00v4fjlir0ag8xvwnr2hqhx89")))

(define-public crate-prototty-0.7.1 (c (n "prototty") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1smdbxmx8vh1lhy7pchdlxsbyzycw2mlj305rmjw0iqn8kimbvna")))

(define-public crate-prototty-0.8.0 (c (n "prototty") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "049f96wp0i0vl39g639kn0gagj5ry9bdj7wrq5889b88j5alidzn")))

(define-public crate-prototty-0.8.1 (c (n "prototty") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0cpxj2v325rcac2ah0wvn1l09885rlamxpss7prbkn5mq2xxiqjg")))

(define-public crate-prototty-0.8.2 (c (n "prototty") (v "0.8.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hhb4g5gg6sfaw7922c1lw4h5nm8r6kpnfl65c3dli5srlcd3d14")))

(define-public crate-prototty-0.9.0 (c (n "prototty") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0b1dbffjyzd0i0a9gjcwdkr8shv7xakgmj0skyvwqhw36rz9xmy1")))

(define-public crate-prototty-0.10.0 (c (n "prototty") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1kf6plwmgwp03xcx29293qsyh5qzfg8jjqk846rfwqkdpz3p9nx6")))

(define-public crate-prototty-0.11.0 (c (n "prototty") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ql8mj3wncqgkhsashl2n052w54ic6m6jg79x1bzhk1mwyd4w2w8")))

(define-public crate-prototty-0.11.1 (c (n "prototty") (v "0.11.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yfl6fnv4ry6xy6yhxxxjv7f36iqknpn949magf7hwwb7s9nykd5")))

(define-public crate-prototty-0.11.2 (c (n "prototty") (v "0.11.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14p9k6ifrpd4wri3sfz8sz2a42s69yc8mz83qdpzq802ff8nwfji")))

(define-public crate-prototty-0.11.3 (c (n "prototty") (v "0.11.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ni4l5c3c1klmbxd09kbjwfflcim3dhyp6b872mlnnxip0ynjp3a")))

(define-public crate-prototty-0.12.0 (c (n "prototty") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g8g3d9vdwx4xwf16djnnlvmlx272p7q0k8ywp2js1dljhaj42xv")))

(define-public crate-prototty-0.13.0 (c (n "prototty") (v "0.13.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "07xcj65kfcqxplsi5w8c8dq713p8ngvhbrr82mm38fbz774x3nmq")))

(define-public crate-prototty-0.14.0 (c (n "prototty") (v "0.14.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kjb7qfhqgk7ynp49vbi8cmz2d3jq90qk9169425r3v9lbv8xlp8")))

(define-public crate-prototty-0.15.0 (c (n "prototty") (v "0.15.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1dhsxkw8zlwk8g2jxsm5yj680myan48n7bfgzcm3l5prgv15f568")))

(define-public crate-prototty-0.16.0 (c (n "prototty") (v "0.16.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0piwbh80b0dk0f6zi22bcr2nq4j8i5ba37zh5z0hh9qjvw481ys6")))

(define-public crate-prototty-0.17.0 (c (n "prototty") (v "0.17.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1lkbzf5qbyssnpb6vz9330fjmlbssfkp7yr2v7ngpdv80rfr3v4m")))

(define-public crate-prototty-0.17.1 (c (n "prototty") (v "0.17.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hw1ihzqz26a66m96jixynyjzlc8a783337g4hkqbsd0x3dk0bvc")))

(define-public crate-prototty-0.18.0 (c (n "prototty") (v "0.18.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19g71nbsjgnn1i6v3zs8bwfm2rbab3zjhqid71a7mbi1vq13y7pp")))

(define-public crate-prototty-0.19.0 (c (n "prototty") (v "0.19.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zk5grc0vssq6lmksfvf8y13d2yvczm8na3nvzlzk3lb6a2jhr0b")))

(define-public crate-prototty-0.20.0 (c (n "prototty") (v "0.20.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "grid_2d") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1r85vh466rbwqx8q3mn0klmzm5ly2whdwc2qa3z868hl4blvbhfm")))

(define-public crate-prototty-0.21.0 (c (n "prototty") (v "0.21.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nba2svr97qfrdbnfjmg4045bhbynbakxckam08k6yc115xn8w97") (f (quote (("serialize" "serde/serde_derive" "coord_2d/serialize"))))))

(define-public crate-prototty-0.22.0 (c (n "prototty") (v "0.22.0") (d (list (d (n "prototty_common") (r "^0.22") (d #t) (k 0)) (d (n "prototty_input") (r "^0.22") (d #t) (k 0)) (d (n "prototty_render") (r "^0.22") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0p50nm28lrn2wmm54hhk4yl0pbb6iiwn12ygm3h60dqng3y477c2") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_common/serialize"))))))

(define-public crate-prototty-0.23.0 (c (n "prototty") (v "0.23.0") (d (list (d (n "prototty_common") (r "^0.23") (d #t) (k 0)) (d (n "prototty_input") (r "^0.23") (d #t) (k 0)) (d (n "prototty_render") (r "^0.23") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.23") (o #t) (d #t) (k 0)))) (h "1yslkxs7aa3j0y4ix7wzb9vchn6wxdqp7pqzfm93krhhhny42h23") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_common/serialize"))))))

(define-public crate-prototty-0.24.0 (c (n "prototty") (v "0.24.0") (d (list (d (n "prototty_common") (r "^0.24") (d #t) (k 0)) (d (n "prototty_input") (r "^0.24") (d #t) (k 0)) (d (n "prototty_render") (r "^0.24") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.24") (o #t) (d #t) (k 0)))) (h "165hqpx8g8s3fqdzdk2sj06akjyzx2af5lss0i2kraz60fw29ggd") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_common/serialize"))))))

(define-public crate-prototty-0.25.0 (c (n "prototty") (v "0.25.0") (d (list (d (n "prototty_common") (r "^0.25") (d #t) (k 0)) (d (n "prototty_input") (r "^0.25") (d #t) (k 0)) (d (n "prototty_render") (r "^0.25") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.25") (o #t) (d #t) (k 0)))) (h "1wymfssfxhcnq1mijkvhsy1i2xfi7n6vs292h3ckvhrb2fak335w") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_common/serialize"))))))

(define-public crate-prototty-0.26.0 (c (n "prototty") (v "0.26.0") (d (list (d (n "prototty_decorator") (r "^0.26") (d #t) (k 0)) (d (n "prototty_input") (r "^0.26") (d #t) (k 0)) (d (n "prototty_menu") (r "^0.26") (d #t) (k 0)) (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "prototty_text") (r "^0.26") (d #t) (k 0)))) (h "0993lmpmqb8f185y6j9x14vakypg23n3qbf8lzvvczc6dr2gbhrv") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_text/serialize" "prototty_decorator/serialize" "prototty_menu/serialize"))))))

(define-public crate-prototty-0.27.0 (c (n "prototty") (v "0.27.0") (d (list (d (n "prototty_decorator") (r "^0.27") (d #t) (k 0)) (d (n "prototty_input") (r "^0.27") (d #t) (k 0)) (d (n "prototty_menu") (r "^0.27") (d #t) (k 0)) (d (n "prototty_render") (r "^0.27") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "prototty_text") (r "^0.27") (d #t) (k 0)))) (h "07j7ygrjbikzb964wa2hjj0mb34d7y9kmc8r8f1hfiqha3c43y1p") (f (quote (("storage" "prototty_storage") ("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_text/serialize" "prototty_decorator/serialize" "prototty_menu/serialize"))))))

(define-public crate-prototty-0.28.0 (c (n "prototty") (v "0.28.0") (d (list (d (n "prototty_app") (r "^0.28") (d #t) (k 0)) (d (n "prototty_decorator") (r "^0.28") (d #t) (k 0)) (d (n "prototty_event_routine") (r "^0.28") (d #t) (k 0)) (d (n "prototty_input") (r "^0.28") (d #t) (k 0)) (d (n "prototty_menu") (r "^0.28") (d #t) (k 0)) (d (n "prototty_render") (r "^0.28") (d #t) (k 0)) (d (n "prototty_text") (r "^0.28") (d #t) (k 0)))) (h "09n4iykq75hig6w2p0196yqzx6khs3c3kyznrqy0d1b8jy91s2cd") (f (quote (("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_text/serialize" "prototty_decorator/serialize" "prototty_menu/serialize"))))))

(define-public crate-prototty-0.29.0 (c (n "prototty") (v "0.29.0") (d (list (d (n "prototty_app") (r "^0.29") (d #t) (k 0)) (d (n "prototty_decorator") (r "^0.29") (d #t) (k 0)) (d (n "prototty_event_routine") (r "^0.29") (d #t) (k 0)) (d (n "prototty_input") (r "^0.29") (d #t) (k 0)) (d (n "prototty_menu") (r "^0.29") (d #t) (k 0)) (d (n "prototty_render") (r "^0.29") (d #t) (k 0)) (d (n "prototty_text") (r "^0.29") (d #t) (k 0)))) (h "0j479lb6xrg5shwpbw4fnjvifs4s7kyyyqz4spbwfg1g4im1jb4v") (f (quote (("serialize" "prototty_render/serialize" "prototty_input/serialize" "prototty_text/serialize" "prototty_decorator/serialize" "prototty_menu/serialize"))))))

