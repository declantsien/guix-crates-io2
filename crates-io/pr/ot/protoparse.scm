(define-module (crates-io pr ot protoparse) #:use-module (crates-io))

(define-public crate-protoparse-0.0.1 (c (n "protoparse") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0jlv76a2d8zh0ls5yfn8fybw6cz6qsl0bdvdgv08c9xsb4bivn4b")))

(define-public crate-protoparse-0.0.2 (c (n "protoparse") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "00k9grz4pvfbjk1qd6q59nb586cr23slvpcrpqg3zpvp1w8d6rc7")))

(define-public crate-protoparse-0.0.3 (c (n "protoparse") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1r3h1a668bz87bis11kcqljwc64g7pmi509r8yz5rkh7ba9g02d0")))

(define-public crate-protoparse-0.0.4 (c (n "protoparse") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1hq4ggnnlbl7gxd58ad6qnf0j35v3ld354r4f3p2wvvj1l6fd4yr")))

