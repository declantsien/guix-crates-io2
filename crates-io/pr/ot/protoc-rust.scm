(define-module (crates-io pr ot protoc-rust) #:use-module (crates-io))

(define-public crate-protoc-rust-1.3.0 (c (n "protoc-rust") (v "1.3.0") (d (list (d (n "protobuf") (r "^1.3.0") (d #t) (k 0)) (d (n "protoc") (r "^1.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0nnsldr9660mia59hs2nibi0sycxn6rlb2pi4hwridrxzfpkpdh6")))

(define-public crate-protoc-rust-1.3.1 (c (n "protoc-rust") (v "1.3.1") (d (list (d (n "protobuf") (r "^1.3.1") (d #t) (k 0)) (d (n "protoc") (r "^1.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0sksjr8c0361yfg7bhdqgbgc7ll9d9rg7b50sqiky6v12w93abiq")))

(define-public crate-protoc-rust-1.4.0 (c (n "protoc-rust") (v "1.4.0") (d (list (d (n "protobuf") (r "^1.4.0") (d #t) (k 0)) (d (n "protoc") (r "^1.4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "194mn7rds01bkj8cdd2bq18zqyvvm5jzhbv58f72wr0vn8shw5sb")))

(define-public crate-protoc-rust-1.4.1 (c (n "protoc-rust") (v "1.4.1") (d (list (d (n "protobuf") (r "^1.4.1") (d #t) (k 0)) (d (n "protoc") (r "^1.4.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1sl1acwhjw6hbarip3q8mi01kgrz1qm0x09844s7fw7j3x2wi2bd")))

(define-public crate-protoc-rust-1.4.2 (c (n "protoc-rust") (v "1.4.2") (d (list (d (n "protobuf") (r "^1.4.2") (d #t) (k 0)) (d (n "protoc") (r "^1.4.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0y3qx04s62bhchsr3d23mdalcqrl4fzgm2aiz6s74mvms97jfhq9")))

(define-public crate-protoc-rust-1.4.3 (c (n "protoc-rust") (v "1.4.3") (d (list (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "protoc") (r "^1.4.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0ibc66m0ff1p3qiw0i7mggil5zd6vz7m5xl3sichl0iddgssf4g2")))

(define-public crate-protoc-rust-1.4.4 (c (n "protoc-rust") (v "1.4.4") (d (list (d (n "protobuf") (r "^1.4.4") (d #t) (k 0)) (d (n "protoc") (r "^1.4.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0fpiljia7qkkf7m9i5hc3skvsk5gw5z1479xxzc6nj54i4lmmi3h")))

(define-public crate-protoc-rust-1.5.0 (c (n "protoc-rust") (v "1.5.0") (d (list (d (n "protobuf") (r "^1.5.0") (d #t) (k 0)) (d (n "protoc") (r "^1.5.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0fnff0qygccx0g1c8fjifbywnw5pvy384z0bjq3ac8mzp9x899c6")))

(define-public crate-protoc-rust-1.5.1 (c (n "protoc-rust") (v "1.5.1") (d (list (d (n "protobuf") (r "^1.5.1") (d #t) (k 0)) (d (n "protoc") (r "^1.5.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0s5vy4xw2gbx3dpn24lhfbbi1iybp8332gnxmd5n5p0g9vipf93c")))

(define-public crate-protoc-rust-1.4.5 (c (n "protoc-rust") (v "1.4.5") (d (list (d (n "protobuf") (r "^1.4.5") (d #t) (k 0)) (d (n "protoc") (r "^1.4.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0blz0kxdgq469db97m2cqxmk9vccxxvifwlm60ggxqqan4gh4qql")))

(define-public crate-protoc-rust-1.6.0 (c (n "protoc-rust") (v "1.6.0") (d (list (d (n "protobuf") (r "^1.6.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^1.6.0") (d #t) (k 0)) (d (n "protoc") (r "^1.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1sfakkkmnmglrrn1ivf6ccl1k2zah5favb9xbap1kzndlrd4hj48")))

(define-public crate-protoc-rust-1.7.1 (c (n "protoc-rust") (v "1.7.1") (d (list (d (n "protobuf") (r "^1.7.1") (d #t) (k 0)) (d (n "protoc") (r "^1.7.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0rll3n8plc77nfarl2x0dc5z4c9mqn3l95f93zy0nfcwiwfm4n97")))

(define-public crate-protoc-rust-2.0.0 (c (n "protoc-rust") (v "2.0.0") (d (list (d (n "protobuf") (r "^2.0.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0.0") (d #t) (k 0)) (d (n "protoc") (r "^2.0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "09gbsaxl3s2bx3ldns12lm4lpzz0if576sk0572s4a1qd61sgzp2")))

(define-public crate-protoc-rust-2.0.1 (c (n "protoc-rust") (v "2.0.1") (d (list (d (n "protobuf") (r "^2.0.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.0.1") (d #t) (k 0)) (d (n "protoc") (r "^2.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "15jynf49mbdhlscv5jqmmsk47hmksyhnk8wqa9ghl7qcqmmsq6s5")))

(define-public crate-protoc-rust-1.7.2 (c (n "protoc-rust") (v "1.7.2") (d (list (d (n "protobuf") (r "= 1.7.2") (d #t) (k 0)) (d (n "protoc") (r "= 1.7.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0lnpjmml5gxpmpghfyskqjrzx1qnbwcxfaqr9wzgqfqfai9gq702")))

(define-public crate-protoc-rust-2.0.2 (c (n "protoc-rust") (v "2.0.2") (d (list (d (n "protobuf") (r "= 2.0.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1wi68ngqpw944cyqnnq47hqjc5q033adk3nvkd370lmp0prn4has")))

(define-public crate-protoc-rust-1.7.3 (c (n "protoc-rust") (v "1.7.3") (d (list (d (n "protobuf") (r "= 1.7.3") (d #t) (k 0)) (d (n "protoc") (r "= 1.7.3") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1z9sch61z7shijnm6mn553ry40rckqkxh8jvhg8zavdrf4qc5ca7")))

(define-public crate-protoc-rust-2.0.3 (c (n "protoc-rust") (v "2.0.3") (d (list (d (n "protobuf") (r "= 2.0.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.3") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.3") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1wfpw5lhpic8b96sgil9l9wn924k4sm01169ycrbz28fxv4wqjlq")))

(define-public crate-protoc-rust-1.7.4 (c (n "protoc-rust") (v "1.7.4") (d (list (d (n "protobuf") (r "= 1.7.4") (d #t) (k 0)) (d (n "protoc") (r "= 1.7.4") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0h7pdlyx42f6dd7h69m2lbp1c0n0ljivzcn2x50rdbv697v99s1i")))

(define-public crate-protoc-rust-2.0.4 (c (n "protoc-rust") (v "2.0.4") (d (list (d (n "protobuf") (r "= 2.0.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.4") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.4") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0drcabn7a82a72m2dj963cpqpj9j9g7y1abqk850sr6sf9cagjr5")))

(define-public crate-protoc-rust-2.0.5 (c (n "protoc-rust") (v "2.0.5") (d (list (d (n "protobuf") (r "= 2.0.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.5") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.5") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1h3rq81i6rk1d5lwzq1p44jp9xgs1bwd68h96w3ps8jdcjnrz2c1")))

(define-public crate-protoc-rust-2.1.0 (c (n "protoc-rust") (v "2.1.0") (d (list (d (n "protobuf") (r "= 2.1.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.0") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "099np3pfb3h1wp3yy6wyb61iydk21wfwwd9dqk074359miakjl7j")))

(define-public crate-protoc-rust-2.1.1 (c (n "protoc-rust") (v "2.1.1") (d (list (d (n "protobuf") (r "= 2.1.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.1") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0vpcgzgqvna0ribr86l253wkvlwasf06axrszknckyjz8yhzjznd")))

(define-public crate-protoc-rust-2.1.2 (c (n "protoc-rust") (v "2.1.2") (d (list (d (n "protobuf") (r "= 2.1.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "024y4v1mk7jfc3d3gspyj2p51bddc9m4wjnqa6fhmnlyciyjq4as")))

(define-public crate-protoc-rust-2.1.3 (c (n "protoc-rust") (v "2.1.3") (d (list (d (n "protobuf") (r "= 2.1.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.3") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.3") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0q4xarll1397f4wpw6dr3c4mhb692jkl15mxg4ay2xqmbv7dk2vw")))

(define-public crate-protoc-rust-2.1.4 (c (n "protoc-rust") (v "2.1.4") (d (list (d (n "protobuf") (r "= 2.1.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.4") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.4") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "19pzc7kwq5djgc031dbbwhr9k99avb3zrqlbaw73cjcwyqp9hdr5")))

(define-public crate-protoc-rust-2.2.0 (c (n "protoc-rust") (v "2.2.0") (d (list (d (n "protobuf") (r "= 2.2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.0") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1qq4f70j336gp1c19yv6369s40p1608l8q3s36h3kw6lgryg4dz1")))

(define-public crate-protoc-rust-2.2.1 (c (n "protoc-rust") (v "2.2.1") (d (list (d (n "protobuf") (r "= 2.2.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.1") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0byhh06h4y20bnrpl5xdnzy7i9897r2lxn75jnpza3vyvqs3mip1")))

(define-public crate-protoc-rust-2.2.2 (c (n "protoc-rust") (v "2.2.2") (d (list (d (n "protobuf") (r "= 2.2.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1q78nq35r098r6l37iwim2vd1ijh227v9s80zmcm0hg3hshdwhy2")))

(define-public crate-protoc-rust-2.2.3 (c (n "protoc-rust") (v "2.2.3") (d (list (d (n "protobuf") (r "= 2.2.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.3") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.3") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "07l55sxk5wvsq4gl3rxzixlffjb77g8hd72pc0nzry635hfw3sps")))

(define-public crate-protoc-rust-2.2.4 (c (n "protoc-rust") (v "2.2.4") (d (list (d (n "protobuf") (r "= 2.2.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.4") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1cblwj6nsmd9v9n3ncnh5s849d456cw730gvc3wdg9k8546rbww1")))

(define-public crate-protoc-rust-2.1.5 (c (n "protoc-rust") (v "2.1.5") (d (list (d (n "protobuf") (r "= 2.1.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.1.5") (d #t) (k 0)) (d (n "protoc") (r "= 2.1.5") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1i65z8kca5b0pn87qriqrsnrpdswcmgcfqz3rrb1yw0pp8wsy1g8")))

(define-public crate-protoc-rust-2.0.6 (c (n "protoc-rust") (v "2.0.6") (d (list (d (n "protobuf") (r "= 2.0.6") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.0.6") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.6") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0qqyihv0jd1igpx9a46g6wf8abnwbrki3q0rk5smhj1rsrcmjmhl")))

(define-public crate-protoc-rust-2.2.5 (c (n "protoc-rust") (v "2.2.5") (d (list (d (n "protobuf") (r "= 2.2.5") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.2.5") (d #t) (k 0)) (d (n "protoc") (r "= 2.2.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "03d52amw4qbs8krrshsrb759jkywfxbq2rxcs5mazshamhp4s4nb")))

(define-public crate-protoc-rust-2.3.0 (c (n "protoc-rust") (v "2.3.0") (d (list (d (n "protobuf") (r "= 2.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.3.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "03qp9ijcjm84ykn4sjcflxmmznida770gfjhyshj3bl9q5yndmns")))

(define-public crate-protoc-rust-2.3.1 (c (n "protoc-rust") (v "2.3.1") (d (list (d (n "protobuf") (r "= 2.3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.3.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "12v90ah6cqsr5j3hkd116k73zknpk58knsbi2zaa1019ga6iqpb7")))

(define-public crate-protoc-rust-2.4.0 (c (n "protoc-rust") (v "2.4.0") (d (list (d (n "protobuf") (r "= 2.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.4.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1sgha8v70790ikyd5kycw3vdzbs97s7nyrm8cb8xai0l6znn9a0d")))

(define-public crate-protoc-rust-2.4.2 (c (n "protoc-rust") (v "2.4.2") (d (list (d (n "protobuf") (r "= 2.4.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.4.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1w5ilyg4268yk0k185mkad6rnmg7mcyqlljlr4qm8wc8frac178f")))

(define-public crate-protoc-rust-2.5.0 (c (n "protoc-rust") (v "2.5.0") (d (list (d (n "protobuf") (r "= 2.5.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.5.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1dnnv143f5dvv8291v42dwz5dd92337r1ry4742vlddy2mgki9rr")))

(define-public crate-protoc-rust-2.6.0 (c (n "protoc-rust") (v "2.6.0") (d (list (d (n "protobuf") (r "= 2.6.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0vw137jm6j88bmyv6v7kkb6zbpnzjz8c7qqlgskiz9dzqk91ljmf")))

(define-public crate-protoc-rust-1.7.5 (c (n "protoc-rust") (v "1.7.5") (d (list (d (n "protobuf") (r "= 1.7.5") (d #t) (k 0)) (d (n "protoc") (r "= 1.7.5") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0ak7v4ll3291nhsd9fnp7vdp896l1yqc5slb92zj1f6a4m50aj93")))

(define-public crate-protoc-rust-2.6.1 (c (n "protoc-rust") (v "2.6.1") (d (list (d (n "protobuf") (r "= 2.6.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xjf12c12k0m46qyyf6sd9vqc4sa9hz70ddl2dzdl493jn92f03r")))

(define-public crate-protoc-rust-2.6.2 (c (n "protoc-rust") (v "2.6.2") (d (list (d (n "protobuf") (r "= 2.6.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.6.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.6.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1nbvjdlkigvjksl86i13znl8m352s72bq7n12p6kb69jg61qbbxr")))

(define-public crate-protoc-rust-2.7.0 (c (n "protoc-rust") (v "2.7.0") (d (list (d (n "protobuf") (r "= 2.7.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.7.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "029w7mfz54d0p06cf2hi128xfi2p8nrd13lfbxlkmg2gkbzhdkll")))

(define-public crate-protoc-rust-2.8.0 (c (n "protoc-rust") (v "2.8.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "06zfhbhhmxd8q689c636yshgnzm2si09asrkyqn6lw2571aahj1d")))

(define-public crate-protoc-rust-2.8.1 (c (n "protoc-rust") (v "2.8.1") (d (list (d (n "protobuf") (r "= 2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "04s1q3zcyihwzsyz6dp5ii15jzmkgnjszpnhhfl5ifrjkh1rfk13")))

(define-public crate-protoc-rust-2.9.0 (c (n "protoc-rust") (v "2.9.0") (d (list (d (n "protobuf") (r "= 2.9.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.9.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0gyzvviprcd13c0s323hc3262d2by96dsc66qdvyb4658r2wmz26") (y #t)))

(define-public crate-protoc-rust-2.8.2 (c (n "protoc-rust") (v "2.8.2") (d (list (d (n "protobuf") (r "= 2.8.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.8.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.8.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1gr208pd0bl8p0jqyjbrccv4syxxz3hzd6b0b1d96mvwqzfm3a5y")))

(define-public crate-protoc-rust-2.10.0 (c (n "protoc-rust") (v "2.10.0") (d (list (d (n "protobuf") (r "= 2.10.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "152mm6ai0jlm5cvdzmdk5mks5iw43clygpss368hhhvvcxwb4vx9")))

(define-public crate-protoc-rust-2.10.1 (c (n "protoc-rust") (v "2.10.1") (d (list (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.1") (d #t) (k 0)) (d (n "protoc") (r "= 2.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1n174x0xibvighfjcqf160fzvpm8kcb5sbq6s7sw4x1l6smh7zih")))

(define-public crate-protoc-rust-2.10.2 (c (n "protoc-rust") (v "2.10.2") (d (list (d (n "protobuf") (r "= 2.10.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.2") (d #t) (k 0)) (d (n "protoc") (r "= 2.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "129q5846s62ay9332lxc3v4qng3dyl5k4fp2v3ljdy0mnxj04q0l")))

(define-public crate-protoc-rust-2.10.3 (c (n "protoc-rust") (v "2.10.3") (d (list (d (n "protobuf") (r "= 2.10.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.10.3") (d #t) (k 0)) (d (n "protoc") (r "= 2.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1sad2kjfsps4mwm6yrq9k1nyha1zrbdwp1bfqr3k0841j8ra83zn")))

(define-public crate-protoc-rust-2.11.0 (c (n "protoc-rust") (v "2.11.0") (d (list (d (n "protobuf") (r "= 2.11.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.11.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0v74rwzp24wg28ayqvphxn0h86hd3ifzflm27g24j57p58rsv1z5")))

(define-public crate-protoc-rust-2.12.0 (c (n "protoc-rust") (v "2.12.0") (d (list (d (n "protobuf") (r "= 2.12.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.12.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "02kq2h8qq7rjizr47cxl721mr44lpbfaqcz8b9bc294irbhg9h65")))

(define-public crate-protoc-rust-2.13.0 (c (n "protoc-rust") (v "2.13.0") (d (list (d (n "protobuf") (r "= 2.13.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.13.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.13.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0d2hzxg65kg3skdn1r07y2nknqwgy3zn9kf8lm4zrimyzn12ldgm")))

(define-public crate-protoc-rust-2.14.0 (c (n "protoc-rust") (v "2.14.0") (d (list (d (n "protobuf") (r "= 2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "= 2.14.0") (d #t) (k 0)) (d (n "protoc") (r "= 2.14.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0f6zkw0lcf75bdh876gfbrb1y712srcvy20ip4g5ndd1wcfzpwjq")))

(define-public crate-protoc-rust-2.15.0 (c (n "protoc-rust") (v "2.15.0") (d (list (d (n "protobuf") (r "=2.15.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.15.0") (d #t) (k 0)) (d (n "protoc") (r "=2.15.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1bq88785dvils53xl5nyksl7dlawfs91qhsfcnj81gc7421rp2zx")))

(define-public crate-protoc-rust-2.15.1 (c (n "protoc-rust") (v "2.15.1") (d (list (d (n "protobuf") (r "=2.15.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.15.1") (d #t) (k 0)) (d (n "protoc") (r "=2.15.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1r13fs3imfisk3l6ziwpbwlg69nvy24n36f226dm8nb3lhdjk1qx")))

(define-public crate-protoc-rust-2.16.0 (c (n "protoc-rust") (v "2.16.0") (d (list (d (n "protobuf") (r "=2.16.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.0") (d #t) (k 0)) (d (n "protoc") (r "=2.16.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0jmhr7j5jmi53lwgsw4zfgymfrs1kfqqcd535kbdhhn15f8nxf84")))

(define-public crate-protoc-rust-2.16.1 (c (n "protoc-rust") (v "2.16.1") (d (list (d (n "protobuf") (r "=2.16.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.1") (d #t) (k 0)) (d (n "protoc") (r "=2.16.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "08f60qqs66w81a4n9rfw4kam64gy3xqslj6jmcdh9l79fdf3cr3q")))

(define-public crate-protoc-rust-2.16.2 (c (n "protoc-rust") (v "2.16.2") (d (list (d (n "protobuf") (r "=2.16.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.16.2") (d #t) (k 0)) (d (n "protoc") (r "=2.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1py4wsr5smh30zv3qng35jh8f25z55iyyl8il8r94jvd3iwkf7iv")))

(define-public crate-protoc-rust-2.17.0 (c (n "protoc-rust") (v "2.17.0") (d (list (d (n "protobuf") (r "=2.17.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.17.0") (d #t) (k 0)) (d (n "protoc") (r "=2.17.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0igjl7qx51i8fqb44pgnfnbs1m200fg06bfzgkd2dh4zs1kxzm31")))

(define-public crate-protoc-rust-2.18.0 (c (n "protoc-rust") (v "2.18.0") (d (list (d (n "protobuf") (r "=2.18.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.0") (d #t) (k 0)) (d (n "protoc") (r "=2.18.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "18az2kqxza9w5by28ml6jivvrdd9ykx2s72v73qwxv7gycpmih91")))

(define-public crate-protoc-rust-2.18.1 (c (n "protoc-rust") (v "2.18.1") (d (list (d (n "protobuf") (r "=2.18.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.1") (d #t) (k 0)) (d (n "protoc") (r "=2.18.1") (d #t) (k 0)) (d (n "tempfile") (r ">=3.0.0, <4.0.0") (d #t) (k 0)))) (h "05r4lyl98h48n0xhpkcjqqx9n07c9ajpxzqk6j4x9l5hxr83j96s")))

(define-public crate-protoc-rust-2.19.0 (c (n "protoc-rust") (v "2.19.0") (d (list (d (n "protobuf") (r "=2.19.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.19.0") (d #t) (k 0)) (d (n "protoc") (r "=2.19.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1z06grp1gj0kdg091yp7a7splf0l1npijbg6639psr0xcpk74pgq")))

(define-public crate-protoc-rust-2.20.0 (c (n "protoc-rust") (v "2.20.0") (d (list (d (n "protobuf") (r "=2.20.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.20.0") (d #t) (k 0)) (d (n "protoc") (r "=2.20.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0xdc2lbzlhlw7bcxgssj5vs42x1wkd0n1d0ndplpdam3v17zpdnb")))

(define-public crate-protoc-rust-2.21.0 (c (n "protoc-rust") (v "2.21.0") (d (list (d (n "protobuf") (r "=2.21.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.21.0") (d #t) (k 0)) (d (n "protoc") (r "=2.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1izlhrmff4wzx4qywk3mn4cnf7821hjh0v2a9cvblwq6hdqww9ib")))

(define-public crate-protoc-rust-2.22.0 (c (n "protoc-rust") (v "2.22.0") (d (list (d (n "protobuf") (r "=2.22.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.22.0") (d #t) (k 0)) (d (n "protoc") (r "=2.22.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1kayzf3yxmh8p8pms91mvs3x0rh72v4lmjsjxz5ppdhi8n0pykqz")))

(define-public crate-protoc-rust-2.22.1 (c (n "protoc-rust") (v "2.22.1") (d (list (d (n "protobuf") (r "=2.22.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.22.1") (d #t) (k 0)) (d (n "protoc") (r "=2.22.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "19hm40r5lamhyi4nnf7his9lby0zwpnp16nv6sdl2gyaizx8l6g5")))

(define-public crate-protoc-rust-2.18.2 (c (n "protoc-rust") (v "2.18.2") (d (list (d (n "protobuf") (r "=2.18.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.18.2") (d #t) (k 0)) (d (n "protoc") (r "=2.18.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1933a4brz0qk49brlgjmd41wj7y70y7gmqfz23w7jj48qmpn6qql")))

(define-public crate-protoc-rust-2.23.0 (c (n "protoc-rust") (v "2.23.0") (d (list (d (n "protobuf") (r "=2.23.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.23.0") (d #t) (k 0)) (d (n "protoc") (r "=2.23.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0v1zlj6x6929bgqj0f8a3bp7bglrggg7a2v6xfwhmf6qicl8qnmk")))

(define-public crate-protoc-rust-2.24.0 (c (n "protoc-rust") (v "2.24.0") (d (list (d (n "protobuf") (r "=2.24.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.0") (d #t) (k 0)) (d (n "protoc") (r "=2.24.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0p8imvvvfj9w9bpv3qcfs43s2qxmvahz38ym6gjla9lj9xpn0izh")))

(define-public crate-protoc-rust-2.24.1 (c (n "protoc-rust") (v "2.24.1") (d (list (d (n "protobuf") (r "=2.24.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.1") (d #t) (k 0)) (d (n "protoc") (r "=2.24.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "05qkxf0id55ny4rarc36pyl79x83zhzw1znz5zja4540iw1w3ckv")))

(define-public crate-protoc-rust-2.24.2 (c (n "protoc-rust") (v "2.24.2") (d (list (d (n "protobuf") (r "=2.24.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.24.2") (d #t) (k 0)) (d (n "protoc") (r "=2.24.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0fr1x5lkcwp2gfkf12cxna2gwr3ghjr5n6lhdsn7f00m0qsp2ap8")))

(define-public crate-protoc-rust-2.25.0 (c (n "protoc-rust") (v "2.25.0") (d (list (d (n "protobuf") (r "=2.25.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.0") (d #t) (k 0)) (d (n "protoc") (r "=2.25.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1vq7fdjkpl7vqzl0r4kkfzxgd1iy0p9a0cyhdpm6zsqzryf6vnrd")))

(define-public crate-protoc-rust-2.25.1 (c (n "protoc-rust") (v "2.25.1") (d (list (d (n "protobuf") (r "=2.25.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.1") (d #t) (k 0)) (d (n "protoc") (r "=2.25.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0lh6sq3afx8pk12qz18a06c9z9yky322w93if54qbqa3pi29za6y")))

(define-public crate-protoc-rust-2.25.2 (c (n "protoc-rust") (v "2.25.2") (d (list (d (n "protobuf") (r "=2.25.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.25.2") (d #t) (k 0)) (d (n "protoc") (r "=2.25.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "084dxz48qb14199w8z8nc8k6dqrnmrdbbfnc9w10jgjf83473bbb")))

(define-public crate-protoc-rust-3.0.0-alpha.1 (c (n "protoc-rust") (v "3.0.0-alpha.1") (d (list (d (n "protobuf") (r "=3.0.0-alpha.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.0.0-alpha.1") (d #t) (k 0)) (d (n "protoc") (r "=3.0.0-alpha.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1c94ci9r3as2kyzadk0a92mwakdb9rywxg11z771wsi26mwadk7w")))

(define-public crate-protoc-rust-3.0.0-alpha.2 (c (n "protoc-rust") (v "3.0.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "protobuf") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protobuf-parse") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "protoc") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14irqwr5jl321919bs6zri2m3pnsp5imcyl47d38spr3hcrh6ffz")))

(define-public crate-protoc-rust-2.26.0 (c (n "protoc-rust") (v "2.26.0") (d (list (d (n "protobuf") (r "=2.26.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.26.0") (d #t) (k 0)) (d (n "protoc") (r "=2.26.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "03nsm3246gdjf1vna15iz4lv2hrzrbinasdg6j0jdw6bjj1bb37n")))

(define-public crate-protoc-rust-2.26.1 (c (n "protoc-rust") (v "2.26.1") (d (list (d (n "protobuf") (r "=2.26.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.26.1") (d #t) (k 0)) (d (n "protoc") (r "=2.26.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1z7afiijx1zn98w48gr68bb0b521ji7czbaxl2l9gy8zzhzr0nc1")))

(define-public crate-protoc-rust-2.27.0 (c (n "protoc-rust") (v "2.27.0") (d (list (d (n "protobuf") (r "=2.27.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.27.0") (d #t) (k 0)) (d (n "protoc") (r "=2.27.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1hsc8vrdjy580y1shqixfqknzc82wyjfziwwhz2ibwwa396jgqdd")))

(define-public crate-protoc-rust-2.27.1 (c (n "protoc-rust") (v "2.27.1") (d (list (d (n "protobuf") (r "=2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.27.1") (d #t) (k 0)) (d (n "protoc") (r "=2.27.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "156cqmvfmgvcpw9i54z351nhla4jgz9a6h79jq9y5ry8458k37hs")))

(define-public crate-protoc-rust-2.28.0 (c (n "protoc-rust") (v "2.28.0") (d (list (d (n "protobuf") (r "=2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=2.28.0") (d #t) (k 0)) (d (n "protoc") (r "=2.28.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0f3q4rvyq3xmk73n3r6g4h8ac04hqfl78hnw1gr8bi0ppf1a3y12")))

