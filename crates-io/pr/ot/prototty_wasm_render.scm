(define-module (crates-io pr ot prototty_wasm_render) #:use-module (crates-io))

(define-public crate-prototty_wasm_render-0.22.0 (c (n "prototty_wasm_render") (v "0.22.0") (d (list (d (n "prototty_render") (r "^0.22") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0qyvm97p7dfds14ln3b8g3lxp2ca7rh3fvhjm7r1plxh00pvfsrr") (f (quote (("serialize" "prototty_render/serialize"))))))

(define-public crate-prototty_wasm_render-0.23.0 (c (n "prototty_wasm_render") (v "0.23.0") (d (list (d (n "prototty_render") (r "^0.23") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0whnkm3pq3any0qsk4iqsn4n54miy95i47gmrbb0f37fmakpg7k1") (f (quote (("serialize" "prototty_render/serialize"))))))

(define-public crate-prototty_wasm_render-0.24.0 (c (n "prototty_wasm_render") (v "0.24.0") (d (list (d (n "prototty_render") (r "^0.24") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0200a7lm1408vfdnfgqw83mlfwvkpr1ifpxpza5f0q8d7g07iq1h") (f (quote (("serialize" "prototty_render/serialize"))))))

(define-public crate-prototty_wasm_render-0.25.0 (c (n "prototty_wasm_render") (v "0.25.0") (d (list (d (n "prototty_render") (r "^0.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1f8appy4l46iacrljna8wgx9nmlrlvn98mini6six01kw8cjh79g") (f (quote (("serialize" "prototty_render/serialize"))))))

(define-public crate-prototty_wasm_render-0.26.0 (c (n "prototty_wasm_render") (v "0.26.0") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0f18bap027hlqpv0nja2299j6plvbznhd5l447kd7k1sgjk5hxsl") (f (quote (("serialize" "prototty_render/serialize"))))))

(define-public crate-prototty_wasm_render-0.27.0 (c (n "prototty_wasm_render") (v "0.27.0") (d (list (d (n "prototty_render") (r "^0.27") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1pgqnlndvxqrzra45k8f88v76clpvvxibnxlg7gynd9fabjimdv1") (f (quote (("serialize" "prototty_render/serialize"))))))

