(define-module (crates-io pr ot protocrate) #:use-module (crates-io))

(define-public crate-protocrate-0.1.0 (c (n "protocrate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tonic-build") (r "^0.4.0") (f (quote ("rustfmt" "prost" "transport"))) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "17qf0wii1mim9z3sjv8sv64l53sjwch16zaqvv0n440szfxipqrs")))

