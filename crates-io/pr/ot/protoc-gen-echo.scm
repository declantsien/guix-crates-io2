(define-module (crates-io pr ot protoc-gen-echo) #:use-module (crates-io))

(define-public crate-protoc-gen-echo-0.1.0 (c (n "protoc-gen-echo") (v "0.1.0") (h "14lrny0rcfh4731r5gfmbikqv2ly2ki41xj1k1p9hm0g8hw9z63l")))

(define-public crate-protoc-gen-echo-0.2.0 (c (n "protoc-gen-echo") (v "0.2.0") (h "096622fggm9gkp04wy9n7hpqinc0lyywg339xq5fjqpm19ax7z6k")))

