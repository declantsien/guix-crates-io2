(define-module (crates-io pr ot protobuf_codec) #:use-module (crates-io))

(define-public crate-protobuf_codec-0.1.0 (c (n "protobuf_codec") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0z156f2h8vhraj42lsmrj4njqqb1jh1r1vdyh7d0qzfzbmwhwzsx")))

(define-public crate-protobuf_codec-0.1.1 (c (n "protobuf_codec") (v "0.1.1") (d (list (d (n "bytecodec") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1j84hlr32326jqz3xqfs1n3hfsrgj84faqiznim4qhlml4b9pysj")))

(define-public crate-protobuf_codec-0.1.2 (c (n "protobuf_codec") (v "0.1.2") (d (list (d (n "bytecodec") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "184imgsb943mxcvg04fcqbdmdphs299xwdw9k0gwyzg2z5glycx2")))

(define-public crate-protobuf_codec-0.2.0 (c (n "protobuf_codec") (v "0.2.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1rn17wmmy6jjz3yv8j4zljk592bhgbam3sid4l125fdssykkhjhy")))

(define-public crate-protobuf_codec-0.2.1 (c (n "protobuf_codec") (v "0.2.1") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1l8hkf6ayrm2504b8zc7casnji7qipwv675l9lr6vdvhdx25hvin")))

(define-public crate-protobuf_codec-0.2.2 (c (n "protobuf_codec") (v "0.2.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1a63s7kcqirij4h609skz3cns6rcf2gyskhj07c1038zp1krgwqb")))

(define-public crate-protobuf_codec-0.2.3 (c (n "protobuf_codec") (v "0.2.3") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0f5c57ffrqb1swd2y7n9sdcq9ynhjj8xdijiwwijw3znvf80d70j")))

(define-public crate-protobuf_codec-0.2.4 (c (n "protobuf_codec") (v "0.2.4") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "04ah9iz5lkq0hpdk279sxxqshcafyn6dw2p8v5qym3b3698r3c9j")))

(define-public crate-protobuf_codec-0.2.5 (c (n "protobuf_codec") (v "0.2.5") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1xcwxrh8n7s0ag17d8nalp4lmbb6c4zh7vwab33qx3limffcjm2g")))

(define-public crate-protobuf_codec-0.2.6 (c (n "protobuf_codec") (v "0.2.6") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "09bflaldc2wz9vc078axd5gb2gnasg9ikz8hbfrj9b0xcfk1ld74")))

(define-public crate-protobuf_codec-0.2.7 (c (n "protobuf_codec") (v "0.2.7") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "19y0ym4ly3nwjm9pxjks611jxbk71nf06basx4cf7gdvwnz9l1lk")))

(define-public crate-protobuf_codec-0.2.8 (c (n "protobuf_codec") (v "0.2.8") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "01q7vwr9wm4axy6v2p5mqkj5yr7dxcb3rzgig2zxcdj7m5v96hcz")))

