(define-module (crates-io pr ot proton-c) #:use-module (crates-io))

(define-public crate-proton-c-0.1.0 (c (n "proton-c") (v "0.1.0") (d (list (d (n "cortex-m-rtfm") (r "^0.4.3") (f (quote ("timer-queue"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 2)) (d (n "stm32f3xx-hal") (r "^0.2.0") (f (quote ("rt" "stm32f303"))) (d #t) (k 0)) (d (n "stm32f3xx-hal") (r "^0.2.0") (f (quote ("rt" "stm32f303" "unproven"))) (d #t) (k 2)))) (h "0i6jbkvp6b2hj02s77m1v9njzmr8zlfhjlnn9i0lg7jl1b98s369") (f (quote (("rt") ("default" "rt"))))))

