(define-module (crates-io pr ot prototty_wasm_storage) #:use-module (crates-io))

(define-public crate-prototty_wasm_storage-0.22.0 (c (n "prototty_wasm_storage") (v "0.22.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1ifmvmr64d7cwm0wxljy72wyzxbjbwmd49jlmyxlbrs3ac1cyca0")))

(define-public crate-prototty_wasm_storage-0.22.1 (c (n "prototty_wasm_storage") (v "0.22.1") (d (list (d (n "prototty_monolithic_storage") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1050wjx1rsgdv39yd6gnsmiaw0fx89j1zg28wnkm4i511gbvlsly")))

(define-public crate-prototty_wasm_storage-0.23.0 (c (n "prototty_wasm_storage") (v "0.23.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "10ylc86zb8iwsw20wnlw4r8s8vx27s403s9ya8gi8888816p0arb")))

(define-public crate-prototty_wasm_storage-0.24.0 (c (n "prototty_wasm_storage") (v "0.24.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1hjdqljd1cnd6wdflaf82j6sshr6z77ym3xmw914kkbf47i173pd")))

(define-public crate-prototty_wasm_storage-0.25.0 (c (n "prototty_wasm_storage") (v "0.25.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "17i9rb09v2pdqr93hlmzpy4yn20k8bsxqn57zq6qmy8rji6irybc")))

(define-public crate-prototty_wasm_storage-0.26.0 (c (n "prototty_wasm_storage") (v "0.26.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "134pay2ss39xrq9s5c37d0xigxy1cs2y2xixlxyisg41dpx4sk02")))

(define-public crate-prototty_wasm_storage-0.27.0 (c (n "prototty_wasm_storage") (v "0.27.0") (d (list (d (n "prototty_monolithic_storage") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1pdxx76qrby7y62bv3nkgmp3g2k81q013iwyr1j9znjxn8wcdjs1")))

