(define-module (crates-io pr ot protomask-tun) #:use-module (crates-io))

(define-public crate-protomask-tun-0.1.0 (c (n "protomask-tun") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rtnetlink") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.3") (d #t) (k 0)))) (h "03pf4why0vasyab2rg7fvaw3n34lc68g95kilvd44821hmamd8la")))

