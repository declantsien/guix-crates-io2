(define-module (crates-io pr ot protoc-rust-grpc) #:use-module (crates-io))

(define-public crate-protoc-rust-grpc-0.1.11 (c (n "protoc-rust-grpc") (v "0.1.11") (d (list (d (n "grpc-compiler") (r "^0.1.10") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust") (r "1.*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "09s9xswm3jq9nfc6pfnck87gq3mhrmqw2wnh6ygayljbjdaa4znf")))

(define-public crate-protoc-rust-grpc-0.2.0 (c (n "protoc-rust-grpc") (v "0.2.0") (d (list (d (n "grpc-compiler") (r "^0.1.10") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust") (r "1.*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0chh04al0xkhld44b16zhlc7l6al4h20s0jmrp7f3kl049g0vj1b")))

(define-public crate-protoc-rust-grpc-0.2.1 (c (n "protoc-rust-grpc") (v "0.2.1") (d (list (d (n "grpc-compiler") (r "^0.2.1") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust") (r "1.*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1ihq8iw0ka77snlbzrb8qbvjyrh2sz3y6ivd3pj4y9szd2k8zg38")))

(define-public crate-protoc-rust-grpc-0.3.0 (c (n "protoc-rust-grpc") (v "0.3.0") (d (list (d (n "grpc-compiler") (r "= 0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^1.5") (d #t) (k 0)) (d (n "protoc") (r "^1.5") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1zj3p8cjnsd3qcw64lpcgdihx4g2jlm3dmnqyiw04az79n0gp7qg")))

(define-public crate-protoc-rust-grpc-0.3.1 (c (n "protoc-rust-grpc") (v "0.3.1") (d (list (d (n "grpc-compiler") (r "= 0.3.1") (d #t) (k 0)) (d (n "protobuf") (r "~1.5") (d #t) (k 0)) (d (n "protoc") (r "~1.5") (d #t) (k 0)) (d (n "protoc-rust") (r "~1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0vq5njzhfyiz5vlsymjn656xhqpdxvv18lb15qbk1lzxhll8c1jv")))

(define-public crate-protoc-rust-grpc-0.4.0 (c (n "protoc-rust-grpc") (v "0.4.0") (d (list (d (n "grpc-compiler") (r "= 0.4.0") (d #t) (k 0)) (d (n "protobuf") (r "~1.6") (d #t) (k 0)) (d (n "protoc") (r "~1.6") (d #t) (k 0)) (d (n "protoc-rust") (r "~1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1zfdn46dhiwkviclvkmcs75vlaj47h8hc5l5dmvdzgn8kkq70z31")))

(define-public crate-protoc-rust-grpc-0.5.0 (c (n "protoc-rust-grpc") (v "0.5.0") (d (list (d (n "grpc-compiler") (r "= 0.5.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "19i5gqry0x23il7qvkyz3qncb2k3rcqk8axdv30mg7bylvvj4zjn")))

(define-public crate-protoc-rust-grpc-0.6.0 (c (n "protoc-rust-grpc") (v "0.6.0") (d (list (d (n "grpc-compiler") (r "= 0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "054mi250yp8wiawy1l34y2lccjgqlhi8wh4i74y2vlljrrbpcfmf")))

(define-public crate-protoc-rust-grpc-0.6.1 (c (n "protoc-rust-grpc") (v "0.6.1") (d (list (d (n "grpc-compiler") (r "= 0.6.1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1zwnkzwkiswdbzpwb1gjzns64ihz1cfm520jb01a7zd7wc8vdc49")))

(define-public crate-protoc-rust-grpc-0.6.2 (c (n "protoc-rust-grpc") (v "0.6.2") (d (list (d (n "grpc-compiler") (r "= 0.6.2") (d #t) (k 0)) (d (n "protobuf") (r ">= 2.8.0, < 2.10.0") (d #t) (k 0)) (d (n "protoc") (r ">= 2.8.0, < 2.10.0") (d #t) (k 0)) (d (n "protoc-rust") (r ">= 2.8.0, < 2.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1ml09kkygdk93x9s2pc9xr7qxair76y2i8p5w29pc19lk0vrx5bb")))

(define-public crate-protoc-rust-grpc-0.7.0 (c (n "protoc-rust-grpc") (v "0.7.0") (d (list (d (n "grpc-compiler") (r "= 0.7.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0lnkdpspw5l5q8mj3n8pmwjjzablrvm6khwdx7abqjky2krshbh1")))

(define-public crate-protoc-rust-grpc-0.7.1 (c (n "protoc-rust-grpc") (v "0.7.1") (d (list (d (n "grpc-compiler") (r "= 0.7.1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "161sbicayfjdksdbz6ggnd7nyg62hwb3na7q34mk6rjf3lrkjz4h")))

(define-public crate-protoc-rust-grpc-0.8.0 (c (n "protoc-rust-grpc") (v "0.8.0") (d (list (d (n "grpc-compiler") (r "= 0.8.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1ggynn9n8cjxn627bnb2bjk2s27ak9frycjd4qcwmbd080jg5nxv")))

(define-public crate-protoc-rust-grpc-0.8.1 (c (n "protoc-rust-grpc") (v "0.8.1") (d (list (d (n "grpc-compiler") (r "=0.8.1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1cqnykdw8p597847isrf928rarc3y2410cf5x0vp01jvyy0ml7lc")))

(define-public crate-protoc-rust-grpc-0.8.2 (c (n "protoc-rust-grpc") (v "0.8.2") (d (list (d (n "grpc-compiler") (r "=0.8.2") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1zv3z6igm154ayhb737c4jdmb9d7vsxyibh8l85d5jckzpys6z57")))

(define-public crate-protoc-rust-grpc-0.8.3 (c (n "protoc-rust-grpc") (v "0.8.3") (d (list (d (n "grpc-compiler") (r "=0.8.3") (d #t) (k 0)) (d (n "protobuf") (r "~2.18.2") (d #t) (k 0)) (d (n "protoc") (r "~2.18.2") (d #t) (k 0)) (d (n "protoc-rust") (r "~2.18.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0v6vzdx5mrcpz6aw259427vgyvc0d471xklvd92b10hdxjcvzqj1")))

