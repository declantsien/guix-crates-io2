(define-module (crates-io pr ot protein_translation) #:use-module (crates-io))

(define-public crate-protein_translation-0.1.0 (c (n "protein_translation") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rxsn1wvaf12xjcxji5drhy2kqrwcvah6r640zqpg761rln5sjq6")))

(define-public crate-protein_translation-0.1.1 (c (n "protein_translation") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0b972s54mypii065pcm9na2a2n3228nqbx54507wn4apclpvngmm")))

(define-public crate-protein_translation-0.1.2 (c (n "protein_translation") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0gsx9ay7crb90fwgafjbqy5c68vm9c9557fgyq4x5dla2gs49mnd")))

