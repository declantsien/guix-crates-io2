(define-module (crates-io pr ot protocol_v3) #:use-module (crates-io))

(define-public crate-protocol_v3-0.1.0 (c (n "protocol_v3") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a59mcvms55r5zlny5pn93bqybkwclslcf7x2rc888f5jdyj575h")))

(define-public crate-protocol_v3-0.1.1 (c (n "protocol_v3") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d96svkjbnyg72pnq3bh8rn5ivjjr2pj4ijz49pa7z534k74h1ah")))

(define-public crate-protocol_v3-0.1.2 (c (n "protocol_v3") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y65c9z8j0k6jhy056aix12mvmsp4gwba3m4l3lcqsvvajx16yb1")))

(define-public crate-protocol_v3-0.1.3 (c (n "protocol_v3") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01wfbzb5k0gm5glrvfsfsyg7bw9w22mcckga87mpi4cg0b1yjsw6")))

(define-public crate-protocol_v3-0.1.4 (c (n "protocol_v3") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h9lgl40vsm8002a7nd45mskj5vkzsw4wxcvrf1llajy8prsjnsy")))

(define-public crate-protocol_v3-0.1.5 (c (n "protocol_v3") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "protocol_v3_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13brwfxv7jcn6nlyvqgzhjlvpi9wh1abihrf9z7ay4s2h3pa97n6")))

