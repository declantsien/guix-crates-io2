(define-module (crates-io pr ot proton_client) #:use-module (crates-io))

(define-public crate-proton_client-0.1.0 (c (n "proton_client") (v "0.1.0") (d (list (d (n "clickhouse") (r "^0.11") (f (quote ("watch"))) (d #t) (k 0)) (d (n "clickhouse") (r "^0.11") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "clickhouse-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0r2rrgm34j5dgmx7vyv1llmpza69mrqq98c0cb31bz8kmw1l41fn") (r "1.65")))

(define-public crate-proton_client-0.1.1 (c (n "proton_client") (v "0.1.1") (d (list (d (n "clickhouse") (r "^0.11") (f (quote ("watch"))) (d #t) (k 0)) (d (n "clickhouse") (r "^0.11") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "clickhouse-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1w96kkar2nidbmkp3q4cvfk1q55l8qidqkv6v96hlnwpjgsh8l0m") (r "1.65")))

