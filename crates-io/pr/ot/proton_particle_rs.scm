(define-module (crates-io pr ot proton_particle_rs) #:use-module (crates-io))

(define-public crate-proton_particle_rs-0.1.0 (c (n "proton_particle_rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02dyaaf53sc42ns0wffw11h6irc3yb5nsj528ppah6ha55m10mwx") (y #t)))

