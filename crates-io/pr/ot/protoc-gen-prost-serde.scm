(define-module (crates-io pr ot protoc-gen-prost-serde) #:use-module (crates-io))

(define-public crate-protoc-gen-prost-serde-0.0.1 (c (n "protoc-gen-prost-serde") (v "0.0.1") (h "1rfqh8ywi3bba0c5dwdqp0c6fbfl1i9y6bmjyq4l42a6a63m7la2")))

(define-public crate-protoc-gen-prost-serde-0.1.0 (c (n "protoc-gen-prost-serde") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pbjson-build") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "02rni9wvaz8y3iv2ny9m31hjggrlsvcgvxccmpyvcm5clc8syf71")))

(define-public crate-protoc-gen-prost-serde-0.1.1 (c (n "protoc-gen-prost-serde") (v "0.1.1") (d (list (d (n "pbjson-build") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (k 0)) (d (n "prost-types") (r "^0.10.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "0cfaf9sx0dbs9x3xy5s2mb6f5f5l57zjpqv0dra720qzswgj639p")))

(define-public crate-protoc-gen-prost-serde-0.2.0 (c (n "protoc-gen-prost-serde") (v "0.2.0") (d (list (d (n "pbjson-build") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.0") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "10yiac6i11n5cawmyg6vq9nsqxj60148pyrl5l1x9s9k1088f484")))

(define-public crate-protoc-gen-prost-serde-0.2.1 (c (n "protoc-gen-prost-serde") (v "0.2.1") (d (list (d (n "pbjson-build") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r ">=0.11.0, <0.11.2") (k 0)) (d (n "prost-types") (r "^0.11.0") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1azn705apjsdg51zbk3k01glwpwnz3plhy91wbbxvdbqyhsf8c62")))

(define-public crate-protoc-gen-prost-serde-0.2.2 (c (n "protoc-gen-prost-serde") (v "0.2.2") (d (list (d (n "pbjson-build") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.8") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.8") (k 0)) (d (n "prost-types") (r "^0.11.8") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1znihqb791bfylc1zy16rfpywss7ynmmpwsvfvhz6i1ha47rb76w")))

(define-public crate-protoc-gen-prost-serde-0.2.3 (c (n "protoc-gen-prost-serde") (v "0.2.3") (d (list (d (n "pbjson-build") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.8") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.8") (k 0)) (d (n "prost-types") (r "^0.11.8") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1jcd7ip2qrim2bbl1gz71jx32digjd17jb14cirn87n1bvbfxsb0")))

(define-public crate-protoc-gen-prost-serde-0.3.0 (c (n "protoc-gen-prost-serde") (v "0.3.0") (d (list (d (n "pbjson-build") (r "^0.6.2") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.3") (k 0)) (d (n "prost-types") (r "^0.12.3") (k 0)) (d (n "protoc-gen-prost") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "17r1w4snx05c035p94bk82b6jh54aqw82n303jl3984rn3s2cxas")))

