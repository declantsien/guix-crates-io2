(define-module (crates-io pr ot proto-vulcan-macros) #:use-module (crates-io))

(define-public crate-proto-vulcan-macros-0.1.1 (c (n "proto-vulcan-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14il1cpgg48pixy7ln992ikkm175mkqzbmqqiw9q8hrvagbdaig6")))

(define-public crate-proto-vulcan-macros-0.1.2 (c (n "proto-vulcan-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11dw3yzzhvip85h3i9m083024fsj0dx4yj08lqi813kpj276yx9d")))

(define-public crate-proto-vulcan-macros-0.1.3 (c (n "proto-vulcan-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jgmi2vsd514a365dpy9mdbc6nafxwcfz1r528jksx0lyr8a706f")))

(define-public crate-proto-vulcan-macros-0.1.4 (c (n "proto-vulcan-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vbm1jxf413qd4abp2w3jv4slya22vg481p5ifx43n649fx4sgf7")))

(define-public crate-proto-vulcan-macros-0.1.5 (c (n "proto-vulcan-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y9rsr0gbq32qmz0idl1mc954f0syxpgilzb6iblnz0ym5hv0zsq")))

(define-public crate-proto-vulcan-macros-0.1.6 (c (n "proto-vulcan-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "026qdjl7nxwr235y9ly0z2gf76wqcaa4k2z90s8wpj3rxwyr917g")))

