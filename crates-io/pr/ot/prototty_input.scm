(define-module (crates-io pr ot prototty_input) #:use-module (crates-io))

(define-public crate-prototty_input-0.22.0 (c (n "prototty_input") (v "0.22.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1qsbji5v7ajv70niazxi2v9j7bqm6n3r6gqv3fj8wqwg57f3f67m") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.22.1 (c (n "prototty_input") (v "0.22.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1qq4lhmnmmlb7sqg4zzsjky271p4qm3fyvfp670r9d2m4pywfh7p") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.23.0 (c (n "prototty_input") (v "0.23.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0bjsbp4dkdp9sw3jmdsv6iggixv74ahih6n0lbnf8698ijlvsskv") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.24.0 (c (n "prototty_input") (v "0.24.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "05h9wpx7880a2r2qnxxsppvayrmm95lip5ahwjir5zmngsy47w9q") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.25.0 (c (n "prototty_input") (v "0.25.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1hfs040cb1qwszna4ndlyvk6gvz176g8j24v4zjaar65krzhvgvz") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.26.0 (c (n "prototty_input") (v "0.26.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1l8ci4f2i8vyfaalysnqc82z151m5aa36g5mm9ff5a9hw5k8f7c1") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.27.0 (c (n "prototty_input") (v "0.27.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0af3mchjqdxym7wnf4map6qxlwj80iqnlxcniwyv4blzx8y3cvq6") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.28.0 (c (n "prototty_input") (v "0.28.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "183135xs74jd9372hv5awfhqn3hfbqmiika651pmc7wan3455fbv") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_input-0.29.0 (c (n "prototty_input") (v "0.29.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z8kdkpbs1lx7lwy743bahhcaa2aqsicvgjcdi27qcwn43c95jip") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

