(define-module (crates-io pr ot proto) #:use-module (crates-io))

(define-public crate-proto-0.1.0 (c (n "proto") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 2)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 2)))) (h "1pbrcllmdim6ymh1s8if22zw3zcb2wnxj8rdy0km2ylnkxwpdgpg")))

(define-public crate-proto-0.1.1 (c (n "proto") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 2)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 2)))) (h "1w6ibqadf2r8anmwbd3dg960pc1mlp6v9kkja32ybmqdr07ssvr1")))

(define-public crate-proto-0.1.2 (c (n "proto") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 2)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 2)))) (h "04dcm103cm36prh897bhl6466i6bigcngars1pc6vv6vkp4vbh5n")))

