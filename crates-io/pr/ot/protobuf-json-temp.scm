(define-module (crates-io pr ot protobuf-json-temp) #:use-module (crates-io))

(define-public crate-protobuf-json-temp-0.3.0 (c (n "protobuf-json-temp") (v "0.3.0") (d (list (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vbz52s5r7b5pylc08h8isw9jalhg95wn7v0ch3d6ss2ss6i2k2j")))

