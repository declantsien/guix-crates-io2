(define-module (crates-io pr ot protein-core) #:use-module (crates-io))

(define-public crate-protein-core-0.1.0 (c (n "protein-core") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03pacm9dsajrlz3pf5ypzdwnw0qlcd4qbm8h2zdq9w0209bbi4fk")))

(define-public crate-protein-core-0.1.1 (c (n "protein-core") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gj06hbgpngl6d06y5cglrzi3ghaiksikcbhscclxzw7phn2ln8w")))

(define-public crate-protein-core-0.1.2 (c (n "protein-core") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fr38lg8z5sqh1l2jjvbfnb9wrdkjxrdhq17bc7lkp55178zq46z")))

(define-public crate-protein-core-0.1.3 (c (n "protein-core") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w0nnl02yli5h80ndka34xhzgj90jdkjdz6v0cyxh6cwimz0j670")))

(define-public crate-protein-core-0.1.4 (c (n "protein-core") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13qk40li3p1pr4wqdhhsjv35i82j3p2fdphmrfcd558ilmaf1crh")))

(define-public crate-protein-core-0.1.5 (c (n "protein-core") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "060cmv0rlknvavrf4ja97hy4w9y667m6gh0vrhbzxdyy08hk5mi0")))

(define-public crate-protein-core-0.1.6 (c (n "protein-core") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "04gqf84516m4mndbab3z8cvwzr44v7dxhbfn81cq0kdvjv3jf0gr")))

(define-public crate-protein-core-0.1.7 (c (n "protein-core") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dihedral") (r "^0.0") (f (quote ("f32"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "192d1jhp5rshsn6njfqyly0pv2ghjvcdgmm75cpq17mgqia3c98x")))

