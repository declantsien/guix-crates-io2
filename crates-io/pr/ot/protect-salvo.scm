(define-module (crates-io pr ot protect-salvo) #:use-module (crates-io))

(define-public crate-protect-salvo-0.0.1-beta.1 (c (n "protect-salvo") (v "0.0.1-beta.1") (h "1fmzxhx4f0fnpr9v5cnxrkxnjdrj4nnbjwx0xpqmvy2pqfdw48az") (y #t)))

(define-public crate-protect-salvo-0.1.0 (c (n "protect-salvo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 2)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 2)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 2)) (d (n "protect-endpoints-core") (r "^0.1.1") (f (quote ("tower"))) (d #t) (k 0)) (d (n "protect-endpoints-proc-macro") (r "^0.3.2") (f (quote ("salvo"))) (o #t) (d #t) (k 0)) (d (n "salvo") (r "^0.67.2") (f (quote ("tower-compat"))) (k 0)) (d (n "salvo") (r "^0.67.2") (f (quote ("test"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (k 0)))) (h "0yy25d8p3ydapk6a66bys7k5lshydc6nddkbcv3hsaj22117973c") (f (quote (("macro-check" "protect-endpoints-proc-macro") ("default" "macro-check"))))))

