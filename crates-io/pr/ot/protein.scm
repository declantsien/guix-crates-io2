(define-module (crates-io pr ot protein) #:use-module (crates-io))

(define-public crate-protein-0.0.2 (c (n "protein") (v "0.0.2") (d (list (d (n "nom-pdb") (r "^0.0.1") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w7nsdm2ygfp92047kkw11vz4jlw14028r64liw8yi2a879a0b4h")))

(define-public crate-protein-0.0.5 (c (n "protein") (v "0.0.5") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "dihedral") (r "^0.0") (f (quote ("f32"))) (d #t) (k 0)) (d (n "nom-pdb") (r "^0.0.8") (d #t) (k 0)) (d (n "protein-core") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qs9af2g4rkdxq30l20cs9q8d999wbw70fl6yzgxvj9nn4v1biar")))

