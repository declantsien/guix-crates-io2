(define-module (crates-io pr ot protoc-fetcher) #:use-module (crates-io))

(define-public crate-protoc-fetcher-0.1.0 (c (n "protoc-fetcher") (v "0.1.0") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "reqwest") (r "0.11.*") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tonic-build") (r "0.8.*") (d #t) (k 2)) (d (n "zip-extract") (r "^0.1.1") (f (quote ("deflate"))) (d #t) (k 0)))) (h "1n5gns69aa3aqf90x1hr0sjm1n7y870q85b2x6v2qv5xp9hz53gv")))

(define-public crate-protoc-fetcher-0.1.1 (c (n "protoc-fetcher") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 2)) (d (n "zip-extract") (r "^0.1.3") (f (quote ("deflate"))) (d #t) (k 0)))) (h "1za4k63hq5rwc3zs4a8s8ygxqzqqlfk6sdm00gynz1ngqnfs3hwx")))

