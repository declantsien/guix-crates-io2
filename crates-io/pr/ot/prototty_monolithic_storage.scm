(define-module (crates-io pr ot prototty_monolithic_storage) #:use-module (crates-io))

(define-public crate-prototty_monolithic_storage-0.22.0 (c (n "prototty_monolithic_storage") (v "0.22.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ny0jqnlmcwyz19jd01v9hlh1gddkg7g3j25672ns1db46ydfvzn")))

(define-public crate-prototty_monolithic_storage-0.23.0 (c (n "prototty_monolithic_storage") (v "0.23.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0r40dlcfdx1gwm3v038sklxg93nbj6sc4jgx0312x40b9s0r8c9h")))

(define-public crate-prototty_monolithic_storage-0.23.1 (c (n "prototty_monolithic_storage") (v "0.23.1") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1vw2gs0km4njr5qhyb8w0sxfiq1ixp8zb6rzjc47x7qkivaj6zai")))

(define-public crate-prototty_monolithic_storage-0.24.0 (c (n "prototty_monolithic_storage") (v "0.24.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1y1csnd55i436vq2wwkxiwzral9yphyrfwk46fs1qivdq59l1n97")))

(define-public crate-prototty_monolithic_storage-0.25.0 (c (n "prototty_monolithic_storage") (v "0.25.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06b6jfdsna6viwc3rzqa3ppdxxqd8gam6da65p4fj72l2kc6bvpc")))

(define-public crate-prototty_monolithic_storage-0.26.0 (c (n "prototty_monolithic_storage") (v "0.26.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lpxcfk5lwrw9bmhcnjwnzibf0975wk3svc9syn0bxrm5mgl2d5s")))

(define-public crate-prototty_monolithic_storage-0.27.0 (c (n "prototty_monolithic_storage") (v "0.27.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0razalwi4kh209vpldnhpx1yivj46apmbqn61xmqniramym4bqza")))

