(define-module (crates-io pr ot protonsdk_variant) #:use-module (crates-io))

(define-public crate-protonsdk_variant-0.1.0 (c (n "protonsdk_variant") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "1s552n4yd3jhrj2r7klwgj335y6c40v918f4ps27fngzg9akhqnq")))

(define-public crate-protonsdk_variant-0.1.1 (c (n "protonsdk_variant") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "10sbfw3wddx067ygkpv1gh7jdvvmimkyhfn3yl6r96ry3sawr3hn")))

