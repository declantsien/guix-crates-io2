(define-module (crates-io pr ot prototty_wasm) #:use-module (crates-io))

(define-public crate-prototty_wasm-0.6.0 (c (n "prototty_wasm") (v "0.6.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.6.0") (d #t) (k 0)))) (h "1nagq2zfsqnpnz505dyhkh2zidxvy183b44gmysji1cn8ndb9l6v")))

(define-public crate-prototty_wasm-0.7.1 (c (n "prototty_wasm") (v "0.7.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.7.1") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.7.1") (d #t) (k 0)))) (h "0089wdrnqjcw58c7n29l4lwjr7lhbssv5zgm3ryj81hxp80ylfff")))

(define-public crate-prototty_wasm-0.8.0 (c (n "prototty_wasm") (v "0.8.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.8.0") (d #t) (k 0)))) (h "16b39jjvq77qr8wy454vyrcj02y3iynd1zi8lsjvi9q1mdbmb6r3")))

(define-public crate-prototty_wasm-0.8.1 (c (n "prototty_wasm") (v "0.8.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.1") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.8.1") (d #t) (k 0)))) (h "1x0dcsa17k3wannflx3gsr8iyv1k0zh8f97ph8pgpvpbkczl6a1k")))

(define-public crate-prototty_wasm-0.8.2 (c (n "prototty_wasm") (v "0.8.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.8.2") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.8.2") (d #t) (k 0)))) (h "0v3910mip6qp1rlnd191pyjmdnifqcwxiaak46032g2pypis5a33")))

(define-public crate-prototty_wasm-0.9.0 (c (n "prototty_wasm") (v "0.9.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.9.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.9.0") (d #t) (k 0)))) (h "19vnk9v1cm1mbyy12vk9446v782bd9wj32wvlan73a1lw9ybm3c7")))

(define-public crate-prototty_wasm-0.10.0 (c (n "prototty_wasm") (v "0.10.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.10.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.10.0") (d #t) (k 0)))) (h "1a881b4ksjgl8cmv90wxw56lcwljv4p80586cjxjk6xc3cb9zb5j")))

(define-public crate-prototty_wasm-0.11.0 (c (n "prototty_wasm") (v "0.11.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.11.0") (d #t) (k 0)))) (h "1sjc60yf7xi93ijl7j17hghrmk7nzlz21wi06bhpmcfg8gyvgkd9")))

(define-public crate-prototty_wasm-0.11.1 (c (n "prototty_wasm") (v "0.11.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.1") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.11.1") (d #t) (k 0)))) (h "0lv2ja77vfn3kr3qk7zmmg866gy3fxp6rhba4wfy87vv6aaxiavy")))

(define-public crate-prototty_wasm-0.11.2 (c (n "prototty_wasm") (v "0.11.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.2") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.11.2") (d #t) (k 0)))) (h "01sdp3v2gqh4a0hi1a2w3c2nlbiqv72wjrmfbrpbsjklqd8qg7md")))

(define-public crate-prototty_wasm-0.11.3 (c (n "prototty_wasm") (v "0.11.3") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.11.3") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.11.3") (d #t) (k 0)))) (h "14i8yy30byvm87nbq609n8pij7gr44hzvpnm49pmpkybbx2gnm82")))

(define-public crate-prototty_wasm-0.12.0 (c (n "prototty_wasm") (v "0.12.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.12.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.12.0") (d #t) (k 0)))) (h "02msi1nmm522qjjxz0vnblf2gj4dszpshp174cwjzdahsd2f7zrb")))

(define-public crate-prototty_wasm-0.13.0 (c (n "prototty_wasm") (v "0.13.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.13.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.13.0") (d #t) (k 0)))) (h "0kzapcf9lkh0wgmsdbkqnbqjv21zfzwacb6g03h5fzp8y33f1c5k")))

(define-public crate-prototty_wasm-0.14.0 (c (n "prototty_wasm") (v "0.14.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.14.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12cki2n2sqkzj8vnplrq281syaa4ry1gr0xfdhpsphg748rqqhwv")))

(define-public crate-prototty_wasm-0.15.0 (c (n "prototty_wasm") (v "0.15.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.15.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0m9mz1v3ah9ksjnzpzhw8vkn0pcwbhc37cxm2jx6hpjxivd3mpr9")))

(define-public crate-prototty_wasm-0.16.0 (c (n "prototty_wasm") (v "0.16.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.16.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0mfr9hjphcw1wy3h0xdxpqn33xjxk23c3yk9lyfjq8v4dcc41avr")))

(define-public crate-prototty_wasm-0.17.0 (c (n "prototty_wasm") (v "0.17.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0y3xqvygl3vwp2a4dp38cal82q5z9pqdic1dik5lvicd6hrzdnxx")))

(define-public crate-prototty_wasm-0.17.1 (c (n "prototty_wasm") (v "0.17.1") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1c1hncaralfp2nvra2x9nmmw7fhnzm28djr6rr6a4mxvbysgcml0")))

(define-public crate-prototty_wasm-0.17.2 (c (n "prototty_wasm") (v "0.17.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17.0") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hpc8p7gdmpqmmb4d8k24aah4mrijwblmwn6a6rycg7crdqy0rgf")))

(define-public crate-prototty_wasm-0.17.3 (c (n "prototty_wasm") (v "0.17.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.17") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0knfg7cvim0s3wmfkrjg36ssqbgfcal842769dv3l8gzvf8mkn43")))

(define-public crate-prototty_wasm-0.18.0 (c (n "prototty_wasm") (v "0.18.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.18") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0z9mifjvlzyxa3743jkablrw9kdjavahjdifllw0q125ibzjjlyj")))

(define-public crate-prototty_wasm-0.19.0 (c (n "prototty_wasm") (v "0.19.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1lv4jhq01qik1irahf9vdwib3d1r26xlzppskdbj75c6p4jg8ihz")))

(define-public crate-prototty_wasm-0.20.0 (c (n "prototty_wasm") (v "0.20.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "prototty") (r "^0.20") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1abpdmv2sfrxzm7wwvbkj15dysj1nys26axgmrrayjwvvfpggfia")))

(define-public crate-prototty_wasm-0.21.0 (c (n "prototty_wasm") (v "0.21.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "prototty") (r "^0.21") (d #t) (k 0)) (d (n "prototty_grid") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1wa7n25rp5dg2i2j5r1611jvcq18hk7ii4lqf4l13w3061nk3qqy")))

(define-public crate-prototty_wasm-0.22.0 (c (n "prototty_wasm") (v "0.22.0") (d (list (d (n "prototty_wasm_input") (r "^0.22") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.22") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.22") (o #t) (d #t) (k 0)))) (h "183fm5a6skjq88cgsf241gwq0s231pln8cba10q8n6rxijl9qn4g") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.22.1 (c (n "prototty_wasm") (v "0.22.1") (d (list (d (n "prototty_wasm_input") (r "^0.22") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.22") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0k4wsm605nc8m14cri7y91vbd8k93cb9xdx4rlfcb5lr92l42mqq") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.23.0 (c (n "prototty_wasm") (v "0.23.0") (d (list (d (n "prototty_wasm_input") (r "^0.23") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.23") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.23") (o #t) (d #t) (k 0)))) (h "071m72wx5sx0jhbp7ddpf5as1nljxl03fwp2hl4b4ah7nyv0wgkv") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.24.0 (c (n "prototty_wasm") (v "0.24.0") (d (list (d (n "prototty_wasm_input") (r "^0.24") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.24") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.24") (o #t) (d #t) (k 0)))) (h "0ww1pgks930cjwsvrrvia9pb21zzmr0h4wv5jg96m9bfyd2qb2hp") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.25.0 (c (n "prototty_wasm") (v "0.25.0") (d (list (d (n "prototty_wasm_input") (r "^0.25") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.25") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0dl35mjr8s438vh1z09qr3b88c77z94v5i2ch4rv48nydbsbzpz5") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.26.0 (c (n "prototty_wasm") (v "0.26.0") (d (list (d (n "prototty_wasm_input") (r "^0.26") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.26") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1w96qvnpsa8av52lp9ym8jwh5dai4glhfdpzw3ja53pjp3jqy3bm") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

(define-public crate-prototty_wasm-0.27.0 (c (n "prototty_wasm") (v "0.27.0") (d (list (d (n "prototty_wasm_input") (r "^0.27") (d #t) (k 0)) (d (n "prototty_wasm_render") (r "^0.27") (d #t) (k 0)) (d (n "prototty_wasm_storage") (r "^0.27") (o #t) (d #t) (k 0)))) (h "014llzcqh8adj3gv53iyppd8b2vj9lr3nbz10ggyw7ww1i2dq8dq") (f (quote (("storage" "prototty_wasm_storage") ("serialize" "prototty_wasm_render/serialize" "prototty_wasm_input/serialize"))))))

