(define-module (crates-io pr ot prototty_file_storage) #:use-module (crates-io))

(define-public crate-prototty_file_storage-0.14.0 (c (n "prototty_file_storage") (v "0.14.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "prototty") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lzwpwk20fwqjw3qz7pv3wlhxk71zsyqxx789dz1a2fdmb3c3075")))

(define-public crate-prototty_file_storage-0.15.0 (c (n "prototty_file_storage") (v "0.15.0") (d (list (d (n "prototty") (r "^0.15.0") (d #t) (k 0)))) (h "00sk3f89khfn51szrxnfq4zbi70w1y6falsrc9pfsbddxwvx341d")))

(define-public crate-prototty_file_storage-0.16.0 (c (n "prototty_file_storage") (v "0.16.0") (d (list (d (n "prototty") (r "^0.16.0") (d #t) (k 0)))) (h "050q08j46dgqn9w82x82z9il83qja00qcs48z7xdmbdbwpi8abx8")))

(define-public crate-prototty_file_storage-0.17.0 (c (n "prototty_file_storage") (v "0.17.0") (d (list (d (n "prototty") (r "^0.17.0") (d #t) (k 0)))) (h "19bziw723fgcl83sr4pcgc9maph00ilfb1g586g54wg90i1fy3yb")))

(define-public crate-prototty_file_storage-0.17.1 (c (n "prototty_file_storage") (v "0.17.1") (d (list (d (n "prototty") (r "^0.17") (d #t) (k 0)))) (h "17iplqp88ff5z0hvm9pk6ys3dkg2vzfc93j8hxi8fkrp31rk1ivk")))

(define-public crate-prototty_file_storage-0.18.0 (c (n "prototty_file_storage") (v "0.18.0") (d (list (d (n "prototty") (r "^0.18") (d #t) (k 0)))) (h "11bv3194wxhv1ixqw9rbaxgc4hdm1hs7nassvdw1lnd6f1yd2r8d")))

(define-public crate-prototty_file_storage-0.19.0 (c (n "prototty_file_storage") (v "0.19.0") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)))) (h "1bvy3lf8fajzvxzwc9qppnqrd0mvcp7ddcf2z9c5kj4s73mkhbi2")))

(define-public crate-prototty_file_storage-0.20.0 (c (n "prototty_file_storage") (v "0.20.0") (d (list (d (n "prototty") (r "^0.20") (d #t) (k 0)))) (h "0cq7n2f9hnzws9z1m974c20fms0m3rkj0kyf0v73kin2ib1ahs1a")))

(define-public crate-prototty_file_storage-0.21.0 (c (n "prototty_file_storage") (v "0.21.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "prototty") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06fyc78i3zdsqq77hdq99rqz4alsg02qaicsnzhsaw26klqiwbwx") (f (quote (("serialize" "prototty/serialize"))))))

(define-public crate-prototty_file_storage-0.22.0 (c (n "prototty_file_storage") (v "0.22.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "009zciz2bhcacaqvm4f8afkdb4ya0khhsnapx59yl6lfbcwdmybz")))

(define-public crate-prototty_file_storage-0.23.0 (c (n "prototty_file_storage") (v "0.23.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02gv6vkwpgj4hy4si9salg2y9mivaq92gi6y3nfslp542h732d4f")))

(define-public crate-prototty_file_storage-0.23.1 (c (n "prototty_file_storage") (v "0.23.1") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0whl1703waj3q3wvdf4fl6qpq7p83mz6rw9g8a85268kh7sig3w3")))

(define-public crate-prototty_file_storage-0.24.0 (c (n "prototty_file_storage") (v "0.24.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ln81bwkkr6qa4i1mpqgrm2chp1rlx720jdhg7wrpp5q0922n53s")))

(define-public crate-prototty_file_storage-0.25.0 (c (n "prototty_file_storage") (v "0.25.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0kw77gbdjf94kydswdk2rn49yxlh5mw2741f0qf1284adf34305b")))

(define-public crate-prototty_file_storage-0.26.0 (c (n "prototty_file_storage") (v "0.26.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0h0cf19pm4l7wqdnnp8rsgmlw4755jajsmcy0rlqg44wrxf13zpq")))

(define-public crate-prototty_file_storage-0.27.0 (c (n "prototty_file_storage") (v "0.27.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "prototty_storage") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1wn47ndp4jnyk8s0yc962wz5n81wwnkr9r8c16rs6vi5ib6f00vb")))

(define-public crate-prototty_file_storage-0.28.0 (c (n "prototty_file_storage") (v "0.28.0") (d (list (d (n "prototty_storage") (r "^0.28") (d #t) (k 0)))) (h "196rfhwaq2v5ns4m5say6md6hqfcqfan85ksq754ym4y7sh56wnm") (f (quote (("yaml" "prototty_storage/yaml") ("toml" "prototty_storage/toml") ("json" "prototty_storage/json") ("bincode" "prototty_storage/bincode"))))))

(define-public crate-prototty_file_storage-0.29.0 (c (n "prototty_file_storage") (v "0.29.0") (d (list (d (n "prototty_storage") (r "^0.29") (d #t) (k 0)))) (h "0jqb5xhmgw0lrdygi3jws604vfwg178dw1kvjjp5709m71il1qhg") (f (quote (("yaml" "prototty_storage/yaml") ("toml" "prototty_storage/toml") ("json" "prototty_storage/json") ("bincode" "prototty_storage/bincode"))))))

