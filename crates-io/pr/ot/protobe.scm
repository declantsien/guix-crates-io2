(define-module (crates-io pr ot protobe) #:use-module (crates-io))

(define-public crate-protobe-0.1.0 (c (n "protobe") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0qrynhql0dqdlrzkzjch85gbl9svg3y1x7jmkdl574p9svvk38ai") (f (quote (("verbose" "log") ("default" "verbose"))))))

(define-public crate-protobe-0.1.1 (c (n "protobe") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0ncyq6snsafb3q0lni5zxclcx16jbg95kmw46fnqn5rp9qfdxxj8") (f (quote (("verbose" "log") ("default" "verbose"))))))

(define-public crate-protobe-0.1.2 (c (n "protobe") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1fndc3sy77y6ck9dsgkvw55bbmhj47jnfwavrcdq9h0kgcz0zhw9") (f (quote (("verbose" "log") ("default" "verbose"))))))

(define-public crate-protobe-0.1.3 (c (n "protobe") (v "0.1.3") (d (list (d (n "protoc-rust") (r "^2.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "06h6c8l9wjqmfhmdyik309ysnwzlhfbv6qvbvq1q6rs6ws0dpid9")))

