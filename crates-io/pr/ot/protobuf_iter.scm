(define-module (crates-io pr ot protobuf_iter) #:use-module (crates-io))

(define-public crate-protobuf_iter-0.1.0 (c (n "protobuf_iter") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "070slpdr1p4av25i1vcw3n2sgg5zm7jh0gdl4rf6cim1vg685hw7")))

(define-public crate-protobuf_iter-0.1.1 (c (n "protobuf_iter") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1q0i2ll3cjdzgg700naxw11a4igifxyzqhhvrvjc76h3nq4mz7wy")))

(define-public crate-protobuf_iter-0.1.2 (c (n "protobuf_iter") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0x00ic21ivz64kbz4b1cnpfkfp5nimvlpyl1jc00bnvka7p5dwzb")))

