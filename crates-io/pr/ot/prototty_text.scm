(define-module (crates-io pr ot prototty_text) #:use-module (crates-io))

(define-public crate-prototty_text-0.26.0 (c (n "prototty_text") (v "0.26.0") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13l8zlz2y6jwng2ci0sb1kx14a61x24pcxlqsv7rhryh42avrkbl") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_text-0.26.1 (c (n "prototty_text") (v "0.26.1") (d (list (d (n "prototty_render") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "07nja07xivmgnibnw5dsj462vgpm2k0jr3j4sihr13jw1gqzlmnd") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_text-0.27.0 (c (n "prototty_text") (v "0.27.0") (d (list (d (n "prototty_render") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qhsn48x0182nj3rqrg7ra0s88w32z06rhlnsasw0mlzqqr7hhkw") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_text-0.28.0 (c (n "prototty_text") (v "0.28.0") (d (list (d (n "prototty_render") (r "^0.28") (d #t) (k 0)) (d (n "prototty_test_grid") (r "^0.28") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "06c6av325g9s4zxbmi1nb2gmpgwrdgqsii4h8hf644diwh5izj2k") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

(define-public crate-prototty_text-0.29.0 (c (n "prototty_text") (v "0.29.0") (d (list (d (n "prototty_render") (r "^0.29") (d #t) (k 0)) (d (n "prototty_test_grid") (r "^0.29") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1if9bhbxz2lkyskil6xfyajys949863fzbykb51ds5mwv01kb5ml") (f (quote (("serialize" "serde" "prototty_render/serialize"))))))

