(define-module (crates-io pr ot protocoll) #:use-module (crates-io))

(define-public crate-protocoll-0.1.0 (c (n "protocoll") (v "0.1.0") (h "1hg1racp73npkx2xz8c476sk7nf17p9hxfknglkzz39p2xjgh5xk")))

(define-public crate-protocoll-0.1.1 (c (n "protocoll") (v "0.1.1") (h "0a4h5irm7djmklxf97llqyh8giw02ysacnxbwcjnyc67azl3b8b6")))

(define-public crate-protocoll-0.1.2 (c (n "protocoll") (v "0.1.2") (h "0rpx8r8ha6xwq9n3z8sb48hvqpalyxz5a7lclrhzrw4jlx9d8wi8")))

(define-public crate-protocoll-0.2.0 (c (n "protocoll") (v "0.2.0") (h "0dpc5kz5rljav5fzf5kz7wraqbjsgpq1lkg6kdy17bm87rk3pf9j")))

(define-public crate-protocoll-0.2.1 (c (n "protocoll") (v "0.2.1") (h "1j6cagwif7zr3f9vm5irkx2dfh9j6vdanaa0z43qindwas3dg679")))

(define-public crate-protocoll-0.2.2 (c (n "protocoll") (v "0.2.2") (h "02zxijj58956qcahqd2bncfn8g4d6i438s7b8qqplb86f2i8z4sc")))

(define-public crate-protocoll-0.3.0 (c (n "protocoll") (v "0.3.0") (h "0x47rdv33nfiir1rjidwrzhg3k58c3kd820cxakih9b3kq564491")))

(define-public crate-protocoll-0.3.1 (c (n "protocoll") (v "0.3.1") (h "1f99590s4vdlnfdf3k4npkjwnias9zl2yxa022ab6jdbfllzwjjs")))

