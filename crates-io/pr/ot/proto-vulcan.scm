(define-module (crates-io pr ot proto-vulcan) #:use-module (crates-io))

(define-public crate-proto-vulcan-0.1.0 (c (n "proto-vulcan") (v "0.1.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "tt-call") (r "^1.0.7") (d #t) (k 0)))) (h "1cchrh45xzwh3pr906381dj587i6bw1m58d01xvxq7g2xrvapryq")))

(define-public crate-proto-vulcan-0.1.1 (c (n "proto-vulcan") (v "0.1.1") (d (list (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "proto-vulcan-macros") (r "=0.1.1") (d #t) (k 0)))) (h "1pqfy16075dlkw73ca14k39mzy5zjp4s0x76d84n2kia2qd1wa37")))

(define-public crate-proto-vulcan-0.1.2 (c (n "proto-vulcan") (v "0.1.2") (d (list (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "proto-vulcan-macros") (r "=0.1.2") (d #t) (k 0)))) (h "149khc2fmx6a2mawcpbh76sva826mjkv4023s0m4i52xd1aq6h7z")))

(define-public crate-proto-vulcan-0.1.3 (c (n "proto-vulcan") (v "0.1.3") (d (list (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "proto-vulcan-macros") (r "=0.1.3") (d #t) (k 0)))) (h "0k6cn79qym1610ij9n7arixjx7vk7da94m4vlwj0klg7f46rrqka")))

(define-public crate-proto-vulcan-0.1.4 (c (n "proto-vulcan") (v "0.1.4") (d (list (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "proto-vulcan-macros") (r "=0.1.4") (d #t) (k 0)))) (h "1n6qq8yvbnigh7vpp6wxmr7c547qhygmwzf81wxzikvnr6bwd8jd")))

(define-public crate-proto-vulcan-0.1.5 (c (n "proto-vulcan") (v "0.1.5") (d (list (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "proto-vulcan-macros") (r "=0.1.5") (d #t) (k 0)))) (h "1gl1qlfzjd33ifmzhndynpa7mxx9czp8bjhrmgz84mmc1lz9s5yj")))

(define-public crate-proto-vulcan-0.1.6 (c (n "proto-vulcan") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (t "cfg(debugger)") (k 0)) (d (n "crossterm") (r "^0.19") (f (quote ("serde"))) (d #t) (t "cfg(debugger)") (k 0)) (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "proto-vulcan-macros") (r "=0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("std"))) (t "cfg(debugger)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(debugger)") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (t "cfg(debugger)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (t "cfg(debugger)") (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (t "cfg(debugger)") (k 0)))) (h "1pnrs0m5g23zkv4bl4rxcdkv9s8cwa88055y6qlwqanwrzrilljy") (f (quote (("extras") ("default" "core" "extras" "clpfd" "clpz") ("debugger") ("core") ("clpz") ("clpfd"))))))

