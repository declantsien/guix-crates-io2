(define-module (crates-io pr ot prototty_common) #:use-module (crates-io))

(define-public crate-prototty_common-0.6.0 (c (n "prototty_common") (v "0.6.0") (d (list (d (n "ansi_colour") (r "^0.2") (d #t) (k 0)) (d (n "prototty") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jcapbdmvi42iim9h8l6l7zxqfp6c6q18hczn9fd1zmfv5cr00ph")))

(define-public crate-prototty_common-0.7.0 (c (n "prototty_common") (v "0.7.0") (d (list (d (n "prototty") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0byjax3jhz6a9d9xwpwk4fn8c563srdcdhl7gby7pn8y44hf5pl3")))

(define-public crate-prototty_common-0.7.1 (c (n "prototty_common") (v "0.7.1") (d (list (d (n "prototty") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "051n278hbz4an3rdil0xjprwf9602mdb87s9bfknxlr853z1s992")))

(define-public crate-prototty_common-0.8.0 (c (n "prototty_common") (v "0.8.0") (d (list (d (n "prototty") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04i1fvbxnpbn8fflzzv6c3h7cj0n2fm2i8xi9cvvs0w98l0f0rlf")))

(define-public crate-prototty_common-0.8.1 (c (n "prototty_common") (v "0.8.1") (d (list (d (n "prototty") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1x9dmi1z3zq0xp23xpsrn9c0pd28k73i44hf9s9d5c7bhd80divw")))

(define-public crate-prototty_common-0.8.2 (c (n "prototty_common") (v "0.8.2") (d (list (d (n "prototty") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "075zgdwilvhy6vidzi54ladvf4ckxn6j0qv647psngpibbw05k9n")))

(define-public crate-prototty_common-0.9.0 (c (n "prototty_common") (v "0.9.0") (d (list (d (n "prototty") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06cdnm6lq29afxv36h94dq5n759nsl7jjq9qbffpz39wa3vad9yh")))

(define-public crate-prototty_common-0.10.0 (c (n "prototty_common") (v "0.10.0") (d (list (d (n "prototty") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1n0pjpdmz1fiygxbv3axpc0zdlvq16q708lia69hl667rs13pcya")))

(define-public crate-prototty_common-0.11.0 (c (n "prototty_common") (v "0.11.0") (d (list (d (n "prototty") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1rim5ncbkqsmmp1292bd1rn4vaisb4rlv8ijpnp4xsh46g70gf2c")))

(define-public crate-prototty_common-0.11.1 (c (n "prototty_common") (v "0.11.1") (d (list (d (n "prototty") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1x20fgmv9rjj63jm131pl5rzz1s9p99ign51xh2d8765gwcn7rbh")))

(define-public crate-prototty_common-0.11.2 (c (n "prototty_common") (v "0.11.2") (d (list (d (n "prototty") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bshmj5pnkh9j9zizbk3kkxiy85in34ih1pc46ffl50780zpkcmy")))

(define-public crate-prototty_common-0.11.3 (c (n "prototty_common") (v "0.11.3") (d (list (d (n "prototty") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0mgd90c0nxr40jck63bqi32vh8dg8dhas67yspx0q9zzyyp5a60h")))

(define-public crate-prototty_common-0.12.0 (c (n "prototty_common") (v "0.12.0") (d (list (d (n "prototty") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xy9cycpyfqw67qk34lyrmfwmmkwr7la8mkqnzvc6rx7d2d9yy5g")))

(define-public crate-prototty_common-0.13.0 (c (n "prototty_common") (v "0.13.0") (d (list (d (n "prototty") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1afhjk85ghj8c879pmazk07lby7lmqldw4l8dzk9ijynrx72vf7c")))

(define-public crate-prototty_common-0.14.0 (c (n "prototty_common") (v "0.14.0") (d (list (d (n "prototty") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ciyhac3zl7p50253yw02fx34b39q2nm241nd441pn4y5yap9w7i")))

(define-public crate-prototty_common-0.15.0 (c (n "prototty_common") (v "0.15.0") (d (list (d (n "prototty") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0518iyxhd8bgch51gq5wl1ivx98kim49fjzp5hvbl74n7hmhpnsi")))

(define-public crate-prototty_common-0.16.0 (c (n "prototty_common") (v "0.16.0") (d (list (d (n "prototty") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1wq898rw949i5jsf1ljymgsg2301m7zkbr706k12bvw5svh5vwhx")))

(define-public crate-prototty_common-0.17.0 (c (n "prototty_common") (v "0.17.0") (d (list (d (n "prototty") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01z29xsdwz7any2fx1zl8lx85kjkm6n5rm7scamzfbf0da1dc5aw")))

(define-public crate-prototty_common-0.17.1 (c (n "prototty_common") (v "0.17.1") (d (list (d (n "prototty") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08g5l58ah4n97lk0cl6qzwhf9p12s571n67b2z9jm50yp809r0w5")))

(define-public crate-prototty_common-0.18.0 (c (n "prototty_common") (v "0.18.0") (d (list (d (n "prototty") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0r8w8pqi5j5dm3y8ajpp84z62g3z9r91f4m3gfym72l0jdjmgkz7")))

(define-public crate-prototty_common-0.19.0 (c (n "prototty_common") (v "0.19.0") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "159lgp77h3c0hl5bixvji4kl1xd68g8ywq457y3rhsxj416nqkcf")))

(define-public crate-prototty_common-0.19.1 (c (n "prototty_common") (v "0.19.1") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qh95jq83zgxaj7lrabk7nhr73z0wc6jnwny2pcbpb53p5jk1067")))

(define-public crate-prototty_common-0.19.2 (c (n "prototty_common") (v "0.19.2") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hw6l74lpcxqsgzjscyhgr9cf6ravin9zfhalb9hvzzcfd6b1rg8")))

(define-public crate-prototty_common-0.19.3 (c (n "prototty_common") (v "0.19.3") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11li0b56xh6yfs4i3l97iyd2bzlmzsbrvk4mjf931lyx4cqyaj2l")))

(define-public crate-prototty_common-0.19.4 (c (n "prototty_common") (v "0.19.4") (d (list (d (n "prototty") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1lvs2nnx0qwsjhcj40vcyff5qnbi1n2584r17ks3ql31fad2fzn5")))

(define-public crate-prototty_common-0.20.0 (c (n "prototty_common") (v "0.20.0") (d (list (d (n "prototty") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1whn3x4crci9fc1bjx157g17gl7ycgz04k9qahf07sbps6jav02p")))

(define-public crate-prototty_common-0.21.0 (c (n "prototty_common") (v "0.21.0") (d (list (d (n "prototty") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1lkrp88sbgl04vp4jyki4n68xhdf94n75v6h45bfkhqn38f7a39b") (f (quote (("serialize" "serde" "prototty/serialize"))))))

(define-public crate-prototty_common-0.22.0 (c (n "prototty_common") (v "0.22.0") (d (list (d (n "prototty_input") (r "^0.22") (d #t) (k 0)) (d (n "prototty_render") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13987n50034bkqvfl4pidw7x1m6g0nf27a0k0ydfjy9256gqk5ph") (f (quote (("serialize" "serde" "prototty_render/serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_common-0.23.0 (c (n "prototty_common") (v "0.23.0") (d (list (d (n "prototty_input") (r "^0.23") (d #t) (k 0)) (d (n "prototty_render") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "035py649h03d73884jcdz2kwak85ab8cl9g2jja5m6rvw1rsr74a") (f (quote (("serialize" "serde" "prototty_render/serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_common-0.24.0 (c (n "prototty_common") (v "0.24.0") (d (list (d (n "prototty_input") (r "^0.24") (d #t) (k 0)) (d (n "prototty_render") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "02k7hfq3darhx9ac6lhcnm897qrpnq926k89lxkjc1fywifc2985") (f (quote (("serialize" "serde" "prototty_render/serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_common-0.24.1 (c (n "prototty_common") (v "0.24.1") (d (list (d (n "prototty_input") (r "^0.24") (d #t) (k 0)) (d (n "prototty_render") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0fxlk4kfg79z1hvx7n66kv59pwpag77z2k2707r996li5znqj309") (f (quote (("serialize" "serde" "prototty_render/serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_common-0.25.0 (c (n "prototty_common") (v "0.25.0") (d (list (d (n "prototty_input") (r "^0.25") (d #t) (k 0)) (d (n "prototty_render") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "013b3ippj0j12mivhcl8n7if7bgph4pq85fghc4hs8f8xprw11g4") (f (quote (("serialize" "serde" "prototty_render/serialize" "prototty_input/serialize"))))))

