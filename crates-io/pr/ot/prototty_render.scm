(define-module (crates-io pr ot prototty_render) #:use-module (crates-io))

(define-public crate-prototty_render-0.22.0 (c (n "prototty_render") (v "0.22.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1qpx0ylhhyck6gbvkdr2wr4s74pr8v29b40vzwv9zykk8fq9r91w") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_render-0.22.1 (c (n "prototty_render") (v "0.22.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "14qmw0ww2ixj18793jppwsz7w4p6cgjbj1dv87a1gz26jysjkkhd") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_render-0.23.0 (c (n "prototty_render") (v "0.23.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13sqrh24csfd454ifgmcqqkfff0qj059qa51inm87ynlhdrqzml2") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_render-0.23.1 (c (n "prototty_render") (v "0.23.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0lcqgyz8rj6y011ni37wdlqfsnvjs6svmspwi7lmp9xsi8xxr9jv") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-prototty_render-0.24.0 (c (n "prototty_render") (v "0.24.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1cyjbchlci592pam7h566gz5q6l156kms771cybl2cnhwn6fpipi") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.24.1 (c (n "prototty_render") (v "0.24.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13dj6p7qxkf4cidcy9lnp91vsqyg96w8qhgrm2v73akgh3kczwqw") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.24.2 (c (n "prototty_render") (v "0.24.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0xrkj1byx27vc83dyky8mc5v3pyr7zds7qa4vh4a4b6v718dm442") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.25.0 (c (n "prototty_render") (v "0.25.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0y4fqpvsna3q2qd9blr1ka8nxrfzqjid8l3g6fj6j2h63hppacwz") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.26.0 (c (n "prototty_render") (v "0.26.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1myfvmp44bhxzf3ad3ggqa1r55780lbwmam3mpgymysg1inqb5hf") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.26.1 (c (n "prototty_render") (v "0.26.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "080g7ydkapgxxcx76kijkhpjn383kajqb3gg34i9zf9a2p0xzwja") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.27.0 (c (n "prototty_render") (v "0.27.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "rgb24") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1sfa269pf7pih8r8z6b5zcj3482zpl3h3ipvms0l3myswxpswjif") (f (quote (("serialize" "serde" "coord_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.28.0 (c (n "prototty_render") (v "0.28.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "rgb24") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zs9n1cvxk3065qx98iglayliw740gj9jy4fayan7cqb9nc0lwnj") (f (quote (("serialize" "serde" "grid_2d/serialize" "rgb24/serialize"))))))

(define-public crate-prototty_render-0.29.0 (c (n "prototty_render") (v "0.29.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "rgb24") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ksacd0y4lja7r15iay2x95srmdv1phj9yhr6jlygqfv7ddwdpds") (f (quote (("serialize" "serde" "grid_2d/serialize" "rgb24/serialize"))))))

