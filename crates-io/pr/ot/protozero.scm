(define-module (crates-io pr ot protozero) #:use-module (crates-io))

(define-public crate-protozero-0.0.0 (c (n "protozero") (v "0.0.0") (h "1qxja231amga7qx7bg26w2lcwj4vq7chspkj67rryh59wpxfafz0")))

(define-public crate-protozero-0.1.0 (c (n "protozero") (v "0.1.0") (h "1n898l3lkhy7dxza3z3j0k2lfr25cp9li7fh5j3w1r1k32w4xdcm")))

(define-public crate-protozero-0.1.1 (c (n "protozero") (v "0.1.1") (h "0clhvfnd4l9jrccr4zb09zz2n8rs9hnyla6v2dvfj0ig6gg5sdql")))

(define-public crate-protozero-0.1.2 (c (n "protozero") (v "0.1.2") (h "0vinslqkzb1wvh3parph85acsxkq59q09zdx0xsfi2vxjzpxl8i8")))

(define-public crate-protozero-0.1.3 (c (n "protozero") (v "0.1.3") (h "1d2ffbw0wwghvq2i1may68lqzsibbwzfm3dnavg1caqj0r6fcc7n")))

(define-public crate-protozero-0.1.4 (c (n "protozero") (v "0.1.4") (h "0zix56vn691lxj9m9jw0adj9nnz83vycpz8jg6hn68f8wmfdz85l")))

(define-public crate-protozero-0.1.5 (c (n "protozero") (v "0.1.5") (h "16sp710mayxahgqq8s8iq3q8a952pas4qpcqmzivxyfabgwlzw9i")))

(define-public crate-protozero-0.1.6 (c (n "protozero") (v "0.1.6") (h "1if1js90nxbjw9cqyr0vr7ayjfasjc3nwqzfp6aaxc64lbw8i948")))

(define-public crate-protozero-0.1.7 (c (n "protozero") (v "0.1.7") (h "100raw2z83dbck0rwjzh5f0f2mrv4wccr5wf7aanbv9l823k1kx4")))

