(define-module (crates-io pr ot proto_pdk) #:use-module (crates-io))

(define-public crate-proto_pdk-0.1.0 (c (n "proto_pdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "066a1lfp4hspz65drw3pagxb6ghn223y55jma4y0dnyjk6fm2jji")))

(define-public crate-proto_pdk-0.2.0 (c (n "proto_pdk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f10y0pdizjd3dqscwwdm923f0avd3hika2h30ld0r7fja82pkkx")))

(define-public crate-proto_pdk-0.2.1 (c (n "proto_pdk") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vd0hkyi6am4p69hw2m5lzyjp15s79wwxc2yjas6dnkyv5bl5bbj")))

(define-public crate-proto_pdk-0.2.2 (c (n "proto_pdk") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pd72wkb9ywi7ghx1864myi3qd4j7cncdjcvxihgy97ikx56wn2h")))

(define-public crate-proto_pdk-0.2.3 (c (n "proto_pdk") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wzf3y5sy4mw22rfkm21751a8qhbwqzgddwipc9r8c9n9wr4bb21")))

(define-public crate-proto_pdk-0.3.0 (c (n "proto_pdk") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kn76dzadx0x7ji1936qcm9i12pz2zp6vlvkkj4jjyjnl65xyfkx")))

(define-public crate-proto_pdk-0.3.1 (c (n "proto_pdk") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cn9llspgip4qcwhkh015aqvza0jpw01g09mv9cr9fkpcfdmw5dq")))

(define-public crate-proto_pdk-0.4.0 (c (n "proto_pdk") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zc8cs9pwb91dpmmwvf7vn128nia5j04gb7h0rdj3wclmakmhy8d")))

(define-public crate-proto_pdk-0.4.1 (c (n "proto_pdk") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vh54pqi76p1247l4942ansipp3rrc9dak5d6fafvzqvnfsa2wkd")))

(define-public crate-proto_pdk-0.4.2 (c (n "proto_pdk") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.6") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "07b7533609cbp7vjnrqhcmjrjdygmnpq2pxaassy91x78g6cb00b")))

(define-public crate-proto_pdk-0.4.3 (c (n "proto_pdk") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "16rwbkhlmf29k0mi50km4ddwgbxy1wy8awxna23cg60vhvzssg6f")))

(define-public crate-proto_pdk-0.4.4 (c (n "proto_pdk") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hlxczqd050a0j4lrwyji2mpwai580nk4r8fxjk8wynxjd7a7q7n")))

(define-public crate-proto_pdk-0.4.5 (c (n "proto_pdk") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "11wxmbhdnyqnxhj4gkdbb3a5916vqipclka2n2fhnvc5q5rw9dmw")))

(define-public crate-proto_pdk-0.4.6 (c (n "proto_pdk") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "02hnldjgyaczv65dci7xcnxkykppvv4347wjiv86y3qlx3hj6wlv")))

(define-public crate-proto_pdk-0.6.0 (c (n "proto_pdk") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "18j4aqrmd6disa1zn08wnxc0cqjjw4vdwxdbag5w6hql7hjypznk")))

(define-public crate-proto_pdk-0.6.1 (c (n "proto_pdk") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lz11d8x7wv2j25nj3735yvxf9k69gwcczdl2p08xbjpm6qnf8ny")))

(define-public crate-proto_pdk-0.6.2 (c (n "proto_pdk") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jizgcy4a4x0lf2dgaqbvd5cyvf006iz8zl8s2smqysb1slnl6qd")))

(define-public crate-proto_pdk-0.6.3 (c (n "proto_pdk") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.7") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fg9pvbl0pd0x8q9x1f8a36m5k0fa4akapwjkr62ny710p39zlf1")))

(define-public crate-proto_pdk-0.6.4 (c (n "proto_pdk") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cysx60g9gzh2c8ph6q7s5kman9jhd5b9yplldk08dnbws6x271b")))

(define-public crate-proto_pdk-0.6.5 (c (n "proto_pdk") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h0kziwzjx05dxnls21rg4wzdggk0lmwq3pimsg2cjcljkcj0pia")))

(define-public crate-proto_pdk-0.7.0 (c (n "proto_pdk") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "156jbx2hsmpg8p88h5g1mp4qzhx0nyq1fq0sx8xgm6q66ir9cl0z")))

(define-public crate-proto_pdk-0.7.1 (c (n "proto_pdk") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vd91d9gq9yl9sgmcip8kjx2nabm51zwi45xpjiwvdc8wmdfxry2")))

(define-public crate-proto_pdk-0.7.2 (c (n "proto_pdk") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ihlljminlqyphqpk2bn62d9aqqrgyyryvd7c46nkp9c2xnplrn6")))

(define-public crate-proto_pdk-0.7.3 (c (n "proto_pdk") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j7j6i4l064h2hjdbxhcd4y824gbnzpmapw27ymbsn2krskbmgrb")))

(define-public crate-proto_pdk-0.7.4 (c (n "proto_pdk") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zhd7dv0wr15cabvf5xd609amczk8cmr9nljzf9slhrm2h15q0m4")))

(define-public crate-proto_pdk-0.7.5 (c (n "proto_pdk") (v "0.7.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i6s4lzz00f33jyvpcwh04w3686igzgi8xk2x4p3di6s53kz3msr")))

(define-public crate-proto_pdk-0.7.6 (c (n "proto_pdk") (v "0.7.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x987dsk16vw67grrzs8jyap53vs03gisrrdxid0y93v967x338c")))

(define-public crate-proto_pdk-0.7.7 (c (n "proto_pdk") (v "0.7.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sjvyf24frlf5gmw3vn6vrm2y48gfkbmrfdgq9gpjsbi0bjhzg48")))

(define-public crate-proto_pdk-0.7.8 (c (n "proto_pdk") (v "0.7.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "01lm56yxr9xmjgih31ss1x9jajlc1yacmdzhwcqvz8w7wzngwbic")))

(define-public crate-proto_pdk-0.7.9 (c (n "proto_pdk") (v "0.7.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_map") (r "^0.4.8") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zyqn9swlpprf8m7dhazan70w4bymh63mbhki6awxplk2lwvmmg6")))

(define-public crate-proto_pdk-0.8.0 (c (n "proto_pdk") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n38f7n2bjn79ggiphf7qfnql6v3bsdzhkijg7mb5z1k7s1qmyv3")))

(define-public crate-proto_pdk-0.8.1 (c (n "proto_pdk") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sx33g1jfhwnffqfwaj4aa8g3lb3msfyk2h813nqda4mxcnpmm2d")))

(define-public crate-proto_pdk-0.8.2 (c (n "proto_pdk") (v "0.8.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qg5lkp81lwl4bvxrx7ha4h25kny282n1yssdyrqxfmv7995d1ds")))

(define-public crate-proto_pdk-0.9.0 (c (n "proto_pdk") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "00p1rw49kmkgig935gz3b0nwsrwvpz3bf79l3jzrnmq193k4y2yf")))

(define-public crate-proto_pdk-0.10.0 (c (n "proto_pdk") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "05jdnbmsrwzxj94j10qqa4mvl2bd6kg2d19pagybc9pv9n7lhym6")))

(define-public crate-proto_pdk-0.10.1 (c (n "proto_pdk") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ym1kzlqhwcc9qqcf07q5d78nnjgr68lb72fa6cfw00fyd7l0jh8")))

(define-public crate-proto_pdk-0.10.2 (c (n "proto_pdk") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g6w4z266zpdv4qjbq66k5nrj9mgb3ljcrl6mfcm03b9ihyra6hm")))

(define-public crate-proto_pdk-0.10.3 (c (n "proto_pdk") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "07p6z6nsm7y058adxls9jnhysd4wkmpj9zziavf4zir4fcfig2fh")))

(define-public crate-proto_pdk-0.11.0 (c (n "proto_pdk") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fc14m41dd7ji9dmg0f0zz705asnhvsc9bhgmgjla357rpcgr936")))

(define-public crate-proto_pdk-0.11.1 (c (n "proto_pdk") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m1ws1hcwp92xd4k59swx59phydxp4c75bb6qjpyyqslllg04i97")))

(define-public crate-proto_pdk-0.11.2 (c (n "proto_pdk") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jiysiamy02hjwsialpr0cpf3icsr9fy9cbgadfcr43dzabfx4n6")))

(define-public crate-proto_pdk-0.12.0 (c (n "proto_pdk") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "084l3w2gqd6l90ms0gy4nnidnsc896bzpkdjbp3yw4xvksij273p")))

(define-public crate-proto_pdk-0.12.1 (c (n "proto_pdk") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z2kxq2w8xdmjps1grcxxqrv7jcwcwzd0nmyn3yrhm0g9p09spsk")))

(define-public crate-proto_pdk-0.12.2 (c (n "proto_pdk") (v "0.12.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h6xccl39sa60aizcy7ipkhrwk91gwz9wq6vgix8rjvjp397k37l")))

(define-public crate-proto_pdk-0.12.3 (c (n "proto_pdk") (v "0.12.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s6pikhfqnv3y8y2yh0fy5zakzsj5q7ayldl4fj73q4al35c4qph")))

(define-public crate-proto_pdk-0.13.0 (c (n "proto_pdk") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^0.3.4") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m226x1zmv14rvs7yzlgppimhcdcjcx1cl61hfqmgrmya1vmg2wx")))

(define-public crate-proto_pdk-0.14.0 (c (n "proto_pdk") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gz9l8imri2af9gcavvpaf51q6r0cykdrfw056k17gndl7fchlrg")))

(define-public crate-proto_pdk-0.14.1 (c (n "proto_pdk") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "14jyv6p9x71yav9xd4l7j5r43i3p35i363ip0jlsssh8l02xp47a")))

(define-public crate-proto_pdk-0.14.2 (c (n "proto_pdk") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kpgp5v0gmx2pvvq9473pj1vbbf6f2rx9yql1kxixkppy1mib324")))

(define-public crate-proto_pdk-0.14.3 (c (n "proto_pdk") (v "0.14.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sbask9wi65ivx1prq912xc69cksdanyqgnwdlhwr6di1n4l4qhr")))

(define-public crate-proto_pdk-0.15.0 (c (n "proto_pdk") (v "0.15.0") (d (list (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.3.0") (f (quote ("pdk"))) (d #t) (k 0)))) (h "1sfmnwzqds53jwp4ds4q7xhjd8hzmcikf32gqh7nmlxmn6dpyfjk")))

(define-public crate-proto_pdk-0.15.1 (c (n "proto_pdk") (v "0.15.1") (d (list (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.1.0") (d #t) (k 0)))) (h "1jnp3knp0h3hhmp7vwkn7423vgpplvhh4jys9wcmm1vknc2h3yfa")))

(define-public crate-proto_pdk-0.15.2 (c (n "proto_pdk") (v "0.15.2") (d (list (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.15.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.1.0") (d #t) (k 0)))) (h "03f26yg6hp7ddcrylyip4628ccm5jd0m4lxg03nlqwcqhd4id0sr")))

(define-public crate-proto_pdk-0.16.0 (c (n "proto_pdk") (v "0.16.0") (d (list (d (n "extism-pdk") (r "^1.0.1") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.16.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.0") (d #t) (k 0)))) (h "0d20v56jqrzzhm32chgvying0zp98lyf06kblxp5kbrd1k7yylld")))

(define-public crate-proto_pdk-0.16.1 (c (n "proto_pdk") (v "0.16.1") (d (list (d (n "extism-pdk") (r "^1.0.1") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.16.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.1") (d #t) (k 0)))) (h "02ldgh2avpjfs7zk67krs8qip08d3sdwqj32cy0l6a1yi4jcvi97")))

(define-public crate-proto_pdk-0.17.0 (c (n "proto_pdk") (v "0.17.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.16.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.1") (d #t) (k 0)))) (h "138gwhdk6vfsmdvgd4lpp8qk50cm5488vg2ap348cpgdbnnkw0wy")))

(define-public crate-proto_pdk-0.17.1 (c (n "proto_pdk") (v "0.17.1") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.1") (d #t) (k 0)))) (h "1s6c99yy2kz54834990695g834v84slvyzy8z7897qc7dckkf1mb")))

(define-public crate-proto_pdk-0.17.2 (c (n "proto_pdk") (v "0.17.2") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.1") (d #t) (k 0)))) (h "1rj3sff88pd17qbxihnwi0njfl8jbbplf169vm57zacx540893wx")))

(define-public crate-proto_pdk-0.17.3 (c (n "proto_pdk") (v "0.17.3") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.2") (d #t) (k 0)))) (h "01yafzd7ynyj7vm7cnh2yq3m9dxbdairpycknj0pzp5vp80sz5nj")))

(define-public crate-proto_pdk-0.17.4 (c (n "proto_pdk") (v "0.17.4") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.3") (d #t) (k 0)))) (h "03h4h0cg772xf22ydhxgz8f8lll4m5sg4f5ba7b1ngfzfvbcd4fa")))

(define-public crate-proto_pdk-0.17.5 (c (n "proto_pdk") (v "0.17.5") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.3") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.4") (d #t) (k 0)))) (h "1q3w4w5ppfqqfh8apqx8m22cmllqix9xbapzxql1kgrxrpr08hvl")))

(define-public crate-proto_pdk-0.17.6 (c (n "proto_pdk") (v "0.17.6") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.4") (d #t) (k 0)))) (h "17qvga6fbpv5calvnbmjrardj7wyy8i28fbl50yjr3fl3dajyhkm")))

(define-public crate-proto_pdk-0.17.7 (c (n "proto_pdk") (v "0.17.7") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.17.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.3.0") (d #t) (k 0)))) (h "11a51frwdd6lh19qyyl2waj29bsa078iy3yrg7m4ysrik71y85fz")))

(define-public crate-proto_pdk-0.18.0 (c (n "proto_pdk") (v "0.18.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.18.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.4.0") (d #t) (k 0)))) (h "16qfxsyjwqnn69xlwnwyl2nj5a8n1srl202xdmhlz45k35m5wqhl")))

(define-public crate-proto_pdk-0.19.0 (c (n "proto_pdk") (v "0.19.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.19.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.5.0") (d #t) (k 0)))) (h "162iwkvzln65c4k15c3v9xx6lns9g2sayp5lmm86jll9csgs6zdh")))

(define-public crate-proto_pdk-0.19.1 (c (n "proto_pdk") (v "0.19.1") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.19.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.5.1") (d #t) (k 0)))) (h "15gkx5va76568zia23v1m4001864j8i0gykmap1d13v7v7pzdw88")))

(define-public crate-proto_pdk-0.19.2 (c (n "proto_pdk") (v "0.19.2") (d (list (d (n "extism-pdk") (r "^1.2.0") (d #t) (k 0)) (d (n "proto_pdk_api") (r "^0.19.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.5.2") (d #t) (k 0)))) (h "19q5flq19whs7rs4x5dfp13sb34gzg0mw45zm1plyir2pcd08ama")))

