(define-module (crates-io pr ot protore) #:use-module (crates-io))

(define-public crate-protore-0.1.0 (c (n "protore") (v "0.1.0") (h "1wjk40ajfy998wwhw8xwmhf3iavq61ry237gzsdlf2dcpip12812") (y #t)))

(define-public crate-protore-0.1.1 (c (n "protore") (v "0.1.1") (h "1mgazry6wpfj72gdxzg4iravhn3b1x9y53h8j163qmn00iiji7f7")))

