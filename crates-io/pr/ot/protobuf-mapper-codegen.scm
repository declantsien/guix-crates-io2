(define-module (crates-io pr ot protobuf-mapper-codegen) #:use-module (crates-io))

(define-public crate-protobuf-mapper-codegen-0.1.0 (c (n "protobuf-mapper-codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sk17irdrl4n5qw4s34c9wm5vklqx99pr48vsbmalpi759s013ag")))

