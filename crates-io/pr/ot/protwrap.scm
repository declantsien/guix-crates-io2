(define-module (crates-io pr ot protwrap) #:use-module (crates-io))

(define-public crate-protwrap-0.1.0 (c (n "protwrap") (v "0.1.0") (d (list (d (n "tok") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio-util") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0kcpbg1a2vcnlzbfrc8g7lz28knibz76pxqfwivda3icx94namh7") (f (quote (("tokio" "tok" "tokio-util"))))))

(define-public crate-protwrap-0.1.1 (c (n "protwrap") (v "0.1.1") (d (list (d (n "tok") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio-util") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0p9ahdkpm4h101i97hcjwxw7c6ycxw2hasp1aj60b6k67fv76awx") (f (quote (("tokio" "tok" "tokio-util"))))))

(define-public crate-protwrap-0.2.0 (c (n "protwrap") (v "0.2.0") (d (list (d (n "tok") (r "^1.17.0") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio-util") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0v2bb7ivxijaqyxnkk1212480050r3ph1sy5r1mfh4xqrdsj9nzp") (f (quote (("tokio" "tok" "tokio-util"))))))

(define-public crate-protwrap-0.2.1 (c (n "protwrap") (v "0.2.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio-util") (r "^0.7.9") (o #t) (d #t) (k 0)))) (h "19m66z5ivkf6qwhgpbsziqxgq3qwwm7q76h8gmj2lw4m464dfwyy") (y #t) (s 2) (e (quote (("tokio" "dep:tokio" "dep:tokio-util"))))))

(define-public crate-protwrap-0.2.2 (c (n "protwrap") (v "0.2.2") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio-util") (r "^0.7.9") (o #t) (d #t) (k 0)))) (h "0pc87mi0mwly95ybbx08p3jhkkwldxr6945sbvkndxb4bdsigakr") (s 2) (e (quote (("tokio" "dep:tokio" "dep:tokio-util"))))))

