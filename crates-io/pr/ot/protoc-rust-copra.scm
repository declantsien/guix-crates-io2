(define-module (crates-io pr ot protoc-rust-copra) #:use-module (crates-io))

(define-public crate-protoc-rust-copra-0.1.0 (c (n "protoc-rust-copra") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc") (r "^1.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1s1xqrd83vpyfspdc9a3y9riimcbl6hcc9rddgvh5mf66jqqahnf")))

(define-public crate-protoc-rust-copra-0.1.1 (c (n "protoc-rust-copra") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc") (r "^1.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1b9ihlwwxqfhfqzkdzlvwfywi87cyjdf4ic7whxa7ghlpgpm12w0")))

