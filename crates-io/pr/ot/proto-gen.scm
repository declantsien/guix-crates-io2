(define-module (crates-io pr ot proto-gen) #:use-module (crates-io))

(define-public crate-proto-gen-0.1.0 (c (n "proto-gen") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "14fqh49aa33l89ikq7hy3jiqqqdxqfz6ys7759fld7z2pqfqm3ax") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.1.1 (c (n "proto-gen") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0y6mpkvjn38xs18944wm1qi0x7vayk0szywjp98kjxs4gm5l8772") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.1.2 (c (n "proto-gen") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.1") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "00dq7in9bb1zckvawzfyvv7zaj24gf2sjqjg2wf47xhvf7xsdll8") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.0 (c (n "proto-gen") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "1cpa6170zlx4pljjpadq1i0a7gk7g7dl1d5jg7f8090inzhqldfn") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.1 (c (n "proto-gen") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0damrngb5jgpzjffhjg02nn80h7ci8qipkh35364z85wd7dbcmrw") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.2 (c (n "proto-gen") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0l9yghw5x3xnf2zj5jgcybpanf5r7wzdmn2dl4afc4w37phvkrn3") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.3 (c (n "proto-gen") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0ncrq8ng0kl91yfvw33mdkyw4fbm0lcw0gfpha68nlrf35hcfngj") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.4 (c (n "proto-gen") (v "0.2.4") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "169j50iz8j4ig39j3s34z2vzvapg8mh04g3b1mc51qpz3dbikfcd") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.5 (c (n "proto-gen") (v "0.2.5") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0fr4mfmswiy4lx3x621msg10j8liw61m4sslxwv74ld3b8jgjlvm") (f (quote (("protoc-tests") ("default"))))))

(define-public crate-proto-gen-0.2.6 (c (n "proto-gen") (v "0.2.6") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("transport" "prost" "transport" "prost"))) (k 0)))) (h "0a2g3lfj6ahfanw084dwqclm02f0mv82kqy6dyp9mclxfxpzwi32") (f (quote (("protoc-tests") ("default"))))))

