(define-module (crates-io pr ot prototty_test_grid) #:use-module (crates-io))

(define-public crate-prototty_test_grid-0.28.0 (c (n "prototty_test_grid") (v "0.28.0") (d (list (d (n "prototty_render") (r "^0.28") (d #t) (k 0)))) (h "0hlsbxrmgxr5zpg5i5rbgpb9jzw48kg1pgv2bpykxhvm5qkzp2sf")))

(define-public crate-prototty_test_grid-0.29.0 (c (n "prototty_test_grid") (v "0.29.0") (d (list (d (n "prototty_render") (r "^0.29") (d #t) (k 0)))) (h "0q0xcpvgj6qaqv7f9pjfsvrdc5mdr76a7ki0ms3h7s6ndslhd8xq")))

