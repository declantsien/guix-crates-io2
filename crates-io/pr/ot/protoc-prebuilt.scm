(define-module (crates-io pr ot protoc-prebuilt) #:use-module (crates-io))

(define-public crate-protoc-prebuilt-0.1.0 (c (n "protoc-prebuilt") (v "0.1.0") (d (list (d (n "prost-build") (r "^0.11") (d #t) (k 2)) (d (n "protobuf-codegen") (r "^3.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "0zfrv4qcnlnrx3fp82y4abjk5rr5j9x0gr4779lyyaxdllva16mx")))

(define-public crate-protoc-prebuilt-0.2.0 (c (n "protoc-prebuilt") (v "0.2.0") (d (list (d (n "prost-build") (r "^0.11") (d #t) (k 2)) (d (n "protobuf-codegen") (r "^3.2") (d #t) (k 2)) (d (n "ureq") (r "^2.6") (f (quote ("tls"))) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1p84ff9s0dkzw21rzhwgrqyakc9v2fk9l15zhg04w1skzcqfkbip")))

(define-public crate-protoc-prebuilt-0.3.0 (c (n "protoc-prebuilt") (v "0.3.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 2)) (d (n "protobuf-codegen") (r "^3.2") (d #t) (k 2)) (d (n "ureq") (r "^2.9") (f (quote ("tls"))) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1mcywryhixylkv12lsdxcrp6pg3ragirzl6zaglcdf733xjd91ad")))

