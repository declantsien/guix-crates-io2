(define-module (crates-io pr ot protected_integer) #:use-module (crates-io))

(define-public crate-protected_integer-0.1.0 (c (n "protected_integer") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hjjbb3mqiqfkzahyynj3gi7m8nk9iza7rxv7bng056nws5z3wn7")))

(define-public crate-protected_integer-0.1.1 (c (n "protected_integer") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15q4j9042mp01b8wvcr5li8y0dc089q5m7h4n3qgmiggj59yfyl2")))

