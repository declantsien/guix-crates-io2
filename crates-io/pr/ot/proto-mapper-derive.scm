(define-module (crates-io pr ot proto-mapper-derive) #:use-module (crates-io))

(define-public crate-proto-mapper-derive-0.1.0 (c (n "proto-mapper-derive") (v "0.1.0") (d (list (d (n "proto-mapper-core") (r "^0.1.0") (d #t) (k 0)))) (h "1224jlb9z4aqil9ab6xn52d64rr9vfdaxdg8l63gpdgbjaz7vq08") (f (quote (("protobuf" "proto-mapper-core/protobuf") ("prost" "proto-mapper-core/prost"))))))

(define-public crate-proto-mapper-derive-0.1.1 (c (n "proto-mapper-derive") (v "0.1.1") (d (list (d (n "proto-mapper-core") (r "^0.1.1") (d #t) (k 0)))) (h "11rchzxv9f2bz1swarb8m91snq1k6x932lfx2w3mlli5bgg0939k") (f (quote (("protobuf" "proto-mapper-core/protobuf") ("prost" "proto-mapper-core/prost"))))))

(define-public crate-proto-mapper-derive-0.1.2 (c (n "proto-mapper-derive") (v "0.1.2") (d (list (d (n "proto-mapper-core") (r "^0.1.2") (d #t) (k 0)))) (h "0xgn96sdnvjz28qv61kkdza5fg08gjv7bgc4qnh48nj7mx2ldw7z") (f (quote (("protobuf" "proto-mapper-core/protobuf") ("prost" "proto-mapper-core/prost"))))))

