(define-module (crates-io pr ot protoc-rust-no-elision) #:use-module (crates-io))

(define-public crate-protoc-rust-no-elision-2.0.4 (c (n "protoc-rust-no-elision") (v "2.0.4") (d (list (d (n "protobuf") (r "= 2.0.4") (d #t) (k 0)) (d (n "protobuf-codegen-no-elision") (r "= 2.0.4") (d #t) (k 0)) (d (n "protoc") (r "= 2.0.4") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1cbzgzs94z3q0hacrlyjxdgdaq6jj5ip18dp1cgwwfcr65z8gg0h")))

