(define-module (crates-io pr ot protobuf3) #:use-module (crates-io))

(define-public crate-protobuf3-2.27.2 (c (n "protobuf3") (v "2.27.2") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zar4qkc149hxcmrngyij32swq81rdspikcj43fypwylv7w237vm") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

