(define-module (crates-io pr ot prototty_native_audio) #:use-module (crates-io))

(define-public crate-prototty_native_audio-0.28.0 (c (n "prototty_native_audio") (v "0.28.0") (d (list (d (n "prototty_audio") (r "^0.28") (d #t) (k 0)) (d (n "rodio") (r "^0.10") (d #t) (k 0)))) (h "0v1mfshxyql9j8470vsps63rnvn1p6h60y385cqc4gm5kwfnb9j3")))

(define-public crate-prototty_native_audio-0.29.0 (c (n "prototty_native_audio") (v "0.29.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prototty_audio") (r "^0.29") (d #t) (k 0)) (d (n "rodio") (r "^0.10") (d #t) (k 0)))) (h "16360721hz05s8v9awi3i8rdam25dm8dllgljz8zxfkvb5xlj65h") (f (quote (("force_dedicated_audio_thread"))))))

