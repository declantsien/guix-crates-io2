(define-module (crates-io pr ot protomolecule) #:use-module (crates-io))

(define-public crate-protomolecule-0.0.1 (c (n "protomolecule") (v "0.0.1") (h "1lwm710n6z6wny9p4n4b6b240wxryr1nynfmcalp0haj56l8nnqn")))

(define-public crate-protomolecule-0.0.2 (c (n "protomolecule") (v "0.0.2") (h "05m6pqj86g18f4sp265fw2qacrjb53glm4slzxayhkjak7z3plh7")))

