(define-module (crates-io pr ot protoc-gen-prost-utoipa) #:use-module (crates-io))

(define-public crate-protoc-gen-prost-utoipa-0.1.0 (c (n "protoc-gen-prost-utoipa") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.8") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.11.8") (k 0)) (d (n "prost-types") (r "^0.11.8") (k 0)) (d (n "protoc-gen-prost") (r "^0.2.0") (d #t) (k 0)) (d (n "prutoipa-build") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (k 0)))) (h "1930bd0nh5dh45qkrza75plwz1h487g96vfh7a7a8pwrcw5mfb10")))

