(define-module (crates-io pr ot protospec-build) #:use-module (crates-io))

(define-public crate-protospec-build-0.2.0 (c (n "protospec-build") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1x842vqa9sckz9xjg435inmqka43jlbpz5n2b8hfbzwa8hc47np0")))

(define-public crate-protospec-build-0.3.0 (c (n "protospec-build") (v "0.3.0") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ynz9ph20kaygnypchwg7yprxfpirjiygc9q9n0ccjlg2b41rkhd")))

