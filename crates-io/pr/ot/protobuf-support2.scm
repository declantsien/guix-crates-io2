(define-module (crates-io pr ot protobuf-support2) #:use-module (crates-io))

(define-public crate-protobuf-support2-4.0.0-alpha.0 (c (n "protobuf-support2") (v "4.0.0-alpha.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mx16jcxyjv667bxicqqwrpib1ziz7p80v75dhrin7zz7c0van4x")))

(define-public crate-protobuf-support2-4.0.0-alpha.1 (c (n "protobuf-support2") (v "4.0.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0acpjifiykqgm4902jpmr1gdicj64vhirfpbcl9xhvy2ana52vb7")))

(define-public crate-protobuf-support2-4.0.0-alpha.2 (c (n "protobuf-support2") (v "4.0.0-alpha.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v63m2mx13l3s702ig0di7j4i43ix7745lj9w7x28l74k5r96vhw")))

