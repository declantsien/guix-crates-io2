(define-module (crates-io pr ot prototty_graphical_common) #:use-module (crates-io))

(define-public crate-prototty_graphical_common-0.28.0 (c (n "prototty_graphical_common") (v "0.28.0") (h "19xasc41l8fiq4wcxy887fdmgbvmldsjng1g2vhg3akqj9fcy4ja")))

(define-public crate-prototty_graphical_common-0.29.0 (c (n "prototty_graphical_common") (v "0.29.0") (h "1j5rym9w7qwyncfpjgp6bjvs911zymwwk1z1432ljv8yzdar6w1h")))

