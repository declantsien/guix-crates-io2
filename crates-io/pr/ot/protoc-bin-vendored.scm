(define-module (crates-io pr ot protoc-bin-vendored) #:use-module (crates-io))

(define-public crate-protoc-bin-vendored-2.14.0 (c (n "protoc-bin-vendored") (v "2.14.0") (d (list (d (n "protoc") (r "= 2.14.0") (d #t) (k 2)))) (h "0lgyvs3xzgl4dyiymw75h2q8inq9ds7fxgl2zyhrr52528d0smxc")))

(define-public crate-protoc-bin-vendored-2.15.0 (c (n "protoc-bin-vendored") (v "2.15.0") (d (list (d (n "protoc") (r "=2.15.0") (d #t) (k 2)))) (h "06sfw6q15sy3p6dwgj9jw35g4r8xk7mpzqnclqx14i1cd2ilz95a")))

(define-public crate-protoc-bin-vendored-2.15.1 (c (n "protoc-bin-vendored") (v "2.15.1") (d (list (d (n "protoc") (r "=2.15.1") (d #t) (k 2)))) (h "1zws1qf1kyw4l5n31akqzr6cxq3mm0r6996kaddjfxj31i1jan7k")))

(define-public crate-protoc-bin-vendored-2.16.0 (c (n "protoc-bin-vendored") (v "2.16.0") (d (list (d (n "protoc") (r "=2.16.0") (d #t) (k 2)))) (h "0ys3diipm7jcj63710298imkn60n12pr4g85ibnxsw1hf83lvygh")))

(define-public crate-protoc-bin-vendored-2.16.1 (c (n "protoc-bin-vendored") (v "2.16.1") (d (list (d (n "protoc") (r "=2.16.1") (d #t) (k 2)))) (h "16ql3df632p7dibrc4qvyg68b5r8kk5hjlgmv3q2zgmcnv7i3m5w")))

(define-public crate-protoc-bin-vendored-2.16.2 (c (n "protoc-bin-vendored") (v "2.16.2") (d (list (d (n "protoc") (r "=2.16.2") (d #t) (k 2)))) (h "1kgbakr8sbm67xrd6z1g1b4bv44rqcxhba5dn5zlhpygf0n3kaml")))

(define-public crate-protoc-bin-vendored-2.17.0 (c (n "protoc-bin-vendored") (v "2.17.0") (d (list (d (n "protoc") (r "=2.17.0") (d #t) (k 2)))) (h "19cw4wvhjmvgzar4mcd65xw0148ysv60k7sin4z18nwpk8gzdmsz")))

(define-public crate-protoc-bin-vendored-2.18.0 (c (n "protoc-bin-vendored") (v "2.18.0") (d (list (d (n "protoc") (r "=2.18.0") (d #t) (k 2)))) (h "15qwlp13fbcncyvljhg7xdns3b6s4hix72l9z5kysj4jm9a3mqxw")))

(define-public crate-protoc-bin-vendored-2.18.1 (c (n "protoc-bin-vendored") (v "2.18.1") (d (list (d (n "protoc") (r "=2.18.1") (d #t) (k 2)))) (h "17qh5p9117z7l564bjkimzbkrphz2xyng3y26a4r6g30lmc6zhbv")))

(define-public crate-protoc-bin-vendored-2.19.0 (c (n "protoc-bin-vendored") (v "2.19.0") (d (list (d (n "protoc") (r "=2.19.0") (d #t) (k 2)))) (h "0hhmldr4bkycqpgm0d1vrc18amp97af72n91c39kyxsbbz5vw0vf")))

(define-public crate-protoc-bin-vendored-2.20.0 (c (n "protoc-bin-vendored") (v "2.20.0") (d (list (d (n "protoc") (r "=2.20.0") (d #t) (k 2)))) (h "00qgdczsxm0nsl59h0fy519qvkna4rh052ygmsvswm092wdbpjaj")))

(define-public crate-protoc-bin-vendored-2.21.0 (c (n "protoc-bin-vendored") (v "2.21.0") (d (list (d (n "protoc") (r "=2.21.0") (d #t) (k 2)))) (h "1dphpa0qixhjifr0l4c6rzk18zqfzci4k5a1a1s3kdgb527dm0y7")))

(define-public crate-protoc-bin-vendored-2.22.0 (c (n "protoc-bin-vendored") (v "2.22.0") (d (list (d (n "protoc") (r "=2.22.0") (d #t) (k 2)))) (h "1nvqy9icvc3k9l60p3c8fk5fi4027816nrg5bbnf0z2pnk6g2f3c")))

(define-public crate-protoc-bin-vendored-2.22.1 (c (n "protoc-bin-vendored") (v "2.22.1") (d (list (d (n "protoc") (r "=2.22.1") (d #t) (k 2)))) (h "0cc8gb1pkfnklmy4n422zspwpg512kymlqhwdidsj3b2jgpqv5lg")))

(define-public crate-protoc-bin-vendored-2.18.2 (c (n "protoc-bin-vendored") (v "2.18.2") (d (list (d (n "protoc") (r "=2.18.2") (d #t) (k 2)))) (h "0412dpfgz4r3cpackafqnr6pns8k28c6gf8xhax86jsbj087x7kz")))

(define-public crate-protoc-bin-vendored-2.23.0 (c (n "protoc-bin-vendored") (v "2.23.0") (d (list (d (n "protoc") (r "=2.23.0") (d #t) (k 2)))) (h "0yn7jqgdagjx772h67nwj0h8p9q0xf5kbvsmqv19yj3pic39n5ik")))

(define-public crate-protoc-bin-vendored-2.24.0 (c (n "protoc-bin-vendored") (v "2.24.0") (d (list (d (n "protoc") (r "=2.24.0") (d #t) (k 2)))) (h "0r2g6vb6hbn8ai528sqqlfswxawm6h0wh68a22rg4n6a2kb4ij9j")))

(define-public crate-protoc-bin-vendored-2.24.1 (c (n "protoc-bin-vendored") (v "2.24.1") (d (list (d (n "protoc") (r "=2.24.1") (d #t) (k 2)))) (h "0rslx6j7g8r4wkd9g72r0b617b2zjrzmlnynnk1m3rhydvdw7i8h")))

(define-public crate-protoc-bin-vendored-2.24.2 (c (n "protoc-bin-vendored") (v "2.24.2") (d (list (d (n "protoc") (r "=2.24.2") (d #t) (k 2)))) (h "1k8jqpdlp86fsqfzg89rvfrja1s2ych9l1sqx5sb8bpif3dyad3k")))

(define-public crate-protoc-bin-vendored-2.25.0 (c (n "protoc-bin-vendored") (v "2.25.0") (d (list (d (n "protoc") (r "=2.25.0") (d #t) (k 2)))) (h "081g1y71xhgmp0v269pqaykia1lc4d47rngk9g9jgsla20abymws")))

(define-public crate-protoc-bin-vendored-2.25.1 (c (n "protoc-bin-vendored") (v "2.25.1") (d (list (d (n "protoc") (r "=2.25.1") (d #t) (k 2)))) (h "1pd545fg7yxcksch223awg35vnd6f099687v5knypjh8f60nv9f2")))

(define-public crate-protoc-bin-vendored-2.25.2 (c (n "protoc-bin-vendored") (v "2.25.2") (d (list (d (n "protoc") (r "=2.25.2") (d #t) (k 2)))) (h "0mkc2x0k66qif432nssw29b7311d46jagsj1smmjicrng1a3zcx0")))

(define-public crate-protoc-bin-vendored-3.0.0-alpha.1 (c (n "protoc-bin-vendored") (v "3.0.0-alpha.1") (d (list (d (n "protoc") (r "=3.0.0-alpha.1") (d #t) (k 2)))) (h "17ci698fkv20xz8n83wnmvbnla51vpbas2w92nv6fs5asy4f4hih")))

(define-public crate-protoc-bin-vendored-3.0.0-alpha.2 (c (n "protoc-bin-vendored") (v "3.0.0-alpha.2") (d (list (d (n "protoc") (r "=3.0.0-alpha.2") (d #t) (k 2)))) (h "0xnrin9z5ynvlslc2c34vqs50lw2vk3ccs4d4lcsdr1hilwdgr42")))

(define-public crate-protoc-bin-vendored-2.26.0 (c (n "protoc-bin-vendored") (v "2.26.0") (d (list (d (n "protoc") (r "=2.26.0") (d #t) (k 2)))) (h "00b46m5rn7pccl09yvhc13xzzi1r3c3ifzhi4givq7q3kq47ivs0")))

(define-public crate-protoc-bin-vendored-2.26.1 (c (n "protoc-bin-vendored") (v "2.26.1") (d (list (d (n "protoc") (r "=2.26.1") (d #t) (k 2)))) (h "0iq27fp8a02v18jwi2gj4x919js93gfp79b2187k3fcswca4di2s")))

(define-public crate-protoc-bin-vendored-3.0.0-alpha.3 (c (n "protoc-bin-vendored") (v "3.0.0-alpha.3") (d (list (d (n "protoc") (r "=3.0.0-alpha.3") (d #t) (k 2)))) (h "1xd4rl77xpa0fwcvbdbfwhfn1pccsnz1fsznx1pbvc7a76lnkip0")))

(define-public crate-protoc-bin-vendored-3.0.0-alpha.4 (c (n "protoc-bin-vendored") (v "3.0.0-alpha.4") (d (list (d (n "protoc") (r "=3.0.0-alpha.4") (d #t) (k 2)))) (h "1lhswxjyl2bsg47avhl4l5xsggzvq735bswi0lhxlhg3by8gf6wr")))

(define-public crate-protoc-bin-vendored-3.0.0-alpha.5 (c (n "protoc-bin-vendored") (v "3.0.0-alpha.5") (d (list (d (n "protoc") (r "=3.0.0-alpha.5") (d #t) (k 2)))) (h "1rx2rpl0gr24x21vizcqrfplfa8l0b22i29p99glg4b8alvkczkc")))

(define-public crate-protoc-bin-vendored-2.27.0 (c (n "protoc-bin-vendored") (v "2.27.0") (d (list (d (n "protoc") (r "=2.27.0") (d #t) (k 2)))) (h "1sbm0ml6xzw473rlai8g4fniq2k7i0q830nq2il1iym3s7qpc4l3")))

(define-public crate-protoc-bin-vendored-2.27.1 (c (n "protoc-bin-vendored") (v "2.27.1") (d (list (d (n "protoc") (r "=2.27.1") (d #t) (k 2)))) (h "0hzfpfp5gl7nr4r8xslyl1xzkkazk06xnwvgz490xqr61m2v7969")))

(define-public crate-protoc-bin-vendored-2.27.2 (c (n "protoc-bin-vendored") (v "2.27.2") (h "1zjhj0rhbz45qbjz5cwn49jlyxj23wvnykji9wlvij3l4c4064s6")))

(define-public crate-protoc-bin-vendored-2.27.3 (c (n "protoc-bin-vendored") (v "2.27.3") (h "0lcknddf3mhklhh9kmwwzqh4ipggy1mcn3m86pihz24ashf85qb8")))

(define-public crate-protoc-bin-vendored-3.0.0 (c (n "protoc-bin-vendored") (v "3.0.0") (d (list (d (n "protoc-bin-vendored-linux-aarch_64") (r "=3.0.0") (d #t) (k 0)) (d (n "protoc-bin-vendored-linux-ppcle_64") (r "=3.0.0") (d #t) (k 0)) (d (n "protoc-bin-vendored-linux-x86_32") (r "=3.0.0") (d #t) (k 0)) (d (n "protoc-bin-vendored-linux-x86_64") (r "=3.0.0") (d #t) (k 0)) (d (n "protoc-bin-vendored-macos-x86_64") (r "=3.0.0") (d #t) (k 0)) (d (n "protoc-bin-vendored-win32") (r "=3.0.0") (d #t) (k 0)))) (h "0zgjld4kw0331swf7kgkdd0cyjx4w25pv48zmncf4csn7riahp00")))

