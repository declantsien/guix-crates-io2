(define-module (crates-io pr ot protocol_v3_macro) #:use-module (crates-io))

(define-public crate-protocol_v3_macro-0.1.0 (c (n "protocol_v3_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18y6pzjmn45vx8917amhklvc3zz51ddgsvyf20jywimvr2dyq975")))

(define-public crate-protocol_v3_macro-0.1.1 (c (n "protocol_v3_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hdbzc54rsy8kx623bzilg80hq1dd0ydwspdxk3ji5gl021w7k9d")))

(define-public crate-protocol_v3_macro-0.1.2 (c (n "protocol_v3_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nadzq013i95f26cvg91d5lz923366nf3m9syxyqiym2qf1y99vk")))

