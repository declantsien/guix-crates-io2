(define-module (crates-io pr ot proto_shim) #:use-module (crates-io))

(define-public crate-proto_shim-0.1.0 (c (n "proto_shim") (v "0.1.0") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1qmf4f2yqv9vwi4hpdqazbipp978jp3g5ikyc9kyv8hl2j5bal3v")))

(define-public crate-proto_shim-0.2.0 (c (n "proto_shim") (v "0.2.0") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0bya239q2sfa0fqkpmnxl660m6j19p081bc7g43rav29w4b2ynw9")))

(define-public crate-proto_shim-0.2.1 (c (n "proto_shim") (v "0.2.1") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1lxgxpmf5jxh72v8yvmp7czljkf7byshifdqkaidh92crdladih7")))

(define-public crate-proto_shim-0.2.2 (c (n "proto_shim") (v "0.2.2") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "06crkygsls4b72695f3kcc8p025m37cqh967bx4cgv8g7gm2y3qi")))

(define-public crate-proto_shim-0.2.3 (c (n "proto_shim") (v "0.2.3") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1xv3c67b57vchsnp6qz9qnaxg3wmm8vyfyfi3fqybaxk2nxqqa72")))

(define-public crate-proto_shim-0.2.4 (c (n "proto_shim") (v "0.2.4") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1b3grn0qnywnd3706r4h2hmrzc3zpkh9jrn01bzc81qf09bhxl6z")))

(define-public crate-proto_shim-0.2.5 (c (n "proto_shim") (v "0.2.5") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "12qjwy2369q4vl4b68gpv9a57ga17xir8gak1q7jmv14vnraa8vq")))

(define-public crate-proto_shim-0.2.6 (c (n "proto_shim") (v "0.2.6") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1g50ddasga2dqpkzxa5xmsi5kazxpvlbjizhc3pi3sc1dh6ww64q")))

(define-public crate-proto_shim-0.2.7 (c (n "proto_shim") (v "0.2.7") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0yl9s6nyr5nya85cpgqahj24ba40f92yiw2j01cispdzq7kv2r90")))

(define-public crate-proto_shim-0.2.8 (c (n "proto_shim") (v "0.2.8") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1ayx9j3pwk4ygrhmpi9d934yjnl2xhssff0pzcn0hh18viwqdp9s")))

(define-public crate-proto_shim-0.2.9 (c (n "proto_shim") (v "0.2.9") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1dkkdraq5hs7x8pdrm2rw2mi58xnyjcp9j02vc8za4kjxd9hlm9r")))

(define-public crate-proto_shim-0.3.0 (c (n "proto_shim") (v "0.3.0") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "10wpdvxa0a8lw9lgcl9dmm2jiwf3pphp318jjf9ddm1b7crnmsv9")))

(define-public crate-proto_shim-0.3.1 (c (n "proto_shim") (v "0.3.1") (d (list (d (n "command-group") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0m2rjgaafsvqars3nna0ggmnvpad2l1hxig2x9bz9jc9rb2cv6rb")))

