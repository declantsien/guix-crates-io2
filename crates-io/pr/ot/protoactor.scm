(define-module (crates-io pr ot protoactor) #:use-module (crates-io))

(define-public crate-protoactor-0.1.0 (c (n "protoactor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "protoactor-cluster") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "protoactor-persistence") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "protoactor-remote") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18lspckmx781g48sw7w44nizz5azj0lk635231sj7djpxr4ii5al") (f (quote (("remote" "protoactor-remote") ("persistence" "protoactor-persistence") ("default") ("cluster" "protoactor-remote" "protoactor-cluster"))))))

