(define-module (crates-io pr ot protofish) #:use-module (crates-io))

(define-public crate-protofish-0.1.0 (c (n "protofish") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "05jjvwxp1bi7kkp00iiqf1f7acmkz7fsv1c553y64c4pfr6hh89i")))

(define-public crate-protofish-0.1.1 (c (n "protofish") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1hmcin7insap5k7b21zkrsx020r1f82qg1q045jby8p8spa2r2ki")))

(define-public crate-protofish-0.2.0 (c (n "protofish") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0sxqyv6pazic8bbmssc37bx23b27p21rxisx91avxivxla4ra3qc")))

(define-public crate-protofish-0.2.1 (c (n "protofish") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0hpnh3xi3ynh1jzvp0b3raqvxzpjvnfav2dngy13cy5qsmmq0fmb")))

(define-public crate-protofish-0.2.2 (c (n "protofish") (v "0.2.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1vkycg47fc3qbrv8wcyhs31ci023g8z4jjya5avnhjad4d1rz1r3")))

(define-public crate-protofish-0.2.3 (c (n "protofish") (v "0.2.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1zd32rs603jpb0zzdj87n7krpcp6nns6i9yg008fnx6vyi6b748j")))

(define-public crate-protofish-0.2.4 (c (n "protofish") (v "0.2.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0dzpwc4mp6kpmb932ihdhb17xpbhn7camh5i02cn9ya5h2lg0b8k")))

(define-public crate-protofish-0.2.5 (c (n "protofish") (v "0.2.5") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0n5pgna0mqrr8fwgxbz6abqd3vgw1fs2h32gzlfkai449dzj4407")))

(define-public crate-protofish-0.3.0 (c (n "protofish") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1kah008cw0l264dn5gp86p5xhgj1n9248ya2ijqycjl7avfgsy1r")))

(define-public crate-protofish-0.3.1 (c (n "protofish") (v "0.3.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0hgkviqw211kmzah8d0qlz07ydhy5ibrrd659cgnsmmwqc74a7wn")))

(define-public crate-protofish-0.4.0 (c (n "protofish") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0l17sh1qb88rlmph34p2q9l45wjkx723ww06g6hglbay1n1vd1mg")))

(define-public crate-protofish-0.5.0 (c (n "protofish") (v "0.5.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0ig15512sq1giyz10pgw1nwxjid9rajbzl6l700j3k4646p9xwj3")))

(define-public crate-protofish-0.5.1 (c (n "protofish") (v "0.5.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "17k5jy89v3dy0qg7vmc38maanbz3xfz638nq25zmvg4pjfln7y97")))

(define-public crate-protofish-0.5.2 (c (n "protofish") (v "0.5.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0mrjbam6v56v67nchl4annyq7hfjz2qzsfsi03y1pqh42mvzr9g2")))

