(define-module (crates-io pr ot protobuf-parser) #:use-module (crates-io))

(define-public crate-protobuf-parser-0.1.0 (c (n "protobuf-parser") (v "0.1.0") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1w793pm32al3p42l0dppls7pchcijxpy54x8wrarz6fb20g6ggz9")))

(define-public crate-protobuf-parser-0.1.1 (c (n "protobuf-parser") (v "0.1.1") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1vd679fhf1q1plh2i2bfgvrcvvfvk8x07sy61jqjzvvzmcj5pxsz")))

(define-public crate-protobuf-parser-0.1.2 (c (n "protobuf-parser") (v "0.1.2") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "07ikqb95yq3q83fqgqibd1ll6xydx8dl4p2gq6ksaz35y4qdk2c7")))

(define-public crate-protobuf-parser-0.1.3 (c (n "protobuf-parser") (v "0.1.3") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0q9ln1gfcqi4hn559hqwbhmsjp8jwmbdgrjd061l95f50682h1l9")))

