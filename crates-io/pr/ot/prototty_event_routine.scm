(define-module (crates-io pr ot prototty_event_routine) #:use-module (crates-io))

(define-public crate-prototty_event_routine-0.28.0 (c (n "prototty_event_routine") (v "0.28.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prototty_app") (r "^0.28") (d #t) (k 0)) (d (n "prototty_input") (r "^0.28") (d #t) (k 0)) (d (n "prototty_render") (r "^0.28") (d #t) (k 0)))) (h "06vrf3hras36rlfzpxnvv0h74j2hfss16zav2vxqcrfmicmky20q") (f (quote (("serialize" "prototty_render/serialize" "prototty_input/serialize"))))))

(define-public crate-prototty_event_routine-0.29.0 (c (n "prototty_event_routine") (v "0.29.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prototty_app") (r "^0.29") (d #t) (k 0)) (d (n "prototty_input") (r "^0.29") (d #t) (k 0)) (d (n "prototty_render") (r "^0.29") (d #t) (k 0)))) (h "04a5vmc690gwbsmcdicahy8wy005ds3hlv89afpkz3m2vpfkk7fk") (f (quote (("serialize" "prototty_render/serialize" "prototty_input/serialize"))))))

