(define-module (crates-io pr ot proton-call) #:use-module (crates-io))

(define-public crate-proton-call-2.3.0 (c (n "proton-call") (v "2.3.0") (d (list (d (n "lliw") (r "^0.1.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10ml3fx800apci0j63jp0z0b78syyzqr6g5lj5fh1ydgzcw4zgs6")))

(define-public crate-proton-call-3.0.1 (c (n "proton-call") (v "3.0.1") (d (list (d (n "jargon-args") (r "^0.2.3") (d #t) (k 0)) (d (n "lliw") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1md2gmg19phigw53zpi3pn5akf9k73bn1mvb4rccl8792r71z0mv")))

(define-public crate-proton-call-3.1.0 (c (n "proton-call") (v "3.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "jargon-args") (r "^0.2.5") (d #t) (k 0)) (d (n "lliw") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0mpy80mby5qihxd6czkd4v3nny2nbpx6dm6lvvw6h6aq20fi5wsm")))

(define-public crate-proton-call-3.1.1 (c (n "proton-call") (v "3.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "jargon-args") (r "^0.2.5") (d #t) (k 0)) (d (n "lliw") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "01a1xkkr6qrxnym5a2180yhdclgwl9ig5nk1svrz2l8p8570lahs")))

(define-public crate-proton-call-3.1.2 (c (n "proton-call") (v "3.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "jargon-args") (r "^0.2.5") (d #t) (k 0)) (d (n "lliw") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0i8ma0yn5dbfpa6yw32r8zqw99jj8ilv1hsm3bw0hlw3rr25pxim")))

