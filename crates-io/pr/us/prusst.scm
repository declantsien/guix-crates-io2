(define-module (crates-io pr us prusst) #:use-module (crates-io))

(define-public crate-prusst-0.1.0 (c (n "prusst") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a5ilr40m0zrcvj9bp8nqh45049996n6rj9mnjv3iaa0hhffidpa")))

(define-public crate-prusst-1.0.0 (c (n "prusst") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1li2dfnxnx9ajlin3shxwdvaifja7vpwbq189823mkv0n9yw10yx")))

