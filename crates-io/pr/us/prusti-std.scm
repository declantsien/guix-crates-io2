(define-module (crates-io pr us prusti-std) #:use-module (crates-io))

(define-public crate-prusti-std-0.1.0 (c (n "prusti-std") (v "0.1.0") (d (list (d (n "prusti-contracts") (r "^0.1.0") (d #t) (k 0)))) (h "1fiyn959vqlplzcw80hwd5bm8qvgnj0by775j6p7n2mxf2wpza8l") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.1 (c (n "prusti-std") (v "0.1.1") (d (list (d (n "prusti-contracts") (r "^0.1.1") (d #t) (k 0)))) (h "1l51igj37l5favabhs6lgkxrc7iiq5sb0ar8zar9svhzvvay1aq5") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.3 (c (n "prusti-std") (v "0.1.3") (d (list (d (n "prusti-contracts") (r "^0.1.3") (d #t) (k 0)))) (h "02s7bg251wm65qzzb55rqgj7x1pj9gqb3x9j963vjkk4kck6hlii") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.4 (c (n "prusti-std") (v "0.1.4") (d (list (d (n "prusti-contracts") (r "^0.1.4") (d #t) (k 0)))) (h "0vyggxhvwhdx23blaz5p6lisz29mbqsazsqnvc92029yblbiamnk") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.5 (c (n "prusti-std") (v "0.1.5") (d (list (d (n "prusti-contracts") (r "^0.1.5") (d #t) (k 0)))) (h "1gzgv6ngxm8k6wd1z6b2nxvrxfksalgif0qkvijpqrwlna30rij6") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.6 (c (n "prusti-std") (v "0.1.6") (d (list (d (n "prusti-contracts") (r "^0.1.6") (d #t) (k 0)))) (h "0clf6w0ql162ydxkg26qi4bhsfvinfm9d5fy1rbcn5zcxh812xd1") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.8 (c (n "prusti-std") (v "0.1.8") (d (list (d (n "prusti-contracts") (r "^0.1.8") (d #t) (k 0)))) (h "0y8rgyl003fkhdavh8vycp2vpm9d4ql8n6m4ma71sm1wyrjpi25q") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.9 (c (n "prusti-std") (v "0.1.9") (d (list (d (n "prusti-contracts") (r "^0.1.9") (d #t) (k 0)))) (h "1prnhxsj84l32s303bc74lrdjz3vcw6g443h9pqb1xyray4fp6fz") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.1.10 (c (n "prusti-std") (v "0.1.10") (d (list (d (n "prusti-contracts") (r "^0.1.10") (d #t) (k 0)))) (h "199p34i3ga6951l2fqjmfkg2lf01ql4bxi04z4brjvh4bj93wj5a") (f (quote (("prusti" "prusti-contracts/prusti"))))))

(define-public crate-prusti-std-0.2.0 (c (n "prusti-std") (v "0.2.0") (d (list (d (n "prusti-contracts") (r "^0.2.0") (d #t) (k 0)))) (h "0y37w5xbmlxz8526z1sp3fx1smpid8pl924pirlcqddqhkr3xypx") (f (quote (("prusti" "prusti-contracts/prusti"))))))

