(define-module (crates-io pr us prusti-contracts) #:use-module (crates-io))

(define-public crate-prusti-contracts-0.1.0 (c (n "prusti-contracts") (v "0.1.0") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rs1x8hvmhlabxh61p74qd6fpi5xb0z2p982pbzr7hbx5y86h38p") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.1 (c (n "prusti-contracts") (v "0.1.1") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1cwbph2nwsvgk03blipmqzqn1044krzaaphc42lqmwqlwwp537f0") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.2 (c (n "prusti-contracts") (v "0.1.2") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xfl1984f8g1f73k4pippcqsw7605zfzrdyalqj5d7cznddlaxi1") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.3 (c (n "prusti-contracts") (v "0.1.3") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0sdf8rwvjblcc30v4mbcgb3ijrji4dp0rr2qasmxqn4pwcas1ijf") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.4 (c (n "prusti-contracts") (v "0.1.4") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0h69d4a130p2zrq4niacbz8j15r13lp2bpd6hq3krjnfia1blm8a") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.5 (c (n "prusti-contracts") (v "0.1.5") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y1n3cypb60z44rf4kd66v0yzp23q69kbrz50vbqxs1rkkh9k8gz") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.6 (c (n "prusti-contracts") (v "0.1.6") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "084fcsp3616b20h7z8hna1a47js64w445d8n8njdvh5zj20s4xcp") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.8 (c (n "prusti-contracts") (v "0.1.8") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.8") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1kf2ydy78vr063bcd9181l5ml2r10i29vmmyxia8p7174fv1hgma") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.9 (c (n "prusti-contracts") (v "0.1.9") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1scg77kmwvq0s42lv7f3hq2kzh34f6rjfxrhffkqhsnbyny1p3xb") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.1.10 (c (n "prusti-contracts") (v "0.1.10") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.1.10") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zj8y7rzd2i9bkcz9zswl16qskz51xx117xlgcbmzqg0664inl44") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

(define-public crate-prusti-contracts-0.2.0 (c (n "prusti-contracts") (v "0.2.0") (d (list (d (n "prusti-contracts-proc-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hrhdi361hlsld5q5zmsdxn71wjqz5z8hv91bk4c9vnxjn362vjw") (f (quote (("prusti" "prusti-contracts-proc-macros/prusti"))))))

