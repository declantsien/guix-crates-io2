(define-module (crates-io pr us prusti-contracts-proc-macros) #:use-module (crates-io))

(define-public crate-prusti-contracts-proc-macros-0.1.0 (c (n "prusti-contracts-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0l5yba9w0l6acyyb3a3vjyq0f9l02x86cw4hiw70yq3llhh033xg") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.1 (c (n "prusti-contracts-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0f8ca524k6hf4nzva49ciaig0la16i086kx6n825w1zfn25m4y1z") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.2 (c (n "prusti-contracts-proc-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1z7jbil0iahmkp3wcxbk9f9c7nccwv9w6mkf3ggcgcc7vvg6wsbn") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.3 (c (n "prusti-contracts-proc-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "19gwzfn27k7nzzzgipmd10b1hs6a9dv8y3knzaa06nvpjxlrnzjc") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.4 (c (n "prusti-contracts-proc-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0g84v7snl2zh8ai105rx2s3ia9qqzp30gjwrgjdj7wpjk0j02kxp") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.5 (c (n "prusti-contracts-proc-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1dd1fj4rnx6jyc7fmvk9qsd9wvsrwkxv7hp4x6wg4n35zf160bvj") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.6 (c (n "prusti-contracts-proc-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "1p8x3l7rmn3nbhsh7yzbr1lkxkysh3nyxk5bgcprqc62kkhljnnw") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.8 (c (n "prusti-contracts-proc-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "17mfirn6nzijclgn85a11jb5cb7m2hl8yyjplyrisas756ikwdw4") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.9 (c (n "prusti-contracts-proc-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0w6003v7z0lxgb5xni4rv7say4myngvb1d88xdr8k14s63r7hmzs") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.1.10 (c (n "prusti-contracts-proc-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "12kw9nqm6m6mkw1shkivqr51s2gd5x711rf2vwxkdj2v8zphl4yh") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

(define-public crate-prusti-contracts-proc-macros-0.2.0 (c (n "prusti-contracts-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prusti-specs") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "17m9jvkncbpm8rh3if61cf589l5bqdiq15spn22ynzrf7nlihkcz") (s 2) (e (quote (("prusti" "dep:prusti-specs" "dep:proc-macro2"))))))

