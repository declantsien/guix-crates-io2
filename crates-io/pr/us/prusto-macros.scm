(define-module (crates-io pr us prusto-macros) #:use-module (crates-io))

(define-public crate-prusto-macros-0.1.0 (c (n "prusto-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "extra-traits" "full"))) (d #t) (k 0)))) (h "0fp359sjavs6nl5r09av8z8gjxd4k40hnwn9a08i7hp8c4ccjvld")))

(define-public crate-prusto-macros-0.2.0 (c (n "prusto-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "extra-traits" "full"))) (d #t) (k 0)))) (h "1y4wkz4vbpxar2lgbyfnj9ik88f5g72mwia6r1hsj3g883n776kj")))

