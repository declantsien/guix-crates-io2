(define-module (crates-io pr us prust_core) #:use-module (crates-io))

(define-public crate-prust_core-0.1.0 (c (n "prust_core") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "09hjpg2rrigr9yn761a6g4nkd75bjhk75wn0lahp0291vpxmrxhl")))

(define-public crate-prust_core-0.1.1 (c (n "prust_core") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "05ml1bgwyci13m5bbdf1khi40ijhnaf9l0c68afvf4h7aad8pvqw")))

(define-public crate-prust_core-0.1.2 (c (n "prust_core") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0nrjmksxqihnr5d3cd29qv979hfh8izn61510r3058zsy5p8m3yz")))

(define-public crate-prust_core-0.1.3 (c (n "prust_core") (v "0.1.3") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0j1hfac6dn7x76n2f3xgg813v5015y0gs2kx3v60ay5hmiy8r5v4")))

