(define-module (crates-io pr ng prng) #:use-module (crates-io))

(define-public crate-prng-0.1.0 (c (n "prng") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rng_trait") (r "^0.1") (d #t) (k 0)))) (h "1mym8p3jwrmnqlbbwkwm5nbvq8h7k4cxg5fw29vmgl3w2i0g79ih")))

(define-public crate-prng-0.1.1 (c (n "prng") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.7") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rng_trait") (r "^0.1") (d #t) (k 0)))) (h "1ikcjzyql72jxa5x6g9byy5vs8zvyhl024zyzm8xfi8psbkw48x9")))

