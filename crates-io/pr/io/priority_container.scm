(define-module (crates-io pr io priority_container) #:use-module (crates-io))

(define-public crate-priority_container-0.1.0 (c (n "priority_container") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q8i9lq3q83xa8cj42343axzkhxa6sbjafd7b8ifpc5yds40qjfm")))

(define-public crate-priority_container-0.1.1 (c (n "priority_container") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jybylsm8217azg66ncz2iv21x9wfl7mkyjfm39p07m972wnp264")))

