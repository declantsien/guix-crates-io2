(define-module (crates-io pr io priority-matrix) #:use-module (crates-io))

(define-public crate-priority-matrix-0.1.0 (c (n "priority-matrix") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.1") (d #t) (k 0)))) (h "1whrrgbdf39688njz26s486847pz978igldz9d64l4rya060djyr")))

(define-public crate-priority-matrix-0.2.0 (c (n "priority-matrix") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.1") (d #t) (k 0)))) (h "00vbpnl7dz3fqvys7h33a3v7z787h6hdbcjrh4ny0f743mdqfnxk")))

