(define-module (crates-io pr io priomutex) #:use-module (crates-io))

(define-public crate-priomutex-0.1.0 (c (n "priomutex") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1qa1v4rwwa14cglzhn3qmd3j9q4aph8wq4ypv7kzj8grbgwb28zn")))

(define-public crate-priomutex-0.1.1 (c (n "priomutex") (v "0.1.1") (d (list (d (n "easybench") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1gi4k1xb51685dv1c9pk93q91758dnkfd3g07v9184lfd50ij7x8")))

(define-public crate-priomutex-0.2.0 (c (n "priomutex") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0d6rd9s35yvz5c5hd1z2878hdjqvd1z3i0j0gslrljgvylakccdg")))

(define-public crate-priomutex-0.2.1 (c (n "priomutex") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0hdcgjljqgb8l6qnrbw8q38gqsqnga6x9nkfzm1bs138fylr32hn")))

(define-public crate-priomutex-0.2.2 (c (n "priomutex") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1rgcbs60dfkzj4jpk70h1kwylvpkdijbvg5npd8h5dpk977a57l6")))

(define-public crate-priomutex-0.2.3 (c (n "priomutex") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1lxlcbgja04h341s885w3j8jh0d9w1saadnagf6654i746ydg8mh")))

(define-public crate-priomutex-0.3.0 (c (n "priomutex") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1mjb4cghx6mpmhm05myac6lj7csm4x5kignhz4hv59d6i66ha06a")))

(define-public crate-priomutex-0.3.1 (c (n "priomutex") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "081zl0x1yd8pvcams338vsknmfvrri1208vc6i7hikr0n86z7i9i")))

