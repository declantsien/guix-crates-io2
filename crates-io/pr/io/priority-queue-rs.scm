(define-module (crates-io pr io priority-queue-rs) #:use-module (crates-io))

(define-public crate-priority-queue-rs-0.1.0 (c (n "priority-queue-rs") (v "0.1.0") (h "0k7wxj45sxh644i8ayaxbwll36zlpywcvjmhfxyiid1hkzhjikw7")))

(define-public crate-priority-queue-rs-0.1.1 (c (n "priority-queue-rs") (v "0.1.1") (h "08fmijvmk6qcygn82r65dz499ysvva6hlkajgzdsk2l2981shdv6")))

(define-public crate-priority-queue-rs-0.1.2 (c (n "priority-queue-rs") (v "0.1.2") (h "0rm9p5is2cim7vvhnz5vffqr55d9m0m1xzd06pf4lr3ni0w9xr5r")))

(define-public crate-priority-queue-rs-0.1.3 (c (n "priority-queue-rs") (v "0.1.3") (h "1yyvirdh1qfpifyf6m7npr1sdwhk76iwm15jyg5x8j1mind77zwy")))

(define-public crate-priority-queue-rs-0.1.4 (c (n "priority-queue-rs") (v "0.1.4") (h "1h7ddbcnnm46vxyj42f1084n78jaqszwpy1kn6vn4f655fhn2bx9")))

(define-public crate-priority-queue-rs-0.1.5 (c (n "priority-queue-rs") (v "0.1.5") (h "1326sya1b56n7n3clibvp4p69d0r6cnwf6q0xaq44rgxxfa82l7z")))

(define-public crate-priority-queue-rs-0.1.6 (c (n "priority-queue-rs") (v "0.1.6") (h "0g11kdxfba7ghawbbnhahdnzkii42xz7z0aj9nii90nmgmgandmk")))

(define-public crate-priority-queue-rs-0.1.7 (c (n "priority-queue-rs") (v "0.1.7") (h "1d1rnw397saa2sj8riwwy73sga8k8iphnldqvz6nfrcia8v9syq0")))

(define-public crate-priority-queue-rs-0.1.8 (c (n "priority-queue-rs") (v "0.1.8") (h "08h6r50zqllkdz12m4hz9b9l7f7j79kljvizxvxsphq5pp9i5518")))

(define-public crate-priority-queue-rs-0.1.9 (c (n "priority-queue-rs") (v "0.1.9") (h "123hcbfml9bqw4a5756r4s796q7mxh419knh59znkdzi37ss203b")))

(define-public crate-priority-queue-rs-0.1.10 (c (n "priority-queue-rs") (v "0.1.10") (h "1vfb4zkvp5jfx7qx84jpx68ypr10r2bkp4rniwmm9fg5zv07jsf4")))

(define-public crate-priority-queue-rs-0.1.11 (c (n "priority-queue-rs") (v "0.1.11") (h "1j5hd4mi9y0fl2mm3dvdn5wvinchi6y8ccgvq2fmicbac2gychz4")))

(define-public crate-priority-queue-rs-0.1.12 (c (n "priority-queue-rs") (v "0.1.12") (h "17y18b9n1jlbrps0kmpfl3c76v2l7jsdd56vzz7blsw5m3jdwa4g")))

(define-public crate-priority-queue-rs-0.1.16 (c (n "priority-queue-rs") (v "0.1.16") (h "10jhi6zswhfnli8k4rx5i6j9wajvjb2xv54wl9bjz18rn3zk308c")))

(define-public crate-priority-queue-rs-0.1.17 (c (n "priority-queue-rs") (v "0.1.17") (h "0092qm8mj4krgbyz1wbjq9prv9x42swwjfd37biakb90r70ky4p5")))

(define-public crate-priority-queue-rs-0.1.18 (c (n "priority-queue-rs") (v "0.1.18") (h "11kjvbq5pgmc9rvz4dr8ar73jdvz1dc02f45gsclv6qzvmnn800j")))

(define-public crate-priority-queue-rs-0.1.19 (c (n "priority-queue-rs") (v "0.1.19") (h "13qiqdicpww34fvwas1q70fkcjip7mw37ri1c3r85s18bcwrnhg1")))

(define-public crate-priority-queue-rs-0.1.20 (c (n "priority-queue-rs") (v "0.1.20") (h "1bvzsdmwjchw414gizjbir1g5v3yl4pm8x16v7rxgl1vajx98jkl")))

(define-public crate-priority-queue-rs-0.1.21 (c (n "priority-queue-rs") (v "0.1.21") (h "1lmkjjca50f9ydppzgs0yiv6zq47cl6wc8vvivap5891h3jmhxa3")))

(define-public crate-priority-queue-rs-0.1.22 (c (n "priority-queue-rs") (v "0.1.22") (h "05kcbnr67l6w0xvkbah679509il5q1kx1f7kpalnph8f9n6gxp03")))

(define-public crate-priority-queue-rs-0.1.23 (c (n "priority-queue-rs") (v "0.1.23") (h "1k18sq9dq1gnjpbcx60ygqxizpxk6257wv3ygc2zxx9wzj9iskyv")))

(define-public crate-priority-queue-rs-0.1.24 (c (n "priority-queue-rs") (v "0.1.24") (h "08nkmd5rbfx9qrqsp0n2jg29srylabfb5ggvqzbqkjahknx32yf7")))

(define-public crate-priority-queue-rs-0.1.25 (c (n "priority-queue-rs") (v "0.1.25") (h "0rp02iarjfv44mrribhl9mjan0a6szh5z2f4fnv2scqd8d53nzx8")))

(define-public crate-priority-queue-rs-0.1.26 (c (n "priority-queue-rs") (v "0.1.26") (h "1m6p7cc7vwc1pfjslkf121dn2bgpjq92c9l48jdysl4x0f97dlnh")))

