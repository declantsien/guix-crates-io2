(define-module (crates-io pr io prio-graph) #:use-module (crates-io))

(define-public crate-prio-graph-0.1.0 (c (n "prio-graph") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "06whgn7d1hvj4h9v9x740m301yz3sxi9h7gmvg8b8089raljzpbq")))

(define-public crate-prio-graph-0.2.0 (c (n "prio-graph") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0p1vy2j0mvpd5a7jd0xx2zi0a974d2nvdg69czrgczqmh3gr284m")))

(define-public crate-prio-graph-0.2.1 (c (n "prio-graph") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "15v0i5gmsr00cgwn619b3208ci7d5cqglpplk93s8rkhlmfag4k4")))

