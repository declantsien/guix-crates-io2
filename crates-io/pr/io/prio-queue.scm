(define-module (crates-io pr io prio-queue) #:use-module (crates-io))

(define-public crate-prio-queue-0.1.0 (c (n "prio-queue") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0i3c402mkqnx6p3cx25ml86zbz798sq0wzr3lh0dwk4zbmz7q1dr")))

(define-public crate-prio-queue-0.1.1 (c (n "prio-queue") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "11c9bmdymk3hgnnxmlhprs9ixr5nvq4nfz33w7ksfwr181xhyj1g")))

