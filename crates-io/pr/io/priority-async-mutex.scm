(define-module (crates-io pr io priority-async-mutex) #:use-module (crates-io))

(define-public crate-priority-async-mutex-0.1.0 (c (n "priority-async-mutex") (v "0.1.0") (d (list (d (n "oneshot") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "17kqwpm1640qzaz1ng42vsi7dz3h6rgzhxsdw7vmjj0aafjiymfb")))

(define-public crate-priority-async-mutex-0.1.1 (c (n "priority-async-mutex") (v "0.1.1") (d (list (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "oneshot") (r "^0.1.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)))) (h "1pzm4wli2ckflg7iyy0j8xma1bz0vqmrr95r73zcsm8pd93gqwk0")))

