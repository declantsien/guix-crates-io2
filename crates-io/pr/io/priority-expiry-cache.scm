(define-module (crates-io pr io priority-expiry-cache) #:use-module (crates-io))

(define-public crate-priority-expiry-cache-0.1.0 (c (n "priority-expiry-cache") (v "0.1.0") (h "1l0pbhzi1c1bdf08xg7j875bl92rqimaj7rdn2mck5nnj5bf82br") (y #t)))

(define-public crate-priority-expiry-cache-0.1.1 (c (n "priority-expiry-cache") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)))) (h "134rl9325h2mmw1x2sqykzf5x6wi5smrvwz6rvm3lk29z2y1mh25") (y #t)))

(define-public crate-priority-expiry-cache-0.2.0 (c (n "priority-expiry-cache") (v "0.2.0") (d (list (d (n "lru") (r "^0.12.1") (d #t) (k 0)))) (h "1kkxc9ihc9qadl95cxj2hxjgrp5hyz63j7wzfkgv6cvf7q2nfq8v") (y #t)))

(define-public crate-priority-expiry-cache-0.2.1 (c (n "priority-expiry-cache") (v "0.2.1") (d (list (d (n "lru") (r "^0.12.1") (d #t) (k 0)))) (h "0ca17asnhb63dw9hi5rc06h77rq9vyv1wb0v6dk6rswm074dsad0")))

