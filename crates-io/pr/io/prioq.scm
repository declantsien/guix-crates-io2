(define-module (crates-io pr io prioq) #:use-module (crates-io))

(define-public crate-prioq-0.1.0 (c (n "prioq") (v "0.1.0") (h "1cf6fy7ibfmmrclaqyiya9ss6llq7fh0750a357mjb90h3j8jbyv")))

(define-public crate-prioq-0.1.1 (c (n "prioq") (v "0.1.1") (h "18kjghsc4pi55d5x51904cza6f0vi1pr5my0a94xyxy5lf379i22")))

(define-public crate-prioq-0.1.2 (c (n "prioq") (v "0.1.2") (h "143mmb3wryqdnk1vvxz28xrjbbhdm4amsw9jxvghica5kqhxzggs")))

