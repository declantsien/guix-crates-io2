(define-module (crates-io pr ed predicate-macros) #:use-module (crates-io))

(define-public crate-predicate-macros-0.1.0 (c (n "predicate-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "140xvwhq7z78fc7vcklw0nk47fkrj9gxlpwbxh2l7j6vn98h6aly")))

