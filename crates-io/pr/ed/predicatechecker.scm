(define-module (crates-io pr ed predicatechecker) #:use-module (crates-io))

(define-public crate-predicatechecker-0.5.1 (c (n "predicatechecker") (v "0.5.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "18y8v30wswgvfjvd76sr9996dh2r2bksgh6b00jqh0hvysldby9k")))

(define-public crate-predicatechecker-0.5.2 (c (n "predicatechecker") (v "0.5.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "02ilsby8drg72rx41pcgn8wsmxg7hmxfn2dskbp3ac92k88p0rfc")))

