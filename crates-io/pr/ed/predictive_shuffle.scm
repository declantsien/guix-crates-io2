(define-module (crates-io pr ed predictive_shuffle) #:use-module (crates-io))

(define-public crate-predictive_shuffle-0.1.0 (c (n "predictive_shuffle") (v "0.1.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "09nklkv18fsbv2m4sgwgj4hl6l6ay1yxh9ca939nhf89v4kh686d")))

