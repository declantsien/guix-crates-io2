(define-module (crates-io pr ed predicator) #:use-module (crates-io))

(define-public crate-predicator-0.0.1 (c (n "predicator") (v "0.0.1") (d (list (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "40.0.*") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)))) (h "1jgbv1li3ddkn0dlwj2cyn6v71mxlcpy1swcfxzl3g9qbllsiw5p") (y #t)))

(define-public crate-predicator-0.0.2 (c (n "predicator") (v "0.0.2") (d (list (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "40.0.*") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)) (d (n "tempfile") (r "2.1.*") (d #t) (k 0)))) (h "16zcf360kijbcjdx7i7r00jb41rdxapv4nmpl002ziaav4sb3ax6") (y #t)))

(define-public crate-predicator-0.0.3 (c (n "predicator") (v "0.0.3") (d (list (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "40.0.*") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)) (d (n "tempfile") (r "2.1.*") (d #t) (k 0)))) (h "1gbmdx9aip3ikry4nlpid2gfw3ni40iq4h4gc95gbk1y4cc7favn")))

(define-public crate-predicator-0.0.5 (c (n "predicator") (v "0.0.5") (d (list (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "40.0.*") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.17") (d #t) (k 0)))) (h "1cvawan9v9rhsf9g9y4q2ma9l6vl9b86dgnh8jnjfvj5lrp0k7vb")))

