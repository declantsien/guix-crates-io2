(define-module (crates-io pr ed predicates-core) #:use-module (crates-io))

(define-public crate-predicates-core-0.9.0 (c (n "predicates-core") (v "0.9.0") (h "1ig5wi3j2faxss6kshv5xdwnchiwbkq2fgx6v962mh6ij31hpy45")))

(define-public crate-predicates-core-1.0.0 (c (n "predicates-core") (v "1.0.0") (h "0y3ingf2i4xx7r61f1a8wizs57j8hh32hylyjbw9ymcj7qx5q1q6")))

(define-public crate-predicates-core-1.0.1 (c (n "predicates-core") (v "1.0.1") (h "1p04849drkg3n09qv9nfcdmw1ixkpf13lzlcqlllwn4kyymbwggv")))

(define-public crate-predicates-core-1.0.2 (c (n "predicates-core") (v "1.0.2") (h "0l947jviipblyaqsp3p5mvsqq421bg0nxp7mhnm4jpmp4qrmmqsp")))

(define-public crate-predicates-core-1.0.3 (c (n "predicates-core") (v "1.0.3") (h "1jrzczk9mylrf3rkwi3iiw4fzs59w2armhyfbxh1ngjin642676s")))

(define-public crate-predicates-core-1.0.4 (c (n "predicates-core") (v "1.0.4") (h "0mkb07xvpmayihb88c42qmwljymja0b5gjjmk478cwnqhmai5rx6") (r "1.60.0")))

(define-public crate-predicates-core-1.0.5 (c (n "predicates-core") (v "1.0.5") (h "18ni79rrmnqvxsasq3n7qa86bylm2q0rj0pmbgycdls209cq7y3j") (r "1.60.0")))

(define-public crate-predicates-core-1.0.6 (c (n "predicates-core") (v "1.0.6") (h "0x7ij95n63mhgkyrb7hly5ngm41mwfsassfvnjz7lbk10wk0755p") (r "1.64.0")))

