(define-module (crates-io pr ed predicates-tree) #:use-module (crates-io))

(define-public crate-predicates-tree-0.9.0 (c (n "predicates-tree") (v "0.9.0") (d (list (d (n "predicates-core") (r "^0.9") (d #t) (k 0)) (d (n "treeline") (r "^0.1") (d #t) (k 0)))) (h "1ga0yyfmqbwi28naxlr6cvpmiig0qnwx5adc858hmjxxh6dxz1if")))

(define-public crate-predicates-tree-1.0.0 (c (n "predicates-tree") (v "1.0.0") (d (list (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "treeline") (r "^0.1") (d #t) (k 0)))) (h "090148qjilm2c722l873z7g31fhzj5j4qhd2xiv8mcqkj22w8qwf")))

(define-public crate-predicates-tree-1.0.1 (c (n "predicates-tree") (v "1.0.1") (d (list (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "treeline") (r "^0.1") (d #t) (k 0)))) (h "0wxbms3agq8bgax50zwjxa72rm9cxn08q53bq0swp4z8isc5vsdf")))

(define-public crate-predicates-tree-1.0.2 (c (n "predicates-tree") (v "1.0.2") (d (list (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "treeline") (r "^0.1") (d #t) (k 0)))) (h "1wliadgjnl47b4hyp5j4mgjscq58v5gy31gba6a408apbqkm7x8m")))

(define-public crate-predicates-tree-1.0.3 (c (n "predicates-tree") (v "1.0.3") (d (list (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "treeline") (r "^0.1") (d #t) (k 0)))) (h "0ggaqs110d28adlgyk97p6r529srps9dryrf6p4hc0hk2k80zpfp")))

(define-public crate-predicates-tree-1.0.4 (c (n "predicates-tree") (v "1.0.4") (d (list (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.2") (d #t) (k 0)))) (h "1xsx8j9x2s2dgmn1hnshaa7zv52y5c1l0bsak3ijlwsvj3i7p31k")))

(define-public crate-predicates-tree-1.0.5 (c (n "predicates-tree") (v "1.0.5") (d (list (d (n "predicates") (r "^2.0") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.2") (d #t) (k 0)))) (h "0cn02rg1nm83x9h9qmf00whmb7vak9nshhrndmna682hw9nxx1jd")))

(define-public crate-predicates-tree-1.0.6 (c (n "predicates-tree") (v "1.0.6") (d (list (d (n "predicates") (r "^2.1") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4") (d #t) (k 0)))) (h "19a3xs9rlcjr29fwdyjz7yjq7237nvrypprygjy9q4qyssl7ygxd") (r "1.60.0")))

(define-public crate-predicates-tree-1.0.7 (c (n "predicates-tree") (v "1.0.7") (d (list (d (n "predicates") (r "^2.1") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4") (d #t) (k 0)))) (h "03c546vdhvc99swrf4haw5v842wqxqqj2bbjxbvannjhc4c59zsl") (r "1.60.0")))

(define-public crate-predicates-tree-1.0.8 (c (n "predicates-tree") (v "1.0.8") (d (list (d (n "predicates") (r "^3.0") (f (quote ("color"))) (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4.1") (d #t) (k 0)))) (h "15lf6zgjgb3gcz1hbgknn44r9iv5nxkb8gi6gx7af9k9rnhpiz9c") (r "1.64.0")))

(define-public crate-predicates-tree-1.0.9 (c (n "predicates-tree") (v "1.0.9") (d (list (d (n "predicates") (r "^3.0") (f (quote ("color"))) (d #t) (k 2)) (d (n "predicates-core") (r "^1.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4.1") (d #t) (k 0)))) (h "1kyfq3r0s2vg94a9r59n7ar5gv66zvpa0s1fd6mm4l4czcas72rn") (r "1.64.0")))

