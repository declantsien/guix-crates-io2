(define-module (crates-io pr ed prediction-market) #:use-module (crates-io))

(define-public crate-prediction-market-0.1.0 (c (n "prediction-market") (v "0.1.0") (d (list (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0si2bs4an6vqqmcj7dnb00zajdpbad6lkwjd6cj6g0nybqh30ia4")))

