(define-module (crates-io pr op proptest-quickcheck-interop) #:use-module (crates-io))

(define-public crate-proptest-quickcheck-interop-1.0.0 (c (n "proptest-quickcheck-interop") (v "1.0.0") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "01fa7p97nnf6yqql4lciz65vci436fqdlfwx6wsfp6wjdsjszddl")))

(define-public crate-proptest-quickcheck-interop-1.0.1 (c (n "proptest-quickcheck-interop") (v "1.0.1") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "0hd97yxw5yx0pd1d8q8gh4pig79s7g784fmkaqhzrn0ig4bqx4rj")))

(define-public crate-proptest-quickcheck-interop-1.0.2 (c (n "proptest-quickcheck-interop") (v "1.0.2") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "1p5rw5xcmingxnlfygl7znyamg6y19a8lb0c1l4ivs4nfgmks8ml")))

(define-public crate-proptest-quickcheck-interop-1.0.3 (c (n "proptest-quickcheck-interop") (v "1.0.3") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "0vky70jhaclyv5cclxq1yag4k6k92lrphyvp55n2yk2iy4qn8qcq")))

(define-public crate-proptest-quickcheck-interop-1.0.4 (c (n "proptest-quickcheck-interop") (v "1.0.4") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "0zb9nkbn9sfdhc718pfcp4a3sm27pgcngpck17k6dkj75xkwlnqa")))

(define-public crate-proptest-quickcheck-interop-1.0.5 (c (n "proptest-quickcheck-interop") (v "1.0.5") (d (list (d (n "proptest") (r "^0.3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "14yvqx20xlm7lhqcqgfi2xnxg1c92z3zyj51f8b5q93x682ssnbs")))

(define-public crate-proptest-quickcheck-interop-2.0.0 (c (n "proptest-quickcheck-interop") (v "2.0.0") (d (list (d (n "proptest") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.0") (d #t) (k 0)))) (h "1da32m34706y9kwbncx0inxlmb9i873nrhvjg9dfairg7l7ms2zi")))

