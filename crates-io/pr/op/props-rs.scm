(define-module (crates-io pr op props-rs) #:use-module (crates-io))

(define-public crate-props-rs-0.1.0 (c (n "props-rs") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "07x62n16xqiqchjlsfk27gcylaxhjl86ylkmjg0cvh6xqlsrv24q")))

(define-public crate-props-rs-0.1.1 (c (n "props-rs") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0vz8pj3bsq19kjlvp1031cghl1x3jhni53v4cfzricifgkw3am4g")))

