(define-module (crates-io pr op propane-macros) #:use-module (crates-io))

(define-public crate-propane-macros-0.1.0 (c (n "propane-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("fold" "full" "parsing"))) (d #t) (k 0)))) (h "01mgqcdi77mjipls7q4cn9lz0blr90i96i8xrahjyql7rm77jc6v")))

