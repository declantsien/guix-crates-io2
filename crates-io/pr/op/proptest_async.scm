(define-module (crates-io pr op proptest_async) #:use-module (crates-io))

(define-public crate-proptest_async-0.1.0 (c (n "proptest_async") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)))) (h "1pxp5qsqjsgbvy46x24pxqq50253gfd70g763bcphykivz5h21l5")))

(define-public crate-proptest_async-0.1.1 (c (n "proptest_async") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)))) (h "0zd3ry8xik15wjcsg8yvgn9fc3jl9d96v127236mnnr055s0mi9i") (f (quote (("default" "async_std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("async_std" "dep:async-std"))))))

(define-public crate-proptest_async-0.2.0 (c (n "proptest_async") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)))) (h "05kp8pc0snmij5nm9npwnl247pazw7snw8in164zradyaihyqmg9") (f (quote (("default" "async_std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("async_std" "dep:async-std"))))))

