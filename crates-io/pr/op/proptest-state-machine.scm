(define-module (crates-io pr op proptest-state-machine) #:use-module (crates-io))

(define-public crate-proptest-state-machine-0.1.0 (c (n "proptest-state-machine") (v "0.1.0") (d (list (d (n "message-io") (r "^0.14.8") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 0)))) (h "1hiaz81p8s1b69ch7lwx1bsq0iwkb8kd45ln52jicbfy2m4p2amm")))

(define-public crate-proptest-state-machine-0.2.0 (c (n "proptest-state-machine") (v "0.2.0") (d (list (d (n "message-io") (r "^0.18.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (f (quote ("fork" "timeout" "bit-set"))) (d #t) (k 0)))) (h "0avr7gzqb4nrmpbhn0ps6b52xr77vz216iznxv9z5ab0l9ygyd9k") (f (quote (("std" "proptest/std") ("default" "std"))))))

(define-public crate-proptest-state-machine-0.3.0 (c (n "proptest-state-machine") (v "0.3.0") (d (list (d (n "message-io") (r "^0.18.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (f (quote ("fork" "timeout" "bit-set"))) (d #t) (k 0)))) (h "010y7mdhwvi9jhmgz5kdvp901fl6aaz3vhv9qnq688hh25m8s9r8") (f (quote (("std" "proptest/std") ("default" "std"))))))

