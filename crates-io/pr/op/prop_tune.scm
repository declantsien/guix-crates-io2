(define-module (crates-io pr op prop_tune) #:use-module (crates-io))

(define-public crate-prop_tune-0.1.0 (c (n "prop_tune") (v "0.1.0") (d (list (d (n "prop_tune_core") (r "^0.1.0") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0qs459r4drd808gna1qfsbs79m7q5drwi7gl7zakcff6a31kbad0")))

(define-public crate-prop_tune-0.1.1 (c (n "prop_tune") (v "0.1.1") (d (list (d (n "prop_tune_core") (r "^0.1.1") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1za6r5n5j8ya1g1c87zhisb5jg7x2zk1j7bq8233drv6v9s7ji1c")))

(define-public crate-prop_tune-0.1.2 (c (n "prop_tune") (v "0.1.2") (d (list (d (n "prop_tune_core") (r "^0.1") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.1") (d #t) (k 0)))) (h "13v9gd88w9q2hrp502iwh2d500x7984liab78h7391mc1pxvhlrb")))

(define-public crate-prop_tune-0.1.3 (c (n "prop_tune") (v "0.1.3") (d (list (d (n "prop_tune_core") (r "^0.1") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.1") (d #t) (k 0)))) (h "1wv486yld4adk2dnmff3ba6vyqfsg1k0r61znnmm9r1fq4ink5ms")))

(define-public crate-prop_tune-0.1.4 (c (n "prop_tune") (v "0.1.4") (d (list (d (n "prop_tune_core") (r "^0.1") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.1") (d #t) (k 0)))) (h "0ybrkkwbnxx7kcnjps04vxpwwql43k0w17rrfnxygq4dkh5jgw3a")))

(define-public crate-prop_tune-0.2.0 (c (n "prop_tune") (v "0.2.0") (d (list (d (n "prop_tune_core") (r "^0.2") (d #t) (k 0)) (d (n "prop_tune_macro") (r "^0.2") (d #t) (k 0)))) (h "080iaixnxiyfyd572vgfwnprsifps2czhiavlwxv8qzl62vizr7l")))

