(define-module (crates-io pr op proper_path_tools) #:use-module (crates-io))

(define-public crate-proper_path_tools-0.1.0 (c (n "proper_path_tools") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "100hmgf79c7yx23i9dwimzxag4ar7nd79kj9kdafsprp5sqcw2y8") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-proper_path_tools-0.2.0 (c (n "proper_path_tools") (v "0.2.0") (d (list (d (n "mod_interface") (r "~0.15.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "072lsd70i3kk3fn7kys29p21d4w3cna5klq631l2xns5ms5k4hkz") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface/enabled") ("default" "enabled"))))))

(define-public crate-proper_path_tools-0.3.0 (c (n "proper_path_tools") (v "0.3.0") (d (list (d (n "mod_interface") (r "~0.16.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1v18kzqrblsf6w78z29mvb2wk9g70pdkhfhn6yihpqni3mf6h32v") (f (quote (("use_alloc") ("path_unique_folder_name") ("no_std") ("full" "enabled" "path_unique_folder_name") ("enabled" "mod_interface/enabled") ("default" "enabled" "path_unique_folder_name"))))))

(define-public crate-proper_path_tools-0.4.0 (c (n "proper_path_tools") (v "0.4.0") (d (list (d (n "mod_interface") (r "~0.17.0") (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "02g08cxqbhf1mhw21ggvnnvr5rmlwshjrgbwfbbnp14jdz9hkcza") (f (quote (("use_alloc" "no_std") ("path_unique_folder_name") ("no_std") ("full" "enabled" "path_unique_folder_name") ("enabled" "mod_interface/enabled") ("default" "enabled" "path_unique_folder_name"))))))

(define-public crate-proper_path_tools-0.5.0 (c (n "proper_path_tools") (v "0.5.0") (d (list (d (n "mod_interface") (r "~0.19.0") (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)))) (h "0kan09mqy8fvp082dv354dafpsi5x6w69kx1fswnsg3gnwlfl33j") (f (quote (("use_alloc" "no_std") ("path_unique_folder_name") ("no_std") ("full" "enabled" "path_unique_folder_name" "derive_serde") ("enabled" "mod_interface/enabled") ("derive_serde" "serde") ("default" "enabled" "path_unique_folder_name"))))))

