(define-module (crates-io pr op propdayscov) #:use-module (crates-io))

(define-public crate-propdayscov-0.1.0 (c (n "propdayscov") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "nfd2") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zkmsrfa1cnc3v184g274phhrjbm01gb3sjr9zck6v55vg4fv874")))

