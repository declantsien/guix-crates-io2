(define-module (crates-io pr op proplate-tui) #:use-module (crates-io))

(define-public crate-proplate-tui-0.2.0 (c (n "proplate-tui") (v "0.2.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0z9qv5yfj6vwn6xvk35pky3v7z4f31s85jlhjnp3xdizz3x9jf8p")))

(define-public crate-proplate-tui-0.3.0 (c (n "proplate-tui") (v "0.3.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "06h3ryghfkgjz6h2am97cmpqgpr3sbghhp1dazd800npxmvnlzqw")))

(define-public crate-proplate-tui-0.3.1 (c (n "proplate-tui") (v "0.3.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1f7c19kpgsh99d6k16a19w16k2vrdvmi6l6p21nvszhf8qdbnksd")))

