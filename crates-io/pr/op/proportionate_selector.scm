(define-module (crates-io pr op proportionate_selector) #:use-module (crates-io))

(define-public crate-proportionate_selector-0.1.0 (c (n "proportionate_selector") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0j2l2qcsky2z7qhx0jpnzi64rx3v4f5k6r120c5ylygympdvrvwp")))

(define-public crate-proportionate_selector-0.1.1 (c (n "proportionate_selector") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1fphvb3gqwp42qp6jlcg1z0sv0lmxsp69fzx997ibabhfzyni30s")))

(define-public crate-proportionate_selector-0.1.2 (c (n "proportionate_selector") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "154h2v708pfphqbmbygmpggr6rjsz1071wxwmwyrvzkadj76b2w8")))

