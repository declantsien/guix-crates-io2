(define-module (crates-io pr op propositional_logic) #:use-module (crates-io))

(define-public crate-propositional_logic-0.1.0 (c (n "propositional_logic") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4") (d #t) (k 0)))) (h "07xxi7rsrbk7ylxifpnppwybh3yk12dsi45gncacw7y7pgpkjd65")))

(define-public crate-propositional_logic-0.2.0 (c (n "propositional_logic") (v "0.2.0") (d (list (d (n "cli-table") (r "^0.4") (d #t) (k 0)))) (h "0prh05lrgk3d8g1lqjwp43mqsmsy8kdm3196jpcigbvx333nv1gl")))

