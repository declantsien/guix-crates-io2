(define-module (crates-io pr op proptest-derive) #:use-module (crates-io))

(define-public crate-proptest-derive-0.0.1 (c (n "proptest-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0389rvg3sd6b5xnc2z81i3im2xjln2s5s7lnbg032aaaa79qay48")))

(define-public crate-proptest-derive-0.1.0 (c (n "proptest-derive") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.9.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "0il6n70zmcl3ab28pgs0f2zblynz1bw4vghf7j30f780xw2gpvv4")))

(define-public crate-proptest-derive-0.1.1 (c (n "proptest-derive") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp" "stable"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "0ilkz8rzxamdni9k12ajlwibj8v9r6zv5i9r3py61rr58p2n9ch8")))

(define-public crate-proptest-derive-0.1.2 (c (n "proptest-derive") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp" "stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "0nziczbm7w0jisjrd216hh2j45fs5m363ga7r6nawwxcxlbxn7nk")))

(define-public crate-proptest-derive-0.2.0 (c (n "proptest-derive") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp" "stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "1m1jbydld98v3kfr32c1xqgmgkrcnym6x2rjjkjxzsg9vch9s785")))

(define-public crate-proptest-derive-0.3.0 (c (n "proptest-derive") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("tmp" "stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "1p4x6k1zxq9lrpk46npdnzj6894mjx5bpwkwrdk63ird72an5d4h")))

(define-public crate-proptest-derive-0.4.0 (c (n "proptest-derive") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.9") (f (quote ("tmp" "stable"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("visit" "extra-traits" "full"))) (d #t) (k 0)))) (h "0vhb7zmnbmn0qvv6x7ibs88pg0mn6d3131c9qzlq982w80vn7wcw")))

