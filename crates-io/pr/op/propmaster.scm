(define-module (crates-io pr op propmaster) #:use-module (crates-io))

(define-public crate-propmaster-0.1.0 (c (n "propmaster") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "102zmk6c3rq2l98pfmmp2v5lj8708sgz6f8pi1d1l5a6128bxz49") (f (quote (("json_export" "serde") ("default" "serde" "json_export")))) (s 2) (e (quote (("serde" "dep:serde"))))))

