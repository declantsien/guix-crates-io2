(define-module (crates-io pr op proptest-http) #:use-module (crates-io))

(define-public crate-proptest-http-0.1.0 (c (n "proptest-http") (v "0.1.0") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)))) (h "1dy1w8xcbvsj8grql9w26zrqwrbp5qwjl34sy55shfbiv44qd1sf")))

