(define-module (crates-io pr op propfuzz-macro) #:use-module (crates-io))

(define-public crate-propfuzz-macro-0.0.1 (c (n "propfuzz-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xizaahjxxvcz9n91pgpji3nd7b755qgq3m2kmmg53zwjwv9nnsx")))

