(define-module (crates-io pr op proptest-stateful) #:use-module (crates-io))

(define-public crate-proptest-stateful-0.1.0 (c (n "proptest-stateful") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "02qs556jxjfgq46aqg90s73lhj9h2rq47fijyabwr4bq6azhsr72")))

(define-public crate-proptest-stateful-0.1.1 (c (n "proptest-stateful") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1f94kifxxvpl8hafa6ywyp66z9f31i9ir2xkfpdsknjfjvjc6q9l")))

(define-public crate-proptest-stateful-0.1.2 (c (n "proptest-stateful") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0lvgs7qpb49g6aydncdnp6708dldjdfzki0ln0lpd7phwdiys8ac")))

(define-public crate-proptest-stateful-0.1.3 (c (n "proptest-stateful") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "04rcj75dz51fzlgff1nm1xvwcj47ym813n2a4kbx25fw7zhrc6g1")))

