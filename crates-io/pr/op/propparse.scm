(define-module (crates-io pr op propparse) #:use-module (crates-io))

(define-public crate-propparse-0.1.0 (c (n "propparse") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "0bhzmlqw07xssmy45rxwrbf2dc9a3x4as1m38axahfkq1xlr6qla")))

(define-public crate-propparse-0.1.1 (c (n "propparse") (v "0.1.1") (h "0d4bjzwpp4xiqp97nrgbmns4kgdx2hmfz56hp9fdnp8y0mh6ak7i") (y #t)))

(define-public crate-propparse-0.1.2 (c (n "propparse") (v "0.1.2") (h "0l4vg86nw9m5xagn4dc5yln1p4ha1rm8y0x4yszgxv3qf9mkk6by") (y #t)))

(define-public crate-propparse-0.1.3 (c (n "propparse") (v "0.1.3") (h "175j3jynvzmp9c8cn0v55jg182i3sihkby9kk3ivvzf6ja781mds") (y #t)))

(define-public crate-propparse-0.2.0 (c (n "propparse") (v "0.2.0") (h "1h5czn5acf6v14cwkvgcz8m54f14hf18mwgxjc4h5f26ygw28cix") (y #t)))

(define-public crate-propparse-0.2.1 (c (n "propparse") (v "0.2.1") (h "029bw46bfwxs8iw1ayq5abm5qb695r36zbp9kfqdxv2w1ki3i6jm") (y #t)))

(define-public crate-propparse-0.2.2 (c (n "propparse") (v "0.2.2") (h "0wbv2q4fd4nw8wsp2d7s2xhhdhm49yadiphk1pnzij7j5zmwhnkm") (y #t)))

(define-public crate-propparse-0.2.3 (c (n "propparse") (v "0.2.3") (h "10w9c8skzxyinb65camhhr8qgmalq0hvkpvlq9cyj0jnbacr5iby")))

(define-public crate-propparse-0.2.4 (c (n "propparse") (v "0.2.4") (h "012cvkfrh1svl6mi5yy74mqbb6h0l36lwpniw9blahzggw7nm6hd")))

(define-public crate-propparse-0.2.5 (c (n "propparse") (v "0.2.5") (h "06fk02jk7razr3j8h3y30jqs3aambm1a2h5bpmy7qpcdpal1s93l")))

