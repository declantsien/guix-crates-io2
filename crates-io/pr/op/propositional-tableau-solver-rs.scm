(define-module (crates-io pr op propositional-tableau-solver-rs) #:use-module (crates-io))

(define-public crate-propositional-tableau-solver-rs-0.1.0 (c (n "propositional-tableau-solver-rs") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.0") (d #t) (k 2)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^2.0.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (f (quote ("color" "suggestions" "wrap_help" "paw"))) (d #t) (k 0)))) (h "1h0k576jj4z9jrnawalva7jmqa9l2k93cy02zmnihy0j3a70v3x6")))

