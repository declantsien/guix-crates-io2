(define-module (crates-io pr op proplate-integration) #:use-module (crates-io))

(define-public crate-proplate-integration-0.2.0 (c (n "proplate-integration") (v "0.2.0") (d (list (d (n "proplate-errors") (r "^0.2.0") (d #t) (k 0)) (d (n "proplate-tui") (r "^0.2.0") (d #t) (k 0)))) (h "1cp8vhfg7zn8s7m6k0jnc5daj1lhfd84gks76h5lx80hmxk0m6hs")))

(define-public crate-proplate-integration-0.3.0 (c (n "proplate-integration") (v "0.3.0") (d (list (d (n "proplate-errors") (r "^0.3.0") (d #t) (k 0)) (d (n "proplate-tui") (r "^0.3.0") (d #t) (k 0)))) (h "0y16swcscc9r05mv0p55gjs4im104lmcj0ga5a30mhqiy8101zn7")))

(define-public crate-proplate-integration-0.3.1 (c (n "proplate-integration") (v "0.3.1") (d (list (d (n "proplate-errors") (r "^0.4.0") (d #t) (k 0)) (d (n "proplate-tui") (r "^0.3.1") (d #t) (k 0)))) (h "0ws19yiz90bpclv4fjm189q0c0rja28isv8cahglvq0bwixbgwff")))

