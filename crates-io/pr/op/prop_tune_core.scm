(define-module (crates-io pr op prop_tune_core) #:use-module (crates-io))

(define-public crate-prop_tune_core-0.1.0 (c (n "prop_tune_core") (v "0.1.0") (h "16wmcji6jzgpagq85b8v10m0y79mr12vpfvkq2jx41rp4yyf728g")))

(define-public crate-prop_tune_core-0.1.1 (c (n "prop_tune_core") (v "0.1.1") (h "0wskpsgi1cr69ngci2kznwp0j31zwdn78gcjgh55yhhqi2mjq4h3")))

(define-public crate-prop_tune_core-0.1.2 (c (n "prop_tune_core") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k3wflh89xxb0wamwj53m5hk6k6cg0npmbzkr7fbvihj5xk1yi1f")))

(define-public crate-prop_tune_core-0.1.3 (c (n "prop_tune_core") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02q0n0pwl69n3pd62a1snbykpaifc5h29vn2c8faizr1593m349r")))

(define-public crate-prop_tune_core-0.1.4 (c (n "prop_tune_core") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w9cq9znbmql4zg19v1dmy5s52y8s600px19cjji0kh5ygd8h17c")))

(define-public crate-prop_tune_core-0.2.0 (c (n "prop_tune_core") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08vvj1yknpx1ks20khv6y5ssb4lng4hkzjk7hivz9azzcxg1jc3j")))

