(define-module (crates-io pr op propresenter) #:use-module (crates-io))

(define-public crate-propresenter-0.1.0 (c (n "propresenter") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1nkphrpid27qy2vdxm2xawyc56wzylghh6iwhhy530pwyhcpbql6")))

