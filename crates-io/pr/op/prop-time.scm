(define-module (crates-io pr op prop-time) #:use-module (crates-io))

(define-public crate-prop-time-0.1.0 (c (n "prop-time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1flh9iar4lzqhv6g4hff3asjq4w4sa83fd5f7vyifwk8a73rn3mr")))

(define-public crate-prop-time-0.1.1 (c (n "prop-time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 0)))) (h "0b4hz7q9qy7fxfsr7byhhrcgj0wypfl632r4z9c0zjn3hvqrk7r6")))

(define-public crate-prop-time-0.1.2 (c (n "prop-time") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 0)))) (h "1zayb8b3d6zrxy2glwfdpcqkj7zxgz3sxkzwgl8rbq13609bgi7m")))

