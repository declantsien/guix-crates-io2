(define-module (crates-io pr op property) #:use-module (crates-io))

(define-public crate-property-0.1.0 (c (n "property") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0fg2l33902alxdq71sv7gxv1p3fnql83sz7r8lpr9w9fggja0ih3")))

(define-public crate-property-0.2.0 (c (n "property") (v "0.2.0") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "1hdxj7yhcz907cmyb7wdcnq3j2ak7dz6b2r9k0p3k9w508gl26js")))

(define-public crate-property-0.2.1 (c (n "property") (v "0.2.1") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0k3pfgmd52hwnnyq7f1fpb9m0nky3wqslp5w5plc8hyc650wzmah")))

(define-public crate-property-0.2.2 (c (n "property") (v "0.2.2") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c13k7xiax5pxlxh3qgjnqj4xdxl590z5hq0gv6ifnid93d0bxna")))

(define-public crate-property-0.3.0 (c (n "property") (v "0.3.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01bf13c14bi1ahrbcp0961pny1cv1yqly5yilmvhhrqpfhadjb6w")))

(define-public crate-property-0.3.1 (c (n "property") (v "0.3.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "178ffw914snvrpd7pa38m5cn3993ra90n8jfhap1fw5cj97jn2m3")))

(define-public crate-property-0.3.2 (c (n "property") (v "0.3.2") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18yi5gp794p2jj665ywq6cgc8md9s17nrxi5my518jjldb6gvn1l")))

(define-public crate-property-0.3.3 (c (n "property") (v "0.3.3") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mdrb30nl755afw4vqx2ajc5rwdpgscl92arhlkfznvnprpgz6w8")))

(define-public crate-property-0.4.0-alpha.0 (c (n "property") (v "0.4.0-alpha.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bik0nml5882az7sr8yv0kw200fwl54jyhchzlicnzl55zp6snw6")))

