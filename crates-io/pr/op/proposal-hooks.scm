(define-module (crates-io pr op proposal-hooks) #:use-module (crates-io))

(define-public crate-proposal-hooks-0.1.0 (c (n "proposal-hooks") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "indexable-hooks") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "12hzd43bjm6vvfcy4i6lfbkraw3y7hqka6vpi7bbm643hidz38nr")))

