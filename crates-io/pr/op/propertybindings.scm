(define-module (crates-io pr op propertybindings) #:use-module (crates-io))

(define-public crate-propertybindings-0.0.1 (c (n "propertybindings") (v "0.0.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "cstr") (r "^0.1.4") (d #t) (k 0)) (d (n "qmetaobject") (r "^0.0.4") (d #t) (k 0)))) (h "0smlkfvcbik1qsap9nb3j1fmw139wa4v28kypqcg4s2pdw7cchd1")))

(define-public crate-propertybindings-0.0.2 (c (n "propertybindings") (v "0.0.2") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "cstr") (r "^0.1.4") (d #t) (k 0)) (d (n "qmetaobject") (r "^0.0.4") (d #t) (k 0)))) (h "1bp1c02qxjbzgcvl94hl6lapxyfbb2gjgs10l4n0ky040hiv3nj1")))

(define-public crate-propertybindings-0.0.3 (c (n "propertybindings") (v "0.0.3") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "cstr") (r "^0.1.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 2)) (d (n "qmetaobject") (r "^0.1.3") (d #t) (k 0)))) (h "18zw1cipn7n0ilc05m59xwv7y9v2rqsyi0i4z3i0f20sgrmzdr4k")))

