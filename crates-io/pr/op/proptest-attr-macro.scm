(define-module (crates-io pr op proptest-attr-macro) #:use-module (crates-io))

(define-public crate-proptest-attr-macro-0.1.0-pre (c (n "proptest-attr-macro") (v "0.1.0-pre") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1vb5na3xzykl2cp9pv69slhnn8qk0mf7ld440j27aldbp2x7qawg")))

(define-public crate-proptest-attr-macro-0.1.0 (c (n "proptest-attr-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "189wn3iry9ii7nfc6lwqyd49vb5y9vd73r7ghyzbz6zcm25fybdf")))

(define-public crate-proptest-attr-macro-1.0.0 (c (n "proptest-attr-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a1api1mnp67i4p2x3lap4mr294vassisdzs1a708py9mfrnv84z")))

