(define-module (crates-io pr op prop_tune_macro) #:use-module (crates-io))

(define-public crate-prop_tune_macro-0.1.0 (c (n "prop_tune_macro") (v "0.1.0") (d (list (d (n "prop_tune_core") (r "^0.1.0") (d #t) (k 0)))) (h "0ypqw7jc4m0pb4rcl0wzn99f0my7llc9vmcfwc3waismhrnvp45l")))

(define-public crate-prop_tune_macro-0.1.1 (c (n "prop_tune_macro") (v "0.1.1") (d (list (d (n "prop_tune_core") (r "^0.1.0") (d #t) (k 0)))) (h "10fmc6ddfl84kyfql2scm02p1h4v616b6370r7i7f0dml0rxv7qy")))

(define-public crate-prop_tune_macro-0.1.2 (c (n "prop_tune_macro") (v "0.1.2") (d (list (d (n "prop_tune_core") (r "^0.1") (d #t) (k 0)))) (h "1cv6b6jkzkpzf92b671wgj996791hk2mrnbsby9h9vf5z53pdy8r")))

(define-public crate-prop_tune_macro-0.1.3 (c (n "prop_tune_macro") (v "0.1.3") (d (list (d (n "prop_tune_core") (r "^0.1") (d #t) (k 0)))) (h "1gbpdaadhnx04mxh48mzq6v9q9nfkg98vvj1xkmlwq1ia1dq2gj3")))

(define-public crate-prop_tune_macro-0.2.0 (c (n "prop_tune_macro") (v "0.2.0") (d (list (d (n "prop_tune_core") (r "^0.2") (d #t) (k 0)))) (h "0ir01wfc64s8jy25msgh0i7j0vvf3xnv9bxiiidnysrm0knhw9rd")))

