(define-module (crates-io pr op proptest-recurse) #:use-module (crates-io))

(define-public crate-proptest-recurse-0.1.0 (c (n "proptest-recurse") (v "0.1.0") (d (list (d (n "im") (r "~12") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 0)))) (h "1v21jcxxfh4b5j689rihwwvwwhz0bfy5jb2p6w5364jqzd07kdmi")))

(define-public crate-proptest-recurse-0.2.0 (c (n "proptest-recurse") (v "0.2.0") (d (list (d (n "im") (r "~12") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 0)))) (h "12zfgbqi22xcjar4xg3m8j5ka7jrw3ijnkf3355h0ils8r3yhhdn")))

(define-public crate-proptest-recurse-0.3.0 (c (n "proptest-recurse") (v "0.3.0") (d (list (d (n "im") (r "~13") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 0)))) (h "0ywk08ppg8m0qnxr30b5mq937xajv4wb13x99nlqhkihwap5ccfv")))

(define-public crate-proptest-recurse-0.4.0 (c (n "proptest-recurse") (v "0.4.0") (d (list (d (n "im") (r "~15") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 0)))) (h "1ql0lxi4wg2wjbrykxwiakgrm4vsirn8jvfhidfiiksamzqlll9g")))

(define-public crate-proptest-recurse-0.5.0 (c (n "proptest-recurse") (v "0.5.0") (d (list (d (n "im") (r "~15") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1hj3gq3ys464r06cdzkrx3aw27cpfjiin61kw7flxibylv9jbc3k")))

