(define-module (crates-io pr op properties) #:use-module (crates-io))

(define-public crate-properties-0.1.0 (c (n "properties") (v "0.1.0") (h "0bf8vh5qda237kxayzq0vdc4gz4jhqxsn10fyg0jza2s5nhxpqgz")))

(define-public crate-properties-0.1.1 (c (n "properties") (v "0.1.1") (h "15wdy6daw6dccml7rlncrll72d91ihbmcmsncbd0k57cc6i75b4p")))

