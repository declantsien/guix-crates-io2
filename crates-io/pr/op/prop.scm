(define-module (crates-io pr op prop) #:use-module (crates-io))

(define-public crate-prop-0.1.0 (c (n "prop") (v "0.1.0") (h "1q49rqi5yx4whm5ynryc23fgr9l3by755i8a0mwd1h6s382s4xfs")))

(define-public crate-prop-0.1.1 (c (n "prop") (v "0.1.1") (h "0jjdhpkh7gylfq335yds6zq01ga6vllfxwzvpdc2kb5cl2hdcrv4")))

(define-public crate-prop-0.2.0 (c (n "prop") (v "0.2.0") (h "14nh2ri94x2jyyixv7khf9mhv6bhi9c4pi3jz760yh6208wyfzzb")))

(define-public crate-prop-0.3.0 (c (n "prop") (v "0.3.0") (h "0vdh1nyfp2cxxhywphgdhfmdv6r99gf87q4y4by6hbi3qj8370vs")))

(define-public crate-prop-0.4.0 (c (n "prop") (v "0.4.0") (h "049pmv40dl4f973564xkvzdl9b4l9ahjl1y7ajzsci1mbhabglbj")))

(define-public crate-prop-0.5.0 (c (n "prop") (v "0.5.0") (h "1c3yi6vvdkhhh8gc2972nrlvj3jq9ri6bzlz02vbl6q91bmnkyap")))

(define-public crate-prop-0.6.0 (c (n "prop") (v "0.6.0") (h "15i09zc72ffb9ylsbaj7iz2vhcmrs1hrxhvargvkjfkqy7ha8chd")))

(define-public crate-prop-0.7.0 (c (n "prop") (v "0.7.0") (h "03fbvz5ijzwj6q2ix18rl7ci2mhx89z7p27dkpd114j4x4vql2ql")))

(define-public crate-prop-0.8.0 (c (n "prop") (v "0.8.0") (h "137k3p23skshwwa8140bnf58fcklm723s79l32igq201997xfdfh")))

(define-public crate-prop-0.9.0 (c (n "prop") (v "0.9.0") (h "1ji81ypyx86zq6q2zb5xikfj1li2yni0dfn6j4r5vmk2ll5w9xb8") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.9.1 (c (n "prop") (v "0.9.1") (h "0dx8dx2z23qx2c3v46y7a5p68ii933bqqgf6ch6xcrpqgc2m4d4d") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.9.2 (c (n "prop") (v "0.9.2") (h "1lvnmv9rlvyg1kkxs5xr00d11l3lidnrp57f4vksj0zggn6f144w") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.10.0 (c (n "prop") (v "0.10.0") (h "0jzsfidljsf2hpmpl9hkbq2w0r97x07d8cjqshsm2i35a3n7grp8") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.11.0 (c (n "prop") (v "0.11.0") (h "11grsr9hnszq6sgfbf633gs65gmdvppqcn15gzpw8r5rndxj5gz4") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.12.0 (c (n "prop") (v "0.12.0") (h "1xrxlb7j422fxd9mf5x2gc9zm93kgpl0sydwd7mqqj1sjb4fq5lg") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.13.0 (c (n "prop") (v "0.13.0") (h "0g6kqx83apqjy03x6ahx1fnddnb8mh63kgkbh9imqa2vfbi0jsw5") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.14.0 (c (n "prop") (v "0.14.0") (h "00jiip3xcshhd12y41ax6kgjknbgi5l36jf3q8ywgvbyl4a1gxdp") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.15.0 (c (n "prop") (v "0.15.0") (h "104jlhqzfjh5qgdd8cjhdhca2gdlgyhkdpbb5mglzrsjgrb0algx") (f (quote (("pure_seshatism") ("pure_platonism") ("default"))))))

(define-public crate-prop-0.16.0 (c (n "prop") (v "0.16.0") (h "17rwmp9knkdjmjagk74i03vzkln4m7yny4x1m0mfah61ywbkplkl") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.17.0 (c (n "prop") (v "0.17.0") (h "0lill5inl5mdcvafzgq12lsp0vsnn2grfnjjxxck9xhpifgkzp5a") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.18.0 (c (n "prop") (v "0.18.0") (h "1vhhln4qlvl43k01g1fsffpmcqbclxby5kk9q631c7gjrrpc8dfk") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.19.0 (c (n "prop") (v "0.19.0") (h "1mj73z87bf7rrllh6fpz8nhgwy9rq9wdsrmgcgfxsyrwyxbcvqv3") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.20.0 (c (n "prop") (v "0.20.0") (h "0l034ah29pm82c94n9wi72fvp2va14hfl86xdfbk2w3kf5gp21kz") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.21.0 (c (n "prop") (v "0.21.0") (h "0pyax4nyjhh849v3z2pn7w7d754xzk7l94y1srq36ry93i2xpcbk") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.22.0 (c (n "prop") (v "0.22.0") (h "1xg5hrwvqyqxwlm509aixdwlimvgwncplln6lkwichx7s7pa9ii4") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.23.0 (c (n "prop") (v "0.23.0") (h "1hf4sf1lhljzwk2q56ryr3f4km3k8j9iryqqqhm9amkhzyw2wlfr") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.24.0 (c (n "prop") (v "0.24.0") (h "0f8lq3bz0fj1jw6np605q1bkn3k5nn5c85f2si660hrm0751z8vi") (f (quote (("pure_platonism") ("default"))))))

(define-public crate-prop-0.25.0 (c (n "prop") (v "0.25.0") (h "10drnspa6fx67fd63g1gqsmyqhz65s61mvng6mhr5wddskg6l5ia") (f (quote (("subst_equality") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default"))))))

(define-public crate-prop-0.26.0 (c (n "prop") (v "0.26.0") (h "1lh0fi90a517llg4mnpac12gals543w5fq7l4zc631xmx9wszzba") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.27.0 (c (n "prop") (v "0.27.0") (h "147qba6cywlp2kz40lkbin07s9fzghzhbpay209z2jhfpmz9mvf0") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.28.0 (c (n "prop") (v "0.28.0") (h "0hdxmqn8ak056ypjscv8ilkq3h83v8ylrb71y6gaj0hxrc481qc4") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.29.0 (c (n "prop") (v "0.29.0") (h "0qynhcrgfl9z6svj3l110rk6xl4m8yi1dldy50pzwy0d3lm7vz67") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.29.1 (c (n "prop") (v "0.29.1") (h "1spdsnpczk2bhzdqs3i4wxrl2b2wssv7s12ppa2iqii0r0q51463") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.29.2 (c (n "prop") (v "0.29.2") (h "0i2yy25wvvicds9f33mdrrdn0rxmmch263iyr8mf4h1jsqwg66jq") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.30.0 (c (n "prop") (v "0.30.0") (h "1gy3z4iy2921f4xkww5sxph645f17jhjv366n8ka1zy04gsf66n8") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.31.0 (c (n "prop") (v "0.31.0") (h "06f1l0z4l25kgxb4s54s6clv4ccqyx6yisyf3mbh8483rwj704fw") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.32.0 (c (n "prop") (v "0.32.0") (h "0b5r6wql59k0a1921d7hzj6jxmgj97c7s323rmnckczl97gxnnpb") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.33.0 (c (n "prop") (v "0.33.0") (h "1nkw5s18khmlbkzmwapkrgpb8cz47l7kg36dy8dqsharsl4fl35w") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.34.0 (c (n "prop") (v "0.34.0") (h "0ifiy39ryhakvmqds1f5f23izvn0hbgn88p44gxk92j7zamwsafi") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.35.0 (c (n "prop") (v "0.35.0") (h "1v3j6w9kfqvvfwvrk6yg7b4bk9n9zgqxcqdna75ndlmzbkfl62c0") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.36.0 (c (n "prop") (v "0.36.0") (h "0idw4984c2zas047k8d760p7bkcc36m0h2rbx71mgi1inm0h66sz") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.37.0 (c (n "prop") (v "0.37.0") (h "0xcgjsscy598zkq5l2an4rjv2ggff542nmjxdmdwrlnj4rn1f5k1") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.38.0 (c (n "prop") (v "0.38.0") (h "1y2q245p4vy5dd1wr5dsqi1b46f6xj92qzcs39y5qf7jij3f6f8i") (f (quote (("subst_equality") ("quantify") ("pure_platonism") ("old_homotopy_levels" "subst_equality") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.39.0 (c (n "prop") (v "0.39.0") (h "15xk014f8dfdpa4cind4spyvhyjygm3hlfjfp8zj3gx08pq0c0bp") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.40.0 (c (n "prop") (v "0.40.0") (h "1cys0prkdz0kgxja6zxqzkk4pal1xmqzlix2rv8kaipm70jspckc") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.41.0 (c (n "prop") (v "0.41.0") (h "1h4502g7wkv9psshrp3i9s30asrcvxh10kav3hs2dr628xi8dskq") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.42.0 (c (n "prop") (v "0.42.0") (h "133yc6l9sn7cyyls3f9zpyljl21l5wzyxkrv1mqy3c7241kdn42m") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.43.0 (c (n "prop") (v "0.43.0") (h "16qxi1c8mx83dsvshdrvra7nqj0li7k8cjli8gbyzwg0adngpwf4") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.44.0 (c (n "prop") (v "0.44.0") (h "0359mwdpml0wma7ncgv7ark46w1vz2mjypc8dsp2yzl9a102dbwb") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.45.0 (c (n "prop") (v "0.45.0") (h "0ddnxar4lc8ky100kjwplw58p27mbqrc1n6cpmg0c6813bn8x1fq") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.46.0 (c (n "prop") (v "0.46.0") (h "0jdp0mxibarlvr0djmcg0yf70n7nx5z5qs19xi0snv8q1psdrhi3") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

(define-public crate-prop-0.47.0 (c (n "prop") (v "0.47.0") (h "1gjg7vbq6n17dibpdpxl3gwkhjlfi3c9s6h5v26497b0ywpmyg8p") (f (quote (("quantify") ("default") ("avatar_extensions"))))))

