(define-module (crates-io pr op proper) #:use-module (crates-io))

(define-public crate-proper-0.1.0 (c (n "proper") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19i34sdmn1r81znf0dvgi8dsbqw78m6nagg5bx4aa19gpdlpras0")))

(define-public crate-proper-0.1.1 (c (n "proper") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "001c3zbr34wp0pq15wijninz6c1dfqir8zf9lfrqsa7r0ficia7s")))

(define-public crate-proper-0.1.2 (c (n "proper") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1b5qd1k9vpl7742mpplhp1ysahskkqb0c9w8xyvwfhw3m60j2z4v")))

(define-public crate-proper-0.1.3 (c (n "proper") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lrjba1my39i4kbcb1i1nmwyfxyxnbhazsarfzghvwi7p1f8nl3p")))

(define-public crate-proper-0.1.4 (c (n "proper") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1y6scgm8fnj8hw1lrz3d2d13kd70abi1cblwqfsv9zswiszskvwh")))

(define-public crate-proper-0.1.5 (c (n "proper") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wbidds0f330innyf3d60gca7a8xgrxrccr6fyka55jj836430sg")))

