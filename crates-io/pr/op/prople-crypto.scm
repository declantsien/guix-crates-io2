(define-module (crates-io pr op prople-crypto) #:use-module (crates-io))

(define-public crate-prople-crypto-0.1.0 (c (n "prople-crypto") (v "0.1.0") (d (list (d (n "rst-common") (r "^1.1.0") (f (quote ("standard" "with-errors" "with-cryptography"))) (d #t) (k 0)))) (h "1gpynvb60wpd2dlvcxnxza9xd9k2m675d9kpimmywrlgqfmf1mi1") (r "1.74")))

(define-public crate-prople-crypto-0.1.1 (c (n "prople-crypto") (v "0.1.1") (d (list (d (n "rst-common") (r "^1.1.0") (f (quote ("standard" "with-errors" "with-cryptography"))) (d #t) (k 0)))) (h "0vajnx8b8b5imaj1c3v5h8gvp4zfn3dxgricrv6hais7s3x8iw2f") (r "1.74")))

(define-public crate-prople-crypto-0.2.0 (c (n "prople-crypto") (v "0.2.0") (d (list (d (n "rst-common") (r "^1.1.0") (f (quote ("standard" "with-errors" "with-cryptography"))) (d #t) (k 0)))) (h "1q0jcs3x0wjlixfsq7lvhc8gg3jrbq4m2f1fdg24zzn23c7ch2ib") (r "1.74")))

(define-public crate-prople-crypto-0.2.1 (c (n "prople-crypto") (v "0.2.1") (d (list (d (n "rst-common") (r "^1.1.0") (f (quote ("standard" "with-errors" "with-cryptography"))) (d #t) (k 0)))) (h "0san0qpqfcnj6d0zv5y7ywjknrhzj1lmf6dadh6jgz6y5jzwnawy") (r "1.74")))

(define-public crate-prople-crypto-0.2.2 (c (n "prople-crypto") (v "0.2.2") (d (list (d (n "rst-common") (r "^1.1.0") (f (quote ("standard" "with-errors" "with-cryptography"))) (d #t) (k 0)))) (h "1ij72lc511i4i7a6hm5z87n1z2xf0z2dn0b42x2ifhrrw5iw4gfl") (r "1.74")))

