(define-module (crates-io pr op proplate-errors) #:use-module (crates-io))

(define-public crate-proplate-errors-0.2.0 (c (n "proplate-errors") (v "0.2.0") (d (list (d (n "proplate-tui") (r "^0.2.0") (d #t) (k 0)))) (h "0adm0dlinjbsfrs3m3l0r95iikf1443nm0spfb5mifa9v9hrgbg1")))

(define-public crate-proplate-errors-0.3.0 (c (n "proplate-errors") (v "0.3.0") (d (list (d (n "proplate-tui") (r "^0.3.0") (d #t) (k 0)))) (h "0h4wzcl2hnrq268dl2vx3z0yz0brrxp25d5rhiwjlc1a1nnz2lmk")))

(define-public crate-proplate-errors-0.4.0 (c (n "proplate-errors") (v "0.4.0") (d (list (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "proplate-tui") (r "^0.3.0") (d #t) (k 0)))) (h "0z2fp2ijjbg2b7nj7zfa9mbn7p00pqya65dsqa2lkr51zx7y4m6y")))

