(define-module (crates-io pr on prongs) #:use-module (crates-io))

(define-public crate-prongs-1.0.0 (c (n "prongs") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)))) (h "03pnrkqbxi575kcw6brqsd65ydvsd77pqiir62lbm5v4klz5ypk9") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs")))) (y #t)))

(define-public crate-prongs-1.0.1 (c (n "prongs") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w97w3fzh07d0zr7myss7hmd6dh6in34d0whxz56g5f2qsf2lb2d") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs")))) (y #t)))

(define-public crate-prongs-1.0.2 (c (n "prongs") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y446xjpbl4dbwn3yjvak3rv62ngi40arr2gdrxid5zq4j0g2hz0") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs")))) (y #t)))

(define-public crate-prongs-0.0.1 (c (n "prongs") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ni1akmmyybafbpk30hpnmxcjwvdwdwhv5px07khmspadq2r8mf4") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs")))) (y #t)))

(define-public crate-prongs-0.0.2 (c (n "prongs") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8i7xkvgpz2nc7x9nccya6d990m1zd766ssy7i8x0d5sqs2pjcp") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs"))))))

(define-public crate-prongs-0.0.3 (c (n "prongs") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l8526mx99jfjlx4xrq568y6sbj2d4bc570fndwxfdfdf2c86d7l") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs"))))))

(define-public crate-prongs-0.0.4 (c (n "prongs") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h70b9frx8bpj6ysfcxfiyaqrb9raca66qssnynla7z24sv9grs5") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs"))))))

(define-public crate-prongs-0.0.5 (c (n "prongs") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "piston") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p31lsa1c9cnrqfmhn5aihvf1dpacx5ra8mkxyajnfzj4ykcpp69") (f (quote (("backend_piston" "piston") ("backend_gilrs" "gilrs"))))))

