(define-module (crates-io pr on pronto-graphics) #:use-module (crates-io))

(define-public crate-pronto-graphics-0.2.1 (c (n "pronto-graphics") (v "0.2.1") (d (list (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (d #t) (k 0)))) (h "04z79w3i9fzhykmqk7911n6y11ps1yacwa4822d36qc9c700ywxp")))

(define-public crate-pronto-graphics-0.3.0 (c (n "pronto-graphics") (v "0.3.0") (d (list (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (d #t) (k 0)))) (h "191v6d5h9f8i5vykpi48lqdn79rhjjj2c4qnz8dp8h4ajwr695hc")))

(define-public crate-pronto-graphics-0.3.1 (c (n "pronto-graphics") (v "0.3.1") (d (list (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (d #t) (k 0)))) (h "1j0qa6b45da9c8h5warkp5jg619f3f45kvs6y83ay6rv6p5ql835")))

(define-public crate-pronto-graphics-0.4.0 (c (n "pronto-graphics") (v "0.4.0") (d (list (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (d #t) (k 0)))) (h "0sr5yvrj89f22yis9492dbayjkplbvajxp2bcajn9iihj2kl0xa6")))

