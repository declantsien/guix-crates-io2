(define-module (crates-io pr on pronto) #:use-module (crates-io))

(define-public crate-pronto-0.1.0 (c (n "pronto") (v "0.1.0") (d (list (d (n "infrared") (r "^0.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 2)))) (h "1z179afgls7dwyx8gcipwnzp7pkzw3cyk5n2qyn9njw0g5nw4z0a") (y #t)))

