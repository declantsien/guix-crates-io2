(define-module (crates-io pr on pronghorn) #:use-module (crates-io))

(define-public crate-pronghorn-0.1.0 (c (n "pronghorn") (v "0.1.0") (h "0c398bgx19mjfawym2whgf5dmb3gv3hcaj9nl1vg1njp868hr1c8")))

(define-public crate-pronghorn-0.1.1 (c (n "pronghorn") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "1s9ihz3cdhl26gpsr9sx4i4nhq2aq8klli51shg06j7n437kn18k")))

(define-public crate-pronghorn-0.1.2 (c (n "pronghorn") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1812l31hsqvmhdmhsz9qn0znzv6d14h7ai06748zkal5jsagyjdj")))

