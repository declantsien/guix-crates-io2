(define-module (crates-io pr id pride-term) #:use-module (crates-io))

(define-public crate-pride-term-0.3.0 (c (n "pride-term") (v "0.3.0") (d (list (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1dxwqgn8rmnsypmcs1001rp8837b8kh58g53n76yfb36mmf4ghcm")))

(define-public crate-pride-term-0.3.1 (c (n "pride-term") (v "0.3.1") (d (list (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "082v8ayzpc0bxnvax6gh1l5i0r647qiizwdmwz9662jcwpsb38mm")))

