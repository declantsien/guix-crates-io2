(define-module (crates-io pr id prideful) #:use-module (crates-io))

(define-public crate-prideful-0.1.0 (c (n "prideful") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.12") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1q0bx9vjaglvrwi9njfsfmdcg21x3f202j9gyd566n2kxs6isxhp")))

