(define-module (crates-io pr id pride-cli) #:use-module (crates-io))

(define-public crate-pride-cli-0.1.0 (c (n "pride-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1yfvrz33cg0c0j0bm1b5ang8m8z25jmn2svm4bw85l3fjkjg1m4c")))

