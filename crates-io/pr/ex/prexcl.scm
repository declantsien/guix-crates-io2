(define-module (crates-io pr ex prexcl) #:use-module (crates-io))

(define-public crate-prexcl-0.0.1 (c (n "prexcl") (v "0.0.1") (h "1gn0j1c0lk86c1xf7hkki0fl19n0k3skm83qkwh8kj0xkd03fsjn")))

(define-public crate-prexcl-0.0.2 (c (n "prexcl") (v "0.0.2") (h "19kbklmd2pfskmx82j1gfv5qj2f1mbqwgd5kr68i8090fnx1psyw")))

(define-public crate-prexcl-0.0.3 (c (n "prexcl") (v "0.0.3") (h "0fx287pyr9dv6kfrwkxbr6bhzp8i85g2cvbhlnywnmpxan99frj0")))

(define-public crate-prexcl-0.0.4 (c (n "prexcl") (v "0.0.4") (h "0ggapsqjy68yawxcz0pl4ghbdw52b0dph0nxdd7v62p0n2gnbz9b")))

(define-public crate-prexcl-0.0.5 (c (n "prexcl") (v "0.0.5") (h "13r2l88cb7bx1r3vrmv2rlpqlajdaxhxdnrkzm0j7caxw5yspcsr")))

(define-public crate-prexcl-0.0.6 (c (n "prexcl") (v "0.0.6") (h "00zbnys3d44jiz2p0ap2cmfaliqws9nzw6wz62nfxb91mgzakv96")))

(define-public crate-prexcl-0.0.7 (c (n "prexcl") (v "0.0.7") (h "0pdi2zjhh4v8lswhsbrzlbz4gawna8mna546kz5acbj84vxnx88y")))

(define-public crate-prexcl-0.0.8 (c (n "prexcl") (v "0.0.8") (h "04mr0k43llb0vngg98wi5crg8jnx43z65w6gcgg6lmf0qyx9abzx")))

(define-public crate-prexcl-0.0.9 (c (n "prexcl") (v "0.0.9") (h "0v3zjpn8jjd74ks4xqb2ipaf6bjcw46idcbzv06yppsym3ladv9q")))

(define-public crate-prexcl-0.0.10 (c (n "prexcl") (v "0.0.10") (h "1x1lmiz5ggh3vkg204gm9yhx9kyvpj29pyf51i60csg761zvjw71")))

