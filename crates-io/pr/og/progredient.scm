(define-module (crates-io pr og progredient) #:use-module (crates-io))

(define-public crate-progredient-1.0.0 (c (n "progredient") (v "1.0.0") (d (list (d (n "progressing") (r "^3.0.2") (d #t) (k 0)))) (h "14hc9iri8zzmyv5zm3w4b2kxbmnzxdxladr7m5z8fk19bgsk7zd2")))

(define-public crate-progredient-1.1.0 (c (n "progredient") (v "1.1.0") (d (list (d (n "progressing") (r "^3.0.2") (d #t) (k 0)))) (h "0b2x3819bp5p376y3nc13il6x35yz1q97591h34jdg69qj5nmcy9")))

