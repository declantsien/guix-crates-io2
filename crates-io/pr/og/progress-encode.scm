(define-module (crates-io pr og progress-encode) #:use-module (crates-io))

(define-public crate-progress-encode-0.1.0 (c (n "progress-encode") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 2)))) (h "1fna7lz6abfg5kcpmi2zqp59656ymyrqgdjv1basj4mmsdk4gl25") (r "1.56.1")))

(define-public crate-progress-encode-0.1.1 (c (n "progress-encode") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 2)))) (h "1ysrd8xpqfs4j6714pm80vlnbac7nf3dcdc7jrx3prppdv444qli") (r "1.56.1")))

(define-public crate-progress-encode-0.1.2 (c (n "progress-encode") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "109374mjwh4bjafd9lwhqnghcw75b5d6dhqk6j48z8pkzxjskaw9") (r "1.56.1")))

