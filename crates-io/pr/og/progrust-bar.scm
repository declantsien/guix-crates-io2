(define-module (crates-io pr og progrust-bar) #:use-module (crates-io))

(define-public crate-progrust-bar-0.1.0 (c (n "progrust-bar") (v "0.1.0") (h "1g53p305ipk1g58iip4bkv8l5sdl11k951xg5qdqi1l23c0hyiwq") (y #t)))

(define-public crate-progrust-bar-0.1.1 (c (n "progrust-bar") (v "0.1.1") (h "0mxzvw35xfhwf2mr76g2s320w8pdjs8xkd058wk9b08gdwgqab2l") (y #t)))

(define-public crate-progrust-bar-0.1.2 (c (n "progrust-bar") (v "0.1.2") (h "0z6fgqmff3hwban3jxy4qscd061h7c5ivz2ip0hdqh8jzc0p3d88") (y #t)))

(define-public crate-progrust-bar-0.1.3 (c (n "progrust-bar") (v "0.1.3") (h "0ravfr6pslmhzd9qdrg2bnp0q02r4x4ixf5zxvk899yy8dx5ymkf") (y #t)))

