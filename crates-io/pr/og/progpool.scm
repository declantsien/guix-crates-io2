(define-module (crates-io pr og progpool) #:use-module (crates-io))

(define-public crate-progpool-0.1.0 (c (n "progpool") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.14") (d #t) (k 0)) (d (n "tracing_chromium") (r "^0.1") (d #t) (k 0)) (d (n "tracing_facade") (r "^0.1") (d #t) (k 0)))) (h "18zi0y3pk4cin19lcy37p8l10lr6kjbv6rlp8r0a8a5zfpmx06n9")))

(define-public crate-progpool-0.2.0 (c (n "progpool") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.14") (d #t) (k 0)) (d (n "tracing_chromium") (r "^0.1") (d #t) (k 0)) (d (n "tracing_facade") (r "^0.1") (d #t) (k 0)))) (h "1x7s9ch22wsfcbgdgfxwzgnz70qwg7r4b2zfr73qxcy0da4mivbn")))

(define-public crate-progpool-0.2.1 (c (n "progpool") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tracing_chromium") (r "^0.1") (d #t) (k 0)) (d (n "tracing_facade") (r "^0.1") (d #t) (k 0)))) (h "0vpcfl6s06yf6b5s0kxyahchmw46wq0v61d66wr4ydd96kaianwv")))

(define-public crate-progpool-0.2.2 (c (n "progpool") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tracing_chromium") (r "^0.1") (d #t) (k 0)) (d (n "tracing_facade") (r "^0.1") (d #t) (k 0)))) (h "1g2si8fkzqv9h9xhqkys1pyhm21qdc0kbdgpf00aqq62vkcd8gz8")))

