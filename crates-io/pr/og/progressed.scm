(define-module (crates-io pr og progressed) #:use-module (crates-io))

(define-public crate-progressed-0.1.0 (c (n "progressed") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "11l48jibkw4ac54d9b4kkvghmnw9cm34zdzlxlss8j71vnq7hs71")))

(define-public crate-progressed-0.1.4 (c (n "progressed") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "09ykdz9iil12fycnxs8nr4svkv9xn538lmjy9vxk6rbkd2k9f00d")))

(define-public crate-progressed-0.2.0 (c (n "progressed") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "0plcvl0yv2pwxz2mjc1fv2xbcs76596c427yi1ad1aj30kb3dsr9")))

(define-public crate-progressed-0.2.1 (c (n "progressed") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1bg6x7k72wfbyb2w16hy1bqxi6plf9vzsn6rzch0d6grf52vhn03")))

