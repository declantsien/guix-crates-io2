(define-module (crates-io pr og progress_bar) #:use-module (crates-io))

(define-public crate-progress_bar-0.1.0 (c (n "progress_bar") (v "0.1.0") (h "0yqxip826474fn65cnv5wmpiipgsvlrws8xz110g00lw97ic3j5h")))

(define-public crate-progress_bar-0.1.1 (c (n "progress_bar") (v "0.1.1") (h "0p4xmabicgcjlxpj2ipgnf03pcxpaypsf321s8mvf3xvf87ffda4")))

(define-public crate-progress_bar-0.1.2 (c (n "progress_bar") (v "0.1.2") (h "1b97pmq8lvfskmpfajh253f2gipjnh83m8qd34fnf0i1vrb6gxz9")))

(define-public crate-progress_bar-0.1.3 (c (n "progress_bar") (v "0.1.3") (h "1p6292zlchjxgwvb2wzwpv8hs6bh6ss97p3icyh0ymahr5d31zar")))

(define-public crate-progress_bar-1.0.0 (c (n "progress_bar") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0dcnm55dq9z2447a6zyfdq64msv4ncyh1iga4i487rgbrdy7nnwv")))

(define-public crate-progress_bar-1.0.1 (c (n "progress_bar") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "135254spvipix2bkkz0z6cyskpnl8c9yam0j0ra5ibcav34masc2")))

(define-public crate-progress_bar-1.0.2 (c (n "progress_bar") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1ajscncxxn7lyknjfii58dirv0svif9glr1pydgwdwmr5p7yfjsb")))

(define-public crate-progress_bar-1.0.3 (c (n "progress_bar") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0dwhz69gaq6i66q0fa7ahn21n23wm5v68z9f8kmmvr9cf91qi3ic")))

(define-public crate-progress_bar-1.0.4 (c (n "progress_bar") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "15ixhdz84h81ssmaplk7si18iwhjidpzy9cgsfkyaypmsdz2jw62")))

(define-public crate-progress_bar-1.0.5 (c (n "progress_bar") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "153brn40nd83hql4gvqdqy4sv7y0cgchg93blna147a4hbchlcll")))

