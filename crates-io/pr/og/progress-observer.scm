(define-module (crates-io pr og progress-observer) #:use-module (crates-io))

(define-public crate-progress-observer-1.0.0 (c (n "progress-observer") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "06mvb2lhy6cwmy9i6mg5ywj6iq584032bccckycgrnx2ncnclh64")))

(define-public crate-progress-observer-1.0.1 (c (n "progress-observer") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08q1bz31fpvwjlln9gcq1z6apj19ky5m3j806dypjywx4z6b44s5")))

(define-public crate-progress-observer-1.1.0 (c (n "progress-observer") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15nr8m3cz0rhnykxspnh1ckqrrvgpkj7npap924ckxsd6vf9qkp1")))

(define-public crate-progress-observer-2.0.0 (c (n "progress-observer") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "093rpbj275w66ch7ik1d6ir95v2m85vi3hif335aps4f69adlm42")))

(define-public crate-progress-observer-2.0.1 (c (n "progress-observer") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12s276cw1qa2hfyjdrq04dj5d0i68xai0qww7qxinzk7xiv1i687")))

(define-public crate-progress-observer-2.0.2 (c (n "progress-observer") (v "2.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "14l7iv02kdd4p0g7wblxbzaaixqzzylhsk8rn6bzxad2f9qdg2b7")))

(define-public crate-progress-observer-2.0.3 (c (n "progress-observer") (v "2.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fn896zsbv4k3jpazcghdwdia0pji2a274qgzx61ym11ldkvlpda")))

(define-public crate-progress-observer-2.1.0 (c (n "progress-observer") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08x3irz3w3hvw1r2k7brb46z7syg83660plgbwmcbndr4a9nw0cy")))

(define-public crate-progress-observer-2.2.0 (c (n "progress-observer") (v "2.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "089vzxb0x0s4a17m1qi1ad2l23aw49acawrkbq7pc6khs7qzj3ar")))

(define-public crate-progress-observer-3.0.0 (c (n "progress-observer") (v "3.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11axw14kwi5jvgp3rwq7v129nsfhwmlkwy5jj87qvxiz9bw4907r")))

(define-public crate-progress-observer-3.1.0 (c (n "progress-observer") (v "3.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0l4jknmddcpjmamgx6d8ybqwa3s2p8rhbaa3mhiz5xz8mkihf4yr")))

(define-public crate-progress-observer-3.2.0 (c (n "progress-observer") (v "3.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bw244zhiq3im8f5vbxvs7202dxwrmax3j8r72vvasl5h4dn3pl8")))

