(define-module (crates-io pr og proglog) #:use-module (crates-io))

(define-public crate-proglog-0.2.0 (c (n "proglog") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "18va31hndici0dbak8ychrjy9lhmrg4h7a3ncc5mmli72hgd81q9")))

(define-public crate-proglog-0.3.0 (c (n "proglog") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "thousands") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "09fdmz6cak4i8xfrx00k0acw3liz8hs322vm5hpcdglvs52hg1k7") (f (quote (("pretty_counts" "thousands"))))))

