(define-module (crates-io pr og progitoor) #:use-module (crates-io))

(define-public crate-progitoor-0.1.0 (c (n "progitoor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("syslog-3"))) (d #t) (k 0)) (d (n "fuse_mt") (r "^0.5.1") (d #t) (k 0)) (d (n "interprocess") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.22.1") (d #t) (k 0)) (d (n "syslog") (r "^3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.43") (d #t) (k 0)))) (h "1j729ahyajv7ni2zqi14r6mik01pl9q5plx2mwm55zv3ak9zbg9n")))

