(define-module (crates-io pr og progress-monitor) #:use-module (crates-io))

(define-public crate-progress-monitor-0.0.1 (c (n "progress-monitor") (v "0.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0smgvhdri66vw534rdmh4c0grz1ap204bfvixyf4nykwnz518ryp") (r "1.60.0")))

(define-public crate-progress-monitor-0.0.2 (c (n "progress-monitor") (v "0.0.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0rs9ja4wj74kzjqn2mwgzln8vy6bp4yc5jgx5pl59n0w17rz33m4") (r "1.60.0")))

(define-public crate-progress-monitor-0.0.3 (c (n "progress-monitor") (v "0.0.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0w4102mzc47353wf2zhi8h3s0ig64h2zjyyg2zrfqk4wv1axrcmm") (r "1.60.0")))

