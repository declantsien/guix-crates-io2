(define-module (crates-io pr og program) #:use-module (crates-io))

(define-public crate-program-0.1.0 (c (n "program") (v "0.1.0") (h "1ndilx0l7y6jbi3rnxrp73fcglj7pp1s22hj4r6xgqk303l89838")))

(define-public crate-program-0.2.0 (c (n "program") (v "0.2.0") (h "07czl11hxx668xg7g1n7g445b5myp2h892fils6lk14b2vb3cyjj")))

(define-public crate-program-0.3.0 (c (n "program") (v "0.3.0") (h "12g975mfvi6kvnnnj79ah118xp1gbdsrgk6gfi1la5gjqxglbn1x")))

