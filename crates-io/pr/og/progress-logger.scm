(define-module (crates-io pr og progress-logger) #:use-module (crates-io))

(define-public crate-progress-logger-0.1.0 (c (n "progress-logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07knyb5n72ckfd9c3c5rdhak64d49q1613n6fcw61j8n68dgydhc")))

(define-public crate-progress-logger-0.1.1 (c (n "progress-logger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11c35bplwbjp53kv2iv8njah6l5iymw7p5rsbf20zdw0x8ibdhcg")))

(define-public crate-progress-logger-0.2.0 (c (n "progress-logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "0s5g1idyc3lms191yjf3vli26smim0iiqi4qyzfcviv17ymz0mww")))

(define-public crate-progress-logger-0.3.0 (c (n "progress-logger") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "1g89wp0v0790av4qz9x4441l2y9r1j9g56izagdxqcm3vb7gyf74")))

(define-public crate-progress-logger-0.3.1 (c (n "progress-logger") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "02n7hi14viav3yw7b4g24vrnnycz43irzjw7xx62dh0kw5qazycc")))

