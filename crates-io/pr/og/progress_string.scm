(define-module (crates-io pr og progress_string) #:use-module (crates-io))

(define-public crate-progress_string-0.1.0 (c (n "progress_string") (v "0.1.0") (h "1dpw8vm1zz2vlmhxbipgj3pbixvmihhx22isnnnjpjkzcp211pff")))

(define-public crate-progress_string-0.1.1 (c (n "progress_string") (v "0.1.1") (h "02y69xrd2vrjw2sgr8grn50dqksynvlxvq03h8j91402a9mkl58p")))

(define-public crate-progress_string-0.2.0 (c (n "progress_string") (v "0.2.0") (d (list (d (n "termion") (r "^1.0") (d #t) (k 2)))) (h "1pvlwbnb01r54ndw97hljh40ka8slnl4c4hzbhi1qd2czjlyhisc")))

(define-public crate-progress_string-0.2.1 (c (n "progress_string") (v "0.2.1") (d (list (d (n "termion") (r "^1.0") (d #t) (t "cfg(unix)") (k 2)))) (h "0bwvnk0snw5r2kdbxgg41ajz8r4mxq2l588rd4jl18si30sf7f0d")))

