(define-module (crates-io pr og progrust) #:use-module (crates-io))

(define-public crate-progrust-0.1.0 (c (n "progrust") (v "0.1.0") (h "0gjjjmvq2k4wz8pa38s8gpfx0igwi9szhi48qzkf4d87pk4lyzj4") (y #t)))

(define-public crate-progrust-0.2.0 (c (n "progrust") (v "0.2.0") (h "0xpcqz92nn6c5q9jcsaykb7rcddml5sq8iwjwhl2xzgq372h2n4y")))

(define-public crate-progrust-0.2.1 (c (n "progrust") (v "0.2.1") (h "1l24i14l0k18i363baj2w84ghz2djaxnkpnwiic1k33swcibwnwk")))

