(define-module (crates-io pr og program_tools) #:use-module (crates-io))

(define-public crate-program_tools-0.1.0 (c (n "program_tools") (v "0.1.0") (d (list (d (n "error_tools") (r "~0.13.0") (f (quote ("error_for_app"))) (k 0)) (d (n "iter_tools") (r "~0.16.0") (k 0)) (d (n "mod_interface") (r "~0.18.0") (k 0)) (d (n "proper_path_tools") (r "~0.4.0") (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)))) (h "0xb8p60rsskcnqbh3w3m8qrv43m63hjlq573w2fm4v7xcrak1v00") (f (quote (("full" "enabled") ("enabled" "mod_interface/enabled" "proper_path_tools/enabled" "error_tools/enabled" "iter_tools/enabled") ("default" "enabled"))))))

