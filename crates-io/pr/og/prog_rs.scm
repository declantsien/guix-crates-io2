(define-module (crates-io pr og prog_rs) #:use-module (crates-io))

(define-public crate-prog_rs-0.1.0 (c (n "prog_rs") (v "0.1.0") (d (list (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0ll123l801jx4lah6n7w395cshsr0ady70nj0vlb4n1m32vppp6f")))

(define-public crate-prog_rs-0.1.1 (c (n "prog_rs") (v "0.1.1") (d (list (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "05bpc22h5xx6m64l2xyadn6ykx7iw0hc20idvk8ldr0w3rcg2dmr")))

(define-public crate-prog_rs-0.1.2 (c (n "prog_rs") (v "0.1.2") (d (list (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1x2y75b1nb6ynhdzmgijn6w9yyxbzp7lwvjcv0siax7ygb5694pi")))

(define-public crate-prog_rs-0.2.0 (c (n "prog_rs") (v "0.2.0") (d (list (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "04ajnz8b0fqfdzj2xwszim41w205s4dljgip1f7hnw3dgbvb41wb")))

