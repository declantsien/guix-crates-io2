(define-module (crates-io pr og progressing) #:use-module (crates-io))

(define-public crate-progressing-1.0.0 (c (n "progressing") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mrs73nycdvdc2ryjp7i8rybymnbyym37ay5xlqpzhhdchsndplc") (y #t)))

(define-public crate-progressing-1.0.1 (c (n "progressing") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dmpjal4vx869jl57l0pbbsj65mck0s9gz0abmzq34kybzyfmzi2") (y #t)))

(define-public crate-progressing-1.0.2 (c (n "progressing") (v "1.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ixqgvn3cllrb0jkphma5ha8h076bczhi9057q7nm5f3kv8bq93z")))

(define-public crate-progressing-1.0.3 (c (n "progressing") (v "1.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "175vy2bwha3j3v55fz1pslksx1i7c9sq9zp7pmcxcazpycd1myw3")))

(define-public crate-progressing-2.0.0 (c (n "progressing") (v "2.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0djasf65i4qikkhf1lkgp5701sjn1lmh0crfrd44x2bjl871r7fp")))

(define-public crate-progressing-2.0.1 (c (n "progressing") (v "2.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1a87r6ymm2pxdq2hix21bzmjh59b25y3qf08vc884h6jnccm6f47")))

(define-public crate-progressing-2.0.2 (c (n "progressing") (v "2.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x4ik2f5p1nsapnyspmp6p9f7ypq5gyvs88yd2xz2dk5acnfsn2k")))

(define-public crate-progressing-2.1.0 (c (n "progressing") (v "2.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b7yk2li9ihpzzg6lqzlxgm936a33c8sbkz9p6v1zr8x9f42jp4w")))

(define-public crate-progressing-2.2.0 (c (n "progressing") (v "2.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04shvnkbqx5ppx9k5mn6z90v4n5pjpakgc6k9ja7km8xcchyi61i")))

(define-public crate-progressing-2.2.1 (c (n "progressing") (v "2.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fbq7pwwsmlrv96zi0fj02qn8g876ypzl4wbz94mmhl3g540kpc5")))

(define-public crate-progressing-2.2.2 (c (n "progressing") (v "2.2.2") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06a331mwnmc4qmvjja75gk3snhfwxkkgyxxdvrrvms6zqrlg8q7s") (f (quote (("main" "env_logger" "clap"))))))

(define-public crate-progressing-2.2.3 (c (n "progressing") (v "2.2.3") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1885rl9ydr95as8gm8zkfcq1aw44w5i10p7mqrlgpr4r029phdrb") (f (quote (("main" "env_logger" "clap"))))))

(define-public crate-progressing-3.0.0 (c (n "progressing") (v "3.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16syxjb31x6da88blrls7k74fbf7h02v5h03dwzs3rlx27wikl4i")))

(define-public crate-progressing-3.0.1 (c (n "progressing") (v "3.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mkznsj11sjgbzaswamvzng3qjrc8nkngpvprg2gvi0wa6zhs57j")))

(define-public crate-progressing-3.0.2 (c (n "progressing") (v "3.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06sb1cxpkc8lx56s76c95cfljs0513nsnn35wd6w79sblwcxpdwp")))

