(define-module (crates-io pr og progression) #:use-module (crates-io))

(define-public crate-progression-0.1.0 (c (n "progression") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "13pfz273s1l964lmlg19cd7crljjjyvz375g3xxsbyl5wcxd22z7") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.1 (c (n "progression") (v "0.1.1") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1m3q6g0vrkn85xh6bixakq65b02c27dzajjm1cjqngr0ym5sh8gc") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.2 (c (n "progression") (v "0.1.2") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "17ivs6562dsknwsmc9yhbdqz5cqcki3ih56hbjq743miibknvihm") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.3 (c (n "progression") (v "0.1.3") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1s62544sa04xcrhjd0a09ygnri2a3xkq4kcf4d3am9qzv2cvvn3j") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.4 (c (n "progression") (v "0.1.4") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1v2dh4s7y96fil07iqh1x6bgg8jahj9gib7i5lpl4x5p3ynhaz7a") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.5 (c (n "progression") (v "0.1.5") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0ykwqyvhrz9vcn3xaklfph584bnpkhrkavxzmqd7zssn9z6nc3wg") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.6 (c (n "progression") (v "0.1.6") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "147hz5vp4n3mwk256vckjy7cn8h1rq03hznfjcd90zmwhi4iba95") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.7 (c (n "progression") (v "0.1.7") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0irji9943a5fqwaciaf3d1vbh0c4zknp6r0awnga1flb1a0ic7j9") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.8 (c (n "progression") (v "0.1.8") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1xb43ghac03v5whm7lv4lz862lmr9q129kclz58vl84yaf5ck3ib") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.9 (c (n "progression") (v "0.1.9") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1ny83vzkvwwhlkpxj9vbavjqqcraic0sda04fwrpilaqm05hf78r") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.10 (c (n "progression") (v "0.1.10") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0z7xyjlqx7g5lycgxg8jammgjjjzjjz5ph1iw2q83r8q6a7sn8ld") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.11 (c (n "progression") (v "0.1.11") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1r5lh2760jpp4jdsjx63a7hdj2xgf0lymn8v76crlsa497d2p976") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.12 (c (n "progression") (v "0.1.12") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0brkbwhmsmrid2p8zwwc08bkz8a8kzxd1fbm8zmnv1cis9xkl63g") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.13 (c (n "progression") (v "0.1.13") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1ccpq429a4xjzgvgggwabjwxzqw7j759xv5swlvf7xrrgwycns1x") (f (quote (("default" "num-format" "terminal_size")))) (y #t)))

(define-public crate-progression-0.1.14 (c (n "progression") (v "0.1.14") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "07zkb4d6xfpqzlm9kqic9l0zzrd4v4qjg8rl5zwrbg72wanyc70d") (f (quote (("default" "num-format" "terminal_size"))))))

(define-public crate-progression-0.1.15 (c (n "progression") (v "0.1.15") (d (list (d (n "num-format") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "10q82wxhxwyrcxj12qwq0a2padk1l7qvsi05x78mivvqwcnh5nz8") (f (quote (("default" "num-format" "terminal_size"))))))

