(define-module (crates-io pr og progress) #:use-module (crates-io))

(define-public crate-progress-0.1.0 (c (n "progress") (v "0.1.0") (d (list (d (n "terminal_size") (r "^0.1.1") (d #t) (k 0)))) (h "0k0m5myqxd89lbfpv6n80xwc165facl8502kkc3x76ishwnnr9rz")))

(define-public crate-progress-0.1.1 (c (n "progress") (v "0.1.1") (d (list (d (n "terminal_size") (r "^0.1.1") (d #t) (k 0)))) (h "1h3d04ysqfsa6vhyhvysfzfp9gxqj6r8zwhlflwfvnq6il8zd27h")))

(define-public crate-progress-0.2.0 (c (n "progress") (v "0.2.0") (d (list (d (n "terminal_size") (r "^0.1.1") (d #t) (k 0)))) (h "0i0w5zh2hm4l16ibg5mgr9pf3vpybhhx0zrs0nb8d191awq21f0p")))

