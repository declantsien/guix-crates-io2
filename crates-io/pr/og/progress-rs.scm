(define-module (crates-io pr og progress-rs) #:use-module (crates-io))

(define-public crate-progress-rs-0.1.0 (c (n "progress-rs") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "13i3f7v871kgjkb1h7l4d2ayqb6hbkyc515yxhc961ny9c8zb5rc") (y #t)))

(define-public crate-progress-rs-0.1.1 (c (n "progress-rs") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "00sb0b0bzfh1rsa05r42pw382jwmcpdsrpii6vw5mn5ab4f72zaq")))

(define-public crate-progress-rs-0.2.0 (c (n "progress-rs") (v "0.2.0") (d (list (d (n "terminal_size") (r "^0.1.5") (d #t) (k 0)))) (h "03yx37dxhw2b8dqwfv74vyb9x8lp8mc3ad4l5isfa9vilivpq3qa")))

(define-public crate-progress-rs-0.3.0 (c (n "progress-rs") (v "0.3.0") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1ny9r8n2zb58h59700l9p8kyg28y7rg9499qjmmv54wk7ffizyck")))

(define-public crate-progress-rs-0.3.1 (c (n "progress-rs") (v "0.3.1") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "13f7sf83bvjgx4lqna0dxxmrclasrp59ibn5hpbwgbpfhxn4xskz")))

