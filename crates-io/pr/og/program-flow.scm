(define-module (crates-io pr og program-flow) #:use-module (crates-io))

(define-public crate-program-flow-0.1.0 (c (n "program-flow") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1psszki0xly03pqqmwx32jakwahrg8pdvbqdwqaj32gwi396w2bl")))

(define-public crate-program-flow-0.1.1 (c (n "program-flow") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0kh0j7kl62p3m4kpfd8h21wgzj9zgigyh60fdgw753wk9zp60bd0")))

(define-public crate-program-flow-0.1.2 (c (n "program-flow") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1w9s7206kfii1rfbsspn7ghhx169qh2sfgdgrlhkcnkx0iz68svl")))

(define-public crate-program-flow-0.1.3 (c (n "program-flow") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1yqyhm37lya07hbpw5yy21v0cwcm8cb5gn4qcacl9i0p68yarpz2")))

