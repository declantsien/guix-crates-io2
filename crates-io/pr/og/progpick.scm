(define-module (crates-io pr og progpick) #:use-module (crates-io))

(define-public crate-progpick-0.1.0 (c (n "progpick") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1yyqjhz51i5dhbizqlzv609kgh7msv4kw9dw3mnx9z929s1zy3la")))

(define-public crate-progpick-0.2.0 (c (n "progpick") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellwords") (r "^1.0") (d #t) (k 0)))) (h "1lg854ycl0f02kak92hc6k9mh04bniq3glzcrsv3bl1032jxf06z")))

