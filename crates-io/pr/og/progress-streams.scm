(define-module (crates-io pr og progress-streams) #:use-module (crates-io))

(define-public crate-progress-streams-1.0.0 (c (n "progress-streams") (v "1.0.0") (h "0cmx5qdd9gxl1wfj5c4dmcwvgnchminr410ds64m2zk98hf6l044")))

(define-public crate-progress-streams-1.1.0 (c (n "progress-streams") (v "1.1.0") (h "19w6z53nwb484x1rjk8k7hkkbzf9sdx08rldrnq0gik2h5ndjrg9")))

