(define-module (crates-io pr og proget) #:use-module (crates-io))

(define-public crate-proget-0.1.0 (c (n "proget") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "0awssr7al54c05518w3lfywfg4mg9lr7ldpjkfxq6jans5ps9m48")))

(define-public crate-proget-0.1.1 (c (n "proget") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "0nidjk2gjicrs9xv655iy85wdl3n7zy983a07p2v9shfdkqa7v2s")))

(define-public crate-proget-0.1.2 (c (n "proget") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "01cf8lffsm22f8jnh1bp8hcvy4a6j29m120ivzc6vhz25mbf5v89")))

(define-public crate-proget-0.1.3 (c (n "proget") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "02khkgsmzwlbvc1qc9dqfmr54ardqcl7ka1niqfir38rfhsccdgp")))

(define-public crate-proget-0.1.4 (c (n "proget") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "13l6b370jxjhd1z60s9vh3lkc6ygqw38q3zzk6xx9kdx2kiklx38")))

(define-public crate-proget-0.2.0 (c (n "proget") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1j44nbwd4pxrqfq2gj7g9z4hkm5y2r1sdigfamndslddfy58q8wv")))

(define-public crate-proget-0.2.1 (c (n "proget") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1890lqhrqlbmxv054d05kmpqgyw1mv6glqsh7czdkg54nfcmkamj") (f (quote (("rustls" "reqwest/rustls") ("default" "reqwest/default"))))))

(define-public crate-proget-0.2.2 (c (n "proget") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0bn7mwy700c6l5hxzbl3mm8wqnya6xlbyjzvcnr0z96jk5r03dzi") (f (quote (("rustls" "reqwest/rustls") ("default" "reqwest/default"))))))

(define-public crate-proget-0.2.3 (c (n "proget") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "02a5ql6nnam8wivrlw7r4q7l2pqmcxg30ga60d8mr357a47sg9x1") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("default" "reqwest/default"))))))

(define-public crate-proget-0.2.4 (c (n "proget") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "15xya9k4nz9vyxwan9hnw9di7b3iccm45lz11bijkrmxwwxbw39p") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("default" "reqwest/default")))) (s 2) (e (quote (("indexmap" "dep:indexmap") ("__docs" "dep:indexmap"))))))

