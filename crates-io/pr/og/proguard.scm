(define-module (crates-io pr og proguard) #:use-module (crates-io))

(define-public crate-proguard-0.1.0 (c (n "proguard") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "05dd7l42dfl654y3ar5s5yrkp26n76gyri16nd6qabprjqajdgrn")))

(define-public crate-proguard-0.2.0 (c (n "proguard") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v5"))) (d #t) (k 0)))) (h "1npq1kv6c8265lkw2fwmim4vmv504grclzx3cbi8lcb5k8bdp4x5")))

(define-public crate-proguard-0.3.0 (c (n "proguard") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v5"))) (d #t) (k 0)))) (h "1wblwvh1bprkfaprypr7w6aqjpipgd2dwzjc3z0y6bl51hl9xf40")))

(define-public crate-proguard-0.4.0 (c (n "proguard") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v5"))) (d #t) (k 0)))) (h "005s85809d3h29cp6yzrdak1rx23n3gr21qmvrin9zkqibfzgpv8")))

(define-public crate-proguard-1.0.0 (c (n "proguard") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v5"))) (d #t) (k 0)))) (h "1235vnwkla7c8blys8jcr2pkpjpfwjsfxvswm27xfx4wha28484v")))

(define-public crate-proguard-1.1.0 (c (n "proguard") (v "1.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6.0") (f (quote ("v5"))) (d #t) (k 0)))) (h "18br9536z92jlxykddf74bjfg55a4ykjnzc720l5svq18zzyqaa4")))

(define-public crate-proguard-2.0.0 (c (n "proguard") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (f (quote ("v5"))) (d #t) (k 0)))) (h "0bmm5bnfjvqdcg7mn1iq28vhic5gj8hjsvqg65hks69y74fpxzrb")))

(define-public crate-proguard-3.0.0 (c (n "proguard") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v5"))) (d #t) (k 0)))) (h "06zvycn3c6wc9lyaf501nbmg6mac18y48z7zi7z2yywviaiwwlfl")))

(define-public crate-proguard-4.0.0 (c (n "proguard") (v "4.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^0.8.1") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "1lgc5gmk3imrgz1pmzlwzv93dygh8dg9nr4dxh2zikwz2wvyvjg9") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-4.0.1 (c (n "proguard") (v "4.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^0.8.1") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "03v5kv9b0n8hwh8a7103ni552wdn3i1h0zszs2r3zw5r4f78l3ba") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-4.1.0 (c (n "proguard") (v "4.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^0.8.1") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "1dsdp8ycq2ilpzcwvsjqmd9y5shm8b6cfidk4kfwzbxdj5dl8j2v") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-4.1.1 (c (n "proguard") (v "4.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^0.8.1") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "066r7i2d66d9kwppcsgd5fg7lnhrvwh5jsdmy5h3a2jqqfasyakf") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.0.0 (c (n "proguard") (v "5.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "09a5vqdyzvv4q76i9lp8my49xm3b878yy7xbwwbzmvsf9avjm9cy") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.0.1 (c (n "proguard") (v "5.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "1qdm3xz9gfs5bh4pcci7c0pg2mchxn3wi46fik2nxz5brb7gk9dn") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.0.2 (c (n "proguard") (v "5.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "08qw527p9nj37llw3dj21hkrj4qrhc4hbk3y3l0ajdwva9qvbjwq") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.1.0 (c (n "proguard") (v "5.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "1lx8chk6bvsnv17sj7j9qhvixig442s6h943vifjvaqnqw0x8rhp") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.2.0 (c (n "proguard") (v "5.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "1vhzfcm8gkr4s072kvdyn4za0fv5la1rs0g83l5a1hkb9bwsif44") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.3.0 (c (n "proguard") (v "5.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "122x52dm4dg13m7l8dhxdvf91rcilmqbjvy30zi3mvmnjhkzr960") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.4.0 (c (n "proguard") (v "5.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "19p15qn4n0m89m5blwngwn06pmw9r2zwq6kvxk7mbrhrd5sgbv82") (f (quote (("uuid" "uuid_" "lazy_static"))))))

(define-public crate-proguard-5.4.1 (c (n "proguard") (v "5.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "uuid_") (r "^1.0.0") (f (quote ("v5"))) (o #t) (d #t) (k 0) (p "uuid")))) (h "160v7909c0cyq9y2kcdpmnzxi3rmfciplb2xm4wma3xhhsqdv3dq") (f (quote (("uuid" "uuid_" "lazy_static"))))))

