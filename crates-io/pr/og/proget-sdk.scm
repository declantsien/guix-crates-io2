(define-module (crates-io pr og proget-sdk) #:use-module (crates-io))

(define-public crate-proget-sdk-0.1.0 (c (n "proget-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1447y9s2fn72gkj0ywplgkf1xzzzn5wr30p22wlzjsmxf5zbc6wi") (y #t)))

(define-public crate-proget-sdk-0.1.1 (c (n "proget-sdk") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "02yzragcbp9cadsibp97vs5v3axpnhliydims6wjay28w6mx6209") (y #t)))

(define-public crate-proget-sdk-0.1.2 (c (n "proget-sdk") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0yckwqxa48li1dq2c76ifvwavibdwxqv3z10xjbpxra0r3hzidnn") (y #t)))

