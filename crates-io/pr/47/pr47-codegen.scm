(define-module (crates-io pr #{47}# pr47-codegen) #:use-module (crates-io))

(define-public crate-pr47-codegen-0.0.1 (c (n "pr47-codegen") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0x9zqvzx7v9w3y1lhnijk6bzbwvm1b6rjp8ppaf5pyjc928qx4b7")))

(define-public crate-pr47-codegen-0.0.2 (c (n "pr47-codegen") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqznv5nhdkbhl4yild0px9q32wbb7z1pj62syj901cd99pn0wx5")))

