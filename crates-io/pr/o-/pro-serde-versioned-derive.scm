(define-module (crates-io pr o- pro-serde-versioned-derive) #:use-module (crates-io))

(define-public crate-pro-serde-versioned-derive-1.0.0 (c (n "pro-serde-versioned-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17xbx6r0ryj8jycmflkij32jfw6imv4gikrwj7ljy9fbi3jzxn7j")))

(define-public crate-pro-serde-versioned-derive-1.0.1 (c (n "pro-serde-versioned-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06z6vymlffjbixz6h8g3sk86kxrbxfj95sw4nzap0bksz2khcpq5")))

(define-public crate-pro-serde-versioned-derive-1.0.2 (c (n "pro-serde-versioned-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "049nb2d929hlc3shza6fhkzhk2gmxdja0y48n68l383d5vrlcy0m")))

