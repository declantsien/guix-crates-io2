(define-module (crates-io pr o_ pro_metadata) #:use-module (crates-io))

(define-public crate-pro_metadata-3.0.0-rc2 (c (n "pro_metadata") (v "3.0.0-rc2") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("from"))) (k 0)) (d (n "impl-serde") (r "^0.3.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pro_prelude") (r "^3.0.0-rc2") (k 0)) (d (n "pro_primitives") (r "^3.0.0-rc2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tetsy-scale-info") (r "^0.5.1") (f (quote ("derive" "serde"))) (k 0)))) (h "0ah5ql966xsybj6rphqnqpmqskhvx1wcq95kfh4m6hrsmy5ywbq5") (f (quote (("std" "pro_prelude/std" "serde/std" "tetsy-scale-info/std") ("derive") ("default" "std" "derive"))))))

