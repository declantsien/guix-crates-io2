(define-module (crates-io pr o_ pro_csv) #:use-module (crates-io))

(define-public crate-pro_csv-0.1.0 (c (n "pro_csv") (v "0.1.0") (h "02kf0pf4hjiswj8zagpa1g396djpcyz7gwrvfl6262js5z090ci8")))

(define-public crate-pro_csv-0.1.1 (c (n "pro_csv") (v "0.1.1") (h "04lcn066id64m77wczghdmlmg5a63i943a75q2x2s8dn3vwmhbx9")))

