(define-module (crates-io pr o_ pro_storage_derive) #:use-module (crates-io))

(define-public crate-pro_storage_derive-3.0.0-rc2 (c (n "pro_storage_derive") (v "3.0.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0z91zj0f4pk9j8rqpgshkylvr29l2p3bhd8cnjqm7f7bf9lxwf61")))

