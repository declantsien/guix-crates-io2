(define-module (crates-io pr o_ pro_primitives) #:use-module (crates-io))

(define-public crate-pro_primitives-3.0.0-rc2 (c (n "pro_primitives") (v "3.0.0-rc2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "funty") (r "=1.1.0") (d #t) (k 0)) (d (n "pro_prelude") (r "^3.0.0-rc2") (k 0)) (d (n "scale") (r "^2.0.1") (f (quote ("derive" "full"))) (k 0) (p "tetsy-scale-codec")) (d (n "tetsy-scale-info") (r "^0.5.1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0mphi5jsk0lp5hf8z63zqcd5060jlmkw1xl6wjsq8md91iq1kj26") (f (quote (("std" "pro_prelude/std" "scale/std" "tetsy-scale-info/std") ("default" "std"))))))

