(define-module (crates-io pr o_ pro_lang_macro) #:use-module (crates-io))

(define-public crate-pro_lang_macro-0.0.0 (c (n "pro_lang_macro") (v "0.0.0") (h "1sk2y0z76qzi0j16v5i0kzkiwpydx99rychngby48kxw5jpfm63p")))

(define-public crate-pro_lang_macro-3.0.0-rc2 (c (n "pro_lang_macro") (v "3.0.0-rc2") (d (list (d (n "funty") (r "=1.1.0") (d #t) (k 2)) (d (n "pro_lang_codegen") (r "^3.0.0-rc2") (k 0)) (d (n "pro_lang_ir") (r "^3.0.0-rc2") (k 0)) (d (n "pro_primitives") (r "^3.0.0-rc2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "scale") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tetsy-scale-info") (r "^0.5.1") (f (quote ("derive"))) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "10nhwxx0qlsf9nky1wsr510p0rnd2rdig5k20cfwlm91mqwdfwdr") (f (quote (("std" "scale/std" "pro_lang_ir/std" "pro_primitives/std") ("default" "std"))))))

