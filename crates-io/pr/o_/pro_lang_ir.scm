(define-module (crates-io pr o_ pro_lang_ir) #:use-module (crates-io))

(define-public crate-pro_lang_ir-0.0.0 (c (n "pro_lang_ir") (v "0.0.0") (h "1n34g9c23jiqrjcglh18n9i6ll6565p6pm2a6fyh301q0am1m35p")))

(define-public crate-pro_lang_ir-3.0.0-rc2 (c (n "pro_lang_ir") (v "3.0.0-rc2") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0y61xjf3xbzi8v07a68mc9zmsfcq5agghc1a1b99fsds0j4g58wc") (f (quote (("std" "itertools/use_std" "either/use_std") ("default" "std"))))))

