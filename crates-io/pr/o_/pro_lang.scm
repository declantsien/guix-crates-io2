(define-module (crates-io pr o_ pro_lang) #:use-module (crates-io))

(define-public crate-pro_lang-0.0.0 (c (n "pro_lang") (v "0.0.0") (h "0sjw7i3nm80ngvij8h6lybsvyi3a7znn4jwx415a7s060lwvygdf")))

(define-public crate-pro_lang-3.0.0-rc2 (c (n "pro_lang") (v "3.0.0-rc2") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("from"))) (k 0)) (d (n "funty") (r "=1.1.0") (d #t) (k 0)) (d (n "pro_env") (r "^3.0.0-rc2") (k 0)) (d (n "pro_lang_macro") (r "^3.0.0-rc2") (k 0)) (d (n "pro_metadata") (r "^3.0.0-rc2") (o #t) (k 0)) (d (n "pro_prelude") (r "^3.0.0-rc2") (k 0)) (d (n "pro_primitives") (r "^3.0.0-rc2") (k 0)) (d (n "pro_storage") (r "^3.0.0-rc2") (k 0)) (d (n "scale") (r "^2.0.1") (f (quote ("derive" "full"))) (k 0) (p "tetsy-scale-codec")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0jag439n51cl4qddipfjxd9kwnza95cjxg1hq10vah7xndwviqh3") (f (quote (("std" "pro_metadata" "pro_metadata/std" "pro_prelude/std" "pro_primitives/std" "pro_env/std" "pro_storage/std" "pro_lang_macro/std" "scale/std") ("default" "std"))))))

