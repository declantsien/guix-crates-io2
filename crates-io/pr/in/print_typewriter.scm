(define-module (crates-io pr in print_typewriter) #:use-module (crates-io))

(define-public crate-print_typewriter-0.1.0 (c (n "print_typewriter") (v "0.1.0") (h "03gganwzfja0pxllhabf62vf4i5a2ki7xnfnmwichycpqv1s7hia")))

(define-public crate-print_typewriter-0.1.1 (c (n "print_typewriter") (v "0.1.1") (h "1r446i54i3sz4hrmfwc85mganz3hlsj2j548c8yxdvwxqxxf5imj")))

(define-public crate-print_typewriter-0.1.2 (c (n "print_typewriter") (v "0.1.2") (h "0ki2yq19fykgv6qrhbpdrais7hqci74wvpxhxdgdg14v16d79is6")))

(define-public crate-print_typewriter-0.1.3 (c (n "print_typewriter") (v "0.1.3") (h "06k1z4vwn99g0f4h79jh36w2h2yyv0bznv1zxw8vprskh6ninf0j")))

(define-public crate-print_typewriter-0.1.4 (c (n "print_typewriter") (v "0.1.4") (h "1iha6lawgn7yqdafxw6k32w5xiyf8nbcfmn3i05v8zn2lcyri008")))

