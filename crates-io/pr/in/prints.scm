(define-module (crates-io pr in prints) #:use-module (crates-io))

(define-public crate-prints-0.1.0 (c (n "prints") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset"))) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0ys3q9h8arp7j7594cndicw4p3qgyyqspj2q2iayky622lgaba59")))

