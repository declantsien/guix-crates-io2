(define-module (crates-io pr in println_logger) #:use-module (crates-io))

(define-public crate-println_logger-0.1.0 (c (n "println_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0q2hrmvqg2x2yk100a4g1l45hjfxqz94g4683k2wm81lcimvivyr")))

(define-public crate-println_logger-0.2.0 (c (n "println_logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p2hdkwsdw8kvaivby8q1frnhwla0nxq0ik9d4raf5jff8gnfsn9")))

