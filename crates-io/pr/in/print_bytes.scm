(define-module (crates-io pr in print_bytes) #:use-module (crates-io))

(define-public crate-print_bytes-0.1.0 (c (n "print_bytes") (v "0.1.0") (d (list (d (n "os_str_bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05lfxdr111jrzsz1hqa8i1sn6pyklmh6fxay8wr5khng10662mkn") (f (quote (("specialization") ("os_str" "os_str_bytes") ("default" "os_str") ("const_generics"))))))

(define-public crate-print_bytes-0.2.0 (c (n "print_bytes") (v "0.2.0") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07dbgyd3jygbs253ni5h5f56iv7jc0x0gjnc6byx9p8y17i25177") (f (quote (("specialization") ("const_generics"))))))

(define-public crate-print_bytes-0.3.0 (c (n "print_bytes") (v "0.3.0") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05a68nlnixyhi5cm8wj3n9g9ayi86f9f5dk2p1k8ylwk48qz2gni") (f (quote (("specialization") ("const_generics")))) (y #t)))

(define-public crate-print_bytes-0.3.1 (c (n "print_bytes") (v "0.3.1") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bw7inbpbvbljk0l4sbi2dpajr50bkm7pj47g9gxljiw89rcrxd6") (f (quote (("specialization") ("const_generics"))))))

(define-public crate-print_bytes-0.4.0 (c (n "print_bytes") (v "0.4.0") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kigwihiffnn4l69j9gfzhbkzpnfv9wwlqvdwmqb3qi0y9dq3i36") (f (quote (("specialization") ("min_const_generics") ("const_generics"))))))

(define-public crate-print_bytes-0.4.1 (c (n "print_bytes") (v "0.4.1") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v8632iysm8xklq37dl1wby0x8wwhnhf6yibjgwzkbfswbial1pr") (f (quote (("specialization") ("min_const_generics") ("const_generics"))))))

(define-public crate-print_bytes-0.4.2 (c (n "print_bytes") (v "0.4.2") (d (list (d (n "os_str_bytes") (r "^2.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zmvbaxfl4r780j0smg2kn1q3agks601aa43s5zdlzgcp32yjfvm") (f (quote (("specialization") ("min_const_generics") ("const_generics"))))))

(define-public crate-print_bytes-0.5.0 (c (n "print_bytes") (v "0.5.0") (d (list (d (n "os_str_bytes") (r "^4.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d4i9y3jx1chi6w97a8rgdbwm9g3cppr53rw53zl6fcaq31qx0b6") (f (quote (("specialization"))))))

(define-public crate-print_bytes-0.6.0 (c (n "print_bytes") (v "0.6.0") (d (list (d (n "os_str_bytes") (r "^6.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mxcs9m21rpmlhj43pmq5ikaaxrggyy6l1xv0l1lfz36hrlf6mrh") (f (quote (("specialization")))) (r "1.57.0")))

(define-public crate-print_bytes-0.7.0 (c (n "print_bytes") (v "0.7.0") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0viw3xxc4czixajxb05sqzic8w92583sji3x6rnphbdy0wkkn292") (f (quote (("specialization")))) (r "1.57.0")))

(define-public crate-print_bytes-0.6.1 (c (n "print_bytes") (v "0.6.1") (d (list (d (n "os_str_bytes") (r "^6.0") (d #t) (k 2)) (d (n "print_bytes2") (r "^0.7") (d #t) (k 0) (p "print_bytes")) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02vgvyvn3vakz6jxjx0l0v77jqnfk6f9jfsyk7nqjk04nq7rbf8l") (f (quote (("specialization" "print_bytes2/specialization")))) (y #t) (r "1.57.0")))

(define-public crate-print_bytes-0.6.2 (c (n "print_bytes") (v "0.6.2") (d (list (d (n "os_str_bytes") (r "^6.0") (d #t) (k 2)) (d (n "print_bytes2") (r "^0.7") (d #t) (k 0) (p "print_bytes")) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p40w9x7cgmvma93d8z1pqlscxbgqf2npwi224pbk4hliz91jnvr") (f (quote (("specialization" "print_bytes2/specialization")))) (r "1.57.0")))

(define-public crate-print_bytes-1.0.0 (c (n "print_bytes") (v "1.0.0") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qqx679hbd5dj1rsx843mxfcdiz9v5qcb7qzh1wp318k2s9mqwmr") (f (quote (("specialization")))) (r "1.57.0")))

(define-public crate-print_bytes-0.7.1 (c (n "print_bytes") (v "0.7.1") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "print_bytes2") (r "^1.0") (d #t) (k 0) (p "print_bytes")))) (h "09qlcf6iiadg8snmpbv43h8471mmqx4nhyksnm6ci1p7ygv1ch19") (f (quote (("specialization" "print_bytes2/specialization")))) (r "1.57.0")))

(define-public crate-print_bytes-1.1.0 (c (n "print_bytes") (v "1.1.0") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zv83dpzafbyiqml4hjhciwpgh3kv1lg7wpwakiswmrw96s2m9vx") (f (quote (("specialization")))) (r "1.63.0")))

(define-public crate-print_bytes-1.1.1 (c (n "print_bytes") (v "1.1.1") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mra084w54dynb4p1660ziida5vjr94962ky2q3ximwbfsgsakr8") (f (quote (("specialization")))) (r "1.63.0")))

(define-public crate-print_bytes-1.2.0 (c (n "print_bytes") (v "1.2.0") (d (list (d (n "os_str_bytes") (r "^6.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a1pc0bs0f3mgy86mazdy81k6iaj8s23ly49mcka4in2pj27lzxl") (f (quote (("specialization")))) (r "1.63.0")))

(define-public crate-print_bytes-1.3.0 (c (n "print_bytes") (v "1.3.0") (d (list (d (n "os_str_bytes") (r "^7.0") (o #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18qf34b1801w0girkz4gczpqqz8xsqiyh0s5nqb3almkpqls6isk") (f (quote (("specialization")))) (y #t) (r "1.74.0")))

(define-public crate-print_bytes-2.0.0 (c (n "print_bytes") (v "2.0.0") (d (list (d (n "os_str_bytes") (r "^7.0") (o #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10fgl4jqchhqdyrgla7ijgkfw8r57bq41i6g23d1xmxppnbxyp7l") (f (quote (("specialization")))) (r "1.74.0")))

