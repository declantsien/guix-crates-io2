(define-module (crates-io pr in print-flat-tree) #:use-module (crates-io))

(define-public crate-print-flat-tree-1.0.0 (c (n "print-flat-tree") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "flat-tree") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.0") (d #t) (k 0)))) (h "1yssyqcxkam23ikgy7q09k1fgxx3inyi83gq3zbvkp8k1abr5yyx")))

(define-public crate-print-flat-tree-1.1.0 (c (n "print-flat-tree") (v "1.1.0") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "flat-tree") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.0") (d #t) (k 0)))) (h "13awvz96pdmqwvhw4gad58lyfggb65nl8dvybmnn0m6f55hxfdmx")))

(define-public crate-print-flat-tree-1.1.1 (c (n "print-flat-tree") (v "1.1.1") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "flat-tree") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.0") (d #t) (k 0)))) (h "1prkwsx5pgk5vl0j6jd0hhjqf560hl9vflcm165l5x872p7lhzbc")))

(define-public crate-print-flat-tree-1.1.2 (c (n "print-flat-tree") (v "1.1.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "flat-tree") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.0") (d #t) (k 0)))) (h "1cgv4syfnx1hnylix12fp19d02mm7rhhshbgkpsadcvamrjrw4xd")))

(define-public crate-print-flat-tree-1.1.3 (c (n "print-flat-tree") (v "1.1.3") (d (list (d (n "flat-tree") (r "^3.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.0") (d #t) (k 0)))) (h "03l6bijrk4gn47hj3nwsn3z6kg0zgfb7r5a0k8fxx3mpda44k65z")))

