(define-module (crates-io pr in print_service) #:use-module (crates-io))

(define-public crate-print_service-0.1.0 (c (n "print_service") (v "0.1.0") (h "0jz6xpidw3m3vyn5s55scz9javmgbh2jw87jhywjcpcs1bcdwv57")))

(define-public crate-print_service-0.1.1 (c (n "print_service") (v "0.1.1") (h "0zx3vn9h4yqrh8zy1mbgspmzn2wz809yy1ck7ix7im7lzd7sc7dj")))

(define-public crate-print_service-0.1.2 (c (n "print_service") (v "0.1.2") (h "1l88hiq40zn9lk6kyvxakqhmmz3mksnnvh9xx7v0f0wi8cxllhz7")))

(define-public crate-print_service-0.1.3 (c (n "print_service") (v "0.1.3") (h "1y09vxn34hmijhw9s1raf5w59fyfcw1r5jgpswgr2phlvkcczcmw")))

(define-public crate-print_service-0.1.4 (c (n "print_service") (v "0.1.4") (h "0zi7hqw6sdm5fnvnd221g518zvj5rzwhlfgrmn78sby602sd3xkl")))

