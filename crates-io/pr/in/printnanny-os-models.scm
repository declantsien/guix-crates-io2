(define-module (crates-io pr in printnanny-os-models) #:use-module (crates-io))

(define-public crate-printnanny-os-models-0.5.0 (c (n "printnanny-os-models") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "06c8wfrm1dig38sb81rpljb3nn4pq53mv1rrj17bxfykf2xwqvgc") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-os-models-0.1.0 (c (n "printnanny-os-models") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0l01zmnrzwqplh79qyy939gxj6mnpkysv8wwlfbhc43vs5yypd5i") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-os-models-0.1.1 (c (n "printnanny-os-models") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "068abij7www0yg7sf9rxsjwhfgrmb5zllk7v7pbiw62jv54i11zg") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

