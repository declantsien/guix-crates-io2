(define-module (crates-io pr in printrn) #:use-module (crates-io))

(define-public crate-printrn-0.1.0 (c (n "printrn") (v "0.1.0") (h "1l8mwqyn8rv1sk5pnw78hnhbf353kg75wcz8klbrkrva7siqz648")))

(define-public crate-printrn-0.1.1 (c (n "printrn") (v "0.1.1") (h "0br48ls0frysz5pczv3ik04wqdxmw79i8zmcm8q2jq0p7vnzf5hz")))

(define-public crate-printrn-0.1.3 (c (n "printrn") (v "0.1.3") (h "0240z34c24h64xjk2rpnsv91h1sq3c9a0gcbx74rcc6wjcdqv6aq")))

(define-public crate-printrn-0.1.4 (c (n "printrn") (v "0.1.4") (h "1sdljb4j0rxl1fk7060pdi52dnm0sx2xahsxrv5k8492ld68x3v1")))

(define-public crate-printrn-0.1.5 (c (n "printrn") (v "0.1.5") (h "1f7nx2yrgsh9y4hy9jj5fnbabbsb7i7dk5k5kmgr56p646572cxm")))

