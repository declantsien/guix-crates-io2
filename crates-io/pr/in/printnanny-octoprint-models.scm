(define-module (crates-io pr in printnanny-octoprint-models) #:use-module (crates-io))

(define-public crate-printnanny-octoprint-models-0.1.0 (c (n "printnanny-octoprint-models") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0nvzfgngxvpqnr3gnh69ih7indw9lrz4wb2413njqjhw0wydka8h") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.1 (c (n "printnanny-octoprint-models") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "00chrjccp38w4kf6as581nhj5vaykm2i1w08a20bgzc4pd0lcj28") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.2 (c (n "printnanny-octoprint-models") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0hzj78xw3gbxh0vzj9mqkbyaajpxphj4hr4i9bgmkqxm1f6insl4") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.3 (c (n "printnanny-octoprint-models") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0nwl8hwyfygf1s7h1668f1zmg3vv22yfqdnk8qwfvxwci00ld668") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.4 (c (n "printnanny-octoprint-models") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "14p763cb140w84r5620l8b7s59pmgj9lrahrrnbnbm9a9w3lsvwm") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.5 (c (n "printnanny-octoprint-models") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "17pw1ygm5413mgra028dl24mvvl71jwc3x59dwd82afawams12r2") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.6 (c (n "printnanny-octoprint-models") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0adpamvm7sxbgq8q20yaf3bc9d4cpks8xn46krcsakmk2fqmwsqc") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.7 (c (n "printnanny-octoprint-models") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0ynqbdv7igpazqa9adabrb6bachryy2bflb47rg0db3b26ypbcrd") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.8 (c (n "printnanny-octoprint-models") (v "0.1.8") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "05cfi5x59fz05zsh40smnfjyqiiq03lifvdqgh06g5rfdgrmpsny") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-octoprint-models-0.1.9 (c (n "printnanny-octoprint-models") (v "0.1.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "07s1vqrcr8h1rc9ximhypgihqiygiflg2l87dr8aszc9w696v0vj") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

