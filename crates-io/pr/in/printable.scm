(define-module (crates-io pr in printable) #:use-module (crates-io))

(define-public crate-printable-0.0.1 (c (n "printable") (v "0.0.1") (h "1njgscbvl5azx2y4hbwhp1lkrl4bkbl8havrsm7dbmfdsffampk6")))

(define-public crate-printable-0.1.0 (c (n "printable") (v "0.1.0") (h "0xbwmmch86665rq8ir713d2jyjxcwv53vjfx30cahzwmvsj17ihi") (f (quote (("unstable-assert-no-drop")))) (y #t)))

(define-public crate-printable-1.0.0 (c (n "printable") (v "1.0.0") (h "1vd6dynizf2lvzp00b8banckkb2g9n12xghav6v8x63b254vq84x") (f (quote (("unstable-assert-no-drop"))))))

(define-public crate-printable-1.0.1 (c (n "printable") (v "1.0.1") (h "1s3vrha5pqg8g545a3giicgfgpya9s65hhn0nw1n6via8ki45sc5") (f (quote (("unstable-assert-no-drop"))))))

(define-public crate-printable-2.0.0 (c (n "printable") (v "2.0.0") (h "1g40c9jiv404lrzf59kp7bp2d6xmpzz0ydd9gp0zw4lvxjl6izl9") (f (quote (("unstable-assert-no-drop"))))))

(define-public crate-printable-2.1.0 (c (n "printable") (v "2.1.0") (h "0f3znlhrxyr16x6cnrxmja7sf9g4pqlfz7g44b3vnmfagdpi5ywm") (f (quote (("unstable-assert-no-drop"))))))

(define-public crate-printable-2.2.0 (c (n "printable") (v "2.2.0") (h "0c2j8bmz7bz437gi70xvw1v6mn5jgncc7yffckzalwdm0s184lq3") (f (quote (("unstable-assert-no-drop"))))))

