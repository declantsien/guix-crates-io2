(define-module (crates-io pr in print_queues) #:use-module (crates-io))

(define-public crate-print_queues-1.0.0 (c (n "print_queues") (v "1.0.0") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "0xvnw24y5g0s5bf463s0v46qam8kdq8mnajz247jza41mhmx8wz9") (y #t)))

(define-public crate-print_queues-1.0.1 (c (n "print_queues") (v "1.0.1") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "1x8dkycgqpfiazy8bmfx7hmw0262lbsswxy5rnnhm181dncakasg") (y #t)))

(define-public crate-print_queues-1.0.2 (c (n "print_queues") (v "1.0.2") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "1cqfnb57prbkb9ky6bllbyqa499nr3pfmmpjjyr13vlw454qi121") (y #t)))

(define-public crate-print_queues-1.0.3 (c (n "print_queues") (v "1.0.3") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "0q02dxyd9n4wkkix20x5z1pgc4szriqh06m50s74v64rr268c12p")))

(define-public crate-print_queues-1.0.4 (c (n "print_queues") (v "1.0.4") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "0idw0xc2vy3jaxcy9xj7a18x0dz72c1wq507hmkjafryslmhavb5")))

(define-public crate-print_queues-1.0.5 (c (n "print_queues") (v "1.0.5") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "1bqcqavxvsdpaz8sp9nv5saxan82ajxmnrbms4acfji0bbfhcf61")))

(define-public crate-print_queues-1.0.6 (c (n "print_queues") (v "1.0.6") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "1zk1zzl91zbibwa2hsqm98vhjk1vr6ahvpd44g8wv9001y9l35rj")))

(define-public crate-print_queues-1.0.7 (c (n "print_queues") (v "1.0.7") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "1llpilly3f2wm59if80p6c4nb9sdgydb4h7xzz7cbaiqdafbzjm2")))

(define-public crate-print_queues-1.0.8 (c (n "print_queues") (v "1.0.8") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "19syg02rn9biyb7rsl2gaini84q79manhr84qkj1y5nf6hlpp1m0")))

(define-public crate-print_queues-1.0.9 (c (n "print_queues") (v "1.0.9") (d (list (d (n "queues") (r "^1.1.0") (d #t) (k 0)))) (h "0r4v8gsscj5vngy2nw23yr8jqffspsjn2v4zyhg8chpkwsslrqwj")))

