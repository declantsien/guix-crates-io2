(define-module (crates-io pr in printnanny-snapshot) #:use-module (crates-io))

(define-public crate-printnanny-snapshot-0.1.1 (c (n "printnanny-snapshot") (v "0.1.1") (d (list (d (n "printnanny-settings") (r "^0.7") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "13h4zcs30vvq9mqqa4ccil0sa3k56galld8mv5xnai47p4s36sjv") (r "1.66")))

