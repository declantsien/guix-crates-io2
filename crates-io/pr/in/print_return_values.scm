(define-module (crates-io pr in print_return_values) #:use-module (crates-io))

(define-public crate-print_return_values-0.1.0 (c (n "print_return_values") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0h64a458m9d55qq6h00jpfgmgnscnwykkml5lsiglybn8p2yx5mv")))

(define-public crate-print_return_values-0.2.0 (c (n "print_return_values") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "03xr4px4kv7spjgxhi7lcdi3j5nwpczr94ad0lg6zbfn4j8c0b5m")))

