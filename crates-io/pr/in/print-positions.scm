(define-module (crates-io pr in print-positions) #:use-module (crates-io))

(define-public crate-print-positions-0.5.0 (c (n "print-positions") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "arbitrary") (r "^1.2.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0x6krng312vz6m728i5a2i6z4fjf38gh1sx5phpap3s20hq96i6p") (y #t)))

(define-public crate-print-positions-0.5.1 (c (n "print-positions") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "arbitrary") (r "^1.2.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1lmv85bzvfvxm22hccxphgkd6llhrl0dn4nadi4121sa9zp9ilp0") (y #t)))

(define-public crate-print-positions-0.5.2 (c (n "print-positions") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "arbitrary") (r "^1.2.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0nzpb1fxg82gyvn4m24ic539sd3xxxikidylbz58r63d38p7jz23") (y #t)))

(define-public crate-print-positions-0.6.0 (c (n "print-positions") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "arbitrary") (r "^1.2.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0drn722blcdyl4lkssrhli16444kmi62fqwgzqwwn0570mdmkpjx") (y #t)))

(define-public crate-print-positions-0.6.1 (c (n "print-positions") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "026jzdf63b37bb9ix3mpczln2pqylsiwkkxhikj05x9y1r3r7x8x")))

