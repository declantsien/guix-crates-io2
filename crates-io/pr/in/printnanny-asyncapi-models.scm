(define-module (crates-io pr in printnanny-asyncapi-models) #:use-module (crates-io))

(define-public crate-printnanny-asyncapi-models-0.1.0 (c (n "printnanny-asyncapi-models") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "08xqa9mkxh39qw6csp4yjxls3kl0yp85gba01cy3vqnas4xlili6") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.1 (c (n "printnanny-asyncapi-models") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0yv6q3i8i5hda3difqbpjswcglsycgwh0srpakk79a70ad3alcss") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.2 (c (n "printnanny-asyncapi-models") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0i5fy1iay8989kqs9iz0756plazn29yqaq2ajnhx8vv8l8c38fvp") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.3 (c (n "printnanny-asyncapi-models") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1sa8m03mpdh76cd4af4iqinlixkskdr1lhnrfvimz7jag14f9cgc") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.4 (c (n "printnanny-asyncapi-models") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0l5x0csmzs12jzy1bzcyl14qn5db45yqy5fx5nqgi8g01c5m34j1") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.5 (c (n "printnanny-asyncapi-models") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0s7zc91wi0mqja90jvc9ys2ybxi51rk4sv4p0k9s5z65if60fj5w") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.6 (c (n "printnanny-asyncapi-models") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0as03h0fgkib9zivzks1ih2knnb9g25vz3106id20v4x9p7zvmp6") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.7 (c (n "printnanny-asyncapi-models") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "09ml914h4bla93mpv3k7gjkdcq97pgcrr4h0hw08zggdfaklddih") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.8 (c (n "printnanny-asyncapi-models") (v "0.1.8") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "141q37js22a6xnz3bcgq40a2mcwak4axs5yl5slp06gyx92d22hs") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.9 (c (n "printnanny-asyncapi-models") (v "0.1.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0hn01qxzhwl0f2czn4n59rrvgz7ffppydq412hdihx0z4dkh21da") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.10 (c (n "printnanny-asyncapi-models") (v "0.1.10") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0z5zb65dip5snr2vdaxf4bizjia7kdgzj7sm2hq69kc079p33kl6") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.11 (c (n "printnanny-asyncapi-models") (v "0.1.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0hnayifh53pmq8sk31lnzibsysgkgvcx78vdyl59lfarv7ac1j0a") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.12 (c (n "printnanny-asyncapi-models") (v "0.1.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "069cdw8300m2gkd7yiv9iycq5aas9q794zxwqj2lpbmf7jyvx83j") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.13 (c (n "printnanny-asyncapi-models") (v "0.1.13") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "183a30icprrw9n2lnfmihgwvcsbcid6nifcrv75fv1l5xhl7g8nr") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.14 (c (n "printnanny-asyncapi-models") (v "0.1.14") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "16cp87p1y6fvfw1hyk0kkcckl88v16kkck4vqgmfk34r3iai1qss") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.15 (c (n "printnanny-asyncapi-models") (v "0.1.15") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1nffrigwynyc4f0jhawq9k6h7xsrc8lvrnjc3w37f6wjk9i1k2mx") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.19 (c (n "printnanny-asyncapi-models") (v "0.1.19") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0xhl3p5sf4946sppq9xxz9wh421zvxcvxyjdklf23fcn5g2m9y46") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.20 (c (n "printnanny-asyncapi-models") (v "0.1.20") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1wlknpqappvj491wjb5g9ylq8fgx42gacsi533bns80wc2gycv59") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.21 (c (n "printnanny-asyncapi-models") (v "0.1.21") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0fni1792w24xpq963b1q1q5q15ryi6xirvpbfj50k2hyc910wwb5") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.22 (c (n "printnanny-asyncapi-models") (v "0.1.22") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1p3c286ihs6qqhis1g4c4g30zpi9f9h9j239xws3jmnbfv62crgs") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.23 (c (n "printnanny-asyncapi-models") (v "0.1.23") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1yyx663a39rs161zvr07b7g9ppq78ixdjkw06437140k1s8xnavw") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.24 (c (n "printnanny-asyncapi-models") (v "0.1.24") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "17f7fnn00csr6xb9yjaklaviqsanvzls0pyzr4lma8nm6fmz4cxy") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.25 (c (n "printnanny-asyncapi-models") (v "0.1.25") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0c3yrc6z1lh6ra9sm3azqkz8hlavi436v4ri43h5jg90kg3faj1m") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.26 (c (n "printnanny-asyncapi-models") (v "0.1.26") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "14ram9c6wig9x59yilq542kgsbrvrpmwammf7zzhz0gccv5621q1") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.27 (c (n "printnanny-asyncapi-models") (v "0.1.27") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1kfj53pa9q7rg8rbvr998jmfvaznwdddz0ypw0frqw8asi0g6886") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.28 (c (n "printnanny-asyncapi-models") (v "0.1.28") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "09c90c4sqzrnymacykd6rrb24b3ndvclf58bvmsq5hqgyqgns14q") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.29 (c (n "printnanny-asyncapi-models") (v "0.1.29") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0iw08mdmxfq7g8mivz9vlmk1z1n9iwgyj6z93l4lh6qqi161pgxr") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.30 (c (n "printnanny-asyncapi-models") (v "0.1.30") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0nk94bnhhw8f3ika2h6rzxqykq497q5wzcdbjr9kn5dw8nfhaypi") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.31 (c (n "printnanny-asyncapi-models") (v "0.1.31") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0h6p277hx66riz7mgyqbriknm7bjfjvdip79nqypc8za7ci7ywj5") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.32 (c (n "printnanny-asyncapi-models") (v "0.1.32") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1krlvq23cnl5x1azx1lp4bqxyhjbr5w04al2vz3kk3bc6hvhykl1") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.33 (c (n "printnanny-asyncapi-models") (v "0.1.33") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "17cg90pskwafi6i5gb496872j728dh9a48pf95a4j6p2qjhjm5vi") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.34 (c (n "printnanny-asyncapi-models") (v "0.1.34") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1dzv7gsg9ii7mnkbik1gkdsfyx9xq0nyjckzd67xvcz4saaqf6xj") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.35 (c (n "printnanny-asyncapi-models") (v "0.1.35") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1kkphf08h9lk1b88klhdbd3rg9n4c0cw8nbxfz42jb2han5442av") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.36 (c (n "printnanny-asyncapi-models") (v "0.1.36") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0rmh4kgs749qygsbvyj9vniscly2p27y1zdqfvqqk8rnw6v27vyj") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.37 (c (n "printnanny-asyncapi-models") (v "0.1.37") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "00k8wm3q1w0s093qn120lz3nvv4ylny6naxwwm2a7hgkld92pp44") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.38 (c (n "printnanny-asyncapi-models") (v "0.1.38") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1p6klqzr661q3g4c71mw0fpmp098nzv6scm88liq60iipggpipzy") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.39 (c (n "printnanny-asyncapi-models") (v "0.1.39") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0b89cc6vyz4vdyixd3hw6azm0ayg7kcf7fv1dhf3g60mvcn7w6vs") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.40 (c (n "printnanny-asyncapi-models") (v "0.1.40") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "08hbwp2ldnqfxhrm3s3kap8a21imqs9l2y49sgfbwvq6ifab6q3r") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.41 (c (n "printnanny-asyncapi-models") (v "0.1.41") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0rdp898asagqjdwjbm8pj2jpxi7d3rr3xlxfp4s7fyl2bq0fg9j7") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.42 (c (n "printnanny-asyncapi-models") (v "0.1.42") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0b7s9r2fh2p1irmnq9k3jyvx06qagkdc8w4gg7bhlv6d7f0n7902") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.43 (c (n "printnanny-asyncapi-models") (v "0.1.43") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1zm6jwnbhy4y1zkm290b8hpsdbb25axhwqa41wdxxq72iqxaic14") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.44 (c (n "printnanny-asyncapi-models") (v "0.1.44") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0z5x2xx6q8qgf77kxalp2ir7cmfwfk31rraia1srkihgalcaj9mi") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.45 (c (n "printnanny-asyncapi-models") (v "0.1.45") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1hg4q9zk8smgjywfzjgxdxq90qhvz6wwvzq6lf05srgbqy5h4pgn") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.46 (c (n "printnanny-asyncapi-models") (v "0.1.46") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1dzg504wk4p8jx8p1lzdqqrcbd38l01rpxaw6xcbfm0fyd8pc52i") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.47 (c (n "printnanny-asyncapi-models") (v "0.1.47") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "16fiw75gn8417zxk8j7vcspmag2fxbrrnh1z6jvwdfmvirsn1x87") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.48 (c (n "printnanny-asyncapi-models") (v "0.1.48") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "078j6pdc2xa5crrlkpvb8ipgvlrfyhiplzh8jgnw3ii7d8383fa1") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.49 (c (n "printnanny-asyncapi-models") (v "0.1.49") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0cvzx2h9dcfz9m5vsvrc7qfnlx52dnh2jl6j9w0wfm0h0wlfngy7") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.50 (c (n "printnanny-asyncapi-models") (v "0.1.50") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1fsnsmbh7p7sbb4242rppz08smxcys4zfcs4rg2chdi1q2zb83k9") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.51 (c (n "printnanny-asyncapi-models") (v "0.1.51") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1ra1h7nz9yjix1g6q9z8gr5hwcq1pslf3kjs0kxf5pnx5qwjrbg2") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.52 (c (n "printnanny-asyncapi-models") (v "0.1.52") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1d2psphbdhvq1429fic6km4kfml62h1kk6rrlsidzxlmym95m0cs") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.53 (c (n "printnanny-asyncapi-models") (v "0.1.53") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "11cnwpd8vbhynr95zs9zmn3b3g4d580r3svqvgvjcvfckpgg8lyl") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.1.54 (c (n "printnanny-asyncapi-models") (v "0.1.54") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1pa641f5hgk0zin9rlk3dvwkh2ssvvzfilj121g79py5iqxa00cx") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.0 (c (n "printnanny-asyncapi-models") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0skw3hjg9fqrx8gcwh6b4ac1lav8bn9w4kw1ar4wawr1ggxql6v5") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.1 (c (n "printnanny-asyncapi-models") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "00pxbnl3x51kicahfry4qa2yn6xf6vkh62iiy7xkcpcgv489rrwz") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.2 (c (n "printnanny-asyncapi-models") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0kvhlvnc4rysmhk10cbh8s171bmbi557hr41w1v69srm017kp5lf") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.3 (c (n "printnanny-asyncapi-models") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1yxnv4c1l6ylfq9mx2b1b2znfginlsxalb9w1x1a87pd1pa8k7gw") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.4 (c (n "printnanny-asyncapi-models") (v "0.2.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "11y0vqnn1nk3k5jq8jrcrcv4aq46g3p4vw1b6cjwdg62ls58p4vv") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.5 (c (n "printnanny-asyncapi-models") (v "0.2.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "18ak61mxhk6axz7aa5vhxp16c15xgvvfvz8sfrcflypfpfhf0pd1") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.6 (c (n "printnanny-asyncapi-models") (v "0.2.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1f8l8j76hm5ds4w8jlzd82wlw4z6n945qzjbq8x89mzp7m6rapra") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.7 (c (n "printnanny-asyncapi-models") (v "0.2.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1ylq4ljqjjpd05w60p4lxk83y8jx248md2mcrysnp9161ynsfb8k") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.8 (c (n "printnanny-asyncapi-models") (v "0.2.8") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1fqlv5yahsjc9qb4vp3lrl3r0ig9vj78inl6bcimvrs22fsx42hk") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.2.9 (c (n "printnanny-asyncapi-models") (v "0.2.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "06q8v4rpdzld3givzn0h083miz5ap7jiik3s1pfbc5cp2p7qqvbf") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.0 (c (n "printnanny-asyncapi-models") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1v9r4aa28916b5ll9bkxqc62dzc31ba40kb8y3j6bg6302byzkj5") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.1 (c (n "printnanny-asyncapi-models") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "16arylhf24zjhf5ra0i2v0ra3n623ram8dpmwddrqspxahh5dmi3") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.2 (c (n "printnanny-asyncapi-models") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0kv76ivkif7pww7x17iqviyprrw1l2xby2z0rnjwr60gw2d9v2an") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.3 (c (n "printnanny-asyncapi-models") (v "0.3.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0d7lqs3rlqcv30flixrbrii7wpx3w44h0lqd1s60c5m3kbmpni87") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.4 (c (n "printnanny-asyncapi-models") (v "0.3.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "19f7a36d463cblg32n0b2pyn7l5png3wh4xwnl73g937n6qcq69c") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.5 (c (n "printnanny-asyncapi-models") (v "0.3.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0fapxsdar6p83fpdav6x7c7xjf10hdm4lv584ianjhi6xqphp6mw") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.6 (c (n "printnanny-asyncapi-models") (v "0.3.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1pipdh93mgiw1cc1ia746pl1kvwcpz48qmbi7hcpdz0rikimwyby") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.7 (c (n "printnanny-asyncapi-models") (v "0.3.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1m734cdl69700i9sgr6agfs91pmc3pcj9m1c88m6kr3v59ykgxwb") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.8 (c (n "printnanny-asyncapi-models") (v "0.3.8") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0fkafywlv55r7r0yi5hi79iryc6wy5r9n1dsckn698ay4snxygmv") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.9 (c (n "printnanny-asyncapi-models") (v "0.3.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0lxfik1shbh41v710dd0b8qgm9mqamk3bci567jch4jfzrxk9x6m") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.10 (c (n "printnanny-asyncapi-models") (v "0.3.10") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "13fclyq1pq2j0d0zsai5mj8nxhdb98p9lhz6hcm3mjxfw35hb3m0") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.11 (c (n "printnanny-asyncapi-models") (v "0.3.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1xbpc2dg760ijks4y5ibdsc4c4s3sjsg993plk2ckfgszcfa9qll") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.12 (c (n "printnanny-asyncapi-models") (v "0.3.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "05sblhvjf9z7y639df18i8yhind5zpirfxgbgb29miq371cdwf8n") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.13 (c (n "printnanny-asyncapi-models") (v "0.3.13") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1z7q6l812l1ycapw13d9sv7aymvj722ywq8vz4n28kyzsvvsnr1j") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.14 (c (n "printnanny-asyncapi-models") (v "0.3.14") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0h89kxscihr2d01j83vb00yllg8z6aj7jrgbdvdjxwacm9gx7vg7") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.15 (c (n "printnanny-asyncapi-models") (v "0.3.15") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "13gzd3s82l7cw5zwg7j84q772ii7mhmgj858qdqdjcldhp94ilpn") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.16 (c (n "printnanny-asyncapi-models") (v "0.3.16") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1cqwfaviqv18jwcqh5lhh2xagwsnj985a2rslnqhsi45qllyrbrf") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.17 (c (n "printnanny-asyncapi-models") (v "0.3.17") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1cj9pi98rzcbbs0pdk291m5nd1ra9gq5ah0102l51kvh2jvgn8rl") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.3.18 (c (n "printnanny-asyncapi-models") (v "0.3.18") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1r8lyzi2pv02bbcmpl4cp2wbmzjjajyxrh9vlb2hinaf7cjafhbw") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.0 (c (n "printnanny-asyncapi-models") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0lj78qcf9npbd5sgapmrl3m5drznlkc81a5iyhczcqzf3542gg4p") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.1 (c (n "printnanny-asyncapi-models") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0sdznhhb62iiszzjcm6nr50znhffkwvpxs66rrk5h10rm2mpd041") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.2 (c (n "printnanny-asyncapi-models") (v "0.4.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0b6qwi3yqf1lhy70jgk1lmimyf9qh4ngh1zxnczw0vq6cj8v0vnf") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.3 (c (n "printnanny-asyncapi-models") (v "0.4.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0shrmkqh9na7m26ha2a6lnk9y4ri4bvlb9qkk3p3pbwxv6r2xzjj") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.4 (c (n "printnanny-asyncapi-models") (v "0.4.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1pds73zvk2w2qaqi7799kx1r1mv2y50g3bxd7z4m862c3n1cjki4") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.5 (c (n "printnanny-asyncapi-models") (v "0.4.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "09h74dvhsgf2hr49cic32rdcjvd3bjv2m7rqkspcab26nbw19vaz") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.6 (c (n "printnanny-asyncapi-models") (v "0.4.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "03c210arv86sclk5ak71pdd8qgsgy5l8wcyjd25nljx44rzcs614") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.4.7 (c (n "printnanny-asyncapi-models") (v "0.4.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1z2lxksbrjl0l35m6bi5skhc698f8jmv29kl42jlczbw5skz8zhg") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-printnanny-asyncapi-models-0.5.0 (c (n "printnanny-asyncapi-models") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "04s3r8pikafw2r8yhgiwzdwq1ja3rf2c40gqms5c1pawh3ynphfw") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde_json"))))))

