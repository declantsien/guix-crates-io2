(define-module (crates-io pr in printk) #:use-module (crates-io))

(define-public crate-printk-0.1.0 (c (n "printk") (v "0.1.0") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "130i43kcf6h4446i9fdvv92wmw568kabjfsk6g13f0y31k1xq1dl")))

(define-public crate-printk-0.1.1 (c (n "printk") (v "0.1.1") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1i3lavjf381czky2jb0hvz07293drh1fzpfknizh3k4mrrf62lzl")))

(define-public crate-printk-0.1.2 (c (n "printk") (v "0.1.2") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0nw7lfqzlai7bgpwn1nqnr2xmql0c0l9vlarm6mwgxbm22h0alcr")))

(define-public crate-printk-0.1.3 (c (n "printk") (v "0.1.3") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1dqc80zwn3a3akf04kn9xxy6jnnm38984d4wjir1km5238lzsgd0")))

(define-public crate-printk-0.1.4 (c (n "printk") (v "0.1.4") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "08qn4i5lfqgaay4lqdlicc9s9yafdfyrsgaxi2s29g7s8nxqszpg")))

(define-public crate-printk-0.1.5 (c (n "printk") (v "0.1.5") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "16c191d00awqv280340xp3gbx5irv2mmiw99g3bl8k8na2lwkxsc")))

(define-public crate-printk-0.1.6 (c (n "printk") (v "0.1.6") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1j4dy2jdagb1j3psbx9ww09p10zzdjk3qqq3mqnw4h9syxmaz51d")))

(define-public crate-printk-0.1.7 (c (n "printk") (v "0.1.7") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1wwal07nc5swqg4yxdpwy0vm0wrjn4py5rwa7n2331yilymkvajd")))

(define-public crate-printk-0.1.8 (c (n "printk") (v "0.1.8") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0975n9bw3lrdizi3rakjl35p175nd7kv41wpplcbh0a81qchy031")))

(define-public crate-printk-0.2.0 (c (n "printk") (v "0.2.0") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1kn6pqzby8d3w9lzbcr0wa6cjlbyl7ngsvap4bsy72cxkp0a0qmz")))

(define-public crate-printk-0.2.1 (c (n "printk") (v "0.2.1") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1ch5v3lj55vnklnzhm248xvcxxz59xpd06b708ld3hpjy388vz07")))

(define-public crate-printk-0.2.2 (c (n "printk") (v "0.2.2") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0sifzjkkbwx5x0zkqsd857x1ngnsy372n69gc36mwdh8p7p81v1a")))

(define-public crate-printk-0.2.3 (c (n "printk") (v "0.2.3") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "15n0m0pqk194bxgdf6pb2qzqp5l7r0kc2hlf5nrjpwaba1q5da99")))

(define-public crate-printk-0.2.4 (c (n "printk") (v "0.2.4") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "00h1b1ms3h9rnm88x5qkdcj8f7kwy0g5bsqz7li2q6lw946mvqvx")))

(define-public crate-printk-0.2.5 (c (n "printk") (v "0.2.5") (d (list (d (n "bootloader") (r "^0.10.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "152fhqc7xsf0dc9243k1s51rwvz4ykhdiqjad04bf3n26gqz6ry2")))

(define-public crate-printk-0.3.0-beta (c (n "printk") (v "0.3.0-beta") (d (list (d (n "bootloader_api") (r "^0.11.0-beta") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1x9xp8dh1f1z81adm7gm7a9x8am88xgzhnx488ciyymb1bcx2zmb")))

(define-public crate-printk-0.3.0 (c (n "printk") (v "0.3.0") (d (list (d (n "bootloader_api") (r "^0.11.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.6") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "18138jcnc5k0vkz5g867ql6hfhyq1gfnbkslqykyqw9bcvf6ng12")))

(define-public crate-printk-0.3.1 (c (n "printk") (v "0.3.1") (d (list (d (n "bootloader_api") (r "^0.11.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.2.0") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0cbzdgd4laxl6xqx7680rjgczd3fzgf5sgwd32ma0zxggngwmmk2")))

(define-public crate-printk-0.3.2 (c (n "printk") (v "0.3.2") (d (list (d (n "bootloader_api") (r "^0.11.0") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.2.0") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "10s4hqjkb68kw98vnp3zlcmynji9fc7r7asvwql9fr39qsr5ml2j")))

