(define-module (crates-io pr in print_image) #:use-module (crates-io))

(define-public crate-print_image-0.1.0 (c (n "print_image") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1r7yw52rn1il2czdm14xqrqdg6ifv43pwdcjc03c3zkfzj8zbadj")))

(define-public crate-print_image-0.1.1 (c (n "print_image") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1s0r2nm6nqz0sfd02w5nzisry71r01jzsg2s5d22929a66gk7pi0")))

(define-public crate-print_image-0.1.2 (c (n "print_image") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "15q6nd2xkfvivnb998n0rjrhs4pwha9s2alh993rcpv2p2s8qgav")))

