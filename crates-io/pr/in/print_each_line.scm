(define-module (crates-io pr in print_each_line) #:use-module (crates-io))

(define-public crate-print_each_line-0.1.0 (c (n "print_each_line") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1p27nc520hkpb5gbvb049cm2vhq4cvn03awwc6n289y1rm4idr4x")))

