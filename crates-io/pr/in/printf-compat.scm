(define-module (crates-io pr in printf-compat) #:use-module (crates-io))

(define-public crate-printf-compat-0.1.0 (c (n "printf-compat") (v "0.1.0") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (d #t) (k 0)) (d (n "cstr_core") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "cty") (r ">=0.2.1, <0.3.0") (d #t) (k 0)) (d (n "itertools") (r ">=0.9.0, <0.10.0") (k 0)))) (h "0pchqn9aiikvqgix3ss4v72k7zvxx2f4aqqwc15dcdpll1xa3b4p") (f (quote (("std") ("default" "std"))))))

(define-public crate-printf-compat-0.1.1 (c (n "printf-compat") (v "0.1.1") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (d #t) (k 0)) (d (n "cstr_core") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "cty") (r ">=0.2.1, <0.3.0") (d #t) (k 0)) (d (n "itertools") (r ">=0.9.0, <0.10.0") (k 0)))) (h "0164d89yw0fy8fydx5jk9m4ml4i81a0igrra41kksggyizr2l01v") (f (quote (("std") ("default" "std"))))))

