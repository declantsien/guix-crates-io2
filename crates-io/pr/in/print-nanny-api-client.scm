(define-module (crates-io pr in print-nanny-api-client) #:use-module (crates-io))

(define-public crate-print-nanny-api-client-0.16.0 (c (n "print-nanny-api-client") (v "0.16.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "10bmhf2bsnf0vh3bix1i6z788svk11411ig4d1dysj1488pw5s74")))

