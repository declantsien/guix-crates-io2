(define-module (crates-io pr in print_chords) #:use-module (crates-io))

(define-public crate-print_chords-0.1.0 (c (n "print_chords") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.6.2") (d #t) (k 0)) (d (n "midi-consts") (r "^0.1.0") (d #t) (k 0)) (d (n "rsynth") (r "^0.1.1") (f (quote ("backend-jack"))) (d #t) (k 0)))) (h "19s89n6pfklcjrnrhb61c64g4w7v6x9c01bw8vb15s706zilph0l")))

(define-public crate-print_chords-0.1.1 (c (n "print_chords") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.6.2") (d #t) (k 0)) (d (n "midi-consts") (r "^0.1.0") (d #t) (k 0)) (d (n "rsynth") (r "^0.1.1") (f (quote ("backend-jack"))) (d #t) (k 0)))) (h "0jzf2qpa2jzc0a2sbv22872k00rcfdgbgdv0vvvs45j1wykkhi9f")))

(define-public crate-print_chords-0.1.2 (c (n "print_chords") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.6.6") (d #t) (k 0)) (d (n "midi-consts") (r "^0.1.0") (d #t) (k 0)) (d (n "rsynth") (r "^0.1.1") (f (quote ("backend-jack"))) (d #t) (k 0)))) (h "18bbb8iswfh0i50p28w1yjan9ygcjcsy375r9yqs4w7axwzj5dwv")))

(define-public crate-print_chords-0.1.3 (c (n "print_chords") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.11.4") (d #t) (k 0)) (d (n "midi-consts") (r "^0.1.0") (d #t) (k 0)))) (h "1mgi8ahaqrvq9aj7zawbgs0vvv6b2wx1xwpiwx4qk7dmg21c8j9y")))

(define-public crate-print_chords-0.1.4 (c (n "print_chords") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "jack") (r "^0.11.4") (d #t) (k 0)) (d (n "midi-consts") (r "^0.1.0") (d #t) (k 0)))) (h "0vrjgwlb61rgjflklisf1gvx1x0mpjxjl3cg18l2q4cl6jlbkipa")))

