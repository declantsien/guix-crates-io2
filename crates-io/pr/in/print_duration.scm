(define-module (crates-io pr in print_duration) #:use-module (crates-io))

(define-public crate-print_duration-1.0.0 (c (n "print_duration") (v "1.0.0") (h "06k6ripz465qvi7w6v6rl30i3fig6mnd435m23n5myzjfn9b2a1b")))

(define-public crate-print_duration-1.0.1 (c (n "print_duration") (v "1.0.1") (h "1b2wmazmq0rkissad6n2ma4f88acphq4n44vjx44vqvml2x6zy6b")))

