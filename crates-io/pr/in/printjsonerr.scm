(define-module (crates-io pr in printjsonerr) #:use-module (crates-io))

(define-public crate-printjsonerr-0.0.1 (c (n "printjsonerr") (v "0.0.1") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "jsonerr") (r "^0.0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "03zj7v2y6adiwzml3vw3ml41q2plpd30f698wipv0gdccyw35cbz")))

