(define-module (crates-io pr in printnanny) #:use-module (crates-io))

(define-public crate-printnanny-0.10.5 (c (n "printnanny") (v "0.10.5") (d (list (d (n "printnanny-cli") (r "^0.10.4") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.10.4") (d #t) (k 0)))) (h "0jg1ybkr55c2il0884s6nhdw2k7y192lz47ci2k720806lh85sdf")))

(define-public crate-printnanny-0.10.6 (c (n "printnanny") (v "0.10.6") (d (list (d (n "printnanny-cli") (r "^0.10.4") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.10.4") (d #t) (k 0)))) (h "1mbr5j959r229bpfj8xfrnkp79bj9skdm70q9i9s9vgc0axl6vv8")))

(define-public crate-printnanny-0.10.7 (c (n "printnanny") (v "0.10.7") (d (list (d (n "printnanny-cli") (r "^0.10.4") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.10.4") (d #t) (k 0)))) (h "1xw8jws5fy8fv68xwv023sgd0ry4i35rnjabmlb8j6h24rnqd5si")))

(define-public crate-printnanny-0.11.0 (c (n "printnanny") (v "0.11.0") (d (list (d (n "printnanny-cli") (r "^0.11.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.11.0") (d #t) (k 0)))) (h "1i0k28860dacgdyp3zf176w2fvbf031syrbhal7kiij8m08nz9p5")))

(define-public crate-printnanny-0.11.1 (c (n "printnanny") (v "0.11.1") (d (list (d (n "printnanny-cli") (r "^0.11.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.11.0") (d #t) (k 0)))) (h "1b5h6i6a08pkrmqk8j4f1fx7i7r0w5ma25jwffgcghyxxsafja4c")))

(define-public crate-printnanny-0.11.2 (c (n "printnanny") (v "0.11.2") (d (list (d (n "printnanny-cli") (r "^0.11.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.11.0") (d #t) (k 0)))) (h "0c5f6aav8hksbbwrpgnq3iv1r6s0i26bzg7psp3pj2z73rc8ykak")))

(define-public crate-printnanny-0.12.4 (c (n "printnanny") (v "0.12.4") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "0ygfwwsq9bjf5z4d7kmwr8wsr4wfn9xfnw3rglg82abqf43sjk6k")))

(define-public crate-printnanny-0.12.5 (c (n "printnanny") (v "0.12.5") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "01bxb1sdgxqzx8r7z1wb562bzhp7dqp010jj9hwc3sv6bbxnfnby")))

(define-public crate-printnanny-0.12.6 (c (n "printnanny") (v "0.12.6") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "1znnrpiqg64paj21pszgdh1hv44wyb45y8yqqzgi0hm3kwcs6ym9")))

(define-public crate-printnanny-0.12.7 (c (n "printnanny") (v "0.12.7") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "0mr91ns84zbwj9bfpjx5rhicqi6b5yay28wf5sd73444xid75vik")))

(define-public crate-printnanny-0.12.8 (c (n "printnanny") (v "0.12.8") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "0k6icg014psgh5713xzqh8rcjb0wkqcwyiscb8qzzqpcw79h1wfb")))

(define-public crate-printnanny-0.12.9 (c (n "printnanny") (v "0.12.9") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "1s0f2ff8c690b0c9am46z6nv3ba6yd0m6i1dmmy1lsasinjla087")))

(define-public crate-printnanny-0.12.10 (c (n "printnanny") (v "0.12.10") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "1l3lkwsg5r6xzh29nynr2z166yvs6x6lndcp88wpl43izg8qak7s")))

(define-public crate-printnanny-0.12.11 (c (n "printnanny") (v "0.12.11") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "134jgpx7gzy49g4r5r1mf7fizjqhzq0cv5yy4icfxhnaggmn2hn1")))

(define-public crate-printnanny-0.12.12 (c (n "printnanny") (v "0.12.12") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "0zgnrv6sq49n37drsd504z5v02l832k8srm2bf4ldjama9ffv3if")))

(define-public crate-printnanny-0.12.13 (c (n "printnanny") (v "0.12.13") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "1ib0ljgkm7xvv6bw5x4r3d242lfi4giwmzrpwhjhkc6xwcy2a36w")))

(define-public crate-printnanny-0.12.14 (c (n "printnanny") (v "0.12.14") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "1lkyhpsw7il3ri5pv4iv073jxfbv3a05w4p4dw1d3vmwnb0lb8hv")))

(define-public crate-printnanny-0.12.15 (c (n "printnanny") (v "0.12.15") (d (list (d (n "printnanny-cli") (r "^0.12.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.12.0") (d #t) (k 0)))) (h "04xmp9g4mndcqfxh0s3r339dazga9rxd60z5s8qdvkma7h11n26q")))

(define-public crate-printnanny-0.13.0 (c (n "printnanny") (v "0.13.0") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "01rn1fkrbxi31shyfbwxxi4fj4yh2hkzgi1n31l6v5xvw19v5s9f")))

(define-public crate-printnanny-0.13.1 (c (n "printnanny") (v "0.13.1") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "0y3cikzlvdh6yf1gnhy3j1z5pn9vdpdgmfbclpb3jbbjcns0782s")))

(define-public crate-printnanny-0.13.2 (c (n "printnanny") (v "0.13.2") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "15vzivqw9pdsy6xni56lsxxss1cycsngvggy00smv3ifllqr37qg")))

(define-public crate-printnanny-0.13.3 (c (n "printnanny") (v "0.13.3") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "0am75s48zmqjbbk4vg86y63pzaljkwp5939z848xm1aajwqxff5n")))

(define-public crate-printnanny-0.13.4 (c (n "printnanny") (v "0.13.4") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "1idvsn10cxcp1wijadz49lhv6gsl1a95vdpq3h41vkqzjld1iywi")))

(define-public crate-printnanny-0.13.5 (c (n "printnanny") (v "0.13.5") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "1n7pmxy74375k0fai0r17xlxpa6pxwq4z3v7afzkxjbl3qbx2drb")))

(define-public crate-printnanny-0.13.6 (c (n "printnanny") (v "0.13.6") (d (list (d (n "printnanny-cli") (r "^0.13.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.13.0") (d #t) (k 0)))) (h "1ncid5943kxk1688isbqdgw8y5bb1pz32vsj39qs6jlgs1wnf6n5")))

(define-public crate-printnanny-0.14.0 (c (n "printnanny") (v "0.14.0") (d (list (d (n "printnanny-cli") (r "^0.14.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.14.0") (d #t) (k 0)))) (h "1zcka6xy2v5d2i92684psfq055sd4x52yysnf425iys87avncvps")))

(define-public crate-printnanny-0.14.1 (c (n "printnanny") (v "0.14.1") (d (list (d (n "printnanny-cli") (r "^0.14.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.14.0") (d #t) (k 0)))) (h "11ds4kdh8dvpw4basmx8disbyq5y6sh3djyhlhqg446j2lkn5ibx")))

(define-public crate-printnanny-0.15.0 (c (n "printnanny") (v "0.15.0") (d (list (d (n "printnanny-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.15.0") (d #t) (k 0)))) (h "0ih2sjfla0bj7vdn9zvzmbl8qhm9qilzjiln12p8126q0n8wcnzi")))

(define-public crate-printnanny-0.15.1 (c (n "printnanny") (v "0.15.1") (d (list (d (n "printnanny-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.15.0") (d #t) (k 0)))) (h "0ax1dwc24sxjk4q7qlsh2h3nzjpzxq9jzbdli17jrywl4400bxji")))

(define-public crate-printnanny-0.15.3 (c (n "printnanny") (v "0.15.3") (d (list (d (n "printnanny-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.15.0") (d #t) (k 0)))) (h "08k5f93i3sd7il0hskgfvmb7pqi2yyfm0si0zf0y99l4p90majg1")))

(define-public crate-printnanny-0.15.4 (c (n "printnanny") (v "0.15.4") (d (list (d (n "printnanny-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.15.0") (d #t) (k 0)))) (h "10my72daj5ax5xjlwj73q0s319zkax8vw2r3rq39d4vsvaqqs61x")))

(define-public crate-printnanny-0.15.5 (c (n "printnanny") (v "0.15.5") (d (list (d (n "printnanny-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.15.0") (d #t) (k 0)))) (h "0byzrv23x257h3vlrykb7pgnk7zakh1158qimk8i4s94w0d8mm0n")))

(define-public crate-printnanny-0.16.0 (c (n "printnanny") (v "0.16.0") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "0sln8x3kxcpwfnm4z6qgmb4szll21pcj70f6zw2y9wcjcw117g0n")))

(define-public crate-printnanny-0.16.1 (c (n "printnanny") (v "0.16.1") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "03aqmj6b0dvh1ib2mi2jzmyan3crir842qhq9wa4clzdzv4n7w1q")))

(define-public crate-printnanny-0.16.2 (c (n "printnanny") (v "0.16.2") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "0scd1577m9xvkkb793jakva7kxl8x8mvp4mww9139ydsmmh2xvmq")))

(define-public crate-printnanny-0.16.3 (c (n "printnanny") (v "0.16.3") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "18543jlsyj9d9awlv4ljw3jjfxaqmx1c8ijgsmgdz6c6p2k6zn7m")))

(define-public crate-printnanny-0.16.4 (c (n "printnanny") (v "0.16.4") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "0vxsw2qh7c4kpvk3bh2glqx8a7z4jy2zmn2rpm2byyxz9ln1bshn")))

(define-public crate-printnanny-0.16.6 (c (n "printnanny") (v "0.16.6") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "17mz2zmh5w61pvlq508wxki38nnlq9nwb7v662qli98pgnzdz99m")))

(define-public crate-printnanny-0.16.7 (c (n "printnanny") (v "0.16.7") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "1q0q4gv4r8s18cb68ly0m4mqndin9yg9283ad33z66vdc64p51r0")))

(define-public crate-printnanny-0.16.8 (c (n "printnanny") (v "0.16.8") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "097pjfpval66a8b8a7rv9wkblyjq7x18p748708s0b2laqgdyaqz")))

(define-public crate-printnanny-0.16.9 (c (n "printnanny") (v "0.16.9") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "0gbj28lzng51qvnz2zsy08a8vwqs30bl09sn7a4zl52f09qrcjfp")))

(define-public crate-printnanny-0.16.10 (c (n "printnanny") (v "0.16.10") (d (list (d (n "printnanny-cli") (r "^0.16.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.16.0") (d #t) (k 0)))) (h "09hmx4hb1i9x8scxs5q88pqj3knsc1sjcih050lqkxv025pnzjgz")))

(define-public crate-printnanny-0.17.0 (c (n "printnanny") (v "0.17.0") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "0ghmx3k91hmhamyqv9dyzdlkadl0mi80dk501zzj9sldfpjam97h")))

(define-public crate-printnanny-0.17.2 (c (n "printnanny") (v "0.17.2") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "15dhy7pv3z61hv6qqs77zqcy0l69vcpnqbjvhrwk7wl72kkwsiwh")))

(define-public crate-printnanny-0.17.3 (c (n "printnanny") (v "0.17.3") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "06k50143735bsba5g2iyp7pyh28s0vqbxqb8k832n1yk660h4k3w") (r "1.59")))

(define-public crate-printnanny-0.17.4 (c (n "printnanny") (v "0.17.4") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "103nz2bh15gqikvn0453wfzgn3np17fxfgp93xgy0vbb0xgb403p") (r "1.59")))

(define-public crate-printnanny-0.17.5 (c (n "printnanny") (v "0.17.5") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "0f04kf1jqrwjbvh9029swj03acc8v391h9wymjmvnfi73r175kky") (r "1.59")))

(define-public crate-printnanny-0.17.6 (c (n "printnanny") (v "0.17.6") (d (list (d (n "printnanny-cli") (r "^0.17.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.17.0") (d #t) (k 0)))) (h "1dygsh7741g86rbdgykmqc4s76740h9w3wp9z0jqx75b3x1mvq4d") (r "1.59")))

(define-public crate-printnanny-0.18.0 (c (n "printnanny") (v "0.18.0") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "1b0xmm2pfc99xfp28jf8vsi7jgkpw0qrjiz4sj2s4p2c2v69jzl1") (r "1.59")))

(define-public crate-printnanny-0.18.1 (c (n "printnanny") (v "0.18.1") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "11pfvkrxdps6lpyzwi4ddgfx9bvf3ilkqcc8kalrn9v387a1122h") (r "1.59")))

(define-public crate-printnanny-0.18.2 (c (n "printnanny") (v "0.18.2") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "08q0qfbcchmdkhzxd3x4z49drrpxv6lpbw06v4wfg80s0nm3bghy") (r "1.59")))

(define-public crate-printnanny-0.18.3 (c (n "printnanny") (v "0.18.3") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "1rdrs9r7cxmz475pg71gwharpycjrzgmbr7drwyygp053jva4021") (r "1.59")))

(define-public crate-printnanny-0.18.4 (c (n "printnanny") (v "0.18.4") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "0jhdmd3i2rkfvf4kayk8gcas224cwhqhvzhq3ycmhfg1b6pc78zq") (r "1.59")))

(define-public crate-printnanny-0.18.5 (c (n "printnanny") (v "0.18.5") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "0cl0fcwygzz99k5pr1v8p820i710h8qcsvd7m5dxr92rq40s75sz") (r "1.59")))

(define-public crate-printnanny-0.18.6 (c (n "printnanny") (v "0.18.6") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "0i3sgmvr0wnxajnzbhppaypjqrid6w6hrpjffkgrkz74c6n0qayp") (r "1.59")))

(define-public crate-printnanny-0.18.7 (c (n "printnanny") (v "0.18.7") (d (list (d (n "printnanny-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.18.0") (d #t) (k 0)))) (h "00bwfw1gqpgwqsdbxqlhbnpjaja55w3m7az01k7pxhaamxprs9la") (r "1.59")))

(define-public crate-printnanny-0.19.0 (c (n "printnanny") (v "0.19.0") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1idcn9pbh57id5q1lynv1bb435335xq43iv3n01biaqyywj8k03d") (r "1.59")))

(define-public crate-printnanny-0.19.1 (c (n "printnanny") (v "0.19.1") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1ssi6c0fhw4a2bmqb4mi7fxa85gw8s52hwvwwvcjhx2lrx5y2ak3") (r "1.59")))

(define-public crate-printnanny-0.19.2 (c (n "printnanny") (v "0.19.2") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1cbc77m91kihva9k87p6rgzgqwd3vnsdg23a4s0npi6j80lnlq3x") (r "1.59")))

(define-public crate-printnanny-0.19.3 (c (n "printnanny") (v "0.19.3") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1jcnzm9dpnzxvq33vn63nd5knnlmx9irz917nnl1grx6s5z9fa5v") (r "1.59")))

(define-public crate-printnanny-0.19.4 (c (n "printnanny") (v "0.19.4") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "0yqgnyifn14ykdy4l3xknaa22wy8ccr44xsv4gxci79mivqidq0d") (r "1.59")))

(define-public crate-printnanny-0.19.6 (c (n "printnanny") (v "0.19.6") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1p3l7yxazs6cqdv86zmm4mf2gcrlccqd1n915j986zpy133nwr0h") (r "1.59")))

(define-public crate-printnanny-0.19.7 (c (n "printnanny") (v "0.19.7") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1y76clfn9nwv14x3k8j5gaaylxjgdvm6sqxp5m3s9yrq5yqy1qc7") (r "1.59")))

(define-public crate-printnanny-0.19.8 (c (n "printnanny") (v "0.19.8") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "0varvc653c88avivgaiknc8igspqca74lnlak9k2h02i9906m8jv") (r "1.59")))

(define-public crate-printnanny-0.19.9 (c (n "printnanny") (v "0.19.9") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "1c5c1yk7wd8xvhzm4lp8fz1kvqkk72ba6l48bhz41yi7d3vpnwis") (r "1.59")))

(define-public crate-printnanny-0.19.10 (c (n "printnanny") (v "0.19.10") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "04f6d0pq65rlmkzz12lgqdj5rp3si5z72rwyhh0dfkr13k2wrpg3") (r "1.59")))

(define-public crate-printnanny-0.19.11 (c (n "printnanny") (v "0.19.11") (d (list (d (n "printnanny-cli") (r "^0.19.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.19.0") (d #t) (k 0)))) (h "0ri3bz53dxambxxad1pp14l9pl9d62mgrfnfyhcrjz7v7yqa3wsy") (r "1.59")))

(define-public crate-printnanny-0.20.0 (c (n "printnanny") (v "0.20.0") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "10f8cn094dc40kiysjrqa2py3ylmr1rxri5k3d6m6axr830lgp7x") (r "1.59")))

(define-public crate-printnanny-0.20.1 (c (n "printnanny") (v "0.20.1") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "1rhqdzdw5myxc6n8lzgab8byylxd5p2iyiplg3j31isw8bj2g3sw") (r "1.59")))

(define-public crate-printnanny-0.20.2 (c (n "printnanny") (v "0.20.2") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "0rsfsk51cmarwzk6jli3350avxgk0h3dbpj85lwcalc0bybjjksi") (r "1.59")))

(define-public crate-printnanny-0.20.3 (c (n "printnanny") (v "0.20.3") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "1mgi9pb9x3lxvmaq9s486xzm3038crm8rgzsry86vm62jv27faf6") (r "1.59")))

(define-public crate-printnanny-0.20.4 (c (n "printnanny") (v "0.20.4") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "1xncjlfg68z538p4vnkxcbgh7gkkn8icvzkswaij2nipm5v06yxp") (r "1.59")))

(define-public crate-printnanny-0.20.5 (c (n "printnanny") (v "0.20.5") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "1l5j7j14q5m9ljqzp4fy7mj2vv0xl1jksrvxb95fq0f29pmlcsn3") (r "1.59")))

(define-public crate-printnanny-0.20.6 (c (n "printnanny") (v "0.20.6") (d (list (d (n "printnanny-cli") (r "^0.20.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.20.0") (d #t) (k 0)))) (h "17mk7h29vghazp03hsxxgvyg9vdz7k8cq7jnrzm262khjwn9bgnw") (r "1.59")))

(define-public crate-printnanny-0.21.0 (c (n "printnanny") (v "0.21.0") (d (list (d (n "printnanny-cli") (r "^0.21.0") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.21.0") (d #t) (k 0)) (d (n "printnanny-dev") (r "^0.21.0") (d #t) (k 0)))) (h "16kcj9a6sxqiri5bbf8lxf690a3ppjda72rzcfl9m580n82qcsmj") (r "1.59")))

(define-public crate-printnanny-0.22.0-rc.1 (c (n "printnanny") (v "0.22.0-rc.1") (d (list (d (n "printnanny-cli") (r "^0.22.0-rc.1") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.22.0-rc.1") (d #t) (k 0)) (d (n "printnanny-dev") (r "^0.22.0-rc.1") (d #t) (k 0)))) (h "0zmlwlj9fd1mn8q4d6fhdhlyriky87xif73axa1a55whr3pcxmm7") (r "1.59")))

(define-public crate-printnanny-0.22.0-rc.2 (c (n "printnanny") (v "0.22.0-rc.2") (d (list (d (n "printnanny-cli") (r "^0.22.0-rc.1") (d #t) (k 0)) (d (n "printnanny-dash") (r "^0.22.0-rc.1") (d #t) (k 0)) (d (n "printnanny-dev") (r "^0.22.0-rc.1") (d #t) (k 0)))) (h "09mjgvdmyj42lc3r785rxv5ibqqbwb1aibn4vfpf5c5lknqi3jgk") (r "1.59")))

