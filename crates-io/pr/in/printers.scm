(define-module (crates-io pr in printers) #:use-module (crates-io))

(define-public crate-printers-1.0.0 (c (n "printers") (v "1.0.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "1lwxc42ikq5j9nbzm2dld0cw87ap4ppdg1n88200iydf37fxy1i3") (y #t)))

(define-public crate-printers-1.0.1 (c (n "printers") (v "1.0.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "089k3l0y8f7agdngyxsjpfax0fg590d5gy79ss781h7ri597ixym") (y #t)))

(define-public crate-printers-1.0.2 (c (n "printers") (v "1.0.2") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "13za78b6byq7n33g4l9jrkxrpg7jhva4pk7j0mnyb63z3f6riwdr") (y #t)))

(define-public crate-printers-1.1.0 (c (n "printers") (v "1.1.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "1ih0x02x8avf0i5pg7qyq2b0xjskcj5wkhj7n8c413qf56hv9066") (y #t)))

(define-public crate-printers-1.2.0 (c (n "printers") (v "1.2.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "0q37si0z1pvh7rmaqijzsdizd34kk65h5vmjx5ygm620q952rq1d")))

(define-public crate-printers-2.0.0 (c (n "printers") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b8x658y114pgpwdh2h29zq19rr694n47rzh21zf68hc3bqp06b0")))

