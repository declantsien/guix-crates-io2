(define-module (crates-io pr in printr) #:use-module (crates-io))

(define-public crate-printr-0.1.1 (c (n "printr") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 1)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "sentiment") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1j1jr0rkqr81igiqczdwaqiaps3g9vx7n7n36dh1lxrmkahik49n")))

(define-public crate-printr-0.1.2 (c (n "printr") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 1)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "sentiment") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ls8njzi5fsbjs8mnv32f0chi2vf1h0d4ycp3pzs3md1mxrxm0j8")))

(define-public crate-printr-0.1.3 (c (n "printr") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 1)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "sentiment") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "068ryhwlh09fkyasgfyjdzxrfil7gv1cl9barifshkghvxfj9sva")))

