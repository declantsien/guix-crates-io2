(define-module (crates-io pr in printc) #:use-module (crates-io))

(define-public crate-printc-0.1.0 (c (n "printc") (v "0.1.0") (h "0ms44lnwazfk4ldh45m32zgixjh95fzhn68phq00apj298knqx3f") (y #t)))

(define-public crate-printc-0.1.1 (c (n "printc") (v "0.1.1") (h "0w3s7kpy7f3ffwwrsahadfh9s1ni679gablh7vpr15ka4j7ji0is") (y #t)))

(define-public crate-printc-0.1.2 (c (n "printc") (v "0.1.2") (h "1pvm9kdhj3z3396727mpfwd3yzrws0nnbvqk0jklc5mxx9lbh7p6")))

