(define-module (crates-io pr in println-cli) #:use-module (crates-io))

(define-public crate-println-cli-0.1.0 (c (n "println-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1s5sk8fh73dpvw8mnhky5wi9baxl7961ss503dqbsq3zb9xypjnx")))

