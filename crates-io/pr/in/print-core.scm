(define-module (crates-io pr in print-core) #:use-module (crates-io))

(define-public crate-print-core-0.1.0 (c (n "print-core") (v "0.1.0") (h "1nly81hz8vw243fyhyl8r2byaq1rb2pkas3mcb4cprpfkvw0yjhw")))

(define-public crate-print-core-0.1.1 (c (n "print-core") (v "0.1.1") (h "0jgiicpg43685qy7hk3aw7yblhdb6ww12y21hacql46jk6adkvx3")))

(define-public crate-print-core-0.1.2 (c (n "print-core") (v "0.1.2") (h "0k0qfxlz9a2d94vzgifnx86ysplbfyrn3l91mnarm1sb89qfr2yz")))

