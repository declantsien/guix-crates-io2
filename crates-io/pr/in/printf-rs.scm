(define-module (crates-io pr in printf-rs) #:use-module (crates-io))

(define-public crate-printf-rs-0.1.0 (c (n "printf-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1bk62dhm91rafv12w8lbdpzn7g4s4q65g4j5vcfhda111r54vivk") (y #t)))

(define-public crate-printf-rs-0.1.1 (c (n "printf-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "151hdvhfil6k9nvxpg67zbryvma30pp9g08xbss1mv8sbjncmmif") (y #t)))

