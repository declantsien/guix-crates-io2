(define-module (crates-io pr in printer) #:use-module (crates-io))

(define-public crate-printer-0.1.0 (c (n "printer") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "16vzn240lanc0a5lkl7dcv3qz904l55p2hfxprrn3blcbn5y8ml6")))

(define-public crate-printer-0.1.1 (c (n "printer") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "1x78lfbwmc9wxq824r4ycpvva0m7n4s3swsc28bnl72vpi1wr764")))

(define-public crate-printer-0.2.1 (c (n "printer") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0v5qs02vxz3nc4rq3pybw45d5kl0zwvs8vqqn94y806vnw6v795x")))

(define-public crate-printer-0.2.2 (c (n "printer") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "1nxdgwfl6y7qpf9mfm7wsqn3l122r53plvlprqrib5bjvwrvvra0")))

(define-public crate-printer-0.2.3 (c (n "printer") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0i1rd6khnf2nvnq19z37akdjxdhmyzf91sbw4d19q6c49czm14gq")))

(define-public crate-printer-0.2.4 (c (n "printer") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0f9f9c7v5q633kgkf8snp51s8c5f61h2krrmk3l8hr1im6az2p2v")))

(define-public crate-printer-0.2.5 (c (n "printer") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "18birasdgii85dzh5vwjld1cq68svsd2bjcz0qacxrrsnvykwjjk")))

(define-public crate-printer-0.2.6 (c (n "printer") (v "0.2.6") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0pvzhygvpyjh20adpywykgqkwamca5119jy2p7m0i49pp5zqkydp")))

(define-public crate-printer-0.2.7 (c (n "printer") (v "0.2.7") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1rcsdnp4dfmw9vnjrj9qgqpdayc89bwanjcdcp3v3767hwp7myp2")))

(define-public crate-printer-0.2.8 (c (n "printer") (v "0.2.8") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "148h4icjy9rf23cbc08x3fxzsdzwkc8pnficgkg5nfi4s1gv5qjr")))

(define-public crate-printer-0.2.9 (c (n "printer") (v "0.2.9") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1kis5h76f4h3i06h79zywababvqc2bahkxp568x224wvz1wqqysp")))

(define-public crate-printer-0.2.10 (c (n "printer") (v "0.2.10") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1z611x84s3ax4rifx3xmhnyppx1npdwxiq9njvcsk85vgbj5djc2")))

(define-public crate-printer-0.2.11 (c (n "printer") (v "0.2.11") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "12xyx5dpv3v4ybic94shbnqp4jsql3pzisxw83j2nsa5w3kmbkab")))

(define-public crate-printer-0.2.12 (c (n "printer") (v "0.2.12") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1jh2dvdsycs7prw19rf7gj5jq1kzmfxkl9viap2hvw2515h966j2")))

(define-public crate-printer-0.3.0 (c (n "printer") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1ahcvd2rlxgzvfd4ig6x8g5w3n3rcq7ygpd8jb3ifcv8if9i86gf")))

(define-public crate-printer-0.3.1 (c (n "printer") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "00fpk4dbg1wk4l7q9m3b9mnqbxvsxpjhsdhjg3h236s0kw2wcivb")))

(define-public crate-printer-0.4.0 (c (n "printer") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "11vfd8j22rifz0kl8699ixcni9zapdrv5kj8nbmxn91hdx9460vy")))

(define-public crate-printer-0.4.1 (c (n "printer") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0y7gwn86jd5z3ndzma03n9pyrx9lzhxxjmcph88ym25jsmf9wx9l")))

(define-public crate-printer-0.4.2 (c (n "printer") (v "0.4.2") (d (list (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0q1v5rm69gcwqg9rq7f6xdj67ln714jcbdgczzq5xrfbgc93f1ar")))

(define-public crate-printer-0.4.3 (c (n "printer") (v "0.4.3") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1yqxhd6yl64rwm05drjx847jyca21vykicxdccsl16jzdgg44ijp")))

(define-public crate-printer-0.5.0 (c (n "printer") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0mzdjghm4rc4y0d3njsazxa902gbj3hd6qpr3j96f49r5gca37zy")))

(define-public crate-printer-0.5.1 (c (n "printer") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "08qmwr74hvkkj5jzn2qc0nprkzxa1nnxz395f7hlgq4szp41vjhf")))

(define-public crate-printer-0.5.2 (c (n "printer") (v "0.5.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "17dkkrgpgk52gz3hdw4nv4nz2x0mmpgfn9wvakfdi78j9g0yz04i")))

(define-public crate-printer-0.6.0 (c (n "printer") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.26.0") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1gph6jgx5wph0f72nxsqvpdsh38fall564ijdd07yax0vqjkq788")))

(define-public crate-printer-0.6.1 (c (n "printer") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0ziihk3xd3n83997r084l1ap9b43lj3qwalgsjlv2vdjyaqb2ym6")))

(define-public crate-printer-0.6.2 (c (n "printer") (v "0.6.2") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0dwni0l6nkx0xn9bxs7cc2a1h7vv1wbnyq3clqvz9agjia78g7y3")))

(define-public crate-printer-0.6.3 (c (n "printer") (v "0.6.3") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0iqhddkjsbaknx7xh6s5h0l4qrrl647df2zd12wpkfzjbjcc858x")))

(define-public crate-printer-0.7.0 (c (n "printer") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1bhymmj34gnd7prhf9x706nrfdi51lr22smi4b65lqsx3bps3swn")))

(define-public crate-printer-0.7.1 (c (n "printer") (v "0.7.1") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "121hwdy2vyxi4jd0j0vshs2d0s4x1m3qlzldylql16v7mshfidjg")))

(define-public crate-printer-0.7.2 (c (n "printer") (v "0.7.2") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("use-dev-tty"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1zjclavarhhf97visx36ks8nf898a4iasbhc1d7pgvkcaq5mwyvi")))

