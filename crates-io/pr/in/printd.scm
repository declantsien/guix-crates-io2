(define-module (crates-io pr in printd) #:use-module (crates-io))

(define-public crate-printd-0.2.0 (c (n "printd") (v "0.2.0") (h "0jx1gdigfxj2bn58z31wjafyd452n0qf5w0ci2az4m0ijnwyjjph")))

(define-public crate-printd-0.2.1 (c (n "printd") (v "0.2.1") (h "0aqxb8py8gfw7lw05x3j3dd20xbb7ygb15rq1zqjzic31czw4rs3")))

