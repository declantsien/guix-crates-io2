(define-module (crates-io pr in print_key) #:use-module (crates-io))

(define-public crate-print_key-1.0.0 (c (n "print_key") (v "1.0.0") (d (list (d (n "crokey") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)))) (h "1da2gmdm6gs9h5ix76j35wkbkyrlyw4bpmn29b691qdiz7bz2d18")))

(define-public crate-print_key-2.0.0 (c (n "print_key") (v "2.0.0") (d (list (d (n "crokey") (r "^0.6.0") (d #t) (k 0)))) (h "174zj5ygadnn6pbf57d4ggcjh3rs4s64pxanj2w5bgcchynyjzp7")))

(define-public crate-print_key-2.1.0 (c (n "print_key") (v "2.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crokey") (r "^0.6.1") (d #t) (k 0)))) (h "0dxfsjlb8jyhbvcqdbglyicisnf6z2i5z6spzn5pi5ixsy2m969z")))

