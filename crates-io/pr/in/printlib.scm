(define-module (crates-io pr in printlib) #:use-module (crates-io))

(define-public crate-PrintLib-1.2.0 (c (n "PrintLib") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "14554gzlv60y3v01wc7i8jbgfggffl1qjfdchcc6vwpadjj77j3c")))

(define-public crate-PrintLib-1.2.1 (c (n "PrintLib") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0ysyv0mwb4bw9szld2nkb32vd8irlygcxp0hb7g0hh0yzc78bcnv")))

(define-public crate-PrintLib-1.2.2 (c (n "PrintLib") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1v4s71lw1qsz108wskf7hkb84mhb428yd05rv0nsjfxgxcxymdmb")))

(define-public crate-PrintLib-1.2.3 (c (n "PrintLib") (v "1.2.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0dv7p0p1zw0ks8ibisrp2f6p8k7d0wfij70nq6x01lw1cadj7wxj")))

(define-public crate-PrintLib-1.3.0 (c (n "PrintLib") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "16i99jnnh4xyx5zy3zf1n5dwvalwm333idgpi9kc7ihdjs8lw2ms") (y #t)))

(define-public crate-PrintLib-1.3.1 (c (n "PrintLib") (v "1.3.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0zyrvrl8232vsdhkg66wbv22zk6nm1b7j5bwf5y0pv3hcsfcm61c")))

(define-public crate-PrintLib-1.3.2 (c (n "PrintLib") (v "1.3.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "047dhf5caqk77kbihgrqfxi85hs5y6zk57kps70mdi97c473ni2v")))

(define-public crate-PrintLib-1.3.3 (c (n "PrintLib") (v "1.3.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0cmzf32qn1hs107jqb13n93iiw73ar3xn293q8f2845lj8igan3a")))

(define-public crate-PrintLib-1.4.0 (c (n "PrintLib") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0m60l30qil44kc224criyrsl3g2cc0mmliy4lxf6vwmp8jid11z1")))

(define-public crate-PrintLib-1.5.0 (c (n "PrintLib") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "1f67sdp6s013kvmg2ay0f9dkc7nvf0b82486mb7smzgajlknbmx7")))

(define-public crate-PrintLib-1.5.1 (c (n "PrintLib") (v "1.5.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0nv1gw1baj6lz5ghjnwa22dj6gmd1965lwyif8jrby3dr2ywzwy7")))

(define-public crate-PrintLib-1.5.2 (c (n "PrintLib") (v "1.5.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "1p5sysqw6f2i6aikfj6j83dhwdyzq4dgs9al4gjr14924kgb8ibh")))

