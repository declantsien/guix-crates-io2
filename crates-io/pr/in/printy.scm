(define-module (crates-io pr in printy) #:use-module (crates-io))

(define-public crate-printy-0.1.0 (c (n "printy") (v "0.1.0") (h "0mywklz46arxkxzhb4xqc239x77pisswlzpjryqrplpivpzdafd4")))

(define-public crate-printy-0.2.0 (c (n "printy") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0384b2l5dz3dfpcnciy6y3lzid1bw52mdz24rjgkrh6vs65jk155")))

