(define-module (crates-io pr in print_logger) #:use-module (crates-io))

(define-public crate-print_logger-0.1.0 (c (n "print_logger") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "kv_unstable"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mfcyvswfy6jpw7bn0g5a348jy488xh26c6amc1srs6pckivn7qm")))

(define-public crate-print_logger-0.1.1 (c (n "print_logger") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "kv_unstable"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ilq88yw54jqi8n7a1nkapjdpyvasg5wp0nqibmwync9l0x6xn03")))

(define-public crate-print_logger-0.2.0 (c (n "print_logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "kv_unstable"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4") (d #t) (k 0)))) (h "0cy6dk009274la6lpw3nbn2xrslky06rs5r68vhpafvdp9qanpib")))

