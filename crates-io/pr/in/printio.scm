(define-module (crates-io pr in printio) #:use-module (crates-io))

(define-public crate-printio-0.1.0 (c (n "printio") (v "0.1.0") (h "0l0cmksnp3h0sgsf1k6g86x413zpcv2vcn7b16g19yvq82kjwqnl")))

(define-public crate-printio-0.2.0 (c (n "printio") (v "0.2.0") (h "1i1adxrlxxddc6qwm16q06nhmmhwzmchy2xxsqy3cl9lf67msznb")))

(define-public crate-printio-0.2.1 (c (n "printio") (v "0.2.1") (h "1il60q1gky9f2jg90ngbkwnymgh8yj5q1rr975ji5fmb1gz56sb7")))

(define-public crate-printio-0.2.2 (c (n "printio") (v "0.2.2") (h "04p7khjvv12pzav2hp03lbpzrbn107jg6qw96mq7jica4f1bx038")))

