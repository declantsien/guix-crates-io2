(define-module (crates-io pr in print_perf) #:use-module (crates-io))

(define-public crate-print_perf-0.1.0 (c (n "print_perf") (v "0.1.0") (h "0j6jhdrxpwy82i8572kq6ag7i41dzh4cmfw44agg8yvnhq2q1wz1")))

(define-public crate-print_perf-0.1.1 (c (n "print_perf") (v "0.1.1") (h "0vdfd1si07gc1hxyhhjah5844clxa68by05p0vyrdalp5jnj1mb1")))

(define-public crate-print_perf-0.1.2 (c (n "print_perf") (v "0.1.2") (h "1xn4ji4w9xxby4j4vg5aa01080gk28rjr9zz469y9ivans44bj21")))

(define-public crate-print_perf-0.1.3 (c (n "print_perf") (v "0.1.3") (h "0dmw1bbm7zc5m1azz4nmcasfy2kxmiqfbzskcmrnn2vvz3sxp00m")))

(define-public crate-print_perf-0.1.4 (c (n "print_perf") (v "0.1.4") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)))) (h "0yfi50v7daicvc47a94qaxlbc4csfglz48i4m58azkqdv9wi496l")))

(define-public crate-print_perf-0.1.5 (c (n "print_perf") (v "0.1.5") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)))) (h "17dr97n277awmp67d6pj8x10i288m1lgfq2wlrs6yy26klnicrm0")))

(define-public crate-print_perf-0.1.6 (c (n "print_perf") (v "0.1.6") (h "0zb34wcpsm1rnmlrbl6bgpxsyb9b6cxvg3l4w9hk9gq818914lnm")))

(define-public crate-print_perf-0.1.7 (c (n "print_perf") (v "0.1.7") (h "1zy24b697s1hp4p2n8hjdiv0gxb9z93bpj344di8l4askdagcdhy")))

(define-public crate-print_perf-0.1.8 (c (n "print_perf") (v "0.1.8") (h "1srn8zxf8rc5n2haf98py332q4p6c56hmrkgab1a67i4l3nvpcz3")))

(define-public crate-print_perf-0.1.9 (c (n "print_perf") (v "0.1.9") (h "0iwaip7dlmchgc515cia3xflbrp3wns4a4npc5fcsdl0b6f8sdq4")))

