(define-module (crates-io pr e_ pre_hf) #:use-module (crates-io))

(define-public crate-pre_hf-0.0.0 (c (n "pre_hf") (v "0.0.0") (d (list (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1fm54k0r46cjks5bqsp4wdnfnzmw3shrwqz0li8gib9hi63v2arl")))

(define-public crate-pre_hf-0.0.1 (c (n "pre_hf") (v "0.0.1") (d (list (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0w98x4andgnyf6lq6wy0z388h39360rymfbz44i2ffvqx09qvqz3")))

