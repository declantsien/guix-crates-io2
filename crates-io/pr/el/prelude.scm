(define-module (crates-io pr el prelude) #:use-module (crates-io))

(define-public crate-prelude-0.1.0 (c (n "prelude") (v "0.1.0") (h "0b2i95y2kvlgw4v5r7a1zv71wnlmncpjdc9cl7yskpr31mjjjqwb")))

(define-public crate-prelude-0.2.0 (c (n "prelude") (v "0.2.0") (h "1sk539s5xd8h05ka4hq6ba5s20p3p0z83km5633aq3jaqkjwmhhv")))

(define-public crate-prelude-0.2.1 (c (n "prelude") (v "0.2.1") (h "1frc9j1gkx7p0cl8q4z2v5pgkvnfn3kjqgd1m0x0dm6nxwbjl3f7")))

