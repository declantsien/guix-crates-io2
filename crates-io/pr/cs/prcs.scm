(define-module (crates-io pr cs prcs) #:use-module (crates-io))

(define-public crate-prcs-0.1.0 (c (n "prcs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0dsk06j7iz84ka5xw44b8x8d3bi8g9g1hbfmjjby92mlp9ayy2vj")))

(define-public crate-prcs-0.2.0 (c (n "prcs") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "127ldij3paiqwl76iywi3h8qisyv2bz8bs03jpl2p55ia5s5fykx")))

