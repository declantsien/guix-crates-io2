(define-module (crates-io pr ow prowl-queue) #:use-module (crates-io))

(define-public crate-prowl-queue-0.1.0 (c (n "prowl-queue") (v "0.1.0") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prowl") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "097vp26fx73mh2ps5xpqfch7b63s9h22rngk9paxnrnwydjdx34c")))

(define-public crate-prowl-queue-0.1.1 (c (n "prowl-queue") (v "0.1.1") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prowl") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "180qxhv5q489wv863jy3x6533rc43vn0d2i7aw6bkx5bn9ppmb0i")))

(define-public crate-prowl-queue-0.1.2 (c (n "prowl-queue") (v "0.1.2") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prowl") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f2718y7bwx3jyzmm0snwmb62aiscvk61sxr407vn5qlwmfrkcik")))

(define-public crate-prowl-queue-0.1.3 (c (n "prowl-queue") (v "0.1.3") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prowl") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1agm8vib8x8igigs3kzzc7a98gkrlkqf2d3903wfwq9vzy06mi1r")))

(define-public crate-prowl-queue-0.1.4 (c (n "prowl-queue") (v "0.1.4") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prowl") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13z3v51b1b3k704r81k7a97s39d7wm4py2cks50q458qkylj6n5h")))

