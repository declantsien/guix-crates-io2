(define-module (crates-io pr ow prowl) #:use-module (crates-io))

(define-public crate-prowl-0.0.0 (c (n "prowl") (v "0.0.0") (h "05hjhkz7k9q9a96wppxzahbzqjhs9kqnwn84913zwzim00lx7ykh")))

(define-public crate-prowl-0.1.0 (c (n "prowl") (v "0.1.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "05cnrfasypg5vp36vawsywnyqnl3g30s6cynrmngwwh06sbn51s6")))

(define-public crate-prowl-0.1.1 (c (n "prowl") (v "0.1.1") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1j9vc4phl0hcnzv16q4mpky1a4a4j4cm62b1y0irnfdsgj0vczm2")))

(define-public crate-prowl-0.2.0 (c (n "prowl") (v "0.2.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0qb63193gdw8kfhfzw87jviy6jm9ldgpfyl3x6j0a3ql7zx7y30f")))

(define-public crate-prowl-0.2.1 (c (n "prowl") (v "0.2.1") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0q46n6h72hx8ka94whf6h13mmymbr2dg0ibiw10rxpp0rx8gjnk2")))

(define-public crate-prowl-0.2.2 (c (n "prowl") (v "0.2.2") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0zwhcpr21za1s85hix9lpm4k4f7vh9ixjc3mnr5fnz618w3cp0lm")))

(define-public crate-prowl-0.2.3 (c (n "prowl") (v "0.2.3") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0hc5glgfjd8vsbsq6npwnhz467ba096a8q73bh62ycbg2j957lsz")))

(define-public crate-prowl-0.2.4 (c (n "prowl") (v "0.2.4") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1ha48gyckcp3dv4ldkvxri95n172c4qggz7j6929zyvfqzwy24qc")))

(define-public crate-prowl-0.2.5 (c (n "prowl") (v "0.2.5") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1y9l2hw29xwimbk4bki2dk0l5s3wglb1bscd84q6ql4hijl9jmn9") (s 2) (e (quote (("serde" "dep:serde"))))))

