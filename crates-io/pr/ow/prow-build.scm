(define-module (crates-io pr ow prow-build) #:use-module (crates-io))

(define-public crate-prow-build-0.1.0 (c (n "prow-build") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls" "gzip" "deflate"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1fgaqrp5cxklncii3vsgpfdr3lc37s78ab7l0svpqsrdwhp83shq")))

