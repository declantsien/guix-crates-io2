(define-module (crates-io pr os prost-bytes05) #:use-module (crates-io))

(define-public crate-prost-bytes05-0.0.1 (c (n "prost-bytes05") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "087mhmx13hwph13y5pq3wd4jz0ncxml5ll3jh92fbd6gdpwjjha0") (f (quote (("no-recursion-limit") ("default" "prost-derive"))))))

(define-public crate-prost-bytes05-0.0.2 (c (n "prost-bytes05") (v "0.0.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "18rx332jvmf6kpqzmysgnrbwlqnmcgn00p1x8c1jynza81w9c42g") (f (quote (("no-recursion-limit") ("default" "prost-derive"))))))

