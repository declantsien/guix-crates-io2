(define-module (crates-io pr os pros-math) #:use-module (crates-io))

(define-public crate-pros-math-0.1.0 (c (n "pros-math") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (k 0)) (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)))) (h "1vggpmi5ld3qp672ljbywz3ybanpk5wv8s1x3f5s8bfykrb3p7p2")))

