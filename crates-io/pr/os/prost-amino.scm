(define-module (crates-io pr os prost-amino) #:use-module (crates-io))

(define-public crate-prost-amino-0.4.0 (c (n "prost-amino") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-amino-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "19x1xjd89lzzmyk6x3szilkdnyck3zaqh32a0r60agkckccc9i84")))

(define-public crate-prost-amino-0.4.1 (c (n "prost-amino") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-amino-derive") (r "^0.4.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1plkvlpjgb2snacn472mrzm8ys8f5amjphdawv3q8p0azwkqzhci") (y #t)))

(define-public crate-prost-amino-0.5.0 (c (n "prost-amino") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-amino-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0cy45bhvdiabd8ibr72wqab1afik13zvc9ppahkd884zgjiv9g23")))

(define-public crate-prost-amino-0.6.0 (c (n "prost-amino") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-amino-derive") (r "^0.6.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0vjm8xdnmpa78xgl1qnsxqi9fag5lgr5d9x5c7i4bbi1dim01nkd")))

