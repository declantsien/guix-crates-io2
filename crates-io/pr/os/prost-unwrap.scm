(define-module (crates-io pr os prost-unwrap) #:use-module (crates-io))

(define-public crate-prost-unwrap-0.1.2 (c (n "prost-unwrap") (v "0.1.2") (d (list (d (n "prost-unwrap-transform") (r "^0.1") (d #t) (k 0)))) (h "0ari1z6w49h5iy8vprzgnc1xj7k8qwn1cwmq3gzhnb0gjfyvdz8p") (y #t)))

(define-public crate-prost-unwrap-0.1.3 (c (n "prost-unwrap") (v "0.1.3") (d (list (d (n "prost-unwrap-transform") (r "^0.1.1") (d #t) (k 0)))) (h "1pmxqj3qfy7nhm56jfh4i9khhjh6bxy82nbcgm1yw8lq0bk0w42p") (y #t)))

(define-public crate-prost-unwrap-0.1.4 (c (n "prost-unwrap") (v "0.1.4") (d (list (d (n "prost-unwrap-transform") (r "^0.1.2") (d #t) (k 0)))) (h "0p11j7h7nrg7m9pa4sdvs6x529z31d0qf33cr2ysx21113x39f7w") (y #t)))

(define-public crate-prost-unwrap-0.1.5 (c (n "prost-unwrap") (v "0.1.5") (d (list (d (n "prost-unwrap-transform") (r "^0.1.2") (d #t) (k 0)))) (h "1jknjjx54zd3rg3x23ir8lvymh23r500wxinc5gja7igrdg4dgap") (y #t)))

(define-public crate-prost-unwrap-1.0.0 (c (n "prost-unwrap") (v "1.0.0") (d (list (d (n "prost-unwrap-transform") (r "^1.0.0") (d #t) (k 0)))) (h "0njzhf0mws9w5255n5bb9p8yj4qsn3fb00vwx1rlxjbd4gr0rwb4")))

(define-public crate-prost-unwrap-1.0.1 (c (n "prost-unwrap") (v "1.0.1") (d (list (d (n "prost-unwrap-transform") (r "^1.0.1") (d #t) (k 0)))) (h "0p05xcd01vrxzqc26zn5z9pv2q9cpxlm30gp8dyswwlkpacy88dk")))

(define-public crate-prost-unwrap-1.1.0 (c (n "prost-unwrap") (v "1.1.0") (d (list (d (n "prost-unwrap-transform") (r "^1.0.1") (d #t) (k 0)))) (h "1p9yn3s3x8sm9xzijh5v96p9vkw56cs2y829y40mhcaad133sd3f")))

