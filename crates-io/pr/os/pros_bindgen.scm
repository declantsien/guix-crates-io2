(define-module (crates-io pr os pros_bindgen) #:use-module (crates-io))

(define-public crate-pros_bindgen-0.1.0 (c (n "pros_bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 0)))) (h "1gsi9ivvr2hvkb63zi6ybb8zbknckpbyklj9xlx7l9kqflvpq23w")))

(define-public crate-pros_bindgen-0.2.0 (c (n "pros_bindgen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 0)))) (h "1g385x671xn5xid89rjsslwhcki349rzyclf0bb5qvgm60grwhck")))

