(define-module (crates-io pr os prost-arrow) #:use-module (crates-io))

(define-public crate-prost-arrow-0.0.1 (c (n "prost-arrow") (v "0.0.1") (d (list (d (n "arrow-array") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-arrow-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1rkcqwwzh2i2a2lwnnalbfacr7p5acl4xprf3jr17g605sz0hdqd")))

(define-public crate-prost-arrow-0.0.2 (c (n "prost-arrow") (v "0.0.2") (d (list (d (n "arrow-array") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-arrow-derive") (r "^0.0.2") (d #t) (k 0)))) (h "0w9h9jdnxv3pgv1j4pz3s8brzhbibc6c3mbvidyi84w7p5bm755i")))

(define-public crate-prost-arrow-0.0.3 (c (n "prost-arrow") (v "0.0.3") (d (list (d (n "arrow-array") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-arrow-derive") (r "^0.0.3") (d #t) (k 0)))) (h "1p3xnf809dpsd3ck1ha8qfdng88a1ckga97blhszf8y3sw6cqx0d")))

