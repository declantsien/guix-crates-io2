(define-module (crates-io pr os prost-serde) #:use-module (crates-io))

(define-public crate-prost-serde-0.1.0 (c (n "prost-serde") (v "0.1.0") (d (list (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "prost-helper") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qjscw5rhmsj9lrpi3v8a7l82wg5fdnrj67j9w133h4fp479dzd4")))

(define-public crate-prost-serde-0.1.1 (c (n "prost-serde") (v "0.1.1") (d (list (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "prost-helper") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "013vvjc1frz7pmdgwa1ps0g3a6bxxjnlahhj4w1p2i7rac7fys46")))

(define-public crate-prost-serde-0.1.2 (c (n "prost-serde") (v "0.1.2") (d (list (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "prost-helper") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16cvff35b8jphz5xjsm8f8ddwyjihj40qa5jjbssjniffnv468rh")))

(define-public crate-prost-serde-0.1.3 (c (n "prost-serde") (v "0.1.3") (d (list (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "prost-helper") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rygd44vgpm5hh8rbwp2w0shqqvrks3kmlkbrs647q90vmrcsfnl")))

(define-public crate-prost-serde-0.2.0 (c (n "prost-serde") (v "0.2.0") (d (list (d (n "prost-build") (r "^0.7") (d #t) (k 0)) (d (n "prost-helper") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kq8x0pvwiya9znpwhhzvdxr45gya6qwk9rj89sx6g9dp0q8sd4v")))

(define-public crate-prost-serde-0.3.0 (c (n "prost-serde") (v "0.3.0") (d (list (d (n "prost-build") (r "^0.9") (d #t) (k 0)) (d (n "prost-helper") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jcgi267vygj5yp0dz0199xwm3yz8n3smnaiyzbnb7pbl40j39vb")))

