(define-module (crates-io pr os pros-sys) #:use-module (crates-io))

(define-public crate-pros-sys-0.0.1 (c (n "pros-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "1fndw7414n76mgj908zn8g7yzgfmxnnhxvlqwjvq0nlm3lpi92bh")))

(define-public crate-pros-sys-0.1.0 (c (n "pros-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "08id9r63gc7clgd0zcmww1r0c663v6q34kpjwqszg2gq6za36l1f") (f (quote (("xapi"))))))

(define-public crate-pros-sys-0.1.1 (c (n "pros-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "1ahb9lwv8gm07z1f4lajwadpzjqifpn3ai0qssrf08svks70kwj5") (f (quote (("xapi"))))))

(define-public crate-pros-sys-0.1.2 (c (n "pros-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "15dc3x7fbswq9nibxg9ffqbjgnzg7m8cgllfz8gs37xmswix9a4v") (f (quote (("xapi"))))))

(define-public crate-pros-sys-0.1.3 (c (n "pros-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "15lsf8v5spzpks9hr70qhhdm8d26w65rwa9z7jqxmx8qr48zvkw2") (f (quote (("xapi")))) (y #t) (l "pros")))

(define-public crate-pros-sys-0.1.4 (c (n "pros-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "0p4hwlmmjdz74sxy4hjb2pj7hwa58kbchs4z495cshpndl99gkjd") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.1.5 (c (n "pros-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "161x6qbnflxfcpr0kzhxcwqm78l7dz26y5cvifa2lrmd1ja53a8z") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.1.6 (c (n "pros-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.4") (d #t) (k 1)))) (h "0r4a5q8dvsfxm146s0xdxyspanfhkkn5hnh3a9abpljyqilppkpk") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.2.0 (c (n "pros-sys") (v "0.2.0") (h "0jlqa53zqbg1gjsz5ll69n08ib72x860h63k5h66bhbibqxlbzkw") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.2.1 (c (n "pros-sys") (v "0.2.1") (h "0mr735pzrhshma1bfcc34llniid6dfjk6agl3rgz506szaadwn72") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.2.2 (c (n "pros-sys") (v "0.2.2") (h "16kdjsvlfd4pyl5hc97ah8bdlk8rpmjl98xxdb0gv6zwisb7v5fi") (f (quote (("xapi")))) (l "pros")))

(define-public crate-pros-sys-0.3.0 (c (n "pros-sys") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "1sgdm1ip00awgsip5apymgiv11s8wjyxz0559p375b70zhm5bzhq") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.3.1 (c (n "pros-sys") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "1vr9n0l3jnd1761p0cs37anxhq778h0h055fkjn58cgy233iyfrh") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.4.0 (c (n "pros-sys") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "0kmvpw217y2qf8nnipyc7pfv2hxc52dn3x3pqakva46gbrdbdsjx") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.4.1 (c (n "pros-sys") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "0m61x3lw60pi0814xx8wdzlq88v8rb2r66cfa6w17llfx67q9nsr") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.4.2 (c (n "pros-sys") (v "0.4.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "02czzi2f2lkm1k3f28ha6j8wlf1qbiyx7401s7vya7l5wgsyn7ff") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.4.3 (c (n "pros-sys") (v "0.4.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "14hdywk69fjwvz11x6as88kxap8gvd7z2glwzw51r8yk74xfqicd") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.5.0 (c (n "pros-sys") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "1d17v7rss8ahyssa3lc5w9ihzbiaij0bzn24ck6rd3cp04fz0ly8") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.6.0 (c (n "pros-sys") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "0jwhpica927nlsdn5vwvjb43lxq20nc2qn078j5ci3cjbz6jms59") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.7.0 (c (n "pros-sys") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "0ql2vakf515aqv1bhmg69zm11rylmg6hlwd7jc7xyl48k9im5s26") (f (quote (("xapi") ("no-link")))) (l "pros")))

(define-public crate-pros-sys-0.8.0 (c (n "pros-sys") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "0idardf15f0w6ks7bh12ya7dzclaqvxlzcqw9k325kayzsmxdhrq") (f (quote (("xapi") ("no-link")))) (l "pros")))

