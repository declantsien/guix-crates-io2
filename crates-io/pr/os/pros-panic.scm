(define-module (crates-io pr os pros-panic) #:use-module (crates-io))

(define-public crate-pros-panic-0.1.0 (c (n "pros-panic") (v "0.1.0") (d (list (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-devices") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pros-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0l0ghc0fis230lh03lvxdbnp3lbdbdq32cibw0xrd8r3afb3gi1r") (f (quote (("default" "display_panics")))) (s 2) (e (quote (("display_panics" "dep:pros-devices"))))))

(define-public crate-pros-panic-0.1.1 (c (n "pros-panic") (v "0.1.1") (d (list (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-devices") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "pros-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1csamry9xc3v5ivkgch4p6n5g926napgd9pbslhvvilkvhrk63l5") (f (quote (("default" "display_panics")))) (s 2) (e (quote (("display_panics" "dep:pros-devices"))))))

