(define-module (crates-io pr os prost-unwrap-core) #:use-module (crates-io))

(define-public crate-prost-unwrap-core-0.1.3 (c (n "prost-unwrap-core") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0kd3s6wh7rd4q1yz6bklmq66171llrdkfh93zl6aahif24wl48")))

(define-public crate-prost-unwrap-core-0.1.4 (c (n "prost-unwrap-core") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02ah5sl5bnwdqm5ahvqy9ysj9s34cv4i7y3isas54fp9wiib0mw3")))

(define-public crate-prost-unwrap-core-0.1.5 (c (n "prost-unwrap-core") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfcy8p6fzxlpbswdjr88ir2zf6j15142mlpprblc95nfb78869l")))

(define-public crate-prost-unwrap-core-1.0.0 (c (n "prost-unwrap-core") (v "1.0.0") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06wg81chw1930gxwcb3dg2mqdvfwdlcqz4i22fm6i5aq0slxyn40")))

(define-public crate-prost-unwrap-core-1.0.1 (c (n "prost-unwrap-core") (v "1.0.1") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0krmbvds4s84cn281xyqfi824vlqbpqq5x80pw0fm3jl3y54b1a1")))

(define-public crate-prost-unwrap-core-1.1.0 (c (n "prost-unwrap-core") (v "1.1.0") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "131k3mj7v4s2qzbfckcnggi5347xscyk861m45ryfklk8mw3yhhv")))

