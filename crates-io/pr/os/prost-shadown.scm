(define-module (crates-io pr os prost-shadown) #:use-module (crates-io))

(define-public crate-prost-shadown-0.5.0 (c (n "prost-shadown") (v "0.5.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "prost-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0lhyjacfwggflm0k3hw6nxibh1fcmsv6dgzy0whf8yrp0idp3847") (f (quote (("no-recursion-limit") ("default" "prost-derive"))))))

