(define-module (crates-io pr os prost-simple-rpc-build) #:use-module (crates-io))

(define-public crate-prost-simple-rpc-build-0.1.1 (c (n "prost-simple-rpc-build") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.3.2") (d #t) (k 0)))) (h "1bsvj2r27i1yrgmmsc9b2vw7v8hkb4cc3zpzv49jnvb71bplhvbs")))

(define-public crate-prost-simple-rpc-build-0.1.2 (c (n "prost-simple-rpc-build") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.3.2") (d #t) (k 0)))) (h "1dbs3qay10lim0b2031rzby2z28f6jyssjfp1ywqjhzz6knh4x4y")))

(define-public crate-prost-simple-rpc-build-0.2.0 (c (n "prost-simple-rpc-build") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.3.2") (d #t) (k 0)))) (h "1dafsq928h1ig8hk24rry20jmgf74lhslmaz0fnzx0n6gwjn2z7x")))

(define-public crate-prost-simple-rpc-build-0.2.1 (c (n "prost-simple-rpc-build") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.3.2") (d #t) (k 0)))) (h "1lxqjpag4bq081d8a74d6aqdzqnqpym1njlqfmy23pnycn52991d")))

(define-public crate-prost-simple-rpc-build-0.3.0 (c (n "prost-simple-rpc-build") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.4.0") (d #t) (k 0)))) (h "18mbfbm5aqd5ycwbyvkixing1qw0mjlr5za8h7qy0dncnd88w9ps")))

