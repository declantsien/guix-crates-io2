(define-module (crates-io pr os prose-rs) #:use-module (crates-io))

(define-public crate-prose-rs-0.1.0 (c (n "prose-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "procfs") (r "^0.13.0") (d #t) (k 0)))) (h "1178m75c5ry3w9cll2bzn8yyg1kmba7j38hii4lmna8538rrqwzd")))

(define-public crate-prose-rs-0.2.0 (c (n "prose-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "procfs") (r "^0.13.0") (d #t) (k 0)))) (h "0irxqywy14i243srnhkk2jxkkhzl7ncrncrkhp0hk6rzjlg5wvjb")))

(define-public crate-prose-rs-1.0.0 (c (n "prose-rs") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0y1j890xlswzyfrvg2k08c2x1arn6pkj7ifpan8x1m8fm35ihhlb")))

