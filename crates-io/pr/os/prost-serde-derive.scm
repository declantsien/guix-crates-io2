(define-module (crates-io pr os prost-serde-derive) #:use-module (crates-io))

(define-public crate-prost-serde-derive-0.1.0 (c (n "prost-serde-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wjb58fwm3fbpmjha4lhhzgf9kladzg4s5ras9ikprlzg9i0c1vy")))

(define-public crate-prost-serde-derive-0.1.1 (c (n "prost-serde-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qycs1ffs0nfvh07g88rd51d45406422jgr5a4mdq5xfclimglx5")))

(define-public crate-prost-serde-derive-0.1.2 (c (n "prost-serde-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11jpwd4vv0kisrpz9yi5wva7wa405i7yk9kyf21s4qaz1af93mrq")))

(define-public crate-prost-serde-derive-0.1.4 (c (n "prost-serde-derive") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ghbrpds06jrfds3gfgl8qkzj9vczgaiw0sxqp45j430xaqywzpn")))

(define-public crate-prost-serde-derive-0.1.5 (c (n "prost-serde-derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gvyzywl72m7cdckczk8c0zp4wrcbcj11slpqgcrf9yp09wlabgl")))

