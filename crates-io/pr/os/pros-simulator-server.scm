(define-module (crates-io pr os pros-simulator-server) #:use-module (crates-io))

(define-public crate-pros-simulator-server-0.1.0 (c (n "pros-simulator-server") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonl") (r "^4.0") (d #t) (k 0)) (d (n "pros-simulator") (r "^0.2.1") (k 0)) (d (n "pros-simulator-interface") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0gslrh3zr2l20pq08lhm476bvk1y4df994xwf065ngqkjhw4ry71")))

(define-public crate-pros-simulator-server-0.1.1 (c (n "pros-simulator-server") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonl") (r "^4.0") (d #t) (k 0)) (d (n "pros-simulator") (r "^0.2.1") (k 0)) (d (n "pros-simulator-interface") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0ydavs555h2f67962gg8j0f98bks5464gs8kimiw4kzgz0f2nqay")))

(define-public crate-pros-simulator-server-0.3.0 (c (n "pros-simulator-server") (v "0.3.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonl") (r "^4.0") (d #t) (k 0)) (d (n "pros-simulator") (r "^0.3") (d #t) (k 0)) (d (n "pros-simulator-interface") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0p5fs9a4px8d0s9m1fwsynz520ny9jbryr4hngaz2bwlhgqfr9ig")))

(define-public crate-pros-simulator-server-0.4.0 (c (n "pros-simulator-server") (v "0.4.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonl") (r "^4.0") (d #t) (k 0)) (d (n "pros-simulator") (r "^0.4") (d #t) (k 0)) (d (n "pros-simulator-interface") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1ms8hl2d66i3rsflxyb9zyh9xcqpb2703waf1q94w91lw61rbrzi")))

(define-public crate-pros-simulator-server-0.5.0 (c (n "pros-simulator-server") (v "0.5.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonl") (r "^4.0") (d #t) (k 0)) (d (n "pros-simulator") (r "^0.5") (d #t) (k 0)) (d (n "pros-simulator-interface") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1n20bjd0qrgfhq84pv2b21ard9c7hfii715y9kmm7ja5m3pj6g93")))

