(define-module (crates-io pr os prose-lsp) #:use-module (crates-io))

(define-public crate-prose-lsp-0.0.1 (c (n "prose-lsp") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.19.0") (f (quote ("proposed"))) (d #t) (k 0)))) (h "1vqp5ynnlg2dfxz1amdwdpxwc05a1im13lx9wqi4shs3ghi0zj5k")))

