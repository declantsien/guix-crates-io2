(define-module (crates-io pr os prost-types) #:use-module (crates-io))

(define-public crate-prost-types-0.2.0 (c (n "prost-types") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 0)))) (h "06979x6zspj6bf86lv8zi0spvir4zgw54y7y33w9v4k4f21sb8gx")))

(define-public crate-prost-types-0.2.1 (c (n "prost-types") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 0)))) (h "1zqx243r6l1qxvm68fbm2ybflw5i9q5znnwxdpn7j5lng8yav7sr")))

(define-public crate-prost-types-0.2.2 (c (n "prost-types") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 0)))) (h "07ziq333mniam7qvjkr45zfdpnrblchl7s4i4din06ib33zg7zch")))

(define-public crate-prost-types-0.2.3 (c (n "prost-types") (v "0.2.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 0)))) (h "1v8awqkbx0qc34c897qzscnp5kwd3gqdphcjl06bjw9mk78lsf03")))

(define-public crate-prost-types-0.3.0 (c (n "prost-types") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1hlgjmw7hhgmjk65jj6yy3aqm9ncrm0h24ilmb6xn3g9kndv2lyw")))

(define-public crate-prost-types-0.3.1 (c (n "prost-types") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.3.1") (d #t) (k 0)) (d (n "prost-derive") (r "^0.3.1") (d #t) (k 0)))) (h "0rjvkjgn8bqrqyy61njsavds48b0z2w4rqklhm8cwkvlrxkrjmrn")))

(define-public crate-prost-types-0.3.2 (c (n "prost-types") (v "0.3.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.3.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.3.2") (d #t) (k 0)))) (h "0n3pf8z8vvdpg3xnjqw4qsz0ryah6yj5qidakpwzy4z9v8dgrd29")))

(define-public crate-prost-types-0.4.0 (c (n "prost-types") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.4.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0yd6h3rp4inbjv1dq057xidiimq83xf4j4z1f2amy25warywai2n")))

(define-public crate-prost-types-0.5.0 (c (n "prost-types") (v "0.5.0") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)))) (h "0vxcic65kb8kfpp1jqb95a7m026a16n0yrcvs5b8s74lcsiq5r0x")))

(define-public crate-prost-types-0.6.0 (c (n "prost-types") (v "0.6.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.6.0") (d #t) (k 0)))) (h "1c10s70qr49gngz5kjak4rns3f8pzpq6vd27lng4szjrk0px93bk") (y #t)))

(define-public crate-prost-types-0.6.1 (c (n "prost-types") (v "0.6.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "1ahp6j6b62zfv1d0hfns5rs8k76xgbv7dgkm9cq03h4p0rygcd0q")))

(define-public crate-prost-types-0.7.0 (c (n "prost-types") (v "0.7.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (f (quote ("prost-derive"))) (k 0)))) (h "1fziv7q5gy9rl7idksfxd333c63pc2dgl1yg488ivarxv76xf65m") (f (quote (("std" "prost/std") ("default" "std"))))))

(define-public crate-prost-types-0.8.0 (c (n "prost-types") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.8.0") (f (quote ("prost-derive"))) (k 0)))) (h "06vp090a8d0j31bc3kw7b1m5mlz9gnfabbasybri67bhjiivsfv0") (f (quote (("std" "prost/std") ("default" "std"))))))

(define-public crate-prost-types-0.9.0 (c (n "prost-types") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.9.0") (f (quote ("prost-derive"))) (k 0)))) (h "02pgz98nn62bb8glspb9m4fn3rrr5sc0y1wk4qnlhg3fhc77ljsk") (f (quote (("std" "prost/std") ("default" "std"))))))

(define-public crate-prost-types-0.10.0 (c (n "prost-types") (v "0.10.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.10.0") (f (quote ("prost-derive"))) (k 0)))) (h "1z8641zhn36hsib7680yrks186jjwnxlmzff7hm52vmf330q2rlj") (f (quote (("std" "prost/std") ("default" "std"))))))

(define-public crate-prost-types-0.10.1 (c (n "prost-types") (v "0.10.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.10.0") (f (quote ("prost-derive"))) (k 0)))) (h "0s0y8sc045xjynikw7n9ywm0z39fdkna3j39ivf1241n551022id") (f (quote (("std" "prost/std") ("default" "std"))))))

(define-public crate-prost-types-0.11.0 (c (n "prost-types") (v "0.11.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)))) (h "07y0xn4mdr9g3838p9lfdql4grgqirhg0kq7pqa76d4vl83ch2yk") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

(define-public crate-prost-types-0.11.1 (c (n "prost-types") (v "0.11.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)))) (h "0gkf7za8mkhg8w89padkvih93yf82wxxbi762ls4p93nmlcagyjd") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

(define-public crate-prost-types-0.11.2 (c (n "prost-types") (v "0.11.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)))) (h "06jm74axinvm885lbwqrla5jidkcrw2mdxivals9m3y47ny62xvl") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

(define-public crate-prost-types-0.11.5 (c (n "prost-types") (v "0.11.5") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.5") (f (quote ("prost-derive"))) (k 0)))) (h "149h2xgnqz3nfz83lcjwflks1ajxwkh4ybdw40lfdkb8fxipjzq1") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

(define-public crate-prost-types-0.11.6 (c (n "prost-types") (v "0.11.6") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.6") (f (quote ("prost-derive"))) (k 0)))) (h "123pafnx4kvn8qdfdrrraz2xp3hik4ddh3bm7pc9cgj315i55q55") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

(define-public crate-prost-types-0.11.7 (c (n "prost-types") (v "0.11.7") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.7") (f (quote ("prost-derive"))) (k 0)))) (h "15bn59a0v7l56lrd6hpyk19r061qqp4d864m5an2qr69bk6nmrbx") (f (quote (("std" "prost/std") ("default" "std")))) (y #t) (r "1.60")))

(define-public crate-prost-types-0.11.8 (c (n "prost-types") (v "0.11.8") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.8") (f (quote ("prost-derive"))) (k 0)))) (h "124w9k7dp1j6b84qh37nkb76sym6dzwnraj3l3bsz899d5k1k49p") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.11.9 (c (n "prost-types") (v "0.11.9") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (f (quote ("prost-derive"))) (k 0)))) (h "04ryk38sqkp2nf4dgdqdfbgn6zwwvjraw6hqq6d9a6088shj4di1") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.12.0 (c (n "prost-types") (v "0.12.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.0") (f (quote ("prost-derive"))) (k 0)))) (h "1xihsxclxjvlgp7ral2r86gqnvwn9rszsxhggchnry4pij8hmgnf") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.12.1 (c (n "prost-types") (v "0.12.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.1") (f (quote ("prost-derive"))) (k 0)))) (h "1kr7ffva3sfc0ya0qkwa5pwnq5gr4kj3z7zwbk3lnfnqcfgv50g0") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.12.2 (c (n "prost-types") (v "0.12.2") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.2") (f (quote ("prost-derive"))) (k 0)))) (h "0lls5w7yh2jxi764kd9k44dwskrr85j2fs335wg2i47m6qig6fc3") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.12.3 (c (n "prost-types") (v "0.12.3") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (f (quote ("prost-derive"))) (k 0)))) (h "03j73llzljdxv9cdxp4m3vb9j3gh4y24rkbx48k3rx6wkvsrhf0r") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.60")))

(define-public crate-prost-types-0.12.4 (c (n "prost-types") (v "0.12.4") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.4") (f (quote ("prost-derive"))) (k 0)))) (h "1zjzwlv1w9s38dk159lcnn7h6jr6ig3k9gmx58hiw7rcn0zc6d9j") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.70")))

(define-public crate-prost-types-0.12.5 (c (n "prost-types") (v "0.12.5") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.5") (f (quote ("prost-derive"))) (k 0)))) (h "0ra3ykdqk8dnp7wqwm6vv8739kzygnp863pjfx07ags9ypx9r2pd") (f (quote (("std" "prost/std") ("default" "std")))) (y #t) (r "1.70")))

(define-public crate-prost-types-0.12.6 (c (n "prost-types") (v "0.12.6") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.6") (f (quote ("prost-derive"))) (k 0)))) (h "1c6mvfhz91q8a8fwada9smaxgg9w4y8l1ypj9yc8wq1j185wk4ch") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.70")))

