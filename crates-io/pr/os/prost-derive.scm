(define-module (crates-io pr os prost-derive) #:use-module (crates-io))

(define-public crate-prost-derive-0.1.0 (c (n "prost-derive") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1ws0m794zm8j9v6irb1zygvwv0cjbfcfil1qk7rmkqd4v503h83s")))

(define-public crate-prost-derive-0.2.0 (c (n "prost-derive") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0n9i3y0cqsjlnvkgjb3x17hyl0lvkczc2jjpxpqg5m9y2l21cr3v")))

(define-public crate-prost-derive-0.2.1 (c (n "prost-derive") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "04czx8rpm3g82winaffyqkp935jf00zm7f1zl7n6h3axnbgpdypp")))

(define-public crate-prost-derive-0.2.2 (c (n "prost-derive") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1v6sr7rbhx5l9qqp0r4pcmdj0kwm3sm5yibr5yd8d59q7rkn65j1")))

(define-public crate-prost-derive-0.2.3 (c (n "prost-derive") (v "0.2.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1vl9dffpl6ihqlv8vkm6p068r00hw6945pkjwv83p6nfp0m9vyv9")))

(define-public crate-prost-derive-0.3.0 (c (n "prost-derive") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kfmw36mlxxj3ri6bp1n1d8dx70h8pxd3qqpb6zyiw3shc690mxw")))

(define-public crate-prost-derive-0.3.1 (c (n "prost-derive") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00ryib7b6627z6cclia45psp0ziq4b8kjxjxmjgxpzyx0n104bg4")))

(define-public crate-prost-derive-0.3.2 (c (n "prost-derive") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bib4b7h5gldp84bsvib0ybqdwbj4vqbb641yvi8xa1nd1bpdw7m")))

(define-public crate-prost-derive-0.4.0 (c (n "prost-derive") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ll2yic4krfzg3z9fy37bs0s490321kawilfsmk80bm7gsbx31wp")))

(define-public crate-prost-derive-0.5.0 (c (n "prost-derive") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "048fknvqcjx9ydvsqjcbr30nx6pmxdy298l185j79hsap5wc6zay")))

(define-public crate-prost-derive-0.6.0 (c (n "prost-derive") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xs8kr9z5jyvxymwabjy6lx36blc8s99lg68cqy3fkhm4lr5cwa6") (y #t)))

(define-public crate-prost-derive-0.6.1 (c (n "prost-derive") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wlgn7nbb3x79xdlxkg47m05gxw6cd302hzc5yhi1pmcjnds2yjk")))

(define-public crate-prost-derive-0.7.0 (c (n "prost-derive") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m7m5bamjpnl2yhqgyqcsfv02xhad76pplx7rdh53jwf03rib6hn")))

(define-public crate-prost-derive-0.8.0 (c (n "prost-derive") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fpg0knfsixpi1jq9dsq3gas9pmn3bpigqjmlw1cnnm098rjy3b0")))

(define-public crate-prost-derive-0.9.0 (c (n "prost-derive") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zi0qway5anz5ik3k1yrc2av81sjcqvqy9lnivv0nzp0ccr1mk7r")))

(define-public crate-prost-derive-0.10.0 (c (n "prost-derive") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vzwvrb4vk6ilq8a1x9wlgdmjsqklmnpqwv9ysgmxdvp0y7ijdfz")))

(define-public crate-prost-derive-0.10.1 (c (n "prost-derive") (v "0.10.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k77nir4xa06gbsdjzlygyv73razj9d11dnvxd18byspv92hyrvv")))

(define-public crate-prost-derive-0.11.0 (c (n "prost-derive") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0r33m5pds2l93ldhl450y2mpg7i3j0jraabjmkbkc1ccw3qdaibk") (r "1.56")))

(define-public crate-prost-derive-0.11.2 (c (n "prost-derive") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01n31hfyr97v120n2yr1w784i8mz0087zgyk0sjiq047cn5ycjhn") (r "1.56")))

(define-public crate-prost-derive-0.11.5 (c (n "prost-derive") (v "0.11.5") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0837v4r0jfxsyfswxzf22g21kg7nk2kknrmc9shvq6al3anjp168") (r "1.56")))

(define-public crate-prost-derive-0.11.6 (c (n "prost-derive") (v "0.11.6") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pdc52n10rjnn3yamhbp4i33p40b7pdpd4v2p4p757zah448rnlb") (r "1.56")))

(define-public crate-prost-derive-0.11.7 (c (n "prost-derive") (v "0.11.7") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04nwnkw9q214q3jwvnrb9i85qa8lx3mawk47rlxbqsc35qv3b6cf") (r "1.60")))

(define-public crate-prost-derive-0.11.8 (c (n "prost-derive") (v "0.11.8") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06rb3mrza7wpj7b3gv37npn2inv8cvchn0rd0j55mqg5rgwb1aaf") (r "1.60")))

(define-public crate-prost-derive-0.11.9 (c (n "prost-derive") (v "0.11.9") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d3mw2s2jba1f7wcjmjd6ha2a255p2rmynxhm1nysv9w1z8xilp5") (r "1.60")))

(define-public crate-prost-derive-0.12.0 (c (n "prost-derive") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0v28ff6vq9p6z2ck1rlzi17pir9kqd6qlyr41z829r8an8kmq1sn") (r "1.60")))

(define-public crate-prost-derive-0.12.1 (c (n "prost-derive") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cjcib5w99sycw01j4a1j1xcx97crg9gfyc10zsnqhdxzaksnnr6") (r "1.60")))

(define-public crate-prost-derive-0.12.2 (c (n "prost-derive") (v "0.12.2") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1g268fzmswaf6rx1sm8000h6a4rigd4b6zg55wysi95cvyjifmq6") (r "1.60")))

(define-public crate-prost-derive-0.12.3 (c (n "prost-derive") (v "0.12.3") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03l4yf6pdjvc4sgbvln2srq1avzm1ai86zni4hhqxvqxvnhwkdpg") (r "1.60")))

(define-public crate-prost-derive-0.12.4 (c (n "prost-derive") (v "0.12.4") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <=0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j4yiqd7zh8vg5x3yk8qhiz5h4ab07dx8fzfddbbyx80l3i2vphr") (r "1.70")))

(define-public crate-prost-derive-0.12.5 (c (n "prost-derive") (v "0.12.5") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <=0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nnmq599p8dmx2hxzpqsjg6mq3f310filkvh0cj962iz4fmy6m4m") (y #t) (r "1.70")))

(define-public crate-prost-derive-0.12.6 (c (n "prost-derive") (v "0.12.6") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <=0.12") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1waaq9d2f114bvvpw957s7vsx268licnfawr20b51ydb43dxrgc1") (r "1.70")))

