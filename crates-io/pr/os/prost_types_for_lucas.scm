(define-module (crates-io pr os prost_types_for_lucas) #:use-module (crates-io))

(define-public crate-prost_types_for_lucas-0.11.2 (c (n "prost_types_for_lucas") (v "0.11.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n4gwyp2q8x3a3ixh73s916kjwnz7i3cw3pg370ignc5mkj5xql2") (f (quote (("std" "prost/std") ("default" "std")))) (r "1.56")))

