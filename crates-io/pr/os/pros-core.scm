(define-module (crates-io pr os pros-core) #:use-module (crates-io))

(define-public crate-pros-core-0.1.0 (c (n "pros-core") (v "0.1.0") (d (list (d (n "dlmalloc") (r "^0.2.4") (f (quote ("global"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "no_std_io") (r "^0.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pros-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_61" "unstable-core-error"))) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0bj9b2295g01h1ddhdcsjyqwm5918nhpkzwbwqxkzjhw5lwx93ba")))

(define-public crate-pros-core-0.1.1 (c (n "pros-core") (v "0.1.1") (d (list (d (n "dlmalloc") (r "^0.2.4") (f (quote ("global"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "no_std_io") (r "^0.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pros-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_61" "unstable-core-error"))) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0aap2m9mi096ydd4w8kjznmsznjw1w4ax16mpilq12lqph3zvzqz")))

