(define-module (crates-io pr os prost-unwrap-transform) #:use-module (crates-io))

(define-public crate-prost-unwrap-transform-0.1.0 (c (n "prost-unwrap-transform") (v "0.1.0") (d (list (d (n "prost-unwrap-core") (r "^0.1") (d #t) (k 0)))) (h "15j5giawczpgmshqkx9fhpd3zdisfc1nqm1m6k3qd6dkixjz50yd")))

(define-public crate-prost-unwrap-transform-0.1.1 (c (n "prost-unwrap-transform") (v "0.1.1") (d (list (d (n "prost-unwrap-core") (r "^0.1.4") (d #t) (k 0)))) (h "1y8l200yz05b7hg6fpdrlhnfp1x9ii1c9i12sca06qf06wdqys7g")))

(define-public crate-prost-unwrap-transform-0.1.2 (c (n "prost-unwrap-transform") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "prost-unwrap-core") (r "^0.1.4") (d #t) (k 0)))) (h "0rrmhm1w31ml58srmgwl1z39nggksdlpcdh8ra9kmkndgg4v55ar")))

(define-public crate-prost-unwrap-transform-0.1.5 (c (n "prost-unwrap-transform") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "prost-unwrap-core") (r "^0.1.4") (d #t) (k 0)))) (h "02anwfin92zmvh3dpx5dqjlrflld7j1akl79bjb9mfsihn3s8mah")))

(define-public crate-prost-unwrap-transform-1.0.0 (c (n "prost-unwrap-transform") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "prost-unwrap-core") (r "^1.0.0") (d #t) (k 0)))) (h "0yp50ny7nmscn7hzjwsydwi9kam5n7rij1cg5yybwq580rrcpich")))

(define-public crate-prost-unwrap-transform-1.0.1 (c (n "prost-unwrap-transform") (v "1.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "prost-unwrap-core") (r "^1.0.1") (d #t) (k 0)))) (h "0377lydbbdybpwimilv21qwkzipkg44ysdfh53asl77jmvwbm7dw")))

(define-public crate-prost-unwrap-transform-1.1.0 (c (n "prost-unwrap-transform") (v "1.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "prost-unwrap-core") (r "^1.0.1") (d #t) (k 0)))) (h "0mk3jivqnjisr5821jp79i2inpjwrjcv3g04w8wyb8ra72n3866a")))

