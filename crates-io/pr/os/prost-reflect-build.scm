(define-module (crates-io pr os prost-reflect-build) #:use-module (crates-io))

(define-public crate-prost-reflect-build-0.6.0 (c (n "prost-reflect-build") (v "0.6.0") (d (list (d (n "prost-build") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.6.1") (d #t) (k 0)))) (h "1fr2igyr4l6lnjsm0wqzijc39spp5k8pn0naqwhrymi3h38q8haz") (r "1.54.0")))

(define-public crate-prost-reflect-build-0.7.0 (c (n "prost-reflect-build") (v "0.7.0") (d (list (d (n "prost-build") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.7.0") (d #t) (k 0)))) (h "1xzqdswas5n9qidwij6r84442dp9ns2dcb2rkbz86jd31z4wjhm3") (r "1.54.0")))

(define-public crate-prost-reflect-build-0.8.0 (c (n "prost-reflect-build") (v "0.8.0") (d (list (d (n "prost-build") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.8.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r26zwbn6kv025zri286rv3h7m1wxy8kcppxm953m3cpwdnn7fnr") (r "1.54.0")))

(define-public crate-prost-reflect-build-0.8.1 (c (n "prost-reflect-build") (v "0.8.1") (d (list (d (n "prost-build") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.8.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09vqchk5kz3lfhn8nh6370zs2m7h54xa91a2b6gllrbh6iypfk52") (f (quote (("vendored" "prost-build/vendored") ("default")))) (r "1.54.0")))

(define-public crate-prost-reflect-build-0.9.0 (c (n "prost-reflect-build") (v "0.9.0") (d (list (d (n "prost-build") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.9.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yv45h76i2wgb2xs68sa9l1c9b51rg27af56z47dh030915pgah8") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-prost-reflect-build-0.10.0 (c (n "prost-reflect-build") (v "0.10.0") (d (list (d (n "prost-build") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19lk8glb6mq0pv481ab1bay90mfwi0jr9733amyn1k7hh71sph4f") (f (quote (("default")))) (r "1.60.0")))

(define-public crate-prost-reflect-build-0.11.0 (c (n "prost-reflect-build") (v "0.11.0") (d (list (d (n "prost-build") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kxgz12x7hpdcp759iaqf5r68zw927plb720w24d0x7mdmbrz5bd") (f (quote (("default")))) (r "1.60.0")))

(define-public crate-prost-reflect-build-0.12.0 (c (n "prost-reflect-build") (v "0.12.0") (d (list (d (n "prost-build") (r "^0.12.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.12.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jmqd5vvpjri4spmn1a5pr0kq2vm02zdmd3p9hhkdz70h86aml5l") (f (quote (("default")))) (r "1.64.0")))

(define-public crate-prost-reflect-build-0.13.0 (c (n "prost-reflect-build") (v "0.13.0") (d (list (d (n "prost-build") (r "^0.12.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.13.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0572g00dbhsjv40r1h11z5kv05fdxymf8idm1nhx4nwki1iwivw8") (f (quote (("default")))) (r "1.70.0")))

