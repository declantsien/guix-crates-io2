(define-module (crates-io pr os prost-codec) #:use-module (crates-io))

(define-public crate-prost-codec-0.1.0 (c (n "prost-codec") (v "0.1.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "01yh9hzbr7hffggh40my16bidxbaypdz58zxg7616j1vqf91xbq0") (r "1.56.1")))

(define-public crate-prost-codec-0.2.0 (c (n "prost-codec") (v "0.2.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "11q9nadhfw9vs5dxqkaspmzy5q6rrn8xa0kkz4apkpsrhgzyj6h1") (r "1.56.1")))

(define-public crate-prost-codec-0.3.0 (c (n "prost-codec") (v "0.3.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "1h3rr1zs6qgwmhzm6qkfxa3w75i5rs0qj5q624a6x2w9zxwlkhqd") (r "1.60.0")))

