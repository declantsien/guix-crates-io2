(define-module (crates-io pr os prost-wkt-build) #:use-module (crates-io))

(define-public crate-prost-wkt-build-0.2.1 (c (n "prost-wkt-build") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "123c5x4ckbnn5bza5d5anyz0pnr9f5jyp1yw0hwgb0xvj5gfrjy5")))

(define-public crate-prost-wkt-build-0.3.0 (c (n "prost-wkt-build") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0ks5jrmr5h4inv9fxds1vkavxn19d4ma2z2als1jwb8301r59qfv")))

(define-public crate-prost-wkt-build-0.3.1 (c (n "prost-wkt-build") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0fd1rnykj0hcyhf4c68sf89k898mrla18d4jazn6rpp8kglkz3f4")))

(define-public crate-prost-wkt-build-0.3.2 (c (n "prost-wkt-build") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "110qi5zyp7mr8k0aiiimvibylcjaism926rs83gdzs38hjp0hir3")))

(define-public crate-prost-wkt-build-0.3.3 (c (n "prost-wkt-build") (v "0.3.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1lya2xqlm2yh9yxbsb6559kqifc44vjrpfpcxqf698p3djfpmyj0")))

(define-public crate-prost-wkt-build-0.3.4 (c (n "prost-wkt-build") (v "0.3.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.2") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.2") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "13zy3ya2ppss5a3vyk878s79pghhbwanxiay2rdcdmvg83a20s9q")))

(define-public crate-prost-wkt-build-0.3.5 (c (n "prost-wkt-build") (v "0.3.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1fn5c38vl58ia0gzn00k5v93slwdz6kqbnnpfdgbdgfp7k9fb3jz")))

(define-public crate-prost-wkt-build-0.4.0 (c (n "prost-wkt-build") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1kzr8yg67mnnp32rahmssql2shamq5l8i7l7pgww4hggifa6iwpd")))

(define-public crate-prost-wkt-build-0.4.1 (c (n "prost-wkt-build") (v "0.4.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0nap4s1icgd4dbbm6572pmw78a5ld9wv5srlq6clrhdfd5rd4n49")))

(define-public crate-prost-wkt-build-0.4.2 (c (n "prost-wkt-build") (v "0.4.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0a3k1xqc98ckqmmqkxn41qrb4k3zbv4pq0fslyk2idykxayaip64")))

(define-public crate-prost-wkt-build-0.5.0 (c (n "prost-wkt-build") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0883g26vrhx07kv0dq85559pj95zxs10lx042pp4za2clplwlcav")))

(define-public crate-prost-wkt-build-0.5.1 (c (n "prost-wkt-build") (v "0.5.1") (d (list (d (n "heck") (r ">=0.4, <=0.5") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1wyfl5sx7irm4f76whyswddbxbrsclrhbph257ix8bicjmjp72sr") (r "1.70")))

