(define-module (crates-io pr os prost-arrow-derive) #:use-module (crates-io))

(define-public crate-prost-arrow-derive-0.0.1 (c (n "prost-arrow-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0smz9lrd6blb5m3awxh9yymbn34bpj7vvlial9s3yavx43pz519a")))

(define-public crate-prost-arrow-derive-0.0.2 (c (n "prost-arrow-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0w5p7jd0d6k39nw0785f84l3qbwcgzvdvdpmkvn204l6b4n1gjba")))

(define-public crate-prost-arrow-derive-0.0.3 (c (n "prost-arrow-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12zbpi3nvxw4x1ai354kcqv19rymvfxpcncgfvrgkridr8hyfiq1")))

