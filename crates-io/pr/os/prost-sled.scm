(define-module (crates-io pr os prost-sled) #:use-module (crates-io))

(define-public crate-prost-sled-0.0.1 (c (n "prost-sled") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1g9q8dhzzrbhka8hvlk2bv48qzgbi9bfp63d11kg54c1jjf7qdzc")))

(define-public crate-prost-sled-0.0.2 (c (n "prost-sled") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "17rk6zaw306sb23l5z2rascnnq1kyr3s85ajxc005h53cwhwjy2l")))

(define-public crate-prost-sled-0.0.3 (c (n "prost-sled") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1zl82wxp7cmpqag9j7f196ld0mdy8zgrkn42d1yhj22mxr93f17w")))

(define-public crate-prost-sled-0.0.4 (c (n "prost-sled") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0gcbywlz2ycldvvcjdzjn6qy5m3m8h6dliygcb45hax1q0r8qg1j")))

(define-public crate-prost-sled-0.0.5 (c (n "prost-sled") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02fyviczkl6br5f9bdcf5fky767l3pgy2xldxghlnv5grphni4qb")))

