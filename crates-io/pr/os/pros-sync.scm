(define-module (crates-io pr os pros-sync) #:use-module (crates-io))

(define-public crate-pros-sync-0.1.0 (c (n "pros-sync") (v "0.1.0") (d (list (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)))) (h "18091d53gqcjz2bsz1ghsghhgyijkv778h2f9q2ypm6zp8zmix8h")))

(define-public crate-pros-sync-0.2.0 (c (n "pros-sync") (v "0.2.0") (d (list (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)))) (h "0fwjgcdwzh407cxm8y701g475bmmbacml5ylfc3q6sid2ncsr6ch")))

