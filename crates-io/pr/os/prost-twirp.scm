(define-module (crates-io pr os prost-twirp) #:use-module (crates-io))

(define-public crate-prost-twirp-0.1.0 (c (n "prost-twirp") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "prost") (r "^0.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gf9w7shvi5yvnal19v4hy8r1hbjv2rdnxclja4flkm0krv5gd9a") (f (quote (("service-gen" "prost-build"))))))

(define-public crate-prost-twirp-0.2.0 (c (n "prost-twirp") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "server" "http1" "http2" "tcp"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bcbfphbya4cjm5svi9h3zp9alwpl56p3q3z085ni65qdk1fjrqs") (s 2) (e (quote (("service-gen" "prost-build" "dep:quote" "dep:proc-macro2"))))))

