(define-module (crates-io pr os prost-codegen) #:use-module (crates-io))

(define-public crate-prost-codegen-0.1.0 (c (n "prost-codegen") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "multimap") (r "^0.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.1.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0lisllgdf31xix8c51ci54ry32h0q43sl82hpqzxdkmj0cq30x31")))

