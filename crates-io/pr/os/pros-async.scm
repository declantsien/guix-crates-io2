(define-module (crates-io pr os pros-async) #:use-module (crates-io))

(define-public crate-pros-async-0.1.0 (c (n "pros-async") (v "0.1.0") (d (list (d (n "async-task") (r "^4.5.0") (k 0)) (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.1") (d #t) (k 0)))) (h "1n4r9yp5hrd4ahcw07k2pwphki94nmydhb78az62nnhhx3n8ya2y")))

(define-public crate-pros-async-0.2.0 (c (n "pros-async") (v "0.2.0") (d (list (d (n "async-task") (r "^4.5.0") (k 0)) (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.1") (d #t) (k 0)))) (h "1l4w9r4ivsbbrqrkziyhxfm0dzjhz9q0s8xzkwzbas84pm99fkmf")))

