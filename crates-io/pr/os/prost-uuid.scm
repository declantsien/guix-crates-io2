(define-module (crates-io pr os prost-uuid) #:use-module (crates-io))

(define-public crate-prost-uuid-0.1.0 (c (n "prost-uuid") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.0") (f (quote ("as_mut" "as_ref" "constructor" "deref" "deref_mut" "display" "from" "from_str" "into"))) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qw1472mg161la1ba47myscfj7ic1v2p9d9hkgmdk17vhcicddfz")))

(define-public crate-prost-uuid-0.2.0 (c (n "prost-uuid") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.0") (f (quote ("as_mut" "as_ref" "constructor" "deref" "deref_mut" "display" "from" "from_str" "into"))) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1i02jc9b60pw1rz6cynd4bysdkcy4nfcwkwcgmnkqrw2q8q14krs")))

