(define-module (crates-io pr os pros-devices) #:use-module (crates-io))

(define-public crate-pros-devices-0.1.0 (c (n "pros-devices") (v "0.1.0") (d (list (d (n "no_std_io") (r "^0.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-sys") (r "^0.7.0") (f (quote ("xapi"))) (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_61" "unstable-core-error"))) (k 0)))) (h "09sbqn5cdz4sy1bywvz700r6a8vg3v0pfvymcr0in72amgvn4hrk")))

(define-public crate-pros-devices-0.2.0 (c (n "pros-devices") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "no_std_io") (r "^0.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pros-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pros-sys") (r "^0.8.0") (f (quote ("xapi"))) (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_61" "unstable-core-error"))) (k 0)))) (h "0mnixj2xabdbl2l4m8gwyjfvv1h4b1d71468hjfxnpp1kzdjg8fg") (f (quote (("dangerous_motor_tuning"))))))

