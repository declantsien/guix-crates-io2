(define-module (crates-io pr os prost-build-config) #:use-module (crates-io))

(define-public crate-prost-build-config-0.4.0 (c (n "prost-build-config") (v "0.4.0") (d (list (d (n "prost-build") (r "^0.9") (d #t) (k 0)) (d (n "prost-helper") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "00n5pw8ayg5gsy6gbc2y1r1bjjqlphm99wx6ycanjk1s5jjygrvl")))

(define-public crate-prost-build-config-0.4.1 (c (n "prost-build-config") (v "0.4.1") (d (list (d (n "prost-build") (r "^0.9") (d #t) (k 0)) (d (n "prost-helper") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0caldzr37naza463b7ykbx4amklxvzq56fs4263f2f8dxrd4czxw")))

(define-public crate-prost-build-config-0.5.0 (c (n "prost-build-config") (v "0.5.0") (d (list (d (n "prost-build") (r "^0.11.1") (d #t) (k 0)) (d (n "prost-helper") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 2)))) (h "1ixcxpyqy5k3fk6vrjds7m8csh1khmpa1x1pfa2rxp1kyqzvaz4g")))

(define-public crate-prost-build-config-0.6.0 (c (n "prost-build-config") (v "0.6.0") (d (list (d (n "prost-build") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-helper") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)))) (h "01bmc8x7v89isijyjw4562m7sapy16hx0nr7v43h0krm5hhwbkkd")))

(define-public crate-prost-build-config-0.6.1 (c (n "prost-build-config") (v "0.6.1") (d (list (d (n "prost-build") (r "^0.12.3") (f (quote ("prettyplease"))) (d #t) (k 0)) (d (n "prost-helper") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)))) (h "0baga4bibq899kp0m3kp12gisvfqxd8n54psfwchxyrjxnvxz682")))

(define-public crate-prost-build-config-0.6.3 (c (n "prost-build-config") (v "0.6.3") (d (list (d (n "prost-build") (r "^0.12.3") (f (quote ("prettyplease"))) (d #t) (k 0)) (d (n "prost-helper") (r "^0.8.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)))) (h "12ih1blj0fs563vm676izxaxsaxsa22h0g3xl6ndcj6vcwy0ri1w")))

