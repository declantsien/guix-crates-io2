(define-module (crates-io pr os prost-stream) #:use-module (crates-io))

(define-public crate-prost-stream-0.1.0 (c (n "prost-stream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17ga0ll6p7xl6q2h7jsbfqzs2qlryd3g34z4i0bc4fqqn27z6q5y") (f (quote (("default")))) (y #t) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-prost-stream-0.1.1 (c (n "prost-stream") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1w3zwc3ysps0x5ha2q575a9gjvp5p0jbhf2b9qwr9fiw8cr3y4wx") (f (quote (("default")))) (y #t) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-prost-stream-0.1.2 (c (n "prost-stream") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1w5xn6ll786f1851nllsm33wswrhapqn9nbs3qxphp60jnlgakv0") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:tokio"))))))

