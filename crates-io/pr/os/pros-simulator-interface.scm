(define-module (crates-io pr os pros-simulator-interface) #:use-module (crates-io))

(define-public crate-pros-simulator-interface-0.1.0 (c (n "pros-simulator-interface") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xh8pgaxi46kbs88yv6ik5w7xp4ic7zq2zzpzky5rn4fzz3whydp")))

(define-public crate-pros-simulator-interface-0.2.0 (c (n "pros-simulator-interface") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "17skbnlawmgzadrvrhli16hdm1nzrfqciv6gwfv7y9cwnacw2r39")))

(define-public crate-pros-simulator-interface-0.3.0 (c (n "pros-simulator-interface") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cgcz605h2z54cqwklna9v0mpj4hvlvb3bwldjksqfx1nrscxnn9")))

(define-public crate-pros-simulator-interface-0.4.0 (c (n "pros-simulator-interface") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aq3jvs6vli856zmfi0sakjjhi0cfq58mf1a6zw80zay4i357rag")))

(define-public crate-pros-simulator-interface-0.5.0 (c (n "pros-simulator-interface") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "088vymcgjsmjsl75c1i15n45pmfa3y91mcljy673rlq91fx9v122")))

