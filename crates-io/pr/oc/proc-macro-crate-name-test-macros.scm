(define-module (crates-io pr oc proc-macro-crate-name-test-macros) #:use-module (crates-io))

(define-public crate-proc-macro-crate-name-test-macros-0.1.0 (c (n "proc-macro-crate-name-test-macros") (v "0.1.0") (h "0l0j4fcvdxk4gkl5a5fay94qgh668fr01pym4r79vij7839anyz7")))

(define-public crate-proc-macro-crate-name-test-macros-0.1.1 (c (n "proc-macro-crate-name-test-macros") (v "0.1.1") (h "079q4zn99n6v4l6vpasb84663i05fg1c1i2ys7w30pmqspbjwz3k")))

(define-public crate-proc-macro-crate-name-test-macros-0.1.2 (c (n "proc-macro-crate-name-test-macros") (v "0.1.2") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hx02f63jlr7asgj3r6g27xnskpyxsxgmkybz4q4r1kwmyi6jqll")))

(define-public crate-proc-macro-crate-name-test-macros-0.1.3 (c (n "proc-macro-crate-name-test-macros") (v "0.1.3") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0il629xxjpwm78i8219nflapmqq44czxqcrkzbkfjs4mawmbckiv")))

