(define-module (crates-io pr oc proc-macro-error-attr) #:use-module (crates-io))

(define-public crate-proc-macro-error-attr-0.3.0-rc1 (c (n "proc-macro-error-attr") (v "0.3.0-rc1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "1lm6z5mrhwqyxkx36azl0npjd1dr8kd7f9rrybapf42airwrbxky")))

(define-public crate-proc-macro-error-attr-0.3.0-rc.1 (c (n "proc-macro-error-attr") (v "0.3.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "17yxwcrrp9r17nbp7py8jvbs7ybjr0lv0q80w78m42bi96lkmv4b")))

(define-public crate-proc-macro-error-attr-0.3.0 (c (n "proc-macro-error-attr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "0mjvzn7w13rjw0p1jjmzp9csws249z8xss7cy5c3grfi5hxn4118")))

(define-public crate-proc-macro-error-attr-0.3.2 (c (n "proc-macro-error-attr") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0kmcnjnxf9qd5b8ljx825v0dbwyr3cyp0xc70qmc9i4il4x209dx")))

(define-public crate-proc-macro-error-attr-0.3.3 (c (n "proc-macro-error-attr") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1mzyby57f78bc4xqvix2wjipb5k8amzlbqrfzdg464n2xwphb3h4")))

(define-public crate-proc-macro-error-attr-0.3.4 (c (n "proc-macro-error-attr") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1jg71mqzzhvjxsj8c64h7yg7ji05lkggpani34vsmcqrm0nf4h6i")))

(define-public crate-proc-macro-error-attr-0.4.0 (c (n "proc-macro-error-attr") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "07q43gzriwy5883k5pb2l5p0im30kqmdsr65ixcc0323diz3sjjv")))

(define-public crate-proc-macro-error-attr-0.4.1 (c (n "proc-macro-error-attr") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cqx54rsyhzbffkxj0hh6hnfy8s5iwq31pjz800254823ayhhq51")))

(define-public crate-proc-macro-error-attr-0.4.3 (c (n "proc-macro-error-attr") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.4") (d #t) (k 0)))) (h "0v1c75qy0gmd931ajw40zh6q6pj49rwn3zfcpsbf5lzm7i4mvgy2")))

(define-public crate-proc-macro-error-attr-0.4.5 (c (n "proc-macro-error-attr") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.4") (d #t) (k 0)))) (h "14nq1h9gxrsfm8g17kdmfqvydvday7p0018rmjxmdb3bmrg2x0r3")))

(define-public crate-proc-macro-error-attr-0.4.6 (c (n "proc-macro-error-attr") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "05kzzk5hlwqxhynbv3jq87a764988bwsbkwf6kdk0gxf1sm4sghz")))

(define-public crate-proc-macro-error-attr-0.4.7 (c (n "proc-macro-error-attr") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0c4wy1sw6wykd0j6p3bys2037546azny71s9ibnkn4rsjrsn6dh7")))

(define-public crate-proc-macro-error-attr-0.4.8 (c (n "proc-macro-error-attr") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0yiwdy12lhv1l1jwnhnb46jh377hdppsafyp3sh52hv6lagpswf5")))

(define-public crate-proc-macro-error-attr-0.4.9 (c (n "proc-macro-error-attr") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0lwjv931536h8bpx6hy0dbq04dgz7w92fmhnwdijx467h7sbwxfi")))

(define-public crate-proc-macro-error-attr-0.4.10 (c (n "proc-macro-error-attr") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1qbhj6jc1fk6rcmwv3nn2rbc0grgpqcq7dj8baspdwb29d9h0p87")))

(define-public crate-proc-macro-error-attr-0.4.11 (c (n "proc-macro-error-attr") (v "0.4.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0fjl9mh6kj2dvmi29jvp9v181swm387ajh5rkw0mw7crangjs074")))

(define-public crate-proc-macro-error-attr-0.4.12 (c (n "proc-macro-error-attr") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1pk9mwcfnpf8favgc2cl4sqlmi818p96hg8pfb51wg5nzmvlnnwa")))

(define-public crate-proc-macro-error-attr-1.0.0 (c (n "proc-macro-error-attr") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "00x50vlc7mdmd0r5n8k80z4iyqw6h3qd7h94p6i1r3mak6c8h8fx")))

(define-public crate-proc-macro-error-attr-1.0.1 (c (n "proc-macro-error-attr") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "161gifrkcpzljri267jrckyj00naf80xakin55asxrrf85pm6ir1")))

(define-public crate-proc-macro-error-attr-1.0.2 (c (n "proc-macro-error-attr") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0lrf9rwp1529q9g6s2c8r2d5d82jx3vm3p17gyymm4z9skm48m2g")))

(define-public crate-proc-macro-error-attr-1.0.3 (c (n "proc-macro-error-attr") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0l2w8caxbx5n30qx3pgj6hzmlcy2gzd3cjf4bql83dbyl5f7kj9w")))

(define-public crate-proc-macro-error-attr-1.0.4 (c (n "proc-macro-error-attr") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0sgq6m5jfmasmwwy8x4mjygx5l7kp8s4j60bv25ckv2j1qc41gm1")))

