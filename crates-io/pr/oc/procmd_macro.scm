(define-module (crates-io pr oc procmd_macro) #:use-module (crates-io))

(define-public crate-procmd_macro-0.1.0 (c (n "procmd_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vec1") (r "^1.6") (d #t) (k 0)))) (h "17v73rw15qs6z4mzz9qgszrzpaa4crx2g1x3l5xrgc1jr42n2la6")))

