(define-module (crates-io pr oc proc_macro_global_state) #:use-module (crates-io))

(define-public crate-proc_macro_global_state-0.1.0 (c (n "proc_macro_global_state") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0ci0473hsnvqrn49k3q5vjnn6xz97ihypbm06wp95hjx6vsfmfri")))

