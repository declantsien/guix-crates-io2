(define-module (crates-io pr oc proclock-macro) #:use-module (crates-io))

(define-public crate-proclock-macro-0.1.0 (c (n "proclock-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "1x843l1vp9q2g74f841c5kbn6xflw608zjq8bqr7gp5s9m00h800")))

(define-public crate-proclock-macro-0.1.1 (c (n "proclock-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0pggkbqq6rvyjzb9nkvs2zxm85jn9cbs8zplsf01h75qb5d02960")))

(define-public crate-proclock-macro-0.1.2 (c (n "proclock-macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0jxi0glfsx6bzlhw30irb5skl9amax374mswcjm6chhzwbcaxahw")))

(define-public crate-proclock-macro-0.2.0 (c (n "proclock-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8gvhsnl5225q19by7maanxp1k0i5vmv6l8vam7crc29hh8f00f")))

(define-public crate-proclock-macro-0.2.1 (c (n "proclock-macro") (v "0.2.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0ii5kkkij2bvjhw4a87cbsxskp4kmad75wnzr1zr5lbb4pdxrmiz")))

(define-public crate-proclock-macro-0.2.2 (c (n "proclock-macro") (v "0.2.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "08idyl2m32m1bh98l1ag0yrpc07ims632i333zxhfhwzf2k24mr6")))

