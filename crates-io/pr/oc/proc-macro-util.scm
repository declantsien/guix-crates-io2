(define-module (crates-io pr oc proc-macro-util) #:use-module (crates-io))

(define-public crate-proc-macro-util-0.1.0 (c (n "proc-macro-util") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "09q719vxqylz6rjr86x5bjf6h9r94c4imi41djspn1lz5vi2f8ra")))

(define-public crate-proc-macro-util-0.1.1 (c (n "proc-macro-util") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "082mjwa92xijwlp2503hcsxf57bad8lzgllwb83b9x1ggzhpgm6j")))

