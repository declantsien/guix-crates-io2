(define-module (crates-io pr oc proc_macro2_helper) #:use-module (crates-io))

(define-public crate-proc_macro2_helper-0.1.0 (c (n "proc_macro2_helper") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "125j75gn0z4nr85yc1n5n9xf0jnbfvd4dgkhkjlssvrxvaki8d4n")))

(define-public crate-proc_macro2_helper-0.1.1 (c (n "proc_macro2_helper") (v "0.1.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rrykm92fm4nvayl5nblrqkargb7hwsl3qf3i2z23221mwibjak7")))

(define-public crate-proc_macro2_helper-0.1.2 (c (n "proc_macro2_helper") (v "0.1.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1aila3kv1r9p99q92gbph9sdrcm7nkbw7a2li66p0jlf3lifgymp")))

(define-public crate-proc_macro2_helper-0.2.0 (c (n "proc_macro2_helper") (v "0.2.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "07f9wmz1f2rglpwfm5zgc1gp88kh6z1x2iwczq1h6c4syq523y3m")))

(define-public crate-proc_macro2_helper-0.2.1 (c (n "proc_macro2_helper") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qj9jzkk7cws4jdqllp9gwjwnkwp0jra2nf27apgjd6fkhhdapws")))

(define-public crate-proc_macro2_helper-0.2.2 (c (n "proc_macro2_helper") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15225jcwyy0l59dmia2r3wldlvgn44c4yf50ilw60l24s3a5qyic")))

(define-public crate-proc_macro2_helper-0.2.3 (c (n "proc_macro2_helper") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19x0sr32ghd4nfzx6m1dpss641sxmrr7wnrv6q34hmlq3w5xdq4v")))

(define-public crate-proc_macro2_helper-0.2.4 (c (n "proc_macro2_helper") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1chczjz9hxqyr399j9nq29rvavymrswlqwmn366vvq1n3fv8fcr2")))

(define-public crate-proc_macro2_helper-0.2.5 (c (n "proc_macro2_helper") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18paivpx5glsl7261m35xcka7nbd862q7ghi73ayi4j9i2b0adfw")))

(define-public crate-proc_macro2_helper-0.2.6 (c (n "proc_macro2_helper") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "02frl263qvdp9s7bw18dh1wkgjxkx8v921j9qdvyd2lxchbf07ya")))

(define-public crate-proc_macro2_helper-0.2.7 (c (n "proc_macro2_helper") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18aa3fgzmp2wc5mah8inh1m6dpjk999wysi8pg96876h1q445hcc")))

(define-public crate-proc_macro2_helper-0.2.8 (c (n "proc_macro2_helper") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0p39896m4vdhf9pi0w0kx6j75azd23qnggxllg8678ir2j30c8ql")))

(define-public crate-proc_macro2_helper-0.2.9 (c (n "proc_macro2_helper") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "16f77bd7ikknf648ahmsvgiaik3z3ajixryd89211xcy7qwihzfy")))

(define-public crate-proc_macro2_helper-0.2.10 (c (n "proc_macro2_helper") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00ami03ym287fr54gmk00qvfb50k9yvbdv5mzqb224fsf3pqnlkr")))

