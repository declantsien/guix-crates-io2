(define-module (crates-io pr oc proc-bitfield-macros) #:use-module (crates-io))

(define-public crate-proc-bitfield-macros-0.0.1 (c (n "proc-bitfield-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "19ihp1ps8mf019s0rahx3v3v8c4vzc51najhg3nm34wprs991gmr") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.1.0 (c (n "proc-bitfield-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0la7jq32jyvbvfc1s4gj9wq3jpsni2km5qd1mldi4r6lz04a4z89") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.2.0 (c (n "proc-bitfield-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0mdni002vm0r6lv7zkid8nf14cvsv5qf2hv1wlr9lbs07rdg9yz1") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.2.1 (c (n "proc-bitfield-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1xp4d1z1sf508ynpnqkcypkswxyayd6c4g7pbxs6idvinxbywrl1") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.2.2 (c (n "proc-bitfield-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1nlbjhpnaa8iiryh3832myrqa6wrhxl30xc8bjr437n8lhabcnzp") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.2.3 (c (n "proc-bitfield-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0xzb3mkn9z7j64yzpcfb63wp44p1q5yy4ycs9m67zbay65mrwh49") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.3.0 (c (n "proc-bitfield-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1d26h1c84nfc6895j63bn7c6xsg1r6sxd5dzwza4xjlkx5126aq4") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.3.1 (c (n "proc-bitfield-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "05cm2ap74kxm858rkadiapshcbxc1phkc30lwbai2mwvlxaqgmwh") (f (quote (("nightly"))))))

(define-public crate-proc-bitfield-macros-0.4.0 (c (n "proc-bitfield-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1yirkh1911px1fh8y96kmyf0k8x7zjdk35sv7qpngsk08w9r7hf8") (f (quote (("nightly"))))))

