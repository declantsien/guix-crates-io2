(define-module (crates-io pr oc proctitle) #:use-module (crates-io))

(define-public crate-proctitle-0.1.0 (c (n "proctitle") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zgah4d1i8ilgzrkr5bpk2wsv0ajv2sjxvjhccwdbz059ys1b1br")))

(define-public crate-proctitle-0.1.1 (c (n "proctitle") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.53") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ddalbvfmwb7d9l3g2wrpjh1768jx8sm176izrikswlhvshdhk4j")))

