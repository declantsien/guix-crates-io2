(define-module (crates-io pr oc procinfo) #:use-module (crates-io))

(define-public crate-procinfo-0.1.1 (c (n "procinfo") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^0.3") (d #t) (k 0)))) (h "05whg3jw10iccdjwnbl27mw3jx12l8fpmgp2s15js10asb7wxr2w")))

(define-public crate-procinfo-0.2.0 (c (n "procinfo") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.0") (d #t) (k 0)))) (h "1w2nqp762aiv5rkkbm997i507qazkjf99w3rih979nzcrvvbqa15")))

(define-public crate-procinfo-0.2.1 (c (n "procinfo") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "1mbx7wrg2k45i9raskhzc31ajx4bf5c09jwy8kpvsi2zlz4ckf8q")))

(define-public crate-procinfo-0.2.2 (c (n "procinfo") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "03ixwsrndw6m70hcx8vy1bp46i83qm8mn71hn0jw1yv8sih9k3wx")))

(define-public crate-procinfo-0.2.3 (c (n "procinfo") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "1ymn6mjjja45jlmqa4sfl9xn0x38vpr5n557mqcvymspjqp7dpb6")))

(define-public crate-procinfo-0.3.0 (c (n "procinfo") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "0qns39jfz4gj2pg6lmacirdgcgi6gz8688z107aw0c6c2ywdm889")))

(define-public crate-procinfo-0.3.1 (c (n "procinfo") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0v5gfwmhfv7br7q2m8csjaaafav4qpcdmbl1i61hccrahmw8abpl")))

(define-public crate-procinfo-0.4.0 (c (n "procinfo") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0yr3q2gl77mqlmxv7hladxfdl9wy1m80kb081vsd367l72q4b7rf")))

(define-public crate-procinfo-0.4.1 (c (n "procinfo") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "19s8zg186lzc1z7wwnppwh3jkq781gd898w3z358kdirjk8mwcb4")))

(define-public crate-procinfo-0.4.2 (c (n "procinfo") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "175l4cprsb56jrkycpz0743clgc8fyhxv4i8hhgqjd967mzl5cba")))

