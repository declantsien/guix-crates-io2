(define-module (crates-io pr oc process_killer) #:use-module (crates-io))

(define-public crate-process_killer-0.1.0 (c (n "process_killer") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "010fh3dm02rk1siqp3br4fqc1vc2czml4yxl7wacx00chbv0flz8")))

