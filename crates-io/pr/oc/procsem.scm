(define-module (crates-io pr oc procsem) #:use-module (crates-io))

(define-public crate-procsem-0.1.0 (c (n "procsem") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1ksk1r8m6akj1flrllxa81sy15k18bx2f7sa7zicf8zzcv3624fy")))

