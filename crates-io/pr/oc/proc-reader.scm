(define-module (crates-io pr oc proc-reader) #:use-module (crates-io))

(define-public crate-proc-reader-0.1.0 (c (n "proc-reader") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1iv2b2fhgbp665cwwjj1djcclvqd1zw3i9ql90zhjpy9hrcwn9jh")))

(define-public crate-proc-reader-0.1.1 (c (n "proc-reader") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1i0svrch03d3yyaail1b129jgv7pw01rfis3kjj9pv7gi9advawk")))

(define-public crate-proc-reader-0.1.2 (c (n "proc-reader") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0k16ky812i8x7i2j5z6cafdiw7x0m56pzf10jw4gkyx6nbim434b")))

(define-public crate-proc-reader-0.2.0 (c (n "proc-reader") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1p308lvswb4rvmln273lz440n11j71d0qvjsikri8x0mmmky759i")))

(define-public crate-proc-reader-0.2.1 (c (n "proc-reader") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0hqfx7km13nzqridh83izck88aasf51c778dlzgyh8adqwb53fcm")))

(define-public crate-proc-reader-0.2.2 (c (n "proc-reader") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "15pbd32cs7pf4i52ic382xkdm2j31z7h2qmghyppgxpb5b92fdv3")))

(define-public crate-proc-reader-0.3.0 (c (n "proc-reader") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "01w6s4jhjb7m56fl93mvy6cz8xsh1a9asrb2x8pdd5784csrkkhj")))

(define-public crate-proc-reader-0.3.1 (c (n "proc-reader") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0wn6y5cg2472kkc82a5nz504cj724rwyrkv3xlgv70njvk8vpbxa")))

(define-public crate-proc-reader-0.4.0 (c (n "proc-reader") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "14rzz3lhgmfrh8awm0zkv8lccxzbb5wi5x4g93f62cnh0k961110")))

(define-public crate-proc-reader-0.5.0 (c (n "proc-reader") (v "0.5.0") (d (list (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "nix") (r ">=0.19.0, <0.20.0") (d #t) (k 0)) (d (n "skeptic") (r ">=0.13.0, <0.14.0") (d #t) (k 1)) (d (n "skeptic") (r ">=0.13.0, <0.14.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "16zrz8sknjyb47r12c8mwjzjafiwv03rypja5xsrw22niwyxfzgs")))

(define-public crate-proc-reader-0.5.1 (c (n "proc-reader") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gb0f9n5wwzwv5kymf1nyl6mmdqkqmh1ing692g8slbgzcyxcmcj")))

