(define-module (crates-io pr oc proc-macro-kwargs) #:use-module (crates-io))

(define-public crate-proc-macro-kwargs-0.1.0 (c (n "proc-macro-kwargs") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "11ff6vgqs3p5if7kw5wpynm92zqfkwxgb3rwhkfhcijya843pzbg")))

(define-public crate-proc-macro-kwargs-0.1.1 (c (n "proc-macro-kwargs") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "1nv7nnj8aga5900xlmrzidj70mbvxlzp60k0id13vs4di9ds5cr8")))

(define-public crate-proc-macro-kwargs-0.1.2 (c (n "proc-macro-kwargs") (v "0.1.2") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "157n4rn7cgrx7bl3zyq4bzc5ncr36p88rw2wxbpcbyciz0z353vf")))

(define-public crate-proc-macro-kwargs-0.1.3 (c (n "proc-macro-kwargs") (v "0.1.3") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "0v9i0zpna51hl0nrcfrwalcibqqqhkm8wglq51cx2yxl1b0lhq52")))

(define-public crate-proc-macro-kwargs-0.1.4 (c (n "proc-macro-kwargs") (v "0.1.4") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "0sklbbiv0lsq7f4jvx4mkbz4wgfvgbn32vlv3fq7rknik197mcp8")))

(define-public crate-proc-macro-kwargs-0.1.5 (c (n "proc-macro-kwargs") (v "0.1.5") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "extra-traits"))) (k 0)))) (h "0yllkhb9bhl5hb2gb92178j8g0r5vprp7p2la06mqm1pi9nmrakd")))

(define-public crate-proc-macro-kwargs-0.2.0-alpha.1 (c (n "proc-macro-kwargs") (v "0.2.0-alpha.1") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "derive" "extra-traits"))) (k 0)))) (h "0iqpizfnlfivsp20lwmmvqgp3b729nnw4j4dqclib20q5xgm9yhb")))

(define-public crate-proc-macro-kwargs-0.2.0 (c (n "proc-macro-kwargs") (v "0.2.0") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "proc-macro-kwargs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "derive" "extra-traits"))) (k 0)))) (h "0d3nkg2b1j43kcszxzq99iri1knnsfg3qmrkg3i669r13ihaxdry")))

