(define-module (crates-io pr oc proc-test-catalog) #:use-module (crates-io))

(define-public crate-proc-test-catalog-0.1.0 (c (n "proc-test-catalog") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "test-catalog") (r "^0.1.0") (d #t) (k 0)))) (h "0dddkkjvfsi9r7sfdb4ajnzbij6nc2pmyk5qp1gkacxg0smz3xaf")))

