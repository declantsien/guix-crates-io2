(define-module (crates-io pr oc proc-lock-macro) #:use-module (crates-io))

(define-public crate-proc-lock-macro-0.3.0 (c (n "proc-lock-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "1mfry1ldgxaw9xa1lckih8i6ii3spw3lxggs70g9r09c95bdyrp4")))

(define-public crate-proc-lock-macro-0.3.1 (c (n "proc-lock-macro") (v "0.3.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0shwxrymp6s5308viibd1zha6482nn88d5q68r09g3iixc82v4cy")))

(define-public crate-proc-lock-macro-0.3.2 (c (n "proc-lock-macro") (v "0.3.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0k7vkfcfim5zpxwmp2niyvcjpwsdld7zsdvi50hm9cg8grj7zs30")))

(define-public crate-proc-lock-macro-0.3.3 (c (n "proc-lock-macro") (v "0.3.3") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "003a0g18a4002nzlhgryv8mw2ssk4149mrng4rilv1ib28b5p21h")))

(define-public crate-proc-lock-macro-0.4.0 (c (n "proc-lock-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-lock-api") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "1w1ainhmh5lb23j2w3zaz2wddqcy6d6i4cxfz6x4n2q5vz9g5l2q")))

