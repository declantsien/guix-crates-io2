(define-module (crates-io pr oc procdump) #:use-module (crates-io))

(define-public crate-procdump-0.0.0 (c (n "procdump") (v "0.0.0") (d (list (d (n "procfs") (r "^0.7.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0ldymab59sbw2hnxbbbvgbxl6rllxp6p755f0jvbfqjg74l5yih4")))

(define-public crate-procdump-0.1.0 (c (n "procdump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "procfs") (r "^0.7.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.8") (d #t) (k 0)))) (h "09r354hx671lyr2hbdp95pka9qa9zk3dqq0xvsjydmkcbpvvrd90")))

(define-public crate-procdump-0.1.3 (c (n "procdump") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.40") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "procfs") (r "^0.9") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.13") (d #t) (k 0)))) (h "0j48h3337vf1svhlli7x0akczqksmd407kbri0qgfmv27ng3fsam")))

