(define-module (crates-io pr oc proclynx) #:use-module (crates-io))

(define-public crate-proclynx-0.1.0 (c (n "proclynx") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1alf7pjwk3zz386wvwlfpsq9bsmb4kv4kr9hy18k7c79sqm4k51q") (y #t)))

(define-public crate-proclynx-0.1.1 (c (n "proclynx") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0ahmljvqw3i7i4c9wjl4jvs8ymglrd9rqj12k1y6mwmg9qv445al")))

