(define-module (crates-io pr oc procrec) #:use-module (crates-io))

(define-public crate-procrec-0.3.0 (c (n "procrec") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "psutil") (r "^3.0.1") (f (quote ("process"))) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "07374rlpy67a5azzc6c0k81cvrfnq8h6ljfdhqjya75n9z8rwrw0")))

