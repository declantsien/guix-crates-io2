(define-module (crates-io pr oc procure) #:use-module (crates-io))

(define-public crate-procure-0.1.0 (c (n "procure") (v "0.1.0") (d (list (d (n "sysconf") (r "*") (d #t) (k 0)))) (h "1s2ccij6pf9k82kkk20m0354g5gjwblha8w97j84jvfvkl94ddkg")))

(define-public crate-procure-0.1.1 (c (n "procure") (v "0.1.1") (d (list (d (n "sysconf") (r "*") (d #t) (k 0)))) (h "1cmzf01jwzbkfqxi0pl421y21b96z9xjf3jzb4qk3pp75jzdf7zs")))

(define-public crate-procure-0.1.2 (c (n "procure") (v "0.1.2") (d (list (d (n "sysconf") (r "^0.1") (d #t) (k 0)))) (h "0ijlkrn1fvl5sbkwj1y3nqv9mvxmpyx1p1dw1vql8lqr7rp6dhyi")))

