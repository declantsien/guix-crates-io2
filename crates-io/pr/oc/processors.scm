(define-module (crates-io pr oc processors) #:use-module (crates-io))

(define-public crate-processors-0.1.0 (c (n "processors") (v "0.1.0") (d (list (d (n "pcm-flow") (r "^0.3.0") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "1533ff6sw2gzhxi9maa1l4vhvmsv4ban0dz1621rgb4sri2cg8yk")))

(define-public crate-processors-0.2.0 (c (n "processors") (v "0.2.0") (d (list (d (n "pcm-flow") (r "^0.5.0") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "08gc7s9hzihx3iw84ijbpflha0qg69rvj6ab4i90yayf5ys5l2yf")))

(define-public crate-processors-0.2.1 (c (n "processors") (v "0.2.1") (d (list (d (n "pcm-flow") (r "^0.5.0") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "09893kll0s6zcqr9ayqb6wf1viksfp92a85zmc3jcz2gyj4wcgb6")))

