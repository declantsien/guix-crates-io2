(define-module (crates-io pr oc proc-macro-rules) #:use-module (crates-io))

(define-public crate-proc-macro-rules-0.1.0 (c (n "proc-macro-rules") (v "0.1.0") (d (list (d (n "proc-macro-rules-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wg0ivjn17zbw88a8l9zwx7rj5ivbqd7xrs3j5pkl74pf6bmbyg4")))

(define-public crate-proc-macro-rules-0.1.1 (c (n "proc-macro-rules") (v "0.1.1") (d (list (d (n "proc-macro-rules-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d1q6mqv9xh8499nfhh3k5cz7zjz3rf543yj8qzbwfhbg3z0h383")))

(define-public crate-proc-macro-rules-0.2.0 (c (n "proc-macro-rules") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-rules-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01r6v1yfczs0mnl5pbvbk672iv2xjfjm07ra5h3j9cxrjd9dnjfr")))

(define-public crate-proc-macro-rules-0.2.1 (c (n "proc-macro-rules") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-rules-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r78fjbd4izdrhmlrqlpfy6lynhqcahghz1ngbd6jyyddmhsmn87")))

(define-public crate-proc-macro-rules-0.4.0 (c (n "proc-macro-rules") (v "0.4.0") (d (list (d (n "proc-macro-rules-macros") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pwv2dcyml5x025a0fzbywn6gqsmyrrwd4rk4g0h1vs3wvj7ghh7")))

