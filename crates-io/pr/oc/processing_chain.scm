(define-module (crates-io pr oc processing_chain) #:use-module (crates-io))

(define-public crate-processing_chain-0.1.0 (c (n "processing_chain") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "14grk2y3in81n0vi8vvqjl64f5c2npm0a2fm2lqjdjjv8b88l022")))

(define-public crate-processing_chain-0.1.1 (c (n "processing_chain") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "03sa40q02s36dsad5pqy9r4ir7rxrxkdyfywhi9xrrgqlrqdczmk")))

(define-public crate-processing_chain-0.2.0 (c (n "processing_chain") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0yjyk8qppl6dqlnrw1wik965nj7x6j4farszil2zip7y9k2f0zhd")))

(define-public crate-processing_chain-0.2.1 (c (n "processing_chain") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1yhfhjslsnsfzwby9hjvd44npabllihs91xwqc43wm6lb0fqacms")))

(define-public crate-processing_chain-0.2.2 (c (n "processing_chain") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "05gd6al420mc6fqz14rnj01lfr5y821apagddxyfwfgqiavrakzk")))

