(define-module (crates-io pr oc process_vm_io) #:use-module (crates-io))

(define-public crate-process_vm_io-1.0.0 (c (n "process_vm_io") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "1n42sm4p2g0i4n1fzb2nmxlysmsihy8nnb1mil356pby6bsl5fj8")))

(define-public crate-process_vm_io-1.0.1 (c (n "process_vm_io") (v "1.0.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)))) (h "12abr6fg64alb6rq399zm1j1f9nhk667kn70cn810f31kfqmbsw6")))

(define-public crate-process_vm_io-1.0.2 (c (n "process_vm_io") (v "1.0.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0dxkvfp6yv6cxl81dsbm7fwmba5h3qlpqhibrvsfkjhgbmsdcysw")))

(define-public crate-process_vm_io-1.0.3 (c (n "process_vm_io") (v "1.0.3") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "191myj2prrl8z6imw1675584ny3n0pj0vwivlgi1ah5mydx6bwkc")))

(define-public crate-process_vm_io-1.0.4 (c (n "process_vm_io") (v "1.0.4") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "02zbsyg55jl9gcyhclsy40qgmpk9ljl5qr270kbc07743ypczfcp")))

(define-public crate-process_vm_io-1.0.5 (c (n "process_vm_io") (v "1.0.5") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "19dqakzd879wl39dhmazv0wkfaadm731bhh9ghwqhg31xz9f9laj")))

(define-public crate-process_vm_io-1.0.6 (c (n "process_vm_io") (v "1.0.6") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "050rrrmz2drj8bn90nf81fdx1f8725pw24kj3zgiq91qh4b030pk")))

(define-public crate-process_vm_io-1.0.7 (c (n "process_vm_io") (v "1.0.7") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (d #t) (k 0)))) (h "12pi41nb6g2b4s0c1v8k06nvlhp2hlcka12566kpwcbdb8hhcfqk")))

(define-public crate-process_vm_io-1.0.8 (c (n "process_vm_io") (v "1.0.8") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0x0mh76sawwr57jadfhnx8abbsjpfvdpcdxjzqgg6j6a6idv8ja6")))

(define-public crate-process_vm_io-1.0.9 (c (n "process_vm_io") (v "1.0.9") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (d #t) (k 0)))) (h "03z2wmvr0zpjradwhavbbd1nfwddnskqb380iihk810bafp8kbpa")))

(define-public crate-process_vm_io-1.0.10 (c (n "process_vm_io") (v "1.0.10") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0dz2zzqrj0bjzc1lh2vcz927q0hafi9w3rc6spm7j0q5ignxvs95")))

