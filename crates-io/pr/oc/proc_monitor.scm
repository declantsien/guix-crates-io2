(define-module (crates-io pr oc proc_monitor) #:use-module (crates-io))

(define-public crate-proc_monitor-0.1.0 (c (n "proc_monitor") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ia2llb5maygnws8c57qzl90i7q2v991pc4bgkrg9arb5pqvha50")))

(define-public crate-proc_monitor-0.1.1 (c (n "proc_monitor") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06q9bfnr51x61yz5ckn7g1x2fyq034jwd1grkfyi4kpxv4bz53mv")))

(define-public crate-proc_monitor-0.1.2 (c (n "proc_monitor") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rs50wfar52l6sy6lwi76wac9gckyix03g17p1c01jwfb8681rr6")))

