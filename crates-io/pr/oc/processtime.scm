(define-module (crates-io pr oc processtime) #:use-module (crates-io))

(define-public crate-processtime-0.1.0 (c (n "processtime") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)))) (h "1zjd0dkpp5d4nfm1s6v76vswpvvvw9pa8d8vk8g3m5l9ldi19zf0")))

(define-public crate-processtime-0.1.1 (c (n "processtime") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)))) (h "1xhq6d2gfagcx72ic1dfbghv68y5d7va39bs5m9dxfksvq1zh43y")))

(define-public crate-processtime-0.2.0 (c (n "processtime") (v "0.2.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("cargo" "color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)))) (h "1y8ajql47yj4c9inyik8rqdvbd575y7ydp8mr6iqmc3jscyylfzj")))

