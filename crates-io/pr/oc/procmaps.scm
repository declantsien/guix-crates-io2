(define-module (crates-io pr oc procmaps) #:use-module (crates-io))

(define-public crate-procmaps-0.1.0 (c (n "procmaps") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "02gras601hd2ivaq2mjzic9xrd36zax1p8ahfd68wzcbk4san7bq")))

(define-public crate-procmaps-0.2.0 (c (n "procmaps") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "1ly1ms0qk646nqhc3rzrlp479gpi13dql0rq47krhl0rprw65gbi")))

(define-public crate-procmaps-0.3.0 (c (n "procmaps") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "1hmhcq44d7mnkbmbz5qfrpf8fw1mn9x6x3rn21dv6p49yyshb155")))

(define-public crate-procmaps-0.4.0 (c (n "procmaps") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "0p14353ay4hmr7bs380s09ai1lz69h8b990qvj3fncw314jxigf7")))

(define-public crate-procmaps-0.4.1 (c (n "procmaps") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "0y5ppaib9pl5a08l6gy7krh46v0bvs15qlly264gz5bcvz2q3myn")))

