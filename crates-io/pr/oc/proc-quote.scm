(define-module (crates-io pr oc proc-quote) #:use-module (crates-io))

(define-public crate-proc-quote-0.1.0 (c (n "proc-quote") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (k 0)) (d (n "proc-quote-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "0gjmhx1kvfsrpp21f4s2iwd0hdbyqmvzb34v2m1ssnv5rdjimvs1")))

(define-public crate-proc-quote-0.1.1 (c (n "proc-quote") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "proc-quote-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "05scim10g7m511nvibkmy1jr74xwqqpnnp685laradvxdy8vqpqh")))

(define-public crate-proc-quote-0.2.0 (c (n "proc-quote") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "proc-quote-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1cckl1sy7j157zz7mjzjlif1lhvvm15a60fb3j0q16n58j014cir")))

(define-public crate-proc-quote-0.2.1 (c (n "proc-quote") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "proc-quote-impl") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "1s3kg04kwjdd99ibsklcmai8i7klz4w8p2lzv049q03xiblcplqm")))

(define-public crate-proc-quote-0.2.2 (c (n "proc-quote") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "proc-quote-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (d #t) (k 0)))) (h "1dc5aswbakm7xi67prc2wp317lx9hm90pqvc3qz03nizy91jaqgs")))

(define-public crate-proc-quote-0.2.4 (c (n "proc-quote") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "proc-quote-impl") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02k9213ndzb1h74qidrx50sm3px53hr11k6irmrz5k7f0ddza409") (y #t)))

(define-public crate-proc-quote-0.3.0 (c (n "proc-quote") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "proc-quote-impl") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11bc558nxs0v0gz80xj0f77xbf498gabznjw940a88pgd9phpznq")))

(define-public crate-proc-quote-0.3.1 (c (n "proc-quote") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "proc-quote-impl") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ajrafwrgdib9iqjlk324g7rm3h3iwih6xq4bfz5wn6fzipglgyn")))

(define-public crate-proc-quote-0.3.2 (c (n "proc-quote") (v "0.3.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "proc-quote-impl") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10f0lhpiwzb33zb61s9gkjfxdmhk7rl7z75y75wd0f94i0k45sh6")))

(define-public crate-proc-quote-0.4.0 (c (n "proc-quote") (v "0.4.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "proc-quote-impl") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0051nax31x1yzr1imbp200l2gpz6pqcmlcna099r33773lbap12y")))

