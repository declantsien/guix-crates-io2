(define-module (crates-io pr oc procr_ansi_format) #:use-module (crates-io))

(define-public crate-procr_ansi_format-0.1.0 (c (n "procr_ansi_format") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "ra-ap-rustc_index") (r "^0.14.0") (d #t) (k 0)) (d (n "ra-ap-rustc_lexer") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1knzkg4k1pix6xdn2sgyz41g2yb6biwca2h6i21n2jc71mrx939j")))

