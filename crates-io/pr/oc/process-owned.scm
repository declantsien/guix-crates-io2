(define-module (crates-io pr oc process-owned) #:use-module (crates-io))

(define-public crate-process-owned-0.1.0 (c (n "process-owned") (v "0.1.0") (h "1s1ri9iirk825pva0hfimnhvl5dmhwbm9p4v40mr9ymms57shrsk")))

(define-public crate-process-owned-0.2.0 (c (n "process-owned") (v "0.2.0") (h "0hnr47j7jk42i5pwimyzadj6jwqw8c5xfzhg96612igg0rjang00")))

