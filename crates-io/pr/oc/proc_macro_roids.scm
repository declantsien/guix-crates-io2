(define-module (crates-io pr oc proc_macro_roids) #:use-module (crates-io))

(define-public crate-proc_macro_roids-0.1.0 (c (n "proc_macro_roids") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1cbmza1z32h6nq1gc3r140vdxssv97c7831zhgxq610dy4fg0j4f")))

(define-public crate-proc_macro_roids-0.2.0 (c (n "proc_macro_roids") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "13zymb4xdwq2v8sn9m2m0kphzib8m8rj74argqqa60jfnbq6k13m")))

(define-public crate-proc_macro_roids-0.2.1 (c (n "proc_macro_roids") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "001rmyh65vdw4c89b7wmal944fy15qkm016dppjj4a51dab6igl8")))

(define-public crate-proc_macro_roids-0.3.0 (c (n "proc_macro_roids") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "14liw4kh9v86hxn31dxl5r29r1k845f0ms8c3gfp5c4gzzpj83hs")))

(define-public crate-proc_macro_roids-0.4.0 (c (n "proc_macro_roids") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1fssjd4x23kfjpfzxw20rd4xl64k83agnpj87knbf22qn75b1796")))

(define-public crate-proc_macro_roids-0.5.0 (c (n "proc_macro_roids") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0j1769fdqvvkzibc73ywnycq360pdp463irz4r659vj511n0a53p")))

(define-public crate-proc_macro_roids-0.6.0 (c (n "proc_macro_roids") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0zcxl2hagy3i64w0njkpxaimhfaidf1yxg3h6k0mgzhgszm7mb0x")))

(define-public crate-proc_macro_roids-0.6.1 (c (n "proc_macro_roids") (v "0.6.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0prkjyplviyjqmza60k0q3vkm53w5ljmabbcxrddyfdvpwf4n0nq")))

(define-public crate-proc_macro_roids-0.7.0 (c (n "proc_macro_roids") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "06k1r71nxihjq1y0j25a9wapsm1774913dgvfz7jpxbpqni5yrq6")))

(define-public crate-proc_macro_roids-0.8.0 (c (n "proc_macro_roids") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1ivi20wsgx3x5zqsnxpmhv47njwzs556hym2dpv2kalarnca1hnh")))

