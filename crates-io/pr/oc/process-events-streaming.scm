(define-module (crates-io pr oc process-events-streaming) #:use-module (crates-io))

(define-public crate-process-events-streaming-0.1.0 (c (n "process-events-streaming") (v "0.1.0") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "1ps5hrzvazsi11wbz6bxrspckzxy965ibq56v4a5mrggv3iin4ri") (y #t)))

(define-public crate-process-events-streaming-0.1.1 (c (n "process-events-streaming") (v "0.1.1") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "0fd4i4ydlbq1d2l62smb4376cb67f2bismjrkvi839x6z5dycllb") (y #t)))

(define-public crate-process-events-streaming-0.1.2 (c (n "process-events-streaming") (v "0.1.2") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "0ybq5s3rqgy9lqigz37irx2p09brb996yxrdsdnz3hqzzn7xsbnl") (y #t)))

(define-public crate-process-events-streaming-0.2.1 (c (n "process-events-streaming") (v "0.2.1") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "1bv70hqn7fk1174m4ay1bfs12a7ggc546zgwnni3nwqlllkm0hhf") (y #t)))

(define-public crate-process-events-streaming-0.2.2 (c (n "process-events-streaming") (v "0.2.2") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "06phnb5sjg9a94v56cacqlji7w2nwy30a2rcyn2c3m5crjgifzfm") (y #t)))

(define-public crate-process-events-streaming-0.2.3 (c (n "process-events-streaming") (v "0.2.3") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "02g70971p8q347w7sdq93wld9k63n08iyf54nqpwsyqqsb48iny2") (y #t)))

(define-public crate-process-events-streaming-0.2.4 (c (n "process-events-streaming") (v "0.2.4") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "1x1mabpk8r4wqy7cza9gamzsnrndhnmms4g5c7vqmvp7zsfjam53") (y #t)))

(define-public crate-process-events-streaming-0.2.5 (c (n "process-events-streaming") (v "0.2.5") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)))) (h "17jy9xxs6yhfs194f9w74rh4xgm5ci1vqqj4334z792kjh52fbg5")))

