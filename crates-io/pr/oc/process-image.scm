(define-module (crates-io pr oc process-image) #:use-module (crates-io))

(define-public crate-process-image-0.1.0 (c (n "process-image") (v "0.1.0") (h "1400wf6csvx0l6p9gmbh67lb96ld7h6imiw00dnscw1y3hsg3y82")))

(define-public crate-process-image-0.1.1 (c (n "process-image") (v "0.1.1") (h "0kwsyb91019d40y59ad5lsqxmrhkwllzsil4lajimbi56xlaa1xr")))

(define-public crate-process-image-0.2.0 (c (n "process-image") (v "0.2.0") (h "1c74mppjkjl5xmkll6p4nzqwf0jl4dlna36r52x7yd3fwri87bx5")))

