(define-module (crates-io pr oc proc-macro-rules-macros) #:use-module (crates-io))

(define-public crate-proc-macro-rules-macros-0.1.0 (c (n "proc-macro-rules-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a1k68qkb0za2db1c4i2k1hr1kwkp5gbrh4jb3dmv0dcf8yxfwrq")))

(define-public crate-proc-macro-rules-macros-0.1.1 (c (n "proc-macro-rules-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qjsi33wwq56rzv578gvw84pchxlhml6f87yyfxg33n6i19v54xv")))

(define-public crate-proc-macro-rules-macros-0.2.0 (c (n "proc-macro-rules-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "038sparhn6804badpj5mf7ph07y83aw05nlnf7qp2shpdld34hf0")))

(define-public crate-proc-macro-rules-macros-0.4.0 (c (n "proc-macro-rules-macros") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ivsw9xz0ksmhpb21i6vkf98apj0jckwr65gyr3ispb5zsqgyzr0")))

