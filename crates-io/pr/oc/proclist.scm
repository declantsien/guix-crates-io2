(define-module (crates-io pr oc proclist) #:use-module (crates-io))

(define-public crate-proclist-0.0.0 (c (n "proclist") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "easybench") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0snbbn0ni0v3kkl81hc6kwa1kp05nm5vwdq7xx5v68r4b36424gk") (y #t)))

(define-public crate-proclist-0.9.0 (c (n "proclist") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase" "tlhelp32"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f2sby2lmb65qsrv6nv4hc2gi9yyqq5y3sjqcc7fi5l01ppgycf6") (y #t)))

(define-public crate-proclist-0.9.1 (c (n "proclist") (v "0.9.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase" "tlhelp32" "handleapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g00avi0b6s34srbpw5nq6v81bjk7sv8bl6zq6xh1yj4a30ckw00")))

(define-public crate-proclist-0.9.2 (c (n "proclist") (v "0.9.2") (d (list (d (n "easybench") (r "^0.1") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase" "tlhelp32" "handleapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06mlfzwgiall7libqb0vilc7lx2y0yn8pzs2mf7jsl0nkwqzwnr7")))

