(define-module (crates-io pr oc proc_unroll) #:use-module (crates-io))

(define-public crate-proc_unroll-0.1.0 (c (n "proc_unroll") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1viy06c0lq7g597j2j7agkw9hhm30jr462dckyh33ym05z0na45h")))

(define-public crate-proc_unroll-0.1.1 (c (n "proc_unroll") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0m1gyxg3zwljrywi67d83hn5fnn8sd1m1bcb2hrl6dwn7jcpvfca")))

