(define-module (crates-io pr oc proc-macro-kwargs-derive) #:use-module (crates-io))

(define-public crate-proc-macro-kwargs-derive-0.1.0 (c (n "proc-macro-kwargs-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ya7nclvz5gv47jhr00gmjynmyp2klaibdg0v0jc3b5rlvd0k902")))

(define-public crate-proc-macro-kwargs-derive-0.1.4 (c (n "proc-macro-kwargs-derive") (v "0.1.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n9k7fwvzqhvfgpqsywwjx8r20bwb5mg8cy2mpwgcrfgf6s5ch3r")))

(define-public crate-proc-macro-kwargs-derive-0.1.5 (c (n "proc-macro-kwargs-derive") (v "0.1.5") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pxm0aimmvgbz766axiy393i6kcgs78rgij6w4n6syfh7xd89n9q")))

(define-public crate-proc-macro-kwargs-derive-0.2.0-alpha.1 (c (n "proc-macro-kwargs-derive") (v "0.2.0-alpha.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "derive" "extra-traits" "proc-macro"))) (k 0)))) (h "0r9snkh88qkl773m5q6pf0sc89j992nh1m9yrga1xw82q4my1b32")))

(define-public crate-proc-macro-kwargs-derive-0.2.0 (c (n "proc-macro-kwargs-derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "derive" "extra-traits" "proc-macro"))) (k 0)))) (h "0bbvr7lc24iwl8simwmhfg0qmml144p4wvpi3rbpdlkkfd6i5yq8")))

