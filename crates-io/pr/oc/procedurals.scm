(define-module (crates-io pr oc procedurals) #:use-module (crates-io))

(define-public crate-procedurals-0.2.0 (c (n "procedurals") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.3") (d #t) (k 0)))) (h "0vj9r3bg8iliyyby7dld0lpsh2qkdl3c1mngvd8f99sgd6ay3n64")))

(define-public crate-procedurals-0.2.1 (c (n "procedurals") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.3") (d #t) (k 0)))) (h "0x6ynpj9c8gj7h3r19gfvlmk39b6jlv1d7s7qkr6d40alqi42pab")))

(define-public crate-procedurals-0.2.2 (c (n "procedurals") (v "0.2.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.3") (d #t) (k 0)))) (h "0qky8my00vv4rn11qplz9kmqq2hpffjicdgvlgc04zz1rqnb37az")))

(define-public crate-procedurals-0.2.3 (c (n "procedurals") (v "0.2.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1far2h93j2hkjl08f7w3frimiicamrbp2f2wiww114r4xa2ymmx6")))

(define-public crate-procedurals-0.3.0 (c (n "procedurals") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0d67k7076g42wfqv0h3jxrhhk733dfimx4f0qx9vp8fmfb2zz3hr")))

(define-public crate-procedurals-0.3.1 (c (n "procedurals") (v "0.3.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "08s31ayjln8v1wcn9p7r1g81g2y1s5rip4zpx07aa7yh4kx6cghd")))

