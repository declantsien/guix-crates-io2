(define-module (crates-io pr oc proc_self) #:use-module (crates-io))

(define-public crate-proc_self-0.1.0 (c (n "proc_self") (v "0.1.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1kcxr9zyfaq02fwhd8svln4rdhz8psn2fja964piyyj37lpx30r7")))

(define-public crate-proc_self-0.1.1 (c (n "proc_self") (v "0.1.1") (h "12f8h54xcmg65bvfbzajc6b9kkv7r751npxwnbpn3n9s0r6hblwk") (y #t)))

(define-public crate-proc_self-0.2.0 (c (n "proc_self") (v "0.2.0") (h "04bch1v2ff27v5hny80ahrs99whvpkwh7pks3fyzm93rvy3s9fjl")))

