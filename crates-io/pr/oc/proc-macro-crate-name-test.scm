(define-module (crates-io pr oc proc-macro-crate-name-test) #:use-module (crates-io))

(define-public crate-proc-macro-crate-name-test-0.1.0 (c (n "proc-macro-crate-name-test") (v "0.1.0") (d (list (d (n "proc-macro-crate-name-test-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0iagpnkfkyhxxs1wd1y2d7smy96fsqkx8nrrhwq4c5j8n5fvgf8v")))

(define-public crate-proc-macro-crate-name-test-0.1.2 (c (n "proc-macro-crate-name-test") (v "0.1.2") (d (list (d (n "proc-macro-crate-name-test-macros") (r "^0.1.0") (d #t) (k 0)))) (h "081ij0w021alg4vci6myj22j19xxg0xvssa18y7p64zrw40qf0m3")))

