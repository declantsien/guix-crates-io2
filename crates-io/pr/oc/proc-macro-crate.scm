(define-module (crates-io pr oc proc-macro-crate) #:use-module (crates-io))

(define-public crate-proc-macro-crate-0.1.0 (c (n "proc-macro-crate") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0y009n5g9hgj4qxwahj6d2s4pbijg3zq4iv7q7y9pjx4czpxsx46")))

(define-public crate-proc-macro-crate-0.1.1 (c (n "proc-macro-crate") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "04xv5r6vns3zhbq6rsmkr6p3fwvf65b7b2rxn4l8wy1lxyirnzn9")))

(define-public crate-proc-macro-crate-0.1.2 (c (n "proc-macro-crate") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (d #t) (k 2)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1hm0fbw59chnbwqlz5qirxkh716qaavi9gdisxl05dfcna69wldd")))

(define-public crate-proc-macro-crate-0.1.3 (c (n "proc-macro-crate") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (d #t) (k 2)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "036imbb2194s480l5cgf8a7ii9fz57skkbpzvx8x2003n3jz8v2c")))

(define-public crate-proc-macro-crate-0.1.4 (c (n "proc-macro-crate") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (d #t) (k 2)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "07pyc64bxlcz9g4hl0gc8cwqcjxpgh4ssvgxjvxsgj2ly58ln3g1")))

(define-public crate-proc-macro-crate-0.1.5 (c (n "proc-macro-crate") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "11cpihdk9ba68hzw95aa8zxn0i5g6kdrfd4l2cy3d5jvb72a6vhx")))

(define-public crate-proc-macro-crate-1.0.0 (c (n "proc-macro-crate") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "14pzgkpnlzq6y7yc749h2lwd1mv44min4iszjk2znmi1yqfvvza1")))

(define-public crate-proc-macro-crate-1.1.0 (c (n "proc-macro-crate") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "10vgiwpp9rbi999pbn67p3r560z92bpfqszpsfs8ky6ai5lcxfhy")))

(define-public crate-proc-macro-crate-1.1.1 (c (n "proc-macro-crate") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "0f2w1l44mh7xr2zp4nvl33pfd4cqwcrry8nhpn9yq0qy10im32vk") (y #t)))

(define-public crate-proc-macro-crate-1.1.2 (c (n "proc-macro-crate") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "1p233nxvm0wcv1z24ram6g54la59jvbhq3rwkj935kqzk34sibcx") (y #t)))

(define-public crate-proc-macro-crate-1.1.3 (c (n "proc-macro-crate") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "06pi2jqncn1kscwsp7zm0p04iki3vl70n99j0d2dxx2bj774fzg1")))

(define-public crate-proc-macro-crate-1.2.0 (c (n "proc-macro-crate") (v "1.2.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "1j36pf7pknyap8vjkd8vhw5jxvrmba5xj02shlakkwi3ikxhpm96") (y #t)))

(define-public crate-proc-macro-crate-1.2.1 (c (n "proc-macro-crate") (v "1.2.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "1sclzva81n2lpjyfpdpdcd03f5ys9684vqap2xipbjdp1wxzr87d")))

(define-public crate-proc-macro-crate-1.3.0 (c (n "proc-macro-crate") (v "1.3.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "toml_edit") (r "^0.18") (d #t) (k 0)))) (h "0d1zq41h7aymny4p87nmz20688zr9zrrn6nmczz7l77cwj4q6qb6") (r "1.56.0")))

(define-public crate-proc-macro-crate-1.3.1 (c (n "proc-macro-crate") (v "1.3.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 2)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (d #t) (k 2)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "069r1k56bvgk0f58dm5swlssfcp79im230affwk6d9ck20g04k3z") (r "1.60.0")))

(define-public crate-proc-macro-crate-2.0.0 (c (n "proc-macro-crate") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 2)) (d (n "toml_edit") (r "^0.20.2") (d #t) (k 0)))) (h "1s23imns07vmacn2xjd5hv2h6rr94iqq3fd2frwa6i4h2nk6d0vy") (r "1.66.0")))

(define-public crate-proc-macro-crate-2.0.1 (c (n "proc-macro-crate") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 2)) (d (n "toml_datetime") (r "=0.6.3") (d #t) (k 0)) (d (n "toml_edit") (r "=0.20.2") (d #t) (k 0)))) (h "06jbv5w6s04dbjbwq0iv7zil12ildf3w8dvvb4pqvhig4gm5zp4p") (r "1.66.0")))

(define-public crate-proc-macro-crate-3.0.0 (c (n "proc-macro-crate") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 2)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1kf9zx73ihpqln8ax3dyd36l63qghj4nlf4dglrp2dwa43fqa9kb") (r "1.67.0")))

(define-public crate-proc-macro-crate-3.1.0 (c (n "proc-macro-crate") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 2)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "110jcl9vnj92ihbhjqmkp19m8rzxc14a7i60knlmv99qlwfcadvd") (r "1.67.0")))

(define-public crate-proc-macro-crate-2.0.2 (c (n "proc-macro-crate") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 2)) (d (n "toml_datetime") (r "=0.6.3") (d #t) (k 0)) (d (n "toml_edit") (r "=0.20.2") (d #t) (k 0)))) (h "092x5acqnic14cw6vacqap5kgknq3jn4c6jij9zi6j85839jc3xh") (r "1.66.0")))

