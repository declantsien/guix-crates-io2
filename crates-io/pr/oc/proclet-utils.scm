(define-module (crates-io pr oc proclet-utils) #:use-module (crates-io))

(define-public crate-proclet-utils-0.1.0 (c (n "proclet-utils") (v "0.1.0") (d (list (d (n "proclet") (r "^0.1") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "1v2zak045r5c16bvdxfk71h7b4n4xal28090a0w1apx3xp5fbhj8")))

(define-public crate-proclet-utils-0.1.1 (c (n "proclet-utils") (v "0.1.1") (d (list (d (n "proclet") (r "^0.1") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "1z6kwcbxqmdsxz9vnhk2cq0bavds5a00vpy2l9c45z3g9mxkws11")))

(define-public crate-proclet-utils-0.1.2 (c (n "proclet-utils") (v "0.1.2") (d (list (d (n "proclet") (r "^0.2") (k 0)) (d (n "proclet-utils-macros") (r "^0.1.1") (d #t) (k 0)))) (h "04y6wlpf67qyr7xll38axpbbqsqw2c0brrylvcxca0nafs07bynk") (y #t)))

(define-public crate-proclet-utils-0.1.3 (c (n "proclet-utils") (v "0.1.3") (d (list (d (n "proclet") (r "^0") (k 0)) (d (n "proclet-utils-macros") (r "^0.1.1") (d #t) (k 0)))) (h "05a2i6ipgdb3q0nz8vqbz7cj5rlklfbjrf1psmdz0wnjjz0rc7d1") (y #t)))

(define-public crate-proclet-utils-0.1.4 (c (n "proclet-utils") (v "0.1.4") (d (list (d (n "proclet") (r "^0") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "1aw2ssv05cbdh8241hv2b1q460zqzgqnqq6am2c9i457d49ng78s") (y #t)))

(define-public crate-proclet-utils-0.1.5 (c (n "proclet-utils") (v "0.1.5") (d (list (d (n "proclet") (r "^0.1") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "0b06fipw4q052841wizv0angw1pf6yyzjd3f2zsk92zdshr80f8z")))

(define-public crate-proclet-utils-0.2.0 (c (n "proclet-utils") (v "0.2.0") (d (list (d (n "proclet") (r "^0.2") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "13p0b0awvcfrqddlkvr92pax6sphhjfhdsf04c7j8c9i1p7sdaav")))

(define-public crate-proclet-utils-0.3.0 (c (n "proclet-utils") (v "0.3.0") (d (list (d (n "proclet") (r "^0.3") (k 0)) (d (n "proclet-utils-macros") (r "^0.1") (d #t) (k 0)))) (h "1js1zjjrl451kjsawill9sxq0hwdlwkgqh72ak5czf59jx113hdq")))

