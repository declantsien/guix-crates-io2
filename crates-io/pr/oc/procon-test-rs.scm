(define-module (crates-io pr oc procon-test-rs) #:use-module (crates-io))

(define-public crate-procon-test-rs-0.1.0 (c (n "procon-test-rs") (v "0.1.0") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.1.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "19pghjay4g4l75y4gnsq5fakxxgynf40rkr505xlk6sbvdxbsv13") (y #t)))

(define-public crate-procon-test-rs-0.1.1 (c (n "procon-test-rs") (v "0.1.1") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.1.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0m2a4inq855az9wsxqjsk6w3bjjd3sikhjz141lsw7ra1y4b0a95") (y #t)))

