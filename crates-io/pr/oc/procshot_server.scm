(define-module (crates-io pr oc procshot_server) #:use-module (crates-io))

(define-public crate-procshot_server-0.1.0 (c (n "procshot_server") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "15nvfnyb3lyqakgqik27424awr1z0l7jdpa2pk5dcg7qhksp2ajx")))

(define-public crate-procshot_server-0.1.1 (c (n "procshot_server") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "0jfjs9ghr296p46dyaabcc6dn9zxnfk63b1rdizmw8dxdkd8mq2b")))

(define-public crate-procshot_server-0.1.2 (c (n "procshot_server") (v "0.1.2") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "026m09f1mj33l812d8ds04s7fh09dd9p7wyz0p4q5g7lypy35b3w")))

(define-public crate-procshot_server-0.1.3 (c (n "procshot_server") (v "0.1.3") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "16mpbsvf2ls9f1wv3xbkxy0qayj1jaa07iaaiab49ns3f4vwkpcg")))

(define-public crate-procshot_server-0.1.4 (c (n "procshot_server") (v "0.1.4") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "1fxlp1860ikv8cfvayi640wc7958ywj4bp8a8nipwdb87ymkj66p")))

(define-public crate-procshot_server-0.1.5 (c (n "procshot_server") (v "0.1.5") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "procfs") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "07liaxv2f58sva7ky4p31q78ahv7b4vzrbyrfxkvqznwc9jvf3j2")))

