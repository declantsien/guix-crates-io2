(define-module (crates-io pr oc proc-lock) #:use-module (crates-io))

(define-public crate-proc-lock-0.3.0 (c (n "proc-lock") (v "0.3.0") (d (list (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-lock-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1b1gbksb45pfqfqg5bikp4k7n938m3dxsfl00vxrl6zx94zwvgc7")))

(define-public crate-proc-lock-0.3.1 (c (n "proc-lock") (v "0.3.1") (d (list (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-lock-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0qc3sh98z3r8ljwgxz5mhhjky0cgh5yca2nnf2sgfqxy4xjnjsbl")))

(define-public crate-proc-lock-0.3.2 (c (n "proc-lock") (v "0.3.2") (d (list (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-lock-macro") (r "^0.3.0") (d #t) (k 0)))) (h "04psi5gkws3y24drgzj6cij5d7lkph5xwfx6320v1s5iv8wwbrwg")))

(define-public crate-proc-lock-0.3.3 (c (n "proc-lock") (v "0.3.3") (d (list (d (n "proc-lock-api") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-lock-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1pw2358fj8m4vra62cc8f9aw2wdz88jqb9q20dbmric43pk0pdxd")))

(define-public crate-proc-lock-0.4.0 (c (n "proc-lock") (v "0.4.0") (d (list (d (n "proc-lock-api") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-lock-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0ybyd25s13j188c4dbv7xkh0dxj96ixmf52wpk2982fg5nlaid2c")))

