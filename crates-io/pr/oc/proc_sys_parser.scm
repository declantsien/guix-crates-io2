(define-module (crates-io pr oc proc_sys_parser) #:use-module (crates-io))

(define-public crate-proc_sys_parser-0.1.0 (c (n "proc_sys_parser") (v "0.1.0") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)))) (h "0d9kaqzakla1fnn61xgx1hvm0ix79b4zqyqnzgwfabmcw2170w3w")))

(define-public crate-proc_sys_parser-0.1.1 (c (n "proc_sys_parser") (v "0.1.1") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)))) (h "0lbrp0s8vq58lcrg6advwj4d429fqz6z42d5lgfqlq4hj51v70xg")))

(define-public crate-proc_sys_parser-0.1.2 (c (n "proc_sys_parser") (v "0.1.2") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)))) (h "13s80xxgdvahaj3m8m52kn2va00wg0kjggsfz1mdwch5m07i126z")))

(define-public crate-proc_sys_parser-0.1.4 (c (n "proc_sys_parser") (v "0.1.4") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)))) (h "094f9vm7ac26qrycfyd4ci2f4c5qv08nnkl0hqardvbdm4sdrn38")))

(define-public crate-proc_sys_parser-0.1.5 (c (n "proc_sys_parser") (v "0.1.5") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)))) (h "0j192v9m5jy7q45ry6ac33avi0mqg1h96755kjfqm54rn6m8iigi")))

(define-public crate-proc_sys_parser-0.1.6 (c (n "proc_sys_parser") (v "0.1.6") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17wyjplw69yjnxznfnxx9wqlpdmsiinbva2f12zvhkbiji2wz64a")))

(define-public crate-proc_sys_parser-0.1.7 (c (n "proc_sys_parser") (v "0.1.7") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06d277w11cqlvqiq5vm7r9j8xd0fp6fh0xfkgycfji789hphpczc")))

(define-public crate-proc_sys_parser-0.1.8 (c (n "proc_sys_parser") (v "0.1.8") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kqiz4bq1666aqdpqgfh60ydsn8jzp40ysb7inzcsyv8k4gjx69n")))

(define-public crate-proc_sys_parser-0.1.9 (c (n "proc_sys_parser") (v "0.1.9") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08xpcvyjns96i5sya21bviwr4xli3zb87drjw32wkix2941g38zm")))

(define-public crate-proc_sys_parser-0.1.10 (c (n "proc_sys_parser") (v "0.1.10") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gq70gqrdlhbkvlj37p86ks5hxcllrlydgjfk3a05zzsilwkbrzq")))

(define-public crate-proc_sys_parser-0.1.11 (c (n "proc_sys_parser") (v "0.1.11") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pl4dn1c0c0418w545iwdcnp78zinzja7bfk2p4z8pgmd4v0xk29")))

(define-public crate-proc_sys_parser-0.1.12 (c (n "proc_sys_parser") (v "0.1.12") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hswf1vv0y00d4vnxfwwhlbh229lvfwd0m2d10pgzpbg3d3f0y8q")))

(define-public crate-proc_sys_parser-0.1.13 (c (n "proc_sys_parser") (v "0.1.13") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b1f92gwx0s78ikcpfr306skk29c1fznnwxzcmaip2fylvxdwcns")))

(define-public crate-proc_sys_parser-0.1.14 (c (n "proc_sys_parser") (v "0.1.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "001wiqw5flzc8iyyqsc8rp4l0r3cj7swwnc08cyfz0dfhfzys834")))

(define-public crate-proc_sys_parser-0.1.15 (c (n "proc_sys_parser") (v "0.1.15") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ypj92i4hixlxr7i07sh47sja8pip492w9kl2b3zbg90jkk199lh")))

(define-public crate-proc_sys_parser-0.1.16 (c (n "proc_sys_parser") (v "0.1.16") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1549sxj4kavb3k4c0zsichdy6nbrjpqrdkrcl0rciibmmxmll0qz")))

(define-public crate-proc_sys_parser-0.1.17 (c (n "proc_sys_parser") (v "0.1.17") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f4rs6mxb8zf84skkgmw5fiqrz1yjzcpz61d9nnaqpxyk4k4y90i")))

(define-public crate-proc_sys_parser-0.1.18 (c (n "proc_sys_parser") (v "0.1.18") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12arbpqfhsaip5ycq0lmgwyfba53cavqfaizhx4gl79dhmd1pyhn")))

(define-public crate-proc_sys_parser-0.1.19 (c (n "proc_sys_parser") (v "0.1.19") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1lign0vplwnlindgzrjrdm314b87p7vlr7i240xrjrmw28szzagm")))

(define-public crate-proc_sys_parser-0.1.20 (c (n "proc_sys_parser") (v "0.1.20") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("feature"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "18zzwkscrzl7nfya8vyabn3p2nkiqbcji74cx8ki6bp3qnzigrx5")))

