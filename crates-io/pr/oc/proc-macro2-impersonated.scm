(define-module (crates-io pr oc proc-macro2-impersonated) #:use-module (crates-io))

(define-public crate-proc-macro2-impersonated-0.1.0 (c (n "proc-macro2-impersonated") (v "0.1.0") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "17sy7nv4yv1zj5qy9pgv2hfs999qh0475vrhrzhvv0vjf5fx7mxi") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-impersonated-0.1.1 (c (n "proc-macro2-impersonated") (v "0.1.1") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1q36jbdhgjhxflcqbyl6r2m0nyk1gm44pn49f11jy4rcg29zfbfk") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

