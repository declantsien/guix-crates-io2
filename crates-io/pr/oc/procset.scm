(define-module (crates-io pr oc procset) #:use-module (crates-io))

(define-public crate-procset-0.1.0 (c (n "procset") (v "0.1.0") (h "0gpxq22s45ymq8zrrws2ia47awlhmwafjfnyh5xy34xqj9w2h13x")))

(define-public crate-procset-0.1.1 (c (n "procset") (v "0.1.1") (h "1axrc5aasnsw4icszv4hj6l2nwy05f9scivwqz9dh5452cv7agy0")))

(define-public crate-procset-0.1.2 (c (n "procset") (v "0.1.2") (h "0qv2l0pbfdhw3pna5j6lsaa7i6h71d2s31a3avci5pzi4vq8rwm8")))

