(define-module (crates-io pr oc process_guard) #:use-module (crates-io))

(define-public crate-process_guard-0.1.1 (c (n "process_guard") (v "0.1.1") (d (list (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "ticktock") (r "^0.7.0") (d #t) (k 0)))) (h "0iai45p6diklfbdi6620l7yikvm4nrrzyrqfaw2zhl7j9fxqzs0w")))

(define-public crate-process_guard-0.1.2 (c (n "process_guard") (v "0.1.2") (d (list (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "ticktock") (r "^0.7.0") (d #t) (k 0)))) (h "0zqh3zbvy1xrlbfnqrnnzyrxcf09rrvcg604791h12ixbm5458j0")))

(define-public crate-process_guard-0.2.0 (c (n "process_guard") (v "0.2.0") (d (list (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "ticktock") (r "^0.7.0") (d #t) (k 0)))) (h "0xaw1qnwpni0ldrwm4rl3fiipg4fj292sbrghbxdl1ww7mg704n5")))

