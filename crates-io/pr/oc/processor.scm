(define-module (crates-io pr oc processor) #:use-module (crates-io))

(define-public crate-processor-0.1.0 (c (n "processor") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "11rpxwl4cnfd71sx24lndiv2ripd8f052p5gbd6946vnikiq8lg8") (r "1.71")))

(define-public crate-processor-0.1.1 (c (n "processor") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1i83271mb4rx07db191mv429kf5q8galfg4xvq79krkhrj2rljk5") (r "1.71")))

