(define-module (crates-io pr oc procfsps) #:use-module (crates-io))

(define-public crate-procfsps-0.1.0 (c (n "procfsps") (v "0.1.0") (d (list (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0w6cdiwgr20n5fi5lzx1s4ihyzz11wmc3yhb14bnq83chzf01r17")))

(define-public crate-procfsps-0.1.1 (c (n "procfsps") (v "0.1.1") (d (list (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1mf9glr64kck3d85v1pnz3yz3s3jy2wi9gmfk6aqk3s86xfd56z9")))

(define-public crate-procfsps-0.1.2 (c (n "procfsps") (v "0.1.2") (d (list (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0rgjwir54sbxx4q2x2r3rychgzfw6cfcdbq1mkadarp7jwa6sizz")))

