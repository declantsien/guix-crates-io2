(define-module (crates-io pr oc procedure) #:use-module (crates-io))

(define-public crate-procedure-0.1.0 (c (n "procedure") (v "0.1.0") (d (list (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "10gxax08w19dj28bx9s2i7v0z1xflvk2fcwg3waqfmidgc80p164")))

(define-public crate-procedure-0.2.0 (c (n "procedure") (v "0.2.0") (d (list (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1gqsy91q5d7910ylfjschzh69m1zddjxxwlzjciqplx2pwwi8gww")))

(define-public crate-procedure-0.2.1 (c (n "procedure") (v "0.2.1") (d (list (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0ribzyji8mjddm81ga5glqgvqf1x53rp9w352pyf9h8yvwsxnn9f")))

(define-public crate-procedure-0.3.0 (c (n "procedure") (v "0.3.0") (d (list (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "01bcb2gsbwfjh7as0s6b99pxb74ryi8mxr0d5cl8zgh6pj8i5shm")))

