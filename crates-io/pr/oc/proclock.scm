(define-module (crates-io pr oc proclock) #:use-module (crates-io))

(define-public crate-proclock-0.1.0 (c (n "proclock") (v "0.1.0") (d (list (d (n "proclock-api") (r "^0.1.0") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1gz0dsschz3qg704l7pi9cbqfay93hmwd9jlb0zgfz35fi9x3c78")))

(define-public crate-proclock-0.1.1 (c (n "proclock") (v "0.1.1") (d (list (d (n "proclock-api") (r "^0.1.0") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0sn92rvjf8ymfxqhg99m0iijn1kzrrj52gf6xwd4v6pxyf8svrz5")))

(define-public crate-proclock-0.1.2 (c (n "proclock") (v "0.1.2") (d (list (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.2") (d #t) (k 0)))) (h "105rfa7y8y81p7m4063fns8caj5pdkq27kwzn34r11dj053smlmg")))

(define-public crate-proclock-0.2.0 (c (n "proclock") (v "0.2.0") (d (list (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1j4kns7hs0shj3k2mj3v0p5y82hd6gmxzysx9gpx7fl78907hpmr")))

(define-public crate-proclock-0.2.1 (c (n "proclock") (v "0.2.1") (d (list (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0b5ybnilaki0jbi87g1nm77clknpam2ji7w73avpb42alfx2l6ax")))

(define-public crate-proclock-0.2.2 (c (n "proclock") (v "0.2.2") (d (list (d (n "proclock-api") (r "^0.1.2") (d #t) (k 0)) (d (n "proclock-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1a08hbqyg34zjzzgcx5awaik0f3wi6qk9awvhn56ld9knpqhfr62")))

