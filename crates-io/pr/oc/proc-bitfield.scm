(define-module (crates-io pr oc proc-bitfield) #:use-module (crates-io))

(define-public crate-proc-bitfield-0.0.1 (c (n "proc-bitfield") (v "0.0.1") (d (list (d (n "macros") (r "^0.0.1") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1i6v2l0cwp6gcn4abcp4ajj45qh4vgr1g2a4kqq6n5yb18x6wn6f") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.0.2 (c (n "proc-bitfield") (v "0.0.2") (d (list (d (n "macros") (r "^0.0.1") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1hrjmha7wzbvyqrmyy5hfrq355yzax5115f8nsy2pzaw23pasg43") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.1.0 (c (n "proc-bitfield") (v "0.1.0") (d (list (d (n "macros") (r "^0.0.1") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0v3f6g43xr07yyd9xia7rvvbcrq77mrlqi43xq5bdj80n6jb24pw") (f (quote (("nightly" "macros/nightly")))) (y #t)))

(define-public crate-proc-bitfield-0.1.1 (c (n "proc-bitfield") (v "0.1.1") (d (list (d (n "macros") (r "^0.1.0") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1h9v675vr42xis4gj1mdqn0bq4n34clcm4r8n4xsr8ynksgby20c") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.2.0 (c (n "proc-bitfield") (v "0.2.0") (d (list (d (n "macros") (r "^0.2.0") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "14bqs7vxr1bryj2vgpr4w1ihl7ik638lsyymsfkfv9h78y8qsbni") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.2.1 (c (n "proc-bitfield") (v "0.2.1") (d (list (d (n "macros") (r "^0.2.0") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0ym36jfvm07gfic8zwrqxbckr3zgd4frw77rrq4f8ajyia93nwmd") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.2.2 (c (n "proc-bitfield") (v "0.2.2") (d (list (d (n "macros") (r "^0.2.2") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0fx7d6b9lsx2pyjgfndzfinxgygq81xn7v408i2bz8jhnxkzpv68") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.2.3 (c (n "proc-bitfield") (v "0.2.3") (d (list (d (n "macros") (r "^0.2.2") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1xc5jxyb2j0nkn2f0ncwmb295s7dyijlxsvyfisxdwmpx5al6x5n") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.2.4 (c (n "proc-bitfield") (v "0.2.4") (d (list (d (n "macros") (r "^0.2.3") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1pjaa7gna1nzmwbj7rmwh0yhj5xhjbjf7mz3gkwdwzf90sr6wp09") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.3.0 (c (n "proc-bitfield") (v "0.3.0") (d (list (d (n "macros") (r "^0.3.0") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1m0vy2f6ac0fmrqxxivj41y1001cbv2b8jr99rk4mqkn806zibx2") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.3.1 (c (n "proc-bitfield") (v "0.3.1") (d (list (d (n "macros") (r "^0.3.1") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1m7nzq35ppmva4bkndkni2c69dr9934ww1kdzddd3y1kdwb3cwz7") (f (quote (("nightly" "macros/nightly"))))))

(define-public crate-proc-bitfield-0.4.0 (c (n "proc-bitfield") (v "0.4.0") (d (list (d (n "macros") (r "^0.4.0") (d #t) (k 0) (p "proc-bitfield-macros")) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "01aafblaynzascjpb2zrs2yysnrabr5h2dpizn9f5cxgx73c245j") (f (quote (("nightly" "macros/nightly"))))))

