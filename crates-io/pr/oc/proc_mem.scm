(define-module (crates-io pr oc proc_mem) #:use-module (crates-io))

(define-public crate-proc_mem-0.1.0 (c (n "proc_mem") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase"))) (d #t) (k 0)))) (h "13gfzqfbajai4i65sv7gf2i166vzkfyyqj2r0cc747vmrrhfmlnc") (y #t)))

(define-public crate-proc_mem-0.1.1 (c (n "proc_mem") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase"))) (d #t) (k 0)))) (h "0040954x6afywasdq5xn8qhmcwaq37mvv6ch8q9cw9pjvwn24lch")))

(define-public crate-proc_mem-0.1.2 (c (n "proc_mem") (v "0.1.2") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase" "wow64apiset"))) (d #t) (k 0)))) (h "1kb1prjny5s90ny1nzxwpd8nqf1lbvzh4ygvv28jyb85avppm7ji")))

(define-public crate-proc_mem-0.1.3 (c (n "proc_mem") (v "0.1.3") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase" "wow64apiset"))) (d #t) (k 0)))) (h "1blpb2b1sy47arkgvld8qkhhcgd7awfkgw80vr18rbqnj7nxh5bz")))

(define-public crate-proc_mem-0.1.4 (c (n "proc_mem") (v "0.1.4") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase" "wow64apiset"))) (d #t) (k 0)))) (h "1xdjhsnypm1pwa6xvifb34fg7j5arwilsml16j19vkjpffbs619w")))

(define-public crate-proc_mem-0.1.5 (c (n "proc_mem") (v "0.1.5") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase" "wow64apiset"))) (d #t) (k 0)))) (h "0hdamngip9ifbdqk78546l29rs542a34j9fbalxyfbiwwc9y68v2")))

(define-public crate-proc_mem-0.1.6 (c (n "proc_mem") (v "0.1.6") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "tlhelp32" "memoryapi" "processthreadsapi" "psapi" "winbase" "wow64apiset"))) (d #t) (k 0)))) (h "1fcyp1mvl603g6mizm8phx50ghlrsy29q2y3bbd2xhxq3d4w03aw")))

