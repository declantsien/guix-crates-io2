(define-module (crates-io pr oc proc-macro-hack) #:use-module (crates-io))

(define-public crate-proc-macro-hack-0.1.0 (c (n "proc-macro-hack") (v "0.1.0") (h "0hij8y3nfvi9d012qkpkqbq0424pk1nsvgzlk5jiw40wp63k0hmf")))

(define-public crate-proc-macro-hack-0.2.0 (c (n "proc-macro-hack") (v "0.2.0") (h "1sbw9m89nh4nw420bq77k390mbqgsqd94xcn1r22g6xvgc9x9qgn")))

(define-public crate-proc-macro-hack-0.2.1 (c (n "proc-macro-hack") (v "0.2.1") (h "1x06fcnjpvm9nym38iqw3qyq5qzvlhm0m0cfvafvhjrn09qvp8yv")))

(define-public crate-proc-macro-hack-0.2.2 (c (n "proc-macro-hack") (v "0.2.2") (h "1di38yymdihdnawvqy2wvniaggnzw9znlaxpb1hrqfyx5c0bixlq")))

(define-public crate-proc-macro-hack-0.2.3 (c (n "proc-macro-hack") (v "0.2.3") (h "0wk9qyg7kfsfrh52irq72gswmy829n0rylb7900asha00fd27z21")))

(define-public crate-proc-macro-hack-0.2.4 (c (n "proc-macro-hack") (v "0.2.4") (h "13800x09q94brmzvgbwcmsah7xrv1xf0chwigz2rfhql31545ci8")))

(define-public crate-proc-macro-hack-0.3.0 (c (n "proc-macro-hack") (v "0.3.0") (d (list (d (n "proc-macro-hack-impl") (r "^0.3") (d #t) (k 0)))) (h "1v1ajfhwy9z3cnbprm7qb5azbg4gsvm0rlhyiyrvvsz4y9n8f9b5")))

(define-public crate-proc-macro-hack-0.3.1 (c (n "proc-macro-hack") (v "0.3.1") (d (list (d (n "proc-macro-hack-impl") (r "^0.3") (d #t) (k 0)))) (h "024q1dr0z0hgjvvr47a20k6i4r9cakbh297w7kqbvk1dnm4z2qbc")))

(define-public crate-proc-macro-hack-0.3.2 (c (n "proc-macro-hack") (v "0.3.2") (d (list (d (n "proc-macro-hack-impl") (r "^0.3") (d #t) (k 0)))) (h "0dsp97wg70kzx5wcrdgx1ybmx9hqgmgg14k7mpr7rb3m274crh5j") (f (quote (("proc_macro") ("default" "proc_macro"))))))

(define-public crate-proc-macro-hack-0.3.3 (c (n "proc-macro-hack") (v "0.3.3") (d (list (d (n "demo-hack") (r "^0.0.3") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.3") (d #t) (k 2)) (d (n "proc-macro-hack-impl") (r "^0.3") (d #t) (k 0)))) (h "0bykqg3a57r4jcpdmjnyf6cr95p0hp7xvkazz0rmhj9azwjghwxp") (f (quote (("proc_macro") ("default" "proc_macro"))))))

(define-public crate-proc-macro-hack-0.4.0 (c (n "proc-macro-hack") (v "0.4.0") (d (list (d (n "proc-macro-hack-impl") (r "^0.4") (d #t) (k 0)))) (h "1w151j1a8gj98fms5720n6m2018rlg75bw0kvxnfp1bv4pwx9a1v")))

(define-public crate-proc-macro-hack-0.4.1 (c (n "proc-macro-hack") (v "0.4.1") (d (list (d (n "demo-hack") (r "^0.0.4") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.4") (d #t) (k 2)) (d (n "proc-macro-hack-impl") (r "^0.4") (d #t) (k 0)))) (h "1n6j30r9bmz1h9ni1bwciwndjdwykfcrqkijz5xszxwxr4v5nwic")))

(define-public crate-proc-macro-hack-0.5.0 (c (n "proc-macro-hack") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0mz7j95di14ah72mjwp7zhnga08hjm8xsyy8ylxx6d6v07b6nc2r")))

(define-public crate-proc-macro-hack-0.5.1 (c (n "proc-macro-hack") (v "0.5.1") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1dpw5v9ygvfpz4f3zmwav34y0rrb8s8255sm17dcy0sm43f2505l")))

(define-public crate-proc-macro-hack-0.5.2 (c (n "proc-macro-hack") (v "0.5.2") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "08iccavgdaxavl1h8crzlpdqll86h0djhgjmz1gq6y8605370z4x")))

(define-public crate-proc-macro-hack-0.5.3 (c (n "proc-macro-hack") (v "0.5.3") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0w5kz7y2hmwd47npxl9a2m5r0fcpm5fjj17g620akdk4c314b4ak")))

(define-public crate-proc-macro-hack-0.5.4 (c (n "proc-macro-hack") (v "0.5.4") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0rmb97j8xnp6im1j3d8axxgp6g877x3hg10y1qnxrpkkrlcsm41y")))

(define-public crate-proc-macro-hack-0.5.5 (c (n "proc-macro-hack") (v "0.5.5") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1g5q5vxpci0hp4d73cl5cjp2w0059p2qcx3dlzim7ks0pjgfv6va")))

(define-public crate-proc-macro-hack-0.5.6 (c (n "proc-macro-hack") (v "0.5.6") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1v3535y1vibcywrv37x8yrspm0d893n5ly6xm5hf7qzp36cxl0n7")))

(define-public crate-proc-macro-hack-0.5.7 (c (n "proc-macro-hack") (v "0.5.7") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (d #t) (k 0)))) (h "1www5lrvsk7pq04clgfmjlnnrshikgs1h51l17vrc7qy58bx878c")))

(define-public crate-proc-macro-hack-0.5.8 (c (n "proc-macro-hack") (v "0.5.8") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.14") (d #t) (k 0)))) (h "0y2ix90qdvsfks4j1g5qnpyzaa6a4j6sdi35klqvm120378kaalq")))

(define-public crate-proc-macro-hack-0.4.2 (c (n "proc-macro-hack") (v "0.4.2") (d (list (d (n "demo-hack") (r "^0.0.4") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.4") (d #t) (k 2)) (d (n "proc-macro-hack-impl") (r "^0.4") (d #t) (k 0)))) (h "0fxn3qfhw76c518dfal2qqjwj5dbf0a1f7z0r5c4wd0igygg4fs6")))

(define-public crate-proc-macro-hack-0.5.9 (c (n "proc-macro-hack") (v "0.5.9") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j11n87chsas9w54nyj3xqa2l3rwfxnlw6vsqpfw3mzzj8fz7276")))

(define-public crate-proc-macro-hack-0.5.10 (c (n "proc-macro-hack") (v "0.5.10") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "17cs9y27xmrvkf9yqjl1yrb635606cxgbx8sy18gbdvf88gxyk0i")))

(define-public crate-proc-macro-hack-0.5.11 (c (n "proc-macro-hack") (v "0.5.11") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1idz5vmnjjhvr51yvwyjb45mza18wa53fr05m1skqvbdyw15gm7c")))

(define-public crate-proc-macro-hack-0.5.12 (c (n "proc-macro-hack") (v "0.5.12") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1d0ip1qkrk5pxr2wp3l2sjvaaav8xzx4aa8wdj1slfzr06vg467r")))

(define-public crate-proc-macro-hack-0.5.13 (c (n "proc-macro-hack") (v "0.5.13") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fw3mkk4kd08kdx0lxyza0yawf7745b9hk1grd8c6h047h4v69zd") (y #t)))

(define-public crate-proc-macro-hack-0.5.14 (c (n "proc-macro-hack") (v "0.5.14") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "080lvrp8x8gl9ihbg352hc9wdxy0yj7a543ry4fa4z6mqfnyzzgw")))

(define-public crate-proc-macro-hack-0.5.15 (c (n "proc-macro-hack") (v "0.5.15") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qqbfm1byabjkph56r2rlvv4cliz4960j6hav3ljazyjqvkryr8d")))

(define-public crate-proc-macro-hack-0.5.16 (c (n "proc-macro-hack") (v "0.5.16") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1r657v7d9vh1ivrvq65rvg9gjb29dpa0l3zg2fgrn5j8znz5c13y")))

(define-public crate-proc-macro-hack-0.5.17 (c (n "proc-macro-hack") (v "0.5.17") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0h7mzm0rzkai1x3g9y471g42kb0lyv6xzfc2x8hldlyljwal2drs") (y #t)))

(define-public crate-proc-macro-hack-0.5.18 (c (n "proc-macro-hack") (v "0.5.18") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16055crxm9l5skrl96l5cs6xc4xnvhiggcf625r7pixdl2whbilr")))

(define-public crate-proc-macro-hack-0.5.19 (c (n "proc-macro-hack") (v "0.5.19") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rg0kzsj7lj00qj602d3h77spwfz48vixn1wbjp7a4yrq65w9w6v")))

(define-public crate-proc-macro-hack-0.4.3 (c (n "proc-macro-hack") (v "0.4.3") (d (list (d (n "demo-hack") (r "^0.0.4") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.4") (d #t) (k 2)) (d (n "proc-macro-hack-impl") (r "=0.4.3") (d #t) (k 0)))) (h "1qlfck1fiwrj0wdv71z06bm0alpfsyq9pywfzx2cr607b145dyfp")))

(define-public crate-proc-macro-hack-0.5.20+deprecated (c (n "proc-macro-hack") (v "0.5.20+deprecated") (d (list (d (n "demo-hack") (r "^0.0.5") (d #t) (k 2)) (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0s402hmcs3k9nd6rlp07zkr1lz7yimkmcwcbgnly2zr44wamwdyw") (r "1.31")))

