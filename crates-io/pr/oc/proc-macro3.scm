(define-module (crates-io pr oc proc-macro3) #:use-module (crates-io))

(define-public crate-proc-macro3-0.1.0 (c (n "proc-macro3") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gxsi567jz720c2qjwfw4hfbqkkbw4nnlfrp44w9i3pbvdrqy1xp") (f (quote (("span-locations" "proc-macro2/span-locations"))))))

(define-public crate-proc-macro3-0.1.1 (c (n "proc-macro3") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17hi55hjkbs3y0p00nim21yhl0yr0s1vxcrqwws39hdz3xg86hgn") (f (quote (("span-locations" "proc-macro2/span-locations"))))))

