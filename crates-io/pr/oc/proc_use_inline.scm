(define-module (crates-io pr oc proc_use_inline) #:use-module (crates-io))

(define-public crate-proc_use_inline-0.1.1 (c (n "proc_use_inline") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14sz42amjg7sr46n8vbcw2pd4yn6ppg5gk0sqnb3krgs9a5n7zxj")))

