(define-module (crates-io pr oc proc-macro0) #:use-module (crates-io))

(define-public crate-proc-macro0-1.0.40-dev.0 (c (n "proc-macro0") (v "1.0.40-dev.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0pnqrbfj7gw2ckwz7wamkyajwg834zxqa7s3v2ad17h2vyj9558c") (y #t)))

(define-public crate-proc-macro0-1.0.40-dev.1 (c (n "proc-macro0") (v "1.0.40-dev.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0za2myvp3kfmhgrhi983yk5vy5rqdm5ncjxcss8b37hzylzw3lfs") (f (quote (("incompatible") ("default" "incompatible")))) (y #t)))

(define-public crate-proc-macro0-0.0.0-- (c (n "proc-macro0") (v "0.0.0--") (h "0397srh2yc5whpq5ccqfkd18ma2n3x4dpib9a5iv09l8q214psaj") (y #t)))

