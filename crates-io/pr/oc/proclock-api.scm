(define-module (crates-io pr oc proclock-api) #:use-module (crates-io))

(define-public crate-proclock-api-0.1.0 (c (n "proclock-api") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0znxdmmw1cayq2l1fg2dnl5yjbmx608kxav5rym6255b7hrssnd0")))

(define-public crate-proclock-api-0.1.1 (c (n "proclock-api") (v "0.1.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0mf6sh9rflc53yq6ymdqqry2q00x7wc9kzh7qb95lr5n4vgapbc0")))

(define-public crate-proclock-api-0.1.2 (c (n "proclock-api") (v "0.1.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1fgyzl3p5zpmls3fh0jqh6ccvw9pk6w04a3a2x0xkb0ml1jc25ws")))

(define-public crate-proclock-api-0.2.0 (c (n "proclock-api") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1rrgh0sil3mcbgcqgbnj22k4dgr5dldp6ci0xzhfb34c2xhcg9qd")))

(define-public crate-proclock-api-0.2.1 (c (n "proclock-api") (v "0.2.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "05k8g0kzp78c39fnmiab8vchi2bd1fn6himwy7zdva5a85i3ywba")))

(define-public crate-proclock-api-0.2.2 (c (n "proclock-api") (v "0.2.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "16prlpc8mcrk8fkg9nzh505ryy2hy8waw9f1vq4j90p3mb3i1nml")))

