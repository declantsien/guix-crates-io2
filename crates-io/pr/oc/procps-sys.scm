(define-module (crates-io pr oc procps-sys) #:use-module (crates-io))

(define-public crate-procps-sys-0.1.0 (c (n "procps-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1y1xi1k2jksw8akmqidim86njc8mwas4yd4adw4s5i7dk2nk94gz")))

(define-public crate-procps-sys-0.1.1 (c (n "procps-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0q6jrw1b1mhc79cgmz8fsj63frdzfj6nmf8856x3ri61wl0y1n8d")))

(define-public crate-procps-sys-0.1.2 (c (n "procps-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "00913pi13gg1c7gawky317121lw104pm2p8vgwj2md64qb1ag5pz") (l "procps")))

