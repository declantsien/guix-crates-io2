(define-module (crates-io pr oc procontest) #:use-module (crates-io))

(define-public crate-procontest-0.1.0 (c (n "procontest") (v "0.1.0") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.1.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1xs649z1mqwfkhj5dqpq1177q0gfr1dwdzkzb7hvb239m57nj00a")))

(define-public crate-procontest-0.1.1 (c (n "procontest") (v "0.1.1") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1daxbgryz2pifskpr091prsyrlbpq91assz8hsbj2m89wll7cy3j")))

(define-public crate-procontest-0.1.2 (c (n "procontest") (v "0.1.2") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "09g1jyrgjj1kw4fsp1hdfypkm4r25gl60lp2plgw0m05s17f8dza")))

(define-public crate-procontest-0.1.3 (c (n "procontest") (v "0.1.3") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1v8bdz8i7vc4ajz5a8q68y5j5ways3603ap3bg67mqmlj7vd3y0k")))

(define-public crate-procontest-0.1.4 (c (n "procontest") (v "0.1.4") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1sx5f1jhasvx6v0prj0m9nfqf05vqvw1dvm2xh3drziymfjhfpzs") (y #t)))

(define-public crate-procontest-0.1.5 (c (n "procontest") (v "0.1.5") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0ybnv5rwzb1n8bzh1gjv29hg7d1mq8zymhlqpz3sv7sr909a364x") (y #t)))

(define-public crate-procontest-0.1.6 (c (n "procontest") (v "0.1.6") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "11mzxrami0hrjq67h9iim094bm549wrnlh4l7yn3irhfcjj17c4x") (y #t)))

(define-public crate-procontest-0.1.7 (c (n "procontest") (v "0.1.7") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1qk49n36w1dwzmhyh4m39mfhipkyns0w2hk4wbz8g9pdifv7nnkv")))

(define-public crate-procontest-0.1.8 (c (n "procontest") (v "0.1.8") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0ggkjk6g6iqrgs7xqa8wpa6b5nkj8jvvvf6mbn0krrs0jr0q3sqb")))

(define-public crate-procontest-0.1.9 (c (n "procontest") (v "0.1.9") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0c3kxhp0z62r6l0ks8vbagh3ggpy2imwgdh76l8qrkmqmjc73jay")))

(define-public crate-procontest-0.1.10 (c (n "procontest") (v "0.1.10") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0r2m0scklf7kngb1msf9dsks2a5c6v845wjp2xr30vizig5vmbdc")))

(define-public crate-procontest-0.1.11 (c (n "procontest") (v "0.1.11") (d (list (d (n "cli_test_dir") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "splitv") (r "^0.2.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0r373j4xsc3m8cw46c4l8fxvli0c4zk52czw7v763qml6sqh8hmn")))

