(define-module (crates-io pr oc proc-easy) #:use-module (crates-io))

(define-public crate-proc-easy-0.0.0 (c (n "proc-easy") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ml63ac5q2myhxpkf0pl55d74ilc4i4fjh4k08l4cyf240mb8j7w")))

(define-public crate-proc-easy-0.0.1 (c (n "proc-easy") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1isp9wc24i4y1yicp71z98fpgbpn1lgigw7km3gmx42d5ypcz831")))

(define-public crate-proc-easy-0.1.0 (c (n "proc-easy") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0szyw8rzmdl4f2q08bk3wdaj3d45pxc22cq4d8vg8b6ifgvsjlrg")))

(define-public crate-proc-easy-0.2.0 (c (n "proc-easy") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0572mfl83afpncbf358k2f0njkdyjk0pmajac5w07figl8mbynnn")))

(define-public crate-proc-easy-0.2.1 (c (n "proc-easy") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zc7jpx9fsbrbs9y7wndm9i70qd66m5inh65hifff4h98apgqldf")))

(define-public crate-proc-easy-0.2.2 (c (n "proc-easy") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jra0layfc9bh1vsfg5rbjgckny36vvrkp9jsipk5dzlmsp0ngqc")))

(define-public crate-proc-easy-0.2.3 (c (n "proc-easy") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hqckg3kh705b57zla046kjpyvgyvxv9qx27bldrqf2n2gd9b8wv")))

(define-public crate-proc-easy-0.3.0 (c (n "proc-easy") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06av50j8xlrbwi3j1gzxzqbwpdz9km78b2g532p72sqfrlvwcnga")))

