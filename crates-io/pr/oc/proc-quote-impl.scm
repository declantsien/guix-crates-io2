(define-module (crates-io pr oc proc-quote-impl) #:use-module (crates-io))

(define-public crate-proc-quote-impl-0.1.0 (c (n "proc-quote-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "009kic5lgdsw1cvkdcd28kgv21v1lqq65fa4y30qn1bpqkhdi5dk")))

(define-public crate-proc-quote-impl-0.1.1 (c (n "proc-quote-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1g0ncc161ppga8rwsshsywjfnl76b55h4q2xj2q9p04cnwk9c2x5")))

(define-public crate-proc-quote-impl-0.2.0 (c (n "proc-quote-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1z6a4xprirgjyy6vihblfw7hhqh42g4crjpymws9g772wwsfzwg7")))

(define-public crate-proc-quote-impl-0.2.1 (c (n "proc-quote-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "1nc8wfphh5jlmbhkih350xsqx3n1h60hyyq3w9vg2cybbx0z0cs5")))

(define-public crate-proc-quote-impl-0.2.2 (c (n "proc-quote-impl") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)))) (h "1nhz226xqhbzws1qsngcmiday1pb0n1dyfrzzig99dq0rpwg11gp")))

(define-public crate-proc-quote-impl-0.2.4 (c (n "proc-quote-impl") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1fqrw9wyid3rgzldh7h4fzx1m1qdfmgjp8kvvp2h1c6vg36cszhw") (y #t)))

(define-public crate-proc-quote-impl-0.3.0 (c (n "proc-quote-impl") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "133as7k6lb6q14c1vgd5jnq49mmb8q2j9mzw2b9qd5bs6fcyvlv6")))

(define-public crate-proc-quote-impl-0.3.1 (c (n "proc-quote-impl") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1yaw3vyp1fvhyicmr5w7x3iwcalw8f0g6xpzxqq965bav2kkfspx")))

(define-public crate-proc-quote-impl-0.3.2 (c (n "proc-quote-impl") (v "0.3.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "184ax14pyazv5g6yma60ls7x4hd5q6wah1kf677xng06idifrcvz")))

