(define-module (crates-io pr oc process-results) #:use-module (crates-io))

(define-public crate-process-results-0.1.0 (c (n "process-results") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)))) (h "0flv5rv8zmszlblvqpghgbjqjdm6d3a7i88zvqhbym7lmnqkj4jy")))

(define-public crate-process-results-0.1.1 (c (n "process-results") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)))) (h "080xhwp5ipy500yqrhpv1b3pjhr3i24206nwwf0mz7c2j8jcmb2q")))

(define-public crate-process-results-0.1.2 (c (n "process-results") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 2)))) (h "19hnj51bca8al9jh77qcc5fk8pclb085ywq1sbsv455mmhhzi2jk")))

