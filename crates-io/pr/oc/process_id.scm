(define-module (crates-io pr oc process_id) #:use-module (crates-io))

(define-public crate-process_id-0.1.0 (c (n "process_id") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)))) (h "02k26n982qji5fwp80jrwbkfb6qf7jgjqpya93hc9mj5gxb8yffb")))

