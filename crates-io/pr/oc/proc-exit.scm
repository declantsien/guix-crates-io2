(define-module (crates-io pr oc proc-exit) #:use-module (crates-io))

(define-public crate-proc-exit-0.1.0 (c (n "proc-exit") (v "0.1.0") (h "0hcvq454qc2am20i67s1i5ljhpc3r67jqqcx6f69wlsd9k4p7i8s") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-0.2.0 (c (n "proc-exit") (v "0.2.0") (h "0zs145jd2g9sb9vvd6krv39a52qn0c3s55v4yf2yzpqhjvx6ss20") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-0.3.0 (c (n "proc-exit") (v "0.3.0") (h "1d9cfv3s51xz67q6a3fj4k6k8y00kmijls5r62h2ln2m337wn0bx") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-1.0.0 (c (n "proc-exit") (v "1.0.0") (h "0izqk6dycrfdx350g43xqsgnyhnyb6yq1jm9289bsp9lxrc843l4") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-1.0.1 (c (n "proc-exit") (v "1.0.1") (h "00a5jnkxm93kxzlcqch9c5qzjncxq94bmzcjdd2xj6m72bq8mybv") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-1.0.2 (c (n "proc-exit") (v "1.0.2") (h "0vr7iasaxajaql96395g0plfd607ammkm63wvy7vjgn0s7b40jnr") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-1.0.3 (c (n "proc-exit") (v "1.0.3") (h "0jfbr3ym8mvadxx0d4dsgzbz5hvmncnx1nbab57lscc7xz4bp9hd") (f (quote (("portable") ("default" "portable"))))))

(define-public crate-proc-exit-2.0.0 (c (n "proc-exit") (v "2.0.0") (h "1ijfgw6jnikw5g74zd51qlh12daakwb5v4lirlvmslc1k19pimyj") (r "1.61.0")))

(define-public crate-proc-exit-2.0.1 (c (n "proc-exit") (v "2.0.1") (h "1jl2l8bqbdqm7pcyl0dszyrv5pcv5c2dpqg815hn52qwyg7mml6v") (r "1.61.0")))

