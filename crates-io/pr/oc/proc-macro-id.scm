(define-module (crates-io pr oc proc-macro-id) #:use-module (crates-io))

(define-public crate-proc-macro-id-1.0.0 (c (n "proc-macro-id") (v "1.0.0") (h "0vl57gsl0y3y2452faj2hsschbbw6wk32mwascfkv2n2lh75s1a5")))

(define-public crate-proc-macro-id-1.0.1 (c (n "proc-macro-id") (v "1.0.1") (h "07hypndh4h4k4kq1lw427xbnxl9xj9q7mpjb48yg45k69abv986g")))

