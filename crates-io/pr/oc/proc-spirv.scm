(define-module (crates-io pr oc proc-spirv) #:use-module (crates-io))

(define-public crate-proc-spirv-0.0.1 (c (n "proc-spirv") (v "0.0.1") (d (list (d (n "shaderc") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bdf8ammimrljacj1nbs53zhji0cg1xaldvzmziizjxfgrfdpph2")))

