(define-module (crates-io pr oc process_alive) #:use-module (crates-io))

(define-public crate-process_alive-0.1.0 (c (n "process_alive") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "winnt" "handleapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15ydcapdidylliqhnrq69dawfa4z7wn13nzwyl33wpny9ih7ijhz")))

(define-public crate-process_alive-0.1.1 (c (n "process_alive") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.127") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "winnt" "handleapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ivcz8184pis8fj57z68xb5b556gs2gwxpbl5gkdra8a9r45w6zw")))

