(define-module (crates-io pr oc proc-macro-error2-attr) #:use-module (crates-io))

(define-public crate-proc-macro-error2-attr-0.0.2 (c (n "proc-macro-error2-attr") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0an2iz6c4adc4a5qkj3cl9jnn1nr8g243s2r5b16sr6bm6ckl1b9") (y #t)))

