(define-module (crates-io pr oc proclet) #:use-module (crates-io))

(define-public crate-proclet-0.1.0 (c (n "proclet") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (k 0)))) (h "1bq8igs6y82zf669c8nlkbx99jd4z5hvszgg20g0a3h17f5vlrc1") (f (quote (("literal-value") ("default" "proc-macro" "literal-value")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2") ("proc-macro" "proc-macro2?/proc-macro"))))))

(define-public crate-proclet-0.2.0 (c (n "proclet") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (k 0)))) (h "15js7h0j482sfdprqd1q7x12151r13pby6wbqn0qmrjkafbjvhnx") (f (quote (("literal-value") ("default" "proc-macro" "literal-value")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2") ("proc-macro" "proc-macro2?/proc-macro"))))))

(define-public crate-proclet-0.3.0 (c (n "proclet") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (k 0)))) (h "1kcx3f0rm94zl1g1gadsvrq1k3x5b6pjgadkvlznlyqdz9c657rh") (f (quote (("literal-value") ("default" "proc-macro" "literal-value")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2") ("proc-macro" "proc-macro2?/proc-macro"))))))

