(define-module (crates-io pr oc proc_macro_helper) #:use-module (crates-io))

(define-public crate-proc_macro_helper-0.1.0 (c (n "proc_macro_helper") (v "0.1.0") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sbz7spdkn7ifsrbv9zkx1nfr9hnvnpb33a22wxnvk7gza3zl9vz")))

(define-public crate-proc_macro_helper-0.2.0 (c (n "proc_macro_helper") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m8bbi3brbs3l3550y6mbr286xr3vphjgx12dgl14vmr2wp42w9f")))

(define-public crate-proc_macro_helper-0.3.0 (c (n "proc_macro_helper") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xp0ykr0gf7p7jwfhx2v4im5vcy3jfqa8621nczw903d3gnrnskg")))

