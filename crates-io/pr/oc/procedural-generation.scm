(define-module (crates-io pr oc procedural-generation) #:use-module (crates-io))

(define-public crate-procedural-generation-0.1.0 (c (n "procedural-generation") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1d3k4mms3fydyj1iz281g57w3lpyjdwmzxskmf22zsfdi65ywv7g")))

(define-public crate-procedural-generation-0.1.1 (c (n "procedural-generation") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1f0ssnss3cjg876x42p8saa3vj3ir0diws9nll05di1gcyw5l3hb")))

(define-public crate-procedural-generation-0.1.2 (c (n "procedural-generation") (v "0.1.2") (d (list (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17piyv050c5faiyq3ypcxvqbamkib19c115nkxzgwxrs8mmpcl4q")))

(define-public crate-procedural-generation-0.1.3 (c (n "procedural-generation") (v "0.1.3") (d (list (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14yjx9zip311ff3ybpwv7r7zh3krc8850xz0ycx1q2ly0px770a9")))

(define-public crate-procedural-generation-0.1.4 (c (n "procedural-generation") (v "0.1.4") (d (list (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "144dxqc35iflmf94piqcwcy3gbsiaqyfdxakp33gxv0p839b6m68")))

(define-public crate-procedural-generation-0.1.5 (c (n "procedural-generation") (v "0.1.5") (d (list (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1qnn7xshxmlsdxxsp639kn5mlhlp4gd0061n561wbfk2gvk3laq7")))

(define-public crate-procedural-generation-0.2.0 (c (n "procedural-generation") (v "0.2.0") (d (list (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "09zspgar2vpc6y1rdcywz1ym83hjkj5i62mzhqf1vs7dlxk0k4nf")))

(define-public crate-procedural-generation-0.3.0 (c (n "procedural-generation") (v "0.3.0") (d (list (d (n "noise") (r "^0.6.0") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "0ixnwq56zhxmhnw60pzhddgia93v4rhs7zgj1s4fj4xd0x5iqzmw")))

(define-public crate-procedural-generation-0.3.1 (c (n "procedural-generation") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "noise") (r "^0.6.0") (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.4.1") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "1qw9ncx101ch4wybpr8h559nyp4wa7217k5nc8ibg8zikpy5ifdj")))

