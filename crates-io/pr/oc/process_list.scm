(define-module (crates-io pr oc process_list) #:use-module (crates-io))

(define-public crate-process_list-0.1.0 (c (n "process_list") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p3xysiimz2pb7xz2lkwnjmlnc9wpm892r810pm5jrdkz30xq06b")))

(define-public crate-process_list-0.1.1 (c (n "process_list") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dsq8xkfgnc9y8ih5r4d9r1zimm2x6akj29s8awm93nzbnhk07l5")))

(define-public crate-process_list-0.1.2 (c (n "process_list") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0apgkfk6pmj26wnfc1rsvhpjl2w8nqvq0a6bbkf8cla7nv05b4w2")))

(define-public crate-process_list-0.1.3 (c (n "process_list") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05c6k3ydcilpgr0lixan25p7z2m2369gsffz5iyjcan4imwdgc65")))

(define-public crate-process_list-0.2.0 (c (n "process_list") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z11wlyw7bn9af0l3wvy218v0fd7s9cbgg2n142bvn5p8r2d6w3p")))

(define-public crate-process_list-0.2.1 (c (n "process_list") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w4gcqaalafw9r9mhsq7ajrmrj7av5q6k0vq42z1n79gfhib4chi")))

