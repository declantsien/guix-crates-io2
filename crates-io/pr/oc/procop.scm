(define-module (crates-io pr oc procop) #:use-module (crates-io))

(define-public crate-procop-0.1.0 (c (n "procop") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "sanitise-file-name") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.24") (d #t) (k 0)) (d (n "testcontainers") (r "^0.14.0") (d #t) (k 0)))) (h "1yrp6s3x2r8is4wc0v1k6pxlx3bkm93xfrrnhcz11v9qimbmdswk")))

