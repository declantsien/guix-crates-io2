(define-module (crates-io pr oc procedural-grass) #:use-module (crates-io))

(define-public crate-procedural-grass-0.1.0 (c (n "procedural-grass") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.22.1") (d #t) (k 0)))) (h "0y54zkxap264kynym66iah2bsg2hnm17wvvp0jfi6gr4i7vjmk5x") (y #t) (r "1.76.0")))

