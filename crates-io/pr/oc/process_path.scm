(define-module (crates-io pr oc process_path) #:use-module (crates-io))

(define-public crate-process_path-0.1.0 (c (n "process_path") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0bdzz334s5wrx13k19x3cjhqhvc6akqbzr8da7nibimivf37zmdx")))

(define-public crate-process_path-0.1.1 (c (n "process_path") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "09h578fzdxnl60vlixg80vkwjq6yf9s77b75a61f4kmlnsrwxggr")))

(define-public crate-process_path-0.1.3 (c (n "process_path") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"macos\"))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "libloaderapi" "minwindef" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06w1ld7lyj3900ixmkwa1p5n1v39b16j03k8a8hh3fhmvd2g7rxb")))

(define-public crate-process_path-0.1.4 (c (n "process_path") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"netbsd\", target_os = \"macos\"))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "libloaderapi" "minwindef" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wb19xf4dpvkpz2svj5qjwi9ws06p2k8y8gfp87ymqmkn0gg2xpn")))

