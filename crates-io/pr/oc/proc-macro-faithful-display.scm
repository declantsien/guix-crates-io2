(define-module (crates-io pr oc proc-macro-faithful-display) #:use-module (crates-io))

(define-public crate-proc-macro-faithful-display-0.1.0 (c (n "proc-macro-faithful-display") (v "0.1.0") (h "0la3agz0g0sm2wd837b2r5x80bl9980d636gv0wfqf6c7zqk3qnv")))

(define-public crate-proc-macro-faithful-display-0.2.0 (c (n "proc-macro-faithful-display") (v "0.2.0") (h "0fkpyjp1p3gbzqg6f8sd55brk9j5308pz0s25vav7rz048xgkgdm") (r "1.76")))

