(define-module (crates-io pr oc proceed) #:use-module (crates-io))

(define-public crate-proceed-0.0.1 (c (n "proceed") (v "0.0.1") (d (list (d (n "ggez") (r "^0.4") (f (quote ("cargo-resource-root"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "1iw93shy3gzwn0n79wfapqh1rpdfnqz3jqjfp1k0mf8hj0ci89ma") (y #t)))

(define-public crate-proceed-0.1.0 (c (n "proceed") (v "0.1.0") (d (list (d (n "termios") (r "^0.3") (o #t) (d #t) (k 0)))) (h "10s4c1j7vm8imbyj61y4r6rgcajx6zg6k47g7aslw66blscxai3s") (f (quote (("term" "termios") ("default"))))))

