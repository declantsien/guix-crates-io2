(define-module (crates-io pr oc procedural-meshes) #:use-module (crates-io))

(define-public crate-procedural-meshes-0.1.0 (c (n "procedural-meshes") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("dynamic_linking"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.2") (d #t) (k 0)) (d (n "lyon") (r "^1.0.1") (d #t) (k 0)))) (h "14xxjj6l7wj1g1y0bmvci7zmmxz32nklbhlwbbg90914kcafpfls") (y #t) (r "1.76.0")))

