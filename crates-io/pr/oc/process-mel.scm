(define-module (crates-io pr oc process-mel) #:use-module (crates-io))

(define-public crate-process-mel-0.8.0-rc1 (c (n "process-mel") (v "0.8.0-rc1") (d (list (d (n "async-std") (r "~1.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc1") (d #t) (k 0)))) (h "081qxx7biqx9x13maq3w8flwq7zz1igh79zjzi10ks65400gm9l7") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-process-mel-0.8.0-rc2 (c (n "process-mel") (v "0.8.0-rc2") (d (list (d (n "async-std") (r "~1.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc2") (d #t) (k 0)))) (h "0882fypf0bp9wpznihqs8pdll8pac3kqv37w9cl0v6wkqy5ms15f") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-process-mel-0.8.0-rc3 (c (n "process-mel") (v "0.8.0-rc3") (d (list (d (n "async-std") (r "~1.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc3") (d #t) (k 0)))) (h "1p1gsrbdcr6wjz45kccxnc7vh0g45469yfic9ajg1753lb8jw93y") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-process-mel-0.8.0 (c (n "process-mel") (v "0.8.0") (d (list (d (n "async-std") (r "~1.12") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.8.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "std-mel") (r "^0.8.0") (d #t) (k 0)))) (h "193lqmpp7ldal8j7pwfjsr2kb1jdnnyfiyz2f10aglbj9ppmv13d") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

