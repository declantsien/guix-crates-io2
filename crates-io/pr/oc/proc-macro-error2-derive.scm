(define-module (crates-io pr oc proc-macro-error2-derive) #:use-module (crates-io))

(define-public crate-proc-macro-error2-derive-0.0.1 (c (n "proc-macro-error2-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0ha9jr2zac850ja07im91mxn9gh5day3kpyyjg4jj427pwvrz4hw") (y #t)))

