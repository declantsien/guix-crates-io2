(define-module (crates-io pr oc proc-find) #:use-module (crates-io))

(define-public crate-proc-find-0.1.0 (c (n "proc-find") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "libproc") (r "^0.10.0") (d #t) (k 0)) (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.0") (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0yg8nfdkp6wf37alfa5xiji9cyq2c1y1kkm8hywbgbxwaj50hjrb")))

