(define-module (crates-io pr oc process_utils) #:use-module (crates-io))

(define-public crate-process_utils-0.1.0 (c (n "process_utils") (v "0.1.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "nix") (r "~0.9") (d #t) (k 0)) (d (n "procinfo") (r "~0.4") (d #t) (k 0)))) (h "1va0hf65bfxm7dlgmv31682jrvb0wwnwncj9gy72zygazaj3hgb2")))

