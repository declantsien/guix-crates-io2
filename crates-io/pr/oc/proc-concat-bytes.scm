(define-module (crates-io pr oc proc-concat-bytes) #:use-module (crates-io))

(define-public crate-proc-concat-bytes-0.1.0 (c (n "proc-concat-bytes") (v "0.1.0") (d (list (d (n "proc-concat-bytes-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "14ip0dffnz8anc6fphbjdv73316s9358bcgslv30l9xc7k06ampm")))

