(define-module (crates-io pr oc proc_getter) #:use-module (crates-io))

(define-public crate-proc_getter-0.0.1 (c (n "proc_getter") (v "0.0.1") (h "0yv8vhk17kkv1z64rz93ywaxgamb1mlgj5max6b7vffl7k3zfxq1")))

(define-public crate-proc_getter-0.0.2 (c (n "proc_getter") (v "0.0.2") (h "1sx6hl2yddzlb7dm4azj40xamb158sr20vmgxy5bw1d80r5rkhmg")))

(define-public crate-proc_getter-0.0.3 (c (n "proc_getter") (v "0.0.3") (h "0a7i8pm5hsq0mgivbb971fgbay9q9kzdzj5qmw7yci8wvjdhb5bv")))

