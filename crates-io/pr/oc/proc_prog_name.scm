(define-module (crates-io pr oc proc_prog_name) #:use-module (crates-io))

(define-public crate-proc_prog_name-0.1.0 (c (n "proc_prog_name") (v "0.1.0") (h "1lhk26zy8qkch56m9zn86cx1qj6vsaa8nfvg7m4cl18drmpvp5yf")))

(define-public crate-proc_prog_name-0.1.1 (c (n "proc_prog_name") (v "0.1.1") (h "1mamqmj66qds3hf7sdp0rm1lpvwlwszc2kpk0sgkjy6i94rwbfi8")))

