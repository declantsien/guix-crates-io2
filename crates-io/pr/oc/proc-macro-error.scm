(define-module (crates-io pr oc proc-macro-error) #:use-module (crates-io))

(define-public crate-proc-macro-error-0.1.0 (c (n "proc-macro-error") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08wdq3irf8zjfrf5ng1gmjal2c121bcw71931kgris88ny36wpn3")))

(define-public crate-proc-macro-error-0.1.1 (c (n "proc-macro-error") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1nwdaja6wdyhwlljissvicj4aw7p0v7cqkwh88qai881fyycri0f")))

(define-public crate-proc-macro-error-0.1.2 (c (n "proc-macro-error") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "093lh4w33xl9mw6w4lchavhda7gd6bkjids6dnh7slxjyki31d6z")))

(define-public crate-proc-macro-error-0.1.2-docfix (c (n "proc-macro-error") (v "0.1.2-docfix") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0b7qgmpgc50jbjx7pywdn2zqm3ns6lg13i03pazavdvb73sbcl27")))

(define-public crate-proc-macro-error-0.1.3 (c (n "proc-macro-error") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vnh3qz78lk3dw5g0qlj41pqyy4hrmwmw1pavg34g5bfbzas5m7v")))

(define-public crate-proc-macro-error-0.1.4 (c (n "proc-macro-error") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "13mqgh5w9r5p5qfwh3i7r203fzgi36d70pg0pwx53kkb9mv7gjvq")))

(define-public crate-proc-macro-error-0.1.5 (c (n "proc-macro-error") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0f25frrjpsy76a324kq1zlx380qi0nvsbky5r242wmcapnpcny14")))

(define-public crate-proc-macro-error-0.2.0 (c (n "proc-macro-error") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04wnv98fjprqbc4g32aqpp2zpydsvxr1c2v84h6b0rvnvg52fl8b")))

(define-public crate-proc-macro-error-0.2.1 (c (n "proc-macro-error") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0hvjrlrkyb6674vizszdzaxd1zhan9yicx9h2z83h56060l9rv0m") (y #t)))

(define-public crate-proc-macro-error-0.2.2 (c (n "proc-macro-error") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0asb1ic549vyfn4rkdxy0rq3fnbq27y3f36z7xw2yzgv8vzxhx1x")))

(define-public crate-proc-macro-error-0.2.3 (c (n "proc-macro-error") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03m8d0g8ihgm1cdfdj9garvw7ydilqj6yx9f57adab2ps140v8vp")))

(define-public crate-proc-macro-error-0.2.4 (c (n "proc-macro-error") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02acp78103icnmskbsapxw9ax6v4fx20jaw156y79ljvz8fjh3fy")))

(define-public crate-proc-macro-error-0.2.5 (c (n "proc-macro-error") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02lag2ivg3zqfzx75lj9jk3wi4277yvrvw732aiby2fr6pyq0via") (f (quote (("dummy")))) (y #t)))

(define-public crate-proc-macro-error-0.2.6 (c (n "proc-macro-error") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15v0ghpanf8zzdbd76dxccfi5x706w3av8p4y3apb8cfbm6zxk5f") (f (quote (("dummy"))))))

(define-public crate-proc-macro-error-0.3.0-rc1 (c (n "proc-macro-error") (v "0.3.0-rc1") (d (list (d (n "proc-macro-error-attr") (r "^0.3.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06x970fiplrh5a83r5jyk3ksphsrvw0q90c7z34r6wavjgm6767d") (f (quote (("dummy"))))))

(define-public crate-proc-macro-error-0.3.0-rc.1 (c (n "proc-macro-error") (v "0.3.0-rc.1") (d (list (d (n "proc-macro-error-attr") (r "= 0.3.0-rc.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x2sajlnbc0cj7z7ixzl07qbyivzf20zhri61ah1y885npfpwsq1")))

(define-public crate-proc-macro-error-0.3.0 (c (n "proc-macro-error") (v "0.3.0") (d (list (d (n "proc-macro-error-attr") (r "= 0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01h7rbq7f4zhxs2j2yq8k6dic57y7h3732dlnwnybz83sh57ghcx")))

(define-public crate-proc-macro-error-0.3.1 (c (n "proc-macro-error") (v "0.3.1") (d (list (d (n "proc-macro-error-attr") (r "= 0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0slrfgs6jwpvjab141h0i14sj8jz7svs3bj18xp7xmdlid24ank9")))

(define-public crate-proc-macro-error-0.3.2 (c (n "proc-macro-error") (v "0.3.2") (d (list (d (n "proc-macro-error-attr") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ra0bg8cfl2f21v5p8ldm4cbwm2c22gpdwp8bwvlqxlycp0zqjfg")))

(define-public crate-proc-macro-error-0.3.3 (c (n "proc-macro-error") (v "0.3.3") (d (list (d (n "proc-macro-error-attr") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yd00hw4y3ia5mpvcpj3kdaqxpdihx65ykm8ibmcgpbv61s57n94")))

(define-public crate-proc-macro-error-0.3.4 (c (n "proc-macro-error") (v "0.3.4") (d (list (d (n "proc-macro-error-attr") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08i7dqczdhqdzqjw3xi2cqd3c89mwr5k3fcyflxl701b62v05qh3")))

(define-public crate-proc-macro-error-0.4.0 (c (n "proc-macro-error") (v "0.4.0") (d (list (d (n "proc-macro-error-attr") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1") (k 0)))) (h "05wilp7hxg2wj6330xl63pywyzbii0d4nsawswk9cd4bnc2grjfi") (y #t)))

(define-public crate-proc-macro-error-0.4.1 (c (n "proc-macro-error") (v "0.4.1") (d (list (d (n "proc-macro-error-attr") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1") (k 0)))) (h "128ygihaii2bnpk2zcwxw3fsjxs8n5p66qanxa1ccxhipf523dw5") (y #t)))

(define-public crate-proc-macro-error-0.4.3 (c (n "proc-macro-error") (v "0.4.3") (d (list (d (n "proc-macro-error-attr") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1") (k 0)))) (h "09bm4qnx7z637cha4pc1k8a390kg0ffnv76szc9grgr6yqdjijyv")))

(define-public crate-proc-macro-error-0.4.4 (c (n "proc-macro-error") (v "0.4.4") (d (list (d (n "proc-macro-error-attr") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1") (k 0)))) (h "0a24bq822dqkmq71ffbqk93s408dqwfzbbgw4vmlx8dfrr3qbjak")))

(define-public crate-proc-macro-error-0.4.5 (c (n "proc-macro-error") (v "0.4.5") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1") (k 0)))) (h "1dpww37ad5257ymn1161a0iw2v56dwlxhvlarhpm658n8rja8y8v")))

(define-public crate-proc-macro-error-0.4.6 (c (n "proc-macro-error") (v "0.4.6") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1") (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0axc4sb8xyx6ahwfnzzz8faza0xd9w0vvxl0jwhpm59329mif9x4")))

(define-public crate-proc-macro-error-0.4.7 (c (n "proc-macro-error") (v "0.4.7") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1") (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "01zxhfbpsddgaqxy8dkd5d03l4kspw2w9442ki7a0nlbz6529lvs")))

(define-public crate-proc-macro-error-0.4.8 (c (n "proc-macro-error") (v "0.4.8") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1") (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "02jfpscv0fcqdi98a9rzkk423csmh4flgm0ac45s4bzjk9spfl47")))

(define-public crate-proc-macro-error-0.4.9 (c (n "proc-macro-error") (v "0.4.9") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1") (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hpjjci16x9j1h7b9p5fbwlfn6bx90q0b0jz4ja5wzlwyfd3qaq5")))

(define-public crate-proc-macro-error-0.4.10 (c (n "proc-macro-error") (v "0.4.10") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0s356d1qcx3234cp7ys48y9cqbsh90dk2h3yxdlchjdic5y7z1ca")))

(define-public crate-proc-macro-error-0.4.11 (c (n "proc-macro-error") (v "0.4.11") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "12hksgyl1m01dz1r6d6h6qqc8l907dqgfqckcc6haqnrcxj9r5g7")))

(define-public crate-proc-macro-error-0.4.12 (c (n "proc-macro-error") (v "0.4.12") (d (list (d (n "proc-macro-error-attr") (r "= 0.4.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1rvpaadwv7vmsp142qqh2axqrr9v78f1nvdsi9nhmfhy10kk1wqq")))

(define-public crate-proc-macro-error-1.0.0 (c (n "proc-macro-error") (v "1.0.0") (d (list (d (n "proc-macro-error-attr") (r "= 1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0kxfp8p7sixl16i505arnfgzzzhr0990mlqr2hvs58azhaj9l99d")))

(define-public crate-proc-macro-error-1.0.1 (c (n "proc-macro-error") (v "1.0.1") (d (list (d (n "proc-macro-error-attr") (r "= 1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0pipraynh9wbx3sfqmv5s3y8f4m51znc618s7wzwfrda6h806cc9")))

(define-public crate-proc-macro-error-1.0.2 (c (n "proc-macro-error") (v "1.0.2") (d (list (d (n "proc-macro-error-attr") (r "= 1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "= 0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0y46q108l4rfkx2kyzfsrkgxq6wz0i8lgsk54n0q3whf5swf9scq")))

(define-public crate-proc-macro-error-1.0.3 (c (n "proc-macro-error") (v "1.0.3") (d (list (d (n "proc-macro-error-attr") (r "=1.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.107") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "toml") (r "=0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "103qvh4qzlhxgpghl75yr82p9672ngwfi1454hknc4f3fybmw5zw")))

(define-public crate-proc-macro-error-1.0.4 (c (n "proc-macro-error") (v "1.0.4") (d (list (d (n "proc-macro-error-attr") (r "=1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.107") (d #t) (k 2)) (d (n "syn") (r "^1") (o #t) (k 0)) (d (n "toml") (r "=0.5.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1373bhxaf0pagd8zkyd03kkx6bchzf6g0dkwrwzsnal9z47lj9fs") (f (quote (("syn-error" "syn") ("default" "syn-error"))))))

