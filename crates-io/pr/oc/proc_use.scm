(define-module (crates-io pr oc proc_use) #:use-module (crates-io))

(define-public crate-proc_use-0.1.0 (c (n "proc_use") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "058wqy11mwprdsdgy0p5nchvs7c9ccnsdi03fq860fn2valfm76a")))

(define-public crate-proc_use-0.1.1 (c (n "proc_use") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0hld5sfw19qb68paaglr6nwdhlcrywfjzikjna8rbkb7sj1akn9v")))

(define-public crate-proc_use-0.2.0 (c (n "proc_use") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "13lc9idw0sglk81qy5w6m185wz8svpd7v03v14g6prb459yq445v")))

(define-public crate-proc_use-0.2.1 (c (n "proc_use") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "00fbllr15ciwjj1fi276iqbyl7ald7q52ijpnf2avwnyav2b25px")))

