(define-module (crates-io pr oc proc-macro-warning) #:use-module (crates-io))

(define-public crate-proc-macro-warning-0.1.0 (c (n "proc-macro-warning") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0fw5nr3qi6f0h29i4yn9x82qkkqx0mh42r4x11rbf2743arcsghn")))

(define-public crate-proc-macro-warning-0.1.1 (c (n "proc-macro-warning") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0z3z4ffwkj2jhqv786aa1363yns2njwgv5iji3yiczaqn2hj1jsm")))

(define-public crate-proc-macro-warning-0.1.2 (c (n "proc-macro-warning") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.109") (k 0)))) (h "1i1y27h63kpvydiqgdjrr6p0awzc6x1bswmcnphihdwm0kypm502") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.2.0 (c (n "proc-macro-warning") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.109") (k 0)))) (h "0crr07q0s24c5a72rs817h3dcz4kwg2cn8vjylpxrkmrhx6jhkwx") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.3.0 (c (n "proc-macro-warning") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.109") (k 0)))) (h "1b4bnr84wlpd47rqxhwgr6dpq0cimkcdlbh5nnpgrklsc2am9qph") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.3.1 (c (n "proc-macro-warning") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "1m3wyirn3y25cgn9yi6cqd18g2xwpm1p6f8rch89lsxmmw5ng68f") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.4.0 (c (n "proc-macro-warning") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.27") (k 0)) (d (n "syn") (r "^2.0.16") (k 0)))) (h "1zvrhk8dric3yf1m41kdfb7fw4ji9hmsr7vpirvx7kvs780vmrh2") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.4.1 (c (n "proc-macro-warning") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (k 0)) (d (n "quote") (r "^1.0.27") (k 0)) (d (n "syn") (r "^2.0.16") (k 0)))) (h "01wsg727k9hx1igl1r7gjj1xhksbjr7xsw0zqi4w67jx4qb0fmbh") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-0.4.2 (c (n "proc-macro-warning") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.29") (k 0)))) (h "13i5kdman89ms92as52aj5l65pjd4fn6xsvyvzzjj6dal1zsl7ix") (f (quote (("default"))))))

(define-public crate-proc-macro-warning-1.0.0-rc.1 (c (n "proc-macro-warning") (v "1.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.68") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (k 0)))) (h "1cwjyxf8p4s4cyj8nc441ipz9ab652f3y8g0kxd7ll62j5h0cyr9") (f (quote (("derive_debug") ("default" "derive_debug")))) (y #t)))

(define-public crate-proc-macro-warning-1.0.0 (c (n "proc-macro-warning") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.68") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (k 0)))) (h "0p20nj1y6da5dv8bdnc30p9bqm4bdbb35ca739y9n3nl145qnscv") (f (quote (("derive_debug") ("default" "derive_debug"))))))

(define-public crate-proc-macro-warning-1.0.1 (c (n "proc-macro-warning") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (k 0)))) (h "01pzc23hc5s1iwddp99g498p5gr3lbx1lacba14lb88k0zjw0kf2") (f (quote (("publish") ("derive_debug") ("default" "derive_debug")))) (y #t)))

(define-public crate-proc-macro-warning-1.0.2-rc1 (c (n "proc-macro-warning") (v "1.0.2-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (k 0)))) (h "076rks8386q5ijp8sy27n0fi87p4s6dvl1q2z4jc4675f7y3w53h") (f (quote (("derive_debug") ("default" "derive_debug"))))))

(define-public crate-proc-macro-warning-1.0.2-rc2 (c (n "proc-macro-warning") (v "1.0.2-rc2") (d (list (d (n "proc-macro2") (r "^1.0.68") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (k 0)))) (h "1ia6x2fidnviqkyay3bbcf04dz033pljzg3vbl5fzpis380br6la") (f (quote (("derive_debug") ("default" "derive_debug"))))))

(define-public crate-proc-macro-warning-1.0.2-rc3 (c (n "proc-macro-warning") (v "1.0.2-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (k 0)))) (h "179v2vcmzxa8kksc7fs3a8sqkjahzcvi275k2yygwbb6xpgyp09s") (f (quote (("derive_debug") ("default" "derive_debug"))))))

(define-public crate-proc-macro-warning-1.0.2 (c (n "proc-macro-warning") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (k 0)))) (h "1jpda17pbqmk8wpry6g750mb1kaw7srz00q2pf5awf76ry3s2kc3") (f (quote (("derive_debug") ("default" "derive_debug"))))))

