(define-module (crates-io pr oc procedural-masquerade) #:use-module (crates-io))

(define-public crate-procedural-masquerade-0.1.0 (c (n "procedural-masquerade") (v "0.1.0") (h "0dz0s3y8jva4ffk31jqgj3lg6405byg73ixqknbjqsnsx7ravprz")))

(define-public crate-procedural-masquerade-0.1.1 (c (n "procedural-masquerade") (v "0.1.1") (h "0q4jy05cv7kgnrhpl01lyaqd43hz4xl8k9vw1wsa8v3c4d4n4mlz")))

(define-public crate-procedural-masquerade-0.1.2 (c (n "procedural-masquerade") (v "0.1.2") (h "12fr86dk9mfdfvfjk8nsk6ynq4im1zdznjmwvvrxvy8ancgxqg69")))

(define-public crate-procedural-masquerade-0.1.3 (c (n "procedural-masquerade") (v "0.1.3") (h "15f9l36hjd78ya423fganfzsb62cmzsg3k3gdd7kj5c4x111iqls")))

(define-public crate-procedural-masquerade-0.1.4 (c (n "procedural-masquerade") (v "0.1.4") (h "0cjrdi77l6v7agl95j0x16pp79jzk5dfclqiwg26hz4x2h3p36si")))

(define-public crate-procedural-masquerade-0.1.5 (c (n "procedural-masquerade") (v "0.1.5") (h "0ajb2jdaw5h3c89kj5c5y0iq2cbvcbn4bbi9nfn823srw7zcl6yw")))

(define-public crate-procedural-masquerade-0.1.6 (c (n "procedural-masquerade") (v "0.1.6") (h "1l098px1hwdzqnxl376a9hfxb9q8kmj2n0y0s8k7plrz3jjp85cs")))

(define-public crate-procedural-masquerade-0.1.7 (c (n "procedural-masquerade") (v "0.1.7") (h "17dnfdk0qadh2h38bkwcy14cq8a1ild3j3hqmh1yjbq9ykgq64wg")))

