(define-module (crates-io pr oc processmon) #:use-module (crates-io))

(define-public crate-processmon-0.1.0 (c (n "processmon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "059aggd6qn83l6bgdm805si3kigsg2s0n1xxag2w1qfwlp6rrl5b")))

(define-public crate-processmon-0.2.0 (c (n "processmon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1r67njgr16zdd4i599jby1y5g0m7ak9bwz1mr22z16m1dps6yrxj")))

(define-public crate-processmon-0.3.0 (c (n "processmon") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jrsdxcjddp4l57fk1s6g9kxjc7avqgajybcdfjc5mwf8wnbjysw")))

(define-public crate-processmon-0.3.1 (c (n "processmon") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "040qlkg6dhf4anyknakcp86s0xpyvwgqp9l0xn3xkahgkqnfy63a")))

(define-public crate-processmon-0.3.2 (c (n "processmon") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zc8kbwz2gg9y2k1a882mykynp662mlpjygc7xx6i8gybph23cgd")))

(define-public crate-processmon-0.4.0 (c (n "processmon") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1khh6dighmhv0g4cm4rynki29njqh2rik33q9n69c281izsiwn85")))

(define-public crate-processmon-0.4.1 (c (n "processmon") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1dz4n6ah8wfsx34imbk05vxysgln0620nk7y1rxqy8fr0ajni0q3")))

