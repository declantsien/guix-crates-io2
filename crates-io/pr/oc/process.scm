(define-module (crates-io pr oc process) #:use-module (crates-io))

(define-public crate-process-0.0.1 (c (n "process") (v "0.0.1") (h "1w3h7bv06pjgvwfiks3hx7d8km3vnbf3ry3qj4maqsi8g057pbpi") (y #t)))

(define-public crate-process-0.1.0 (c (n "process") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)))) (h "0wdh0lj6cjv77y6ls0chschlyzhvsdmvgx03g99ciic298kn4jaq") (y #t)))

(define-public crate-process-0.2.0 (c (n "process") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "czt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)))) (h "0hmdi9s9zk5cvxrn54kw78vsia0ca5agn85yngl5pvgvv973hm64") (y #t)))

(define-public crate-process-0.3.0 (c (n "process") (v "0.3.0") (h "09hy7izcqfdv260jnnrgvh8b5x95d0wbbxvs8yppjc2ciwkcapnv") (y #t)))

