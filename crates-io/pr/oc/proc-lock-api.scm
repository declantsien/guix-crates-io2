(define-module (crates-io pr oc proc-lock-api) #:use-module (crates-io))

(define-public crate-proc-lock-api-0.3.0 (c (n "proc-lock-api") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0mf5zcn889lfla0680bs50ir7jx9kljzbdlcx736vhng92052i4p")))

(define-public crate-proc-lock-api-0.3.1 (c (n "proc-lock-api") (v "0.3.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1nrqw9gfhhymiikb0gyqxfmad49r14nnpv8jzvix9ps2hwqpncnb")))

(define-public crate-proc-lock-api-0.3.2 (c (n "proc-lock-api") (v "0.3.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0jqfl13jzvpm36mx9i5pwfz0vb1ybdvz1nq8nsg8dz3cyimvd794")))

(define-public crate-proc-lock-api-0.3.3 (c (n "proc-lock-api") (v "0.3.3") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "02ji7wc68fbmi4c4k90s4xbnlsf54wjhrdd7gmrid57b39h7qscv")))

(define-public crate-proc-lock-api-0.4.0 (c (n "proc-lock-api") (v "0.4.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1y6li89snjhiqgplpwjvkzv2cfiyrlccdkl1aypgi4j5a1lb0y7j")))

