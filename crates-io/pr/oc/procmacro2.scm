(define-module (crates-io pr oc procmacro2) #:use-module (crates-io))

(define-public crate-procmacro2-1.0.69 (c (n "procmacro2") (v "1.0.69") (h "15ndzm77fqnzb8nqjf5h75hals93749qx5kkm7wfl1zgvzhg5pxs") (r "1.56")))

(define-public crate-procmacro2-99.9.99 (c (n "procmacro2") (v "99.9.99") (h "04y1qkib80wkwm06qszbgwpndp6r0djhjk38kafgsgk12rg54b1h") (r "1.56")))

