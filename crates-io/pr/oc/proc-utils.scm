(define-module (crates-io pr oc proc-utils) #:use-module (crates-io))

(define-public crate-proc-utils-0.1.0 (c (n "proc-utils") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-util-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0sqyzps3sck95kl46hl2w9xdksfssjfxk6s1y98n7g8yz2mdjbw4")))

