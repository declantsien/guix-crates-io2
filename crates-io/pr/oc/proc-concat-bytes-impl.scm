(define-module (crates-io pr oc proc-concat-bytes-impl) #:use-module (crates-io))

(define-public crate-proc-concat-bytes-impl-0.1.0 (c (n "proc-concat-bytes-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "14nq2w9l19fr9w46j6milz78q49pmsrzmw56k45gczakx38zcqr6")))

