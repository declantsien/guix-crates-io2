(define-module (crates-io pr oc procmeta-core) #:use-module (crates-io))

(define-public crate-procmeta-core-0.1.0 (c (n "procmeta-core") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13cvl69p25l69r9n8wjhilhcg4x5r6albvmq46bvdmasnya55p6w")))

(define-public crate-procmeta-core-0.1.1 (c (n "procmeta-core") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a9wy8j9phaj0vj9n0nppxsgfri77bkq68rrnbrc5hhkw259rbb4")))

(define-public crate-procmeta-core-0.2.0 (c (n "procmeta-core") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfwb64wd0gn2r24pdq8frw2k6wnhzasp24mn837ib9bcdl7fv8z")))

(define-public crate-procmeta-core-0.2.1 (c (n "procmeta-core") (v "0.2.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x033yy3k1lxcb96q6w9fiy1hz3c63grhh31dnpnkhpc3dibaiqf")))

(define-public crate-procmeta-core-0.2.2 (c (n "procmeta-core") (v "0.2.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01zcjpd97s30xf0f52yy4ywzraxh3g8qrmlaa3v6z0qby5dxmif9")))

(define-public crate-procmeta-core-0.2.3 (c (n "procmeta-core") (v "0.2.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ldl3frbipbpdww7ak75z72zpas9djj7px6bq6mzqvdrmsxavkds")))

(define-public crate-procmeta-core-0.2.4 (c (n "procmeta-core") (v "0.2.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15ykzla8znx1sgni5ddddm2clzyvcc7c5mifb198yha5a3dbd7ni")))

(define-public crate-procmeta-core-0.2.5 (c (n "procmeta-core") (v "0.2.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6hq4mbqz9daf5lnnmcgr5lx9w5ffxam5v7vxc6whc517k07r0f")))

(define-public crate-procmeta-core-0.2.6 (c (n "procmeta-core") (v "0.2.6") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wa9rphnmvcidlv2cyddy3kj8f39j3dxl6clziyziz6n1vmhzdyq")))

(define-public crate-procmeta-core-0.2.7 (c (n "procmeta-core") (v "0.2.7") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1snx00padvc52b9l9nzxgyn3s2yr86vixnk4pvja0jmzprlai58q")))

(define-public crate-procmeta-core-0.2.8 (c (n "procmeta-core") (v "0.2.8") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0na7dsn8z2rqbhvnl7rd80c3lx4q2iahfy9lla29i6d4hlnhv8lw")))

(define-public crate-procmeta-core-0.2.9 (c (n "procmeta-core") (v "0.2.9") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1syp3fk7gsvwk9zvf66c6k2sn5lff2zc84ir6smm7ssazvylw9ji")))

(define-public crate-procmeta-core-0.3.0 (c (n "procmeta-core") (v "0.3.0") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1znyv9gk9yajyxf0l53lc7ivzf1nc28d7vrca088k570n1jpjakw")))

(define-public crate-procmeta-core-0.3.1 (c (n "procmeta-core") (v "0.3.1") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z0hnyi05mgzanxz1bb0g877cybdd1am1wn5ll049456iqbhbz1b")))

(define-public crate-procmeta-core-0.3.2 (c (n "procmeta-core") (v "0.3.2") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "112qm4ja6sabqnyn1zxz3shp04935wxayxkb6rfif5pb6yrqvnag")))

(define-public crate-procmeta-core-0.3.3 (c (n "procmeta-core") (v "0.3.3") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cl1lv17p20ymws5fy7c62nmar63s0p4n244hjs4l3x9rc0jn9ny")))

(define-public crate-procmeta-core-0.3.4 (c (n "procmeta-core") (v "0.3.4") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b3km6ynymsmiv6gidc712psfv48gn5x07yasfd6x4c978wdl75q")))

(define-public crate-procmeta-core-0.3.5 (c (n "procmeta-core") (v "0.3.5") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bmqrpifcj7x2mb4jlvi2r63g5r3igpmhsbgr3qfwfhab19r3p6z")))

