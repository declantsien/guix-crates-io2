(define-module (crates-io pr oc procstat) #:use-module (crates-io))

(define-public crate-procstat-1.0.0 (c (n "procstat") (v "1.0.0") (h "1476alrm5w0kzxpq8p8lk857yaiviwc4qy5w2my3ykaxdx19qrgl")))

(define-public crate-procstat-1.0.1 (c (n "procstat") (v "1.0.1") (h "0gd973p8pd71hg8n5jyc9rx40nj2fs00jr9lrbjjrj305s99idlf")))

(define-public crate-procstat-1.0.2 (c (n "procstat") (v "1.0.2") (h "1vvly18bziwns4jbd5d4fkdqy3vynx58xd52yf5kdk6kh1nkm67y")))

(define-public crate-procstat-1.0.3 (c (n "procstat") (v "1.0.3") (h "1dakcp0abvq299y8nc1i39biy6xchpyhpijjdxmjm91zba66x5pk")))

(define-public crate-procstat-1.0.4 (c (n "procstat") (v "1.0.4") (h "0i5b4lx3xwnn4q0ncdp29wsczbmc5hz1gqfr078pyvkljfawhbsa")))

