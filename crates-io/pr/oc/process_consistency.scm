(define-module (crates-io pr oc process_consistency) #:use-module (crates-io))

(define-public crate-process_consistency-0.5.0 (c (n "process_consistency") (v "0.5.0") (d (list (d (n "blake3") (r "^1.3.1") (o #t) (k 0)) (d (n "crc64fast") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_ToolHelp"))) (d #t) (k 0)))) (h "16hspv7746kmiy2zzj1gxiali1grpc5xc5856bcq9nr0kbhz5ksw") (f (quote (("default" "blake3") ("crc64" "crc64fast")))) (s 2) (e (quote (("blake3" "dep:blake3"))))))

