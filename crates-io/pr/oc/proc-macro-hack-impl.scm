(define-module (crates-io pr oc proc-macro-hack-impl) #:use-module (crates-io))

(define-public crate-proc-macro-hack-impl-0.1.0 (c (n "proc-macro-hack-impl") (v "0.1.0") (h "107qbag2rx8xi374lqafafvvjlkx8fiqc30wrd5bgiv78dl6mw7r")))

(define-public crate-proc-macro-hack-impl-0.2.1 (c (n "proc-macro-hack-impl") (v "0.2.1") (h "0sw4ygy50z4y3s9i1acdlfwfqh0yi70byn07sl3iswpz9z28cfpl")))

(define-public crate-proc-macro-hack-impl-0.2.2 (c (n "proc-macro-hack-impl") (v "0.2.2") (h "0cvn97iwx4h44jslnc2735r2zhk8h4362mvvlbvi88crdm9n18p3")))

(define-public crate-proc-macro-hack-impl-0.2.3 (c (n "proc-macro-hack-impl") (v "0.2.3") (h "16nbg9mnic9jrj996zg50fzwk9hrfprlaa14d8c87khnl6pkn9rr")))

(define-public crate-proc-macro-hack-impl-0.2.4 (c (n "proc-macro-hack-impl") (v "0.2.4") (h "12py651dwjv5h755yiyrzyxi35a42l6y7bbxklq9dgb383wnn238")))

(define-public crate-proc-macro-hack-impl-0.3.0 (c (n "proc-macro-hack-impl") (v "0.3.0") (h "1mdwjrzpckqfdhbv0l4pp0wjrwr7pyjngr09irxfblj44vyij0sw")))

(define-public crate-proc-macro-hack-impl-0.3.1 (c (n "proc-macro-hack-impl") (v "0.3.1") (h "100is4jgwf8n2r54qcd8mp71iv98cnn5ch9jirk183h0f5nn8x15")))

(define-public crate-proc-macro-hack-impl-0.3.2 (c (n "proc-macro-hack-impl") (v "0.3.2") (h "0chq78yqjavfihkvm2pr26ds9rkcdyaibvkx3dchxab0bpl9dkq3")))

(define-public crate-proc-macro-hack-impl-0.3.3 (c (n "proc-macro-hack-impl") (v "0.3.3") (h "19s4cbzmsfklxrkzgfv6145rhkq6mb43wir7albqd93d8k64qrqg")))

(define-public crate-proc-macro-hack-impl-0.4.0 (c (n "proc-macro-hack-impl") (v "0.4.0") (h "14i8zgkiyr50kswgl0fzrxsaxc8hbl77r0g931340wfl1ab6zjym")))

(define-public crate-proc-macro-hack-impl-0.4.1 (c (n "proc-macro-hack-impl") (v "0.4.1") (h "0ji5cmlclxxwy4snv5lp9gjw6swg7i2vhbvxmbz8xpcrxpcklx9b")))

(define-public crate-proc-macro-hack-impl-0.2.0 (c (n "proc-macro-hack-impl") (v "0.2.0") (h "16qhahyxwc6ywcj4nyql50kc7mrkc1smm142qi0g0z7574apdhln")))

(define-public crate-proc-macro-hack-impl-0.4.2 (c (n "proc-macro-hack-impl") (v "0.4.2") (h "0hk8g6s0zsi1ps0w48la2s8q5iqq42g8jfrgq3l2v04l2p5pvi1q")))

(define-public crate-proc-macro-hack-impl-0.4.3 (c (n "proc-macro-hack-impl") (v "0.4.3") (h "09q0jvdm5g0balskv9q446l9h7k3bk0fzmnxqzbz8d8nmvq5prbv")))

