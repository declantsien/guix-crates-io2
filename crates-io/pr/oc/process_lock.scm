(define-module (crates-io pr oc process_lock) #:use-module (crates-io))

(define-public crate-process_lock-0.1.0 (c (n "process_lock") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "errhandlingapi" "handleapi" "minwinbase" "synchapi" "winerror"))) (d #t) (k 0)))) (h "0b0m504grryzrq3lf42rlcfrrki89l26pabjb1cih210x22b2nzn")))

