(define-module (crates-io pr oc proc-macro2) #:use-module (crates-io))

(define-public crate-proc-macro2-0.1.0 (c (n "proc-macro2") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0dra28prn4cdxfgzxzpiyfszs0pl01zqnw7m39i604v3ka2zhzss") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.1 (c (n "proc-macro2") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "110k9h6g7fcnklrpdjgf1mpswhaw7lkn32yqiwmbfdvhhy2ikm1p") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.2 (c (n "proc-macro2") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "075qjcxvmlvdrmmk6iczkqj1cxxkl2pjvdcvarkimv761zzgia2h") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.3 (c (n "proc-macro2") (v "0.1.3") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "144bkf2g19hlikl3p1lq7y8f95xd7x13y1smaahpbvc77isdcivd") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.4 (c (n "proc-macro2") (v "0.1.4") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1z6b575qirdcb57l38zmvwgllm7mqqan7qpqgiqq55cig3d01i53") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.5 (c (n "proc-macro2") (v "0.1.5") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0w9h523qag3dq18akr7r3la82ifx3n0wjpszf0blyswlfxyg0k36") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.6 (c (n "proc-macro2") (v "0.1.6") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1mk007x9p3as2r8var1508fc6vm489k4kmsgbxg2kmnb17qqbfj4") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.7 (c (n "proc-macro2") (v "0.1.7") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1rryhsccjkkyq8dcxc76jy5adx9mk266hv917dc8nisfhfigl7q6") (f (quote (("unstable"))))))

(define-public crate-proc-macro2-0.1.8 (c (n "proc-macro2") (v "0.1.8") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1ih84xy3b9yprz1kpr0d2grn0xz6gxavqhvz78v9qxrwj6rl0r5b") (f (quote (("unstable" "nightly") ("nightly"))))))

(define-public crate-proc-macro2-0.1.9 (c (n "proc-macro2") (v "0.1.9") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1pz5dp8m10jnjyhwakrjwgy2531v5j62p9d2dml1m5lnffbc93hi") (f (quote (("unstable" "nightly") ("nightly"))))))

(define-public crate-proc-macro2-0.1.10 (c (n "proc-macro2") (v "0.1.10") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1k5mcpwn3ph6gp88w68rm8b2108cs4q85840xbx7kzwhpznaqzsm") (f (quote (("unstable" "nightly") ("nightly"))))))

(define-public crate-proc-macro2-0.2.0 (c (n "proc-macro2") (v "0.2.0") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1cri79m92c7036dn3f8pvvzgjhwbyc4jwv6d9y4z8kb246pv73dl") (f (quote (("nightly"))))))

(define-public crate-proc-macro2-0.2.1 (c (n "proc-macro2") (v "0.2.1") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1jl3dyks28fwlpdhs31j7kdm5whih1a88r9k3fmm8mpv7bl8d5sz") (f (quote (("nightly"))))))

(define-public crate-proc-macro2-0.2.2 (c (n "proc-macro2") (v "0.2.2") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "07f6h8i3jjz1hm503pw8xqy2z8j32sxg5wjgn712w0mzljm7mjyi") (f (quote (("nightly"))))))

(define-public crate-proc-macro2-0.2.3 (c (n "proc-macro2") (v "0.2.3") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1q1a53r21dj7zqa8wl5f2wdnpmnz9jqgkiwrg61gznfiqsrxw1yd") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.0 (c (n "proc-macro2") (v "0.3.0") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0xm02rdjafag4qfr8rkwqslqck5xyb9i3whz2j1psiqwmq88b9b4") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.1 (c (n "proc-macro2") (v "0.3.1") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "11mpa06l3khg2w55is2bh395vxnkwz714qxsp7gcri8qffj7x39q") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.2 (c (n "proc-macro2") (v "0.3.2") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "04bmpc2y9s6z7776m649j0jd1jv1sm8m3g3djb5miwwz0f72q738") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.3 (c (n "proc-macro2") (v "0.3.3") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0wpa72miza2f32pn58d639qhl2ziz5p3m4djwg5idhgpiqrv87m0") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.4 (c (n "proc-macro2") (v "0.3.4") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1ai7x2lpc3910ziqzqyz7ma5vd2j68jmyf08zn3qbx4v5ayiw800") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.5 (c (n "proc-macro2") (v "0.3.5") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1m0ksg6hbm46zblq0dpkwrg3n1h7n90yq1zcgwc6vpbfmr9pr6bp") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.6 (c (n "proc-macro2") (v "0.3.6") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0651cjx7d0mnpirfm0027hpmlvglbpq1q38ylpll7dl1vhhsbdj9") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.7 (c (n "proc-macro2") (v "0.3.7") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "187alijfwwpbx9n9198jslxrpmsj13ndzw3kadsr9wr6i59ljrxi") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.3.8 (c (n "proc-macro2") (v "0.3.8") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1ryaynnaj39l4zphcg5w8wszndd80vsrv89m5d2293gl6pry41hv") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.0 (c (n "proc-macro2") (v "0.4.0") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "04rxjfx26dm9hpmb7ld3pwjybcy1c6qp4kw5ix68jly6kzsbm4jr") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.1 (c (n "proc-macro2") (v "0.4.1") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0k8g4scc0bnfd97wm3049pv5cr34c8riip28pjkwimx8lp7kzrn6") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.2 (c (n "proc-macro2") (v "0.4.2") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0k87knypv1r3wqhc93gm0pic42q3n1bdg5z4ak9ihj6p2s55pid5") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.3 (c (n "proc-macro2") (v "0.4.3") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "110z7j3naf013pc4x64q6pm6l97m4nx4b1z6x5ppyxdmw052ypx4") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.4 (c (n "proc-macro2") (v "0.4.4") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0ikxxnapb4npdnajxn59q37v5pygssp8kc8pq5d0zz9wylikia8z") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.5 (c (n "proc-macro2") (v "0.4.5") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0wfcg9qlrbp45zmnmlcd0sf65kwd7lx41m636qrp19vc9xndd21m") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.6 (c (n "proc-macro2") (v "0.4.6") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1mm4c9j9lm7wxys4q6rcy4a2xvzpk1rxchw8ykw59bfd4lxvbzgg") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.7 (c (n "proc-macro2") (v "0.4.7") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "18yh0l7va4rn4grf7kp9a390m63kcadwwy6vqcq67krcl85dybjy") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.8 (c (n "proc-macro2") (v "0.4.8") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1rjy474ghbidxx2xbynb771ym2xqnijifbfyr8idjndqbfhiwny6") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.9 (c (n "proc-macro2") (v "0.4.9") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1dnrrwsi9i62s9hvnrg4z992011sgzgpgw1h612rizlqg9awgkfc") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.10 (c (n "proc-macro2") (v "0.4.10") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0hdqa1camgq6qnfm48jf3l8h5aq9zmwix0pdqwhmp3ch437z0hdb") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.11 (c (n "proc-macro2") (v "0.4.11") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0slw8mbczd373nhm97zbdj308wsr9fk71250dnl004w2ddqylbkn") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.12 (c (n "proc-macro2") (v "0.4.12") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0404ird230wpx6z88hzvnfhcf2jcd4s3m06yg50k83f2ggbs85vs") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.13 (c (n "proc-macro2") (v "0.4.13") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0rygwvwwkb5wg08l9pi4wj2szlhqkycw0nfc1rzkr28diwirfmpf") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.14 (c (n "proc-macro2") (v "0.4.14") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1c3gx5nvijf9xkyp3v34ir443z49mp3qsfa0apalqiqi6jnwccdk") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.15 (c (n "proc-macro2") (v "0.4.15") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0lp422hvj7l1hgyi836bv7pkcf465pwb79bcq4lvxmdirwxgjni9") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.16 (c (n "proc-macro2") (v "0.4.16") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1jagrh0bc4d2jhlxmrk2h1hxij8iwr6lk195xfy1kny0ia2hyiip") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.17 (c (n "proc-macro2") (v "0.4.17") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0jcmfs74ziy8md2v3grzp30ypap2byfwanyihanvqq5fw2xhzs3y") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.18 (c (n "proc-macro2") (v "0.4.18") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1f9mvl9l8i1wn6zkb1bpvba5l55yrzvx74dlw2sjxh3w0rvx795g") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.19 (c (n "proc-macro2") (v "0.4.19") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "009r1hh657ls0wllwr2am0vzl88rdj80acqb9d959llbikxj5q7z") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.20 (c (n "proc-macro2") (v "0.4.20") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1vpy56a2hy9rg47pyd7z9r19wf2sr5k6dsm96alr0jhbm6m7wyrx") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.21 (c (n "proc-macro2") (v "0.4.21") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1i2h86r4i2wpgmqhlkggzdgifkmy0378x0dkyr7kxiwalwdw4bxb") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.22 (c (n "proc-macro2") (v "0.4.22") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1kzrgw18slb0ahkb56nn4sskk06wfl30dkpf28qf6pha6h9i67xn") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.23 (c (n "proc-macro2") (v "0.4.23") (d (list (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0ixcqkpqppblsvv7fzj073mpj7r8vs85mz577rw5ssfs55mybnl8") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.24 (c (n "proc-macro2") (v "0.4.24") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "029z4nywd0lbgcwyj4z135b3wwlvnbq7liblx4ma0cbghabrcqbp") (f (quote (("proc-macro") ("nightly" "proc-macro") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.25 (c (n "proc-macro2") (v "0.4.25") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0zalkyqz1zvzbs0z4szzpg75hygfpf4w07rm9sap9an989qpnyfk") (f (quote (("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.26 (c (n "proc-macro2") (v "0.4.26") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0y0r9zbzhd49vzjnnjnicwkgxm1jarqabv60jz8l88cbv4ixvz9q") (f (quote (("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.27 (c (n "proc-macro2") (v "0.4.27") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "05c92v787snyaq4ss16vxc9mdv6zndfgsdq8k3hnnyffmsf7ycad") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.28 (c (n "proc-macro2") (v "0.4.28") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "04magpsbanhp7ldvnavxhq80g8bzswm7vjngqm29lgsbh57wi4ms") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.29 (c (n "proc-macro2") (v "0.4.29") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "05jkjvzh12x70l327yp2xkb7lq8ig96y1rck8p731ax7lz72gj34") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-0.4.30 (c (n "proc-macro2") (v "0.4.30") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0nd71fl24sys066jrha6j7i34nfkjv44yzw8yww9742wmc8j0gfg") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.0 (c (n "proc-macro2") (v "1.0.0") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0rv9v67rv97jgh7iby56cw8if6lw8iffwbb9ilqd1cn96k18gwhr") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.1 (c (n "proc-macro2") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "00n8db56q9m6qfcla8ddxi6m9qvmjygbx63nwnm6z1w8ms026p2c") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.2 (c (n "proc-macro2") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "15fxhlqvcw5rgf81mbcfddq7i69kpwyn6m060nzyjk2nrywl0nhp") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.3 (c (n "proc-macro2") (v "1.0.3") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1s4pnvpsnrwr2d2kd8mh3yq5rcqvrdmacs2fj95zacdkz6lq72p9") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.4 (c (n "proc-macro2") (v "1.0.4") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1k5brpv0c20whjhjvyjwksg4fgdcgnxjx53248kfsw7cfk67gp5g") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.5 (c (n "proc-macro2") (v "1.0.5") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1h3z1xq8vf2vwk21295lcaw9d0k38x825nwwbrjqxf9mh10mzkwh") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.6 (c (n "proc-macro2") (v "1.0.6") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "09rgb5ab0jgw39kyad0lgqs4nb9yaf7mwcrgxqnsxbn4il54g7lw") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.7 (c (n "proc-macro2") (v "1.0.7") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1k1bxv95j4mvd9nfk6y656ykwvh6mbpav8admwfnhqp4r8nrf683") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.8 (c (n "proc-macro2") (v "1.0.8") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0j45p176fnw0d02dzcky9sxyr4fadiggq07skmblwspqdxy33jrs") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.9 (c (n "proc-macro2") (v "1.0.9") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0d9lr9h0rpa4gi78ha37zqbmkdhrpyjvb5ia94m3ljc1cwf742bc") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.10 (c (n "proc-macro2") (v "1.0.10") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1qxbnl8i3a5b2nxb8kdxbq6kj3pd1ckhm35wm7z3jd7n5wlns96z") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.11 (c (n "proc-macro2") (v "1.0.11") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0kbw3pd72n9fvm5pdrnq9f46w1n3yc0wh26wzfy62sqbgf7c7lcx") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t)))

(define-public crate-proc-macro2-1.0.12 (c (n "proc-macro2") (v "1.0.12") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "06bk2n4iqj7m9kgrcgvbd21k9aq0lxb184bc2mjl5r7f91pwywl8") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.13 (c (n "proc-macro2") (v "1.0.13") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0fgncyhqyg5lpsj972kpfi7xcr3q8y9lirqwkiyf6a3b7bjzzxak") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.14 (c (n "proc-macro2") (v "1.0.14") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0cv943di4q6bhwlpvlcyvzw1brb726mxnadfdnmrp71dz17xsh6y") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.15 (c (n "proc-macro2") (v "1.0.15") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0ky0b5439b3wzj86khzjvdfx2brxqqnp3klkgmddda5za69hp9bh") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.16 (c (n "proc-macro2") (v "1.0.16") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0wapg3kg2k6nrwmm3x2z8pvj3ywcpivyb99rlsy1997n8gs5vsmy") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.17 (c (n "proc-macro2") (v "1.0.17") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0081b9kbpm1fraafv6cpq8mja306w07hsmpzrg4s8jcc8lpd20hm") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.18 (c (n "proc-macro2") (v "1.0.18") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1yn8szcbnm9j2sw427vpf603xjg6v27hfny40ifzdc8nm0qn7bmy") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.19 (c (n "proc-macro2") (v "1.0.19") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "04lb4n7g5z9mq6in39i7yb1m5bb107dfawc2rf4227npnn2z1x84") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.20 (c (n "proc-macro2") (v "1.0.20") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0a8sgk1i60nz7qlf611dxj1npawbv9nb0ch2lafvk7bialym2p0p") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.21 (c (n "proc-macro2") (v "1.0.21") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0b1azz1c2a3rap1kfz2sjinv7narfhssazaq39axvwwlvwb8bqin") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.22 (c (n "proc-macro2") (v "1.0.22") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "17x034dk1bgivnhyhl7ghdg23avvazfqw04x3xw2xlqq32x3vfg4") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.23 (c (n "proc-macro2") (v "1.0.23") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1jf6l45rshj50jfgs43iswvn02yrb2kd3ybvyq571bcfa797rvsi") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.24 (c (n "proc-macro2") (v "1.0.24") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0wcabxzrddcjmryndw8fpyxcq6rw63m701vx86xxf03y3bp081qy") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.25 (c (n "proc-macro2") (v "1.0.25") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1y96nc5af8figaksrdy5rxby4dfpmbaqi21x8p5v0p1fsfn5w09p") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.26 (c (n "proc-macro2") (v "1.0.26") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1v4w5jchgsssr727iyv986r8jaw6z80bzlhqgrbp78nw2lr02lm1") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.27 (c (n "proc-macro2") (v "1.0.27") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0f3h0zl5w5090ajmmvpmhkpr4iwqnn5rip3afacabhc657vwmn7h") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.28 (c (n "proc-macro2") (v "1.0.28") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "04k6kwfsl56xqv46f9k88jmj7x4544hhbr6xswzfm1mqqywdhzjw") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.29 (c (n "proc-macro2") (v "1.0.29") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0gfj25ksq90ifcssfnwv46v856sxd885wmm9bhrj1ays9xfi1xdr") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-proc-macro2-1.0.30 (c (n "proc-macro2") (v "1.0.30") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0w7fc5mvk7jsfgn1pmiphkvjd0min12zj1y0l1zqpg37pj73bhzd") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.31 (c (n "proc-macro2") (v "1.0.31") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "15vqn9zk68w6k9gqqifkbwmz9f06d1wlc0zkqqcllxrdvq5kb0dm") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.32 (c (n "proc-macro2") (v "1.0.32") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0hqbxlvhiaybakl1gai3mgps1dxsmxricxsr2rfdrh222z0qql5s") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.33 (c (n "proc-macro2") (v "1.0.33") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0yph154mbnzab21yabw7glkbnljz8b7n11cgya1fah7pbpgx4dzv") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.34 (c (n "proc-macro2") (v "1.0.34") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1c93jhwl8lv3hiqqvdhd2d2xhjryh5bqb9w5icr5i7bw1wnfk11g") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.35 (c (n "proc-macro2") (v "1.0.35") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0bnfpn0rggp172c4jnncms4cy95kx1n0x79pcg37qsysdxa58air") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.36 (c (n "proc-macro2") (v "1.0.36") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0adh6gvs31x6pfwmygypmzrv1jc7kjq568vsqcfaxk7vhdc2sd67") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.37 (c (n "proc-macro2") (v "1.0.37") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "1ldg6l97xlr4dal4kmk0c4l8kn7nn8w1a17wd8hdlpwd8cc74xgc") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.38 (c (n "proc-macro2") (v "1.0.38") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "1aj8mil84prgjga536bk45q17hcigxaz7b8q4bx7b4ackn7b89wh") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.39 (c (n "proc-macro2") (v "1.0.39") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0vzm2m7rq6sym9w73ca3hpc5m9wkwm500hyya6bgrdr5j1b2ajy5") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.40 (c (n "proc-macro2") (v "1.0.40") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1xyazdlnqmnkapjah7mjbanzb0zc4i4z5rgaz0vw75i5xpla35nx") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.41 (c (n "proc-macro2") (v "1.0.41") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1lk0bpmfk0k5sy695vvj03z424jljsir40pldy3w3070rlb2kk6d") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.42 (c (n "proc-macro2") (v "1.0.42") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "16y8maaixxaij77xk1krws51f4lpwz9y6vg9w3b35kyqy5jyjy62") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.43 (c (n "proc-macro2") (v "1.0.43") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1avvpf4qki8mg2na60yr3afbsfl5p6vllac6516xgwy93g3a4b0a") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.44 (c (n "proc-macro2") (v "1.0.44") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0n3wxpfagpm6kw7rmirgra27jdbk1il7icl29aic9di2h5m3bmvv") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.45 (c (n "proc-macro2") (v "1.0.45") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0dmfl82alzcsjqjjdmlk6ryarb5irjwd7pf9wqd8vagyyj6d1p1y") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.46 (c (n "proc-macro2") (v "1.0.46") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "02s7ycspw8fdk7f8cbi6njdwldifxrwgg428146b2iy3py6yzqll") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.47 (c (n "proc-macro2") (v "1.0.47") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "09g7alc7mlbycsadfh7lwskr1qfxbiic9qp9z751cqz3n04dk8sy") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.48 (c (n "proc-macro2") (v "1.0.48") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0vvngp8zilxwmm8vadh7y5zsmaap2f7vy840czjmlwi4p9frxn79") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.49 (c (n "proc-macro2") (v "1.0.49") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "19b3xdfmnay9mchza82lhb3n8qjrfzkxwd23f50xxzy4z6lyra2p") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.50 (c (n "proc-macro2") (v "1.0.50") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1qmsy8372anynndlfa0qig5y73gjnyvxldsrxs52vbygx9xxbxvf") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.51 (c (n "proc-macro2") (v "1.0.51") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1mj9r146akd3ldfic70bzqr7hwxd35lr0h551yk1vlirbfp7qwjx") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.52 (c (n "proc-macro2") (v "1.0.52") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0922fkhi689x134yh6l97lnpwgarhbv0vnv3vpnkpk1nx3lil3hx") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.53 (c (n "proc-macro2") (v "1.0.53") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0wwvd6s093q3xc1lvzrc4j40z1h4rkjq8m01mygw0fc2qwwnhims") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.54 (c (n "proc-macro2") (v "1.0.54") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0d3mv0fy9hp3a3n3ks1pvr33aj5a0h0ic9kjllabax4wg42a2wp4") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.55 (c (n "proc-macro2") (v "1.0.55") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0r05jpyb5dm2fizcc41mmmnavfsrvj4dacm42amgxp7w4jzd838x") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.56 (c (n "proc-macro2") (v "1.0.56") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0ddlk2c7s9c0fhmf8cd0wikayicv9xrm9ck9vzgg9w86rnqbsqrb") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.57 (c (n "proc-macro2") (v "1.0.57") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "05kwsqfm5d5b56a76ndjw2sgnnzmih8l814sgjraqh5iw1gnvv64") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.58 (c (n "proc-macro2") (v "1.0.58") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1a2w01q4pfnw823sr5kvjspixgpbf6vnc6qhf6bdv0f2q0pvh7zs") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.59 (c (n "proc-macro2") (v "1.0.59") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "06s5yglnz3h3x53rpp7az7ka6j169sg33al1nxhcc4xlhs5s3v3a") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.60 (c (n "proc-macro2") (v "1.0.60") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "01jl37rkmnxscz0k0arbjb7l80w7z8a64281w96wyqm8ny3b1hny") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.61 (c (n "proc-macro2") (v "1.0.61") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "07wilsrnsyzd142g9im38f3a2fkljl5iaxgdyvmds2hck9rnyfin") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.62 (c (n "proc-macro2") (v "1.0.62") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0b8ln3q5ismckgbmjq2cdn7l46w02abi5y0pkqm79lg9kyyzk3gy") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t) (r "1.31")))

(define-public crate-proc-macro2-1.0.63 (c (n "proc-macro2") (v "1.0.63") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1ssr3643nwfhw7yvqhibjw1k6nsnbv0lxq7mc1zcw38vjax8ydkv") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.64 (c (n "proc-macro2") (v "1.0.64") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1nm7qw54jn9n0g1ygkw4f63vj90m241yih6ph3g6zx7irdi3p03q") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.65 (c (n "proc-macro2") (v "1.0.65") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1zhxdbfn0g2q5wqzd4xcbglrgyr9yzwcjy8w4sqpia3h8q8jbplj") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-proc-macro2-1.0.66 (c (n "proc-macro2") (v "1.0.66") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1ngawak3lh5p63k5x2wk37qy65q1yylk1phwhbmb5pcv7zdk3yqq") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.67 (c (n "proc-macro2") (v "1.0.67") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0a0k7adv0yswsgzsqkd7r6ng8rpcdyqrhra5v5ii531y3agkshrx") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.68 (c (n "proc-macro2") (v "1.0.68") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0p001ki7dbfbv295rxs91d6rhmpg3kwc1jlcv5nyqqlnq3z0c4av") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.69 (c (n "proc-macro2") (v "1.0.69") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1nljgyllbm3yr3pa081bf83gxh6l4zvjqzaldw7v4mj9xfgihk0k") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.70 (c (n "proc-macro2") (v "1.0.70") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0fzxg3dkrjy101vv5b6llc8mh74xz1vhhsaiwrn68kzvynxqy9rr") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.71 (c (n "proc-macro2") (v "1.0.71") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1f7lnm5s24870wf649gpad847fjffg9dmpf4rkxvimfvz901bjvm") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.72 (c (n "proc-macro2") (v "1.0.72") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1i99vh7bqgyrcxj77awzmrg5p38a754ir8nj3bn7hr6g2s1k34x2") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.73 (c (n "proc-macro2") (v "1.0.73") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1qbzg6g5pgwfj5yfc6azf2qynahc2i88x2fm98i47702y6hyim9d") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.74 (c (n "proc-macro2") (v "1.0.74") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1nynw3s8in1pz5l0mfk4bgbvszlxyk85nc5vs2lcxkqjy818bs9d") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.75 (c (n "proc-macro2") (v "1.0.75") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "021pn6vxs5l4y08819q4ynijcr0p0m6w67fgs6fg5hk41yyn2ylh") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.76 (c (n "proc-macro2") (v "1.0.76") (d (list (d (n "quote") (r "^1.0") (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "136cp0fgl6rg5ljm3b1xpc0bn0lyvagzzmxvbxgk5hxml36mdz4m") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.77 (c (n "proc-macro2") (v "1.0.77") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0h336gfsll7nzf3xs7imxs8yml7q695vwj1bx6rplqjcl88ib6wz") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.78 (c (n "proc-macro2") (v "1.0.78") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1bjak27pqdn4f4ih1c9nr3manzyavsgqmf76ygw9k76q8pb2lhp2") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.79 (c (n "proc-macro2") (v "1.0.79") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0bn004ybzdqid81cqppr5c9jrvqsxv50x60sxc41cwpmk0igydg8") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.80 (c (n "proc-macro2") (v "1.0.80") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "03mgcpad63z6ga7hx8hvi89bvvaf1aaf59csid0997m2n0bflvd5") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.81 (c (n "proc-macro2") (v "1.0.81") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1fiyxjg5x5nn4vnazz93dnirf0s3grdnbf63m44qyq94q2q9f59x") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.82 (c (n "proc-macro2") (v "1.0.82") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "06qk88hbf6wg4v1i961zibhjz512873jwkz3myx1z82ip6dd9lwa") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.83 (c (n "proc-macro2") (v "1.0.83") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0hwzkqwnam5b2fsh6nsszc20jbwx9z8klnz5m5ic7pi7qdbfncqb") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-proc-macro2-1.0.84 (c (n "proc-macro2") (v "1.0.84") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1mj998115z75c0007glkdr8qj57ibv82h7kg6r8hnc914slwd5pc") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

