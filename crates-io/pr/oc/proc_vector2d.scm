(define-module (crates-io pr oc proc_vector2d) #:use-module (crates-io))

(define-public crate-proc_vector2d-1.0.1 (c (n "proc_vector2d") (v "1.0.1") (h "1cqg3yhamp6z4s5pyjdm4p5n2h89pa5sfimfsqvrrpy4ryzbcv9b")))

(define-public crate-proc_vector2d-1.0.2 (c (n "proc_vector2d") (v "1.0.2") (h "0hl3v70xym798vxixsvxfrc6gavjd3lkq26rfakkrphnmvxc1v22")))

