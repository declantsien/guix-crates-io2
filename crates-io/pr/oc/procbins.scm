(define-module (crates-io pr oc procbins) #:use-module (crates-io))

(define-public crate-procbins-0.4.1 (c (n "procbins") (v "0.4.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.17.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "0ky67nvrczdfkgx5458zk60z8013wkmhk69f4chr43hxbp3prg1s")))

