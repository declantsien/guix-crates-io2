(define-module (crates-io pr oc procfs-core) #:use-module (crates-io))

(define-public crate-procfs-core-0.16.0-RC1 (c (n "procfs-core") (v "0.16.0-RC1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (o #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i63s0rs4dc427ix93yyi1nzsfq4mldpga760gigqhqmlj80mq4f") (f (quote (("serde1" "serde" "bitflags/serde") ("default" "chrono")))) (r "1.48")))

(define-public crate-procfs-core-0.16.0 (c (n "procfs-core") (v "0.16.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (o #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0acs0p19yfcs0h787ls24vi5ql4g6c62cm57qh2cxx397a958d9d") (f (quote (("serde1" "serde" "bitflags/serde") ("default" "chrono")))) (r "1.48")))

