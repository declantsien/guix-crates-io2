(define-module (crates-io pr oc proc-caesar) #:use-module (crates-io))

(define-public crate-proc-caesar-1.0.0 (c (n "proc-caesar") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "16k4pxa6zr2bpis0p1i2mrkb4kic1yyn0nbf0cgarvya08cb4lv4")))

(define-public crate-proc-caesar-1.1.0 (c (n "proc-caesar") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)))) (h "1invxqwp4yydgpa1cg55dsri9wsj5qicq1pqdxp3blm4i8hdyr6n")))

