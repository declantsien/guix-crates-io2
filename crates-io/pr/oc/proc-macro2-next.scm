(define-module (crates-io pr oc proc-macro2-next) #:use-module (crates-io))

(define-public crate-proc-macro2-next-0.0.0 (c (n "proc-macro2-next") (v "0.0.0") (h "1mp9ggixzhzv9xs57pbnpcn3cibx2yziz1msvyi03iaqi8wp44s2") (y #t)))

(define-public crate-proc-macro2-next-1.0.0-rc1 (c (n "proc-macro2-next") (v "1.0.0-rc1") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0ya5jsc8f7nzff7cp8klafksffsivwn3qx4844a1pa40zp9cnhs4") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t)))

(define-public crate-proc-macro2-next-1.0.0-rc2 (c (n "proc-macro2-next") (v "1.0.0-rc2") (d (list (d (n "quote-next") (r "^1.0.0-rc1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1v4q8d1lj9aqcy4f77b1r4i2isxs9fvkmr6arc18dfabn3fwsmmg") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t)))

(define-public crate-proc-macro2-next-1.0.0-rc3 (c (n "proc-macro2-next") (v "1.0.0-rc3") (d (list (d (n "quote-next") (r "^1.0.0-rc1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "16abhbgafm9szvbxiz7rvmyazhc8484xx6772liffwi9dakkjf45") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t)))

(define-public crate-proc-macro2-next-1.0.0-rc4 (c (n "proc-macro2-next") (v "1.0.0-rc4") (d (list (d (n "quote-next") (r "^1.0.0-rc1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0s87b66n36c33m47bspxa50r0h10yic0x36pv7djradl9vr43x5r") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (y #t)))

