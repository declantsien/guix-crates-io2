(define-module (crates-io pr oc proc-mounts) #:use-module (crates-io))

(define-public crate-proc-mounts-0.1.0 (c (n "proc-mounts") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1rz244vcaz9v9lpqi6vlhz5fhrrpg7v4vddsy2nd9zf0znxi25cb")))

(define-public crate-proc-mounts-0.1.1 (c (n "proc-mounts") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "07a5xw5w0xkhd9yxhk2ksxgha7jkvikjgaxlwp7lc0rjdlxhi472")))

(define-public crate-proc-mounts-0.1.2 (c (n "proc-mounts") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.0") (d #t) (k 0)))) (h "01whbp4ya1550bfzm2qq3g08gsc53rmiyqy0ngsg83fr2pmw7grx")))

(define-public crate-proc-mounts-0.2.0 (c (n "proc-mounts") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.0") (d #t) (k 0)))) (h "1wqsa3mrf0py45sfyqr6c2z56c71k66wdfwm4ljilcdhdjy6z8f8")))

(define-public crate-proc-mounts-0.2.1 (c (n "proc-mounts") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.0") (d #t) (k 0)))) (h "0jc7fl77vrn9jdgmkf2ss63b1b9w9225q2dbcfh25nr1gizy9bcw")))

(define-public crate-proc-mounts-0.2.2 (c (n "proc-mounts") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.6") (d #t) (k 0)))) (h "1ygyab2i9d52yp3qj2cryr6lwqbgw845pcam33607mdvgddh90k2")))

(define-public crate-proc-mounts-0.2.3 (c (n "proc-mounts") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.6") (d #t) (k 0)))) (h "1nmd8bg59lr24n8g54md68d5gxacs41dfxs4rdzx47a36hhlg8ga")))

(define-public crate-proc-mounts-0.2.4 (c (n "proc-mounts") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.8") (d #t) (k 0)))) (h "0hj49vxr5k89nn8j8yihq4vmhnijq32dfqadm0b0zhmqs74fkmra")))

(define-public crate-proc-mounts-0.3.0 (c (n "proc-mounts") (v "0.3.0") (d (list (d (n "partition-identity") (r "^0.3.0") (d #t) (k 0)))) (h "1wpw3z2qq8wm3da8d0253a1h95nma6lad41m9yzp1ayh6n22yr8d")))

