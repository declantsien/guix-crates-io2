(define-module (crates-io pr oc process-sync) #:use-module (crates-io))

(define-public crate-process-sync-0.1.0 (c (n "process-sync") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "17df16wdzf8b8pg996xabvf4hp79hygl95d70xp0gwdljh4h25zr")))

(define-public crate-process-sync-0.2.0 (c (n "process-sync") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1v9ima7m0swgnc3rgal24yx5qqqf2c0mkmbwysr9rbxnzqcf91dd")))

(define-public crate-process-sync-0.2.1 (c (n "process-sync") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1n5ck346xj83ja5a4pcp83x30r0s9pspipcf545inbihr7jrargk")))

(define-public crate-process-sync-0.2.2 (c (n "process-sync") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "08rd0dlswdprj3axy7iiqz6z7m573wd7hiaxhjky6xrl4xryqi4b")))

