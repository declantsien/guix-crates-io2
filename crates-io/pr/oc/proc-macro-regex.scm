(define-module (crates-io pr oc proc-macro-regex) #:use-module (crates-io))

(define-public crate-proc-macro-regex-1.0.0 (c (n "proc-macro-regex") (v "1.0.0") (d (list (d (n "criterion") (r "~0.3.4") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0.26") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "regex") (r "~1.4.6") (d #t) (k 2)) (d (n "regex-syntax") (r "~0.6.23") (d #t) (k 0)) (d (n "syn") (r "~1.0.70") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "~1.0.24") (d #t) (k 0)))) (h "1lrmf71hmflhdccrnfcmpixpwd4qnr2rzgcsqllpaz9pi9pvk0z1")))

(define-public crate-proc-macro-regex-1.1.0 (c (n "proc-macro-regex") (v "1.1.0") (d (list (d (n "criterion") (r "~0.3.5") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0.36") (d #t) (k 0)) (d (n "quote") (r "~1.0.18") (d #t) (k 0)) (d (n "regex") (r "~1.5.6") (d #t) (k 2)) (d (n "regex-syntax") (r "~0.6.26") (d #t) (k 0)) (d (n "syn") (r "~1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "~1.0.31") (d #t) (k 0)))) (h "0zf36gvs3mi5kv460rsk0difqqli09vammy2r8mkx8xhfq9kz595")))

