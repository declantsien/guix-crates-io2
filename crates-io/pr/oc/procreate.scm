(define-module (crates-io pr oc procreate) #:use-module (crates-io))

(define-public crate-procreate-0.1.0 (c (n "procreate") (v "0.1.0") (d (list (d (n "nskeyedarchiver_converter") (r "^0.1.1") (d #t) (k 0)) (d (n "plist") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "zip") (r "^1.2.3") (d #t) (k 0)))) (h "0gmzl1b0pvqiq212rv11a48fvfyv14y6z2fk13czapypkgv87prf")))

