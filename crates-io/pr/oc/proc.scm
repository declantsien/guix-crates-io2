(define-module (crates-io pr oc proc) #:use-module (crates-io))

(define-public crate-proc-0.1.0 (c (n "proc") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^0.3") (d #t) (k 0)))) (h "12wxf2k3maykq6f3ikmi7n1vpal4d1nv03fspbshvcnyqsnkmpvq") (y #t)))

(define-public crate-proc-0.1.1 (c (n "proc") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^0.3") (d #t) (k 0)))) (h "1634gy2vjwdbz719i95s7wvhjy14nw8p8i1r4w3xz91fpj6f0p63") (y #t)))

(define-public crate-proc-0.1.2 (c (n "proc") (v "0.1.2") (h "09m8fhs91hxwj324l8v60zgz1wnpgrcvq0fiwiqm5505rb4ns4vh") (y #t)))

(define-public crate-proc-0.1.3 (c (n "proc") (v "0.1.3") (h "09i8vi7nkbfhwcpa31blg9pfqmykpjs6vlbmzdzi2p615xjnfdvp") (y #t)))

(define-public crate-proc-0.1.4 (c (n "proc") (v "0.1.4") (h "0cm3lmaa6m661yq8i84i38skw2fnlvrfaqrby7if21iq2ryw8x3k")))

