(define-module (crates-io pr oc proconio) #:use-module (crates-io))

(define-public crate-proconio-0.1.0 (c (n "proconio") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1df8psbq9br5jdlhyx5i2wfmwf7l9zknh1822r6vy7x1a9dx529l")))

(define-public crate-proconio-0.1.1 (c (n "proconio") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "17d6j05q7bp0l84yhmllcvh6lcjday1hzgx0l4864l2acc9d8c5p")))

(define-public crate-proconio-0.1.2 (c (n "proconio") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0v1acp1rd7v0za1y3zyvrxj73csgxds9h47hkg2hkqnp9dk0ka93")))

(define-public crate-proconio-0.1.3 (c (n "proconio") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "161ba3xc1vg5cvkvr3mx0hmf18dfr5n49vxhsvx149hyyrdjbxqq")))

(define-public crate-proconio-0.1.4 (c (n "proconio") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0wm2blhh02r1dsncwpnqx44jdihyk6c64qr306ynzdrjka0d40hz")))

(define-public crate-proconio-0.1.5 (c (n "proconio") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "14wypp5i4w3y3yqhn1c84cb70vr7db89ynpvwhym0x4hkmgypj77")))

(define-public crate-proconio-0.1.6 (c (n "proconio") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0raiyq17cgwlxp1g3vzr027z27a5z7wi1czcvw7qihj9pj0lvy2p")))

(define-public crate-proconio-0.2.0 (c (n "proconio") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1grhpsvllfridykwj6jc4impdx7z4x8fwdkiqsypxfrx91ymhq3m")))

(define-public crate-proconio-0.2.1 (c (n "proconio") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.5") (d #t) (k 0)))) (h "08x9nqyp8rw001jzmwnk0vg6g20fnpywnixj55c1bacx5l3ahamf")))

(define-public crate-proconio-0.2.2 (c (n "proconio") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 2)))) (h "0i6pqa8nbdqqvjrgldsch0frsxkfy92wp0dlki185lp4yjz8f1m9") (y #t)))

(define-public crate-proconio-0.3.0 (c (n "proconio") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1kxgni8brxjc7mbk930gw2lk6hjnzxxa1307f3hl1grcz4nlm7xy")))

(define-public crate-proconio-0.3.3 (c (n "proconio") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "05h73vvyjj5l9mfk04ffmq6kz9c0p59dssnl5f2a424ilq5i018m") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.3.4 (c (n "proconio") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "05ymh45bia95bwwj65fh4dc9v6fsqnybjp6gfipcydl7wgs7fbgm") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.3.5 (c (n "proconio") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0n3jxviny66mzqg9wmh02jz71ihx3prn2qc94fqm9jp5iqfk6l1z") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.3.6 (c (n "proconio") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1k8whvfikbsdwbiavpq57yawmrk3sj0c1nsizn7bp14dz2alzv9v") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.4.0 (c (n "proconio") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0afihqhkg92gzc18y7sf432i6cz7s6i1n9anfzmr26kjalxigg3z") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.4.1 (c (n "proconio") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0vsic9dfxj262ra6dgnafqnl9h1gwhapyjspfp9saik985z7m1zs") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.4.2 (c (n "proconio") (v "0.4.2") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "02xamx7pmzff3pmp54zrlg15s3jdhf7vch9h6fpq5cisbcwi81ha") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.3.7 (c (n "proconio") (v "0.3.7") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "01igfgcsr3zlipjlg23fgmaiq97257gaaqbscr0gw73sjhiclkx5") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.3.8 (c (n "proconio") (v "0.3.8") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1dg13p05vlvv15hqfcmaf8n09rs8v4famdicwrab0jwfcnf366d4") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.4.3 (c (n "proconio") (v "0.4.3") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0ndxl47hdcf550mb0jn0xdlc232dzrsbq003h676z4zj7az37hwi") (f (quote (("derive" "proconio-derive"))))))

(define-public crate-proconio-0.4.4 (c (n "proconio") (v "0.4.4") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "once_cell") (r "=1.14.0") (d #t) (k 0)) (d (n "proconio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "1qhzv60rfngg24jaa6s9c5nxsj0z9xngmx95pp8sp6igjnpqnmr1") (f (quote (("derive" "proconio-derive")))) (r "1.67.1")))

(define-public crate-proconio-0.4.5 (c (n "proconio") (v "0.4.5") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "proconio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "17nwjacwpfm7mlmdwxl45qryj96s1b7ml4g7j0irw1wmcxhh9d0f") (f (quote (("derive" "proconio-derive")))) (r "1.70.0")))

