(define-module (crates-io pr oc procedural-masquarade) #:use-module (crates-io))

(define-public crate-procedural-masquarade-0.0.0 (c (n "procedural-masquarade") (v "0.0.0") (h "05bs175kwbxs32m3hhps6m1kxzi7lmcdccvily5cidgdw85ynxr0")))

(define-public crate-procedural-masquarade-0.1.0 (c (n "procedural-masquarade") (v "0.1.0") (h "0f1knm4qfj1swmhayzsyj3bp7wmyjjq7w3xryz4lbn9d5rhkypdk")))

(define-public crate-procedural-masquarade-0.2.0 (c (n "procedural-masquarade") (v "0.2.0") (h "0pd1lcjjn6k7b4rl3awkvhc6bgap492v6xvdyn5b1194rip0jppv")))

