(define-module (crates-io pr oc proc-macro-nested) #:use-module (crates-io))

(define-public crate-proc-macro-nested-0.1.0 (c (n "proc-macro-nested") (v "0.1.0") (h "064bi57b27hcv1859fp89a9wv1w60ykglmv9vw8p6z9jsix5zkfq")))

(define-public crate-proc-macro-nested-0.1.1 (c (n "proc-macro-nested") (v "0.1.1") (h "1j7zcr1nk56jdx7rsac4ks6az95x87hygn2niy8aphh9lschxxjz")))

(define-public crate-proc-macro-nested-0.1.2 (c (n "proc-macro-nested") (v "0.1.2") (h "1dmmspxwi3jx73bidrvm4i4gbnrh3q2j7g4k78kx2s33bl18x5p9")))

(define-public crate-proc-macro-nested-0.1.3 (c (n "proc-macro-nested") (v "0.1.3") (h "0bmlksm8vl44wkwihmwr7jsjznhbg0n7aibcw1cs2jgjcp86x6in")))

(define-public crate-proc-macro-nested-0.1.4 (c (n "proc-macro-nested") (v "0.1.4") (h "15664z2pjnn7ajpbrir4wh4c16jxz4i8r46y73n2kvfkz6an154f")))

(define-public crate-proc-macro-nested-0.1.5 (c (n "proc-macro-nested") (v "0.1.5") (h "1pmnm488ff22n8miizanbj2yqshldc5vbw70s18yvsdrcga1pzha")))

(define-public crate-proc-macro-nested-0.1.6 (c (n "proc-macro-nested") (v "0.1.6") (h "0nnwm9bvp1fmr8nqjp8ynrkj97yzpsdh3062li8b0f4hzgd818gb")))

(define-public crate-proc-macro-nested-0.1.7 (c (n "proc-macro-nested") (v "0.1.7") (h "11hh1jynh62f3m1ii0f9gf1l3y0fhkwpmr40lz3704v848n1p25w")))

