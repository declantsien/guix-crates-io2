(define-module (crates-io pr oc process_uptime) #:use-module (crates-io))

(define-public crate-process_uptime-0.1.0 (c (n "process_uptime") (v "0.1.0") (h "1dhiaqbp3mfr1vbzb6007g4xxyz1nspscf5q9hgpcxgm5dnvd4r6")))

(define-public crate-process_uptime-0.1.1 (c (n "process_uptime") (v "0.1.1") (h "0kf2xz5hsk8hk89pw6vqavfdwi677ppxdnhw4aar4gy4zhhahzkh")))

(define-public crate-process_uptime-0.1.2 (c (n "process_uptime") (v "0.1.2") (h "1wwzl16rv1knsf37zr9q7hmw7j7rv68ir9wripk5vj7rlzzdshxg")))

(define-public crate-process_uptime-0.1.3 (c (n "process_uptime") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)))) (h "09s17jjdb33569lwrvds7c3zc5bk5bx6p969v60n0pslfabz65a4")))

(define-public crate-process_uptime-0.1.4 (c (n "process_uptime") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "09jh63c99ff44k11sy0ar0g0hc46xb8958bz9wr2kxr5r6i099nc")))

