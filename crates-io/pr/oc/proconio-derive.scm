(define-module (crates-io pr oc proconio-derive) #:use-module (crates-io))

(define-public crate-proconio-derive-0.1.0 (c (n "proconio-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b6ya5xgd023byqp9b91p0b3hspd0gkh0j1g8gg1rnf5lmr7vw1w")))

(define-public crate-proconio-derive-0.1.1 (c (n "proconio-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.1.1") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pzlaxw01q7mwddyh1ch7c7y3cpp3k47cmfw50w0fjg6b1d05gg8")))

(define-public crate-proconio-derive-0.1.2 (c (n "proconio-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.1.2") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qmqnjm1vlxdzim0qk62s1whcjbcim7m84wclmhg0nmyz4vrqbr2")))

(define-public crate-proconio-derive-0.1.3 (c (n "proconio-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.1.2") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vq0c7db8f3avf2w94jj5jsfiv3rvmwlwpd6qkf5qsgw4c8ipcvq")))

(define-public crate-proconio-derive-0.1.4 (c (n "proconio-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.1.2") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d1ihl1wkqpv1w0ppfv91cwb8d829kcc22albrza8b4r63fjz54y")))

(define-public crate-proconio-derive-0.1.5 (c (n "proconio-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gzy356sb1glwnal84w9qd18cq2ppdnjd0skr7xp81mi7ycvir6b")))

(define-public crate-proconio-derive-0.1.6 (c (n "proconio-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05ic19ra96x6l29a9j1nash0ij8q911kq54il9dxyajnm48j2bzw")))

(define-public crate-proconio-derive-0.1.7 (c (n "proconio-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "proconio") (r "^0.4.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1n07k3wa9g5jnvdigqwd3axp03lbnfa5kqnnniibi749x07mm5vb") (y #t)))

(define-public crate-proconio-derive-0.2.0 (c (n "proconio-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "proconio") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0c1rjdq7s5vcr0a68ww09qmnlppxkb6amffra85yjjgvmia1lwhz")))

(define-public crate-proconio-derive-0.2.1 (c (n "proconio-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "proconio") (r "^0.4.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "02flphzl82v2hbfli7v0vydis852k6brhc9mg5inbnx3y5wa1kxw")))

(define-public crate-proconio-derive-0.1.8 (c (n "proconio-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wq3d00a8c9p56ihrys2la620sb5avwlzr0m6xa982yjkg5d54s4") (y #t)))

(define-public crate-proconio-derive-0.1.9 (c (n "proconio-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proconio") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f37791qkgw84s6rdqv49zfbwvgyykykacpr16zsa7207cqwk5l0")))

