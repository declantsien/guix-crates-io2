(define-module (crates-io pr oc procmem) #:use-module (crates-io))

(define-public crate-procmem-0.0.1 (c (n "procmem") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "windows") (r "^0.32") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Memory"))) (d #t) (k 0)))) (h "1pigrgh6v888kb4npiw4g7hppy31spkg9qsk5m0nvkswpf8i9873")))

(define-public crate-procmem-0.0.2 (c (n "procmem") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "windows") (r "^0.32") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Memory"))) (d #t) (k 0)))) (h "02nwm2y2dgv6ixv4cycsnsdzxa3knq1nvx6z4n7j8dck06mnldf1")))

