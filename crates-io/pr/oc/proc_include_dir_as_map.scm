(define-module (crates-io pr oc proc_include_dir_as_map) #:use-module (crates-io))

(define-public crate-proc_include_dir_as_map-1.0.0 (c (n "proc_include_dir_as_map") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0nmp548x81b6bm87rlycdk7myabdkrrcpx4qxx7s6jv671zz0a9q")))

(define-public crate-proc_include_dir_as_map-1.1.0 (c (n "proc_include_dir_as_map") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "12lsf3sf6c9z0ps6lrw6jq8m7qxgbkl8ycddcp9cvr6rr3cqx2r9") (f (quote (("always-embed"))))))

