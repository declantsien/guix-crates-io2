(define-module (crates-io pr oc procmeta-proc) #:use-module (crates-io))

(define-public crate-procmeta-proc-0.1.0 (c (n "procmeta-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18yx4pbrm5zckybkz4cqk70dddmm6wdc6z2x8jzq55qml4p1sha1")))

(define-public crate-procmeta-proc-0.1.1 (c (n "procmeta-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0384s4c11p3pzvh2sx35mla8lb0v6kwscvi5jhydbl5qkdvpa9rb")))

(define-public crate-procmeta-proc-0.2.0 (c (n "procmeta-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0g6aribgahkdqjfxgyzdwdzf16s6abprc8l8sr1n7gwp0knwy9nj")))

(define-public crate-procmeta-proc-0.2.1 (c (n "procmeta-proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07ccnqhm7jva09yl06djhamsslxgxdana9ny6x5866w3499nb1yr")))

(define-public crate-procmeta-proc-0.2.2 (c (n "procmeta-proc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12ydrhgs1c0f2x07damz5rhzbpm7cz4x1rk6sdjf3lz97wsrvgap")))

(define-public crate-procmeta-proc-0.2.3 (c (n "procmeta-proc") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14rimjykp2rlsciy9rxnpdjisqhw3dgc2l6a974ms7755cv2ffq5")))

(define-public crate-procmeta-proc-0.2.4 (c (n "procmeta-proc") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "150hlnspscvaj58xqapahhs4k9lmx2g7qb8hnayvi1zk9x28ap0q")))

(define-public crate-procmeta-proc-0.2.5 (c (n "procmeta-proc") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0d4i58spm9is0wnzank0w1rckizqgxpslxapbsnwp3bg7sy2wri8")))

(define-public crate-procmeta-proc-0.2.6 (c (n "procmeta-proc") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16rlmi6cpcdm306g74p2lzcakj3n1ib525i2x0psc5dxiz94lmvi")))

(define-public crate-procmeta-proc-0.2.7 (c (n "procmeta-proc") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1dagfrg4lq823nbk0fliw5cjk2yk98g5g3l1hpm23mkr9jfx21k6")))

(define-public crate-procmeta-proc-0.2.8 (c (n "procmeta-proc") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1r8n84f2nwbaz0va93lmndf2ahhgcddnyn3gjaq75h6vy2hry9jf")))

(define-public crate-procmeta-proc-0.2.9 (c (n "procmeta-proc") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qz1m3qlsrg3swl8kfmg72xiajp1lq7mizl0cah7s0ba70m8warq")))

(define-public crate-procmeta-proc-0.3.0 (c (n "procmeta-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00nqpq0hb38kkrghr25sxrcpbgf2h4zpmrwk2m2c3syqrgnvjikm")))

(define-public crate-procmeta-proc-0.3.1 (c (n "procmeta-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17gffxckc00vxd4cnf7cmdzrd0ym2y647lyh1gpk1nyqzbvci91v")))

(define-public crate-procmeta-proc-0.3.2 (c (n "procmeta-proc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0l543vx5s903c5dsy245r64yikjxnr9hjvcgb3a4m46ha42r2qmr")))

(define-public crate-procmeta-proc-0.3.3 (c (n "procmeta-proc") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dk4mkl9zrnsjl0wn2sb1bq6f02lhvqcma0fdg9y9aiilzw0ci4w")))

(define-public crate-procmeta-proc-0.3.4 (c (n "procmeta-proc") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14p2604krlvm5fhbfkhb242qksb123afp44b1szk0pmdkr45wj5a")))

(define-public crate-procmeta-proc-0.3.5 (c (n "procmeta-proc") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta-core") (r "^0.3.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0z934ffwl3y79d3xnl55mvlvmwv8b3p2qbgmkl90cli91qmj17gv")))

