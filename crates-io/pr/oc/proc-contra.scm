(define-module (crates-io pr oc proc-contra) #:use-module (crates-io))

(define-public crate-proc-contra-1.0.0 (c (n "proc-contra") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rzyfya9wriavmnkxd72pzayd5w36grb398hla2897p6gir7gr3s")))

(define-public crate-proc-contra-1.0.1 (c (n "proc-contra") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05n4234brpz47bkjd1ap8szq7gv04zxs1biwlk0ilnlawb04spjv")))

(define-public crate-proc-contra-1.1.1 (c (n "proc-contra") (v "1.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vh3a27fvk06mlbzb23f5jjkybbcv9fwb7gg4in4jajxanlqdx1l")))

(define-public crate-proc-contra-2.0.1 (c (n "proc-contra") (v "2.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17va1b9ipcgzh3ndrrnn1kda1sv3f80gv1q7469wc83dwfbrmvx9")))

(define-public crate-proc-contra-2.0.3 (c (n "proc-contra") (v "2.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18qfyak91f85a3dxkhj5r5s9pkjy945h3bp5ilviygk9bnbh4yif")))

(define-public crate-proc-contra-2.0.4 (c (n "proc-contra") (v "2.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q6slg2hamk3m65mmpxkcrwg2i0lx99rg6jdmw9xv7y66spjylpk")))

(define-public crate-proc-contra-2.0.5 (c (n "proc-contra") (v "2.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "154z8jwg617j2kwq51jd3r1hdcx6c13szzdj5sh548f6rqhj79lp")))

(define-public crate-proc-contra-2.0.6 (c (n "proc-contra") (v "2.0.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11iqj9hlwm3ahq562lk3wfy91jmjvvsqyx6lrypw87nsbi3w6xlv")))

(define-public crate-proc-contra-2.0.7 (c (n "proc-contra") (v "2.0.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18r91cq6qxyd0x50f4i5dpaqzkn1bnymjckjdx1g2d6h1a95x6q8")))

(define-public crate-proc-contra-2.0.8 (c (n "proc-contra") (v "2.0.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w1pdgp55518v215nby19big6mbrc0b44zmkq3n9ww69cbl7k234")))

(define-public crate-proc-contra-2.0.9 (c (n "proc-contra") (v "2.0.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z63a2ygrni336j84hgrpw8jvmbs972iqqlf4vm8adgicnzgbcq8")))

(define-public crate-proc-contra-2.0.11 (c (n "proc-contra") (v "2.0.11") (d (list (d (n "lib-contra") (r "^2.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0si1kh75jnl73gibn0ylqccqmsizm0wdfj0i7275nmszspwbhff1")))

(define-public crate-proc-contra-2.1.1 (c (n "proc-contra") (v "2.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ffdkglri20f52l01dsm3qc0cggpyb3qvza2099nlx465bd4m9af")))

(define-public crate-proc-contra-4.0.0 (c (n "proc-contra") (v "4.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s1j62j4v98haqprcqm8sdvgfq2dbzhw3x7xrl744sqgy6bhfqi8")))

(define-public crate-proc-contra-4.2.0 (c (n "proc-contra") (v "4.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1884mdqnlci4qr74yx03d0qrv8fj1qwvwggykfaix79fiqimg71i")))

(define-public crate-proc-contra-4.8.0 (c (n "proc-contra") (v "4.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sg83mhlglb7v6p6w8hfdhw5m78kn1fpjjahfk1brfh879x1vlsm")))

(define-public crate-proc-contra-4.9.0 (c (n "proc-contra") (v "4.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "194kibnv4gya5gs0bhw10ihljiw1cnwg455pxl6757847m459wip")))

(define-public crate-proc-contra-4.10.0 (c (n "proc-contra") (v "4.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gnxy3wgh6rbsy4wfnwhcbq49wh7rhjm3amjlczg22hfsw6ji1l2")))

(define-public crate-proc-contra-4.11.0 (c (n "proc-contra") (v "4.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jk635ip361mc76xbv7zh2xwfcd8y3jal9pz9ymiiid0snq54083")))

(define-public crate-proc-contra-4.12.0 (c (n "proc-contra") (v "4.12.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m2g4mzr9x58bf4clh6xhgrqsdxi5j59wh90fj49yfgnavf9hlz0")))

(define-public crate-proc-contra-4.13.0 (c (n "proc-contra") (v "4.13.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cqascph75zdw7b7dd6z1fi5bzx0bkdiig6rndim5fm55cvh7nqy")))

(define-public crate-proc-contra-4.13.1 (c (n "proc-contra") (v "4.13.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s99ffgw7llbhrzm9n3z9s4b2adv2957is9i15l140aaga51l1qy")))

(define-public crate-proc-contra-4.13.2 (c (n "proc-contra") (v "4.13.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04qr88mc5qysan0qj79zbgjw7svpwlhwgmgd1jv65lj1lslwdr8f")))

(define-public crate-proc-contra-4.13.3 (c (n "proc-contra") (v "4.13.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04bw0nicy59wswq8hlgzi92237xlv76ksw3xnmar69i7gzgkaq40")))

(define-public crate-proc-contra-4.13.4 (c (n "proc-contra") (v "4.13.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yxckpnam397jm1x873d21nz0f61sbsbk96l621l0wsmlm13iycy")))

(define-public crate-proc-contra-4.13.5 (c (n "proc-contra") (v "4.13.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0is3zp21kzf10ljz4qjkj69ykwry6hcq3sh4zvi038rggvsncla1")))

(define-public crate-proc-contra-4.14.0 (c (n "proc-contra") (v "4.14.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03d6mznxpcvdx6jvvxx0vrwn61bmlp6r9zsklwrlr5qa9za1j1va")))

(define-public crate-proc-contra-4.15.0 (c (n "proc-contra") (v "4.15.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19zs1ysp2hvbcg4p4hnyjyfw1zspnrz8xldzxms2qhib5qay5xyj")))

(define-public crate-proc-contra-5.0.0 (c (n "proc-contra") (v "5.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yjvravnkjxab9djm233gw4554w05070bg16q8x9dczia45qd51c")))

(define-public crate-proc-contra-5.0.1 (c (n "proc-contra") (v "5.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nwzl7zx33nbs7pw61h84qm0xh2zwwbyqiz26bqzkjcyxzwmm7w5")))

(define-public crate-proc-contra-5.0.2 (c (n "proc-contra") (v "5.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yh81j5y4a82a36gv82w9nq0pg67vdl3w8k84fpqwnjrjq5gnddw")))

