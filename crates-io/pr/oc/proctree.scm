(define-module (crates-io pr oc proctree) #:use-module (crates-io))

(define-public crate-proctree-0.1.0 (c (n "proctree") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "1yw5imv3p9lm8dbby5pdgcsxxznxwlbbnvrd9yg5052bn1vs6jbq")))

(define-public crate-proctree-0.1.1 (c (n "proctree") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "1cxrk6ssgj505bp4jniqqb814i123lbwng0zax3yz66f3kawr1i9")))

(define-public crate-proctree-0.1.2 (c (n "proctree") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0wvsy92l8ilp10v11q4kz99pij1j8cc4sanxyjalws2hbl7njlqh")))

