(define-module (crates-io pr oc proc-opt) #:use-module (crates-io))

(define-public crate-proc-opt-0.0.1 (c (n "proc-opt") (v "0.0.1") (h "192gw33h8p3dwnsj0rx0iqlwl7m6d7sgdygwzzm58m6c5rs4lmfd") (y #t)))

(define-public crate-proc-opt-0.1.0 (c (n "proc-opt") (v "0.1.0") (h "1qy9wa475mwpkr5vx557cm528rncs47p7yx6dlsjz71ph45yk0kg")))

(define-public crate-proc-opt-0.1.1 (c (n "proc-opt") (v "0.1.1") (h "0b05a0bc9a6ycdl5v6fzb040a759mmcisrrsawygaxi8a7i686vj") (y #t)))

(define-public crate-proc-opt-0.1.2 (c (n "proc-opt") (v "0.1.2") (h "1wqmlrik4246b2hidbn2dhylk9c7771pmc9isv2z48jh0x0ai899") (y #t)))

(define-public crate-proc-opt-0.1.3 (c (n "proc-opt") (v "0.1.3") (h "02lc9j0yn9wp0m23w4afkadxasia7241hq6z09bk2b74nclsv3r9")))

