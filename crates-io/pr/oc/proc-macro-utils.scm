(define-module (crates-io pr oc proc-macro-utils) #:use-module (crates-io))

(define-public crate-proc-macro-utils-0.1.0 (c (n "proc-macro-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)))) (h "102jm8wy4gfw4dmw33h8c28qppygv1cg0igwhxknyxpypac45bvj") (f (quote (("default" "proc-macro2"))))))

(define-public crate-proc-macro-utils-0.2.0 (c (n "proc-macro-utils") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)))) (h "0a5bngl3l24mqnbvkvx78bbb3mr3s8fagmwig6x0xibhd9jllaki") (f (quote (("default" "proc-macro2"))))))

(define-public crate-proc-macro-utils-0.3.0 (c (n "proc-macro-utils") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)))) (h "1lhwcsqd3ngvv6xj9d7ppnw0kdxfqjqc8my250rkmshlrrz651zq") (f (quote (("proc-macro") ("default" "proc-macro2" "proc-macro"))))))

(define-public crate-proc-macro-utils-0.4.0 (c (n "proc-macro-utils") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)))) (h "08js6dabbnp9d1jc5jjshs9kbgr45p9rffs8zly0z3xn98nj97q0") (f (quote (("proc-macro") ("default" "proc-macro2" "proc-macro"))))))

(define-public crate-proc-macro-utils-0.5.0 (c (n "proc-macro-utils") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1ssi161p8nvp651zx3iv4y6iqdp8gbvvvh8fdy2hjzcq1k2dli1a") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2"))))))

(define-public crate-proc-macro-utils-0.5.1 (c (n "proc-macro-utils") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "12hhv47jh1d099hjs7ar4jpnz1kwzpzrp5h3jy1nvwdi8hxbxn42") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2"))))))

(define-public crate-proc-macro-utils-0.5.2 (c (n "proc-macro-utils") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0qz3nws8wqm75z1zgv440i4jvcifylkls2vcvl381gf86dhdnnjw") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2"))))))

(define-public crate-proc-macro-utils-0.6.0 (c (n "proc-macro-utils") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0n2bbvh8dqj2w85pckznmbkqxflmph2ljak8v0zxllnganfy7cds") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser")))) (s 2) (e (quote (("proc-macro2" "dep:proc-macro2"))))))

(define-public crate-proc-macro-utils-0.7.0 (c (n "proc-macro-utils") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)))) (h "1192xmqh4rh3g9sk0217jx7dzl5kq0pswglc6zj5zsyx4qryzx9y") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser" "quote"))))))

(define-public crate-proc-macro-utils-0.8.0 (c (n "proc-macro-utils") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.5") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)))) (h "1a21rpa1245f4n07f5j6ywwq21h1q56qqmv91q3sb5gpw84y2n9z") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser" "quote"))))))

(define-public crate-proc-macro-utils-0.9.0 (c (n "proc-macro-utils") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.5") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)))) (h "01gf0q99v7m5zwszr9kxaps14ynyx2xfwwv9xm6azk8wmwzil44b") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser" "quote"))))))

(define-public crate-proc-macro-utils-0.9.1 (c (n "proc-macro-utils") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.5") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)))) (h "0jlscg3qjiyyn6z790azfx4z0wwyyad8ak2dpv6966dg6j8xhrf4") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser" "quote"))))))

(define-public crate-proc-macro-utils-0.10.0 (c (n "proc-macro-utils") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.5") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)))) (h "0wbhpdb1rp4h6f6m82ignhpb2hgji30bvdbpb0hvq0747nhhibzf") (f (quote (("proc-macro") ("parser" "smallvec" "proc-macro2") ("default" "proc-macro2" "proc-macro" "parser" "quote"))))))

