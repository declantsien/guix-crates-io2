(define-module (crates-io pr um prumpt) #:use-module (crates-io))

(define-public crate-prumpt-0.1.1 (c (n "prumpt") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05w8nrs9kvgancwvfcd635x26wff32nn58bz1plaz4f6jfaq4cnr")))

(define-public crate-prumpt-0.1.2 (c (n "prumpt") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nv74xbzvycsn72547cwfsnmn7nn7hgj16dxnp1ncgvgjwgznq32")))

