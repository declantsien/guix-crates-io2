(define-module (crates-io pr ep preprocess-macro) #:use-module (crates-io))

(define-public crate-preprocess-macro-0.2.0 (c (n "preprocess-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s6akwyysjhaykx7nzw3qd35m2mdv36k8c454awjx4bvndqs2kn1")))

(define-public crate-preprocess-macro-0.2.1 (c (n "preprocess-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0apbkaw0ar7frp4lvg3243jb95484d78f61idvr9rzy7q4pv2vh1")))

(define-public crate-preprocess-macro-0.3.0 (c (n "preprocess-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19f8ld5hg3z115a2dvnqndmlcas4csv6qs8hbyb07hbf7p64983b")))

(define-public crate-preprocess-macro-0.3.1 (c (n "preprocess-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gf0f8f0mg7cn55zi06m05vvscb794dgvfgads8ds21iyalkj0m9")))

(define-public crate-preprocess-macro-0.3.2 (c (n "preprocess-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xqf8akrf4lpxcv2inw77yvmbpk9adlr92hqr3w1canmijs4rv3s")))

(define-public crate-preprocess-macro-0.3.3 (c (n "preprocess-macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvj9zjghknrgxzqysqmdjaj0b6v92k1q8k9wjy5q06sv18aa8vq")))

(define-public crate-preprocess-macro-0.4.0 (c (n "preprocess-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w11g6fikh09n4bmdrd0v53ly344g0pmgz842gnyqcaj09fgk93h")))

(define-public crate-preprocess-macro-0.4.1 (c (n "preprocess-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0abpysbfkilqglpx3m2m8cvjc4zdss9n1a7530fy56djyipswqd8")))

(define-public crate-preprocess-macro-0.5.0 (c (n "preprocess-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1d0zj8bqlm9pgjlz7882qbwg6jb42azrakgqjyg7g40davvgzc91")))

(define-public crate-preprocess-macro-0.5.1 (c (n "preprocess-macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ck14pwrbk5y69l145hv229cqk2vs6ssm5plg41x7ky3mcal4y0f")))

(define-public crate-preprocess-macro-0.5.2 (c (n "preprocess-macro") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("default"))) (k 0)) (d (n "quote") (r "^1") (f (quote ("default"))) (k 0)) (d (n "regex") (r "^1") (f (quote ("default"))) (k 0)) (d (n "syn") (r "^2") (f (quote ("default" "full"))) (k 0)))) (h "1vx9znknxdcmfjmzy1lsw9scbjim30k9hd50vbdkqp1rxfzinvmp")))

