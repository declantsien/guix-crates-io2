(define-module (crates-io pr ep prepare_fasta) #:use-module (crates-io))

(define-public crate-prepare_fasta-0.1.0 (c (n "prepare_fasta") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("default" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "needletail") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (f (quote ("asm" "asm-aarch64"))) (d #t) (k 0)))) (h "1k6iwpv66jjdc4r5yzhgc80mwpsi2wfbm62059ia3zp0bnjl8b4z")))

