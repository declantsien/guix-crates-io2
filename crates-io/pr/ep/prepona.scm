(define-module (crates-io pr ep prepona) #:use-module (crates-io))

(define-public crate-prepona-0.0.1 (c (n "prepona") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.22.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1px41dknjmbmh8w5ri5ip4kl6wh3f6z1b6f7w283fkxp9bf4nmhg")))

(define-public crate-prepona-0.1.0 (c (n "prepona") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "magnitude") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0hw5kvc8xqr1sf36awg059ks43sxjy0n91p06hb8ppcig21n0rw0")))

