(define-module (crates-io pr ep preprocess) #:use-module (crates-io))

(define-public crate-preprocess-0.1.0 (c (n "preprocess") (v "0.1.0") (h "16xfaivffq059i697ywfp5012zcx90ml7isqcklppcah5mc5yy3x")))

(define-public crate-preprocess-0.2.0 (c (n "preprocess") (v "0.2.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1jc7nmm0x07pqkcxx02igl9m51df9rfir685wggmi1qvmm3xzmk6")))

(define-public crate-preprocess-0.2.1 (c (n "preprocess") (v "0.2.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0wr2qassyk03ilakisnvm3ppy4q2vdlk2ll8bpdil71hx9x575qw")))

(define-public crate-preprocess-0.3.0 (c (n "preprocess") (v "0.3.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "173s0gz9k4rj8f8ri7qnwgha4cnxhfhvw020wijbgxp6d7x4mjk8")))

(define-public crate-preprocess-0.3.1 (c (n "preprocess") (v "0.3.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1n704bivgsd5d7xqr72pynycgrbpbjzzwkgn4g7qfdrz2b26p4a7")))

(define-public crate-preprocess-0.3.2 (c (n "preprocess") (v "0.3.2") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "17d5v9czjb0qyijjh8975j0811zs7xka86qmjfiq2zhnqhzrw5w6")))

(define-public crate-preprocess-0.3.3 (c (n "preprocess") (v "0.3.3") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ijl5wq5pnvrq91wkjdp0pgcn5d6nagjwnfyc2l57dmsgjvp2brb")))

(define-public crate-preprocess-0.4.0 (c (n "preprocess") (v "0.4.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1wxracsp6sjh59szgnsmshdi0i911fmpmwagabkvhg3svr75cmgs")))

(define-public crate-preprocess-0.4.1 (c (n "preprocess") (v "0.4.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0jwq2rzk0s74gnzzpa4hzy9dvag7grgz2adyb7jqaxsckmawppvd")))

(define-public crate-preprocess-0.5.0 (c (n "preprocess") (v "0.5.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1zhfqgn5jgggfp91r28michbn0axcvh46sj02v8y2wg123w5b1av")))

(define-public crate-preprocess-0.5.1 (c (n "preprocess") (v "0.5.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "idna") (r "^0.4") (d #t) (k 0)) (d (n "preprocess-macro") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1larra3zlk9bidmrkj3r1jg7b1804vrksf99qvqyfcvxl4sz52wm")))

(define-public crate-preprocess-0.5.2 (c (n "preprocess") (v "0.5.2") (d (list (d (n "dashmap") (r "^5") (k 0)) (d (n "idna") (r "^0.5") (f (quote ("default"))) (k 0)) (d (n "preprocess-macro") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("default"))) (k 0)) (d (n "url") (r "^2") (f (quote ("default"))) (k 0)))) (h "01ddh287798r7ygjpp48irf6nrks6sr5fngaijvlk9p1i00gambf")))

