(define-module (crates-io pr im prime-derived) #:use-module (crates-io))

(define-public crate-prime-derived-0.1.0 (c (n "prime-derived") (v "0.1.0") (d (list (d (n "prime-forge") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "18zdzn7lkahp0cgcv1s9vpw0z8qcpkwmj30qql8gskkpp37kbz1s")))

(define-public crate-prime-derived-0.2.0 (c (n "prime-derived") (v "0.2.0") (d (list (d (n "prime-forge") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1grn13l6zjbssgira0c92nv9p15b8ydm9fmpy1zz7phyp82rhk24")))

(define-public crate-prime-derived-0.2.1 (c (n "prime-derived") (v "0.2.1") (d (list (d (n "prime-forge") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1y2jg534bdyqx4b8ziylcl6j5fhfb3lh7x71dagn1jyb0sjp2qz0")))

(define-public crate-prime-derived-0.2.2 (c (n "prime-derived") (v "0.2.2") (d (list (d (n "prime-forge") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "007ryj02hdfvnhkrz9ryjbhn89jx72d6qdjc8jcanlrxakg99rx1")))

(define-public crate-prime-derived-0.2.3 (c (n "prime-derived") (v "0.2.3") (d (list (d (n "prime-forge") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1lvzx9i94nx0xns9dc9qkx8337m8z1kvfz4vijrmmvd533wz3y88")))

(define-public crate-prime-derived-0.3.1 (c (n "prime-derived") (v "0.3.1") (d (list (d (n "prime-forge") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0yi6rvmqshanp8prg6l6gcjrvj0q46vi4g3l0ynd6z6z663h413s")))

(define-public crate-prime-derived-0.3.2 (c (n "prime-derived") (v "0.3.2") (d (list (d (n "prime-forge") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0cqdn3qfmpa2w10ng0hi309xhk9wlp43ajq0a64w5l7vicv2qcqc")))

(define-public crate-prime-derived-0.3.21 (c (n "prime-derived") (v "0.3.21") (d (list (d (n "prime-forge") (r "^0.3.21") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1fhpzandqy8wjxg4kpaaycy0dmn5lxhkfxy251i2dq88x0brv4wy")))

