(define-module (crates-io pr im primal-bit) #:use-module (crates-io))

(define-public crate-primal-bit-0.2.0 (c (n "primal-bit") (v "0.2.0") (h "0qni3falan6yx04bpipq5fg7dvzanal6ylqzxkqa3ryvjlfg2i9r") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.2.1 (c (n "primal-bit") (v "0.2.1") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "0yy572ha1gxr67gmyk1s92s7zjsi6sspp02a0gq64a84av5akfhw") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.2.2 (c (n "primal-bit") (v "0.2.2") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "1znjgrz0x7adchp8lq3shmaxk0l8f0w9szklc8wjkn30m6ckg4nz") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.2.3 (c (n "primal-bit") (v "0.2.3") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "13xyswg9g2p4py0dy4hh8a7dm3gdnmvkbfpmsj1nf5h6pkz92jj6") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.2.4 (c (n "primal-bit") (v "0.2.4") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "08cvigajy2pdf9yn137qp1sqfvkbkrwzaalr894wd501ypi68sk8") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.3.0 (c (n "primal-bit") (v "0.3.0") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "06zc4phj5zf7alvcpfkmdv6akcfs2080z27cn087i434ag4fsdr8") (f (quote (("unstable"))))))

(define-public crate-primal-bit-0.3.1 (c (n "primal-bit") (v "0.3.1") (d (list (d (n "hamming") (r "^0.1") (d #t) (k 0)))) (h "0h92gxkzziba82z23cnsksh34vpnp5y9rp6m7g550y58n88zxr3c") (f (quote (("unstable"))))))

