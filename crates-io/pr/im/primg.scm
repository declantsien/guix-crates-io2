(define-module (crates-io pr im primg) #:use-module (crates-io))

(define-public crate-primg-0.1.0 (c (n "primg") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "jni") (r "~0.8.1") (t "cfg(target_os = \"android\")") (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1xf3lfwjby6b1cbx8bqb2rbbf62wp1bklnfy3gn6y3w4cl9dlimf")))

