(define-module (crates-io pr im primwrap) #:use-module (crates-io))

(define-public crate-primwrap-1.0.0 (c (n "primwrap") (v "1.0.0") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "virtue") (r "^0.0.15") (d #t) (k 0)))) (h "0db3hvwdcq0kqr8n9mjywpnnf9bad63fcjwvcd8adwzqxdjrqszm")))

(define-public crate-primwrap-1.1.0 (c (n "primwrap") (v "1.1.0") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "virtue") (r "^0.0.15") (d #t) (k 0)))) (h "1082h9p9n930fiif4mb14mqkf7r25lwajax6jv497f0hhlz2cq3n")))

