(define-module (crates-io pr im prime_bag) #:use-module (crates-io))

(define-public crate-prime_bag-0.1.0 (c (n "prime_bag") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gcd") (r "^2.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "17834isd4zr8gwrvfl8ybnradghz07rjppbjnb5ma1c92khrjpmm")))

(define-public crate-prime_bag-0.2.0 (c (n "prime_bag") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gcd") (r "^2.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vcfs1d0iy13i725hg6xa8jbb40dr91r0c5zwicv00iwnc6nmh6v")))

(define-public crate-prime_bag-0.3.0 (c (n "prime_bag") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gcd") (r "^2.3.0") (d #t) (k 0)) (d (n "iai-callgrind") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0srmpqp1qdf9isarhvz942m7lx84r5fdagq4iayblkcvfb0qw3qx") (f (quote (("primes256"))))))

