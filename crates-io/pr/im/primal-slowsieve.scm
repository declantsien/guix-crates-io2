(define-module (crates-io pr im primal-slowsieve) #:use-module (crates-io))

(define-public crate-primal-slowsieve-0.2.0 (c (n "primal-slowsieve") (v "0.2.0") (d (list (d (n "primal-bit") (r "^0.2") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.2") (d #t) (k 0)))) (h "0pxrwvkq1pf2a40x9vpg78zdsq8ha30sw90vnan6ysk631sadl7d") (f (quote (("unstable"))))))

(define-public crate-primal-slowsieve-0.3.0 (c (n "primal-slowsieve") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "primal-bit") (r "^0.3") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.3") (d #t) (k 0)))) (h "1ph6rjyyplwm2xydwrdg6w1nfsvgz27qapirk51p2jpwym693gy9") (f (quote (("unstable") ("slow_tests"))))))

(define-public crate-primal-slowsieve-0.3.1 (c (n "primal-slowsieve") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal-bit") (r "^0.3") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.3") (d #t) (k 0)))) (h "1ls6pydn0hsl670b0ncrjf981i1zbr1qf5m17ylnp7gnqwarq312") (f (quote (("unstable") ("slow_tests"))))))

