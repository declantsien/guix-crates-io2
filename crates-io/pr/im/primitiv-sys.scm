(define-module (crates-io pr im primitiv-sys) #:use-module (crates-io))

(define-public crate-primitiv-sys-0.3.0 (c (n "primitiv-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "1bf62w7v3q54f9hfs8xiaxi9rdmi71nqcwbj6fggqagn03x14vp4") (f (quote (("opencl") ("eigen") ("cuda"))))))

