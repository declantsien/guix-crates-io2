(define-module (crates-io pr im primes) #:use-module (crates-io))

(define-public crate-primes-0.1.0 (c (n "primes") (v "0.1.0") (h "0jvv5jz13dnvsgasddhhlrryyzq7mqyz6y0niyi1mg2gvr4vj3a4")))

(define-public crate-primes-0.1.1 (c (n "primes") (v "0.1.1") (h "1wzj16nas4hxxazpfmk4ia6k2hhq2a7ngj53ymvvnsmhspj4vix1")))

(define-public crate-primes-0.1.2 (c (n "primes") (v "0.1.2") (h "19mf8k2viq4kv1l62kxihg6lclixbarav9zrqksbjwgy0p29psm3")))

(define-public crate-primes-0.1.3 (c (n "primes") (v "0.1.3") (h "18fnb43n4vcb6a16nvvim4lk757p0xk0q6cs0mkdg43fni77r8z9")))

(define-public crate-primes-0.1.4 (c (n "primes") (v "0.1.4") (h "1lzb0azm4886isgz829yjc7akarn4baq04hzbnk7k9fwcirgspbq")))

(define-public crate-primes-0.1.5 (c (n "primes") (v "0.1.5") (h "0f7cm913blyb6nv2a9nrgmsa2wy03v7jh20jqbqrxsd0sff7z46f")))

(define-public crate-primes-0.1.6 (c (n "primes") (v "0.1.6") (h "1r4ixs3xlynk5acr6ylbcp8mvlg84zyl67zsi6ng782ilvc1jig6")))

(define-public crate-primes-0.1.7 (c (n "primes") (v "0.1.7") (h "0cq56dzijw3wrb3p34m28rmavsq9y1an6cm7zxf9ynsj9nwznd8i")))

(define-public crate-primes-0.2.0 (c (n "primes") (v "0.2.0") (h "1lcav9p9brslwjf4vp2lhnzg757nhrmnx4s5mqlsfqckv3pshn1n")))

(define-public crate-primes-0.2.1 (c (n "primes") (v "0.2.1") (h "0mh95vpjkbjf4bd6bj941m3y87bnkv81z5j5k696x6fskwd5ynvz")))

(define-public crate-primes-0.2.2 (c (n "primes") (v "0.2.2") (h "19cdl39j3mavlh4p1j6cr5vb7cvk2qaap1vhd2xjs7v3f2sa7xgi")))

(define-public crate-primes-0.2.3 (c (n "primes") (v "0.2.3") (h "12f5s2b2nz0iryrfnmjiz0l15cx41jyf53qyrc1gfkzc3jinas5q")))

(define-public crate-primes-0.2.4 (c (n "primes") (v "0.2.4") (h "0rpj6jfa1wr3wdgr29086lh0156x27xn9704hz1n57z1ddvgzcwx")))

(define-public crate-primes-0.3.0 (c (n "primes") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1j7rzs17g23y49v8pan7m5xmzdrb01i923l7ldqwvvmwv21119k8")))

