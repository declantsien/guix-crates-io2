(define-module (crates-io pr im prime-forge) #:use-module (crates-io))

(define-public crate-prime-forge-0.1.0 (c (n "prime-forge") (v "0.1.0") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1ipfcvma2mgp0vww3sl9g0500p39fim2v3jw2x6zjp1vvwxjx4w4")))

(define-public crate-prime-forge-0.2.1 (c (n "prime-forge") (v "0.2.1") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "04y9x5z6f8x8xg2lg3qc7x3v3dnj9xnklsbvp3xdlcmh2vkjlwzv")))

(define-public crate-prime-forge-0.2.2 (c (n "prime-forge") (v "0.2.2") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0x3y603w4jxzxgjrbyxlk0hkiax647i7vj6fckj9bjfjbxnrwbqc")))

(define-public crate-prime-forge-0.3.0 (c (n "prime-forge") (v "0.3.0") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1j93zg9pql4gbc4zxhd9xsjw9zrmgiiajdcjv65f36wc5liyk7xb")))

(define-public crate-prime-forge-0.3.1 (c (n "prime-forge") (v "0.3.1") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1c3rp5aayl9cl5mldzn7rkzplvm81k3b79zf041413n03pvvxy9y")))

(define-public crate-prime-forge-0.3.2 (c (n "prime-forge") (v "0.3.2") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "09ql80rp5ijnjqb8raap7qizlcdb51pjircg34d7515b183y5pf0")))

(define-public crate-prime-forge-0.3.21 (c (n "prime-forge") (v "0.3.21") (d (list (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0habmfaamk7kqy9dk42lvlbhb474z5rmi3ciafjpsclh9f6dsx3r")))

