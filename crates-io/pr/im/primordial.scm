(define-module (crates-io pr im primordial) #:use-module (crates-io))

(define-public crate-primordial-0.1.0 (c (n "primordial") (v "0.1.0") (h "04yd5gpb75l0vk2wglbg7yghpkd9abap2jxcbif4i9jp761r97cp")))

(define-public crate-primordial-0.3.0 (c (n "primordial") (v "0.3.0") (d (list (d (n "const-default") (r "^0.1") (o #t) (d #t) (k 0)))) (h "099zvpg5i4hb3g0y5q26qhaw71yf37c88zkczfrmh9r2c8j33mjm") (f (quote (("alloc"))))))

(define-public crate-primordial-0.4.0 (c (n "primordial") (v "0.4.0") (d (list (d (n "const-default") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1zhc93pkyk1wslnzr9q7481r5l5nrmkfii834ws4iyggf236y55k") (f (quote (("alloc"))))))

(define-public crate-primordial-0.5.0 (c (n "primordial") (v "0.5.0") (d (list (d (n "const-default") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "07brn5631p5fawn2gv5n59xjxhfi278pjjyyrjbghk1ps328himj") (f (quote (("alloc"))))))

