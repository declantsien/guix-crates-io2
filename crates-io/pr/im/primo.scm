(define-module (crates-io pr im primo) #:use-module (crates-io))

(define-public crate-primo-0.0.1 (c (n "primo") (v "0.0.1") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "0kr3kw2j7rwyyxhlwa8ww95hdkd8yj87xk0a8lb4q16ka4a8wvnq")))

