(define-module (crates-io pr im primapalooza) #:use-module (crates-io))

(define-public crate-primapalooza-0.1.0 (c (n "primapalooza") (v "0.1.0") (h "1wjbym0mfmds2b70vhnp7c1aq43qamgfkzp6vi7lmk8gk0krsdji")))

(define-public crate-primapalooza-0.1.1 (c (n "primapalooza") (v "0.1.1") (h "1h9p6vn8ca5hf0vj00waimc8nn9f2gaa8ipv6nj2a0646q6ik5r7")))

(define-public crate-primapalooza-0.1.2 (c (n "primapalooza") (v "0.1.2") (h "1kgj90xwvk1zw9c12g5xmijgldfz20lp1l2q5m6ywn46w6mj2zzx")))

(define-public crate-primapalooza-0.1.3 (c (n "primapalooza") (v "0.1.3") (h "1lk0jqbc7wadwx82hvr0w7mwl8b6md29smdyx0m3d5mrxb9ng5kz")))

(define-public crate-primapalooza-0.1.4 (c (n "primapalooza") (v "0.1.4") (h "0c8kmlg8cr3wkf1c8lsb23amh80271km89j06gj1n4k76haza498")))

(define-public crate-primapalooza-0.1.5 (c (n "primapalooza") (v "0.1.5") (h "1ffw683ar134xp5qpxd20vryf6nzlhbxqycp3rscaial78givc5c")))

(define-public crate-primapalooza-0.1.6 (c (n "primapalooza") (v "0.1.6") (h "0zgngmhqahmj5ybl5qnlwwrnv06maf56z5fplvwaab8jbs2pcyiy")))

(define-public crate-primapalooza-0.1.7 (c (n "primapalooza") (v "0.1.7") (h "1xjsy2qxcbr24dvpshr39wyqqcqnaky6s5lgibb2hcjz1fhrfgmn")))

(define-public crate-primapalooza-0.1.8 (c (n "primapalooza") (v "0.1.8") (h "1vbdcqcl4nb35pwx2wkb5cdi4v378p86nbc9nkgdids0jsnklh57")))

(define-public crate-primapalooza-0.1.9 (c (n "primapalooza") (v "0.1.9") (h "0b2srclivwlwmch6yw6r9lmyxn6advvdk484h7gf69z6rg8fmjqf")))

(define-public crate-primapalooza-0.2.0 (c (n "primapalooza") (v "0.2.0") (h "0a4cphiyc40mrcigxfkmz5ipmxzb4mwkvgc21r4c328nqkdpj42h")))

(define-public crate-primapalooza-0.3.0 (c (n "primapalooza") (v "0.3.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "190d7fhm6nqj2g67gpvrqlwl7zav2wjl5179fcb0fvm6marw270l")))

(define-public crate-primapalooza-0.3.1 (c (n "primapalooza") (v "0.3.1") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "17l0fgg27v78l96x9pvy7dcfg0skjdh47ppvpmqlnx7yvzi1h3lj")))

(define-public crate-primapalooza-0.3.2 (c (n "primapalooza") (v "0.3.2") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "074crkfccr0h210bhp6rymv1i5h3zcd9a6a7cq6pzvj5m78i9nkd")))

(define-public crate-primapalooza-0.3.3 (c (n "primapalooza") (v "0.3.3") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "13rgicwi7qi2kj26w8yw3n229dn6hn5f436bxmhvg9s0siz6190a")))

(define-public crate-primapalooza-0.3.4 (c (n "primapalooza") (v "0.3.4") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "1ia7qi84aqc1bqi97xz2pg56p2rp5sidv92vd6cny2v5gd7p4pbc")))

