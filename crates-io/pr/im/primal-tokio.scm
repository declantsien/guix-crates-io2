(define-module (crates-io pr im primal-tokio) #:use-module (crates-io))

(define-public crate-primal-tokio-0.1.0 (c (n "primal-tokio") (v "0.1.0") (d (list (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "stream" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "stream" "sync" "macros"))) (d #t) (k 2)))) (h "0bpsllp6il78fv9d9qvpb3vc3zdf39sspj2sjamyxivjnbxf67n1")))

