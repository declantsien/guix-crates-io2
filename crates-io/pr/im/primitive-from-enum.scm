(define-module (crates-io pr im primitive-from-enum) #:use-module (crates-io))

(define-public crate-primitive-from-enum-0.1.0 (c (n "primitive-from-enum") (v "0.1.0") (d (list (d (n "primitive-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v0laidvn9vpjqd3k7hnc11fp5zshd7yczsa66i2h6wrjz60cvv6") (f (quote (("std") ("default" "std"))))))

(define-public crate-primitive-from-enum-0.2.0 (c (n "primitive-from-enum") (v "0.2.0") (d (list (d (n "primitive-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1361rnbj0r3qvwdd15p4aa8llh1wvkqhwh115h4j95z7wy89l8jq") (f (quote (("std") ("default" "std"))))))

(define-public crate-primitive-from-enum-0.2.1 (c (n "primitive-from-enum") (v "0.2.1") (d (list (d (n "primitive-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pn4jscw40af5v3ddxdfj90xd66zcjdzfkywyb8fscdqwp06c1lf") (f (quote (("std") ("default" "std"))))))

(define-public crate-primitive-from-enum-0.2.2 (c (n "primitive-from-enum") (v "0.2.2") (d (list (d (n "primitive-enum-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1840bnnh9y8gb01yc0bz65qn4kdnpmp4wcf6fwg6gfnidg89wnk6") (f (quote (("std") ("default" "std"))))))

(define-public crate-primitive-from-enum-0.3.0 (c (n "primitive-from-enum") (v "0.3.0") (d (list (d (n "primitive-enum-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m0hx68fzw2w50c8d04ns271ylf8dah3lv6c1rg9kqnyd89qk321") (f (quote (("std") ("default" "std"))))))

(define-public crate-primitive-from-enum-0.3.1 (c (n "primitive-from-enum") (v "0.3.1") (d (list (d (n "primitive-enum-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1irp6criw7j3d1mzhvivg8z6wjk5fnh7cw7s86abqfbkwyvw9c4l") (f (quote (("std") ("default" "std"))))))

