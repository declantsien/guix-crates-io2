(define-module (crates-io pr im primitive_promotion) #:use-module (crates-io))

(define-public crate-primitive_promotion-0.1.0 (c (n "primitive_promotion") (v "0.1.0") (h "0vn4nh3l4cji17d39h8krga01ibc3lgkyvl3bw5dsqllhf718hyf") (r "1.56.1")))

(define-public crate-primitive_promotion-0.1.1 (c (n "primitive_promotion") (v "0.1.1") (h "1wa0rvfmqnx138k5x4vr50rizlnz0rb79nsnfjhj8b52ixvms5a8") (r "1.56.1")))

(define-public crate-primitive_promotion-0.1.2 (c (n "primitive_promotion") (v "0.1.2") (h "10463bq8dmp01sdwvc6pd1f416dj5hkqg85fs3f2j1v91lbjp7j8") (r "1.56.1")))

(define-public crate-primitive_promotion-0.1.3 (c (n "primitive_promotion") (v "0.1.3") (h "052xx3q5r72axgqhpxs5iccsilclh9136m1nqwhv4hqczs9z0n8m") (r "1.56.1")))

(define-public crate-primitive_promotion-0.1.4 (c (n "primitive_promotion") (v "0.1.4") (h "0vqr9v6wkgmzpf8vjv6yyf5l3igaqy5b6d4vhws0bi9b19q9nzj2") (r "1.56.1")))

