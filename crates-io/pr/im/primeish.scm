(define-module (crates-io pr im primeish) #:use-module (crates-io))

(define-public crate-primeish-0.1.0 (c (n "primeish") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)))) (h "1d92cg217cnlq7kg5l5w8hdq2v7c1022g7n49wwvbmpnwi8hn560")))

