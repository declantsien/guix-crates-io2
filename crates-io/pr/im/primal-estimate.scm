(define-module (crates-io pr im primal-estimate) #:use-module (crates-io))

(define-public crate-primal-estimate-0.2.0 (c (n "primal-estimate") (v "0.2.0") (d (list (d (n "primal") (r "*") (d #t) (k 2)))) (h "1qx76i86aip6kpg2ppnig9ac3snsjvwaq2hq2877s9qyn5nck2bc") (f (quote (("unstable"))))))

(define-public crate-primal-estimate-0.2.1 (c (n "primal-estimate") (v "0.2.1") (d (list (d (n "primal") (r "*") (d #t) (k 2)))) (h "1mgq54jbvjn9p47sinlwpw3ld88xch28cg290rlvamz7vlqlbsjn") (f (quote (("unstable"))))))

(define-public crate-primal-estimate-0.3.0 (c (n "primal-estimate") (v "0.3.0") (d (list (d (n "primal") (r "^0.2") (d #t) (k 2)))) (h "1xargn9i557rnm1fwlxw2dm237xzmb5i0d58i98gfsddik7cywkb") (f (quote (("unstable"))))))

(define-public crate-primal-estimate-0.3.1 (c (n "primal-estimate") (v "0.3.1") (d (list (d (n "primal") (r "^0.3") (d #t) (k 2)))) (h "0gw55nckjlps6cb77d8msrm02j4mj5181lmwcyjwmgvw19appq0j") (f (quote (("unstable"))))))

(define-public crate-primal-estimate-0.3.2 (c (n "primal-estimate") (v "0.3.2") (d (list (d (n "primal") (r "^0.3") (d #t) (k 2)))) (h "0ncibx4i5i5w20phlaqkb4mybnaxmj0nk05ywrqi4gpjfr6g2x3k") (f (quote (("unstable"))))))

