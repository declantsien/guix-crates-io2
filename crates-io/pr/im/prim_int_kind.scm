(define-module (crates-io pr im prim_int_kind) #:use-module (crates-io))

(define-public crate-prim_int_kind-0.1.0 (c (n "prim_int_kind") (v "0.1.0") (d (list (d (n "konst") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ksnxnsyywskaklpfshx83yw2h5w7zsx1zaypapcxllp6nm76hrm") (f (quote (("const_trait_impl")))) (r "1.56.1")))

(define-public crate-prim_int_kind-0.1.1 (c (n "prim_int_kind") (v "0.1.1") (d (list (d (n "konst") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gm3j5msz2rr3jp180xjmdhrbsvyy37sfn14qvfbdinicpfnvazc") (f (quote (("const_trait_impl")))) (r "1.56.1")))

(define-public crate-prim_int_kind-0.1.2 (c (n "prim_int_kind") (v "0.1.2") (d (list (d (n "just_prim_int") (r "^0.1.0") (d #t) (k 0)) (d (n "konst") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0w5b8na2njhy8kncn3xq136ild9c5f85nsv7aya50nsxizffg424") (f (quote (("const_trait_impl")))) (r "1.56.1")))

(define-public crate-prim_int_kind-0.1.3 (c (n "prim_int_kind") (v "0.1.3") (d (list (d (n "just_prim_int") (r "^0.1.0") (d #t) (k 0)) (d (n "konst") (r "^0.2.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1f19npxlxpiypc15sfx7ky6m6kjmkgaf9639lmanbg598nin6dyb") (f (quote (("const_trait_impl")))) (r "1.56.1")))

