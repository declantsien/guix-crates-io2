(define-module (crates-io pr im prime_gen) #:use-module (crates-io))

(define-public crate-prime_gen-1.0.0 (c (n "prime_gen") (v "1.0.0") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0ywvfk2vx3h23higbwxm18xd8blwvgrivkx8g5w1g66zqy4qx58v")))

(define-public crate-prime_gen-1.0.1 (c (n "prime_gen") (v "1.0.1") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "1zwz7s8xfwc5qwk3yfxxpy1bgmpb961ggg1r7d1yh82wpg8wkhss")))

