(define-module (crates-io pr im primenumbe-rs) #:use-module (crates-io))

(define-public crate-primenumbe-rs-0.1.0 (c (n "primenumbe-rs") (v "0.1.0") (h "0alj4qwk49f1l6fdp5hladylz03fv0qkqfmg4b0ly4ws062xw7wr")))

(define-public crate-primenumbe-rs-0.1.1 (c (n "primenumbe-rs") (v "0.1.1") (h "1siadhk5wi361z015x6slrrwf3nxr0s752g6a75lv2si65623l12")))

(define-public crate-primenumbe-rs-0.1.2 (c (n "primenumbe-rs") (v "0.1.2") (h "003d9pzj2jisdfkm0ddprvzchhnsds7k7haf4c4yf1g8dklsz0ci")))

