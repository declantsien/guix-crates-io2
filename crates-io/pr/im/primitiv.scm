(define-module (crates-io pr im primitiv) #:use-module (crates-io))

(define-public crate-primitiv-0.3.0 (c (n "primitiv") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "primitiv-sys") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0zyqp7bywary5q261mkgxpp8yn4mzp4piv2mjlxza20pr297d66a") (f (quote (("opencl" "primitiv-sys/opencl") ("eigen" "primitiv-sys/eigen") ("cuda" "primitiv-sys/cuda"))))))

