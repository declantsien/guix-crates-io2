(define-module (crates-io pr im primitive_fixed_point_decimal) #:use-module (crates-io))

(define-public crate-primitive_fixed_point_decimal-0.1.0 (c (n "primitive_fixed_point_decimal") (v "0.1.0") (h "1gaq4bmj334dklxb6a80p59jbxgqx3yj5q4vfzzgjrqlkvws7va5")))

(define-public crate-primitive_fixed_point_decimal-0.1.1 (c (n "primitive_fixed_point_decimal") (v "0.1.1") (h "1b6av8rx4jpg7y8d8ipv9ardd0vz72xas1npm92ngp3sddwsln8z")))

(define-public crate-primitive_fixed_point_decimal-0.1.2 (c (n "primitive_fixed_point_decimal") (v "0.1.2") (h "1kfsfg7awzb0dkrjb8gnc5bkqkzy7az068q0zqi1rwpygyl72l91")))

(define-public crate-primitive_fixed_point_decimal-0.1.3 (c (n "primitive_fixed_point_decimal") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "13y86f1g6jzcgj88mcx92rjxmzhy7lcjvzs9xv2fk725dr9456kc")))

(define-public crate-primitive_fixed_point_decimal-0.1.4 (c (n "primitive_fixed_point_decimal") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1dw6dbkp0b82hwipfgl9kz90w08w6n1grqhjcjflpl08lga5mify")))

(define-public crate-primitive_fixed_point_decimal-0.1.5 (c (n "primitive_fixed_point_decimal") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0zfv4asmb0kx5hz3p8qkq07w6c0m5cgiyzs536cmfhk0drkb20al")))

(define-public crate-primitive_fixed_point_decimal-0.1.6 (c (n "primitive_fixed_point_decimal") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1bl4nw3marqn6k8b3d1jymckz7gfjw30ywff9pns5hbybgip3nzl")))

