(define-module (crates-io pr im primeval) #:use-module (crates-io))

(define-public crate-primeval-0.0.3 (c (n "primeval") (v "0.0.3") (h "0s7irhzdvhj65cw8l6h712g2p1skpwx29xfd41g71hc8f359h2bw")))

(define-public crate-primeval-0.0.4 (c (n "primeval") (v "0.0.4") (h "0r08b70hpsg5zrajzpinji5g4ls3qy6d2ba01svrf8bd0iijw9qa")))

(define-public crate-primeval-0.1.4 (c (n "primeval") (v "0.1.4") (h "0rk166yw77ndqn4965bgg9apm8807kr1pswh6hi5j0ga5319jfch")))

(define-public crate-primeval-0.1.5 (c (n "primeval") (v "0.1.5") (h "0xdyh399wspfw66yd30k9qhrwx38q7cwg1q9qv56y386089gq999")))

(define-public crate-primeval-0.2.0 (c (n "primeval") (v "0.2.0") (h "0ryds3i6sqxlwljd1lxrssjib5aa3s5pkylwwj8vvp4rdpn8jvyd")))

(define-public crate-primeval-0.2.1 (c (n "primeval") (v "0.2.1") (h "0pmhknzrppz30ap13n5rx4028vkjwjphawddv5hsqnpdcsspxdg3")))

(define-public crate-primeval-0.2.2 (c (n "primeval") (v "0.2.2") (h "1xxqj9ph2msj165vdwjysjjw9zy2bka3cvm7ddlw3i3lswz9gcpp")))

(define-public crate-primeval-0.2.3 (c (n "primeval") (v "0.2.3") (h "1l4xq2j8dk0112magx31f3jsbmd968d3xng0dq4j57h5mfzsx497")))

(define-public crate-primeval-0.2.4 (c (n "primeval") (v "0.2.4") (h "193rqbsw06fsf87lrrxcla426flxshrwxbrpzfq0argqj51n9nfy")))

(define-public crate-primeval-1.0.0 (c (n "primeval") (v "1.0.0") (h "0ypm1ra4dq0l8icqljwlm9z8gyp92jvirldwxiq435n26dajjfs4")))

