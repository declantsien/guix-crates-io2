(define-module (crates-io pr im primeshor) #:use-module (crates-io))

(define-public crate-primeshor-0.1.0 (c (n "primeshor") (v "0.1.0") (h "15mybv0n0vpvgsl8fixzcffahcljny68qbni3nqjylg2ra7kmg25")))

(define-public crate-primeshor-0.1.1 (c (n "primeshor") (v "0.1.1") (h "06n00q7pl8kdl3vmn7wqvrpwpk0s327f7n8ds0dk10nx4kq2n95h")))

(define-public crate-primeshor-0.2.0 (c (n "primeshor") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0srkz9kk3rkylvdf0p8z030fkrkgn5y8gj5xi7a9vfksgmgb5qzf")))

