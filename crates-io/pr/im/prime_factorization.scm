(define-module (crates-io pr im prime_factorization) #:use-module (crates-io))

(define-public crate-prime_factorization-1.0.0 (c (n "prime_factorization") (v "1.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14n38y2n3maakjy12nsfpw9v4d2vmhl1mqkwg3vcwpf5g5fnbgfd")))

(define-public crate-prime_factorization-1.0.1 (c (n "prime_factorization") (v "1.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1rwd4vmnf2f9k0i4bqa1zpxyvgycdzhhmzsi6zmybf9dlzhq36ym")))

(define-public crate-prime_factorization-1.0.2 (c (n "prime_factorization") (v "1.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xix6fimhlc6d4hnsy61pqk077dkfwjqcfpldwjgg6sd2mw8vx0n")))

(define-public crate-prime_factorization-1.0.3 (c (n "prime_factorization") (v "1.0.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0h1m2r1zhhiyfgzhj03jrkd3a2hsfz1ckdyd1gw9f0w0fkl0a9lx")))

(define-public crate-prime_factorization-1.0.4 (c (n "prime_factorization") (v "1.0.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "030gnl61lzqcpq8smcix5h8amvh7kw93687pd73s77z4spa3rd31")))

