(define-module (crates-io pr im prime_tools) #:use-module (crates-io))

(define-public crate-prime_tools-0.1.0 (c (n "prime_tools") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "0h72apcafpj0spm3b091x4isjm5yvv5s6pzfz6y5ipkjmk3dd49d")))

(define-public crate-prime_tools-0.1.1 (c (n "prime_tools") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "1sap5hhqgdn9jlppzg1ki8imr6sm2r3scnj26px0vly5drnk759f")))

(define-public crate-prime_tools-0.1.3 (c (n "prime_tools") (v "0.1.3") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "1cghzaidydkhsmpxbi39k6rp2s31xf9ppjklh2vjnwxskq6y1zsn")))

(define-public crate-prime_tools-0.2.1 (c (n "prime_tools") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "0fx24bh7agk3s4inxxy9gh1gncgp4kw1yg0myf7dzcvz3i748yfp")))

(define-public crate-prime_tools-0.2.2 (c (n "prime_tools") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "137v9zfr1gl8wz5g3c1lzv7l81wmx686mkrskz2pmcvhdlcnwmad")))

(define-public crate-prime_tools-0.3.2 (c (n "prime_tools") (v "0.3.2") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "0kbza8xmfblngb3rvx5h70m847sggyglgqj2ngz4r2kbh0qa0n22")))

(define-public crate-prime_tools-0.3.3 (c (n "prime_tools") (v "0.3.3") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "0lihdsp55f0ccx9zb20g0x1mqrzsglspamzd43lb3m075b62qzvp")))

(define-public crate-prime_tools-0.3.4 (c (n "prime_tools") (v "0.3.4") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "libmath") (r "^0.1.4") (d #t) (k 0)))) (h "0fy4ssqhrjc5lcfvg9x4n5746aarxndx0s29lmr2mn040prznww3")))

