(define-module (crates-io pr im primality-test) #:use-module (crates-io))

(define-public crate-primality-test-0.1.0 (c (n "primality-test") (v "0.1.0") (h "03k881kzw9zav917g5ch0cg02bbs809rqidflqr8svyjjzlvnqg5")))

(define-public crate-primality-test-0.1.1 (c (n "primality-test") (v "0.1.1") (h "0lfhy1v83y1anhxmyfa2il0gg2gkh1pf8s3pdd4j9sv4qnbgkqrb")))

(define-public crate-primality-test-0.2.0 (c (n "primality-test") (v "0.2.0") (h "17x8fz7k2p8z9zhpja34429lrybdjhxh0zgv4b5jh2n0nnjkklws")))

(define-public crate-primality-test-0.2.1 (c (n "primality-test") (v "0.2.1") (h "1jadgh6drczwmcc98cn9n453zjcikzggy7j8bilcp7dag731rxfq")))

(define-public crate-primality-test-0.3.0 (c (n "primality-test") (v "0.3.0") (h "095fc2rjy8k2ja53kifx967c0kam68jwif6smcrqlm5rb2b9whwq")))

