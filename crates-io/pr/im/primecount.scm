(define-module (crates-io pr im primecount) #:use-module (crates-io))

(define-public crate-primecount-0.1.0 (c (n "primecount") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1dy3fran5dxdcvz8hfdq2r6jvy33256kxd0drjxhfg827cgb4mfx")))

(define-public crate-primecount-0.1.1 (c (n "primecount") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "09pzxryn0a67pv3n45aphhkbhjkzrxf9rp4n7gd6svz4ks5nb3nk")))

(define-public crate-primecount-0.1.2 (c (n "primecount") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0cwf7l6kkg6fakqp832l1crvf11dvgwqng0ahy2n37kr3sphdyz3")))

(define-public crate-primecount-0.2.0 (c (n "primecount") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1s27dhrrh8dx5kpp0l82kkd1g62fn1x269dfj3xrycrfvn4wa1qz")))

(define-public crate-primecount-0.2.1 (c (n "primecount") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1b5q6ms5l36b22b1rkwdq8dwblpw32gjm3gm6a3vc0mv2q15ps0c")))

