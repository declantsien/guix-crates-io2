(define-module (crates-io pr im prim) #:use-module (crates-io))

(define-public crate-prim-0.1.0 (c (n "prim") (v "0.1.0") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0q3lm99cxlssdp8650mljbdkhx4hczvi34na1dy91y3w29xfhrbs") (y #t)))

(define-public crate-prim-0.1.1 (c (n "prim") (v "0.1.1") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0yhb3vaywsb8ggsb3jl7kzilgqzpk3ysqhkwcxdzzjkdlvj3bmws") (y #t)))

(define-public crate-prim-0.1.2 (c (n "prim") (v "0.1.2") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0zjammz7lfv4mazwazmapahxphgpj1kpgavlblfbcgxg1cn18lwc")))

(define-public crate-prim-0.1.3 (c (n "prim") (v "0.1.3") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0zv9vv9bx9dj6s51q6bb3jq8ch37c0wdj4dg73dbjnad8zzmz245")))

(define-public crate-prim-0.1.4 (c (n "prim") (v "0.1.4") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "19dfzb81vpz9k6mk6a99v9vxwk185mybmmdkq8g9czz98hszq6j9")))

(define-public crate-prim-0.1.5 (c (n "prim") (v "0.1.5") (d (list (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)))) (h "0y8ynrm0xrmd28ii3brbis2ifrdr5zc5na155gq2j20v3g7ijj49")))

