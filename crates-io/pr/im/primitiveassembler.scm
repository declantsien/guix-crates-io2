(define-module (crates-io pr im primitiveassembler) #:use-module (crates-io))

(define-public crate-PrimitiveAssembler-0.1.0 (c (n "PrimitiveAssembler") (v "0.1.0") (h "0ngf00wf08ckb3prgg6h6cgd6ga0nh058pc3gza27pjd52hb53jh")))

(define-public crate-PrimitiveAssembler-0.1.1 (c (n "PrimitiveAssembler") (v "0.1.1") (h "1mcxpjpn7r7ij2lw2rxj7l138h0b8n1h0z4kzlyvgrcw7mwhp26j")))

(define-public crate-PrimitiveAssembler-0.1.2 (c (n "PrimitiveAssembler") (v "0.1.2") (h "0m76ch5kb9gv12s7ky958ww7cmfxqkb5lcqxffprswlgwna8rjiz")))

