(define-module (crates-io pr im prime-number-utils) #:use-module (crates-io))

(define-public crate-prime-number-utils-0.1.0 (c (n "prime-number-utils") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "num-iter") (r "^0.1.43") (o #t) (d #t) (k 0)))) (h "1ikns6kgvlm5h33xknm972c75m6isbvslpkpivb7jk5gz2aykaqq") (f (quote (("default" "num-bigint")))) (s 2) (e (quote (("num-bigint" "dep:num-bigint" "dep:num-iter"))))))

