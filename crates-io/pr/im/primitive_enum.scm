(define-module (crates-io pr im primitive_enum) #:use-module (crates-io))

(define-public crate-primitive_enum-1.0.0 (c (n "primitive_enum") (v "1.0.0") (h "1v8fdwsy7iygi4cpijsc1gk540zgp9125iw1hygn2cwadmfyci91")))

(define-public crate-primitive_enum-1.0.1 (c (n "primitive_enum") (v "1.0.1") (h "127sl66iqh89wpsd7pnr5795kg4f4qcsbgfic0miw39qypznvhwg")))

(define-public crate-primitive_enum-1.0.2 (c (n "primitive_enum") (v "1.0.2") (h "1jcqdfza614mryyz2dp3f3qg9hgksql0xfnyz820708fpjvhlb5c")))

(define-public crate-primitive_enum-1.1.0 (c (n "primitive_enum") (v "1.1.0") (h "1vc9nrs15jvf0gmydah6dal5ikidlbs93z1qzxlmlqcf2bbxzc0c")))

(define-public crate-primitive_enum-1.1.1 (c (n "primitive_enum") (v "1.1.1") (h "05xvzyy79a383bjzhbg12j3kpfajzp1pvgkhkkmgq71wf2rsyacc")))

(define-public crate-primitive_enum-1.2.0 (c (n "primitive_enum") (v "1.2.0") (h "0pzwl3jq5c5hfn1wa5a0dbnvnl5q97lvbcwp6vdqmhbmvx16jncv")))

