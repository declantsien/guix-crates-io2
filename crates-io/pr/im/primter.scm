(define-module (crates-io pr im primter) #:use-module (crates-io))

(define-public crate-primter-0.1.0 (c (n "primter") (v "0.1.0") (h "160cjy0yirxs7s4ljvnh2f0jq4qxw352yp1j37xhd9hqwkjiylxp")))

(define-public crate-primter-0.2.0 (c (n "primter") (v "0.2.0") (h "1i5vf2r0faizryzjn1c1lqg90dp39sqdjwzvpncsihcwxy2ks0w4")))

