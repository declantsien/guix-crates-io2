(define-module (crates-io pr im primaldimer_rs) #:use-module (crates-io))

(define-public crate-primaldimer_rs-0.1.1 (c (n "primaldimer_rs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "04c142qs4qhvw93i3m221amkd02pq0hjcd0xbp573h3qa65dy8jx")))

(define-public crate-primaldimer_rs-0.1.2 (c (n "primaldimer_rs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1f85s4n0w3fanh174mi0nw43z6lmkj11vp2jx2dymjxkg2ncx1lc")))

(define-public crate-primaldimer_rs-0.1.3 (c (n "primaldimer_rs") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1mqa8lqbzdbjdy1mlr7nvdy6npkg6d245sd3qrg8a496n8d0brj9")))

(define-public crate-primaldimer_rs-0.2.0 (c (n "primaldimer_rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1cfy29c2v2lssca7hkgmanvccxgb02rr35bx41d7b5cjn1xffjv0")))

