(define-module (crates-io pr im primesieve) #:use-module (crates-io))

(define-public crate-primesieve-0.2.0 (c (n "primesieve") (v "0.2.0") (h "13n2pdmm40xxr2vy40salncilf8s2gg77q7yjf4fa0jifjqandwq")))

(define-public crate-primesieve-0.2.1 (c (n "primesieve") (v "0.2.1") (h "0ligg926d8v4lxg4m9kbi7bakhxjsb5hzm0gypa6x4g438p7k6ja")))

