(define-module (crates-io pr im primefactor) #:use-module (crates-io))

(define-public crate-primefactor-0.1.0 (c (n "primefactor") (v "0.1.0") (h "0914g5dik2dlaq93mqaxgkw7x3vha1amhx4pi8dxzad2g1lild5v")))

(define-public crate-primefactor-0.1.1 (c (n "primefactor") (v "0.1.1") (h "1jrrjqj0sql3gv2vb2q5l4d5nmgx8xi47gr3spkc9n8iwx30amxf")))

(define-public crate-primefactor-0.1.2 (c (n "primefactor") (v "0.1.2") (h "0cxfgmdmyj8hbkl700di9jpwyxpg2lpn6gsdvwmm1an11xqgqlfj")))

