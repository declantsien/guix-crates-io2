(define-module (crates-io pr im primer-api) #:use-module (crates-io))

(define-public crate-primer-api-1.0.0 (c (n "primer-api") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jznampwwqvxm6mk01c6xqjvw55asfklkng5gjrwsyglm3qmx19c")))

(define-public crate-primer-api-2.0.0 (c (n "primer-api") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0dv37x1sghiy3iq4k39zlbq9kz5d8r4lk21r6w2rfddmjc1qaxhk")))

