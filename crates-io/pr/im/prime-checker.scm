(define-module (crates-io pr im prime-checker) #:use-module (crates-io))

(define-public crate-prime-checker-0.1.0 (c (n "prime-checker") (v "0.1.0") (h "0h7ngd1a8z7g6rg7ng38g3dgszw6nfwzr21i42r2b5ca895i7zig") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.1.2 (c (n "prime-checker") (v "0.1.2") (h "13a0h7hrdapikjid072xq0gxzwbz5qb1fyws6hwhrpsqg991fi3v") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.1.22 (c (n "prime-checker") (v "0.1.22") (h "1iispms6jzd5farmrw3cq2ngf5wqg5r4z76xg69rg2z0xah9p5q2") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.1.24 (c (n "prime-checker") (v "0.1.24") (h "1dycwjn2raq07dpcn58mbxc8f961ghx65isg9dwn2li81yvbrskj") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.1 (c (n "prime-checker") (v "0.2.1") (h "0p3322cc4lnmgh2y3g2ddaca1wdaampi7qyq04g4g68jq69id43i") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.12 (c (n "prime-checker") (v "0.2.12") (h "05a1xqakab9qh6ff6qbjrnxyxhhyrnlcdc0a88h0gh8nd0qni7fb") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.14 (c (n "prime-checker") (v "0.2.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zkfbnvbvpdyma5kpbxirvki5v86flb14qajyigykmm58z4na73w") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.17 (c (n "prime-checker") (v "0.2.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1awpzyvwllnq5d701dlgswk22vsij26rhaknqldkwpf1i5afhnw2") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.19 (c (n "prime-checker") (v "0.2.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01fl3pl9llpj3iwks8g5c6ilvabl655wsbdy4xqpmlsgc2lqdigh") (y #t) (r "1.69.0")))

(define-public crate-prime-checker-0.2.21 (c (n "prime-checker") (v "0.2.21") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0lz44hr9vf6pny04i15cajwbwm6rna1zackrj2ph8qvq8w76lxsy") (r "1.69.0")))

