(define-module (crates-io pr im primal-crossbeam) #:use-module (crates-io))

(define-public crate-primal-crossbeam-0.1.0 (c (n "primal-crossbeam") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "primal") (r "^0.2") (d #t) (k 0)))) (h "0acc1a0qwky4ij966275mfs3abcpq70vzr12bgyqkq2ip47inq0w")))

