(define-module (crates-io pr im prime-portraits) #:use-module (crates-io))

(define-public crate-prime-portraits-0.1.0 (c (n "prime-portraits") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1fymzqgvj8q02rzcsnm4ly07lnvbbrsvpmd3cnx2m1l2bkp1bysn")))

