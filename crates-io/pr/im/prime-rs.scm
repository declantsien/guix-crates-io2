(define-module (crates-io pr im prime-rs) #:use-module (crates-io))

(define-public crate-prime-rs-0.1.0 (c (n "prime-rs") (v "0.1.0") (h "19x02y47x26a6sbrrbi0b5z8g4dbgbs7h75s14zpwh7nxjw7fnjr")))

(define-public crate-prime-rs-0.1.1 (c (n "prime-rs") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "0rb3s1xmrf9lai8cah96l7brz1agl952f3vdsa3rpnag8ba8wpc6")))

