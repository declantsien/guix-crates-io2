(define-module (crates-io pr im primal) #:use-module (crates-io))

(define-public crate-primal-0.1.14 (c (n "primal") (v "0.1.14") (d (list (d (n "num") (r "~0") (d #t) (k 0)))) (h "076lgqg9l1243s6gh6rafzajsi4k69r3msj5z9i8n7563cd217rp") (f (quote (("unstable"))))))

(define-public crate-primal-0.2.0 (c (n "primal") (v "0.2.0") (d (list (d (n "primal-check") (r "^0.2") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2") (d #t) (k 0)))) (h "0prvkchxzyvgbfw99f53hbgj3jk2zmjhdgzhklf09ix8brwgcdyx") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.2.1 (c (n "primal") (v "0.2.1") (d (list (d (n "primal-check") (r "^0.2") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.2") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1qj1czfkhbyg0x7zrlxkwz5934kb0p6finy06pdlsnkr053kkskc") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.2.2 (c (n "primal") (v "0.2.2") (d (list (d (n "primal-check") (r "^0.2") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.2") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "11c33d8g72pm3ipdcfgwxn0fs0w9mpba5l0qy4wil26bw589z0hd") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.2.3 (c (n "primal") (v "0.2.3") (d (list (d (n "primal-check") (r "^0.2") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.2") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1900ilhxiid26krry95694qpi0lnd1k1l5sjahiv3bnyz9pbhc8f") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.3.0 (c (n "primal") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "primal-check") (r "^0.3") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.3") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.3") (d #t) (k 2)))) (h "1l5mwfw7ycmkhjckm554pixijf74qvabbcdg3y9jlbiam6pf1ybv") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.3.1 (c (n "primal") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal-check") (r "^0.3") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.3") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.3") (d #t) (k 2)))) (h "0pki1rvwdfky1fwm0xyynaz5rx3ajp3ppy2p3bi5yysjmhvgp6rq") (f (quote (("unstable" "primal-sieve/unstable"))))))

(define-public crate-primal-0.3.2 (c (n "primal") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal-check") (r "^0.3") (d #t) (k 0)) (d (n "primal-estimate") (r "^0.3") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3") (d #t) (k 0)) (d (n "primal-slowsieve") (r "^0.3") (d #t) (k 2)))) (h "0wghlarmfx8aqxgxr5642fp0jjy90qwads0qfrr63i4jr2cwqlsb") (f (quote (("unstable" "primal-sieve/unstable"))))))

