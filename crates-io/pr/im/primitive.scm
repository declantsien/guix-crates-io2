(define-module (crates-io pr im primitive) #:use-module (crates-io))

(define-public crate-primitive-0.1.0 (c (n "primitive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "image") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "loglog") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "1z6bd3mlcq1f903yhsmzh38j02064fr19jmjaf0n4sbqmb7hjb35")))

