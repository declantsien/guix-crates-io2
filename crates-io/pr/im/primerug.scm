(define-module (crates-io pr im primerug) #:use-module (crates-io))

(define-public crate-primerug-0.1.0 (c (n "primerug") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.19.0") (d #t) (k 0)))) (h "1ilykqnd41w4lzg7qry5ss6rd6yw2ka1bx8l5g8x6lgm6m0bwv6r")))

