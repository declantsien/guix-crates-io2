(define-module (crates-io pr im primitivemap) #:use-module (crates-io))

(define-public crate-primitivemap-0.1.0 (c (n "primitivemap") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 2)) (d (n "smallvec") (r "^0.6.1") (d #t) (k 0)))) (h "17q9pxjvv84mwnbq62kyvm97vr2f1jpai2bmlxfq8533kryj0w2d")))

