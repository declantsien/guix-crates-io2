(define-module (crates-io pr im primitive-from) #:use-module (crates-io))

(define-public crate-primitive-from-1.0.0 (c (n "primitive-from") (v "1.0.0") (h "046b18nsjch3cl48zlj14ajcya07sfv18zmpc5vg7k192hxhy9xj")))

(define-public crate-primitive-from-1.0.1 (c (n "primitive-from") (v "1.0.1") (h "0vijhanw77qh21cl5pccxs4xrn4pb2q7ic7vglqyr0c1qpjknvrj")))

(define-public crate-primitive-from-1.0.2 (c (n "primitive-from") (v "1.0.2") (h "0szxs9bqybv11m1wsakiw4wfzgljd0rmx1g5nbszw820mn77gzxr")))

