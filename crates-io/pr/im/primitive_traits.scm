(define-module (crates-io pr im primitive_traits) #:use-module (crates-io))

(define-public crate-primitive_traits-0.1.0 (c (n "primitive_traits") (v "0.1.0") (h "0g8254yv4zli63cyay6gxsch3j3xmbhak8s6lx5x53flrbcwh14g")))

(define-public crate-primitive_traits-0.2.0 (c (n "primitive_traits") (v "0.2.0") (h "1cqfha6i3ns8hzvf5xrv61imiz3xnhw0ziipcy36vak4gs7ww7zz")))

