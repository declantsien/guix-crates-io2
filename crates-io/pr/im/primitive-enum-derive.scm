(define-module (crates-io pr im primitive-enum-derive) #:use-module (crates-io))

(define-public crate-primitive-enum-derive-0.1.0 (c (n "primitive-enum-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1hwpa8a5hkk483rb1vy0bisspv46mn64c63a63asiw3hmza9f5")))

(define-public crate-primitive-enum-derive-0.2.0 (c (n "primitive-enum-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xrbxs6vizlcv9694zpyxx6wk7zrj4jwd49ccw8q9ksdhwg2x206")))

(define-public crate-primitive-enum-derive-0.3.0 (c (n "primitive-enum-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19sffpr3k5ivq0am3wacpkd7j2cwyfps2wmrbmkb3875pspr05b3")))

(define-public crate-primitive-enum-derive-0.3.1 (c (n "primitive-enum-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0agldmn8dhykvlnv1wjy5x1vvpds27dvwfdvbmb3xqafzqdribgz")))

