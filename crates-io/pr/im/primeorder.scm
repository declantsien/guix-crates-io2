(define-module (crates-io pr im primeorder) #:use-module (crates-io))

(define-public crate-primeorder-0.0.0 (c (n "primeorder") (v "0.0.0") (h "041p1j738g5iimnma6i2jamms0cp6ry7dqkswvphjs0sf76d0vcs")))

(define-public crate-primeorder-0.0.1 (c (n "primeorder") (v "0.0.1") (d (list (d (n "elliptic-curve") (r "^0.12.3") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.1") (o #t) (k 0)))) (h "0k928wymwcflmjkawy1anxzgwkhzvfdcxbypb5ff0yvdxzswahsp") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect")))) (r "1.57")))

(define-public crate-primeorder-0.0.2 (c (n "primeorder") (v "0.0.2") (d (list (d (n "elliptic-curve") (r "^0.12.3") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.1") (o #t) (k 0)))) (h "0xl580wpxi0rwi50r89s81h4m91hjjbkzfmiynxra0427fhzms98") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect")))) (r "1.57")))

(define-public crate-primeorder-0.12.0 (c (n "primeorder") (v "0.12.0") (d (list (d (n "elliptic-curve") (r "^0.12.3") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.1") (o #t) (k 0)))) (h "1xqar4197wkwyncwsra2zm8vwv1k96kwkc9q6a0iqjrn785y3ds9") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect")))) (r "1.60")))

(define-public crate-primeorder-0.12.1 (c (n "primeorder") (v "0.12.1") (d (list (d (n "elliptic-curve") (r "^0.12.3") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.1") (o #t) (k 0)))) (h "1cn5lh5pb1g7x9l0cq888qp6im36bg95pkqlyji6bfix3c9zfm0b") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect")))) (r "1.60")))

(define-public crate-primeorder-0.13.0 (c (n "primeorder") (v "0.13.0") (d (list (d (n "elliptic-curve") (r "^0.13") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "1l73l8qbb1d8ar9y4lbg8nv99aiczal3x0v9z9h01h9i136gs4vn") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect")))) (r "1.65")))

(define-public crate-primeorder-0.13.1 (c (n "primeorder") (v "0.13.1") (d (list (d (n "elliptic-curve") (r "^0.13") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "1ddz0d0fzzcpdlsj6c6989va8ykf702g3zmf7dszfa0y6rski3fg") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev")))) (r "1.65")))

(define-public crate-primeorder-0.13.2 (c (n "primeorder") (v "0.13.2") (d (list (d (n "elliptic-curve") (r "^0.13") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "1qqyvzkfx6g30ibc74n3fggkr6rrdi27ifbrq7yfxihf5kwcwbrw") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev")))) (r "1.65")))

(define-public crate-primeorder-0.13.3 (c (n "primeorder") (v "0.13.3") (d (list (d (n "elliptic-curve") (r "^0.13") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "1d7fqxa8vrwcj43ckbgb19rx7z8pkkz35sw3jkcqndjn7gnykny7") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev")))) (r "1.65")))

(define-public crate-primeorder-0.13.4 (c (n "primeorder") (v "0.13.4") (d (list (d (n "elliptic-curve") (r "^0.13.7") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "0ny5p2w7jcpyv57b30y7ndyagwkbmmkbbzp77vzjzdg9gh6mwxk8") (f (quote (("std" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev")))) (y #t) (r "1.65")))

(define-public crate-primeorder-0.13.5 (c (n "primeorder") (v "0.13.5") (d (list (d (n "elliptic-curve") (r "^0.13.7") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "0w2zjw1islgx7i772wh9dkja53d8w62xlscfabdz3fa37gpp4cz3") (f (quote (("std" "alloc" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev") ("alloc" "elliptic-curve/alloc")))) (y #t) (r "1.65")))

(define-public crate-primeorder-0.13.6 (c (n "primeorder") (v "0.13.6") (d (list (d (n "elliptic-curve") (r "^0.13.7") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "1rp16710mxksagcjnxqjjq9r9wf5vf72fs8wxffnvhb6i6hiqgim") (f (quote (("std" "alloc" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev") ("alloc" "elliptic-curve/alloc")))) (r "1.65")))

(define-public crate-primeorder-0.14.0-pre.0 (c (n "primeorder") (v "0.14.0-pre.0") (d (list (d (n "elliptic-curve") (r "=0.14.0-pre.3") (f (quote ("arithmetic" "sec1"))) (k 0)) (d (n "serdect") (r "^0.2") (o #t) (k 0)))) (h "19jg8gwps8043bxi9l1hya2xj9bs6hmhl94v25vxvrilci804rw9") (f (quote (("std" "alloc" "elliptic-curve/std") ("serde" "elliptic-curve/serde" "serdect") ("dev") ("alloc" "elliptic-curve/alloc")))) (r "1.73")))

