(define-module (crates-io pr et pretok) #:use-module (crates-io))

(define-public crate-pretok-0.1.0 (c (n "pretok") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "strcursor") (r "^0.2.5") (d #t) (k 0)))) (h "02dfwccqq7cp17r1c7lylbwif4148jiv9zx78v8lmsh158zwz9ic")))

