(define-module (crates-io pr et pretty-hex) #:use-module (crates-io))

(define-public crate-pretty-hex-0.1.0 (c (n "pretty-hex") (v "0.1.0") (h "1xxq2vdf45kykzczbpilq6v2iny1qdfinylgi0yvncdplfi2k68i")))

(define-public crate-pretty-hex-0.1.1 (c (n "pretty-hex") (v "0.1.1") (h "135x46pijq239byn1j37r1ndiqdawxall6bcdb29sybk7v2br4dy") (f (quote (("default") ("alloc"))))))

(define-public crate-pretty-hex-0.2.0 (c (n "pretty-hex") (v "0.2.0") (h "06v5rag0y3fp8ppnlii9cl17jsr7ffdzxv6j7hcikv7kg5zs59n8") (f (quote (("default") ("alloc"))))))

(define-public crate-pretty-hex-0.2.1 (c (n "pretty-hex") (v "0.2.1") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 2)))) (h "0c91f9sdwmn3mz2d414dp1xk4iw0k1nsif7lyqvhklzh57arjp5w") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pretty-hex-0.3.0 (c (n "pretty-hex") (v "0.3.0") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 2)))) (h "1mf8xvlfri4impj2paj4azx7hxh7l0i38cjyib1hiikwvlqhiyn6") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pretty-hex-0.4.0 (c (n "pretty-hex") (v "0.4.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "0pykw8bmli359slvhs4xjavv0cd2pw9vmfpynhsjxmipxmlbkii3") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pretty-hex-0.4.1 (c (n "pretty-hex") (v "0.4.1") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 2)))) (h "0m0j8pqmh6gq1mq7yzp12z0ix159fw0di5lhiwv2y1j0m3j3xj5v") (f (quote (("default" "alloc") ("alloc"))))))

