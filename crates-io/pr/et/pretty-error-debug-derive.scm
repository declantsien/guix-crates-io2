(define-module (crates-io pr et pretty-error-debug-derive) #:use-module (crates-io))

(define-public crate-pretty-error-debug-derive-0.1.0 (c (n "pretty-error-debug-derive") (v "0.1.0") (d (list (d (n "find-crate") (r "^0.6.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)) (d (n "toml") (r "=0.5.2") (d #t) (k 0)))) (h "1rlqdmb8jx602v0h4a58c9czk1v7a05brrr0cnb2vm2aai3wvgkw")))

(define-public crate-pretty-error-debug-derive-0.1.1 (c (n "pretty-error-debug-derive") (v "0.1.1") (d (list (d (n "find-crate") (r "^0.6.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "07mlybkjjwijnqc06ip9lzjy5w9fqca0rn1ijj8vpw91xjxll5dd")))

(define-public crate-pretty-error-debug-derive-0.2.0 (c (n "pretty-error-debug-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "07mhn2lpwbsgiw94rbh7jdlyhaddgnfjlrf81a4gir6633spp1wh") (r "1.60")))

(define-public crate-pretty-error-debug-derive-0.3.0 (c (n "pretty-error-debug-derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "14gpj33rlqx7f19aqmpl6ik2r4yvvqi3bm5rqsm8n3li9iwli2jj") (r "1.56")))

