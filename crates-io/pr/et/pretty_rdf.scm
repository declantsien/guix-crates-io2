(define-module (crates-io pr et pretty_rdf) #:use-module (crates-io))

(define-public crate-pretty_rdf-0.1.0 (c (n "pretty_rdf") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.21") (d #t) (k 0)) (d (n "rio_api") (r "^0.5.2") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.5.2") (d #t) (k 2)))) (h "0wcskmwacxvp574q753mzpj4xzvy8fxpmx9rj7dpnd79n3xvxsch")))

(define-public crate-pretty_rdf-0.1.1 (c (n "pretty_rdf") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.21") (d #t) (k 0)) (d (n "rio_api") (r "^0.5.2") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.5.2") (d #t) (k 2)))) (h "02bwi74yw5wl810pmvkqndgacdqf27x9b6icbwz6wndc1bwfrcy0")))

(define-public crate-pretty_rdf-0.1.2 (c (n "pretty_rdf") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.21") (d #t) (k 0)) (d (n "rio_api") (r "^0.6.1") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.6.1") (d #t) (k 2)))) (h "1w6r6wayb0fihp5jpzd2mdrrsd6s4iidddgrv8jlhdnkq03bwi1z")))

(define-public crate-pretty_rdf-0.2.0 (c (n "pretty_rdf") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.21") (d #t) (k 0)) (d (n "rio_api") (r "^0.7.1") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.7.1") (d #t) (k 2)))) (h "02djbhzm9w1plwc1c239jb934xr0w8x1szc4jmkn3whmzw7hb7b1")))

(define-public crate-pretty_rdf-0.3.0 (c (n "pretty_rdf") (v "0.3.0") (d (list (d (n "indexmap") (r "1.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "1.*") (d #t) (k 2)) (d (n "quick-xml") (r "0.*") (d #t) (k 0)) (d (n "rio_api") (r "0.*") (d #t) (k 0)) (d (n "rio_turtle") (r "0.*") (d #t) (k 2)))) (h "11fbfkzxlgvdc905ch9il1xp8avkqx4f45m7aiwv5k270lcq5wlv")))

(define-public crate-pretty_rdf-0.4.0 (c (n "pretty_rdf") (v "0.4.0") (d (list (d (n "indexmap") (r "1.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "1.*") (d #t) (k 2)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "rio_api") (r "0.*") (d #t) (k 0)) (d (n "rio_turtle") (r "0.*") (d #t) (k 2)))) (h "0fj62plircdi85da3bkbwj2xhfpnpyxxsc7c3s7127gbwm8hjwwy")))

(define-public crate-pretty_rdf-0.5.0 (c (n "pretty_rdf") (v "0.5.0") (d (list (d (n "indexmap") (r "1.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "1.*") (d #t) (k 2)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "rio_api") (r "^0.7.1") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.7.1") (d #t) (k 2)))) (h "09f6jg48l8rw5dc5n4mvmgbm97rn3wi763mjcazacr0w6lmsciqz")))

(define-public crate-pretty_rdf-0.6.0 (c (n "pretty_rdf") (v "0.6.0") (d (list (d (n "indexmap") (r "1.*") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "1.*") (d #t) (k 2)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "rio_api") (r "^0.8.3") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.8.3") (d #t) (k 2)) (d (n "rio_xml") (r "^0.8.3") (d #t) (k 2)))) (h "05i3jkiiwzbqzxk0s8x6bncfr9ydyg7n7gs70j5fygc3y2ihvzcm")))

