(define-module (crates-io pr et prettytable) #:use-module (crates-io))

(define-public crate-prettytable-0.10.0 (c (n "prettytable") (v "0.10.0") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^1.0") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "10g7081qxpzvv17jvihf4mx56cb8z7y3k6fkh8s9lz5ps4h0aj26") (f (quote (("win_crlf") ("evcxr") ("default" "win_crlf" "csv"))))))

