(define-module (crates-io pr et pretty_debug) #:use-module (crates-io))

(define-public crate-pretty_debug-0.1.0 (c (n "pretty_debug") (v "0.1.0") (h "01c6kp4f92lrp06mdqknkqqr1z89n4sfzak09l9qr6vvc6pic3q3")))

(define-public crate-pretty_debug-0.1.1 (c (n "pretty_debug") (v "0.1.1") (h "1n6nyjhf4yzi1myjsbwh3z846x0r05ijzzbxwi0rkd187zkrdwp4")))

(define-public crate-pretty_debug-0.1.2 (c (n "pretty_debug") (v "0.1.2") (h "1r94iwh3xqv35ziigni0b8ag8iybz9a3yrc8lphpzaayl7aj05l8")))

(define-public crate-pretty_debug-0.1.3 (c (n "pretty_debug") (v "0.1.3") (h "0gd0hjhrm4gbqcc47c4kwhafsbdg78yh4x608b818lp895rz1sxf")))

(define-public crate-pretty_debug-0.1.4 (c (n "pretty_debug") (v "0.1.4") (h "1h9gj3dbi5k67cp47za5a2hxdrs9y40w06qh3wj99agngm0kh52x")))

