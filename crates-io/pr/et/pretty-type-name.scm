(define-module (crates-io pr et pretty-type-name) #:use-module (crates-io))

(define-public crate-pretty-type-name-0.1.0 (c (n "pretty-type-name") (v "0.1.0") (h "0gvixqx81f0zzz352wvywxhylmdjj6i5hjwfqjm072w0j5dax6ia")))

(define-public crate-pretty-type-name-1.0.0 (c (n "pretty-type-name") (v "1.0.0") (h "03ch6l0vxxd286cml65br5x5lf9a56mvv5j82n8v8k7v3h85v0d8")))

(define-public crate-pretty-type-name-1.0.1 (c (n "pretty-type-name") (v "1.0.1") (h "0jxx73mqhk49qd7b3mk9m7gs85710ri61hw56qafcllvy7d3rxzh")))

