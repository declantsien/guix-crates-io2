(define-module (crates-io pr et pretty_date) #:use-module (crates-io))

(define-public crate-pretty_date-0.1.0 (c (n "pretty_date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hsgsjil79gswp0r1wfzn37wkgbd33kyq91wng8cjr1xy0wf1l5l")))

(define-public crate-pretty_date-0.1.1 (c (n "pretty_date") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05yh5azs8nd5h19kfmjbnl1m0c6cwqq5wic2gm5q068fx2rxyiag")))

(define-public crate-pretty_date-0.1.2 (c (n "pretty_date") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0agbasbd07vn4313sj0bq409g05lcw2chy8li153kj0n22xismrf")))

