(define-module (crates-io pr et pretty-hash) #:use-module (crates-io))

(define-public crate-pretty-hash-0.1.0 (c (n "pretty-hash") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1a22h2gyz8xqw3cc499y2rr0b4350i6cbqsd6aa10zjs9iypz585")))

(define-public crate-pretty-hash-0.1.1 (c (n "pretty-hash") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0dhq12jjsbxswhip9nb1wzwkqygkil8hb9wx36i88hn2f2x266vh")))

(define-public crate-pretty-hash-0.1.2 (c (n "pretty-hash") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0bcajdwfjgvz28f90m1k6skncf9dvhc1x84xn580w5vj2d7266pp")))

(define-public crate-pretty-hash-0.2.0 (c (n "pretty-hash") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0ww68prm36h0rj4c6ciaw0hc4zh19rmdslm1y67isf3x2kis31md")))

(define-public crate-pretty-hash-0.4.0 (c (n "pretty-hash") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0c678hbsi2895ldl2cdgv6gbfwnjlvp1hp9klm8096dbskrymz8p")))

(define-public crate-pretty-hash-0.4.1 (c (n "pretty-hash") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "00jn7dj0j3lj229fjsr6hi1nh865zzapw4qdd9741jr7icagz1yk")))

