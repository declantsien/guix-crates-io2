(define-module (crates-io pr et pretty_ini) #:use-module (crates-io))

(define-public crate-pretty_ini-0.1.0 (c (n "pretty_ini") (v "0.1.0") (h "1bnh8rsvyvs97rjwdp57c57rf1snw18858s5mlc72sbx9r0v0g8w")))

(define-public crate-pretty_ini-0.1.1 (c (n "pretty_ini") (v "0.1.1") (h "14kfxgv80n5cq20cgd9x355ni02rbnjxsmgczd7v9pk1sdjj3rrb")))

(define-public crate-pretty_ini-0.1.2 (c (n "pretty_ini") (v "0.1.2") (h "147p0g89nx9xasmf22bffs8slf5fn6ravdf3wxhvcv31scb2l2xj")))

(define-public crate-pretty_ini-0.1.3 (c (n "pretty_ini") (v "0.1.3") (h "1qhd137f99l1g84xl3j40wf2rzxf3mw8ryddhspgxsy7pd64x9v1")))

(define-public crate-pretty_ini-0.1.4 (c (n "pretty_ini") (v "0.1.4") (h "03fqg1m83cvxly529zj7k542bl9jy3gxzs8zdl9sfc1j4ylr57jj")))

(define-public crate-pretty_ini-0.1.5 (c (n "pretty_ini") (v "0.1.5") (h "0a7qfxvcqzlgr5vzl2ci239ldfs1ihp6h15jmcxx0gm0m9v70laa")))

(define-public crate-pretty_ini-0.1.6 (c (n "pretty_ini") (v "0.1.6") (h "14fchxsg532yh706lci5svdh7v0f3xmn4xlkzbdl1zk6yi3ard4c")))

(define-public crate-pretty_ini-0.1.7 (c (n "pretty_ini") (v "0.1.7") (h "097spi0n3k1vr2hvi4zxfpz2lq7lzadiyyjsb3f6v6snb4hj7qp4")))

