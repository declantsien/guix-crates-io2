(define-module (crates-io pr et pretty_dbg) #:use-module (crates-io))

(define-public crate-pretty_dbg-1.0.47 (c (n "pretty_dbg") (v "1.0.47") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "0lh0i8bdi0v1hbka209r20n3szci0kg68z9brqzx4plix6qg8qqp")))

(define-public crate-pretty_dbg-1.0.49 (c (n "pretty_dbg") (v "1.0.49") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1q8i3gc2pdxynrqvmwzyzj6ynrx4mjzc0sbn529sps1frxnwrjc5")))

(define-public crate-pretty_dbg-1.0.53 (c (n "pretty_dbg") (v "1.0.53") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "05yhgykpbcrx8ikwhylkvaq4mcx6y1m5dcxpzmvfl00kyc6gaypi")))

(define-public crate-pretty_dbg-1.0.54 (c (n "pretty_dbg") (v "1.0.54") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "0l1snzf7livjca54qrq8lb6dgqzmgznpk1fmi5z8p5rcdx2dim10")))

