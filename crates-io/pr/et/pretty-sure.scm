(define-module (crates-io pr et pretty-sure) #:use-module (crates-io))

(define-public crate-pretty-sure-0.1.0 (c (n "pretty-sure") (v "0.1.0") (h "1ca85i698axzifiya617q6rvp27zywn2592win5adcj62bpbl29a")))

(define-public crate-pretty-sure-0.1.1 (c (n "pretty-sure") (v "0.1.1") (h "0yc7mzan1k49qcqzh4nl3grpq2navjxnq5x78g0jj8xpn43cmlsg")))

