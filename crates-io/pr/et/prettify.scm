(define-module (crates-io pr et prettify) #:use-module (crates-io))

(define-public crate-prettify-0.1.0 (c (n "prettify") (v "0.1.0") (h "07p1pq26i5hs3gsibl5pvaywblvl2ks67d7f353x2xw2x2zwa4c1")))

(define-public crate-prettify-0.2.0 (c (n "prettify") (v "0.2.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11h6110yf19xassdi43824d43svq5a5hhmzxdmhqk0sf9bgmz1xg")))

(define-public crate-prettify-0.2.1 (c (n "prettify") (v "0.2.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lnshqw875847mwqn50wximjbcgpw227cpq7bdc1sjgkjjr75947")))

(define-public crate-prettify-0.3.0 (c (n "prettify") (v "0.3.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13zql5jr9d65dlngvia594ibxngg2c6qf1kqk7y4iyzz6rcf47p0")))

