(define-module (crates-io pr et pretree) #:use-module (crates-io))

(define-public crate-pretree-1.0.0 (c (n "pretree") (v "1.0.0") (h "0z4pci10la27iab04gqwydrl27jrbcij5bs3ss4f130r0aryg5w0")))

(define-public crate-pretree-1.0.1 (c (n "pretree") (v "1.0.1") (h "1fagn1bx59nkiy291qbw58gi514ybp0di82wcbxk9m9dbdk3sgdv")))

(define-public crate-pretree-1.0.2 (c (n "pretree") (v "1.0.2") (h "178qn9h4in7k2l5288z8cis6azh3yhcbkpjaxvk4d50igr4lsfwd")))

