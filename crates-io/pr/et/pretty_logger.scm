(define-module (crates-io pr et pretty_logger) #:use-module (crates-io))

(define-public crate-pretty_logger-0.1.0 (c (n "pretty_logger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0lq0m84v0k0w03il99rcc77a1f6fha20ngc053x28bgr0i1jp0i2")))

(define-public crate-pretty_logger-0.1.1 (c (n "pretty_logger") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0h1hgcxif3b1p9w1ar41a9d4m0xg59ka4r7md97fx549llffpk82")))

(define-public crate-pretty_logger-0.1.2 (c (n "pretty_logger") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0yl7jzj4400031y7wl4p676wjhzabgq5ah0wcbxbap126whkimjy")))

(define-public crate-pretty_logger-0.1.3 (c (n "pretty_logger") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0yra8b07bk0fci9kp7zmvjva7ynhax8rsn4g4mckg55k7fs7f3bw")))

(define-public crate-pretty_logger-0.1.4 (c (n "pretty_logger") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "15w61yi48cyf3v1xc0lfs87c7yjlvhd1zr90835889v1sm13kdcb")))

(define-public crate-pretty_logger-0.1.5 (c (n "pretty_logger") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0yibg3f1zin1x65j3v260fv34d8r3qmnhhj57d1z69q0dvkvsqmv")))

(define-public crate-pretty_logger-0.1.6 (c (n "pretty_logger") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0jsqmhfnsqf24qiz234nm8hw7p6kbd7z4bb8f04nzwmdcygpk4iz")))

(define-public crate-pretty_logger-0.1.7 (c (n "pretty_logger") (v "0.1.7") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0hsz1lfxvj0a1k56gyr74cp820dbwvfvz2xxl2jrkdakvmvbakf8")))

(define-public crate-pretty_logger-0.1.8 (c (n "pretty_logger") (v "0.1.8") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0asr9ca2ikmnvp4c6w00286y630zg69wm9y66ikifsfq71gcjny6")))

