(define-module (crates-io pr et prettyjson) #:use-module (crates-io))

(define-public crate-prettyjson-0.1.1 (c (n "prettyjson") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "rust_util") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)))) (h "1k41psa7618a6fsa65k5npb0cymc2dpib714if0y805lypmcprdp")))

(define-public crate-prettyjson-0.1.2 (c (n "prettyjson") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "rust_util") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)))) (h "0sc3bw4mvkcsv373kgm6hvz9vzpijyafg3nz0pbbi6av2l606yap")))

