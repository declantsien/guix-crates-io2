(define-module (crates-io pr et prettier-print) #:use-module (crates-io))

(define-public crate-prettier-print-0.1.0 (c (n "prettier-print") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "11j596dgpipb389sf7qg93rbdzgh1ka504nigzqvb60zkwhrpipy")))

(define-public crate-prettier-print-0.1.1 (c (n "prettier-print") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "104dpcff6vd7x3p3296wraxf9qh8bc435jwg6p41vyl3sqvf5j0h")))

(define-public crate-prettier-print-0.1.2 (c (n "prettier-print") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "1lm2vwfk8xzzvkcjnm322m0jksdrasaxj5zyj5g5vzp3ah83dfj7")))

