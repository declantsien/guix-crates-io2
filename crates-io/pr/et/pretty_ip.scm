(define-module (crates-io pr et pretty_ip) #:use-module (crates-io))

(define-public crate-pretty_ip-0.1.0 (c (n "pretty_ip") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "18253qmh2fmxgqvfwyzxakvv9j3mrgmja0z9svp07adjzzkri8p0")))

(define-public crate-pretty_ip-0.1.1 (c (n "pretty_ip") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0lpbp4w0spym6v2xah4wx32f8xhq69b5vhplybrspj2ccsisdgsg")))

