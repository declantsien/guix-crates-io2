(define-module (crates-io pr et prettyish-html) #:use-module (crates-io))

(define-public crate-prettyish-html-0.1.0 (c (n "prettyish-html") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0aam02m22nnagyj2amz7jrfarh9lq52r64n1inhd3ayrl604b9s0")))

(define-public crate-prettyish-html-0.1.1 (c (n "prettyish-html") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1x0v9zh6p0ahppzfq8lb4lwmdmrikc6fizkx62vfpmh0mrikdkgi")))

