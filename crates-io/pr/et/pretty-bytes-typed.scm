(define-module (crates-io pr et pretty-bytes-typed) #:use-module (crates-io))

(define-public crate-pretty-bytes-typed-0.1.0 (c (n "pretty-bytes-typed") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.182") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "14763nsii3cksbkg2x8knabpmkssswdq630xbiaypzivv98fpny9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-pretty-bytes-typed-0.1.1 (c (n "pretty-bytes-typed") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.182") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0k2f3sv9w7gphywqbsyb9hapiq3q5kjg8x4fnpfrrqpv2pzdxhnk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-pretty-bytes-typed-0.2.0 (c (n "pretty-bytes-typed") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.182") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0khsb9m2c2n3azq2cgcbmahssg88j3hkln4ga00sh20x0szsqgb2") (s 2) (e (quote (("serde" "dep:serde"))))))

