(define-module (crates-io pr et prettify_pinyin) #:use-module (crates-io))

(define-public crate-prettify_pinyin-0.1.0 (c (n "prettify_pinyin") (v "0.1.0") (h "024cja9bph9zchs7903dwpj37qkb9cmga1vnpzfj3a3c6rg6dg1q")))

(define-public crate-prettify_pinyin-0.1.1 (c (n "prettify_pinyin") (v "0.1.1") (h "1fxkqddqdfmwr6vv4bb18rqb97cxk2kknwh9dx242vzbxnl2w96s")))

(define-public crate-prettify_pinyin-1.0.0 (c (n "prettify_pinyin") (v "1.0.0") (h "0fimd4yldzz320fqgxnkfih1q9l8gmn81vlia6vb7hqxg8jpwlh5")))

(define-public crate-prettify_pinyin-1.0.1 (c (n "prettify_pinyin") (v "1.0.1") (h "0pw83xfgk53mh26fg5yml1wffal3cnjl154i9crkgqnv9yz3dxv3")))

(define-public crate-prettify_pinyin-1.1.0 (c (n "prettify_pinyin") (v "1.1.0") (h "0jdrbp60nrv887bhqmc62i715ydfk85mknlv4awsq80k9xjp1kd1")))

(define-public crate-prettify_pinyin-1.1.1 (c (n "prettify_pinyin") (v "1.1.1") (h "121b87ya2jjssx022i9pcg71lmpwm87xjsyipwlma2yja39m8ipm")))

(define-public crate-prettify_pinyin-1.1.2 (c (n "prettify_pinyin") (v "1.1.2") (h "0jilaagg110ynric6nw3srbckgd3wn7g2aimfzjsfgfcc4vpxbwn")))

(define-public crate-prettify_pinyin-1.1.3 (c (n "prettify_pinyin") (v "1.1.3") (h "09h13gd78wkck97c2vrs8rwikfm4f29gdrn1q2dy0ljcy5y02iq8")))

(define-public crate-prettify_pinyin-1.2.0 (c (n "prettify_pinyin") (v "1.2.0") (h "1incd8rj90b5kzxw0lpbqp4dyr02z7ghs6wbs6zr7sh79bnpy2hg")))

(define-public crate-prettify_pinyin-2.0.0 (c (n "prettify_pinyin") (v "2.0.0") (h "1m92529ykmbml7mzdzi5pn08fkbwbni0yjiyswhhh8lwxbals7sk")))

