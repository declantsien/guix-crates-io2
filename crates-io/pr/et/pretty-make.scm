(define-module (crates-io pr et pretty-make) #:use-module (crates-io))

(define-public crate-pretty-make-0.1.0 (c (n "pretty-make") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0bswpgyy21xanf3n3k07dv90m65456aypd3456rsmc0v35281av4")))

(define-public crate-pretty-make-0.2.0 (c (n "pretty-make") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1m9pbh2f9k4iyqh7vrymvz8vsdqnhdpiycm70sscpg59rq1vmaxk")))

(define-public crate-pretty-make-0.3.0 (c (n "pretty-make") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1xprjswgwqp7xzm7wmqzxql1yvvl4x1b16p7sa3sjllag55c6znm")))

(define-public crate-pretty-make-0.4.0 (c (n "pretty-make") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1wcib2qw82y2qdp05aj7967fr429c8wgpqwskafipp925ci8k8rd")))

(define-public crate-pretty-make-0.4.1 (c (n "pretty-make") (v "0.4.1") (d (list (d (n "clap") (r ">=3.0.0-beta.1, <4.0.0") (d #t) (k 0)) (d (n "colored") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "colors-transform") (r ">=0.2.11, <0.3.0") (d #t) (k 0)) (d (n "pest") (r ">=2.1.3, <3.0.0") (d #t) (k 0)) (d (n "pest_derive") (r ">=2.1.0, <3.0.0") (d #t) (k 0)))) (h "06d18y89xv2n01r0wxapjvybfpb34n4jh2r1qq3r04rbx3v5kgn9")))

(define-public crate-pretty-make-0.5.0 (c (n "pretty-make") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0gxz8x13zjmbz9vhb2h3pg5g9vaxdkxw7ppa1y2kr7f6qa2yivmw")))

(define-public crate-pretty-make-0.5.1 (c (n "pretty-make") (v "0.5.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0nrrwjjnrz31g0rwi384ynfya34jh3qdl89jrcz0jsip49l414s9")))

(define-public crate-pretty-make-0.5.2 (c (n "pretty-make") (v "0.5.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1dd2ikd3nlaqi0kngs5xbyb234y63zs9r14hhldm6xz8k70zng05")))

(define-public crate-pretty-make-0.5.3 (c (n "pretty-make") (v "0.5.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0v0icd2455kapm1f5ybi2qc8lb13dpci5kc1jirvzgxj6ah6p7hz")))

(define-public crate-pretty-make-0.5.4 (c (n "pretty-make") (v "0.5.4") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1aq1kijs09dnvbix1carbahi5pyh1rmaj7ac45vbs5gswvx0hy00")))

(define-public crate-pretty-make-0.6.0 (c (n "pretty-make") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0zg75la4ypdc15c82c426lkmd3jzn1izrlrhfyj1jw215hr127dg")))

(define-public crate-pretty-make-0.6.1 (c (n "pretty-make") (v "0.6.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1jn7bvx15vvfdz91maf7kx0wrrsfqw44wk0g1irpk4xjcx00r8fc")))

(define-public crate-pretty-make-0.6.2 (c (n "pretty-make") (v "0.6.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1qbm8n0ydl2vb5z1mdgf06ikzzjfl90fsnkb9j0x3anfpy8rhi4x")))

(define-public crate-pretty-make-0.6.3 (c (n "pretty-make") (v "0.6.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "08yygmznaj68ffjz2fz5mxa8fgwydy866b8bb4azdsrpmwvinjaw")))

