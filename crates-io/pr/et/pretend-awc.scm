(define-module (crates-io pr et pretend-awc) #:use-module (crates-io))

(define-public crate-pretend-awc-0.2.0 (c (n "pretend-awc") (v "0.2.0") (d (list (d (n "awc") (r "^2.0") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)))) (h "1hrsym8326533wfcaqzsmyr53ii4rw8fbh7rywhabywmmbw7512v")))

(define-public crate-pretend-awc-0.2.1 (c (n "pretend-awc") (v "0.2.1") (d (list (d (n "awc") (r "^2.0") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)))) (h "0w4sb2w7fx837nbnarw5h6863s812b0q2bf7bf6cgfq91dl2wx0z")))

(define-public crate-pretend-awc-0.2.2 (c (n "pretend-awc") (v "0.2.2") (d (list (d (n "awc") (r "^2.0") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (f (quote ("local-error"))) (d #t) (k 0)))) (h "0ikvjd9gmihh2cbil792x78mv47bwc9gh25pb8fz9if5lz8a84q0")))

(define-public crate-pretend-awc-0.3.0 (c (n "pretend-awc") (v "0.3.0") (d (list (d (n "awc") (r "^2") (k 0)) (d (n "pretend") (r "^0.3.0") (f (quote ("local-error"))) (d #t) (k 0)))) (h "024xvdvlzczp0sjczis0fajqhrkzy3ps8dc9m94vgr93c87gmypl") (f (quote (("default" "awc/default"))))))

(define-public crate-pretend-awc-0.4.0 (c (n "pretend-awc") (v "0.4.0") (d (list (d (n "awc") (r "^2") (k 0)) (d (n "pretend") (r "^0.4.0") (f (quote ("local-error"))) (d #t) (k 0)))) (h "1a3risz0qrvwp9h5jrn3m2mn04rqiagzaaxb4hml81bhz705abr1") (f (quote (("default" "awc/default"))))))

