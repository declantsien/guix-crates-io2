(define-module (crates-io pr et pretty-lint) #:use-module (crates-io))

(define-public crate-pretty-lint-0.1.0 (c (n "pretty-lint") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0488dqaai9kcgaivaw00v90cbgrw92wvimzd0q15y3jlmbyhfgvj")))

(define-public crate-pretty-lint-0.1.1 (c (n "pretty-lint") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "16qri2lgm9dqqh5dj2a00i813xm7yc89i5hnl7da24dpanq8x8ip")))

