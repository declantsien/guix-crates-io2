(define-module (crates-io pr et pretty-readme) #:use-module (crates-io))

(define-public crate-pretty-readme-0.1.0 (c (n "pretty-readme") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "13snswrpr04bxv2j4mywpclc0g1pxw0b9pihc1zf31sr6cvnl7r4")))

