(define-module (crates-io pr et pretty-bytes) #:use-module (crates-io))

(define-public crate-pretty-bytes-0.1.0 (c (n "pretty-bytes") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1sb8m5jcawchghnxv75s67jzm4qxpjqvgzpdj651nipdvm27bm99")))

(define-public crate-pretty-bytes-0.2.0 (c (n "pretty-bytes") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wi2wjg40aip6ilf8ymzm3xyd7jpxggpq5h3c0n7564ndfj6q3zg")))

(define-public crate-pretty-bytes-0.2.1 (c (n "pretty-bytes") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jb3fz9h9hgbvyi7n6kvic2xjhd4flwcaqdn1d74pqgsk4wvk59h")))

(define-public crate-pretty-bytes-0.2.2 (c (n "pretty-bytes") (v "0.2.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "05jkd1f824b93jh0jwfskba9hd70crvjz2nl1hf2xgqx5kfnx780")))

