(define-module (crates-io pr et pretty-log) #:use-module (crates-io))

(define-public crate-pretty-log-1.0.0 (c (n "pretty-log") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0zj2qsjkplh665nx05jvwp6rs3rf45c38gw4q0aivmy2ijjch78a")))

(define-public crate-pretty-log-1.1.0 (c (n "pretty-log") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1yl05v7iczv8xdcw3lgddkxia4xpcfz7n32yjq72vh8zycqgjs0g")))

(define-public crate-pretty-log-1.5.0 (c (n "pretty-log") (v "1.5.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0ashlp9c7yr2shmyi214r13gdrjr4gssfi8x33288nh4v749jp0h")))

(define-public crate-pretty-log-1.5.1 (c (n "pretty-log") (v "1.5.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0ic0rz10rqx1h6y0frip5fkxi1i1b7zkqszmf4k98xa8kmczza44")))

