(define-module (crates-io pr et prettygooey) #:use-module (crates-io))

(define-public crate-prettygooey-0.1.0 (c (n "prettygooey") (v "0.1.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xy1rxr6l2ghn97rnl34h8z6ih2jdc7kfx51gs4r27j0qbdgraj8")))

(define-public crate-prettygooey-0.1.1 (c (n "prettygooey") (v "0.1.1") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fqms9p4byidyhcl3zhw8w8vr66pf2gpfsqakrajksa46dwz9k6g")))

