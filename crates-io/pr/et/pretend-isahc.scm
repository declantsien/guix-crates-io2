(define-module (crates-io pr et pretend-isahc) #:use-module (crates-io))

(define-public crate-pretend-isahc-0.1.0 (c (n "pretend-isahc") (v "0.1.0") (d (list (d (n "isahc") (r "^1.3") (d #t) (k 0)) (d (n "pretend") (r "^0.1.0") (d #t) (k 0)))) (h "1mkp4c8acdf43yh8zr1rj0cy57f613fjxg92vhmj24hhcnqvdgfr")))

(define-public crate-pretend-isahc-0.1.1 (c (n "pretend-isahc") (v "0.1.1") (d (list (d (n "isahc") (r "^1.3") (d #t) (k 0)) (d (n "pretend") (r "^0.1.0") (d #t) (k 0)))) (h "10pbrjmm864jq4b6wszhk7bhkh4jywzqqhdhp82g9y1fmf88hjpa")))

(define-public crate-pretend-isahc-0.2.0 (c (n "pretend-isahc") (v "0.2.0") (d (list (d (n "isahc") (r "^1.3") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)))) (h "1svlic1xxgv2j2ys3jv68916viyi15a0p00phbba4xidblw3wwl9")))

(define-public crate-pretend-isahc-0.2.1 (c (n "pretend-isahc") (v "0.2.1") (d (list (d (n "curl") (r ">=0.4, <0.4.36") (d #t) (k 0)) (d (n "isahc") (r "^1.3") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)))) (h "1pnzg12nhcngj078a6l0166bxhwsrf1ybd14hd9x19dh6127dhgn")))

(define-public crate-pretend-isahc-0.2.2 (c (n "pretend-isahc") (v "0.2.2") (d (list (d (n "curl") (r ">=0.4, <0.4.36") (d #t) (k 0)) (d (n "isahc") (r "^1.3") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)))) (h "0s7pxzxrzgjqq60w3n72zljvgy79mgi3ybhab7qkq0nxzvlzccna")))

(define-public crate-pretend-isahc-0.3.0 (c (n "pretend-isahc") (v "0.3.0") (d (list (d (n "isahc") (r "^1") (k 0)) (d (n "pretend") (r "^0.3.0") (d #t) (k 0)))) (h "1fzng7070303cz7h63hfmdigi5n93yg7kkwh4nl43jzj13h0l4vz") (f (quote (("default" "isahc/default"))))))

(define-public crate-pretend-isahc-0.4.0 (c (n "pretend-isahc") (v "0.4.0") (d (list (d (n "isahc") (r "^1") (k 0)) (d (n "pretend") (r "^0.4.0") (d #t) (k 0)))) (h "07mq80qii4yggllr9zcfr4acjmq5pydyidqmf84x3b6axxz3gbc5") (f (quote (("default" "isahc/default"))))))

