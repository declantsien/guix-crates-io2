(define-module (crates-io pr et pretty_env_logger) #:use-module (crates-io))

(define-public crate-pretty_env_logger-0.1.0 (c (n "pretty_env_logger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1iak1j9jndnvalv7mkbpah1df74fdkn7zdp3yas9cki9gzpd2wnx")))

(define-public crate-pretty_env_logger-0.1.1 (c (n "pretty_env_logger") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1wjsxq3dkj8cj0d8x78ccjjqr6x874m08xsj64ndnnxyx3yx35wa")))

(define-public crate-pretty_env_logger-0.2.0-rc.1 (c (n "pretty_env_logger") (v "0.2.0-rc.1") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mfkfmgxwscwvyr06kmfi9pdd1fa7n095m2lkdaaagi0f9lbkhki")))

(define-public crate-pretty_env_logger-0.2.0-rc.2 (c (n "pretty_env_logger") (v "0.2.0-rc.2") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04fbg9rzfwqknk6gmai8jjcddvp2kzjdr661k0676ijrbj1jwnhg")))

(define-public crate-pretty_env_logger-0.2.0 (c (n "pretty_env_logger") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00nyqzbcar5pwskzk2sbyq2ipam68nway5nrv1ck8hvbxphcwsh5")))

(define-public crate-pretty_env_logger-0.2.1 (c (n "pretty_env_logger") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0afqhgla87x5zy6r696pk151117rlanyv5j7d2mp5c9lb9kpn4k4")))

(define-public crate-pretty_env_logger-0.2.2 (c (n "pretty_env_logger") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (t "cfg(not(all(windows, target_arch = \"x86\")))") (k 0)) (d (n "ansi_term") (r "^0.9") (d #t) (t "cfg(all(windows, target_arch = \"x86\"))") (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w6xxrbscrvff11143qpz97hx4pj8km2p4fkvdy9d7kjsry50p7x")))

(define-public crate-pretty_env_logger-0.2.3 (c (n "pretty_env_logger") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mj8fdz544nfrwsm7ipgnr46piqpdy90cxr5ms74x2knxw8brlr6")))

(define-public crate-pretty_env_logger-0.2.4 (c (n "pretty_env_logger") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ydx6vlmn26nlgsmbcjyydg08pbznmy9ky1mshx63xjv4miv9qca")))

(define-public crate-pretty_env_logger-0.2.5 (c (n "pretty_env_logger") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a0c53plsr4abw0y1iyjxs0d64f0a6dn48464a2rp21f0iiix3gd")))

(define-public crate-pretty_env_logger-0.3.0 (c (n "pretty_env_logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qgxmypqxs831lv7n40yfr4ri5062dd8xpp5qbczgpkm0i73z2yz")))

(define-public crate-pretty_env_logger-0.3.1 (c (n "pretty_env_logger") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x4hyjlnvvhyk9m74iypzybm22w3dl2k8img4b956239n5vf8zki")))

(define-public crate-pretty_env_logger-0.4.0 (c (n "pretty_env_logger") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17gva1rlf9fhgr0jr19kv39f8bir3f4pa4jz02qbhl9qanwkcvcj")))

(define-public crate-pretty_env_logger-0.5.0 (c (n "pretty_env_logger") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "076w9dnvcpx6d3mdbkqad8nwnsynb7c8haxmscyrz7g3vga28mw6")))

