(define-module (crates-io pr et prettyplease) #:use-module (crates-io))

(define-public crate-prettyplease-0.0.0 (c (n "prettyplease") (v "0.0.0") (h "164xjp79h938k38641i0zmxqf2ykqcdn44jk4428g24mc2gfrla2") (y #t)))

(define-public crate-prettyplease-0.1.0 (c (n "prettyplease") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "16lkg2bxhhkx3jp2jna9nd1c5xah9x47f1h0r3i7dqj5k5bbsgiz") (r "1.56")))

(define-public crate-prettyplease-0.1.1 (c (n "prettyplease") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "1cfb47rmicyi1f0srxgs9xq9bzdyhs3pzvccp130sd4rz5g5chhr") (r "1.56")))

(define-public crate-prettyplease-0.1.2 (c (n "prettyplease") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "0nmmb7awq99r1gjyxsxs3w791qndx6bvvjxghgqv22dffnh3fvn7") (r "1.56")))

(define-public crate-prettyplease-0.1.3 (c (n "prettyplease") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "0q6g1q0as8r2wj0al529wnw0i3j6s93l5wbmcs7cab9r8lykpv20") (r "1.56")))

(define-public crate-prettyplease-0.1.4 (c (n "prettyplease") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "18vh8rhyd6z9849vv5z00c34my7bmvar98xby0dxhjfan67v2w41") (r "1.56")))

(define-public crate-prettyplease-0.1.5 (c (n "prettyplease") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "0na4a5q349lqn32pqpbpkrkmgk7b3y7glg0mygaagvq7aj4mgp93") (r "1.56")))

(define-public crate-prettyplease-0.1.6 (c (n "prettyplease") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "11sxhk5lgdbwd2326bd8wy6s72rfcms05zk6bvhvacx580z4b0mh") (r "1.56")))

(define-public crate-prettyplease-0.1.7 (c (n "prettyplease") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "18yi87w25lmdzqp0cd0ynkqr0xbx25dxx4dmr2i7d8m6cc68nx2k") (r "1.56")))

(define-public crate-prettyplease-0.1.8 (c (n "prettyplease") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "05wgkn770c6b3znmr165svn0hk91lwsxva8z5gm56f2mmav4sm0s") (r "1.56")))

(define-public crate-prettyplease-0.1.9 (c (n "prettyplease") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing"))) (k 2)))) (h "063zx715xdx9nazh95nkkwxj8klkk2gjrxbz4mbcbigm18nyr0rv") (r "1.56")))

(define-public crate-prettyplease-0.1.10 (c (n "prettyplease") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "00v89kflh3wn8hps36v77ss30a404wal9x5m0s51lxyh8qx7xq6r") (r "1.56")))

(define-public crate-prettyplease-0.1.11 (c (n "prettyplease") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "099igcy80d272c1qsfnxqywjw0lsaya32d4rrxi5hmljn7l573zj") (r "1.56")))

(define-public crate-prettyplease-0.1.12 (c (n "prettyplease") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1bav04c00kshxiphq3kpz9mj4cjs0dx3rsf2n1yn1k609yfmndg5") (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.13 (c (n "prettyplease") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "13jw6yd2k615hawbasx4km4cgqr1fb3l2qkd8dsqyi2li7q36hjp") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.14 (c (n "prettyplease") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1108fz9m1liy4r00plfbsp1i1akn2x8b7f7m83vnmgahwqbj8rn3") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.15 (c (n "prettyplease") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1976lmmc4ga2gd5r70lfwksj4x00fgkdqps8jphfyv1rid81c5cy") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.16 (c (n "prettyplease") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1hp44l36v1whdcgzk240aijxdds49s6a9h5i7h0la0kqcblgnvys") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.17 (c (n "prettyplease") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1q1q1s93cvzd5n7zy3544j3cmj7vhpxhzwfiadr00p4hy55yiyzz") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.18 (c (n "prettyplease") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1sgylnwgg2vzbpb450vi7zc1cagygw8dn087kr1iy082xqhffyk9") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.19 (c (n "prettyplease") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "0niz1h36cdcwdpq87ysxn0ck81kxs4zs3bx3jjc0a93aqb98d7m4") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.20 (c (n "prettyplease") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "1h5nmrisqk6jb9djrk9mr887wk6fv2ki5ijd4zw6wybqw50svzl3") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.21 (c (n "prettyplease") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "0ldv26y7v98fwg87qdvyq7q817jnnz2yigi8qph1y5spdgjc0hn1") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.22 (c (n "prettyplease") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "0wvpc7vwki5spvc5xvhd4cxlx1yki2w7s4vnvy5s54wfbnl9529c") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.23 (c (n "prettyplease") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "0y2wbmflbkgp13ywd7qyq7hyi59x5zazmljnw8gg09wnfwak4zp9") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.24 (c (n "prettyplease") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "015zydsyz3wsbfbwfmf8j7c081cmiqw5cc530hj0ljhas9wx5g2f") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.1.25 (c (n "prettyplease") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("parsing"))) (k 2)))) (h "11lskniv8pf8y8bn4dc3nmjapfhnibxbm5gamp2ad9qna3lld1kc") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.2.0 (c (n "prettyplease") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.7") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.7") (f (quote ("parsing"))) (k 2)))) (h "1b0p26i8xxjchjyp5bmmqylbvy0vsvflppil21ahiryiykpw0225") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.2.1 (c (n "prettyplease") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.7") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.7") (f (quote ("parsing"))) (k 2)))) (h "15fpbvj4ga1k196w8wby4rdh16rd1pap43d1mq2vhbyiyll5wzy1") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease01") (r "1.56")))

(define-public crate-prettyplease-0.2.2 (c (n "prettyplease") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("parsing"))) (k 2)))) (h "1iwz57fa087rjypx832q1f0a0sssawdwq5m2dxjfqqlwv29jwsn5") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.3 (c (n "prettyplease") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("parsing"))) (k 2)))) (h "0y7k6s6q73gf9hvw8l0vwkpw96is45mg2npq230xmii0s1d3nqlw") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.4 (c (n "prettyplease") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("parsing"))) (k 2)))) (h "0n5hd90q3ji2l4j3vls63263d43jymszz7ykgvn4cp2vyjmaiv0w") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.5 (c (n "prettyplease") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing"))) (k 2)))) (h "00vjng1n0092wg8cmp74v9ll21iq3y6gp1j8xcxmjssnh6xylzv1") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.6 (c (n "prettyplease") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing"))) (k 2)))) (h "1lc0h1bq7ldvsxgwdn523ih38y1y95qbk37yybknkl2lmfdd6s9v") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.7 (c (n "prettyplease") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing"))) (k 2)))) (h "00bml2iygdfgkmkfpfbac2ry3prqsxkh6m5qb831ymq4nasx5pj3") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.8 (c (n "prettyplease") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing"))) (k 2)))) (h "0ka5w06w4y0mv750fsa31azlmwxji49dlv542c975phbf9xkgc7j") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.9 (c (n "prettyplease") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing"))) (k 2)))) (h "10n2s6b11pmh8qxz9kjmrb6pgnv5dnsydi3rxpz221nn053a09cq") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.10 (c (n "prettyplease") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing"))) (k 2)))) (h "11rkz208cp0pcpd7wljwcad4jmnr63k97b9zsi804hbvjnc924wj") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.11 (c (n "prettyplease") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing"))) (k 2)))) (h "1rfiy3d3hpsynwfnlcb3480kc2qklrdwyzy23fjrqv22cw0pzadf") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.12 (c (n "prettyplease") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing"))) (k 2)))) (h "0qnxcpxlnxhyqjay6jqpbar0xcxswax0y8xjw6icxkb316xdjr3c") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.13 (c (n "prettyplease") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.30") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.30") (f (quote ("parsing"))) (k 2)))) (h "18d6qqif5g4s2pi0n198ypzd7x6m257rlxx22rg017hb7f3yfndg") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.14 (c (n "prettyplease") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("parsing"))) (k 2)))) (h "02q3r5cdcbyb1qcx9nnmdhmkwg6dsb7mcqkyf9hawg3ypvww0cl8") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.15 (c (n "prettyplease") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("parsing"))) (k 2)))) (h "17az47j29q76gnyqvd5giryjz2fp7zw7vzcka1rb8ndbfgbmn05f") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.16 (c (n "prettyplease") (v "0.2.16") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.74") (k 0)) (d (n "proc-macro2") (r "^1.0.74") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("parsing"))) (k 2)))) (h "1dfbq98rkq86l9g8w1l81bdvrz4spcfl48929n0pyz79clhzc754") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.17 (c (n "prettyplease") (v "0.2.17") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.74") (k 0)) (d (n "proc-macro2") (r "^1.0.74") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("parsing"))) (k 2)))) (h "1ivhwangvv3wxnkap8f6wfbr4n24057h3zwiz23cns5pbpxjhfcd") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.56")))

(define-public crate-prettyplease-0.2.18 (c (n "prettyplease") (v "0.2.18") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.80") (k 0)) (d (n "proc-macro2") (r "^1.0.80") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2.0.59") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("parsing"))) (k 2)))) (h "1f3rjgm85j6v0hphi7h8himww482hbk26xyrq3z2ln878vninsa3") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.60")))

(define-public crate-prettyplease-0.2.19 (c (n "prettyplease") (v "0.2.19") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.80") (k 0)) (d (n "proc-macro2") (r "^1.0.80") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2.0.59") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("parsing"))) (k 2)))) (h "0l35djp14h2jbw7984vcabvqwl26szldl1zxbygv8hjg5q7wzhjs") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.60")))

(define-public crate-prettyplease-0.2.20 (c (n "prettyplease") (v "0.2.20") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.80") (k 0)) (d (n "proc-macro2") (r "^1.0.80") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2.0.59") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("parsing"))) (k 2)))) (h "0pk4vm9fir1p0bl11p9fkgl9r1x9vi4avv8l7flb1wx2i1a364jz") (f (quote (("verbatim" "syn/parsing")))) (l "prettyplease02") (r "1.60")))

