(define-module (crates-io pr et pretty_goodness) #:use-module (crates-io))

(define-public crate-pretty_goodness-0.1.0 (c (n "pretty_goodness") (v "0.1.0") (d (list (d (n "corlib") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "1.0.*") (d #t) (k 0)))) (h "03775l0h3qliimdqdy0npa2x1pwx0hhj5q6sz6zqz6g46k9sr2gv")))

(define-public crate-pretty_goodness-0.1.1 (c (n "pretty_goodness") (v "0.1.1") (d (list (d (n "corlib") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "1.0.*") (d #t) (k 0)))) (h "0gn9pnw74r3yaravhx2l3ynwmc8nyzjg2snn1lhl19897pbxcqxm")))

