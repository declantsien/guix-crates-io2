(define-module (crates-io pr et prettify-js) #:use-module (crates-io))

(define-public crate-prettify-js-0.1.0 (c (n "prettify-js") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ress") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wbpw93a6z854hsmlsn887xwbhw0v3kywpkihq8r4j96f7n1izy7")))

