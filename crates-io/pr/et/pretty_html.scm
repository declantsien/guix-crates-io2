(define-module (crates-io pr et pretty_html) #:use-module (crates-io))

(define-public crate-pretty_html-0.0.1 (c (n "pretty_html") (v "0.0.1") (h "088f8xrph3gds05fy8i5xmzvjgajm7f4wzzxahgk2yj8x56bxxs5")))

(define-public crate-pretty_html-0.1.0 (c (n "pretty_html") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "syntect") (r "^2.1.0") (d #t) (k 0)))) (h "0jdahs4w1jz8vfcnmgalsbk29c0naryhsvywdag6d0w2hsd1fcpy")))

