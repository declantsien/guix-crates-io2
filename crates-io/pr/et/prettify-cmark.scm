(define-module (crates-io pr et prettify-cmark) #:use-module (crates-io))

(define-public crate-prettify-cmark-0.1.0 (c (n "prettify-cmark") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)))) (h "0iqr5ny79bagl1kgc71d8wplfi9a53i2qz2icnvjqsi7fwizzrsr")))

(define-public crate-prettify-cmark-0.1.1 (c (n "prettify-cmark") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)))) (h "0wm4fgw3005cc642m8slnl4kxgsw6mwfj9c76bk6b99r5mv9m94q")))

(define-public crate-prettify-cmark-0.1.2 (c (n "prettify-cmark") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)))) (h "1cs6laxs6jmcwhl730kh1f4xg7ayzidnjkccix3pp4y2c79skz6l")))

