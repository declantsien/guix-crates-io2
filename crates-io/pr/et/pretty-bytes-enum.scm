(define-module (crates-io pr et pretty-bytes-enum) #:use-module (crates-io))

(define-public crate-pretty-bytes-enum-0.1.0 (c (n "pretty-bytes-enum") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.182") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "19sa0krn39hwzn5wsgncxrdi3m2vc90ap30sgrswbcq9plam65y6") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

