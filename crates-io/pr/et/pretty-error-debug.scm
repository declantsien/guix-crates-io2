(define-module (crates-io pr et pretty-error-debug) #:use-module (crates-io))

(define-public crate-pretty-error-debug-0.1.0 (c (n "pretty-error-debug") (v "0.1.0") (h "14awgj66l4w96r9wgbj9khx106i6h4ld101h0pwia89az74rkcmh")))

(define-public crate-pretty-error-debug-0.1.1 (c (n "pretty-error-debug") (v "0.1.1") (d (list (d (n "pretty-error-debug-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "03rf002xnywvx5nl0262nnm1ksinh40m77aiq7lm90cvw60m935r") (f (quote (("derive" "pretty-error-debug-derive") ("default" "derive"))))))

(define-public crate-pretty-error-debug-0.2.0 (c (n "pretty-error-debug") (v "0.2.0") (d (list (d (n "pretty-error-debug-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)))) (h "0c605vpyjq2qh9valsvjgq9acwzbif40yz2bivlclbv0wf8x64j5") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:pretty-error-debug-derive")))) (r "1.60")))

(define-public crate-pretty-error-debug-0.3.0 (c (n "pretty-error-debug") (v "0.3.0") (d (list (d (n "pretty-error-debug-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)))) (h "1c2b5v92l980fbbm7hbyj2in7dhl02mjx5qhfsbrssnyy643i3ss") (f (quote (("derive" "pretty-error-debug-derive") ("default" "derive")))) (r "1.56")))

