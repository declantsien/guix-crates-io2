(define-module (crates-io pr et pretty-menu) #:use-module (crates-io))

(define-public crate-pretty-menu-0.1.0 (c (n "pretty-menu") (v "0.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0gvm8gpf1l8l8rb12ci771krrgi842jkf5vxlal0v4j2q83sv08c") (y #t)))

(define-public crate-pretty-menu-0.1.1 (c (n "pretty-menu") (v "0.1.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1qwn07ij07k87hl31z9sm73pc15bq3n0h4mivqp5v3imqh6h1jdr")))

