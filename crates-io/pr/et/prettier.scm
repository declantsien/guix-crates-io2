(define-module (crates-io pr et prettier) #:use-module (crates-io))

(define-public crate-prettier-0.1.0 (c (n "prettier") (v "0.1.0") (h "0qp0nq36jkgj08wcnw0yx84kb4v3q2k18rg4i9xfqbdcpxx26vqd")))

(define-public crate-prettier-0.1.1 (c (n "prettier") (v "0.1.1") (h "0ri7akx5amsw6ika6i14hr4h20zcz2llq1mvqk6cjsz0lafr4z45")))

(define-public crate-prettier-0.1.5 (c (n "prettier") (v "0.1.5") (h "1xwn0va8x4cyvqv473r09yp2swsl3igfg9jbksmp39bw8jfgv5n1")))

