(define-module (crates-io pr et pretend-codegen) #:use-module (crates-io))

(define-public crate-pretend-codegen-0.1.0 (c (n "pretend-codegen") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xh7i1qianwa10pdjb8ddqgwrd0zsl9ng617d8fvdqp0q4y3nab9")))

(define-public crate-pretend-codegen-0.1.1 (c (n "pretend-codegen") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q371s5jxnp9yd99n6hlwwd8m78xam7mppsidpzwjqr1w8d9hgz2")))

(define-public crate-pretend-codegen-0.2.0 (c (n "pretend-codegen") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0piqj1ify4dyzw1cikbswjf354vi3345k2km9j0ln86jwhgnbpx6")))

(define-public crate-pretend-codegen-0.2.1 (c (n "pretend-codegen") (v "0.2.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4harpvs4m9z4y1w8h105fwvn2c55yald7kak0rdj66q84gnmcm")))

(define-public crate-pretend-codegen-0.2.2 (c (n "pretend-codegen") (v "0.2.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w08lrign6y08jp8kp1qvq1p11a572vzda8wxf9i0afdsfc291f1")))

(define-public crate-pretend-codegen-0.3.0 (c (n "pretend-codegen") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06mk6p5i6dygzpr2b914nnc05p9xvvcgcvipzvqjdqjrlpbyxmz1")))

(define-public crate-pretend-codegen-0.4.0 (c (n "pretend-codegen") (v "0.4.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g070a50hxpvhi1nlhm4c9x9wl8axn7bw8bmw9lixllnk8k3vnj0")))

