(define-module (crates-io pr et pretend-ureq) #:use-module (crates-io))

(define-public crate-pretend-ureq-0.2.2 (c (n "pretend-ureq") (v "0.2.2") (d (list (d (n "pretend") (r "^0.2.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1vjffl45mycbbdksmmqzrgra1pvazc63il0c7nfjfddk7rl9vzkl")))

(define-public crate-pretend-ureq-0.3.0 (c (n "pretend-ureq") (v "0.3.0") (d (list (d (n "pretend") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "1rri6qg0j850gbmvrpaxc266igfsn1195k9n1gb4wni6xdd075j5") (f (quote (("default" "ureq/default"))))))

(define-public crate-pretend-ureq-0.4.0 (c (n "pretend-ureq") (v "0.4.0") (d (list (d (n "pretend") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "02s04dxcklqjfdrg6hpp93qb37962ffyy8cqvv8jj98d49r3rkvh") (f (quote (("default" "ureq/default"))))))

