(define-module (crates-io pr et pretty_toa) #:use-module (crates-io))

(define-public crate-pretty_toa-0.1.0 (c (n "pretty_toa") (v "0.1.0") (d (list (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0hpgvvjhmzcv0mkjg316ypwgpcsigjh2375iqca29hiljzzbdf9i")))

(define-public crate-pretty_toa-0.1.1 (c (n "pretty_toa") (v "0.1.1") (d (list (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "10qw4idd2xbzy0xsn0slaw9gmqkg4rw7q4gqqdgi1nb3jc9fis23")))

(define-public crate-pretty_toa-1.0.0 (c (n "pretty_toa") (v "1.0.0") (d (list (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0jbv5h50zlqk6hyhqzw8x5fa08an5f8gkaayp85zlxhly3fzl8xb")))

