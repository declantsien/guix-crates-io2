(define-module (crates-io pr et pretty) #:use-module (crates-io))

(define-public crate-pretty-0.0.1 (c (n "pretty") (v "0.0.1") (h "0p3ndbcl69j3hwylf9jgmhxq75kjh3kbys0fyyjyz09rm70aws6h")))

(define-public crate-pretty-0.0.2 (c (n "pretty") (v "0.0.2") (h "0x724lgdr07dm6mvqmv6i7691yq9f0jsz2wjgfic3am7d1i95w5h")))

(define-public crate-pretty-0.0.3 (c (n "pretty") (v "0.0.3") (h "0c6h597zqrwaymqcfl08q7pyszicqhkai0pay98snkyhfb9ffsqw")))

(define-public crate-pretty-0.0.4 (c (n "pretty") (v "0.0.4") (h "0ak704c4380kjylg7p5hkzg0pd5yalnlc7i0q5mghhxijapyarrn")))

(define-public crate-pretty-0.0.5 (c (n "pretty") (v "0.0.5") (h "0iqg967pv22rssald2ff1pb9c1jq8fvii8i1nn515mcp7kxl1lyr")))

(define-public crate-pretty-0.0.6 (c (n "pretty") (v "0.0.6") (h "0rp4ba6gn76arsv5mk413yf46x7j7rsds8dyalpn1s2l9nlhxlpp")))

(define-public crate-pretty-0.0.7 (c (n "pretty") (v "0.0.7") (h "101s4l7na16205zfrz3b7i8b0czxrbz234qhbiqrf0x5bsflmq5n")))

(define-public crate-pretty-0.1.0 (c (n "pretty") (v "0.1.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "158bvlrhcik4w2m2ymbf68sgp9bp8xg5bll8wlaa58bwplvnm9va")))

(define-public crate-pretty-0.2.0 (c (n "pretty") (v "0.2.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0knjm7ff9s3p97vhn69sx27xcqdd6mw665h9dqfxw3y6im51hgh2")))

(define-public crate-pretty-0.3.0 (c (n "pretty") (v "0.3.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "1zhwi00dgl9p7ja4pzww30jhzpcdx6zliw7rrg39gnfndwgllddb")))

(define-public crate-pretty-0.3.1 (c (n "pretty") (v "0.3.1") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0ng7k1i8h4pddkrbcjfjq96dwx49m46mqd4cqlpwyh8j3b9km34r")))

(define-public crate-pretty-0.3.2 (c (n "pretty") (v "0.3.2") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "18gwi7g07wq2a20z5ca655ygy9xbrvdjv3y1199cc1yyswzgq769")))

(define-public crate-pretty-0.3.3 (c (n "pretty") (v "0.3.3") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0bkzbvdh9wafddzkklmmfzj215512b4hmwb0qlsj0icacfwshyh5")))

(define-public crate-pretty-0.4.0 (c (n "pretty") (v "0.4.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0dc613jbf3hy9d43cn01sc12hgzyw94a92b5ky638xnh1yzcrsg8")))

(define-public crate-pretty-0.5.0 (c (n "pretty") (v "0.5.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "1xng17lm4yq0jlb06i9ksds07rvys1dmghwzi1x1mdbc0ncvijk4")))

(define-public crate-pretty-0.5.1 (c (n "pretty") (v "0.5.1") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0zadrlck7k741krb42rsmxqyf7y4wqq71wz6mv40p00jaajfdiyn") (y #t)))

(define-public crate-pretty-0.5.2 (c (n "pretty") (v "0.5.2") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "1lzn7d60l79vsdwcfgrh6jmk193nzwh1j36r8p9cv3n8dyghs37n")))

(define-public crate-pretty-0.6.0 (c (n "pretty") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "14182x5f545pdwmm1vczixfwgb56gdm664q397v9dg7y96y8cvd5")))

(define-public crate-pretty-0.7.0 (c (n "pretty") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "01pp9h3sp4m2pwfjhc7il764nsp97szh2y85mcj67v60y6nvyrzy")))

(define-public crate-pretty-0.7.1 (c (n "pretty") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "1px01j68nn57hxllg8kxw69f7k1vrd53kqwjglh9z3alx7j8dbf6")))

(define-public crate-pretty-0.8.0 (c (n "pretty") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "termcolor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0k54fnxl9shfa3dli5ym583by02f0jd4v8xav085si82irrysi16")))

(define-public crate-pretty-0.9.0 (c (n "pretty") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0k520wahc1bp4lh07mav6czfwsmqq7zf3zapd5q43ki7hizpb48q")))

(define-public crate-pretty-0.10.0 (c (n "pretty") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0q9ars0diyfc2mj1lapl6s10ciyxg78kqbgc9bcxsmpf2fwl16dd")))

(define-public crate-pretty-0.11.0 (c (n "pretty") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0g59y70ncb6q3sfr4pk21c4640dasivzcr3v29pwzk87xdw2yl51")))

(define-public crate-pretty-0.11.1 (c (n "pretty") (v "0.11.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "010qivawk964nr265wkqzcraz2456nrima3g919xq1ikjd9b593k")))

(define-public crate-pretty-0.11.2 (c (n "pretty") (v "0.11.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1282l4pa9hhamvbnd5mjrwhdgcsjy1l1lj44i0m4pczsf1cd3br9")))

(define-public crate-pretty-0.11.3 (c (n "pretty") (v "0.11.3") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0ikq0dfcgj99m8z50zpkfz140sqpmiji4imp9l93nzd87hgamww3")))

(define-public crate-pretty-0.12.0 (c (n "pretty") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1h493j0li8bpb7vrmxz41s9s60xcb6r1qxbyx28wjdkjf0kdprm9")))

(define-public crate-pretty-0.12.1 (c (n "pretty") (v "0.12.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1k4sbqiv0ag62mnigfi5j45vx750gd827kprmbzxyc9s3iq9sg2n")))

(define-public crate-pretty-0.12.2 (c (n "pretty") (v "0.12.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1iap48xvwd3bxvr7i5ydpki1vn33gwbgwhrz8rwyrzdqmvy1k2rh")))

(define-public crate-pretty-0.12.3 (c (n "pretty") (v "0.12.3") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0yd5kyv3l73sbmkpsds2asfj03b6bpffbxnsyki3gdllv4blsp5m")))

