(define-module (crates-io pr et prettydiff) #:use-module (crates-io))

(define-public crate-prettydiff-0.1.0 (c (n "prettydiff") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "06xwccyk11vdw0x3qfnawmvjxfaza1gb2ax783yn6k3da62ggpi3")))

(define-public crate-prettydiff-0.2.0 (c (n "prettydiff") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1np00p668hv6lacwd2azn7m2bz513y76336l54l17whd9hd23h1q")))

(define-public crate-prettydiff-0.2.1 (c (n "prettydiff") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0yb7q30vqbmdxp8x25vprdf3718i5bchsyad18smhxz2lpwrr6k8")))

(define-public crate-prettydiff-0.2.2 (c (n "prettydiff") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "114yyk50k0pqkbj3h9bb5ylnkb36ivvymyi8cmavsnjzgfddy9xq")))

(define-public crate-prettydiff-0.2.3 (c (n "prettydiff") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0rkgk8l1b0mvvh7fhnm0mqxvm6ksvkr8rqxy27l6lxc5cb1vfkky")))

(define-public crate-prettydiff-0.2.4 (c (n "prettydiff") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0631pjsf2w3v57sxvw8pc61wng7vffz5ns32nxjy47cz0dm4zbvw")))

(define-public crate-prettydiff-0.2.5 (c (n "prettydiff") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "02b6x99dskk19vbkx1mm0lyrm797nm35hq025adjm81jzxj1j2pn")))

(define-public crate-prettydiff-0.2.6 (c (n "prettydiff") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1k30ag31386zrcx9f7g8lhfn0jr8b4hnxaqvcvyggfrfx3ny88gg")))

(define-public crate-prettydiff-0.3.0 (c (n "prettydiff") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "007acrmx9k9cbcgzysy9dfkwxicvg0ysh5wsi5rcwb4scd4qwc05")))

(define-public crate-prettydiff-0.3.1 (c (n "prettydiff") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1kvsqvyw3j49p7jhi63lb0fbfpj7p562cdlsh63pig51kq6bwh2j")))

(define-public crate-prettydiff-0.4.0 (c (n "prettydiff") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ngmz2gzpm1jq5g89dgnaiqd2i5rlasgnx7pbybk8va4zsyyij9v")))

(define-public crate-prettydiff-0.5.0 (c (n "prettydiff") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1yqgq9nv87j9qyk5ymbmyzxzfgal4ii2ri1xp4s765mnmm90w0lk") (f (quote (("default" "cli") ("cli" "structopt"))))))

(define-public crate-prettydiff-0.5.1 (c (n "prettydiff") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0w8x2hc2ih9fa0sm0myr5knkdgq1ay8x8xmpwmgpbch96yl72mpv") (f (quote (("default" "cli") ("cli" "structopt"))))))

(define-public crate-prettydiff-0.6.0 (c (n "prettydiff") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "18mgi8pqqh5micjnghhrq34c4d8hswc6l3mh63z87x5345jy3hpl") (f (quote (("default" "cli") ("cli" "prettytable-rs" "structopt"))))))

(define-public crate-prettydiff-0.6.1 (c (n "prettydiff") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1752bsq5wsax4p19z76s835pqwjqwzxq42w26ihd8dqn1wcpcq8b") (f (quote (("default" "cli") ("cli" "prettytable-rs" "structopt"))))))

(define-public crate-prettydiff-0.6.2 (c (n "prettydiff") (v "0.6.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0a094szpmn2w38ralhdfl248p507mjph7q7vpzb4lcvy1klav4ym") (f (quote (("default" "cli") ("cli" "prettytable-rs" "structopt"))))))

(define-public crate-prettydiff-0.6.3 (c (n "prettydiff") (v "0.6.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0x9g3d17fwp4nyafnrmrx1l6lzliqwgr8fvdh66ylg7w9gg4zpk8") (f (quote (("default" "cli") ("cli" "prettytable-rs" "structopt"))))))

(define-public crate-prettydiff-0.6.4 (c (n "prettydiff") (v "0.6.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "04fwd2xlgcnk63ffx83jib764aqpx0a0qv6g6s11z0l2233gxwcg") (f (quote (("default" "cli") ("cli" "prettytable-rs" "structopt"))))))

(define-public crate-prettydiff-0.7.0 (c (n "prettydiff") (v "0.7.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1s8mfbmvwn699l0i2xdahag4wrscm5ynjrs3hnrn01n1hfq3zv5b") (f (quote (("default" "cli") ("cli" "prettytable-rs")))) (r "1.70")))

