(define-module (crates-io pr et prettify-markdown) #:use-module (crates-io))

(define-public crate-prettify-markdown-0.1.0 (c (n "prettify-markdown") (v "0.1.0") (d (list (d (n "comrak") (r "^0.13.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "prettify") (r ">=0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bslqfyvjcgar5hgjggmhfkms2yzvs9risqswcgmy09cfyp9sacr")))

(define-public crate-prettify-markdown-0.2.0 (c (n "prettify-markdown") (v "0.2.0") (d (list (d (n "comrak") (r "^0.13.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "prettify") (r ">=0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r3madzsrz27s1gm1cqhwwvccdrzgsr6c9cg4qdzvsiaf98pi702")))

