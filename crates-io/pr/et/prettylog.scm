(define-module (crates-io pr et prettylog) #:use-module (crates-io))

(define-public crate-prettylog-0.1.0 (c (n "prettylog") (v "0.1.0") (h "00rsp4bdlckvih11ms9z5qm6qdkb7qwg3n8l01sxmd63h488fllh")))

(define-public crate-prettylog-0.1.1 (c (n "prettylog") (v "0.1.1") (h "1ydsn3yjh7vgnds9kfi72c2b4dclpxpr58if2n27243lnmvzvxi6")))

(define-public crate-prettylog-0.1.2 (c (n "prettylog") (v "0.1.2") (h "0i6xz5w3czdhrpg76idpf4ybayx2wi4x95nn29qvwpzqxnqsyy2j")))

(define-public crate-prettylog-0.1.3 (c (n "prettylog") (v "0.1.3") (h "1fd2xyq210gv1ikyvvlli4jy0dzyq7ks4bwmzja6d483g2vlai6v")))

(define-public crate-prettylog-0.1.4 (c (n "prettylog") (v "0.1.4") (h "1zp7m2zznznbcr1pxhfdvy713qdk48qy440dx8cb86ag8m782mh6")))

