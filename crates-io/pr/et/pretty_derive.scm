(define-module (crates-io pr et pretty_derive) #:use-module (crates-io))

(define-public crate-pretty_derive-0.1.0 (c (n "pretty_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00p7269fsm0lz10jfbm3lc8pbjn66xwdxj6awnvcy85x810idrb7")))

