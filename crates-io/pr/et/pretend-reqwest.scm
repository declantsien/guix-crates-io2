(define-module (crates-io pr et pretend-reqwest) #:use-module (crates-io))

(define-public crate-pretend-reqwest-0.1.0 (c (n "pretend-reqwest") (v "0.1.0") (d (list (d (n "hyper") (r ">=0.14, <0.14.5") (d #t) (k 0)) (d (n "pretend") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1wp9h6plbpqrf6wph5s6w32g5819c8b069g6ipj6dqswq7shsbj4")))

(define-public crate-pretend-reqwest-0.1.1 (c (n "pretend-reqwest") (v "0.1.1") (d (list (d (n "hyper") (r ">=0.14, <0.14.5") (d #t) (k 0)) (d (n "pretend") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "09j5k2l8a5xdc3mrn0qd6yr8rwrvjq4h474m4qi1b2zkbvm4d0mv")))

(define-public crate-pretend-reqwest-0.2.0 (c (n "pretend-reqwest") (v "0.2.0") (d (list (d (n "hyper") (r ">=0.14, <0.14.5") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0ihfy41sz5chss8jkwqhw6wxmsy160bp53lil4kc928pngpmfbvp") (f (quote (("default") ("blocking" "reqwest/blocking"))))))

(define-public crate-pretend-reqwest-0.2.1 (c (n "pretend-reqwest") (v "0.2.1") (d (list (d (n "hyper") (r ">=0.14, <0.14.5") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1hvdfm8f7a25zmi8isx176jxc0djkl3w5albwgznlgak9rfxwpzg") (f (quote (("default") ("blocking" "reqwest/blocking"))))))

(define-public crate-pretend-reqwest-0.2.2 (c (n "pretend-reqwest") (v "0.2.2") (d (list (d (n "hyper") (r ">=0.14, <0.14.5") (d #t) (k 0)) (d (n "pretend") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0pni08myiwn8v661ykaf09h4g03lm1k55dsd573hjxlqlya26zln") (f (quote (("default") ("blocking" "reqwest/blocking"))))))

(define-public crate-pretend-reqwest-0.3.0 (c (n "pretend-reqwest") (v "0.3.0") (d (list (d (n "pretend") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)))) (h "1phj7rb93k0hc29xn322kkwmsr7i5agqq9lyckw71yfkp1xqrl4r") (f (quote (("default" "reqwest/default") ("blocking" "reqwest/blocking"))))))

(define-public crate-pretend-reqwest-0.4.0 (c (n "pretend-reqwest") (v "0.4.0") (d (list (d (n "pretend") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)))) (h "0ggs1x778wqi1wwq1nzydaz3db5ghjj0p3nzk9n3z44xssq1gn8f") (f (quote (("default" "reqwest/default") ("blocking" "reqwest/blocking"))))))

