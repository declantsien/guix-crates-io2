(define-module (crates-io pr et pretty-panic) #:use-module (crates-io))

(define-public crate-pretty-panic-1.0.0 (c (n "pretty-panic") (v "1.0.0") (h "15pn377hsagxa4syp88q3z1qx0h45y3jprx7r92bw551hyg5yck7") (y #t)))

(define-public crate-pretty-panic-1.0.1 (c (n "pretty-panic") (v "1.0.1") (h "19gxc2a6p94w0p7n7cdnk82j9bchxsxmqjy9mgc1sm921mb0n78h") (y #t)))

(define-public crate-pretty-panic-1.0.2 (c (n "pretty-panic") (v "1.0.2") (h "1vg351bs885qgggwm4cgzxakbfikfnbgq6m50z1h9rvgyh80n4wc") (y #t)))

(define-public crate-pretty-panic-1.0.3 (c (n "pretty-panic") (v "1.0.3") (h "19hi8cw8zkc1m620k58vfmclwm56b29zb9gklv7sb8plj1cmwwkm") (y #t)))

(define-public crate-pretty-panic-1.0.4 (c (n "pretty-panic") (v "1.0.4") (h "0lbl6r2jf4rmwdf7qhww5skaacw5wc2bc5mcmabvqx138qi1kg06")))

