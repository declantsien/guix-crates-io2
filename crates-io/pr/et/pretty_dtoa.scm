(define-module (crates-io pr et pretty_dtoa) #:use-module (crates-io))

(define-public crate-pretty_dtoa-0.1.0 (c (n "pretty_dtoa") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "ryu_floating_decimal") (r "^0.1.0") (d #t) (k 0)))) (h "0g7jb3c781clc9r5xk10gc32drrg0iqy7ka7mkh8xizc2sjc36al")))

(define-public crate-pretty_dtoa-0.2.0 (c (n "pretty_dtoa") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "ryu_floating_decimal") (r "^0.1.0") (d #t) (k 0)))) (h "0ynk8wmrmymlcind3j780q0g58jh9mk9nr571ygb98p1iks4wrhh")))

(define-public crate-pretty_dtoa-0.3.0 (c (n "pretty_dtoa") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "ryu_floating_decimal") (r "^0.1.0") (d #t) (k 0)))) (h "1wnzgrc4a5g9ajwvbkz3fl0ga9b2q2alcfyx3bd5ys1cvbgvqfd2")))

