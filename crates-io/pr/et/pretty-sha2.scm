(define-module (crates-io pr et pretty-sha2) #:use-module (crates-io))

(define-public crate-pretty-sha2-0.1.0 (c (n "pretty-sha2") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "08k1k1nwg3nbhs7f0rxhm8c0l7n7j28msqi8v0ab8kq339mrzg3b")))

(define-public crate-pretty-sha2-0.1.1 (c (n "pretty-sha2") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "14qxfxcm3wjxqi4hqbrq1h4dyxv0520ily9l8vqqzqp5g45x7942")))

(define-public crate-pretty-sha2-0.1.2 (c (n "pretty-sha2") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1lqmpbrab7sz93385avn0dbjl2jbcnlyss9k8yws1gp725yhayb3")))

(define-public crate-pretty-sha2-0.1.3 (c (n "pretty-sha2") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0az6da4lzg7mf336lzsfjxs84lgrwwa5y8rdwhajmr8hzaxqfw62")))

(define-public crate-pretty-sha2-0.1.4 (c (n "pretty-sha2") (v "0.1.4") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0p7fdq6in4mhzbnz649mws2s31d6441mha5x402hykd9g026z1hi")))

(define-public crate-pretty-sha2-0.1.5 (c (n "pretty-sha2") (v "0.1.5") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1b288b1vmbdipdx7aykgnbm7p72rhf0zpbrdvf3jsqslak195aw6")))

