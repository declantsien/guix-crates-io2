(define-module (crates-io pr et prettycli) #:use-module (crates-io))

(define-public crate-prettycli-0.1.0 (c (n "prettycli") (v "0.1.0") (h "1la4z1nwd4vdn9kd27accplkqvnn4mhx5f0hvdj97ic2rabyq7h7")))

(define-public crate-prettycli-0.1.1 (c (n "prettycli") (v "0.1.1") (h "0zglhgyikclhr6ydj4az3c5jmqdgrfd8s4mbvilwhxcg8vwp7i5i")))

