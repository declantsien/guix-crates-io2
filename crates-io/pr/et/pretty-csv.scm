(define-module (crates-io pr et pretty-csv) #:use-module (crates-io))

(define-public crate-pretty-csv-0.1.0 (c (n "pretty-csv") (v "0.1.0") (d (list (d (n "csvstream") (r "^0.2.0") (d #t) (k 0)))) (h "11ij2jvy57lbvnvan1vkmfgcssi3zigxd6cpa930ms9sl0a0z3ci")))

(define-public crate-pretty-csv-0.2.0 (c (n "pretty-csv") (v "0.2.0") (d (list (d (n "csvstream") (r "^0.2.0") (d #t) (k 0)))) (h "0y52dms693kb4msyc9l6aprb2454nyv7zcjjfdw7yz8d1n316m63")))

