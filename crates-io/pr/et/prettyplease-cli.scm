(define-module (crates-io pr et prettyplease-cli) #:use-module (crates-io))

(define-public crate-prettyplease-cli-0.1.0-rc1 (c (n "prettyplease-cli") (v "0.1.0-rc1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1bbcfk4mzvcvf61pkfybgn48fch58qi7bjp7r9dz3ma8jq27111r") (f (quote (("default" "check-shows-diffs") ("check-shows-diffs" "prettydiff") ("better-docs"))))))

(define-public crate-prettyplease-cli-0.1.0-rc2 (c (n "prettyplease-cli") (v "0.1.0-rc2") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1hzifl4kqz5087l3bs8ppw1nmbhzx94m5djynd1lkx5r96hgkp44") (f (quote (("default" "check-shows-diffs") ("check-shows-diffs" "prettydiff"))))))

(define-public crate-prettyplease-cli-0.1.0 (c (n "prettyplease-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "prettydiff") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1krslsl0bxldwds4r7w74gzbqnnmc6psn9hxb89qmbj1mvdas4b8") (f (quote (("default" "check-shows-diffs") ("check-shows-diffs" "prettydiff"))))))

