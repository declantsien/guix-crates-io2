(define-module (crates-io pr et prettythanks) #:use-module (crates-io))

(define-public crate-prettythanks-0.1.0 (c (n "prettythanks") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "camino") (r "^1.1") (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (k 0)))) (h "0di0gpcw9kxgzjzf73z2ylkfscqmdin09wr05n31r1pwdphw7g9p") (r "1.56")))

