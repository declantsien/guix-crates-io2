(define-module (crates-io pr et pretty-print) #:use-module (crates-io))

(define-public crate-pretty-print-0.0.0 (c (n "pretty-print") (v "0.0.0") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (k 0)))) (h "0kwsbxfks96nf1p26y8kr4jyngx4qlsbvnxfkqyp19c2lv3p4q9l") (f (quote (("std") ("default"))))))

(define-public crate-pretty-print-0.1.0 (c (n "pretty-print") (v "0.1.0") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (k 0)))) (h "1z8l3y86g1xgfaqqy2xfn2p3glqkgycz4hlqkz7f5p8sa1pb6hdb") (f (quote (("std") ("default"))))))

(define-public crate-pretty-print-0.1.1 (c (n "pretty-print") (v "0.1.1") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (k 0)))) (h "0r6r95s22hzdl0pqfshl7nc6vqiwc3204i1r0g1bm9ic2knknf7k") (f (quote (("std") ("default"))))))

(define-public crate-pretty-print-0.1.2 (c (n "pretty-print") (v "0.1.2") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (k 0)))) (h "0lfq89892lpcd77jfs029mm0z60j2b1f6n3aznk1ixqsazwlb306") (f (quote (("std") ("default"))))))

(define-public crate-pretty-print-0.1.3 (c (n "pretty-print") (v "0.1.3") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (k 0)) (d (n "termcolor") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "09g4lxpma0g3qh5snfx20mb06rl6i4ca1j96f3c898ixpqn85qa8") (f (quote (("std") ("default" "std" "termcolor"))))))

(define-public crate-pretty-print-0.1.4 (c (n "pretty-print") (v "0.1.4") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1f04fndvqv7ds56xk1cqpclg8i8mv2xc4gpp3p60jfr6jh6dgs2p") (f (quote (("std") ("default" "std"))))))

(define-public crate-pretty-print-0.1.5 (c (n "pretty-print") (v "0.1.5") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "030z27v5wybgm7ifkkibv9qnkr332ni9wkfw0w63h06n83hn3kwd") (f (quote (("std") ("default" "std"))))))

(define-public crate-pretty-print-0.1.6 (c (n "pretty-print") (v "0.1.6") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1icr0fclb2j5l9izv7vlvp0pxpax9mbg2jrwq584jrmwv5s5xpa9") (f (quote (("std") ("default" "std"))))))

(define-public crate-pretty-print-0.1.7 (c (n "pretty-print") (v "0.1.7") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0a7spx4s0czyj0q2k5av8ayfn69xyn59c6ydjk9ljvgb6jgypjrh") (f (quote (("std") ("default" "std"))))))

(define-public crate-pretty-print-0.1.8 (c (n "pretty-print") (v "0.1.8") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "06jr766ljky4x225xr72ly872l94j7zi1qpazymziy69wyd1gj8m") (f (quote (("std") ("default" "std"))))))

(define-public crate-pretty-print-0.1.9 (c (n "pretty-print") (v "0.1.9") (d (list (d (n "color-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0gz693sc2y5rfa3fjw5a70v41rhywrjb3f8jwiy1w6gjhrjm3bf5") (f (quote (("std") ("default" "std"))))))

