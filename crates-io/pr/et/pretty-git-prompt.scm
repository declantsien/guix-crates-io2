(define-module (crates-io pr et pretty-git-prompt) #:use-module (crates-io))

(define-public crate-pretty-git-prompt-0.2.0 (c (n "pretty-git-prompt") (v "0.2.0") (d (list (d (n "clap") (r "^2.19") (k 0)) (d (n "git2") (r "^0.6") (k 0)) (d (n "yaml-rust") (r "^0.3.4") (d #t) (k 0)))) (h "12sl51ifqnv2l6l6278jd12k1iprlm1lkkvasda1sd2dkkzg2gmx")))

(define-public crate-pretty-git-prompt-0.2.1 (c (n "pretty-git-prompt") (v "0.2.1") (d (list (d (n "clap") (r "^2.19") (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "15wqnw0n1z7k6vs2w2wdrmvpf5bdmwsgmmbj84lzd8f4knywydq0")))

(define-public crate-pretty-git-prompt-0.2.2 (c (n "pretty-git-prompt") (v "0.2.2") (d (list (d (n "clap") (r "4.*") (f (quote ("std" "cargo"))) (k 0)) (d (n "git2") (r "^0.18") (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1p97jz8dpgqcckzl8mclync0wdj2i9xs8wrnndvy36wl7jjhsr2m")))

