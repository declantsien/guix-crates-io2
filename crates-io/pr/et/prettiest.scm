(define-module (crates-io pr et prettiest) #:use-module (crates-io))

(define-public crate-prettiest-0.1.0 (c (n "prettiest") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.25") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.28") (f (quote ("KeyboardEvent"))) (d #t) (k 2)))) (h "0y9l7dry8dswl4minvrpz7m5gajwnj29776cqqr5zylsq2jzjzzh")))

(define-public crate-prettiest-0.2.0 (c (n "prettiest") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.25") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.28") (f (quote ("Document" "Element" "Event" "EventTarget" "HtmlElement" "KeyboardEvent" "KeyboardEventInit" "Window"))) (d #t) (k 0)))) (h "0dpq0mp2h7jk73wbcrjyli13cdi0ivlnxfx9sywi4kqyrw436km1")))

(define-public crate-prettiest-0.2.1 (c (n "prettiest") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.25") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.28") (f (quote ("Document" "Element" "Event" "EventTarget" "HtmlElement" "KeyboardEvent" "KeyboardEventInit" "Window"))) (d #t) (k 0)))) (h "1q0xm27pzqgcw1jxpcwmmr2sg0mval1xfmxf249nd6x4ngdnx09w")))

