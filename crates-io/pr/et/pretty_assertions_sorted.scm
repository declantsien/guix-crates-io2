(define-module (crates-io pr et pretty_assertions_sorted) #:use-module (crates-io))

(define-public crate-pretty_assertions_sorted-1.0.0 (c (n "pretty_assertions_sorted") (v "1.0.0") (d (list (d (n "darrentsung_debug_parser") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "0qj6zrc81070fkhlg9whm11gby9l5l7417s0whq4ns6fkpr33x6w") (y #t)))

(define-public crate-pretty_assertions_sorted-1.0.1 (c (n "pretty_assertions_sorted") (v "1.0.1") (d (list (d (n "darrentsung_debug_parser") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "094g7lc61k629zhpxivqjj6z753k37bnza4dnf3lcq0rcqr98yxq") (y #t)))

(define-public crate-pretty_assertions_sorted-1.0.2 (c (n "pretty_assertions_sorted") (v "1.0.2") (d (list (d (n "darrentsung_debug_parser") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "15j32vrmh2g826grhwnl5b76ik5izjkyfpvwiwaz895cnzyi0vmr") (y #t)))

(define-public crate-pretty_assertions_sorted-1.1.0 (c (n "pretty_assertions_sorted") (v "1.1.0") (d (list (d (n "darrentsung_debug_parser") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "00qyd1y6c6nf66sl2648msnxvriplfbghbfcgam7y1spw86j1rf9") (y #t)))

(define-public crate-pretty_assertions_sorted-1.1.1 (c (n "pretty_assertions_sorted") (v "1.1.1") (d (list (d (n "darrentsung_debug_parser") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "0vyh389778z7sci2h2l0qdgjfn57g8gasbx0y367mlh2s161qi82")))

(define-public crate-pretty_assertions_sorted-1.1.2 (c (n "pretty_assertions_sorted") (v "1.1.2") (d (list (d (n "darrentsung_debug_parser") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "0n41ngzmacix7ymhjywb65543mr29gpwq6ln9h6wb6zqky6nx3p5")))

(define-public crate-pretty_assertions_sorted-1.2.0 (c (n "pretty_assertions_sorted") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "darrentsung_debug_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)))) (h "0g9pfwggh4sfrhslps971nyinhcgnf37q8ybnrsajvqziq9sxnwg")))

(define-public crate-pretty_assertions_sorted-1.2.1 (c (n "pretty_assertions_sorted") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "darrentsung_debug_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "17ll6kdvfvrmcgyspkcrnmy9ddpcgm7ksi55pxx843pwcbgpyf04")))

(define-public crate-pretty_assertions_sorted-1.2.2 (c (n "pretty_assertions_sorted") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "darrentsung_debug_parser") (r "^0.3.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "090s752kzlbxs7haw54h4xxh4r38hyknc99zlc2cm868kqhrgzxv") (y #t)))

(define-public crate-pretty_assertions_sorted-1.2.3 (c (n "pretty_assertions_sorted") (v "1.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "darrentsung_debug_parser") (r "^0.3.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "186679311rdg47f7igq7h3p2gic33ckk69ri63yvvbgjh8ld75gs")))

