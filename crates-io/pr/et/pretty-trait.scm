(define-module (crates-io pr et pretty-trait) #:use-module (crates-io))

(define-public crate-pretty-trait-0.1.0 (c (n "pretty-trait") (v "0.1.0") (h "1dfgqlz2i8yl741w966diihhly9gacq13fm52nrr4r4s023k703j")))

(define-public crate-pretty-trait-0.1.1 (c (n "pretty-trait") (v "0.1.1") (h "0vybi00xgvjw3q62ipnf13alvbv312crjghcj3jgmkdcf858v7p2")))

(define-public crate-pretty-trait-0.1.2 (c (n "pretty-trait") (v "0.1.2") (h "0fpk1b2gx99v3sk4l4hbdkyq1cvzniwaa0gw4k43cn335illvv6s")))

