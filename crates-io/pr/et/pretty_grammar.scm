(define-module (crates-io pr et pretty_grammar) #:use-module (crates-io))

(define-public crate-pretty_grammar-0.1.0 (c (n "pretty_grammar") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "18x1df0yj81yj9wzk1g4v1a4h8rgrkfdghdbarw9nc5cbk522gfy")))

(define-public crate-pretty_grammar-0.1.1 (c (n "pretty_grammar") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ghy4ibr2h4nwfw7y5d9822bspqhqjp0fgq2d0jp2xkmnjfphr9z") (y #t)))

(define-public crate-pretty_grammar-0.1.2 (c (n "pretty_grammar") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "12jlkylga8z1lxb03z9mvh7pzx89ajwa27x801bp7l2lq2grwlly") (y #t)))

(define-public crate-pretty_grammar-0.1.3 (c (n "pretty_grammar") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1s36422kvqmnpfwa6y29amd8zbqkzbgpinyyc63ak6xdgg8mf6z3")))

