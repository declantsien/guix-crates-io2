(define-module (crates-io pr et pretty_assertions) #:use-module (crates-io))

(define-public crate-pretty_assertions-0.1.0 (c (n "pretty_assertions") (v "0.1.0") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "19hdjxg6w6r52ikdb9wmpx6ivyhyih9gfqq1iifd2ii7gf26jf26")))

(define-public crate-pretty_assertions-0.1.1 (c (n "pretty_assertions") (v "0.1.1") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "05bhlvaaffwp9hkib68km7z261y0qcxaxwjz1ayhsslp8f4fvvpf")))

(define-public crate-pretty_assertions-0.1.2 (c (n "pretty_assertions") (v "0.1.2") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "0vkbkw45xxg5j01lgbaqjw51hcjg3368v63806js5iq758rz64i4")))

(define-public crate-pretty_assertions-0.2.0 (c (n "pretty_assertions") (v "0.2.0") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "0d3jma6d074yl18c2x5p4yz0wv6ymw57dphllyiys0wk9sw7dprj")))

(define-public crate-pretty_assertions-0.2.1 (c (n "pretty_assertions") (v "0.2.1") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "1b3nv70i16737w3qkk1q5vqswwnb19znz8r9v2kcg1qyhh3h0l8x")))

(define-public crate-pretty_assertions-0.3.0 (c (n "pretty_assertions") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "10x0v97mjms2h86siskazkfm9vy3bv1p4fd4jd0wvm39nbdy6xr9")))

(define-public crate-pretty_assertions-0.3.1 (c (n "pretty_assertions") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "04qlqh9v8590xzsq02csiw783nbnp3sxvfz6idn8w33424n1s908")))

(define-public crate-pretty_assertions-0.3.2 (c (n "pretty_assertions") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "0jndbr6d14x7yyzyicqac2dv3j52fq4n57hcsjgwfss2pvhvhhhg")))

(define-public crate-pretty_assertions-0.3.3 (c (n "pretty_assertions") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "0cv2a94j39qq87m48fvj6f8qlrz6pn2lj334yhx41ivv53rv318i")))

(define-public crate-pretty_assertions-0.3.4 (c (n "pretty_assertions") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "13b34fv3sp2div3wklr7mqm0y5s3hd8yn07rnsp1k7msz2kl9p5m")))

(define-public crate-pretty_assertions-0.4.0 (c (n "pretty_assertions") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "158ldkwc4zqp4w4dgbcpbr0bhbsja0cf5packh0rmn13lg4bpdll")))

(define-public crate-pretty_assertions-0.4.1 (c (n "pretty_assertions") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^1.0.0") (d #t) (k 0)))) (h "1llxlnhh4qz9kda27v6nllgzvgi1fv08i3djfk4zn6zlw8c53si8")))

(define-public crate-pretty_assertions-0.5.0 (c (n "pretty_assertions") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0zk7pvhhzcbc2irsk28mw2qlgfkqyqiik39fy9601klvf6h8cdv9")))

(define-public crate-pretty_assertions-0.5.1 (c (n "pretty_assertions") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1ins6swkpxmrh8q5h96h8nv0497d3dclsiyx2lyvqi6py0q980is")))

(define-public crate-pretty_assertions-0.6.0 (c (n "pretty_assertions") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "ctor") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1xl7929qrn0msn74gjv2z5hvcb3p3zdppf217008rl42k14qixrw")))

(define-public crate-pretty_assertions-0.6.1 (c (n "pretty_assertions") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "ctor") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "09yl14gnmpygiqrdlsa64lcl4w6ydjl9m8jri6kgam0v9rjf309z")))

(define-public crate-pretty_assertions-0.7.0 (c (n "pretty_assertions") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0rgfaw9hab6qwbaw0v0jdy49pryi5vz2wxp5i60p5xfczir8s6rc")))

(define-public crate-pretty_assertions-0.7.1 (c (n "pretty_assertions") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1101lnyl9qf3davi8v6h6b93m25b101n43ibvr2qvpx74wn595zj")))

(define-public crate-pretty_assertions-0.7.2 (c (n "pretty_assertions") (v "0.7.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "12yris0ni87wvzhj23a5nnz7amskvahhnpikx5snhdyg09y0xaqw")))

(define-public crate-pretty_assertions-1.0.0 (c (n "pretty_assertions") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1k01jc9gxgiyqbd9445q7x4kw2pf0q4m0ki31yx75w834hdzw37c") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

(define-public crate-pretty_assertions-1.1.0 (c (n "pretty_assertions") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0l2xpgqa1a73fkbacn0qxngixwmyp1fb90k496sql095nx4bbmbn") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

(define-public crate-pretty_assertions-1.2.0 (c (n "pretty_assertions") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "12r2zd27kr3wg2i1i0a3mp0gwnr73lk7q8lwpw2cgf8rag5kih2p") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

(define-public crate-pretty_assertions-1.2.1 (c (n "pretty_assertions") (v "1.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0qrmkdwqn56af498vi8zjyq44wzcyvj5ic1dv54d01s2r6d9i7y8") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

(define-public crate-pretty_assertions-1.3.0 (c (n "pretty_assertions") (v "1.3.0") (d (list (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0mgp1ajl3fdc55h989ph48znnk86m41j9dqnpg80yy5a435rnpm2") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

(define-public crate-pretty_assertions-1.4.0 (c (n "pretty_assertions") (v "1.4.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0rmsnqlpmpfjp5gyi31xgc48kdhc1kqn246bnc494nwadhdfwz5g") (f (quote (("unstable") ("std") ("default" "std") ("alloc"))))))

