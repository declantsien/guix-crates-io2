(define-module (crates-io pr et pretable) #:use-module (crates-io))

(define-public crate-pretable-0.1.0 (c (n "pretable") (v "0.1.0") (h "07sflqpifjijg1cc00525xx0n068k1xdzyxb28ab6y8kf4wgzlvk")))

(define-public crate-pretable-0.2.0 (c (n "pretable") (v "0.2.0") (h "0mcz3fsnhm2s1kpg98mddhcy9j977haxv76ss1rsirfvf6b9zr7d")))

(define-public crate-pretable-0.3.0 (c (n "pretable") (v "0.3.0") (h "08dfwqhilvmfhrjsksqd8bp7g61fs918fv2riq41g6ldv33yw52y")))

(define-public crate-pretable-0.3.1 (c (n "pretable") (v "0.3.1") (h "1ki18dcyz5asjfldkhqf0vgd8ghb5j3d3i26pw3k87g0lkv33kr6")))

(define-public crate-pretable-0.3.2 (c (n "pretable") (v "0.3.2") (h "1yb3sc82il533ibmssygjrqfc1daf5k127h133s6pa3wbcb5hxvb")))

(define-public crate-pretable-0.3.3 (c (n "pretable") (v "0.3.3") (h "0awl0g4b2pv3ynl0brwn8vigsa0myl4m8akc6dxbsv5f1fngh9g5")))

(define-public crate-pretable-0.4.0 (c (n "pretable") (v "0.4.0") (h "1a7zcpm6s1jf6g8bdbsv21ncf6j3smjqbaf7j0gzhax56yz7lhxa")))

(define-public crate-pretable-0.4.1 (c (n "pretable") (v "0.4.1") (h "0pk109s0vh6y9vwk2zhrw55j3r4lz58lgjqdcmx4wfxk5v72fija")))

(define-public crate-pretable-0.4.2 (c (n "pretable") (v "0.4.2") (h "1zm8dcsd7gs8k6djmwdi7qk1gxphwbiakp42n3smcckq07vjhpmw")))

