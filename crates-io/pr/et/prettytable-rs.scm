(define-module (crates-io pr et prettytable-rs) #:use-module (crates-io))

(define-public crate-prettytable-rs-0.1.0 (c (n "prettytable-rs") (v "0.1.0") (h "1clzqprsp45h657mbfbsb93km3ip2141dj0cms8853qadwj72yx5")))

(define-public crate-prettytable-rs-0.1.1 (c (n "prettytable-rs") (v "0.1.1") (h "0ygaa6d87lqdg0wm09gqk6nah1xx4929x4ivgsig32s841xsc7z5")))

(define-public crate-prettytable-rs-0.1.2 (c (n "prettytable-rs") (v "0.1.2") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1s6v97r2vj67czvndw7vay7qi37x3hd4xq0mn504g7djr13p25nr")))

(define-public crate-prettytable-rs-0.2.0 (c (n "prettytable-rs") (v "0.2.0") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1s2g6qq8512mprdwzbvxnimll40c9jx4wll5w0bys81ws5mxfmq1")))

(define-public crate-prettytable-rs-0.3.0 (c (n "prettytable-rs") (v "0.3.0") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "0ivwhrjx8myfn5531svw18g8vy667pfrmmhyp2n951jmxbqk565q")))

(define-public crate-prettytable-rs-0.4.0 (c (n "prettytable-rs") (v "0.4.0") (d (list (d (n "term") (r "*") (d #t) (k 0)) (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1gc7nkszkvyf5rg570gr4b8rgzdlj2nz9lw6mypzicvldg03rq2m")))

(define-public crate-prettytable-rs-0.5.0 (c (n "prettytable-rs") (v "0.5.0") (d (list (d (n "term") (r "*") (d #t) (k 0)) (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1bgp4dbvnmgic049857xz3awzxj965qqfkkr98aam5idc8pk3r2n")))

(define-public crate-prettytable-rs-0.5.1 (c (n "prettytable-rs") (v "0.5.1") (d (list (d (n "term") (r "^0.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1bikirkq5skaf0flq1isfrs605y1vxw2zvz5mgxffdyz9qbpks5n")))

(define-public crate-prettytable-rs-0.5.2 (c (n "prettytable-rs") (v "0.5.2") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1l69sbks0bvzpm3w229xdkxk6ggapigmn3x68m54hyar905q1ddm")))

(define-public crate-prettytable-rs-0.6.0 (c (n "prettytable-rs") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "02qhxnxxi78hsdjy7y315brg5np74ad033a7frwq3gq5sjj7r3bn")))

(define-public crate-prettytable-rs-0.6.1 (c (n "prettytable-rs") (v "0.6.1") (d (list (d (n "atty") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "05f4pvjnlrw9lx4cpwdiq9kfy7110gw9a27c7vr8qgdhsa73zjrg")))

(define-public crate-prettytable-rs-0.6.2 (c (n "prettytable-rs") (v "0.6.2") (d (list (d (n "atty") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0v35jxks87iiwy5n8kyhb8vz5g3l1ibi4cnj9g3lb7mp4bnkhi1m")))

(define-public crate-prettytable-rs-0.6.3 (c (n "prettytable-rs") (v "0.6.3") (d (list (d (n "atty") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "13cdh8vkc98jz78pcjwxjr9fb42mysqfc9scr7hn9bdmnkh9q3av") (f (quote (("win_crlf") ("default" "win_crlf"))))))

(define-public crate-prettytable-rs-0.6.4 (c (n "prettytable-rs") (v "0.6.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "encode_unicode") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1ghx9qwnbfqzyng4lxxfhvv8lg8nyalsk93xc89cki2wv82vaygq") (f (quote (("win_crlf") ("default" "win_crlf"))))))

(define-public crate-prettytable-rs-0.6.5 (c (n "prettytable-rs") (v "0.6.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1xvc9bcbmdbkbalvls48rig9hn8pfmq9jnkyn2nzk89ngzd5029h") (f (quote (("win_crlf") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.6.6 (c (n "prettytable-rs") (v "0.6.6") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1r65pzyigx4cn43pw0lmgz8s8vbjwn4w3361gimx9fywmazi31b8") (f (quote (("win_crlf") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.6.7 (c (n "prettytable-rs") (v "0.6.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0pzz6jh52krjq4s3y4mgy3xqbd9ilv94zzgc13q3payxdm7izp1l") (f (quote (("win_crlf") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.7.0 (c (n "prettytable-rs") (v "0.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "17nd0vhvrwhmyx7g828cnq448rhx4cipkgpnpw55z8ssh16cl4am") (f (quote (("win_crlf") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.8.0 (c (n "prettytable-rs") (v "0.8.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0bmcsxkcy94hi0jz5db0fz137w5aaf17z2j1ryn2vyh400blpl0g") (f (quote (("win_crlf") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.9.0 (c (n "prettytable-rs") (v "0.9.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "00gqr25ya7cc870pk6abkm0bdxgvn54fpzbzjciivd939jvmqdsz") (f (quote (("win_crlf") ("evcxr") ("default" "win_crlf" "csv"))))))

(define-public crate-prettytable-rs-0.10.0 (c (n "prettytable-rs") (v "0.10.0") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "encode_unicode") (r "^1.0") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0nnryfnahfwy0yxhv4nsp1id25k00cybx3ih8xjsp9haa43mx8pf") (f (quote (("win_crlf") ("evcxr") ("default" "win_crlf" "csv"))))))

