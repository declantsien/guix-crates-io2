(define-module (crates-io pr ct prctl) #:use-module (crates-io))

(define-public crate-prctl-0.0.1 (c (n "prctl") (v "0.0.1") (h "0ww3h7szgpjhwn6xpmclgpcpznlgif4qa3jzrvg68f3vn8a15wxm") (f (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.0.2 (c (n "prctl") (v "0.0.2") (h "0isbv1g5gxkzswzbmxbg7pfqb5sgwmas9az8x6j3b5xgmy0c632k") (f (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.1.0 (c (n "prctl") (v "0.1.0") (h "0h9m4rcibw9gyix8mbf57bn05xiwr8639f5rnbiw5089xwxkcd2i") (f (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.1.1 (c (n "prctl") (v "0.1.1") (h "0c183lw16wc24fb0lqi1d70bkbpfpaxxfmls2fzd7prykzz7sbha") (f (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.2.0 (c (n "prctl") (v "0.2.0") (h "1gpknzwnrwaqrl40x4yc20zb048b05gp2m4cqzlv7hm4qaavk49g") (f (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-1.0.0 (c (n "prctl") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)))) (h "0lkgnid3sjfbqf3sbcgyihlw80a6n9l6m0n23b7f5pm927qk96h5") (f (quote (("root_test") ("not_travis"))))))

