(define-module (crates-io pr ev previuwu) #:use-module (crates-io))

(define-public crate-previuwu-0.0.0 (c (n "previuwu") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.18.0") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.18.0") (f (quote ("image"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)))) (h "19h9wdxynal5fa55bgbmgxsc9kv4kq6nsjjaq60gnxzqd8i9583n") (y #t)))

