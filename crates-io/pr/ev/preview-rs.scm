(define-module (crates-io pr ev preview-rs) #:use-module (crates-io))

(define-public crate-preview-rs-0.1.0 (c (n "preview-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1q448w12z583490slaacq908j59716jz8gfm0fb1lqxk61i39wvd")))

(define-public crate-preview-rs-0.1.1 (c (n "preview-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0zn1cgyy8bzliyd2j3idsyhpl8zwl9bbpizrdq2lixl4mp3jicmd")))

(define-public crate-preview-rs-0.1.2 (c (n "preview-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "03rsz3v3mk5liw8xz90xcdacamdr66rfan2yzfkm4zklw8cw4nlq")))

(define-public crate-preview-rs-0.1.3 (c (n "preview-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0jqp9smy156zyp1zgshkjy99mamzb7ailizgnjml51kg6vgp73ck")))

