(define-module (crates-io pr pc prpc-serde-bytes) #:use-module (crates-io))

(define-public crate-prpc-serde-bytes-0.1.0 (c (n "prpc-serde-bytes") (v "0.1.0") (d (list (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "rustfmt-snippet") (r "^0.1.1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1x8ivmnhx8qm5w69ljylsvc14g86v4nb1gj2rfyzdnvf0rahib3g")))

