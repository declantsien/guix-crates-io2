(define-module (crates-io pr iv privie) #:use-module (crates-io))

(define-public crate-privie-0.1.0 (c (n "privie") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "crypto_box") (r "^0.6") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1h1imwnxl82zm1159nspqdvqsbjq9wzy051z1q6sbwd184vhg4y5")))

