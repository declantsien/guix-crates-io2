(define-module (crates-io pr iv priv_sep) #:use-module (crates-io))

(define-public crate-priv_sep-0.1.0 (c (n "priv_sep") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (t "cfg(openbsd)") (k 0)))) (h "0ybn4cs4lbyqf2jz1knkzfqxf7x7fjyxsi8295f8rfjs9wzw8wra")))

(define-public crate-priv_sep-0.2.0 (c (n "priv_sep") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0f21d6112whnysmmqhcpf32jjp25rggrpzpcvnmgab63djmfy5al") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.3.0 (c (n "priv_sep") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "17vjhgfhwwxzwwmyb61p1rcjnsqbnihvkhs00h3fsbinsqcz42xy") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.4.0 (c (n "priv_sep") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0yrz9qiabh1xa8jghprn961jpycgziidkd8661dzpj4vnw9vks6k") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.4.1 (c (n "priv_sep") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.147") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "125fqq8vcsirvf3n2iyvhk0rbfc36vs5ssrzny61vi8zxhrv531s") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.5.0 (c (n "priv_sep") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "08fra006ln77jw8w28wpl70k5ji88rzm2q16yzl74lck6256p6z0") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.5.1 (c (n "priv_sep") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1wli6ahhy1db427awcr7ilmx47dvn0hiahlhi1r5ak6zfi3jpl97") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.6.0 (c (n "priv_sep") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "08y5pa9q0h6paxgwq1a79x342z44r10np5g458cqiz9hngkqmhj8") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.6.1 (c (n "priv_sep") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1dj61xlahd77k04c0ahcs3c389kfqpr6p3jn00rkx9brflrskg5n") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.7.0 (c (n "priv_sep") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.149") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "054kpfwlk8ap1scng9yfvcgmbb7d2nirby41k3w2m6ja7zfi8liw") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.8.0 (c (n "priv_sep") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.149") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1ynsnykcisfl4nx75nmiy77qhv92wblczsr4qbajnlvc6s6bbz4d") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-0.8.1 (c (n "priv_sep") (v "0.8.1") (d (list (d (n "libc") (r "^0.2.150") (f (quote ("std"))) (o #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1i11fvp5amjx6n8v7gra9l8gmj9sqpjzj4m14w0j9vsxlswi4wvd") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-1.0.0 (c (n "priv_sep") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.153") (f (quote ("std"))) (o #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0q8a92q9jjp5mnld6b4kpr0a1nzyfl27qvi0i2vb2p7f6x0qs9qp") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

(define-public crate-priv_sep-1.0.1 (c (n "priv_sep") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.153") (f (quote ("std"))) (o #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "16wfg3vxkabcbmmlwnkpara1pny33cn5sm4pr0ka37zkc8zz7g43") (f (quote (("default" "openbsd")))) (s 2) (e (quote (("openbsd" "dep:libc"))))))

