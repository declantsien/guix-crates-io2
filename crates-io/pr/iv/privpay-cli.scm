(define-module (crates-io pr iv privpay-cli) #:use-module (crates-io))

(define-public crate-privpay-cli-0.1.0 (c (n "privpay-cli") (v "0.1.0") (d (list (d (n "bip351") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "secstr") (r "^0.5.1") (d #t) (k 0)))) (h "0cwf52045zj3lgdln8ksnkxb1nn6prsl4crg6xl1s0lwhf69ipci") (y #t) (r "1.65")))

