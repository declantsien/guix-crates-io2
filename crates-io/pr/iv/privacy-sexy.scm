(define-module (crates-io pr iv privacy-sexy) #:use-module (crates-io))

(define-public crate-privacy-sexy-0.1.0 (c (n "privacy-sexy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)))) (h "1fylc3cgp4l7ac01yn1dxaj9wdpkf7yx3jy81m8nigdxkqp5w6jy")))

(define-public crate-privacy-sexy-0.1.1 (c (n "privacy-sexy") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.11") (d #t) (k 0)))) (h "03sz7xpjx1lmnzni6k7dn74a0w29yv0qwl58hdji7dm8msc1mwif")))

(define-public crate-privacy-sexy-0.2.0 (c (n "privacy-sexy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "13l8czrgawcjjklm7g0d153glz7jgdws1qxagdb5bhiymgzirxjj")))

