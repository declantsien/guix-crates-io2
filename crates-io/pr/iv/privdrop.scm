(define-module (crates-io pr iv privdrop) #:use-module (crates-io))

(define-public crate-privdrop-0.1.0 (c (n "privdrop") (v "0.1.0") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.7") (d #t) (k 0)))) (h "0309gb3gv7z9qir8cjrqhsljpf295imipkx8vsjy2sgzifg2c4jn")))

(define-public crate-privdrop-0.1.1 (c (n "privdrop") (v "0.1.1") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.7") (d #t) (k 0)))) (h "0qqi0ws9ihff85b2imbd4clvgkm5s9d835wdg2cg1yp5cmgc7dlf")))

(define-public crate-privdrop-0.1.2 (c (n "privdrop") (v "0.1.2") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.7") (d #t) (k 0)))) (h "036scc8696drvp6liwxgd2b981f993szzf7lhjips69n0z8zncsz")))

(define-public crate-privdrop-0.1.3 (c (n "privdrop") (v "0.1.3") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.7") (d #t) (k 0)))) (h "0blvsqlva7gagaj5bsfq04bzr1yf3an537fpvgj3k9ifml26cp8m")))

(define-public crate-privdrop-0.2.0 (c (n "privdrop") (v "0.2.0") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.7") (d #t) (k 0)))) (h "011dz510759wdikdqdqsbzxqywx6lbrgmicpqdwn4rih966n9vkg")))

(define-public crate-privdrop-0.2.1 (c (n "privdrop") (v "0.2.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.11") (d #t) (k 0)))) (h "13kin7jdani0j1a27pydyrcwg8ckrwxnggx9hngvs2hv3hsxxyi4")))

(define-public crate-privdrop-0.2.2 (c (n "privdrop") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)))) (h "07brvgwb8v7j3f6nk9y3wj4bhs7ng97b1ziph949w1wgdiz5s9rf")))

(define-public crate-privdrop-0.3.0 (c (n "privdrop") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)))) (h "1dsxhcp342y2q48cd0gxl9xg0642idxc0c431jdgxaapjg17zqcd")))

(define-public crate-privdrop-0.3.1 (c (n "privdrop") (v "0.3.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.11") (d #t) (k 0)))) (h "1aqyh9swygn0ffkq43qq2pq7x0p6pgfn1djxcsvw1jvhmrlhvbhi")))

(define-public crate-privdrop-0.3.2 (c (n "privdrop") (v "0.3.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.14") (d #t) (k 0)))) (h "1ghks4h23km1j87j63rcr2k9cs84g5swrmsg77n2w8bvrrwjg049")))

(define-public crate-privdrop-0.3.3 (c (n "privdrop") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "1ad53kw40pqpkbdbqisnrn6c8jg3ml3via8h8z0y6mlxdld2wb23")))

(define-public crate-privdrop-0.3.4 (c (n "privdrop") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)))) (h "0ydlnacgk9x43fm5d9ap2z54v8gjydbyvcrgrxjmrhgrzv5sg7wk")))

(define-public crate-privdrop-0.4.0 (c (n "privdrop") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "11r2hyw9v6cns38lnwzll8idc2wdg4f1q2v71gl7msbsjl4f780m")))

(define-public crate-privdrop-0.5.0 (c (n "privdrop") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "11wc09kc7wlvlb6v6fc729a1wp64xrdm9nf0s4wh9rs2jrrw5m7b")))

(define-public crate-privdrop-0.4.1 (c (n "privdrop") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)))) (h "01cafsamnkskkf4i508q4r2lwvgwsjfsdw8wnal02v182hqy7l4j")))

(define-public crate-privdrop-0.4.2 (c (n "privdrop") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "0vk7gbcx3c5jh5rlnrgv905viwa5yfqak8hmh2bx1mk5g9qdzxxb")))

(define-public crate-privdrop-0.4.3 (c (n "privdrop") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1hl9qzxqfad0h092m1h7cfrnp3q96sn3jiplji6ll4nq8ad2v201")))

(define-public crate-privdrop-0.5.1 (c (n "privdrop") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "19d6ipf5i3pffd3fiif3d41pxsjx1miiknyaph3vir0hgcjwy0jc")))

(define-public crate-privdrop-0.5.2 (c (n "privdrop") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "14zfgjznmpnajv5zv5qrqrn0d0xy2grx6h0fz7wa4a7n81z1ynxd")))

(define-public crate-privdrop-0.5.3 (c (n "privdrop") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "15pxli18w3mzjjmkxqvn7ndz1ibl2ibj36lrvqn5ybfq6xa9xvc1")))

(define-public crate-privdrop-0.5.4 (c (n "privdrop") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1yab0dakzrh633vm96qp8jn8sa8a8njax3s8d6dysdjmjgijvhcv")))

