(define-module (crates-io pr iv privilege_level) #:use-module (crates-io))

(define-public crate-privilege_level-0.1.0 (c (n "privilege_level") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.5") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0bqxz1m87lq4zg0pgb7whz10bw4b23xij7659xb012iiq2fhym8f")))

(define-public crate-privilege_level-0.1.1 (c (n "privilege_level") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.5") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0asqjgkvmgzrfnjrd8ml6wp3qrc2f5ygybnx3cjsgb9mmbd2r0yi")))

