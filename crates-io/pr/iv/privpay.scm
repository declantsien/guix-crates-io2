(define-module (crates-io pr iv privpay) #:use-module (crates-io))

(define-public crate-privpay-0.1.0 (c (n "privpay") (v "0.1.0") (d (list (d (n "bip351") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "secstr") (r "^0.5.1") (d #t) (k 0)))) (h "0sxfnjqb3ksz1wxcmb2yqyghy5ffs2yqc9wm6r9flaw0k1l3jfh8") (r "1.65")))

(define-public crate-privpay-0.2.0 (c (n "privpay") (v "0.2.0") (d (list (d (n "bip351") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "secstr") (r "^0.5.1") (d #t) (k 0)))) (h "0k8bg56a6jwk6l7k16ah24py0ylws65pl2rs8x0cqjbk64s5bibz") (r "1.65")))

