(define-module (crates-io pr iv privsep-derive) #:use-module (crates-io))

(define-public crate-privsep-derive-0.0.1-test.2 (c (n "privsep-derive") (v "0.0.1-test.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "14s4flgv5rrkb4l1njvcwz3anqax5xr99y8w3qkyg99m8szlqn3s") (y #t)))

(define-public crate-privsep-derive-0.0.1-test.3 (c (n "privsep-derive") (v "0.0.1-test.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0n8qq492z1pl64xz94cmxx0k7f6zdlsgnnixpghkwybn9kxcmd8j") (y #t)))

(define-public crate-privsep-derive-0.0.1-test.4 (c (n "privsep-derive") (v "0.0.1-test.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "17dsznjd06rrp5ffa9j28xscwmm4fsbi2lwzfh2g0w034x8gzcbl")))

(define-public crate-privsep-derive-0.0.1-test.5 (c (n "privsep-derive") (v "0.0.1-test.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0s7samyr2yk35xs3n8yrvvb51n7q2qgsxkkyvb4yq1195h3b2lw8")))

(define-public crate-privsep-derive-0.0.1-test.6 (c (n "privsep-derive") (v "0.0.1-test.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "1kwapy4rpjr30y2jh36xy6bllakmzbcczn2vc3r91j4wvsniianr")))

(define-public crate-privsep-derive-0.0.1-test.7 (c (n "privsep-derive") (v "0.0.1-test.7") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "00fk6xkmzvqbzr3jvhp4c96yxj3qag1vjv2b8xcl3sn248pwaczs")))

(define-public crate-privsep-derive-0.0.1 (c (n "privsep-derive") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0fk5n9zkbhrajbm0qja2xrs6qy11xybj25pj0vbyq96xikndngh9")))

