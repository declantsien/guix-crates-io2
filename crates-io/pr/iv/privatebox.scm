(define-module (crates-io pr iv privatebox) #:use-module (crates-io))

(define-public crate-privatebox-0.1.0 (c (n "privatebox") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.8.0") (f (quote ("xchacha20poly1305"))) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (k 0)) (d (n "zeroize") (r "^1.3.0") (k 0)))) (h "16hly3934373l39ck109w58vvlrqp93y81s2823yh8mcd55hzbvb") (f (quote (("default" "alloc") ("alloc" "chacha20poly1305/alloc")))) (y #t)))

(define-public crate-privatebox-0.1.1 (c (n "privatebox") (v "0.1.1") (d (list (d (n "chacha20poly1305") (r "^0.8.0") (f (quote ("xchacha20poly1305"))) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (k 0)) (d (n "zeroize") (r "^1.3.0") (k 0)))) (h "08q1z6vfypvm6pl6lsx15l0xhpvhww3xqvkpg0gl65fj1s9gszp5") (f (quote (("default" "alloc") ("alloc" "chacha20poly1305/alloc"))))))

