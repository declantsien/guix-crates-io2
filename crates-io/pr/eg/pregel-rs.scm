(define-module (crates-io pr eg pregel-rs) #:use-module (crates-io))

(define-public crate-pregel-rs-0.0.1 (c (n "pregel-rs") (v "0.0.1") (d (list (d (n "duckdb") (r "^0.7.1") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "polars") (r "^0.28.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1l56k0j40j7fa5ppj7w9n9rchsgr8zsjprzggpanf59ch9gmffma")))

(define-public crate-pregel-rs-0.0.2 (c (n "pregel-rs") (v "0.0.2") (d (list (d (n "duckdb") (r "^0.7.1") (d #t) (k 0)) (d (n "duckdb") (r "^0.7.1") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "polars") (r "^0.28.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "0kbrms9h3q40625mkis3wx7ki7qsprdj3a9027rw7zns1j8ky67h")))

(define-public crate-pregel-rs-0.0.3 (c (n "pregel-rs") (v "0.0.3") (d (list (d (n "polars") (r "^0.28.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1b4ylrjhhmcjaywcd4lyvkgkpdkqvs1vpg6ghmjxxq9z2f0p2332")))

(define-public crate-pregel-rs-0.0.4 (c (n "pregel-rs") (v "0.0.4") (d (list (d (n "polars") (r "^0.28.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "0b8wg14mzy5yv8gak496n5bi71r2km1yqw35sr025cdyf5b55vgn")))

(define-public crate-pregel-rs-0.0.5 (c (n "pregel-rs") (v "0.0.5") (d (list (d (n "polars") (r "^0.28.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "1yd881a1jpq60fzg3j3bv1n6nfbm37g6vpv12np1kcdwpdl1cm70")))

(define-public crate-pregel-rs-0.0.6 (c (n "pregel-rs") (v "0.0.6") (d (list (d (n "polars") (r "^0.29.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "0426z4dhr85xy89r512grxxxz7mb9pcmnjaj08ibhv875g25fk4i")))

(define-public crate-pregel-rs-0.0.7 (c (n "pregel-rs") (v "0.0.7") (d (list (d (n "polars") (r "^0.29.0") (f (quote ("lazy"))) (d #t) (k 0)))) (h "15qpyx7ssyffadjfvqswy6b83rrxacmv9zv813qbrgg5f34xmph9")))

(define-public crate-pregel-rs-0.0.8 (c (n "pregel-rs") (v "0.0.8") (d (list (d (n "polars") (r "^0.29.0") (f (quote ("lazy" "streaming"))) (d #t) (k 0)))) (h "0qbgp9h2d8zfbbanf2q0gyjl3c1ms05llyi8b8vvznca7011zkc5")))

(define-public crate-pregel-rs-0.0.9 (c (n "pregel-rs") (v "0.0.9") (d (list (d (n "polars") (r "^0.29.0") (f (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (d #t) (k 0)))) (h "0qvvv5wh18yxgxybincj0g9nw99c90nzvl9rf421k3y888pap3rm")))

(define-public crate-pregel-rs-0.0.10 (c (n "pregel-rs") (v "0.0.10") (d (list (d (n "polars") (r "^0.30.0") (f (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (d #t) (k 0)))) (h "0q44wvxcc2pp9pkbcn111sqim4g6hxz2xk19fmm5qzha41k0lbcd")))

(define-public crate-pregel-rs-0.0.11 (c (n "pregel-rs") (v "0.0.11") (d (list (d (n "polars") (r "^0.30.0") (f (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (d #t) (k 0)))) (h "1w5wlm6564jlwbs19cfwa13az5nas053cm8h5qis2lkhzi70l90p")))

(define-public crate-pregel-rs-0.0.12 (c (n "pregel-rs") (v "0.0.12") (d (list (d (n "polars") (r "^0.30.0") (f (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (d #t) (k 0)))) (h "18xz8ahgjiihvs5cfg371s9dbpcyybzfr1h4c4mp8if3dax4m0vc")))

(define-public crate-pregel-rs-0.0.13 (c (n "pregel-rs") (v "0.0.13") (d (list (d (n "polars") (r "^0.30.0") (f (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (d #t) (k 0)))) (h "1v2znm7iy1579hnmkmqimv0d14alqa5g4m4mcb68f687fw81qkby")))

