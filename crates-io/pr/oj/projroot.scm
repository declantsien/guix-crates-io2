(define-module (crates-io pr oj projroot) #:use-module (crates-io))

(define-public crate-projroot-0.1.0 (c (n "projroot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03960igds7l8scqqvvccrqck1jnyh7c3665zbf50vbb3qkdcdagw")))

