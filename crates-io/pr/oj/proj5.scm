(define-module (crates-io pr oj proj5) #:use-module (crates-io))

(define-public crate-proj5-0.1.0 (c (n "proj5") (v "0.1.0") (d (list (d (n "scoped_threadpool") (r "^0.1.8") (d #t) (k 0)))) (h "05gjcwzjcd5xzvkxvyx2dmgazlwmhgs4hqpd2asyglr531z8c894")))

(define-public crate-proj5-0.1.1 (c (n "proj5") (v "0.1.1") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "0vfc411vzvhwqwj7dk6051jpnhnx877szswfmdy07718amvb01d0")))

(define-public crate-proj5-0.1.2 (c (n "proj5") (v "0.1.2") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "10pcimz7253v07hv4w3q0sb0iibrdhhxrwwbd5955i75873k6aky")))

(define-public crate-proj5-0.1.3 (c (n "proj5") (v "0.1.3") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "0wrx71dh6rvfapd3x8zl2hyknhq8ydp6aif9gm1hqq0m8qf4lf44")))

(define-public crate-proj5-0.1.4 (c (n "proj5") (v "0.1.4") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)))) (h "0wsgsjkw5iajkyqc1wsy9y8w9ljmp42m62k2vc9jwq8m25z7rpdd")))

(define-public crate-proj5-0.1.5 (c (n "proj5") (v "0.1.5") (d (list (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "148vrnc0cf43wchz1zap6v37jc9gvlp7hn051q9nizi78cy09sgr")))

(define-public crate-proj5-0.1.6 (c (n "proj5") (v "0.1.6") (d (list (d (n "scoped_threadpool") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1bnvii0b1jbgvda6y8x4n3zq7lwhhxq5rfnzzvalmrldabl5nrqv") (f (quote (("multithreading" "scoped_threadpool"))))))

(define-public crate-proj5-0.1.7 (c (n "proj5") (v "0.1.7") (d (list (d (n "scoped_threadpool") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1wxxk0a85q086wmrw07mgpggd943rzz6qz3kpri0im86r1j942b0") (f (quote (("multithreading" "scoped_threadpool"))))))

