(define-module (crates-io pr oj project14) #:use-module (crates-io))

(define-public crate-project14-0.1.0 (c (n "project14") (v "0.1.0") (h "0nrgi1vz5i92z94j8g7b5p03vyfmvjrycf59n68gpl6w9qhfv344")))

(define-public crate-project14-0.1.1 (c (n "project14") (v "0.1.1") (h "1hpamdb9i8dyicx7hbafrap8g6ydhd4kzdqxd3ac954gjvhifxj8")))

