(define-module (crates-io pr oj projectm) #:use-module (crates-io))

(define-public crate-projectm-1.0.5 (c (n "projectm") (v "1.0.5") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.7") (f (quote ("playlist"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "005d2wq2kg95y6zwz8j6pqhqzby16z9pmjsar546wrj3kf6kj50a") (f (quote (("playlist") ("default" "playlist")))) (r "1.65")))

(define-public crate-projectm-2.0.0-alpha (c (n "projectm") (v "2.0.0-alpha") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.8") (f (quote ("playlist"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dsjaff6rxvq160s449b4836fidp4shwfjibibwdcafdbmn1s0vf") (f (quote (("playlist") ("default" "playlist")))) (r "1.65")))

(define-public crate-projectm-2.0.1-alpha (c (n "projectm") (v "2.0.1-alpha") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.8") (f (quote ("playlist"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l573xsp81fgx41vdy4aac11p3r2s6ncvb36wwaffalxydbamjrl") (f (quote (("playlist") ("default" "playlist")))) (r "1.65")))

