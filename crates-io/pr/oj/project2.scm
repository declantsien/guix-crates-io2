(define-module (crates-io pr oj project2) #:use-module (crates-io))

(define-public crate-Project2-0.1.0 (c (n "Project2") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1xxpnvsz6pkahwvw6hc0v5kl155zqf9ghg6gr21qj3aww9zxmg1n")))

(define-public crate-Project2-0.1.1 (c (n "Project2") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0mrhamj8irrcczhny4lsh2jcxq8avmaxhw7l0nflgadxyln8jrfy")))

(define-public crate-Project2-0.1.2 (c (n "Project2") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0dmw2h9vsx2kqhglwd5cnfaxndhxn89gvvxkcqg7y210i7lxwyij")))

(define-public crate-Project2-0.1.3 (c (n "Project2") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0sn0p39y9jxpmfc8p0iiabwf60d59pzpms9z59p4lma31ri3ahb9")))

