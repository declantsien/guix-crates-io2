(define-module (crates-io pr oj project-root) #:use-module (crates-io))

(define-public crate-project-root-0.1.0 (c (n "project-root") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0v4inhznmniivclhg68n0dan96p11wimcdwir8rcjhcxd1n2blrg")))

(define-public crate-project-root-0.1.1 (c (n "project-root") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "13cax65zbblm8nj8gy8qkk9qqzln3ip07h60bm4a83mw6m4cv2iz")))

(define-public crate-project-root-0.2.0 (c (n "project-root") (v "0.2.0") (h "0a16f18z1x1v6p8k186cy0pc2r7ch77rczgg4b8bsb0fbw6clrm8")))

(define-public crate-project-root-0.2.1 (c (n "project-root") (v "0.2.1") (h "07i2wixagfx7b2hih4b250g21s2i13mqykkmnm4p49i8p9k4lhyv")))

(define-public crate-project-root-0.2.2 (c (n "project-root") (v "0.2.2") (h "1pv3ry35p9bn71y4ipzfxm0n3asjg8qdf83x1328kmjygpqbzk4b")))

