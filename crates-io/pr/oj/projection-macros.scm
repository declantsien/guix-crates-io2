(define-module (crates-io pr oj projection-macros) #:use-module (crates-io))

(define-public crate-projection-macros-0.1.0 (c (n "projection-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0xlz7a4zmrw90nx3y5xv1pbjnkl9c60j6n0dwy5kwl4k28c1ds36")))

(define-public crate-projection-macros-0.1.1 (c (n "projection-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "06han7kw73191ln8skq4cqn0q98qabpga6f7bq5876qas9ralrcg")))

