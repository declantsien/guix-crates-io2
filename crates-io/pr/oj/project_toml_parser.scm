(define-module (crates-io pr oj project_toml_parser) #:use-module (crates-io))

(define-public crate-project_toml_parser-0.1.0 (c (n "project_toml_parser") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "0qvfav9qkv398jfjdrw5n37jfz2r68zqpcicsijnzd10a1ljxxy9")))

(define-public crate-project_toml_parser-0.1.1 (c (n "project_toml_parser") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "strfmt") (r "^0.2.4") (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "0fnj4csn6rrmfi7a0w5jvppviq38jfgc5dld4yanryrgznq7qwy0")))

