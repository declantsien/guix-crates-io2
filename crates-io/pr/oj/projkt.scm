(define-module (crates-io pr oj projkt) #:use-module (crates-io))

(define-public crate-projkt-0.2.0 (c (n "projkt") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive" "std" "suggestions"))) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "spdx") (r "^0.8.1") (f (quote ("text"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1di9zzvqwsa673xj9n70hnac85nrwn1cccqfhx0pgdrvsp61n6v5")))

(define-public crate-projkt-0.2.1 (c (n "projkt") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive" "std" "suggestions"))) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "spdx") (r "^0.8.1") (f (quote ("text"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "07hgh191l1n58l44h30w4w5jb2n82ydh3g9p4xajjkms2yvidygv")))

