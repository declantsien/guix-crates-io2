(define-module (crates-io pr oj project_64) #:use-module (crates-io))

(define-public crate-project_64-0.1.0 (c (n "project_64") (v "0.1.0") (d (list (d (n "gfx_64") (r "^0.1.0") (d #t) (k 0)) (d (n "gui_64") (r "^0.1.0") (d #t) (k 0)) (d (n "log_64") (r "^0.1.0") (d #t) (k 0)) (d (n "math_64") (r "^0.1.0") (d #t) (k 0)) (d (n "mem_64") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl_64") (r "^0.1.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (o #t) (d #t) (k 1)))) (h "0nh4j5cynkxkyxw5lc2n1wai38wv2v7y6h60686a7fvk2i79znpb") (f (quote (("std") ("log" "std") ("edit" "std" "ttf-parser"))))))

