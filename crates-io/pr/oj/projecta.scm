(define-module (crates-io pr oj projecta) #:use-module (crates-io))

(define-public crate-projecta-0.1.0 (c (n "projecta") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.3.3") (d #t) (k 0)))) (h "116p25klrmjc10llzy1bkrvjdy476w85551n1nznvdbiwh7l7bxs")))

(define-public crate-projecta-0.2.0 (c (n "projecta") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "simple-home-dir") (r "^0.3.3") (d #t) (k 0)))) (h "11gm9hysyhpvjamqpd1fdhlqfs867zv0lkq7zd72g36sr72fqxzz")))

