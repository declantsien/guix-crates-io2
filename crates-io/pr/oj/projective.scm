(define-module (crates-io pr oj projective) #:use-module (crates-io))

(define-public crate-projective-0.0.0 (c (n "projective") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0gzwdd8ld4dbsz70afkglq6ywbnnvrbj7gfi0n21g63aj6vkh5ix") (f (quote (("default"))))))

(define-public crate-projective-0.1.0 (c (n "projective") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "03y13pycblacs53c9884bhfg910hcgzzmlmli82qg4lxg52vibwh") (f (quote (("default"))))))

(define-public crate-projective-0.2.0 (c (n "projective") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ji4q38b55kb86a6df62azjqbl1v95hqnc3h9pgzh3zgmf1nz0w8") (f (quote (("default"))))))

(define-public crate-projective-0.2.1 (c (n "projective") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "02m5zffz9svi4q19l14rbf6sismsmazxiab62sny422qz5x10x2w") (f (quote (("default"))))))

(define-public crate-projective-0.2.2 (c (n "projective") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1wvhf1bmz6whc11h08mrsbqa84fw45gkc0b9li86bs7n1axdhk7r") (f (quote (("default"))))))

(define-public crate-projective-0.2.3 (c (n "projective") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "06wn8qdjjzf6vn0cw8yv916bfk8qx4jqvw634lvkgmqsazy41f4d") (f (quote (("default"))))))

(define-public crate-projective-0.2.4 (c (n "projective") (v "0.2.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "100dgjajlgci1kb99f8pspiamcsp6gwm0gw6lb4lbgrkq7dxn4zs") (f (quote (("default"))))))

(define-public crate-projective-0.2.5 (c (n "projective") (v "0.2.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1vx2cd4fdp1zaw18wlbaz22hc3fg2l2gn32kr7xrpaa8xf8c2r9j") (f (quote (("default"))))))

(define-public crate-projective-0.3.0 (c (n "projective") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0y9shda23gmjfrz3l5m7qaffk50imjnfb4n33pdv99sy2pi0fpf5") (f (quote (("default"))))))

