(define-module (crates-io pr oj projfs) #:use-module (crates-io))

(define-public crate-projfs-0.1.0 (c (n "projfs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.0") (d #t) (k 0)) (d (n "projfs-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 2)))) (h "0q472x5f7n37wnki1pyh7cycwjd5jp0ixm14r39dyrk6wv0r8382")))

(define-public crate-projfs-0.1.1 (c (n "projfs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.0") (d #t) (k 0)) (d (n "projfs-sys") (r "=0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 2)))) (h "0p0z4xgj3qdmn6g470xbk0jf6z1hvrzfzgd03xwaj48lb8c8z1a0")))

(define-public crate-projfs-0.1.2 (c (n "projfs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.0") (d #t) (k 0)) (d (n "projfs-sys") (r "=0.1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 2)))) (h "0fl3ahwiyqznm6i9kbncy5q2chlnrkr3ss6g3ij91lnj41jd5f4k")))

