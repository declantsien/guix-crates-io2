(define-module (crates-io pr oj project-creator) #:use-module (crates-io))

(define-public crate-project-creator-0.1.0 (c (n "project-creator") (v "0.1.0") (d (list (d (n "rand_word") (r "^0.1.18") (d #t) (k 0)))) (h "0ga3065fnwa20ncfa8m87936b6prcmwx652wcpfnd8p8bkilzaqh")))

(define-public crate-project-creator-0.1.1 (c (n "project-creator") (v "0.1.1") (d (list (d (n "rand_word") (r "^0.1.18") (d #t) (k 0)))) (h "1g0wsjakjqnd7i5zv5jnnnrc36hlpkqhb1rj7icjqf6yxb8vyj9b")))

(define-public crate-project-creator-0.1.2 (c (n "project-creator") (v "0.1.2") (d (list (d (n "rand_word") (r "^0.1.18") (d #t) (k 0)))) (h "19c65i39w935xiby6f4vzbi8asl4bpy7fdia0cl0rmsy5v6hg6b7")))

