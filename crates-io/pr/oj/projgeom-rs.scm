(define-module (crates-io pr oj projgeom-rs) #:use-module (crates-io))

(define-public crate-projgeom-rs-0.1.0 (c (n "projgeom-rs") (v "0.1.0") (d (list (d (n "fractions-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1lws7vy2jfyncs16w7af2z9341nyrf3lfksxfippfxm95n5qivlg")))

(define-public crate-projgeom-rs-0.1.1 (c (n "projgeom-rs") (v "0.1.1") (d (list (d (n "fractions-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "svgbobdoc") (r "^0.3") (f (quote ("enable"))) (d #t) (k 0)))) (h "003gkv0lasrdyq1avbv4h3rsvnj2giydbh98bmf0yl4r8kam3swv")))

