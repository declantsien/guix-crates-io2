(define-module (crates-io pr oj project-absence) #:use-module (crates-io))

(define-public crate-project-absence-0.1.0 (c (n "project-absence") (v "0.1.0") (d (list (d (n "project-absence-kernel") (r "^0.1.0") (d #t) (k 0)))) (h "1ip92wh0srdvz8h1rg22r66m1zlkgnlx11w5w62f8b7j5hm8ivz1")))

(define-public crate-project-absence-0.1.2 (c (n "project-absence") (v "0.1.2") (d (list (d (n "project-absence-kernel") (r "^0.1.2") (d #t) (k 0)))) (h "0ax4gz65zjrp1q4p7x1wdlq51p314fq413ldg8y024914wbaghdg")))

(define-public crate-project-absence-0.1.3 (c (n "project-absence") (v "0.1.3") (d (list (d (n "project-absence-kernel") (r "^0.1.3") (d #t) (k 0)))) (h "19bibhhd7j4my93pgm88a3d6fws5ci5w19svbfbjl93x1c6f0jb4")))

(define-public crate-project-absence-0.1.4 (c (n "project-absence") (v "0.1.4") (d (list (d (n "project-absence-kernel") (r "^0.1.4") (d #t) (k 0)))) (h "0pwpqf0bx46d2rqyyzayla4rgv9qz39mdrifa82awh2ishsbqbg8")))

(define-public crate-project-absence-0.2.0 (c (n "project-absence") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "project-absence-kernel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "04xkb70a77waglc47vbiy58lr1v4fb0ly40ihpy2m50q3psh46mc")))

(define-public crate-project-absence-0.2.1 (c (n "project-absence") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "project-absence-kernel") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0inskngm3g4lwirwd7l83n2p1m9gfihxpaz1mdgbw7p01wwkdvcy")))

