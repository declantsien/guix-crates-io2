(define-module (crates-io pr oj projectm-rs) #:use-module (crates-io))

(define-public crate-projectm-rs-0.1.0 (c (n "projectm-rs") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1kxzb7s3anrii9b98ynajni72khxkxjajvvbdpb576wkz87f9x8n") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.1 (c (n "projectm-rs") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1qa0z8h4zb1r2kava67i29sq64mmb2p7za2p46qck5gp2c0f0xsg") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.2 (c (n "projectm-rs") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1jsqk3fdn9x76wdq3v7mpl67gdnas11zvls1aaraq51vnza02w2i") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.3 (c (n "projectm-rs") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "111h1cfacbd3nav9ilf7k04nkasl5q2gds6xp0grwj420wnh60wp") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.4 (c (n "projectm-rs") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0p0v5ca0pyzrgwzc4mf1sg5zp94bg8blv2i9d43rrzxf1yqb30vl") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.5 (c (n "projectm-rs") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0xlhafhsds5b101hikh1apa26vdbps2dhwpmap4vffyvhddqyjwq") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.6 (c (n "projectm-rs") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1vvrxbnmg26ag376s2ccrr3rpbsnkax3gf9lhj141y8jg7fs2qpf") (y #t) (r "1.59")))

(define-public crate-projectm-rs-0.1.7 (c (n "projectm-rs") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "01mlpwicgfj9xhq5fvc86grc3jzm4xn45j5dpxc36kjpjrl8mpy2") (y #t) (r "1.59")))

(define-public crate-projectm-rs-1.0.0 (c (n "projectm-rs") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1hw4ln7bs6qrv1qyil6nravc1rdr5mk7m984gxn7xdbsg16jdnfv") (f (quote (("playlist") ("default")))) (y #t) (r "1.65")))

(define-public crate-projectm-rs-1.0.1 (c (n "projectm-rs") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1gd9z9dn1pqlylyqb6y3qhv6fkrqsz3zwgrsd8qvf85d2kv3kwm1") (f (quote (("playlist") ("default")))) (y #t) (r "1.65")))

(define-public crate-projectm-rs-1.0.2 (c (n "projectm-rs") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "123b1cp1757cam06001j2vid0jrs9jqsxx1bwivk9bl6vichb3vd") (f (quote (("playlist") ("default")))) (y #t) (r "1.65")))

(define-public crate-projectm-rs-1.0.3 (c (n "projectm-rs") (v "1.0.3") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1v9m78pm6pnlx6bbq1bsp0si9air89lcg66ds7kfp7pia6ljji2l") (f (quote (("playlist") ("default")))) (y #t) (r "1.65")))

(define-public crate-projectm-rs-1.0.4 (c (n "projectm-rs") (v "1.0.4") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.5") (d #t) (k 0)))) (h "0vscqrsa0arz3z2x9q4mqyjas8qprx2awn0k6c2qhlg8i4z2p90c") (f (quote (("playlist") ("default")))) (y #t) (r "1.65")))

(define-public crate-projectm-rs-1.0.5 (c (n "projectm-rs") (v "1.0.5") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "projectm-sys") (r "^1.0.7") (f (quote ("playlist"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17fh0cs75wznmcjwkq3lnjbdiw92gp57kck5h89zqsxvr2cxqykc") (f (quote (("playlist") ("default" "playlist")))) (y #t) (r "1.65")))

