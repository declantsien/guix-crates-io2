(define-module (crates-io pr oj projects) #:use-module (crates-io))

(define-public crate-projects-1.0.0 (c (n "projects") (v "1.0.0") (d (list (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "webdesserts-confy") (r "^1") (d #t) (k 0)))) (h "11pj6jg34i8zf1mbn5k432036cd880qjp43aaqsi1vwl8bg8jlp6")))

(define-public crate-projects-1.0.1 (c (n "projects") (v "1.0.1") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "webdesserts-confy") (r "^1") (d #t) (k 0)))) (h "020wrwdap21cxs3jk1dzb0iv3hh6cja2frp6kqzvx8s6588v06ws")))

(define-public crate-projects-1.1.0 (c (n "projects") (v "1.1.0") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "webdesserts-confy") (r "^1") (d #t) (k 0)))) (h "0sa0alxn4961xfqvypim18l14s8xm5cy0dghbq606dcz28iq70aa")))

(define-public crate-projects-1.1.1 (c (n "projects") (v "1.1.1") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "webdesserts-confy") (r "^1") (d #t) (k 0)))) (h "1wggk4lwcxwzjp788z3d6fr0acfzdabanqisxmxc1s1mray4lkj6")))

(define-public crate-projects-1.1.2 (c (n "projects") (v "1.1.2") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "webdesserts-confy") (r "^1") (d #t) (k 0)))) (h "1gxkkk88vr1dnm705g8abgqkq1b5i1yw46bdqyh29s2w36dzj0z6")))

