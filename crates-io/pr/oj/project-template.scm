(define-module (crates-io pr oj project-template) #:use-module (crates-io))

(define-public crate-project-template-0.1.0 (c (n "project-template") (v "0.1.0") (h "1gj8h27f6s2rk5l81m3dzfixiqd0hgahwkrl8qz66biwpdhns7da") (r "1.58.1")))

(define-public crate-project-template-0.1.1 (c (n "project-template") (v "0.1.1") (h "08q68kmaxgyjv24mqs3c266g5xv590l241hrk88h7q3pmcvszrc9") (r "1.58.1")))

(define-public crate-project-template-0.1.2 (c (n "project-template") (v "0.1.2") (h "0mypjqvb5x0h5w93d4aawc7y6026x0pj8mb7y6yhii5hxxba04ja") (r "1.58.1")))

(define-public crate-project-template-0.1.3 (c (n "project-template") (v "0.1.3") (h "1d4k8hkswc155k6yd1mmqmshh2340r4z4bcw2716xawpzl28j754") (r "1.58.1")))

(define-public crate-project-template-0.1.4 (c (n "project-template") (v "0.1.4") (h "02w97rcs3fbv1spnlg29ymr4wam89qf0ib43hb6b2ixk7ycssjzz") (r "1.58.1")))

(define-public crate-project-template-0.1.5 (c (n "project-template") (v "0.1.5") (h "0jvd42iz527map4bvdazd7km1b031356as2x0brp095z84283isp") (r "1.58.1")))

(define-public crate-project-template-0.1.6 (c (n "project-template") (v "0.1.6") (h "0xabgpf32r60wrlp3wp2ymvirhlvh6lmplylw974yvdqdzsw2ikb") (r "1.58.1")))

(define-public crate-project-template-0.1.7 (c (n "project-template") (v "0.1.7") (h "1aj79n78h3pbfh21qllxjxd82r2g0xviiyfydq10mgzvhln3qc94") (r "1.58.1")))

(define-public crate-project-template-0.1.8 (c (n "project-template") (v "0.1.8") (h "0bdlz7qxy1ppqvpanpqkfg8ds3mkwl2xqwmfvdy71wrc1a812642") (r "1.58.1")))

