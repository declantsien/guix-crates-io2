(define-module (crates-io pr oj projfs-sys) #:use-module (crates-io))

(define-public crate-projfs-sys-0.1.0 (c (n "projfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)))) (h "1b4qvv17mvpxsv8wks6lwy7csld3wsxir1ajs0ff50cgp2vbz7mf") (f (quote (("default"))))))

(define-public crate-projfs-sys-0.1.1 (c (n "projfs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)))) (h "1k9yb9cjw2qq3jxwcm0mfpvl3xqvx0ixksm4hbks0saly6f9dqxd") (f (quote (("default"))))))

(define-public crate-projfs-sys-0.1.2 (c (n "projfs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)))) (h "14657mb7rlq1r76yhbl27cibcfn9rbkzqws19r659mz04lxgmp7i") (f (quote (("default"))))))

