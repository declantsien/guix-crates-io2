(define-module (crates-io pr oj project_path) #:use-module (crates-io))

(define-public crate-project_path-0.1.0 (c (n "project_path") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11x3flxp5xr4y8r4bs58r8rdgbr3i69b84ybrrf1slvrhw1mas7s")))

