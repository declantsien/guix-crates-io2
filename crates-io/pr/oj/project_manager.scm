(define-module (crates-io pr oj project_manager) #:use-module (crates-io))

(define-public crate-project_manager-0.1.0 (c (n "project_manager") (v "0.1.0") (d (list (d (n "crossterm") (r ">=0.18.0, <0.19.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.58, <2.0.0") (d #t) (k 0)) (d (n "smart-default") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "tui") (r ">=0.12.0, <0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "16iv5c9im0f6jifg75g9v67rgf7hl2qn14jgrzbcf40v99skl4lz")))

(define-public crate-project_manager-0.1.1 (c (n "project_manager") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.12") (f (quote ("crossterm"))) (k 0)))) (h "136qamxcvkzxw01z95j5jpndh7nfbalapsswdbp4niif1z3rx6l6")))

(define-public crate-project_manager-0.1.2 (c (n "project_manager") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "0spmwdlv827p82rhj3w3x7zissk2z22glff77sb7y7c4wssrk4gw")))

(define-public crate-project_manager-0.1.3 (c (n "project_manager") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (f (quote ("crossterm"))) (k 0)))) (h "1yv2f7mfj698wg1d2aa485qy39v0kp403z7gsy49xi2jznr6h6ss")))

