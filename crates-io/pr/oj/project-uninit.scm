(define-module (crates-io pr oj project-uninit) #:use-module (crates-io))

(define-public crate-project-uninit-0.1.0 (c (n "project-uninit") (v "0.1.0") (h "1m77k33mmp346c4b3qs4bnpj9dxpgpq7a9bf7f6v7xqv8k1mprm3")))

(define-public crate-project-uninit-0.1.1 (c (n "project-uninit") (v "0.1.1") (h "0pid3s89ddx1iiiyq4ab88hfzm1rrbp1zmqmvanfhic9i2cr631j")))

