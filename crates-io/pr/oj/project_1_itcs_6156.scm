(define-module (crates-io pr oj project_1_itcs_6156) #:use-module (crates-io))

(define-public crate-project_1_itcs_6156-0.1.0 (c (n "project_1_itcs_6156") (v "0.1.0") (h "1mnvvgackhaznh1np870xjc1dzwl5scpnxdb6aqvjx7j82n2vl0p") (y #t)))

(define-public crate-project_1_itcs_6156-0.1.1 (c (n "project_1_itcs_6156") (v "0.1.1") (h "0yp28s21q7zzmhffviw59wqx0a8xshyznibqq7zqppkzfc43vqyi") (y #t)))

(define-public crate-project_1_itcs_6156-0.1.2 (c (n "project_1_itcs_6156") (v "0.1.2") (h "17dhzdw92jwq1gw102cgjl68xw051cj1y82bmjp5sixihzwk8bph") (y #t)))

(define-public crate-project_1_itcs_6156-0.1.3 (c (n "project_1_itcs_6156") (v "0.1.3") (h "0iiv4v2lyn91rq5xff5zn3hi9ldf796s6f9jfz6xg18g0h2nl6wp") (y #t)))

