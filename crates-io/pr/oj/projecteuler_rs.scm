(define-module (crates-io pr oj projecteuler_rs) #:use-module (crates-io))

(define-public crate-projecteuler_rs-0.1.0 (c (n "projecteuler_rs") (v "0.1.0") (d (list (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15cnjnm7kjb8h1pya1wsbkp926p06h0g49s7b140qlska01az01m") (f (quote (("pe_0003" "primal") ("pe_0002") ("pe_0001"))))))

