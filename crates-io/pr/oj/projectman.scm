(define-module (crates-io pr oj projectman) #:use-module (crates-io))

(define-public crate-projectman-0.1.0 (c (n "projectman") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0r7cxp83fzp9lzni664fxz81cyrp37ww6jcraizcjck402bg37d8")))

(define-public crate-projectman-0.1.1 (c (n "projectman") (v "0.1.1") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0lwgig8lflifnk25qdfn68wfvn547zalgk6wn7qifxcks08h8vp3")))

(define-public crate-projectman-0.1.2 (c (n "projectman") (v "0.1.2") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0x4sbmnf1pyjnpkqmifb0w6hmyqk89ppdsvfimmjpb12n720lrlp")))

(define-public crate-projectman-0.1.3 (c (n "projectman") (v "0.1.3") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "08cfiivb81rwdrjiwijkn1qfg4lbai6i9dii7snizismb8bfvlq5")))

(define-public crate-projectman-0.1.4 (c (n "projectman") (v "0.1.4") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "1cl801zinzwf5cmvnl5l8zr7vnr42f80pbbncjlaic77pvkbl8vr")))

(define-public crate-projectman-0.1.5 (c (n "projectman") (v "0.1.5") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "06lmds0azwsa800p7q3a3ycn1hjfd03y55ldymiy8lg5ab160487")))

(define-public crate-projectman-0.1.6 (c (n "projectman") (v "0.1.6") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0cndwi993s1pkn7cb1vwlcc6jrfvrmjapqfacwd0n8gwrkpn8gxi")))

