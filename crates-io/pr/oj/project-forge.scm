(define-module (crates-io pr oj project-forge) #:use-module (crates-io))

(define-public crate-project-forge-1.0.0 (c (n "project-forge") (v "1.0.0") (d (list (d (n "chrobry-core") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1") (d #t) (k 0)))) (h "1f9lqvaafl5w3zfbllvjpl45nkay1b1ljr52nbn8pi89mrlgb5xg")))

(define-public crate-project-forge-1.0.1 (c (n "project-forge") (v "1.0.1") (d (list (d (n "chrobry-core") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1") (d #t) (k 0)))) (h "0nc54k1r9v9vjpm2qcld591nl7nbzj08zkjsah66w3ckr3rp5nrn")))

