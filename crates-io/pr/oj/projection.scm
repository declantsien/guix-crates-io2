(define-module (crates-io pr oj projection) #:use-module (crates-io))

(define-public crate-projection-0.1.0 (c (n "projection") (v "0.1.0") (d (list (d (n "projection-macros") (r "=0.1.0") (d #t) (k 0)))) (h "0j0wszr1l7lss8k39yymrwszm29sb3fg693y9djl4649hl984xak")))

(define-public crate-projection-0.1.1 (c (n "projection") (v "0.1.1") (d (list (d (n "projection-macros") (r "=0.1.1") (d #t) (k 0)))) (h "1az4bpps2jkqbylx8f3s1ggag8ibzijbqkw9qk55q42z9481m9l7")))

