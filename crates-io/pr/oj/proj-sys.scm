(define-module (crates-io pr oj proj-sys) #:use-module (crates-io))

(define-public crate-proj-sys-0.1.0 (c (n "proj-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "022g5jnz7gl7rpfk4n28d24nbzph5zsfwmniwydnhk0k0bzzqvgp")))

(define-public crate-proj-sys-0.2.0 (c (n "proj-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1ibxdn5ngsp8bj084gzlwys9r7hz54pnp98x3kv8axd1ai3s6qv8")))

(define-public crate-proj-sys-0.3.0 (c (n "proj-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1c6ilv413a9syf2pkc0f51xazyd805vc1zi2wib7cm6r3ih39ms6")))

(define-public crate-proj-sys-0.4.0 (c (n "proj-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "097nzai640xdg5wi364s7fim3z8ixl379icbdav0pmf423xpkkn1")))

(define-public crate-proj-sys-0.5.0 (c (n "proj-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1g1v4n4yq8cj6fqx3n1kvz7qcsfs8478jrxbspr4h74vm8kis2sn")))

(define-public crate-proj-sys-0.6.0 (c (n "proj-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "14vcyg6jj5z7ifm5zbq5h4ysb1azlqnra35m5hmmaq2vm3z1pf8c")))

(define-public crate-proj-sys-0.6.1 (c (n "proj-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "00vkv9h9npgzjv119qnqkpjl27hagwdha1nbcq01w2kj2fl88aw9")))

(define-public crate-proj-sys-0.6.2 (c (n "proj-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1ivny0y9f111a8dv7p8zzsvzllhcr28cpjipdd5kai4668q1xacy")))

(define-public crate-proj-sys-0.7.0 (c (n "proj-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0gj798p68j3apvjac2rzkvdf4m0b4gv1f6smxj0xn8102s5g7h20")))

(define-public crate-proj-sys-0.8.0 (c (n "proj-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "19m9dmz222lp7a1bwqw58xds0mbvs83mcimz93m5wi48nvingf8p")))

(define-public crate-proj-sys-0.9.0 (c (n "proj-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)))) (h "1ljff19wdi1mvqddll1k1iv7fq20n07gl49wwd038gmaxllx55pf")))

(define-public crate-proj-sys-0.10.0 (c (n "proj-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.49.1") (d #t) (k 1)))) (h "0jhsky7xw7gzly50qgxlzw0vwllw2grxdaf3nr56nxvn22aq2g3s")))

(define-public crate-proj-sys-0.10.1 (c (n "proj-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.49.1") (d #t) (k 1)))) (h "0pq42wzl2767q186iwz9jzg058rrcjjgqw458w9hqhgk5li7smz2") (f (quote (("docs-rs"))))))

(define-public crate-proj-sys-0.11.0 (c (n "proj-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "1h1iaf0lx0m6yffjy1krzhbch4npbqw39i953s465ic80nfa41da") (f (quote (("docs-rs"))))))

(define-public crate-proj-sys-0.11.1 (c (n "proj-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "1lxhrqgdqhyg93xx2k1m7mhp1hx2fnbggyjlif0wqys04iial3vh") (f (quote (("docs-rs"))))))

(define-public crate-proj-sys-0.11.2 (c (n "proj-sys") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "18pg27sjd9lkmcv1dpd1cvj4x8n45rhh9c8jjvpmcd6miz53pavz") (f (quote (("fallback-bindings") ("docs-rs"))))))

(define-public crate-proj-sys-0.12.0 (c (n "proj-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "08s4ys8vwhq54mmn3dz1x7fwyf5i6xdwmms112c9qgnq8757shdv") (f (quote (("docs-rs"))))))

(define-public crate-proj-sys-0.12.1 (c (n "proj-sys") (v "0.12.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0frfaf596a6w4ybzm55v8i362f2vz01j4c044wc56fpd6zadngf3") (f (quote (("nobuild"))))))

(define-public crate-proj-sys-0.12.2 (c (n "proj-sys") (v "0.12.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "17fhsi0lqlq3pkhzq37pgqh5kw5n8nrzbkirzqx3568nw279m4jx") (f (quote (("nobuild"))))))

(define-public crate-proj-sys-0.13.0 (c (n "proj-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1h0zn0kshm65m9r5a76c3m05ix8vz8wl5wgi5k2gcfadgjsi352l") (f (quote (("nobuild"))))))

(define-public crate-proj-sys-0.15.0 (c (n "proj-sys") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0h2ncffghvn48jfhy9kcbblqki7dvm84i7zwnb5jknl2gdyyrikl") (f (quote (("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.16.1 (c (n "proj-sys") (v "0.16.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0ybhgj1vrgi4x6c7f4qbffvfngm9m19z2aqs540cbl86a4lwnqps") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.16.3 (c (n "proj-sys") (v "0.16.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0pzk1nz15iwfq51yxa71pxczr3bw091vjyy796d06c4yd0xrzyxz") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.16.4 (c (n "proj-sys") (v "0.16.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "009j4y5r06vym1s07j1nd3i1c0855xpqmyi2xbpnjdizsmvbjsd3") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.17.0 (c (n "proj-sys") (v "0.17.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0zf19xbhcpqda4qiynf3g0yxcwxbsz6nayhf8wwiprwp0vshdrn4") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.17.1 (c (n "proj-sys") (v "0.17.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0s7khpy8jbib289v8ffiqc8v656zxlhqb1fsfv0krw7n6qcfkxlj") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.18.0 (c (n "proj-sys") (v "0.18.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0pl7s97canr157h7wh9xpbl3krj62xchwwkgijawn8lrbqb7qx8f") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.18.1 (c (n "proj-sys") (v "0.18.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0qm9h1gq9ylqzb38x8xrmx2xqnlm74vb2q3bycfwk648mzz6plck") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.18.2 (c (n "proj-sys") (v "0.18.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0ryr2cawxvi21ijvzicmy3m46n9wrbcpngyp3xxj215l5w875xsk") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.18.3 (c (n "proj-sys") (v "0.18.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1swa8jwnmaijy01dvzprdidsq7c2b39yk7ns9w06qmk5p7awq5hg") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.18.4 (c (n "proj-sys") (v "0.18.4") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1jjvvnajrwxm9h1i0vqcg7fmznagywgyimk5pgv25rxnv7y22iaw") (f (quote (("pkg_config") ("nobuild") ("bundled_proj_tiff" "bundled_proj") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.19.0 (c (n "proj-sys") (v "0.19.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1j69jsgnz5bn4g7hkcw3vvz2hjblj2zhw0vr9559f9w6qf5fda3g") (f (quote (("pkg_config") ("nobuild") ("bundled_proj_tiff" "bundled_proj") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.19.1 (c (n "proj-sys") (v "0.19.1") (d (list (d (n "bindgen") (r "^0.58.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1carn43fbiinqkbrfwkcq9qyd3gmg3dil5ja4vqf4l86v9lidqw8") (f (quote (("pkg_config") ("nobuild") ("bundled_proj_tiff" "bundled_proj") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.20.0 (c (n "proj-sys") (v "0.20.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1ny2wmjmfmviqs20mzbr8c72cdgi1kknsizsq9532gqr85jvcjp5") (f (quote (("pkg_config") ("nobuild") ("bundled_proj_tiff" "bundled_proj") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.20.1 (c (n "proj-sys") (v "0.20.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0y00sli080nvkj3cdr8y5p7k8szq63jdlr3g3r4irkv4bi8gv1zh") (f (quote (("pkg_config") ("nobuild") ("bundled_proj_tiff" "bundled_proj") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.21.0 (c (n "proj-sys") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "13vd2i76mkjjbk0rl9yjhsz253x74lnfdz47s2yjdv1cd1861c58") (f (quote (("pkg_config") ("nobuild") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.22.0 (c (n "proj-sys") (v "0.22.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0biyiy1yipsin19jdbcbcm7ljh15l4l46nsl4pwllxlhygg9psan") (f (quote (("pkg_config") ("nobuild") ("network") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.23.0 (c (n "proj-sys") (v "0.23.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "flate2") (r "^1.0.22") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "0qxbpi1y3zw9lv2rx2930vxdi2vqwpmbmlp23ca6sqyh5ynw4d8i") (f (quote (("pkg_config") ("nobuild") ("network") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.23.1 (c (n "proj-sys") (v "0.23.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "flate2") (r "^1.0.22") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "1agxq1pb6889aikyhzjbirnwfhbnnip1d3xs2cyv5jsn4pxff6lj") (f (quote (("pkg_config") ("nobuild") ("network") ("bundled_proj")))) (l "proj")))

(define-public crate-proj-sys-0.23.2 (c (n "proj-sys") (v "0.23.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "flate2") (r "^1.0.24") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "1v4gcf9wa4icdifsysrchbqrr5k5bknpngrhdnjy3z8p3vxg86v0") (f (quote (("tiff") ("pkg_config") ("nobuild") ("network" "tiff") ("bundled_proj")))) (l "proj")))

