(define-module (crates-io pr oj projective-transform) #:use-module (crates-io))

(define-public crate-projective-transform-0.3.8 (c (n "projective-transform") (v "0.3.8") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0i35ags7j2xp62v6xddrrrk7rhhmq5sp1xdac0i5d3abk1kx5bjs") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

