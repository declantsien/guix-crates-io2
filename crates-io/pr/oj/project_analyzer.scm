(define-module (crates-io pr oj project_analyzer) #:use-module (crates-io))

(define-public crate-project_analyzer-2.1.0 (c (n "project_analyzer") (v "2.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "piechart") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (f (quote ("include-exclude"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "1b8lcfhi3gab4v1kqd4k68901ij6ri8sw5v8zh8yfcj56svdgwkd")))

