(define-module (crates-io pr oj projections) #:use-module (crates-io))

(define-public crate-projections-0.1.0 (c (n "projections") (v "0.1.0") (h "0c385rkg7wfv9i6nhc5lq9d9gpyw2hdm4dskixn5ls1rphwagb0m")))

(define-public crate-projections-0.2.0 (c (n "projections") (v "0.2.0") (h "0p0cil7gccn2pskv9fh98447rlfwz0jjrpc324n3i9c4rddip6fm")))

