(define-module (crates-io pr oj projectm-sys) #:use-module (crates-io))

(define-public crate-projectm-sys-0.1.0 (c (n "projectm-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "13m8w0yl73yfbli2786h4grdmbjpp7yxjr5y81lmg6lmc67x3ryw") (y #t) (l "projectm") (r "1.59")))

(define-public crate-projectm-sys-0.1.1 (c (n "projectm-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0p4sky2615nncj9sa0gm3abm7spqvbiv8pi4ylq62573frblqx5g") (y #t) (l "projectm") (r "1.59")))

(define-public crate-projectm-sys-1.0.0 (c (n "projectm-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0kqxq0irpdqpj6y9sb6myzxnchq2vz0r602zai2iviz2sdq3hy1f") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.59")))

(define-public crate-projectm-sys-1.0.1 (c (n "projectm-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0wa2vp5xb1zcpsnz30dngy0id0v13akq256s9hf939rd91sajxzb") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.59")))

(define-public crate-projectm-sys-1.0.2 (c (n "projectm-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "188pwyb0krhfi1l3k75vibhal9xxljnnjh8gh6dfwvszspla46rh") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.59")))

(define-public crate-projectm-sys-1.0.3 (c (n "projectm-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1h3641378i3wsxyf49xk71g71n0nhvmpkw1vs1c1qh9jd11pxkqn") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.65")))

(define-public crate-projectm-sys-1.0.4 (c (n "projectm-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "0nlzp2j58nrxndc6gbhjm3cys8yac0qjql4gn2qjckz98jgmnfb8") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.65")))

(define-public crate-projectm-sys-1.0.5 (c (n "projectm-sys") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1sajxh4d8lpg5mb9clqyzy3hdczrzhfk6ygfnv8sa1818aq4lz4p") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.65")))

(define-public crate-projectm-sys-1.0.6 (c (n "projectm-sys") (v "1.0.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "0762z9yic308ykz1m867c6c6nxw0fxjwa30wfy9apkldj5s00n4x") (f (quote (("playlist") ("default")))) (y #t) (l "projectm") (r "1.65")))

(define-public crate-projectm-sys-1.0.7 (c (n "projectm-sys") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1wq8ja85qbdhircmac4q5s1d0rjkiam7wzalwg6vaph43n33k5hj") (f (quote (("playlist") ("default" "playlist")))) (l "projectm") (r "1.65")))

(define-public crate-projectm-sys-1.0.8 (c (n "projectm-sys") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1f6lj38bm0l31f7j0hw3ya2cmgjc7k2baq612alq1hgjcbh0ijwk") (f (quote (("playlist") ("default" "playlist")))) (l "projectm") (r "1.65")))

