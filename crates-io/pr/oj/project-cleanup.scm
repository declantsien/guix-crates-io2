(define-module (crates-io pr oj project-cleanup) #:use-module (crates-io))

(define-public crate-project-cleanup-0.4.0 (c (n "project-cleanup") (v "0.4.0") (d (list (d (n "humansize") (r "^1.1.0") (d #t) (k 0)))) (h "1zc0r0myx6f5lnykm5qdmhg57nc71dprv85s1ipnx2ygz6nalrvr")))

(define-public crate-project-cleanup-0.4.1 (c (n "project-cleanup") (v "0.4.1") (d (list (d (n "humansize") (r "^1.1.0") (d #t) (k 0)))) (h "0lhy879hmfvqvgly5ga9b4qnb5mrlrm3a8riq9cx5c43119ybkmi")))

(define-public crate-project-cleanup-0.5.0 (c (n "project-cleanup") (v "0.5.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1f7q8fcibb9m2i5gwm5gixwv6nlb1v8rhqgk0sm9avhbnlwd9a9m")))

(define-public crate-project-cleanup-0.5.1 (c (n "project-cleanup") (v "0.5.1") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "185n5hh5kxhm9raljdbliw25grkwmm28cpzf9pvd77w9192is86v")))

(define-public crate-project-cleanup-0.5.2 (c (n "project-cleanup") (v "0.5.2") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0rcjls3yc2za6jsdvivsnc4xicv1nqq57dj4zqkzlrbdxzzwvhxh")))

(define-public crate-project-cleanup-0.5.3 (c (n "project-cleanup") (v "0.5.3") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0xyfvs5sr2l9y88zvc5f746w2ns8wsj0j85cq4rmcjscixm2hdgz")))

(define-public crate-project-cleanup-0.6.0 (c (n "project-cleanup") (v "0.6.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.9.6") (f (quote ("terminal" "style"))) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0sa8ibcwsbyipibldndqkcfpsbx2xfqppzmihv18w77idz2m04j0")))

(define-public crate-project-cleanup-0.7.0-rc2 (c (n "project-cleanup") (v "0.7.0-rc2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1n612hkfza43f34476s8d7f0mnnf3c1r0ggp0kiij3y0knlrbk96")))

(define-public crate-project-cleanup-0.7.0-rc3 (c (n "project-cleanup") (v "0.7.0-rc3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1vyrfczwn3nknpp8xi9yyja9syd4malxk2j3nxm8zaqjz1lppli0")))

(define-public crate-project-cleanup-0.7.0-rc6 (c (n "project-cleanup") (v "0.7.0-rc6") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0a66sxyjk703q45bpy1xv9dilhfi8kzsxgkji1iralc295d7xvnd")))

(define-public crate-project-cleanup-0.7.0 (c (n "project-cleanup") (v "0.7.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "17jp8iw6g5i3hmx6i9827k1l43vi609id45660vhsvwv50ba215z")))

(define-public crate-project-cleanup-0.7.1 (c (n "project-cleanup") (v "0.7.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0ji0hzxliqw22mjha5hp0q5qjk3gg476xkhcyp3jsjkviin1qal0")))

(define-public crate-project-cleanup-0.7.3 (c (n "project-cleanup") (v "0.7.3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0pwsx97kra5flfnsmnk8zqqzxwww2h8bfrzg5shanhdwfna52pzi")))

(define-public crate-project-cleanup-0.7.4 (c (n "project-cleanup") (v "0.7.4") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0lfi22p2g1kszjqpjjyhxm33mb3v6if8cchq0aas430vsv3h70vk")))

(define-public crate-project-cleanup-0.7.5 (c (n "project-cleanup") (v "0.7.5") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "05qyr6m21qngndl5nd4lrc4pwzs4k5vslnwhqngw7azf99p4838y")))

(define-public crate-project-cleanup-1.0.0 (c (n "project-cleanup") (v "1.0.0") (h "0zznz1qpw1b1y5gbq67hmx8iszcvbv4bsa7d7w428c9mhk6x54mw")))

