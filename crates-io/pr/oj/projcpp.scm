(define-module (crates-io pr oj projcpp) #:use-module (crates-io))

(define-public crate-projcpp-0.1.0 (c (n "projcpp") (v "0.1.0") (h "0lri25m3aq5c91vn1y91vdli55f6v87n8prax1xjj94wgjlsq7zw")))

(define-public crate-projcpp-0.1.1 (c (n "projcpp") (v "0.1.1") (h "0mvn769a1ncbi9a1zjsgh9vd6q873855jkaw66l1rk0r4bpin14i")))

