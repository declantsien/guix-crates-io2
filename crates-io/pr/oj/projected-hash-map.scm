(define-module (crates-io pr oj projected-hash-map) #:use-module (crates-io))

(define-public crate-projected-hash-map-0.1.0 (c (n "projected-hash-map") (v "0.1.0") (h "00wmxp9k6dbhka8piy24gz9sl1d9ivbdd740f02xs7g6im99xqc2") (y #t)))

(define-public crate-projected-hash-map-0.1.1 (c (n "projected-hash-map") (v "0.1.1") (h "1a81q1ina54796zqnhsbin3x2mxv52skr0ncmiyscj5g83i8qna5") (y #t)))

(define-public crate-projected-hash-map-0.1.2 (c (n "projected-hash-map") (v "0.1.2") (h "0l2c5pbqbsw2zm9dpy01s90zjsy9qh5109a21790kiy2mmanfb8w")))

