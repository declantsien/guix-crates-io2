(define-module (crates-io pr oj projecture) #:use-module (crates-io))

(define-public crate-projecture-0.0.1 (c (n "projecture") (v "0.0.1") (d (list (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)))) (h "046lih6ax89i7c1byhx9333s402a3khzswr8awd7dk931m9qd0r3")))

(define-public crate-projecture-0.0.2 (c (n "projecture") (v "0.0.2") (d (list (d (n "atomic") (r "^0.5.1") (o #t) (k 0)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)))) (h "054f3vjkmzlqhf2jdj7aijcwfawnnhvgpcix0abrhd4pm33q9bgq") (f (quote (("std") ("default" "std"))))))

(define-public crate-projecture-0.0.3 (c (n "projecture") (v "0.0.3") (d (list (d (n "atomic") (r "^0.5.1") (o #t) (k 0)) (d (n "backtrace") (r "^0.3.65") (d #t) (k 2)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)))) (h "1rn4z4bilp7ylzpvvc5z3aqiqkf362qmzrc6v09paq3k2n98qsr0") (f (quote (("std") ("default" "std"))))))

(define-public crate-projecture-0.0.4 (c (n "projecture") (v "0.0.4") (d (list (d (n "atomic") (r "^0.5.1") (o #t) (k 0)) (d (n "backtrace") (r "^0.3.65") (d #t) (k 2)) (d (n "macro_rules_attribute") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17czd7b8287x3gi2wxkc3saywqasd176f19y7n5gm51x0ssx8635") (f (quote (("std") ("nightly") ("default" "std"))))))

