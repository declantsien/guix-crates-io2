(define-module (crates-io pr oj project-tree) #:use-module (crates-io))

(define-public crate-project-tree-0.1.0 (c (n "project-tree") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "1n0xxcqq30sm7wjxd4j3iw2xbwnf545rav20h1frry052yyscsn3")))

(define-public crate-project-tree-0.1.1 (c (n "project-tree") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0p313rkx1rfpzw6gipdzb8h5pci3zr1510wmdapy4v5xfj1ag2mk")))

(define-public crate-project-tree-0.2.0 (c (n "project-tree") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0jpzbinfnxzhhd6xsp73911hg887l488f5dd1bdbmjfkmphmsmi8")))

(define-public crate-project-tree-0.2.1 (c (n "project-tree") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "024dmgv7l4jp6avs4jaix83nbz3yql9vy0pcc8q1zwpdk41wqwhf")))

(define-public crate-project-tree-0.2.2 (c (n "project-tree") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0nyy075h9b4hg4a8z3hw9q1rwk0znmrawswmh5bfijpfi2cxl6np")))

