(define-module (crates-io pr ol prolly) #:use-module (crates-io))

(define-public crate-prolly-0.1.0 (c (n "prolly") (v "0.1.0") (h "04yczg62v4swy27ix4w23g6z9xdl7kipp7hwwdn8r93ka4zxf2v6") (y #t) (r "1.60")))

(define-public crate-prolly-0.1.1 (c (n "prolly") (v "0.1.1") (h "0cy551q4q9qlgvdbaf9b4nrzkc8pa03avpv26a7y9b8sgprvh9f0") (r "1.60")))

