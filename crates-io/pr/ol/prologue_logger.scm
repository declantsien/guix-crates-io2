(define-module (crates-io pr ol prologue_logger) #:use-module (crates-io))

(define-public crate-prologue_logger-0.1.0 (c (n "prologue_logger") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0148ji6h5apav1r7fsj8dz4xcc94q876cgbhwf5ivnrpgd9w25fc")))

