(define-module (crates-io pr ol prolog_parser_rebis) #:use-module (crates-io))

(define-public crate-prolog_parser_rebis-0.8.68 (c (n "prolog_parser_rebis") (v "0.8.68") (d (list (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "num-rug-adapter") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rug") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "0d63g4z0fphr9q1jyii8r96yzsxrzsxk8v8cx3wqn80jnbxi68pm") (f (quote (("num" "num-rug-adapter") ("default" "rug"))))))

