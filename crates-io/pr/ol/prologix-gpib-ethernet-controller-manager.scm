(define-module (crates-io pr ol prologix-gpib-ethernet-controller-manager) #:use-module (crates-io))

(define-public crate-prologix-gpib-ethernet-controller-manager-0.1.0 (c (n "prologix-gpib-ethernet-controller-manager") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "09jfwlhigxaac8g2x4b9hxxpl28jw3962qln9v8dibiilwfmwy4n")))

(define-public crate-prologix-gpib-ethernet-controller-manager-0.1.1 (c (n "prologix-gpib-ethernet-controller-manager") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "05axh3byhmf375i51mqrnlkj0x951l1n52l04lf97y68sr9f41d2")))

(define-public crate-prologix-gpib-ethernet-controller-manager-0.1.2 (c (n "prologix-gpib-ethernet-controller-manager") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1iclx7pbxc4f8s7kjrfaw33g6gz3lb2cnfdr9v2w1xv783qawjri")))

