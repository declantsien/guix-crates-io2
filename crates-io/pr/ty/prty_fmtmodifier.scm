(define-module (crates-io pr ty prty_fmtmodifier) #:use-module (crates-io))

(define-public crate-prty_fmtmodifier-0.1.0 (c (n "prty_fmtmodifier") (v "0.1.0") (h "0adz8gjcjhmb8gldc03j7bh6jzizg8fjypv9fp4cvdizc399mv12")))

(define-public crate-prty_fmtmodifier-0.1.1 (c (n "prty_fmtmodifier") (v "0.1.1") (h "0n1s7mgi3wsksscqpv3aq2d6fhl9hp5fh97vcprpqpw5s9jhn2dr")))

