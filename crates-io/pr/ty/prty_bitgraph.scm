(define-module (crates-io pr ty prty_bitgraph) #:use-module (crates-io))

(define-public crate-prty_bitgraph-0.1.0 (c (n "prty_bitgraph") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "prty_fmtmodifier") (r "^0.1.0") (d #t) (k 0)))) (h "0ak7x68y6wzyg3nrkrrw92rkx30v18mnh0lslbanq4nidyvbpg08")))

(define-public crate-prty_bitgraph-0.1.1 (c (n "prty_bitgraph") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "prty_fmtmodifier") (r "^0.1.1") (d #t) (k 0)))) (h "10y0yd51bdxnyzq4n6srxdfzjl8ni5940nyrby6jlribw6ynmwan")))

