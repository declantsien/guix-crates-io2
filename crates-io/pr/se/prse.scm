(define-module (crates-io pr se prse) #:use-module (crates-io))

(define-public crate-prse-0.1.0 (c (n "prse") (v "0.1.0") (d (list (d (n "prse-derive") (r "^0.1.0") (k 0)))) (h "1zk64s907xqk4im2bs2gr31rl2rqpw0h069arci7aqvlm6hcy7jq") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-0.2.0 (c (n "prse") (v "0.2.0") (d (list (d (n "prse-derive") (r "^0.2.0") (k 0)))) (h "087kdwlh5a5hawdcbllm2d9knvgfsvfi0hk8xbwp15z1gp40b201") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-1.0.0 (c (n "prse") (v "1.0.0") (d (list (d (n "prse-derive") (r "^1.0.0") (k 0)))) (h "15q05y9jc7fan8lvs5ql5b8j91h18c76kq228nqg1yywq1ifmrfj") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-1.0.1 (c (n "prse") (v "1.0.1") (d (list (d (n "prse-derive") (r "^1.0.1") (k 0)))) (h "1pcxad6h4jdjrp96vd1a1lanbznlm0aphha3wiwk5yjl7csm6nyd") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-1.0.2 (c (n "prse") (v "1.0.2") (d (list (d (n "prse-derive") (r "^1.0.2") (k 0)))) (h "0hrz85gynrc34vgp1dmx66wqmlk5q9rz07adgsskjvvdy7by9v0k") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-1.0.3 (c (n "prse") (v "1.0.3") (d (list (d (n "prse-derive") (r "^1.0.3") (k 0)))) (h "103sww353qympwvzgw459z2ryrxmhgx3kkh0r3ysjkjf4ch2hzml") (f (quote (("std" "alloc" "prse-derive/std") ("default" "std") ("alloc" "prse-derive/alloc"))))))

(define-public crate-prse-1.1.0 (c (n "prse") (v "1.1.0") (d (list (d (n "memchr") (r "^2.6.4") (k 0)) (d (n "prse-derive") (r "^1.1.0") (k 0)))) (h "1559flip0lsgm2lh9614va3mz0l7z05iwicifynl8gh5gpq479pq") (f (quote (("std" "alloc" "prse-derive/std" "memchr/std") ("default" "std") ("alloc" "prse-derive/alloc")))) (r "1.61.0")))

(define-public crate-prse-1.1.1 (c (n "prse") (v "1.1.1") (d (list (d (n "memchr") (r "^2.6.4") (k 0)) (d (n "prse-derive") (r "^1.1.1") (k 0)))) (h "0vgjzqrqbldkil54qc97gbv9sdw7x253fqa3shljaypxdqj141wk") (f (quote (("std" "alloc" "prse-derive/std" "memchr/std") ("default" "std") ("alloc" "prse-derive/alloc")))) (r "1.61.0")))

(define-public crate-prse-1.2.0 (c (n "prse") (v "1.2.0") (d (list (d (n "memchr") (r "^2.7.1") (k 0)) (d (n "prse-derive") (r "^1.1.1") (k 0)))) (h "0877ad9s6z2ij615x11a5wg3xdavvqhw00cpgdyazl9h3s01b1fn") (f (quote (("std" "alloc" "prse-derive/std" "memchr/std") ("default" "std") ("alloc" "prse-derive/alloc")))) (r "1.61.0")))

(define-public crate-prse-1.2.1 (c (n "prse") (v "1.2.1") (d (list (d (n "memchr") (r "^2.7.1") (k 0)) (d (n "prse-derive") (r "^1.1.1") (k 0)))) (h "1klrgmn4g9yq2rziy5wzp8a2hl697fvj66s3qnzrfgd7z259m7sk") (f (quote (("std" "alloc" "prse-derive/std" "memchr/std") ("default" "std") ("alloc" "prse-derive/alloc")))) (r "1.70.0")))

