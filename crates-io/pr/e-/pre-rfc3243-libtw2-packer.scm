(define-module (crates-io pr e- pre-rfc3243-libtw2-packer) #:use-module (crates-io))

(define-public crate-pre-rfc3243-libtw2-packer-0.1.0 (c (n "pre-rfc3243-libtw2-packer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "buffer") (r "^0.1.9") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.1") (d #t) (k 2)) (d (n "pre-rfc3243-libtw2-common") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "warn") (r ">=0.1.1, <0.3.0") (d #t) (k 0)))) (h "08pgkpb7ln4hm66jm4nyxz1lx3l3svpwqh1830271nqxziqilgng")))

