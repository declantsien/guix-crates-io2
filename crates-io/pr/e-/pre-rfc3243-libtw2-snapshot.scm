(define-module (crates-io pr e- pre-rfc3243-libtw2-snapshot) #:use-module (crates-io))

(define-public crate-pre-rfc3243-libtw2-snapshot-0.1.0 (c (n "pre-rfc3243-libtw2-snapshot") (v "0.1.0") (d (list (d (n "buffer") (r "^0.1.9") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-common") (r "^0.1") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-gamenet-snap") (r "^0.1") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-packer") (r "^0.1") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)) (d (n "warn") (r ">=0.1.1, <0.3.0") (d #t) (k 0)))) (h "08b446jd2mnldav2zajr2waipf4mjrvfx846dphpbd3lnxplhh1v")))

