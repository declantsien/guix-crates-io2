(define-module (crates-io pr e- pre-rfc3243-libtw2-huffman) #:use-module (crates-io))

(define-public crate-pre-rfc3243-libtw2-huffman-0.1.0 (c (n "pre-rfc3243-libtw2-huffman") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "buffer") (r "^0.1.9") (d #t) (k 0)) (d (n "itertools") (r ">=0.3.0, <0.5.0") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-common") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "18sgi0h49hdzrbm1kjbbwiq2if9f7dz32b7iggxn27472mrksbzl")))

