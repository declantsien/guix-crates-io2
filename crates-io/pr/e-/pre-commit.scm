(define-module (crates-io pr e- pre-commit) #:use-module (crates-io))

(define-public crate-pre-commit-0.1.0 (c (n "pre-commit") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "1qvvd9j1fg38hs2zv3c7r6b8z90lr0l0i54sgi3f8nwx0xbg6m9a")))

(define-public crate-pre-commit-0.2.0 (c (n "pre-commit") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "1g7s124g36i6svfrbibna0g9m2hd550fnwxc389yhdr2fqk58j4j") (y #t)))

(define-public crate-pre-commit-0.3.0 (c (n "pre-commit") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "0czsj3w3agqj2g4n7kqbhz97nvzikcirakam3mi5983v66p9862i") (y #t)))

(define-public crate-pre-commit-0.5.0 (c (n "pre-commit") (v "0.5.0") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "1k9awimrdd0f7r1srlg3ijbzvb99mcvs3gg7zkp2l0xxvnrx2gg1") (y #t)))

(define-public crate-pre-commit-0.5.1 (c (n "pre-commit") (v "0.5.1") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "00wqa9rwh978jacsvkwh5zg4ld464b877hbbqscm2iqvv5fvqh1l") (y #t)))

(define-public crate-pre-commit-0.5.2 (c (n "pre-commit") (v "0.5.2") (d (list (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 1)) (d (n "toml") (r "^0.2") (d #t) (k 1)))) (h "13xglppqipxiqrik6alcpi2df29ji0xij77phv6r3v44gy4bkwnh")))

