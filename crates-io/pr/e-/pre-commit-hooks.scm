(define-module (crates-io pr e- pre-commit-hooks) #:use-module (crates-io))

(define-public crate-pre-commit-hooks-0.1.0 (c (n "pre-commit-hooks") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 1)) (d (n "toml") (r "^0.7") (d #t) (k 1)))) (h "0zh34s60dks23prabnsl3vj7yxnafhggs4f7kmpvmrz48iv4b54l") (y #t)))

(define-public crate-pre-commit-hooks-0.1.1 (c (n "pre-commit-hooks") (v "0.1.1") (d (list (d (n "toml") (r "^0.7") (d #t) (k 1)))) (h "0701hw7xxaqiip8rlsw3y2jwa8mjl1lvlz79dvvhlxyxbysdlj6k") (y #t)))

(define-public crate-pre-commit-hooks-0.2.0 (c (n "pre-commit-hooks") (v "0.2.0") (d (list (d (n "toml") (r "^0.7") (d #t) (k 1)))) (h "1w2qzdm5934yl3dhn0fd5lkkr78zafjdgcp9jxxylhxizbn9qp7c") (y #t)))

(define-public crate-pre-commit-hooks-0.3.0 (c (n "pre-commit-hooks") (v "0.3.0") (d (list (d (n "toml") (r "^0.7") (d #t) (k 1)))) (h "0rkrl3k0svnzw37qgscl2r5pj27c1lnxjl6ida8bmjjrkdc4p5j7")))

