(define-module (crates-io pr e- pre-rfc3243-libtw2-gamenet-snap) #:use-module (crates-io))

(define-public crate-pre-rfc3243-libtw2-gamenet-snap-0.1.0 (c (n "pre-rfc3243-libtw2-gamenet-snap") (v "0.1.0") (d (list (d (n "buffer") (r "^0.1.9") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-common") (r "^0.1") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-gamenet-common") (r "^0.1") (d #t) (k 0)) (d (n "pre-rfc3243-libtw2-packer") (r "^0.1") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "warn") (r ">=0.1.1, <0.3.0") (d #t) (k 0)))) (h "0xdbvnvg077pmpc11b0mfkpdzkbcis7qpxqgm12ncf9xq35d0bix")))

