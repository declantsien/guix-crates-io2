(define-module (crates-io pr e- pre-rfc3243-libtw2-common) #:use-module (crates-io))

(define-public crate-pre-rfc3243-libtw2-common-0.1.0 (c (n "pre-rfc3243-libtw2-common") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "file_offset") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.23") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "0aipggp1fb834iczq4nrnl5pxlyyf7aqp95s8sgbh5pmndyi45b7")))

