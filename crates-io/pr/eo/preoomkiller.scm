(define-module (crates-io pr eo preoomkiller) #:use-module (crates-io))

(define-public crate-preoomkiller-0.0.1 (c (n "preoomkiller") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1sshz8w6hc61rsgb3mdh3fzbjm3higc0vvhd22ihg9zlvyhz0a18")))

(define-public crate-preoomkiller-0.0.2 (c (n "preoomkiller") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0zr61m9j3n43yzy5491nllq5a9kxzlwnvmrdxd4ggwiwmi147hx2")))

(define-public crate-preoomkiller-0.0.4 (c (n "preoomkiller") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0gl9m6a7w8wyi7b1jn5l9zgwy22dn0zdvkzx70kn4d1yiw1vbbq8")))

(define-public crate-preoomkiller-0.0.5 (c (n "preoomkiller") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "08fayl7bx3qxk98k1zrxlmx82djx1j3wlx4rjdmqyah6fil2f3x7")))

(define-public crate-preoomkiller-0.1.0 (c (n "preoomkiller") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (k 0)))) (h "06ayc9907gs34a94w48m6w8qdv8gghging2q84b8lsdqpjj8c3yy")))

