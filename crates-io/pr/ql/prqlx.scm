(define-module (crates-io pr ql prqlx) #:use-module (crates-io))

(define-public crate-prqlx-0.1.0 (c (n "prqlx") (v "0.1.0") (d (list (d (n "prql-compiler") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("macros"))) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("macros" "runtime-tokio-rustls" "sqlite" "migrate"))) (k 2)) (d (n "syn") (r "^2.0.39") (f (quote ("proc-macro" "quote" "parsing"))) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "14pgqlj6s4kl3np3v6msi8jxa34b2j4m7kjbllm2nwhaahs66qms")))

(define-public crate-prqlx-0.2.0 (c (n "prqlx") (v "0.2.0") (d (list (d (n "prql-compiler") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("macros" "runtime-tokio-rustls" "sqlite" "migrate"))) (k 2)) (d (n "syn") (r "^2.0.39") (f (quote ("proc-macro" "quote" "parsing"))) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0x2lr4smc9w8xp2yp37fhfij0a7js1nvkvgqjlwhn0a7s91qviz6")))

(define-public crate-prqlx-0.3.0 (c (n "prqlx") (v "0.3.0") (d (list (d (n "prql-compiler") (r "^0.11.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("macros" "runtime-tokio-rustls" "sqlite" "migrate"))) (k 2)) (d (n "syn") (r "^2.0.53") (f (quote ("proc-macro" "quote" "parsing"))) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1i9201l9hpdxb764j9g93mw92pf8cm01nld35r1wd8x9hid3dv7w")))

