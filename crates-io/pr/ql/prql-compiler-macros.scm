(define-module (crates-io pr ql prql-compiler-macros) #:use-module (crates-io))

(define-public crate-prql-compiler-macros-0.4.2 (c (n "prql-compiler-macros") (v "0.4.2") (d (list (d (n "prql-compiler") (r "^0.4.2") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m6fxm2x1lvylbcy72ijl3pknrcxblmb1vzd4gj9361plpzyswml") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.5.0 (c (n "prql-compiler-macros") (v "0.5.0") (d (list (d (n "prql-compiler") (r "^0.5.0") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07ri145l9rl7h0cj6nwniwf2648i6qqzx15iszrpwaz3j17k8iwx") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.5.1 (c (n "prql-compiler-macros") (v "0.5.1") (d (list (d (n "prql-compiler") (r "^0.5.1") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bvnd1pj75jgi49f0p5mcfcl4ba0f1g9d97cnpnv3g5hzgk9cc6d") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.5.2 (c (n "prql-compiler-macros") (v "0.5.2") (d (list (d (n "prql-compiler") (r "^0.5.2") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16qjnrr722cf7lmq0fhibvidpsbvyv68088g30z7cm4w372y7d17") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.6.0 (c (n "prql-compiler-macros") (v "0.6.0") (d (list (d (n "prql-compiler") (r "^0.6.0") (k 0)) (d (n "syn") (r "=1.0.107") (d #t) (k 0)))) (h "1wficdqpf8nh5p37qfxshab3774ja39nz496dmdbzznfmkbm00nr") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.6.1 (c (n "prql-compiler-macros") (v "0.6.1") (d (list (d (n "prql-compiler") (r "^0.6.1") (k 0)) (d (n "syn") (r "=1.0.107") (d #t) (k 0)))) (h "05az4fbx2idafdaqvk5icj016q6ks4mwyfzx3xcb9kv8ry6xafl3") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.7.0 (c (n "prql-compiler-macros") (v "0.7.0") (d (list (d (n "prql-compiler") (r "^0.7.0") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1101p2f0grwdjs6pa98g8p2xwdrb93vw3qjbbbasggppmx05n6wl") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.7.1 (c (n "prql-compiler-macros") (v "0.7.1") (d (list (d (n "prql-compiler") (r "^0.7.1") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "04f7ixjqq38krxl4jk1xhp9lks43d5wsv66m0g96qn05l2gf322n") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.8.0 (c (n "prql-compiler-macros") (v "0.8.0") (d (list (d (n "prql-compiler") (r "^0.8.0") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "08vw7hsvn9dqdi6vxgnxida6zmpap1x84zlc3jb233m8wa47r1n0") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.8.1 (c (n "prql-compiler-macros") (v "0.8.1") (d (list (d (n "prql-compiler") (r "^0.8.1") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "07pb74v8ngcrz30lx3v6pl2msj6qlab71amkya8dzcyqbiwzix3i") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.9.2 (c (n "prql-compiler-macros") (v "0.9.2") (d (list (d (n "prql-compiler") (r "^0.9.2") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1r76f2q7gwxn9fgai4rbfby7h537z3dr6bhjav6bc0lhzdjghq1h") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.9.3 (c (n "prql-compiler-macros") (v "0.9.3") (d (list (d (n "prql-compiler") (r "^0.9.3") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "04c235cd2790j90n4jvjvd05qzl5054wz9yilvcqwaw41s34vq1p") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.9.4 (c (n "prql-compiler-macros") (v "0.9.4") (d (list (d (n "prql-compiler") (r "^0.9.4") (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1az92k2k06y7snjsp937v1pgaxbxfqlk72pbfdvqriiqdll08673") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.9.5 (c (n "prql-compiler-macros") (v "0.9.5") (d (list (d (n "prql-compiler") (r "^0.9.5") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1g0kplghffjwpzdg02wja8q7c4ww24c48wwvsy7kb2978kfb79qn") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.10.0 (c (n "prql-compiler-macros") (v "0.10.0") (d (list (d (n "prql-compiler") (r "^0.10.0") (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "00945qi8bb71bjr073vl64qj5ryg3d8xra7y91a6zqx65iwvcrxm") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.10.1 (c (n "prql-compiler-macros") (v "0.10.1") (d (list (d (n "prql-compiler") (r "^0.10.1") (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0ifxgfxzwla89snpsjpvfm5g8kh82l43b4mnv648xs69r29v62zj") (r "1.65.0")))

(define-public crate-prql-compiler-macros-0.11.0 (c (n "prql-compiler-macros") (v "0.11.0") (d (list (d (n "prql-compiler") (r "^0.11.0") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "07ldklzad4vrlpcpvv75fq8q4p2x9vp826pfvv8a2gxrh0hfs98n") (r "1.70.0")))

(define-public crate-prql-compiler-macros-0.11.1 (c (n "prql-compiler-macros") (v "0.11.1") (d (list (d (n "prql-compiler") (r "^0.11.1") (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1jz5yk0l1p1n3pjmg95vi7xl3g9pqzyycymnaz785rzwh2m8w94l") (r "1.70.0")))

(define-public crate-prql-compiler-macros-0.11.2 (c (n "prql-compiler-macros") (v "0.11.2") (d (list (d (n "prqlc") (r "^0.11.2") (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "00frn66f5h0p1s7bmh53dsnld8cbrcr76nidz6pxklr5vqd17yl1") (r "1.70.0")))

(define-public crate-prql-compiler-macros-0.11.3 (c (n "prql-compiler-macros") (v "0.11.3") (d (list (d (n "prqlc") (r "^0.11.3") (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0r52j3lzz5i5lfbxb5hf9qr350acvr0kw9kfn4c8rcai0f6f3klf") (r "1.70.0")))

