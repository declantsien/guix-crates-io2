(define-module (crates-io pr ql prql4j) #:use-module (crates-io))

(define-public crate-prql4j-0.1.0 (c (n "prql4j") (v "0.1.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "prql-compiler") (r "^0.1.1") (d #t) (k 0)))) (h "0lk2c55h7nhpryl42bqpq2vl0gvnla8yd6vdkwdr3l3251b22a02")))

