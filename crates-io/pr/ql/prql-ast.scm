(define-module (crates-io pr ql prql-ast) #:use-module (crates-io))

(define-public crate-prql-ast-0.9.2 (c (n "prql-ast") (v "0.9.2") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("std" "derive"))) (d #t) (k 0)))) (h "1wr3z3di0r7j0a7i3i41q4jnc7fsd71sa5xlwq3w47mkbakc5vpx") (r "1.65.0")))

(define-public crate-prql-ast-0.9.3 (c (n "prql-ast") (v "0.9.3") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("std" "derive"))) (d #t) (k 0)))) (h "0nh3gr08xdcqvbhnygfj3cd04y69284clpibvzmpvgjdy5slw6bi") (r "1.65.0")))

(define-public crate-prql-ast-0.9.4 (c (n "prql-ast") (v "0.9.4") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("std" "derive"))) (d #t) (k 0)))) (h "12jy3cf1hvmlcnkxdmkpv6higm09i1jf46pxhbmmywrdp97j6hrc") (r "1.65.0")))

(define-public crate-prql-ast-0.9.5 (c (n "prql-ast") (v "0.9.5") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("std" "derive"))) (d #t) (k 0)))) (h "0finxvbzdp0s6g4fgn759x1f75isd5ayrzxr15a0avgiz4i1bnfr") (r "1.65.0")))

