(define-module (crates-io pr ai praiya) #:use-module (crates-io))

(define-public crate-praiya-0.1.0-rc1 (c (n "praiya") (v "0.1.0-rc1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0-rc1") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.9") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "0y3slhviwdgys88k05c88vsdg2npj1cr1ffxbrg9hjjdnzcbvcir")))

(define-public crate-praiya-0.1.0 (c (n "praiya") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "1n8ff1377wjnyfcirslbarrjf0i6g89hrcy93l5m868dwcr4nn5k")))

(define-public crate-praiya-0.2.0 (c (n "praiya") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "1lw4aix8c2jr4fi5k9y33a32smda7skdf0a13ljg2nhjfs66r2g8")))

(define-public crate-praiya-0.2.1 (c (n "praiya") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "1gn63fvcnrm0r8zgp44ff2yyry6mv27p4jjxzf1xv64vgznbwppp")))

(define-public crate-praiya-0.3.0 (c (n "praiya") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "0p8qjh2id62l6aj3jsa0j7gndrqjw8fj95f24z9z3walj9mwwfpb")))

(define-public crate-praiya-0.3.1 (c (n "praiya") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "11bm03s57g6bifbi8nkppfjv8cgddr3hnj7k56xs4bg24yfk0c9p")))

(define-public crate-praiya-0.3.2 (c (n "praiya") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.1") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "0qyjbx6wfhyyzhjmky7zwr4wjahqd4nfc6qvyx0sa4g9bqnzl7rw")))

(define-public crate-praiya-0.4.0 (c (n "praiya") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ct-logs") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "praiya-macro") (r "=0.1.1") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "00dsbv058235q2jnv5784l7fqbfvmblgp600zsfkxcz26g1zz2im")))

