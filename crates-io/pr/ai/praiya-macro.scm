(define-module (crates-io pr ai praiya-macro) #:use-module (crates-io))

(define-public crate-praiya-macro-0.1.0-rc1 (c (n "praiya-macro") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y6bnwscd9qiwy1b6cbrvdq1qpgfiw5dzm929bcip7d1sabdjdgd")))

(define-public crate-praiya-macro-0.1.0 (c (n "praiya-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g0n4ff58v385wjvdr0ijw8jrfvpnzwjx607kg4zbfvz819h1mp0")))

(define-public crate-praiya-macro-0.1.1 (c (n "praiya-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ggg16hcqsfzhx8ivbf6rrvklg6ngppmzx1rf5zdx4qrm3qwxcpi")))

