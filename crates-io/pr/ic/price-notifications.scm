(define-module (crates-io pr ic price-notifications) #:use-module (crates-io))

(define-public crate-price-notifications-0.1.0 (c (n "price-notifications") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "coingecko") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "twilio") (r "^1.0.2") (d #t) (k 0)))) (h "0qsmf3w33wqmbwf284j04mw86ajydgfg0q9sqisab4xg51zrb7bc")))

