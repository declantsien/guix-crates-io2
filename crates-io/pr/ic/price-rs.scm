(define-module (crates-io pr ic price-rs) #:use-module (crates-io))

(define-public crate-price-rs-0.0.1 (c (n "price-rs") (v "0.0.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0v84cig56gjbgs15mqq80q3nkx0k8dm3k8cp0gk6r6pvwwkp0rcl")))

