(define-module (crates-io pr ic price-gatherer-db) #:use-module (crates-io))

(define-public crate-price-gatherer-db-0.1.0 (c (n "price-gatherer-db") (v "0.1.0") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s6hifywjwr1kvfja1hgkcnya7rfg20l6vn6hmn0mqs6h6bdn787")))

(define-public crate-price-gatherer-db-0.2.0 (c (n "price-gatherer-db") (v "0.2.0") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wacx976a6q9rkw7b7x4w5ki6ja4znsblffbk5g4iabp108nlfig")))

(define-public crate-price-gatherer-db-0.3.0 (c (n "price-gatherer-db") (v "0.3.0") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06wch3ar8s69dr0587zi5qb2rqdkryjxqx7113bs2v1kx60ji8wl")))

(define-public crate-price-gatherer-db-0.3.1 (c (n "price-gatherer-db") (v "0.3.1") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0scmldh4xivhsa27lma2jcsl595fvqql72qxba1rmgzfvzyj2fb4")))

