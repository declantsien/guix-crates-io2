(define-module (crates-io pr ic price-info) #:use-module (crates-io))

(define-public crate-price-info-1.12.0 (c (n "price-info") (v "1.12.0") (d (list (d (n "fake-fetch") (r "^0.0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-fetch") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-runtime") (r "^0.1.0") (d #t) (k 0)))) (h "1yf67kcg5440hscsly80zclr7l3ib07xc0j3bariabxlm004sa3w")))

