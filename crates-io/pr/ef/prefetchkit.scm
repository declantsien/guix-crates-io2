(define-module (crates-io pr ef prefetchkit) #:use-module (crates-io))

(define-public crate-prefetchkit-1.0.0 (c (n "prefetchkit") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "libprefetch") (r "^0.1.1") (d #t) (k 0)))) (h "0i1xjr1s1an5yzqah9l1m31vykkawncqgkr3y10vb2kx43fhg2kj")))

(define-public crate-prefetchkit-1.0.1 (c (n "prefetchkit") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "libprefetch") (r "^0.1.1") (d #t) (k 0)))) (h "1qvxqiaidy8gwqqd02aflpgys52v3cnl6jd1kr3i6alcyi0mxdj9")))

(define-public crate-prefetchkit-1.0.2 (c (n "prefetchkit") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "libprefetch") (r "^0.1.1") (d #t) (k 0)))) (h "0a5v1il0rvhsypnln7kl2c8kcifwg1fqxdj2ck0fbrz28gdxvk9z")))

