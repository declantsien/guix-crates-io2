(define-module (crates-io pr ef preflate-bindings) #:use-module (crates-io))

(define-public crate-preflate-bindings-0.1.0 (c (n "preflate-bindings") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.110") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 2)) (d (n "libpreflate-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0cdndqjjcp6q6y30bfiayshpm71jlvhhfj0vv4m7l2n983fnafkp")))

