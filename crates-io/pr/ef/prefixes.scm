(define-module (crates-io pr ef prefixes) #:use-module (crates-io))

(define-public crate-prefixes-0.1.0 (c (n "prefixes") (v "0.1.0") (d (list (d (n "prefixes-f") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-ms") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-os") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-p") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-re") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-s") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-uppercase-os") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-uppercase-p") (r "^0.1.0") (d #t) (k 0)) (d (n "prefixes-uppercase-re") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "04lqa6x6l7sad4x6r8kk3gypb46la8wqicz3r1gjzl8ii1sjjw8s")))

