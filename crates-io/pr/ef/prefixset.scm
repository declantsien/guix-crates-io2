(define-module (crates-io pr ef prefixset) #:use-module (crates-io))

(define-public crate-prefixset-0.1.0-rc.1 (c (n "prefixset") (v "0.1.0-rc.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vy24dif6jchl7ic8f7k62bhndb7bn1hgh70xc042s40jvfjgi6w")))

(define-public crate-prefixset-0.1.0-rc.2 (c (n "prefixset") (v "0.1.0-rc.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1dy1a8apdwqwqcp5q8kpzcwj6w72n59pb0192isxs2dyzyadqs8q")))

