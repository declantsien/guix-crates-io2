(define-module (crates-io pr ef prefix-tree) #:use-module (crates-io))

(define-public crate-prefix-tree-0.1.0 (c (n "prefix-tree") (v "0.1.0") (h "14iiwmq7m0js2bymxgd6l3xwxcrrjdy9s7z170wfda15rkalprlh")))

(define-public crate-prefix-tree-0.2.1 (c (n "prefix-tree") (v "0.2.1") (h "0ac678h67f0gmn9anxmql66an8pv9icsindh0ylf9a3h9f9h6b2f")))

(define-public crate-prefix-tree-0.3.0 (c (n "prefix-tree") (v "0.3.0") (h "0h5frb1js86gf8rh42xnsjksa7wiw3x4s4nj777n33icrm4qmq1i")))

(define-public crate-prefix-tree-0.4.0 (c (n "prefix-tree") (v "0.4.0") (h "1f6iagcikc28s9zm9d1vri7270zcizx2qr5wkrvlqd8b6f2yqn8p")))

(define-public crate-prefix-tree-0.5.0 (c (n "prefix-tree") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0i7h2f134d54mg26pw7hgwnhd1x14vzawbxv26zznz4vr1h9cj8g")))

