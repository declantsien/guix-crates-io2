(define-module (crates-io pr ef prefixtree) #:use-module (crates-io))

(define-public crate-prefixtree-0.0.2 (c (n "prefixtree") (v "0.0.2") (h "0j4f0vsg4z7fx4yicy8ydmk6n1jca8nha243h0h8qicald0zgksj")))

(define-public crate-prefixtree-0.0.3 (c (n "prefixtree") (v "0.0.3") (h "0x652lh2b7wpp3842qqzay9ag322ia4ll9cysdybbsbbc5vllyav")))

(define-public crate-prefixtree-0.0.5 (c (n "prefixtree") (v "0.0.5") (h "0h52x8saqx42fx07rjzqi84ij3x1jricr5x0ablzpf9fp01a8kpb")))

(define-public crate-prefixtree-0.1.0 (c (n "prefixtree") (v "0.1.0") (h "1566qfnrf56qrn07x7wd5aadd29xsvgg5585q8niw2r0ljbjdpvp")))

(define-public crate-prefixtree-0.1.1 (c (n "prefixtree") (v "0.1.1") (h "0z83gg987ql64lg9pbf09jrbs4z0fhgf5l3qn1ml5ikb706n9anr")))

(define-public crate-prefixtree-0.1.2 (c (n "prefixtree") (v "0.1.2") (h "1pk6yim9nn19slx35iyx8kvfqmy9zkyanvwi4wjxiynbgxm1nc14")))

(define-public crate-prefixtree-0.1.3 (c (n "prefixtree") (v "0.1.3") (h "1k7qw82b1j8sq3z1bklg6fhxw4jfd5hqv4dg8yzh7npdv6vn0xil")))

