(define-module (crates-io pr ef prefetch) #:use-module (crates-io))

(define-public crate-prefetch-0.1.0 (c (n "prefetch") (v "0.1.0") (d (list (d (n "llvmint") (r "^0.0.2") (d #t) (k 0)))) (h "1280pn6nf767bjqkvgy7yxwnzaq9633m9v6085ix0ymf7lvvk2v6")))

(define-public crate-prefetch-0.2.0 (c (n "prefetch") (v "0.2.0") (h "1bz56kcr8c65plz5h8apcmrb2fr6c8l74sybrsd3w64fqrlfv5vq")))

