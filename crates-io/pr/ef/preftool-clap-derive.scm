(define-module (crates-io pr ef preftool-clap-derive) #:use-module (crates-io))

(define-public crate-preftool-clap-derive-0.1.0 (c (n "preftool-clap-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "preftool") (r "^0.1.0") (d #t) (k 2)) (d (n "preftool-clap") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y0pfbwpc7clxd2r6m5xi26qf5cj5rr067fv6hr0sda5mzb8f6gq")))

(define-public crate-preftool-clap-derive-0.2.0 (c (n "preftool-clap-derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "preftool") (r "^0.2.0") (d #t) (k 2)) (d (n "preftool-clap") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v304srawgc54r0ch8jlci6h42pf3x86ckndm6gs701sn08s5kba")))

