(define-module (crates-io pr ef prefer-dynamic) #:use-module (crates-io))

(define-public crate-prefer-dynamic-0.1.0 (c (n "prefer-dynamic") (v "0.1.0") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)))) (h "0kcxs7fj1kb7xxjq4sp28g2h0hi4ycsfmzn8hx15kwif3kpc4pxl") (f (quote (("link-test")))) (y #t)))

(define-public crate-prefer-dynamic-0.1.1 (c (n "prefer-dynamic") (v "0.1.1") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)))) (h "13r9ffysyj8gnlap2z8sv6fp1jfnkvby9cf9b3wrxbzci7p3i2dn") (f (quote (("link-test")))) (y #t)))

(define-public crate-prefer-dynamic-0.1.2 (c (n "prefer-dynamic") (v "0.1.2") (d (list (d (n "build_cfg") (r "^1") (d #t) (k 1)))) (h "1zm4f5k4lr6wvq5da6b7wd5p8habyxrqamm865451zh1gm8lpr3m") (f (quote (("link-test"))))))

