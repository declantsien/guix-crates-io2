(define-module (crates-io pr ef prefixes-uppercase-os) #:use-module (crates-io))

(define-public crate-prefixes-uppercase-os-0.1.0 (c (n "prefixes-uppercase-os") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "12nkvv6qa3pgcwb06svg2f7805hq3ywwc4xgvj9n8m5c1ay7q2qq")))

