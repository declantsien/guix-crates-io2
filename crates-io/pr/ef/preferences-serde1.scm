(define-module (crates-io pr ef preferences-serde1) #:use-module (crates-io))

(define-public crate-preferences-serde1-2.0.0 (c (n "preferences-serde1") (v "2.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0qzrb7g97ngpaigcfff6phdg6020xpddyzgkxnhyjkj2x229rpkm")))

