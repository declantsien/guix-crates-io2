(define-module (crates-io pr ef prefixes-s) #:use-module (crates-io))

(define-public crate-prefixes-s-0.1.0 (c (n "prefixes-s") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0pqfwln6m8kb61qmrzp2ixqdm45zbd0vj5njxiypsbc2pd840a03")))

