(define-module (crates-io pr ef preftool-file) #:use-module (crates-io))

(define-public crate-preftool-file-0.1.0 (c (n "preftool-file") (v "0.1.0") (d (list (d (n "preftool") (r "^0.1.0") (d #t) (k 0)) (d (n "preftool-dirs") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "0fjrpfs6bd0nawg2w48d93847xy8rzp7qw76dkpa103z2xjy1ld9")))

(define-public crate-preftool-file-0.2.0 (c (n "preftool-file") (v "0.2.0") (d (list (d (n "preftool") (r "^0.2.0") (d #t) (k 0)) (d (n "preftool-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "16bqyxpzyzv6nin6ndwxkm8idk67vb3r7hj077lh2al6s6qwdkx3")))

