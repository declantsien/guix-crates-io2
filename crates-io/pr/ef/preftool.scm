(define-module (crates-io pr ef preftool) #:use-module (crates-io))

(define-public crate-preftool-0.1.0 (c (n "preftool") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "caseless") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "1npnnwj6as4f22an2q4ppm2bm575fs33g15k496nis8d8xavgrb1")))

(define-public crate-preftool-0.2.0 (c (n "preftool") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "caseless") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "09hslqlk13lf53k0bjkchwjbfz9hqrr1l5vixzkbvzsx7am5mfkp")))

