(define-module (crates-io pr ef preftool-dirs) #:use-module (crates-io))

(define-public crate-preftool-dirs-0.1.0 (c (n "preftool-dirs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.1") (d #t) (k 0)) (d (n "preftool") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "060hd2dzzk6jqcl2ngrnzaf2gv9ak04y9p2nsx11irihi8nq7whh")))

(define-public crate-preftool-dirs-0.2.0 (c (n "preftool-dirs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.1") (d #t) (k 0)) (d (n "preftool") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "06q501kik83gd2vbqhvj3qq5zkj3mday2f1w0fi9bsy69imi4jrr")))

