(define-module (crates-io pr ef prefix-sum-vec) #:use-module (crates-io))

(define-public crate-prefix-sum-vec-0.1.0 (c (n "prefix-sum-vec") (v "0.1.0") (h "062n2k4w7ipy1942l7g06bbiqb27dqhcfszapr70bgzjcfpskhb2")))

(define-public crate-prefix-sum-vec-0.1.1 (c (n "prefix-sum-vec") (v "0.1.1") (h "1q1rlv9r87ys9vjlbb4j29zllflrv2kzdpfc2578p916v8s7qgvl")))

(define-public crate-prefix-sum-vec-0.1.2 (c (n "prefix-sum-vec") (v "0.1.2") (h "1fmxv0g6vfrzcbr25mqn56ylg9jg2ssazdm9kfn7cvlbcd8vs1ma")))

