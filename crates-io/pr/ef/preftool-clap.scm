(define-module (crates-io pr ef preftool-clap) #:use-module (crates-io))

(define-public crate-preftool-clap-0.1.0 (c (n "preftool-clap") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "preftool") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "1wxr3lbaskkh0l4h9h4np04ymgnskvmxnjj2igvj494d1j42xqfd")))

(define-public crate-preftool-clap-0.2.0 (c (n "preftool-clap") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "preftool") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "0iap8g7yjycfc8pwnm31yz7bpgljmw11azf0ff7y0wm1s7ixxkwy")))

