(define-module (crates-io pr ef prefix-manager) #:use-module (crates-io))

(define-public crate-prefix-manager-0.0.1 (c (n "prefix-manager") (v "0.0.1") (h "09k1v0xqix18d9w8gv1pcw1mvr4mkqn1lg7qwwjydxmp7v69ksmq") (y #t) (r "1.62")))

(define-public crate-prefix-manager-0.0.2 (c (n "prefix-manager") (v "0.0.2") (h "0g3ivsf8pgdjs3f73pk6v8w9wf3kh4s4rfh5php9nv9zcclmp5i1") (r "1.62")))

