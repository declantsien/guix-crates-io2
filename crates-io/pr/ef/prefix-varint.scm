(define-module (crates-io pr ef prefix-varint) #:use-module (crates-io))

(define-public crate-prefix-varint-0.0.1-alpha.1 (c (n "prefix-varint") (v "0.0.1-alpha.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1mad1r5pj3r6qyc917zbmnkb88sg8r2afv2al6as6afa4h96igrh")))

(define-public crate-prefix-varint-0.0.1-alpha.2 (c (n "prefix-varint") (v "0.0.1-alpha.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1926kyb1s04j57c5g1iqa65l57kqkgx0d5b2196x19ihw98g2jk7")))

(define-public crate-prefix-varint-0.0.1-alpha.3 (c (n "prefix-varint") (v "0.0.1-alpha.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ccxg4ma6sjrrf6lvjzbbjgzz16wx767w8s8vf12ixxc2lf4p704")))

(define-public crate-prefix-varint-0.0.1-alpha.4 (c (n "prefix-varint") (v "0.0.1-alpha.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "086a7ia5alkl0g9cpibwjb89b0w4v1fwx2p7pyhwaipcf89hjxs6")))

(define-public crate-prefix-varint-0.0.1-alpha.5 (c (n "prefix-varint") (v "0.0.1-alpha.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hd1v5jv5009rnd92p1671z0j1awv8iq5by36day8hdhqwynwcbl")))

(define-public crate-prefix-varint-0.0.1-alpha.6 (c (n "prefix-varint") (v "0.0.1-alpha.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14530jvvxssnjgqjgfjh0vpyzmmkiqypcb9rgsrcmq0nj5cnigwb")))

