(define-module (crates-io pr ef prefab_macro) #:use-module (crates-io))

(define-public crate-prefab_macro-0.1.0 (c (n "prefab_macro") (v "0.1.0") (d (list (d (n "prefab_core") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0n9y3dw2362h8lf7qcv3y6nghkivbmyj3flys5n0zxji260m8zf7") (y #t)))

