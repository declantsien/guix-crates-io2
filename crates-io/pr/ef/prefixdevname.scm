(define-module (crates-io pr ef prefixdevname) #:use-module (crates-io))

(define-public crate-prefixdevname-0.2.0 (c (n "prefixdevname") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "libudev") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "1fac1hidljvw5gv71rghsri175nqyzmcsmhh9kzn44s024yxv5sx")))

