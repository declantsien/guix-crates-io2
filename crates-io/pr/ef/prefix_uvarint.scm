(define-module (crates-io pr ef prefix_uvarint) #:use-module (crates-io))

(define-public crate-prefix_uvarint-0.1.0 (c (n "prefix_uvarint") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nv2mqazxcx1v119n9m1ra7b1vj47jnmypi9p12zl6aw65vqsrcb")))

(define-public crate-prefix_uvarint-0.2.0 (c (n "prefix_uvarint") (v "0.2.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0iqmrkmydavg9rilplxvsknym0i2l1dzfazdifvyvxnf1fx7wjd4")))

(define-public crate-prefix_uvarint-0.3.0 (c (n "prefix_uvarint") (v "0.3.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bqvrf5krcb3i1ns6c1rg2hiy5l9jcc8iqj4286m48va27wz7sil")))

(define-public crate-prefix_uvarint-0.4.0 (c (n "prefix_uvarint") (v "0.4.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0d4p7fhbkk79q5q8ja78ccbqpl3kwyakcjx4i0jkjxw15py3hm4l")))

(define-public crate-prefix_uvarint-0.4.1 (c (n "prefix_uvarint") (v "0.4.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0l1hzd0019vj8njj8p358djni81pp6jddbw008g40wj5823dcvkz")))

(define-public crate-prefix_uvarint-0.5.0 (c (n "prefix_uvarint") (v "0.5.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ncb6jb9pfxfm7pcgdxadlj6m2kpiqssl6s7s8l10ksmar2hsys8")))

(define-public crate-prefix_uvarint-0.5.1 (c (n "prefix_uvarint") (v "0.5.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1iqrmgpbii5fwyvfpdpx68hxa5r4h59r29w4hx42rl3539sizkx0")))

(define-public crate-prefix_uvarint-0.6.0 (c (n "prefix_uvarint") (v "0.6.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "004izcimcrk9j2ahpasyl2ixf50xzhm55dksjx9qkdqd611jh4d4")))

(define-public crate-prefix_uvarint-0.6.1 (c (n "prefix_uvarint") (v "0.6.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04ha0lydfpdkr08xm781wrjjnvzjx2ggzizxjlscx5ps0scda1p5")))

