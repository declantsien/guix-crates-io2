(define-module (crates-io pr ef prefab) #:use-module (crates-io))

(define-public crate-prefab-0.1.0 (c (n "prefab") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "openssh") (r "^0.8.0") (d #t) (k 0)) (d (n "prefab_core") (r "^0") (d #t) (k 0)) (d (n "prefab_macro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0z3vmpg5h0pyp32j13pwm9abkqxh5bsvkiankycj2cwaxybp16hk") (y #t)))

