(define-module (crates-io pr ef prefixopt_derive) #:use-module (crates-io))

(define-public crate-prefixopt_derive-0.1.0 (c (n "prefixopt_derive") (v "0.1.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "prefixopt") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1yz3g3yc57jywq5pdmqj6xi8ja6v8lw4bsx39h7vmyp99sbv2n18")))

(define-public crate-prefixopt_derive-0.2.0 (c (n "prefixopt_derive") (v "0.2.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "prefixopt") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0qcanaqw2g5kwaphdl3vvxay83mrki0wik3fvfwch0w6fijby79j")))

(define-public crate-prefixopt_derive-0.3.0 (c (n "prefixopt_derive") (v "0.3.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "prefixopt") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1w6k3abr4dbpcr8032ma6l30mq13x7nxgvmsvdj4abcjir7khpfd")))

(define-public crate-prefixopt_derive-0.4.0 (c (n "prefixopt_derive") (v "0.4.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "prefixopt") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0ydmn16bqpjxlg5rznnj9af96g4i4rpn9yq2zdyr385l8nq7jix8")))

