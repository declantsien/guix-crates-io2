(define-module (crates-io pr ef preferences) #:use-module (crates-io))

(define-public crate-preferences-0.1.0 (c (n "preferences") (v "0.1.0") (d (list (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_json") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 0)))) (h "1nndf3xwi6jndznxgkjy3xfc83dbj0dsvx5qdmljjiqlqd61jdzi")))

(define-public crate-preferences-0.2.0 (c (n "preferences") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.46") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1xpnsgjcmihcsb6wax0kqjmyvp1zcxhb94f4g2wsf56ihimjgm7y") (f (quote (("default"))))))

(define-public crate-preferences-0.2.1 (c (n "preferences") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1sarf4nm49r16x1bf3h25c0iywca7g7h9gmvs6pg9phly5q6vp6i")))

(define-public crate-preferences-0.3.0 (c (n "preferences") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0k6p9f8sg48p6az2rgqfjx2kpfkkanczb6ndmy36p7xyibbas3gy")))

(define-public crate-preferences-0.4.0 (c (n "preferences") (v "0.4.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0jqdvl7p90m2fz55jr0aci5ar9hz4xsbqp76d4nmlh86krk8syvz")))

(define-public crate-preferences-0.5.0 (c (n "preferences") (v "0.5.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "052gxc799aipig2lql18d7bmxryd97vm5k0x9018g5lgymyyl7ms")))

(define-public crate-preferences-0.6.0 (c (n "preferences") (v "0.6.0") (d (list (d (n "app_dirs") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1az3ainjplkpizc8v9mjqrbpclncahrvil238yif0rlj6nm3j0qd")))

(define-public crate-preferences-0.7.0 (c (n "preferences") (v "0.7.0") (d (list (d (n "app_dirs") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1cbimfndzch7s50zz1447n25q4ibbz354kf5jdl1w9fcl552dxfj")))

(define-public crate-preferences-0.7.1 (c (n "preferences") (v "0.7.1") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "00s213b2c9n4j3q3zafxd05ca1gqzqfw2flazddq89rg681xfqyv")))

(define-public crate-preferences-0.7.2 (c (n "preferences") (v "0.7.2") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0dxnbnf9196mdrd993dy94pvnd7anpq33jdix4pg6lk3p55fzsrf")))

(define-public crate-preferences-0.7.3 (c (n "preferences") (v "0.7.3") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0p88sr23qkjivcq024q9s6bq04bya66pv0qfcmy9ac96k7nkr77z")))

(define-public crate-preferences-1.0.0 (c (n "preferences") (v "1.0.0") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 0)))) (h "1gfg6x832fmr885ilpfx50l4csp3dqjzvn10dz7bhl8fi499hil7")))

(define-public crate-preferences-1.1.0 (c (n "preferences") (v "1.1.0") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 0)))) (h "0nk063iqqs0rzyw4hnvk7mx9i0rf975b04dwp820ky7chvd4nwns")))

