(define-module (crates-io pr ef prefix_array) #:use-module (crates-io))

(define-public crate-prefix_array-0.1.0 (c (n "prefix_array") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.15") (d #t) (k 0)))) (h "0igvsx27ddp0fy2gqghzi7q0nf06wrsvnfya174n79235h26657w") (f (quote (("std") ("default" "std"))))))

(define-public crate-prefix_array-0.2.0 (c (n "prefix_array") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.15") (d #t) (k 0)))) (h "1d591f22fdnr0x48kk2f54qgxlqfn6j6f651v964whx8hlhjc043") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-prefix_array-0.2.1 (c (n "prefix_array") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "1vvgcmksl1n4ka431slb9aly6dcmf6f9fdn1w2dfzxf169clvxij") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-prefix_array-0.2.2 (c (n "prefix_array") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "11b9gnjwbjdwbb3nik9mxicjnmwzc301hqnkyxs2zikxxxswm27z") (f (quote (("std") ("default" "std"))))))

(define-public crate-prefix_array-0.2.3 (c (n "prefix_array") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "0lh8a2gqw2ssgcsq4nbnk656xpp7n993yvqgsrllrnpf3p3w5y8l") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-prefix_array-0.2.4 (c (n "prefix_array") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "1nhgv1frzj6mrppsnggmg61d4x5wqg220jvmzsq8rrqqzh58bviz") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-prefix_array-0.2.5 (c (n "prefix_array") (v "0.2.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "1prnrg2mr6rnb9qs5k6z6ys5bc49g46i55zava0xznvnlwl6dj58") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-prefix_array-0.3.0 (c (n "prefix_array") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "1v1jr3x7iryhan6mplakbrg2psdarbbwfxndv30zd6mid58n6hj1") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-prefix_array-0.3.1 (c (n "prefix_array") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "1qc64gcsw4js8660b34c1gkgd3wcgas335qq6a4bx8hsy407l2b7") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-prefix_array-0.3.2 (c (n "prefix_array") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 2)))) (h "15m73hw2af9v2ds64ps5dxmkz49dwhsll4a45i74b1anjxli5rqz") (f (quote (("std") ("default" "std")))) (r "1.65")))

