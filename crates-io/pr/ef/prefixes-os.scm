(define-module (crates-io pr ef prefixes-os) #:use-module (crates-io))

(define-public crate-prefixes-os-0.1.0 (c (n "prefixes-os") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0yf9kjj1sfa9v9k5vxhqa7qq822gc8dxmlij8hd78jqdzzgq3zhx")))

