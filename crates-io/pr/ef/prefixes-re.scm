(define-module (crates-io pr ef prefixes-re) #:use-module (crates-io))

(define-public crate-prefixes-re-0.1.0 (c (n "prefixes-re") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0vc1bhj4dc0bg0143n89waqjldpmqzpy05pi5bmgwrl8sjcwzdm1")))

