(define-module (crates-io pr ef prefixes-uppercase-p) #:use-module (crates-io))

(define-public crate-prefixes-uppercase-p-0.1.0 (c (n "prefixes-uppercase-p") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1dc64mwywpv1nqwnlxh4iflsici1191jzqng3d4ndy42604mxs4b")))

