(define-module (crates-io pr ef preftool-derive-core) #:use-module (crates-io))

(define-public crate-preftool-derive-core-0.2.0 (c (n "preftool-derive-core") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sf92df3pbqgdpfqzflbq8z1h4pqyjk728pnfzm8bashqdy3kfx8")))

