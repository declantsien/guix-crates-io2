(define-module (crates-io pr ef prefixes-f) #:use-module (crates-io))

(define-public crate-prefixes-f-0.1.0 (c (n "prefixes-f") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1ilqqglkbr9gqm7grxmjciwas4rbm8y2lq3jbab797iklz8qx2m7")))

