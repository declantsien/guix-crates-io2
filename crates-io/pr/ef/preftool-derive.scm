(define-module (crates-io pr ef preftool-derive) #:use-module (crates-io))

(define-public crate-preftool-derive-0.2.0 (c (n "preftool-derive") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "preftool") (r "^0.2.0") (d #t) (k 2)) (d (n "preftool-derive-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nz2s6cmb9npj33gnp9hsx9rq0mq6hb7k3797k9533wpm7y4b6yl")))

