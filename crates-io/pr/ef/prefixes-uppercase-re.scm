(define-module (crates-io pr ef prefixes-uppercase-re) #:use-module (crates-io))

(define-public crate-prefixes-uppercase-re-0.1.0 (c (n "prefixes-uppercase-re") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "05in5jc7fas3ifrix4hyxsfp79ng7y0zvcwgfwp0qmb9grm0diwi")))

