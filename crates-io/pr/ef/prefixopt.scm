(define-module (crates-io pr ef prefixopt) #:use-module (crates-io))

(define-public crate-prefixopt-0.1.0 (c (n "prefixopt") (v "0.1.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "map_in_place") (r "^0.1.0") (d #t) (k 0)))) (h "0lanv24hyw6c9jvhsa7l3f9g1rzxg7jlr6g4v072s7mf88prgydq")))

(define-public crate-prefixopt-0.2.0 (c (n "prefixopt") (v "0.2.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "map_in_place") (r "^0.1.0") (d #t) (k 0)))) (h "1vnz8jwy6fy2s7qkvn4b8n7z42y4mm626x9h7vs0bw6jal2s3p29")))

(define-public crate-prefixopt-0.2.1 (c (n "prefixopt") (v "0.2.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "map_in_place") (r "^0.1.0") (d #t) (k 0)))) (h "01j9aibr4i91m2595n5qbp2lql1m1lcw7p2pxkkm538khh9rb828")))

(define-public crate-prefixopt-0.3.0 (c (n "prefixopt") (v "0.3.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "map_in_place") (r "^0.1.0") (d #t) (k 0)))) (h "0cdd22sg6fy5wyzsfdk8bf2xzgrcy0wlrhccrpha2q51bnyc7fgw")))

(define-public crate-prefixopt-0.4.0 (c (n "prefixopt") (v "0.4.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "map_in_place") (r "^0.1.0") (d #t) (k 0)))) (h "0n51cm6lhvf9kfbq0xzffrf6j4lfgd0mxx9iyy08nblf06psnqg4")))

