(define-module (crates-io pr ef prefix_num_ops) #:use-module (crates-io))

(define-public crate-prefix_num_ops-0.1.0 (c (n "prefix_num_ops") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1say7s16mvkl3h33jd87bdyszlx8babqaz6ns1kq8qsn67n84s0c") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-prefix_num_ops-0.1.1 (c (n "prefix_num_ops") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1998flr17vvf4y7xgjlabz3d0ghlgg9j0wbqg16f7r5jz9clm06w") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-prefix_num_ops-0.1.2 (c (n "prefix_num_ops") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0k3b1qavqd1gjdb1pkp8k84x2gnjvw7yv3hv8qx4znwlwwnqv4bm") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-prefix_num_ops-0.1.3 (c (n "prefix_num_ops") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g5cnyzgjfmjxqh96my3cimki9c5hj06xxgd40shriaa6v70wd9a") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

