(define-module (crates-io pr ef preferences-ron) #:use-module (crates-io))

(define-public crate-preferences-ron-2.0.0 (c (n "preferences-ron") (v "2.0.0") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1az569ihdsnki68c0mrpq4n1cb851vfdv1ylbp1wqi3kqvswfsll")))

(define-public crate-preferences-ron-2.0.1 (c (n "preferences-ron") (v "2.0.1") (d (list (d (n "app_dirs") (r "^2.3") (d #t) (k 0) (p "app_dirs2")) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1v56wj0k3c74z0i91r2jri5l2nqj9ckbclwfz9vwfkasj28sbsml")))

