(define-module (crates-io pr ef prefixes-ms) #:use-module (crates-io))

(define-public crate-prefixes-ms-0.1.0 (c (n "prefixes-ms") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0kj64j3r246j24iq8bhwl94xw1a3gn1jl8hm0pkk72fyvwakrvfx")))

