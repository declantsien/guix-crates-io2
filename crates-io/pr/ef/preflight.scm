(define-module (crates-io pr ef preflight) #:use-module (crates-io))

(define-public crate-preflight-0.1.0 (c (n "preflight") (v "0.1.0") (h "0rb9z5hmm4i1h1vkr4xqchpiqhd3zdn61mc899lv16hi2chfl76z") (y #t)))

(define-public crate-preflight-0.1.1 (c (n "preflight") (v "0.1.1") (h "0hn6jg3kkak30gbasa4y6bkb5sa7x5kcfmh7i5g7za3k06bzhm7d") (y #t)))

(define-public crate-preflight-0.1.2 (c (n "preflight") (v "0.1.2") (d (list (d (n "url") (r "^2.2.0") (o #t) (d #t) (k 0)))) (h "19m5mg27a3b7lgszssf333n4rag55lh548r3b7gwasc0ia8l7wbq") (y #t)))

(define-public crate-preflight-0.1.3 (c (n "preflight") (v "0.1.3") (d (list (d (n "url") (r "^2.2.0") (o #t) (d #t) (k 0)))) (h "09dy0ibpx7yywrb26m091kl123k219fyss572vfbk281n9p1aj8p") (y #t)))

