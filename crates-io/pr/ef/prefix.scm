(define-module (crates-io pr ef prefix) #:use-module (crates-io))

(define-public crate-prefix-0.1.0 (c (n "prefix") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0rx2ai67iqm8pbbh21mjiwi6ayy7hcir5wrk51dv8348d7z0lffl")))

(define-public crate-prefix-0.1.1 (c (n "prefix") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1c0baj9yqpxpqll7mbjjffg2rq41j74cwsg0l718xrasdm9b19xr") (y #t)))

(define-public crate-prefix-0.1.2 (c (n "prefix") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "13amv7ql0lq9sjs4fznfvmap7wa9f2i9dj63qfcgli5lx00wblw7")))

(define-public crate-prefix-0.1.3 (c (n "prefix") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07wb08gcf61sizavcd1xhkm7b7yk9nzmzj4nmdzjli6plvfgjydh")))

(define-public crate-prefix-0.1.4 (c (n "prefix") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0z7gifz9762d043m63rrcimfl0fgxm581hw2apn0l2aqbm957lic")))

(define-public crate-prefix-0.1.5 (c (n "prefix") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "15m2q65skvdkyzxp28xk7gylkv2x1sfblfrb1gkj3kxbcksivdk4")))

(define-public crate-prefix-1.0.0 (c (n "prefix") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1mxvnda21i8942r3mv1xf8qw36q4knssxi1fqs1a7f6h4ahsfygx")))

(define-public crate-prefix-1.0.1 (c (n "prefix") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "11qgf9kbpz150fga9r9rn9vasz4kllxqm3xklfgsdvdyx7h7ns5x")))

(define-public crate-prefix-1.0.2 (c (n "prefix") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1.6") (d #t) (k 1)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1nncpna0j6ychc3bls293rb2n86df1sgr9ysn84k3vcwpdyg782m")))

(define-public crate-prefix-1.0.3 (c (n "prefix") (v "1.0.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1.6") (d #t) (k 1)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1p5c7yxgb4sdmbvqa0q3w58fx5k9pn0dq4nd2dww18hia318hak3")))

(define-public crate-prefix-1.0.4 (c (n "prefix") (v "1.0.4") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1.6") (d #t) (k 1)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "12gvpnwcnv77s84c8677002y4q4gb3pzfxshv7a1gvqmbxhgn564")))

