(define-module (crates-io pr ef prefab_core) #:use-module (crates-io))

(define-public crate-prefab_core-0.1.0 (c (n "prefab_core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "openssh") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0qns5qjia8wsvd0yr4qzr4kf1bmq3q62r614cakahyvz636g8k9m") (y #t)))

