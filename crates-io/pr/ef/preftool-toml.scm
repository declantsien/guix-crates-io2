(define-module (crates-io pr ef preftool-toml) #:use-module (crates-io))

(define-public crate-preftool-toml-0.1.0 (c (n "preftool-toml") (v "0.1.0") (d (list (d (n "preftool") (r "^0.1.0") (d #t) (k 0)) (d (n "preftool-file") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03rfi6xdq99maslz3m19h9a63vg273hsagfs0492clsx2baxiywq")))

(define-public crate-preftool-toml-0.2.0 (c (n "preftool-toml") (v "0.2.0") (d (list (d (n "preftool") (r "^0.2.0") (d #t) (k 0)) (d (n "preftool-file") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1v2ilbmplbc42xpdwydzi7kwblj1hppq0l6l4za0j6cvwzsxqy8k")))

