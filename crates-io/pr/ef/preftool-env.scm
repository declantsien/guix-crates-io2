(define-module (crates-io pr ef preftool-env) #:use-module (crates-io))

(define-public crate-preftool-env-0.1.0 (c (n "preftool-env") (v "0.1.0") (d (list (d (n "preftool") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "098zbmkr29fzhyfafhnb2nq72n6afvf0zljqs4wvffqf4a5xi3gl")))

(define-public crate-preftool-env-0.2.0 (c (n "preftool-env") (v "0.2.0") (d (list (d (n "preftool") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)))) (h "1b3jax02r0jy4s7xs0lydj5nyr0xaaf5zfar18gdlqkds9k0n595")))

