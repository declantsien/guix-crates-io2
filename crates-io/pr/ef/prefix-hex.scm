(define-module (crates-io pr ef prefix-hex) #:use-module (crates-io))

(define-public crate-prefix-hex-0.1.0 (c (n "prefix-hex") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0.6") (k 2)) (d (n "primitive-types") (r "^0.10.1") (o #t) (k 0)) (d (n "uint") (r "^0.9.3") (o #t) (k 0)))) (h "17a5n6js409rj0fj0wmbsx6d0lq17f1jvj35pa4shns6cn99xjiy") (f (quote (("primitive-types1" "primitive-types" "uint"))))))

(define-public crate-prefix-hex-0.2.0 (c (n "prefix-hex") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0.6") (k 2)) (d (n "primitive-types") (r "^0.10.1") (o #t) (k 0)) (d (n "uint") (r "^0.9.3") (o #t) (k 0)))) (h "1iq2m4gwkal36wf34q40idl2sqznj4smv66rv06sa1qg713hj4hg") (f (quote (("primitive-types1" "primitive-types" "uint"))))))

(define-public crate-prefix-hex-0.3.0 (c (n "prefix-hex") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0.6") (k 2)) (d (n "primitive-types") (r "^0.10.1") (o #t) (k 0)) (d (n "uint") (r "^0.9.3") (o #t) (k 0)))) (h "0b6pc10n8f96kgv1wgcy7xac3f2h1iys25kaph8ya4imswpbxjwz") (f (quote (("primitive-types1" "primitive-types" "uint"))))))

(define-public crate-prefix-hex-0.4.0 (c (n "prefix-hex") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0.6") (k 2)) (d (n "primitive-types") (r "^0.11.1") (o #t) (k 0)) (d (n "uint") (r "^0.9.3") (o #t) (k 0)))) (h "05r35da9m3gnil0wsvyvfjbg13y8sm6r7jymcs035rcghjv6ald8") (f (quote (("std")))) (s 2) (e (quote (("primitive-types" "dep:primitive-types" "uint"))))))

(define-public crate-prefix-hex-0.5.0 (c (n "prefix-hex") (v "0.5.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0") (k 2)) (d (n "primitive-types") (r "^0.12") (o #t) (k 0)) (d (n "uint") (r "^0.9") (o #t) (k 0)))) (h "1177c8v0pxrd9ms7k8mwq39md7h37wxqiz68wjk7cnqqi1l3kqvm") (f (quote (("std")))) (s 2) (e (quote (("primitive-types" "dep:primitive-types" "dep:uint")))) (r "1.60")))

(define-public crate-prefix-hex-0.6.0 (c (n "prefix-hex") (v "0.6.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0") (k 2)) (d (n "primitive-types") (r "^0.12") (o #t) (k 0)) (d (n "uint") (r "^0.9") (o #t) (k 0)))) (h "00c1yzfskrbn3k0mwcn3chsa0ad9lf24ih3a6m4pr52gxsynbdz3") (f (quote (("std")))) (s 2) (e (quote (("primitive-types" "dep:primitive-types" "dep:uint")))) (r "1.60")))

(define-public crate-prefix-hex-0.7.0 (c (n "prefix-hex") (v "0.7.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0") (k 2)) (d (n "primitive-types") (r "^0.12") (o #t) (k 0)) (d (n "uint") (r "^0.9") (o #t) (k 0)))) (h "0jimrlwk88pj0va84d68wsx4n2apdcajvy8cb02ifpn5q6c3jrfr") (f (quote (("std")))) (s 2) (e (quote (("primitive-types" "dep:primitive-types" "dep:uint")))) (r "1.60")))

(define-public crate-prefix-hex-0.7.1 (c (n "prefix-hex") (v "0.7.1") (d (list (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0") (k 2)) (d (n "primitive-types") (r "^0.12") (o #t) (k 0)) (d (n "uint") (r "^0.9") (o #t) (k 0)))) (h "0jf226rjis37gchjxnyr8rfd68pk7xnl9v2ig6axc6ipk3rrj5qz") (f (quote (("std")))) (s 2) (e (quote (("primitive-types" "dep:primitive-types" "dep:uint")))) (r "1.60")))

