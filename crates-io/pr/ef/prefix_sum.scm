(define-module (crates-io pr ef prefix_sum) #:use-module (crates-io))

(define-public crate-prefix_sum-0.1.0 (c (n "prefix_sum") (v "0.1.0") (d (list (d (n "num-bigint") (r "~0.2.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "~0.2.0") (o #t) (d #t) (k 0)) (d (n "num-rational") (r "~0.2.0") (o #t) (d #t) (k 0)))) (h "19mds6910zs3vfravq6zzwqw98ckwvbxdr4x2799w8d19rmjlf9l") (f (quote (("num" "num-bigint" "num-rational" "num-complex"))))))

