(define-module (crates-io pr ef prefix-map) #:use-module (crates-io))

(define-public crate-prefix-map-0.1.0 (c (n "prefix-map") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "1i2dc4mm2hf5k9456ydzv3nw30vmc31aj57q444dsjax2aa4wbhc") (f (quote (("unstable") ("default"))))))

(define-public crate-prefix-map-0.2.0 (c (n "prefix-map") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "1ak5kmc0bs0z0bcjawdqvbzhlwzm4waysln06v0vb40sklkhg0pk") (f (quote (("unstable") ("default"))))))

(define-public crate-prefix-map-0.2.1 (c (n "prefix-map") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "0z4cksbb0wppy8psynj1ng2h9ph8d17h43bx3qh45rssx9b8rjf3") (f (quote (("unstable") ("default"))))))

(define-public crate-prefix-map-0.3.0 (c (n "prefix-map") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "15zpl7f7i2njazyfzvj9hw6ck6clliw1r7mwmkrfid1aznnm3z4n") (f (quote (("unstable") ("default"))))))

(define-public crate-prefix-map-0.3.1 (c (n "prefix-map") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "0r9zkp3nm0spnwz264g3q71y3z4s7n8xczsb09bphjcracqpdmi7") (f (quote (("unstable") ("default"))))))

