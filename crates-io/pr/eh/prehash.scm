(define-module (crates-io pr eh prehash) #:use-module (crates-io))

(define-public crate-prehash-0.1.0 (c (n "prehash") (v "0.1.0") (h "1cm3mhw5j8czbk1fhc2xk4r36vvbfrdxdlhigaxi68h867lkrvq7") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-prehash-0.2.0 (c (n "prehash") (v "0.2.0") (h "180zwx4qajqsrc82b3mdnqlfz8i7z72r1wwm33j4h1z29gb6n0r2") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-prehash-0.2.1 (c (n "prehash") (v "0.2.1") (h "1rhdw718wn7kzykln7l1l5y6blcim6crvi01lgvhf16xm079hj60") (f (quote (("std") ("default" "std"))))))

(define-public crate-prehash-0.3.0 (c (n "prehash") (v "0.3.0") (h "12w8aracld2if9lwq5lj180ws2kvb9zycbl58s4767ljvkcbn42m") (f (quote (("std") ("default" "std"))))))

(define-public crate-prehash-0.3.1 (c (n "prehash") (v "0.3.1") (h "0ickisqan6mnd123p7j5ncrc9pa0l0kfkkqam0b70p6dg27hl9g2") (f (quote (("std") ("default" "std"))))))

(define-public crate-prehash-0.3.2 (c (n "prehash") (v "0.3.2") (h "1khkns6gqw27z9fbxrpr958ifiq1lvp2wv3xmf4kqi9ymdcnxiql") (f (quote (("std") ("default" "std"))))))

(define-public crate-prehash-0.3.3 (c (n "prehash") (v "0.3.3") (h "0k384bmbzvbk0mscqi19lpyy22yfmsjq3vw8b50ri3np6np5qbrb") (f (quote (("std") ("doc_cfg") ("default" "std"))))))

(define-public crate-prehash-1.0.0 (c (n "prehash") (v "1.0.0") (h "1lb46pvy0c2q644s4drlxayg1rqach0laxniz2nrp3ff0qlsdgq4") (f (quote (("std") ("doc_cfg") ("default" "std"))))))

