(define-module (crates-io pr sq prsqlite) #:use-module (crates-io))

(define-public crate-prsqlite-0.1.0 (c (n "prsqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ll6i9cb2hv4f4cki06710mzgldw7ld786d6979jrbk7k138i9wv")))

