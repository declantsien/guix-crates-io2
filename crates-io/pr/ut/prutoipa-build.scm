(define-module (crates-io pr ut prutoipa-build) #:use-module (crates-io))

(define-public crate-prutoipa-build-0.0.0 (c (n "prutoipa-build") (v "0.0.0") (h "1yg0bgm1jsm1a8g32lzha2l7h0qc5x10ak8iigv1i1v27mk9zkzg")))

(define-public crate-prutoipa-build-0.1.0 (c (n "prutoipa-build") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1ci91fpdi6jl5ah2fl5bv3gsrxvd67qj4yrwjyhj5raq4wymk3g7")))

(define-public crate-prutoipa-build-0.1.1 (c (n "prutoipa-build") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0dziqdc4dys0hwmcy7mdc1ygswhmkfr1r10rr6kryp9msddi4z6f")))

(define-public crate-prutoipa-build-0.1.2 (c (n "prutoipa-build") (v "0.1.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0kiw4pyfwshcxdrwjv5h5fwrfjd3mxigjcgns26m6srcjs22x1p3")))

(define-public crate-prutoipa-build-0.1.3 (c (n "prutoipa-build") (v "0.1.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0knddnccf46ycsy4md2lcbv87di1mbyln20jh2c0lc0mjym9scj2")))

(define-public crate-prutoipa-build-0.1.4 (c (n "prutoipa-build") (v "0.1.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1vs1s69njl316cln6g3gki6sdcm6nq0b6bf0sz6a239kwrd15q8m")))

