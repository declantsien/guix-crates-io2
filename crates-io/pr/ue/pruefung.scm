(define-module (crates-io pr ue pruefung) #:use-module (crates-io))

(define-public crate-pruefung-0.1.0 (c (n "pruefung") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (o #t) (d #t) (k 0)))) (h "13igbfhr8qgx4g0m9n9bxzb995ishlhy7s6g5z20p3pcxxii8fsi") (f (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

(define-public crate-pruefung-0.2.0 (c (n "pruefung") (v "0.2.0") (d (list (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1jic9rczs1fsnspk23rjg456gw7g9qkkhfs3jkms16af4v2ahlv4") (f (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

(define-public crate-pruefung-0.2.1 (c (n "pruefung") (v "0.2.1") (d (list (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (o #t) (d #t) (k 0)))) (h "116pplp5awz2b90s0c2c0vj232gan524svcj310smr6lb37iykbk") (f (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

