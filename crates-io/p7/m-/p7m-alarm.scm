(define-module (crates-io p7 m- p7m-alarm) #:use-module (crates-io))

(define-public crate-p7m-alarm-0.1.0 (c (n "p7m-alarm") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1wkjxbf2x69rwfi471z0vz4jd95x37vj7x1zjhsbfws89g78s469")))

