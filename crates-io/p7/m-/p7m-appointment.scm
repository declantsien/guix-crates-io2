(define-module (crates-io p7 m- p7m-appointment) #:use-module (crates-io))

(define-public crate-p7m-appointment-0.11.0 (c (n "p7m-appointment") (v "0.11.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1fxp8b295pan8diqrkkv5d0gza6q01cp8411za46ynagh1qqxg43")))

