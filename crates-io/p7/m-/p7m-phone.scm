(define-module (crates-io p7 m- p7m-phone) #:use-module (crates-io))

(define-public crate-p7m-phone-0.3.2 (c (n "p7m-phone") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1wjrjyw0a9jg6zi1b1q08divkh02xns9rlicw6sh5w9gl7v19br8")))

(define-public crate-p7m-phone-0.3.3 (c (n "p7m-phone") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "13gh2214a6ww7n1b2b5mfm57n4s3dsijph84nnx36n82z53zphwg")))

(define-public crate-p7m-phone-0.3.4 (c (n "p7m-phone") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1zfmxx1lpg03k1kndxd7j6zzdmjiaasgr5dw247k2cbalnh63vkv")))

(define-public crate-p7m-phone-0.3.5 (c (n "p7m-phone") (v "0.3.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1bal9jmcqxswzm9qd9nqwwjh6cxamb1m1n2m538yi028ky4km58d")))

