(define-module (crates-io p7 m- p7m-userauth) #:use-module (crates-io))

(define-public crate-p7m-userauth-0.11.0 (c (n "p7m-userauth") (v "0.11.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0wqsjqq1mzm3w86j09g2s4ybpd2wb4svxifbwginrkp0jmay21vb")))

