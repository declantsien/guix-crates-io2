(define-module (crates-io sw -s sw-sw-logger-rs) #:use-module (crates-io))

(define-public crate-sw-sw-logger-rs-1.0.0 (c (n "sw-sw-logger-rs") (v "1.0.0") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bv79mbxic0s8dxgz00wqb9il59zv2dzynm9wkschqzkv6afa2xl") (y #t)))

