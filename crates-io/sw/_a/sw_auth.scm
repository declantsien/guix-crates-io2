(define-module (crates-io sw _a sw_auth) #:use-module (crates-io))

(define-public crate-sw_auth-1.1.0 (c (n "sw_auth") (v "1.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0z2h99q5xxnyy0bnfkkk2mzmc9i94rb4b0fch3yi1wgv4w9zwcfz")))

(define-public crate-sw_auth-1.1.1 (c (n "sw_auth") (v "1.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1dr872h75kh4rmk0xkqnfnwdv19sf9k9akp7j5v8b2337djs9wsm")))

(define-public crate-sw_auth-1.1.2 (c (n "sw_auth") (v "1.1.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0f302vizqh35q43cy57pb86j4yfmsrkm06574b83pg19ik9apwqn")))

