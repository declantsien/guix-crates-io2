(define-module (crates-io sw is swiss_uid) #:use-module (crates-io))

(define-public crate-swiss_uid-1.0.0 (c (n "swiss_uid") (v "1.0.0") (d (list (d (n "itertools") (r "0.12.*") (d #t) (k 0)) (d (n "regex") (r "1.10.*") (d #t) (k 0)))) (h "0jfvx71pmns38ggygdslc3mzyjpghf17v63b8ajdliczypq657hi")))

