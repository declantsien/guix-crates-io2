(define-module (crates-io sw is swish-api) #:use-module (crates-io))

(define-public crate-swish-api-0.1.0 (c (n "swish-api") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "hyper") (r "^0.12.16") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)))) (h "0979cxlhn4bmirmwyin39yc96b4pvf31cq17icmbxcsbzzxqfsg2")))

