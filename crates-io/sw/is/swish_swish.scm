(define-module (crates-io sw is swish_swish) #:use-module (crates-io))

(define-public crate-swish_swish-0.1.0 (c (n "swish_swish") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1df83apgxxr86fcy969imjnsjkdq3ch3kfghxm0zrs7pjw4v54ai")))

(define-public crate-swish_swish-0.1.1 (c (n "swish_swish") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05jnw5nafjdbqkcpm96j40104s6f843rxv7h6rd01jrsdl45567m")))

(define-public crate-swish_swish-0.1.2 (c (n "swish_swish") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "142rza7lknbanpapb4a9v79qq5fbgksjjw0y7lp5m9dfkqan4lgk")))

(define-public crate-swish_swish-0.1.3 (c (n "swish_swish") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hyzdpfgnlrs2warnxbrv9ari25g96lx32ip0ffp1d21mp4mhjin")))

(define-public crate-swish_swish-0.1.5 (c (n "swish_swish") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k9qf97ba6qf5wiid6wi2jacxyj4viah773lr45d8kqy04xwn9yz")))

(define-public crate-swish_swish-0.1.6 (c (n "swish_swish") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aacxwr2drgyhcbjazm8mi3xqp2nwcyw5xv46vv10w52bk0hk5d8")))

(define-public crate-swish_swish-1.0.0 (c (n "swish_swish") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0win8jy4pxp6rmnq0s5gjcp9f27vldpdznmia4qc3rhmysvc79xw")))

(define-public crate-swish_swish-1.0.1 (c (n "swish_swish") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17pyaz7jax5lmp150pxah7pnh198iggg81gysrqfq2jkyscqa2a7")))

(define-public crate-swish_swish-1.0.2 (c (n "swish_swish") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "011sq6wv64z3dx7wpv5262i7qjdfmg5v5w4ffh76qz4x9qd5a2qs")))

(define-public crate-swish_swish-1.0.3 (c (n "swish_swish") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "057axxa264ssigqqlb2pqww5spq4mxc8f5kqx2v3x4kkc1gpl8yx")))

(define-public crate-swish_swish-1.0.4 (c (n "swish_swish") (v "1.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19gx0y6clacgrv2jpbfqp6lqrq1457nffhiqsxbmc5phyf6qvaqk")))

(define-public crate-swish_swish-1.0.5 (c (n "swish_swish") (v "1.0.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hppgzfrb9jpycpnskjygkir9bvbqwxvvq587rxq2i7cq8wk5n63")))

(define-public crate-swish_swish-1.0.6 (c (n "swish_swish") (v "1.0.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q5sqiqkz87x19bchjxqagf4ljv17wc0wc43khs6fkpl4jv80kq9")))

(define-public crate-swish_swish-1.0.7 (c (n "swish_swish") (v "1.0.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h1kicvjrfcf310bmj39z3xhvykcjzmyzyyfmbhww5643ybglnvd")))

