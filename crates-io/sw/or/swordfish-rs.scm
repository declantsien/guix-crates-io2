(define-module (crates-io sw or swordfish-rs) #:use-module (crates-io))

(define-public crate-swordfish-rs-0.1.0 (c (n "swordfish-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "126ilimxj4d58hlzrdd12zvxjahx74w79l1d9jzfbm9brh2dxzm5")))

(define-public crate-swordfish-rs-0.1.1 (c (n "swordfish-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1fr6wx10nfzy1p4fhds9m4xw9wch5mc7n8718ivs1hzwp2nh22zr")))

(define-public crate-swordfish-rs-0.2.0 (c (n "swordfish-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0p0322c10lsv34ix0jpl5jh340d8pyxpw1022h87m418qf85gqx8")))

(define-public crate-swordfish-rs-0.2.1 (c (n "swordfish-rs") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "05vzz21angymjd1wrghpbg4dfr19r73zldx9kw54423xsks6avbb")))

(define-public crate-swordfish-rs-0.2.2 (c (n "swordfish-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "12w4ilziac5r8ix7rlww4jcwbzi73a4ay88dir002fpgiwvldgdz")))

(define-public crate-swordfish-rs-0.2.3 (c (n "swordfish-rs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "0s7lbl3z89078jpvww4qg0cg18cr37gqbf8x6hjdfx9s2r1kp0mj")))

(define-public crate-swordfish-rs-0.3.0 (c (n "swordfish-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "0hrq7scmds9mi5drb89sypk6p12aagjrhrkvm3xp5hwkn6vmvdji")))

(define-public crate-swordfish-rs-0.3.1 (c (n "swordfish-rs") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "1j6kxxj1h2g7raxxwjjlsq00mhjgd9m147plbjy777jh23vwcgyn")))

(define-public crate-swordfish-rs-0.3.2 (c (n "swordfish-rs") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "01a0sm2ahi3zykhip70mkvwax14a7i1z677za6a2djqyirgblp2w")))

(define-public crate-swordfish-rs-0.4.0 (c (n "swordfish-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "1yafm0c1nvhgy5s0gipnzlwcckpr537bf9s4ggjk30mpz27sk7s1")))

(define-public crate-swordfish-rs-0.5.0 (c (n "swordfish-rs") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "1844s3m4ywcvggizm1l7ajhqazrf4dkm6z48xqn0387hlbbyj57a")))

(define-public crate-swordfish-rs-0.5.1 (c (n "swordfish-rs") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "1q5gdig5q5c7235q7sq0yf0pkhdgzyd1v62l47ji32j4imxgndkn")))

(define-public crate-swordfish-rs-0.5.2 (c (n "swordfish-rs") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "03ikdi9q15pcl1ircw43i109lavw343mz5dxv1jy6ag65a6grz47")))

(define-public crate-swordfish-rs-0.6.0 (c (n "swordfish-rs") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)))) (h "1xyb94hsrjiiisx5gdv971sfs1q72rsvq7010q9pd93mdcy7p8j1")))

