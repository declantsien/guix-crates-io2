(define-module (crates-io sw or swords) #:use-module (crates-io))

(define-public crate-swords-1.0.0 (c (n "swords") (v "1.0.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "1c49ijfi7pyq94gh60z002lwpnghia20zxqhgag7al2822ldbfgj")))

(define-public crate-swords-1.0.1 (c (n "swords") (v "1.0.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0hlszpawrqzj5qr81w3xvg2s7abz177a49xsrq9ddjd27pwldkl2")))

(define-public crate-swords-1.0.2 (c (n "swords") (v "1.0.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "16ji66121mz1rn067m9zpyh3bhljxmcsc8v2x8zs8d8g19iqqf9d")))

