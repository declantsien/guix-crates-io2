(define-module (crates-io sw #{4r}# sw4rm-rs) #:use-module (crates-io))

(define-public crate-sw4rm-rs-0.1.0 (c (n "sw4rm-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)))) (h "0cj0q3iffrpch7jgljvjd1l98fz9fia75rgina29pcqnd1yiqmmd")))

(define-public crate-sw4rm-rs-0.2.0 (c (n "sw4rm-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0yffmbd3jl1m7zaafvj95197dfz699yz9w2fvi4w5232i3wmgjwb")))

