(define-module (crates-io sw #{4r}# sw4rm-rs-generation) #:use-module (crates-io))

(define-public crate-sw4rm-rs-generation-0.2.0 (c (n "sw4rm-rs-generation") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-numerals") (r "^0.1") (d #t) (k 0)) (d (n "sw4rm-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0b56b3m7ilkp1lzsphv6imfh7id57y2m73p7z98g8h4vdxakxfh1")))

