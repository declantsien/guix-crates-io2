(define-module (crates-io sw s- sws-scraper) #:use-module (crates-io))

(define-public crate-sws-scraper-0.2.4 (c (n "sws-scraper") (v "0.2.4") (d (list (d (n "cssparser") (r "^0.31") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "selectors") (r "^0.25") (d #t) (k 0)) (d (n "sws-tree") (r "^1") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)))) (h "0cxmc5887xcngwnn3w02lbrnpbs47bpx29d9xrwczzz5qx2kc15w")))

