(define-module (crates-io sw im swim-contrib) #:use-module (crates-io))

(define-public crate-swim-contrib-0.2.0 (c (n "swim-contrib") (v "0.2.0") (h "0yfs22dnsy8rcmyy2snb6rcrrydii23ifnn165z7dv3q9miz39q5")))

(define-public crate-swim-contrib-0.2.1 (c (n "swim-contrib") (v "0.2.1") (h "1sq6amva6i0471cx02flf79kl9jgzizdr3107640xfhqkag782hk")))

(define-public crate-swim-contrib-0.3.0 (c (n "swim-contrib") (v "0.3.0") (h "0anw05kyc2g506niylm7j0dqvgsajw1kmmiaj87j9ff8jj5fh42p")))

