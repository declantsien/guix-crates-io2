(define-module (crates-io sw im swim-db) #:use-module (crates-io))

(define-public crate-swim-db-0.2.0 (c (n "swim-db") (v "0.2.0") (h "0qd81z4ls82fbr0mxf0k8glw385qxkpi8xx7mjlhhg0bzf35pigi")))

(define-public crate-swim-db-0.2.1 (c (n "swim-db") (v "0.2.1") (h "1p5q5dz47h0404kwmqb8vzpvmxj4rh1scgbcdnpfjw4mlajri2ns")))

(define-public crate-swim-db-0.3.0 (c (n "swim-db") (v "0.3.0") (h "1dp9aifnr19r02lqaj78b8flqyg67a514k4m0sadpgzdx61raxc3")))

