(define-module (crates-io sw im swimming) #:use-module (crates-io))

(define-public crate-swimming-0.1.0 (c (n "swimming") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "lock_pool") (r "^0.2.0") (d #t) (k 0)))) (h "057m4gzpjvdg7qx2rwy2mqs0ck0d9hslh1b0z9da6rc74nihyr57") (f (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1.1 (c (n "swimming") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "lock_pool") (r "^0.2.0") (d #t) (k 0)))) (h "0qj8xd2kvw29svahkmx9qniz2gmy1ch0z7bd13glfw45laymp70n") (f (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1.2 (c (n "swimming") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "lock_pool") (r "^0.2.0") (d #t) (k 0)))) (h "13j4bj989bbrg5brafn1a37jia7xvxcv24kzdm4k5r91biamywm6") (f (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1.3 (c (n "swimming") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "lock_pool") (r "^0.2.0") (d #t) (k 0)))) (h "0y7zpzxhccbl1r2i4axm2kmxhr6b7d0w76a5k7frz1spf66vi49n") (f (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

