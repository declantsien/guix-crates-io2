(define-module (crates-io sw im swim-util) #:use-module (crates-io))

(define-public crate-swim-util-0.2.0 (c (n "swim-util") (v "0.2.0") (h "00xffqz0fhanxm6dzcaqafk781i9b322pkl62v2sbcz44adqmrky")))

(define-public crate-swim-util-0.2.1 (c (n "swim-util") (v "0.2.1") (h "15zsmcr623m296q89mkdj2cxkajyiv2zi0c4v9nf8in8k3i87kag")))

(define-public crate-swim-util-0.3.0 (c (n "swim-util") (v "0.3.0") (h "1wxkjk0y90qjclv2mk13svl62zhjqgx1vdh2lkyy1hdkmz6w4490")))

