(define-module (crates-io sw im swimming-pool) #:use-module (crates-io))

(define-public crate-swimming-pool-0.1.0 (c (n "swimming-pool") (v "0.1.0") (h "0n0d7hw3p8ksyvm137vpq2w62kw6v01aflmjn0nhkwz3d201zxfc")))

(define-public crate-swimming-pool-0.1.1 (c (n "swimming-pool") (v "0.1.1") (h "1la9jms4c8288gyphsmpd55fp4s7gi0sqp198ybapwgm84f3js3h")))

(define-public crate-swimming-pool-0.1.2 (c (n "swimming-pool") (v "0.1.2") (h "0fvbg9my872nlw852ncm1vcda93w3pdcqnkrd5n2d26al5msdjpw")))

(define-public crate-swimming-pool-0.1.3 (c (n "swimming-pool") (v "0.1.3") (h "1b074lcxv8bh9lng6bppkh20g0ch36vy2367mqz9llcqqv837nlp")))

(define-public crate-swimming-pool-0.1.4 (c (n "swimming-pool") (v "0.1.4") (h "1n60ifjmqia8c6mj0i4g4pvydkyla2b1lnd7d4slgir21aw9qjlj")))

