(define-module (crates-io sw im swimlane) #:use-module (crates-io))

(define-public crate-swimlane-0.1.0 (c (n "swimlane") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "httpmock") (r "^0.6.8") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0vjzgyi3jvgbkqg0r752h54aj6228b394cygm0ybspmrrc9v54z3")))

