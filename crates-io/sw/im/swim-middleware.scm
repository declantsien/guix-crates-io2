(define-module (crates-io sw im swim-middleware) #:use-module (crates-io))

(define-public crate-swim-middleware-0.2.0 (c (n "swim-middleware") (v "0.2.0") (h "0xhxr5pb7f2q5mvysllkfszsn86li8lwfc5rzb2afq4swrl1zp80")))

(define-public crate-swim-middleware-0.2.1 (c (n "swim-middleware") (v "0.2.1") (h "08miix3x0nzpwqy7r76scw1i76p9735fm334s7hrmnsakxvckbni")))

(define-public crate-swim-middleware-0.3.0 (c (n "swim-middleware") (v "0.3.0") (h "1g54x7rv4hw4z30dhajhmjp5xhky1d799dc8bg8420rzrcr2fdgw")))

