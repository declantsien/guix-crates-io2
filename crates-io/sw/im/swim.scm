(define-module (crates-io sw im swim) #:use-module (crates-io))

(define-public crate-swim-0.1.0 (c (n "swim") (v "0.1.0") (h "0yh0wb1rgik0ybl056zgan3x7czvra5l8wkc6adi2pgx89ilaknb") (y #t)))

(define-public crate-swim-0.2.0 (c (n "swim") (v "0.2.0") (d (list (d (n "swim-core") (r "^0.2.0") (d #t) (k 0)) (d (n "swim-util") (r "^0.2.0") (d #t) (k 0)))) (h "1lkclzfsm7jz0r8vh05j3pdild34cjl44as7yisnnrzqba7imksr") (y #t)))

(define-public crate-swim-0.2.1 (c (n "swim") (v "0.2.1") (d (list (d (n "swim-core") (r "^0.2") (d #t) (k 0)) (d (n "swim-util") (r "^0.2") (d #t) (k 0)))) (h "147z7shpbb3r4flbgffyl7gpwip3wrg7sv5cpjkpb4b7x35wxykd") (y #t)))

(define-public crate-swim-0.2.2 (c (n "swim") (v "0.2.2") (d (list (d (n "swim-core") (r "^0.2.1") (d #t) (k 0)) (d (n "swim-util") (r "^0.2.1") (d #t) (k 0)))) (h "0shlz9png4lbh0y96330kz56cgf33xmx9xgdw93q41mx970vayys")))

(define-public crate-swim-0.3.0 (c (n "swim") (v "0.3.0") (d (list (d (n "swim-core") (r "^0.3.0") (d #t) (k 0)) (d (n "swim-util") (r "^0.3.0") (d #t) (k 0)))) (h "1lq8i6iia8cs8fnan9ixcfb00vz1892cgkq99757q82akfdy7dkc")))

