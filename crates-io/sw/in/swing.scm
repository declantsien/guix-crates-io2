(define-module (crates-io sw in swing) #:use-module (crates-io))

(define-public crate-swing-0.1.0 (c (n "swing") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0vxrszyyw1z3rz8wwa6g6ggixrkirjp718chagc9dm9y8nlhnrzx")))

