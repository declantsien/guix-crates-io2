(define-module (crates-io sw m3 swm341-pac) #:use-module (crates-io))

(define-public crate-swm341-pac-0.1.0 (c (n "swm341-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03vqfhvixyzj48hfzf867phnfyv4zfpd223rzqbiljxn1aliji36") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-swm341-pac-0.2.0 (c (n "swm341-pac") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11ca27cxfjamknd22g1a9wkp4r1bzbb03whzpfcy11rimxl836jf") (f (quote (("rt" "cortex-m-rt/device"))))))

