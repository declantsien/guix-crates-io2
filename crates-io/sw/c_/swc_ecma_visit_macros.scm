(define-module (crates-io sw c_ swc_ecma_visit_macros) #:use-module (crates-io))

(define-public crate-swc_ecma_visit_macros-0.1.0 (c (n "swc_ecma_visit_macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "09lxk9j3iaq19zff1dqkk6nn3lslwxr2l4x49zwv61d59cca7cyf")))

(define-public crate-swc_ecma_visit_macros-0.2.0 (c (n "swc_ecma_visit_macros") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0f7fkfj5y4r655bgs8a5v1km4c9lwrbwwaciv4qpd66bgc8sz0wh")))

(define-public crate-swc_ecma_visit_macros-0.3.0 (c (n "swc_ecma_visit_macros") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1gli9dysial329vz175bjx0byxzdilvqsfrzwh790xdrpcg0dyw3")))

(define-public crate-swc_ecma_visit_macros-0.4.0 (c (n "swc_ecma_visit_macros") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0jvc2fzfd1vim5gswgknc61zc06f8i1w4n2qv5rdf00q6ix8fchs")))

