(define-module (crates-io sw c_ swc_tailwind) #:use-module (crates-io))

(define-public crate-swc_tailwind-0.1.1 (c (n "swc_tailwind") (v "0.1.1") (h "1smxrhj1dckp9xj3vnb2637yvr7vykk50zgz6lz4i911ch2js2ax")))

(define-public crate-swc_tailwind-0.1.2 (c (n "swc_tailwind") (v "0.1.2") (h "0374kljl9znkgll7z2a6jy4d9n0360k8xmpgadzqpmvr73ms18pi")))

(define-public crate-swc_tailwind-0.1.3 (c (n "swc_tailwind") (v "0.1.3") (h "06g9rdqg27gmr0xh2j6hh33acxibkfpbkxx1188m5g6l112kn24w")))

(define-public crate-swc_tailwind-0.1.4 (c (n "swc_tailwind") (v "0.1.4") (h "0bkhpabskmlngsz5agfa1vmwh3idaz5inhds4n7hwdjhis4lvyxg")))

(define-public crate-swc_tailwind-0.1.5 (c (n "swc_tailwind") (v "0.1.5") (h "1qyla3ggawcniza48cpkr83y4yf7xljvr5zdfyzg965imhkf55k2")))

(define-public crate-swc_tailwind-0.1.6 (c (n "swc_tailwind") (v "0.1.6") (h "0f2mjs2zvrsi5l94dj2svzppsjq76hll8a7x5wzhzdma9ig8mbj9")))

(define-public crate-swc_tailwind-0.1.7 (c (n "swc_tailwind") (v "0.1.7") (h "099ix6r7imkkf5mavmrhj2q1ff0mz3c5ln9l7qz38wfly4vfx0xg")))

(define-public crate-swc_tailwind-0.1.8 (c (n "swc_tailwind") (v "0.1.8") (h "06qfjksr7qym43nyv2w5insvlzw91zh0h9axfhsdghfmvbcgd6kz")))

(define-public crate-swc_tailwind-0.1.9 (c (n "swc_tailwind") (v "0.1.9") (h "1fnj93k14gzzch49cdar7nk34zi9q9mxzws5xh6jr7fgk5ck9w73")))

