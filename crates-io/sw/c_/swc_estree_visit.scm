(define-module (crates-io sw c_ swc_estree_visit) #:use-module (crates-io))

(define-public crate-swc_estree_visit-0.1.0 (c (n "swc_estree_visit") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.2") (d #t) (k 0)) (d (n "swc_estree_ast") (r "^0.1") (d #t) (k 0)) (d (n "swc_estree_macros") (r "^0.1") (d #t) (k 0)) (d (n "swc_visit") (r "^0.2") (d #t) (k 0)))) (h "18rzqvv5324klbfqvar2nvpwk6y2mad57dzqznhiha66jhzlr357") (f (quote (("flavor-babel" "swc_estree_ast/flavor-babel") ("flavor-acorn" "swc_estree_ast/flavor-acorn"))))))

