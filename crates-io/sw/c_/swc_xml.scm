(define-module (crates-io sw c_ swc_xml) #:use-module (crates-io))

(define-public crate-swc_xml-0.1.0 (c (n "swc_xml") (v "0.1.0") (d (list (d (n "swc_xml_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.1.0") (d #t) (k 0)))) (h "0nvnyjq57rmzp6v3mgq4jws24qys72iwyh9ld6nn6bzhxai2f2f5")))

(define-public crate-swc_xml-0.2.0 (c (n "swc_xml") (v "0.2.0") (d (list (d (n "swc_xml_ast") (r "^0.2.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.0") (d #t) (k 0)))) (h "096g8if3zjzx2j8pvrsqjckjjm0ynbbmj6prflr7wx6c6cnjz0dx")))

(define-public crate-swc_xml-0.2.1 (c (n "swc_xml") (v "0.2.1") (d (list (d (n "swc_xml_ast") (r "^0.2.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.2") (d #t) (k 0)))) (h "1n1bfsi021wkwyfjjaz386f1rc16njzd6smifvq3gqn13lxyazdd")))

(define-public crate-swc_xml-0.2.2 (c (n "swc_xml") (v "0.2.2") (d (list (d (n "swc_xml_ast") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.3") (d #t) (k 0)))) (h "0y3vqfiddp3kpfv8jsy95hbhkwbmrsxr9wxm51j8ndv0xk5dp9pm")))

(define-public crate-swc_xml-0.2.3 (c (n "swc_xml") (v "0.2.3") (d (list (d (n "swc_xml_ast") (r "^0.2.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.4") (d #t) (k 0)))) (h "1di21hf626dxghxprkfxzl7y823cjfl9lxzdk5bp36536x5qfy64")))

(define-public crate-swc_xml-0.2.4 (c (n "swc_xml") (v "0.2.4") (d (list (d (n "swc_xml_ast") (r "^0.2.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.5") (d #t) (k 0)))) (h "1yhh66c61p829sk12mdh002wafm70k2vnsighkq269j59rsd1ppw")))

(define-public crate-swc_xml-0.2.5 (c (n "swc_xml") (v "0.2.5") (d (list (d (n "swc_xml_ast") (r "^0.2.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.6") (d #t) (k 0)))) (h "1qg4asyzwlhmkmazrs9fm0n3bcclicl6mdyya1fpwwqc49k65djv")))

(define-public crate-swc_xml-0.2.6 (c (n "swc_xml") (v "0.2.6") (d (list (d (n "swc_xml_ast") (r "^0.2.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.7") (d #t) (k 0)))) (h "0dq86apw36myvwqhplk4r015y892m584p47zh1hcxn04cscxa1ps")))

(define-public crate-swc_xml-0.2.7 (c (n "swc_xml") (v "0.2.7") (d (list (d (n "swc_xml_ast") (r "^0.2.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.2.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.2.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.2.8") (d #t) (k 0)))) (h "087w1c0s93g8dwj0i6vr23ka5x2mgg0f1y8xcix6fq7c2p481wdd")))

(define-public crate-swc_xml-0.3.0 (c (n "swc_xml") (v "0.3.0") (d (list (d (n "swc_xml_ast") (r "^0.3.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.0") (d #t) (k 0)))) (h "18g1m8zcpn6lhq7vxprbsycswcd2xcgv6dsbh4yxkdrqhbnqnsqi")))

(define-public crate-swc_xml-0.3.1 (c (n "swc_xml") (v "0.3.1") (d (list (d (n "swc_xml_ast") (r "^0.3.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.1") (d #t) (k 0)))) (h "1qqdlyr6sldrmnq545zxm3xq5lxyz3zg9kiz4fx288840p086yjd")))

(define-public crate-swc_xml-0.3.2 (c (n "swc_xml") (v "0.3.2") (d (list (d (n "swc_xml_ast") (r "^0.3.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.2") (d #t) (k 0)))) (h "1r5kqrl2a8kdmskshczx0r0z8ng8fbi80j8zdpa5hjn0gx9f97w4")))

(define-public crate-swc_xml-0.3.3 (c (n "swc_xml") (v "0.3.3") (d (list (d (n "swc_xml_ast") (r "^0.3.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.3") (d #t) (k 0)))) (h "1qpp375y5r0kik4pf25gxz6rlbb8w4c7n97rw16mmcdi9vs93a0d")))

(define-public crate-swc_xml-0.3.4 (c (n "swc_xml") (v "0.3.4") (d (list (d (n "swc_xml_ast") (r "^0.3.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.4") (d #t) (k 0)))) (h "0b6r38s3lhz958yxg18s3267pb7zfrvcm5p60b0440jy40diagji")))

(define-public crate-swc_xml-0.3.5 (c (n "swc_xml") (v "0.3.5") (d (list (d (n "swc_xml_ast") (r "^0.3.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.5") (d #t) (k 0)))) (h "18fllddfwg5apg3r02m74qrs2xi8sg3vxx7x88v0snhfld25ik9y")))

(define-public crate-swc_xml-0.3.6 (c (n "swc_xml") (v "0.3.6") (d (list (d (n "swc_xml_ast") (r "^0.3.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.6") (d #t) (k 0)))) (h "1mv97ip7y04mriwg2fzpav1cwjjk4bir0x2a2hxyrlrdlrb0yqws")))

(define-public crate-swc_xml-0.3.7 (c (n "swc_xml") (v "0.3.7") (d (list (d (n "swc_xml_ast") (r "^0.3.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.7") (d #t) (k 0)))) (h "0hymhb4g416b20rsabag1rw2qi0pkrwx3s2kb58k2lik5kyvq80k")))

(define-public crate-swc_xml-0.3.8 (c (n "swc_xml") (v "0.3.8") (d (list (d (n "swc_xml_ast") (r "^0.3.8") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.8") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.8") (d #t) (k 0)))) (h "1qf1401xbdfvwpmkwqis1qlgan3b7vamz90dk8b03z8x78q7p39d")))

(define-public crate-swc_xml-0.3.9 (c (n "swc_xml") (v "0.3.9") (d (list (d (n "swc_xml_ast") (r "^0.3.9") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.9") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.9") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.10") (d #t) (k 0)))) (h "0jy6817j8ga77nyclzh34hcrrcjqjm1wabqlhzn14kr3mahhfb93")))

(define-public crate-swc_xml-0.3.10 (c (n "swc_xml") (v "0.3.10") (d (list (d (n "swc_xml_ast") (r "^0.3.10") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.3.10") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.3.10") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.3.11") (d #t) (k 0)))) (h "0snnwcdv59b79k40mrg71x8w8zf1rsi5gsgdg75zbm1mj299sfqh")))

(define-public crate-swc_xml-0.4.0 (c (n "swc_xml") (v "0.4.0") (d (list (d (n "swc_xml_ast") (r "^0.4.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.0") (d #t) (k 0)))) (h "04wkkhqa8s2r2dh6ccg52c9a1x9jrgk7habijvbncfll9z5bm0dp")))

(define-public crate-swc_xml-0.4.1 (c (n "swc_xml") (v "0.4.1") (d (list (d (n "swc_xml_ast") (r "^0.4.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.1") (d #t) (k 0)))) (h "1vs8n1jbmangxim2wwhsi0cf03548ji4cmhzq8wzjk9qqs5zscjd")))

(define-public crate-swc_xml-0.4.2 (c (n "swc_xml") (v "0.4.2") (d (list (d (n "swc_xml_ast") (r "^0.4.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.2") (d #t) (k 0)))) (h "0m6bx2c1vcq10ykyj3xaq8hnx14bjbaqkpqlfbik1i9lvvb0n2a4")))

(define-public crate-swc_xml-0.4.3 (c (n "swc_xml") (v "0.4.3") (d (list (d (n "swc_xml_ast") (r "^0.4.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.3") (d #t) (k 0)))) (h "166nqz9909xjcnpn3y70krxnwb8w20xply1adk9fp2whkhinjniz")))

(define-public crate-swc_xml-0.4.4 (c (n "swc_xml") (v "0.4.4") (d (list (d (n "swc_xml_ast") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.4") (d #t) (k 0)))) (h "1ljyd1bhm2l09s2ms9q0qnps0a235ksgggjyqjvaswbxfz5a3bir")))

(define-public crate-swc_xml-0.4.5 (c (n "swc_xml") (v "0.4.5") (d (list (d (n "swc_xml_ast") (r "^0.4.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.5") (d #t) (k 0)))) (h "1fp7q5phqlzml5n84bny1y8mvcfnzvpw194x20g8p7y30fg3747d")))

(define-public crate-swc_xml-0.4.6 (c (n "swc_xml") (v "0.4.6") (d (list (d (n "swc_xml_ast") (r "^0.4.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.6") (d #t) (k 0)))) (h "0kj52cgcrhcaylcx7mr0q8pzliq16cvw48cs0pxjchx8ahi9k1zs")))

(define-public crate-swc_xml-0.4.7 (c (n "swc_xml") (v "0.4.7") (d (list (d (n "swc_xml_ast") (r "^0.4.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.6") (d #t) (k 0)))) (h "11d3md6q4p4dr2xalq6icpampnpls0qrjwr796l8jds54vk3q9ja")))

(define-public crate-swc_xml-0.4.8 (c (n "swc_xml") (v "0.4.8") (d (list (d (n "swc_xml_ast") (r "^0.4.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.8") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.7") (d #t) (k 0)))) (h "1vkl6mcnwljhrhl2x3f8a7adlvizhkfvhbvqc37nzygcr0xwxybr")))

(define-public crate-swc_xml-0.4.9 (c (n "swc_xml") (v "0.4.9") (d (list (d (n "swc_xml_ast") (r "^0.4.8") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.9") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.9") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.8") (d #t) (k 0)))) (h "101jqfhgj574cmdlfb303lg0m1mliwkw3yx75kmv975p7hkc814k")))

(define-public crate-swc_xml-0.4.10 (c (n "swc_xml") (v "0.4.10") (d (list (d (n "swc_xml_ast") (r "^0.4.9") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.9") (d #t) (k 0)))) (h "0k7cjksg38v2yvplcnscp0x24zdnhwwmwsfr59b7wdnhkcxi9xhl")))

(define-public crate-swc_xml-0.4.11 (c (n "swc_xml") (v "0.4.11") (d (list (d (n "swc_xml_ast") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.11") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.11") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.10") (d #t) (k 0)))) (h "0iil336im1lpiplx952lmz9b7ymkj3k11kivpxz5m8ig2q18jlgc")))

(define-public crate-swc_xml-0.4.12 (c (n "swc_xml") (v "0.4.12") (d (list (d (n "swc_xml_ast") (r "^0.4.11") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.12") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.12") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.11") (d #t) (k 0)))) (h "082dipwx9hyb6037vg02p1azgrl5r6hzhrcmal4dimvwbg49i9z8")))

(define-public crate-swc_xml-0.4.13 (c (n "swc_xml") (v "0.4.13") (d (list (d (n "swc_xml_ast") (r "^0.4.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.13") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.13") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.12") (d #t) (k 0)))) (h "1xxid2mim3rcpbq19nkyix22cca63gisvpa6g8qc9jr5a0fc9fdg")))

(define-public crate-swc_xml-0.4.14 (c (n "swc_xml") (v "0.4.14") (d (list (d (n "swc_xml_ast") (r "^0.4.13") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.13") (d #t) (k 0)))) (h "1naczp21wb15k8sgjw7v7cvz89j9lysjkpgifz87rlyqk3ji0l0f")))

(define-public crate-swc_xml-0.4.15 (c (n "swc_xml") (v "0.4.15") (d (list (d (n "swc_xml_ast") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.14") (d #t) (k 0)))) (h "0m5q9bkwn66lr0pzxapn1imwsc10dni6538ah38fz278sv2p6dk1")))

(define-public crate-swc_xml-0.4.16 (c (n "swc_xml") (v "0.4.16") (d (list (d (n "swc_xml_ast") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.4.16") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.4.16") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.15") (d #t) (k 0)))) (h "0zpqrcxlh876g131xs62xnhljbaq8a1b9hbmvc9bqyp5fqfp6p80")))

(define-public crate-swc_xml-0.5.0 (c (n "swc_xml") (v "0.5.0") (d (list (d (n "swc_xml_ast") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.5.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.15") (d #t) (k 0)))) (h "0jyq3pvlcyjvjpcc7n8mv47qncvibx7aprd4p68gib041s2q9cbr")))

(define-public crate-swc_xml-0.5.1 (c (n "swc_xml") (v "0.5.1") (d (list (d (n "swc_xml_ast") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.5.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.15") (d #t) (k 0)))) (h "0y6wrvsglznangm2fygzw336m1h77fglicszbmb5k2pmvy7j799g")))

(define-public crate-swc_xml-0.5.2 (c (n "swc_xml") (v "0.5.2") (d (list (d (n "swc_xml_ast") (r "^0.4.16") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.5.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.5.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.16") (d #t) (k 0)))) (h "1hlygma7qrlkqaiajpavy4z78w891cbl0f1yv52jqf85cqificgx")))

(define-public crate-swc_xml-0.5.3 (c (n "swc_xml") (v "0.5.3") (d (list (d (n "swc_xml_ast") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.17") (d #t) (k 0)))) (h "0xcf7aky0q3lbfb7q664pn593zpbz9zj6sxr6gp1pqw8n6cy0www")))

(define-public crate-swc_xml-0.5.4 (c (n "swc_xml") (v "0.5.4") (d (list (d (n "swc_xml_ast") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.5.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.5.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.4.17") (d #t) (k 0)))) (h "0nn6f67d3idpm2x5lfcj4kabgrx3117s78sy9hzi6xmphkbbhxsa")))

(define-public crate-swc_xml-0.6.0 (c (n "swc_xml") (v "0.6.0") (d (list (d (n "swc_xml_ast") (r "^0.5.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.5.0") (d #t) (k 0)))) (h "1scd8py5r1whdv4c3li6dvf0daw68lx79r0q1451rbfb5p04p0mk")))

(define-public crate-swc_xml-0.7.0 (c (n "swc_xml") (v "0.7.0") (d (list (d (n "swc_xml_ast") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.6.0") (d #t) (k 0)))) (h "16krg4536zbnln1ckhxa51l6fl9bqx1mlxrixc059ib49yizi8ly")))

(define-public crate-swc_xml-0.7.1 (c (n "swc_xml") (v "0.7.1") (d (list (d (n "swc_xml_ast") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.7.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.7.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.6.0") (d #t) (k 0)))) (h "1s600sj0n0wf15hz657pja7hx4x0kn51smprcyj4pgwvcvbwxbc0")))

(define-public crate-swc_xml-0.7.2 (c (n "swc_xml") (v "0.7.2") (d (list (d (n "swc_xml_ast") (r "^0.6.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.7.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.6.1") (d #t) (k 0)))) (h "0pv0ar1nc2jkvn1v8iqa81mdami18i0a1yz4nnxcrcf6x8wf1z9b")))

(define-public crate-swc_xml-0.7.3 (c (n "swc_xml") (v "0.7.3") (d (list (d (n "swc_xml_ast") (r "^0.6.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.7.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.7.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.6.2") (d #t) (k 0)))) (h "0iv6qbrkbgkl26y801gv69na1k77fx4b51yjpbybhwzss9s7fmks")))

(define-public crate-swc_xml-0.8.0 (c (n "swc_xml") (v "0.8.0") (d (list (d (n "swc_xml_ast") (r "^0.7.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.0") (d #t) (k 0)))) (h "0yr4kbz8s3wvpyvhyz7q5fkq85v8547i6bv8vpj7azrm0wjnv374")))

(define-public crate-swc_xml-0.8.1 (c (n "swc_xml") (v "0.8.1") (d (list (d (n "swc_xml_ast") (r "^0.7.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.1") (d #t) (k 0)))) (h "17y8z00nbr5yynhx5rm1798r584qcl8dcbrxhpiljb98v4k18fvw")))

(define-public crate-swc_xml-0.8.2 (c (n "swc_xml") (v "0.8.2") (d (list (d (n "swc_xml_ast") (r "^0.7.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.2") (d #t) (k 0)))) (h "1qs3bsj5agqm3b3d7h5lda02wshbxdr3d0bf50xmkgvc5svh7sgl")))

(define-public crate-swc_xml-0.8.3 (c (n "swc_xml") (v "0.8.3") (d (list (d (n "swc_xml_ast") (r "^0.7.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.3") (d #t) (k 0)))) (h "0sj78lss28b8658kk7xkv5dkqlhyb2lx87lyr654qc3p2mw7dgk3")))

(define-public crate-swc_xml-0.8.4 (c (n "swc_xml") (v "0.8.4") (d (list (d (n "swc_xml_ast") (r "^0.7.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.4") (d #t) (k 0)))) (h "1p189yvfrzdgwwrrql0g95pqixjzm35vwf2slrpa9igcpsarlmsm")))

(define-public crate-swc_xml-0.8.5 (c (n "swc_xml") (v "0.8.5") (d (list (d (n "swc_xml_ast") (r "^0.7.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.5") (d #t) (k 0)))) (h "15siwyax5nciws9nxvs37pl653p4k4lmcmxr3a761xxcxpz7ymgl")))

(define-public crate-swc_xml-0.8.6 (c (n "swc_xml") (v "0.8.6") (d (list (d (n "swc_xml_ast") (r "^0.7.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.5") (d #t) (k 0)))) (h "1xc9pv1n9fdxhd2mafsjmpmbc5n5gpf0lxs0ayvl0w1gja3zgpll")))

(define-public crate-swc_xml-0.8.7 (c (n "swc_xml") (v "0.8.7") (d (list (d (n "swc_xml_ast") (r "^0.7.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.6") (d #t) (k 0)))) (h "0dw4yv8mx65z8lfnzn3wxisnjbbbd69arvzg5bbncrmr9b9a1p7q")))

(define-public crate-swc_xml-0.8.8 (c (n "swc_xml") (v "0.8.8") (d (list (d (n "swc_xml_ast") (r "^0.7.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.8") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.7") (d #t) (k 0)))) (h "1jz8kg6lipdycm2y3f9wji2k7zr7sg2pcmaimk4idkjaylgpmx8s")))

(define-public crate-swc_xml-0.8.9 (c (n "swc_xml") (v "0.8.9") (d (list (d (n "swc_xml_ast") (r "^0.7.8") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.9") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.9") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.8") (d #t) (k 0)))) (h "00jq4y1rgy8y8p4d831jaxa8n0if0kdzxc5c9qv12lblcvabdpqh")))

(define-public crate-swc_xml-0.8.10 (c (n "swc_xml") (v "0.8.10") (d (list (d (n "swc_xml_ast") (r "^0.7.9") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.10") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.10") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.9") (d #t) (k 0)))) (h "18fbczsxmm0axn6rppb38v62vma4ai8fs5iknkz9pfzc8csl0s2a")))

(define-public crate-swc_xml-0.8.11 (c (n "swc_xml") (v "0.8.11") (d (list (d (n "swc_xml_ast") (r "^0.7.10") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.11") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.11") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.10") (d #t) (k 0)))) (h "1p8gra2089i0wspzjbdmdi9ay1n8gqh8clx60bn0acbasm61y4cj")))

(define-public crate-swc_xml-0.8.12 (c (n "swc_xml") (v "0.8.12") (d (list (d (n "swc_xml_ast") (r "^0.7.11") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.12") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.12") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.11") (d #t) (k 0)))) (h "0admfwmi1kwldrk846112xqpg7dyi9xkrkcgs8752jd2i3ms4k9c")))

(define-public crate-swc_xml-0.8.13 (c (n "swc_xml") (v "0.8.13") (d (list (d (n "swc_xml_ast") (r "^0.7.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.13") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.13") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.12") (d #t) (k 0)))) (h "1q8k1lsx9zl75z3s1k1yss8ylvcp9vbvy3fr8i5s6hz6yxlmlqzq")))

(define-public crate-swc_xml-0.8.14 (c (n "swc_xml") (v "0.8.14") (d (list (d (n "swc_xml_ast") (r "^0.7.13") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.14") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.14") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.13") (d #t) (k 0)))) (h "0qfpx2r807bvxfpx9pd3lg7nckxqkz08pxi97x5avp5znkkjrgbq")))

(define-public crate-swc_xml-0.8.15 (c (n "swc_xml") (v "0.8.15") (d (list (d (n "swc_xml_ast") (r "^0.7.14") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.15") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.15") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.14") (d #t) (k 0)))) (h "1nq1dqc51apaphrw5hpjj8myxp1z4cij74fwm888ixwnmxw32iwy")))

(define-public crate-swc_xml-0.8.16 (c (n "swc_xml") (v "0.8.16") (d (list (d (n "swc_xml_ast") (r "^0.7.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.16") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.16") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.15") (d #t) (k 0)))) (h "0kcy1sanm63cqraski8xc6y9yyagxq95nxhhq4dcvy2qzc9d6wqd")))

(define-public crate-swc_xml-0.8.17 (c (n "swc_xml") (v "0.8.17") (d (list (d (n "swc_xml_ast") (r "^0.7.16") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.17") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.17") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.16") (d #t) (k 0)))) (h "0yjiy4fvm58vsn1z5r0f6dgs3lskfmnzaf3zhlf44dc8an5xixvb")))

(define-public crate-swc_xml-0.8.18 (c (n "swc_xml") (v "0.8.18") (d (list (d (n "swc_xml_ast") (r "^0.7.17") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.18") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.18") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.17") (d #t) (k 0)))) (h "0pwqqj21gsrz8ds3ksna68cr2bxcfs40h92d34lwql783j0ad79s")))

(define-public crate-swc_xml-0.8.19 (c (n "swc_xml") (v "0.8.19") (d (list (d (n "swc_xml_ast") (r "^0.7.18") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.19") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.19") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.18") (d #t) (k 0)))) (h "01q4nrd9fgw5z7akbirqvmf7vz1piad380nx9ffslrhi5d6qbqh4")))

(define-public crate-swc_xml-0.8.20 (c (n "swc_xml") (v "0.8.20") (d (list (d (n "swc_xml_ast") (r "^0.7.19") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.20") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.20") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.19") (d #t) (k 0)))) (h "18mjf8r87ww7qfhzlpwdk5npg0mki4qp5fzf9l00pgmzjy0z90pz")))

(define-public crate-swc_xml-0.8.21 (c (n "swc_xml") (v "0.8.21") (d (list (d (n "swc_xml_ast") (r "^0.7.20") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.21") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.21") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.20") (d #t) (k 0)))) (h "10hxairqw5ihjhvglzm0i9l3x4ba9hbs0r43izykvcmch14zdrmp")))

(define-public crate-swc_xml-0.8.22 (c (n "swc_xml") (v "0.8.22") (d (list (d (n "swc_xml_ast") (r "^0.7.20") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.22") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.22") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.20") (d #t) (k 0)))) (h "1sqh5cipnghs8hjvkm9qqv65kgsff0r56wga30q7yps4ifaicvdl")))

(define-public crate-swc_xml-0.8.23 (c (n "swc_xml") (v "0.8.23") (d (list (d (n "swc_xml_ast") (r "^0.7.21") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.23") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.23") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.21") (d #t) (k 0)))) (h "18wg8hvx0as7bhzjrk4rgcf5fjbn6v6q484wmmmw4a17pswigj1i")))

(define-public crate-swc_xml-0.8.24 (c (n "swc_xml") (v "0.8.24") (d (list (d (n "swc_xml_ast") (r "^0.7.22") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.8.24") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.8.24") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.7.22") (d #t) (k 0)))) (h "03w8i75jj1k62n8q7bav09cc6dmbdhb0r59mm56im75yjs1sqsq5")))

(define-public crate-swc_xml-0.9.0 (c (n "swc_xml") (v "0.9.0") (d (list (d (n "swc_xml_ast") (r "^0.8.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.9.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.8.0") (d #t) (k 0)))) (h "1s6x5mkwfw0dmf4ic29bkfqpsp6hd380w3vgwbqczc9bgh9g1p19")))

(define-public crate-swc_xml-0.10.0 (c (n "swc_xml") (v "0.10.0") (d (list (d (n "swc_xml_ast") (r "^0.9.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.0") (d #t) (k 0)))) (h "0mffb3yhi71fcz7na77sd32zjk8qzmh508zhq5bjnq0sqcj3av48")))

(define-public crate-swc_xml-0.10.1 (c (n "swc_xml") (v "0.10.1") (d (list (d (n "swc_xml_ast") (r "^0.9.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.1") (d #t) (k 0)))) (h "00klaz8dw0dk563lxskyxsdf1q6b6ggqadsfgn8haxwnppaa1yq1")))

(define-public crate-swc_xml-0.10.2 (c (n "swc_xml") (v "0.10.2") (d (list (d (n "swc_xml_ast") (r "^0.9.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.2") (d #t) (k 0)))) (h "1bn1v1lkd0ynxw80v9hphkzk1k4ykhsy2iazfsyqj0sg5ilixraa")))

(define-public crate-swc_xml-0.10.3 (c (n "swc_xml") (v "0.10.3") (d (list (d (n "swc_xml_ast") (r "^0.9.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.3") (d #t) (k 0)))) (h "0083ybz1gxk704dr7khlnajxdcwgl1sib92csl1inbrbl8km6lmd")))

(define-public crate-swc_xml-0.10.4 (c (n "swc_xml") (v "0.10.4") (d (list (d (n "swc_xml_ast") (r "^0.9.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.4") (d #t) (k 0)))) (h "1z8zzqbadhajy24wp5cnwgf26b439pcs226sq1rvhcbam45ik1aj")))

(define-public crate-swc_xml-0.10.5 (c (n "swc_xml") (v "0.10.5") (d (list (d (n "swc_xml_ast") (r "^0.9.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.10.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.10.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.9.5") (d #t) (k 0)))) (h "1y803ypx4qa2csvbys5nn4kzyg4df5n5y5jgfbxa8byw6irm26sv")))

(define-public crate-swc_xml-0.11.0 (c (n "swc_xml") (v "0.11.0") (d (list (d (n "swc_xml_ast") (r "^0.10.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.0") (d #t) (k 0)))) (h "0br34kfraddsrgks8i5bcsw0jgg39d748hcpl1vlka12d9wf0agj")))

(define-public crate-swc_xml-0.11.1 (c (n "swc_xml") (v "0.11.1") (d (list (d (n "swc_xml_ast") (r "^0.10.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.1") (d #t) (k 0)))) (h "1s48l9bxl6w6xk79bxacil1k3zjfyxsi73pp1h1k39anksmsl0pd")))

(define-public crate-swc_xml-0.11.2 (c (n "swc_xml") (v "0.11.2") (d (list (d (n "swc_xml_ast") (r "^0.10.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.2") (d #t) (k 0)))) (h "0rbr96lvsij6vk2yf5y8w569pjb1nsyc8zliay3byc4hj2y60vmb")))

(define-public crate-swc_xml-0.11.3 (c (n "swc_xml") (v "0.11.3") (d (list (d (n "swc_xml_ast") (r "^0.10.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.3") (d #t) (k 0)))) (h "10hpzvbvy4k2v0a0gwnxq7n1w4i4vaw1yj6a2f4szl0nm891kcvy")))

(define-public crate-swc_xml-0.11.4 (c (n "swc_xml") (v "0.11.4") (d (list (d (n "swc_xml_ast") (r "^0.10.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.4") (d #t) (k 0)))) (h "0l1bmrqyghdkwkdx13672abk0ybc17pnlzy1n82k62d8r9475zpc")))

(define-public crate-swc_xml-0.11.5 (c (n "swc_xml") (v "0.11.5") (d (list (d (n "swc_xml_ast") (r "^0.10.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.4") (d #t) (k 0)))) (h "1r5s8bl5gdia43ivxn30sfq35ygy52s5fz44qs42jxrrfq8m9dxd")))

(define-public crate-swc_xml-0.11.6 (c (n "swc_xml") (v "0.11.6") (d (list (d (n "swc_xml_ast") (r "^0.10.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.5") (d #t) (k 0)))) (h "04yisgc0k2jvp3pq39n1a6rkxy0m0dizp6hkd6dhr1ckdjy0x1nx")))

(define-public crate-swc_xml-0.11.7 (c (n "swc_xml") (v "0.11.7") (d (list (d (n "swc_xml_ast") (r "^0.10.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.5") (d #t) (k 0)))) (h "09ks10vrz5rbdy5w2r49pv0yn4yagnasq78mwa5v86kbba0wajg5")))

(define-public crate-swc_xml-0.11.8 (c (n "swc_xml") (v "0.11.8") (d (list (d (n "swc_xml_ast") (r "^0.10.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.6") (d #t) (k 0)))) (h "0q7nhyry4y1hn4adcsgch6apac0lflkj1l9an0rmazvy1b852xnp")))

(define-public crate-swc_xml-0.11.9 (c (n "swc_xml") (v "0.11.9") (d (list (d (n "swc_xml_ast") (r "^0.10.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.9") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.8") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.7") (d #t) (k 0)))) (h "0wzrx5xh99n8wsq2p76bq1a4vhwp6k7whfqlzvck5cbnfazzji41")))

(define-public crate-swc_xml-0.11.10 (c (n "swc_xml") (v "0.11.10") (d (list (d (n "swc_xml_ast") (r "^0.10.8") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.10") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.9") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.8") (d #t) (k 0)))) (h "0p6kj6zl96psw90h01w45ywd8i18dbaifi572wr77jb7qwzwr9xa")))

(define-public crate-swc_xml-0.11.11 (c (n "swc_xml") (v "0.11.11") (d (list (d (n "swc_xml_ast") (r "^0.10.9") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.11") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.10") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.9") (d #t) (k 0)))) (h "0r4wqi14lyjz78gxg2472kl3m65lvnvssmc582gbwfx5zh421ldi")))

(define-public crate-swc_xml-0.11.12 (c (n "swc_xml") (v "0.11.12") (d (list (d (n "swc_xml_ast") (r "^0.10.10") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.12") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.11") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.10") (d #t) (k 0)))) (h "19qs24f75pqvh5bv5h241wra7shfnj12a9ikbf4fgram8cpdig3l")))

(define-public crate-swc_xml-0.11.13 (c (n "swc_xml") (v "0.11.13") (d (list (d (n "swc_xml_ast") (r "^0.10.11") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.13") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.12") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.11") (d #t) (k 0)))) (h "1rd01i1gmss4nns9xyxxydh3zcq6j2dmwfdbiawwi4zr994l5jqv")))

(define-public crate-swc_xml-0.11.14 (c (n "swc_xml") (v "0.11.14") (d (list (d (n "swc_xml_ast") (r "^0.10.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.14") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.13") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.12") (d #t) (k 0)))) (h "0ykpfrjjwrif6k6vn23szbjz7yys61k5h96wzj91jc2j018d79gx")))

(define-public crate-swc_xml-0.11.15 (c (n "swc_xml") (v "0.11.15") (d (list (d (n "swc_xml_ast") (r "^0.10.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.15") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.14") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.12") (d #t) (k 0)))) (h "0bh0p00xban55rphcas326fcnlw882drj7rxkvrs1qq2prra6w09")))

(define-public crate-swc_xml-0.11.16 (c (n "swc_xml") (v "0.11.16") (d (list (d (n "swc_xml_ast") (r "^0.10.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.16") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.15") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.12") (d #t) (k 0)))) (h "161gp8qyih6gpjg3yqwk09gfrmwkvjb36ldqz2fi3kd37r3kvlls")))

(define-public crate-swc_xml-0.11.17 (c (n "swc_xml") (v "0.11.17") (d (list (d (n "swc_xml_ast") (r "^0.10.13") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.17") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.16") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.13") (d #t) (k 0)))) (h "0j4x7a80ywmmq68mza3rji87lwx8v0pn7msfvc9aryv40q1jcxp2")))

(define-public crate-swc_xml-0.11.18 (c (n "swc_xml") (v "0.11.18") (d (list (d (n "swc_xml_ast") (r "^0.10.14") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.18") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.17") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.14") (d #t) (k 0)))) (h "1r655dlbv8qp9qkisgafpysd89v0gr9r40pc7q532phfscddrwa6")))

(define-public crate-swc_xml-0.11.19 (c (n "swc_xml") (v "0.11.19") (d (list (d (n "swc_xml_ast") (r "^0.10.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.19") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.18") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.15") (d #t) (k 0)))) (h "0mx6lccm3z2ay2qq083hvpq21vbza404xa7xsgyfxxz5zgd0xfgi")))

(define-public crate-swc_xml-0.11.20 (c (n "swc_xml") (v "0.11.20") (d (list (d (n "swc_xml_ast") (r "^0.10.16") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.20") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.19") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.16") (d #t) (k 0)))) (h "02bjb8q7r1zvamp8jkl9zp5bfvfk5bgrjcrpaparzn6ga4sfq2pq")))

(define-public crate-swc_xml-0.11.21 (c (n "swc_xml") (v "0.11.21") (d (list (d (n "swc_xml_ast") (r "^0.10.16") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.21") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.19") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.16") (d #t) (k 0)))) (h "06rynz6rw54zflg5gnqhd1p65ckxbngddccy1fsgiizvfkzha7rp")))

(define-public crate-swc_xml-0.11.22 (c (n "swc_xml") (v "0.11.22") (d (list (d (n "swc_xml_ast") (r "^0.10.17") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.22") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.20") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.17") (d #t) (k 0)))) (h "1iiaz38pwwqjrnaqwqw6y6sccf489q79kliskx5cbzr66phb5a44")))

(define-public crate-swc_xml-0.11.23 (c (n "swc_xml") (v "0.11.23") (d (list (d (n "swc_xml_ast") (r "^0.10.18") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.23") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.21") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.18") (d #t) (k 0)))) (h "1vljj26a0a0dhh6wa0cx9hlck0hk990p0h17wc01q0zajrlnp0aj")))

(define-public crate-swc_xml-0.11.24 (c (n "swc_xml") (v "0.11.24") (d (list (d (n "swc_xml_ast") (r "^0.10.19") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.24") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.22") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.19") (d #t) (k 0)))) (h "0zxlmnwqxs4fjq7jv4vsd6vcq19givfn59a6jf5hyfsxqxfd3hvn")))

(define-public crate-swc_xml-0.11.25 (c (n "swc_xml") (v "0.11.25") (d (list (d (n "swc_xml_ast") (r "^0.10.20") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.25") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.23") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.20") (d #t) (k 0)))) (h "1a9bdzhq7bqb4ap7scc9qgg5yfw3j4a0lr416rp04s11cfq126mw")))

(define-public crate-swc_xml-0.11.26 (c (n "swc_xml") (v "0.11.26") (d (list (d (n "swc_xml_ast") (r "^0.10.21") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.26") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.24") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.21") (d #t) (k 0)))) (h "0qwcqy6q301r3jc6ckckd7g39f4k6qrfzah0xkahrpmaxhyf1xvg")))

(define-public crate-swc_xml-0.11.27 (c (n "swc_xml") (v "0.11.27") (d (list (d (n "swc_xml_ast") (r "^0.10.22") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.11.27") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.11.25") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.10.22") (d #t) (k 0)))) (h "1vlxsqpjjk8srcfasbilhg0c2cb6155xjxmym30z44c6jjqq6p0p")))

(define-public crate-swc_xml-0.12.0 (c (n "swc_xml") (v "0.12.0") (d (list (d (n "swc_xml_ast") (r "^0.11.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.12.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.12.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.11.0") (d #t) (k 0)))) (h "1dlhn0vkbhy02zjjxxq92xnb1whyqif341ll05qvzbxld7lvbxcr")))

(define-public crate-swc_xml-0.12.1 (c (n "swc_xml") (v "0.12.1") (d (list (d (n "swc_xml_ast") (r "^0.11.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.12.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.12.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.11.1") (d #t) (k 0)))) (h "1q3b1csfhvvpzjx534kxdp0ddy743shhyhal5b4kncccpv7f4y84")))

(define-public crate-swc_xml-0.12.2 (c (n "swc_xml") (v "0.12.2") (d (list (d (n "swc_xml_ast") (r "^0.11.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.12.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.12.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.11.2") (d #t) (k 0)))) (h "1v2wagfqzc7i2ny9dpl892d276yslzb9a2dw2dlvnk6f8d1dgpm3")))

(define-public crate-swc_xml-0.13.0 (c (n "swc_xml") (v "0.13.0") (d (list (d (n "swc_xml_ast") (r "^0.12.0") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.0") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.0") (d #t) (k 0)))) (h "1i899vxx6lhsr533lzpgp448w09j3j7vinwm38dnm3rs65w150l5")))

(define-public crate-swc_xml-0.13.1 (c (n "swc_xml") (v "0.13.1") (d (list (d (n "swc_xml_ast") (r "^0.12.1") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.1") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.1") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.1") (d #t) (k 0)))) (h "073afr784z0jd1894227ryrxgnx4p4v16r8dx1psxdki6q74ya30")))

(define-public crate-swc_xml-0.13.2 (c (n "swc_xml") (v "0.13.2") (d (list (d (n "swc_xml_ast") (r "^0.12.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.2") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.2") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.2") (d #t) (k 0)))) (h "1r1dfdbi3jbknfx0cqx1j11vsf1d9fb5snmfs01aia9dpvvbcrjh")))

(define-public crate-swc_xml-0.13.3 (c (n "swc_xml") (v "0.13.3") (d (list (d (n "swc_xml_ast") (r "^0.12.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.3") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.3") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.2") (d #t) (k 0)))) (h "0s9xcsap75fbk3gz48j6qzplmv24axlf6cg9z0qlz2ccs3vciwhm")))

(define-public crate-swc_xml-0.13.4 (c (n "swc_xml") (v "0.13.4") (d (list (d (n "swc_xml_ast") (r "^0.12.2") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.4") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.4") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.2") (d #t) (k 0)))) (h "1fppw3iv3237bqnnav67d923f45z6c24kwsasm6gqiic9dg0f088")))

(define-public crate-swc_xml-0.13.5 (c (n "swc_xml") (v "0.13.5") (d (list (d (n "swc_xml_ast") (r "^0.12.3") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.5") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.5") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.3") (d #t) (k 0)))) (h "1f1746mspbja552x6sqhjlnaymhsymn828anrnypjcwd2rlrhnz1")))

(define-public crate-swc_xml-0.13.6 (c (n "swc_xml") (v "0.13.6") (d (list (d (n "swc_xml_ast") (r "^0.12.4") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.6") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.6") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.4") (d #t) (k 0)))) (h "1g5lgp8j90fy2ihisgwy7akhma1fas58nv5fard5d9vgqv39xn90")))

(define-public crate-swc_xml-0.13.7 (c (n "swc_xml") (v "0.13.7") (d (list (d (n "swc_xml_ast") (r "^0.12.5") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.7") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.7") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.5") (d #t) (k 0)))) (h "0c2rqjhql5l8yxlqd31d6lkqfvcph7vn9i99i2fsppjxk7ljk9dk")))

(define-public crate-swc_xml-0.13.8 (c (n "swc_xml") (v "0.13.8") (d (list (d (n "swc_xml_ast") (r "^0.12.6") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.8") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.8") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.6") (d #t) (k 0)))) (h "18xfs1l5j5f1hq13amvdpr8pqx0s3j23ygcsra42vv72zvfpbf6g")))

(define-public crate-swc_xml-0.13.9 (c (n "swc_xml") (v "0.13.9") (d (list (d (n "swc_xml_ast") (r "^0.12.7") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.9") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.9") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.7") (d #t) (k 0)))) (h "0xk1pf1ba6283hb2yhsicxclxsixdhsrv0rxyi3z0zdizwkbgk5w")))

(define-public crate-swc_xml-0.13.10 (c (n "swc_xml") (v "0.13.10") (d (list (d (n "swc_xml_ast") (r "^0.12.8") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.10") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.10") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.8") (d #t) (k 0)))) (h "1fw4kvcqajigrhrn32zxka3brmhcmn1wp48jvnfkx5yvwvn2s9km")))

(define-public crate-swc_xml-0.13.11 (c (n "swc_xml") (v "0.13.11") (d (list (d (n "swc_xml_ast") (r "^0.12.9") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.11") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.11") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.9") (d #t) (k 0)))) (h "1j740mz08zawqwjwp0gg3fmrg9sc9wfrxpyy3b5rv314r64flvix")))

(define-public crate-swc_xml-0.13.12 (c (n "swc_xml") (v "0.13.12") (d (list (d (n "swc_xml_ast") (r "^0.12.10") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.12") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.12") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.10") (d #t) (k 0)))) (h "0y0z8i09kn9c8ivydc1q3hhh2y079scqq3g1ypbqjswkx24f4sm5")))

(define-public crate-swc_xml-0.13.13 (c (n "swc_xml") (v "0.13.13") (d (list (d (n "swc_xml_ast") (r "^0.12.11") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.13") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.13") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.11") (d #t) (k 0)))) (h "1rismfqmmav2rnpdbpan5qc6fpbsgvypnbvrgzhyk7dbldj33s0f")))

(define-public crate-swc_xml-0.13.14 (c (n "swc_xml") (v "0.13.14") (d (list (d (n "swc_xml_ast") (r "^0.12.12") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.14") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.14") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.12") (d #t) (k 0)))) (h "1bh4hnhiyc5gx4sydq945k4dwkb2c0lhi2da9n2jb1s2hycpy1rk")))

(define-public crate-swc_xml-0.13.15 (c (n "swc_xml") (v "0.13.15") (d (list (d (n "swc_xml_ast") (r "^0.12.13") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.15") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.15") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.13") (d #t) (k 0)))) (h "19m93la6xj7gk4iix4qyr8nws980774j6d645l3igf2v370c9ara")))

(define-public crate-swc_xml-0.13.16 (c (n "swc_xml") (v "0.13.16") (d (list (d (n "swc_xml_ast") (r "^0.12.14") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.16") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.16") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.14") (d #t) (k 0)))) (h "18sgq7ib49khqlk7xg89sijmr4dsa92w6ppjsljhb77bxcsnhab0")))

(define-public crate-swc_xml-0.13.17 (c (n "swc_xml") (v "0.13.17") (d (list (d (n "swc_xml_ast") (r "^0.12.15") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.17") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.17") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.15") (d #t) (k 0)))) (h "04gydxqhmg487ran7c19271iyjbzmj6k87vq8g6qn2fmbwj2i5ih")))

(define-public crate-swc_xml-0.13.18 (c (n "swc_xml") (v "0.13.18") (d (list (d (n "swc_xml_ast") (r "^0.12.16") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.18") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.18") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.16") (d #t) (k 0)))) (h "0ph30vp979izg1kjvf853pmcxipy2rndd7cmkwmfizv1wwb2hm5r")))

(define-public crate-swc_xml-0.13.19 (c (n "swc_xml") (v "0.13.19") (d (list (d (n "swc_xml_ast") (r "^0.12.17") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.19") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.19") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.17") (d #t) (k 0)))) (h "18zdgzidi05bg5ckkwfx1kf1xk9njyyxzrw7d9qfb2vzw5qy5xxr")))

(define-public crate-swc_xml-0.13.20 (c (n "swc_xml") (v "0.13.20") (d (list (d (n "swc_xml_ast") (r "^0.12.18") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.20") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.20") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.18") (d #t) (k 0)))) (h "0447szz8dk9p30d5fk97473z850nanhj3ajswjcfg574pnbnc0d5")))

(define-public crate-swc_xml-0.13.21 (c (n "swc_xml") (v "0.13.21") (d (list (d (n "swc_xml_ast") (r "^0.12.19") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.21") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.21") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.19") (d #t) (k 0)))) (h "06m52q3sdi5p18dgs3acv7843lrffdviwj7l49h5g2fbcv3w9dqh")))

(define-public crate-swc_xml-0.13.22 (c (n "swc_xml") (v "0.13.22") (d (list (d (n "swc_xml_ast") (r "^0.12.21") (d #t) (k 0)) (d (n "swc_xml_codegen") (r "^0.13.24") (d #t) (k 0)) (d (n "swc_xml_parser") (r "^0.13.24") (d #t) (k 0)) (d (n "swc_xml_visit") (r "^0.12.20") (d #t) (k 0)))) (h "15rj9mhfd3j912fslj2fnc4y6jaf818gvlg306hb0q1swby3j094")))

