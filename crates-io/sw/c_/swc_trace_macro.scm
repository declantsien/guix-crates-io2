(define-module (crates-io sw c_ swc_trace_macro) #:use-module (crates-io))

(define-public crate-swc_trace_macro-0.1.0 (c (n "swc_trace_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d43xsgna72fav0rr5iiabz8dy1h8cfqnfsmf8r8gga6djwiyd45")))

(define-public crate-swc_trace_macro-0.1.1 (c (n "swc_trace_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02y17qmxgrsxa63ssdhlxmnad1j86xf4xzrfrf3xchh4nkyha6ld")))

(define-public crate-swc_trace_macro-0.1.2 (c (n "swc_trace_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvjgmdij3s973l6i5vizhpywa94wl5a5h6az7p65pp04f6mqyd4")))

(define-public crate-swc_trace_macro-0.1.3 (c (n "swc_trace_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fjjw1jrynl29k32fbayz444pqbvjf0shf0rsr7q5ljx12v1k5zz")))

