(define-module (crates-io sw c_ swc_plugin_comments) #:use-module (crates-io))

(define-public crate-swc_plugin_comments-0.1.0 (c (n "swc_plugin_comments") (v "0.1.0") (d (list (d (n "better_scoped_tls") (r "^0.1.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.36") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.0") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "0h2wh0kqzbgh85m46bnx0i3z09sjwipamrfwdpsbzgivrdgrb1cw") (f (quote (("plugin-rt") ("plugin-mode"))))))

(define-public crate-swc_plugin_comments-0.1.1 (c (n "swc_plugin_comments") (v "0.1.1") (d (list (d (n "better_scoped_tls") (r "^0.1.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.36") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.19") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "0i28jkjzrlnf7wcppczds6xxn8ivmgah17654rprs8g2pqz9wjqy") (f (quote (("plugin-rt") ("plugin-mode"))))))

(define-public crate-swc_plugin_comments-0.1.2 (c (n "swc_plugin_comments") (v "0.1.2") (d (list (d (n "better_scoped_tls") (r "^0.1.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.36") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.20") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "0lv1xldq0sbpylqf8lxg7rif1pl8si424yfi6xhv4agwyqzwirv0") (f (quote (("plugin-rt") ("plugin-mode"))))))

