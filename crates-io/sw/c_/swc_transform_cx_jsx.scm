(define-module (crates-io sw c_ swc_transform_cx_jsx) #:use-module (crates-io))

(define-public crate-swc_transform_cx_jsx-0.1.0 (c (n "swc_transform_cx_jsx") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.13") (d #t) (k 0)) (d (n "swc_core") (r "0.18.*") (f (quote ("plugin_transform"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.118.6") (d #t) (k 0)) (d (n "swc_ecma_transforms_base") (r "^0.108.7") (d #t) (k 0)) (d (n "testing") (r "^0.30.2") (d #t) (k 0)))) (h "092ld7pimyzh9j5whkmymv6x6vmlnlrmxv00sjscn2sa6vmz5m7c")))

