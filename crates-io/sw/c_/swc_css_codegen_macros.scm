(define-module (crates-io sw c_ swc_css_codegen_macros) #:use-module (crates-io))

(define-public crate-swc_css_codegen_macros-0.1.0 (c (n "swc_css_codegen_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0578rbrbwcvdbwfgi49g40f5ppkizmwry9b3wyvpyn5glqrf8nkm")))

(define-public crate-swc_css_codegen_macros-0.2.0 (c (n "swc_css_codegen_macros") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "1x9k4g4ij8mf3mjh658vyr1f1d9ci36x1qfv7pjgw6ni91al49zy")))

(define-public crate-swc_css_codegen_macros-0.2.1 (c (n "swc_css_codegen_macros") (v "0.2.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "06rf95m9i1dpyyhb0z2b22ja8qmk1556qxwxqkvl68snpbck5h81")))

(define-public crate-swc_css_codegen_macros-0.2.2 (c (n "swc_css_codegen_macros") (v "0.2.2") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0rf5vrlqwdhcz99flh46791l04bhqk8gyzx1qpib56lfdlvqg8kx")))

(define-public crate-swc_css_codegen_macros-0.2.3 (c (n "swc_css_codegen_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1x0yl58vari94lv0xmrm3r2v73c0iw5k5y9b9rljppyjphsddc8d")))

(define-public crate-swc_css_codegen_macros-0.2.4 (c (n "swc_css_codegen_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1mqkpknc13av6z1fl4hkpzc50x0zyk2n95yc3fm8bpmxgn6cwbny")))

