(define-module (crates-io sw c_ swc_html_utils) #:use-module (crates-io))

(define-public crate-swc_html_utils-0.1.0 (c (n "swc_html_utils") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.18.0") (d #t) (k 0)))) (h "1prixbbxafj445ywgx3hvfm869l3dbxdkkn43fvcwvl917m8jmv7")))

(define-public crate-swc_html_utils-0.2.0 (c (n "swc_html_utils") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.19.0") (d #t) (k 0)))) (h "0iwd46zlni3a0zgmsx16r0dvaazjiwmbgm8bj0wy22c3jnrmfqy3")))

(define-public crate-swc_html_utils-0.3.0 (c (n "swc_html_utils") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.20.0") (d #t) (k 0)))) (h "0qm7wvvypyiidb7h1vizh5k1zap1scik245zq89r7978c59zm1zx")))

(define-public crate-swc_html_utils-0.4.0 (c (n "swc_html_utils") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.21.0") (d #t) (k 0)))) (h "1qgb4w1xbhccs4wyi6lhjxh7x19xyycriywf8kdz2mlwrr2s93fx")))

(define-public crate-swc_html_utils-0.5.0 (c (n "swc_html_utils") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.22.0") (d #t) (k 0)))) (h "0hyz5s7snq6xjmymyhyfscm98bbafddd3cxzsd5shb9pm2vxskxv")))

(define-public crate-swc_html_utils-0.6.0 (c (n "swc_html_utils") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.23.0") (d #t) (k 0)))) (h "0hcr32jmrlrfgvx1j1l5xlzi1dar40xd2nv1gz643wyd8v98symc")))

(define-public crate-swc_html_utils-0.7.0 (c (n "swc_html_utils") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.24.0") (d #t) (k 0)))) (h "121zj5pl5dn5fb8m773jf04xvk0yc4pb5xxn861gyd92fql0z7q3")))

(define-public crate-swc_html_utils-0.8.0 (c (n "swc_html_utils") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.24.0") (d #t) (k 0)))) (h "10grv6pp5mn51cbm4pkshvwkzxqmnvyfr2c1vgfq85igdnz8n45p")))

(define-public crate-swc_html_utils-0.9.0 (c (n "swc_html_utils") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.24.2") (d #t) (k 0)))) (h "1hzhx3pqykxnysf56w9ivxwy06jwxr0bslyb3qrrywa3ayd71dfg")))

(define-public crate-swc_html_utils-0.10.0 (c (n "swc_html_utils") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.25.0") (d #t) (k 0)))) (h "0ary8k0pfh7x2y9x29l713z9df5j0dgr91jl02jdjwy8b12hkmkr")))

(define-public crate-swc_html_utils-0.11.0 (c (n "swc_html_utils") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.26.0") (d #t) (k 0)))) (h "0ian18mmh2x358gnwgac28wysycd109w42h77vqjxqra9q204jc0")))

(define-public crate-swc_html_utils-0.12.0 (c (n "swc_html_utils") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.0") (d #t) (k 0)))) (h "1dz15d7xz3hzayln0gl5p1c14yb1i4dr654jqhh8qa30sjl2rxgi")))

(define-public crate-swc_html_utils-0.12.1 (c (n "swc_html_utils") (v "0.12.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.0") (d #t) (k 0)))) (h "1113rbnh1xawpcs43n6qq8bypfac5mmwnbvwkinby0y43b411rll")))

(define-public crate-swc_html_utils-0.12.2 (c (n "swc_html_utils") (v "0.12.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.3") (d #t) (k 0)))) (h "0pp6bma745na6ssy66h153wk28alj62j7spdp39ss33h5lwcmh8j")))

(define-public crate-swc_html_utils-0.12.3 (c (n "swc_html_utils") (v "0.12.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.4") (d #t) (k 0)))) (h "0nr94bvb4qgjpx7rf5nssm76apb1jpy3vv7vbzv7mf2m3q533a6y")))

(define-public crate-swc_html_utils-0.12.4 (c (n "swc_html_utils") (v "0.12.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.5") (d #t) (k 0)))) (h "19g9lmrdfcplyl8wf6k53l8a4fl09n8q78nxik88cbqdy17qmman")))

(define-public crate-swc_html_utils-0.12.5 (c (n "swc_html_utils") (v "0.12.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.6") (d #t) (k 0)))) (h "1mdla4jmp2w5cc9ysd6xgiva2ljkr48qb2a6jrn9ggfngi02yqmp")))

(define-public crate-swc_html_utils-0.12.6 (c (n "swc_html_utils") (v "0.12.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.14") (d #t) (k 0)))) (h "1rnq52vajj8alyfk91bphsf760d7gvsim33lxqjwvgspmxrylahs")))

(define-public crate-swc_html_utils-0.12.7 (c (n "swc_html_utils") (v "0.12.7") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.15") (d #t) (k 0)))) (h "1nxhrc07f40gq727l5gnf97p70sjax323idcg7zp5yj8d7961xbg")))

(define-public crate-swc_html_utils-0.12.8 (c (n "swc_html_utils") (v "0.12.8") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.16") (d #t) (k 0)))) (h "053l8843az094nfmi3apxcyzzi51rc0lrxw26pampdvjw3x76lha")))

(define-public crate-swc_html_utils-0.13.0 (c (n "swc_html_utils") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.0") (d #t) (k 0)))) (h "1w7xhi25mpipbchga5ixmdh582f55cr8rnc01i5yvhmb59q4jdh4")))

(define-public crate-swc_html_utils-0.13.1 (c (n "swc_html_utils") (v "0.13.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.1") (d #t) (k 0)))) (h "106idwvzj9a165dfhw5w3gr457s6bz5qpw71kwrhqihaiycjfpzs")))

(define-public crate-swc_html_utils-0.13.2 (c (n "swc_html_utils") (v "0.13.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.2") (d #t) (k 0)))) (h "0qjk3h1gvx05qqcqpdlsxrybw1j4096frdn2gq03bzgc86w0dpb0")))

(define-public crate-swc_html_utils-0.13.3 (c (n "swc_html_utils") (v "0.13.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.3") (d #t) (k 0)))) (h "1i5sa8bgxriji05x812qcrz0vwijbmhl4i3n3h7s3q15wf4is1ks")))

(define-public crate-swc_html_utils-0.13.4 (c (n "swc_html_utils") (v "0.13.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.4") (d #t) (k 0)))) (h "1cpbzyfx2gdwa7a0q5rrxpbhbllv0w6xpm33by3x5fmksnf3cmbs")))

(define-public crate-swc_html_utils-0.13.5 (c (n "swc_html_utils") (v "0.13.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.5") (d #t) (k 0)))) (h "0lsdvvcfh2ld37a2gvjq5a9hlr2pnn7m5rpkpbyld2jcpibg6hy9")))

(define-public crate-swc_html_utils-0.13.6 (c (n "swc_html_utils") (v "0.13.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.6") (d #t) (k 0)))) (h "0z8w7mh8668y9n9mvv3swj7dp3mmhl023sj39zzgs2hs9gkmh6yc")))

(define-public crate-swc_html_utils-0.13.7 (c (n "swc_html_utils") (v "0.13.7") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.7") (d #t) (k 0)))) (h "10lmijpyayxhail29lp07cwsspk14zb0f9la0gw72xq5yqqs8831")))

(define-public crate-swc_html_utils-0.13.8 (c (n "swc_html_utils") (v "0.13.8") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.8") (d #t) (k 0)))) (h "1a0vrx95ny9zqlwm5pa7ylbwkgdpiqnqn1wfmf4ldysx5144zgxm")))

(define-public crate-swc_html_utils-0.13.9 (c (n "swc_html_utils") (v "0.13.9") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.9") (d #t) (k 0)))) (h "00qlxlb46a82szly2r1qrzffr0jnzkrl7g77nwl6b3xkaygf9i3p")))

(define-public crate-swc_html_utils-0.13.10 (c (n "swc_html_utils") (v "0.13.10") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.10") (d #t) (k 0)))) (h "0qa2d6rp0n3fh7as60icgcksmh1x69zafnmisyv8z2vbqpkf819m")))

(define-public crate-swc_html_utils-0.14.0 (c (n "swc_html_utils") (v "0.14.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.0") (d #t) (k 0)))) (h "0h9h6fd583y6hfc62j0z9lcrrvzxvdx7m8drz9kfi6h17qbqr2l6")))

(define-public crate-swc_html_utils-0.14.1 (c (n "swc_html_utils") (v "0.14.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.1") (d #t) (k 0)))) (h "1hq2yq91v8ypbwlg4yyc0hawsdklhlinqwsdlik161xwy5ck4aad")))

(define-public crate-swc_html_utils-0.14.2 (c (n "swc_html_utils") (v "0.14.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.2") (d #t) (k 0)))) (h "11rhd176l3cib1lfl6z1aii59zz0qka9qrn6w1yzv376j20n4zna")))

(define-public crate-swc_html_utils-0.14.3 (c (n "swc_html_utils") (v "0.14.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.3") (d #t) (k 0)))) (h "15jksfn0mpp127cfjayy4320v7jhchdfnkp2a4g04nm10qfllyvg")))

(define-public crate-swc_html_utils-0.14.4 (c (n "swc_html_utils") (v "0.14.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.3") (d #t) (k 0)))) (h "086x15d24zsd9zjd0sz2dzqxgw5pmkg4yvsv3isn1c1ap8vpg4ai")))

(define-public crate-swc_html_utils-0.14.5 (c (n "swc_html_utils") (v "0.14.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.4") (d #t) (k 0)))) (h "1jddidkh9zkf0fja6mpl4g06n8n90mlpqar14yw9yb6q837ja0bd")))

(define-public crate-swc_html_utils-0.14.6 (c (n "swc_html_utils") (v "0.14.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.5") (d #t) (k 0)))) (h "0j60x5b7670pi565sdhzj0792pm0ybvq1cc7mb13zqw4a2dpbg05")))

(define-public crate-swc_html_utils-0.14.7 (c (n "swc_html_utils") (v "0.14.7") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.19") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.6") (d #t) (k 0)))) (h "0q0mms7xln7l59lq1p6jycynn821f13awpk3xm2vyvpyw46dfp17")))

(define-public crate-swc_html_utils-0.14.8 (c (n "swc_html_utils") (v "0.14.8") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.20") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.7") (d #t) (k 0)))) (h "1kn3kvyk4jkmpscdpk21122myhvk5wgmcy21kfvjx13kbbjp11zz")))

(define-public crate-swc_html_utils-0.14.9 (c (n "swc_html_utils") (v "0.14.9") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.21") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.8") (d #t) (k 0)))) (h "09y7ssjchrzr66jwp535jgyyck0w15f4adjk24rwk6l5a6cayx2d")))

(define-public crate-swc_html_utils-0.14.10 (c (n "swc_html_utils") (v "0.14.10") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.22") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.9") (d #t) (k 0)))) (h "0711bqhw57h1zxjy6kjvw89lvdkj8qybh567zg3h01nkz3jik5cz")))

(define-public crate-swc_html_utils-0.14.11 (c (n "swc_html_utils") (v "0.14.11") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.23") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.10") (d #t) (k 0)))) (h "0biswxaxm94a58nbxwzz87sy3mrzy24cjsw3jb4mw922q3r2w3ig")))

(define-public crate-swc_html_utils-0.14.12 (c (n "swc_html_utils") (v "0.14.12") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.11") (d #t) (k 0)))) (h "1pcfpfwr0xwx5vbnrnygm5z6v68z9zfc7a18b3dl8afdwzf8j7bp")))

(define-public crate-swc_html_utils-0.14.13 (c (n "swc_html_utils") (v "0.14.13") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.12") (d #t) (k 0)))) (h "1283cvl6ndw3vmbk3a9899pvx49w158z7kyiyxmcx2v6f3vc2897")))

(define-public crate-swc_html_utils-0.14.14 (c (n "swc_html_utils") (v "0.14.14") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.13") (d #t) (k 0)))) (h "14524fw4ka0pkxbjgwazb0j7ngagg3rcrbz8s0z0k0w0vmj731q7")))

(define-public crate-swc_html_utils-0.14.15 (c (n "swc_html_utils") (v "0.14.15") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.14") (d #t) (k 0)))) (h "0prjfgciahkp547522v7hv06pjfwpxarw3k2i87f08npcc8wzjc1")))

(define-public crate-swc_html_utils-0.14.16 (c (n "swc_html_utils") (v "0.14.16") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.15") (d #t) (k 0)))) (h "16fgzpp0hfhc5cp684sjb03wd2a1b9s2lps99x4lx7vp4yccgs0h")))

(define-public crate-swc_html_utils-0.14.17 (c (n "swc_html_utils") (v "0.14.17") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.16") (d #t) (k 0)))) (h "153pivjh8xlby849vi3dxpmvss62jwmc7373x35gzbvkqckhj869")))

(define-public crate-swc_html_utils-0.14.18 (c (n "swc_html_utils") (v "0.14.18") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.17") (d #t) (k 0)))) (h "11ql1ing9nn6dibcxg1j5x0z33h4g2xgsjpwmhsykl7zv53b586z")))

(define-public crate-swc_html_utils-0.14.19 (c (n "swc_html_utils") (v "0.14.19") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.18") (d #t) (k 0)))) (h "1xcmblj2vl5mx47v5m5ni4c2njaj9s3z0lj6chsmbv1xpmqkc8h2")))

(define-public crate-swc_html_utils-0.14.20 (c (n "swc_html_utils") (v "0.14.20") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.19") (d #t) (k 0)))) (h "098yq1vag7yl2x9rczkkk5lq6qipi276mp0v3di78dvca51fq8wf")))

(define-public crate-swc_html_utils-0.14.21 (c (n "swc_html_utils") (v "0.14.21") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.26") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.20") (d #t) (k 0)))) (h "0pmci2q7hxmvr1s7pmz1az6y47315njxi5viln3haj1hxgiv6jks")))

(define-public crate-swc_html_utils-0.14.22 (c (n "swc_html_utils") (v "0.14.22") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.27") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.21") (d #t) (k 0)))) (h "1m1gzghjc7gn8rdi21qzxyyw454vcnv9sprcr0kidh1b1nm5n8aq")))

(define-public crate-swc_html_utils-0.14.23 (c (n "swc_html_utils") (v "0.14.23") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.28") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.22") (d #t) (k 0)))) (h "0h8v6disi7j95hz1lciw7i9nbnw7abza457q72xhq3vq6yb6q2jl")))

(define-public crate-swc_html_utils-0.14.24 (c (n "swc_html_utils") (v "0.14.24") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.29") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.23") (d #t) (k 0)))) (h "0ndpvkphxd0yyyny5vcv4705ai1432k54x01672syvx6fvaigd69")))

(define-public crate-swc_html_utils-0.14.25 (c (n "swc_html_utils") (v "0.14.25") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.31") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.24") (d #t) (k 0)))) (h "12w59gnpm9ip14w3nkyhzcb4b250s2xhffdl7jzm8syv47cwv849")))

(define-public crate-swc_html_utils-0.14.26 (c (n "swc_html_utils") (v "0.14.26") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.25") (d #t) (k 0)))) (h "1hlzzg3adnrnqihmxiwdzx76l0cczs2gwm4s48jai69m04a21wd8")))

(define-public crate-swc_html_utils-0.14.27 (c (n "swc_html_utils") (v "0.14.27") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.26") (d #t) (k 0)))) (h "12d6yyvza83jf9f57jnzr285mxyjw0835wnxq21m4rp03ab2d9mm")))

(define-public crate-swc_html_utils-0.14.28 (c (n "swc_html_utils") (v "0.14.28") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.27") (d #t) (k 0)))) (h "122316h41f0rpf5mr26gba6gfyhcc0wyqb90zvpw1bm4gihzjkj3")))

(define-public crate-swc_html_utils-0.14.29 (c (n "swc_html_utils") (v "0.14.29") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.28") (d #t) (k 0)))) (h "06dv8xs0s18bh55r3a25clv2adivc52cly3r37d585vwizhafh76")))

(define-public crate-swc_html_utils-0.14.30 (c (n "swc_html_utils") (v "0.14.30") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.29") (d #t) (k 0)))) (h "18q684rcz7scm58cyq02qk1s90lg93vqlc7gnygcyf9b2msndvqg")))

(define-public crate-swc_html_utils-0.14.31 (c (n "swc_html_utils") (v "0.14.31") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.35") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.30") (d #t) (k 0)))) (h "03r0b95xyb2izx57ni25cvkiz6z34jk455wsrkjw6j81s2rsl5n8")))

(define-public crate-swc_html_utils-0.14.32 (c (n "swc_html_utils") (v "0.14.32") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.36") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.31") (d #t) (k 0)))) (h "0b7b88q8c5552x57gf9ik3cayfcqfynbg3424hyafsak24h54i2p")))

(define-public crate-swc_html_utils-0.14.33 (c (n "swc_html_utils") (v "0.14.33") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.37") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.32") (d #t) (k 0)))) (h "0jdgsnxwqpwainffx22742sg99rphlqzj4n95pq709bj6pzhljix")))

(define-public crate-swc_html_utils-0.14.34 (c (n "swc_html_utils") (v "0.14.34") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.38") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.33") (d #t) (k 0)))) (h "0s2iwx9a7zlr3n7prayhzr55shl63zyj4g5wfi686680f251a6fk")))

(define-public crate-swc_html_utils-0.14.35 (c (n "swc_html_utils") (v "0.14.35") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.38") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.34") (d #t) (k 0)))) (h "14njykzfl3wykz9wflx5bgkizl27sqqa33q9p7didr6qlrj9k0wc")))

(define-public crate-swc_html_utils-0.14.36 (c (n "swc_html_utils") (v "0.14.36") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.35") (d #t) (k 0)))) (h "098myg21zvyr49gcvk6i0np1nra80y39mmscrg887xiwcldyycan")))

(define-public crate-swc_html_utils-0.14.37 (c (n "swc_html_utils") (v "0.14.37") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.36") (d #t) (k 0)))) (h "0rg0ryms8k802svb37j09a2wmq0si8x043jyjq70q1jfrl5wg4d5")))

(define-public crate-swc_html_utils-0.14.38 (c (n "swc_html_utils") (v "0.14.38") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.37") (d #t) (k 0)))) (h "0ldhzvbcw7dcq8k9f8468vhq2qzby38s2r9z77h6dqdnl40sdpdp")))

(define-public crate-swc_html_utils-0.14.39 (c (n "swc_html_utils") (v "0.14.39") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.38") (d #t) (k 0)))) (h "03vzq50nnvfais34mrnpsrlrl4r4h3mzl8pp5v23wvdimmr2qwqz")))

(define-public crate-swc_html_utils-0.14.40 (c (n "swc_html_utils") (v "0.14.40") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.39") (d #t) (k 0)))) (h "0iahh6zvhc8lvq0bzfsv92x6pvg2aaq7rr2afdxdjvqq2l194ddm")))

(define-public crate-swc_html_utils-0.14.41 (c (n "swc_html_utils") (v "0.14.41") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.40") (d #t) (k 0)))) (h "1x25h81x3hwaipbvlsyhjl7833yn8apv8yici82hk37l4j2nc87j")))

(define-public crate-swc_html_utils-0.15.0 (c (n "swc_html_utils") (v "0.15.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.0") (d #t) (k 0)))) (h "1y55zkrlfrp2l0b4jy9yf22jq21vbsq8zw5jxwa018nsjzasxvc6")))

(define-public crate-swc_html_utils-0.15.1 (c (n "swc_html_utils") (v "0.15.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.40") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.1") (d #t) (k 0)))) (h "1w7aa190gin6ax9ycm3rq3kwa7s6b3zlrnxqc2j6n70ahsmldadw")))

(define-public crate-swc_html_utils-0.15.2 (c (n "swc_html_utils") (v "0.15.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.41") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.2") (d #t) (k 0)))) (h "1cg80y9csfbdc5jgk38vgw0i0fj4c72bmhiz176nqp4s7zy8gpmr")))

(define-public crate-swc_html_utils-0.15.3 (c (n "swc_html_utils") (v "0.15.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.42") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.3") (d #t) (k 0)))) (h "1km97fpzkiw1bsxzx5vnlf06930b6gr0nhy380qqn3g9q8y3bmkk")))

(define-public crate-swc_html_utils-0.15.4 (c (n "swc_html_utils") (v "0.15.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.43") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.4") (d #t) (k 0)))) (h "12zd568w51ap6cij4d3wa2y5wmcdi2h4w8amrzikhj70gvw8zzrl")))

(define-public crate-swc_html_utils-0.15.5 (c (n "swc_html_utils") (v "0.15.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.43") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.5") (d #t) (k 0)))) (h "12hv0wp30n9y25510h6016pb2nslp519lmhcjxysysh9zpjri2pd")))

(define-public crate-swc_html_utils-0.16.0 (c (n "swc_html_utils") (v "0.16.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.0") (d #t) (k 0)))) (h "0a50zq7ql4zlw1v9kxvm5x9hnirvqmbw2gn1hg5gb6045k3pj57w")))

(define-public crate-swc_html_utils-0.16.1 (c (n "swc_html_utils") (v "0.16.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.1") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.1") (d #t) (k 0)))) (h "0rypyk9jlfk21kclrdi53yqpr56pif16ij7v6z7hszcrjaz4lxis")))

(define-public crate-swc_html_utils-0.16.2 (c (n "swc_html_utils") (v "0.16.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.2") (d #t) (k 0)))) (h "1xk4b2ki6nzzqh6m1qpds0y23xp5ck6jcws65bwga4v6dd89haf0")))

(define-public crate-swc_html_utils-0.16.3 (c (n "swc_html_utils") (v "0.16.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.3") (d #t) (k 0)))) (h "18z3f2xj2h4bhvjcx9ramyz5a71947l9jinv24g3fw38d05r7isw")))

(define-public crate-swc_html_utils-0.16.4 (c (n "swc_html_utils") (v "0.16.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.4") (d #t) (k 0)))) (h "0429ckg66x3ahlflsagjwchs4lva1m83ibav780nhpn61aal18s3")))

(define-public crate-swc_html_utils-0.16.5 (c (n "swc_html_utils") (v "0.16.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.5") (d #t) (k 0)))) (h "12hv6azrqh6vgbaayfw58igkwmnqk229irx4g1zwyq6plh63b1j3")))

(define-public crate-swc_html_utils-0.16.6 (c (n "swc_html_utils") (v "0.16.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.6") (d #t) (k 0)))) (h "12p4cwxvdph8gdpzqvc3aml9lnzq1q53zsld3m9anhppkn33nmsv")))

(define-public crate-swc_html_utils-0.16.7 (c (n "swc_html_utils") (v "0.16.7") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.7") (d #t) (k 0)))) (h "134fqh1ccfvfpxq2l43p7f7927qwc3dxpbksg1yn4vfa447111hf")))

(define-public crate-swc_html_utils-0.16.8 (c (n "swc_html_utils") (v "0.16.8") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.8") (d #t) (k 0)))) (h "0k3gwfzcq2bnay9cn3jjqxrz6b276nr2xi1my593a6lfvjzxb9h3")))

(define-public crate-swc_html_utils-0.16.9 (c (n "swc_html_utils") (v "0.16.9") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.9") (d #t) (k 0)))) (h "0zrh1cdc01gcg8774hcsb8r60fqw2spznxm2dsr8ig2b68wkr2g0")))

(define-public crate-swc_html_utils-0.16.10 (c (n "swc_html_utils") (v "0.16.10") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.10") (d #t) (k 0)))) (h "1d6kpqbdycnmndf16j2pwkrb79msp0k1qb55xgnyccq30jzhfpbq")))

(define-public crate-swc_html_utils-0.16.11 (c (n "swc_html_utils") (v "0.16.11") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.11") (d #t) (k 0)))) (h "1agyr25km5869g1zx24i05r10cn7f3na4acx6gnfbprbkqpv6i88")))

(define-public crate-swc_html_utils-0.16.12 (c (n "swc_html_utils") (v "0.16.12") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.12") (d #t) (k 0)))) (h "181zvy8j7rhr5z8i6iyim2wkqy440ad2mvpk5c4b345y3h82k8sg")))

(define-public crate-swc_html_utils-0.16.13 (c (n "swc_html_utils") (v "0.16.13") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.13") (d #t) (k 0)))) (h "0nw6gl13ck208g8grvjniix01qv5pc4v9qzykx24d9fnpb6jnbjn")))

(define-public crate-swc_html_utils-0.16.14 (c (n "swc_html_utils") (v "0.16.14") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.14") (d #t) (k 0)))) (h "0mrqxnmprybkwrqh2019gxvjbz1rpnqblsg42mmwxrkzh40armgr")))

(define-public crate-swc_html_utils-0.16.15 (c (n "swc_html_utils") (v "0.16.15") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.15") (d #t) (k 0)))) (h "0395v2209l5dx1llqxcg5d0nkgycd4sycphy5zf7bqf5zcp6wpdc")))

(define-public crate-swc_html_utils-0.16.16 (c (n "swc_html_utils") (v "0.16.16") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.16") (d #t) (k 0)))) (h "1chm58fp1jx24ap8ikdy303w4j5dxn9yk7ic139s11rdz9r5wyd8")))

(define-public crate-swc_html_utils-0.16.17 (c (n "swc_html_utils") (v "0.16.17") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.7") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.17") (d #t) (k 0)))) (h "0v6apf1fd7q0kfl7jh3zddd6vzhxvkh9zi36l50xl61nalkx4i2l")))

(define-public crate-swc_html_utils-0.16.18 (c (n "swc_html_utils") (v "0.16.18") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.18") (d #t) (k 0)))) (h "0y4wqw0gzrkgjs5dxiv28jizb9z40vsvp67yfymlf8wxybnkyw2b")))

(define-public crate-swc_html_utils-0.16.19 (c (n "swc_html_utils") (v "0.16.19") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.19") (d #t) (k 0)))) (h "0zljaxk5ami3lpmwy7v2qp3mkxr0s61z2av4hfzn269bh2hm3kx0")))

(define-public crate-swc_html_utils-0.16.20 (c (n "swc_html_utils") (v "0.16.20") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.20") (d #t) (k 0)))) (h "11y1vwq18f46xg8bqfbpnnlgzhgzp9kp4msxfavfs26yilliqm2n")))

(define-public crate-swc_html_utils-0.16.21 (c (n "swc_html_utils") (v "0.16.21") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.21") (d #t) (k 0)))) (h "1713vnjpsh6hdb55ffrjh11k1p3b6k3gaaimgx588ri7n7szp53j")))

(define-public crate-swc_html_utils-0.16.22 (c (n "swc_html_utils") (v "0.16.22") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.22") (d #t) (k 0)))) (h "0c74g491z4pmahlcwv507jljby7x072hlp6z44h38ww341791h8s")))

(define-public crate-swc_html_utils-0.17.0 (c (n "swc_html_utils") (v "0.17.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.0") (d #t) (k 0)))) (h "0qd7czk88gfm7bjnssbwzayv250kgxnb4iwfh2cq2bb6rjdrbh11")))

(define-public crate-swc_html_utils-0.17.1 (c (n "swc_html_utils") (v "0.17.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.1") (d #t) (k 0)))) (h "01ibfjj50cxrchlacj82aapvqnxax0sqbh4vpvss0yzsdi25pkhm")))

(define-public crate-swc_html_utils-0.17.2 (c (n "swc_html_utils") (v "0.17.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.2") (d #t) (k 0)))) (h "0lhbzcm4lnddr3rbdhglhmqb3mdf1nrb65g3ynnwhmwkzcxwn4nd")))

(define-public crate-swc_html_utils-0.18.0 (c (n "swc_html_utils") (v "0.18.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.0") (d #t) (k 0)))) (h "0jr1h579qfx2qsq135vd81ngmkd2qrbnib8axnwsf2xkz60lrbmy")))

(define-public crate-swc_html_utils-0.18.1 (c (n "swc_html_utils") (v "0.18.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.1") (d #t) (k 0)))) (h "0wg5fzijsw9zvgg7xlbb81s40bsdlxij10s2w7plp89mvw5ga2md")))

(define-public crate-swc_html_utils-0.18.2 (c (n "swc_html_utils") (v "0.18.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)))) (h "1m5rphwis6nqcpznz4nxacshh52d5yzp17blk1c04fhhmbayakpi")))

(define-public crate-swc_html_utils-0.18.3 (c (n "swc_html_utils") (v "0.18.3") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.3") (d #t) (k 0)))) (h "1r0km7ds6p1j1y38kd8hk6dblk4prndf2pqfnrx6dd3fy456q3sb")))

(define-public crate-swc_html_utils-0.18.4 (c (n "swc_html_utils") (v "0.18.4") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.4") (d #t) (k 0)))) (h "05qrxndzzg6l8l5dbgx7nvzklv1j4378fxs0l90bqhjvls3b8x7d")))

(define-public crate-swc_html_utils-0.18.5 (c (n "swc_html_utils") (v "0.18.5") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.1") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.5") (d #t) (k 0)))) (h "1rdsmc1gwg0f890bqx8bzrkaj6fsgzi6w6m7pkvpljs2qp38adbg")))

(define-public crate-swc_html_utils-0.18.6 (c (n "swc_html_utils") (v "0.18.6") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.6") (d #t) (k 0)))) (h "07c51d7aqc00kqpg4ggcnpz4ksfp4a7ivi4iqgkkhwmjsihnn048")))

(define-public crate-swc_html_utils-0.18.7 (c (n "swc_html_utils") (v "0.18.7") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.7") (d #t) (k 0)))) (h "1l3fadnmi9kq37j87vfas3q23zg79kkkigh4hxp147n2gsww2zyh")))

(define-public crate-swc_html_utils-0.18.8 (c (n "swc_html_utils") (v "0.18.8") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.8") (d #t) (k 0)))) (h "0kx3m89gh0s5ya3w04r4a9q5r77i2mgd5s1cinwca1463qygqgq2")))

(define-public crate-swc_html_utils-0.18.9 (c (n "swc_html_utils") (v "0.18.9") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.9") (d #t) (k 0)))) (h "1nm9dx2m0zy12n25wy9mvnx29614mvlbga8c47inpinb9ygd1maa")))

(define-public crate-swc_html_utils-0.18.10 (c (n "swc_html_utils") (v "0.18.10") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.10") (d #t) (k 0)))) (h "1x560ksvc1chafxa2b158m6lip8hpi0nmjc9r0igqk6rv5vi0zq1")))

(define-public crate-swc_html_utils-0.18.11 (c (n "swc_html_utils") (v "0.18.11") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.11") (d #t) (k 0)))) (h "0g4qh87a24zf3ip5691233y3ns69ccgviyp2nfydz98ndrjr3hs8")))

(define-public crate-swc_html_utils-0.18.12 (c (n "swc_html_utils") (v "0.18.12") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.12") (d #t) (k 0)))) (h "0mx4jsjyaf0f2asr95bf4xzhiy2mwywfwyndf6xqlfbzl7w2dyms")))

(define-public crate-swc_html_utils-0.18.13 (c (n "swc_html_utils") (v "0.18.13") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.14") (d #t) (k 0)))) (h "1x5aqq39lxbj97kyrf2qc0hh8qxpicy6xadp1pba9w7jk6lkamcl")))

(define-public crate-swc_html_utils-0.18.14 (c (n "swc_html_utils") (v "0.18.14") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.15") (d #t) (k 0)))) (h "00h40vjky5n67ibiplg43gymfq74ncrfrx3wiq9mfjgjmma69g4q")))

(define-public crate-swc_html_utils-0.18.15 (c (n "swc_html_utils") (v "0.18.15") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.16") (d #t) (k 0)))) (h "0s897fvlvg1dhg0qnh1976dmig9zrc6d7gbk695f0lid553p3x4c")))

(define-public crate-swc_html_utils-0.18.16 (c (n "swc_html_utils") (v "0.18.16") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.17") (d #t) (k 0)))) (h "0ygnah5ychg7kkb53n7z8fs5q2idvyq56jpzp20rzvg08z17xh2i")))

(define-public crate-swc_html_utils-0.18.17 (c (n "swc_html_utils") (v "0.18.17") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.18") (d #t) (k 0)))) (h "0g6xmfcw1hp0bmbmyxzn1s3h39fgz3n5sahdqjmcliihzd3xa893")))

(define-public crate-swc_html_utils-0.18.18 (c (n "swc_html_utils") (v "0.18.18") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.19") (d #t) (k 0)))) (h "0ib44n4qdpycc57y0hxhp7fsq6yqhb8f88k613ba4s7srjrdkzps")))

(define-public crate-swc_html_utils-0.18.19 (c (n "swc_html_utils") (v "0.18.19") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.20") (d #t) (k 0)))) (h "0pxsbvpsv09x24fygq9s6d2j3qrk4rykp7zhzz88s860k56811ly")))

(define-public crate-swc_html_utils-0.18.20 (c (n "swc_html_utils") (v "0.18.20") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.20") (d #t) (k 0)))) (h "10fp7rw5s9c822l4k5jx22qss3yjhgmlhayknwd1rap0agbq7wq8")))

(define-public crate-swc_html_utils-0.18.21 (c (n "swc_html_utils") (v "0.18.21") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.26") (d #t) (k 0)))) (h "1a1mx6sd9m0appch32bh75idcdkzbvlajq2h0qk6fnlid77iyva0")))

