(define-module (crates-io sw c_ swc_macros_common) #:use-module (crates-io))

(define-public crate-swc_macros_common-0.1.0 (c (n "swc_macros_common") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0kkp3rm49lkal11bnx18hycys2fqkr2gn9w4ns1azyzpaldjxfax")))

(define-public crate-swc_macros_common-0.1.1 (c (n "swc_macros_common") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "02nd0g8f4qp8gb37ycj7rxi6j89qjr7qp7pvks94pv8y1wcfi8xc")))

(define-public crate-swc_macros_common-0.1.2 (c (n "swc_macros_common") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0k6sxfvi3fcvlvbg3s2fnmrvcnfyf09md576v0lhmxcl62pdml6r")))

(define-public crate-swc_macros_common-0.1.3 (c (n "swc_macros_common") (v "0.1.3") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "11qldmyvj8ns370m0yyl4pny84zx831l4v20zyr101sc2ixmyir5")))

(define-public crate-swc_macros_common-0.1.4 (c (n "swc_macros_common") (v "0.1.4") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0lyk5azdvsr66a83zdvac8hrsnw13irz8vv5brq60jrssh16pv0v")))

(define-public crate-swc_macros_common-0.2.0 (c (n "swc_macros_common") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1zv00g77l8kap0ckhj6klp51fqgqi02w77wdrb28l5yczib0j6dn")))

(define-public crate-swc_macros_common-0.3.0 (c (n "swc_macros_common") (v "0.3.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0axry61mznmnik9whymizprk0nrjsawsskx71vpsachxb09qwqyi")))

(define-public crate-swc_macros_common-0.3.1 (c (n "swc_macros_common") (v "0.3.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "04n5sgpcsiajxdljzwwrpkifhjxsfyddyqykjw2kff0955yz5a8q")))

(define-public crate-swc_macros_common-0.3.2 (c (n "swc_macros_common") (v "0.3.2") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "18xdj1q20dv58arisc69nn47i8z1l1v0nvdyib9s7jzvizknhz5z")))

(define-public crate-swc_macros_common-0.3.3 (c (n "swc_macros_common") (v "0.3.3") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "=1.0.65") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0y4syp526i14jcwy3gwlbl707xlnlb9hzjb2zrql06js1y9jxv88")))

(define-public crate-swc_macros_common-0.3.4 (c (n "swc_macros_common") (v "0.3.4") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "15q7a0d8ll1ir5br7hk74angrbs14zsv881a8a71m6f45xp8ngq3")))

(define-public crate-swc_macros_common-0.3.5 (c (n "swc_macros_common") (v "0.3.5") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1qyclrjvq4mhn4sc9w00xa0qy4j5q3vm0c9pqf24dnh2ipqa7p6m")))

(define-public crate-swc_macros_common-0.3.6 (c (n "swc_macros_common") (v "0.3.6") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "18nav28q73x9i42947i1y0755dr26fkscwbwpkclh9l80y1rigm4")))

(define-public crate-swc_macros_common-0.3.7 (c (n "swc_macros_common") (v "0.3.7") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "19ifaq560p91la1pdkr9pqfh37p4bggq2dwj4j2j6s927hz2qn1y")))

(define-public crate-swc_macros_common-0.3.8 (c (n "swc_macros_common") (v "0.3.8") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0cp889m1s4831vi6m8809hf679656hxrzi48pvx536xhrh2k49vs")))

(define-public crate-swc_macros_common-0.3.9 (c (n "swc_macros_common") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1sgw6vrx12hvjra5k6r105q0bgizqlg9vzn684pv52xw3ky6q5sh")))

(define-public crate-swc_macros_common-0.3.10 (c (n "swc_macros_common") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "18jsv3s8lv7p6i5hj6gg609bf29qnsmax2v1xl6q98lmd9vffnss")))

(define-public crate-swc_macros_common-0.3.11 (c (n "swc_macros_common") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "visit" "parsing" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0rl498f9hv7i0ap0fr9ryzgpksf04z23g13nvb996x05c4smyx4i")))

