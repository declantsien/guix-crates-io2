(define-module (crates-io sw c_ swc_babel_ast) #:use-module (crates-io))

(define-public crate-swc_babel_ast-0.1.0 (c (n "swc_babel_ast") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.14") (d #t) (k 0)) (d (n "swc_node_base") (r "^0.5") (d #t) (k 0)))) (h "179vyddlcfj6yb21zbaw6hx23xvs80g6d4cx6qzhfz38fp0ysi5y")))

