(define-module (crates-io sw c_ swc_ecma_parser_macros) #:use-module (crates-io))

(define-public crate-swc_ecma_parser_macros-0.1.0 (c (n "swc_ecma_parser_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("fold"))) (d #t) (k 0)))) (h "01idb368dijnp2malz6jg6bpyd16fyr3h85xibi1dpyrrkjcxj46")))

(define-public crate-swc_ecma_parser_macros-0.2.0 (c (n "swc_ecma_parser_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("fold"))) (d #t) (k 0)))) (h "1sh58zkb7v38lcp3xlh9wzpw9268a4cm64lgrvajspjkjblgdz1h")))

(define-public crate-swc_ecma_parser_macros-0.2.1 (c (n "swc_ecma_parser_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("fold"))) (d #t) (k 0)))) (h "047i1j19y8cywvps34c4wgdgkiys55q85804v6lxh67jrhm4iq4p")))

(define-public crate-swc_ecma_parser_macros-0.3.0 (c (n "swc_ecma_parser_macros") (v "0.3.0") (d (list (d (n "pmutil") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("fold" "parsing"))) (d #t) (k 0)))) (h "1g5rk7gk7zkpxkgh5g3pw6ak3nfj6md43407wmmz7454d08npbwh")))

(define-public crate-swc_ecma_parser_macros-0.4.0 (c (n "swc_ecma_parser_macros") (v "0.4.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "parsing" "full"))) (d #t) (k 0)))) (h "0alvab99d74kifb31gds0hsiygkfj8dvp58hz5zmk39p8vrycfkv")))

(define-public crate-swc_ecma_parser_macros-0.4.1 (c (n "swc_ecma_parser_macros") (v "0.4.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "parsing" "full"))) (d #t) (k 0)))) (h "1rrql4w6wjxmxqf4ywlixqhm2dqjpns75jwb4g7q9f3r5h783647")))

