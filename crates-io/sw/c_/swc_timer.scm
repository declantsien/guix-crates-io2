(define-module (crates-io sw c_ swc_timer) #:use-module (crates-io))

(define-public crate-swc_timer-0.1.0 (c (n "swc_timer") (v "0.1.0") (d (list (d (n "testing") (r "^0.15.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1h32d3pm6v46k4c58y5jy4fnsg7xhcwpqxypizqx3amr3858v663")))

(define-public crate-swc_timer-0.2.0 (c (n "swc_timer") (v "0.2.0") (d (list (d (n "testing") (r "^0.16.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wh39xzy2r9l2sqigcj9ilaj5ahxfa29qvcqbl79jhq9d60z2rkb")))

(define-public crate-swc_timer-0.2.1 (c (n "swc_timer") (v "0.2.1") (d (list (d (n "testing") (r "^0.16.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gr5dvi1rmq23im19b2a8i5p4kc97lhwm8yczb6glhq31h4xls09")))

(define-public crate-swc_timer-0.3.0 (c (n "swc_timer") (v "0.3.0") (d (list (d (n "testing") (r "^0.17.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mwycxjmkwpv01jiasqz7awv3gaiax28qvnhl9d1k8443qpkksl1")))

(define-public crate-swc_timer-0.4.0 (c (n "swc_timer") (v "0.4.0") (d (list (d (n "testing") (r "^0.18.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07vk9yd8d4d7p5nm23z5nx59xzvzkdhypgpp5cddwli5818ajw8k")))

(define-public crate-swc_timer-0.5.0 (c (n "swc_timer") (v "0.5.0") (d (list (d (n "testing") (r "^0.19.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0v59ksl737jvqm9qjmaapkw450alg7xwa9hwy1s9x1qrqdnn486s")))

(define-public crate-swc_timer-0.6.0 (c (n "swc_timer") (v "0.6.0") (d (list (d (n "testing") (r "^0.20.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1vgq4z8cw541kgm9amsa3qmchizh6dnfl7kj7p3lif62glbnrjk0")))

(define-public crate-swc_timer-0.7.0 (c (n "swc_timer") (v "0.7.0") (d (list (d (n "testing") (r "^0.21.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "19a7bs8zr0738n109hrqnax4apci9ahflr2am1skd0s1wb00vk53")))

(define-public crate-swc_timer-0.8.0 (c (n "swc_timer") (v "0.8.0") (d (list (d (n "testing") (r "^0.22.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "14y8cb98ca3mg8pknjc5dlw4g3d9pfajrrnbsrhl7jm4c05iflzf")))

(define-public crate-swc_timer-0.9.0 (c (n "swc_timer") (v "0.9.0") (d (list (d (n "testing") (r "^0.23.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0s1578nignhh37iy34g32pgs6r8qnaganzq2b555nwaij7xx0gsl")))

(define-public crate-swc_timer-0.10.0 (c (n "swc_timer") (v "0.10.0") (d (list (d (n "testing") (r "^0.24.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1appahd56vaq00kjbfgcdf48938za2gd95s7nqw6s5wwrxdfa403")))

(define-public crate-swc_timer-0.11.0 (c (n "swc_timer") (v "0.11.0") (d (list (d (n "testing") (r "^0.25.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "022c702gbmg1jy8011wkhx2qjd9kgqvn16xjzj8b6sisiirpph3m")))

(define-public crate-swc_timer-0.12.0 (c (n "swc_timer") (v "0.12.0") (d (list (d (n "testing") (r "^0.26.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "019f89wxr5fy74a31lz5936algkwrf7dx9ysfz451s0vvfyh87yx")))

(define-public crate-swc_timer-0.13.0 (c (n "swc_timer") (v "0.13.0") (d (list (d (n "testing") (r "^0.27.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1gnl8yr46708qz2mkgmhmhlcrr5nhr4b4cnfplhfc2672gmgwiqd")))

(define-public crate-swc_timer-0.14.0 (c (n "swc_timer") (v "0.14.0") (d (list (d (n "testing") (r "^0.28.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1yck21x9ixjdqjw9alihhnaj31bbjh6xy4cq0dix21iplxjlfzn4")))

(define-public crate-swc_timer-0.15.0 (c (n "swc_timer") (v "0.15.0") (d (list (d (n "testing") (r "^0.29.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1iym5h2csj0rdzc4zigi097s5jjcm1dm1s8dvg7jwpsxdhfdgflq")))

(define-public crate-swc_timer-0.15.1 (c (n "swc_timer") (v "0.15.1") (d (list (d (n "testing") (r "^0.29.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1d6n6i89rc5hbj8sa20cj1appv99pafqk39llbaxsadw9nr4nqnl")))

(define-public crate-swc_timer-0.15.2 (c (n "swc_timer") (v "0.15.2") (d (list (d (n "testing") (r "^0.29.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1kcxy3sa0cksw79222q8nsmg0j03291nr42i5pbb61kp8yj2pnxw")))

(define-public crate-swc_timer-0.15.3 (c (n "swc_timer") (v "0.15.3") (d (list (d (n "testing") (r "^0.29.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1dhfza837rqmpx97x1mxn4qcdhlpb02zy2015ar2f7zdsbp6y4hj")))

(define-public crate-swc_timer-0.15.4 (c (n "swc_timer") (v "0.15.4") (d (list (d (n "testing") (r "^0.29.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "15n7bcwihk096idxfyi3bn38zkb28yli8i0p73b7jf3nj9gqfk7r")))

(define-public crate-swc_timer-0.15.5 (c (n "swc_timer") (v "0.15.5") (d (list (d (n "testing") (r "^0.29.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1ggxhh1jsvsh0xqvfjfpj7mwmzgqvbv983z6qzjg2p6b3cx5xqpy")))

(define-public crate-swc_timer-0.15.6 (c (n "swc_timer") (v "0.15.6") (d (list (d (n "testing") (r "^0.29.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0pr5d3fql9drbpn7267zks90xn826b9fiqx044yscjc22gdg3qkl")))

(define-public crate-swc_timer-0.15.7 (c (n "swc_timer") (v "0.15.7") (d (list (d (n "testing") (r "^0.29.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0fsmk7ckaca9pvc4lrcgisgs3k4a5xs6kj1mbc0paydiyxdliphy")))

(define-public crate-swc_timer-0.16.0 (c (n "swc_timer") (v "0.16.0") (d (list (d (n "testing") (r "^0.30.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0fqvmrn9fh9yxpnbcx527p36xbaq6ifbx6ka122vz0hbp2gy823n")))

(define-public crate-swc_timer-0.16.1 (c (n "swc_timer") (v "0.16.1") (d (list (d (n "testing") (r "^0.30.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "13l9vw0dkkcma30s0gsf90k66zj56x321b3i8w6saq81ivgi4lkn")))

(define-public crate-swc_timer-0.16.2 (c (n "swc_timer") (v "0.16.2") (d (list (d (n "testing") (r "^0.30.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1h0czcxgz3zij7zwi5mrjx8nc0ngzw31z464358499mify3841yn")))

(define-public crate-swc_timer-0.16.3 (c (n "swc_timer") (v "0.16.3") (d (list (d (n "testing") (r "^0.30.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1d23w9wvwy7m365p8vi1gd7xg6ggncsdi0bw3b91x71b4by3c5ly")))

(define-public crate-swc_timer-0.16.4 (c (n "swc_timer") (v "0.16.4") (d (list (d (n "testing") (r "^0.30.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "04cd5gswy5qy1bdqflc3dvk5jq9mzbqxmfnwj4a2ljw5vj11v985")))

(define-public crate-swc_timer-0.16.5 (c (n "swc_timer") (v "0.16.5") (d (list (d (n "testing") (r "^0.30.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "09q99yy0mx4wh00h0vfkrgyshn8qqp7vwidy866yj24jpd3b1rfi")))

(define-public crate-swc_timer-0.16.6 (c (n "swc_timer") (v "0.16.6") (d (list (d (n "testing") (r "^0.30.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1xa56aqdgi6ixj9bfklg3fi8krki3m3wx1v3p16czf871i2nyzlv")))

(define-public crate-swc_timer-0.16.7 (c (n "swc_timer") (v "0.16.7") (d (list (d (n "testing") (r "^0.30.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0zf5qx0zi6mhwq1wlj1684jlysvn7zbrndicv49ybwhf39hpgg5m")))

(define-public crate-swc_timer-0.16.8 (c (n "swc_timer") (v "0.16.8") (d (list (d (n "testing") (r "^0.30.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0d9p3krrsfrglsb41yaddl5kv2gndqfifwlp2w89p25vgk4sm3mj")))

(define-public crate-swc_timer-0.16.9 (c (n "swc_timer") (v "0.16.9") (d (list (d (n "testing") (r "^0.30.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0yn65qlm8pcniz6ybp2ykq9yw6d1a8nmi6mypy8xdwld7iyvvvcv")))

(define-public crate-swc_timer-0.16.10 (c (n "swc_timer") (v "0.16.10") (d (list (d (n "testing") (r "^0.30.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0n3qhqbml47p4v3yidnrqahmngj9i5h77pkrsqss2dz41896wqly")))

(define-public crate-swc_timer-0.17.0 (c (n "swc_timer") (v "0.17.0") (d (list (d (n "testing") (r "^0.31.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1xa2wlrz2pqq36mdbhrwf620bfcyislwlwdsk4kr36yg05q4kz41")))

(define-public crate-swc_timer-0.17.1 (c (n "swc_timer") (v "0.17.1") (d (list (d (n "testing") (r "^0.31.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0gd74byhhv1vf93700hxklkdxnk4p6v86d2m6khx7g380b0w3kl1")))

(define-public crate-swc_timer-0.17.2 (c (n "swc_timer") (v "0.17.2") (d (list (d (n "testing") (r "^0.31.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "07bf16vjla0a2011fxlr5rlir6zigh3j7q7dglpxq8frzszpxfm0")))

(define-public crate-swc_timer-0.17.3 (c (n "swc_timer") (v "0.17.3") (d (list (d (n "testing") (r "^0.31.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1km9c4q2ich6nq2blwqg7n2nkwpvby2d9mg622mqxx3ylpsvf0wx")))

(define-public crate-swc_timer-0.17.4 (c (n "swc_timer") (v "0.17.4") (d (list (d (n "testing") (r "^0.31.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1scbivp8ahq0zjh75f87jcr80c2m2fs5i32j3hfl49azrdd1s3gr")))

(define-public crate-swc_timer-0.17.5 (c (n "swc_timer") (v "0.17.5") (d (list (d (n "testing") (r "^0.31.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1bflbnkwc8wynwbvi47nqyrqhd0l81cdxc9jvpsvw02zrr0rh824")))

(define-public crate-swc_timer-0.17.6 (c (n "swc_timer") (v "0.17.6") (d (list (d (n "testing") (r "^0.31.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "116x3s00y9zr4xp8l1wjyhgfy56a6xq9217zlzqpm3d7zg8yxsgb")))

(define-public crate-swc_timer-0.17.7 (c (n "swc_timer") (v "0.17.7") (d (list (d (n "testing") (r "^0.31.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0nvi3ayjcxaw4c12n5wxyvqlwgs3kr6hzdlxahbv9bwc795v39h8")))

(define-public crate-swc_timer-0.17.8 (c (n "swc_timer") (v "0.17.8") (d (list (d (n "testing") (r "^0.31.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0b4qra79diysa9qd1v6j9x7427qr64abm9s5lcm9fjqp6rzq3gjr")))

(define-public crate-swc_timer-0.17.9 (c (n "swc_timer") (v "0.17.9") (d (list (d (n "testing") (r "^0.31.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0kr6ksw4vp9rpyw9ma30cl9x16g44x2c0v7h166j6l9jq1svlfr4")))

(define-public crate-swc_timer-0.17.10 (c (n "swc_timer") (v "0.17.10") (d (list (d (n "testing") (r "^0.31.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1rgn5ac1sixjfzx2y1bzdis8s9k4wl08cr5md3si7ajjdpw2y6h0")))

(define-public crate-swc_timer-0.17.11 (c (n "swc_timer") (v "0.17.11") (d (list (d (n "testing") (r "^0.31.11") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "02dqsdxf4gzbdriqr0jb6gqwc6glxx5jkfmyz47rkvsn68fk0f1i")))

(define-public crate-swc_timer-0.17.12 (c (n "swc_timer") (v "0.17.12") (d (list (d (n "testing") (r "^0.31.12") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1gpfha17rz4z2nd3a7y5m734w0s34dqx0lcgv15yin18azkzh1ic")))

(define-public crate-swc_timer-0.17.13 (c (n "swc_timer") (v "0.17.13") (d (list (d (n "testing") (r "^0.31.13") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1p7h2jgvkab6l0i411mib3s3dny45rfba13ij6hlb12nr1ki21dv")))

(define-public crate-swc_timer-0.17.14 (c (n "swc_timer") (v "0.17.14") (d (list (d (n "testing") (r "^0.31.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0xixr1snass8m3rv2mabp3cpv9pbh9g3yjwam9g12k4xfdc5s01l")))

(define-public crate-swc_timer-0.17.15 (c (n "swc_timer") (v "0.17.15") (d (list (d (n "testing") (r "^0.31.15") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0sjf6yp17g8y55azqpjc3y2k33nr9bn009xchw3g9z13f0z7ilmm")))

(define-public crate-swc_timer-0.17.16 (c (n "swc_timer") (v "0.17.16") (d (list (d (n "testing") (r "^0.31.16") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "11czi74r4afbi7dyilwwyh2m6kk16m7c4vq2r3amilyzg63jzh4i")))

(define-public crate-swc_timer-0.17.17 (c (n "swc_timer") (v "0.17.17") (d (list (d (n "testing") (r "^0.31.17") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0wpivcah5x0ii4n9rnqvznhljkd4w4gzngi8jc72nvmqcf59g4gi")))

(define-public crate-swc_timer-0.17.18 (c (n "swc_timer") (v "0.17.18") (d (list (d (n "testing") (r "^0.31.18") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1xvbmhgagfs8j5kds82vp9y7avpi1xbydj9avm556c2gyp3mg01j")))

(define-public crate-swc_timer-0.17.19 (c (n "swc_timer") (v "0.17.19") (d (list (d (n "testing") (r "^0.31.19") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0k28sz6g6r2j88v9ivygqfyc4d49ld0irdhvb5pf6vg299nyj9pf")))

(define-public crate-swc_timer-0.17.20 (c (n "swc_timer") (v "0.17.20") (d (list (d (n "testing") (r "^0.31.20") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0kh6lqz7gnmasadizahdhj7crs621n1jjdqrkgv997yg21fnhxlc")))

(define-public crate-swc_timer-0.17.21 (c (n "swc_timer") (v "0.17.21") (d (list (d (n "testing") (r "^0.31.21") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1svdvrdbbvl5hmb4qc02qhy56b8xdkfzd1dan9d8rr6riagzwn1b")))

(define-public crate-swc_timer-0.17.22 (c (n "swc_timer") (v "0.17.22") (d (list (d (n "testing") (r "^0.31.22") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "11yl22szb3zbwqv23yxmzqvpkfj82vi41zcj49y8gg6blr6hda6z")))

(define-public crate-swc_timer-0.17.23 (c (n "swc_timer") (v "0.17.23") (d (list (d (n "testing") (r "^0.31.23") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1c2z7pm352n8f5d9pakhz80nc3phygmr1qllkxggihpfra1l53qs")))

(define-public crate-swc_timer-0.17.24 (c (n "swc_timer") (v "0.17.24") (d (list (d (n "testing") (r "^0.31.24") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "13gral9s2mvqvjjjqk8ybqhz4w4r1ysi10rbm5irmgyx6py1bibd")))

(define-public crate-swc_timer-0.17.25 (c (n "swc_timer") (v "0.17.25") (d (list (d (n "testing") (r "^0.31.25") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0km75vmi72hi1ah7ml48qv90vjfw04pr352nkbssw9vsi9nj5i6f")))

(define-public crate-swc_timer-0.17.26 (c (n "swc_timer") (v "0.17.26") (d (list (d (n "testing") (r "^0.31.26") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1k2rxx25sdm823nip54wxsyyyf4jg9wja5hqpw2bclhfhygpind4")))

(define-public crate-swc_timer-0.17.27 (c (n "swc_timer") (v "0.17.27") (d (list (d (n "testing") (r "^0.31.27") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "05vxqfiy7kzfwvildp4b43lf1xr08ybj9jq4l3ndy3na5n18n2qm")))

(define-public crate-swc_timer-0.17.28 (c (n "swc_timer") (v "0.17.28") (d (list (d (n "testing") (r "^0.31.28") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0jq2q65sr5xp28lrs87rz2k8ln9z1a36gh4wd415m63c028gpxbj")))

(define-public crate-swc_timer-0.17.29 (c (n "swc_timer") (v "0.17.29") (d (list (d (n "testing") (r "^0.31.29") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0jspf0r384gv0lq77c653mbhpgvyxxhgb6w0c3y0168dl48pwphy")))

(define-public crate-swc_timer-0.17.30 (c (n "swc_timer") (v "0.17.30") (d (list (d (n "testing") (r "^0.31.30") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "02sml5xpqqqa80yfs7mcfjvcml38i9y866m0xwwx4c4fn7m72mgv")))

(define-public crate-swc_timer-0.17.31 (c (n "swc_timer") (v "0.17.31") (d (list (d (n "testing") (r "^0.31.31") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0vwvf84v85220qaswg734jpibw4k7pwanwcjafbzav110lvfisw2")))

(define-public crate-swc_timer-0.17.32 (c (n "swc_timer") (v "0.17.32") (d (list (d (n "testing") (r "^0.31.32") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "07b95ynr4ccrd1sbyydwqx96xc5fpxj3minjbp5cya8y67j5j3xr")))

(define-public crate-swc_timer-0.17.33 (c (n "swc_timer") (v "0.17.33") (d (list (d (n "testing") (r "^0.31.33") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0fkrxa6gf7k1acywvg3606jfjyz3n99lk638jbqq5pzkajjc5zqk")))

(define-public crate-swc_timer-0.17.34 (c (n "swc_timer") (v "0.17.34") (d (list (d (n "testing") (r "^0.31.34") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0i04ylybql0iddgn5bnqk1w261dljilprca2syyskgvkjvimg9vs")))

(define-public crate-swc_timer-0.17.35 (c (n "swc_timer") (v "0.17.35") (d (list (d (n "testing") (r "^0.31.35") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1i61m5bb3pal8ba2i2wvxw4rzrdhk25lk19jdhskdc4b8ii991d8")))

(define-public crate-swc_timer-0.17.36 (c (n "swc_timer") (v "0.17.36") (d (list (d (n "testing") (r "^0.31.36") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "079m6zqplkcnig53z5i315sdi6ga86ir6cacsgig7az8r4bjjvgr")))

(define-public crate-swc_timer-0.17.37 (c (n "swc_timer") (v "0.17.37") (d (list (d (n "testing") (r "^0.31.37") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1h6qd3sx30ma04jqic3c26a0xlp5y6yba827lzxwpyy4xg48zh9g")))

(define-public crate-swc_timer-0.17.38 (c (n "swc_timer") (v "0.17.38") (d (list (d (n "testing") (r "^0.31.38") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0vxchgdxcdw0wmzkh208xx0zy1z5b08fjmvrl4hz24k63nj4s8ba")))

(define-public crate-swc_timer-0.17.39 (c (n "swc_timer") (v "0.17.39") (d (list (d (n "testing") (r "^0.31.39") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "02rppy5xac5q8lp0vzriymdi5fhl0bsjpssvar5gh0vc40sr2jd9")))

(define-public crate-swc_timer-0.17.40 (c (n "swc_timer") (v "0.17.40") (d (list (d (n "testing") (r "^0.31.40") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "17zzcy61nynas5irrjk3i6wsx721zk0jy1dzi4fmgjls1ar7vgzq")))

(define-public crate-swc_timer-0.17.41 (c (n "swc_timer") (v "0.17.41") (d (list (d (n "testing") (r "^0.31.41") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1x9f24cidy9jw66r8zrmkqj94182s4sgkal2r30dlqjrfsckxpva")))

(define-public crate-swc_timer-0.17.42 (c (n "swc_timer") (v "0.17.42") (d (list (d (n "testing") (r "^0.31.42") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "02zslpqvc0snqnsf5zimami8ybwzwjry2w8j0qjlgckkg3dgl6ni")))

(define-public crate-swc_timer-0.17.43 (c (n "swc_timer") (v "0.17.43") (d (list (d (n "testing") (r "^0.31.43") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "09jva6dw9a3w477s305wyrs6py3r0ry95dly0dq1rxrb6nzsjlx6")))

(define-public crate-swc_timer-0.18.0 (c (n "swc_timer") (v "0.18.0") (d (list (d (n "testing") (r "^0.32.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0mlnm9hr48c58ijvyc1kcizwy36lnh9kihwfri3czfk1wnwdaymc")))

(define-public crate-swc_timer-0.18.1 (c (n "swc_timer") (v "0.18.1") (d (list (d (n "testing") (r "^0.32.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0bs3sdcllwwivrfb823ib7jcp2s171b4949i22dgdh76mqdk7jhh")))

(define-public crate-swc_timer-0.18.2 (c (n "swc_timer") (v "0.18.2") (d (list (d (n "testing") (r "^0.32.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1dpxpycindg25m93ffv9b5xyz487cgsmjkl1zmd73jq0sgfb6bn2")))

(define-public crate-swc_timer-0.18.3 (c (n "swc_timer") (v "0.18.3") (d (list (d (n "testing") (r "^0.32.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "13dnvcia1v4hxfp8gpdahshpwmf4p6h605y108zmhv7mdl4c3wv1")))

(define-public crate-swc_timer-0.18.4 (c (n "swc_timer") (v "0.18.4") (d (list (d (n "testing") (r "^0.32.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1qx6iv50ip2dhsa92awsp2j45cwqdp4bllldhz98hpbw62y7prql")))

(define-public crate-swc_timer-0.18.5 (c (n "swc_timer") (v "0.18.5") (d (list (d (n "testing") (r "^0.32.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1z60n49a7cfiyy9dbf9s55wb5bib1vbxds9ld5r2kk38w6acjdfz")))

(define-public crate-swc_timer-0.19.0 (c (n "swc_timer") (v "0.19.0") (d (list (d (n "testing") (r "^0.33.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0152r9s6x4pcxlklk0rc2dyy63nf2vsxjqikb9wxl0739jpz3fim")))

(define-public crate-swc_timer-0.19.1 (c (n "swc_timer") (v "0.19.1") (d (list (d (n "testing") (r "^0.33.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1vlhl5lfxvd6s22aqgdgz7dp6fixamfdi5zfpydzhh9bvd4w6q52")))

(define-public crate-swc_timer-0.19.2 (c (n "swc_timer") (v "0.19.2") (d (list (d (n "testing") (r "^0.33.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0700cv1b0dmkkq2f8iq7ys6vlga9pw1kwnbfh9xbhmv9xwdk9r4g")))

(define-public crate-swc_timer-0.19.3 (c (n "swc_timer") (v "0.19.3") (d (list (d (n "testing") (r "^0.33.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "01rqnk5iijzc2yi2c9x629q6az1ghkbrkdm3fc8w5677srr4akak")))

(define-public crate-swc_timer-0.19.4 (c (n "swc_timer") (v "0.19.4") (d (list (d (n "testing") (r "^0.33.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0hg0f7nafrm8nwjmp4nqg7qjb6vbv6zlgp5a5cp5nrb8pgxmfgxx")))

(define-public crate-swc_timer-0.19.5 (c (n "swc_timer") (v "0.19.5") (d (list (d (n "testing") (r "^0.33.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1lzzc3l0mbcc5pag0q59jjhlh42drsc8nlnafhw3yg2zqi0kp5j4")))

(define-public crate-swc_timer-0.19.6 (c (n "swc_timer") (v "0.19.6") (d (list (d (n "testing") (r "^0.33.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1dvcy8wh4v4nkwydd10cjh9vy3fbbczpmvm5gmwv04062ir5clkx")))

(define-public crate-swc_timer-0.19.7 (c (n "swc_timer") (v "0.19.7") (d (list (d (n "testing") (r "^0.33.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "11a2l17pbd1hz0c4laql9hdsq59vdh27k423yi87wgpgw47jrsxp")))

(define-public crate-swc_timer-0.19.8 (c (n "swc_timer") (v "0.19.8") (d (list (d (n "testing") (r "^0.33.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "14wl71gsc40jma1m0h0yjfafhc1w4v1dlypcfzsm80rgv9xv0nnv")))

(define-public crate-swc_timer-0.19.9 (c (n "swc_timer") (v "0.19.9") (d (list (d (n "testing") (r "^0.33.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0kyiq3vb99qq1b61zlcqw0m2a8446xqkj3zk4gs9n2q8q43npi7c")))

(define-public crate-swc_timer-0.19.10 (c (n "swc_timer") (v "0.19.10") (d (list (d (n "testing") (r "^0.33.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "197shv80pbfm7p9xvrah9d20h4sb5rann4x0ph79c70wcw9x2m25")))

(define-public crate-swc_timer-0.19.11 (c (n "swc_timer") (v "0.19.11") (d (list (d (n "testing") (r "^0.33.11") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1h77grfkd5pq2qkvd9xx38ylw71mqgqb0i2ff4q0ym2wksl8cksy")))

(define-public crate-swc_timer-0.19.12 (c (n "swc_timer") (v "0.19.12") (d (list (d (n "testing") (r "^0.33.12") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0f057vjdmg0c9azqf1rkqiga0zflvnqy253hmakwd6p6w3zx9sji")))

(define-public crate-swc_timer-0.19.13 (c (n "swc_timer") (v "0.19.13") (d (list (d (n "testing") (r "^0.33.13") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "14x861xgx79857wyqfg4agkxpcnwk8pk23rqdynbz4gwg0qnkpfh")))

(define-public crate-swc_timer-0.19.14 (c (n "swc_timer") (v "0.19.14") (d (list (d (n "testing") (r "^0.33.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "17ybbncwpwkbgfxi4cgcmxqg15jfnxs250j606wc8diixyx6csb2")))

(define-public crate-swc_timer-0.19.15 (c (n "swc_timer") (v "0.19.15") (d (list (d (n "testing") (r "^0.33.15") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "070b12qls3i4zn4lccp6l677r1kizpkd94c8a1jl1sydsiih7xhg")))

(define-public crate-swc_timer-0.19.16 (c (n "swc_timer") (v "0.19.16") (d (list (d (n "testing") (r "^0.33.16") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "02dg8zi4qlx8wja0wc355b66zb8bh8nwhsahjfbcyy6s7gj4qa0m")))

(define-public crate-swc_timer-0.19.17 (c (n "swc_timer") (v "0.19.17") (d (list (d (n "testing") (r "^0.33.17") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "00iim73ppnv2cvffm1a1j11fiwz4lk4jd8r6blifawxp8cv98hbs")))

(define-public crate-swc_timer-0.19.18 (c (n "swc_timer") (v "0.19.18") (d (list (d (n "testing") (r "^0.33.18") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "02bh75br33011ll9pnydbadx8h9b0rk1axydvnz87v5f3i1n3lfx")))

(define-public crate-swc_timer-0.19.19 (c (n "swc_timer") (v "0.19.19") (d (list (d (n "testing") (r "^0.33.19") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0vr3635q6azn8rinrpd4h29lwrwf4g8vhgil1wzf32cjj14bz2cq")))

(define-public crate-swc_timer-0.19.20 (c (n "swc_timer") (v "0.19.20") (d (list (d (n "testing") (r "^0.33.20") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1y5ld432i4nsb97x715x8zgszw20v5jz1cv7vq9dvnrfim5k7qg6")))

(define-public crate-swc_timer-0.19.21 (c (n "swc_timer") (v "0.19.21") (d (list (d (n "testing") (r "^0.33.21") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1prksxcq58j29gbwbwdikgd4dd049n8rmlxz5yx7z0axlr2cjq25")))

(define-public crate-swc_timer-0.19.22 (c (n "swc_timer") (v "0.19.22") (d (list (d (n "testing") (r "^0.33.22") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0gj4767s4vnf31h4c1r64h4j19zh5pjgdvrv0sxv08fxq7xg40j5")))

(define-public crate-swc_timer-0.19.23 (c (n "swc_timer") (v "0.19.23") (d (list (d (n "testing") (r "^0.33.23") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0yi787jlfzdrq5325j985r45y1frnp8408vz3j860wlb50ahn609")))

(define-public crate-swc_timer-0.19.24 (c (n "swc_timer") (v "0.19.24") (d (list (d (n "testing") (r "^0.33.24") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "10azv82rsrzcxrz6l6awsm74k0kh29v2syi540893lxcwyxrnxrm")))

(define-public crate-swc_timer-0.19.25 (c (n "swc_timer") (v "0.19.25") (d (list (d (n "testing") (r "^0.33.25") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1f2p94d3xk638kymn2ji0r75r482xcir34yizmwyiisinlnmij0w")))

(define-public crate-swc_timer-0.20.0 (c (n "swc_timer") (v "0.20.0") (d (list (d (n "testing") (r "^0.34.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1zxyx0nwq0dj5cvfc39c2abm67i9l1n3sgaqxxfiq7c1bik4iicm")))

(define-public crate-swc_timer-0.20.1 (c (n "swc_timer") (v "0.20.1") (d (list (d (n "testing") (r "^0.34.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0371nhc5h6vf7cppqhz8xw6zwr67j97lzp18pmv43l1f81mwwh5p")))

(define-public crate-swc_timer-0.20.2 (c (n "swc_timer") (v "0.20.2") (d (list (d (n "testing") (r "^0.34.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1zihs97zw9n1dm5wskmkhypi5jgasiif3f177r56hkv7jays8h8q")))

(define-public crate-swc_timer-0.21.0 (c (n "swc_timer") (v "0.21.0") (d (list (d (n "testing") (r "^0.35.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1i79033pnjmqhg2124x78p1bn07vbywrgdznlsncqq0pz58f39kp")))

(define-public crate-swc_timer-0.21.1 (c (n "swc_timer") (v "0.21.1") (d (list (d (n "testing") (r "^0.35.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0dk8bvhfqhilwanq9v83ndlv53skyc8x0dc648nqadj1qjai06v1")))

(define-public crate-swc_timer-0.21.2 (c (n "swc_timer") (v "0.21.2") (d (list (d (n "testing") (r "^0.35.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0s8a4hr1xpmr0agw3bjff1a63dy38nwaw9hs4w27y9nyra4j8s5c")))

(define-public crate-swc_timer-0.21.3 (c (n "swc_timer") (v "0.21.3") (d (list (d (n "testing") (r "^0.35.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "00cphg3c4rvbdk5f395xzbr1gbi14a97inwc5ywqwrqdqpivi03j")))

(define-public crate-swc_timer-0.21.4 (c (n "swc_timer") (v "0.21.4") (d (list (d (n "testing") (r "^0.35.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0l1wf3r629p2y42gngdsfnmsw3v3fy7jfmx68ckxkaxpi529f5r8")))

(define-public crate-swc_timer-0.21.5 (c (n "swc_timer") (v "0.21.5") (d (list (d (n "testing") (r "^0.35.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1l1zanlfr9plfzjadcc1w2ym3fvbbzifbmp8hhqbzkkc4zl41c44")))

(define-public crate-swc_timer-0.21.6 (c (n "swc_timer") (v "0.21.6") (d (list (d (n "testing") (r "^0.35.6") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ydca1drbghkpjg3jxwv22cf4vf1zpb8jk3b1i5s6ri2ap4l8cda")))

(define-public crate-swc_timer-0.21.7 (c (n "swc_timer") (v "0.21.7") (d (list (d (n "testing") (r "^0.35.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0552vgizq2v5w6q4njcwjgq2g2dhw367cjb9xv1kp6p2phrw747c")))

(define-public crate-swc_timer-0.21.8 (c (n "swc_timer") (v "0.21.8") (d (list (d (n "testing") (r "^0.35.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1gsn2jz3l24ir81mcrkayrbpx035fq2fd45fh26xp97c0ppgsrh5")))

(define-public crate-swc_timer-0.21.9 (c (n "swc_timer") (v "0.21.9") (d (list (d (n "testing") (r "^0.35.9") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1bn2ma0qwcmnhpgqnn2cg520pmgrcsnzc41c3x48apdr28w6h6ha")))

(define-public crate-swc_timer-0.21.10 (c (n "swc_timer") (v "0.21.10") (d (list (d (n "testing") (r "^0.35.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1vdk33nx9s5rvgygpdq35hfa03xq6hhs6l4jysjwj1yrcy59v39q")))

(define-public crate-swc_timer-0.21.11 (c (n "swc_timer") (v "0.21.11") (d (list (d (n "testing") (r "^0.35.11") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1xxzldhfi844zaq05hplfxikfwp9q0p38qm5a97zg5n2yd1h482s")))

(define-public crate-swc_timer-0.21.12 (c (n "swc_timer") (v "0.21.12") (d (list (d (n "testing") (r "^0.35.12") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1d7vs0h0vrsi6spi3zhmyyd1gj19ad0safbpmndbrdsacr01xxqk")))

(define-public crate-swc_timer-0.21.13 (c (n "swc_timer") (v "0.21.13") (d (list (d (n "testing") (r "^0.35.13") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1nb4r8svyk8maa7hp68yz5f9j74q14j3yfpyg3mqjb0hnydnmjhw")))

(define-public crate-swc_timer-0.21.14 (c (n "swc_timer") (v "0.21.14") (d (list (d (n "testing") (r "^0.35.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ip0297w169dx4cr5qfjwps3lgs7wjw21w0yw74l3lklb3d10w5k")))

(define-public crate-swc_timer-0.21.15 (c (n "swc_timer") (v "0.21.15") (d (list (d (n "testing") (r "^0.35.15") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0idia62xiyrx8a4xd0q4v39bhkdhrb90msr4hy7sgc67bf3mx85d")))

(define-public crate-swc_timer-0.21.16 (c (n "swc_timer") (v "0.21.16") (d (list (d (n "testing") (r "^0.35.16") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0xh2zy9qy4xslsp2ilylipj477vc6sg0dbvklflhns92radyq5cx")))

(define-public crate-swc_timer-0.21.17 (c (n "swc_timer") (v "0.21.17") (d (list (d (n "testing") (r "^0.35.17") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0wwl8n01j1acd8g8wvhfbffpdsdxqyzmxmnlwr20piv0216pally")))

(define-public crate-swc_timer-0.21.18 (c (n "swc_timer") (v "0.21.18") (d (list (d (n "testing") (r "^0.35.18") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ddidynmy0gnzzh5igxpgb56lss7q69psmv5w7nnwrjipf5dyiqj")))

(define-public crate-swc_timer-0.21.19 (c (n "swc_timer") (v "0.21.19") (d (list (d (n "testing") (r "^0.35.19") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "09ihps0a4d82r4n3ni88akwkgfcq2mrl3wd76sbrq15ssl400wqv")))

(define-public crate-swc_timer-0.21.20 (c (n "swc_timer") (v "0.21.20") (d (list (d (n "testing") (r "^0.35.20") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1vndk4h4kkf9shkiyc6w2rxix0qybgj88glw5q5m9sl78cx072dm")))

(define-public crate-swc_timer-0.21.21 (c (n "swc_timer") (v "0.21.21") (d (list (d (n "testing") (r "^0.35.21") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1sci93hfjy8kwc0sih3zhl353was3lh9dmvk2015mdyi7wvy0p77")))

(define-public crate-swc_timer-0.21.22 (c (n "swc_timer") (v "0.21.22") (d (list (d (n "testing") (r "^0.35.24") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0kshhmwyvsid6qpm83m95246wjlnnmbly03kcbw2i8f7xhxc218c")))

