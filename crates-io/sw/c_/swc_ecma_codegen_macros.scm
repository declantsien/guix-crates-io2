(define-module (crates-io sw c_ swc_ecma_codegen_macros) #:use-module (crates-io))

(define-public crate-swc_ecma_codegen_macros-0.1.0 (c (n "swc_ecma_codegen_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("fold"))) (d #t) (k 0)))) (h "0avbs45phz9kl8sp3h3lxpqbpjq3z68fljszagg8aw6mlvxypn8j")))

(define-public crate-swc_ecma_codegen_macros-0.2.0 (c (n "swc_ecma_codegen_macros") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("fold"))) (d #t) (k 0)))) (h "05vmpdk2lfr5lvwv0smaiqg1zlxmbiiifyg0lzn2izqj4szwczz6")))

(define-public crate-swc_ecma_codegen_macros-0.3.0 (c (n "swc_ecma_codegen_macros") (v "0.3.0") (d (list (d (n "pmutil") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("fold"))) (d #t) (k 0)))) (h "1nnf508b3k02j6zixl80z1yw95lg7395hrgqc9hx5mfm0w15sqg5")))

(define-public crate-swc_ecma_codegen_macros-0.4.0 (c (n "swc_ecma_codegen_macros") (v "0.4.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0sp7sz7cn55a9z3gfnybc8a9mh0nf348ikzz23bj47sqj7jnyjfn")))

(define-public crate-swc_ecma_codegen_macros-0.5.0 (c (n "swc_ecma_codegen_macros") (v "0.5.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "06w5m65g5mv29pa54ly2lf5byvzgdc4jxsisvmqsz5hjzm1q2dq4")))

(define-public crate-swc_ecma_codegen_macros-0.5.1 (c (n "swc_ecma_codegen_macros") (v "0.5.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "11jn5gvbhay6avi76lly7lxzy5995qs3xbxd5v9dw9pm7s72a1b5")))

(define-public crate-swc_ecma_codegen_macros-0.5.2 (c (n "swc_ecma_codegen_macros") (v "0.5.2") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "1lr0mp1wfacpj6h153ki9cca62afqh3a4pxiwa4aaknb4s043bsi")))

(define-public crate-swc_ecma_codegen_macros-0.6.0 (c (n "swc_ecma_codegen_macros") (v "0.6.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "098fiynpyfhjwwq7aa5i2lffm1km3byq6f62nfrwv0cjfdn85gxx")))

(define-public crate-swc_ecma_codegen_macros-0.7.0 (c (n "swc_ecma_codegen_macros") (v "0.7.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0hlhp5dy1ybhnqxi1cvla25ngg67qgr0b79rdkdywiggn8crd52r")))

(define-public crate-swc_ecma_codegen_macros-0.7.1 (c (n "swc_ecma_codegen_macros") (v "0.7.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "1sisnbhzw7qbpqlnck5i0qq5vd4h14dszxrfd7z4hbpmh6gwjn81")))

(define-public crate-swc_ecma_codegen_macros-0.7.2 (c (n "swc_ecma_codegen_macros") (v "0.7.2") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "00a9np25z3r394fa36mi0kax7zvsrf899l7cjj6q060hxv5f0kmz")))

(define-public crate-swc_ecma_codegen_macros-0.7.3 (c (n "swc_ecma_codegen_macros") (v "0.7.3") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1123h6258xcyr67gjvdwh91h2rmhry52qahb1rmcr9ncvivg1pyw")))

(define-public crate-swc_ecma_codegen_macros-0.7.4 (c (n "swc_ecma_codegen_macros") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "19fa3mwv484krd9m0d7ndg74y96hdrr1isrc0499lcsb88wq4jrr")))

(define-public crate-swc_ecma_codegen_macros-0.7.5 (c (n "swc_ecma_codegen_macros") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0bjq8dyd8jcld5v3qfay99aw6njrpayqqjmbjk9yy1dfh6x8gaqp")))

(define-public crate-swc_ecma_codegen_macros-0.7.6 (c (n "swc_ecma_codegen_macros") (v "0.7.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1j295c3jyd5dq9ikxsq74acdlknx17nv3amk2cy1m3cwyjd403h9")))

