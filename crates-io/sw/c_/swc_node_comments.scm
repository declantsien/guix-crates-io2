(define-module (crates-io sw c_ swc_node_comments) #:use-module (crates-io))

(define-public crate-swc_node_comments-0.1.0 (c (n "swc_node_comments") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "swc_common") (r "^0.14.6") (d #t) (k 0)))) (h "1zyk60vviiwfzsxz0mnrz7fl2cg1c3scmcb6k5ixavqyzxp1izaj")))

(define-public crate-swc_node_comments-0.2.0 (c (n "swc_node_comments") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "swc_common") (r "^0.15.0") (d #t) (k 0)))) (h "1spcq1lxmb7nfjncpbdhwmvx1jch5ybkw2jvpfjplsx2m5w5qg21")))

(define-public crate-swc_node_comments-0.3.0 (c (n "swc_node_comments") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "swc_common") (r "^0.16.0") (d #t) (k 0)))) (h "1b9wrm5nlg98jzyb4y8ymrq93j0p243h9i4k4cshidp8znhvy9nm")))

(define-public crate-swc_node_comments-0.4.0 (c (n "swc_node_comments") (v "0.4.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.0") (d #t) (k 0)))) (h "1rnpxkjq17qgpfz77a79j36w321f188j6g74k9hmpa6aixr41f9c")))

(define-public crate-swc_node_comments-0.5.0 (c (n "swc_node_comments") (v "0.5.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.18.0") (d #t) (k 0)))) (h "14899smy83qh4micy87hwalnn5ani72hglprsrw7rg7g0ns40hwb")))

(define-public crate-swc_node_comments-0.6.0 (c (n "swc_node_comments") (v "0.6.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.19.0") (d #t) (k 0)))) (h "04hqf05lm6wbdspw9p6lhy7d59rx3z8811n058db4j80x6lb94hh")))

(define-public crate-swc_node_comments-0.7.0 (c (n "swc_node_comments") (v "0.7.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.20.0") (d #t) (k 0)))) (h "0saqmdx6vyrjjh54f2q1af7y980vqfp5d7zilq2xj5ca29h099xg")))

(define-public crate-swc_node_comments-0.8.0 (c (n "swc_node_comments") (v "0.8.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.21.0") (d #t) (k 0)))) (h "0zwz7ldwnd5j2jv1mkflm9q5vg40l8nlxpic56gwh2zdmj6dgy2d")))

(define-public crate-swc_node_comments-0.9.0 (c (n "swc_node_comments") (v "0.9.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.22.0") (d #t) (k 0)))) (h "014d47sfhkmdv1j1qv8n4h71a731shv7rwhlj2dfsm9wxsascbac")))

(define-public crate-swc_node_comments-0.10.0 (c (n "swc_node_comments") (v "0.10.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.23.0") (d #t) (k 0)))) (h "0zddibvffgk7bsqgcxpyh561g4lj1cckclwvwkp67y71daglvvl6")))

(define-public crate-swc_node_comments-0.11.0 (c (n "swc_node_comments") (v "0.11.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.24.0") (d #t) (k 0)))) (h "13i8xzagi0pr058jhma1a86jxvv80njy81xb8bxlwdn8vcgkm6fl")))

(define-public crate-swc_node_comments-0.12.0 (c (n "swc_node_comments") (v "0.12.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.3.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.25.0") (d #t) (k 0)))) (h "0km321a912p88rxzfayaa39zk9rhi10829pmyzfyngsvn8b9afar")))

(define-public crate-swc_node_comments-0.13.0 (c (n "swc_node_comments") (v "0.13.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.3.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.26.0") (d #t) (k 0)))) (h "1r79ymc4jg7y5vq43m3bzz9n4qr9j381jlvjwgs1c231nmv4am35")))

(define-public crate-swc_node_comments-0.14.0 (c (n "swc_node_comments") (v "0.14.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.0") (d #t) (k 0)))) (h "0f9fl7spv6kzr0gk5diw7db82ri9h28gi5zv9irndhdkw9i4vzcq")))

(define-public crate-swc_node_comments-0.14.1 (c (n "swc_node_comments") (v "0.14.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.3") (d #t) (k 0)))) (h "1xgysx0f86nx1193jrxvp1a3r2gchxczj7lfk1q22ldish7k8acb")))

(define-public crate-swc_node_comments-0.14.2 (c (n "swc_node_comments") (v "0.14.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.4") (d #t) (k 0)))) (h "1d8crcrnyg60ibsw0779fx2nn731inq2rc17zrlwpn1qny8m54nj")))

(define-public crate-swc_node_comments-0.14.3 (c (n "swc_node_comments") (v "0.14.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.5") (d #t) (k 0)))) (h "0ispky4mh5ipjlz731z592zpfrkx0cbr3l5yhcaig9mrhvqwqbqj")))

(define-public crate-swc_node_comments-0.14.4 (c (n "swc_node_comments") (v "0.14.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.6") (d #t) (k 0)))) (h "1kjcl59q7z9acwk9x7s2jy3hn9mxs34i0c833wbv048a08lqv6l0")))

(define-public crate-swc_node_comments-0.14.5 (c (n "swc_node_comments") (v "0.14.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.14") (d #t) (k 0)))) (h "00dlzfz61zs2n2mxfpk0svqbrgc7ivmpci3r1bjy2qik6rh82zh7")))

(define-public crate-swc_node_comments-0.14.6 (c (n "swc_node_comments") (v "0.14.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.15") (d #t) (k 0)))) (h "1nmlv4b225ibmmiavyl7h8np25rvdymnkiaw7rzqfi383xmr2djd")))

(define-public crate-swc_node_comments-0.14.7 (c (n "swc_node_comments") (v "0.14.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.10") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.16") (d #t) (k 0)))) (h "0kkgdmqknh3gij4svixwj5ji2hpiqi3cgyg0vyl33nmspfxbiwww")))

(define-public crate-swc_node_comments-0.15.0 (c (n "swc_node_comments") (v "0.15.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.11") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.0") (d #t) (k 0)))) (h "00dwfi2cb07zl018giikqnnbpf7sim1jyv0xc5daznri2dgp4gpv")))

(define-public crate-swc_node_comments-0.15.1 (c (n "swc_node_comments") (v "0.15.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.12") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.1") (d #t) (k 0)))) (h "191swngj4ryqpb4hiamz47fb9827699as1wa8x1s9qr8n8xazq3p")))

(define-public crate-swc_node_comments-0.15.2 (c (n "swc_node_comments") (v "0.15.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.13") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.2") (d #t) (k 0)))) (h "1j09m0zxqxh46nb6zivz423rkxpfhd2rccxi8cnpamq500kbcfrw")))

(define-public crate-swc_node_comments-0.15.3 (c (n "swc_node_comments") (v "0.15.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.3") (d #t) (k 0)))) (h "0b1r14cjkd7mbra3s4lcnbypssidw7kbf46m32hfvizm8q4as4pq")))

(define-public crate-swc_node_comments-0.15.4 (c (n "swc_node_comments") (v "0.15.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.4") (d #t) (k 0)))) (h "18azbnnx1zsfs6c0dffzp9ns50kyb417kxz987jj0yg7adl8ibig")))

(define-public crate-swc_node_comments-0.15.5 (c (n "swc_node_comments") (v "0.15.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.5") (d #t) (k 0)))) (h "0xbyl2qwv5zm523705lzjh321p009pwb5kazmlp85ip0ygnhdg78")))

(define-public crate-swc_node_comments-0.15.6 (c (n "swc_node_comments") (v "0.15.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.6") (d #t) (k 0)))) (h "0fi4n8lrnkm78d5qmvd8bn909hpxkmvd2f3q6lkicrjvccd0cmqr")))

(define-public crate-swc_node_comments-0.15.7 (c (n "swc_node_comments") (v "0.15.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.14") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.7") (d #t) (k 0)))) (h "0byfkrk5hkqnr5s26m4jfh2ixw8i9v34jp1p55pi08q44j6mx8m6")))

(define-public crate-swc_node_comments-0.15.8 (c (n "swc_node_comments") (v "0.15.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.15") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.8") (d #t) (k 0)))) (h "0944p23v5s6acaxwb6mlmqv7sbkng9nfmyz347a2ram6xh8apisn")))

(define-public crate-swc_node_comments-0.15.9 (c (n "swc_node_comments") (v "0.15.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.16") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.9") (d #t) (k 0)))) (h "12shy445qdxva06plm21rqr3zyjqmvqf29pkzrsfk6mn6d0xzd4m")))

(define-public crate-swc_node_comments-0.15.10 (c (n "swc_node_comments") (v "0.15.10") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.10") (d #t) (k 0)))) (h "1fbc8yx4qaasfdik2af1v9cz92x1skigwkypiw8yhnarwcay9iq3")))

(define-public crate-swc_node_comments-0.16.0 (c (n "swc_node_comments") (v "0.16.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.0") (d #t) (k 0)))) (h "1jfshsndvibvrriag23bhq5ad3knpfn86dqdjpnflpbi1ii64bda")))

(define-public crate-swc_node_comments-0.16.1 (c (n "swc_node_comments") (v "0.16.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.1") (d #t) (k 0)))) (h "0wh7jb4i3k6n1cg9l9pdw03qpn5r3i595cs4aqsw0237b4r91s78")))

(define-public crate-swc_node_comments-0.16.2 (c (n "swc_node_comments") (v "0.16.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.17") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.2") (d #t) (k 0)))) (h "0z0m830hyaccdckf57h83hh34g2g3pipxm1xcf6wpi0fnj6ayhyh")))

(define-public crate-swc_node_comments-0.16.3 (c (n "swc_node_comments") (v "0.16.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.18") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.3") (d #t) (k 0)))) (h "05yyz33h9la3lm76nsfwf0hqjq7b70ymd0r1m1q24sdi1wjyfajl")))

(define-public crate-swc_node_comments-0.16.4 (c (n "swc_node_comments") (v "0.16.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.18") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.4") (d #t) (k 0)))) (h "0qbbkshivffiwl40b3gycpvmsz1ds1n00plhcm284x811w7405wd")))

(define-public crate-swc_node_comments-0.16.5 (c (n "swc_node_comments") (v "0.16.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.18") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.5") (d #t) (k 0)))) (h "1by8mrdwqdq43j33684h02myl367pdm7y3x2g2dwd5bcxscarw51")))

(define-public crate-swc_node_comments-0.16.6 (c (n "swc_node_comments") (v "0.16.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.19") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.6") (d #t) (k 0)))) (h "1y29y3h4zpfda9j6kibc5p41yrmx6ldf5hb2pi6776p07cla4msg")))

(define-public crate-swc_node_comments-0.16.7 (c (n "swc_node_comments") (v "0.16.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.20") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.7") (d #t) (k 0)))) (h "08pgzfmjn40yb33gf27037rarwbf3wq4wy4znwpyd0jjyg80ajih")))

(define-public crate-swc_node_comments-0.16.8 (c (n "swc_node_comments") (v "0.16.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.21") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.8") (d #t) (k 0)))) (h "1x2dpg0plfg9nd7r3s94fpa11vfxp3ml8zxhm04gsj9a6xrq6vzn")))

(define-public crate-swc_node_comments-0.16.9 (c (n "swc_node_comments") (v "0.16.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.22") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.9") (d #t) (k 0)))) (h "1fw4g701ibdirvmb19374gz6b9s4r27gbzxmd7ljdhyra61rf1wc")))

(define-public crate-swc_node_comments-0.16.10 (c (n "swc_node_comments") (v "0.16.10") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.23") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.10") (d #t) (k 0)))) (h "1zfc9xhxzcv46dyxrgz5xn6y38glzmxk8far63kq8qgj7v9ndv3g")))

(define-public crate-swc_node_comments-0.16.11 (c (n "swc_node_comments") (v "0.16.11") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.11") (d #t) (k 0)))) (h "1q50r47ya5kw22ig07v9bnv2irhhmyvr3f3xx9341r6fkngi76lf")))

(define-public crate-swc_node_comments-0.16.12 (c (n "swc_node_comments") (v "0.16.12") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.12") (d #t) (k 0)))) (h "0hnsipxqa8jy55hap4zrq349isnpca3cc9wrf1sy62zslkpylxmz")))

(define-public crate-swc_node_comments-0.16.13 (c (n "swc_node_comments") (v "0.16.13") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.13") (d #t) (k 0)))) (h "05k3wffa41abagv2342k6vskym22lbv0gkzhdmdvcqnrh0dbl38x")))

(define-public crate-swc_node_comments-0.16.14 (c (n "swc_node_comments") (v "0.16.14") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.24") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.14") (d #t) (k 0)))) (h "0saqnplg1fzig9xipqlazvqx3p41zb5css730p73dnbbvgkb9mzf")))

(define-public crate-swc_node_comments-0.16.15 (c (n "swc_node_comments") (v "0.16.15") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.15") (d #t) (k 0)))) (h "021zch5fihnwcmbdwdhw94yk3478mf47c485b0idc3mwaa6frl1s")))

(define-public crate-swc_node_comments-0.16.16 (c (n "swc_node_comments") (v "0.16.16") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.16") (d #t) (k 0)))) (h "0m4cldly96sbkgmq5gh02yrh6vn2pabiwkiyvjkwmbghw9cyw51v")))

(define-public crate-swc_node_comments-0.16.17 (c (n "swc_node_comments") (v "0.16.17") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.17") (d #t) (k 0)))) (h "1anaa45djpvvfj88v8h75mdc5js2g49fh1vpjmb24fv0d83brpjz")))

(define-public crate-swc_node_comments-0.16.18 (c (n "swc_node_comments") (v "0.16.18") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.18") (d #t) (k 0)))) (h "1mk3nq7cfz3rc2ja687s4skfplhzh1jfy1asdib82gb2w0rcbmqw")))

(define-public crate-swc_node_comments-0.16.19 (c (n "swc_node_comments") (v "0.16.19") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.25") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.19") (d #t) (k 0)))) (h "1bvy4cmm7rkq30m8xwvshzf5jxphfkgxvm2492nrn816bj1zw2gg")))

(define-public crate-swc_node_comments-0.16.20 (c (n "swc_node_comments") (v "0.16.20") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.26") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.20") (d #t) (k 0)))) (h "1j754pzxswq6db53wvkicjvfnrv9y706mag5nk06p64v41a30347")))

(define-public crate-swc_node_comments-0.16.21 (c (n "swc_node_comments") (v "0.16.21") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.27") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.21") (d #t) (k 0)))) (h "1kn4w55pxgw5qyizpxs8qxw349gfkv7fhvmj03d7hp0ggldp4139")))

(define-public crate-swc_node_comments-0.16.22 (c (n "swc_node_comments") (v "0.16.22") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.28") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.22") (d #t) (k 0)))) (h "1m9q9df5vgzblxdgr8zj4qik4yrqg0m6w5xjq116vdam42jwi91r")))

(define-public crate-swc_node_comments-0.16.23 (c (n "swc_node_comments") (v "0.16.23") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.29") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.23") (d #t) (k 0)))) (h "0hyrfcg3ddsa0dhmkr9b84w2zlzk0hvsw0j8igkwzb82anrdsgxs")))

(define-public crate-swc_node_comments-0.16.24 (c (n "swc_node_comments") (v "0.16.24") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.31") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.24") (d #t) (k 0)))) (h "1mraq08qh61dcjzrj1sia991zn8i454hlpa3q0sds4ff0qr7l972")))

(define-public crate-swc_node_comments-0.16.25 (c (n "swc_node_comments") (v "0.16.25") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.25") (d #t) (k 0)))) (h "17gdp2pwbkk0k9v62anyxybyg8afa8g658d5s7509nqnhafixrnp")))

(define-public crate-swc_node_comments-0.16.26 (c (n "swc_node_comments") (v "0.16.26") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.26") (d #t) (k 0)))) (h "10xjii4jv0yb44rl9mynkpz6lb8cw4mqd1bsl3wi6dj5npy7av8n")))

(define-public crate-swc_node_comments-0.16.27 (c (n "swc_node_comments") (v "0.16.27") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.27") (d #t) (k 0)))) (h "1dywiabqr0kpk9iwza8vjj4dfkdckk1cza4x3qaznj4y3afqbnkz")))

(define-public crate-swc_node_comments-0.16.28 (c (n "swc_node_comments") (v "0.16.28") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.28") (d #t) (k 0)))) (h "0d7jlvcgc1gyg5wngdzdad9226hwsacviv4sx6cigr2f8dm706g0")))

(define-public crate-swc_node_comments-0.16.29 (c (n "swc_node_comments") (v "0.16.29") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.32") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.29") (d #t) (k 0)))) (h "14qwxk20xdmvndlihgmpsj1yhnz003v7wp41csh9k2scnm158dqj")))

(define-public crate-swc_node_comments-0.16.30 (c (n "swc_node_comments") (v "0.16.30") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.35") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.30") (d #t) (k 0)))) (h "0xbmm8bsgyy9w7y8b34zp1jh8ki14ca6zr9dbzfdzck3143xwh4r")))

(define-public crate-swc_node_comments-0.16.31 (c (n "swc_node_comments") (v "0.16.31") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.36") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.31") (d #t) (k 0)))) (h "1ni977pq7j1hns4b655v5rh4jqap5dymawdxqcrnqhygas7jgaq0")))

(define-public crate-swc_node_comments-0.16.32 (c (n "swc_node_comments") (v "0.16.32") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.37") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.32") (d #t) (k 0)))) (h "1s35niggkbiyjvqn94n14ba7gpnniph2kzdcw36q4m7vfcxji8v5")))

(define-public crate-swc_node_comments-0.16.33 (c (n "swc_node_comments") (v "0.16.33") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.38") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.33") (d #t) (k 0)))) (h "0mxl1wknldlcrddxgvk7bnq2bbaml4l35aafnmwj2whwpybhzr2k")))

(define-public crate-swc_node_comments-0.16.34 (c (n "swc_node_comments") (v "0.16.34") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.38") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.34") (d #t) (k 0)))) (h "0d9h0yibhxr38sd75p3jqmiwd4wwky45inirg5qgmh627c31ffl3")))

(define-public crate-swc_node_comments-0.16.35 (c (n "swc_node_comments") (v "0.16.35") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.35") (d #t) (k 0)))) (h "142dpyhwb44q3rd6r6566jkx40bh5nndhrx1hv9wyc5bd0d1gjq8")))

(define-public crate-swc_node_comments-0.16.36 (c (n "swc_node_comments") (v "0.16.36") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.36") (d #t) (k 0)))) (h "0vkpwix64s1g2dapjc6pcazjj2d515xpab7x1zprakrm21pdn41g")))

(define-public crate-swc_node_comments-0.16.37 (c (n "swc_node_comments") (v "0.16.37") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.37") (d #t) (k 0)))) (h "14ks8fjpam49gw6kd4g0hm8pd0m37s5jj0sncnaxyh7rq6dzfqn7")))

(define-public crate-swc_node_comments-0.16.38 (c (n "swc_node_comments") (v "0.16.38") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.38") (d #t) (k 0)))) (h "10rhdrvdqi0gbfryphizqj2lschciib8hyah1yjwpbvfh2r3ind1")))

(define-public crate-swc_node_comments-0.16.39 (c (n "swc_node_comments") (v "0.16.39") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.39") (d #t) (k 0)))) (h "0a55h9cv8anv4xrwnzs9nwgi9md0ak42fivf25rjjqx5hj8iqw6k")))

(define-public crate-swc_node_comments-0.16.40 (c (n "swc_node_comments") (v "0.16.40") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.40") (d #t) (k 0)))) (h "0aaiimw30c7d4vr9yx0r3zjq4hkkkyz9nl0n2a365bgcllj8i92a")))

(define-public crate-swc_node_comments-0.17.0 (c (n "swc_node_comments") (v "0.17.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.39") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.0") (d #t) (k 0)))) (h "1djs3kw0sw6rmyxy45s5wv9hxmsll7cifcyci121q5z51qdb42z1")))

(define-public crate-swc_node_comments-0.17.1 (c (n "swc_node_comments") (v "0.17.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.40") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.1") (d #t) (k 0)))) (h "1n7zqzv1kb17ddarzm7fy6xv0l7zqs738i5pfk9y7nvkaxyjq2nz")))

(define-public crate-swc_node_comments-0.17.2 (c (n "swc_node_comments") (v "0.17.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.41") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.2") (d #t) (k 0)))) (h "0gj4wd5wqd4xzifdh8mq5kar0h4714ggsb5pqkl0vn35kg4d8ypb")))

(define-public crate-swc_node_comments-0.17.3 (c (n "swc_node_comments") (v "0.17.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.42") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.3") (d #t) (k 0)))) (h "01pk667km7b5dqcn64aiq33q9p7s4ri5w4gyx7yr42kq6pzhxr4j")))

(define-public crate-swc_node_comments-0.17.4 (c (n "swc_node_comments") (v "0.17.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.43") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.4") (d #t) (k 0)))) (h "11la68h617b51zgwy3sjscjac7jcp08id7irgjyxg1rrcdyyfbx7")))

(define-public crate-swc_node_comments-0.17.5 (c (n "swc_node_comments") (v "0.17.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.4.43") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.5") (d #t) (k 0)))) (h "1lx1jj6q2fhzsqzzvrd98ghd3z2svgvzq4lf9b57dr9ycwww8d6y")))

(define-public crate-swc_node_comments-0.18.0 (c (n "swc_node_comments") (v "0.18.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.0") (d #t) (k 0)))) (h "0dfwqgw76sym9hwhiyskp9frclhing6r6siz7qk21ns42fcnwsn9")))

(define-public crate-swc_node_comments-0.18.1 (c (n "swc_node_comments") (v "0.18.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.1") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.1") (d #t) (k 0)))) (h "0pmv5dx8pv07ngkrfmhwhyjjfxhyhcggm7h2yird39z8sjmm84bd")))

(define-public crate-swc_node_comments-0.18.2 (c (n "swc_node_comments") (v "0.18.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.2") (d #t) (k 0)))) (h "18jx77x5ah238djk7phs9xghikii53byhgxc4b66vqw4vnfls8lg")))

(define-public crate-swc_node_comments-0.18.3 (c (n "swc_node_comments") (v "0.18.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.3") (d #t) (k 0)))) (h "1i35llhqa48wkv4bjf9ci4q480vvfx03g2aj9pz1zvppawkjfgsy")))

(define-public crate-swc_node_comments-0.18.4 (c (n "swc_node_comments") (v "0.18.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.4") (d #t) (k 0)))) (h "174ign5khfcph9yr1x0xyz6cjrf6mhphc8w0wr33wp7bjqvkqwhy")))

(define-public crate-swc_node_comments-0.18.5 (c (n "swc_node_comments") (v "0.18.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.5") (d #t) (k 0)))) (h "14g7cqs2pmbg85vfikw228ybigcpy8psc5kskw2r2clqrh4bxlzp")))

(define-public crate-swc_node_comments-0.18.6 (c (n "swc_node_comments") (v "0.18.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.6") (d #t) (k 0)))) (h "0p7ir12fmchw1ll5fs8h316rraxwpjj9j2a3cpcdy5jihl09mgc2")))

(define-public crate-swc_node_comments-0.18.7 (c (n "swc_node_comments") (v "0.18.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.7") (d #t) (k 0)))) (h "1rpx9akk2bixvl5a5c67xsvsmpyi4rnbgbm4whpybnla3iq52ssn")))

(define-public crate-swc_node_comments-0.18.8 (c (n "swc_node_comments") (v "0.18.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.8") (d #t) (k 0)))) (h "05rba8si4dq44nsh1yy81nhwfikwqs5jzd6xz16m40nrwljjvzx4")))

(define-public crate-swc_node_comments-0.18.9 (c (n "swc_node_comments") (v "0.18.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.9") (d #t) (k 0)))) (h "0bq2v2rgva41nmwcs3230zbknv5nkx5va0n7fyhqq8c59ygkyhl0")))

(define-public crate-swc_node_comments-0.18.10 (c (n "swc_node_comments") (v "0.18.10") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.10") (d #t) (k 0)))) (h "1620xisl4z3avkciqplr9prdixp8a6ysbxkxlqgqydf8kn4w6byi")))

(define-public crate-swc_node_comments-0.18.11 (c (n "swc_node_comments") (v "0.18.11") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.11") (d #t) (k 0)))) (h "0sv7gsp5awfq94v954r1abpshdylx2ja4y1052wa3xknx833axxm")))

(define-public crate-swc_node_comments-0.18.12 (c (n "swc_node_comments") (v "0.18.12") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.12") (d #t) (k 0)))) (h "12j4nxhjii0mgmmiiylbrcmsc8wc777nkiqmy427llf0122k03c0")))

(define-public crate-swc_node_comments-0.18.13 (c (n "swc_node_comments") (v "0.18.13") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.13") (d #t) (k 0)))) (h "1ckvqdqcx061m2hrxfpbfpi0f9mgyzmda8w9mnpbvn33d66cimlp")))

(define-public crate-swc_node_comments-0.18.14 (c (n "swc_node_comments") (v "0.18.14") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.14") (d #t) (k 0)))) (h "0gkcc5y8b0vb3dvf2iwy2zl7285ycv7kwcli9fzg7wzbr6jxyjnk")))

(define-public crate-swc_node_comments-0.18.15 (c (n "swc_node_comments") (v "0.18.15") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.15") (d #t) (k 0)))) (h "0rzypmw747fw4jby6id9i6g40hhnds9grn4s6jfai8nsfjil9wjl")))

(define-public crate-swc_node_comments-0.18.16 (c (n "swc_node_comments") (v "0.18.16") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.16") (d #t) (k 0)))) (h "1sl4m9z8xzj1vk1mdlcvxr70jjwlpca9jlsdck79ap3bjf59z0v1")))

(define-public crate-swc_node_comments-0.18.17 (c (n "swc_node_comments") (v "0.18.17") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.7") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.17") (d #t) (k 0)))) (h "0z5njf4i4g62gcjxdcm7rzk213l261hcynw81addfvb4r20vlvcr")))

(define-public crate-swc_node_comments-0.18.18 (c (n "swc_node_comments") (v "0.18.18") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.18") (d #t) (k 0)))) (h "04a9pmd4hqxl5p91phl0k02ixkng192jb876i2ylz1ff38dvy89n")))

(define-public crate-swc_node_comments-0.18.19 (c (n "swc_node_comments") (v "0.18.19") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.19") (d #t) (k 0)))) (h "0lvyis8d67mnks3rh5gv2yrq8qjf52853lkr92s8546zdhla1bd1")))

(define-public crate-swc_node_comments-0.18.20 (c (n "swc_node_comments") (v "0.18.20") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.20") (d #t) (k 0)))) (h "1bf0l6sd447sxajqkacx87wh30k61cyr2wfmy7bp1xh9kf73fbqj")))

(define-public crate-swc_node_comments-0.18.21 (c (n "swc_node_comments") (v "0.18.21") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.8") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.21") (d #t) (k 0)))) (h "1bw65l69jhrk68xf7hcl7xmmy6rinnnrbyicap9vdybw9yx85rz6")))

(define-public crate-swc_node_comments-0.18.22 (c (n "swc_node_comments") (v "0.18.22") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.22") (d #t) (k 0)))) (h "1jkgfqpi2ypk0q2m8qxk5vhwsxk42yv24ypvkzb19zjlwpqy71ns")))

(define-public crate-swc_node_comments-0.19.0 (c (n "swc_node_comments") (v "0.19.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.0") (d #t) (k 0)))) (h "1j9vac8ir06mzpq3sb5nv4gf4hrs6hda9l1xwlchs7nwvvgfbkkk")))

(define-public crate-swc_node_comments-0.19.1 (c (n "swc_node_comments") (v "0.19.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.1") (d #t) (k 0)))) (h "047735i22pmfnjvg3967al4ysk111mayz7ijfap8pazifdsmkfdj")))

(define-public crate-swc_node_comments-0.19.2 (c (n "swc_node_comments") (v "0.19.2") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.5.9") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.2") (d #t) (k 0)))) (h "0yzwfglj6gb6x3jdr4ryndcj7gf9nz1afyvqmjla6gyq54l4283h")))

(define-public crate-swc_node_comments-0.20.0 (c (n "swc_node_comments") (v "0.20.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.0") (d #t) (k 0)))) (h "0rf385v0r1vc2rr9naa66i8npm7qn7sn6395da2w8h4s73x0l9fg")))

(define-public crate-swc_node_comments-0.20.1 (c (n "swc_node_comments") (v "0.20.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.1") (d #t) (k 0)))) (h "0s7866kylw4k80ffcw3alfdngw3yj6cg532hkc5nazm6vgfyjvzm")))

(define-public crate-swc_node_comments-0.20.2 (c (n "swc_node_comments") (v "0.20.2") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)))) (h "0z8y6jg3pih7074fisrbwha989fwick7xnfn3gz7fqck4aiadg8j")))

(define-public crate-swc_node_comments-0.20.3 (c (n "swc_node_comments") (v "0.20.3") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.3") (d #t) (k 0)))) (h "0n3i4mpizjmpjz9nv709pp1klfsmy65kvn2flhp0mpr3xn0hl839")))

(define-public crate-swc_node_comments-0.20.4 (c (n "swc_node_comments") (v "0.20.4") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.4") (d #t) (k 0)))) (h "0q38pkvzybp8sh8byykwnyrp13fn711x0jl4yp8xnk9xpc3f909l")))

(define-public crate-swc_node_comments-0.20.5 (c (n "swc_node_comments") (v "0.20.5") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.1") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.5") (d #t) (k 0)))) (h "01h8fbx3nfk4a9xprq50jaqnhkb6z85za6jpqyy8vfgfjy2qz7bp")))

(define-public crate-swc_node_comments-0.20.6 (c (n "swc_node_comments") (v "0.20.6") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.6") (d #t) (k 0)))) (h "17k16qjwl9nzbhx7mx9x7p4gjhpr4lbplwypdrp2w67smyc60qjp")))

(define-public crate-swc_node_comments-0.20.7 (c (n "swc_node_comments") (v "0.20.7") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.3") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.7") (d #t) (k 0)))) (h "1nbn1ipcbys21n9lvml071fwjgy5f22pc89npw82s0gp5fyavr06")))

(define-public crate-swc_node_comments-0.20.8 (c (n "swc_node_comments") (v "0.20.8") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.8") (d #t) (k 0)))) (h "0l34aiz4775c2c0vadw8529h4rcxv0rpyxjwbh159smw6mcnr714")))

(define-public crate-swc_node_comments-0.20.9 (c (n "swc_node_comments") (v "0.20.9") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.9") (d #t) (k 0)))) (h "1xy4xzz305mlgz78pd95jwxp3n9g502dg89nm974qs82yr4gabi8")))

(define-public crate-swc_node_comments-0.20.10 (c (n "swc_node_comments") (v "0.20.10") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.10") (d #t) (k 0)))) (h "00353fmfmdaxz3b4nwk6xycki1222wvyp3w4ss5jgxrm80n45wfb")))

(define-public crate-swc_node_comments-0.20.11 (c (n "swc_node_comments") (v "0.20.11") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.11") (d #t) (k 0)))) (h "0vzbgs23xr5ydvafab8sd7bf7sma7kf1z7j9hjr96pzaqg96rnrv")))

(define-public crate-swc_node_comments-0.20.12 (c (n "swc_node_comments") (v "0.20.12") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.12") (d #t) (k 0)))) (h "1cg5rqwvwrcfp5k4ayaws1rmyqn1km9xvdgprnr1fnvkp67pr9l6")))

(define-public crate-swc_node_comments-0.20.13 (c (n "swc_node_comments") (v "0.20.13") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.14") (d #t) (k 0)))) (h "1z042s8x5yyq0098shhvvzh6yhns475qizl161rl9gv1v5wr25nn")))

(define-public crate-swc_node_comments-0.20.14 (c (n "swc_node_comments") (v "0.20.14") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.15") (d #t) (k 0)))) (h "0d9z4slyqyhwcnd8z7rcq96rjqsn03qr1xp8rf1b8l0i4i4821d8")))

(define-public crate-swc_node_comments-0.20.15 (c (n "swc_node_comments") (v "0.20.15") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.16") (d #t) (k 0)))) (h "0d5kfgj6m2bma1md4v782hpkypk1x4vg7dl1km9fa5bbwbg48nmk")))

(define-public crate-swc_node_comments-0.20.16 (c (n "swc_node_comments") (v "0.20.16") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.17") (d #t) (k 0)))) (h "14arg5ccjfpsl5bnzwxjdqpllq1q4h9jpbmr6201n2xbr7gzg9md")))

(define-public crate-swc_node_comments-0.20.17 (c (n "swc_node_comments") (v "0.20.17") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.18") (d #t) (k 0)))) (h "0szjyg2lq2cfvvfp05a0ckk2n28lcp888gyyq83vpw6pirj5fym2")))

(define-public crate-swc_node_comments-0.20.18 (c (n "swc_node_comments") (v "0.20.18") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.19") (d #t) (k 0)))) (h "12v8yyrj0vbkslqm8pgbpags13wf3n0sl4ab1i52lw59y28fxp62")))

(define-public crate-swc_node_comments-0.20.19 (c (n "swc_node_comments") (v "0.20.19") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.20") (d #t) (k 0)))) (h "1bnp3vl6d6ls101z6r15ibmrf15pdsqknsfwr30n2jdmwhswcxgx")))

(define-public crate-swc_node_comments-0.20.20 (c (n "swc_node_comments") (v "0.20.20") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.6.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.26") (d #t) (k 0)))) (h "0r9hfyf4wnh65va35inwirhmxxpn5yhr29x2jj6m6cngzszii4nk")))

