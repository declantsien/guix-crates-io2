(define-module (crates-io sw c_ swc_fast_graph) #:use-module (crates-io))

(define-public crate-swc_fast_graph-0.1.0 (c (n "swc_fast_graph") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.14.6") (d #t) (k 0)))) (h "0dikzvv8x3vjvglv3cbdbwnnasvy2gkf1fk625kvmydfbb9mhv11")))

(define-public crate-swc_fast_graph-0.2.0 (c (n "swc_fast_graph") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.15.0") (d #t) (k 0)))) (h "0ci620s9sglihycgvr5mxv7qrmam9dhnj466fa6270vj9j0qrq5l")))

(define-public crate-swc_fast_graph-0.3.0 (c (n "swc_fast_graph") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.16.0") (d #t) (k 0)))) (h "0g080ynmndn40s070wy5xp7lldczz06wm96wa5pq7sbrkhm98ms8")))

(define-public crate-swc_fast_graph-0.4.0 (c (n "swc_fast_graph") (v "0.4.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.0") (d #t) (k 0)))) (h "0kvhliiqljy9f25jvq2rmjamnhhz4621zbw3y93wamdhnk5vnlqx")))

(define-public crate-swc_fast_graph-0.5.0 (c (n "swc_fast_graph") (v "0.5.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.17.0") (d #t) (k 0)))) (h "0q34jrm8y239zq2w0yv87qg372l300jhm9irpi2xzviizj7yyq4q")))

(define-public crate-swc_fast_graph-0.6.0 (c (n "swc_fast_graph") (v "0.6.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.18.0") (d #t) (k 0)))) (h "158x300lypnhl79qiznlzbjh8vv6hvpkdyvxzkic31yrybhwgkfw")))

(define-public crate-swc_fast_graph-0.7.0 (c (n "swc_fast_graph") (v "0.7.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.19.0") (d #t) (k 0)))) (h "1xdnygzraks1v2hp2cy4wk6yvxrr1nd120wjfmfqny98vwd44skw")))

(define-public crate-swc_fast_graph-0.8.0 (c (n "swc_fast_graph") (v "0.8.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.20.0") (d #t) (k 0)))) (h "10ajadi22zrmnij3gj3n2zs44rfp27w1ibr8x2cxq2xjdgy43jgs")))

(define-public crate-swc_fast_graph-0.9.0 (c (n "swc_fast_graph") (v "0.9.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.21.0") (d #t) (k 0)))) (h "0sx42xdx3wx87zf6jsnqj8g7vmdw2575xza4js9yzi9w52k92y1y")))

(define-public crate-swc_fast_graph-0.10.0 (c (n "swc_fast_graph") (v "0.10.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.22.0") (d #t) (k 0)))) (h "02ppipkjy2mf3glk8kai0zz47s0mdlzjz3inyjfw4h21qjqpi191")))

(define-public crate-swc_fast_graph-0.11.0 (c (n "swc_fast_graph") (v "0.11.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.23.0") (d #t) (k 0)))) (h "13qcmpj4cw5s88pvfrmml7mknrgjjq0x2lx6djagp094p1mq4q4j")))

(define-public crate-swc_fast_graph-0.12.0 (c (n "swc_fast_graph") (v "0.12.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.24.0") (d #t) (k 0)))) (h "06rxn8z7fghbc4fk8wk6z4d3i91w6lwsmjfm9fqs2wf2aj3xkk4d")))

(define-public crate-swc_fast_graph-0.13.0 (c (n "swc_fast_graph") (v "0.13.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.25.0") (d #t) (k 0)))) (h "0k50a57qpz2vswk15niz9am1dhagygr8lhias49hb1g5038ycwr8")))

(define-public crate-swc_fast_graph-0.14.0 (c (n "swc_fast_graph") (v "0.14.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.26.0") (d #t) (k 0)))) (h "14hbq9c0qw0i55v5djhz2mla9mq4zyzl8x8g0vmx4gch4fqc8rns")))

(define-public crate-swc_fast_graph-0.15.0 (c (n "swc_fast_graph") (v "0.15.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.0") (d #t) (k 0)))) (h "0z2cparbz3kwakkjpn9rzpf6samrrh932s1iix8q3i2aqinp12jq")))

(define-public crate-swc_fast_graph-0.15.1 (c (n "swc_fast_graph") (v "0.15.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.3") (d #t) (k 0)))) (h "10x22246gvp4ayhdd34dfbyxwc30sjb615prwy8lfql4rdgd499j")))

(define-public crate-swc_fast_graph-0.15.2 (c (n "swc_fast_graph") (v "0.15.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.4") (d #t) (k 0)))) (h "0zaslnmd0lrxiac0gwghlaz381zill9ada7rbr4axcvbcx06dvba")))

(define-public crate-swc_fast_graph-0.15.3 (c (n "swc_fast_graph") (v "0.15.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.5") (d #t) (k 0)))) (h "02vic2pcghxfhszhhaa61fxjc3m5acmnx9yyyarys2l7ni72aawz")))

(define-public crate-swc_fast_graph-0.15.4 (c (n "swc_fast_graph") (v "0.15.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.6") (d #t) (k 0)))) (h "0q29rjh7cn79wz22qbvz09c11iybp1iray5704l75f47m57zzc7p")))

(define-public crate-swc_fast_graph-0.15.5 (c (n "swc_fast_graph") (v "0.15.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.13") (d #t) (k 0)))) (h "0mgvrxmdg0r4xrr5ins5nh94f53kmw4d9bb71r40gmh1arq5qsq5")))

(define-public crate-swc_fast_graph-0.15.6 (c (n "swc_fast_graph") (v "0.15.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.13") (d #t) (k 0)))) (h "008mwfx3qhf7dav529d63cdx5zdn8pih03n6f409caj5ay0b8biz")))

(define-public crate-swc_fast_graph-0.15.7 (c (n "swc_fast_graph") (v "0.15.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.14") (d #t) (k 0)))) (h "0y223hy113ixxlcvc64yhry1925y1bkp6dvw33qsf2y87c1gybmq")))

(define-public crate-swc_fast_graph-0.15.8 (c (n "swc_fast_graph") (v "0.15.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.15") (d #t) (k 0)))) (h "00lmqzm90madn709fnh3zskkrg0s2xiad5a7l2v81pbyxwqh9l16")))

(define-public crate-swc_fast_graph-0.15.9 (c (n "swc_fast_graph") (v "0.15.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.27.16") (d #t) (k 0)))) (h "0cqpc2f58jpkakvsgjv1xhrycifpi8x92ik12p54nw4l4b9cq6rv")))

(define-public crate-swc_fast_graph-0.16.0 (c (n "swc_fast_graph") (v "0.16.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.0") (d #t) (k 0)))) (h "0jz6al97yclqhj7246zp4bdr3d6x87b3z79r5qh5y4yjyf8npa33")))

(define-public crate-swc_fast_graph-0.16.1 (c (n "swc_fast_graph") (v "0.16.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.1") (d #t) (k 0)))) (h "1l4z3x5rgfdv26i87q90whvnaml2pqjvm6c5mgfnzwqid357gk74")))

(define-public crate-swc_fast_graph-0.16.2 (c (n "swc_fast_graph") (v "0.16.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.2") (d #t) (k 0)))) (h "1zwhmxax259pkbk1csj78770ji3i525kswfs4rfg4734as3bnjir")))

(define-public crate-swc_fast_graph-0.16.3 (c (n "swc_fast_graph") (v "0.16.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.3") (d #t) (k 0)))) (h "08kzxz2kf6lw062cbsx322qzncc3278ybk70pb1l0bamh4bj88jp")))

(define-public crate-swc_fast_graph-0.16.4 (c (n "swc_fast_graph") (v "0.16.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.4") (d #t) (k 0)))) (h "1xawj8i5g552w2zkrfzyannjikbls8yi4y7rxygk85ri11642xza")))

(define-public crate-swc_fast_graph-0.16.5 (c (n "swc_fast_graph") (v "0.16.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.5") (d #t) (k 0)))) (h "0h06x6j7zxsgvsqsbqzjv5falz72492fcvf26cy8iig3r01cmcng")))

(define-public crate-swc_fast_graph-0.16.6 (c (n "swc_fast_graph") (v "0.16.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.6") (d #t) (k 0)))) (h "1fzf5fk5c6mjknl12yhn0x2mnq5pn9327n14nq0zjxk303vdqg9h")))

(define-public crate-swc_fast_graph-0.16.7 (c (n "swc_fast_graph") (v "0.16.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.7") (d #t) (k 0)))) (h "0dfkv40c8cp2h5yjqigqqav9k343cay543jpm8d2l6jncnpbmg37")))

(define-public crate-swc_fast_graph-0.16.8 (c (n "swc_fast_graph") (v "0.16.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.8") (d #t) (k 0)))) (h "1pw19hh0qgabsy68q7cy45q56lb7c55w7cn5nz0jsxdaj71qh0wx")))

(define-public crate-swc_fast_graph-0.16.9 (c (n "swc_fast_graph") (v "0.16.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.9") (d #t) (k 0)))) (h "0bsjcxfpcldn3pcc1j62bz8qqj5fdn86dqkpy2p81cbhrplxcfrm")))

(define-public crate-swc_fast_graph-0.16.10 (c (n "swc_fast_graph") (v "0.16.10") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.28.10") (d #t) (k 0)))) (h "116lpbzdf38nj4mri3igz3zx9x37n21ifm89xhx6jgb7i23diawp")))

(define-public crate-swc_fast_graph-0.17.0 (c (n "swc_fast_graph") (v "0.17.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.0") (d #t) (k 0)))) (h "10fk1jr7vf6fj31s4pz52rphp0hgw9f37kc1cj9y17bdbnd1xzcc")))

(define-public crate-swc_fast_graph-0.17.1 (c (n "swc_fast_graph") (v "0.17.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.1") (d #t) (k 0)))) (h "0cicgsykscmxmhbk81lib78z0hr5qrvpwrafbgfa4n0ch0fzfbmn")))

(define-public crate-swc_fast_graph-0.17.2 (c (n "swc_fast_graph") (v "0.17.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.2") (d #t) (k 0)))) (h "0dnrvwim3nv9cgg5n91yvalvjwxsjk83rnik6251m16gf4w2k3y3")))

(define-public crate-swc_fast_graph-0.17.3 (c (n "swc_fast_graph") (v "0.17.3") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.3") (d #t) (k 0)))) (h "1p7y20dws2dqbh6fwkpvzghp4k4n53cias0lpjkljp35l6cvrdgj")))

(define-public crate-swc_fast_graph-0.17.4 (c (n "swc_fast_graph") (v "0.17.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.4") (d #t) (k 0)))) (h "1hlbwdhr9bnn5b9jgxz1zn9gh2x0v1nm8yc994aza6kbp73rdj51")))

(define-public crate-swc_fast_graph-0.17.5 (c (n "swc_fast_graph") (v "0.17.5") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.4") (d #t) (k 0)))) (h "15wlizg3nz57hy42zx3yicm7ffnr0qxhf4ncc08hcswwhz8spgq0")))

(define-public crate-swc_fast_graph-0.17.6 (c (n "swc_fast_graph") (v "0.17.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.5") (d #t) (k 0)))) (h "1g64n89rr3byylf52cw1gi1kj24zdb2iqg0s47wv9ifpwcg941hj")))

(define-public crate-swc_fast_graph-0.17.7 (c (n "swc_fast_graph") (v "0.17.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.6") (d #t) (k 0)))) (h "07hdv54jx8rf48wgxdviyq0c3dhsd3pcivzfrd3qrrx5c65xnjvj")))

(define-public crate-swc_fast_graph-0.17.8 (c (n "swc_fast_graph") (v "0.17.8") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.7") (d #t) (k 0)))) (h "0w5jg7p85y03xh263qgn93z6avdcaqjqqz2h1gzxrx0cily68xn8")))

(define-public crate-swc_fast_graph-0.17.9 (c (n "swc_fast_graph") (v "0.17.9") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.8") (d #t) (k 0)))) (h "0ryn3802xckwhjv321wml7q7p7lvr3ill2n7qmdfg16l54a45dx8")))

(define-public crate-swc_fast_graph-0.17.10 (c (n "swc_fast_graph") (v "0.17.10") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.9") (d #t) (k 0)))) (h "11vkd6a64wwdaiwrpxqnwbkif1xa3ll8pald78cdglmzi0315smx")))

(define-public crate-swc_fast_graph-0.17.11 (c (n "swc_fast_graph") (v "0.17.11") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.10") (d #t) (k 0)))) (h "1wj4hp6cxwn3pb13apz0f8r4y3qpp59hcac5dsbxzbrq6cpjdl0f")))

(define-public crate-swc_fast_graph-0.17.12 (c (n "swc_fast_graph") (v "0.17.12") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.11") (d #t) (k 0)))) (h "1rzsnjfrqcfg5gkpn0i1nfw0apynd31dqv6bzhzmym7hm78ajm62")))

(define-public crate-swc_fast_graph-0.17.13 (c (n "swc_fast_graph") (v "0.17.13") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.12") (d #t) (k 0)))) (h "0pz6nlhk5fw7xhrbi9la7rw7lr8rxpkas1155lcq7ghhwdfj8dzz")))

(define-public crate-swc_fast_graph-0.17.14 (c (n "swc_fast_graph") (v "0.17.14") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.13") (d #t) (k 0)))) (h "0bncq0rb3n6898lsw759rxlyz5z8pib6sj8r827d60820vp8si75")))

(define-public crate-swc_fast_graph-0.17.15 (c (n "swc_fast_graph") (v "0.17.15") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.14") (d #t) (k 0)))) (h "0y3xi44rxf9kqrv8c6lascr0c9afvqd71illg7n6lnj48xxnd5gx")))

(define-public crate-swc_fast_graph-0.17.16 (c (n "swc_fast_graph") (v "0.17.16") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.15") (d #t) (k 0)))) (h "1y85hyar2a1x8jplhs9wm6svdwp67wwq70hi8z9k5xabfq10p0qk")))

(define-public crate-swc_fast_graph-0.17.17 (c (n "swc_fast_graph") (v "0.17.17") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.16") (d #t) (k 0)))) (h "0pwkw0w5g0djj0nzx6ap7yzrlwj31b5x6c09a003skxpy1sipdyw")))

(define-public crate-swc_fast_graph-0.17.18 (c (n "swc_fast_graph") (v "0.17.18") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.17") (d #t) (k 0)))) (h "0b5yqn1h90iy2bzd1g8jknxwk8s673gnvw78cv8xcgymwa0d7818")))

(define-public crate-swc_fast_graph-0.17.19 (c (n "swc_fast_graph") (v "0.17.19") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.18") (d #t) (k 0)))) (h "1kv79r0dn1x87m60vj7iabb6ak2pmphpp5kvadm2v484fgadyzfy")))

(define-public crate-swc_fast_graph-0.17.20 (c (n "swc_fast_graph") (v "0.17.20") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.19") (d #t) (k 0)))) (h "1bicf4qf1vwfl3qpvqbq9badylrykr5ih9d1cal6gxsv1n6ggz22")))

(define-public crate-swc_fast_graph-0.17.21 (c (n "swc_fast_graph") (v "0.17.21") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.20") (d #t) (k 0)))) (h "0s197dxpjn1gdggpjsn8pvhbrixkm5p7l60xinyyzlna63sj5gyw")))

(define-public crate-swc_fast_graph-0.17.22 (c (n "swc_fast_graph") (v "0.17.22") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.21") (d #t) (k 0)))) (h "1fnyrfnn4awgzlmbfd0impdx369xik5cvzwf9a94qf867d6i14if")))

(define-public crate-swc_fast_graph-0.17.23 (c (n "swc_fast_graph") (v "0.17.23") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.22") (d #t) (k 0)))) (h "01i2aqip8q14wgz4nw5hz6gzb4dc1z60vvxggvy6kc54a97kra4m")))

(define-public crate-swc_fast_graph-0.17.24 (c (n "swc_fast_graph") (v "0.17.24") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.23") (d #t) (k 0)))) (h "013xbyiqiqlsgciflv0cg6g2cc0gdf68cbs8p2h2wzk0r3fh75p6")))

(define-public crate-swc_fast_graph-0.17.25 (c (n "swc_fast_graph") (v "0.17.25") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.24") (d #t) (k 0)))) (h "10rcrncl2v3iahln4f0g7aiq5gwdjl2g47h0lk7fqwv8zifhy18j")))

(define-public crate-swc_fast_graph-0.17.26 (c (n "swc_fast_graph") (v "0.17.26") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.25") (d #t) (k 0)))) (h "0jq3jd32l4wcarnvwpyb38zc3kgs7cxjck8n5nby2f93cql4yn06")))

(define-public crate-swc_fast_graph-0.17.27 (c (n "swc_fast_graph") (v "0.17.27") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.26") (d #t) (k 0)))) (h "106d1ivfklfhh7y5118yds2fvn08ynpyvzdzvs5zpmkzmc82hp67")))

(define-public crate-swc_fast_graph-0.17.28 (c (n "swc_fast_graph") (v "0.17.28") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.27") (d #t) (k 0)))) (h "02plbbhw1mnb6x1sg606gb18mq76irm141m9d16s25arjcd3cm0w")))

(define-public crate-swc_fast_graph-0.17.29 (c (n "swc_fast_graph") (v "0.17.29") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.28") (d #t) (k 0)))) (h "05m0328323w9dr7kc8r5z83wfrbyrywbq89hpnalwmc5rm21wmj3")))

(define-public crate-swc_fast_graph-0.17.30 (c (n "swc_fast_graph") (v "0.17.30") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.29") (d #t) (k 0)))) (h "15ggvq15cf5sgs6xcmvxm625x7gw975w380dyn7s4akb9qq3lrj2")))

(define-public crate-swc_fast_graph-0.17.31 (c (n "swc_fast_graph") (v "0.17.31") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.30") (d #t) (k 0)))) (h "0rz96j166p59a3qs1k2m7i35vz7rbsa4npblpa2kc8w7qbw620df")))

(define-public crate-swc_fast_graph-0.17.32 (c (n "swc_fast_graph") (v "0.17.32") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.31") (d #t) (k 0)))) (h "0pbzgsy4xz1l4q61y6sry4rmqczzzbk08kx87gjdjglxa3jbac7w")))

(define-public crate-swc_fast_graph-0.17.33 (c (n "swc_fast_graph") (v "0.17.33") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.32") (d #t) (k 0)))) (h "0kpni9729a2s3vq04kdkm73r589hsjbyy3l7wgq77ff3i6cf48dw")))

(define-public crate-swc_fast_graph-0.17.34 (c (n "swc_fast_graph") (v "0.17.34") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.33") (d #t) (k 0)))) (h "0cckb177lj0310gw3gijpihik7si8x3x7mrnfqq1yah13k7g2shk")))

(define-public crate-swc_fast_graph-0.17.35 (c (n "swc_fast_graph") (v "0.17.35") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.34") (d #t) (k 0)))) (h "050wqm3mxiha83j6lzig1jyhaccs7lmdhk41r9plalqlvg4rcm8k")))

(define-public crate-swc_fast_graph-0.17.36 (c (n "swc_fast_graph") (v "0.17.36") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.35") (d #t) (k 0)))) (h "1i6sdgx5byv9wc3vx6kdwa358xf8z7s4wj6kfrads0qbxlx90h43")))

(define-public crate-swc_fast_graph-0.17.37 (c (n "swc_fast_graph") (v "0.17.37") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.36") (d #t) (k 0)))) (h "0q0gkzn56vxp1ac8gdwsg2rbc94m9jy93msv904dvngnw408krq4")))

(define-public crate-swc_fast_graph-0.17.38 (c (n "swc_fast_graph") (v "0.17.38") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.37") (d #t) (k 0)))) (h "02v0d7yng47mjfbr551rxqyi939iswp8d703jblwdmi8h2nj19z3")))

(define-public crate-swc_fast_graph-0.17.39 (c (n "swc_fast_graph") (v "0.17.39") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.38") (d #t) (k 0)))) (h "0s925qch1d7gfrs5zc24adqcjbap51v6m6w2m1ar36swmy9ms0dq")))

(define-public crate-swc_fast_graph-0.17.40 (c (n "swc_fast_graph") (v "0.17.40") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.39") (d #t) (k 0)))) (h "1r1n9nxisz3i0bgy8nn9cdl44d9d5ibvx5k47fmyvp4x6h8g7j87")))

(define-public crate-swc_fast_graph-0.17.41 (c (n "swc_fast_graph") (v "0.17.41") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.39") (d #t) (k 0)))) (h "0mjwznqs7gv581dnlkqx4n3068n6h9gf23k74r7dl2ldd9irsmdy")))

(define-public crate-swc_fast_graph-0.17.42 (c (n "swc_fast_graph") (v "0.17.42") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.40") (d #t) (k 0)))) (h "0hy7ya6baba4h034fv9w6sl39jks8kpvf4pl6f2x25i71k8f00hh")))

(define-public crate-swc_fast_graph-0.18.0 (c (n "swc_fast_graph") (v "0.18.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.0") (d #t) (k 0)))) (h "0h7rzq0aw0paxir006zvvmkgrai72f43sj2cnbr4ybi107vfl1pi")))

(define-public crate-swc_fast_graph-0.18.1 (c (n "swc_fast_graph") (v "0.18.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.1") (d #t) (k 0)))) (h "05nv9s8qbfpr3razb1bhbsqpdx552ddkx7l2cscr7n9l3jilh8zw")))

(define-public crate-swc_fast_graph-0.18.2 (c (n "swc_fast_graph") (v "0.18.2") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.2") (d #t) (k 0)))) (h "0lp8jjy1iraxblynv32jfdwrzq06whg86zim89glgw2qaxcn4qkh")))

(define-public crate-swc_fast_graph-0.18.3 (c (n "swc_fast_graph") (v "0.18.3") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.3") (d #t) (k 0)))) (h "05bja8xfz1l71q0zk986jnsvgw0zpsx7prh636r2msw16ilzr6dk")))

(define-public crate-swc_fast_graph-0.18.4 (c (n "swc_fast_graph") (v "0.18.4") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.4") (d #t) (k 0)))) (h "1zscbxiysp82yci22s82jlhnxzmgi424b6qkmqyvi3r4yjk5yk52")))

(define-public crate-swc_fast_graph-0.18.5 (c (n "swc_fast_graph") (v "0.18.5") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.30.5") (d #t) (k 0)))) (h "1v7pkx2a3f3vbyrdiwq6bx632ayb6b4z88zg4l2pdn9cjhdhij99")))

(define-public crate-swc_fast_graph-0.19.0 (c (n "swc_fast_graph") (v "0.19.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.0") (d #t) (k 0)))) (h "1kcqi30li86y4i7s3r2ycpg6k6g3br7hf0kqgspn7q7s93ajj7f2")))

(define-public crate-swc_fast_graph-0.19.1 (c (n "swc_fast_graph") (v "0.19.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.1") (d #t) (k 0)))) (h "1j3qv6rlkfzrdrzq45r55vl64jsp3vfl028661xmh0pkpqdbg9in")))

(define-public crate-swc_fast_graph-0.19.2 (c (n "swc_fast_graph") (v "0.19.2") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.2") (d #t) (k 0)))) (h "0sh33vyz6xj54iabpxpmyh7k9qpdy06mal82fy27l7izcfmscfrg")))

(define-public crate-swc_fast_graph-0.19.3 (c (n "swc_fast_graph") (v "0.19.3") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.3") (d #t) (k 0)))) (h "1acxir35jmnplqrmgih5n3rb8dwpj204s3d1w5kzcyqcd5k60jgc")))

(define-public crate-swc_fast_graph-0.19.4 (c (n "swc_fast_graph") (v "0.19.4") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.4") (d #t) (k 0)))) (h "15pmh0j5vdfcjhwdyajplgxvzarhcpibx9i6lsddrcpphzh94alr")))

(define-public crate-swc_fast_graph-0.19.5 (c (n "swc_fast_graph") (v "0.19.5") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.5") (d #t) (k 0)))) (h "1p6c6zx3kak0nlx6pfcy9mkn0qcg9r6x32mzw08byb3xwjp3ns4m")))

(define-public crate-swc_fast_graph-0.19.6 (c (n "swc_fast_graph") (v "0.19.6") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.6") (d #t) (k 0)))) (h "1c0g1zsy4cywjg04zl3cq1dprfnj919hzw8k8v2h5m33ir2vz5sv")))

(define-public crate-swc_fast_graph-0.19.7 (c (n "swc_fast_graph") (v "0.19.7") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.7") (d #t) (k 0)))) (h "0mmisvmr6m5r6vvjk1hs90fdhgkvlk2x657a5shqgmcnvb5nbssz")))

(define-public crate-swc_fast_graph-0.19.8 (c (n "swc_fast_graph") (v "0.19.8") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.8") (d #t) (k 0)))) (h "1kkr78v14i4cyrkm6qpy6c7psanf5wdw13s631afa9aw6d4vxxxi")))

(define-public crate-swc_fast_graph-0.19.9 (c (n "swc_fast_graph") (v "0.19.9") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.9") (d #t) (k 0)))) (h "17zc6n6ssycgw9081dh5smdnn6qy9qyiwbra4xqcrvzznixv0h9w")))

(define-public crate-swc_fast_graph-0.19.10 (c (n "swc_fast_graph") (v "0.19.10") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.10") (d #t) (k 0)))) (h "1jgfxbp9gn2hcg5bd22a7qirszng6rbmg8rgpqymy1wkn5smf1vw")))

(define-public crate-swc_fast_graph-0.19.11 (c (n "swc_fast_graph") (v "0.19.11") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.11") (d #t) (k 0)))) (h "0xlw5nrlfn7dcg8xmni7hlslzlr0camkd6n4sapzc9ilw9y8l0mq")))

(define-public crate-swc_fast_graph-0.19.12 (c (n "swc_fast_graph") (v "0.19.12") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.12") (d #t) (k 0)))) (h "029hxjwnm9lsy6rc6wgmn81zghfahkmjr8alzmv519abxjd194b2")))

(define-public crate-swc_fast_graph-0.19.13 (c (n "swc_fast_graph") (v "0.19.13") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.13") (d #t) (k 0)))) (h "10kbbj7fsvii4amlzgxl740bxx8yb3sq89ahzx6q6rj9y67vbzar")))

(define-public crate-swc_fast_graph-0.19.14 (c (n "swc_fast_graph") (v "0.19.14") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.14") (d #t) (k 0)))) (h "1cjf694h56sba0k02s5p1iq51m4rqvih0yqmgvwh8mww4zxwrmsh")))

(define-public crate-swc_fast_graph-0.19.15 (c (n "swc_fast_graph") (v "0.19.15") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.15") (d #t) (k 0)))) (h "0lmkihg8rkgx3nl8an82ch204diwcqsnkqh7x7vispxhqcxg9cpy")))

(define-public crate-swc_fast_graph-0.19.16 (c (n "swc_fast_graph") (v "0.19.16") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.16") (d #t) (k 0)))) (h "1xczcshk9l0kn0c839xbxjhy27wmz89givzx9v1j7f8rh937d6xi")))

(define-public crate-swc_fast_graph-0.19.17 (c (n "swc_fast_graph") (v "0.19.17") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.17") (d #t) (k 0)))) (h "126b1yg1kp8yb35rj76nj34w1nq7f7zf4f815x7n6cmw624y8vj1")))

(define-public crate-swc_fast_graph-0.19.18 (c (n "swc_fast_graph") (v "0.19.18") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.18") (d #t) (k 0)))) (h "05fsrscfj3n9wbxldxr21vpbw6ajiywr84x0lz2h22bfczpq9k0i")))

(define-public crate-swc_fast_graph-0.19.19 (c (n "swc_fast_graph") (v "0.19.19") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.19") (d #t) (k 0)))) (h "0y7xnd09rcyr59m34gw84ykiqhw48r2lixyhk9k8cb5cw3l5kawc")))

(define-public crate-swc_fast_graph-0.19.20 (c (n "swc_fast_graph") (v "0.19.20") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.20") (d #t) (k 0)))) (h "1qgp8rbvvbp44ipr20k5d7azi2l5pc6kkkncda94h2p0xh0j0adi")))

(define-public crate-swc_fast_graph-0.19.21 (c (n "swc_fast_graph") (v "0.19.21") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.21") (d #t) (k 0)))) (h "1dyplp83sj6a2n5ajxnb6nncaisbk6rq9cvghbkbfn8n9nd6qyxh")))

(define-public crate-swc_fast_graph-0.19.22 (c (n "swc_fast_graph") (v "0.19.22") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.31.22") (d #t) (k 0)))) (h "1h0790i4nfgnmsvifwjdz7p63mmkxdajbhxvwlrbk8ryxfsxhkpr")))

(define-public crate-swc_fast_graph-0.20.0 (c (n "swc_fast_graph") (v "0.20.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.0") (d #t) (k 0)))) (h "1kjcv4l588xdk1nhnl0k2ay8a0mylwqrbp9va7kvxqswxgrgy1x4")))

(define-public crate-swc_fast_graph-0.20.1 (c (n "swc_fast_graph") (v "0.20.1") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.1") (d #t) (k 0)))) (h "073s8izslrc3jwjhrzgf3xdq49iqxhd4qag0166lvdgxvry2kxz2")))

(define-public crate-swc_fast_graph-0.20.2 (c (n "swc_fast_graph") (v "0.20.2") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.32.2") (d #t) (k 0)))) (h "1q26z2dp9qcd0szcslmkpglb0nkhfaf337wxjp3s2xsrl75qic1c")))

(define-public crate-swc_fast_graph-0.21.0 (c (n "swc_fast_graph") (v "0.21.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.0") (d #t) (k 0)))) (h "0mskv9s5g19053px020p1337zaqjlhjgfk1874g624vrc7lnjzmr")))

(define-public crate-swc_fast_graph-0.21.1 (c (n "swc_fast_graph") (v "0.21.1") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.1") (d #t) (k 0)))) (h "1lsxkir8idadf80lr3wmbsx889rafbiw594nqn9p5wiq2j9xr017")))

(define-public crate-swc_fast_graph-0.21.2 (c (n "swc_fast_graph") (v "0.21.2") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)))) (h "1lhi5sdwf1pm7gx507zavg2ipmjsgrrldyzn9hvlz89y8wcbzfmd")))

(define-public crate-swc_fast_graph-0.21.3 (c (n "swc_fast_graph") (v "0.21.3") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.3") (d #t) (k 0)))) (h "1vcf7jplp23qmspx2l1b7g10w6h4rzallx53iw8cw43dqjk44p0k")))

(define-public crate-swc_fast_graph-0.21.4 (c (n "swc_fast_graph") (v "0.21.4") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.4") (d #t) (k 0)))) (h "06108fgz1y5c6hyqmv2wy2ir6mvahxjclm7sh52v9ip9q3qzs13x")))

(define-public crate-swc_fast_graph-0.21.5 (c (n "swc_fast_graph") (v "0.21.5") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.5") (d #t) (k 0)))) (h "0cc2vikjj5vhkasjck627ciq9gmy34hxg43721134201sp77sp0p")))

(define-public crate-swc_fast_graph-0.21.6 (c (n "swc_fast_graph") (v "0.21.6") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.6") (d #t) (k 0)))) (h "0xcr07kjpv5iyavrfs2x0w64r065f2320h9jpk74micr3281mvkc")))

(define-public crate-swc_fast_graph-0.21.7 (c (n "swc_fast_graph") (v "0.21.7") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.7") (d #t) (k 0)))) (h "0yhf9bqkyd3sccadqmm8l70s1l4nmf9w51cvfw09k0abpm18f781")))

(define-public crate-swc_fast_graph-0.21.8 (c (n "swc_fast_graph") (v "0.21.8") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.8") (d #t) (k 0)))) (h "0qlwhm56d4ha6p9cq2xvdpz5003wc8cpwzpi5v0gkdks0k74f81r")))

(define-public crate-swc_fast_graph-0.21.9 (c (n "swc_fast_graph") (v "0.21.9") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.9") (d #t) (k 0)))) (h "13bqbj1ac1c1h3hjq6xp7iqpqvfb6xvgmmj9wnrhrcxw1g8zc5w1")))

(define-public crate-swc_fast_graph-0.21.10 (c (n "swc_fast_graph") (v "0.21.10") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.10") (d #t) (k 0)))) (h "1rsirjhzakabg9fmq4bqk0d500ggy3vysqqazc3c38aysf1dnizr")))

(define-public crate-swc_fast_graph-0.21.11 (c (n "swc_fast_graph") (v "0.21.11") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.10") (d #t) (k 0)))) (h "1w6v8b24vy06kv6lxlql42p9rhfkwjpjdl9942cxwcyyj46hw77p")))

(define-public crate-swc_fast_graph-0.21.12 (c (n "swc_fast_graph") (v "0.21.12") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.11") (d #t) (k 0)))) (h "0y5343n9njawi39z3b4d0q72vqxpwpf736zvdkyrkiscnl4lyp4h")))

(define-public crate-swc_fast_graph-0.21.13 (c (n "swc_fast_graph") (v "0.21.13") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.12") (d #t) (k 0)))) (h "1dl1akhrcpli0xfl2xmkwvdkz0bf4kmw2qs7dbiby3vs0rbc1kwa")))

(define-public crate-swc_fast_graph-0.21.14 (c (n "swc_fast_graph") (v "0.21.14") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.14") (d #t) (k 0)))) (h "1k5q24x9q9ynawv2skjp82jy2rigyrgzc9g1rcy8r7v7aiapwc4q")))

(define-public crate-swc_fast_graph-0.21.15 (c (n "swc_fast_graph") (v "0.21.15") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.15") (d #t) (k 0)))) (h "1816is86pv1z1fw58rqkh7kfq7qmpjjv2x0gaapmgq8lf1b9x70c")))

(define-public crate-swc_fast_graph-0.21.16 (c (n "swc_fast_graph") (v "0.21.16") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.16") (d #t) (k 0)))) (h "1a84271n36bqzxxd3f2qqfkcy95yhf297kkwdd4mnnfmgs3q1k06")))

(define-public crate-swc_fast_graph-0.21.17 (c (n "swc_fast_graph") (v "0.21.17") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.17") (d #t) (k 0)))) (h "12r3yx86fky0yapk0p7zwhc62immqc9h13a4skw2biyj5pd2xlzz")))

(define-public crate-swc_fast_graph-0.21.18 (c (n "swc_fast_graph") (v "0.21.18") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.18") (d #t) (k 0)))) (h "0yr6qw9xs82lclksr4mxr8jrsgimgm1ilg5qxlqv3dsyfm3sigli")))

(define-public crate-swc_fast_graph-0.21.19 (c (n "swc_fast_graph") (v "0.21.19") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.19") (d #t) (k 0)))) (h "0kc2mk45yb9d9kjvvn0hvn9l8xsc1svivspvnlhdvwshjinq83vl")))

(define-public crate-swc_fast_graph-0.21.20 (c (n "swc_fast_graph") (v "0.21.20") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.20") (d #t) (k 0)))) (h "0401a81wa2k6llgrs0fibww72163j4rdqxhr4hzj73m8cs0wchxa")))

(define-public crate-swc_fast_graph-0.21.21 (c (n "swc_fast_graph") (v "0.21.21") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.20") (d #t) (k 0)))) (h "0sa5xzdcjxy8z6wqfg7mx36kxxdll7r4zd9a12swhk4jpp6q7nsl")))

(define-public crate-swc_fast_graph-0.21.22 (c (n "swc_fast_graph") (v "0.21.22") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.26") (d #t) (k 0)))) (h "0p97w73ckgr0ak78wyvyqspk58abkrvyb2lsxb0xcqfiqd5xdzgk")))

