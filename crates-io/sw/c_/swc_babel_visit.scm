(define-module (crates-io sw c_ swc_babel_visit) #:use-module (crates-io))

(define-public crate-swc_babel_visit-0.1.0 (c (n "swc_babel_visit") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.2") (d #t) (k 0)) (d (n "swc_babel_ast") (r "^0.1") (d #t) (k 0)) (d (n "swc_visit") (r "^0.2") (d #t) (k 0)))) (h "14kacc51zlnvf7a0lk4q094fdj1xl19j0i9c8vrlccyw534dsav1")))

