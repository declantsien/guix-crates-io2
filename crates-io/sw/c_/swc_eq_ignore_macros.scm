(define-module (crates-io sw c_ swc_eq_ignore_macros) #:use-module (crates-io))

(define-public crate-swc_eq_ignore_macros-0.1.0 (c (n "swc_eq_ignore_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0plgi4sw7jykf8jzd9bxw6ljzm76csmgm1d6q7kkindf5q5213wc")))

(define-public crate-swc_eq_ignore_macros-0.1.1 (c (n "swc_eq_ignore_macros") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xml40xrh86z6fxg8nb9ba6ib5swqyw7p541nnx2p3366j34c80c")))

(define-public crate-swc_eq_ignore_macros-0.1.2 (c (n "swc_eq_ignore_macros") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s0fzc7a8jyvb68mv0zgafsvciygzn8njcsk9i455392gqv5va85")))

(define-public crate-swc_eq_ignore_macros-0.1.3 (c (n "swc_eq_ignore_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1r7875c62p34w2j5pkd9dd6jqyjdmpqgpgmm54jd6cqh8s5isnk9")))

