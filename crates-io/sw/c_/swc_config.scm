(define-module (crates-io sw c_ swc_config) #:use-module (crates-io))

(define-public crate-swc_config-0.1.0 (c (n "swc_config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0ih037cgplmj1abjb3fa3za8dfh0qvk8k5hbnkwghrnzvq7xzggk")))

(define-public crate-swc_config-0.1.1 (c (n "swc_config") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1m43zcvi77x5dhgpya27ljwi3y1s2mh5ksb2gvfrajy1avphbfxq")))

(define-public crate-swc_config-0.1.2 (c (n "swc_config") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0b3m1kd0pz1cikj9cixbh4k15pqa6ll9bffw8bmjmwgk20a745zw")))

(define-public crate-swc_config-0.1.3 (c (n "swc_config") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0kg0jjw1fvwgjf9cdha9d4k3vgpck1jxynfjm65qal1ckc9qxfvp")))

(define-public crate-swc_config-0.1.4 (c (n "swc_config") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.0") (d #t) (k 0)))) (h "04djn9v1qfkz4m7l4b9cb7wcq9ikp3q732g6sk68yjdr9qi3dpml")))

(define-public crate-swc_config-0.1.5 (c (n "swc_config") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.1") (d #t) (k 0)))) (h "17cmdx7xq5gjvk6lwb669h1qzbb3p6vckhrgqg3k85mv28ngrj49")))

(define-public crate-swc_config-0.1.6 (c (n "swc_config") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.2") (d #t) (k 0)))) (h "01cmz44wl641x4ixx8fza127154vpi57gfi093b8lci3j781d4qn")))

(define-public crate-swc_config-0.1.7 (c (n "swc_config") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.2") (d #t) (k 0)))) (h "0jsq749g1n747fb2njxk6wlz8nprzb9pas84k97dvy9q1njcg8cv")))

(define-public crate-swc_config-0.1.8 (c (n "swc_config") (v "0.1.8") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1vnllq0d2vmisyzhwv5zsf87mf45ykbkahn33hwgxrz84m12j868")))

(define-public crate-swc_config-0.1.9 (c (n "swc_config") (v "0.1.9") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.3") (d #t) (k 0)))) (h "16bcgb2jcixddgicbv6hdvm7yaxqp28qn4qnyk019rk0dgk88a0i")))

(define-public crate-swc_config-0.1.10 (c (n "swc_config") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sourcemap") (r "^6.4") (o #t) (d #t) (k 0)) (d (n "swc_cached") (r "^0.3.19") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.3") (d #t) (k 0)))) (h "0nynd5q2ni6nrnjrpjmgl4fz54y0vw9w86qniwqyy49ilrv3p7n2")))

(define-public crate-swc_config-0.1.11 (c (n "swc_config") (v "0.1.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sourcemap") (r "^6.4") (o #t) (d #t) (k 0)) (d (n "swc_cached") (r "^0.3.19") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.3") (d #t) (k 0)))) (h "017cfnkqmvrrkg32nxfpcbni4gcvzn4yj3cl22ih1chwmrg7r0yf")))

(define-public crate-swc_config-0.1.12 (c (n "swc_config") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sourcemap") (r "^8.0") (o #t) (d #t) (k 0)) (d (n "swc_cached") (r "^0.3.19") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.3") (d #t) (k 0)))) (h "1dggi6wjg987s8ylfxn3rdssdv3dlpl5gyca7il038r8bsn159xd")))

(define-public crate-swc_config-0.1.13 (c (n "swc_config") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "sourcemap") (r "^8.0.0") (o #t) (d #t) (k 0)) (d (n "swc_cached") (r "^0.3.19") (d #t) (k 0)) (d (n "swc_config_macro") (r "^0.1.4") (d #t) (k 0)))) (h "0di9iqnzkzwngfz6nagz3nh0zkqx0v5q550kafp1xgj6w64sdqbv")))

