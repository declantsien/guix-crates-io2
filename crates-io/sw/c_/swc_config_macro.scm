(define-module (crates-io sw c_ swc_config_macro) #:use-module (crates-io))

(define-public crate-swc_config_macro-0.1.0 (c (n "swc_config_macro") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0l926ys59xd34km337pkgnak3gbz3lmvn5xrd86wkm8gv41vqr7v")))

(define-public crate-swc_config_macro-0.1.1 (c (n "swc_config_macro") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0120vylwqa2j9blcqvwcxrwiai3r4l4mmvaqympc6psgincvkbbx")))

(define-public crate-swc_config_macro-0.1.2 (c (n "swc_config_macro") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ab81nxs5xhh5059qnsvsaf85x8v376bxyzh2m2vx0h0kb5amdg5")))

(define-public crate-swc_config_macro-0.1.3 (c (n "swc_config_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0szbdhm9wghbr8j2n5wlzi2jkradlb96q4chv4kjlcl2a3vp89cb")))

(define-public crate-swc_config_macro-0.1.4 (c (n "swc_config_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16c3mrmy67dd7m3kv7fckcv2s5z0l2x4ijpmaidskha2j09mcpvw")))

