(define-module (crates-io sw c_ swc_plugin_macro) #:use-module (crates-io))

(define-public crate-swc_plugin_macro-0.1.0 (c (n "swc_plugin_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0adjblrpzyn2l0pyb3yrn52123fnihrk57z9c5wk0svzpl0lb994")))

(define-public crate-swc_plugin_macro-0.2.0 (c (n "swc_plugin_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16pldbvnjc133n2m66jk6jm18znwp2mpmm0cmdl8625yyxbcgbvb")))

(define-public crate-swc_plugin_macro-0.2.1 (c (n "swc_plugin_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m63lk11706y5m003lhgvjx7ccdiqvj2qqa40jqs22g5bjbryy1i")))

(define-public crate-swc_plugin_macro-0.3.0 (c (n "swc_plugin_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aszmr7wqv2x0qpdy8dd8337hy2wv11aws9x8kpqi44cy17rlwcv")))

(define-public crate-swc_plugin_macro-0.3.1 (c (n "swc_plugin_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17hrsggmkc315vbklry4m21c7cplm34x65xzhs16c8rfhdd5a7yp")))

(define-public crate-swc_plugin_macro-0.4.0 (c (n "swc_plugin_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "125xk54rd4y90c9ai1lz6vdw58s75vmhaxqwzrsm7v6nc0i4szz6")))

(define-public crate-swc_plugin_macro-0.4.1 (c (n "swc_plugin_macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bm2qpa1r3d0wmx03f5zkn0haai7i915cn6ncpv3wx78yraam1cc")))

(define-public crate-swc_plugin_macro-0.5.0 (c (n "swc_plugin_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ma389rk45k65zsj75vycnvailb7yrgzhk5d2mqaww4xh6l8dwfs")))

(define-public crate-swc_plugin_macro-0.6.0 (c (n "swc_plugin_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09r54r1hpfkji2ah6c85q7br7s5dcxa40a9g5ljmwfvhb6ac21kf")))

(define-public crate-swc_plugin_macro-0.6.1 (c (n "swc_plugin_macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p480c0249991v4l3gw278ar091akkr19dpy3q6cwnpjrnczk23q")))

(define-public crate-swc_plugin_macro-0.7.0 (c (n "swc_plugin_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14idm0ffzzcqm6nzs2vky4432ng4ir4aylbbpxih77ixxavqqfcr")))

(define-public crate-swc_plugin_macro-0.8.0 (c (n "swc_plugin_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18wn1kasxz0b14pjpywjr98xgr5h8njasascf8bqvf7lmm6l898c")))

(define-public crate-swc_plugin_macro-0.8.1 (c (n "swc_plugin_macro") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z04b2vxyspmkqwi7c2afpj4asm14mw4wkk8p21l0hwnz7ig4lgq")))

(define-public crate-swc_plugin_macro-0.9.0 (c (n "swc_plugin_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03apr0icky7wdrfglhfnr123qhwiajji575q9v02ixy9cf4clhhl")))

(define-public crate-swc_plugin_macro-0.9.1 (c (n "swc_plugin_macro") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15156rk5jykdmx4zdz8zy741xymlmadjc96if6jiidpj05fbip4m")))

(define-public crate-swc_plugin_macro-0.9.2 (c (n "swc_plugin_macro") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18cd0f056mjvwvav9wikwyfspa9xjgwih6y8pr45158kwsz2y4fg")))

(define-public crate-swc_plugin_macro-0.9.3 (c (n "swc_plugin_macro") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04bzcbg706yyfg7m6hpkk3qlp50w8a7bn8vk4shhkw6vwxns5is5")))

(define-public crate-swc_plugin_macro-0.9.4 (c (n "swc_plugin_macro") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n8wxm1y4h38x6hp9hqf66jvry7m7qiy1pnf4acid5s2nk9ycpwb")))

(define-public crate-swc_plugin_macro-0.9.5 (c (n "swc_plugin_macro") (v "0.9.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11icanmkhbns85p68p04bazdjns6nbpzzj6wz4nzfv65inix1x68")))

(define-public crate-swc_plugin_macro-0.9.6 (c (n "swc_plugin_macro") (v "0.9.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z7f11ybll5n3b1xa5kj5gv83cf2j63p8cyj3pj33c7jb0baxchj")))

(define-public crate-swc_plugin_macro-0.9.7 (c (n "swc_plugin_macro") (v "0.9.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6adrlcy2yad4nlyk5x4x4j49qbhp61szafhmr46nn4xa4gyd4z")))

(define-public crate-swc_plugin_macro-0.9.8 (c (n "swc_plugin_macro") (v "0.9.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10j3wfzxppfmbif8b4v3z264hbq16bhn3ld47ih63bkszg5yaha2")))

(define-public crate-swc_plugin_macro-0.9.9 (c (n "swc_plugin_macro") (v "0.9.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16k9kpc9jl53z8565drqcqdmadjg9rzqz561rx472xjzmxlb2zp9")))

(define-public crate-swc_plugin_macro-0.9.10 (c (n "swc_plugin_macro") (v "0.9.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j8ix19n5iiy5zq03dlwk77bbfd8x31ghqghficz3gyzapwwl04c")))

(define-public crate-swc_plugin_macro-0.9.11 (c (n "swc_plugin_macro") (v "0.9.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hacidd9csmsxslwm8w9nh8zwwx246r0dvr9indawdrh298my2df")))

(define-public crate-swc_plugin_macro-0.9.12 (c (n "swc_plugin_macro") (v "0.9.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ss3yb172582yzla30v69g9y4v32l6lwils307n6pz8p4wdp30c4")))

(define-public crate-swc_plugin_macro-0.9.13 (c (n "swc_plugin_macro") (v "0.9.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mf8lc9xqhbpabl1252dpw720s85lmmsf8aljcf3l1jwimyih9c5")))

(define-public crate-swc_plugin_macro-0.9.14 (c (n "swc_plugin_macro") (v "0.9.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a0rd2ff2ldk53rwy2nipjw3kp4h59ailw6ncw0ah65dgl4gflaj")))

(define-public crate-swc_plugin_macro-0.9.15 (c (n "swc_plugin_macro") (v "0.9.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1dqwk33ja77k8c38yjy4rai7486r7f2ngf574z97d68b9hjlvq")))

(define-public crate-swc_plugin_macro-0.9.16 (c (n "swc_plugin_macro") (v "0.9.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1x3l6ldnc65ni5xj98jgl0dymh4n8035q58an8vhc1w42i4dncij")))

