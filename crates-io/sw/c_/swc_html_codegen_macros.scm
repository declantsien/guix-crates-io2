(define-module (crates-io sw c_ swc_html_codegen_macros) #:use-module (crates-io))

(define-public crate-swc_html_codegen_macros-0.1.0 (c (n "swc_html_codegen_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0lpj1wazkz8ixmsc0ajwm4v7a5f17cnwp4xmrgvz79l53jmy6xks")))

(define-public crate-swc_html_codegen_macros-0.2.0 (c (n "swc_html_codegen_macros") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0i4rjwsj1k38jdkyjv6kdpi8223d2mzqmm6mfx1fz9sd7qdyiq9y")))

(define-public crate-swc_html_codegen_macros-0.2.1 (c (n "swc_html_codegen_macros") (v "0.2.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "1hg0hv225wim9gj2cfbhv9qk9qc8wibk95wi9jirjpjlppj63iid")))

(define-public crate-swc_html_codegen_macros-0.2.2 (c (n "swc_html_codegen_macros") (v "0.2.2") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "14z528fighz006lq4lx5kn911h9v9iywdiip2mjpl8z3r86y1j7q")))

(define-public crate-swc_html_codegen_macros-0.2.3 (c (n "swc_html_codegen_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1zpcsh4xafx9wkqg81mfmrrfgza2p3dbf2j2n7g830rgqxwjg4a1")))

(define-public crate-swc_html_codegen_macros-0.2.4 (c (n "swc_html_codegen_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1a4b8280cmbjx7wskn9l1bib23vx7x4x8w9imgxk14m4ng5sd4z5")))

