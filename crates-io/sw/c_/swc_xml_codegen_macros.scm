(define-module (crates-io sw c_ swc_xml_codegen_macros) #:use-module (crates-io))

(define-public crate-swc_xml_codegen_macros-0.1.0 (c (n "swc_xml_codegen_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "0962a0183ngqr03xiirzww1mqyx299awnfscn49ijfmgvbr4dxb9")))

(define-public crate-swc_xml_codegen_macros-0.1.1 (c (n "swc_xml_codegen_macros") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold"))) (d #t) (k 0)))) (h "11h0v0kw82alwfwa43s8cq3895s8kjm8xjbpr22p5jckzg6njmad")))

(define-public crate-swc_xml_codegen_macros-0.1.2 (c (n "swc_xml_codegen_macros") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0cdrqj99z1sa63hc1fjwcafawc8hd9d9k1d2vi9cd974h405phl7")))

(define-public crate-swc_xml_codegen_macros-0.1.3 (c (n "swc_xml_codegen_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1sfx8v87jamvhi61xdl98iicqjayxjax35ki3b9gxw1kvvc2y5ws")))

(define-public crate-swc_xml_codegen_macros-0.1.4 (c (n "swc_xml_codegen_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0xx9kcvrbssb5xcq49nlab039xg2wnq83zlyjsx9js1qwl69l4ma")))

