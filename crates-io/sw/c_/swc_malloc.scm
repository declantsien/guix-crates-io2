(define-module (crates-io sw c_ swc_malloc) #:use-module (crates-io))

(define-public crate-swc_malloc-0.5.9 (c (n "swc_malloc") (v "0.5.9") (d (list (d (n "mimalloc-rust") (r "^0.2") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (f (quote ("disable_initial_exec_tls"))) (d #t) (t "cfg(all(target_os = \"linux\", target_env = \"gnu\", any(target_arch = \"x86_64\", target_arch = \"aarch64\")))") (k 0)))) (h "12hcw3g2xbmv9pb69r8750bgkngvi251p4lkmiwsfrj4h69r89ws")))

(define-public crate-swc_malloc-0.5.10 (c (n "swc_malloc") (v "0.5.10") (d (list (d (n "mimalloc-rust") (r "^0.2") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (f (quote ("disable_initial_exec_tls"))) (d #t) (t "cfg(all(target_os = \"linux\", target_env = \"gnu\", any(target_arch = \"x86_64\", target_arch = \"aarch64\")))") (k 0)))) (h "04dpr7bm11nh8dg738hgg9r5klfzfs0jwi3a59qqm6msn7p88nb7")))

