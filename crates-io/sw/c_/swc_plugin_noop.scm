(define-module (crates-io sw c_ swc_plugin_noop) #:use-module (crates-io))

(define-public crate-swc_plugin_noop-0.1.0 (c (n "swc_plugin_noop") (v "0.1.0") (d (list (d (n "swc_common") (r "^0.29.3") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.29.10") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "04cr0537x6jhh3apgm6ncpq163zfk5y4kc400hnyzn7s5zmwrg4z")))

(define-public crate-swc_plugin_noop-0.2.0 (c (n "swc_plugin_noop") (v "0.2.0") (d (list (d (n "swc_common") (r "^0.29.3") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.29.10") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "0j9ghhzg30pyshpy3lmzl10qhcq8jv5gb02wsywysd7apin54q15")))

(define-public crate-swc_plugin_noop-0.3.0 (c (n "swc_plugin_noop") (v "0.3.0") (d (list (d (n "swc_common") (r "^0.29.5") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.32.8") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "05m43849qd6kpc2zdgzafz7y5l1saq1pv9ab2mzg66zn7zcm1k62")))

(define-public crate-swc_plugin_noop-0.4.0 (c (n "swc_plugin_noop") (v "0.4.0") (d (list (d (n "swc_common") (r "^0.29.5") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.34.1") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "1w5yv6faqljc58ri9fdzgd6b8d9aj13r8447d9rin4jawyy4j5gp")))

(define-public crate-swc_plugin_noop-0.5.0 (c (n "swc_plugin_noop") (v "0.5.0") (d (list (d (n "swc_common") (r "^0.29.8") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.38.1") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "12vphbpws83q5fzmaykdmgfcpspc0h2fskfc85zgbc8iyidvfa0x")))

(define-public crate-swc_plugin_noop-0.6.0 (c (n "swc_plugin_noop") (v "0.6.0") (d (list (d (n "swc_common") (r "^0.29.8") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.38.1") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "0phn39zsi9xr8n1bcsgv1g8lq0cfriq165gg80y8rr03r3c4v1vi")))

(define-public crate-swc_plugin_noop-0.7.0 (c (n "swc_plugin_noop") (v "0.7.0") (d (list (d (n "swc_common") (r "^0.29.9") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.39.7") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "0idrnis03d31w80c3rmxs625p83ff1vbmimrjfiq94z0whg8b1fi")))

(define-public crate-swc_plugin_noop-0.8.0 (c (n "swc_plugin_noop") (v "0.8.0") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.0") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "01gj2ajdyciihd8iqz6mn00lfsvfarnbh0smvclz9sdsnhk4i5ck")))

(define-public crate-swc_plugin_noop-0.9.0 (c (n "swc_plugin_noop") (v "0.9.0") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.1") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "1v4ljvy4y8lc2hrbgpazvrvrqwixpir0ac239wkvriclral6kimd")))

(define-public crate-swc_plugin_noop-0.10.0 (c (n "swc_plugin_noop") (v "0.10.0") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.2") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "1y4wwccsz02s32m4x6jbn9j8wdc6kjik3jzd9inwgqgw6l45ilnp")))

(define-public crate-swc_plugin_noop-0.11.0 (c (n "swc_plugin_noop") (v "0.11.0") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.3") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "10r4var394xqrbacrgs4d9gjryyj9dblvj1v1ax7ra5pr4la5603")))

(define-public crate-swc_plugin_noop-0.12.0 (c (n "swc_plugin_noop") (v "0.12.0") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.3") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "1k5syd0n93vmf1k2cssk9h0k5fzjz9scixj3b6znkgzpnyicavy6")))

(define-public crate-swc_plugin_noop-0.12.1 (c (n "swc_plugin_noop") (v "0.12.1") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.3") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "1x2xq246kpby9sfkhq6f3jb5q6db8rhp9w8m76a6v1ikzbs8an0g")))

(define-public crate-swc_plugin_noop-0.12.2 (c (n "swc_plugin_noop") (v "0.12.2") (d (list (d (n "swc_common") (r "^0.29.10") (f (quote ("concurrent"))) (d #t) (k 0)) (d (n "swc_core") (r "^0.40.16") (f (quote ("plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)))) (h "0bbgmbi7rmb3ysfk8ci1vwixnsd6rvlarkw7xhk2a0imjpr5cbmg")))

