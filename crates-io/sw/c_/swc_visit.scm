(define-module (crates-io sw c_ swc_visit) #:use-module (crates-io))

(define-public crate-swc_visit-0.0.0 (c (n "swc_visit") (v "0.0.0") (h "1gv6v7516hxj4g893113kfkf7pkai6k1ywjknm9i113ih1fmsn2z")))

(define-public crate-swc_visit-0.1.0 (c (n "swc_visit") (v "0.1.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1jgy4fnlyd4rmx90r58dzdfpbyys9p2a8k8dgs48wjcpddgwv0v6")))

(define-public crate-swc_visit-0.1.1 (c (n "swc_visit") (v "0.1.1") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1caxqp5kl1ka3jcp2xl16z0mv5dh4vl0r73b24z38yx48jzshwxj")))

(define-public crate-swc_visit-0.2.0 (c (n "swc_visit") (v "0.2.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0hn7vkdw4h51svd0gppj0240j9kjd5hdkv8c2j0gjsvd9ibcwpmr")))

(define-public crate-swc_visit-0.2.1 (c (n "swc_visit") (v "0.2.1") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1vf1ff56xm2rilzm212d425vwqgxnzjchg8s7afj15ixcfqgkcyk")))

(define-public crate-swc_visit-0.2.2 (c (n "swc_visit") (v "0.2.2") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1n3i1097wr25gz0a094wqil0pc1id09a2zrzjjpc7jvpj46araac")))

(define-public crate-swc_visit-0.2.3 (c (n "swc_visit") (v "0.2.3") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "0pmy9hjvgp6qdlgw05j5g677y3qv15hf7mqsrcyj5808jycb377k")))

(define-public crate-swc_visit-0.2.4 (c (n "swc_visit") (v "0.2.4") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "17m31k7vyn82rm42bdq3nhv5jjzkh60vh24kalc12bh0ys1zwg2q")))

(define-public crate-swc_visit-0.2.5 (c (n "swc_visit") (v "0.2.5") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "1ryy5fkc1li5xbvv1d6d14vhdrpvn496m3frk6p6rq4l7caxcjxj")))

(define-public crate-swc_visit-0.2.6 (c (n "swc_visit") (v "0.2.6") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "1h1jj4qb9il49p6dw98b6ylsk4mmaanz3n5s9lb1hlaqnjhcl8x4")))

(define-public crate-swc_visit-0.2.7 (c (n "swc_visit") (v "0.2.7") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "1aciw2kcpq55l1y912mc0k9axjrrx4xdgs0lvvbvfz8l7ywbl7g8")))

(define-public crate-swc_visit-0.2.8 (c (n "swc_visit") (v "0.2.8") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.2.3") (d #t) (k 0)))) (h "15g61dqhwpd4qkz9hk67xvj9nb69m8jy88zf1gqdladbi13illgq")))

(define-public crate-swc_visit-0.3.0 (c (n "swc_visit") (v "0.3.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.3.0") (d #t) (k 0)))) (h "06ncvw4j99ad0mh6bknal19vm56mpnpjzqd13wia1a6jklvkkip5")))

(define-public crate-swc_visit-0.3.1 (c (n "swc_visit") (v "0.3.1") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0y3nfqs8fjmjfdx40w2alqn7qllcyvs5w1m6m96ichqbyhp22k0h")))

(define-public crate-swc_visit-0.3.2 (c (n "swc_visit") (v "0.3.2") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.3.2") (d #t) (k 0)))) (h "09vnlh4y9gi16a6lfiqc01n6vdyds19gf0yc6gfn22k1ikn2zsnj")))

(define-public crate-swc_visit-0.4.0 (c (n "swc_visit") (v "0.4.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.4.0") (d #t) (k 0)))) (h "124xcg5g75jb5cckjl2y5bsh6ap8b5npfh0ia7sh3rnvdfa6ryps") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.0 (c (n "swc_visit") (v "0.5.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.0") (d #t) (k 0)))) (h "0lmq1n4qx2qxzrvg2y1qrnl0qrak0na4b9wjdm0laq8wpkcc7a91") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.1 (c (n "swc_visit") (v "0.5.1") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.2") (d #t) (k 0)))) (h "1010yyx3fayj1nqgi4mzapd606mwag44bljmxnxic12ckmn846yf") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.2 (c (n "swc_visit") (v "0.5.2") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.3") (d #t) (k 0)))) (h "1xb9k8y5myvvg9nhacqhpyind59izndph5ajzmlv8kk1y80yym5p") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.3 (c (n "swc_visit") (v "0.5.3") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.4") (d #t) (k 0)))) (h "0c4igrs7klg8nfvkybp7snzx5iny0w90qpmzgi65q61y4avvrwl2") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.4 (c (n "swc_visit") (v "0.5.4") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.5") (d #t) (k 0)))) (h "05hfi0f51a9d41l3d3an1vd2jwxg5kysgqs6mjxxqbqqrxiij2j7") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.5 (c (n "swc_visit") (v "0.5.5") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.6") (d #t) (k 0)))) (h "1jlw1gjgpwhsarqr9s0x88xly978p6h57g7jl0hqw7224fgrkmfi") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.6 (c (n "swc_visit") (v "0.5.6") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.7") (d #t) (k 0)))) (h "0l9v9c4v0gc83gq350ly2gy44c9k7205qkz60jd513y5zga2shaz") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.7 (c (n "swc_visit") (v "0.5.7") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.8") (d #t) (k 0)))) (h "1hwgr77sc8mi65n9q64gvfd8j0bzjmmflg8pf7rin69dpdzk6z78") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.8 (c (n "swc_visit") (v "0.5.8") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.9") (d #t) (k 0)))) (h "1jdp48ikjk2gpcf6kkv7v0vnl2c9vz8qsq7gaam27ghsazc7hw5j") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.9 (c (n "swc_visit") (v "0.5.9") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.10") (d #t) (k 0)))) (h "06k96dk5n3y6girqpbf1nk0bdyf21lvf3k7bmkpyhjpbxmnj93im") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.10 (c (n "swc_visit") (v "0.5.10") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.11") (d #t) (k 0)))) (h "06qgygr8x8r5780cmx5i1sm1b72xch939vcg6nawp9v92a6kwnrz") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.11 (c (n "swc_visit") (v "0.5.11") (d (list (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.11") (d #t) (k 0)))) (h "0061w67872ygn29wwx6vz5x84bpi8251bdfgn1g41dxmqyzk3by6") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.12 (c (n "swc_visit") (v "0.5.12") (d (list (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.11") (d #t) (k 0)))) (h "0w604lmnlag9cpfka3xzvx9bhvbcryf82xcxyrmhsdnlzizy1kvx") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.13 (c (n "swc_visit") (v "0.5.13") (d (list (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.11") (d #t) (k 0)))) (h "1bnv85zf9xa87xwxxqpz6v0gmgdmfzc87vzzfz4fkgws51avwqq2") (f (quote (("path"))))))

(define-public crate-swc_visit-0.5.14 (c (n "swc_visit") (v "0.5.14") (d (list (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_visit_macros") (r "^0.5.12") (d #t) (k 0)))) (h "1fbig3pp2sf1h932wlnfzbsmlsl9q02r9m7ahd2r7jrxd3z12g84") (f (quote (("path"))))))

