(define-module (crates-io sw if swift_bot_api) #:use-module (crates-io))

(define-public crate-swift_bot_api-0.1.0 (c (n "swift_bot_api") (v "0.1.0") (d (list (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "09xfgc5hh2qyb5dzp29kghj13r09k5gxr45nq969da0lafz5sz9l")))

(define-public crate-swift_bot_api-0.1.1 (c (n "swift_bot_api") (v "0.1.1") (d (list (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "03460czd1s58ii6nsgij1ah2lhnxv0g9v3mcdg60vqs7w050ghyk")))

(define-public crate-swift_bot_api-0.1.2 (c (n "swift_bot_api") (v "0.1.2") (d (list (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "134q6v3nvi9pqys7ab44cwy4cq6qmaskn8bg1l36rqd7lgizmxpk")))

(define-public crate-swift_bot_api-0.1.3 (c (n "swift_bot_api") (v "0.1.3") (d (list (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "0s1r61cfnskxrhlylhrni8vn3i66bkyjyfr67qvf341bzrgrk661")))

(define-public crate-swift_bot_api-0.1.4 (c (n "swift_bot_api") (v "0.1.4") (d (list (d (n "rascam") (r "^0.0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "1q8lx1ghbnllw2dzx50p800wclikfyhn7v4x8mix0w5wqdrp0lf5")))

(define-public crate-swift_bot_api-0.1.5 (c (n "swift_bot_api") (v "0.1.5") (d (list (d (n "rascam") (r "^0.0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "0fyqfdphvxf7x03sbmwzwcp5h3gfzan735x4jfj3fx30sgnia3lp")))

(define-public crate-swift_bot_api-0.1.6 (c (n "swift_bot_api") (v "0.1.6") (d (list (d (n "rascam") (r "^0.0.2") (d #t) (k 0)) (d (n "rppal") (r "^0.15.0") (d #t) (k 0)))) (h "1m64q07sd32526hgxk6y938mg8137zr79s2hk5vj5b48n5msb2qj")))

