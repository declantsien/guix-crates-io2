(define-module (crates-io sw if swift_vec) #:use-module (crates-io))

(define-public crate-swift_vec-0.1.0 (c (n "swift_vec") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0rmhvnxrw744blqj5nwvapkk6i8jrm2gk3lz80pfmq66f9x2wfx9")))

(define-public crate-swift_vec-0.1.1 (c (n "swift_vec") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0killgjk4cmgrvkni7bjwd8lbjkrqykmzw39yvxwws57nsiiwnnw")))

(define-public crate-swift_vec-0.1.2 (c (n "swift_vec") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "02v23x1y2s0wlw0jiyrn63799b7ps1na499npipm4hj7ihs584m4")))

(define-public crate-swift_vec-0.1.3 (c (n "swift_vec") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "090hs13ipxi0mbmbp6gzg932ldrhw4a0f8y1xg1lbf31s3msn45y")))

(define-public crate-swift_vec-0.2.0 (c (n "swift_vec") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "10i2idffb2ldaxby2srj15virir98kxfprhy63wzbv9d4q1d9vni")))

