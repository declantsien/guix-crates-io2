(define-module (crates-io sw if swift-bridge-build) #:use-module (crates-io))

(define-public crate-swift-bridge-build-0.1.0 (c (n "swift-bridge-build") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "184mfmyxldnlrpfyh4m5jpnnvyayac597zxfmmc5q0r885ca5937")))

(define-public crate-swift-bridge-build-0.1.1 (c (n "swift-bridge-build") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "107bajgqpdlm5kikhx22ahc60i5gf8ny4g9vvjd49hyqsgxpmpri")))

(define-public crate-swift-bridge-build-0.1.2 (c (n "swift-bridge-build") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17p95jv0dlqcgqw0qkh6v8zcbr2jjk3jag87sxp5qsg601bw0nms")))

(define-public crate-swift-bridge-build-0.1.3 (c (n "swift-bridge-build") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "172xc2m9g3jj8kxff9lamgwpfn0g74vqzpmj9wsy4n51zi8lwmqh")))

(define-public crate-swift-bridge-build-0.1.4 (c (n "swift-bridge-build") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0g3lbpmbwma6qwdq6icafvxsi0rqykhd3l05yvsfhfvgs0w4xwk2")))

(define-public crate-swift-bridge-build-0.1.5 (c (n "swift-bridge-build") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vvvwbd09awghwxpsi0qlv3f61m05p4lmizrlxv91w67wgmcljhp")))

(define-public crate-swift-bridge-build-0.1.6 (c (n "swift-bridge-build") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zjmhs3vgfjrvws98klj2f3xjbh016bz9knkb27yfpk80p3y8rdv")))

(define-public crate-swift-bridge-build-0.1.7 (c (n "swift-bridge-build") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.7") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13xvc6fvzxhmpzi1kvq966al316i6nncnj5p1wh31qir4xkk9pza")))

(define-public crate-swift-bridge-build-0.1.8 (c (n "swift-bridge-build") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.8") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1sdrzvadvqfjgqgxxwsmy408l9f86h1kvcb44vy8jkb67ansj7p7")))

(define-public crate-swift-bridge-build-0.1.9 (c (n "swift-bridge-build") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "170ymk96qgm5nw4az3gqlhr1igh7v6gq5v3j4kmyyil20wjv1g3i")))

(define-public crate-swift-bridge-build-0.1.10 (c (n "swift-bridge-build") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.10") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xjkbzz6lphlszc5zy19kb6sbiayr9yqvxq6q5zcxr6ydav2k1ba")))

(define-public crate-swift-bridge-build-0.1.11 (c (n "swift-bridge-build") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.11") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jb8z198h2zk086dsn1siclyzzr18fy8qwh29qpmz1r51lqw56c3")))

(define-public crate-swift-bridge-build-0.1.12 (c (n "swift-bridge-build") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.12") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f3cw9dipxl8qy0p7bcbrjnqryxf3y3yw55v9f528x6zai7zifv1")))

(define-public crate-swift-bridge-build-0.1.13 (c (n "swift-bridge-build") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.13") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10wk80dlcgmvsdw8ndg3q60qwqx8sh9iljmj0al6ip1rl3kapjsb")))

(define-public crate-swift-bridge-build-0.1.14 (c (n "swift-bridge-build") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.14") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pyk5xr6hnhq9vqak56jxfsxkn19p3n3k0sf6sc9l1lfhksmh0q1")))

(define-public crate-swift-bridge-build-0.1.15 (c (n "swift-bridge-build") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.15") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1my56kkir01x5apq34cw51l4i018g6kywgzqfblj8fjns3yn3v3i")))

(define-public crate-swift-bridge-build-0.1.16 (c (n "swift-bridge-build") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.16") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0iwg97bj0lxl14wb3w28ysqm7qqbppnj9gv4bfwwrzp198nbpj91")))

(define-public crate-swift-bridge-build-0.1.17 (c (n "swift-bridge-build") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.17") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m93fv7mixc1sc6gcdik6bs699mb4xmrr8hinvhkbdz1f8cx802v")))

(define-public crate-swift-bridge-build-0.1.18 (c (n "swift-bridge-build") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.18") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0842pidfda4pkkkp3b4dw8vpc3r9rbim3xnjzxlhsx962qmmj61j")))

(define-public crate-swift-bridge-build-0.1.19 (c (n "swift-bridge-build") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.19") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wfkxqbq4x2ynsmnw636x19zzcph085fs7dxn5968z9jmqd82p3b")))

(define-public crate-swift-bridge-build-0.1.20 (c (n "swift-bridge-build") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.20") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kqcnrb4q5m8gmyqdsmnfbrkv5hr5x4f94hnjj84g4rrb643r33c")))

(define-public crate-swift-bridge-build-0.1.21 (c (n "swift-bridge-build") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.21") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0s575lk8ndd1a17nw63bxwakg9bwmjz28cz18k840sayx4n863pb")))

(define-public crate-swift-bridge-build-0.1.22 (c (n "swift-bridge-build") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.22") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k6y772a2svgnic1pr0hkd9nzf529wd9ka2ijpqbpdsxabh5dmq2")))

(define-public crate-swift-bridge-build-0.1.23 (c (n "swift-bridge-build") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.23") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0fs79gllhi2lhp02zgby2999dc4i316rfvj1408aag5kkv6d3snh")))

(define-public crate-swift-bridge-build-0.1.24 (c (n "swift-bridge-build") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.24") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1nd4xijngf7hrgivlisjrj4xwkzcdik59lak9i8gz7m5va5kz3cd")))

(define-public crate-swift-bridge-build-0.1.25 (c (n "swift-bridge-build") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.25") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08pd38vsi537apm3w2fbcm1k67bc4yw6zqdfx2bxby9dahgsk6r6")))

(define-public crate-swift-bridge-build-0.1.26 (c (n "swift-bridge-build") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.26") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1c23vxryp25d6zb9n2672xygvkags0jrdlhmcf5h0d0hjhgmzi7n")))

(define-public crate-swift-bridge-build-0.1.27 (c (n "swift-bridge-build") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.27") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vynil7dpdnc5q2pcrkd9jfcbhcjmrcwgv3ai9mk6m3s2ksbqvq5")))

(define-public crate-swift-bridge-build-0.1.28 (c (n "swift-bridge-build") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.28") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0y7syflld0xfqcpj8yxjq5y1cps574yvjhxis6q7r0lia0s40jka")))

(define-public crate-swift-bridge-build-0.1.29 (c (n "swift-bridge-build") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.29") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0clavhhri2an9y13yd33qfrzdz272nwdfqvgcgri97krvvbxgkcm")))

(define-public crate-swift-bridge-build-0.1.30 (c (n "swift-bridge-build") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.30") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1bi1mkixby06d7x5c92f05xp1ckf1sk2v48nsrz5hn0jvign55jh")))

(define-public crate-swift-bridge-build-0.1.31 (c (n "swift-bridge-build") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.31") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1gip5bparf982an55mnr3v00qyi2w23an87zqdxw25x0jiwyykhr")))

(define-public crate-swift-bridge-build-0.1.32 (c (n "swift-bridge-build") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.32") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0m710963z2wn674pqjf61p7zfsh4v1h2z5pg2w4hrdkhra7l75zn")))

(define-public crate-swift-bridge-build-0.1.33 (c (n "swift-bridge-build") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.33") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1h5ixlqkf9j97jwpbzy3im5cq2fgk2gagdyvbaypvycdkakc8vvz")))

(define-public crate-swift-bridge-build-0.1.34 (c (n "swift-bridge-build") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.34") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1k8yxx09ng2y3a47f9ray43dbrgd8syzfl5xs30y7yqk4rs3r01r")))

(define-public crate-swift-bridge-build-0.1.35 (c (n "swift-bridge-build") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.35") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "02hl2fvm8x2dkn5vnvs9zi3kjhqyyc9l0d7k88jqlgk8m9smlpf9")))

(define-public crate-swift-bridge-build-0.1.36 (c (n "swift-bridge-build") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.36") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0rj2jka43y37yb6s9zxz12d23iy8jjyrgb93rxry8qvicbs627ic")))

(define-public crate-swift-bridge-build-0.1.37 (c (n "swift-bridge-build") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.37") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "086jaa4gv3q35yscni52vjya64vy61alimwx142x05vyqljbifdi")))

(define-public crate-swift-bridge-build-0.1.38 (c (n "swift-bridge-build") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.38") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1b58qhcdbf2jc4j8cynj518vzlks4r0hg3vcvn8ds02z8jax8a0r")))

(define-public crate-swift-bridge-build-0.1.39 (c (n "swift-bridge-build") (v "0.1.39") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.39") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0ib6z4pqiqvf6xyknm84mdhwx24wgxawsw1hrmig45086zmk4lbq")))

(define-public crate-swift-bridge-build-0.1.40 (c (n "swift-bridge-build") (v "0.1.40") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.40") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "13i6z7kj5kpx5h1sf38z1d5naqh6gmwxdbq1dyzlqlf1w6mk56v4")))

(define-public crate-swift-bridge-build-0.1.41 (c (n "swift-bridge-build") (v "0.1.41") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.41") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "11qfqn2xfx9bpvcsjxp375f73g7k2gwp6n4p6z7mynfn2x3w9wnj")))

(define-public crate-swift-bridge-build-0.1.42 (c (n "swift-bridge-build") (v "0.1.42") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.42") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "09yhg3qgmdfzis0fxfhgsiyvrrw8a1hp3hs1hrk69jg9ci7djgjv")))

(define-public crate-swift-bridge-build-0.1.43 (c (n "swift-bridge-build") (v "0.1.43") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.43") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0rd87q3hqvmr5z59821gibrhnikn3p4baqsxjdbvzz0djrj92g1b")))

(define-public crate-swift-bridge-build-0.1.44 (c (n "swift-bridge-build") (v "0.1.44") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.44") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0dmwc34qdj06cf6pm84rl51hk883w9z0rajcpvp2nzkvqnsdx24g")))

(define-public crate-swift-bridge-build-0.1.45 (c (n "swift-bridge-build") (v "0.1.45") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.45") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1rz316djkg2ryq9iqps9syk4zhrxqmnfq8dc5wjsxb5frh7r7vn4")))

(define-public crate-swift-bridge-build-0.1.46 (c (n "swift-bridge-build") (v "0.1.46") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.46") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0wxylvaishapibammdibyp89nz04z79fdb6wyf7b61p2kcl0j9al")))

(define-public crate-swift-bridge-build-0.1.47 (c (n "swift-bridge-build") (v "0.1.47") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.47") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "007mzs47717x8d4fvhlgb8bvvi50g780r6d9n6y9f2v5na5izx6x")))

(define-public crate-swift-bridge-build-0.1.48 (c (n "swift-bridge-build") (v "0.1.48") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.48") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1f0a5bb43s0akvp7n3s61rgbgd97raj1qxxzf3iq5ciz0pybk9n6")))

(define-public crate-swift-bridge-build-0.1.49 (c (n "swift-bridge-build") (v "0.1.49") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.49") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0s31rn4r0mamd54107p306kqnl1m1gqk9fvrpx7wnvv2vcir6gd4")))

(define-public crate-swift-bridge-build-0.1.50 (c (n "swift-bridge-build") (v "0.1.50") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.50") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1pxxlrmryqs1kvkf5c9gby8zvx5345h03rxjq2id4nazg5brrbn5")))

(define-public crate-swift-bridge-build-0.1.51 (c (n "swift-bridge-build") (v "0.1.51") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.51") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1ps3znfjnkn74rrwff5slyaq52yh8fzvw1jcswg6lwr2r5yp4vr8")))

(define-public crate-swift-bridge-build-0.1.52 (c (n "swift-bridge-build") (v "0.1.52") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.52") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0fsw32rh3h3fw8r129pf1i3qsl5pq2kk4ma4ybcbg8dgjkf84hms")))

(define-public crate-swift-bridge-build-0.1.53 (c (n "swift-bridge-build") (v "0.1.53") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.53") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "00113zbqczd3fkbbn723yam2ibcz7j2c9ya695qgn49sqs26q8d0")))

(define-public crate-swift-bridge-build-0.1.54 (c (n "swift-bridge-build") (v "0.1.54") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.54") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0xj6ha3mimly0ki1vqckxc7ha3cp55p2qhq6da0yj66gn6xzx0kg")))

(define-public crate-swift-bridge-build-0.1.55 (c (n "swift-bridge-build") (v "0.1.55") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swift-bridge-ir") (r "^0.1.55") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1paz4izqqicxj9lgf84vl9n71cj2bs7m45xixapramy3v395d0kv")))

