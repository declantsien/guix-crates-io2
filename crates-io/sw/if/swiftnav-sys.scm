(define-module (crates-io sw if swiftnav-sys) #:use-module (crates-io))

(define-public crate-swiftnav-sys-0.7.0 (c (n "swiftnav-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0qzmws3b8kmsb5k28q9p2c7qyaz33n0x4yf9jgy7djm2819vf6ms") (f (quote (("testcpp"))))))

(define-public crate-swiftnav-sys-0.8.0 (c (n "swiftnav-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "0f8519b8yb5svnqjppdvk2cd7s5d5nypabl77qq38sig6b1rbd3y") (f (quote (("testcpp"))))))

(define-public crate-swiftnav-sys-0.8.1 (c (n "swiftnav-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "1yrj3axyqyphmp5b1wzm6b2n1l43q371ipw6c7vpxxgxlg3b0smg") (f (quote (("testcpp")))) (r "1.56.1")))

(define-public crate-swiftnav-sys-0.8.2 (c (n "swiftnav-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "0mf96n0pqwypdyhxf6ayfxjfnf81c5sjx7x71mmnwxf25kknp3wm") (f (quote (("testcpp")))) (r "1.56.1")))

(define-public crate-swiftnav-sys-0.8.3 (c (n "swiftnav-sys") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "02a5gf32x71r7z4r8hslxsrq8yrvcmjc86cmkw34v850ns6hc4h2") (f (quote (("testcpp")))) (r "1.62.1")))

