(define-module (crates-io sw if swiftnav) #:use-module (crates-io))

(define-public crate-swiftnav-0.7.0 (c (n "swiftnav") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "swiftnav-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1n74gp95mcags8dfvh43hxigrk4h5mblnc9xc0k0cfff1acb29qk")))

(define-public crate-swiftnav-0.8.0 (c (n "swiftnav") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "swiftnav-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1zn536nrwgnqqflfywww6i7bdmd3y5jyn7n8s2vraa7bvpfmsvsr")))

(define-public crate-swiftnav-0.8.1 (c (n "swiftnav") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "swiftnav-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1r69z2aay9hq5h9sa2h32j07my95l8fx6wr6r3yccf4ij1n1jgm5") (r "1.56.1")))

(define-public crate-swiftnav-0.8.2 (c (n "swiftnav") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "swiftnav-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1j00ca0imr6zfzcj5w734c70k8pqzbkr02r8f2g7wr0ak6v3g978") (r "1.56.1")))

(define-public crate-swiftnav-0.8.3 (c (n "swiftnav") (v "0.8.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "swiftnav-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1d6ahv76mgg95583cvrzb9qbpxkxk5crd9ijl1ssq7f43n7yhsh1") (r "1.62.1")))

