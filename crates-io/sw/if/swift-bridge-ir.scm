(define-module (crates-io sw if swift-bridge-ir) #:use-module (crates-io))

(define-public crate-swift-bridge-ir-0.1.0 (c (n "swift-bridge-ir") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19hpzfi5nhyag209507cnc9ry03gx8rzfzrvkyfip9xdvkwmy44i")))

(define-public crate-swift-bridge-ir-0.1.1 (c (n "swift-bridge-ir") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzjwjf5r92a4vr0m1cg7zd6wn4wbxam490ys8d4n2kdfn7j0y37")))

(define-public crate-swift-bridge-ir-0.1.2 (c (n "swift-bridge-ir") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0835p8mprz00l3ibnbgmp1g4a673a938sydjwx6zx3yl3y46i3la")))

(define-public crate-swift-bridge-ir-0.1.3 (c (n "swift-bridge-ir") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "037nksryzd328w3jk721jbh2dqik0s8r4lyl32zi93wnz4yg3jp2")))

(define-public crate-swift-bridge-ir-0.1.4 (c (n "swift-bridge-ir") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3nf6i4m4mr7igfhjlx8kia49jygjyz7z59yblqimxsh6gr4z5c")))

(define-public crate-swift-bridge-ir-0.1.5 (c (n "swift-bridge-ir") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "064j9bpxc8wsd6mxrvhlsp2i66vnfcy8wbqxm459qj984w344z9d")))

(define-public crate-swift-bridge-ir-0.1.6 (c (n "swift-bridge-ir") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "057v0ga6s0fxh2nh5riih6idqrqhf86fkw73m3gmpjxbldrd3xkj")))

(define-public crate-swift-bridge-ir-0.1.7 (c (n "swift-bridge-ir") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lqki55wf94l1fbq9g1w7903hw4s9086wgsdaikjivshyi5pr57z")))

(define-public crate-swift-bridge-ir-0.1.8 (c (n "swift-bridge-ir") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jrjhyj23pkla60lnjqrwag30ja33wk4lm5kzzdp2ij7if1b2v8b")))

(define-public crate-swift-bridge-ir-0.1.9 (c (n "swift-bridge-ir") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hf5wrf6k3mysj8jsiywgjkynijjfwcr0wifvqhh84ijhsjx6wfk")))

(define-public crate-swift-bridge-ir-0.1.10 (c (n "swift-bridge-ir") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07n4crrl9dr63f4j20ng1xnjh70zh263d0q81c6i67hp4009z1al")))

(define-public crate-swift-bridge-ir-0.1.11 (c (n "swift-bridge-ir") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vdp793s68cxdl4njnb9v5xhchadng7smy5mkyfqhcm3q857xl0y")))

(define-public crate-swift-bridge-ir-0.1.12 (c (n "swift-bridge-ir") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "053h9gfj0l975d9m0k8c7gxv8clxyvxdk6wqy2fs0fvck4wncr9d")))

(define-public crate-swift-bridge-ir-0.1.13 (c (n "swift-bridge-ir") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c9ls1lk7yhdzrkhkb7qqqqyzg1d75jhqgj6kg79g8f49pwsxaq1")))

(define-public crate-swift-bridge-ir-0.1.14 (c (n "swift-bridge-ir") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2wgndxvbi4m7kcz2jplal8qyz4m096jkn1qvbld8ngg80gdp9k")))

(define-public crate-swift-bridge-ir-0.1.15 (c (n "swift-bridge-ir") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m074kmz1wi1g5qpi1yaabyx3h155lhknsq62jk3bhi3fm4nq5hl")))

(define-public crate-swift-bridge-ir-0.1.16 (c (n "swift-bridge-ir") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jn6gcjfy9258xc7gyv1mzb6ndppldbxm86xwfnzqji4njri9ds7")))

(define-public crate-swift-bridge-ir-0.1.17 (c (n "swift-bridge-ir") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fqmhrihw9kgygla1l4yc2nick3dp2b5rarsifiycgnzgn7jzv28")))

(define-public crate-swift-bridge-ir-0.1.18 (c (n "swift-bridge-ir") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qdhl5gznrdwb3j6lb8nwara1kx5lnncv7vhfn6xw1zg1yavz4mk")))

(define-public crate-swift-bridge-ir-0.1.19 (c (n "swift-bridge-ir") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "178v6532i6kalriy4maczz3vqnnm4w73h2bl7wri85dwqrklcbh7")))

(define-public crate-swift-bridge-ir-0.1.20 (c (n "swift-bridge-ir") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ckknarzdq4qaypaahhvl0kayylxhpg0ym3776j3na3irf3zx0cm")))

(define-public crate-swift-bridge-ir-0.1.21 (c (n "swift-bridge-ir") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0292szbb4pw1if6bjqffy1q7vp2cq2rnmh96vym0xcm5hwgv7931")))

(define-public crate-swift-bridge-ir-0.1.22 (c (n "swift-bridge-ir") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cc9lxpydpi5rabfpadb0kwn1ff6d8fgd18h40xak9769j2xc79k")))

(define-public crate-swift-bridge-ir-0.1.23 (c (n "swift-bridge-ir") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rbwcvf46q09bz7lrmlhjr51fgv3c74i0i8dsql37v9m5hgdp602")))

(define-public crate-swift-bridge-ir-0.1.24 (c (n "swift-bridge-ir") (v "0.1.24") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x86ba150bsw6d91mpqcx3v3zqnr7zlh36vn689klrhm9r8hy1qz")))

(define-public crate-swift-bridge-ir-0.1.25 (c (n "swift-bridge-ir") (v "0.1.25") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09bhzh8sra1g39i3qax0p6waqzihdczs95kmjpp0zl0y68asdlch")))

(define-public crate-swift-bridge-ir-0.1.26 (c (n "swift-bridge-ir") (v "0.1.26") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02dnpwz6mc6xpw43sfgsi00jpfhz2ibgjnm6pkwwp08yfwhfhzji")))

(define-public crate-swift-bridge-ir-0.1.27 (c (n "swift-bridge-ir") (v "0.1.27") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11dn4cfilwnx1g881gqqwjhk67vjvvh89djnv5wa0qbpmjmhdg69")))

(define-public crate-swift-bridge-ir-0.1.28 (c (n "swift-bridge-ir") (v "0.1.28") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q4pbwmx4wlsh29ghkj678y1lqsk1rg6z9m87jsfk2q77cibhd15")))

(define-public crate-swift-bridge-ir-0.1.29 (c (n "swift-bridge-ir") (v "0.1.29") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v7xkc63vb9fx5c0yjfs87j08s8qxkk3l92mvdzpsgqpwvpzg5dq")))

(define-public crate-swift-bridge-ir-0.1.30 (c (n "swift-bridge-ir") (v "0.1.30") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vxwg8d36150mr5kg8crvm7sjf2yf85drkhpazksxgizjmrpn13f")))

(define-public crate-swift-bridge-ir-0.1.31 (c (n "swift-bridge-ir") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n9zd5vnk3rg6dmylmfj43nnc4d4vvaw9in5p5zxzimd79jh8a1p")))

(define-public crate-swift-bridge-ir-0.1.32 (c (n "swift-bridge-ir") (v "0.1.32") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06yxs6fss8wlfi5gz2r4c7gny2gb4lk3jrvcm2k1hmqay3nfs6z1")))

(define-public crate-swift-bridge-ir-0.1.33 (c (n "swift-bridge-ir") (v "0.1.33") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0k1qqjmjszgqizag98z67ja9n16ryrq65c8rjrllsl7i5y1ylb5y")))

(define-public crate-swift-bridge-ir-0.1.34 (c (n "swift-bridge-ir") (v "0.1.34") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vjg9c0s017v4pad89k76kyi0b7rpyqq0qdwmkfbs2hpxg11vh7r")))

(define-public crate-swift-bridge-ir-0.1.35 (c (n "swift-bridge-ir") (v "0.1.35") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cbqfy6ciq1lzk5whgm5ibjhzihvnxy34y5a80ly9ix8q7i39988")))

(define-public crate-swift-bridge-ir-0.1.36 (c (n "swift-bridge-ir") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yc9gbgbhcsga5clmqiiih4nj29kafz0sva5dlflawflrirfnk6w")))

(define-public crate-swift-bridge-ir-0.1.37 (c (n "swift-bridge-ir") (v "0.1.37") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aj06abs6vgsvs34vaas3ggs2ymyyr6j2nwnbch6s2a236jd0irq")))

(define-public crate-swift-bridge-ir-0.1.38 (c (n "swift-bridge-ir") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10d6ylnhcx5vrf4kzm6myzri5j575b30la44f54609f8cnzr8zrx")))

(define-public crate-swift-bridge-ir-0.1.39 (c (n "swift-bridge-ir") (v "0.1.39") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16fb45w8zal74rlg3qdc0xyf9vsy1y38lh02kb3mn86mx2zdvlcl")))

(define-public crate-swift-bridge-ir-0.1.40 (c (n "swift-bridge-ir") (v "0.1.40") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrxm12qjdz6g3jjkw6iiixhygsnlmzrmj3d2gnc965s9qy35mj0")))

(define-public crate-swift-bridge-ir-0.1.41 (c (n "swift-bridge-ir") (v "0.1.41") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08jclv6jdhdj6gqmjrvp69zmilqh374z8m5aadhlw8nl96k2z07j")))

(define-public crate-swift-bridge-ir-0.1.42 (c (n "swift-bridge-ir") (v "0.1.42") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "143kxvxzv9zvvvf0hdlsa7d8ldsg56j2m9yj4yk4csxckr46c42h")))

(define-public crate-swift-bridge-ir-0.1.43 (c (n "swift-bridge-ir") (v "0.1.43") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fhn7aqq0bmhb4fic413ynkmrqq2x3zr70fp8dns40lc8izg734b")))

(define-public crate-swift-bridge-ir-0.1.44 (c (n "swift-bridge-ir") (v "0.1.44") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rq0w7fndh6ffccvdv6xp348mqzbg8cjiwsgld6bnqcc7wsribgs")))

(define-public crate-swift-bridge-ir-0.1.45 (c (n "swift-bridge-ir") (v "0.1.45") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hkav91yzq1r54208qly0gjnpwpqqbyv1yj210ny727bpn2vzqrb")))

(define-public crate-swift-bridge-ir-0.1.46 (c (n "swift-bridge-ir") (v "0.1.46") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "186synqq9czc49b7j95w3d53w4k6dv0g8mp18d7dblbkyvmwx0nn")))

(define-public crate-swift-bridge-ir-0.1.47 (c (n "swift-bridge-ir") (v "0.1.47") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1izdnwz65kqihq36b7d2zbwdyh5ayph4fgdxdm5xwijwqkiivgji")))

(define-public crate-swift-bridge-ir-0.1.48 (c (n "swift-bridge-ir") (v "0.1.48") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aqz7vk4k3g1f242vw4vmp6cmif1znfrrzikddv3llnziangc5s6")))

(define-public crate-swift-bridge-ir-0.1.49 (c (n "swift-bridge-ir") (v "0.1.49") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00fa18iwpyxb7xq43m36svadq6p3g8h384w1g1f19x93vapifivn")))

(define-public crate-swift-bridge-ir-0.1.50 (c (n "swift-bridge-ir") (v "0.1.50") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m7n2zfqirh5ibmp9sjwi4gbg9lxzr7ahp6w9l6dd4wvxxn06vf8")))

(define-public crate-swift-bridge-ir-0.1.51 (c (n "swift-bridge-ir") (v "0.1.51") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p474wg3ma418hk8i8mbfcy74b87m3xd9xh13jy0mqmxk9zfjk9b")))

(define-public crate-swift-bridge-ir-0.1.52 (c (n "swift-bridge-ir") (v "0.1.52") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1njqvi87qn64xpqj71na3s9h5y5l9ikydpdmv6chca22zf16l2fj")))

(define-public crate-swift-bridge-ir-0.1.53 (c (n "swift-bridge-ir") (v "0.1.53") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03sm0rq49cq944wz6d5hi51z5vlljsw81pcj66jv3z0lcwq3cc0q")))

(define-public crate-swift-bridge-ir-0.1.54 (c (n "swift-bridge-ir") (v "0.1.54") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jg7rpm009n0nh6h07vncdzbkfscj73m3yqqp26ygal46qa2bi0m")))

(define-public crate-swift-bridge-ir-0.1.55 (c (n "swift-bridge-ir") (v "0.1.55") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wg0iifrs64kim5yzgmh7isa6qqzzgpa054kihzaqzxmi3p0g152")))

