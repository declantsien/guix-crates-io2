(define-module (crates-io sw if swifft) #:use-module (crates-io))

(define-public crate-swifft-0.1.0 (c (n "swifft") (v "0.1.0") (d (list (d (n "ff") (r "^0.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "halo2_proofs") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)))) (h "1hrq2zqs4rp1w79ncx7i5fynd7pwm3pn6nd9yqa5kik3d9ghfpm0")))

