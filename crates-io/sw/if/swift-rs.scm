(define-module (crates-io sw if swift-rs) #:use-module (crates-io))

(define-public crate-swift-rs-0.1.0 (c (n "swift-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "081f011rjygl00pf05pwvaxs7izqi8yd48d6v770fgw88is08mjf")))

(define-public crate-swift-rs-0.2.0 (c (n "swift-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ddq58n7n3y5ans8qk3xlcv0i71ywsdmzpwgf5a9587fvsyd0q9r")))

(define-public crate-swift-rs-0.2.1 (c (n "swift-rs") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k3jkvkwnms22pjnhbm1rgs2xb1a38yl5p3dzank0i13j390d14b")))

(define-public crate-swift-rs-0.2.2 (c (n "swift-rs") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zn8jh891s9iqwbgf1p0sdh1qy2l4a27y2m6zc7cda8cra9wp7s2")))

(define-public crate-swift-rs-0.2.3 (c (n "swift-rs") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bvx2gfn36snjiigkzv1j2202hs23anv03lsgnh7ks1vkp3aa6p3")))

(define-public crate-swift-rs-0.3.0 (c (n "swift-rs") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02flm2lgsgc5cg7cp5vynhn36mavk41sf3kzsyd4y5684khpdr6m") (f (quote (("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.0 (c (n "swift-rs") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "0aqylvz3n2ikw0a7wr089fns5vdchjfi8bm1jdp2zxw6n363v46w") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.1 (c (n "swift-rs") (v "1.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "0wy859f7a6jz96x5f5zxpjfkm342wkpcybs28hbaka028f8g0vw0") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.2 (c (n "swift-rs") (v "1.0.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "0zmj8g2rgw9phn9nzhmaaxr8a95vsgh1y8j2x7dxz5ia7i9xs1i7") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.3 (c (n "swift-rs") (v "1.0.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "0sfp04d6nhn4zqvpsqy7834mkaw3s15mpz92z87x5kh29ps5barf") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.4 (c (n "swift-rs") (v "1.0.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "13k8p5zb39138110fiz2iyj0x7ikrfhk0m3pqnysgpvnf5j7v9lg") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.5 (c (n "swift-rs") (v "1.0.5") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "1mn2ha250356yc9m860qwb7qpb2mfs59yhjgc444izsz5dpivr85") (f (quote (("default") ("build" "serde" "serde_json"))))))

(define-public crate-swift-rs-1.0.6 (c (n "swift-rs") (v "1.0.6") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.10") (d #t) (k 2)))) (h "01324x5507k1kvvf0i6lavhbm98243rn29df2y6iyc5nfy2vbg8v") (f (quote (("default") ("build" "serde" "serde_json"))))))

