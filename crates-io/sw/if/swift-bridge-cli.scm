(define-module (crates-io sw if swift-bridge-cli) #:use-module (crates-io))

(define-public crate-swift-bridge-cli-0.1.0 (c (n "swift-bridge-cli") (v "0.1.0") (h "0dfwk9dwm3v2d75x1jh0nk0yy8frrmxlz06wz6v02y1f9bncrf9y")))

(define-public crate-swift-bridge-cli-0.1.29 (c (n "swift-bridge-cli") (v "0.1.29") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.29") (d #t) (k 0)))) (h "0cw67n5gnqj7ahsfwzga899vacgnhp7jgc8q1iq0h29appbpdncl")))

(define-public crate-swift-bridge-cli-0.1.30 (c (n "swift-bridge-cli") (v "0.1.30") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.30") (d #t) (k 0)))) (h "1blf9dkfbv03a45nl4kv2847bs6mm21i5wx7grdg73pha7y8hax3")))

(define-public crate-swift-bridge-cli-0.1.31 (c (n "swift-bridge-cli") (v "0.1.31") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.31") (d #t) (k 0)))) (h "0y1qyd496dyk2mxciclcjm9rl5jzjhmm52rnm4a6v5vmjk7qf9aq")))

(define-public crate-swift-bridge-cli-0.1.32 (c (n "swift-bridge-cli") (v "0.1.32") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.32") (d #t) (k 0)))) (h "12pxampyxnwpbqw8lrgh1y6zmdyfxf9h78zw54qcnyf78fddgx6c")))

(define-public crate-swift-bridge-cli-0.1.33 (c (n "swift-bridge-cli") (v "0.1.33") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.33") (d #t) (k 0)))) (h "18npwwlk4s697kh0asj9zfmq9cr9g0mq9bb7m62xnzfixa1fz1v0")))

(define-public crate-swift-bridge-cli-0.1.34 (c (n "swift-bridge-cli") (v "0.1.34") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.34") (d #t) (k 0)))) (h "07xdg0h6mvmqr5z3r0g4irr5nrfml2xfbamhlvqr25d79r0c77ws")))

(define-public crate-swift-bridge-cli-0.1.35 (c (n "swift-bridge-cli") (v "0.1.35") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.35") (d #t) (k 0)))) (h "072yrgp992sxbmrdb8xf195xph2bx5i77nif70rsff8caz91bq6q")))

(define-public crate-swift-bridge-cli-0.1.36 (c (n "swift-bridge-cli") (v "0.1.36") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.36") (d #t) (k 0)))) (h "1biy8md27x60vnbcb6gc01l29sypzswpsc01xdg6nsjilnd30ki6")))

(define-public crate-swift-bridge-cli-0.1.37 (c (n "swift-bridge-cli") (v "0.1.37") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.37") (d #t) (k 0)))) (h "04vcx2bi847iwa9rx2pmg1s7202566bm90isrd8y70znh73bar8l")))

(define-public crate-swift-bridge-cli-0.1.38 (c (n "swift-bridge-cli") (v "0.1.38") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.38") (d #t) (k 0)))) (h "163a50g66qnw7imh66wkf9mrahccrj5y9z26pc3k6yvj391f09gh")))

(define-public crate-swift-bridge-cli-0.1.39 (c (n "swift-bridge-cli") (v "0.1.39") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.39") (d #t) (k 0)))) (h "05h1rwcamsiw331cngivw1qq5xinpkm1ns4mwgv5g1z5raqzdj6j")))

(define-public crate-swift-bridge-cli-0.1.40 (c (n "swift-bridge-cli") (v "0.1.40") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.40") (d #t) (k 0)))) (h "1kx41b94w1yakqrzrbfl8hg08w7vwdqs57mk6shzbrj09m3nhnvv")))

(define-public crate-swift-bridge-cli-0.1.41 (c (n "swift-bridge-cli") (v "0.1.41") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.41") (d #t) (k 0)))) (h "0dpl84c4dqdacmk4776sf3ic3gqlbi9zzr6dng2yacprwj1322r3")))

(define-public crate-swift-bridge-cli-0.1.42 (c (n "swift-bridge-cli") (v "0.1.42") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.42") (d #t) (k 0)))) (h "1pkd0h5r3wfzhy3w01a5k4kic6wshb125wdsx1rrhy59xg0y548s")))

(define-public crate-swift-bridge-cli-0.1.43 (c (n "swift-bridge-cli") (v "0.1.43") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.43") (d #t) (k 0)))) (h "0yl541fn4pb4amzzcw2z4d58xm0n6x5wwzhi2ljxapp7ys5s0z8q")))

(define-public crate-swift-bridge-cli-0.1.44 (c (n "swift-bridge-cli") (v "0.1.44") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.44") (d #t) (k 0)))) (h "0cz180nfxdvrigc7irx9lw0nvvqzwvysc5247y0a3l6bb379p5cg")))

(define-public crate-swift-bridge-cli-0.1.45 (c (n "swift-bridge-cli") (v "0.1.45") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.45") (d #t) (k 0)))) (h "0shz46bixiv2858nn14l9acmcaqfzk7kn8sl3j0hb09f144qmjci")))

(define-public crate-swift-bridge-cli-0.1.46 (c (n "swift-bridge-cli") (v "0.1.46") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.46") (d #t) (k 0)))) (h "0slanal1syjrs8qwimlrlh4lsn39x4hy3fwpzbhx59jpvfnqc1yl")))

(define-public crate-swift-bridge-cli-0.1.47 (c (n "swift-bridge-cli") (v "0.1.47") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.47") (d #t) (k 0)))) (h "18fvsg6qvg02b758vvg5ix17nzb83bcfs0ylxdg7lzr0l0px5pmg")))

(define-public crate-swift-bridge-cli-0.1.48 (c (n "swift-bridge-cli") (v "0.1.48") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.48") (d #t) (k 0)))) (h "1ihwn757mbja0v8zib9q0d85m9g7abxi85wwqjgqli3hk07zknwb")))

(define-public crate-swift-bridge-cli-0.1.49 (c (n "swift-bridge-cli") (v "0.1.49") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.49") (d #t) (k 0)))) (h "0xw1zhzjz5yjflnaymivfpgg3vch6slsv21frha5wc80rlqlb9rj")))

(define-public crate-swift-bridge-cli-0.1.50 (c (n "swift-bridge-cli") (v "0.1.50") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.50") (d #t) (k 0)))) (h "0bv6gkd2mmiiv2w5yqzkq5wq1lq4rnicvwivnp07zmck8i351zad")))

(define-public crate-swift-bridge-cli-0.1.51 (c (n "swift-bridge-cli") (v "0.1.51") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.51") (d #t) (k 0)))) (h "0yprbbb4yfm6qb1438h8v1pbhsgjisnwwpcb3dywjczifd0izfg4")))

(define-public crate-swift-bridge-cli-0.1.52 (c (n "swift-bridge-cli") (v "0.1.52") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.52") (d #t) (k 0)))) (h "0dzawwdv78icfrg336qc920g9dba1465a94b9gnwijcaqwa34k0s")))

(define-public crate-swift-bridge-cli-0.1.53 (c (n "swift-bridge-cli") (v "0.1.53") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.53") (d #t) (k 0)))) (h "1z827d0zy494c0b3gmqmwy5xc120pyyir9577hgzzl36xq6w1dgw")))

(define-public crate-swift-bridge-cli-0.1.54 (c (n "swift-bridge-cli") (v "0.1.54") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.54") (d #t) (k 0)))) (h "0n51lpa3yl5mzz0cf1n7ac6y6c9hfzy7kzp8c6w20dqd8k11swz5")))

(define-public crate-swift-bridge-cli-0.1.55 (c (n "swift-bridge-cli") (v "0.1.55") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.55") (d #t) (k 0)))) (h "09c0k82n2v70g7qx7kbizyhsri120lgszi2iam0gkcr3x7jp7nvi")))

