(define-module (crates-io sw if swift_localizable_json_parser) #:use-module (crates-io))

(define-public crate-swift_localizable_json_parser-0.1.0 (c (n "swift_localizable_json_parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sr3kyrab279bcrl9dpqr9w36pgi9hwfinsppi52wvrn8dr0vp34")))

(define-public crate-swift_localizable_json_parser-0.1.1 (c (n "swift_localizable_json_parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hqdxfnv6r7mvaq46b5rlxmzmpiz3hcqlkswndggsw1fsy898grx")))

(define-public crate-swift_localizable_json_parser-0.1.2 (c (n "swift_localizable_json_parser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w0s90vxnqm88m3smb5qscys3hq4c8zwj5kwlkw56qmcabinvljw")))

(define-public crate-swift_localizable_json_parser-0.1.3 (c (n "swift_localizable_json_parser") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lx2dygdyc4hplrfsrlji6z2w7sjpgiq3fzf115r73d647pg1qaj")))

