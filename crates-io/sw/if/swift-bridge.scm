(define-module (crates-io sw if swift-bridge) #:use-module (crates-io))

(define-public crate-swift-bridge-0.1.0 (c (n "swift-bridge") (v "0.1.0") (d (list (d (n "swift-bridge-build") (r "^0.1.0") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1ffdjwk6wisnfs07wc2ajjrwjy56w2yip29pfvk004mq81xk5q76")))

(define-public crate-swift-bridge-0.1.1 (c (n "swift-bridge") (v "0.1.1") (d (list (d (n "swift-bridge-build") (r "^0.1.0") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1sd83d0ki2h0p63l2kc5cbb0vj5jfq8i91797wsigzabizwxyy49")))

(define-public crate-swift-bridge-0.1.2 (c (n "swift-bridge") (v "0.1.2") (d (list (d (n "swift-bridge-build") (r "^0.1.2") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.2") (d #t) (k 0)))) (h "15vih979c17xsc2sc1ggnck25j982w411bhsj3c5vgivapg2bnkx")))

(define-public crate-swift-bridge-0.1.3 (c (n "swift-bridge") (v "0.1.3") (d (list (d (n "swift-bridge-build") (r "^0.1.3") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.3") (d #t) (k 0)))) (h "1s24mygif6iflvff12bnn79z7yhzfywjzkvjj2f9k5nb3w2j989y")))

(define-public crate-swift-bridge-0.1.4 (c (n "swift-bridge") (v "0.1.4") (d (list (d (n "swift-bridge-build") (r "^0.1.4") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0ynyyqqzyzj9rzjrjp6dmigmxlvqbh4c5fgr6h3cmi4slnhj00aq")))

(define-public crate-swift-bridge-0.1.5 (c (n "swift-bridge") (v "0.1.5") (d (list (d (n "swift-bridge-build") (r "^0.1.5") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1ybk06zfzy0m2dpqmhik7m3zw06cgqkf6az28l0wyyxyvr7dkr8g")))

(define-public crate-swift-bridge-0.1.6 (c (n "swift-bridge") (v "0.1.6") (d (list (d (n "swift-bridge-build") (r "^0.1.6") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.6") (d #t) (k 0)))) (h "18hl798l8p1zk76zn1dyjdyhny4fmv2yv4d42d972pbsb36qhpkv")))

(define-public crate-swift-bridge-0.1.7 (c (n "swift-bridge") (v "0.1.7") (d (list (d (n "swift-bridge-build") (r "^0.1.7") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.7") (d #t) (k 0)))) (h "18l5jfp8vwfgnn2yxfcjwcpiy64zbd1m2zraa8dll8pdbgz8nyzx")))

(define-public crate-swift-bridge-0.1.8 (c (n "swift-bridge") (v "0.1.8") (d (list (d (n "swift-bridge-build") (r "^0.1.8") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.8") (d #t) (k 0)))) (h "0nivrz7i1jq3mmb79jfz53nh6ldp343lwrm82cj9pvh9sm0rqgpg")))

(define-public crate-swift-bridge-0.1.9 (c (n "swift-bridge") (v "0.1.9") (d (list (d (n "swift-bridge-build") (r "^0.1.9") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.9") (d #t) (k 0)))) (h "113f2sfmrr18p9vfpp6cbbnrvxi8mmxplddikfmfawlc9d036r78")))

(define-public crate-swift-bridge-0.1.10 (c (n "swift-bridge") (v "0.1.10") (d (list (d (n "swift-bridge-build") (r "^0.1.10") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.10") (d #t) (k 0)))) (h "0gqflbsy0vxrjasg3aayrkhqj9fh9km2h24hz24w9sgwp2qa7s4d")))

(define-public crate-swift-bridge-0.1.11 (c (n "swift-bridge") (v "0.1.11") (d (list (d (n "swift-bridge-build") (r "^0.1.11") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.11") (d #t) (k 0)))) (h "0yn78rnmda1dsv1j79sbh57wsfgnfhmpi8pcm874pb8zp64p8w5v")))

(define-public crate-swift-bridge-0.1.12 (c (n "swift-bridge") (v "0.1.12") (d (list (d (n "swift-bridge-build") (r "^0.1.12") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.12") (d #t) (k 0)))) (h "15zd3qpi6xiblv1g8ca6fcia43rs1ashs7j5inmvg1iar6flvr2l")))

(define-public crate-swift-bridge-0.1.13 (c (n "swift-bridge") (v "0.1.13") (d (list (d (n "swift-bridge-build") (r "^0.1.13") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.13") (d #t) (k 0)))) (h "1hsw3ysx7fcvkn4xhaha7rsxgmr6g6fx0wvkff0a5pkgazjiyfpz")))

(define-public crate-swift-bridge-0.1.14 (c (n "swift-bridge") (v "0.1.14") (d (list (d (n "swift-bridge-build") (r "^0.1.14") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.14") (d #t) (k 0)))) (h "0l25ws14i6h6i5l5j0c1yrilwynxiz9sp477jp79q0hrvc51yx88")))

(define-public crate-swift-bridge-0.1.15 (c (n "swift-bridge") (v "0.1.15") (d (list (d (n "swift-bridge-build") (r "^0.1.15") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.15") (d #t) (k 0)))) (h "1p8bn46x0mn1ipwjh61vf4k50inxh7b5w3rkf0n88ah97qsjy58j")))

(define-public crate-swift-bridge-0.1.16 (c (n "swift-bridge") (v "0.1.16") (d (list (d (n "swift-bridge-build") (r "^0.1.16") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.16") (d #t) (k 0)))) (h "1gijdcc60ss93rkj6bvvlrhp09rvr0akrsaz1sic85myz144c14f")))

(define-public crate-swift-bridge-0.1.17 (c (n "swift-bridge") (v "0.1.17") (d (list (d (n "swift-bridge-build") (r "^0.1.17") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.17") (d #t) (k 0)))) (h "1j8dbhk8h195ggprd3kfgh7aik5cy16nyzlly12m8z3wmb70lcdf")))

(define-public crate-swift-bridge-0.1.18 (c (n "swift-bridge") (v "0.1.18") (d (list (d (n "swift-bridge-build") (r "^0.1.18") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.18") (d #t) (k 0)))) (h "0pz5pjp9z9prfxq6zqg5brs4spygy9xbcijnjb2bv8ap3n82xg27")))

(define-public crate-swift-bridge-0.1.19 (c (n "swift-bridge") (v "0.1.19") (d (list (d (n "swift-bridge-build") (r "^0.1.19") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.19") (d #t) (k 0)))) (h "0jm4j96iqf5dg5paw8yisrdy6bkfc9lryqz8hxcrw0m1nms5bdx0")))

(define-public crate-swift-bridge-0.1.20 (c (n "swift-bridge") (v "0.1.20") (d (list (d (n "swift-bridge-build") (r "^0.1.20") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.20") (d #t) (k 0)))) (h "19wyp129x0j5s4ggfrqwsggvm1a3f72pd6kp9a939q3b44vr1a05")))

(define-public crate-swift-bridge-0.1.21 (c (n "swift-bridge") (v "0.1.21") (d (list (d (n "swift-bridge-build") (r "^0.1.21") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.21") (d #t) (k 0)))) (h "0klswryld7d15j3xf68sc7v62s4z97gf1yz8wbcx6xy8g6m09f5m")))

(define-public crate-swift-bridge-0.1.22 (c (n "swift-bridge") (v "0.1.22") (d (list (d (n "swift-bridge-build") (r "^0.1.22") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.22") (d #t) (k 0)))) (h "0cx2405nxiagk2nb7zyg5rdrps1wyxg6ia5k0w6bgij9pdl4sgyj")))

(define-public crate-swift-bridge-0.1.23 (c (n "swift-bridge") (v "0.1.23") (d (list (d (n "swift-bridge-build") (r "^0.1.23") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.23") (d #t) (k 0)))) (h "01jpwjpfvls4dwgbh26cv0l9y6my0nv7kag4hp6aphsdcd5cavnm")))

(define-public crate-swift-bridge-0.1.24 (c (n "swift-bridge") (v "0.1.24") (d (list (d (n "swift-bridge-build") (r "^0.1.24") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.24") (d #t) (k 0)))) (h "1zxvmnhq03inympyms37sbbs91gf5kzl07216vhi99a9f5b43fr7")))

(define-public crate-swift-bridge-0.1.25 (c (n "swift-bridge") (v "0.1.25") (d (list (d (n "swift-bridge-build") (r "^0.1.25") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.25") (d #t) (k 0)))) (h "1qd4v9ad186cafcn7j6y8b2dkr8nay3r5ja76phz7j17b4gmvl5a")))

(define-public crate-swift-bridge-0.1.26 (c (n "swift-bridge") (v "0.1.26") (d (list (d (n "swift-bridge-build") (r "^0.1.26") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.26") (d #t) (k 0)))) (h "13fyj0d8q4pfdvvi2ahrlayg2mav0xb62vfibncahx4nfa26chka")))

(define-public crate-swift-bridge-0.1.27 (c (n "swift-bridge") (v "0.1.27") (d (list (d (n "swift-bridge-build") (r "^0.1.27") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.27") (d #t) (k 0)))) (h "1g8pbzihiw5sllyrld3602yln348gv39da1kn5vbwnlxq69pji27")))

(define-public crate-swift-bridge-0.1.28 (c (n "swift-bridge") (v "0.1.28") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.28") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.28") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "02n393wxv9icv4xrg4362wl7ixpfzcis9an845r2wpy4z94wj31q") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.29 (c (n "swift-bridge") (v "0.1.29") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.29") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.29") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0sj10wz0ipdk3ldnf8q48p19ha7xdqq92rx0x0pqcz75xsfxjz2d") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.30 (c (n "swift-bridge") (v "0.1.30") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.30") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1dk5ydfin5rd127k7bh1cy063lmq6qn7lx08zjggkwg10anlmdw4") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.31 (c (n "swift-bridge") (v "0.1.31") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.31") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "11lsr72zgzip9v63c4g9xjwxlry59gvzjcimf0yskixn539iw3wn") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.32 (c (n "swift-bridge") (v "0.1.32") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.32") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.32") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0px412bhhd2hilc1pfi3q1rmj142a8qfi4n9z3glylkpg9fhklaj") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.33 (c (n "swift-bridge") (v "0.1.33") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.33") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.33") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "187lk4cx26m3k55ly5742ix2vzskilx3vax7zld76bkhfka3l6d0") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.34 (c (n "swift-bridge") (v "0.1.34") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.34") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "19maycnva77lsb7ghfd93q3l7ns2fc2s6kay0q1lwzk03ndzmdws") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.35 (c (n "swift-bridge") (v "0.1.35") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.35") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.35") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "17kia1z72pxl6cb97v06917778ix5pvnc2wvgww0s0jbq8bhnhwk") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.36 (c (n "swift-bridge") (v "0.1.36") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.36") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.36") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1r81p4lk4i879cgb8pi22y2jy5r6fph108l3pq21xjmj8ynjvbmr") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.37 (c (n "swift-bridge") (v "0.1.37") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.37") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0l3054575piamrkx50zphh51vvy0mk52w1dqgx14mbmkjy5fswl1") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.38 (c (n "swift-bridge") (v "0.1.38") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.38") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0paxv30kb50ni0c8jlbddb0vnjlvsb107xqc7dqrrw5csgl3w00j") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.39 (c (n "swift-bridge") (v "0.1.39") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.39") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "01dprid3lcnw3z2vykh1h6wxy2cnm7rdycp04v3fy728lv4r0h4i") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.40 (c (n "swift-bridge") (v "0.1.40") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.40") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.40") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0hwam3jij22ilz39pzspbq6cmqz8q67na2h1mnrzpyq0piphnnld") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.41 (c (n "swift-bridge") (v "0.1.41") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.41") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.41") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "03j32wrmsx750im4b373sf2zya93cyk5x5dzxzs0jjp9qrif73m9") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.42 (c (n "swift-bridge") (v "0.1.42") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.42") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.42") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0jwkmar3ap5v82zx5klsjs4pfbs62521d2d2j1v9m1wdv94hwxz0") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.43 (c (n "swift-bridge") (v "0.1.43") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.43") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.43") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1pkr4pnw3d4dlv8zydzwqwm0k2nv67gnrrfwaf1dpyprc1mda43p") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.44 (c (n "swift-bridge") (v "0.1.44") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.44") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.44") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1vx9y8maqdybwd8jylrykvdjmkmhzqyjzxx103srl3zhpffs3fsq") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.45 (c (n "swift-bridge") (v "0.1.45") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.45") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.45") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "11v5nyyy3qgw9jqdynv9h7251c63d8nciiffic2h435q3vk62pmq") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.46 (c (n "swift-bridge") (v "0.1.46") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.46") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.46") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "159mkp3z1srs619m55hj72r5f37icpvd1f92w9lvgrvlkps4xyq6") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.47 (c (n "swift-bridge") (v "0.1.47") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.47") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.47") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "02asdiapi4c3hz7p7sivvw38alrf835jmf33sgrman29m18f5gxm") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.48 (c (n "swift-bridge") (v "0.1.48") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.48") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.48") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1s4s4yp6sxn5nils044y4hpppbhw6dnpv3vxs9zfvnixgrs03qj2") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.49 (c (n "swift-bridge") (v "0.1.49") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.49") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.49") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1riffyhlh1nfbf48cx0ia42q9q23d2r4jblpzh6im666a0jsdvsn") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.50 (c (n "swift-bridge") (v "0.1.50") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.50") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.50") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0iin386h0m6461nlciamkfsg1y0i91c3sdngqkx75nam30y64lk3") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.51 (c (n "swift-bridge") (v "0.1.51") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.51") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0bxxvbxlp6f1ml1vvrsp6kp3xzg3b78spxcninj7qb9brp3hgymq") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.52 (c (n "swift-bridge") (v "0.1.52") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.52") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "174jw6c58i51bbg1znjlspdd4xr8h24ybb5pwrrkd2bnqfgs8app") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.53 (c (n "swift-bridge") (v "0.1.53") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.53") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.53") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0x7xgcq7aivnmh975sccyr8691rc48461g2c346r175xh9w8s23j") (f (quote (("default") ("async" "tokio" "once_cell"))))))

(define-public crate-swift-bridge-0.1.54 (c (n "swift-bridge") (v "0.1.54") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.54") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.54") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1ngygqxbc2b0sqjrpw3xv69d9qab4jnpmvqdy8k98wvsb433hrp2") (f (quote (("default") ("async" "tokio" "once_cell")))) (y #t)))

(define-public crate-swift-bridge-0.1.55 (c (n "swift-bridge") (v "0.1.55") (d (list (d (n "once_cell") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1.55") (d #t) (k 1)) (d (n "swift-bridge-macro") (r "^0.1.55") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "06xc5rb4v6yfndhjl1pz2nmy7p1gxsq82nnp36yf09i9i5lcd031") (f (quote (("default") ("async" "tokio" "once_cell"))))))

