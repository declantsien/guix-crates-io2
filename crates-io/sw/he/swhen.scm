(define-module (crates-io sw he swhen) #:use-module (crates-io))

(define-public crate-swhen-0.1.0 (c (n "swhen") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "073k9qhnb5g1ssqbd9v0lzj9iakv3m8dg77ijwvkvfy2p7ig67xc")))

(define-public crate-swhen-0.1.1 (c (n "swhen") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "010pyfibrywbs2xi4rai295plhn38hb84d4aizqv8nkxmsmxsgcg")))

(define-public crate-swhen-0.1.2 (c (n "swhen") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1sbvgy0k1nca42yf1vx1msbapvxrwa4shx2sw3r8jpdc6zy7flb1")))

