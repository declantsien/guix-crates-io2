(define-module (crates-io sw -l sw-logger-rs) #:use-module (crates-io))

(define-public crate-sw-logger-rs-1.0.0 (c (n "sw-logger-rs") (v "1.0.0") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q96p8kw2csl494h2r1m99r539pqb30s1xzn1bnil3yrqfgrqw66")))

(define-public crate-sw-logger-rs-1.0.1 (c (n "sw-logger-rs") (v "1.0.1") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03dyk00hnqk6zg66axlxgjh5symjl6p4dl84amsbp8j81gqy5c7g")))

(define-public crate-sw-logger-rs-1.1.1 (c (n "sw-logger-rs") (v "1.1.1") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f2lqksh7wlda3dhc929zg2by0sk3vimg4laxpm04d1j12ba937b")))

(define-public crate-sw-logger-rs-1.1.2 (c (n "sw-logger-rs") (v "1.1.2") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03jcvhnmrs096i55c1csmjlx1xy1q23x1dd9hld2qilyjxnlhdld")))

