(define-module (crates-io sw ag swagger_to_md) #:use-module (crates-io))

(define-public crate-swagger_to_md-1.0.0 (c (n "swagger_to_md") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.6.14") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.14") (d #t) (k 0)))) (h "1hlsljp2lpx0jpjzmg1mak7plzklq6sr1yda4drqs88xrf75x4vw")))

