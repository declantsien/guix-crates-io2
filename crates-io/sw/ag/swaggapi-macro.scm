(define-module (crates-io sw ag swaggapi-macro) #:use-module (crates-io))

(define-public crate-swaggapi-macro-0.1.0 (c (n "swaggapi-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~2") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6f8xn9g9zv2lrw5zqpcpdaagb5fik5nkgif94flypb1ni4a97i") (y #t)))

(define-public crate-swaggapi-macro-0.1.1 (c (n "swaggapi-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~2") (f (quote ("full"))) (d #t) (k 0)))) (h "00n730a9kr52md6jxizjc46rvgssy670yxy51v7cqq5l8y5blzrv")))

(define-public crate-swaggapi-macro-0.2.0 (c (n "swaggapi-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ipzn105j6s12vxm0kcip0ws58xbffsffwaz3brwyivcqfipdi0w")))

