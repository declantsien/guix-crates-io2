(define-module (crates-io sw ag swagstract) #:use-module (crates-io))

(define-public crate-swagstract-0.1.0 (c (n "swagstract") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "14innlyv3s2lfd0vcj52ry4xwaqhi304a8b754wvg1icgi36qglg")))

(define-public crate-swagstract-0.1.1 (c (n "swagstract") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1d9z0nd7m78gvji7yakhcjw5mjjr254rj6mnkr30wsz8g9p7njnz")))

(define-public crate-swagstract-0.1.2 (c (n "swagstract") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "05bnwxk74gvcx08sgkrc1lv0nvc3353dmb9q70daf9ia7qfcnr68")))

(define-public crate-swagstract-0.1.3 (c (n "swagstract") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1nsl1kfxvkvbl4i97s7z38g9fhxn8n7hlvnxqs7pk2g9x2dx5kxr")))

(define-public crate-swagstract-0.1.4 (c (n "swagstract") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "14496rix8g8qglbwiq86ar7vafgnq4nx1i11p3p695mmzh8wk660")))

(define-public crate-swagstract-0.1.5 (c (n "swagstract") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "19dxpryl85fvwmv2w38xn47lpqq9vj0q3axnqy8cg3qk4wqbvgpj")))

(define-public crate-swagstract-0.1.6 (c (n "swagstract") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0w8346zm28851mfcz0bwzjwq83722dnfiid1dg1lqh0mix0vn5z7")))

