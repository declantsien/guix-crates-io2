(define-module (crates-io sw ag swagger-ui2) #:use-module (crates-io))

(define-public crate-swagger-ui2-0.1.0 (c (n "swagger-ui2") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0p9nx46m9016np4yngaadrncv6bmx6ajm3dkig82yxi7r53jhvci")))

(define-public crate-swagger-ui2-0.1.1 (c (n "swagger-ui2") (v "0.1.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0nv7y0n834w4a4d63a93gdlrdmi98n62pc3qm49w6gw069c6hn7j")))

(define-public crate-swagger-ui2-0.5.2 (c (n "swagger-ui2") (v "0.5.2") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1kfc8549z73s8cjlhmkifnacal5z263y68g5zmfmqd6hmx7v9wwj")))

(define-public crate-swagger-ui2-0.5.3 (c (n "swagger-ui2") (v "0.5.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "100019rlb4rkbf66ckygvp777zg5yb24r43i663dlrhila2kjyi9")))

(define-public crate-swagger-ui2-0.6.0 (c (n "swagger-ui2") (v "0.6.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "0dcdi0rlqdwwb8rap62nsbngrb1smiwr802s1xka7x0scq4gr34k")))

