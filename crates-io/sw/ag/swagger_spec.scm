(define-module (crates-io sw ag swagger_spec) #:use-module (crates-io))

(define-public crate-swagger_spec-0.1.0 (c (n "swagger_spec") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03a620vh89msfqwl2yyzscyf6dx8gfqwka17yjbvnszc4q58var9")))

(define-public crate-swagger_spec-0.2.0 (c (n "swagger_spec") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05j848jwr2drci82qpwg11g75n5yaynkghkzyw2kmhyc0srwj5ck")))

