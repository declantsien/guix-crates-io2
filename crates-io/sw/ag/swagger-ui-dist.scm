(define-module (crates-io sw ag swagger-ui-dist) #:use-module (crates-io))

(define-public crate-swagger-ui-dist-5.17.10 (c (n "swagger-ui-dist") (v "5.17.10") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1n4gxbjw40p6ghydcs364irz7mpm7snfkwa7w7an54r1m8180929")))

(define-public crate-swagger-ui-dist-5.17.12 (c (n "swagger-ui-dist") (v "5.17.12") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0wk9azs0aqx4jgdr2kas8p3jx8ix3393blji231ifdk3l44nidaf")))

(define-public crate-swagger-ui-dist-5.17.13 (c (n "swagger-ui-dist") (v "5.17.13") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1rk4xz7j2j5z2qmpm8v1d5sm27i52brx0b43gf4y60r9sk2phj10")))

(define-public crate-swagger-ui-dist-5.17.14 (c (n "swagger-ui-dist") (v "5.17.14") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wpviiy6a2fxyvhigdpmd2kjnh8blgj8ql1ympr6x7n6k3zc8djv")))

