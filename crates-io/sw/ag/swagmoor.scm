(define-module (crates-io sw ag swagmoor) #:use-module (crates-io))

(define-public crate-swagmoor-0.0.1 (c (n "swagmoor") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.0.5") (f (quote ("api-all"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.0.4") (d #t) (k 1)))) (h "0ysw06dnf4s491dcm7720m3ncyp12jq0rqmlfizk9mrgmlan02h0") (f (quote (("default" "custom-protocol") ("custom-protocol" "tauri/custom-protocol")))) (r "1.57")))

(define-public crate-swagmoor-0.0.2 (c (n "swagmoor") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.0.5") (f (quote ("api-all"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.0.4") (d #t) (k 1)))) (h "0iahnmvq2bwjhgck2hkiqmwd8z45zv7z10q6n9151bs1gl8vzvn3") (f (quote (("default" "custom-protocol") ("custom-protocol" "tauri/custom-protocol")))) (r "1.57")))

