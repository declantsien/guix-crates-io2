(define-module (crates-io sw ag swagger-utils) #:use-module (crates-io))

(define-public crate-swagger-utils-0.1.0 (c (n "swagger-utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "03zanrxd3bzylh1qwn6pbd013qg8baizq49lr5wa3rxjrjzjdm3p")))

