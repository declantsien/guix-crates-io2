(define-module (crates-io sw ag swagger_to) #:use-module (crates-io))

(define-public crate-swagger_to-0.1.0 (c (n "swagger_to") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1vw71n01n4ii1ffsxrpv6jxns27cp4c04pja57wpzgpqa4645k6g")))

(define-public crate-swagger_to-0.2.0 (c (n "swagger_to") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0lrny108rkrrv0vaff8ain6dqxbs6ni0f1vzgjmkiswjnbxz2iy8")))

(define-public crate-swagger_to-0.2.1 (c (n "swagger_to") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0qs7qazi9gx0y1zhqhad4rk1f3q3js0s5jdb72h87vq9by70im1z")))

(define-public crate-swagger_to-0.3.1 (c (n "swagger_to") (v "0.3.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x022910mnhdpqy09byjjzsq4z4jmh7zyq77ii470bggq61iv2zp")))

(define-public crate-swagger_to-0.4.1 (c (n "swagger_to") (v "0.4.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3bjknyfsfp2nyms5hj3jb3308n11mw7gydpb80s4f540lfybwd")))

(define-public crate-swagger_to-0.4.2 (c (n "swagger_to") (v "0.4.2") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yw41hx17gjr5d90lvachjpawmjxmhh9hmimy5hhlbgdrcvhaf4d")))

(define-public crate-swagger_to-0.4.3 (c (n "swagger_to") (v "0.4.3") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13lc1fdrlfqndfy08z7brc5rmn3q0rjb1h012ixx1pd49n5cw35a")))

(define-public crate-swagger_to-0.4.4 (c (n "swagger_to") (v "0.4.4") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zlnln7j4pl4d4hb0fvr0v34bl1qw1y6sc6bd61vxwji7r2spb5l")))

