(define-module (crates-io sw ar swarm-overlord) #:use-module (crates-io))

(define-public crate-swarm-overlord-0.0.0 (c (n "swarm-overlord") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("ssh2"))) (d #t) (k 0)) (d (n "find-target") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1kxb8mccj823zg8fm7dmypdkammq1mwf21v9xdhcdcg29z0iqla7") (f (quote (("default"))))))

