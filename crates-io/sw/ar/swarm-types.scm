(define-module (crates-io sw ar swarm-types) #:use-module (crates-io))

(define-public crate-swarm-types-0.0.0 (c (n "swarm-types") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("num" "rust_decimal"))) (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "peginator_codegen") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05n79m1jz1zryx2rr8w073i46zhv6ryx7ifq2rf9ghrqdk51aklw") (f (quote (("default"))))))

