(define-module (crates-io sw ar swarmd_slug-rs) #:use-module (crates-io))

(define-public crate-swarmd_slug-rs-0.1.1-alpha.2 (c (n "swarmd_slug-rs") (v "0.1.1-alpha.2") (d (list (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n4ggzfyma2kia0p79c5wyqlcy030pns6vl5gh1kr8dli41aafrm")))

