(define-module (crates-io sw ar swarm_it) #:use-module (crates-io))

(define-public crate-swarm_it-0.1.0 (c (n "swarm_it") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0bzs9y0cfia46h3ryxjrl6j1mab5qdnyvkcrcfkynd0059n6hm9n")))

(define-public crate-swarm_it-0.1.1 (c (n "swarm_it") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0nj2p19qj3zqwkq6xz4ahyc6lgbj2hmslbx8i7ypjsml66pf48ic")))

