(define-module (crates-io sw ar swarm_pool) #:use-module (crates-io))

(define-public crate-swarm_pool-0.1.0 (c (n "swarm_pool") (v "0.1.0") (h "173zr7kpl3z4v8ygyc1m1pq7fz46556vmjqhnvrynzi4panx179k")))

(define-public crate-swarm_pool-0.1.1 (c (n "swarm_pool") (v "0.1.1") (h "1phdh5a3qm5nhwcys1s2i0zm4625yihqskfnv1miydpgm83yf9c3")))

(define-public crate-swarm_pool-0.1.2 (c (n "swarm_pool") (v "0.1.2") (h "1ac0kml1r9gxhh1cdjgaslzlbj0w9xkcbskbpml0sxjfd60kmyzb")))

(define-public crate-swarm_pool-0.1.3 (c (n "swarm_pool") (v "0.1.3") (h "08qjmmw0h2zrra10ac950fshayananznhzif8p3lvikfn8xqd17l")))

(define-public crate-swarm_pool-0.1.4 (c (n "swarm_pool") (v "0.1.4") (h "0szwb8h3jl4xm4ng5pzw0dvjfw89h63x1d3mvb6i397lyh7ni7yc")))

(define-public crate-swarm_pool-0.1.5 (c (n "swarm_pool") (v "0.1.5") (h "07p8ajpsra7dq75rnb7pc3v8rl74v19mxx9mdn49frw8b1xkdzyr")))

(define-public crate-swarm_pool-0.1.6 (c (n "swarm_pool") (v "0.1.6") (h "0wx6qannm7v93w57kr9icnpn8g7nf4x385xk37yjwggrqygy9nhn")))

(define-public crate-swarm_pool-0.1.7 (c (n "swarm_pool") (v "0.1.7") (h "1zyaq0k6rcy4bfg4hrgyrpdkh3xk40x8kfqaayyw0cyh4ddcx3hp")))

(define-public crate-swarm_pool-0.1.8 (c (n "swarm_pool") (v "0.1.8") (h "0b406g3vgkf6s5yaws55gx461cwrhg6fymx1ym2kgprywx2isp3y")))

(define-public crate-swarm_pool-0.1.9 (c (n "swarm_pool") (v "0.1.9") (h "1pd6hrz3jagz0bw0f1qg9z48qmh774zilqvpywpw9n894r4jzwqb")))

(define-public crate-swarm_pool-0.2.0 (c (n "swarm_pool") (v "0.2.0") (h "0z758rxfyrdk43qvigsd1xajix84vknb1h64i86x9b5g4rm8xfm0")))

