(define-module (crates-io sw ar swarmd_instruments) #:use-module (crates-io))

(define-public crate-swarmd_instruments-0.1.0 (c (n "swarmd_instruments") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "sentry") (r "^0.32") (d #t) (k 0)) (d (n "sentry-tracing") (r "^0.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "env-filter" "json"))) (d #t) (k 0)))) (h "1bpbm2yig67xs30hpfhgxylysl2gkddswfdmgbxrd7fdjbknncal") (f (quote (("default"))))))

(define-public crate-swarmd_instruments-0.1.1 (c (n "swarmd_instruments") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "sentry") (r "^0.32") (d #t) (k 0)) (d (n "sentry-tracing") (r "^0.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "env-filter" "json"))) (d #t) (k 0)))) (h "1hsz3x5xx2mhss4dy2xby9cyhjmcmshihafm5h2hyb8yg4pl93b6") (f (quote (("default"))))))

(define-public crate-swarmd_instruments-0.1.2 (c (n "swarmd_instruments") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "sentry") (r "^0.32") (d #t) (k 0)) (d (n "sentry-tracing") (r "^0.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "env-filter" "json"))) (d #t) (k 0)))) (h "0xjs9kn7qhy233r9vmarxq7xhhdxpwj0la1j6srna7sn8c8zgi1z") (f (quote (("default"))))))

