(define-module (crates-io sw ar swarm_proxy) #:use-module (crates-io))

(define-public crate-swarm_proxy-0.1.0 (c (n "swarm_proxy") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0fy4ss9xk3ipa3n6ldvnpnin4il584nqjbw2g0sa9gv4j5adk6")))

