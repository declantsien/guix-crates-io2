(define-module (crates-io sw ar swarm-ssh) #:use-module (crates-io))

(define-public crate-swarm-ssh-0.0.0 (c (n "swarm-ssh") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("ssh2" "url" "mime"))) (d #t) (k 0)) (d (n "find-target") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "pubgrub") (r "^0.2.1") (d #t) (k 0)) (d (n "relative-path") (r "^1.7.2") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0qlj4va7zg7fmbffnq3s70rkcrdwsbg5w28fwn197fq94vbq9xp5") (f (quote (("default"))))))

(define-public crate-swarm-ssh-0.1.0 (c (n "swarm-ssh") (v "0.1.0") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("ssh2"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1wq6rijcqzv2x2ikx02r838hg5cd3k2n9vzichkz31y725xzn19k") (f (quote (("default"))))))

(define-public crate-swarm-ssh-0.1.1 (c (n "swarm-ssh") (v "0.1.1") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("ssh2"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ys6g1vrsva25mi28a4nv2hgsdyfpx3c6723nn2hairwf1kngfm7") (f (quote (("default"))))))

(define-public crate-swarm-ssh-0.1.2 (c (n "swarm-ssh") (v "0.1.2") (d (list (d (n "diagnostic-quick") (r "^0.3.4") (f (quote ("ssh2"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "13dlr125dpcb7myvhsch97dsrh8sndfzx9smlyjj2nwp6kskh129") (f (quote (("default"))))))

