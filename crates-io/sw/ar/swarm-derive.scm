(define-module (crates-io sw ar swarm-derive) #:use-module (crates-io))

(define-public crate-swarm-derive-0.0.0 (c (n "swarm-derive") (v "0.0.0") (h "0sxa45hw9hi6ibbk2k098s9nz3ac65cn0kwz1hixbxy9pkx9ybla") (y #t)))

(define-public crate-swarm-derive-0.22.0 (c (n "swarm-derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "12sani1jgk46iyc55f23av7h464gk092pxgckgnb3ccl4bblds61")))

