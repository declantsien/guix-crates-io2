(define-module (crates-io sw ar swarm-commander) #:use-module (crates-io))

(define-public crate-swarm-commander-0.9.0 (c (n "swarm-commander") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0m8sh3hf4aah8a41s9z1karsrxa9bq8z9v1dqfh70dk2fjvwh239")))

(define-public crate-swarm-commander-0.9.1 (c (n "swarm-commander") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "1cmmvdsv28qsnj712crp702mbda1x34cpabb2ijlps6z1kjj89br")))

(define-public crate-swarm-commander-0.9.2 (c (n "swarm-commander") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0p7vkgaijvj9l9980v538r8apzvdl64mxr2v47qgrqch01g2x0v3")))

(define-public crate-swarm-commander-0.9.3 (c (n "swarm-commander") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0lwj84ds58nyjvnqzpiv1ydp8l1iyvr4x77bvhbvzdriy0fvw8xm")))

(define-public crate-swarm-commander-0.9.4 (c (n "swarm-commander") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0n8ksq31jk4s69364nh2dy5gdf4shp48vzzd1241rqpw9d2z7n94")))

(define-public crate-swarm-commander-0.9.5 (c (n "swarm-commander") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "1k8m9bx4vryf99s1fawydfcswj6i8l5dwkjrz5pxfjfx1jbq2035")))

(define-public crate-swarm-commander-0.9.6 (c (n "swarm-commander") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0p3jb9jv1as0jpc1jpiwasr3rkdw6pjk4szr1s21p6q5p03w14xd")))

(define-public crate-swarm-commander-0.9.7 (c (n "swarm-commander") (v "0.9.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "process" "io-util" "macros"))) (d #t) (k 0)))) (h "0bzd5521mwam9iwy5s9r395g1pwlmbw8v1d9waxx2klnwq3cdzyv")))

