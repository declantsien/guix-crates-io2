(define-module (crates-io sw ar swarm-bot-packets-macro) #:use-module (crates-io))

(define-public crate-swarm-bot-packets-macro-0.1.0 (c (n "swarm-bot-packets-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0j0pcw0gy2l2q07ymgbnbac2m98dvjdnlyp6xn4hjz4cy8k75kp3")))

(define-public crate-swarm-bot-packets-macro-0.1.1 (c (n "swarm-bot-packets-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0f1w291aw9lghikkswjgzg1qi2vmr7dbmn8w2mmil4f5f5jhvh7y")))

(define-public crate-swarm-bot-packets-macro-0.1.2 (c (n "swarm-bot-packets-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1iml65xnis8adwjrah84nwi3v9rg3d3xrma1dvxwhzm5p789l441")))

