(define-module (crates-io sw ar swar) #:use-module (crates-io))

(define-public crate-swar-0.1.0 (c (n "swar") (v "0.1.0") (h "0srbd1msmkcighw7waf0cagn1lr7l8p80zkzasyrf4pal5wi4ni5")))

(define-public crate-swar-0.1.1 (c (n "swar") (v "0.1.1") (h "1nwj3xpb5jpn3jgb9z34vcgibq76l2xggx6qcqs6m3qmny8vpxfa")))

(define-public crate-swar-0.2.0 (c (n "swar") (v "0.2.0") (h "0fxbildri8zfxkgj4iy4p1sqp6rfwv8z5dgfwiribgfpczk3izpw")))

(define-public crate-swar-0.2.1 (c (n "swar") (v "0.2.1") (h "13nnmc7vysmibjn97bs3jvlqqc312pa1ccrpqf499ka7y7v86117")))

(define-public crate-swar-0.2.2 (c (n "swar") (v "0.2.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0hgsrdhl61iv7c7q0d32891ycjgir2zrzsn5v4y83901i46wx0nb")))

(define-public crate-swar-0.3.0 (c (n "swar") (v "0.3.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0q5d3sj0s48wvnsjdng97kx65jqrwp8zaw4xyqchs120bva1jgcj")))

(define-public crate-swar-0.3.1 (c (n "swar") (v "0.3.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0rwbq2lc9h2h2kidih69k22a0m8g1gvvrgpr49x805kwp1xy0n7p")))

(define-public crate-swar-0.4.0 (c (n "swar") (v "0.4.0") (d (list (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1yqid7n6r6k0dp7i7zfv32b2wf2fan55rfn1bb7i1br268xda8cw")))

