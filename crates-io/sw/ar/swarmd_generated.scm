(define-module (crates-io sw ar swarmd_generated) #:use-module (crates-io))

(define-public crate-swarmd_generated-0.1.1-alpha.2 (c (n "swarmd_generated") (v "0.1.1-alpha.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1gz0nrz4821jdnmhzal74q3wpp8srk60xm2cra13bfmvn0srqdw0")))

(define-public crate-swarmd_generated-0.1.1-alpha.3 (c (n "swarmd_generated") (v "0.1.1-alpha.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ci1xz8rhhgfh2wgxf8pvdrslclfqkmxwxg65wjyqhvj3k9p803l")))

