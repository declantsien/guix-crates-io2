(define-module (crates-io sw ot swot) #:use-module (crates-io))

(define-public crate-swot-0.1.0 (c (n "swot") (v "0.1.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "phf_shared") (r "^0.10") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1i5xvpzirfhrkfn2w5gp0vxhr27i75xxvb1dn3h9m0dp95rjsg99")))

