(define-module (crates-io sw ri swrite) #:use-module (crates-io))

(define-public crate-swrite-0.0.1 (c (n "swrite") (v "0.0.1") (h "05zi6jyfnk840bkbgwdwr1y4qhvhsvrykkikfzfi4609r4cxpccq") (f (quote (("osstring"))))))

(define-public crate-swrite-0.0.2 (c (n "swrite") (v "0.0.2") (h "14faf57hi4prjhfbhl7r5bllqy7316qqlf0fb58nv226sp7ppalc") (f (quote (("osstring"))))))

(define-public crate-swrite-0.1.0 (c (n "swrite") (v "0.1.0") (h "00vsicglylq4qq6dc497jdgzfnxi5mh7padwxijnvh1d1giyqgvz") (f (quote (("osstring"))))))

