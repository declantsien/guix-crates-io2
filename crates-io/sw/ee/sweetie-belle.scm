(define-module (crates-io sw ee sweetie-belle) #:use-module (crates-io))

(define-public crate-sweetie-belle-0.0.0 (c (n "sweetie-belle") (v "0.0.0") (h "1ylld4nnsjq3zxvy763w2j9i6xx2pswsafnzl205r95b49a0bmab") (y #t)))

(define-public crate-sweetie-belle-0.0.1 (c (n "sweetie-belle") (v "0.0.1") (h "0a34pk2z5hb4qmgkmj4ky184wfxsjw896sda735mwywqqr56icp8") (y #t)))

