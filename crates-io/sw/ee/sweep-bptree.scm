(define-module (crates-io sw ee sweep-bptree) #:use-module (crates-io))

(define-public crate-sweep-bptree-0.1.0 (c (n "sweep-bptree") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xwyw8ryipm8h3k41zrgfwbx89jliy1yhsnfp1rcr39gxq9pvjdm")))

(define-public crate-sweep-bptree-0.2.0 (c (n "sweep-bptree") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lpil26g8jfjn828xp43kgizw1rl2qngwbycah7ms6hibfwj2ndp")))

(define-public crate-sweep-bptree-0.3.0 (c (n "sweep-bptree") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1grh0dqpnqs9m7qwr08h2vjk25nvd5rjkbkqrbpcbk9ql27mrnc9")))

(define-public crate-sweep-bptree-0.4.0 (c (n "sweep-bptree") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pbr2828rwndbxvphac99d7n5j24200gh47ivnsrwv2hjx5yzv8n")))

(define-public crate-sweep-bptree-0.4.1 (c (n "sweep-bptree") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q4rbjhiwajxzqqc9gy9j5zdj8r5y6r9iv44mc5w9azaqnvv39xy")))

