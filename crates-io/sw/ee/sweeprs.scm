(define-module (crates-io sw ee sweeprs) #:use-module (crates-io))

(define-public crate-sweeprs-0.1.0 (c (n "sweeprs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0k30iflm9sczinzk5gdw4kwvah7s1jgyrsc2v4sydxdgsivdjhw2")))

(define-public crate-sweeprs-0.2.0 (c (n "sweeprs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0spb31fj6g8alyg2npb1dvmgg91fwwbs0lq1l6v0aah1dghzhnh9")))

