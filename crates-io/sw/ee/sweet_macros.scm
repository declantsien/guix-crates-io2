(define-module (crates-io sw ee sweet_macros) #:use-module (crates-io))

(define-public crate-sweet_macros-0.1.31 (c (n "sweet_macros") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfmpg7yn46x08fyv4c3mgnqa4f9xpk2sv6zpdck3kzr9njpsbcv")))

(define-public crate-sweet_macros-0.1.32 (c (n "sweet_macros") (v "0.1.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1clpq828hmwfirm1ybrrgqkgs7n5d2sj21xyd4rm613h4k2qab4z")))

(define-public crate-sweet_macros-0.1.33 (c (n "sweet_macros") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3abzd1r45d8jj4bqnjxmifp8nkcca0bwnpdhzfxmir9na4267c")))

(define-public crate-sweet_macros-0.1.34 (c (n "sweet_macros") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0k183zrr1x6ybg73n05p7ivr2m93ykzvs19cgy5ir11rbw5srqrf")))

(define-public crate-sweet_macros-0.1.35 (c (n "sweet_macros") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0074q4ksx54m3gm7856z5ayy42q91q6xxgh6dnvn05pnddm6bgk3")))

(define-public crate-sweet_macros-0.1.36 (c (n "sweet_macros") (v "0.1.36") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0ilnc4qkfsgqwq8ycqhyvnsb6kqri0yqywwb0k19glcr5kdrxl18")))

(define-public crate-sweet_macros-0.1.37 (c (n "sweet_macros") (v "0.1.37") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "07560ambmjp8pf3vkkibzpmvaylqp0bw61j9ymsd45i2svmnwzvr")))

(define-public crate-sweet_macros-0.1.38 (c (n "sweet_macros") (v "0.1.38") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "092kagdcx5iw29r87cg8hg7wpxjdrs719ynm75r0lzvn5jz6x1kw")))

(define-public crate-sweet_macros-0.1.39 (c (n "sweet_macros") (v "0.1.39") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmxr61qxqdfngn54hmnmy765hn8lq9pidslfnkfxdm7gbk3g5b7")))

(define-public crate-sweet_macros-0.1.40 (c (n "sweet_macros") (v "0.1.40") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9mbjj663g8xskcrlyx9k2fjbrv6vipllf7rimlkvyb81i9p0k0")))

(define-public crate-sweet_macros-0.1.41 (c (n "sweet_macros") (v "0.1.41") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "00hkfd0lxj4kphipr4vmlcxn3xlnyd6ls3gpwzw2gkhv85cbwsvz")))

(define-public crate-sweet_macros-0.1.42 (c (n "sweet_macros") (v "0.1.42") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0c7mrpmpi1ajxcpzq0a9m5f3pzh66kdqxdz1z02haf4133zjlfrx")))

(define-public crate-sweet_macros-0.1.43 (c (n "sweet_macros") (v "0.1.43") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1pnazjy773c42pvrxjp2hx409ldas0i40g2mrks9961whsrwkcrm")))

(define-public crate-sweet_macros-0.1.44 (c (n "sweet_macros") (v "0.1.44") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0kcn8xxx5r45zkb3b9qm4hwayzpkgyrz3c6si0ar95dxqwnjs4hr")))

(define-public crate-sweet_macros-0.1.45 (c (n "sweet_macros") (v "0.1.45") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "13awq9wibn0qf42my2s2xmdha6wwwagxjn7dchcz28lgl5n0yq9x")))

(define-public crate-sweet_macros-0.1.46 (c (n "sweet_macros") (v "0.1.46") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1whc2rqcpk6445bpir7kcl84f4brd2364a5zs2smnnk7sgkw7j45")))

