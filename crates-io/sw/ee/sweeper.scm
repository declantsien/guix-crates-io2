(define-module (crates-io sw ee sweeper) #:use-module (crates-io))

(define-public crate-sweeper-0.1.0 (c (n "sweeper") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0r8as8m05gyqcqd9zcp8mzwn79vrwr1rrhfirjyybksjp26y9kfz") (f (quote (("track_caller") ("std" "rand/std") ("generation" "rand") ("default" "std" "generation"))))))

(define-public crate-sweeper-0.2.0 (c (n "sweeper") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0jisa5d6iwrxzi2d244qdz4nnqplndx51clnrmpp1l2xcxy8arvr") (f (quote (("track_caller") ("std" "rand/std") ("generation" "rand") ("default" "std" "generation"))))))

(define-public crate-sweeper-0.2.1 (c (n "sweeper") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "03kf411ixhig6jdbmkm36sawkz2yffnxmmqhx1i67jl630ixgmzg") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serde")))) (y #t)))

(define-public crate-sweeper-0.2.2 (c (n "sweeper") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1rq08wx8d0xh9y74dcx74833cq5w40f07flslnzs59r2wjhnd8pz") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization")))) (y #t)))

(define-public crate-sweeper-0.2.3 (c (n "sweeper") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1r5fvcsl32ijaglh2zlrvxan1kfkj7zgsv1i1ibc9b9x0h4qv727") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization"))))))

(define-public crate-sweeper-0.3.0 (c (n "sweeper") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0yx0xmqs6mj1vfc76rcfx4lr6dyiz4ayclam01cdb8q7199w1g5b") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization")))) (y #t)))

(define-public crate-sweeper-0.3.1 (c (n "sweeper") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1rk1pi1jk0kk3567j7fwd286im2h54jsmxf7c22xxfjfqrb5m5x0") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization"))))))

(define-public crate-sweeper-1.0.0-rc1 (c (n "sweeper") (v "1.0.0-rc1") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0mrv8v4x0vwa8z5pw635nkvylajnlz6j692072sw0xhciz4299bh") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization"))))))

(define-public crate-sweeper-1.0.0-rc2 (c (n "sweeper") (v "1.0.0-rc2") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0lwzkmqgqmq5b9nnwfqnallwg32syyk1g844spbkb2m0adqsg553") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization"))))))

(define-public crate-sweeper-1.0.0-rc3 (c (n "sweeper") (v "1.0.0-rc3") (d (list (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1j8y4wskc217s7sjxjl286hrcs9cy8yv2qj5lkyf7vdhmizcjbfz") (f (quote (("track_caller") ("std" "rand/std" "serde/std") ("serialization" "serde") ("generation" "rand") ("default" "std" "generation" "serialization"))))))

