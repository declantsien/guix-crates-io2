(define-module (crates-io sw ay sway-easyfocus) #:use-module (crates-io))

(define-public crate-sway-easyfocus-0.1.0 (c (n "sway-easyfocus") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6") (f (quote ("v0_6"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "1d9ylkclj7sbs0ip5qxcq7znr9wl6mkfsglfh0dvvk7m5yinb952")))

(define-public crate-sway-easyfocus-0.1.1 (c (n "sway-easyfocus") (v "0.1.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6") (f (quote ("v0_6"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "13gqm5ccd1xbpnnx8y79my7s1icvx8h3x2678yqibhs0v6wkqdjx")))

(define-public crate-sway-easyfocus-0.1.2 (c (n "sway-easyfocus") (v "0.1.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6") (f (quote ("v0_6"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.0") (d #t) (k 0)))) (h "1pnb6cb0f45z5hbwhca5bhgdfsb17bvq9m4dgshhi6xajbdxghv5")))

(define-public crate-sway-easyfocus-0.1.3 (c (n "sway-easyfocus") (v "0.1.3") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "gtk") (r "^0.17") (d #t) (k 0)) (d (n "gtk-layer-shell") (r "^0.6") (f (quote ("v0_6"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.0") (d #t) (k 0)))) (h "1ry9zmy6hbyz1diirwnbb49cfz9qb0iwrxqk7akzgm5v7hnh8dhc")))

