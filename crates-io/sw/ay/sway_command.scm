(define-module (crates-io sw ay sway_command) #:use-module (crates-io))

(define-public crate-sway_command-0.1.0 (c (n "sway_command") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 2)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "1gfq94gwvw5f2pvp70r9rnf7kvvl9xfp4pyws1xndddppwjm7jk9") (f (quote (("default" "serde"))))))

(define-public crate-sway_command-0.1.1 (c (n "sway_command") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 2)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "11ln4nnkyfx9jgr4rccxw4g3r2kjygi2mg13vc6lrb9c79am36vv") (f (quote (("default" "serde"))))))

