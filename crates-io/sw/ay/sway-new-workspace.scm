(define-module (crates-io sw ay sway-new-workspace) #:use-module (crates-io))

(define-public crate-sway-new-workspace-0.1.0 (c (n "sway-new-workspace") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "0pjzhmyjhxlql107sayksszl0amkjsnrlsvakw1rmp03b1s88qcd")))

(define-public crate-sway-new-workspace-0.1.1 (c (n "sway-new-workspace") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "07kk4xq13dmr9d5zjgwppkp8al0rhx9z6zl5djphsq79hnhdb24f")))

(define-public crate-sway-new-workspace-0.1.2 (c (n "sway-new-workspace") (v "0.1.2") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "1k8xz8y46wgz3bc2fzgpv84fdfmg0c50924vsrn9wc8sqdn60zqw")))

(define-public crate-sway-new-workspace-0.1.3 (c (n "sway-new-workspace") (v "0.1.3") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "03n4wwsn2q9lmxhk4b6rjdzw55imz5q3riai8y1hmgb9v8kvdvab")))

(define-public crate-sway-new-workspace-0.1.4 (c (n "sway-new-workspace") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "1klwgm4r8qwh6fs4agqi21jzizvz19d1nn7da2v8b89vfb4pjnsh")))

(define-public crate-sway-new-workspace-0.1.5 (c (n "sway-new-workspace") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "04qv546ld07m787l4q02b4i5gwx8i6dn9yfxf3biyz0s58f72zjh")))

