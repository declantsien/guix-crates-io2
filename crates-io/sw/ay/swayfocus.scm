(define-module (crates-io sw ay swayfocus) #:use-module (crates-io))

(define-public crate-swayfocus-0.1.0 (c (n "swayfocus") (v "0.1.0") (d (list (d (n "daemonize") (r "^0.3.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "i3ipc") (r "^0.9.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0rvwcnmck263grp8hl7b5z1nccdlddggmlyq8r75cqrh9da0dx80")))

(define-public crate-swayfocus-0.1.1 (c (n "swayfocus") (v "0.1.1") (d (list (d (n "daemonize") (r "^0.3.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "i3ipc") (r "^0.9.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0159ws0q3fnmb5373hqxd0kjzd72faz90gm0zi9dhpkpwfcvx552")))

(define-public crate-swayfocus-0.1.2 (c (n "swayfocus") (v "0.1.2") (d (list (d (n "daemonize") (r "^0.3.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "i3ipc") (r "^0.9.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0nw7iz6cga1zjq8hprkyljfymj0mfh6nxqhzys76chkn4b7wyf4n")))

