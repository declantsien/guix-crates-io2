(define-module (crates-io sw ay sway-mpris-idle-inhibit) #:use-module (crates-io))

(define-public crate-sway-mpris-idle-inhibit-0.1.6 (c (n "sway-mpris-idle-inhibit") (v "0.1.6") (d (list (d (n "mpris") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.5") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.30.0-beta.5") (f (quote ("client" "unstable"))) (d #t) (k 0)))) (h "1zcbpzxnzmnnhwafamkqdc084rs7bvpm4qf08vdvgd90mj005riz")))

