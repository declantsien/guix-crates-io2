(define-module (crates-io sw ay sway-fmt) #:use-module (crates-io))

(define-public crate-sway-fmt-0.1.1 (c (n "sway-fmt") (v "0.1.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.1") (d #t) (k 0)))) (h "0v8vb6xdvdfrin8hsl1s5ajv1dyrk7gmjbd0ddq7ifh9ydhhh4x5")))

(define-public crate-sway-fmt-0.1.2 (c (n "sway-fmt") (v "0.1.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.2") (d #t) (k 0)))) (h "0fl7qskc05q394fl2xcpi25wbhwavnbrx9l2bv9l7a46w75amlzz")))

(define-public crate-sway-fmt-0.1.3 (c (n "sway-fmt") (v "0.1.3") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.3") (d #t) (k 0)))) (h "0nzsys5minjz8k1igzz7p6haw8az0gf0q6cn143avrbmxb607zfm")))

(define-public crate-sway-fmt-0.1.4 (c (n "sway-fmt") (v "0.1.4") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.4") (d #t) (k 0)))) (h "0zd395a0ixzik8zjafhl8m35x056l8bidcrn7c1s0x1gs0h7h0x8")))

(define-public crate-sway-fmt-0.1.5 (c (n "sway-fmt") (v "0.1.5") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.5") (d #t) (k 0)))) (h "1iakmd8li84lvrxny6vda7l6m2z3qxjdiiq69ry50gzsa75ri7cv")))

(define-public crate-sway-fmt-0.1.6 (c (n "sway-fmt") (v "0.1.6") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.6") (d #t) (k 0)))) (h "0mr985rc1v4x7z8xdx8j40mfdb4dq7w9wpwampyw1y60gsmb4f3f")))

(define-public crate-sway-fmt-0.1.7 (c (n "sway-fmt") (v "0.1.7") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.7") (d #t) (k 0)))) (h "0f64299x0dz443crmz7mrhmhhv19bn82078601253lxb7n9nlg83")))

(define-public crate-sway-fmt-0.1.9 (c (n "sway-fmt") (v "0.1.9") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.1.9") (d #t) (k 0)))) (h "1glpdqz0axz0y5qb4zhz9s7gxwn91hig0jby47g6d35vhl0ymcdw")))

(define-public crate-sway-fmt-0.2.1 (c (n "sway-fmt") (v "0.2.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.2.1") (d #t) (k 0)))) (h "161g49ydzx4gj2g3hjik180aqr8phnimmm994gmvxdn09l295gb1")))

(define-public crate-sway-fmt-0.3.0 (c (n "sway-fmt") (v "0.3.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.3.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.3.0") (d #t) (k 0)))) (h "11zc9hyp7hwf7cwcv3dci3br1gri49scpk0kv0iqnibr4kz57zvb")))

(define-public crate-sway-fmt-0.3.1 (c (n "sway-fmt") (v "0.3.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.3.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.3.1") (d #t) (k 0)))) (h "0p7rrf9z6lymc6m8ghkqk5mh02hn6qgx2n0156b1yvlg6x7cpqgh")))

(define-public crate-sway-fmt-0.3.3 (c (n "sway-fmt") (v "0.3.3") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.3.3") (d #t) (k 0)) (d (n "sway-types") (r "^0.3.3") (d #t) (k 0)))) (h "1vf03c5blch5xbf836p61ahbdrds242nw111fxh7pp8lbi86yz0i")))

(define-public crate-sway-fmt-0.4.0 (c (n "sway-fmt") (v "0.4.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.4.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.4.0") (d #t) (k 0)))) (h "1gznz0i2sh0awrn00hh07dbv4wivx6ds2zhahxfvniik13gmw6lb")))

(define-public crate-sway-fmt-0.5.0 (c (n "sway-fmt") (v "0.5.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.5.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.5.0") (d #t) (k 0)))) (h "000yknr039y0ngn5slxq4rbpkpffv73plvhh7dqxbrvp11sk4yin")))

(define-public crate-sway-fmt-0.6.0 (c (n "sway-fmt") (v "0.6.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.6.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.6.0") (d #t) (k 0)))) (h "1wjw1qrqd6yapx6h7bjwmxczzx6fscn2n91lwwf8rnx8d2f408xq")))

(define-public crate-sway-fmt-0.6.1 (c (n "sway-fmt") (v "0.6.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.6.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.6.1") (d #t) (k 0)))) (h "1mgdiixljd385dxcp3isx5ckgnschi5rnzq3d5lkbvlksw62raln")))

(define-public crate-sway-fmt-0.7.0 (c (n "sway-fmt") (v "0.7.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.7.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.7.0") (d #t) (k 0)))) (h "1d4skj3rya62wwpln52il43l4ykl20vy17ap6nzry2bqfzf63lgj")))

(define-public crate-sway-fmt-0.8.0 (c (n "sway-fmt") (v "0.8.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.8.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.8.0") (d #t) (k 0)))) (h "1if2gxlzf03x3hy2hxmy6wznwk4111fp364i0rxhxh4q336f9nyg")))

(define-public crate-sway-fmt-0.9.1 (c (n "sway-fmt") (v "0.9.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.9.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.9.1") (d #t) (k 0)))) (h "168b4yhg89il0zk6y7dsp6j39rbfs71jwmhlq3g8lvahck86zhl8")))

(define-public crate-sway-fmt-0.9.2 (c (n "sway-fmt") (v "0.9.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.9.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.9.2") (d #t) (k 0)))) (h "0zc4h05hm7nxmlc4iv51hlx5y31fwxnryvjf7rdiihlgvdb4qrwr")))

(define-public crate-sway-fmt-0.10.0 (c (n "sway-fmt") (v "0.10.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.10.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.10.0") (d #t) (k 0)))) (h "0cylh6ss2ijpawx40d7k1v85mc77ss1wp4i3rn848f3mlpvgwgih")))

(define-public crate-sway-fmt-0.10.1 (c (n "sway-fmt") (v "0.10.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.10.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.10.1") (d #t) (k 0)))) (h "072fw1m1d9j3by0pvwj0f0nab55lgawg0fjcnnzxhnrmpypga7nw")))

(define-public crate-sway-fmt-0.10.3 (c (n "sway-fmt") (v "0.10.3") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.10.3") (d #t) (k 0)) (d (n "sway-types") (r "^0.10.3") (d #t) (k 0)))) (h "1pxqs2iw32fjk3p31rzzjcn75l2vrmc04259dlgpjq6p74k8qmdm")))

(define-public crate-sway-fmt-0.11.0 (c (n "sway-fmt") (v "0.11.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.11.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.11.0") (d #t) (k 0)))) (h "1kh4xqnb1jzmqnmg54dzd3mhxzzk15qb9fyrjsnhl4mnzghkpzmx")))

(define-public crate-sway-fmt-0.12.1 (c (n "sway-fmt") (v "0.12.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.12.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.12.1") (d #t) (k 0)))) (h "05rfbld56ag62djbwcp0gf2fgjjr3dgpwjp5bnqqf8nksd999rgk")))

(define-public crate-sway-fmt-0.12.2 (c (n "sway-fmt") (v "0.12.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.12.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.12.2") (d #t) (k 0)))) (h "0jdfac0lnsv7b5m16n5f9rhdsnb00rn0g9xvdipvmj9aklilb32z")))

(define-public crate-sway-fmt-0.13.0 (c (n "sway-fmt") (v "0.13.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.13.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.13.0") (d #t) (k 0)))) (h "0vw7al4jiqgjkjgzvvg4gq00zxzip3mclx46663x0g149jn0c3hw")))

(define-public crate-sway-fmt-0.13.1 (c (n "sway-fmt") (v "0.13.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.13.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.13.1") (d #t) (k 0)))) (h "0cricr16d95r79div5k26xl9035d3hqld3351an1m7gxkgk1clnw")))

(define-public crate-sway-fmt-0.13.2 (c (n "sway-fmt") (v "0.13.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.13.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.13.2") (d #t) (k 0)))) (h "0lkn2cnb8xwzajqkyywx2462rngwh9qvsrc8gyjc2mimvrfmmj21")))

(define-public crate-sway-fmt-0.14.0 (c (n "sway-fmt") (v "0.14.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.0") (d #t) (k 0)))) (h "1ybvh70qk8ip6gy6ivyzfg9v04jsn1410gwj17jf18bpy9abbdia")))

(define-public crate-sway-fmt-0.14.1 (c (n "sway-fmt") (v "0.14.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.1") (d #t) (k 0)))) (h "093832w940mld3m424mlxrmfjffycwwy5yvp2np8ydvi2dlmz450")))

(define-public crate-sway-fmt-0.14.2 (c (n "sway-fmt") (v "0.14.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.2") (d #t) (k 0)))) (h "1f2i61l508z9lb0y7xwd252sdc3crhla8gkwyvdlb5f4h0crjdk8")))

(define-public crate-sway-fmt-0.14.3 (c (n "sway-fmt") (v "0.14.3") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.3") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.3") (d #t) (k 0)))) (h "1kb7wjzs1zks9jlqkp4y06pa928hwyh8r6if9k9hgdrgqjqsf4mr")))

(define-public crate-sway-fmt-0.14.4 (c (n "sway-fmt") (v "0.14.4") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.4") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.4") (d #t) (k 0)))) (h "02f5ysgq3zh1g2w0ssjyn6fsidgimjlkwqz15ym5b54m4bpn2q8y")))

(define-public crate-sway-fmt-0.14.5 (c (n "sway-fmt") (v "0.14.5") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.14.5") (d #t) (k 0)) (d (n "sway-types") (r "^0.14.5") (d #t) (k 0)))) (h "1c71xbxc1cjcvn3wi47d9sgzdpvyaad7b1hxhc5nv1vza9hqlhxb")))

(define-public crate-sway-fmt-0.15.1 (c (n "sway-fmt") (v "0.15.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.15.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.15.1") (d #t) (k 0)))) (h "18nhhg2yiyb0d0fqs867b23w9b0isvyxdvvsy5w6kjh7i6cyl67p")))

(define-public crate-sway-fmt-0.15.2 (c (n "sway-fmt") (v "0.15.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.15.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.15.2") (d #t) (k 0)))) (h "06jkhwvhwy6907w0a75rwqyg0acw3all1hxwj0gh01f8v4fk24m8")))

(define-public crate-sway-fmt-0.16.0 (c (n "sway-fmt") (v "0.16.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.16.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.16.0") (d #t) (k 0)))) (h "1rcz68649rnirgwbx77809kqxfjg6hlkwqk90rif5rhfcj6q2ry6")))

(define-public crate-sway-fmt-0.16.1 (c (n "sway-fmt") (v "0.16.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.16.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.16.1") (d #t) (k 0)))) (h "0k3lcwh4inpndhd4v552q6h4sx2v1pqxs1dfi5j8hk05v4mfhmpn")))

(define-public crate-sway-fmt-0.16.2 (c (n "sway-fmt") (v "0.16.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.16.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.16.2") (d #t) (k 0)))) (h "16dmi9ha53lhxl87jxbs1qncg8r6fz7hakf1y2jb6776dz246pbx")))

(define-public crate-sway-fmt-0.17.0 (c (n "sway-fmt") (v "0.17.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.17.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.17.0") (d #t) (k 0)))) (h "1zgqxy2pm57vcf5zlpm1p1n0pibvg47gdfsgzm02g84kl99lafz9")))

(define-public crate-sway-fmt-0.18.0 (c (n "sway-fmt") (v "0.18.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.18.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.18.0") (d #t) (k 0)))) (h "1dp07mq3g40lmj4anz4dd5a6rkzpkr79y1fmqlsnjvy5gbjfn627")))

(define-public crate-sway-fmt-0.18.1 (c (n "sway-fmt") (v "0.18.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.18.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.18.1") (d #t) (k 0)))) (h "0kngx3ha3s2lp3b4rgb2ynlm3njifaxc5033s5pyk3irmq5cx6wf")))

(define-public crate-sway-fmt-0.19.0 (c (n "sway-fmt") (v "0.19.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.19.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.19.0") (d #t) (k 0)))) (h "1nx5jw5r95hkqif7x4lk0dlhk9n6wqxcji901fqjw2yw6jgzx4b8")))

(define-public crate-sway-fmt-0.19.1 (c (n "sway-fmt") (v "0.19.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.19.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.19.1") (d #t) (k 0)))) (h "021xffxf3s5s4b3gfh6rbdp2infppbm1fswx94dlwpsw4hvazyli")))

(define-public crate-sway-fmt-0.19.2 (c (n "sway-fmt") (v "0.19.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.19.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.19.2") (d #t) (k 0)))) (h "17vibqf46in15xwp42hcq6yjh3haf15d6hkg26gagv8wlcxbd33m")))

(define-public crate-sway-fmt-0.20.0 (c (n "sway-fmt") (v "0.20.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.20.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.0") (d #t) (k 0)))) (h "14xahzyxajy32mplqg445pw2slgqx0brg90pm9nz5g1pzdd80khm")))

(define-public crate-sway-fmt-0.20.1 (c (n "sway-fmt") (v "0.20.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.20.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.1") (d #t) (k 0)))) (h "1l9ks2r9z13g2449pm4v6a8s5ng66jyj1rrc98zai2jl4gfphlws")))

(define-public crate-sway-fmt-0.20.2 (c (n "sway-fmt") (v "0.20.2") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.20.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.2") (d #t) (k 0)))) (h "1xa9xhfvkfl8sn863rbrn7xz4z792sj6wk4airha578fvd4y8vds")))

(define-public crate-sway-fmt-0.21.0 (c (n "sway-fmt") (v "0.21.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.21.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.21.0") (d #t) (k 0)))) (h "1vff53cydz6fqscf29pf1sa564b2ywp0yx6xglqryavi0f0rjdda")))

(define-public crate-sway-fmt-0.22.0 (c (n "sway-fmt") (v "0.22.0") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.22.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.22.0") (d #t) (k 0)))) (h "1zvbyx7dsqmhxkdddkiar88v4y7v1d99lnc6jg850ly66xn8pdzr")))

(define-public crate-sway-fmt-0.22.1 (c (n "sway-fmt") (v "0.22.1") (d (list (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "sway-core") (r "^0.22.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.22.1") (d #t) (k 0)))) (h "1v3b3n8dd7b2ha6mw9phfq324ar2mjz26m216hq389xwmshcxs48")))

