(define-module (crates-io sw ay swaywm-alttab) #:use-module (crates-io))

(define-public crate-swaywm-alttab-0.2.1 (c (n "swaywm-alttab") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "09qhjr41gg9yvczjrf02254w52ldiymljv9f2dhk1j2qgjikya2c")))

(define-public crate-swaywm-alttab-0.2.2 (c (n "swaywm-alttab") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "0gvs29yran4qs10fcr9aiki00a59878b5nqcqhz6vkkrxrz6s35k")))

(define-public crate-swaywm-alttab-0.3.0 (c (n "swaywm-alttab") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "1nllrival51ps1vps9bvqbq82ma9asnlqhirbq6y1gi6wldpbpz5")))

(define-public crate-swaywm-alttab-0.3.1 (c (n "swaywm-alttab") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "0sn52nkidl48kivl4g4w0wjvj96hcr6rmggw13w2pkrq1b3538mk")))

(define-public crate-swaywm-alttab-0.3.2 (c (n "swaywm-alttab") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "1hpk0licasp0ax87wv22z4zr9ab23g64dpmlvkf3pkmcj1yrnv40")))

