(define-module (crates-io sw ay sway-ast) #:use-module (crates-io))

(define-public crate-sway-ast-0.19.2 (c (n "sway-ast") (v "0.19.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.19.2") (d #t) (k 0)))) (h "0jrgrh5z8pid6l2cd90fpk2k862i76px0i2kwrgfj5nycjphrayq")))

(define-public crate-sway-ast-0.20.0 (c (n "sway-ast") (v "0.20.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.0") (d #t) (k 0)))) (h "1hbig0lvmy886hlvw0m8ccjphhk3703rvqqpikx7f542blin7q1m")))

(define-public crate-sway-ast-0.20.1 (c (n "sway-ast") (v "0.20.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.1") (d #t) (k 0)))) (h "0xk45yn407775mj5qg12ld6rh5x3adpp5h8izqvk4bjpv96kwnp8")))

(define-public crate-sway-ast-0.20.2 (c (n "sway-ast") (v "0.20.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.20.2") (d #t) (k 0)))) (h "11ihf4cz64j9w46yl10298sp1gg8mzc2174kw5sjxjbdi1rclsxk")))

(define-public crate-sway-ast-0.21.0 (c (n "sway-ast") (v "0.21.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.21.0") (d #t) (k 0)))) (h "1gnfj5f5d7xnk2x0y1p99sj8kpfsh62nc17g525z1rvs8p4ig5fv")))

(define-public crate-sway-ast-0.22.0 (c (n "sway-ast") (v "0.22.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.22.0") (d #t) (k 0)))) (h "106fysf93yckg7ifdkzzm7k0izmbxj1z1hbgd27j05pmwijyy459")))

(define-public crate-sway-ast-0.22.1 (c (n "sway-ast") (v "0.22.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.22.1") (d #t) (k 0)))) (h "0hcdlw5kcyjbq2sbai4l011129zg7rnfwi3arn0jifay50j0i2p1")))

(define-public crate-sway-ast-0.23.0 (c (n "sway-ast") (v "0.23.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.23.0") (d #t) (k 0)))) (h "0b1hxdl2hadk6h0pdxj38w52x9v6wcrmj1hwprfd6ngkixhrp4g7")))

(define-public crate-sway-ast-0.24.0 (c (n "sway-ast") (v "0.24.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.0") (d #t) (k 0)))) (h "18l8bmy2lw31r3skfq1xgqd4kkcai8f46xq7xc5vdb6vh74idky1")))

(define-public crate-sway-ast-0.24.1 (c (n "sway-ast") (v "0.24.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.1") (d #t) (k 0)))) (h "1ib18370g9r580rgzm9gy8fcwgmz2pbi96341x6fdlw0k3rsiiv8")))

(define-public crate-sway-ast-0.24.2 (c (n "sway-ast") (v "0.24.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.2") (d #t) (k 0)))) (h "1h6ypxx0y4sgpc2s2kfi05ck6x0ass4vjzap9llxjza6k7qarrfy")))

(define-public crate-sway-ast-0.24.3 (c (n "sway-ast") (v "0.24.3") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.3") (d #t) (k 0)))) (h "05749p6wvs6fskrsqswqqyl4mb4i0r9fawr8l9p25n0pqhx2adv1")))

(define-public crate-sway-ast-0.24.4 (c (n "sway-ast") (v "0.24.4") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.4") (d #t) (k 0)))) (h "1bvqqynzzmz0z2jycd23kgffnwr3zvc35jh7wgp55k4v1r7y63b2")))

(define-public crate-sway-ast-0.24.5 (c (n "sway-ast") (v "0.24.5") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.24.5") (d #t) (k 0)))) (h "0s4v6ik9sgvrk8519kixncjhwcxmgljjdvbvivlp3rsk3qwxgd97")))

(define-public crate-sway-ast-0.25.0 (c (n "sway-ast") (v "0.25.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.25.0") (d #t) (k 0)))) (h "1qd101mz2ixvny7r7ff3lg0vk9cdbjrmh7i2b3fd2dff6gr82kdb")))

(define-public crate-sway-ast-0.25.1 (c (n "sway-ast") (v "0.25.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.25.1") (d #t) (k 0)))) (h "0zywc2qswxcxxy479dxkpmddd1z2qps3ahy502vp8fhmgilpjka0")))

(define-public crate-sway-ast-0.25.2 (c (n "sway-ast") (v "0.25.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.25.2") (d #t) (k 0)))) (h "0ran74f13h9vpc8pdg95av1jr0za90r3cqhymv0lkxwgy626f0zk")))

(define-public crate-sway-ast-0.26.0 (c (n "sway-ast") (v "0.26.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.26.0") (d #t) (k 0)))) (h "1jkaqf13cgnd40l4y8sclrz5vr76rx31r5hpwydfb9g6w2jpcg5i")))

(define-public crate-sway-ast-0.27.0 (c (n "sway-ast") (v "0.27.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.27.0") (d #t) (k 0)))) (h "0dgxry3zpq1012yjg1d4zw80736n2nng1pq97ygg2wnwfwb7w2g9")))

(define-public crate-sway-ast-0.28.0 (c (n "sway-ast") (v "0.28.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.28.0") (d #t) (k 0)))) (h "0h48vx8yjd9swrgjlvzvwfpjmf820qf63j9r30xxc4jwsbfih42q")))

(define-public crate-sway-ast-0.28.1 (c (n "sway-ast") (v "0.28.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.28.1") (d #t) (k 0)))) (h "0v9ypjk5hdmi58rrms8bs5nfzdc7yq07v1rpbqv197brz6sjgqfv")))

(define-public crate-sway-ast-0.29.0 (c (n "sway-ast") (v "0.29.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.29.0") (d #t) (k 0)))) (h "1s6idwha9mcn04bwifvkyjbn57i1nc4jxsd3gylswi4fqfkxrnmi")))

(define-public crate-sway-ast-0.30.0 (c (n "sway-ast") (v "0.30.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.30.0") (d #t) (k 0)))) (h "0b6p0azvsy0jzpllg5yxjb7z9dkzgixraafgvdy75mk4mp9njg0n")))

(define-public crate-sway-ast-0.30.1 (c (n "sway-ast") (v "0.30.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.30.1") (d #t) (k 0)))) (h "0d3gbqpw8izj4x9labwrkl6r3l8gan4k852jpzkv95h6z258j79f")))

(define-public crate-sway-ast-0.31.0 (c (n "sway-ast") (v "0.31.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.31.0") (d #t) (k 0)))) (h "136sar3z8szv2na2am795jif4l0n5rvq6rp98haqmc2si1s3pwgl")))

(define-public crate-sway-ast-0.31.1 (c (n "sway-ast") (v "0.31.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.31.1") (d #t) (k 0)))) (h "1hv6bw47r9adijhxf6jfpzjlcm2cp3zr1hkjym4r4gh6jhfks1jy")))

(define-public crate-sway-ast-0.31.2 (c (n "sway-ast") (v "0.31.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.31.2") (d #t) (k 0)))) (h "0yij082bcs5lz2n7r0gad7cbwz2xn4x15z5smz5iy7ycs1qvqsyw")))

(define-public crate-sway-ast-0.31.3 (c (n "sway-ast") (v "0.31.3") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.31.3") (d #t) (k 0)))) (h "0mgg33zdcpl1rw9w9w70ixcb9swkpmv8wzrwyqxqxiq69givx7h7")))

(define-public crate-sway-ast-0.32.0 (c (n "sway-ast") (v "0.32.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.32.0") (d #t) (k 0)))) (h "15w2lazlmbmwl46ayximnmdg188sj1vkcrym07385bvgazxxl2fn")))

(define-public crate-sway-ast-0.32.1 (c (n "sway-ast") (v "0.32.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.32.1") (d #t) (k 0)))) (h "028j8imjc1i5mab4sjq13zdgzfpn43vcmx7i46klwfy0lfjig1jz")))

(define-public crate-sway-ast-0.32.2 (c (n "sway-ast") (v "0.32.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.32.2") (d #t) (k 0)))) (h "17dr1ba4yfc79nqz22m9awznvdp2smllnn831cvf0i9nayj641pa")))

(define-public crate-sway-ast-0.33.0 (c (n "sway-ast") (v "0.33.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.33.0") (d #t) (k 0)))) (h "1priaxw5ihk7j995l7fixsfdbw1ljfl005l0qszh4ng729j1s7wc")))

(define-public crate-sway-ast-0.33.1 (c (n "sway-ast") (v "0.33.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.33.1") (d #t) (k 0)))) (h "1rv7i5ac9zdxylxjpg3z7w42l8mpzs4v0ryq8965dlhni60qnbcs")))

(define-public crate-sway-ast-0.34.0 (c (n "sway-ast") (v "0.34.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.34.0") (d #t) (k 0)))) (h "1g5hv1cwzbmqvk52nhgbmp3dgps8ja1cv6zdv5hxyplyw07v69pj")))

(define-public crate-sway-ast-0.35.0 (c (n "sway-ast") (v "0.35.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.0") (d #t) (k 0)))) (h "05nvja2x0s3mjscqn7zl2i9jaj1b52mxr3p4s58wnjm111sjiflk")))

(define-public crate-sway-ast-0.35.1 (c (n "sway-ast") (v "0.35.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.1") (d #t) (k 0)))) (h "0dmm2jaklgh81dkivdaa6lhi30kanswhvfa3xskmxg74vmp9j3y8")))

(define-public crate-sway-ast-0.35.2 (c (n "sway-ast") (v "0.35.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.2") (d #t) (k 0)))) (h "0hk8xb5jk5k4jpia41409rswbsvs79iy2z80kwc2vlms1xq7y3qp")))

(define-public crate-sway-ast-0.35.3 (c (n "sway-ast") (v "0.35.3") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.3") (d #t) (k 0)))) (h "0xyqiih9q9nfhq8aqab4lfm7sx8szkglyi139szdnfdi6qd2fpkg")))

(define-public crate-sway-ast-0.35.4 (c (n "sway-ast") (v "0.35.4") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.4") (d #t) (k 0)))) (h "09w8cm6h27m02rjkmzjbj5a4lxwiqlmzap3pcmsgpxa93l540ay5")))

(define-public crate-sway-ast-0.35.5 (c (n "sway-ast") (v "0.35.5") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sway-types") (r "^0.35.5") (d #t) (k 0)))) (h "1479ibzv528v78ca3wm6ykklns9hpfk8qasn13yc99nr0r5slms2")))

(define-public crate-sway-ast-0.36.0 (c (n "sway-ast") (v "0.36.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.36.0") (d #t) (k 0)))) (h "0wp1qgh35g92gx8zv8qkqjnwmh2k0qzrv5zx8vs4i8ikbmrrz6hx")))

(define-public crate-sway-ast-0.36.1 (c (n "sway-ast") (v "0.36.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.36.1") (d #t) (k 0)))) (h "0sc4nhmcv2b6kg3y9b68pfml19y6wk6x4m6m84i1p1wrib61d3rw")))

(define-public crate-sway-ast-0.37.0 (c (n "sway-ast") (v "0.37.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.37.0") (d #t) (k 0)))) (h "0bi5qhnx40sjsbg9ip37w95gdgpyn2yzn193gwnpyaqrmmy371r3")))

(define-public crate-sway-ast-0.37.1 (c (n "sway-ast") (v "0.37.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.37.1") (d #t) (k 0)))) (h "1p6rp545rhfm2wlbrh2rlkq97jj37ga8v5mcj2iba7hmslqnr5xw")))

(define-public crate-sway-ast-0.37.2 (c (n "sway-ast") (v "0.37.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.37.2") (d #t) (k 0)))) (h "12m64xqhk5msyvliwcq5k0m7in5a96p9ql18248973rgbaj8rk6v")))

(define-public crate-sway-ast-0.37.3 (c (n "sway-ast") (v "0.37.3") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.37.3") (d #t) (k 0)))) (h "0pjwwych55s82cq9hnfln6wxdpqm5ndm5x2cxmcjcg1miagfpxy1")))

(define-public crate-sway-ast-0.38.0 (c (n "sway-ast") (v "0.38.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.38.0") (d #t) (k 0)))) (h "0fn2khczg8hdfphpx7rhdns5ac47yhfki7vrqi2axlrprcmibzdz")))

(define-public crate-sway-ast-0.39.0 (c (n "sway-ast") (v "0.39.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.39.0") (d #t) (k 0)))) (h "07bamjzcmh2q1x2hiw39hc0pypaxc7a9vf1ghvbxjlwppmpn7qj7")))

(define-public crate-sway-ast-0.39.1 (c (n "sway-ast") (v "0.39.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.39.1") (d #t) (k 0)))) (h "0k7cy801mxcsjk89w12qbdmgr71fq08g0gcibprg40c2rlyhx86m")))

(define-public crate-sway-ast-0.40.0 (c (n "sway-ast") (v "0.40.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.40.0") (d #t) (k 0)))) (h "0zw6f44i481agxzq7ppd88zizdf5jqf66isb6lxrjzsiairg75v8")))

(define-public crate-sway-ast-0.40.1 (c (n "sway-ast") (v "0.40.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.40.1") (d #t) (k 0)))) (h "1x6yf5v8j62b1nxbmklk1xmdr7wb39mfrn7aqdpf13mc5vm6smr8")))

(define-public crate-sway-ast-0.41.0 (c (n "sway-ast") (v "0.41.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.41.0") (d #t) (k 0)))) (h "0gnfkq7crw15gbhrvir827fwdsmkrq9syy2k36blzirsdww7mbbi")))

(define-public crate-sway-ast-0.42.0 (c (n "sway-ast") (v "0.42.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.42.0") (d #t) (k 0)))) (h "0wy9xl2r13qc7mm1qd3l31pzp365g4yj0kfxpxbp70shnjw1z703")))

(define-public crate-sway-ast-0.42.1 (c (n "sway-ast") (v "0.42.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-types") (r "^0.42.1") (d #t) (k 0)))) (h "120x8bh1yinhl8a4hz0yndv7sn5yqyrpf5vcjwgdadpv1ydmd9x1")))

(define-public crate-sway-ast-0.43.0 (c (n "sway-ast") (v "0.43.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.43.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.43.0") (d #t) (k 0)))) (h "0z7vqi8jmjkcipd725xyjibwvycxvz4f1gkgdnr1idxwgq2ap5vf")))

(define-public crate-sway-ast-0.43.2 (c (n "sway-ast") (v "0.43.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.43.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.43.2") (d #t) (k 0)))) (h "1w0kjyk2rsl37ribj6jn687jnlmyw0q1q2jb4v3waj0gyvq4gck7")))

(define-public crate-sway-ast-0.44.0 (c (n "sway-ast") (v "0.44.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.44.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.44.0") (d #t) (k 0)))) (h "056rkvjz3snhxphmr5vh5pzbn59n01r38ax90jsv9dccbfcxm457")))

(define-public crate-sway-ast-0.44.1 (c (n "sway-ast") (v "0.44.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.44.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.44.1") (d #t) (k 0)))) (h "0as2rn9nk7ln83bxllli9lqj6n913xln7k7ryv3dczsgsx8j0z9x")))

(define-public crate-sway-ast-0.45.0 (c (n "sway-ast") (v "0.45.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.45.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.45.0") (d #t) (k 0)))) (h "1inlfg955fzvs71wzgn88w93nmlv5mb1phyd5kkkds6z5z9p515a")))

(define-public crate-sway-ast-0.46.0 (c (n "sway-ast") (v "0.46.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.46.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.46.0") (d #t) (k 0)))) (h "17bgq06c44wxhs5ny0gjqk0jp4yn0ykqvala9d2p2z657bbg9dg7")))

(define-public crate-sway-ast-0.46.1 (c (n "sway-ast") (v "0.46.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.46.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.46.1") (d #t) (k 0)))) (h "1hhmrg3lbz2d2vx3yvbjv9lc9pz9gqs94sbwnrgq1hshhblyfg22")))

(define-public crate-sway-ast-0.47.0 (c (n "sway-ast") (v "0.47.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.47.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.47.0") (d #t) (k 0)))) (h "0nvknmbld5hrq7pfikv968ww8310g95g4rwi7d3g1cxafagxjqic")))

(define-public crate-sway-ast-0.48.0 (c (n "sway-ast") (v "0.48.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.48.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.48.0") (d #t) (k 0)))) (h "1c29x4wq1qzg7k25nh3xjcxlfb3v4d8m7xcnirp5bv9pas8ac7q2")))

(define-public crate-sway-ast-0.48.1 (c (n "sway-ast") (v "0.48.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.48.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.48.1") (d #t) (k 0)))) (h "0rq37mf6llv1d54m6ljgyq63r6frwl6ym2hd79fwwm9cj3kmrccj")))

(define-public crate-sway-ast-0.49.1 (c (n "sway-ast") (v "0.49.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.49.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.49.1") (d #t) (k 0)))) (h "1cmh3mwavkwabsp615qmngx2w5c9zlm7i7xjzpqxbh7cqp6mclqy")))

(define-public crate-sway-ast-0.49.2 (c (n "sway-ast") (v "0.49.2") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.49.2") (d #t) (k 0)) (d (n "sway-types") (r "^0.49.2") (d #t) (k 0)))) (h "1dyd3zh0x7b5kin88y29xz07lrjfwpbjx56rsb5i2f5dk48sf585")))

(define-public crate-sway-ast-0.50.0 (c (n "sway-ast") (v "0.50.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.50.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.50.0") (d #t) (k 0)))) (h "19z0sf72xm92n427by4a46dyjmbav1hcnqb02a690lgmq5gg1isk")))

(define-public crate-sway-ast-0.51.0 (c (n "sway-ast") (v "0.51.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.51.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.51.0") (d #t) (k 0)))) (h "029264g02i6ib2ihl9wj3ynzxv9lms3hlwbyi23r6g55x2p8ib9c")))

(define-public crate-sway-ast-0.51.1 (c (n "sway-ast") (v "0.51.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.51.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.51.1") (d #t) (k 0)))) (h "06h8dk6ji3s1vcid57d90acgr9jiir4scszqk06lpflnizh4jync")))

(define-public crate-sway-ast-0.49.3 (c (n "sway-ast") (v "0.49.3") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.49.3") (d #t) (k 0)) (d (n "sway-types") (r "^0.49.3") (d #t) (k 0)))) (h "0lf62hjncas32zqrqdjrz75kp423drcwvihrldg2si0frmy9cns7")))

(define-public crate-sway-ast-0.52.0 (c (n "sway-ast") (v "0.52.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.52.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.52.0") (d #t) (k 0)))) (h "06479j6jgglh49capjpyc6zvgzhi4a11aybxjrj5c07cv6vy9baf")))

(define-public crate-sway-ast-0.52.1 (c (n "sway-ast") (v "0.52.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.52.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.52.1") (d #t) (k 0)))) (h "0707s0iglvyw0c9xn1qlsf23475skz6jn3frrbr7mgxgqci916c4")))

(define-public crate-sway-ast-0.53.0 (c (n "sway-ast") (v "0.53.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.53.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.53.0") (d #t) (k 0)))) (h "0cp6mb0n6hbggxxbzxv867znhck6k3f2w7875kr4fpzpw13xmis4")))

(define-public crate-sway-ast-0.54.0 (c (n "sway-ast") (v "0.54.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.54.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.54.0") (d #t) (k 0)))) (h "0x95bhfcj3f8dd3yhjf883qc6g161p82s1qfjg5j8sifydaxr2l1")))

(define-public crate-sway-ast-0.55.0 (c (n "sway-ast") (v "0.55.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.55.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.55.0") (d #t) (k 0)))) (h "1pli555g2n6yl2dwha35fml0lslvfh3mn6fr2wgqcjm1mlg1ayb6")))

(define-public crate-sway-ast-0.56.0 (c (n "sway-ast") (v "0.56.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.56.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.56.0") (d #t) (k 0)))) (h "1qq9wmkqzkifw8h0l5bdsv2ldhhm9nny0k10k0g2yzqnnm0mir4i")))

(define-public crate-sway-ast-0.56.1 (c (n "sway-ast") (v "0.56.1") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.56.1") (d #t) (k 0)) (d (n "sway-types") (r "^0.56.1") (d #t) (k 0)))) (h "1q0353b1b05dsn19kkm2f5jpmx011ldak5w05i9bh0w8caci6jg1")))

(define-public crate-sway-ast-0.57.0 (c (n "sway-ast") (v "0.57.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.57.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.57.0") (d #t) (k 0)))) (h "0jkx7f6y9bmzsmkw18y7xyj8h1mav2l571cvgvlypw61l3dyydky")))

(define-public crate-sway-ast-0.58.0 (c (n "sway-ast") (v "0.58.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.58.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.58.0") (d #t) (k 0)))) (h "1dqvnk9rdayz7ib64nbqm44cj10758jlbaycyqf07krqjb3pd4l4")))

(define-public crate-sway-ast-0.59.0 (c (n "sway-ast") (v "0.59.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.59.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.59.0") (d #t) (k 0)))) (h "19jfws47njxwhfaviy6w8fhxr83cvdi968akp99q4gy4bgwb7cx9")))

(define-public crate-sway-ast-0.60.0 (c (n "sway-ast") (v "0.60.0") (d (list (d (n "extension-trait") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-error") (r "^0.60.0") (d #t) (k 0)) (d (n "sway-types") (r "^0.60.0") (d #t) (k 0)))) (h "0gf1cvfx9x0a7r3xjhk24p6w2v5225mh7sq5w2xzwzbbsbxsspv5")))

