(define-module (crates-io sw ay swayipc-async) #:use-module (crates-io))

(define-public crate-swayipc-async-2.0.0-alpha (c (n "swayipc-async") (v "2.0.0-alpha") (d (list (d (n "async-io") (r "^1.1.10") (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "0v9h4sz88l8wd5z4ywi2kxlafhdgkpyqsa2qslwn89yq3ss5fma2")))

(define-public crate-swayipc-async-2.0.0-alpha.1 (c (n "swayipc-async") (v "2.0.0-alpha.1") (d (list (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "0y1w6ridi16z9s6lmls90jdb6yc9by2z4sinqj6dhx0afqr6wcrj")))

(define-public crate-swayipc-async-2.0.0-alpha.2 (c (n "swayipc-async") (v "2.0.0-alpha.2") (d (list (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0-alpha.2") (d #t) (k 0)))) (h "1r5gr691yicixjl78swzpd2cdpbcd97m9ad8arjsrcl94jhxm2an")))

(define-public crate-swayipc-async-2.0.0-alpha.3 (c (n "swayipc-async") (v "2.0.0-alpha.3") (d (list (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0-alpha.3") (d #t) (k 0)))) (h "0lkhxk1aaicvkk8hp4k1w1vkq35qmr46mmr1vrm2w9ml9pdp4vjs")))

(define-public crate-swayipc-async-2.0.0 (c (n "swayipc-async") (v "2.0.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0") (d #t) (k 0)))) (h "1pdwfihcq8pc9ydmbwgz6jn1ypd6j82lar12qrzyr5p71z6gja7q")))

(define-public crate-swayipc-async-2.0.1 (c (n "swayipc-async") (v "2.0.1") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "swayipc-types") (r "^1.0.0") (d #t) (k 0)))) (h "0x2gb68ah524sjfmgmmaph642n3vimvf5zqday3qarcydz6swbl1")))

(define-public crate-swayipc-async-2.0.2 (c (n "swayipc-async") (v "2.0.2") (d (list (d (n "async-io") (r "^2") (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "swayipc-types") (r "^1") (d #t) (k 0)))) (h "1yyv7jwsr2z5azjal5hj8hgxb06dqrnxsaxrnjfjnp1pmwvjch48")))

