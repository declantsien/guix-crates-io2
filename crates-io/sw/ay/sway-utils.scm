(define-module (crates-io sw ay sway-utils) #:use-module (crates-io))

(define-public crate-sway-utils-0.1.1 (c (n "sway-utils") (v "0.1.1") (h "02sx0h1q9mgf6girfqsw9w0s3khyygkf27yw0fgkjk0vv0frcbpl")))

(define-public crate-sway-utils-0.1.2 (c (n "sway-utils") (v "0.1.2") (h "0rkgahhsbfdh6kd62shlsbpwv1n8qlqa21f4w5v60ly47vhb8wgb")))

(define-public crate-sway-utils-0.1.3 (c (n "sway-utils") (v "0.1.3") (h "1kxfyb9ihpgmlpp8c773j8nlqwhwja30n1h4wnjv4bfrx8s01xzx")))

(define-public crate-sway-utils-0.1.4 (c (n "sway-utils") (v "0.1.4") (h "0pdgxdkqs7x232mam9h6xicdsv4d825ils7mjbpsj5rpsg2g87f6")))

(define-public crate-sway-utils-0.1.5 (c (n "sway-utils") (v "0.1.5") (h "0babaqps78qa2i747x92hqdmdb47aigwx4ifv9m516bdhpq78rmv")))

(define-public crate-sway-utils-0.1.6 (c (n "sway-utils") (v "0.1.6") (h "0sjq0nqd1rz46ac6nbby5fww1j38j7krzlr3w1vbzxpbdx70q820")))

(define-public crate-sway-utils-0.1.7 (c (n "sway-utils") (v "0.1.7") (h "042xbc7vzrry6p0v7c7ldk7p450732ka9dyi6m4dk0zxy9lq1wrs")))

(define-public crate-sway-utils-0.1.9 (c (n "sway-utils") (v "0.1.9") (h "08554ay5s9n10mlm0ly21qm598k3s1nj4d693y8ikfs8gmcibgrg")))

(define-public crate-sway-utils-0.2.1 (c (n "sway-utils") (v "0.2.1") (h "100b9z7qh3577nw1c5z8yrs6i6xd928v9jc0falsqcg53kbdl6lx")))

(define-public crate-sway-utils-0.3.0 (c (n "sway-utils") (v "0.3.0") (h "1dsngrr5ndhyq7lxr0msb8rwpys8gp7sb335p0w53p38lav9z207")))

(define-public crate-sway-utils-0.3.1 (c (n "sway-utils") (v "0.3.1") (h "0cxanbgzwjzb27hk9iszkn5944nqlqf28i6gjzsxm44m9qq09s0q")))

(define-public crate-sway-utils-0.3.3 (c (n "sway-utils") (v "0.3.3") (h "0xky06fipk0vf3sx271mjf82pfgvfhn5cbk78ls3n4p65xl6x8cv")))

(define-public crate-sway-utils-0.4.0 (c (n "sway-utils") (v "0.4.0") (h "05z6mqxjhxwg001dighwy2v28h33z3iyz0cfhk09mwjsm69hgxq4")))

(define-public crate-sway-utils-0.5.0 (c (n "sway-utils") (v "0.5.0") (h "01vbf3nisn80j3pn53mjjypizl3vdb7wsr45filgdfqqn73xphmz")))

(define-public crate-sway-utils-0.6.0 (c (n "sway-utils") (v "0.6.0") (h "00fkqyvgl4z6sr7g6gzdds7kvjb553q8qksg9zm79y4dcgnv7lpl")))

(define-public crate-sway-utils-0.6.1 (c (n "sway-utils") (v "0.6.1") (h "150lr4i1r18xmi7hhvfgzj85xvpi4iqr26hcpjyyw0qyjkq6db20")))

(define-public crate-sway-utils-0.7.0 (c (n "sway-utils") (v "0.7.0") (h "0nk8cdsbr4gjrb678ninixxzw9y4npv6kjh954570iyv6xkclivd")))

(define-public crate-sway-utils-0.8.0 (c (n "sway-utils") (v "0.8.0") (h "13w1mriq8i22j136kmr31cj56rcxyfpavv37xvvra5vgaczsf54j")))

(define-public crate-sway-utils-0.9.1 (c (n "sway-utils") (v "0.9.1") (h "1kybcy8y78fvp54j3fyy59xn40y3vd8326fsqfjx3rd7wfnmjmy8")))

(define-public crate-sway-utils-0.9.2 (c (n "sway-utils") (v "0.9.2") (h "12c1qk441whzlb2k0wrykc7xpchcsgirf9dy0wflal2fd2dfy0c5")))

(define-public crate-sway-utils-0.10.0 (c (n "sway-utils") (v "0.10.0") (h "0jykwdzbznp1pby9mdjyrkrlays1grn8j2pl764nvr7j4a2hcc28")))

(define-public crate-sway-utils-0.10.1 (c (n "sway-utils") (v "0.10.1") (h "0ryv82jg9gc2sp0yblz8c4kiwfslaak9r3f8gw23bcnh9bfkiibs")))

(define-public crate-sway-utils-0.10.2 (c (n "sway-utils") (v "0.10.2") (h "1cwv7wb5651k051x3j2k8r2g7wj51qvh88paj9hjkx5m03g2z3cp")))

(define-public crate-sway-utils-0.10.3 (c (n "sway-utils") (v "0.10.3") (h "0xi5v5fmbha009pxwgyxz0sqxdbf2jnr7v8gzhxn4mbvflimqpfg")))

(define-public crate-sway-utils-0.11.0 (c (n "sway-utils") (v "0.11.0") (h "1ralj9djakj0yhgqaqwm2ffwjwjp7xw45qsyy2sj1xdhbafdrvk2")))

(define-public crate-sway-utils-0.12.1 (c (n "sway-utils") (v "0.12.1") (h "172pn3a3kn3xqwd2ky2fglh89mz362n81dlb9gx0bgz921dv2cbq")))

(define-public crate-sway-utils-0.12.2 (c (n "sway-utils") (v "0.12.2") (h "1zwfs8dkx8qhf7sb61j9hrv0v04cjy1y7v3xsj8gbv0hfjdka1yg")))

(define-public crate-sway-utils-0.13.0 (c (n "sway-utils") (v "0.13.0") (h "096hj8grq4rkg1q1pi8gnjpfjhig4sz9l00q80ikp17lvhaz4yvh")))

(define-public crate-sway-utils-0.13.1 (c (n "sway-utils") (v "0.13.1") (h "0sghrv9wfb318bixlcrmm7n7sdynhb2z2jv3b38pq6pvwv9dzas1")))

(define-public crate-sway-utils-0.13.2 (c (n "sway-utils") (v "0.13.2") (h "0gmhnv7krax13qid8r7ai6b2mdmm2401ndma23pv5xpx6pa48akn")))

(define-public crate-sway-utils-0.14.0 (c (n "sway-utils") (v "0.14.0") (h "0b29hf55l85n42yvkf0qm8fba2bdwd67hcyv99x66gs2g1n1d1pb")))

(define-public crate-sway-utils-0.14.1 (c (n "sway-utils") (v "0.14.1") (h "0787bzyig9zprfck09n8xwxysjfwcckk4c1ljb7520l8m1dg765y")))

(define-public crate-sway-utils-0.14.2 (c (n "sway-utils") (v "0.14.2") (h "1b6iaszf1hbhl34j01n6wa7fhplzg80wlxwpm496bbd04csqmf04")))

(define-public crate-sway-utils-0.14.3 (c (n "sway-utils") (v "0.14.3") (h "09940hwga3c6gaadhydirkks5xas2wcb61713z6wcxykkidycnpp")))

(define-public crate-sway-utils-0.14.4 (c (n "sway-utils") (v "0.14.4") (h "14f281702ly6r6rcx0v2l0dp39c2hpw2swp4y7vzaika2qjfp0f3")))

(define-public crate-sway-utils-0.14.5 (c (n "sway-utils") (v "0.14.5") (h "1npad997w7s0x75hq6x8ndlwfwg38acb9mpq9975mj22jbpa58y2")))

(define-public crate-sway-utils-0.15.1 (c (n "sway-utils") (v "0.15.1") (h "1i48qy8f89w6czhpqnm10hv7is40hn906815vca043rsbnm6pxa6")))

(define-public crate-sway-utils-0.15.2 (c (n "sway-utils") (v "0.15.2") (h "18v46lmc8l37pisxx7fkknnaiamibgwbnpk65fq7xqwmkj14lm3c")))

(define-public crate-sway-utils-0.16.0 (c (n "sway-utils") (v "0.16.0") (h "1ygp1lgifxzfgflfj2s82v7lax2syvpqd9v415n38mn20s69c5hj")))

(define-public crate-sway-utils-0.16.1 (c (n "sway-utils") (v "0.16.1") (h "1r27643c6asrcn6663gsh36bydahfm0a4wb0vh9d4cabxlwda1xf")))

(define-public crate-sway-utils-0.16.2 (c (n "sway-utils") (v "0.16.2") (h "1n74s694j0dald0y8xxfkn9avr4kkb5zwp98qfgxi82q2hl9qnfm")))

(define-public crate-sway-utils-0.17.0 (c (n "sway-utils") (v "0.17.0") (h "00wff1cd36jh4rdl5f3cm5hyx62pfa5apcizm5sf7bghb6hp9x6i")))

(define-public crate-sway-utils-0.18.0 (c (n "sway-utils") (v "0.18.0") (h "0kvsqw95n55naf8y4xfhjzgb7fcpmi5cs2m7hyzmxxxivx2l22wc")))

(define-public crate-sway-utils-0.18.1 (c (n "sway-utils") (v "0.18.1") (h "0s01xrglv94vwwzzxd05qbrs66pjrh31pnm1056jr9p87df4nxgx")))

(define-public crate-sway-utils-0.19.0 (c (n "sway-utils") (v "0.19.0") (h "1c24l5pd1cfk08sdif3snxnrpm229lg1v2d84qvragn9vv7c0s84")))

(define-public crate-sway-utils-0.19.1 (c (n "sway-utils") (v "0.19.1") (h "0l5zp83dz0rvcfx4wcg43qwkqpmsnzh9b1cm5q7x5f0svc94d2xq")))

(define-public crate-sway-utils-0.19.2 (c (n "sway-utils") (v "0.19.2") (h "1a7a4xphdb5sacaq6g7sm6qg168n1d1m08h71jzfay0s3gdm5k5s")))

(define-public crate-sway-utils-0.20.0 (c (n "sway-utils") (v "0.20.0") (h "1v3m4jfz4cfxkfychqfbwacbj8q6c1j548xkk7zag6h4mha50an3")))

(define-public crate-sway-utils-0.20.1 (c (n "sway-utils") (v "0.20.1") (h "0yi441p3bgbax7a52b2hvj5gjfgmsl4bazamgzwkbx5zl8g69nri")))

(define-public crate-sway-utils-0.20.2 (c (n "sway-utils") (v "0.20.2") (h "1x566m6v1hildckcc7m9hyxsj21snb6jlpp59n0yqh5kkm8n8kzp")))

(define-public crate-sway-utils-0.21.0 (c (n "sway-utils") (v "0.21.0") (h "1mkz8bm3jc1w7i3793lcd88dslfyg7mglyms05i8n8j4bz2z3rqb")))

(define-public crate-sway-utils-0.22.0 (c (n "sway-utils") (v "0.22.0") (h "04aill67sdyzp574yhci1dg8m6n95k6ymvm72dblmz5qr7kq6ydp")))

(define-public crate-sway-utils-0.22.1 (c (n "sway-utils") (v "0.22.1") (h "0xikl3j4xxk5y3pwxggbj8q6abminlgr9bq16qmhwxp14mjhwcmq")))

(define-public crate-sway-utils-0.23.0 (c (n "sway-utils") (v "0.23.0") (h "1rjxdqnwdipamf6iv31ns0gsqh5qspr653snjxilry4c18bnv3dp")))

(define-public crate-sway-utils-0.24.0 (c (n "sway-utils") (v "0.24.0") (h "1nrfc366hk2xnhw6dpmrll1m1fqk1faf09s5b4fdbpsz7by8rl9h")))

(define-public crate-sway-utils-0.24.1 (c (n "sway-utils") (v "0.24.1") (h "058i9d2mq864dp8zn91mmrk980lzpz4pb9v1s9zz4yljf0bc3gsf")))

(define-public crate-sway-utils-0.24.2 (c (n "sway-utils") (v "0.24.2") (h "07ark0fxc4gkz6wrj3lr49aqj2nh54ay4r37qhd27jgg7dqx2igc")))

(define-public crate-sway-utils-0.24.3 (c (n "sway-utils") (v "0.24.3") (h "0xj2zy048nc4sh82livikf593v2g486hpqrwy1a20wns1z5bgsxa")))

(define-public crate-sway-utils-0.24.4 (c (n "sway-utils") (v "0.24.4") (h "1xvhjacrf0p6hvnisnandlbr57zmjdlyz42gwljs2zqgq3z7l3gd")))

(define-public crate-sway-utils-0.24.5 (c (n "sway-utils") (v "0.24.5") (h "1mm7ja3ihqznyzbpjbvg92pssfy2n4fhhi0897bgz6vpai4cvrhh")))

(define-public crate-sway-utils-0.25.0 (c (n "sway-utils") (v "0.25.0") (h "0nakbp5872ivadzirlwlai6pqxp4dv80116078rcssqcv65kacj9")))

(define-public crate-sway-utils-0.25.1 (c (n "sway-utils") (v "0.25.1") (h "1mkqgdy88qzqc4p172gkz1fb0g3p6xqw9gdnghajw46bxah469si")))

(define-public crate-sway-utils-0.25.2 (c (n "sway-utils") (v "0.25.2") (h "0jaq3dj89pn57m2ck9wvciqc2ba3dhxvyh4zigdyzf2kapa183sd")))

(define-public crate-sway-utils-0.26.0 (c (n "sway-utils") (v "0.26.0") (h "192gjrw3vq2rnlp0y2vf4vq9jm3z8skynr1j4h808vd46f6n2p6l")))

(define-public crate-sway-utils-0.27.0 (c (n "sway-utils") (v "0.27.0") (h "18w07kg9xk4j5ld217v9a55yjlqdrm1ng3n0br2r3ky51ndzx34h")))

(define-public crate-sway-utils-0.28.0 (c (n "sway-utils") (v "0.28.0") (h "1dlf76sr00g0aqh5sp8cpyx3mqgla3s3yfmh6fj4shqhq3f46i82")))

(define-public crate-sway-utils-0.28.1 (c (n "sway-utils") (v "0.28.1") (h "010x2qzfhgqzkwbf7r8qswf9ra8qqpcrpylfkwdd9vj3ahxsh4aq")))

(define-public crate-sway-utils-0.29.0 (c (n "sway-utils") (v "0.29.0") (h "0hjlj97mi41ggl47136lq2v8162r4rjmpy59vn4w87zjjbf3skw2")))

(define-public crate-sway-utils-0.30.0 (c (n "sway-utils") (v "0.30.0") (h "0wzvylsbi5cfp38zrpmpy12c5nw8sazwm9g0kf3gqhm4xga2ni8s")))

(define-public crate-sway-utils-0.30.1 (c (n "sway-utils") (v "0.30.1") (h "1bqm43rqyplb6ycvjpsj6q2qzqyr7q8i164pwg2szgn7v3b7fbgv")))

(define-public crate-sway-utils-0.31.0 (c (n "sway-utils") (v "0.31.0") (h "1vkk806fd4l3gfcn3mqis6n5cyqpbv5xzgbcfs7ddv93x6cm7jpw")))

(define-public crate-sway-utils-0.31.1 (c (n "sway-utils") (v "0.31.1") (h "1pg0mllsj601rhyw259rjr66r71fxl51r7vx0gqf4bzrn5119czy")))

(define-public crate-sway-utils-0.31.2 (c (n "sway-utils") (v "0.31.2") (h "1fyq20kgd1dngj910iqkydrw7fzsrf9169hyqndbv8z8qm5qcmhg")))

(define-public crate-sway-utils-0.31.3 (c (n "sway-utils") (v "0.31.3") (h "07kqv49bs525vnzq4qys02vb5qxrqcyzndmkcmms1bnv7krb7v5k")))

(define-public crate-sway-utils-0.32.0 (c (n "sway-utils") (v "0.32.0") (h "06491g2hssp8jpw4cvbnb00ga78dg9808dw6sdwpdnf8qcalgkxx")))

(define-public crate-sway-utils-0.32.1 (c (n "sway-utils") (v "0.32.1") (h "0j8alchbhsq41mhf8pwnpqb3yw8wfqwcdqir5sb1imi50jpn7py1")))

(define-public crate-sway-utils-0.32.2 (c (n "sway-utils") (v "0.32.2") (h "164xjbankniqycs4qdnq7b7a3ghjy6927qv55p406dpa738xk0x2")))

(define-public crate-sway-utils-0.33.0 (c (n "sway-utils") (v "0.33.0") (h "1r26bmp1yr67kls5scdr9b82c1g0lc1aqiadhds1wxv9yd67pd8y")))

(define-public crate-sway-utils-0.33.1 (c (n "sway-utils") (v "0.33.1") (h "0ypqjpl3cljw1hbvifa3vh1skchzzpfacrpf5wkqj5nmcr2vgadh")))

(define-public crate-sway-utils-0.34.0 (c (n "sway-utils") (v "0.34.0") (h "0c1gr2l4irn3wkmw803xz075ik5glizqh667561m7c16g85kdnda")))

(define-public crate-sway-utils-0.35.0 (c (n "sway-utils") (v "0.35.0") (h "0brdhbbmnmpfrq0jxa79ipyf42lp4nivwfpb6lanp546961zvib5")))

(define-public crate-sway-utils-0.35.1 (c (n "sway-utils") (v "0.35.1") (h "1p4n9v5smcpzssxfvj097la8y64v4sf93ih8hs391wjzkzvzknjc")))

(define-public crate-sway-utils-0.35.2 (c (n "sway-utils") (v "0.35.2") (h "0s6d75bmfyh4hba965akbyvpg31j36qd0svfn3ddbqbv4x1s8l3j")))

(define-public crate-sway-utils-0.35.3 (c (n "sway-utils") (v "0.35.3") (h "00imry8hnpz3mqrrhfb0w40mnznxk4ajwyblxwl20lddggx7mqva")))

(define-public crate-sway-utils-0.35.4 (c (n "sway-utils") (v "0.35.4") (h "002jjprz7xjg38p5xjna3f7vb12n1fffi5mkx9ginc6cxvi7xc65")))

(define-public crate-sway-utils-0.35.5 (c (n "sway-utils") (v "0.35.5") (h "0zsfisw0j708a6higacr3jwnzlvx4rkw5nlq8ak2x4srmm35n6zn")))

(define-public crate-sway-utils-0.36.0 (c (n "sway-utils") (v "0.36.0") (h "14hlnzyy1cm5cjk9m7wn2wmq71g2ndmd8h30px6b5f8mxqfrqy65")))

(define-public crate-sway-utils-0.36.1 (c (n "sway-utils") (v "0.36.1") (h "1kw5sgfd77l5gdrjavp3injw6wbwcwhdkya8jxn66wvxgh3b2av4")))

(define-public crate-sway-utils-0.37.0 (c (n "sway-utils") (v "0.37.0") (h "0c7wl70p0wlxkn9r2fmdj2cw191j2ycfvas14ba74dy2a927i3z4")))

(define-public crate-sway-utils-0.37.1 (c (n "sway-utils") (v "0.37.1") (h "0aj69by9b5ckfifja53idbv4n0v17mxwk8hpmlj298r8y6v0wzsr")))

(define-public crate-sway-utils-0.37.2 (c (n "sway-utils") (v "0.37.2") (h "03qa1g3351rxv0mby06fm1smyh9fh4b4qswdmm7g0pdpg93y3cl6")))

(define-public crate-sway-utils-0.37.3 (c (n "sway-utils") (v "0.37.3") (h "01ky1a3728j645dqyin65z3lqa903bkgkh3j2n1z07h3yjbd8wvx")))

(define-public crate-sway-utils-0.38.0 (c (n "sway-utils") (v "0.38.0") (h "1vki67i07kzrqh14fgx5xlhkjmayb2kh9dpbjmn6mzmr7c2li94y")))

(define-public crate-sway-utils-0.39.0 (c (n "sway-utils") (v "0.39.0") (h "0fw1dfl7flfan562zzjgq9bn15j8bjfa4170qwnw0z4vp87pzrn7")))

(define-public crate-sway-utils-0.39.1 (c (n "sway-utils") (v "0.39.1") (h "12b796dq8xxxidli8948spks0x9n3vgprw7am7vrx73x6nwjivn6")))

(define-public crate-sway-utils-0.40.0 (c (n "sway-utils") (v "0.40.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04h0di2n6s5ccnzpwija6ky2bhij34j9xbyp25xs37ja2ngd1qpc")))

(define-public crate-sway-utils-0.40.1 (c (n "sway-utils") (v "0.40.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f9q9ll1h6yjhzxcbic6jrxpfyabibnlizdjkvnsnna6cav9j3gd")))

(define-public crate-sway-utils-0.41.0 (c (n "sway-utils") (v "0.41.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k6c170as2zz0cavnwks5c7pglh8x4r6qxa95x27jif39nvsgyf6")))

(define-public crate-sway-utils-0.42.0 (c (n "sway-utils") (v "0.42.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d1bkcmfyysn1xqcm2m3whqp4h8lvabxknbbcryvdzywd61faazi")))

(define-public crate-sway-utils-0.42.1 (c (n "sway-utils") (v "0.42.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02sacr75vx22172z6ynzwj8h9lkxydr9mglbq4124xkzp2qzhbzs")))

(define-public crate-sway-utils-0.43.0 (c (n "sway-utils") (v "0.43.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2ly5arw3czbdvdssx94mcmp659dw51iybpj30l494wn9djds5g")))

(define-public crate-sway-utils-0.43.2 (c (n "sway-utils") (v "0.43.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s9h6d33fb49vwjhjf1f3imrgi9wjbjh2i8d5i8dfpyzhhxflv9z")))

(define-public crate-sway-utils-0.44.0 (c (n "sway-utils") (v "0.44.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f47avlyqmmvaabgbm22knwf1grhs35ncpca452j5fx5k27prhh9")))

(define-public crate-sway-utils-0.44.1 (c (n "sway-utils") (v "0.44.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dsqigrj9s3ar61v26wsbiaq3lqbs1f4jkg7g391mp0qsgayc6zx")))

(define-public crate-sway-utils-0.45.0 (c (n "sway-utils") (v "0.45.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q7vcbfqm2cq37hpp5b9zmsa32scwbvdryx4gwhzbrf8s8kixld9")))

(define-public crate-sway-utils-0.46.0 (c (n "sway-utils") (v "0.46.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rmjhhwll3s0wl4ddz040qddbvsh0yzppipx9fdccn0zhhxggg87")))

(define-public crate-sway-utils-0.46.1 (c (n "sway-utils") (v "0.46.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ridxzd8dlsldfkpxgh3nfrp5cqrrkfwdw3askx1gnq0cc031cns")))

(define-public crate-sway-utils-0.47.0 (c (n "sway-utils") (v "0.47.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1d1nq4639ck9ggqjvwnzl18pg3j1yggzni2jl2777c57afq418qf")))

(define-public crate-sway-utils-0.48.0 (c (n "sway-utils") (v "0.48.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0y0p3j4dvkpha4acmfc1lxhdw5sr73inw03fannq8mz0h0k04jid")))

(define-public crate-sway-utils-0.48.1 (c (n "sway-utils") (v "0.48.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0m92gxrrr2941dbsr8343c778wvjrj8p8gikahd677ig92pywnw6")))

(define-public crate-sway-utils-0.49.1 (c (n "sway-utils") (v "0.49.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0zfavhqzwj69rix767cnpxk0q4i3z0b5v3ldxw7zd18xvrrwwhzs")))

(define-public crate-sway-utils-0.49.2 (c (n "sway-utils") (v "0.49.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "17gha48am5hxg3kisjf9n0ncd2vpa2gn71jb53kkxkl4gqpkm7bw")))

(define-public crate-sway-utils-0.50.0 (c (n "sway-utils") (v "0.50.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0ppbhgvk97bz31zx3lvi93i38mc69f03s4k44j8q053g4ycyr16l")))

(define-public crate-sway-utils-0.51.0 (c (n "sway-utils") (v "0.51.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0lqi7gz8lp7hr1gnqw6xr5bn8hp04xmd19lq9smgvwks85f1mmd8")))

(define-public crate-sway-utils-0.51.1 (c (n "sway-utils") (v "0.51.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1p3arnqfm7f0bi956m6k6mz2x83j790fkhxjl9wh16bzk6wdq0bi")))

(define-public crate-sway-utils-0.49.3 (c (n "sway-utils") (v "0.49.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "04x9vvrlmngd7y2q7n3w4gl5y6z1kr1hn9gyd8ccbli15jz6sq7w")))

(define-public crate-sway-utils-0.52.0 (c (n "sway-utils") (v "0.52.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "053wqdf11dbhyrg9m6q6pjci3918js5qw2w7i4c3kqw9ygrr152c")))

(define-public crate-sway-utils-0.52.1 (c (n "sway-utils") (v "0.52.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "046bn3i0hj86f0m3kkcdcd6si54ym0lpx6m3wciv93q25pbb4n54")))

(define-public crate-sway-utils-0.53.0 (c (n "sway-utils") (v "0.53.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0nsij38jrjdx2cxkq0n2ipycch3w6ndfpnqzpbmpnjlrh0hn39g2")))

(define-public crate-sway-utils-0.54.0 (c (n "sway-utils") (v "0.54.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "15cr0k5zhlqznawhmcj2mqqcgi8rgm39dqnnqqqxglx1sbjhix98")))

(define-public crate-sway-utils-0.55.0 (c (n "sway-utils") (v "0.55.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1h66b41aaccd110057xfl5g6mqn08vqap8ljnl9fkg7nhqanjqph")))

(define-public crate-sway-utils-0.56.0 (c (n "sway-utils") (v "0.56.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "120737clis71g5m549rdp6zi0qngcl7cg1mn4gkx6nkcjllfd0v1")))

(define-public crate-sway-utils-0.56.1 (c (n "sway-utils") (v "0.56.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0p4cc2jdjw9kqfw99fh4sq0ckn5rdgsybby1y1s0rmh68ddrb901")))

(define-public crate-sway-utils-0.57.0 (c (n "sway-utils") (v "0.57.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "14a6qyqwah4nwr1wihl3wazypbxn21bf88wfnd4g418pa7f273gi")))

(define-public crate-sway-utils-0.58.0 (c (n "sway-utils") (v "0.58.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "10gkxpkxra0mz1vxlhc57h4vbg0zd37liwzx4hngpzdcj37wkqv9")))

(define-public crate-sway-utils-0.59.0 (c (n "sway-utils") (v "0.59.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1d3l2ak4qyhv5q8qa9wj0bdvyzy4fr8gv6r28ixp4rbjg7pfzakn")))

(define-public crate-sway-utils-0.60.0 (c (n "sway-utils") (v "0.60.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "13msl5xblif56s7z39nk5xr54hw8391hcza4i34ngqymgz9mkdhx")))

