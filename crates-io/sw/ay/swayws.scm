(define-module (crates-io sw ay swayws) #:use-module (crates-io))

(define-public crate-swayws-0.2.0 (c (n "swayws") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "1rgz5x3z08f2q3px5ymwb45ngn69306hn1icy877c3yw5752xanz")))

(define-public crate-swayws-0.2.1 (c (n "swayws") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "1km1p6bjwfz67s0cfxa5h98rgk2p6ypcqgdhgqhgd8njihka4dal")))

(define-public crate-swayws-0.2.2 (c (n "swayws") (v "0.2.2") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "0hk4p3sa9mja36lja61fa44ynixq19my54lpzd44limg4i8ck7yv")))

(define-public crate-swayws-0.2.3 (c (n "swayws") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "0q3q85h6y280l5499pc7wz7v5x7cyi4mjl3ji2z83nk6injyvsyg")))

(define-public crate-swayws-1.0.0 (c (n "swayws") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "09jkmciszkdkfs4lc7i66a8kjrgbbqf9xw3ibpafv6pvsi3i9y5b")))

(define-public crate-swayws-1.1.0 (c (n "swayws") (v "1.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1rz5s16b1x2mk37aqa49lj4mn7kfb9s1q60b8yvd4pbq3mdbl9x9")))

(define-public crate-swayws-1.1.1 (c (n "swayws") (v "1.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0jlahw0pc84m7xzm0yadg69061p57k8z0a4344nc1j0vkb6hpw23")))

(define-public crate-swayws-1.2.0 (c (n "swayws") (v "1.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0x640adcz1zp0sabbr33ks9mh5w1h5fzpajlpb4wv2yahprbzfiv")))

(define-public crate-swayws-1.3.0 (c (n "swayws") (v "1.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "1s4widxfwvry8jlw5vnminiha7cv5wwjq50hjb5fv8dgs7by0s89")))

