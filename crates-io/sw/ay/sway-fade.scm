(define-module (crates-io sw ay sway-fade) #:use-module (crates-io))

(define-public crate-sway-fade-0.1.0 (c (n "sway-fade") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "swayipc-async") (r "^2.0.0-alpha.3") (d #t) (k 0)))) (h "1fz29ncnlacnq2vwhvzgbbf1k5aa9vcjazwpsfg6jx92bmlaf7w3")))

