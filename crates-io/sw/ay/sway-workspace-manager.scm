(define-module (crates-io sw ay sway-workspace-manager) #:use-module (crates-io))

(define-public crate-sway-workspace-manager-0.1.0 (c (n "sway-workspace-manager") (v "0.1.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "14diams2026q79bx3dn3b945iwg0xak3mhpf7vhlb4w2avqsnjpb")))

(define-public crate-sway-workspace-manager-0.2.0 (c (n "sway-workspace-manager") (v "0.2.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "1ib46kvch1hla8zmb4bx10js4wxa85nf8yfrb90h5c2hb8965yxb")))

(define-public crate-sway-workspace-manager-1.0.0 (c (n "sway-workspace-manager") (v "1.0.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "07sz1w7ssdsj0kzlh1y1qw39vd117spgihqx3999cs4f8156kwfn")))

(define-public crate-sway-workspace-manager-2.0.0 (c (n "sway-workspace-manager") (v "2.0.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "03ljqjh426l7x3l16kzqjd9fbd5cyd1fihfszc3lbxcyqy19mjn2")))

(define-public crate-sway-workspace-manager-2.1.0 (c (n "sway-workspace-manager") (v "2.1.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "1w4bs01ra075xwjg693j9vb6fp6dn9hhqnrcdzvdi9yd5ib8qi7y")))

