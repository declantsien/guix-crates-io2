(define-module (crates-io sw ay sway-workspace) #:use-module (crates-io))

(define-public crate-sway-workspace-0.1.0 (c (n "sway-workspace") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "17d8dfp4cf8qk41vdjh9w331f3bg5lkkc01b32gy8by2mdv7fmya")))

(define-public crate-sway-workspace-0.2.0 (c (n "sway-workspace") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0x8lhyzw5gm8nzdazg0q063q9qa8fcfnnkcdhm8rqs78z3nlhc73")))

(define-public crate-sway-workspace-0.2.1 (c (n "sway-workspace") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1qa5r0kh7lgzq4g9rzfsljz2qw9f6s2cw6n1zyp66pavp9za7fx7")))

