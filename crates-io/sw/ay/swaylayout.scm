(define-module (crates-io sw ay swaylayout) #:use-module (crates-io))

(define-public crate-swaylayout-0.3.1 (c (n "swaylayout") (v "0.3.1") (d (list (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (f (quote ("paw"))) (d #t) (k 0)) (d (n "swayipc") (r "^2.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0c4xla8zkh0xc2rrwzw96d4rnpw4p06xji2j8bb6lbb4y2pf38zj")))

