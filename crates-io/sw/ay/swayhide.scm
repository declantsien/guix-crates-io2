(define-module (crates-io sw ay swayhide) #:use-module (crates-io))

(define-public crate-swayhide-0.1.0 (c (n "swayhide") (v "0.1.0") (d (list (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "0vvpa5v2jpgmfyr2y0plg8byhmpp5d4a0apnzfinpr7fp1ri7ksp")))

(define-public crate-swayhide-0.2.0 (c (n "swayhide") (v "0.2.0") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "0x172ffj0lfmbv5nix708l1mfsizxzy74gpxp5amvx0bbaq0p78s")))

(define-public crate-swayhide-0.2.1 (c (n "swayhide") (v "0.2.1") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.7") (d #t) (k 0)))) (h "0synzfd35494vlp2wnqmqbzgc0vg2ivn90hnxvk6qak0w65xhxcv")))

