(define-module (crates-io sw ay sway-alttab) #:use-module (crates-io))

(define-public crate-sway-alttab-1.0.0 (c (n "sway-alttab") (v "1.0.0") (d (list (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.2.3") (d #t) (k 0)))) (h "1c6lhlz9il727hfgx3kvfwfssbnw1239kcm4hc6zhzad3bxrwwij")))

(define-public crate-sway-alttab-1.0.1 (c (n "sway-alttab") (v "1.0.1") (d (list (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.2.3") (d #t) (k 0)))) (h "1x2m4mw8brqkvv4grglwidyh52b20dc211wid2sahcziyls3jfbi")))

(define-public crate-sway-alttab-1.1.0 (c (n "sway-alttab") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.2.3") (d #t) (k 0)))) (h "0g2lmmz2z90yhj5kk0wlqbaxx7q040125vlmslbva01cn4wfpfv5")))

(define-public crate-sway-alttab-1.1.1 (c (n "sway-alttab") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.2.3") (d #t) (k 0)))) (h "02y78v5q6f21x6ivdgajkx2infp76rk9ksbsk79awqdppxbg9yhn")))

(define-public crate-sway-alttab-1.1.2 (c (n "sway-alttab") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)) (d (n "swayipc") (r "^2.2.3") (d #t) (k 0)))) (h "1hq14rjdvik4n0yp76cnypk2a63wj0cks02k0ygmprxzw1gl3s02")))

