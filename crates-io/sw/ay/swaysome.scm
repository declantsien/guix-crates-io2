(define-module (crates-io sw ay swaysome) #:use-module (crates-io))

(define-public crate-swaysome-1.1.1 (c (n "swaysome") (v "1.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "015g7snm48z925zwa89g0qy479g974pkqvakc0lbsif4xqzkk557")))

(define-public crate-swaysome-1.1.2 (c (n "swaysome") (v "1.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zpxnf4dajxc893ls2xynfca7wm3qigciq4z9799ra5w3q155cpm")))

(define-public crate-swaysome-1.1.3 (c (n "swaysome") (v "1.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0183maa839y3v6a6s2x6i8b7k32fw9jhyni3l8w6vd7h0hi9gdln")))

(define-public crate-swaysome-1.1.4 (c (n "swaysome") (v "1.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cm7ckxgbvy3sikm6fp5hmihqsqnv8l2ifij0rdjgyb548vwqj67")))

(define-public crate-swaysome-1.1.5 (c (n "swaysome") (v "1.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1q6dmcyi90v3yk9yifmsqmphan25c9jxh5s7b6qrg5c1j0v78mgk")))

(define-public crate-swaysome-2.0.0 (c (n "swaysome") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1axh4qzv1mrj590a1jhvl1w9wlqs9imv7mgfqramaid12210l2jw")))

(define-public crate-swaysome-2.1.0 (c (n "swaysome") (v "2.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1dgaxxhi0vh3ab5vc3vh7p7f3rw68avl426dg41bw8q2d6rcm625")))

(define-public crate-swaysome-2.1.1 (c (n "swaysome") (v "2.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "02lvq4pr7dbslxvlhfha6rhbqfmj8a6kq0p41b25a56p0pwhd7qq")))

