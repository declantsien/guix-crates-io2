(define-module (crates-io sw ay sway-balance-workspace) #:use-module (crates-io))

(define-public crate-sway-balance-workspace-0.1.0 (c (n "sway-balance-workspace") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.3") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0410bagwf434z1s9zasmyh074wxk5m9ddfy8pa2h4c83k21n9lq4")))

(define-public crate-sway-balance-workspace-0.1.1 (c (n "sway-balance-workspace") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.3") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "06c12pqig56g3l7z1allc5yhg7j8dq641ypg6h4zw9inm8w2lvbn")))

