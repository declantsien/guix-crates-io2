(define-module (crates-io sw ay sway-scratchpad) #:use-module (crates-io))

(define-public crate-sway-scratchpad-0.1.0 (c (n "sway-scratchpad") (v "0.1.0") (d (list (d (n "async-process") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "18762chi72hx20rq6xg3rm562ilazy4frg41lj3v1801n3hjaala")))

(define-public crate-sway-scratchpad-0.1.2 (c (n "sway-scratchpad") (v "0.1.2") (d (list (d (n "async-process") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12gfvi5zc938glv9fkazqzzakfmijfv48zkc94znlvz3djrwyy5w")))

(define-public crate-sway-scratchpad-0.1.4 (c (n "sway-scratchpad") (v "0.1.4") (d (list (d (n "async-process") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "190kz64v90qdn2a979k7vivjl19j7jszn89s0a3nncm2pxgqg4p0")))

(define-public crate-sway-scratchpad-0.2.0 (c (n "sway-scratchpad") (v "0.2.0") (d (list (d (n "async-process") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ksway") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1wb44wijrhs0yxbn070p9qfhk0b4hv8cn2hgp6q81p21p2zwn7jl")))

