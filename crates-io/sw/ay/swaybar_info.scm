(define-module (crates-io sw ay swaybar_info) #:use-module (crates-io))

(define-public crate-swaybar_info-0.1.0 (c (n "swaybar_info") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mmwb9s5xjhw6lhy1jamir2p4fl2nhdxmziky5bz5rla9qaa13ji")))

(define-public crate-swaybar_info-0.1.1 (c (n "swaybar_info") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d24d80pnxwmmf4h4m6spd4bilzc2ysd8pgnn031ff2d5kxa8027")))

(define-public crate-swaybar_info-0.1.2 (c (n "swaybar_info") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "101ih69lv6rzg8r5k89l4c4k56fh5ivhyc4j22n3zl899d76dnxh")))

(define-public crate-swaybar_info-0.1.3 (c (n "swaybar_info") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n9cjwq0f3hbaln9qw5cl0gpvh6ggnpwxdqpn5gld3271x6a1imh")))

(define-public crate-swaybar_info-0.1.4 (c (n "swaybar_info") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kz7b66d7c3ybpinsg6qk2vwf48rpc3w96h32a6vdpf1sdqxh0j1")))

(define-public crate-swaybar_info-0.1.5 (c (n "swaybar_info") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19gyx9zr8y83lgm5wbc0vgm1qprwi7b030j517wvm7q5dy696bl3")))

(define-public crate-swaybar_info-0.1.6 (c (n "swaybar_info") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08nda4fr8b0r690djirnlylb18jrnfxkvvk6j63jcgk187sf7865")))

(define-public crate-swaybar_info-0.1.7 (c (n "swaybar_info") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0saa8byy9jjywr2nbbs9p9k0mzzvji2c3yq1ykzcj30sl9w21lgr")))

(define-public crate-swaybar_info-0.1.8 (c (n "swaybar_info") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m5pnwgfw81l2bmnrq7gzh558579md4xhkgv85iaizvrjbr3zd6j")))

(define-public crate-swaybar_info-0.1.9 (c (n "swaybar_info") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02adfl8w70r02ic10p1n78d5pgx7a2yhccsbjhcwdzqbdc1qh9nz")))

(define-public crate-swaybar_info-0.1.10 (c (n "swaybar_info") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fj11w2lw928rgwi98ibzi3hxaifhhh1g2jr876gs2b89bdf3xa9")))

(define-public crate-swaybar_info-0.1.11 (c (n "swaybar_info") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h7w0f9ap1l5kyhgdicynsdnf3hjfld16kihn3205wsh9n40c0ih")))

(define-public crate-swaybar_info-0.1.12 (c (n "swaybar_info") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rqnymphzf4iws6q9vl2fxm72ql1vski60ydv2awwd1f8wx3ywy0")))

(define-public crate-swaybar_info-0.1.13 (c (n "swaybar_info") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nhcjk98rjqq0y92aalgb70cf2k25yjm7670212pm14f0d9w1g9z")))

(define-public crate-swaybar_info-0.1.14 (c (n "swaybar_info") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sgslbi5zlyis52yc1rzyzakn5aq34iv962j767lmzbj5cwgi0p6")))

(define-public crate-swaybar_info-0.1.15 (c (n "swaybar_info") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dcjni9wdh06kmy95i71nqvmgf5rcn4zspkv1vrrncvyi4frsmcb")))

