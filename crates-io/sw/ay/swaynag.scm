(define-module (crates-io sw ay swaynag) #:use-module (crates-io))

(define-public crate-swaynag-0.1.0 (c (n "swaynag") (v "0.1.0") (d (list (d (n "async-io") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (o #t) (d #t) (k 0)))) (h "1ghzcv8gj6zdf3qk9090pr173pbnv040aramm0vdk7f01lrjzz86") (f (quote (("async" "async-io" "async-pidfd" "futures-lite"))))))

(define-public crate-swaynag-0.1.1 (c (n "swaynag") (v "0.1.1") (d (list (d (n "async-io") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (o #t) (d #t) (k 0)))) (h "0jv7nhfcfv7b10mfli44sx6v7fd9pzmgqvfm62sbz3zy4f1xk2il") (f (quote (("async" "async-io" "async-pidfd" "futures-lite"))))))

(define-public crate-swaynag-0.2.0 (c (n "swaynag") (v "0.2.0") (d (list (d (n "async-io") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "async-pidfd") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (o #t) (d #t) (k 0)))) (h "19rz7lggv95yjxb5gg6nzzyqlwy7fg5jiwv8lqs9baznc3lg7s34") (f (quote (("async" "async-io" "async-pidfd" "futures-lite"))))))

