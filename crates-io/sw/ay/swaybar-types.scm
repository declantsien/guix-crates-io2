(define-module (crates-io sw ay swaybar-types) #:use-module (crates-io))

(define-public crate-swaybar-types-1.0.0 (c (n "swaybar-types") (v "1.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hn66gmq5jyjfb41y63inzjvxd1zc1491anjpma4mpjwnq2dimm7")))

(define-public crate-swaybar-types-1.0.1 (c (n "swaybar-types") (v "1.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zzxrk8px1l50c2lr9b0li08rnj8hyciz2p4zbvd6xifn1bg7ihv")))

(define-public crate-swaybar-types-1.0.2 (c (n "swaybar-types") (v "1.0.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01b3fbv1jspj8vyz6fx3x4c85xcw5lyv7n40ca1k8xlan0kzajc5")))

(define-public crate-swaybar-types-1.0.3 (c (n "swaybar-types") (v "1.0.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "070nny4sa72p3fknrji6gvvyf93xi18q1avd87lm9c7r050g59yg")))

(define-public crate-swaybar-types-1.0.4 (c (n "swaybar-types") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wlvv1sq5818yv8qha3vkxs97y0jxq8m6s7hvx73gdl7jkf272nz")))

(define-public crate-swaybar-types-1.0.5 (c (n "swaybar-types") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15mb1mywfxaff42hyysziymkws97wslgdcsl2wqwgqiy3fgc1p92")))

(define-public crate-swaybar-types-2.0.0 (c (n "swaybar-types") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14awd0c38ayn6jc283hr1jv46x74319k471syawjmg13c027lz32")))

(define-public crate-swaybar-types-3.0.0 (c (n "swaybar-types") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hz1ialgs4l0ki5s9gqhsdlfy0v3fph7mkw2z1rdi29bjlsv9h52")))

