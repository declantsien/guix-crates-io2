(define-module (crates-io sw ay swaycons) #:use-module (crates-io))

(define-public crate-swaycons-0.1.0 (c (n "swaycons") (v "0.1.0") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "1pg1h11lpyc9kivdzvisz9ny3mnj6502pgaxj0g09wwclbb0q9x8")))

(define-public crate-swaycons-0.2.0 (c (n "swaycons") (v "0.2.0") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "0b4himbxgpwi4cqzc581n9d1ak6hp42c7kwrimiqzbvq94s0j2i9")))

(define-public crate-swaycons-0.3.0 (c (n "swaycons") (v "0.3.0") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "0xzpx57g216bji9hgm083d6xx5gj6500gkl7nkqlkmmjdhj5l3ii")))

(define-public crate-swaycons-0.3.1 (c (n "swaycons") (v "0.3.1") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "11vzqc3ns8gizcqfkmib9bcq5pfcg72w9m86v418xriicw2m1mmv")))

