(define-module (crates-io sw ay sway_mm) #:use-module (crates-io))

(define-public crate-sway_mm-0.1.0 (c (n "sway_mm") (v "0.1.0") (d (list (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "1b9986sdy3zjrwyc2sanvihzjli123q6ma1apkwm7c6l2v61067j")))

