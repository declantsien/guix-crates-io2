(define-module (crates-io sw ay sway-video-idle-inhibit) #:use-module (crates-io))

(define-public crate-sway-video-idle-inhibit-0.1.3 (c (n "sway-video-idle-inhibit") (v "0.1.3") (d (list (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.5") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.30.0-beta.5") (f (quote ("client" "unstable"))) (d #t) (k 0)))) (h "1id57ffs3qhmrhsvmvfl232jqdbdz9y4cwgjnshwyfxlvlajv109")))

