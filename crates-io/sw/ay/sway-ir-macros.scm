(define-module (crates-io sw ay sway-ir-macros) #:use-module (crates-io))

(define-public crate-sway-ir-macros-0.24.4 (c (n "sway-ir-macros") (v "0.24.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1r6q3g51k2xlzc0x8fg70r1avl0nqii6s46mlmf6j7gl1jsf7jqx")))

(define-public crate-sway-ir-macros-0.24.5 (c (n "sway-ir-macros") (v "0.24.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "09g5npqp7sw7npnihwq6qik3dqk5pb7q77qaskyqvfzw58mq36fb")))

(define-public crate-sway-ir-macros-0.25.0 (c (n "sway-ir-macros") (v "0.25.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "06adarj856n4m9mdqsmf1wq8zjypas8jqbvgm2nhgs3zqb7jpd5d")))

(define-public crate-sway-ir-macros-0.25.1 (c (n "sway-ir-macros") (v "0.25.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1vji6kn3895i1r1j5lgk7v26fy059ws0x9zskbnbsrx7sky5s35l")))

(define-public crate-sway-ir-macros-0.25.2 (c (n "sway-ir-macros") (v "0.25.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0vwbvwxa4gqmqfvr800w21igb64dlin9ixymc5y5fppg065dv4gf")))

(define-public crate-sway-ir-macros-0.26.0 (c (n "sway-ir-macros") (v "0.26.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0rbgz4rl6lgs6nndn6i6gr88np7arp65s857ml0c5m97x27jd562")))

(define-public crate-sway-ir-macros-0.27.0 (c (n "sway-ir-macros") (v "0.27.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0jxhx0f79d6wxjhxjyp3zcggxw4rk0v7z7fs2i9c5krc9dkrd4k9")))

(define-public crate-sway-ir-macros-0.28.0 (c (n "sway-ir-macros") (v "0.28.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0nbs26z6bvkshyg8qgml8brshg2ixnm0bqqsfkxr54vqin4cffaw")))

(define-public crate-sway-ir-macros-0.28.1 (c (n "sway-ir-macros") (v "0.28.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1nf0v803mgfx0h97dghkw1va1nwg4rl5wwbw13nfab4br6kc6gs3")))

(define-public crate-sway-ir-macros-0.29.0 (c (n "sway-ir-macros") (v "0.29.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0mrc15ny1zigc973d4bjmm4zkx2njys5wfn85wayj13pxg38ljf2")))

(define-public crate-sway-ir-macros-0.30.0 (c (n "sway-ir-macros") (v "0.30.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0gzkhycfv9w0b42nxc5xan1bs0v97b3bc0lkykg6hn54p138vcfg")))

(define-public crate-sway-ir-macros-0.30.1 (c (n "sway-ir-macros") (v "0.30.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "07qvrwypnkaghv9xya91ny4amz5wrrhw51gy2ld763vvvljapnga")))

(define-public crate-sway-ir-macros-0.31.0 (c (n "sway-ir-macros") (v "0.31.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1p4v4nknf11g4nqxq6snyirsjisbhfzqp80mykv6ljcbxmh25kc8")))

(define-public crate-sway-ir-macros-0.31.1 (c (n "sway-ir-macros") (v "0.31.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0ps9ijyyy43yw9lqc1vvldkqflq57047ry0cnw69ad583i2y4398")))

(define-public crate-sway-ir-macros-0.31.2 (c (n "sway-ir-macros") (v "0.31.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1hc4c09fzbmfh7acr0nszli9x9c4rz2fy1p4b3k07ksmijg7wy2y")))

(define-public crate-sway-ir-macros-0.31.3 (c (n "sway-ir-macros") (v "0.31.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "17ks8hq5vy47661z6x8p4kpqny0n6p7d4jm2blzp3hag6a44g7gf")))

(define-public crate-sway-ir-macros-0.32.0 (c (n "sway-ir-macros") (v "0.32.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "05a0sisij8jqfzn6zgqqlklxrbx2g2ijkjj24n62wvmvqcj82q18")))

(define-public crate-sway-ir-macros-0.32.1 (c (n "sway-ir-macros") (v "0.32.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "02i3k1yqlalzkpqrfibdigwig160msvb0ygi8m72y1km3qrvn0fi")))

(define-public crate-sway-ir-macros-0.32.2 (c (n "sway-ir-macros") (v "0.32.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "03v47sx62ds4bhgvkqhp9nshz76s0ary4w6j0dyzd4ah5gk2bqgf")))

(define-public crate-sway-ir-macros-0.33.0 (c (n "sway-ir-macros") (v "0.33.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0mqqclgwxpsm2fdsbkdkrs6rracn8pvzn3qcsd2vifk838y2qyza")))

(define-public crate-sway-ir-macros-0.33.1 (c (n "sway-ir-macros") (v "0.33.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "184zh822cfijbj25fsxl4xqbgr3gg3b6bas7vpm26y44pk1xvvbp")))

(define-public crate-sway-ir-macros-0.34.0 (c (n "sway-ir-macros") (v "0.34.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "14psqc2f7vp8xpznrzjask5kz4f1qff7a88r7fwpy01q47zzbip8")))

(define-public crate-sway-ir-macros-0.35.0 (c (n "sway-ir-macros") (v "0.35.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0jbfrya5bqaqrk6kn5rzhawzfka6g3s6vgp9vamia14cis2w0r1m")))

(define-public crate-sway-ir-macros-0.35.1 (c (n "sway-ir-macros") (v "0.35.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "05x2f82nvjih3dibcg37lqc6ryc5fk73kcgkiqvzcafh9nyj51l6")))

(define-public crate-sway-ir-macros-0.35.2 (c (n "sway-ir-macros") (v "0.35.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0g027hd4zv1sz805c33s8f80sacg08pamsfjmdfn66qz5nvqplqy")))

(define-public crate-sway-ir-macros-0.35.3 (c (n "sway-ir-macros") (v "0.35.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0xfqmjj935mv9x9hrj0qymfb8xj257rblkp215kjisgqxrmk2njy")))

(define-public crate-sway-ir-macros-0.35.4 (c (n "sway-ir-macros") (v "0.35.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1qndgq16s9bwifqbqs9ydwrmfc5hzqyy5f5b844pm4napsihxz4d")))

(define-public crate-sway-ir-macros-0.35.5 (c (n "sway-ir-macros") (v "0.35.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "015fx87aqxnqrds1qpi1shr31i20ndsg8vg7vvvclxbymm6l3f01")))

(define-public crate-sway-ir-macros-0.36.0 (c (n "sway-ir-macros") (v "0.36.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1aipzjhr075434r07k2lkarwrhg1m2k7ywy8lwj2ccv1fgbdhy3x")))

(define-public crate-sway-ir-macros-0.36.1 (c (n "sway-ir-macros") (v "0.36.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "00pmayk4dsnib9qdkzy9014614kwawvxa49acl8pybbrprzcww6a")))

(define-public crate-sway-ir-macros-0.37.0 (c (n "sway-ir-macros") (v "0.37.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "06qqs15qz25cpgcq2khgd59p58izkms00m2fz72dzh43lyvaxy77")))

(define-public crate-sway-ir-macros-0.37.1 (c (n "sway-ir-macros") (v "0.37.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1jqah8jb38kfm3f6qp7wk3y12vwsrs992rsa9798k22prnscjljy")))

(define-public crate-sway-ir-macros-0.37.2 (c (n "sway-ir-macros") (v "0.37.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "04s19kr9li05w1k531f4nh0l4xfl7mraxfdh2kp2ylsyjnly6p92")))

(define-public crate-sway-ir-macros-0.37.3 (c (n "sway-ir-macros") (v "0.37.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0p9r2m5njy54jkap95c4c7y8mvl6lnvhxnq5dl7a8cfhrljsbvqs")))

(define-public crate-sway-ir-macros-0.38.0 (c (n "sway-ir-macros") (v "0.38.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1091pgiz5gp5cdqh7n1wqq2jl9kk989cqpa3jkdilvhgk59fc5yr")))

(define-public crate-sway-ir-macros-0.39.0 (c (n "sway-ir-macros") (v "0.39.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "19nvfbh9jhws0fd2qk0h0xdyaw3iiq3s8fgmb2jay8psk8sxphh7")))

(define-public crate-sway-ir-macros-0.39.1 (c (n "sway-ir-macros") (v "0.39.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0gp9klmpkzmf5bkpwm849qs6mzmdgrfjryi8s60kj5cihpxnhg10")))

(define-public crate-sway-ir-macros-0.40.0 (c (n "sway-ir-macros") (v "0.40.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0cgviyhfzv746sfdy1q3a8carpq92bhl5az68cflbr3i9lx8knyz")))

(define-public crate-sway-ir-macros-0.40.1 (c (n "sway-ir-macros") (v "0.40.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "07d957r5i0bqjan9sraz1jr0rgnjjq94cbci7vprdm2ykf8n906x")))

(define-public crate-sway-ir-macros-0.41.0 (c (n "sway-ir-macros") (v "0.41.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "17hsgars9gdwl6x1a8d7w0dbad2dpfn70jn9kamw7ms0nlhpcica")))

(define-public crate-sway-ir-macros-0.42.0 (c (n "sway-ir-macros") (v "0.42.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1m909nb4y8ycg033iszfk7ajwac1ixqnain70z519986lp395cqg")))

(define-public crate-sway-ir-macros-0.42.1 (c (n "sway-ir-macros") (v "0.42.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1ajwgcd9npw7ykzwjn0063g55898jg193xqfdbqqyvl0z0v4m8fa")))

(define-public crate-sway-ir-macros-0.43.0 (c (n "sway-ir-macros") (v "0.43.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1hzpxxm54pghraxbdaw6zigqffasi01qlv0jrpya94pr8crsdg6b")))

(define-public crate-sway-ir-macros-0.43.2 (c (n "sway-ir-macros") (v "0.43.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "17fa0wx0vhprly49qs9xq3lxadq01n9dk807x545mfnxmagkqmbv")))

(define-public crate-sway-ir-macros-0.44.0 (c (n "sway-ir-macros") (v "0.44.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1sdijyirj0fc2h8n54qg2czs5dbngic0m6j1dimrzy5qv3l8jglf")))

(define-public crate-sway-ir-macros-0.44.1 (c (n "sway-ir-macros") (v "0.44.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1bwwvhacdg045cx5phcw394aa8j7ybjx4lxwn5hg47x07lsvh0ai")))

(define-public crate-sway-ir-macros-0.45.0 (c (n "sway-ir-macros") (v "0.45.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "15zrp6k3c91y0ff1853i21jkchdp5mv82yqsqyl7bbi9zsnj82ps")))

(define-public crate-sway-ir-macros-0.46.0 (c (n "sway-ir-macros") (v "0.46.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "022ciwiyjpzqgajz5av920mj7zhfm28vpybnk38b1vk1piq54j8r")))

(define-public crate-sway-ir-macros-0.46.1 (c (n "sway-ir-macros") (v "0.46.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "02mgld4xl909wqi8qc3vd0i1lf7s4ijn91ddvw20ybx98x7gwbbp")))

(define-public crate-sway-ir-macros-0.47.0 (c (n "sway-ir-macros") (v "0.47.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1ayz6s448dw558zaaxvd00vkghaj9vaj33mp8rhwxl6jaix9schz")))

(define-public crate-sway-ir-macros-0.48.0 (c (n "sway-ir-macros") (v "0.48.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "102h4ay414s1qq9lwfk5mqmp0108frsyi0nbs736zcd3hbk8ndi7")))

(define-public crate-sway-ir-macros-0.48.1 (c (n "sway-ir-macros") (v "0.48.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "03f46r8b0abjv7s9l3s3jliz3rw5lfvb4bj22f1sr9ami1qm59v4")))

(define-public crate-sway-ir-macros-0.49.1 (c (n "sway-ir-macros") (v "0.49.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0ipnj89hp0kn6g78rqvwn33srmydcdrg78c87w2j8vvcx99ir0n1")))

(define-public crate-sway-ir-macros-0.49.2 (c (n "sway-ir-macros") (v "0.49.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "19s36i0cq7bawgvqsm38gbhw04qvvssdvhcgy7l6pydbbm8h21qp")))

(define-public crate-sway-ir-macros-0.50.0 (c (n "sway-ir-macros") (v "0.50.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0z5wmi75rzjn6m45a0c42r9ki36zh8xg3igwpfp9wmkv7phj68hf")))

(define-public crate-sway-ir-macros-0.51.0 (c (n "sway-ir-macros") (v "0.51.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1qc41j9fbmlf4ig83spm3jqzgzbdfmmszkm0lww6h6iv7220hs8r")))

(define-public crate-sway-ir-macros-0.51.1 (c (n "sway-ir-macros") (v "0.51.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0bjs9nkppf3d9dp7q5nbpvi5b50v26rlrch7aw6fx1x06s6mbi0g")))

(define-public crate-sway-ir-macros-0.49.3 (c (n "sway-ir-macros") (v "0.49.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0ksip5zy749gin6fxmx6pjdy0vvazsis1qb99d8yh53ih69wbbyy")))

(define-public crate-sway-ir-macros-0.52.0 (c (n "sway-ir-macros") (v "0.52.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "09iparx7wp3sa96d98zyi4052qapmvy9fksb2ckacmlqq521jp2l")))

(define-public crate-sway-ir-macros-0.52.1 (c (n "sway-ir-macros") (v "0.52.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1z6q56zfvwykjxrvyxyidxw4nyqvgjcp369x3cp54116sxx6a73c")))

(define-public crate-sway-ir-macros-0.53.0 (c (n "sway-ir-macros") (v "0.53.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "10nhasx3n0xfxpaydqbk53szkmdcw6xvsmhlpzymna8fiwpdv46z")))

(define-public crate-sway-ir-macros-0.54.0 (c (n "sway-ir-macros") (v "0.54.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0pgcgjg8cwf9ry5shq63g9335kdayg8pjc529pnnfdin7safakr3")))

(define-public crate-sway-ir-macros-0.55.0 (c (n "sway-ir-macros") (v "0.55.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1g8ykxj1zv15rqsxnhkwzh527mk12a7s55hkfynw98f7l9y5cix3")))

(define-public crate-sway-ir-macros-0.56.0 (c (n "sway-ir-macros") (v "0.56.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "11hf6jbivgghf6dkba61fb2zg11bahiv2hxvd4ncsh4vazj45yr1")))

(define-public crate-sway-ir-macros-0.56.1 (c (n "sway-ir-macros") (v "0.56.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "11nzkx58v24yhncd2mvy27cqk80lc1zznw25da25lc3c98ji3vd6")))

(define-public crate-sway-ir-macros-0.57.0 (c (n "sway-ir-macros") (v "0.57.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "11bn39n8996zrbcgzyplx44ym0l6035p3gf71r2p0kl66xd7a130")))

(define-public crate-sway-ir-macros-0.58.0 (c (n "sway-ir-macros") (v "0.58.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "102qc99n4dxpy5ylqc9f134lgdg0n4akj1zmw9n97b7c9vhhc3r8")))

(define-public crate-sway-ir-macros-0.59.0 (c (n "sway-ir-macros") (v "0.59.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0zk2spn60ncxaxbqigrf82hg6gqqx2qxwr7w4iykq6qz7y2f78vq")))

(define-public crate-sway-ir-macros-0.60.0 (c (n "sway-ir-macros") (v "0.60.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "01316qdckwr8z0db5l0jqivibz4j86bncqybarsn7mk7hsnh82hn")))

