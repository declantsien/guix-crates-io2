(define-module (crates-io sw -c sw-composite) #:use-module (crates-io))

(define-public crate-sw-composite-0.1.0 (c (n "sw-composite") (v "0.1.0") (h "134fgh7vz88xmxns40k5ymcwicfsiib13s2c0r3ihxdb4pb59lxg")))

(define-public crate-sw-composite-0.1.1 (c (n "sw-composite") (v "0.1.1") (h "1yzz2fngbz0arkhrl0w3sqq9sxydpp1irraw89sjn9fyqzhwn7pb")))

(define-public crate-sw-composite-0.1.2 (c (n "sw-composite") (v "0.1.2") (h "0rc6g72mzvxlm2zadi0ivp6aaa4vih34r5scxlh9l9bibvd2lmfi")))

(define-public crate-sw-composite-0.1.3 (c (n "sw-composite") (v "0.1.3") (h "0v2amrvai330ch9az7agm2ihmmwvhih2y9mhhv79j0kcgnssxaw4")))

(define-public crate-sw-composite-0.1.4 (c (n "sw-composite") (v "0.1.4") (h "1kx9yi1bs4a81ajyhr5fq5lh2glimkzrjw8phq9snkfs7pjm2qrw")))

(define-public crate-sw-composite-0.2.0 (c (n "sw-composite") (v "0.2.0") (h "0rr3r4kx0z8sw9wj4g4gbjii21jl1ff1nav6jkymyg2gszp0ljy7")))

(define-public crate-sw-composite-0.2.1 (c (n "sw-composite") (v "0.2.1") (h "1i8prb66k8c5gbacyxf93zz61bg38vhhh3x747250bmjl56mjki4")))

(define-public crate-sw-composite-0.3.0 (c (n "sw-composite") (v "0.3.0") (h "04bimg4gsc6nwh1288jsf5msmbsnh81466yrigqribf17n8r3vys")))

(define-public crate-sw-composite-0.4.0 (c (n "sw-composite") (v "0.4.0") (h "1v0j9kq5idg4mqdwjmpxykx8b25zjsk8r6gl9rh3dyh4p96g0rj4")))

(define-public crate-sw-composite-0.4.1 (c (n "sw-composite") (v "0.4.1") (h "02bj2xkv3b33qqmljblq5202fg44dnns1b6j99qdwla5cpadp5rd")))

(define-public crate-sw-composite-0.4.2 (c (n "sw-composite") (v "0.4.2") (h "0xa60a974qwa4m1f2jqdakysyj22nh5mj2r096107svpl0v28wz8")))

(define-public crate-sw-composite-0.4.3 (c (n "sw-composite") (v "0.4.3") (h "0l4l2xk7p3hzwxk8ggbadv23kn7sd4cypwli5xzrgh0z395mnbi0")))

(define-public crate-sw-composite-0.5.0 (c (n "sw-composite") (v "0.5.0") (h "122j4ky9pibjfqbxp8xijfhmnrm53nwfzd4kyrnwd6p3gjx5gs65")))

(define-public crate-sw-composite-0.5.1 (c (n "sw-composite") (v "0.5.1") (h "0fqbayxr6lpqna1q1v904b2fdpwzqsg8y747yd24v7c8877ps2pj")))

(define-public crate-sw-composite-0.5.2 (c (n "sw-composite") (v "0.5.2") (h "04cghhwlki88fxk7i8knhqw11dbpr95sca3ljy1zr5afrxrnn11r")))

(define-public crate-sw-composite-0.5.3 (c (n "sw-composite") (v "0.5.3") (h "14wh4fpaxp052zdph26l71d8yb9q8vqbln4y5v9cm54m1lh8yqx1")))

(define-public crate-sw-composite-0.5.4 (c (n "sw-composite") (v "0.5.4") (h "1g3ihcpgq8bhizgynbf5xsyhc59m84qzjnrlzmnahsy0jflrg3gz")))

(define-public crate-sw-composite-0.5.5 (c (n "sw-composite") (v "0.5.5") (h "0nqbkc0434pggq7nxzbnqjh236z4yq6l8kgaac6d7957mfi48zir")))

(define-public crate-sw-composite-0.5.6 (c (n "sw-composite") (v "0.5.6") (h "07x98fgqrxz1n7lhfrd6r1dvyswjk1iirxdmvfh42xfkq51bkxfn")))

(define-public crate-sw-composite-0.5.7 (c (n "sw-composite") (v "0.5.7") (h "1isjy19591r9jzw66dfq94f7b38ssq6j9sv5vdix426z1a9fsfah")))

(define-public crate-sw-composite-0.5.8 (c (n "sw-composite") (v "0.5.8") (h "1wdk332k748j8z65pg0vg8w1ylvjfk9jpdkrhxqg16w30rvl4zmn")))

(define-public crate-sw-composite-0.5.9 (c (n "sw-composite") (v "0.5.9") (h "0q6ywfsr3cc21q222fqrddna4w9ilzdryz51i6gkr3m1v3gzb2bz")))

(define-public crate-sw-composite-0.5.10 (c (n "sw-composite") (v "0.5.10") (h "1cmrizpmxi91g3ys53jmdqz5qd2k0j5s5xviy0b65a2d15aigfjy")))

(define-public crate-sw-composite-0.6.0 (c (n "sw-composite") (v "0.6.0") (h "1r243x28qjr6d0zg2864wla2qc49z0mjdcjaqlscdpd4z2j5rvxv")))

(define-public crate-sw-composite-0.7.0 (c (n "sw-composite") (v "0.7.0") (h "16sfys00k37pcdm1h4icwwivs1i5bdy7czz3zgjvgnrbfzbp3gbi")))

(define-public crate-sw-composite-0.7.1 (c (n "sw-composite") (v "0.7.1") (h "1mgpk2cf5r081p5yalbihzkzx7dkibwlbjvmi9bv05fys098a9r6")))

(define-public crate-sw-composite-0.7.2 (c (n "sw-composite") (v "0.7.2") (h "1lpif6ybdvxrvvdd7r4xmcb0h8dj353wky3da3n7zrf770bnz8sh")))

(define-public crate-sw-composite-0.7.3 (c (n "sw-composite") (v "0.7.3") (h "1x8pm6f66dw13r2garhzmad0p9qrqh1iz6ifynwd23dh48d1q79v")))

(define-public crate-sw-composite-0.7.4 (c (n "sw-composite") (v "0.7.4") (h "04y5bjiisf13znx2ykp2rrpsv4w42igrak0w1clzmw49i7h4pby1")))

(define-public crate-sw-composite-0.7.5 (c (n "sw-composite") (v "0.7.5") (h "1564a8mgxiiamhykvvyfqr4bfpbpys2crpfjr1r8dihd3brkwfqf")))

(define-public crate-sw-composite-0.7.6 (c (n "sw-composite") (v "0.7.6") (h "154vnfd1d252rah7d2vz58ipw1c4dr823yddhiv01i6diaw015a8")))

(define-public crate-sw-composite-0.7.7 (c (n "sw-composite") (v "0.7.7") (h "0wv9z5ib2dxc1va7023lr57kvkx8j4kaasz2x4bh8z6caiifl57b")))

(define-public crate-sw-composite-0.7.8 (c (n "sw-composite") (v "0.7.8") (h "1141w9w6g0ci2hi54zipvlvh9wmwbwg8jg36d76995iwig24hvsd")))

(define-public crate-sw-composite-0.7.9 (c (n "sw-composite") (v "0.7.9") (h "1fkxwhbvwrr9yr5m6xm605v2csnr71lnzk3glwk61lnba5szcz8s")))

(define-public crate-sw-composite-0.7.10 (c (n "sw-composite") (v "0.7.10") (h "1zrdvr6zfw4sm3ybr136bjdgnlgw1ykl9w24mbk59slcch64wh8z")))

(define-public crate-sw-composite-0.7.11 (c (n "sw-composite") (v "0.7.11") (h "00dgxgi03vl96z5hq9n7fr2zvjkwsbqsf57qns1hjvasmlxzniz6") (y #t)))

(define-public crate-sw-composite-0.7.12 (c (n "sw-composite") (v "0.7.12") (h "0kdc4bx2vfl7i4n8pxggnbqwskmq8293d82bcawvizhnqfisnhm0")))

(define-public crate-sw-composite-0.7.13 (c (n "sw-composite") (v "0.7.13") (h "0iys92m4p5qpzk5w53kxsipgs0k0vkib5xw68j5fmk45cyvdbw4z")))

(define-public crate-sw-composite-0.7.14 (c (n "sw-composite") (v "0.7.14") (h "12w06zv25lcd4k1f69sbicnx6yv3lrr2f53592zyhj0pgc2795p3")))

(define-public crate-sw-composite-0.7.15 (c (n "sw-composite") (v "0.7.15") (h "07wb17xy665hhf0kc99h80f4k52chy7fffir5kvf6h45scflz38b")))

(define-public crate-sw-composite-0.7.16 (c (n "sw-composite") (v "0.7.16") (h "1m3i4vdmixzcsnbffiws8jimsxdq1n3346kkmmha1bxljmwgpj4s")))

