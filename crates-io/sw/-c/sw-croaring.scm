(define-module (crates-io sw -c sw-croaring) #:use-module (crates-io))

(define-public crate-sw-croaring-0.3.11 (c (n "sw-croaring") (v "0.3.11") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)) (d (n "stack-croaring-sys") (r "^0.3.10") (d #t) (k 0)))) (h "03s62c8ksah0nkmqz6294nr4m6rlqrb5fzvvrn50lbmgc6pr2477")))

