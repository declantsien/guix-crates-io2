(define-module (crates-io sw ct swctx) #:use-module (crates-io))

(define-public crate-swctx-0.1.0 (c (n "swctx") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0g69lm1d1cmj4lxpyiqqr8cj6s69b7wjh9dlmg189k46sdm5ls8h") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-swctx-0.1.1 (c (n "swctx") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1cgaa5wvi9pb9kabnj1b0mlrxpqc6lfc9h35qxmyblxc4sj77xp8") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-swctx-0.2.0 (c (n "swctx") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1jrgm5icj1xm9zbqwkpsdvqr3ciqjjayr9y87665252lh7wsmv1w") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-swctx-0.2.1 (c (n "swctx") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "14qh07s3xkraas943f3bzzwrd8ga0j5x1j5hlc39n9s1iyja7vvs") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-swctx-0.2.2 (c (n "swctx") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1mdwbn3rkz4qsdvdjn566dp0n73kay6x6xmr3mrs98xrzvmayzb4") (r "1.56")))

