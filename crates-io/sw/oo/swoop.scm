(define-module (crates-io sw oo swoop) #:use-module (crates-io))

(define-public crate-swoop-0.1.0 (c (n "swoop") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "19d6j7j9qcw6d9z278vllcwqhvyry893iravndjlwbrgl8gnfbr6")))

