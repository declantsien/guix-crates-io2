(define-module (crates-io sw oo swoosh) #:use-module (crates-io))

(define-public crate-swoosh-0.1.0 (c (n "swoosh") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vjlv6pcndgdh1axknhdd9k4rhxdns7h7swybbjg76k8r67vaddl") (y #t)))

(define-public crate-swoosh-0.1.1 (c (n "swoosh") (v "0.1.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rx1361cqjcsc9w7fcig8iax098ha2aw65hvx7iq8c76crf76g4r") (y #t)))

