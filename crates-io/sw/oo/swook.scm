(define-module (crates-io sw oo swook) #:use-module (crates-io))

(define-public crate-swook-1.0.0 (c (n "swook") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slack-hook3") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "0y5vxvksqpv34llpi318wm7iv7cp9d32fklvdc6idfcrpql8w6gh")))

