(define-module (crates-io sw it switcher2) #:use-module (crates-io))

(define-public crate-switcher2-0.1.0 (c (n "switcher2") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14jkcndx20xpfp6i8q52zhzwdpbj7iwrspwpj09qh712afsfmqxx")))

