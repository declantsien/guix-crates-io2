(define-module (crates-io sw it switchstring) #:use-module (crates-io))

(define-public crate-switchstring-0.1.0 (c (n "switchstring") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1max910mfvl0d1s10q36i01px0ca637anx7i851rfzgx9pr8gng1")))

(define-public crate-switchstring-0.1.1 (c (n "switchstring") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "15zb14gja86cdslazwmc1vq1q53nr9ck80p4q9rkf6i7v4wgy1vp")))

