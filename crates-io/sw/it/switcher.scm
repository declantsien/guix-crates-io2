(define-module (crates-io sw it switcher) #:use-module (crates-io))

(define-public crate-switcher-0.0.1-alpha (c (n "switcher") (v "0.0.1-alpha") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "0fzilsm3zxnf7yjpf3kmlifw8a6vmilgigdvdsxiyjnps154hg9h") (r "1.57")))

(define-public crate-switcher-0.0.2-alpha (c (n "switcher") (v "0.0.2-alpha") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "1pfmmkda6w14z334xzj93q8w3xqda9w07ydazlx4mxgzbznddkc9") (r "1.57")))

(define-public crate-switcher-0.0.3-alpha (c (n "switcher") (v "0.0.3-alpha") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "1cx5ns6ik81kypk43xclk7m7pwc178vbvbx78358q616yr2f0xim") (r "1.57")))

(define-public crate-switcher-0.0.4-alpha (c (n "switcher") (v "0.0.4-alpha") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "0csbs4pf17hiadxk10dd3ghmxygadgkxm3ha5gs1ma65qdldp3yq") (r "1.57")))

(define-public crate-switcher-0.0.5-alpha (c (n "switcher") (v "0.0.5-alpha") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "0f5zx91hywc2bbz2m7xlz51p0hkvhg00n9gx01cg1a391wk54vla") (r "1.57")))

(define-public crate-switcher-0.0.6-alpha (c (n "switcher") (v "0.0.6-alpha") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "177j4q7lalv38943f0wxn60bpzbrq6n7m51c526sv9n8ikbqfqql") (r "1.57")))

(define-public crate-switcher-0.0.7-alpha (c (n "switcher") (v "0.0.7-alpha") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "1gkyj165yv6p345a80x3rhv7n8zj3iizbg3mvc848gcyhvr5z0am") (r "1.57")))

(define-public crate-switcher-0.0.8-alpha (c (n "switcher") (v "0.0.8-alpha") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "1l93ca6c2vxfg6wgyvn293zibc559x13nrazh8f5wvnr0vpvgg8k") (r "1.57")))

