(define-module (crates-io sw it switchable) #:use-module (crates-io))

(define-public crate-switchable-0.1.0 (c (n "switchable") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tear") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1mni0d4x05cc6cb1c1mba1577pxbbnffdgjnsna2a9g6bxqckwjc")))

