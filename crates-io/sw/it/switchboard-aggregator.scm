(define-module (crates-io sw it switchboard-aggregator) #:use-module (crates-io))

(define-public crate-switchboard-aggregator-0.1.0 (c (n "switchboard-aggregator") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04prpw4iaizrlrqqx2s1zs287fv0j3h0avlsxnb6knn94y0qlf30")))

(define-public crate-switchboard-aggregator-0.1.1 (c (n "switchboard-aggregator") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vsyi378hzzhg124g6q1lym5kra6zxjbkqds35l5i0wy76ldlsz1")))

(define-public crate-switchboard-aggregator-0.1.2 (c (n "switchboard-aggregator") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0clks6mnz7pw0hmmgnz745zb2n1yiwgnq0ni512n045anq52shm8")))

(define-public crate-switchboard-aggregator-0.1.3 (c (n "switchboard-aggregator") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "0hz7w3mjzx3j5sbhnv6zc3xwhygr3as3zbv3cnaq2w1ipx70sybx")))

(define-public crate-switchboard-aggregator-0.1.4 (c (n "switchboard-aggregator") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "0mpjsmvs9dkgbf098ffbqip4mhw6haihmi7g8dihlfxq138cwfpj")))

(define-public crate-switchboard-aggregator-0.1.5 (c (n "switchboard-aggregator") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "0v11pcfb03igwbsb1zp5dqdyvf5qnbjwcd1x1w4k6zc1h19ykazv")))

(define-public crate-switchboard-aggregator-0.1.6 (c (n "switchboard-aggregator") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "1sp6xvyqwbysvp8qq0fripvmxcsq1vrv01lx2ch825liy3q0j8v8")))

(define-public crate-switchboard-aggregator-0.1.7 (c (n "switchboard-aggregator") (v "0.1.7") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "0crg3jj2gjbq2yxff8lh9izjjigh7b9l692h5mk51hcvys59cyjc")))

(define-public crate-switchboard-aggregator-0.1.8 (c (n "switchboard-aggregator") (v "0.1.8") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "0432kk6h50imj2b0v86x7kawngc8z0zmdgnvmvk4s1pnxcl3vib8")))

(define-public crate-switchboard-aggregator-0.1.9 (c (n "switchboard-aggregator") (v "0.1.9") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)))) (h "090kgb0rwbl13k9grpbd24zb7p874jgadrxnhs29v5zazhp6pxk7")))

