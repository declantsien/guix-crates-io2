(define-module (crates-io sw it switch) #:use-module (crates-io))

(define-public crate-switch-0.1.0 (c (n "switch") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "litep2p") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "18pw64m2b2mpvqrby52d1dqjlqf66wa0xdlpr4qh9i93fj32jmv1")))

(define-public crate-switch-0.1.1 (c (n "switch") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "litep2p") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "16mc51f394sqwhlpska6jnnzr5a7l9schxjni8dca3wv41gz08gh")))

