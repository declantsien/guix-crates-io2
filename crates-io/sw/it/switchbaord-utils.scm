(define-module (crates-io sw it switchbaord-utils) #:use-module (crates-io))

(define-public crate-switchbaord-utils-0.1.1 (c (n "switchbaord-utils") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.33") (d #t) (k 0)) (d (n "zerocopy") (r "^0.5.0") (d #t) (k 0)))) (h "00z478xhx18lam2w54dbs2j437cab77sh0wppqy711aqvgdgzkgz")))

(define-public crate-switchbaord-utils-0.1.2 (c (n "switchbaord-utils") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.33") (d #t) (k 0)) (d (n "zerocopy") (r "^0.5.0") (d #t) (k 0)))) (h "15hgzklj1b57b4bc6iqhxg1wbax40gsc5c0zp2ykhxnlavbkrysk")))

