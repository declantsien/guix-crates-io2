(define-module (crates-io sw it switchboard-program-bm) #:use-module (crates-io))

(define-public crate-switchboard-program-bm-0.1.59 (c (n "switchboard-program-bm") (v "0.1.59") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "=0.8.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.9") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.58") (d #t) (k 0)) (d (n "switchboard-utils-bm") (r "^0.1.32") (d #t) (k 0)))) (h "0qr4kbay31px98azxngwj3jddp015y6jnp754gx1i4f8c10xmx27")))

