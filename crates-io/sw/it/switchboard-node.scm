(define-module (crates-io sw it switchboard-node) #:use-module (crates-io))

(define-public crate-switchboard-node-0.1.0 (c (n "switchboard-node") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "switchboard-common") (r "^0.11.2") (d #t) (k 0)) (d (n "switchboard-node-health") (r "^0.1") (d #t) (k 0)) (d (n "switchboard-node-macros") (r "^0.1") (d #t) (k 0)) (d (n "switchboard-node-metrics") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkm0hsm3nvfmpaws5sm03k6grf0hfgni0l64idpq41hmaf570x8") (f (quote (("default") ("all"))))))

