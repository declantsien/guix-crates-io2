(define-module (crates-io sw it switchtec-user-sys) #:use-module (crates-io))

(define-public crate-switchtec-user-sys-0.1.0 (c (n "switchtec-user-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sdsj17xdpbddwzn9s7dn9573ws2skislsq2fvmirg92sg26khli")))

(define-public crate-switchtec-user-sys-0.1.1 (c (n "switchtec-user-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yfj9h4v7nksd7bx23z1jzx10057pr46bg65fv1hic0s31fal9lg")))

(define-public crate-switchtec-user-sys-0.1.3 (c (n "switchtec-user-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nxpclfa3xbmbh759bfg3fnkzf1qcn7bff85dra4hdpp1dy48vkx")))

(define-public crate-switchtec-user-sys-0.1.4 (c (n "switchtec-user-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mfpfawp8lf3ix8f7rqplvxr1l8mxxkpmkdyh7f47md4la307fdl") (y #t)))

(define-public crate-switchtec-user-sys-0.1.5 (c (n "switchtec-user-sys") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y36smk0niygknj7c40yp4000k0l4lmg3sxwxnj3ca3whnwmcb8b") (y #t)))

(define-public crate-switchtec-user-sys-0.1.6 (c (n "switchtec-user-sys") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i1ra34f08vr7zvwr3dxp193aidm7h6489i698vin55pnswbz71w")))

(define-public crate-switchtec-user-sys-0.1.7 (c (n "switchtec-user-sys") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s1kwgwczqqv6pab2vcvjaya6kalfkqr2njixzxb9fiid776kin4")))

(define-public crate-switchtec-user-sys-0.1.8 (c (n "switchtec-user-sys") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yxwnjr97gjgyvr91bpgdsfpsw2lj64rq96xxc5wq9zm57rhz8am")))

(define-public crate-switchtec-user-sys-0.1.9 (c (n "switchtec-user-sys") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13fnwqcx7gx66cp5lrhdkxnaimardf06s3dn4zrv51pnvplqbdkc")))

(define-public crate-switchtec-user-sys-0.1.10 (c (n "switchtec-user-sys") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n9lbv5yx6jdvzanc5qsaqzavvc6x9bn6jfdjmffrb1061bawnd6")))

(define-public crate-switchtec-user-sys-0.1.11 (c (n "switchtec-user-sys") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f9b0gfm4381j8av5xn90mp9p05wa6195b9gx5pvnc6ma8yzs0c6")))

(define-public crate-switchtec-user-sys-0.2.0 (c (n "switchtec-user-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bgxy1mp5bir6pp02vc6fmpwccwxry35rzlfzzrxdmbngwwgw1n4")))

(define-public crate-switchtec-user-sys-0.2.1 (c (n "switchtec-user-sys") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jpk5sq9kw4xy5y7b5pqfskjsnxaps6m48v7f47s6pf52cxqi7ls")))

(define-public crate-switchtec-user-sys-0.3.0 (c (n "switchtec-user-sys") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1av51c7dgii7cmid8pkfddhajkmrynlxrfzhzhdzp9znd11d5kh1")))

(define-public crate-switchtec-user-sys-0.3.1 (c (n "switchtec-user-sys") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h7sdzsf44yf3g8cg7lbzjiizlbp216nbwsqhs4bsy3rhjq0qssj") (l "switchtec")))

(define-public crate-switchtec-user-sys-0.4.0 (c (n "switchtec-user-sys") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11xq3bl2wwclh372c9vfx0malrng53abj6k43ylbsv9ai29v62k9") (l "switchtec")))

(define-public crate-switchtec-user-sys-0.4.1 (c (n "switchtec-user-sys") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16w1hp4pxadbspk298hnmaj2g3lvayl89182fish0f94cq61s6zh") (l "switchtec")))

(define-public crate-switchtec-user-sys-0.4.2 (c (n "switchtec-user-sys") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jpmkmp8anin19m3arjfipip2fq5ymxyl2a4lxqzx5d8kkfkw2fl") (l "switchtec")))

