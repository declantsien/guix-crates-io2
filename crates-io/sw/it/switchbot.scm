(define-module (crates-io sw it switchbot) #:use-module (crates-io))

(define-public crate-switchbot-0.1.0 (c (n "switchbot") (v "0.1.0") (h "1c03n3qwkzw2zgxim9qixpf5fs20qcc8n1cm94w59nngdhfwzqjj")))

(define-public crate-switchbot-0.1.1 (c (n "switchbot") (v "0.1.1") (h "0qrpmfa9h8n5p1hsdswwkny0c4dgwbz2c3q465in90wii7ac73af")))

(define-public crate-switchbot-0.1.2 (c (n "switchbot") (v "0.1.2") (h "1r9scj9a8jl5axwal2vnk3g7b4cpxlwwsyr6n1wph1dl4rx6kc5h")))

