(define-module (crates-io sw it switchboard-utils-bm) #:use-module (crates-io))

(define-public crate-switchboard-utils-bm-0.1.32 (c (n "switchboard-utils-bm") (v "0.1.32") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.14") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.14") (d #t) (k 0)) (d (n "solana-program") (r "^1.6") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.58") (d #t) (k 0)))) (h "09lal42x0kkagz53x7rm1jh0h86qbw6gnmpx0d1mln8zspklrxcg")))

