(define-module (crates-io sw it switcheroo) #:use-module (crates-io))

(define-public crate-switcheroo-0.1.0 (c (n "switcheroo") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bpcx8n4bna418a00w9hz998ad2bqvgiq184qj1ia2d07sqqphlj")))

(define-public crate-switcheroo-0.1.1 (c (n "switcheroo") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lifhx7gp5yzk1gzjz90d268vxmnck8qc4nizhzxsd5cr9j8694a")))

(define-public crate-switcheroo-0.1.2 (c (n "switcheroo") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mlm7r1jllifx3p8qvssddmivdcqwhnk6dbb95dbm1kqcpf8zb1d")))

(define-public crate-switcheroo-0.2.0 (c (n "switcheroo") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j21fiqn2x7lfcj9d7p9nf3c4qsri5ja6r2vz80lc8sspz57fkzg")))

(define-public crate-switcheroo-0.2.1 (c (n "switcheroo") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gnx5j0r8qcs8vbi62liy803qkr9xryghk2sa8cz30m1xyhj4qc5")))

(define-public crate-switcheroo-0.2.2 (c (n "switcheroo") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1a9g6p6gisnrnc94gi5m6q9h9iy5bmcqklm1w3rbl8gb4s33nmy8")))

(define-public crate-switcheroo-0.2.3 (c (n "switcheroo") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vxm6ligr8z9pzqw742h3ccfj8zsjm1ligyd5x5ilbk5h5rxf0sf")))

(define-public crate-switcheroo-0.2.4 (c (n "switcheroo") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0h03sgczigkxzjvd0npp0sf96yxcph2brqphawrwgfsxgiw55r5p")))

(define-public crate-switcheroo-0.2.5 (c (n "switcheroo") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0g9r10c6gnp32zpgawzc6jyabgwbzn6y06blg91rzb9fhsrkiqwh")))

(define-public crate-switcheroo-0.2.6 (c (n "switcheroo") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pgkzj1hdx0yi0dkspcn5hrjj0ys0my64162bfvmf6w459wfp2kx")))

(define-public crate-switcheroo-0.2.8 (c (n "switcheroo") (v "0.2.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h37hzgwch2r0hki5nfb71bz2h4yd3ryvbspc89rjvkbd5f2mn7a")))

(define-public crate-switcheroo-0.2.9 (c (n "switcheroo") (v "0.2.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xz7l8rb359ch48l4myn4c1faxwn1fsl7xknhkcx42iads3vkh67")))

