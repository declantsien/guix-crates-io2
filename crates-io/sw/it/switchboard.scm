(define-module (crates-io sw it switchboard) #:use-module (crates-io))

(define-public crate-switchboard-0.1.0 (c (n "switchboard") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)) (d (n "netbuf") (r "*") (d #t) (k 0)))) (h "1fnr9jf42b1hwiz09lqkv5bhjqh4nvx3l5359lr7hqkjl4d823cr") (y #t)))

(define-public crate-switchboard-0.2.0 (c (n "switchboard") (v "0.2.0") (d (list (d (n "awaken") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.8.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1d0q7xv3j28w14jw3syq9d1rmhni92303knjahk6sxcn8gx6xxz3")))

(define-public crate-switchboard-0.2.1 (c (n "switchboard") (v "0.2.1") (d (list (d (n "awaken") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1z3q2wbc6dp12m6g8xhgj2mjng6y3n6j3rinwra54vhhkz88wmyk")))

(define-public crate-switchboard-0.3.0 (c (n "switchboard") (v "0.3.0") (d (list (d (n "awaken") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "17f2w6ki988sf9as169nkhb09048l0lgr457d77cf13r0khqpdl1")))

