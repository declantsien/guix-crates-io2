(define-module (crates-io sw it switchboard-node-macros) #:use-module (crates-io))

(define-public crate-switchboard-node-macros-0.1.0 (c (n "switchboard-node-macros") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1b5b420vk56pj6kpdkc94h4xxq2ccdb8n47x8rf5sjr7sladz0g9") (f (quote (("default") ("all"))))))

