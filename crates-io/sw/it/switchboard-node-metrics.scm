(define-module (crates-io sw it switchboard-node-metrics) #:use-module (crates-io))

(define-public crate-switchboard-node-metrics-0.1.0 (c (n "switchboard-node-metrics") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "prometheus-client") (r "^0.21.2") (d #t) (k 0)))) (h "0w4j629qa5yn64k16mfq95xzx30mqwjs3z2zn7qxgs1z8gj5r8b1") (f (quote (("default") ("all"))))))

