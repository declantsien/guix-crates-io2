(define-module (crates-io sw it switch_statement) #:use-module (crates-io))

(define-public crate-switch_statement-0.1.0 (c (n "switch_statement") (v "0.1.0") (h "11qgpd42hn22gfi70ka058i5bqzfiryrimw57nlzbxi9a0laagn5")))

(define-public crate-switch_statement-0.1.1 (c (n "switch_statement") (v "0.1.1") (h "062clqhwzgd48sfxlliwckh7nqqc44h35p8hl1y8wvy41phqb3vd")))

(define-public crate-switch_statement-1.0.0 (c (n "switch_statement") (v "1.0.0") (h "100z7ahh1np9fdr4722dlqn6ibw3s2giwx6xnfsi9bs38gnb4dn8")))

