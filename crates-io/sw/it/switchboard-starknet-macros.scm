(define-module (crates-io sw it switchboard-starknet-macros) #:use-module (crates-io))

(define-public crate-switchboard-starknet-macros-0.0.1 (c (n "switchboard-starknet-macros") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "ethers") (r "^2.0.7") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.30.0") (f (quote ("maths" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "15g9iw7kkmq952qmpqqb8mcil6alflwl87py5160mz1pqinrjwsk")))

(define-public crate-switchboard-starknet-macros-0.0.2 (c (n "switchboard-starknet-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gd8a3n68qhllxi9mhncqwgrd6z5rfzw2gh2cg1ri4cpz19bnyq7")))

