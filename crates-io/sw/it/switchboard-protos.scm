(define-module (crates-io sw it switchboard-protos) #:use-module (crates-io))

(define-public crate-switchboard-protos-0.1.0 (c (n "switchboard-protos") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0dkvx3gvbkb016kh8dr3673amlck7km0amc94g43czw47lnnp472")))

(define-public crate-switchboard-protos-0.1.1 (c (n "switchboard-protos") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "141jcdzlffzwrlixyaq6rhdb011vi2wa8fkd66s4y84qx2bp2a3n")))

(define-public crate-switchboard-protos-0.1.2 (c (n "switchboard-protos") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0bnby6z70lqi7vr5riz0qlyga9zynaj8zdzipx9zisrpswhv1qzb")))

(define-public crate-switchboard-protos-0.1.3 (c (n "switchboard-protos") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0qxj6m1gcd1jay68q1lqmh53d5icnnn1b2xp409lz855dkbys8j4")))

(define-public crate-switchboard-protos-0.1.4 (c (n "switchboard-protos") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1a5k1q1ymxm6785j6gdx53gam3nah8hzcvm6f59b6iki24r482ih")))

(define-public crate-switchboard-protos-0.1.5 (c (n "switchboard-protos") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0wdvgkghmzbyir07j0nib6ai0fc7mbf00f4y55g53w69b1wk975m")))

(define-public crate-switchboard-protos-0.1.6 (c (n "switchboard-protos") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "02q14c1lzkhxqh1fkd1qz04dprjsv2c39kx3l20hpsk1754iizlh")))

(define-public crate-switchboard-protos-0.1.7 (c (n "switchboard-protos") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0bdahd9gd1xcqm219p8r2sci7xjvqpn212c43hsg6nd25v2jpd6p")))

(define-public crate-switchboard-protos-0.1.8 (c (n "switchboard-protos") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0xjsam2xrcnc01l04pz36j1bb1a91x5685m2w3qkwbqjr46x9mbd")))

(define-public crate-switchboard-protos-0.1.9 (c (n "switchboard-protos") (v "0.1.9") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1x3w36x9yq0r0fx6q7ifdb9iq3ssa0bswzji0ryzx1cv1dsrf3gf")))

(define-public crate-switchboard-protos-0.1.10 (c (n "switchboard-protos") (v "0.1.10") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1zrc19ysdqxhbx7hbx5fml547c89iczk0q618gh97bcymykf78v8")))

(define-public crate-switchboard-protos-0.1.11 (c (n "switchboard-protos") (v "0.1.11") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0mnnbx01qf5iry0nj8bxf52aj1bxbb5a3rfm835hdvjzla9vb3gr")))

(define-public crate-switchboard-protos-0.1.12 (c (n "switchboard-protos") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "01rdpani7a2wm44pzgzjs4kg5fky62072sr7npfvhbh43g4vvybm")))

(define-public crate-switchboard-protos-0.1.13 (c (n "switchboard-protos") (v "0.1.13") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0l0jbznx4vy0fzr0k6ls6s5fb91zb7fw440rd1w68zgflvp7azha")))

(define-public crate-switchboard-protos-0.1.14 (c (n "switchboard-protos") (v "0.1.14") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "094pi7sh2zqaf32yqivm50n90dwvi6giq6xpq6gcniywmjyijw0j")))

(define-public crate-switchboard-protos-0.1.15 (c (n "switchboard-protos") (v "0.1.15") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1dcrzwwvz9mw2vhs1zqp4xy5mlff2wi0c0qpwbgp7vin3l0wrf86")))

(define-public crate-switchboard-protos-0.1.16 (c (n "switchboard-protos") (v "0.1.16") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "11q958spjz0w17746g11azcpzcbg3ah0x5m4ak2yw0x5s1argsiv")))

(define-public crate-switchboard-protos-0.1.18 (c (n "switchboard-protos") (v "0.1.18") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1anclgvl44mb7560yc6m6spfrz3lwsx8szfm4g57w1s1k5svvbfl")))

(define-public crate-switchboard-protos-0.1.19 (c (n "switchboard-protos") (v "0.1.19") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0lhrfvfzp9045vscbxnxdyirgmgrnnwvvyjwfkfwlqjj0gg5csm8")))

(define-public crate-switchboard-protos-0.1.20 (c (n "switchboard-protos") (v "0.1.20") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1v59md6gsp9la70qzlapxvsljmj4njavbwdgx2yigbnbv8z8zip1")))

(define-public crate-switchboard-protos-0.1.21 (c (n "switchboard-protos") (v "0.1.21") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "16q7nbm7z8wnxx7zmzw5rzvq5i3dka30yl42sb8apc5lr62jxxl9")))

(define-public crate-switchboard-protos-0.1.22 (c (n "switchboard-protos") (v "0.1.22") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1fa85vf8wi0sriwsgjc1nqyl8zwxzs4cwpc4ml2gr8kfa3qw5b24")))

(define-public crate-switchboard-protos-0.1.23 (c (n "switchboard-protos") (v "0.1.23") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "00kdlr9yy61cd14f3q8120nrayjqqcv6p8shl9gfnixcq679fhv0")))

(define-public crate-switchboard-protos-0.1.24 (c (n "switchboard-protos") (v "0.1.24") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1cigyi2kw2lwlz9kfa6fjy1k95bbigckl74kg8ccpql9m7b38lqk")))

(define-public crate-switchboard-protos-0.1.25 (c (n "switchboard-protos") (v "0.1.25") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0y7x8riih85klf2sd3k8gq9lmajc3r7avr5ah685f7x0908d3x89")))

(define-public crate-switchboard-protos-0.1.26 (c (n "switchboard-protos") (v "0.1.26") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1sdypc0y3xdlk0cx9mqvaj0sgfxgc8i661mba191wpsykax8jky9")))

(define-public crate-switchboard-protos-0.1.27 (c (n "switchboard-protos") (v "0.1.27") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0575462iklhcac643xai96f5hs0m4il5c7br4gas0pd1lzzdpa9z")))

(define-public crate-switchboard-protos-0.1.28 (c (n "switchboard-protos") (v "0.1.28") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1i4k07n1j0z24jdii5v67m3nn7yxxzwl4ra34hh7z80y7v36djjr")))

(define-public crate-switchboard-protos-0.1.29 (c (n "switchboard-protos") (v "0.1.29") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1m4swydbcg98bd2lj5qz9qv9lnmlilsd9lja7qwgd0za4ylbl20w")))

(define-public crate-switchboard-protos-0.1.30 (c (n "switchboard-protos") (v "0.1.30") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "079518kckfbsjd71zhhm4mmnyvpy7yck7yf67grcr4wlgbfccn2i")))

(define-public crate-switchboard-protos-0.1.31 (c (n "switchboard-protos") (v "0.1.31") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1nvc9ssvrsg7ckyfs0zj8lgb2394rfk4p6i86vcfs9apak9ghwq6")))

(define-public crate-switchboard-protos-0.1.32 (c (n "switchboard-protos") (v "0.1.32") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0nlbygcd0ki0sqgfnc35rhg90vx38cpwz0p252xk0jkz3axn2k4j")))

(define-public crate-switchboard-protos-0.1.33 (c (n "switchboard-protos") (v "0.1.33") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "18i8vyb9mkm1asmrn0hwmdwda9dxymrfysi5cxiw103mswnqgkj8")))

(define-public crate-switchboard-protos-0.1.34 (c (n "switchboard-protos") (v "0.1.34") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1vbln05v04w3a791lgrb0bzdiijq3f8nv1ilq4y0r7qkc6wyhdfg")))

(define-public crate-switchboard-protos-0.1.35 (c (n "switchboard-protos") (v "0.1.35") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0fjd25kbly584c2m5qy6434hph39pii93l87pz3xr45dyrc40w55")))

(define-public crate-switchboard-protos-0.1.36 (c (n "switchboard-protos") (v "0.1.36") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "035yw9afl0g7igbwfmx33ssgpw8dr2ql7pwp7y3vgzia563y0i8v")))

(define-public crate-switchboard-protos-0.1.37 (c (n "switchboard-protos") (v "0.1.37") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "195qa0q5gv9wkmagznkwhd6sidij12kgja1pxnf9w8mdhdvbnmaq")))

(define-public crate-switchboard-protos-0.1.38 (c (n "switchboard-protos") (v "0.1.38") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "10bwrd4k6g3kdf8prlh8fsh1j6smya1ijqddmigagj3333mcm2wb")))

(define-public crate-switchboard-protos-0.1.39 (c (n "switchboard-protos") (v "0.1.39") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1ghfksrla0in47y3b6gcryx1c2xbm2x3wswwslwksc85cl0xa26z")))

(define-public crate-switchboard-protos-0.1.40 (c (n "switchboard-protos") (v "0.1.40") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0qc8jhv2np3kqd7m959a75jr06wl4wh72saz0p1y2by8m1n2pda8")))

(define-public crate-switchboard-protos-0.1.41 (c (n "switchboard-protos") (v "0.1.41") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "01z708zikb7l51wyd73n52yk49ph8dyv5hijwwnw8yl1g69hdkf8")))

(define-public crate-switchboard-protos-0.1.42 (c (n "switchboard-protos") (v "0.1.42") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1pa38yi4r9xfn9wc1np5gcqgc616krkcjjhp1zzrhvrx2ik3sqpi")))

(define-public crate-switchboard-protos-0.1.43 (c (n "switchboard-protos") (v "0.1.43") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "16alrkk3s6pxqqljrynm6762l5kv032xvk04472gj81cx9v656s1")))

(define-public crate-switchboard-protos-0.1.44 (c (n "switchboard-protos") (v "0.1.44") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "00lb218czx1fpvxsyph8nbvz60fbwdqwbpir6rkxghab7gnjx7mm")))

(define-public crate-switchboard-protos-0.1.45 (c (n "switchboard-protos") (v "0.1.45") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1fcv81314zddw40nfr9l3agyzxlqiavfjfxqr8hdqvclmznhvhx8")))

(define-public crate-switchboard-protos-0.1.46 (c (n "switchboard-protos") (v "0.1.46") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1sbw57izs301slzc3ba4da646kashhab06hgjp193c8vd8mca0a1")))

(define-public crate-switchboard-protos-0.1.47 (c (n "switchboard-protos") (v "0.1.47") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0fm37hx1bl6sy2nn8ga0v16cq1zf9ila5hb74y1gwhvpx8p8nakz")))

(define-public crate-switchboard-protos-0.1.48 (c (n "switchboard-protos") (v "0.1.48") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "01b1mgfng5rp9cavw7dxq7zwqh7xwjdlgand8k3aqr9bi44rv22y")))

(define-public crate-switchboard-protos-0.1.49 (c (n "switchboard-protos") (v "0.1.49") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "047zbavw3si3f4khm4fghdkwlca6mimgyfsd0i4axpqha2anigcz")))

(define-public crate-switchboard-protos-0.1.50 (c (n "switchboard-protos") (v "0.1.50") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0vjpcmb4x6jpc4wnjlwch12hhplawr1h6mzwiyz2sdvsfkdqj3hr")))

(define-public crate-switchboard-protos-0.1.51 (c (n "switchboard-protos") (v "0.1.51") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "14clrc5iq1lc82qbmq9s3zyyvs1hszfsn7yavrakhzixarjbzkl2")))

(define-public crate-switchboard-protos-0.1.52 (c (n "switchboard-protos") (v "0.1.52") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "05vl8kg1pa3yzzqaj0zbfyypcah3w9iry9vdqsjcqssy0ppy5n07")))

(define-public crate-switchboard-protos-0.1.53 (c (n "switchboard-protos") (v "0.1.53") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1zm06483xwwvxisf8d686px8jh0xmkpqn7mmabq4x53wcnfhlji1")))

(define-public crate-switchboard-protos-0.1.54 (c (n "switchboard-protos") (v "0.1.54") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "151gl62d82vbficn0iyvcrb405qqrx5whkh43nis3wv61n962jcf")))

(define-public crate-switchboard-protos-0.1.55 (c (n "switchboard-protos") (v "0.1.55") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "17zaqi1m7zfc2rv3kiqbwwsamxrqkfqq8xnnyq70995mgsb91f50")))

(define-public crate-switchboard-protos-0.1.56 (c (n "switchboard-protos") (v "0.1.56") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0x74hc4807p0gsymvsjc0mlw1dzn4pgcyyqaw260ffg8dbvgfb6s")))

(define-public crate-switchboard-protos-0.1.57 (c (n "switchboard-protos") (v "0.1.57") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1jkxk1ps7716164i5zhcfxr66dv8lxgv4y164brxc0a9dwx9rwpj")))

(define-public crate-switchboard-protos-0.1.58 (c (n "switchboard-protos") (v "0.1.58") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "1fxyfa09fh2w59lim11ay4lra8vvap5asf93x7r5p2j3yrgzg024")))

(define-public crate-switchboard-protos-0.1.59 (c (n "switchboard-protos") (v "0.1.59") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "07b67nb58j0spzxg9jcwjrpyk0jr3yx7maxrsyb9sxf3wjdgvshs")))

(define-public crate-switchboard-protos-0.1.60 (c (n "switchboard-protos") (v "0.1.60") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)))) (h "0wma05c919kg6pbip79s9fsymi8s5pccrmhqg7m14bgpby3qjb9f")))

