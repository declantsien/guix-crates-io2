(define-module (crates-io sw it switchboard-program-packed) #:use-module (crates-io))

(define-public crate-switchboard-program-packed-0.1.59 (c (n "switchboard-program-packed") (v "0.1.59") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "=0.8.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.9") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.58") (d #t) (k 0)) (d (n "switchboard-utils-packed") (r "^0.1.33") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "0640wsas8f87yj9rrfv1vbzvv6maskaaa5xllm8kp254g0r675r4")))

