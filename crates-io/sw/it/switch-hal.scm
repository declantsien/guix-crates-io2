(define-module (crates-io sw it switch-hal) #:use-module (crates-io))

(define-public crate-switch-hal-0.1.0 (c (n "switch-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "1gli5s62yilq44hrs7g9ncq7r9sk7x561yq57vxrq81ij97qdh5v")))

(define-public crate-switch-hal-0.1.1 (c (n "switch-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "0wy02flxcfr9w8j66br8dkisnpkk5v496b6gz372wl728n5q7llj")))

(define-public crate-switch-hal-0.2.0 (c (n "switch-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "07cqps4sxdh7rk6194dxivan0fk9sna3gqmj50057jh7hqjfl14d")))

(define-public crate-switch-hal-0.2.1 (c (n "switch-hal") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "0mpgpzyl28mmq030k7mdp6l56p60v8fayrk4ga7nfrfk4k5iairx")))

(define-public crate-switch-hal-0.3.0 (c (n "switch-hal") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0zkrb43dxbrg27im4w2cl2nblndf3bgyvjrj6ympp44q7dkw9d1d") (y #t)))

(define-public crate-switch-hal-0.3.1 (c (n "switch-hal") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "09dyryj5731b6fxkh44kgvl730gqljcz2bi2v50c6rd2bh1h0iqj")))

(define-public crate-switch-hal-0.3.2 (c (n "switch-hal") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1dxc89b3kahapkyjk86bnvl2ivfpk2bk6i3sp7hvf8vj2r007hcj")))

(define-public crate-switch-hal-0.3.3 (c (n "switch-hal") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0wwqq19sjjnx705jqbfcb3q84x8d5c9vjgbp3lhzy64953k1j08w")))

(define-public crate-switch-hal-0.4.0 (c (n "switch-hal") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1grz53ns9ywqpk5q9isdsdscxwgkw148x2b1n54n4wnirg4av94h")))

