(define-module (crates-io sw it switchboard-program2) #:use-module (crates-io))

(define-public crate-switchboard-program2-0.1.0 (c (n "switchboard-program2") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.4") (d #t) (k 0)) (d (n "switchboard-protos") (r "^0.1.0") (d #t) (k 0)))) (h "1id7khmg9g324b9ysx4pld8i79ybhqw58x4bpp5fmw2p54pd8s2z") (y #t)))

