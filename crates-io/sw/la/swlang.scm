(define-module (crates-io sw la swlang) #:use-module (crates-io))

(define-public crate-swlang-0.0.2 (c (n "swlang") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0pkmvl8w7p2znm5sbdzplrvfvarnapnf1mwnffdyc5bx9s9cmsln")))

(define-public crate-swlang-0.0.3 (c (n "swlang") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "11bpw5hw91cfg8r2b216i59v5fawmv5m1lxkyvk72fbmkm0dzw5m")))

(define-public crate-swlang-0.0.4 (c (n "swlang") (v "0.0.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1sm454ym1diw20p397wf7263mwsvcr79v4bglfvqiq7y6xsvfm6n")))

(define-public crate-swlang-0.0.5 (c (n "swlang") (v "0.0.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0yl5i3d2pyfw943abrvg7bh8fplxbcc32vxk1mpcdhn87rsl59yn")))

(define-public crate-swlang-0.0.6 (c (n "swlang") (v "0.0.6") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "09nw2i5yisxzgx225993rbbyyzn0cjca5gy8ajq045mbn5qcdc2j")))

(define-public crate-swlang-0.0.7 (c (n "swlang") (v "0.0.7") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0g2ill2qj9n7ls06hh68r8c357af05sf9hb2kfrn79jd773b3n60")))

(define-public crate-swlang-0.0.8 (c (n "swlang") (v "0.0.8") (d (list (d (n "actix-rt") (r "^2.8.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h19j71dvi6lf57kwaml0dmmici3fnagl1c1khpaysgzzs9j8h4w")))

(define-public crate-swlang-0.0.9 (c (n "swlang") (v "0.0.9") (d (list (d (n "actix-rt") (r "^2.8.0") (d #t) (k 0)) (d (n "cargo-tarpaulin") (r "^0.26.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03cv55hwfjjdyqxh8qji0fmdphy2vrwyxmr4hy00v0f51n5zci1x")))

(define-public crate-swlang-0.0.10 (c (n "swlang") (v "0.0.10") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03f1n1p76g4rr7n6h454a2q5jyh7sjfx40nvn68vm7g5ajslisg2")))

(define-public crate-swlang-0.0.11 (c (n "swlang") (v "0.0.11") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dj874lpxz763if61nksqf3daplimbhldsspmb23gkxga25vzgp2")))

(define-public crate-swlang-0.0.12-pre-alpha (c (n "swlang") (v "0.0.12-pre-alpha") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f7v7kmz2xwq0mj44zcbvp2dwkbcjsxqnjawq13k4dnybz3zi4wc")))

