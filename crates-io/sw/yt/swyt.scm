(define-module (crates-io sw yt swyt) #:use-module (crates-io))

(define-public crate-swyt-1.0.0 (c (n "swyt") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "psutil") (r "^3.1.0") (d #t) (k 0)))) (h "1r9hv8fp9qpk2bm9j9irncpvav21ivcrppvrmdsks42in7f3qzcx")))

(define-public crate-swyt-1.0.1 (c (n "swyt") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "psutil") (r "^3.1.0") (d #t) (k 0)))) (h "1a2rg57cqg2irld73s5rkgismcvbg2vlzjk1smkkhwprbflf7m4h")))

(define-public crate-swyt-1.1.0 (c (n "swyt") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (f (quote ("process" "runtime-polyfill"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r7kglfvnqxpmajb4whwz3qsl9zb95wp7rlkp490bsbn410x8bsw")))

(define-public crate-swyt-1.2.0 (c (n "swyt") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (f (quote ("process" "runtime-polyfill"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "09mcni8dlxa15z9k478vb5p28pb3ssjrd6plx9my0y5ni435l719")))

(define-public crate-swyt-1.3.0 (c (n "swyt") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28") (d #t) (k 0)))) (h "17rk3bp05ckbnpd0lj7kmf9gaxkq846j73h8qlpip8dqv1njq0gq")))

