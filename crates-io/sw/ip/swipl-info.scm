(define-module (crates-io sw ip swipl-info) #:use-module (crates-io))

(define-public crate-swipl-info-0.1.0 (c (n "swipl-info") (v "0.1.0") (h "0iqss7samqgzrbgm2c54xml5z4i2cyrdw7g1m3xmp997fi357m08")))

(define-public crate-swipl-info-0.2.0 (c (n "swipl-info") (v "0.2.0") (h "0nl0nx0d6b2zfs4qk76gvz9axk9sx94g81wy4xp2rhkv52zihy41")))

(define-public crate-swipl-info-0.3.0 (c (n "swipl-info") (v "0.3.0") (h "14jx9aj125bd3i2h62d42dd33x3js81nqgi8qdh5fncza4mg1lm1")))

(define-public crate-swipl-info-0.3.1 (c (n "swipl-info") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0a28qx8rf6z8hk8rkqmf6rhwgz3lk1i8x1b7dn8lsj55pxnczdzi")))

(define-public crate-swipl-info-0.3.2 (c (n "swipl-info") (v "0.3.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "06kc9gawxwg745w2lk4q0a5py746d80g7byc4g79rxgl7vhlxhnk")))

