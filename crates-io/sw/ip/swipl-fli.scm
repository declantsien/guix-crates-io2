(define-module (crates-io sw ip swipl-fli) #:use-module (crates-io))

(define-public crate-swipl-fli-0.1.0 (c (n "swipl-fli") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.1") (d #t) (k 1)))) (h "0i9yvmjbvaa5j5nky4s19g3f3acq8scxrdflr3kj5siyxlqwkl92") (l "swipl")))

(define-public crate-swipl-fli-0.2.0 (c (n "swipl-fli") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.2") (d #t) (k 1)))) (h "16ni6khsxyp0rl8xr3zgxw3ck7zvyq1w28qg5sb2mjhzd2n905bs") (l "swipl")))

(define-public crate-swipl-fli-0.3.0 (c (n "swipl-fli") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3") (d #t) (k 1)))) (h "1930phzz99v732m4kckccz467pxkyma414fc8lhx4914nmbbr485") (l "swipl")))

(define-public crate-swipl-fli-0.3.1 (c (n "swipl-fli") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.1") (d #t) (k 1)))) (h "1lwkliw937nd9ji9zb4p7g21scd4hb55bchgsnh6njgrmhxj2h5m") (l "swipl")))

(define-public crate-swipl-fli-0.3.2 (c (n "swipl-fli") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.1") (d #t) (k 1)))) (h "17jv9vb8vpbdx67cv27q77v31dxw610pfy178c3rz5p38mx0rbps") (l "swipl")))

(define-public crate-swipl-fli-0.3.3 (c (n "swipl-fli") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.2") (d #t) (k 1)))) (h "00j1rmnn6p4lkq6ns3k289pqiqqd7vvsb6zw2dszqmaxk04i4h4p") (l "swipl")))

(define-public crate-swipl-fli-0.3.4 (c (n "swipl-fli") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.2") (d #t) (k 1)))) (h "1hc4b0c42isb5fz2mq69q32lvg2pish856qgsxsrj7zqbb8k6q23") (l "swipl")))

(define-public crate-swipl-fli-0.3.5 (c (n "swipl-fli") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.2") (d #t) (k 1)))) (h "17dlqwf5adrzl36hi3m3lprag43753zsp0s14i1isycm9gr9cs5v") (l "swipl")))

(define-public crate-swipl-fli-0.3.6 (c (n "swipl-fli") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "swipl-info") (r "^0.3.2") (d #t) (k 1)))) (h "1rhfy673027rm32abi04q5qq3izjazsbhv2088hjgpyk4h3xbjgx") (l "swipl")))

