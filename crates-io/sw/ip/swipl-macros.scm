(define-module (crates-io sw ip swipl-macros) #:use-module (crates-io))

(define-public crate-swipl-macros-0.1.0 (c (n "swipl-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1rq43syacb9m3w8v8z3h1igwmmqpd4g8ccs9id8lpbsalvgppb49")))

(define-public crate-swipl-macros-0.2.0 (c (n "swipl-macros") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1a0w4k4zicabcrnhnv5dmwm1zz58q167ycsjn1zcri0p1mr4jv2y")))

(define-public crate-swipl-macros-0.3.0 (c (n "swipl-macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0b06pqpxkr4z61r3c5bxyvvixbb2d1aqqn482azjymcl55pwgfjx")))

(define-public crate-swipl-macros-0.3.1 (c (n "swipl-macros") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1l4rw7m1bq1day2dg8s4mdifmhgnr0rrzj1iqllxip9mwb0fwaaz")))

(define-public crate-swipl-macros-0.3.2 (c (n "swipl-macros") (v "0.3.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1mv1qv7cf67i2jwz6x787fp015q2qhbsv30pp04l8pv72jklwfi5")))

(define-public crate-swipl-macros-0.3.3 (c (n "swipl-macros") (v "0.3.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "180hnr3pifv9rh79dcgfz8xxljq51s5213hw7ghm3r8jvh8zqxd5")))

(define-public crate-swipl-macros-0.3.4 (c (n "swipl-macros") (v "0.3.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1ni081fkawzf9x9j2cbn7dkhzffh90kj35kn4idxa4a56h15wh70")))

(define-public crate-swipl-macros-0.3.5 (c (n "swipl-macros") (v "0.3.5") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0r1a3iwfknkawxriiqr003xn823ayl9rsjxam6vdw9ynb0n85las")))

(define-public crate-swipl-macros-0.3.6 (c (n "swipl-macros") (v "0.3.6") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0qnnpri8xlf6rjqfc9805vch4ayqqmvgmyafw0myvmbc6zkgndmj")))

(define-public crate-swipl-macros-0.3.7 (c (n "swipl-macros") (v "0.3.7") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1c5gnrnf5wy7f44di1pnzmdmcadgm7n4xip6a79i14bgpbl90s46")))

(define-public crate-swipl-macros-0.3.8 (c (n "swipl-macros") (v "0.3.8") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0hsj01b1jx4wl3j7mv03yqc2lmc9jak04dg4sy4408x29hdvs6bb")))

