(define-module (crates-io sw il swil) #:use-module (crates-io))

(define-public crate-swil-0.1.0 (c (n "swil") (v "0.1.0") (d (list (d (n "raw-window-handle") (r "^0.6.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.12.0") (f (quote ("allow-unsafe-code" "dl-libxcb" "resource_manager" "xkb"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "xkbcommon") (r "^0.7.0") (f (quote ("x11"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0il1ywxszniysw24f3bm2wnnlqmgj7v3n1yl5cvyrkfqlcvcjg33")))

(define-public crate-swil-0.2.0 (c (n "swil") (v "0.2.0") (d (list (d (n "raw-window-handle") (r "^0.6.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.12.0") (f (quote ("allow-unsafe-code" "dl-libxcb" "resource_manager" "xkb"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "xkbcommon") (r "^0.7.0") (f (quote ("x11"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0dgq47706w4ymchd4crk8wlhx1gfb021m37fzzkjycgvggi6myi2")))

