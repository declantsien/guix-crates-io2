(define-module (crates-io sw rs swrs) #:use-module (crates-io))

(define-public crate-swrs-0.1.0 (c (n "swrs") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ritelinked") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ynw61vywv1lclw4ig8f07xz04mwzbdh1lcaqzzvjchmcsirxg23") (y #t)))

(define-public crate-swrs-0.1.1 (c (n "swrs") (v "0.1.1") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ritelinked") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c3jdqppixyzsngxzqzk7pivx5dbz2349hkr9zgg0iyb274hg6xl")))

