(define-module (crates-io sw ym swym) #:use-module (crates-io))

(define-public crate-swym-0.1.0-preview (c (n "swym") (v "0.1.0-preview") (d (list (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 2)) (d (n "jemallocator") (r "^0.1.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lock_api") (r "^0.1.5") (d #t) (k 0)))) (h "00lbag878127i29d9a02p0cgkaqvii2v2hbx7vjbjn2q6h572nxx") (f (quote (("stats" "lazy_static") ("default") ("debug-alloc"))))))

