(define-module (crates-io sw nb swnb-complex) #:use-module (crates-io))

(define-public crate-swnb-complex-0.1.0 (c (n "swnb-complex") (v "0.1.0") (h "1bypr5q5hvasxf6jn4nk0d04qh2slyl26saf6kx49ig3s7w0vjjz")))

(define-public crate-swnb-complex-0.2.0 (c (n "swnb-complex") (v "0.2.0") (h "1mnilym4n9nig88m56944xrkj4526np4yn894593rk8hz41mpkgc")))

(define-public crate-swnb-complex-0.2.2 (c (n "swnb-complex") (v "0.2.2") (h "1zy1j7y8jmrlb54dkhl7wzh5kccrkl04gijr18zqhigq7pfx6f2a")))

