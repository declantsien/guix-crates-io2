(define-module (crates-io sw nb swnb-timer) #:use-module (crates-io))

(define-public crate-swnb-timer-0.1.0 (c (n "swnb-timer") (v "0.1.0") (h "1gdwcw71qkhsa9i0cls2zjpqxr5bjyad36hp8iw9hk6flqz948d3")))

(define-public crate-swnb-timer-0.1.1 (c (n "swnb-timer") (v "0.1.1") (h "17l5nsrx0jl0v2xlh680dsg18cdrhfcy7ambl5m18a9g9ga44nxi")))

(define-public crate-swnb-timer-0.1.2 (c (n "swnb-timer") (v "0.1.2") (h "12yypfs72ffca2bhg6dmp0fpdn1550rb7k3yp6f9z7zmdz26nqbq")))

(define-public crate-swnb-timer-0.2.0 (c (n "swnb-timer") (v "0.2.0") (h "1sjv8kcwgl4b0gafhzqdbri5k2vi1irv81zjs4i2dbaz81r506b4")))

