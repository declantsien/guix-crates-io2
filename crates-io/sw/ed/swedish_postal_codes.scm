(define-module (crates-io sw ed swedish_postal_codes) #:use-module (crates-io))

(define-public crate-swedish_postal_codes-0.2.0 (c (n "swedish_postal_codes") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dwkyz24pc85a9qp6sg6xl84lw07455yvc6y89zjf1nfsj4gp1hj")))

