(define-module (crates-io sw as swasmi-validation) #:use-module (crates-io))

(define-public crate-swasmi-validation-0.1.0 (c (n "swasmi-validation") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "swasm") (r "^0.31") (k 0)))) (h "0rqj580skzpc2a327wy67bvkswpnjilx5d048sjz2xjnlnh2yg0k") (f (quote (("std" "swasm/std") ("default" "std") ("core" "hashbrown/nightly"))))))

