(define-module (crates-io sw as swas) #:use-module (crates-io))

(define-public crate-swas-0.1.0 (c (n "swas") (v "0.1.0") (h "0xxyr0qaipqylrh8sj4xpvj028c1cfnix66vhfd3zdl0z0v2pzib")))

(define-public crate-swas-0.2.0 (c (n "swas") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1jw7hh2m248hc935128ngljn32xibbcics7dfbnlha6n64shlb1i") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-swas-0.3.0 (c (n "swas") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "0sn5c4r348c9r633cd4jnnqx51c0xy77q76qhdhmzkk01fbvgisf") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-swas-0.3.1 (c (n "swas") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)))) (h "1j3cqb9nfyc39kf9c3hs69d0z7gxicrp8i586gwg6c788jcar8f7") (f (quote (("default" "application") ("application" "clap"))))))

