(define-module (crates-io sw as swash) #:use-module (crates-io))

(define-public crate-swash-0.1.0 (c (n "swash") (v "0.1.0") (d (list (d (n "yazi") (r "^0.1.3") (d #t) (k 0)) (d (n "zeno") (r "^0.2.0") (d #t) (k 0)))) (h "00nrm091yvpfpy5gbd7dvx8cfnqmsbnxgwibk42mcdm4wzbnwr5l")))

(define-public crate-swash-0.1.1 (c (n "swash") (v "0.1.1") (d (list (d (n "yazi") (r "^0.1.3") (d #t) (k 0)) (d (n "zeno") (r "^0.2.0") (d #t) (k 0)))) (h "189ljgviwllwmyi6djllcz5dm9r16dwzf0n7bm5nxcmysbli3w1q")))

(define-public crate-swash-0.1.2 (c (n "swash") (v "0.1.2") (d (list (d (n "yazi") (r "^0.1.3") (d #t) (k 0)) (d (n "zeno") (r "^0.2.0") (d #t) (k 0)))) (h "1lxnnk934n6kxhfdzm1c9jbajwvj7033wlhid3ky8clq0ajds5qz")))

(define-public crate-swash-0.1.3 (c (n "swash") (v "0.1.3") (d (list (d (n "yazi") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "14zd4dhc5yrb7c2qyxk969x27qicpmxacp1167f23xa51f5y14rf") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.4 (c (n "swash") (v "0.1.4") (d (list (d (n "yazi") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0bgqwxz1j1kid9ql0znbc8ihs5pa385xf8p7mzrzbdbn9c0b5gd1") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.5 (c (n "swash") (v "0.1.5") (d (list (d (n "yazi") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "17av5n7lqi4p1py6k3xsxg2ikf74lsw02wbhlsb17y4h6s6rsg0y") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.6 (c (n "swash") (v "0.1.6") (d (list (d (n "yazi") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "18133w2g574g3b2vd0wir93r6iywhdldzsx3kslg1dd9i1c8xccb") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.7 (c (n "swash") (v "0.1.7") (d (list (d (n "yazi") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0ll7sg7y3x3gnyrqklmm5m72p297b1bi01k440h8nkzy6mmy39pv") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.8 (c (n "swash") (v "0.1.8") (d (list (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0zz739j4z7h48cmm7r95wq2qn1l552ps36lif9r38g1m2g476z1v") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.9 (c (n "swash") (v "0.1.9") (d (list (d (n "read-fonts") (r "^0.15.2") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "06ff96migmq8xn6sanhhi48a198xyjj4j27dm8hhzac7ixqgrl3b") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render")))) (y #t)))

(define-public crate-swash-0.1.10 (c (n "swash") (v "0.1.10") (d (list (d (n "read-fonts") (r "^0.15.2") (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "1wylf1lcwbbrcr13nkffg4scgx2kna0h9vr4ianswmwjvjrb5njk") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render")))) (y #t)))

(define-public crate-swash-0.1.11 (c (n "swash") (v "0.1.11") (d (list (d (n "read-fonts") (r "^0.15.2") (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0a2kd2dqnszxvzci5hwwik265nk9am5sg288cnw0mx6k2y8zm53y") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render")))) (y #t)))

(define-public crate-swash-0.1.12 (c (n "swash") (v "0.1.12") (d (list (d (n "read-fonts") (r "^0.15.2") (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "1rwx10vn5msh9g3vx11wr0663k64ym2wcqa2c0jkd4pq99kg8vyh") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.13 (c (n "swash") (v "0.1.13") (d (list (d (n "read-fonts") (r "^0.16.0") (k 0)) (d (n "read-fonts") (r "^0.16.0") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "090frzdb5dfzsr4vp83lx2sdlqi8gsiqh45f1ijmi66kj3xkdxls") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.14 (c (n "swash") (v "0.1.14") (d (list (d (n "read-fonts") (r "^0.18.0") (k 0)) (d (n "read-fonts") (r "^0.18.0") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0ad8qvp4qfplkahr6w23sqkkfl6h4x4sgnvz071cyaiqdxv3c5vw") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.15 (c (n "swash") (v "0.1.15") (d (list (d (n "read-fonts") (r "^0.19.0") (k 0)) (d (n "read-fonts") (r "^0.18.0") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "0ardr8mwwckww22qfiwlkshf23xyyvqwi5hr0j8wnvqaisd8iv06") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

(define-public crate-swash-0.1.16 (c (n "swash") (v "0.1.16") (d (list (d (n "read-fonts") (r "^0.19.0") (k 0)) (d (n "read-fonts") (r "^0.18.0") (f (quote ("scaler_test"))) (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zeno") (r "^0.2.2") (o #t) (k 0)))) (h "1m227pmwv4wwn6yb2ldpvj85c5bckvsfqiwm0f59xw5sa0mn2ak8") (f (quote (("scale" "yazi" "zeno") ("render" "scale" "zeno/eval") ("default" "scale" "render"))))))

