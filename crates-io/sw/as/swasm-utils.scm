(define-module (crates-io sw as swasm-utils) #:use-module (crates-io))

(define-public crate-swasm-utils-0.6.1 (c (n "swasm-utils") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "swasm") (r "^0.31") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)))) (h "1swapqyxrg80mgwnr1ad8zvy1kly1df6a6s8fv328f6zifqb1ikl") (f (quote (("std" "swasm/std" "log/std" "byteorder/std") ("default" "std"))))))

