(define-module (crates-io sw as swasm) #:use-module (crates-io))

(define-public crate-swasm-0.31.3 (c (n "swasm") (v "0.31.3") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1p9cxkrnsgh4kk3hjg6zzw6xpjfyjbva5500a03gm9zayf9i4h5m") (f (quote (("std" "byteorder/std") ("default" "std"))))))

