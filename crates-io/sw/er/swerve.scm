(define-module (crates-io sw er swerve) #:use-module (crates-io))

(define-public crate-swerve-0.1.0 (c (n "swerve") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0nvcampyjd9x1l5d5di76ln8c3gdgid52xdnc1apg33vg946794f")))

(define-public crate-swerve-0.2.0 (c (n "swerve") (v "0.2.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "formdata") (r "^0.12.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rhai") (r "^0.7") (d #t) (k 0)) (d (n "rocket") (r "^0.3.8") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.8") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0xrassgbm9rgpjv5c7z5z8sc0jp8x105pmr8fbb1jkxrfsfp4if3")))

