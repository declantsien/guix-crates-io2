(define-module (crates-io sw ap swapbytes-derive) #:use-module (crates-io))

(define-public crate-swapbytes-derive-0.1.1 (c (n "swapbytes-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1iv1slyq9bi81p54h6cc18r54bbkvw2c4ywa8gj06ghbfry8vlsb")))

(define-public crate-swapbytes-derive-0.2.0 (c (n "swapbytes-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v9a086pf1wwkaxz77ssccxz1afybfg2wf9mdldx9jc63dh492iv")))

