(define-module (crates-io sw ap swappy) #:use-module (crates-io))

(define-public crate-swappy-0.1.0 (c (n "swappy") (v "0.1.0") (d (list (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)))) (h "14d1zzd9gwv1d1yr1impi63yrs1lmnkz40gsjy49iyna76g2r1ry") (f (quote (("benchmark"))))))

(define-public crate-swappy-0.2.0 (c (n "swappy") (v "0.2.0") (d (list (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)))) (h "04w8l22ark57mjn9bh99kfrikbcr43wrrvr1chpgil0xqljd6a56") (f (quote (("benchmark"))))))

(define-public crate-swappy-0.3.0 (c (n "swappy") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "shellexpand") (r "^1.1.1") (d #t) (k 0)))) (h "1cnrbd7n0by1pv7p725m83bvb1p7m8fa5fwy3g993r6k34qfx2vk") (f (quote (("benchmark"))))))

