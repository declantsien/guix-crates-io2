(define-module (crates-io sw ap swapper) #:use-module (crates-io))

(define-public crate-swapper-0.0.1 (c (n "swapper") (v "0.0.1") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1anib65af84j0kyhmvvfl1gx182vwx01bqxavc04gj6kmxjdv4sj")))

(define-public crate-swapper-0.0.2 (c (n "swapper") (v "0.0.2") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1i9cfv58w4isgh708dyc6x0hfnky1bmwvn3qgw2h2qrg2lw4svjz")))

(define-public crate-swapper-0.0.3 (c (n "swapper") (v "0.0.3") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "16c18s2z0y6n8v1vxjfsnm01jlvqfwp0nqa74iis9n3vjfvi6kqs")))

(define-public crate-swapper-0.0.4 (c (n "swapper") (v "0.0.4") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "133mrw6yx2rchx1x7594b2v71syz3qx0qj05yzkwbgxq5fri19iw")))

(define-public crate-swapper-0.1.0 (c (n "swapper") (v "0.1.0") (h "1ss5mg10fvhippcicpvqmvia3wxvfb87pdyxpw0d09smvd4d0m74")))

