(define-module (crates-io sw ap swap3) #:use-module (crates-io))

(define-public crate-swap3-0.1.0 (c (n "swap3") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1yg4m2z4vlgcxgkygqd5anvv6gafrwadhxvyyn0sv7p64zzlbmxf") (f (quote (("unsafe"))))))

(define-public crate-swap3-0.2.0 (c (n "swap3") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ss22316ca0xyl1xpkij5km3mq66xy44xwspza8aylckvx80lh0x") (f (quote (("unsafe"))))))

(define-public crate-swap3-0.2.1 (c (n "swap3") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mq71yq6zh9ix5g21frhff2np3yz4pgffrg41vlibwi0vngnzpm2") (f (quote (("unsafe"))))))

