(define-module (crates-io sw ap swap_channel) #:use-module (crates-io))

(define-public crate-swap_channel-0.0.1 (c (n "swap_channel") (v "0.0.1") (h "0ki8p4zylpk10p1z4qpl9fg9prlaya8m0qwncs3hs9imrq4k4m2p")))

(define-public crate-swap_channel-0.1.0 (c (n "swap_channel") (v "0.1.0") (h "0845k8h9b9y4qqhvq732m61dh9jidsqm7bpw03i6kwm28wyadvix")))

(define-public crate-swap_channel-0.2.0 (c (n "swap_channel") (v "0.2.0") (h "1dsg52d9h33jcbg48zdvqqbssnq0n7x8m591v0f48bqcabrc3k4n")))

(define-public crate-swap_channel-0.2.1 (c (n "swap_channel") (v "0.2.1") (h "1rs98gdaaqhzdxixvxnvjjnwinksym029hgw268vqcc0gxpqq4zp")))

(define-public crate-swap_channel-0.2.2 (c (n "swap_channel") (v "0.2.2") (h "1x6f77wx0rhsxi9qw1hm5p8hpv973dbyy8q6wx108d5lkn0wkngr")))

(define-public crate-swap_channel-0.2.3 (c (n "swap_channel") (v "0.2.3") (h "1rxrybzfqlh3ijpmzsa1vl0m8q523j4p2wy0l7p57myqvbzlzc2v")))

(define-public crate-swap_channel-0.2.4 (c (n "swap_channel") (v "0.2.4") (h "1h5r7f3r7pd6v65xp5dpf7k2ir0vy36l2pmz7d6gqqfc20n60igk")))

(define-public crate-swap_channel-0.2.5 (c (n "swap_channel") (v "0.2.5") (h "11dhkiynsdph2hhfkrwxq11637hr9dnk8a63k650iqihkn42s2h6")))

(define-public crate-swap_channel-0.2.6 (c (n "swap_channel") (v "0.2.6") (h "11ndcmjg9z1c2flwpzqcj4wr9n3hzbarqvixdd93bik14s4mym73")))

(define-public crate-swap_channel-0.2.7 (c (n "swap_channel") (v "0.2.7") (h "09sa1qib15nsii1yk666vhf6f2f0p8qh4npn6hp198plclml79n1")))

(define-public crate-swap_channel-0.2.8 (c (n "swap_channel") (v "0.2.8") (h "1j01jgmrdchbv1862i8d254ffif3nnb2ak7p8za6i4ax7bxlb9iw")))

(define-public crate-swap_channel-0.3.0 (c (n "swap_channel") (v "0.3.0") (h "1k3b2ways3fcaq7fhhkyms3pji8x627zllj3jwraxlkgpdbzfhhb")))

