(define-module (crates-io sw ap swaperooni) #:use-module (crates-io))

(define-public crate-swaperooni-0.1.0 (c (n "swaperooni") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mi9v98dz36rsjg8gq7pm7fvy3x22hhg156rvnca3wslcycknnrx")))

