(define-module (crates-io sw ap swapvec) #:use-module (crates-io))

(define-public crate-swapvec-0.1.0 (c (n "swapvec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "001cz7xa15avjdbsm537lbj49558my7cc5fjmrarypdsm9w9cs2i")))

(define-public crate-swapvec-0.1.1 (c (n "swapvec") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "13mf3gcayj0126yl5kh0xjf02m9ljx1prvppc2dnvpdrnc630l9q")))

(define-public crate-swapvec-0.1.2 (c (n "swapvec") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0haahkisdffggsl28bmki424rwnkds7dr3bmg36ss9v7qn9cv0rq")))

(define-public crate-swapvec-0.1.3 (c (n "swapvec") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "151gxxlq4ryjddfia1qza3gbqbh25m21hvnh690fr2hbaq2malnx")))

(define-public crate-swapvec-0.2.0 (c (n "swapvec") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0jdw9zd2fv6vdrx274jng98hplj3i1bzsdcdwnndvq95j1sjwv9z")))

(define-public crate-swapvec-0.3.0 (c (n "swapvec") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "00kxg54hzlaz4w1ddsyar2bf635h23pdq5cggkny53r9f998jpzc")))

(define-public crate-swapvec-0.4.0 (c (n "swapvec") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "12fn33f0g346g2bi087j4j3i9c59j5bd3f5v30mylsks52v08mpw")))

(define-public crate-swapvec-0.4.1 (c (n "swapvec") (v "0.4.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "054fnkgwjx1mkficc172wi9gzhn1fwlykf1hv1x1dvzjjjjfq6i9")))

(define-public crate-swapvec-0.4.2 (c (n "swapvec") (v "0.4.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "10xg016lshdza0paa8synlgr8mcxrfh2147gsrw7kl0dz6djlaz6")))

