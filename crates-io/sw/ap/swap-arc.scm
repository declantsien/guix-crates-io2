(define-module (crates-io sw ap swap-arc) #:use-module (crates-io))

(define-public crate-swap-arc-0.1.0 (c (n "swap-arc") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.14") (d #t) (k 0)) (d (n "likely_stable") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.5") (d #t) (k 0)) (d (n "arc-swap") (r "^1.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0101dwzh3kc2dnla41ihikh2ph8pgbfqfq28vyl64n4sm7na2pzj") (f (quote (("ptr-ops"))))))

(define-public crate-swap-arc-0.1.1 (c (n "swap-arc") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.14") (d #t) (k 0)) (d (n "likely_stable") (r "^0.1.2") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)) (d (n "arc-swap") (r "^1.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18imfzgp3c35dn3ig5rs8h7w15cb6gf8ygx8gx4z6c3jl92nsw2n") (f (quote (("ptr-ops"))))))

