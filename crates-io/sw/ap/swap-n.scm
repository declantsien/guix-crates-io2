(define-module (crates-io sw ap swap-n) #:use-module (crates-io))

(define-public crate-swap-n-0.1.0 (c (n "swap-n") (v "0.1.0") (h "0yrfd85r2pj22svv751bipcv78bj0bgwz5sjni89svvc95b5cv85")))

(define-public crate-swap-n-0.1.1 (c (n "swap-n") (v "0.1.1") (h "0bran79b78h97bz92yy2gc46dg2k37i519mdga3mj9091hzfb8lk")))

