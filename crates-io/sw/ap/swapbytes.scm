(define-module (crates-io sw ap swapbytes) #:use-module (crates-io))

(define-public crate-swapbytes-0.1.0 (c (n "swapbytes") (v "0.1.0") (d (list (d (n "swapbytes-derive") (r "^0.1") (d #t) (k 0)))) (h "13hdcb6skgr4mcya7k8j0492a6wnzzjbnn637qfswpjdi01kjgv7")))

(define-public crate-swapbytes-0.1.1 (c (n "swapbytes") (v "0.1.1") (d (list (d (n "swapbytes-derive") (r "^0.1") (d #t) (k 0)))) (h "106v3hvb9j2crj2jjigvrapp347nir8fw1cvll5xc4mpqic34pgf")))

(define-public crate-swapbytes-0.2.0 (c (n "swapbytes") (v "0.2.0") (d (list (d (n "swapbytes-derive") (r "^0.2") (d #t) (k 0)))) (h "1w7mcy24lrhxjwxwishphgfld8yr99gz0mxq25qb8w1dsnqhy0cw")))

(define-public crate-swapbytes-0.2.1 (c (n "swapbytes") (v "0.2.1") (d (list (d (n "swapbytes-derive") (r "^0.2") (d #t) (k 0)))) (h "1zg4cmm4bfnzqkv7ffbcvl6gas02y6j2nfdda1a5hzmz65gp79rc")))

