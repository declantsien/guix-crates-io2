(define-module (crates-io sw ap swap-queue) #:use-module (crates-io))

(define-public crate-swap-queue-0.0.1-beta.1 (c (n "swap-queue") (v "0.0.1-beta.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.1") (d #t) (t "cfg(loom)") (k 0)))) (h "0n6ll80kp4m0zcad30b4bsy26rf081217dsp0ksidp80913v83x2") (y #t)))

(define-public crate-swap-queue-0.0.2 (c (n "swap-queue") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.1") (d #t) (t "cfg(loom)") (k 0)))) (h "0adclv7sdp3df9gms9ba8cd3y08diyj7ymrqp8zmkd47pi79j7vv") (y #t)))

(define-public crate-swap-queue-0.0.3-beta.1 (c (n "swap-queue") (v "0.0.3-beta.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.1") (d #t) (t "cfg(loom)") (k 0)))) (h "1wck5xiq955apnc19m3r0bg6ml19q20yif3f8zjyvii9a7gslq4v") (y #t)))

(define-public crate-swap-queue-0.0.3 (c (n "swap-queue") (v "0.0.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.1") (d #t) (t "cfg(loom)") (k 0)))) (h "1kvqb098bz6dn20cfxxvrpw28gvyhmw5gx8l5wd4xrrx87pc22cn") (y #t)))

(define-public crate-swap-queue-0.0.4-beta.1 (c (n "swap-queue") (v "0.0.4-beta.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.1") (d #t) (t "cfg(loom)") (k 0)))) (h "1hxi37lklk0wg64iwrcz3iy75h1xlxy45ggpnd2c3jx6386ykka4") (y #t)))

(define-public crate-swap-queue-0.0.4-beta.2 (c (n "swap-queue") (v "0.0.4-beta.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "oneshot") (r "^0.1.2") (d #t) (k 0)))) (h "1dy9rb4qbvbybndns7gzw84pavndkwz5d2z614kpv2b1h97gp7b9")))

(define-public crate-swap-queue-0.0.4 (c (n "swap-queue") (v "0.0.4") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)))) (h "0pdb8man1vvrb3ngz4frzv89dysmqrw8b96sfnx7d2541w0q7c9d")))

(define-public crate-swap-queue-0.0.5 (c (n "swap-queue") (v "0.0.5") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "1clxzraaqvkc6kqsz73cd12hy4fsf414fn0ays49ajsa6d3i50az")))

(define-public crate-swap-queue-0.0.6 (c (n "swap-queue") (v "0.0.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "0j0j6dcj9ddj5r4nsavhva7xdkrw30yh33wa1a4lmsp2ads6lg4d")))

(define-public crate-swap-queue-1.0.0 (c (n "swap-queue") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "flume") (r "^0.10.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "0jhl464qhfiyixa7cnz17mvwnckgxgpzn0fqx3i1lkccxjcpjmip")))

(define-public crate-swap-queue-1.1.0 (c (n "swap-queue") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("async_tokio" "html_reports"))) (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "flume") (r "^0.10.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "1zm08ln4z4h4bsxyiac72h57pi344d9w7i5wrgms9kydadwrf1k0")))

