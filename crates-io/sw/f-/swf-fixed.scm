(define-module (crates-io sw f- swf-fixed) #:use-module (crates-io))

(define-public crate-swf-fixed-0.1.0 (c (n "swf-fixed") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 2)))) (h "1cg0s7nqn1i646j5kshwwxmnyb2wx61daqz9v60lhkrc375fy5d4")))

(define-public crate-swf-fixed-0.1.1 (c (n "swf-fixed") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "04gr83959z42sm5c570af0lifm74hzhhqna75prgr1xq7j53z3zm")))

(define-public crate-swf-fixed-0.1.2 (c (n "swf-fixed") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1v062dkxvq6mp1xrxmpvww808p4mng1fj6a9l3j4gz9n352z0scd")))

(define-public crate-swf-fixed-0.1.3 (c (n "swf-fixed") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0w0b0vw96xr4gr5mhgbj0z5rnvna8jxsjk28hbzav0848xadwy7w")))

(define-public crate-swf-fixed-0.1.4 (c (n "swf-fixed") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0hgrf3paknffv76clmys87sblihacd9qnmkrw32l31pi0xc5fg5z")))

(define-public crate-swf-fixed-0.1.5 (c (n "swf-fixed") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "00p3a7x84fps438sbw7h82qg150wkxyqs2d48b285cshvwh2q8bb") (f (quote (("default" "serde"))))))

