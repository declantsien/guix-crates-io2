(define-module (crates-io sw f- swf-headers) #:use-module (crates-io))

(define-public crate-swf-headers-0.1.1 (c (n "swf-headers") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "flate2") (r "^0.2.7") (d #t) (k 0)) (d (n "lzma") (r "^0.2.1") (d #t) (k 0)))) (h "0ddyrr0p2lk5m7m2agwc6i5nbm3vcminnvfmf3r8q95yf9fx1f1b")))

(define-public crate-swf-headers-0.1.2 (c (n "swf-headers") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "flate2") (r "^0.2.7") (d #t) (k 0)) (d (n "lzma") (r "^0.2.1") (d #t) (k 0)))) (h "0cjra0ac5231dpvdisb8hzxq3d8kl7q7hsjcml5d630k0dxzvj6f")))

(define-public crate-swf-headers-0.2.0 (c (n "swf-headers") (v "0.2.0") (d (list (d (n "bit_range") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "flate2") (r "^0.2.7") (d #t) (k 0)) (d (n "lzma") (r "^0.2.1") (d #t) (k 0)))) (h "1h208yw3pl89h6zy2qss6lcpmn7bk7ds2y1dlngc26dxgxriwij9")))

