(define-module (crates-io sw ah swahili-dsl) #:use-module (crates-io))

(define-public crate-swahili-dsl-0.1.0 (c (n "swahili-dsl") (v "0.1.0") (h "1ygbbw0qa10wzakih6glipn2v8af4zm0z68lpkvlsv26kb16zln2")))

(define-public crate-swahili-dsl-0.1.1 (c (n "swahili-dsl") (v "0.1.1") (h "0dsvig9cmfxyq27qlvbwy3bshcapqwjr0j4050bday0lkb5mn2p5")))

(define-public crate-swahili-dsl-0.1.2 (c (n "swahili-dsl") (v "0.1.2") (h "0fsqfl2dil9sbrdh4fwg4gmvj0x5wqzghqiba1jl17gcqi9vl3zy")))

(define-public crate-swahili-dsl-0.1.3 (c (n "swahili-dsl") (v "0.1.3") (h "1c0jxkjb3vadg5m93xkl8yy13zlrgcc6i47hinkfmnvh370yn9cx")))

(define-public crate-swahili-dsl-0.1.4 (c (n "swahili-dsl") (v "0.1.4") (h "1bvpr4h9i18j1gyxbv0kfsbq0l40f9kqccfvq3shbjmwz0fam7vn")))

(define-public crate-swahili-dsl-0.1.5 (c (n "swahili-dsl") (v "0.1.5") (h "1ibx455d743yzv56zd3h6q8swi5c0b9hgngrd2y7x79cfqykm6pm")))

(define-public crate-swahili-dsl-0.1.6 (c (n "swahili-dsl") (v "0.1.6") (h "0127vrahj3m562jxazqs0cqln4vhkbj3896l8khjkbrwhm0l6r5x")))

(define-public crate-swahili-dsl-0.1.7 (c (n "swahili-dsl") (v "0.1.7") (h "01rzbhyn2gwajgdsmzmqlr2v0w3cavd0vqsdripj3l2zazjmqsww")))

(define-public crate-swahili-dsl-0.1.8 (c (n "swahili-dsl") (v "0.1.8") (h "0wahrxra5w8g1y8bp1xrnxcbc3bnyn9sani1934f88afcwjldvdl")))

