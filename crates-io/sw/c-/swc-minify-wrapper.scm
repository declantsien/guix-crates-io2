(define-module (crates-io sw c- swc-minify-wrapper) #:use-module (crates-io))

(define-public crate-swc-minify-wrapper-0.1.0 (c (n "swc-minify-wrapper") (v "0.1.0") (d (list (d (n "swc") (r "^0.273") (d #t) (k 0)) (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.112") (d #t) (k 0)))) (h "0sp4x23b9s860lg2v2i0b23k3jbb9vjsgf7fr5g1dxmjbqhypahn") (y #t)))

(define-public crate-swc-minify-wrapper-0.1.1 (c (n "swc-minify-wrapper") (v "0.1.1") (d (list (d (n "swc") (r "^0.273") (d #t) (k 0)) (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.112") (d #t) (k 0)))) (h "1iwfrypqn1zw8xia7g8qsj5qsg5x4rifaz6y25z9kx7clapa852q") (y #t)))

