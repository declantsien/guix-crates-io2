(define-module (crates-io sw c- swc-minify) #:use-module (crates-io))

(define-public crate-swc-minify-0.1.0 (c (n "swc-minify") (v "0.1.0") (d (list (d (n "swc") (r "^0.273") (d #t) (k 0)) (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.112") (d #t) (k 0)))) (h "1p11sz26xh4r1z5xhryn20ikaz3bpddj9fplrp5kv4c4rvqh83gd")))

(define-public crate-swc-minify-0.1.1 (c (n "swc-minify") (v "0.1.1") (d (list (d (n "swc") (r "^0.273") (d #t) (k 0)) (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.112") (d #t) (k 0)))) (h "1650am8qik5y0b2mkg02vbm2pyp704wfbzlil8sqww3q46wr0wxp")))

