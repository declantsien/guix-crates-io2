(define-module (crates-io sw c- swc-helper-module-import) #:use-module (crates-io))

(define-public crate-swc-helper-module-import-0.1.0 (c (n "swc-helper-module-import") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "swc_core") (r "^0.52") (f (quote ("ecma_plugin_transform" "ecma_utils"))) (d #t) (k 0)) (d (n "testing") (r "^0.31.27") (d #t) (k 2)))) (h "1yk8wf2q40y66aabmynza2knhy5avdji3rv1k4rrha792qsm6m7b")))

(define-public crate-swc-helper-module-import-0.1.1 (c (n "swc-helper-module-import") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "swc_core") (r "^0.52") (f (quote ("ecma_plugin_transform" "ecma_utils"))) (d #t) (k 0)) (d (n "testing") (r "^0.31.28") (d #t) (k 2)))) (h "1a3s0ypa0dpqgcrjjzmdcgk17k4c95dn79f2829xrfqjirldkdq8")))

