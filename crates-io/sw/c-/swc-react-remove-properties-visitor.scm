(define-module (crates-io sw c- swc-react-remove-properties-visitor) #:use-module (crates-io))

(define-public crate-swc-react-remove-properties-visitor-0.0.0 (c (n "swc-react-remove-properties-visitor") (v "0.0.0") (d (list (d (n "matchable") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "swc_core") (r "^0.48") (f (quote ("ecma_ast" "ecma_plugin_transform" "ecma_utils"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.123") (d #t) (k 2)))) (h "0rmk9hzjbf786xb238ixc1y3wivqyrk4wdhgbi9jkj8716q588w4")))

(define-public crate-swc-react-remove-properties-visitor-0.1.2 (c (n "swc-react-remove-properties-visitor") (v "0.1.2") (d (list (d (n "matchable") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "swc_core") (r "^0.75") (f (quote ("ecma_ast" "ecma_plugin_transform" "ecma_utils"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.133") (d #t) (k 2)))) (h "1wf2k2p1kapp0vad1rg06ry02qfaary5adggjbxs3x2w05p7c2x1")))

(define-public crate-swc-react-remove-properties-visitor-0.1.4 (c (n "swc-react-remove-properties-visitor") (v "0.1.4") (d (list (d (n "matchable") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "swc_core") (r "^0.75") (f (quote ("ecma_ast" "ecma_plugin_transform" "ecma_utils"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.133") (d #t) (k 2)))) (h "11p5yp3apaznh6k7rbcm4s81bp27x6hncsc4viqlql4zkicjir6c")))

