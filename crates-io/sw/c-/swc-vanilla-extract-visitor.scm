(define-module (crates-io sw c- swc-vanilla-extract-visitor) #:use-module (crates-io))

(define-public crate-swc-vanilla-extract-visitor-0.0.1 (c (n "swc-vanilla-extract-visitor") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "swc_core") (r "^0.23.13") (f (quote ("common" "ecma_quote" "ecma_ast" "ecma_visit" "ecma_visit_path"))) (d #t) (k 0)))) (h "06xnfpqlkbhvjp25q2igncvkj6rg9xjj9wri7gxhpn044wfvafdm")))

(define-public crate-swc-vanilla-extract-visitor-0.0.2 (c (n "swc-vanilla-extract-visitor") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "swc_core") (r "^0.43.2") (f (quote ("common" "ecma_quote" "ecma_ast" "ecma_visit" "ecma_visit_path"))) (d #t) (k 0)))) (h "0nxxcyqlasrcc0mcw8x1bnvyzq2xv2z0ijmbiiwvf3hmsah72004")))

