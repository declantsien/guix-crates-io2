(define-module (crates-io sw c- swc-plugin-graphql-codegen-client-preset-optimizer-test) #:use-module (crates-io))

(define-public crate-swc-plugin-graphql-codegen-client-preset-optimizer-test-0.1.0 (c (n "swc-plugin-graphql-codegen-client-preset-optimizer-test") (v "0.1.0") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "swc_core") (r "0.44.*") (f (quote ("plugin_transform" "ecma_utils" "common" "testing"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.123.11") (d #t) (k 0)))) (h "0pnmqbsva0shv82v3zwxgb21j5x81rrjinv2w85irhld5sxvs72x") (y #t)))

