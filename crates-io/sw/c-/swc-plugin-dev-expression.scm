(define-module (crates-io sw c- swc-plugin-dev-expression) #:use-module (crates-io))

(define-public crate-swc-plugin-dev-expression-0.2.0 (c (n "swc-plugin-dev-expression") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "0.44.*") (f (quote ("plugin_transform"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0579f9jn1071ihhyb7jj4vqxh9iq7j86zga04hbkyprl306cjl7g")))

(define-public crate-swc-plugin-dev-expression-0.2.1 (c (n "swc-plugin-dev-expression") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "0.44.*") (f (quote ("plugin_transform"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0abjzdl2fyxj189hzscnx7bnmz8xmjrkp8si9zn448qmnjvr8f8r")))

(define-public crate-swc-plugin-dev-expression-0.2.2 (c (n "swc-plugin-dev-expression") (v "0.2.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "0.50.*") (f (quote ("ecma_plugin_transform"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0vmnk5bwyii6api90y3yz06y51ymplpj5sh32jq2v2hlywswjpaf")))

(define-public crate-swc-plugin-dev-expression-0.2.10 (c (n "swc-plugin-dev-expression") (v "0.2.10") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "^0.59.4") (f (quote ("ecma_plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s0bl2hd9pimhrd4bkmml33ixmni9ljf8kj7j3sf1g4fdkfzlgs2")))

(define-public crate-swc-plugin-dev-expression-0.2.11 (c (n "swc-plugin-dev-expression") (v "0.2.11") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "^0.59.5") (f (quote ("ecma_plugin_transform" "ecma_utils" "ecma_visit" "ecma_ast" "common"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cdfnqw191ds35lmzi9ixwwg0gml36if0rr8a9iddhskqc1az0yf")))

