(define-module (crates-io sw c- swc-neuron) #:use-module (crates-io))

(define-public crate-swc-neuron-0.1.1 (c (n "swc-neuron") (v "0.1.1") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "cargo-release") (r ">=0.13.0, <0.14.0") (d #t) (k 2)) (d (n "csv") (r ">=1.1.3, <2.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.20, <2.0.0") (d #t) (k 0)))) (h "1jmkncbdk96h45x9mn0gr6wqipx743ng0zqdfknz25ac9ydv3lna") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

(define-public crate-swc-neuron-0.2.0 (c (n "swc-neuron") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "13qrpqwzl1pw3vdk4mvg334lk208z6fzzs1bykw205c35v7iyjfj") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

(define-public crate-swc-neuron-0.2.1 (c (n "swc-neuron") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0cw51q5r790ffyygxzw7j2s4h8nr3a96hlp2308hd6nd2g6nnazp") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

(define-public crate-swc-neuron-0.3.0 (c (n "swc-neuron") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0ppdpdpblrqvl67mv10npb7dypasgx3dnhxs8qb48qg5zqadpdqx") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

(define-public crate-swc-neuron-0.4.0 (c (n "swc-neuron") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "103q6jazygsrycq45a8k8h96zh2bxcf8x1c04k50qx6k0x2fdkb1") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

(define-public crate-swc-neuron-0.4.1 (c (n "swc-neuron") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0rz67p0q3rqj3k07qc3q1gy6p257x7rwy759vdpad8zzpc4zjj3h") (f (quote (("default") ("cli" "structopt" "anyhow"))))))

