(define-module (crates-io ih ex ihex_ext) #:use-module (crates-io))

(define-public crate-ihex_ext-1.0.0 (c (n "ihex_ext") (v "1.0.0") (d (list (d (n "ihex") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)))) (h "0zbixk97w0v17l12xda0pzskibwc7qj1bs2xpfqbkmbv7qzismwn")))

