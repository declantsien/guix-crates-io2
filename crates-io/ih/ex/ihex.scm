(define-module (crates-io ih ex ihex) #:use-module (crates-io))

(define-public crate-ihex-0.1.0 (c (n "ihex") (v "0.1.0") (h "0gmwg9169j4z0p1pzciw4grbcnx1knd5iraxk1n2s815w1wsm763")))

(define-public crate-ihex-0.1.1 (c (n "ihex") (v "0.1.1") (h "09a4by68x2c0l0gxmd7r97f78a9kpvlz7bxlpdfrvw0gy285zlvf")))

(define-public crate-ihex-0.1.2 (c (n "ihex") (v "0.1.2") (h "1w7z8d9zisqvz34j6jxwclx8khnyarlv9zys29a6ip2g0zwpwf93")))

(define-public crate-ihex-0.1.3 (c (n "ihex") (v "0.1.3") (h "0wbw3qgp9x5bzj8rkd21qc7fsykr3ik10h74mvly5fgz3sq50lfd")))

(define-public crate-ihex-1.0.0 (c (n "ihex") (v "1.0.0") (h "17s4jc55g5wm9zh1pw78dsp0mlsxny2jlnby4hyjikxrmzph02fd")))

(define-public crate-ihex-1.0.1 (c (n "ihex") (v "1.0.1") (h "0xgxxh8a0ki6d8xp75p38ixwzk3ivd1g7vhvhi9b15gmk0n5n2b7")))

(define-public crate-ihex-1.0.2 (c (n "ihex") (v "1.0.2") (h "0vpph6awp0p6hj8ffpvxlh2i1iwcr6zip9v3qc3w49vcpasph0qd")))

(define-public crate-ihex-1.1.0 (c (n "ihex") (v "1.1.0") (h "0adnvks74dsih0y8ai2i3dxs64yanqq0swsrzvnk980dympar3ld")))

(define-public crate-ihex-1.1.1 (c (n "ihex") (v "1.1.1") (h "11q5cwwysb871hyvz194b14avx01iy9hcx8s7zh6rf1aw5gps1p5")))

(define-public crate-ihex-1.1.2 (c (n "ihex") (v "1.1.2") (h "1mc9r39ycxlldnpw125sv3izcffwzxzpl9msayrxzyrsphgyr54k")))

(define-public crate-ihex-2.0.0 (c (n "ihex") (v "2.0.0") (h "1fn32hryvg8lw0kjpq4zd55dzdmr35q7jcswx1xhlwv6632wr6sk") (y #t)))

(define-public crate-ihex-3.0.0 (c (n "ihex") (v "3.0.0") (h "1wlzfyy5fsqgpki5vdapw0jjczqdm6813fgd3661wf5vfi3phnin")))

