(define-module (crates-io ih ex ihex-merge) #:use-module (crates-io))

(define-public crate-ihex-merge-0.1.0 (c (n "ihex-merge") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ihex") (r "^3.0.0") (d #t) (k 0)))) (h "17ig586awpxsvlyyhflmwsmciq1zrinnzrydi76195x2crpcr1b1")))

