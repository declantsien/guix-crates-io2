(define-module (crates-io ih at ihateintegrals) #:use-module (crates-io))

(define-public crate-ihateintegrals-0.1.0 (c (n "ihateintegrals") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jsqh4kh5yrgnm2xwd8vvijljjhpb8syj0mhln93dj78bsfmfbcw")))

(define-public crate-ihateintegrals-0.1.1 (c (n "ihateintegrals") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1455qayj4klxn6dyfbnbgjd9d8fk2b0fs6j3lrbhf1x5jzm2c07r")))

(define-public crate-ihateintegrals-0.1.2 (c (n "ihateintegrals") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15iczagyfg5fq5a829nh6g7l72p70rxw71d1zc04a41s0m8hgiwp")))

