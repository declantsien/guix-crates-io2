(define-module (crates-io ih ls ihlsh) #:use-module (crates-io))

(define-public crate-ihlsh-0.0.1 (c (n "ihlsh") (v "0.0.1") (d (list (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0711d8mri6a3mjsvp02zy783jgj0q3hv7nqvz5vpvrq7qgmk9zmp") (y #t)))

(define-public crate-ihlsh-0.0.2 (c (n "ihlsh") (v "0.0.2") (d (list (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0mxl0vim7jnxh6wzg2ksx71r47xsf137g8njdb50nslg46yf1081") (y #t)))

