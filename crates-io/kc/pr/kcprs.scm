(define-module (crates-io kc pr kcprs) #:use-module (crates-io))

(define-public crate-kcprs-0.4.12 (c (n "kcprs") (v "0.4.12") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "0q14zl9ypki8f9wpqk5nvrz0z3fi2z488f7bww6acw7kbwny0w0a")))

(define-public crate-kcprs-0.4.13 (c (n "kcprs") (v "0.4.13") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "03pjvpfxhp5yjdfgr915khnwj9w0xxzr8kvksids9q7qhpsmv2wc")))

(define-public crate-kcprs-0.5.0 (c (n "kcprs") (v "0.5.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "1fg6fzb83znln22fvri48ws2rrzksb069c60vbivycmyc4vwrj11")))

