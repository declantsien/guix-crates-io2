(define-module (crates-io kc on kconfig-parser) #:use-module (crates-io))

(define-public crate-kconfig-parser-0.0.1 (c (n "kconfig-parser") (v "0.0.1") (h "0cld5hy2mvdpwkr8vwgnzxij6l4djw0acwb3vs8li1ix8jjdqyf9")))

(define-public crate-kconfig-parser-0.0.2 (c (n "kconfig-parser") (v "0.0.2") (h "04zl824hac0gd7y0mv9ld92fjg20b241ni66d25swiljlq6073r2")))

(define-public crate-kconfig-parser-0.0.3 (c (n "kconfig-parser") (v "0.0.3") (h "05bjh3cqxw8v7r1yv2zcv7dkqrdd00i1rrmk4zfy7fflrfaq8rll")))

(define-public crate-kconfig-parser-0.0.4 (c (n "kconfig-parser") (v "0.0.4") (h "1wc1m8qr66cm3nrn7nnmzxlh7i1wv1gy51mxkd7q9zwq9wkyqjjw")))

(define-public crate-kconfig-parser-0.0.5 (c (n "kconfig-parser") (v "0.0.5") (h "12x76wmz3y7ca08215n2jj8h94a0z13cspk11h84kpbs42v0hwm0")))

(define-public crate-kconfig-parser-0.1.0 (c (n "kconfig-parser") (v "0.1.0") (h "1r6psccdqv2flcb4n0wfnadx2kzzdamxqbwdjj5cswd834mfchan")))

(define-public crate-kconfig-parser-0.1.1 (c (n "kconfig-parser") (v "0.1.1") (h "1amp7p8cw0cb9wvvw1bzavsw1anjf9w54bi99cs30zs7vi759mcf")))

