(define-module (crates-io kc on kconfig-linux) #:use-module (crates-io))

(define-public crate-kconfig-linux-0.1.0 (c (n "kconfig-linux") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0liprj1nfsvfln5lf109lycbmixs0j7z727627ldqxhh09vwwdf4")))

(define-public crate-kconfig-linux-0.1.1 (c (n "kconfig-linux") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0l1zjhqzpbzfsclrnmy28s49djfazqafga3hzjdz2hdn17i87wr8")))

