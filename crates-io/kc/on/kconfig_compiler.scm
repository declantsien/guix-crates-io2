(define-module (crates-io kc on kconfig_compiler) #:use-module (crates-io))

(define-public crate-kconfig_compiler-0.0.1 (c (n "kconfig_compiler") (v "0.0.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s2f1gyh1wh9kand2x54scf7dg3fn0ps61yql37rlf151zmnxdbg")))

