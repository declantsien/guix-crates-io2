(define-module (crates-io kc ap kcapi-sys) #:use-module (crates-io))

(define-public crate-kcapi-sys-0.0.1 (c (n "kcapi-sys") (v "0.0.1") (h "0qijb8aqsah01lq2grbpln9vr0ar1h8cmbvpm00q9fqy32s41gh7")))

(define-public crate-kcapi-sys-0.0.2 (c (n "kcapi-sys") (v "0.0.2") (h "1rsri08y3cs1zva8aw9mlam7jlrfg3wh7xpkl2bnx2ip4qm2jscm")))

(define-public crate-kcapi-sys-0.0.3 (c (n "kcapi-sys") (v "0.0.3") (h "0idiah13h7s2cd4f2n4fqh37hacg4hr2icz2n3jxsd9m64cir171")))

(define-public crate-kcapi-sys-1.1.0 (c (n "kcapi-sys") (v "1.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0gyzz827wfj8gkmjmwk612agmpx85gh0p4lgmbj9qmncycv0g3xb")))

(define-public crate-kcapi-sys-1.2.0 (c (n "kcapi-sys") (v "1.2.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "01lyffi20f1f447s43gl8rr5rh16gbcgcvpai316c0j98rwx42zy")))

(define-public crate-kcapi-sys-1.3.0 (c (n "kcapi-sys") (v "1.3.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "06qs2n22jr1fm18kq8cjg43cwsgyrnd22s4fp4m89xhrg0sz67kk")))

(define-public crate-kcapi-sys-1.4.0 (c (n "kcapi-sys") (v "1.4.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0qa97jmqzagpc5rdvq4pvp4rvvjmzz2rlz8ax2w21fjvj2aa1zwy") (f (quote (("vendored-kcapi") ("local-kcapi") ("default" "vendored-kcapi"))))))

(define-public crate-kcapi-sys-1.4.1 (c (n "kcapi-sys") (v "1.4.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0xxszvdd1kjmfa9j18cgscq7v71zxr6fyj1d77hw81jf997faicg") (f (quote (("vendored-kcapi") ("local-kcapi") ("default" "vendored-kcapi"))))))

