(define-module (crates-io kc ap kcapi) #:use-module (crates-io))

(define-public crate-kcapi-0.1.0 (c (n "kcapi") (v "0.1.0") (d (list (d (n "kcapi-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "06nkxij5p2snk4lnf3476q93dx1klik5vqx9vhgwv30prf90wqy5")))

(define-public crate-kcapi-0.1.1 (c (n "kcapi") (v "0.1.1") (d (list (d (n "kcapi-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "0kpp54w13acf33kpj20ms820i7z03234vbs50f3b9prbmj0p3bzc")))

(define-public crate-kcapi-0.1.2 (c (n "kcapi") (v "0.1.2") (d (list (d (n "kcapi-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "0c0ipqc868absid4ivqw6pqn3pavx221ajy5ch1rqbcz2fkhi8ns")))

(define-public crate-kcapi-0.1.3 (c (n "kcapi") (v "0.1.3") (d (list (d (n "kcapi-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "09wlj7m4gxsmxn53iy66wg9ajir3ia03rwqx62wcamqjasgg5ah3")))

(define-public crate-kcapi-0.1.4 (c (n "kcapi") (v "0.1.4") (d (list (d (n "kcapi-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "07jfm6qj2d607y9w4cipmmy9mn4jhszlg4vfdjrsczr5mwwr24zq")))

(define-public crate-kcapi-0.1.5 (c (n "kcapi") (v "0.1.5") (d (list (d (n "kcapi-sys") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "06ahwwyasd7iskwhnag89f9f8kad79bmyh9i6glc70ic5c0x7xc1")))

(define-public crate-kcapi-0.1.6 (c (n "kcapi") (v "0.1.6") (d (list (d (n "kcapi-sys") (r "^1.4.1") (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)))) (h "15pkhb80pajsmpj9f1rmp2whdns92rsr07gsizqsp3zs40xr4455") (f (quote (("vendored-kcapi" "kcapi-sys/vendored-kcapi") ("local-kcapi" "kcapi-sys/local-kcapi") ("default" "vendored-kcapi" "asym") ("asym"))))))

