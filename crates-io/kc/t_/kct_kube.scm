(define-module (crates-io kc t_ kct_kube) #:use-module (crates-io))

(define-public crate-kct_kube-0.2.0 (c (n "kct_kube") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.4.0") (d #t) (k 0)))) (h "09j50ccdvh1y0na3j1nl6b2838lr5cikk78c3kgv76in13ch2f1n")))

(define-public crate-kct_kube-0.3.0 (c (n "kct_kube") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.4.0") (d #t) (k 0)))) (h "1and6ilw7cjcgnydmkqyk00558wdm0pblbj1ys733j99gr8gabch") (y #t)))

(define-public crate-kct_kube-0.3.1 (c (n "kct_kube") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.4.0") (d #t) (k 0)))) (h "0blbbx533g27vpl7ndssijyj0mq5122aal78aia6awb6iirjh0iz")))

(define-public crate-kct_kube-0.4.0 (c (n "kct_kube") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.4.0") (d #t) (k 0)))) (h "0phps1jyx5i9s0l2xr7bb7kk8qnyvvggy7vx2k2k00lwmy0cv0jc")))

