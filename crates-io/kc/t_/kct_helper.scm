(define-module (crates-io kc t_ kct_helper) #:use-module (crates-io))

(define-public crate-kct_helper-0.2.0 (c (n "kct_helper") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iqav8lq61657j01qglfscsi4255glkrzwj460apkg211yb3wlyb")))

(define-public crate-kct_helper-0.3.0 (c (n "kct_helper") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "179vkwd1cqc9mfr9q67cvy80hs8spf2fbjh27cibkjgqip20k281") (y #t)))

(define-public crate-kct_helper-0.3.1 (c (n "kct_helper") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wsydvjj3s8606fb8qfn3nixqhi659jrwivzkw7hvanxcp715xv6")))

(define-public crate-kct_helper-0.4.0 (c (n "kct_helper") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ypv25blml105g3ig3rm72178ga7yhk24lhhfclm071l69csimsl")))

