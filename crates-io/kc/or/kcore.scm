(define-module (crates-io kc or kcore) #:use-module (crates-io))

(define-public crate-kcore-0.0.1 (c (n "kcore") (v "0.0.1") (d (list (d (n "couchbase") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4" "serde" "use_std"))) (d #t) (k 0)))) (h "0kmjzysy0mdg0v20vgr93cp1l0cmabms8fyp8lb0apnb9h264x99")))

