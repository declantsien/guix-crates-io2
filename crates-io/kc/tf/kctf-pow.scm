(define-module (crates-io kc tf kctf-pow) #:use-module (crates-io))

(define-public crate-kctf-pow-1.0.0 (c (n "kctf-pow") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "1pf2qkf0xm3x6s7b3dp3s1smnx07ss38blahcmn6ac2sy12zjn7c")))

(define-public crate-kctf-pow-1.1.0 (c (n "kctf-pow") (v "1.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "12akr3zvkspqzqgbyjrdjh2vjbfsblqjqi9c3k5kr8zj7jcgzzlk")))

(define-public crate-kctf-pow-1.1.1 (c (n "kctf-pow") (v "1.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "10cmzkabv18949lmv0y0r027wxq324xy7f50dzsgynxl31mfg22l")))

(define-public crate-kctf-pow-1.1.2 (c (n "kctf-pow") (v "1.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "11slqcpy6da779h3jd4jpadnrsr6k9nacfn6fb2wfkwmlcghachq")))

(define-public crate-kctf-pow-1.2.0 (c (n "kctf-pow") (v "1.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "15596ndhkk3dmmj3rf68cswhwpsxscbj0kxy93jq4frsprbdk9qi")))

