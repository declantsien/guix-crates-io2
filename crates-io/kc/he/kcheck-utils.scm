(define-module (crates-io kc he kcheck-utils) #:use-module (crates-io))

(define-public crate-kcheck-utils-0.1.2 (c (n "kcheck-utils") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "18hvg7n0zb5gaij9c86lx4r58bxikhggbr6z83dcvl65bkh14i4y")))

(define-public crate-kcheck-utils-0.1.3 (c (n "kcheck-utils") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "1yq26pyh0mv1g8b342cgarx84l359i8fk17qlwh46ic8amac662r")))

