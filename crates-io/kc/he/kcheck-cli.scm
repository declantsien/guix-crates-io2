(define-module (crates-io kc he kcheck-cli) #:use-module (crates-io))

(define-public crate-kcheck-cli-0.2.0 (c (n "kcheck-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "kcheck") (r "^0.2.0") (d #t) (k 0)))) (h "1x9nya6lck09bpclzcv2y1x5gsjlsmnjvi2m12f7n21dwy78qsy4")))

