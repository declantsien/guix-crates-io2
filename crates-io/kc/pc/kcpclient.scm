(define-module (crates-io kc pc kcpclient) #:use-module (crates-io))

(define-public crate-kcpclient-0.1.0 (c (n "kcpclient") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02hdky2nn81zwg7176497jcw7qbisl88p210kl6bwpd8i3zmaw2m")))

(define-public crate-kcpclient-0.1.1 (c (n "kcpclient") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h7pwfvf8ydx45qm6jgabjmq53w9q7263d2mxhzcwpn93746x35d")))

