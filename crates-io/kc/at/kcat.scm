(define-module (crates-io kc at kcat) #:use-module (crates-io))

(define-public crate-kcat-0.1.0 (c (n "kcat") (v "0.1.0") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "1vxl531kyp8xicfhl8mwv0dblkycc0jvq6l97padnrwr5hl8aa3x")))

(define-public crate-kcat-0.1.1 (c (n "kcat") (v "0.1.1") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "1k2hdyxi3x77z484l6z1kwkylam645ny800mvcrf8x7jhg86hwga")))

(define-public crate-kcat-0.1.2 (c (n "kcat") (v "0.1.2") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "1k20sndbm0ih6v8w0pi77z2md1blhl6arwy5diwn9sgn4cmkdrqp")))

(define-public crate-kcat-0.1.3 (c (n "kcat") (v "0.1.3") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "13hh3xy69biasdwhwlmlmrs8xrk7akgf0z7m04k0ikb6dgnn0dq6")))

(define-public crate-kcat-0.1.4 (c (n "kcat") (v "0.1.4") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "11xc48wij4w5z71s70ibpsxkqy58mwm5ydyqcwfhmm81iv1hn21y")))

(define-public crate-kcat-0.1.5 (c (n "kcat") (v "0.1.5") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "0vsmr56fwx98ndhyf5i91dvwz4yx7d8cx4xnrb7bakvliqjb2g3k")))

(define-public crate-kcat-0.1.6 (c (n "kcat") (v "0.1.6") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntect") (r "^2.0") (d #t) (k 0)))) (h "1gllainahrn0iz1x5jmclm4wv75x9wab62b3j8dmmcnrvmxmfrfn")))

