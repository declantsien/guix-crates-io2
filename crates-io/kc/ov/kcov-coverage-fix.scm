(define-module (crates-io kc ov kcov-coverage-fix) #:use-module (crates-io))

(define-public crate-kcov-coverage-fix-0.1.0 (c (n "kcov-coverage-fix") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "0x52ssaradcmdsqzyiffzxx7vrpkpp9xmmhcfxn7jaapkyhyw415") (y #t)))

