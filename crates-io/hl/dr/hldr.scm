(define-module (crates-io hl dr hldr) #:use-module (crates-io))

(define-public crate-hldr-0.3.0 (c (n "hldr") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "cargo" "derive" "suggestions"))) (k 0)) (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.3") (f (quote ("with-chrono-0_4"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0qxffja2rq978ia9zrppwi442n28pmc70qzj69gkr3wki7yvr06m")))

