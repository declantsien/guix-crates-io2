(define-module (crates-io hl de hldemo) #:use-module (crates-io))

(define-public crate-hldemo-0.1.0 (c (n "hldemo") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 2)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "0bc9q30px0649ipv967mwbq8d78dhya2ga612y9ybdkna5066mds")))

(define-public crate-hldemo-0.2.0 (c (n "hldemo") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "0s178i904i7y72kriqdpfc86zd3iypfj06a5wpd9xxim0z32jh2x")))

(define-public crate-hldemo-0.3.0 (c (n "hldemo") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "10c7ld5amvxlidzskrrsfyzm7xn11klan7m1cypf64c71s408lhc")))

