(define-module (crates-io hl ta hltas-framebulk-analyzer) #:use-module (crates-io))

(define-public crate-hltas-framebulk-analyzer-1.0.0 (c (n "hltas-framebulk-analyzer") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "hltas") (r "^0.5.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.23.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hvr8hjija3v2lwf1x763dzyivf97882kqcs2fj2d7wh8bfbykgp")))

