(define-module (crates-io hl ta hltas_cleaner) #:use-module (crates-io))

(define-public crate-hltas_cleaner-2.0.0 (c (n "hltas_cleaner") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hltas") (r "^0.5.0") (d #t) (k 0)))) (h "05ik1r2svjv7rz8zz0x7xm6m2d6dvxmcysx056nx4s88mwn2xnym")))

(define-public crate-hltas_cleaner-2.1.0 (c (n "hltas_cleaner") (v "2.1.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hltas") (r "^0.5.0") (d #t) (k 0)))) (h "0w8lyp0r9iwg9h2zfqg5vkw5msvsaviqx9jxvdgbxm57z2w4x7h9")))

