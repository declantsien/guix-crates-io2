(define-module (crates-io hl ta hltas) #:use-module (crates-io))

(define-public crate-hltas-0.1.0 (c (n "hltas") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0n3y5ji85iql59sx3irwkxsz9x3v1b2ymhyp41mlhzz2w6xmikqp")))

(define-public crate-hltas-0.2.0 (c (n "hltas") (v "0.2.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "04kpv63w5wkx76raib81jb3nzzr36c9ris7l9cwaabnf6pzx15w5")))

(define-public crate-hltas-0.3.0 (c (n "hltas") (v "0.3.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0r4lickq7ikzldyha43xn27xnk0s4b4jww7ni9jp36bdb9hmg11v")))

(define-public crate-hltas-0.4.0 (c (n "hltas") (v "0.4.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "00cc24pdz5dzdicixkjlc5jxld7kfxrcd1ngjaamwdal5rz8n693")))

(define-public crate-hltas-0.5.0 (c (n "hltas") (v "0.5.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "00s6z3x9sj3vqp0y79hdkhqlmv3cj1gv77r80wsbrksl00p5c73k")))

(define-public crate-hltas-0.6.0 (c (n "hltas") (v "0.6.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kcy1rihxyvgf937r1rrnyw7vn53izw072h9h89fm2lx2gxdc64n") (f (quote (("serde1" "serde") ("proptest1" "proptest" "proptest-derive") ("default"))))))

(define-public crate-hltas-0.7.0 (c (n "hltas") (v "0.7.0") (d (list (d (n "cookie-factory") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16f0bqicb5wj52gbc9gvvaw0swxnnngsk7aidb6fs12grk5rdjj5") (f (quote (("serde1" "serde") ("proptest1" "proptest" "proptest-derive") ("default"))))))

(define-public crate-hltas-0.8.0 (c (n "hltas") (v "0.8.0") (d (list (d (n "cookie-factory") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g406iyjslsjfxrw4idxw2ifbyml3s3bvcm79alhsf6513x2d0jy") (f (quote (("serde1" "serde") ("proptest1" "proptest" "proptest-derive") ("default"))))))

