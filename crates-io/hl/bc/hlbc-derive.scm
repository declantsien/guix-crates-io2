(define-module (crates-io hl bc hlbc-derive) #:use-module (crates-io))

(define-public crate-hlbc-derive-0.1.0 (c (n "hlbc-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g1jrsclxlgr7jgx894jwgn22mvy5vynbdzacw880l2g31rqsl3q")))

(define-public crate-hlbc-derive-0.1.1 (c (n "hlbc-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yhqd02iq1h3snidc06k2mvhzq67kdjrczp36xfnl7gdm3bvqvf8")))

(define-public crate-hlbc-derive-0.2.0 (c (n "hlbc-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16chi4qirxlw54jri7xa9iv722w56mngfdyjncljrsmknqw7qgrb")))

(define-public crate-hlbc-derive-0.3.0 (c (n "hlbc-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p2scbssp80h4lz0kzhygikw97ajcv7lawmybwzsrmd780h5s79d")))

(define-public crate-hlbc-derive-0.6.0 (c (n "hlbc-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ha4zrqwmnrdvm95sqlzxi57bk25vvsm6gfz763wcfdi9dcgr7xh")))

(define-public crate-hlbc-derive-0.6.1 (c (n "hlbc-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xiqdbmnxxcg2gqyzygx5734z1avymnprkbbnrwyaqmf0d5r3ym4")))

(define-public crate-hlbc-derive-0.7.0 (c (n "hlbc-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dlzlj2wjmvycc67q91fx378wr4r6siq3m20bab2ngb8kab0wkba")))

