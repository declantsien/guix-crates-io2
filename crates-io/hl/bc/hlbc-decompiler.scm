(define-module (crates-io hl bc hlbc-decompiler) #:use-module (crates-io))

(define-public crate-hlbc-decompiler-0.4.0 (c (n "hlbc-decompiler") (v "0.4.0") (d (list (d (n "fmtools") (r "^0.1") (d #t) (k 0)) (d (n "hlbc") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("graphmap"))) (o #t) (k 0)))) (h "1fizph2d4v7kr2954a9wcr17m881a7g4ifinia313vfj9y2dvlwy") (f (quote (("default") ("alt-graph" "alt" "petgraph") ("alt")))) (r "1.56")))

(define-public crate-hlbc-decompiler-0.5.0 (c (n "hlbc-decompiler") (v "0.5.0") (d (list (d (n "fmtools") (r "^0.1") (d #t) (k 0)) (d (n "hlbc") (r "^0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("graphmap"))) (o #t) (k 0)))) (h "0idg7nrv6m2z89l5wamb5agbdzgq47y8cvylrkw0ayh5g832fklc") (f (quote (("default") ("alt-graph" "alt" "petgraph") ("alt")))) (r "1.56")))

(define-public crate-hlbc-decompiler-0.6.0 (c (n "hlbc-decompiler") (v "0.6.0") (d (list (d (n "fmtools") (r "^0.1") (d #t) (k 0)) (d (n "hlbc") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("graphmap"))) (o #t) (k 0)))) (h "1m5z0wh9wg54ckhc9z7mwjwg384fbbrls30q71lndcwhcs71wl18") (f (quote (("default") ("alt-graph" "alt" "petgraph") ("alt")))) (r "1.56")))

(define-public crate-hlbc-decompiler-0.7.0 (c (n "hlbc-decompiler") (v "0.7.0") (d (list (d (n "fmtools") (r "^0.1") (d #t) (k 0)) (d (n "hlbc") (r "^0.7") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("graphmap"))) (o #t) (k 0)))) (h "0dcm4yh5zsfq9gly73ssinyllijpk16x1cns04pkffh52ml5jxd1") (f (quote (("default") ("alt-graph" "alt" "petgraph") ("alt")))) (r "1.64")))

