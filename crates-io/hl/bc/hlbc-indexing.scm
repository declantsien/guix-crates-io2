(define-module (crates-io hl bc hlbc-indexing) #:use-module (crates-io))

(define-public crate-hlbc-indexing-0.7.0 (c (n "hlbc-indexing") (v "0.7.0") (d (list (d (n "fuzzy-matcher") (r "^0.3") (d #t) (k 0)) (d (n "hlbc") (r "^0.7") (d #t) (k 0)) (d (n "sublime_fuzzy") (r "^0.7") (d #t) (k 0)) (d (n "tantivy") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1jgr84smk6ffvwnr7wch8r78snwzf7n07ly4b28r7m609qa7ldnl") (r "1.64")))

