(define-module (crates-io hl vm hlvm) #:use-module (crates-io))

(define-public crate-hlvm-0.4.1 (c (n "hlvm") (v "0.4.1") (d (list (d (n "hlvm_runtime") (r "^0.1.0") (d #t) (k 0)))) (h "1bn95m9rda9crjpam2ihl20704iw4ahhibn3gxv4vzpcpicljmz2")))

(define-public crate-hlvm-0.4.2 (c (n "hlvm") (v "0.4.2") (d (list (d (n "hlvm_runtime") (r "^0.1.5") (d #t) (k 0)))) (h "0jiv4r5acqd5zh9dc7mvazyafc92r4v1k8jkq26hx4dqz3sw9wj5")))

(define-public crate-hlvm-0.5.0 (c (n "hlvm") (v "0.5.0") (d (list (d (n "hlvm_runtime") (r "^0.2.0") (d #t) (k 0)))) (h "075wdl8vr98sqbk8mh9qwckds57mxxyyqx1dk1syhgg4bwi5fj87")))

(define-public crate-hlvm-0.5.1 (c (n "hlvm") (v "0.5.1") (d (list (d (n "hlvm_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "0mn5wakc77gcfyavv64x43n7chn7y9znjhi2fpqdmh7xv292gi5h")))

(define-public crate-hlvm-0.5.2 (c (n "hlvm") (v "0.5.2") (d (list (d (n "hlvm_runtime") (r "^0.2.2") (d #t) (k 0)))) (h "08ghf59cxmjhcnq0rd3qhpsaimqwlfqn7av1hj6xx2xc8pl45bl0")))

(define-public crate-hlvm-0.5.3 (c (n "hlvm") (v "0.5.3") (d (list (d (n "hlvm_runtime") (r "^0.2.2") (d #t) (k 0)))) (h "1h2ldq9l2r7k5gqhjxvkbb98p3f5aay53lqp2711n1i8f0pxcx95")))

(define-public crate-hlvm-0.6.0 (c (n "hlvm") (v "0.6.0") (d (list (d (n "hlvm_runtime") (r "^0.3.0") (d #t) (k 0)))) (h "0646m54vsbcrnv1c3mk7r7hmi3w7by3qv8mfv442xlkmf0056c1a")))

(define-public crate-hlvm-0.7.0 (c (n "hlvm") (v "0.7.0") (d (list (d (n "hlvm_runtime") (r "^0.4.0") (d #t) (k 0)))) (h "0rcna5dfvzvv5bxb7g0lisjy6xpz9rn708j5zq9sp2z1v4iqjh4s")))

(define-public crate-hlvm-0.7.1 (c (n "hlvm") (v "0.7.1") (d (list (d (n "hlvm_runtime") (r "^0.4.0") (d #t) (k 0)))) (h "1kzy5zf8mkvirs9p1a4xy7xkv9q0q1fymkbmz1jxhjw3ykz2h5vz")))

(define-public crate-hlvm-0.7.2 (c (n "hlvm") (v "0.7.2") (d (list (d (n "hlvm_runtime") (r "^0.4.1") (d #t) (k 0)))) (h "0p1ji71agzghr1kp5yys9mspwgwz81nc9l6mi7szachz44ai74fn")))

(define-public crate-hlvm-0.8.0 (c (n "hlvm") (v "0.8.0") (d (list (d (n "hlvm_runtime") (r "^0.5.0") (d #t) (k 0)))) (h "0n4nffwbfm0yi9njb27sx3g24nzyi00rrj1h3795w1hnkjdhb4dm")))

(define-public crate-hlvm-0.9.0 (c (n "hlvm") (v "0.9.0") (d (list (d (n "hlvm_runtime") (r "^0.6.0") (d #t) (k 0)))) (h "1l454wy1871ldznivz4pvr5zr51vm65jrhli6wgi0g3pz5xzrs21")))

(define-public crate-hlvm-0.10.0 (c (n "hlvm") (v "0.10.0") (d (list (d (n "hlvm_runtime") (r "^0.7.0") (d #t) (k 0)))) (h "0c3ila5zywwjq0avgk3s5z4q1q44a5i5156a2s433p07hgpj085q")))

