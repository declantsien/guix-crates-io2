(define-module (crates-io hl vm hlvm_runtime) #:use-module (crates-io))

(define-public crate-hlvm_runtime-0.1.1 (c (n "hlvm_runtime") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "1a1w54iwmfcv5wagppa0bwl0gmsdv6cngy9bxq29npaf472az8lr")))

(define-public crate-hlvm_runtime-0.1.2 (c (n "hlvm_runtime") (v "0.1.2") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "1k99qv47hcz982s6xq34pxr6cc0h0dj5h8kax1xsx8l556qkijna")))

(define-public crate-hlvm_runtime-0.1.3 (c (n "hlvm_runtime") (v "0.1.3") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "1wd94lxr7qbpazxsx4vkqwmw6lsb4hd0bq2xzk71dkibzasvm5lf")))

(define-public crate-hlvm_runtime-0.1.5 (c (n "hlvm_runtime") (v "0.1.5") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "05k8z3f1i0xdxgq20iwppd9j1alfgqbd7r6w92f8575k8wdx00rl")))

(define-public crate-hlvm_runtime-0.1.6 (c (n "hlvm_runtime") (v "0.1.6") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "01sy8spfr1p7zv1vp8306z3mnygq9a9ar33r86cmlcjz84bagdvv")))

(define-public crate-hlvm_runtime-0.2.0 (c (n "hlvm_runtime") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "16ggm03009ijvnqwm5qpkiwwd6dy6sqxpv0p1vqb00n6p5sx4sc8")))

(define-public crate-hlvm_runtime-0.2.1 (c (n "hlvm_runtime") (v "0.2.1") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "1ml0zf49yr1bhmib15n0g99v5ws5lfczhxhmqslkbvdrblnl6ra8")))

(define-public crate-hlvm_runtime-0.2.2 (c (n "hlvm_runtime") (v "0.2.2") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "19m7n6pq4fymvl0i7kcp40b1nv0gs7g6437lzwz4767frcg32d50")))

(define-public crate-hlvm_runtime-0.3.0 (c (n "hlvm_runtime") (v "0.3.0") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "0c1nx1dfz197fhyvzrq49p83nfa18f5gq2zp9pq71r00091pn33c")))

(define-public crate-hlvm_runtime-0.4.0 (c (n "hlvm_runtime") (v "0.4.0") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "00g5xaf6ahniiiafg8r0nqwzqb3rdhx3ci76nwjr237ncr4rhdbd")))

(define-public crate-hlvm_runtime-0.4.1 (c (n "hlvm_runtime") (v "0.4.1") (d (list (d (n "bigdecimal") (r "^0.0.15") (d #t) (k 0)))) (h "1w1fp6y47shn43kv1qw4aqv2r2d0wlh7b4kzs8ghb5fx9qf75js3")))

(define-public crate-hlvm_runtime-0.5.0 (c (n "hlvm_runtime") (v "0.5.0") (d (list (d (n "decimal") (r "^2.0.4") (d #t) (k 0)))) (h "0yvv05c1xq89r0sn53q20mfrn94bs31cvazmkzzg9vj52fvmvcyj")))

(define-public crate-hlvm_runtime-0.6.0 (c (n "hlvm_runtime") (v "0.6.0") (d (list (d (n "decimal") (r "^2.0.4") (d #t) (k 0)))) (h "1krlszd77a1r4qmn1l1j0lfqnq2rhz55ixf5879d8vw7fwn7ljxq")))

(define-public crate-hlvm_runtime-0.7.0 (c (n "hlvm_runtime") (v "0.7.0") (d (list (d (n "decimal") (r "^2.0.4") (d #t) (k 0)))) (h "0sfmd8dr0rah9vnskwc9i62q82lph3ihq758504qa4zkfcdkic84")))

