(define-module (crates-io hl ad hladikes_test_lib) #:use-module (crates-io))

(define-public crate-hladikes_test_lib-0.1.0 (c (n "hladikes_test_lib") (v "0.1.0") (h "005qq61ns6awcwnnhdc58cdg9bw5naj3d1692c5xv1lyx33v7jmh")))

(define-public crate-hladikes_test_lib-0.1.1 (c (n "hladikes_test_lib") (v "0.1.1") (h "00cjnwv57mc2jrm4n23d5j894p78z94pn5pbfp9g4lvm3ah86jc1")))

(define-public crate-hladikes_test_lib-0.1.2 (c (n "hladikes_test_lib") (v "0.1.2") (h "048g7iqxrdha8dbccsdh2qjiv4lwar9n8zkz25pgr8y0psbpdnbl")))

