(define-module (crates-io hl #{7-}# hl7-mllp-codec) #:use-module (crates-io))

(define-public crate-hl7-mllp-codec-0.0.1 (c (n "hl7-mllp-codec") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "0cfm3j1zdm94aiwlalbz87zk2bv48kcxrdivngqrsmv64yjkyh2w")))

(define-public crate-hl7-mllp-codec-0.0.2 (c (n "hl7-mllp-codec") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "08gj75plx4arbnmybg8qvl16hzj19xpccrblvkcxwr4q0ciqag1k")))

(define-public crate-hl7-mllp-codec-0.0.3 (c (n "hl7-mllp-codec") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "07129zxn0bq8f97cm6by3bjr88vpvbbxgh8bkqvkfc2x4pqrdd2v")))

(define-public crate-hl7-mllp-codec-0.0.4 (c (n "hl7-mllp-codec") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "08hxbh67ixzzc1acf0dnkmyxhbxp47xrhyvjpwmk7d0ch2bz41rr")))

(define-public crate-hl7-mllp-codec-0.0.5 (c (n "hl7-mllp-codec") (v "0.0.5") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "0pidnw6fi6m32x10wsm4dkbssv1xmxk7z3vpd752g396vryqc8y4")))

(define-public crate-hl7-mllp-codec-0.0.6 (c (n "hl7-mllp-codec") (v "0.0.6") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "1pkap0b61c05jgn03a8crdmig0wyiw42lnc7m8l5r1k6s71mxhsn")))

(define-public crate-hl7-mllp-codec-0.2.0 (c (n "hl7-mllp-codec") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0ncqgzb30h4ync92rsrpdykl44yxxlqkwdrvrzi669g0wlmpzndm") (f (quote (("noncompliance"))))))

(define-public crate-hl7-mllp-codec-0.3.0 (c (n "hl7-mllp-codec") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0bn96xb0kkzi6k5bmdgy5pdqs7443srcqkwpi4w94aa3mg32pdmn") (f (quote (("noncompliance"))))))

(define-public crate-hl7-mllp-codec-0.4.0 (c (n "hl7-mllp-codec") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0d40g07lbf3rvqff81farf8yxd6v28sn5ipr9rsxgy0dac11nhk2") (f (quote (("noncompliance"))))))

