(define-module (crates-io hl ig hlight-dump) #:use-module (crates-io))

(define-public crate-hlight-dump-0.0.0 (c (n "hlight-dump") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("unicode" "derive" "color"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("regex" "auto-color"))) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "yaml-load" "plist-load"))) (k 0)))) (h "0rq4nbw23cdfd39vhmifc22hgdnkbnbyv9xg4kx7zs8jwxailrps")))

