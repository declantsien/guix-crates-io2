(define-module (crates-io hl ig hlight) #:use-module (crates-io))

(define-public crate-hlight-0.0.0 (c (n "hlight") (v "0.0.0") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy"))) (k 0)))) (h "1fg516gvlzbdk0ccpkkymvg862j3yambxaly3sckzd35n60l6g35") (f (quote (("preset-theme-set") ("preset-syntax-set") ("default" "preset-syntax-set" "preset-theme-set")))) (y #t)))

(define-public crate-hlight-0.0.1-alpha.1 (c (n "hlight") (v "0.0.1-alpha.1") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy"))) (k 0)))) (h "01v7wgrsbfzj4qxza1hyy2c461jyir398qdipkqw3ij6h5kgxjvp") (f (quote (("preset-theme-set") ("preset-syntax-set") ("default" "preset-syntax-set" "preset-theme-set")))) (y #t)))

(define-public crate-hlight-0.0.1-alpha.2 (c (n "hlight") (v "0.0.1-alpha.2") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy"))) (k 0)))) (h "02k40pprb5rc1qnyw7k0b38cznjq2m31z6y3lyy53mwcwdcvk3b6") (f (quote (("preset-theme-set") ("preset-syntax-set") ("default" "preset-syntax-set" "preset-theme-set")))) (y #t)))

(define-public crate-hlight-0.0.1 (c (n "hlight") (v "0.0.1") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy"))) (k 0)))) (h "0sq17h3v3vnncidxzhsczl630c9vb9yywk9qp09bcif8is70gnf0") (f (quote (("preset-theme-set") ("preset-syntax-set") ("default" "preset-syntax-set" "preset-theme-set"))))))

