(define-module (crates-io hl in hline) #:use-module (crates-io))

(define-public crate-hline-0.1.0 (c (n "hline") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "grep") (r "^0.2") (d #t) (k 0)) (d (n "stringreader") (r "^0.1") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dwbh01mh9w0kkdra3wk8gdbpg45fndb7fsj84wd0kwaj0ww168p")))

(define-public crate-hline-0.1.1 (c (n "hline") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "grep") (r "^0.2") (d #t) (k 0)) (d (n "stringreader") (r "^0.1") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i91a9mjia8dk8bh9y5nx32w5y81m5v47k13zra9kdxzmpw4ap3b")))

(define-public crate-hline-0.2.0 (c (n "hline") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "grep") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g7jgff6zrrfa2f1aqpssyiijrif71iai8gz4bz5v0lj18l7bm7v")))

(define-public crate-hline-0.2.1 (c (n "hline") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "grep") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18wz8jrn5lc3wbs3gqscc33q7zrh78xkpqf782cn50lhg2658am3")))

