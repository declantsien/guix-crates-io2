(define-module (crates-io hl ua hlua) #:use-module (crates-io))

(define-public crate-hlua-0.1.0 (c (n "hlua") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "08ry94yp9b8q11hrgdaar20nrhd1klbfbzbfdydh60hy3p6yzzpx")))

(define-public crate-hlua-0.1.1 (c (n "hlua") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "0shdlvvffq90wh9hq5zyy6gl96y9lfrajvyrz9www7l6014mnph2")))

(define-public crate-hlua-0.1.2 (c (n "hlua") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "0fyb5g3km6n2lnqxzv4jqpd26qdxl963p6lgi6gbdbjf0i9p3k6j")))

(define-public crate-hlua-0.1.3 (c (n "hlua") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "0g281crpi4pcb4yd14a3zcyga2dy98x042f8937iln8yz3rcjkyf")))

(define-public crate-hlua-0.1.4 (c (n "hlua") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "17505arv49ndkkp9bspx2pnnnlnm22gzv5f5my93zm6d45rbrda2")))

(define-public crate-hlua-0.1.5 (c (n "hlua") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "0w919g0lnnajy5sfssgfwl70rh1dxpr22408mkcshak5ajgsif9b")))

(define-public crate-hlua-0.1.6 (c (n "hlua") (v "0.1.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "1j4p62mibgpz45w0d1n1gv2f260qccf6b250dw0v2y3qx9vrpyz9")))

(define-public crate-hlua-0.1.7 (c (n "hlua") (v "0.1.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lua52-sys") (r "*") (d #t) (k 0)))) (h "1v2ja35qrrw6xnl8spcjgysvim9qv67xn8qp6j00hyd7vwx9y8gr")))

(define-public crate-hlua-0.1.8 (c (n "hlua") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1irsj682zmppsy578kf48571c9sam44vkfsi14b86fv0vkcsi7an")))

(define-public crate-hlua-0.1.9 (c (n "hlua") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0fha54lfn6zxn5xs0n4flcbrly0izcz3fa4ags9530nf2cnqvyxv")))

(define-public crate-hlua-0.2.0 (c (n "hlua") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.0") (d #t) (k 0)))) (h "07k4mmp3pzwn4rd891ch5907w2yxxgnml3xz0dnqv14i7cnnn8j8")))

(define-public crate-hlua-0.3.0 (c (n "hlua") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.0") (d #t) (k 0)))) (h "04ayyiidpqh00vl8vzm846sr6yxnx8zby7zrc855ia3a05wblpl5")))

(define-public crate-hlua-0.3.1 (c (n "hlua") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1sj4f48mqhr4i9d79a01rwbhlmd6xwiviyx08mm8hxwbds4ycfwl")))

(define-public crate-hlua-0.4.0 (c (n "hlua") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0abk6jpkdhvqzn8qk4b5pavs3mr3kd6hgn5hs3kvc03r2g75j2wr")))

(define-public crate-hlua-0.4.1 (c (n "hlua") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1r2lbn1f17pkf11m8pr6piwzzlgh2fnxkgdv4hyvhm9fzwgvg7gd")))

(define-public crate-hlua-0.4.2 (c (n "hlua") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0gd0r1j6h41kpbkj3xavpr51zafm5c3vl616iwd5hqcgpljbc5ci")))

