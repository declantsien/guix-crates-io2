(define-module (crates-io hl ua hlua-badtouch) #:use-module (crates-io))

(define-public crate-hlua-badtouch-0.4.2 (c (n "hlua-badtouch") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lua52-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0xsrplzi9m2kb7yrk5zmf79qjqhx0jr0w25vh81xnnxb0q3qll6p")))

