(define-module (crates-io hl s- hls-playlist) #:use-module (crates-io))

(define-public crate-hls-playlist-0.1.0 (c (n "hls-playlist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.201") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "0ikrhsvghd170n1594ga7s3ppm5jky7xk9jpn3yq820jsdc1751s") (s 2) (e (quote (("steering-manifest" "dep:serde" "dep:serde_json"))))))

