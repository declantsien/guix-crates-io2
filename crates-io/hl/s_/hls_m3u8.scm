(define-module (crates-io hl s_ hls_m3u8) #:use-module (crates-io))

(define-public crate-hls_m3u8-0.1.0 (c (n "hls_m3u8") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1gzp13r40bnc3n6j8v8cs7ysa6y1fgq3mlfj718x82akgvgk899x")))

(define-public crate-hls_m3u8-0.1.1 (c (n "hls_m3u8") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0pqv2zc50gfdj3lj4s46mkzw2rly5h70rj966sm3mlff6ix7alg7")))

(define-public crate-hls_m3u8-0.1.2 (c (n "hls_m3u8") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1abby8fp35y4r4a37g3ihykgjhq9wlhlmxq5bpwqkn4nc4zhq8yb")))

(define-public crate-hls_m3u8-0.1.3 (c (n "hls_m3u8") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0igdczxsnnvkfixi6qzqxgdlhbq7r87jxs1051a4y6fpk75l3yi4")))

(define-public crate-hls_m3u8-0.1.4 (c (n "hls_m3u8") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "045k697h9sqqa55v6w86ryanb4ik6nb9bmbg1gibrm4p089si24n")))

(define-public crate-hls_m3u8-0.2.0 (c (n "hls_m3u8") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1jm3m6znszk5pkb93psb5ryh3xdxl0wnfbbw1ncz2bp53fb8k57f")))

(define-public crate-hls_m3u8-0.2.1 (c (n "hls_m3u8") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "17kiw7x503q5yjcvxld0p2q3laaavzazisx78m0az65dab7hjhc8")))

(define-public crate-hls_m3u8-0.3.0 (c (n "hls_m3u8") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "shorthand") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0jyk0dz9n616czryz6xmy97iamhryjvkkkkmxq2sc74fsp3g38dz") (f (quote (("default"))))))

(define-public crate-hls_m3u8-0.4.0 (c (n "hls_m3u8") (v "0.4.0") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "shorthand") (r "^0.1") (d #t) (k 0)) (d (n "stable-vec") (r "^0.4") (d #t) (k 0)) (d (n "strum") (r "^0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "13vns54n0aqkx9pcgz2rhi783084afr470dd5g2rsk304vmd8f51") (f (quote (("perf") ("default"))))))

(define-public crate-hls_m3u8-0.4.1 (c (n "hls_m3u8") (v "0.4.1") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "shorthand") (r "^0.1") (d #t) (k 0)) (d (n "stable-vec") (r "^0.4") (d #t) (k 0)) (d (n "strum") (r "^0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1flgg0ckrdzgn45106vm21mjzkz598xca6nf8qak1dcjgzgd4aqf") (f (quote (("perf") ("default"))))))

