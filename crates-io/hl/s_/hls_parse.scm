(define-module (crates-io hl s_ hls_parse) #:use-module (crates-io))

(define-public crate-hls_parse-0.1.0 (c (n "hls_parse") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "12h3gkbw5d85ydhdj70grikld0i08qcyw5nhcjpgz35nnhvm9v21")))

(define-public crate-hls_parse-0.1.1 (c (n "hls_parse") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1vs09xn54v2ivnwzkh1mbb4rxnp6gxyql8igh6njjcs81z76vmsc")))

(define-public crate-hls_parse-0.1.2 (c (n "hls_parse") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1dd9hpkq6hgivvjmz32rlmpvl3f4lbbz9ssn1wypqc6jp58j0hzv")))

(define-public crate-hls_parse-0.1.3 (c (n "hls_parse") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0fcbxvb53y6zz1frpc6gm1gl3za02bhfr430bgr8s76i9vglc0nw")))

(define-public crate-hls_parse-0.1.4 (c (n "hls_parse") (v "0.1.4") (d (list (d (n "aes-stream") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1qld8r0f99bvdp3b1wjfaa9y63n1sgb08n6ir6qchyj3jaji6b0a")))

