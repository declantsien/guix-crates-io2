(define-module (crates-io hl lv hllvm-sys) #:use-module (crates-io))

(define-public crate-hllvm-sys-0.1.0 (c (n "hllvm-sys") (v "0.1.0") (d (list (d (n "cpp") (r "^0.1.0") (f (quote ("macro"))) (d #t) (k 0)) (d (n "cpp") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.84") (d #t) (k 0)))) (h "05nvqv2d19q2cc58rfsim7pcf93lz4s83gxnq7h4b0iqh37n662a")))

(define-public crate-hllvm-sys-0.1.1 (c (n "hllvm-sys") (v "0.1.1") (d (list (d (n "cpp") (r "^0.1.0") (f (quote ("macro"))) (d #t) (k 0)) (d (n "cpp") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.84") (d #t) (k 0)))) (h "0jrxc4rb0jy56an40mb9vc2jcbmmaxvk0fa4kp00zhn4in1d89dp")))

(define-public crate-hllvm-sys-0.1.2 (c (n "hllvm-sys") (v "0.1.2") (d (list (d (n "cpp") (r "^0.1.0") (f (quote ("macro"))) (d #t) (k 0)) (d (n "cpp") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.84") (d #t) (k 0)))) (h "0bfqml5rqhzwapzld2wb28klli9362yrhgz4290y03mdc1cnw31k")))

(define-public crate-hllvm-sys-0.1.3 (c (n "hllvm-sys") (v "0.1.3") (d (list (d (n "cpp") (r "^0.1.0") (f (quote ("macro"))) (d #t) (k 0)) (d (n "cpp") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.84") (d #t) (k 0)))) (h "0f85r083cyq15h2lvd2xkwacb7fa89ksfjw55df26js31a81zdd3")))

