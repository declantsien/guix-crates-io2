(define-module (crates-io hl lv hllvm) #:use-module (crates-io))

(define-public crate-hllvm-0.1.0 (c (n "hllvm") (v "0.1.0") (d (list (d (n "hllvm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18jjjvxms33phpm8z678px2vjjfigpjx4qmlc1apdd783q8qc8n4")))

(define-public crate-hllvm-0.1.1 (c (n "hllvm") (v "0.1.1") (d (list (d (n "hllvm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08x47rrb35w4hmb5481y6gmcqaiy3wpry1ns8hrhcyxiak8snamv")))

(define-public crate-hllvm-0.1.2 (c (n "hllvm") (v "0.1.2") (d (list (d (n "hllvm-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wwri71gm8l48jr1gp8qv62vizkm1rgllr7x3s9qf4xkjjzmg3zr")))

(define-public crate-hllvm-0.1.3 (c (n "hllvm") (v "0.1.3") (d (list (d (n "hllvm-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ppn8wqamj6dh0jk0ldv6431jq5xqbispj1m33fq5xh4cmis1v9m")))

