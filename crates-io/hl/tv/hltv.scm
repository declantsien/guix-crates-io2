(define-module (crates-io hl tv hltv) #:use-module (crates-io))

(define-public crate-hltv-0.1.0 (c (n "hltv") (v "0.1.0") (h "0pw54av9322b91dgjps96ridphp8zbl0vzjayisgg2jswhb7pclv") (y #t)))

(define-public crate-hltv-0.1.1 (c (n "hltv") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "tl") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1qsxb9gws82f1fz9f80gl46axlzfg4jnx6q9m7835q7yzf453r0j")))

(define-public crate-hltv-0.2.0 (c (n "hltv") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "tl") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0prbhwh4dchgabvaz561an187y2hk699jh743ndr1x4zw2wxaga9")))

(define-public crate-hltv-0.3.0 (c (n "hltv") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "tl") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0xi30lc9idj3ykm66ccqrva3cx0za80l8rm2sdvr4xc3p7ac4g38")))

(define-public crate-hltv-0.3.1 (c (n "hltv") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "tl") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0lkwarjc0zw3kk9jfjca0s5j7l5bb2pcxvi22pmn6d4hpznlbx2s")))

(define-public crate-hltv-0.3.2 (c (n "hltv") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "tl") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1hyrl0m9wim38gqj021r5w6q7spp145szbrj0gih84fky5qpma17")))

