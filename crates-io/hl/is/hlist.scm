(define-module (crates-io hl is hlist) #:use-module (crates-io))

(define-public crate-hlist-0.1.0 (c (n "hlist") (v "0.1.0") (h "0f9d8dh2i67ph6hjapyhwiqcwc8ghmfc8hvwgksw5dsrsn13nf0d")))

(define-public crate-hlist-0.1.1 (c (n "hlist") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jx38zg31wn3hq0m30pd38sj865iihwlrnclf34wiwmy553d7m6k") (f (quote (("with_serde" "serde" "serde_derive") ("default" "with_serde"))))))

(define-public crate-hlist-0.1.2 (c (n "hlist") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "059vf9jjrl9akd9mvsdm674bdyvqc7vpjqb5k8yj37ywzmigkcrz") (f (quote (("with_serde" "serde" "serde_derive") ("default" "with_serde"))))))

