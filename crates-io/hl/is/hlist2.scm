(define-module (crates-io hl is hlist2) #:use-module (crates-io))

(define-public crate-hlist2-0.0.1 (c (n "hlist2") (v "0.0.1") (h "105fxi5alvfih4wsi2qn66z7ncn465li10b4rjary5cbbfka6j8d")))

(define-public crate-hlist2-0.0.2 (c (n "hlist2") (v "0.0.2") (h "0vyb7kqg0q7dxyil5y9ijfs6i7znfzqkb0pxrn98h6wdbl28nk60")))

(define-public crate-hlist2-0.0.3 (c (n "hlist2") (v "0.0.3") (h "11afmkf87j78gh0d68dabgf4vfb78bcsaiiwyrxvscdcdc2y2rnv")))

(define-public crate-hlist2-0.0.4 (c (n "hlist2") (v "0.0.4") (h "0f1ki5715j1dw74nghjsa426dxxmriig2gqadwxxzhzvl6s3ajhd") (y #t)))

(define-public crate-hlist2-0.0.5 (c (n "hlist2") (v "0.0.5") (h "1swsp9qb504gd96ryrdpqrz96xdfi2rrhcwh5g85ldiyha5nyikn")))

(define-public crate-hlist2-0.0.6 (c (n "hlist2") (v "0.0.6") (h "0ww0ixsq7f2732pzkifzvbmzqm7dbkqiy4z2cklpz4iy3aa4bcw2")))

(define-public crate-hlist2-0.0.7 (c (n "hlist2") (v "0.0.7") (h "1815rwb6rnhvi48gwsl9k9xs6g4l1yx1krjxcq52xl5w3w51bfay") (y #t)))

(define-public crate-hlist2-0.0.8 (c (n "hlist2") (v "0.0.8") (h "06az30ahmiakkbz4s1mlhf991xgcjddfixcyfjqfgbjg8a7nh11f")))

(define-public crate-hlist2-0.0.9 (c (n "hlist2") (v "0.0.9") (h "1qg864dxig0wwhrwf5c06j83zdi7l4r8r68f56sw8snm4fmjz7sv")))

(define-public crate-hlist2-0.0.10 (c (n "hlist2") (v "0.0.10") (h "1zvv9k1xc0bakxw5pv1zwylczdd887qjzq3w4kwywdl0a0r70bdw")))

(define-public crate-hlist2-0.0.11 (c (n "hlist2") (v "0.0.11") (h "003vda4x5330gmi31ybgcm2dfnqcdjrr9w35i5nrzk89w0lj9gcx")))

(define-public crate-hlist2-0.0.12 (c (n "hlist2") (v "0.0.12") (h "0ccnyhs0gdsxdrgh7h4jga42nl7iazvdpi7k6ad0pi7sjps5wvsl")))

(define-public crate-hlist2-0.0.13 (c (n "hlist2") (v "0.0.13") (h "0zjjrach3bcf38f7qsbyycykz6q6z0l5pi9s88h7g16a21psh5dv")))

(define-public crate-hlist2-0.0.14 (c (n "hlist2") (v "0.0.14") (h "08c2hiffhhmrgixq2mjdw2kr8kj5xaffacm86acc650d2xwmq4x3")))

