(define-module (crates-io hl ed hledger-get-market-prices) #:use-module (crates-io))

(define-public crate-hledger-get-market-prices-1.0.0 (c (n "hledger-get-market-prices") (v "1.0.0") (d (list (d (n "alpha_vantage") (r "^0.7") (f (quote ("reqwest-client"))) (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jprdr2s4s739i3d45imlbv2l2h9qw85qplc1lf1m9136xfg0j63")))

(define-public crate-hledger-get-market-prices-1.1.0 (c (n "hledger-get-market-prices") (v "1.1.0") (d (list (d (n "alpha_vantage") (r "^0.7") (f (quote ("reqwest-client"))) (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "142ahll3jjv9capxvddm9j92a5j8akhvzxbq2z1n1ajvc5s6r2qb")))

