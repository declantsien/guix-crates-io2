(define-module (crates-io hl k_ hlk_ld6002) #:use-module (crates-io))

(define-public crate-hlk_ld6002-0.1.0 (c (n "hlk_ld6002") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "embedded-io") (r "^0.6.1") (d #t) (k 0)) (d (n "embedded-io-adapters") (r "^0.6.1") (f (quote ("std" "tokio-1"))) (d #t) (k 2)) (d (n "embedded-io-async") (r "^0.6.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 2)) (d (n "termion") (r "^3.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 2)))) (h "1ycnx1xxa3a5p8hydrfbnzfsgybx5iiark0rfali9r0xxhlzpsdf")))

