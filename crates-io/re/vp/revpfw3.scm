(define-module (crates-io re vp revpfw3) #:use-module (crates-io))

(define-public crate-revpfw3-0.1.0 (c (n "revpfw3") (v "0.1.0") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "0lw4kmapsv4hq8jxjhvcknrsb8bsrd7ls006c011rsfnq1hrqbk4")))

(define-public crate-revpfw3-0.1.1 (c (n "revpfw3") (v "0.1.1") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "0diw9s9kf9x2sqrn8n3blarqmhjcck51g9hczm92k4i7r5lp547p")))

(define-public crate-revpfw3-0.1.2 (c (n "revpfw3") (v "0.1.2") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "0i9n2752nwn61l3wslwxpl7lj1a0nxp9387a71lkklavg0xmhish")))

(define-public crate-revpfw3-0.2.0 (c (n "revpfw3") (v "0.2.0") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "0ps57csjfmsrkzxi5yrcs1lsxjwczx4cr5s8fv3cb71ghs9p20af")))

(define-public crate-revpfw3-0.3.0 (c (n "revpfw3") (v "0.3.0") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "0rzx7di0zmasa7bv637k3zllb5hm3y1lxw15as6smhkmqxldpkpz")))

(define-public crate-revpfw3-0.3.1 (c (n "revpfw3") (v "0.3.1") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "03a6vq8pxy2bcql6rwdd88pd7i0kxs0hmxzrr281jfac2zxwplsy")))

