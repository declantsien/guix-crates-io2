(define-module (crates-io re vp revpolnot) #:use-module (crates-io))

(define-public crate-revpolnot-1.0.0 (c (n "revpolnot") (v "1.0.0") (h "1457j7dqb37wcsyknxnbc2v48nilsm0w3w790j42igp75w3p5jgh")))

(define-public crate-revpolnot-1.0.1 (c (n "revpolnot") (v "1.0.1") (h "1l9qv3snkqlhlwkjhy11vh9v3jyzyfrrjimk6jqqn833bivg5rhm")))

(define-public crate-revpolnot-1.0.2 (c (n "revpolnot") (v "1.0.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a84pizwxaa0f77r39vcrsjhnimf3d0dm4widzlvwb0mr51ihmwf")))

