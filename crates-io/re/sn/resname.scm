(define-module (crates-io re sn resname) #:use-module (crates-io))

(define-public crate-resname-0.1.0 (c (n "resname") (v "0.1.0") (h "0sgcnmpsm14bv87h8njxna9iqvr457ss4ashxxnicq1wb83igsn7")))

(define-public crate-resname-0.1.1 (c (n "resname") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1q7sakfmdbszdxc5ch5njm8xw6jfradhz373zzmjcmv89kypcj7w")))

(define-public crate-resname-0.1.2 (c (n "resname") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1nnzy32lyxlqbl18h079hzs25k51rsq9y40hkg7i6bsidi7h167k")))

