(define-module (crates-io re f_ ref_count) #:use-module (crates-io))

(define-public crate-ref_count-0.1.0 (c (n "ref_count") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("futures" "checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (d #t) (k 2)))) (h "123sh2klsigzwppp95zkmykxdm42c22wklv6ghag6ynvilklk59c") (f (quote (("std") ("alloc"))))))

(define-public crate-ref_count-0.1.1 (c (n "ref_count") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("futures" "checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (d #t) (k 2)))) (h "02haljp2xipznw78gjmvwzjw78mf2c7f7jr2dblkbmbm9izpha2f") (f (quote (("std") ("alloc"))))))

(define-public crate-ref_count-0.1.2 (c (n "ref_count") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("futures" "checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (d #t) (k 2)))) (h "1nazk0p91fvcf2rqwfbpb0s3ns54ydwss326mammwfgi0bv703ib") (f (quote (("std") ("alloc"))))))

