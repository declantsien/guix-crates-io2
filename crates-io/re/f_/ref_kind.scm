(define-module (crates-io re f_ ref_kind) #:use-module (crates-io))

(define-public crate-ref_kind-0.0.0 (c (n "ref_kind") (v "0.0.0") (h "1f12vfshgqridg2366zbfakqfzs2cd7s3dqnyfk1bxmdv6bx0jin") (y #t)))

(define-public crate-ref_kind-0.1.0 (c (n "ref_kind") (v "0.1.0") (h "0bi2mcv950fjrsxzkywillf8fvg67y5ffcgksfp7p7d3kddzgg2b")))

(define-public crate-ref_kind-0.2.0 (c (n "ref_kind") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.11.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "1rig7334dj657sp5jz1zn7r9ny3qdf8dcw32csblw18ycn3rx6cj") (s 2) (e (quote (("bumpalo" "dep:bumpalo" "hashbrown/bumpalo"))))))

(define-public crate-ref_kind-0.2.1 (c (n "ref_kind") (v "0.2.1") (d (list (d (n "bumpalo") (r "^3.11.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "1cljj74y9q5nf8jk8pl2f780433jfiqbcr7k6cldkkcxgb1544jr") (s 2) (e (quote (("bumpalo" "dep:bumpalo" "hashbrown/bumpalo"))))))

(define-public crate-ref_kind-0.3.0 (c (n "ref_kind") (v "0.3.0") (h "1wghav4751napqk1zxgi40zk2mff4d5mc5alw4q6977qif51f8aa")))

(define-public crate-ref_kind-0.4.0 (c (n "ref_kind") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (k 0)))) (h "1m93653l8rjwscq3d6my771cpp6fz1c9g0ypxaa05hm02ybkz3by") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.4.1 (c (n "ref_kind") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (k 0)))) (h "07x3kk0vfgk9hdj6ds3idv4d2gfxaigs609s4imd07zw7rsxvs66") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.4.2 (c (n "ref_kind") (v "0.4.2") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (k 0)))) (h "1hwnakxgrwdz7v55vwzpv39zvvsynwlxjq5h12jbwlm6pnvin8wq") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.5.0 (c (n "ref_kind") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (k 0)))) (h "0h7sw9ipais3852v28si427nw7z5cqyrldwfyb9dhqcqccd6a7a4") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.5.1 (c (n "ref_kind") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (k 0)))) (h "009494zglm1bqgbfaqmadz1xzjjl05cl82jsdvwvj09yd1kapq74") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

