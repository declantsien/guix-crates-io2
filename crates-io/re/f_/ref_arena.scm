(define-module (crates-io re f_ ref_arena) #:use-module (crates-io))

(define-public crate-ref_arena-0.1.0 (c (n "ref_arena") (v "0.1.0") (d (list (d (n "brunch") (r "^0.5.2") (d #t) (k 2)))) (h "0l6ry1vxx7ml55pzv83c8f3xi22zqh2all78c41fhz0rx839r0ww")))

(define-public crate-ref_arena-0.1.1 (c (n "ref_arena") (v "0.1.1") (d (list (d (n "brunch") (r "^0.5.2") (d #t) (k 2)))) (h "083gm8vnfazs2995d33ci87iplxgr6ikrc6cvgmslp5chkm8xrs0")))

