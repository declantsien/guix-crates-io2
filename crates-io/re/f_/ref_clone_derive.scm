(define-module (crates-io re f_ ref_clone_derive) #:use-module (crates-io))

(define-public crate-ref_clone_derive-0.1.0 (c (n "ref_clone_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ref_clone") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "13603j0if0ghfcaf6jnzrlw8prqi3v948cxylbl05xrcsxgh640z")))

(define-public crate-ref_clone_derive-0.2.0 (c (n "ref_clone_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ref_clone") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0dp4gh06ypxnd27rmiqw7fkw8bzqp5fsa4hjqqpk2p7yp2qlih49")))

(define-public crate-ref_clone_derive-0.3.0 (c (n "ref_clone_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ref_clone") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0j0vccpdg6vxdd7x8ifb22m99nxpb6a55a4sbc3ziv3d2zi3v3zw")))

(define-public crate-ref_clone_derive-0.4.0 (c (n "ref_clone_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ref_clone") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1da7xixp0fcikv85wb4ahhq1v27wx1kkvir7f6sr0ig4qcd7z9r5")))

(define-public crate-ref_clone_derive-0.5.0 (c (n "ref_clone_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ref_clone") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "108qv06g3gr8gzma1xq2bchwgdh96hj21spc030fvvcrv170q439")))

(define-public crate-ref_clone_derive-0.6.0 (c (n "ref_clone_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1p24219mrkzszpx1iq7n84inc8xydb44704jh2nyyjdm7z72vmdw")))

(define-public crate-ref_clone_derive-0.7.0 (c (n "ref_clone_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "13klkgdxxzax5j6nr6lnd4qprz8sidn6mkk8f8k4qpp40qns13s8")))

