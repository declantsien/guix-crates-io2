(define-module (crates-io re f_ ref_filter_map) #:use-module (crates-io))

(define-public crate-ref_filter_map-1.0.0 (c (n "ref_filter_map") (v "1.0.0") (h "0gr1x1j5zrpnp04ndljy6wxxcdbh8bx1x3xa2kg0p9i432jm1xsi")))

(define-public crate-ref_filter_map-1.0.1 (c (n "ref_filter_map") (v "1.0.1") (h "1vdm5xmm1kzabdf98f111b8gsr4zniga28pd854dl2a01s2fnp1b")))

