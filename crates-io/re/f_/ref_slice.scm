(define-module (crates-io re f_ ref_slice) #:use-module (crates-io))

(define-public crate-ref_slice-0.1.0 (c (n "ref_slice") (v "0.1.0") (h "1riz17lj5mdy0fb0v6s6d58qaar01y7frspwca3jaaaaj62m7nyc")))

(define-public crate-ref_slice-1.0.0 (c (n "ref_slice") (v "1.0.0") (h "1wmsfkc4r180bqp9f0jpramqa69vyn7gjb8i4l2pzhq3i67izj94")))

(define-public crate-ref_slice-1.1.0 (c (n "ref_slice") (v "1.1.0") (h "1b9dczvky84bvys9lkrhfydax5zajy03qnyc68kj6py8j6mb8ssl")))

(define-public crate-ref_slice-1.1.1 (c (n "ref_slice") (v "1.1.1") (h "09qmyw14dx6i0bd36chm9r1h7g5agr3zdyp74lqd9dwpf42l0mw2")))

(define-public crate-ref_slice-1.2.0 (c (n "ref_slice") (v "1.2.0") (h "0s5csx2w2arg1ya3pdi8apv4052i55w0ab7k11maf802h1w7h6sf")))

(define-public crate-ref_slice-1.2.1 (c (n "ref_slice") (v "1.2.1") (h "1adpmza9cnfsms81riry1p60q1isadlyyams86wakswjzdrivvgl")))

