(define-module (crates-io re f_ ref_clone) #:use-module (crates-io))

(define-public crate-ref_clone-0.1.0 (c (n "ref_clone") (v "0.1.0") (h "1x6mwrcxh4i7h13qrzg7aw8kjl4j508akqdgfpwvh3qqw07rnz5q")))

(define-public crate-ref_clone-0.2.0 (c (n "ref_clone") (v "0.2.0") (h "00di3yzbq9l09hvkgqy4whajrgxpjk147wnnxw68wan82qdr9aq2")))

(define-public crate-ref_clone-0.3.0 (c (n "ref_clone") (v "0.3.0") (h "0hm7lpmmz0flsdzajnpf4hl75a8mj1392i5y546gkx3w63256wld")))

(define-public crate-ref_clone-0.4.0 (c (n "ref_clone") (v "0.4.0") (h "0kbcj4rhfyciyqmc2h462gknk2b5a8wsvng6fbnb8fskp98ky3h9")))

(define-public crate-ref_clone-0.5.0 (c (n "ref_clone") (v "0.5.0") (h "1bq5v5iz5facml0mhn4xdck9nfa59s4v6ghrdxi47q3nd8qdbsam")))

(define-public crate-ref_clone-0.6.0 (c (n "ref_clone") (v "0.6.0") (h "1k6rjl1aa0hcdafv12s1w9bssf3ixj50c8dvmppi23z0p33fpk0w")))

(define-public crate-ref_clone-0.7.0 (c (n "ref_clone") (v "0.7.0") (d (list (d (n "ref_clone_derive") (r "^0.7.0") (d #t) (k 0)))) (h "0pah2lww7aqm93n6fmjyjllkvqand94xmaz35b415yy85jk1zpbx")))

(define-public crate-ref_clone-0.8.0 (c (n "ref_clone") (v "0.8.0") (d (list (d (n "ref_clone_derive") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1yz7ybqbbf9517yvkzwpk94r5mg99j1yc5yccf5s13h9i0120r0h")))

