(define-module (crates-io re f_ ref_wrapper) #:use-module (crates-io))

(define-public crate-ref_wrapper-0.1.0 (c (n "ref_wrapper") (v "0.1.0") (h "07ncihh7kzdy9w3zni19yswf7273bf10jb3818nwlsh0pkqya8qf")))

(define-public crate-ref_wrapper-0.1.1 (c (n "ref_wrapper") (v "0.1.1") (h "1fdc3gn9py681ngq79pkrcw3hzshms1zwg487byb1x1d7n2i4lcp")))

(define-public crate-ref_wrapper-0.1.2 (c (n "ref_wrapper") (v "0.1.2") (h "1g5hmw3qflxpkr8z9nnxrq3w2wbif4g3r26c8qav6bb6d8adqqzp")))

(define-public crate-ref_wrapper-0.1.3 (c (n "ref_wrapper") (v "0.1.3") (h "1bjnpx7q6yhq4xgaqvls2nbjaipjrsfmj729l7i440d5iw38wh8q")))

(define-public crate-ref_wrapper-0.1.4 (c (n "ref_wrapper") (v "0.1.4") (h "1g6ydr80ra2py1nhv6byhba112h2r4h4xm2fwfrg990ipadbhpml")))

(define-public crate-ref_wrapper-0.1.5 (c (n "ref_wrapper") (v "0.1.5") (h "1lq040shwq1wc4j6myhi0x4z7gy2zyg516q33bxf1fqqmg41hb6s")))

(define-public crate-ref_wrapper-0.2.0 (c (n "ref_wrapper") (v "0.2.0") (h "1rbx3dp31dvks26l3nd6d5v860cr61p4k1azw03frrlxcdwq3w7w")))

(define-public crate-ref_wrapper-0.2.1 (c (n "ref_wrapper") (v "0.2.1") (d (list (d (n "drop_tracer") (r "^0.1") (d #t) (k 2)))) (h "017pncsh1z0qq957gv905w85k71qyw8jzsa5pdfa1rwklb7jfrk6")))

(define-public crate-ref_wrapper-0.2.2 (c (n "ref_wrapper") (v "0.2.2") (d (list (d (n "drop_tracer") (r "^0.1") (d #t) (k 2)))) (h "1x37qw9raz4a7mgjl49wqa0c3yznlabhr8i1wnyz6jdfj86nqvzw")))

