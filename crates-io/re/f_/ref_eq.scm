(define-module (crates-io re f_ ref_eq) #:use-module (crates-io))

(define-public crate-ref_eq-0.1.0 (c (n "ref_eq") (v "0.1.0") (h "0hrpf311nxji87gxcxx7b6xhxq1pz3hh86285lxx29wk2iv9da14")))

(define-public crate-ref_eq-1.0.0 (c (n "ref_eq") (v "1.0.0") (h "1ndbgisjhpqrppqdwzjw5bmj3bzn9anjygyvi6b6drdb1ic0bgmm")))

