(define-module (crates-io re sf resfetch) #:use-module (crates-io))

(define-public crate-resfetch-1.0.0 (c (n "resfetch") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libmacchina") (r "^6") (d #t) (k 0)))) (h "09y5y7v6ci4vgsx9f3a0g44hy1zbgl0dz9rzffpmk07dvm2h0jp6")))

(define-public crate-resfetch-1.0.1 (c (n "resfetch") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libmacchina") (r "^6") (d #t) (k 0)))) (h "1g16vsf3fc2qfn934014mndidxhc96irjkrlznnms7kh6m64l78h")))

(define-public crate-resfetch-1.0.2 (c (n "resfetch") (v "1.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libmacchina") (r "^6") (d #t) (k 0)))) (h "14iw27xwfrbfv3h0ic9x0pn3rh41766fs3y28f742rg2kdjxki6l")))

(define-public crate-resfetch-1.1.0 (c (n "resfetch") (v "1.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7") (d #t) (k 0)))) (h "0x6wn6bl6xsb9lx83lnnnsx84vf4q46cwnpk05dvkvva564619w0")))

(define-public crate-resfetch-1.1.1 (c (n "resfetch") (v "1.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7") (d #t) (k 0)))) (h "0djybq72fkdvh8izhv3y2grw9fpysdbxz9km6hm2ikg618llclic")))

(define-public crate-resfetch-1.1.2 (c (n "resfetch") (v "1.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7") (d #t) (k 0)))) (h "1pch245ajxq4rm0b85dmmf24fxp81cf7p4pq75r4d2vlcyyc0r5s")))

