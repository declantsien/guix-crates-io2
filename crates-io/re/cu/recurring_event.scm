(define-module (crates-io re cu recurring_event) #:use-module (crates-io))

(define-public crate-recurring_event-0.1.0 (c (n "recurring_event") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.12") (f (quote ("macros" "local-offset" "serde"))) (d #t) (k 0)) (d (n "typetag") (r "^0.2.3") (d #t) (k 0)))) (h "19hanbs2wlmw6hvls1mhz0phx3l5xsyzdq1ha8h9pd2d0y6z6xz9") (r "1.63.0")))

(define-public crate-recurring_event-0.1.1 (c (n "recurring_event") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.12") (f (quote ("macros" "local-offset" "serde"))) (d #t) (k 0)) (d (n "typetag") (r "^0.2.3") (d #t) (k 0)))) (h "1hksd19dl2pxxw95sdq695l7fndkpf1dyix1n12gmknwabk9vba1") (r "1.63.0")))

(define-public crate-recurring_event-0.1.2 (c (n "recurring_event") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.12") (f (quote ("macros" "local-offset" "serde"))) (d #t) (k 0)) (d (n "typetag") (r "^0.2.3") (d #t) (k 0)))) (h "0i04d4m95ml0r3asd08i4k2xqlmncb0mzya3alrzr35l46pxqbja") (r "1.63.0")))

(define-public crate-recurring_event-0.1.3 (c (n "recurring_event") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("macros" "local-offset" "serde-human-readable"))) (d #t) (k 0)) (d (n "typetag") (r "^0.2.3") (d #t) (k 0)))) (h "1ph8il7bz2khbh7az6c8258414zpwjknvs70cx4257zlf6hiv4yh") (r "1.63.0")))

