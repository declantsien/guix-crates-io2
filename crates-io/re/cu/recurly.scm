(define-module (crates-io re cu recurly) #:use-module (crates-io))

(define-public crate-recurly-48.0.0 (c (n "recurly") (v "48.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "050z1m5ln9hqclnhf8crx913ffnqnsz29111sfmhrsrsgs5cxwm6")))

