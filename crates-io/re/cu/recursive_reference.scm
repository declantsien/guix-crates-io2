(define-module (crates-io re cu recursive_reference) #:use-module (crates-io))

(define-public crate-recursive_reference-0.1.0 (c (n "recursive_reference") (v "0.1.0") (d (list (d (n "void") (r "1.*") (d #t) (k 0)))) (h "0gfxmyzp5691wp4vdlz3jhlwpkn24wgzc297qb6sy6py1q0rdjsa")))

(define-public crate-recursive_reference-0.1.1 (c (n "recursive_reference") (v "0.1.1") (d (list (d (n "void") (r "1.*") (d #t) (k 0)))) (h "0giymprn38si7sia6f5b19665lb9jxwn53ylqkjnl9irjja8cnwm")))

(define-public crate-recursive_reference-0.2.0 (c (n "recursive_reference") (v "0.2.0") (d (list (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "06i2mnp1xgav1q2hab560mzrr4s2i394aqqx4m6zkhm8fndxzi55")))

(define-public crate-recursive_reference-0.3.0 (c (n "recursive_reference") (v "0.3.0") (d (list (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0kw0j3mx4c6sdnqam6189xd1csl8zys83b7p5i62q7mmg729ah19")))

