(define-module (crates-io re cu recurdates) #:use-module (crates-io))

(define-public crate-recurdates-0.1.0 (c (n "recurdates") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "00idnppg2zsbhp5fn202dd8qkfyyykl9vbbqb6w7jdwdrksq10yy")))

(define-public crate-recurdates-0.1.1 (c (n "recurdates") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "0x1l28jkc35p300v3lx19ffr4hwkpy0agaivgy0yyjqiz45i43s5")))

(define-public crate-recurdates-0.2.0 (c (n "recurdates") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "0f9rawzy5vhdsbcfhgm80fj51yclq06dl8g51cklc45frzxi4haz")))

(define-public crate-recurdates-0.3.0 (c (n "recurdates") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "0ps58nkg5pmxzh5kx93s8v47xfi7mgfh032ywwf7kvysqb6aphhg")))

(define-public crate-recurdates-0.3.1 (c (n "recurdates") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "11rj44054a3jqsgwlxxww0f5w1apj95l01ckir47vvsi9zn0bsik")))

