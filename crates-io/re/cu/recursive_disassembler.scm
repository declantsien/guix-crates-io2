(define-module (crates-io re cu recursive_disassembler) #:use-module (crates-io))

(define-public crate-recursive_disassembler-0.1.0 (c (n "recursive_disassembler") (v "0.1.0") (d (list (d (n "capstone") (r "~0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)))) (h "07r65d4smamal402c0829kzpzw3npnqdyzmds9sg3ldd9aybygsa")))

(define-public crate-recursive_disassembler-2.0.0 (c (n "recursive_disassembler") (v "2.0.0") (d (list (d (n "capstone") (r "~0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)))) (h "1ncfcd4yfkalq7hkmsbm7vvabxlv5hwydgmcn4hcka2ijzj0k82f")))

(define-public crate-recursive_disassembler-2.1.0 (c (n "recursive_disassembler") (v "2.1.0") (d (list (d (n "capstone") (r "~0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)))) (h "0zlw94silfcqkrxiby2zx5b59h459hh5vqwfqjlyc9jz78q05lyr")))

(define-public crate-recursive_disassembler-2.1.1 (c (n "recursive_disassembler") (v "2.1.1") (d (list (d (n "capstone") (r "~0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)))) (h "07jal4plipgmalcaq3kccnrdq5lnm4bxmjlhwr4vmxncwycwsz6p")))

(define-public crate-recursive_disassembler-2.1.2 (c (n "recursive_disassembler") (v "2.1.2") (d (list (d (n "capstone") (r "~0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)))) (h "0a3q74dklcda655igphyqn796j9lr7hgxlbd4z10pvgac3wsk9hc")))

