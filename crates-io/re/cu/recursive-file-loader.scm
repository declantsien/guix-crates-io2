(define-module (crates-io re cu recursive-file-loader) #:use-module (crates-io))

(define-public crate-recursive-file-loader-1.0.0 (c (n "recursive-file-loader") (v "1.0.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1cssg23dh2h3b91bzamv8830pz1bxxr1qm1i4ji28jgvzizy8zsq")))

(define-public crate-recursive-file-loader-1.0.1 (c (n "recursive-file-loader") (v "1.0.1") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "00zqk0alnyb5isw3p38kr8pn1san2gb0j2h8vicypy9cn6b0g9z1")))

(define-public crate-recursive-file-loader-1.0.2 (c (n "recursive-file-loader") (v "1.0.2") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "03n8xvq1nmpnmsmssgn9ydjrckpvmydywgmj966b97wmiw8hjsjf")))

(define-public crate-recursive-file-loader-1.0.3 (c (n "recursive-file-loader") (v "1.0.3") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0m8mzixf2nyin211xc6m14m9wp0sxw6cpg6pvwk526b012ididiz")))

