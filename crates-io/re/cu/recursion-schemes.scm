(define-module (crates-io re cu recursion-schemes) #:use-module (crates-io))

(define-public crate-recursion-schemes-0.1.0 (c (n "recursion-schemes") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "recursion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "fs" "macros" "io-util" "sync"))) (d #t) (k 0)))) (h "0b504wzfydar02vaxmqiqysjr2wri5khmijkb39a5wsxhfs3g1hi") (f (quote (("experimental") ("default") ("backcompat" "recursion"))))))

(define-public crate-recursion-schemes-0.1.1 (c (n "recursion-schemes") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "recursion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "fs" "macros" "io-util" "sync"))) (d #t) (k 0)))) (h "03db2x388b7vvixg1dblcanwkhrmqi7kwgkj4pb3irh0ld2y19ks") (f (quote (("experimental") ("default") ("backcompat" "recursion"))))))

(define-public crate-recursion-schemes-0.1.2 (c (n "recursion-schemes") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "recursion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "fs" "macros" "io-util" "sync"))) (d #t) (k 0)))) (h "0lmsyi35rkprj1y6z43vysk4d98gmf9img66v1m0wakzh88xsvkk") (f (quote (("experimental") ("default") ("backcompat" "recursion"))))))

(define-public crate-recursion-schemes-0.1.3 (c (n "recursion-schemes") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "recursion") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "fs" "macros" "io-util" "sync"))) (d #t) (k 0)))) (h "17cz5rkxaismlg20z2ywk878fdpcwrwl3r8barwx1z74r15xh3ws") (f (quote (("experimental") ("default") ("backcompat" "recursion"))))))

