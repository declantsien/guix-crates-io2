(define-module (crates-io re cu recursion-visualize) #:use-module (crates-io))

(define-public crate-recursion-visualize-0.5.0 (c (n "recursion-visualize") (v "0.5.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "recursion") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04f3z9pkrhdf8h7wqrg75nh1wnaqm0wbz88vx1hakqijwp3zcdqh")))

