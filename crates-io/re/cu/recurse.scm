(define-module (crates-io re cu recurse) #:use-module (crates-io))

(define-public crate-recurse-0.0.1 (c (n "recurse") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "10avi575b9r98987gsngmkqxwksihswcj0lk9g4sglkbjmqgynna")))

(define-public crate-recurse-0.1.0 (c (n "recurse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1x8bzlzrl4fnqk1pn2pij6fyw2kabg73bl6rxpgcci9jwd3d92zx")))

(define-public crate-recurse-0.2.0 (c (n "recurse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "18rqha5s4363x5dj1sylppdwrhwfnv5fgh3g8rbhh6j3ks5prjgr")))

(define-public crate-recurse-0.3.0 (c (n "recurse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0148drl27i5bci2lwnrdi3mhrqyxxl99zgm3ld0dv0mybq17gnwb")))

