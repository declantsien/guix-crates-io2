(define-module (crates-io re cu recursive-env) #:use-module (crates-io))

(define-public crate-recursive-env-0.1.0 (c (n "recursive-env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "0jkw0vwxdfvs1126iw7yrnwws0cg7kr3jv9zw7lwnkmfjg4n41ga")))

(define-public crate-recursive-env-0.1.1 (c (n "recursive-env") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "03265a3d5g0l66kx19wcwjygn1z38cs6fhhx2zv4xm0xyl76cvvr")))

