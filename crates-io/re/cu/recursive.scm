(define-module (crates-io re cu recursive) #:use-module (crates-io))

(define-public crate-recursive-0.1.0 (c (n "recursive") (v "0.1.0") (d (list (d (n "recursive-proc-macro-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1q6j3vrcp921gm2lh8z5438s1q3r9v1fagihmcmadd7nny7kdqhi")))

(define-public crate-recursive-0.1.1 (c (n "recursive") (v "0.1.1") (d (list (d (n "recursive-proc-macro-impl") (r "=0.1.1") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "0gmlaih5kyqc1pkbk0klqr9m65c4bvz6j0mwn68z8q5pxcys91h7")))

