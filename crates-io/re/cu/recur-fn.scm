(define-module (crates-io re cu recur-fn) #:use-module (crates-io))

(define-public crate-recur-fn-0.1.0 (c (n "recur-fn") (v "0.1.0") (h "024bww5jzc5qwp117gdrki75i2rfvkcj9n11b814bq8j1lg5jics")))

(define-public crate-recur-fn-0.2.0 (c (n "recur-fn") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0s38mnd4rlja4a50g9qjwyb78m9qlkm2xx49bf752fjxm4vna3f0")))

(define-public crate-recur-fn-0.2.1 (c (n "recur-fn") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0vzi2kzpsngs2pw2dc6yyydi6a09kjc20ma5p37p930kghqpjy77")))

(define-public crate-recur-fn-0.2.2 (c (n "recur-fn") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0whc1hz1jb3s08gz0xd3f7cml5ll21rvhxdscvp3y1pa1721vj1f")))

(define-public crate-recur-fn-0.3.0 (c (n "recur-fn") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1vc27whz8c51rdck1xdymldzwrm565bryzqaxx4vabx72krbj6yj") (y #t)))

(define-public crate-recur-fn-1.0.0 (c (n "recur-fn") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "07w8fhhchrdswc2gm9f1cdd2sd734jybx7jfzdamhdcx3irwczd5") (y #t)))

(define-public crate-recur-fn-1.0.1 (c (n "recur-fn") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "03iw8m6vyrb6pwg1lpbvhv0mk16w99qjccnlq7kgvmn7mzgm27qw") (y #t)))

(define-public crate-recur-fn-1.0.2 (c (n "recur-fn") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0ljksfvvzf7zj9aw85aspp64cbc7qzdz2jc8ksv7qvygl83sdlnh")))

(define-public crate-recur-fn-1.1.0 (c (n "recur-fn") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1jryz9wcbnrvi7plijbi58dwkmsqlxp12ydfc4rmh31avwz1kx5q") (y #t)))

(define-public crate-recur-fn-1.2.0 (c (n "recur-fn") (v "1.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0s43nw8756rjs2flks874201rzgpcyd0d9lk8m0lmr0dydpy5b6r")))

(define-public crate-recur-fn-1.2.1 (c (n "recur-fn") (v "1.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1vgm00msjgzcbx5i0q8kb496spsc8bbg4z293d7g0rjp0hing02l")))

(define-public crate-recur-fn-2.0.0 (c (n "recur-fn") (v "2.0.0") (h "1gw6w2lmkdskm2xc345373aii4x8cbwjcghwjcgyl425myrbaag3")))

(define-public crate-recur-fn-2.1.0 (c (n "recur-fn") (v "2.1.0") (h "114b3bhyad4jjblfrclix9cgfvqc63mads74sgm6a9kcz03casv5") (y #t)))

(define-public crate-recur-fn-2.2.0 (c (n "recur-fn") (v "2.2.0") (h "00c2jjzhshlm3354xg6gi57jiz7ihh26m46s8izplgsgaz83kcnx")))

