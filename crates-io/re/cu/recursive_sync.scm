(define-module (crates-io re cu recursive_sync) #:use-module (crates-io))

(define-public crate-recursive_sync-0.0.1 (c (n "recursive_sync") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1dfjvcrksl0161qpl4458phrkck5whxp5d3anxxwy3x25a6iag5l")))

(define-public crate-recursive_sync-0.0.2 (c (n "recursive_sync") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1jvkfd28av0wbcf7b21a2xahh0wfgig5k0n54399vcmap99i1grd")))

