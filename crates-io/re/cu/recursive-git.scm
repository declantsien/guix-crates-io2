(define-module (crates-io re cu recursive-git) #:use-module (crates-io))

(define-public crate-recursive-git-0.1.0 (c (n "recursive-git") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cz58wp4rkz3ixnw3lwmyk6zkn19g2yr7c74rspqpw4l9g9c9vg7")))

(define-public crate-recursive-git-0.1.1 (c (n "recursive-git") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mk224hhvqsi6jz9ha2q77r79giqg8ygnal20a18bvkcczgbkqny")))

