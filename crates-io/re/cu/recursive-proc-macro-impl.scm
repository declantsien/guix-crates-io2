(define-module (crates-io re cu recursive-proc-macro-impl) #:use-module (crates-io))

(define-public crate-recursive-proc-macro-impl-0.1.0 (c (n "recursive-proc-macro-impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cs2zi8ji134f2iyfdqh3fpyjwaq8jn31ybsh2v05756dh16cjmd")))

(define-public crate-recursive-proc-macro-impl-0.1.1 (c (n "recursive-proc-macro-impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12z3wy2wa4l2dpfdb5vhaaiy78l130x5w9fflb0py1ql0sz9y03n")))

