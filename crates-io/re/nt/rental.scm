(define-module (crates-io re nt rental) #:use-module (crates-io))

(define-public crate-rental-0.1.0 (c (n "rental") (v "0.1.0") (h "1i9d5jdhsa35qfg33ncms247imzlqlxy3mpm6zbkyv8zg46783jh") (y #t)))

(define-public crate-rental-0.2.0 (c (n "rental") (v "0.2.0") (h "0yvw5sjqddk3qf26y5cypknrrhlfb7dkg8sacnb6ivda5s3bgclv") (y #t)))

(define-public crate-rental-0.2.1 (c (n "rental") (v "0.2.1") (h "115762w77vawffbjiczmddn7027n967csdaqz3apkxmx6ighk61n") (y #t)))

(define-public crate-rental-0.2.2 (c (n "rental") (v "0.2.2") (h "1a9w7j694vnl8ihr0p7y7rdj1ihgsxsa3kdkw16pvjg4nf02fsp6") (y #t)))

(define-public crate-rental-0.2.3 (c (n "rental") (v "0.2.3") (h "0avs1ib77ydzz1qqb89g6a7qh0ibjw0dfq6q0y42ackg93bswl6y") (y #t)))

(define-public crate-rental-0.2.4 (c (n "rental") (v "0.2.4") (h "1hgc9bil535j5r6h5838djf85sfvkkmkjbsp34mb5rbaq89l8ls2")))

(define-public crate-rental-0.3.0 (c (n "rental") (v "0.3.0") (h "1x4vpzc8i7kri62nxc5y9qqpryd0m0x0f7qyz38yf0mf7w3kgdsd")))

(define-public crate-rental-0.3.1 (c (n "rental") (v "0.3.1") (h "0kvlsv3phikqga446q2pqy3jnf1khijxxyx4988sfiqmkvkdmrn6")))

(define-public crate-rental-0.3.2 (c (n "rental") (v "0.3.2") (h "0nwp3j2s5xg96gzsqsq9lzqabzp5pgfsqba1n7wzbl323yc498pa")))

(define-public crate-rental-0.3.3 (c (n "rental") (v "0.3.3") (h "0gbsgpgf8wicpha594d2apgsx1hhaivnicabb1rwk2x5cpyi5qpc")))

(define-public crate-rental-0.3.4 (c (n "rental") (v "0.3.4") (h "0x9771yka0zn886yqvfkjpnmqab50l1jnqg7yd11ifas1ajc90zn") (f (quote (("std") ("default" "std"))))))

(define-public crate-rental-0.3.5 (c (n "rental") (v "0.3.5") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1ppavh8i0cnzx3r3adshg45xigxhycbii556hrjdnv7qyhzy7aqy") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.3.6 (c (n "rental") (v "0.3.6") (d (list (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1nhl4sbb7ws3izrzfri561lnqjj6ybbx8dl9yd7gmbsvr96kqq5n") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.0 (c (n "rental") (v "0.4.0") (d (list (d (n "rental-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0d7cscnfypi7x6vfwiwkhyf1qy90ahsdr8sraqhr71qlv70516sc") (f (quote (("std" "stable_deref_trait/std") ("default" "std")))) (y #t)))

(define-public crate-rental-0.4.1 (c (n "rental") (v "0.4.1") (d (list (d (n "rental-impl") (r "= 0.4.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1xdyapzwwqykv92lfy48nvf3ix3g8p4bwx8plzai819vczywx4iy") (f (quote (("std" "stable_deref_trait/std") ("default" "std")))) (y #t)))

(define-public crate-rental-0.4.2 (c (n "rental") (v "0.4.2") (d (list (d (n "rental-impl") (r "= 0.4.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1rphcbd160w51ks714az8vni8272rh73i28024bpg8fyp55sys99") (f (quote (("std" "stable_deref_trait/std") ("default" "std")))) (y #t)))

(define-public crate-rental-0.4.3 (c (n "rental") (v "0.4.3") (d (list (d (n "rental-impl") (r "= 0.4.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1x7ca30kxb3i2kj5jli5vjivvdqb6vncqki60zr32xs00l41hdxv") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.4 (c (n "rental") (v "0.4.4") (d (list (d (n "rental-impl") (r "= 0.4.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1zqlhddyvm7zsk6hdbdzmghidrxvir6zwjfm03h9jr1calw2hc59") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.5 (c (n "rental") (v "0.4.5") (d (list (d (n "rental-impl") (r "= 0.4.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0ac0mc4hxw1rjnv6p03pfjzrw6i27k59nsdc9mwmsbl9lqdqning") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.6 (c (n "rental") (v "0.4.6") (d (list (d (n "rental-impl") (r "= 0.4.6") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1ljmzi8bfh9fwbaf3lzjh2j9sc6mrn3mg1xalgikw37a9xn56723") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.7 (c (n "rental") (v "0.4.7") (d (list (d (n "rental-impl") (r "= 0.4.7") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "07d6nkg3nilifzy3b3r98a1vcyvwx9bgjaw4072ly0bjy8vw52sw") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.8 (c (n "rental") (v "0.4.8") (d (list (d (n "rental-impl") (r "= 0.4.8") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0fcx26n7wfm685wqkl0n0fv9z2g9ysd4abk6i2kqc7fi66q5ql39") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.9 (c (n "rental") (v "0.4.9") (d (list (d (n "rental-impl") (r "= 0.4.9") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0240my0sqs7ivykfcpws8293yqbnv5r39hp7isyyaq8q3z42xc2z") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.10 (c (n "rental") (v "0.4.10") (d (list (d (n "rental-impl") (r "= 0.4.10") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0d7c05gdfi39hff7gy1pqv481jl3q0wci44ha5bzimzrpkfvpfzj") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.11 (c (n "rental") (v "0.4.11") (d (list (d (n "rental-impl") (r "= 0.4.11") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1rm1qkwpby404rggdwpa8vf9nzhqvmpbpxxb8f8bkw9jz9dni2w7") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.12 (c (n "rental") (v "0.4.12") (d (list (d (n "rental-impl") (r "= 0.4.12") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0qydslfs3gzhi2yvjm3agj0cq6i7wg9vp1xxf97qf7c7vjxvqq5g") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.13 (c (n "rental") (v "0.4.13") (d (list (d (n "rental-impl") (r "= 0.4.13") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "123aai3qlbgx9hj2l95jmif0skj5dx3jwdqqldavhni1k9ybd43f") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.14 (c (n "rental") (v "0.4.14") (d (list (d (n "rental-impl") (r "= 0.4.14") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "038ky7mjjk0m6apjkmc4f4ksbmlw9pyzmaqhsmjjg56yrkysnqlr") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.15 (c (n "rental") (v "0.4.15") (d (list (d (n "rental-impl") (r "= 0.4.15") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0qybh3dx8mm13q33q73d68bl1wbp15h0sffxddbanj530cgyh9vx") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4.16 (c (n "rental") (v "0.4.16") (d (list (d (n "rental-impl") (r "= 0.4.15") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0vh3pm4aykrn12la7nxfs53lpbsd4h4nxjljz6x2kff5rfzxb67h") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.0 (c (n "rental") (v "0.5.0") (d (list (d (n "rental-impl") (r "= 0.5.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0yaik4kwdyf5iimv00w8iqakxcam5sllxqr0mp1d2cljdrprdvkp") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.1 (c (n "rental") (v "0.5.1") (d (list (d (n "rental-impl") (r "= 0.5.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0blqq2hmzj59zcs605mx69r7mrqaz1zm2mkl665h6f7b48xhn7y4") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.2 (c (n "rental") (v "0.5.2") (d (list (d (n "rental-impl") (r "= 0.5.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0m7pnlq4ywmfygmca0158gc9601sk7pvifzib6rhppz3k2dvy96a") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.3 (c (n "rental") (v "0.5.3") (d (list (d (n "rental-impl") (r "= 0.5.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "1501c9inwsa00c0bp6b6rgppyaazsncqzja734wnfbmhfpr24xfm") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.4 (c (n "rental") (v "0.5.4") (d (list (d (n "rental-impl") (r "= 0.5.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "07ry2b2y7mj2rk3i8bj0lcnbpxa7zai455fwlmw1ks62kyynx481") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.5 (c (n "rental") (v "0.5.5") (d (list (d (n "rental-impl") (r "= 0.5.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0zw4cxyqf2w67kl5v6nrab0hg6qfaf5n3n6a0kxkkcdjk2zdwic5") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5.6 (c (n "rental") (v "0.5.6") (d (list (d (n "rental-impl") (r "=0.5.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (k 0)))) (h "0bhzz2pfbg0yaw8p1l31bggq4jn077wslf6ifhj22vf3r8mgx2fc") (f (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

