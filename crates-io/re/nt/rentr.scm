(define-module (crates-io re nt rentr) #:use-module (crates-io))

(define-public crate-rentr-0.1.0 (c (n "rentr") (v "0.1.0") (d (list (d (n "notify") (r "^5.0.0-pre.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0ymdzbhcj8wdm5mlpnfc10ysv14cgncj8y0ibrzndxilvgr16975")))

(define-public crate-rentr-0.1.1 (c (n "rentr") (v "0.1.1") (d (list (d (n "notify") (r "^5.0.0-pre.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0qi7ykfdvws8kkrqrs2dx28dhpgh14z03px511jns6c3qv112ddk")))

(define-public crate-rentr-0.1.2 (c (n "rentr") (v "0.1.2") (d (list (d (n "notify") (r "^5.0.0-pre.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1mxdgf5w0klcpyk58shhn6kk5vh2sr9lqqw3dp3fyp77rjixngb4")))

(define-public crate-rentr-0.1.3 (c (n "rentr") (v "0.1.3") (d (list (d (n "notify") (r "^5.0.0-pre.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1cj0608yswn6a7ambxz7p8svq2xxn272a1c6mrfzdkv0v5izjvd0")))

