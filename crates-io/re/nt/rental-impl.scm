(define-module (crates-io re nt rental-impl) #:use-module (crates-io))

(define-public crate-rental-impl-0.0.1 (c (n "rental-impl") (v "0.0.1") (d (list (d (n "procedural-masquerade") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0xvrf6kpklk352klr9xy7zkwkqclk8h3hdws73czxx2li5aw1i")))

(define-public crate-rental-impl-0.4.0 (c (n "rental-impl") (v "0.4.0") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "081l2xqsgy6prklddrdgh5c6bxr3xxznlcq7w88knivb8rxnw4i3") (y #t)))

(define-public crate-rental-impl-0.4.1 (c (n "rental-impl") (v "0.4.1") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "113bmlmjfhjhqqr1bl4vcgf3yphv4r5iivyv3aagp4f4dlcvz4y6") (y #t)))

(define-public crate-rental-impl-0.4.2 (c (n "rental-impl") (v "0.4.2") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11s1223g5jiz53hfqs6ybrrmvjgl3l4ysh791cknc8ppksnwd25y") (y #t)))

(define-public crate-rental-impl-0.4.3 (c (n "rental-impl") (v "0.4.3") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1lyl0pmp6nwbvda4cgsvy81mvvpsh5dc7qw9199slap0y8amr4i4")))

(define-public crate-rental-impl-0.4.4 (c (n "rental-impl") (v "0.4.4") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0j9vc019g41i2izczlmdb1ly43b66n4s92jacgfnp9p98n04wvq1")))

(define-public crate-rental-impl-0.4.6 (c (n "rental-impl") (v "0.4.6") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11pc769y2nwjy7xwazx5yfswk17666hclf4xxkxkzsyn42ha1c5y")))

(define-public crate-rental-impl-0.4.7 (c (n "rental-impl") (v "0.4.7") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10km7m5qx6v6dbjb52mc50zfi7s0f5fbxpv8k00k80hsgsq031w7")))

(define-public crate-rental-impl-0.4.8 (c (n "rental-impl") (v "0.4.8") (d (list (d (n "procedural-masquerade") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1yis4sb87wn2xf8b1pq8hkgy3sc1g4xlypd63rcnlma1aggq4in2")))

(define-public crate-rental-impl-0.4.9 (c (n "rental-impl") (v "0.4.9") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1dv903vkbw2n0v1qkv0l0977b39f5k6cwrqdm7bppbx8diwlzni9")))

(define-public crate-rental-impl-0.4.10 (c (n "rental-impl") (v "0.4.10") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1a2f1533grq8lm6bbwjxxsamn34m7s5qmfdqkbym00kvdqr8k6ln")))

(define-public crate-rental-impl-0.4.11 (c (n "rental-impl") (v "0.4.11") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0xrk999hz7j61grq1ygxqs1689qf54qc38rzd0ngps6011qlb73n")))

(define-public crate-rental-impl-0.4.12 (c (n "rental-impl") (v "0.4.12") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1p5799rlxfnyixbyfki59d3r8wssr2x9i9l9n1mxjz9p482ykxra")))

(define-public crate-rental-impl-0.4.13 (c (n "rental-impl") (v "0.4.13") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1k19ys4g5n8hz9s9sxlmix9x8q01a1im6vb7pfang536ghykb8zx")))

(define-public crate-rental-impl-0.4.14 (c (n "rental-impl") (v "0.4.14") (d (list (d (n "procedural-masquerade") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0yx0kz9j581l4qz0153rwjpr3ps2djq8nf25f5hn64b6a5h70k3c")))

(define-public crate-rental-impl-0.4.15 (c (n "rental-impl") (v "0.4.15") (d (list (d (n "procedural-masquerade") (r "= 0.1.5") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1wbk9agjy9mq5bibcqsi6p0rlflxpws5ra2f2mjb5mzx6imzn96b")))

(define-public crate-rental-impl-0.5.0 (c (n "rental-impl") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1721kbjpvrh9lxr8pshw94krc7hpsxjqxv4yyrxwxram6b70wras")))

(define-public crate-rental-impl-0.5.2 (c (n "rental-impl") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.18") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.1") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0fl0fhrp1v3l37rpjn8q8rfl9k348nv523i6914amfwkkcx56sd2")))

(define-public crate-rental-impl-0.5.4 (c (n "rental-impl") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.4.18") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.1") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1djsnbzw57fkspjgjfm159qaw7z8k1y7w7qnvw49dgicrxa0s9l2")))

(define-public crate-rental-impl-0.5.5 (c (n "rental-impl") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1pj0qgmvwwsfwyjqyjxzikkwbwc3vj7hm3hdykr47dy5inbnhpj7")))

