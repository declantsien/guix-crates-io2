(define-module (crates-io re ra rerast_macros) #:use-module (crates-io))

(define-public crate-rerast_macros-0.1.0 (c (n "rerast_macros") (v "0.1.0") (h "0a5jyhq852vc51ngmi18pivpif1g31wmi9w9p19sbjm15fbqb23r")))

(define-public crate-rerast_macros-0.1.12 (c (n "rerast_macros") (v "0.1.12") (h "1zf54g1ys1vwlm3kx3hv14wwj7nsb5rz93fcndyax97rvh4gqyvz")))

