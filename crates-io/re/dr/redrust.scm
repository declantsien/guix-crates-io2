(define-module (crates-io re dr redrust) #:use-module (crates-io))

(define-public crate-redrust-0.1.0 (c (n "redrust") (v "0.1.0") (d (list (d (n "c2rust-bitfields") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1hrn7bsci9sh1v208yfgq3clvshmyaysff6dfbgmzw0b51gg720y")))

(define-public crate-redrust-0.1.1 (c (n "redrust") (v "0.1.1") (d (list (d (n "c2rust-bitfields") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "040i4pr7970fircvwyiwxx0233qlza42nbn7sjc2rsnr76qp7jps")))

