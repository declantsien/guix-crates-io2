(define-module (crates-io re dr redrock) #:use-module (crates-io))

(define-public crate-redrock-0.1.0 (c (n "redrock") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "rocksdb") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "0ls04g2djfhl7rcc60wisvq9mi5cafy2qw07chbfqcms8qnccx1f")))

(define-public crate-redrock-0.2.0 (c (n "redrock") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "rocksdb") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "0scwcvbnq6xplamch7kxahijakrkga8xlvmxamdmqkxdw8999jr3")))

(define-public crate-redrock-0.3.0 (c (n "redrock") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "rocksdb") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "0c52mp9l5rq8y11vfd7f84vdn0m1gnmbbn6kj85z771yyvgv1qml")))

(define-public crate-redrock-0.4.0 (c (n "redrock") (v "0.4.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "rocksdb") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "164s0n30yqvqv23wnxk9pkcr6r6qsfscnq4aj0xxzbvzn1hxkpha")))

(define-public crate-redrock-0.5.0 (c (n "redrock") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)))) (h "002q7w9afcmk71pmylgcg4k2s99gsvamq9ny19qgawzf9dlrwfqv")))

(define-public crate-redrock-0.5.1 (c (n "redrock") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)))) (h "0d83g0c04c0v85j0sp2769w58gay2yd7bwnmjll575ndxb1rylqp")))

(define-public crate-redrock-0.6.0 (c (n "redrock") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)))) (h "0kiaqpj26mzq2r4mgr90l4iwmhychblfch9kldd42yhm5x8741sz")))

