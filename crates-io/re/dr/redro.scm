(define-module (crates-io re dr redro) #:use-module (crates-io))

(define-public crate-redro-0.0.1 (c (n "redro") (v "0.0.1") (h "1zarf995k3x22lvlcilwgfydxw620267qpynz19mpp1w0ga10gcg") (y #t)))

(define-public crate-redro-0.0.2 (c (n "redro") (v "0.0.2") (h "04qbnxhkicdvp7xpr43bpyqznxp1pwrnff0r453kqqfh0hmdsxs0") (y #t)))

(define-public crate-redro-0.0.5 (c (n "redro") (v "0.0.5") (h "12s5kykimd2nb9swhqnsw6cnrg97fmp7w2npw41q2n6r83nway5z") (y #t)))

(define-public crate-redro-0.0.7 (c (n "redro") (v "0.0.7") (h "03dnjgvk269g8zx42wi55ka78r902c3wxawgads25faxabh3g1h1") (y #t)))

(define-public crate-redro-0.1.1 (c (n "redro") (v "0.1.1") (h "1n72kr2p5p0ni2nk176qk2q9adr5dqgqawm9ks8fz624zq79x0dc") (y #t)))

(define-public crate-redro-0.1.3 (c (n "redro") (v "0.1.3") (h "07jkmxbir3y3cq8dvp14036bppp66ci2dp681ga64bxcn897wjfh") (y #t)))

(define-public crate-redro-0.1.4 (c (n "redro") (v "0.1.4") (h "1l12sp2x2fqrbvqifabn963j4r6p40b7r0s245hyrs52jvq0mkww") (y #t)))

(define-public crate-redro-0.1.77 (c (n "redro") (v "0.1.77") (h "18lz7kg1b5p3v2izjs6awvfdyzrb3d2h7s4ssb4agcs1x8xr4ghb") (y #t)))

(define-public crate-redro-0.77.0 (c (n "redro") (v "0.77.0") (h "05vgcfcwp6pb6d0x5gpmm18aa7my51mxxzcykz9lrk5rmdlskjlw") (y #t)))

(define-public crate-redro-7.7.18 (c (n "redro") (v "7.7.18") (h "18jnk253qic67pv9i4nv99ywwqz7ihvj7nbvxpardzy6yafrcdmx") (y #t)))

(define-public crate-redro-2018.7.7 (c (n "redro") (v "2018.7.7") (h "186wx6jx187iv7dis79vgw281m1yp051d386zny207aqwn8fvsz1") (y #t)))

(define-public crate-redro-2019.12.13 (c (n "redro") (v "2019.12.13") (h "1k0isg9mkiy5g4spwrvzj0y5nh16c1a7x3j9x8di6v5amd7cgrxv") (y #t)))

(define-public crate-redro-9999.999.99 (c (n "redro") (v "9999.999.99") (h "0cs5pvyamj17v4qxbqbbzk38r4nv625qzxzjin5pr62mz6kw40jr") (y #t)))

(define-public crate-redro-9.9.9 (c (n "redro") (v "9.9.9") (h "0y1g9a8bjn2pa96z29asbvdwbyr9kl5nx56vwazlhjpdsjxaqkqx") (y #t)))

(define-public crate-redro-99999.99999.99999 (c (n "redro") (v "99999.99999.99999") (h "1qzd8scc6hjqg899mxjpicy6gn6a2zwkbdl70h6ii6r6waz963kd") (y #t)))

(define-public crate-redro-9999999.9999999.9999999 (c (n "redro") (v "9999999.9999999.9999999") (h "1i3ab8671fwxhd1q9j5r7wxhmxq85caygagn9lmvs4wlin7vw5bb") (y #t)))

(define-public crate-redro-999999999.999999999.999999999 (c (n "redro") (v "999999999.999999999.999999999") (h "188a7gksfsgg8k0zs6j872mk1pbqyy6ydz5fzr2vaykpwkcyzqf3")))

