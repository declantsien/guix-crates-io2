(define-module (crates-io re us reusable-vec) #:use-module (crates-io))

(define-public crate-reusable-vec-0.1.0 (c (n "reusable-vec") (v "0.1.0") (h "0i3djh1sfld53fqvf7sh6f64yj7jba7s26vcp3yzg629ck1iq6dx") (y #t)))

(define-public crate-reusable-vec-0.1.1 (c (n "reusable-vec") (v "0.1.1") (h "0ig0dz30lg4dxjq5d22qmj9qnfm25xad27pavgg5dknxyll4s4l1") (y #t)))

(define-public crate-reusable-vec-0.1.2 (c (n "reusable-vec") (v "0.1.2") (h "14v665zg8vvn1q8ax8flv2ddm8kxqbab8vs64zxx9pwlppc2n7cs")))

