(define-module (crates-io re us reustmann) #:use-module (crates-io))

(define-public crate-reustmann-0.1.0 (c (n "reustmann") (v "0.1.0") (d (list (d (n "colorify") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "rustyline") (r "^0.2") (d #t) (k 0)))) (h "1j619c2cbhqppxc4lkjmrp5vlpzic8s72pxdy6kwgi9rfy1n3xwq")))

(define-public crate-reustmann-0.1.1 (c (n "reustmann") (v "0.1.1") (d (list (d (n "colorify") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "rustyline") (r "^0.2") (d #t) (k 0)))) (h "06vzhyp4g0l3yqg5iqzb3aplr5hrf8piqsdxjv9sx0aj03n6i7bp")))

(define-public crate-reustmann-0.1.2 (c (n "reustmann") (v "0.1.2") (d (list (d (n "colorify") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "rustyline") (r "^0.2") (d #t) (k 0)))) (h "1la3jgbpm13xzkmcajqs3ijwmygddardvbiczmlykbxr49573yls")))

(define-public crate-reustmann-0.2.0 (c (n "reustmann") (v "0.2.0") (d (list (d (n "colorify") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^0.2") (d #t) (k 0)))) (h "1j3vkrg7gs3h4jrfszdifnyy0gsha6jkqv1y826hwlhf2al5kgvl")))

(define-public crate-reustmann-0.2.1 (c (n "reustmann") (v "0.2.1") (d (list (d (n "colorify") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^0.2") (d #t) (k 0)))) (h "1lsvrkzcwy6k2r70j1vgxnwh2gx9wrmjvjk3z2640gcxlknm4xac")))

