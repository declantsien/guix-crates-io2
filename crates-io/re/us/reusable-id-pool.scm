(define-module (crates-io re us reusable-id-pool) #:use-module (crates-io))

(define-public crate-reusable-id-pool-0.1.0 (c (n "reusable-id-pool") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "0hlpk1qn5aws38ljnlvv31lzizkfl83cah253zgzyimnnzismshb") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:linked-hash-map"))))))

(define-public crate-reusable-id-pool-0.1.1 (c (n "reusable-id-pool") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "0ikp7g3b637d6x1hs7ndh962z48idma2j2dxrr0705bcprahcrfg") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:linked-hash-map"))))))

(define-public crate-reusable-id-pool-0.1.2 (c (n "reusable-id-pool") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "1p2d781vaw4blpy819w9863iash6qy7wmba1knljyy21vd5iqqlb") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:linked-hash-map"))))))

