(define-module (crates-io re us reusable-fmt) #:use-module (crates-io))

(define-public crate-reusable-fmt-0.1.0 (c (n "reusable-fmt") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "11s06cw9imd3a1v81kra4jnxfayafp30yda0iwi4kxlp708hrygs")))

(define-public crate-reusable-fmt-0.1.1 (c (n "reusable-fmt") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "1gxpkd326mfpsnr5fix6ak22psnirs11mbklb1vzabcc6m0qi8cl")))

(define-public crate-reusable-fmt-0.1.2 (c (n "reusable-fmt") (v "0.1.2") (d (list (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "012vixi3dq4294frjdls6fpl9n0f9jkvp56p29r2iaxsml6dgc7h")))

(define-public crate-reusable-fmt-0.2.0 (c (n "reusable-fmt") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "0gf8r7sl04mfn9qh04jf8lj42xny0vxgw31m7dhfp7hryi2061q8") (f (quote (("std") ("default" "std"))))))

