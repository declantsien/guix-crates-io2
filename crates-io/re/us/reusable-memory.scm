(define-module (crates-io re us reusable-memory) #:use-module (crates-io))

(define-public crate-reusable-memory-0.1.0 (c (n "reusable-memory") (v "0.1.0") (h "19374997am6zh0q515mxp1c7x21ss2xf091pq642m7q1nfmivgdv")))

(define-public crate-reusable-memory-0.1.1 (c (n "reusable-memory") (v "0.1.1") (h "1pdyrpgvraspbnhf24dslssgcvr2mk1jpgpq33pwmr9aqdqvlqkx")))

(define-public crate-reusable-memory-0.2.0 (c (n "reusable-memory") (v "0.2.0") (h "0dnx8j2g7hh8m29dn86apwiicmalpblibzj3cd1l8aixvf7xgr5a")))

(define-public crate-reusable-memory-0.2.1 (c (n "reusable-memory") (v "0.2.1") (h "1mzc5l421bign615vnrrzp8cqmc41scyg6jbwqayxm3ngi1xpqw8")))

