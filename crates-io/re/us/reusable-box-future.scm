(define-module (crates-io re us reusable-box-future) #:use-module (crates-io))

(define-public crate-reusable-box-future-0.1.0 (c (n "reusable-box-future") (v "0.1.0") (h "1w8bsszx3axs8wgxg4rv9ifhf9vcn0hfvgyzgl54bpk04i8i0h8c")))

(define-public crate-reusable-box-future-0.2.0 (c (n "reusable-box-future") (v "0.2.0") (d (list (d (n "futures-executor") (r "^0.3.13") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1fcv62q81x2ahb9ccqcaq24d608acrsvfrwkpmgxipgv476n23hy")))

