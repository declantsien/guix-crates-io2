(define-module (crates-io re us reuse-notifications) #:use-module (crates-io))

(define-public crate-reuse-notifications-0.1.0 (c (n "reuse-notifications") (v "0.1.0") (d (list (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1pv3j7gn9ys57c3slxsa3hjv1069929x7sjk4cfrh9kn5wxv7ixg")))

(define-public crate-reuse-notifications-0.1.1 (c (n "reuse-notifications") (v "0.1.1") (d (list (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "126s3vvkh6qarkrfccwr8bqw6k0wyn6xnjgsppm1rx3l3ran2d4b")))

