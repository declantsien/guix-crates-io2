(define-module (crates-io re go regorust) #:use-module (crates-io))

(define-public crate-regorust-0.3.5 (c (n "regorust") (v "0.3.5") (h "18g15inggiq55v5nq1hyqi3wa6qm6k8ivps6sxrs0zf1wszpr35n")))

(define-public crate-regorust-0.3.8 (c (n "regorust") (v "0.3.8") (h "1l5r0rjbypn4215mwh27yqv9q67g8bzcicdn6lgz9ag42nv3rv0z")))

(define-public crate-regorust-0.3.9 (c (n "regorust") (v "0.3.9") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1pgmqyaxpls7c13zq267fy8c87g2w3g6q3rzp5c3ab2nrs11749f")))

(define-public crate-regorust-0.3.10 (c (n "regorust") (v "0.3.10") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "075h9q0lb7nlbrb5vif6li8jrrzxq2mvy5wzl5y11kyjmx82z25v")))

(define-public crate-regorust-0.3.11 (c (n "regorust") (v "0.3.11") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0v4yhxalajhsrhvqic9652v5889852bl4n86mhd979jcxffqhrwl")))

