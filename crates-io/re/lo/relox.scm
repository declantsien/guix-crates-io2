(define-module (crates-io re lo relox) #:use-module (crates-io))

(define-public crate-relox-0.1.0 (c (n "relox") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1s983vihvhbx0qp2mmiwis78d5d8nsk5j4za4zy3mnqv775gncla") (f (quote (("no_std") ("no_sanity_check") ("no_bounds_check") ("host" "compress") ("embedded_minimal" "no_std" "decompress" "no_bounds_check" "no_sanity_check") ("embedded" "no_std" "decompress") ("default" "compress" "decompress") ("decompress") ("compress" "byteorder/std"))))))

