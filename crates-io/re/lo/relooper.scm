(define-module (crates-io re lo relooper) #:use-module (crates-io))

(define-public crate-relooper-0.1.0 (c (n "relooper") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "067ixm6cvjc2kw6hqfgbxgih0cba1sw4pkd0h0jqmqndb7jkvbpg")))

