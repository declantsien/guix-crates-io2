(define-module (crates-io re lo relogic) #:use-module (crates-io))

(define-public crate-relogic-0.0.0 (c (n "relogic") (v "0.0.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.24.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "0cfbq5l2c4w5cwglpfcryr7fhy2mk8895jalp4sxr3zxlgd8yjm0") (f (quote (("bdl" "bevy/dynamic_linking"))))))

