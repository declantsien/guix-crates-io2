(define-module (crates-io re lo reloadable-state) #:use-module (crates-io))

(define-public crate-reloadable-state-0.1.0 (c (n "reloadable-state") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1") (d #t) (k 0)) (d (n "reloadable-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (k 0)))) (h "1s2csxr9nsw4x00a0rpqnwwrslrraal08qw9i6wzhl2vsiwfylrq")))

