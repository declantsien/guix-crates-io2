(define-module (crates-io re lo relog) #:use-module (crates-io))

(define-public crate-relog-0.0.1 (c (n "relog") (v "0.0.1") (h "0imn67zm37w9mp5v69gk3xnh4558wpjn6hj26q00xy1ibg9gmx7b")))

(define-public crate-relog-1.0.0 (c (n "relog") (v "1.0.0") (h "1inqrf6lixjy01ams34fhiw4zk1pqfx83kl4xrszyrjxs94nwvza")))

(define-public crate-relog-1.0.1 (c (n "relog") (v "1.0.1") (h "1r5ql27xyq2qg33sq27l2g99vv84dfifxvcwg2vdhbxgnskd3y3p")))

(define-public crate-relog-1.0.2 (c (n "relog") (v "1.0.2") (h "1805ic5g9pw3s0f9r8lqv61s0iz48311dp58wd43cqaihzlssaig")))

(define-public crate-relog-1.0.3 (c (n "relog") (v "1.0.3") (h "1b7vyvaqd4z9cmf92s4n79k2nxh2qnnp5v5csbxarxxzyz970l0v")))

(define-public crate-relog-1.0.4 (c (n "relog") (v "1.0.4") (h "0wkdlj2mxywskm8d015b6zpxlmb7rh92igscbllxinglyq9cyj8i")))

(define-public crate-relog-1.0.5 (c (n "relog") (v "1.0.5") (h "09a6vamy3cw5krvbjyydpz96vig917js219r0dq4jaca3w65amj3")))

(define-public crate-relog-1.0.6 (c (n "relog") (v "1.0.6") (h "0s9ghzxs2mjca83rknba0s9bagxha65xpk3vrih2ljjky534kw11")))

(define-public crate-relog-1.0.7 (c (n "relog") (v "1.0.7") (h "0iwmjg6yxxmyawc8jshabm794r7r5cqn2gbrjq3fjfi94d2d38ih")))

(define-public crate-relog-1.0.8 (c (n "relog") (v "1.0.8") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)))) (h "0kp5nji1sb3f292x5r598lclkgj6kz7zaasqkw7gwz9zqbkib493")))

(define-public crate-relog-1.0.9 (c (n "relog") (v "1.0.9") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)))) (h "1imaxfil1h78dz2iiwgz8xg1fpjnnw494v65gqmg8baxjz7gjhhb")))

(define-public crate-relog-1.0.10 (c (n "relog") (v "1.0.10") (h "1fjvaji2ag0sv4mzlbjr0qagfy478yx80fpkx1yrc1hbdzwwhnqp")))

(define-public crate-relog-1.0.11 (c (n "relog") (v "1.0.11") (h "0piy9pzpvg7wglksmgh3as8l5lgyf108dxny0bk924h457cdx66r")))

(define-public crate-relog-1.0.12 (c (n "relog") (v "1.0.12") (h "0a7l55yhvb6mwxigviakhp3gb3bb76afb61m091c45xzkg2121pa")))

(define-public crate-relog-1.0.13 (c (n "relog") (v "1.0.13") (h "0fwk3ysc123z0i09wjy3fvmic5b27f8ikvvrkipmisxlpyaawlcx")))

