(define-module (crates-io re lo reload_config) #:use-module (crates-io))

(define-public crate-reload_config-0.1.0 (c (n "reload_config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qr1hv95446n84j1xkxp8523qm5cdvj4blp91j4x2zy6pjpda21k")))

(define-public crate-reload_config-0.1.1 (c (n "reload_config") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fcqm0i81xv07v33bhs66y80xbcglvl9gl9lyygssz62bsfvai0s")))

(define-public crate-reload_config-0.1.2 (c (n "reload_config") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08h7ynwr257p3v37k8kdyigh9rpiha6l49c47c0j87qylanlpcs9")))

(define-public crate-reload_config-0.1.3 (c (n "reload_config") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ys5sspd84lh5hhny59ghqd6cj65v5s7ygyyyx5bs2z1b2gdvl5f")))

(define-public crate-reload_config-0.1.4 (c (n "reload_config") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00pls1qc73ziffq8ifp930y547ggyr4y8iw6cklzghp3m5zzcy1h")))

(define-public crate-reload_config-0.1.5 (c (n "reload_config") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ppm79qrjf1i4fl0lzcdx7nn7h8850838k98lfr3r7z4s6wn7f27")))

