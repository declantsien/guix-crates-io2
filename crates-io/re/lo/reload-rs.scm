(define-module (crates-io re lo reload-rs) #:use-module (crates-io))

(define-public crate-reload-rs-0.1.0 (c (n "reload-rs") (v "0.1.0") (d (list (d (n "bus") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "mount") (r "^0.2.1") (d #t) (k 0)) (d (n "notify") (r "^3.0.1") (d #t) (k 0)) (d (n "staticfile") (r "^0.3.1") (d #t) (k 0)))) (h "0dr1kdq6sdbnp41333iacpmf3j0vrc2x6blzqx50bgjnvg389p0v")))

