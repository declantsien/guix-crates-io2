(define-module (crates-io re s2 res2br) #:use-module (crates-io))

(define-public crate-res2br-1.0.0 (c (n "res2br") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "0mc7939q6k898qpr31f3k8b648i6aihsv3w47lr7w66qbkc5hqpn")))

(define-public crate-res2br-1.0.1 (c (n "res2br") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "11m1l8dlpav7pilczxfap9lkdqp82slk8qiarbj69a503d29g9jf")))

(define-public crate-res2br-1.2.1 (c (n "res2br") (v "1.2.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zcgbm1m56dgaf0zap9is4x5rbcn29kdxqcgcyl59lrqa3y8pvd5")))

(define-public crate-res2br-1.3.1 (c (n "res2br") (v "1.3.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04m6fmgcvfajjxj3lcrikp8qn3p35pbpcqa854998a0m3s8cfngp")))

(define-public crate-res2br-1.3.2 (c (n "res2br") (v "1.3.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)))) (h "16cgjc223y6ksipx4dac436i1hj40sq6s70pfim3145681wfmr87")))

(define-public crate-res2br-1.3.3 (c (n "res2br") (v "1.3.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)))) (h "1j6lpwigmb9pyvpsv1q37amnmwr8z14mink7zadbxa7q4axsqh12")))

(define-public crate-res2br-2.0.0 (c (n "res2br") (v "2.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1kfbbsmy1r2mbmliy2xhpxi8s9sbyxnfr63h9ixzy4zrgrd9xj8h")))

(define-public crate-res2br-2.0.1 (c (n "res2br") (v "2.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1j9nah8m6sa5vpxmhalaq00y2r2lj5c48jpd7ijkf36yz4fjz11f")))

(define-public crate-res2br-2.0.2 (c (n "res2br") (v "2.0.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "19a69zkhyhrnxf6xzg33d2x7bir3i2mfsmq1lz2x7ni7wilsq1jh")))

(define-public crate-res2br-2.0.3 (c (n "res2br") (v "2.0.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1ikslnfw9bgh7hbb9kz5fdcm5va85r7788cg9wbssii4r1hpdsl8")))

