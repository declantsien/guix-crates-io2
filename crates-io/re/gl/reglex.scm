(define-module (crates-io re gl reglex) #:use-module (crates-io))

(define-public crate-reglex-0.1.0 (c (n "reglex") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c07kkkrbmnnxncxns5s6gl7bd0ijbibv2jjxswpmwssv5lylypl")))

(define-public crate-reglex-1.0.0 (c (n "reglex") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v7njb1m0vnwj58lq507zv1i50x7ngv91abyjrapz1s4aw8x09jd")))

(define-public crate-reglex-1.0.1 (c (n "reglex") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a3i7pcaf0kpbgchd2nils6pix780a1gqrsxl08473m4k1pcp0af")))

(define-public crate-reglex-1.0.2 (c (n "reglex") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08c36kihy7k7bbixj9wvf3xaqarmw4zb9gmz7737k02i86sq9v2s")))

(define-public crate-reglex-1.1.0 (c (n "reglex") (v "1.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mfayq65s8qi8xjalm8swv1w2m79kb92kh9nfxllr70fdv85wwc0")))

