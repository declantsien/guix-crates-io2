(define-module (crates-io re li reliudp) #:use-module (crates-io))

(define-public crate-reliudp-0.1.0 (c (n "reliudp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "13z7sbjfa774hw25lf2hl15sq7hz88jyd06ysnrasfn36h8wr0cy")))

(define-public crate-reliudp-0.1.1 (c (n "reliudp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "061lahq5h97ix9m3h1r1a2fz8schlc2x66q1vj5bsxxqxzrpy22a")))

(define-public crate-reliudp-0.2.0 (c (n "reliudp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f80qm7nfz7h2xhlmj8rbqb0ns6ny71s58lcja5bwfwyp1yf9cnc")))

