(define-module (crates-io re np renparkn) #:use-module (crates-io))

(define-public crate-renparkn-0.1.0 (c (n "renparkn") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1wndm3zblyidjhc9x4m3q3zhsb4b8wsb9y8j65yyirr23ww29bq0")))

(define-public crate-renparkn-0.1.1 (c (n "renparkn") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0mjpbv8z5lq0w8rw81vg4wvw4snv24v684x23qyspbp0l8agczz2")))

