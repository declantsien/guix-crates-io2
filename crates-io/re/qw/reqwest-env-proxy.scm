(define-module (crates-io re qw reqwest-env-proxy) #:use-module (crates-io))

(define-public crate-reqwest-env-proxy-0.1.0 (c (n "reqwest-env-proxy") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (k 0)))) (h "09v1ly6d61a8bw4vcn8dy8g6p8vkjw080dys9zhf7d9gdif7iydq") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

