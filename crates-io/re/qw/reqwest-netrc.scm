(define-module (crates-io re qw reqwest-netrc) #:use-module (crates-io))

(define-public crate-reqwest-netrc-0.1.0 (c (n "reqwest-netrc") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 2)) (d (n "reqwest-middleware") (r "^0.2.4") (d #t) (k 0)) (d (n "rust-netrc") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5.22") (d #t) (k 2)))) (h "0x9pggby7mdzbmba245rhq1haski85srrqpwdh3hlj798rik995n")))

(define-public crate-reqwest-netrc-0.1.1 (c (n "reqwest-netrc") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 2)) (d (n "reqwest-middleware") (r "^0.2.4") (d #t) (k 0)) (d (n "rust-netrc") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5.22") (d #t) (k 2)))) (h "1nxnl2m6vjrinrp3axfikiaryfbjww147afyjyb8z5xjsj6cb87c")))

