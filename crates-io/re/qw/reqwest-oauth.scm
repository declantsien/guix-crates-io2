(define-module (crates-io re qw reqwest-oauth) #:use-module (crates-io))

(define-public crate-reqwest-oauth-0.10.4 (c (n "reqwest-oauth") (v "0.10.4") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)))) (h "1md3q6lzypknfv1vnfqahhbs8y7vj5qvkqmlcqdszgx63phi36ii") (y #t)))

