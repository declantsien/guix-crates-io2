(define-module (crates-io re qw reqwest_resume) #:use-module (crates-io))

(define-public crate-reqwest_resume-0.1.0 (c (n "reqwest_resume") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1qb44icgm6ni0446six49d7r7v18b88g886haj352slzyfa9m82p")))

(define-public crate-reqwest_resume-0.2.0 (c (n "reqwest_resume") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1qnz5jhr4lzr8k8q76183xw5aciphyrr5jjrmyx2cs278sxxjk9b")))

(define-public crate-reqwest_resume-0.2.1 (c (n "reqwest_resume") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1cx1wjy6x7azcc33sbfa5b7nj05620saaji9ygzrqqg0bagqmyqw")))

(define-public crate-reqwest_resume-0.3.0 (c (n "reqwest_resume") (v "0.3.0") (d (list (d (n "async-compression") (r "^0.3") (f (quote ("gzip" "futures-bufread"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyperx") (r "^1.0") (f (quote ("headers"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (k 2)))) (h "03z2xsapkygr4bvpfdz579nv3rjrvlhvqarix7lyymq0a5h7yrcc")))

(define-public crate-reqwest_resume-0.3.1 (c (n "reqwest_resume") (v "0.3.1") (d (list (d (n "async-compression") (r "^0.3.3") (f (quote ("gzip" "futures-bufread"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyperx") (r "^1.0") (f (quote ("headers"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (k 2)))) (h "0grg1x3c6zs1151lp0zwj8168a6lma3hnl6d4idbcsn8m72svfn9")))

(define-public crate-reqwest_resume-0.3.2 (c (n "reqwest_resume") (v "0.3.2") (d (list (d (n "async-compression") (r "^0.3.3") (f (quote ("gzip" "futures-bufread"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyperx") (r "^1.0") (f (quote ("headers"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (k 2)))) (h "0rnpfkd7xlq4kwrc6rpv60nic8gkxxw22ha5r4r79qfr0kxxpj4j")))

