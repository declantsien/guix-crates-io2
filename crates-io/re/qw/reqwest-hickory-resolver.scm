(define-module (crates-io re qw reqwest-hickory-resolver) #:use-module (crates-io))

(define-public crate-reqwest-hickory-resolver-0.0.1 (c (n "reqwest-hickory-resolver") (v "0.0.1") (d (list (d (n "hickory-resolver") (r "^0.24.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "tcp"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)))) (h "0wn9a3isn5m4gmg2j9mg522jfnp7fy8qljyq24dqi1dgl88qigxv")))

(define-public crate-reqwest-hickory-resolver-0.0.2 (c (n "reqwest-hickory-resolver") (v "0.0.2") (d (list (d (n "hickory-resolver") (r "^0.24.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "tcp"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)))) (h "0vnsvigg3by8p0js86l05zjxqhkaj1nv4qp5cbxfs6hpcrd85idc")))

(define-public crate-reqwest-hickory-resolver-0.1.0 (c (n "reqwest-hickory-resolver") (v "0.1.0") (d (list (d (n "hickory-resolver") (r "^0.24") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (k 0)))) (h "0zp9wlcq41cfdvkj6ml4r8mvb2lx77pwhspz9ds52pkvjwcn070s")))

