(define-module (crates-io re qw reqwest-chain) #:use-module (crates-io))

(define-public crate-reqwest-chain-0.1.0 (c (n "reqwest-chain") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (k 0)) (d (n "reqwest-middleware") (r "^0.2.0") (d #t) (k 0)) (d (n "task-local-extensions") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5.15") (d #t) (k 2)))) (h "0f2fbw03zy4lkygqiq3g595d9y7p8bwkn02hg29iby37ll8h39am")))

(define-public crate-reqwest-chain-0.2.0 (c (n "reqwest-chain") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (k 0)) (d (n "reqwest-middleware") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.6") (d #t) (k 2)))) (h "1vps1qmx1rvmz5nmdzvhm61800bpbrva47d0n5gsmskdf2ygd6kw")))

