(define-module (crates-io re qw reqwest-graphql) #:use-module (crates-io))

(define-public crate-reqwest-graphql-1.0.0 (c (n "reqwest-graphql") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0dh190lw7zirhvxryc7ml3v00mcm2h7svp491b0fa374zr4gzaik")))

