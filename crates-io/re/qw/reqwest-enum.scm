(define-module (crates-io re qw reqwest-enum) #:use-module (crates-io))

(define-public crate-reqwest-enum-0.1.0 (c (n "reqwest-enum") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0mzx2zv5wn0x7xndiq4nh82nab8br7avgwx36dkkajjicgin8yj0") (f (quote (("jsonrpc") ("default" "jsonrpc"))))))

(define-public crate-reqwest-enum-0.1.1 (c (n "reqwest-enum") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1z7ibh1ngnj3acw4jhd0crssm32v90zb8hbbq448xrplbibl8x65") (f (quote (("default" "jsonrpc")))) (s 2) (e (quote (("jsonrpc" "dep:futures"))))))

(define-public crate-reqwest-enum-0.2.0 (c (n "reqwest-enum") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "03ql0i7ypzhxqggq8yb100lmn6118m705g23l78bix3gkch4dfsf") (f (quote (("default" "jsonrpc")))) (s 2) (e (quote (("jsonrpc" "dep:futures"))))))

