(define-module (crates-io re qw reqwest-wasm-ext) #:use-module (crates-io))

(define-public crate-reqwest-wasm-ext-0.1.0 (c (n "reqwest-wasm-ext") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0n3qrw0lgwxvazysbs76230qanmx9m9imxa77j3k793mknldxqhr")))

