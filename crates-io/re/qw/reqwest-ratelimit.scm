(define-module (crates-io re qw reqwest-ratelimit) #:use-module (crates-io))

(define-public crate-reqwest-ratelimit-0.1.0 (c (n "reqwest-ratelimit") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "reqwest-middleware") (r "^0.2") (d #t) (k 0)) (d (n "task-local-extensions") (r "^0.1") (d #t) (k 0)))) (h "0n7bksdjrika70ky343xarg80qcgl408a2793q29bvgmr3nfhm9b")))

(define-public crate-reqwest-ratelimit-0.1.1 (c (n "reqwest-ratelimit") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "reqwest-middleware") (r "^0.2") (d #t) (k 0)) (d (n "task-local-extensions") (r "^0.1") (d #t) (k 0)))) (h "01jkqbjpkz854bjazgsn0n2a9qv1zv6ippcwgabndr6dmz72pwyd")))

(define-public crate-reqwest-ratelimit-0.2.0 (c (n "reqwest-ratelimit") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.0") (k 0)) (d (n "reqwest-middleware") (r "^0.3.0") (d #t) (k 0)))) (h "11wcxf0cbhdy96wmgv53hv6kkaxg435adkzi8h9swz6sbq977ipj")))

