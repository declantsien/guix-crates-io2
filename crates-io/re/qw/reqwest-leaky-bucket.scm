(define-module (crates-io re qw reqwest-leaky-bucket) #:use-module (crates-io))

(define-public crate-reqwest-leaky-bucket-0.1.0 (c (n "reqwest-leaky-bucket") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "leaky-bucket") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 2)) (d (n "reqwest-middleware") (r "^0.2") (d #t) (k 2)) (d (n "reqwest-ratelimit") (r "^0.1") (d #t) (k 0)))) (h "087w1x9h2h76w14z870hs8r2zxf3nirmizyg5kwxs4yhpvigfm99")))

(define-public crate-reqwest-leaky-bucket-0.2.0 (c (n "reqwest-leaky-bucket") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "leaky-bucket") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (k 2)) (d (n "reqwest-middleware") (r "^0.3") (d #t) (k 2)) (d (n "reqwest-ratelimit") (r "^0.2") (d #t) (k 0)))) (h "1v8c9i3dz4zqrlks9iz12ycilbrasyjwi3scr23r48628n64dq2l")))

