(define-module (crates-io re qw reqwest-pretty-json) #:use-module (crates-io))

(define-public crate-reqwest-pretty-json-0.1.1 (c (n "reqwest-pretty-json") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qybmdjb66ca21g609h0vkrbs43wdc0acq4a8hbrddx24619kg69")))

(define-public crate-reqwest-pretty-json-0.1.2 (c (n "reqwest-pretty-json") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wjz9ih78qcxcnd6362sym74w57ddb289h2licv0rjcf8gf8hdba")))

(define-public crate-reqwest-pretty-json-0.1.3 (c (n "reqwest-pretty-json") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vwmkpn3nv83nqll3s9r255szzp280293zbxwhjq639m10a75xgb")))

(define-public crate-reqwest-pretty-json-0.9.0 (c (n "reqwest-pretty-json") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04w19chdm0a6z8cp5m5vh56gkj3b2hbwci8fk35msk4mddmq10s9")))

(define-public crate-reqwest-pretty-json-0.10.0 (c (n "reqwest-pretty-json") (v "0.10.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (k 2)))) (h "0wgwrfz3j9xbl31pxsq3c3p8a8aca46yn1fc0551zvahk0pq7gif") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.11.0 (c (n "reqwest-pretty-json") (v "0.11.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (k 2)))) (h "0vp35nrh3j4drxccilcx8817i0a3qvihg62kz5z9h3dlsgbafjma") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.10.1 (c (n "reqwest-pretty-json") (v "0.10.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (k 2)))) (h "1arg400qfsrgzvi0qz8sazw7n80x4r80nbvpbds285735mbw4513") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.11.1 (c (n "reqwest-pretty-json") (v "0.11.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (k 2)))) (h "0qz8pvvjwnfvw3wvffyv6ila8dqhxkhs7h4am8xzq6qb1v3ly54q") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.12.0 (c (n "reqwest-pretty-json") (v "0.12.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (k 2)))) (h "18051p5qwqa8vmf2dgvdifqa709k06fcn7bgiyl58cacb0inzjwk") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.12.1 (c (n "reqwest-pretty-json") (v "0.12.1") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (k 2)))) (h "1g6inabap9g3i2dg2pa22q1wjdgccnw4v0a6f60dmsbskgccfwz8") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-reqwest-pretty-json-0.12.2 (c (n "reqwest-pretty-json") (v "0.12.2") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (k 2)))) (h "1jgz6dp8xjp1x8zsh27q87nql6jfqpr4sh7swv6frd5sgzm16gwx") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

