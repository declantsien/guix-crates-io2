(define-module (crates-io re ed reedline_sql_highlighter) #:use-module (crates-io))

(define-public crate-reedline_sql_highlighter-0.1.0 (c (n "reedline_sql_highlighter") (v "0.1.0") (d (list (d (n "nu-ansi-term") (r "^0.50") (d #t) (k 0)) (d (n "reedline") (r "^0.30") (d #t) (k 0)) (d (n "sqlparser") (r "^0.44.0") (d #t) (k 0)))) (h "0nbxncpmirrpwg23rrcfqwwdw8cd8x439k11j642h1x6q4rdrdm7")))

