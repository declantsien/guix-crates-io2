(define-module (crates-io re ed reed) #:use-module (crates-io))

(define-public crate-reed-0.1.0 (c (n "reed") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-derive") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "rdkafka") (r "^0.23") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros" "stream"))) (d #t) (k 0)) (d (n "tonic") (r "^0.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0k0wxjr4w4gdb4w4zf01ks8f7ic08w504q78fs37p8q7m3yscf9w")))

