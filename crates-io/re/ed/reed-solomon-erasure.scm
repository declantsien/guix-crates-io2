(define-module (crates-io re ed reed-solomon-erasure) #:use-module (crates-io))

(define-public crate-reed-solomon-erasure-0.9.0 (c (n "reed-solomon-erasure") (v "0.9.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zaa4j71z0mjcvscxaprns0pk3k52fbf1hp0yfphs30dpgbbk7cg")))

(define-public crate-reed-solomon-erasure-0.9.1 (c (n "reed-solomon-erasure") (v "0.9.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "176y02f2c4fd8qnw3lasvngkirj4q2birbzqlqf2qq6d6qp5k3j8")))

(define-public crate-reed-solomon-erasure-1.0.0 (c (n "reed-solomon-erasure") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ll9xvll9nsd0mf1bf8vm1dfjmq72w7y5xhgai9g12jrxicniwm4")))

(define-public crate-reed-solomon-erasure-1.0.1 (c (n "reed-solomon-erasure") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0c0x91q3b145b4ar8kvwqjivw4x4bk9kb3mnvg2nj4fwly424qav")))

(define-public crate-reed-solomon-erasure-1.1.0 (c (n "reed-solomon-erasure") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kyqxmvwf1806ba223xzk1fkm032scf8f46bf93hlqplq7g32xdm")))

(define-public crate-reed-solomon-erasure-2.0.0 (c (n "reed-solomon-erasure") (v "2.0.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0a046aikh5mcfv6hqqvxlhjp7lf26c2nn2ji9cmd5ybmsz4n0wpw")))

(define-public crate-reed-solomon-erasure-2.1.0-beta (c (n "reed-solomon-erasure") (v "2.1.0-beta") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0k9khxrh0ydbl22z6m3f5pjflvnkzr5h5gs2qgkk9prd6211dznx") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.1.0 (c (n "reed-solomon-erasure") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "12s4b1wr7md3z89x1fq84cgbic7p3kznxpf79kl9znxrrw7ssdqj") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.2.0 (c (n "reed-solomon-erasure") (v "2.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0lprspn80hiifrry8xm6sgf8anvddjn7s4m56h9sq43a8pk98b7a") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.3.0 (c (n "reed-solomon-erasure") (v "2.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0fng1km4r8lqcl3rh2cfh8474cyvmdiwsj97mvl62b0q5f7j03dz") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.3.1 (c (n "reed-solomon-erasure") (v "2.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "12m5ypjysc76cwrb8zjydbghbgz82dxfnfr7s1rhjm8q962883ip") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.3.2 (c (n "reed-solomon-erasure") (v "2.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1wwx3cr4chlnvi5w4ypzfsz8kinl995kv6cmk6g7gjjp3qmddif5") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.3.3 (c (n "reed-solomon-erasure") (v "2.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0r93plchyw54rnxapxsl7a6lwl3fai1kirvhwfwpixn72icgs8bz") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-2.4.0 (c (n "reed-solomon-erasure") (v "2.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1f461y94yjr5s6zxmrg123icsfi7r7mk2wb68as8c10gg3k19zn5") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-3.0.0 (c (n "reed-solomon-erasure") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0k020djih4s3x0dpsnvwxih0vvigjcmqsk18j4rywpklnv8xxvpf") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-3.0.1 (c (n "reed-solomon-erasure") (v "3.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1ddfijyxj84yhzbbag79224nx33v3883f21k0aahxdc6nyi04zk6") (f (quote (("pure-rust")))) (y #t)))

(define-public crate-reed-solomon-erasure-3.0.2 (c (n "reed-solomon-erasure") (v "3.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1zykiaird0hvq04m36vq8970l85jynm1jmavspc56j6gllgw0bw8") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-3.0.3 (c (n "reed-solomon-erasure") (v "3.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1zxjv5l95pdxkybmz5142h62fxvg2djlslr576jcfmz0l91h72qh") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-3.1.0 (c (n "reed-solomon-erasure") (v "3.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1myyzxgf26mrrfw45wzap0jlcdl9pm0p95wr2l2zxl0n73wxzaig") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-3.1.1 (c (n "reed-solomon-erasure") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0drw0j5grxqxw5sk985jzwqhf20h3dd2ax4ywigk8gpm096bvjvp") (f (quote (("pure-rust"))))))

(define-public crate-reed-solomon-erasure-4.0.0 (c (n "reed-solomon-erasure") (v "4.0.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "05m0ml77rxrm56fs5lznyy4zqgga1ak5z1nmjwaz9hsx6zv4xryq") (f (quote (("simd-accel" "cc" "libc") ("default"))))))

(define-public crate-reed-solomon-erasure-4.0.1 (c (n "reed-solomon-erasure") (v "4.0.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1264rr8f7kz7l4ds1p97px006gfnz8pykqb72kv2gz5mgphk3iy2") (f (quote (("simd-accel" "cc" "libc") ("default"))))))

(define-public crate-reed-solomon-erasure-4.0.2 (c (n "reed-solomon-erasure") (v "4.0.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)))) (h "179gcq2frkdvrcsljj9paxkxlri592isaa9370hl4pbwvl9s05d4") (f (quote (("simd-accel" "cc" "libc") ("default"))))))

(define-public crate-reed-solomon-erasure-5.0.0 (c (n "reed-solomon-erasure") (v "5.0.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (f (quote ("spin_mutex"))) (k 0)))) (h "1ckx1h8vv78fffmy0h8m33wbfgh46h29nv4jdhkd0zyv797jgvwi") (f (quote (("std" "parking_lot") ("simd-accel" "cc" "libc") ("default" "std"))))))

(define-public crate-reed-solomon-erasure-5.0.1 (c (n "reed-solomon-erasure") (v "5.0.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (f (quote ("spin_mutex"))) (k 0)))) (h "1nr46038m2xp3i7w6dnx691bq44ba6kal36z07hl2s9hv30blw3i") (f (quote (("std" "parking_lot") ("simd-accel" "cc" "libc") ("default" "std"))))))

(define-public crate-reed-solomon-erasure-5.0.2 (c (n "reed-solomon-erasure") (v "5.0.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (f (quote ("spin_mutex"))) (k 0)))) (h "1fldnxswk8b3v883glcx3pbahj85chf988gsrrrfr2m2xjpl44sj") (f (quote (("std" "parking_lot") ("simd-accel" "cc" "libc") ("default" "std"))))))

(define-public crate-reed-solomon-erasure-5.0.3 (c (n "reed-solomon-erasure") (v "5.0.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (f (quote ("spin_mutex"))) (k 0)))) (h "0hc692nb3v8p8y9rd85q1dd3yb41p34311szyqrqnjv85d2k3zn2") (f (quote (("std" "parking_lot") ("simd-accel" "cc" "libc") ("default" "std"))))))

(define-public crate-reed-solomon-erasure-6.0.0 (c (n "reed-solomon-erasure") (v "6.0.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "lru") (r "^0.7.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (f (quote ("spin_mutex"))) (k 0)))) (h "0kqdjvrvb24iivi1wfjhjjm99a3msiiad8j3bm84yk8da0ykfqvj") (f (quote (("std" "parking_lot") ("simd-accel" "cc" "libc") ("default" "std"))))))

