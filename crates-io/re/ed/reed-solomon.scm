(define-module (crates-io re ed reed-solomon) #:use-module (crates-io))

(define-public crate-reed-solomon-0.1.0 (c (n "reed-solomon") (v "0.1.0") (h "0cx3yl4ksxrgfv8va2qg8iwh3vbg9rn7jdn9a4dk37a4fkwgxsph")))

(define-public crate-reed-solomon-0.1.1 (c (n "reed-solomon") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1wfpsbvnyw6ns1574kvsi61hjnj7p01apnwgy86r00vr0mlj64sv")))

(define-public crate-reed-solomon-0.1.2 (c (n "reed-solomon") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1si136jap5vbajw59223zfgi61njglxzxpa9775cfnn0v1dc1idf")))

(define-public crate-reed-solomon-0.1.3 (c (n "reed-solomon") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1pib7byn14m7d7kwr08s9qiwj0c97r52y81jmldgjmpjvn67bs2c") (f (quote (("unsafe_indexing") ("default" "unsafe_indexing"))))))

(define-public crate-reed-solomon-0.1.4 (c (n "reed-solomon") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1ksjb86y8qi0x2mk57bb17pjvjsg6qvy6k90mvwabcqfbmgn37bw") (f (quote (("unsafe_indexing"))))))

(define-public crate-reed-solomon-0.1.5 (c (n "reed-solomon") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0y88y8gssaz7sylg4zm0glsjn3nyz0cwbja9azlia9xzsbmmjxhm") (f (quote (("unsafe_indexing")))) (y #t)))

(define-public crate-reed-solomon-0.1.6 (c (n "reed-solomon") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1sai3yvqj3xcpsyls0wzxz3j89g9bjl6fd1sxxxvf84swk4wzs2s") (f (quote (("unsafe_indexing"))))))

(define-public crate-reed-solomon-0.2.0 (c (n "reed-solomon") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0q8mzrivk8s2h50z8rxd4mcn5sn1ys51rsxlndyxk1rq4kna1nwg") (f (quote (("unsafe_indexing"))))))

(define-public crate-reed-solomon-0.2.1 (c (n "reed-solomon") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0m7fd7dx2qwx9drkpi40jfqg1hmppv475b22aj43azx7fz46iphk") (f (quote (("unsafe_indexing"))))))

