(define-module (crates-io re ed reed-solomon-32) #:use-module (crates-io))

(define-public crate-reed-solomon-32-1.0.0 (c (n "reed-solomon-32") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1kq6d050rdvi5s5nkbnr10vl54hl57pcp0ijciz0an416w0gc402") (f (quote (("std") ("default" "std"))))))

(define-public crate-reed-solomon-32-2.0.0 (c (n "reed-solomon-32") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vmnb3h7via16z5i8n3xc85a9i6njlj5vfq8x50lisj45kvghrj0") (f (quote (("std") ("default" "std"))))))

(define-public crate-reed-solomon-32-2.0.1 (c (n "reed-solomon-32") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "005gsnp4v3wx3mjgns3bbr00zgzqdvx2w4a212blj0w3cil979xr") (f (quote (("std") ("default" "std"))))))

(define-public crate-reed-solomon-32-2.0.2 (c (n "reed-solomon-32") (v "2.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "17jhyx05divd9vx1k6h7011l9fxagdqasc90jm99i0qj811iq20q") (f (quote (("std") ("default" "std"))))))

