(define-module (crates-io re ed reed-solomon-cx) #:use-module (crates-io))

(define-public crate-reed-solomon-cx-0.1.0 (c (n "reed-solomon-cx") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1c61wvzxrdhgn8qvddyxc9i5xj5783r0mrrz94v5j3jqmcq9hjmx")))

(define-public crate-reed-solomon-cx-0.1.1 (c (n "reed-solomon-cx") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1ywlvrbxm67dp177cq8dxish3d5326wlzxyaib5mg8hyqws6ml1c")))

