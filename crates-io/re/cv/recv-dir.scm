(define-module (crates-io re cv recv-dir) #:use-module (crates-io))

(define-public crate-recv-dir-0.1.2 (c (n "recv-dir") (v "0.1.2") (h "1dg5p8k1vh473m0r9z6pchg503scilh56ncnallc299d111l7gix")))

(define-public crate-recv-dir-0.1.3 (c (n "recv-dir") (v "0.1.3") (h "0lbfpg13kflmwrwv2m2c9hvy7fll8fghnlqm7869msgi0r6j1g1s")))

(define-public crate-recv-dir-0.2.0 (c (n "recv-dir") (v "0.2.0") (h "1k8jsim1r70hyj8hsc2gk4azfx5xbks8fhdvsmh9fi9y2gd9fs80")))

(define-public crate-recv-dir-0.2.2 (c (n "recv-dir") (v "0.2.2") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0crdql6nx2yqqfwdanxppswcsj10m6s3zslv397vxl56qfpi9gcq") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.2.3 (c (n "recv-dir") (v "0.2.3") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09f0v4qqxb60hhrdk2di8j405rl0v43gd9rjrdfl34hlkzml21pg") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.3.0 (c (n "recv-dir") (v "0.3.0") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0iwxj7v1j4a5y2rcx5f8xqd29p8cn24szdv44wkwv0hgah2g44dh") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.3.1 (c (n "recv-dir") (v "0.3.1") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hh627vbrr3r0yn5ii72s38iz5x8dm0jfj066pfwj203ahpnbq4z") (f (quote (("default") ("async" "tokio"))))))

