(define-module (crates-io re mo remove_comments) #:use-module (crates-io))

(define-public crate-remove_comments-0.1.0 (c (n "remove_comments") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1cxbsam34qgshanb2xjwqf8x75gknddlnm09hqzqz4z6g2ilvywa")))

