(define-module (crates-io re mo remove_empty_query_params) #:use-module (crates-io))

(define-public crate-remove_empty_query_params-0.1.0 (c (n "remove_empty_query_params") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0zdyhbi42jcpdxxprbxm1p0bipjwyqdq3srkgch1ca9vycaf5q9g")))

(define-public crate-remove_empty_query_params-0.1.1 (c (n "remove_empty_query_params") (v "0.1.1") (d (list (d (n "form_urlencoded") (r "^1.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "1ylizwbijqlzljff2caszgg54yckgahzgvgs5kn9vsrhy3x0iafg")))

