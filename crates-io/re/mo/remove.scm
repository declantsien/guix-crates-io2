(define-module (crates-io re mo remove) #:use-module (crates-io))

(define-public crate-remove-0.1.0 (c (n "remove") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "06bxaqfwnwshhv8djsv6d76am151h4ykhzlpg19yhnrgdbx39asc")))

(define-public crate-remove-0.1.1 (c (n "remove") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "1lv8jb2v4yid0297nxirvavq2zn96ca4bcaq6xb80xjwy8fh7d4m")))

(define-public crate-remove-0.1.2 (c (n "remove") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "038qramza66rj007ripz5bjn35zp1wph5a4xsmcbk0bjc7c1wy92")))

(define-public crate-remove-0.1.3 (c (n "remove") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.2") (d #t) (k 0)))) (h "092xwz0fzysisbjhjmynwgswsqbwfqyr0wasaxc5za3rj3hmdcam")))

