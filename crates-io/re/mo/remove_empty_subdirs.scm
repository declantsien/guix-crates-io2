(define-module (crates-io re mo remove_empty_subdirs) #:use-module (crates-io))

(define-public crate-remove_empty_subdirs-0.1.0 (c (n "remove_empty_subdirs") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "07870660jmygmpjj5i11h6by9v76wnd0bir6p89f6hql3cahbx0g")))

(define-public crate-remove_empty_subdirs-0.1.1 (c (n "remove_empty_subdirs") (v "0.1.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0vlqr50a8yldy2ffdgpgd4wgab4pxbk3bpiya2wh0ld675dn6an5")))

