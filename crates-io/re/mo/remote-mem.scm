(define-module (crates-io re mo remote-mem) #:use-module (crates-io))

(define-public crate-remote-mem-0.1.0 (c (n "remote-mem") (v "0.1.0") (d (list (d (n "robs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_Debug"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winproc") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "06j17m90j83q2zgik7ggy7qpnrjb7sg4szzgg8246glwdlhnkim4")))

(define-public crate-remote-mem-0.1.1 (c (n "remote-mem") (v "0.1.1") (d (list (d (n "robs") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_Debug"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winproc") (r "^0.6.4") (d #t) (t "cfg(windows)") (k 0)))) (h "13c4n4imyzyznskylcizc1kmabv6wdn60vr5v549iz7yf9hcsjrd")))

