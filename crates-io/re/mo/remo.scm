(define-module (crates-io re mo remo) #:use-module (crates-io))

(define-public crate-remo-0.1.0 (c (n "remo") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1zy3gzs6c6wf496zkcz0p6fyrnid7h4fjilnpzdj8kcbsj656rs2")))

(define-public crate-remo-0.1.2 (c (n "remo") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1bvy65a4yqlwkhvgsmxlqcb8fby5fm81c330k010m9dyhv7n4g3s")))

