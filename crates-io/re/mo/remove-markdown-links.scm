(define-module (crates-io re mo remove-markdown-links) #:use-module (crates-io))

(define-public crate-remove-markdown-links-0.1.0 (c (n "remove-markdown-links") (v "0.1.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "0fxsrmqsyb783x8pv351al3k68m2yjxdzpvgq9aqjky8sk33m9pw")))

(define-public crate-remove-markdown-links-1.0.0 (c (n "remove-markdown-links") (v "1.0.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1sxv9j6rm1shlclw9wr6wmiffn4gibn582596y5yawl9n87304ig")))

