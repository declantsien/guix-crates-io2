(define-module (crates-io re mo remoc_macro) #:use-module (crates-io))

(define-public crate-remoc_macro-0.8.0-pre1 (c (n "remoc_macro") (v "0.8.0-pre1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0dwqi3gk87m1l32yr33k9bsdv3k0gh0amcjw7gm3zrryjh001aiv") (y #t)))

(define-public crate-remoc_macro-0.8.0-pre3 (c (n "remoc_macro") (v "0.8.0-pre3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "141y5ps8d5v6v0axbrq9lqv53lxx9f4gm710c0w2z7dy5ap9prhj") (y #t)))

(define-public crate-remoc_macro-0.8.0-pre4 (c (n "remoc_macro") (v "0.8.0-pre4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "02xpg5kxig79pcyahy88f3llqbdh3h895f2rd30d7846mg7pxv7h") (y #t)))

(define-public crate-remoc_macro-0.8.0-pre5 (c (n "remoc_macro") (v "0.8.0-pre5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0l70cfrh7w13dngq967jj9b3gx1d5yky1g2aff70h68i93w1c9xc") (y #t)))

(define-public crate-remoc_macro-0.8.0-pre6 (c (n "remoc_macro") (v "0.8.0-pre6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "143cnb9cjcvzjiqfnf324sfcv6dbsz8czalbnzr52n5z5apaij47") (y #t)))

(define-public crate-remoc_macro-0.8.0-pre7 (c (n "remoc_macro") (v "0.8.0-pre7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1p0vs2snk6ssl09pxris025jx3n59wak3d93bbx7sfsckqnl61sh") (y #t) (r "1.51")))

(define-public crate-remoc_macro-0.8.0-pre8 (c (n "remoc_macro") (v "0.8.0-pre8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1czh2gbk08n3gcdjx7vkkrl5l2ngw14a5icdhs61j0c4n3d3drzq") (y #t) (r "1.51")))

(define-public crate-remoc_macro-0.8.0-pre9 (c (n "remoc_macro") (v "0.8.0-pre9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1nx0dpmk5ba1rvqm7a7qz3lv71zwxzl109lj16gzrjhj1l507bjd") (y #t) (r "1.51")))

(define-public crate-remoc_macro-0.8.0 (c (n "remoc_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0623fy3qirq97yr3i8s1b8rpbc7gfr0z291b9q609pdwyd35spm6") (r "1.51")))

(define-public crate-remoc_macro-0.8.1 (c (n "remoc_macro") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "18f3qw5l3yhv5wb6mx127lzbr44ga266bc4irvqhlfdqw2dn8rim") (r "1.51")))

(define-public crate-remoc_macro-0.8.2 (c (n "remoc_macro") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "09ancyr43wa70p5zbljbfk43gx2j61a5r3rp9wdv89mvnnjip43y") (r "1.51")))

(define-public crate-remoc_macro-0.9.0 (c (n "remoc_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "042z4irkwyp4qwqqjm1v063wfn1lhhrlrmfc410xykqlx5qicy5x") (r "1.51")))

(define-public crate-remoc_macro-0.9.1 (c (n "remoc_macro") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1hi3078s6gfl0i75q0dhpazyxsqg92bq4lp7lknwf9hhyw5dgqvp") (r "1.51")))

(define-public crate-remoc_macro-0.9.2 (c (n "remoc_macro") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "104qy7fv7fpjahz8w45zpwph4r1lz38kwf2swrvzp1jkwzzqyjfp") (r "1.51")))

(define-public crate-remoc_macro-0.9.3 (c (n "remoc_macro") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "13cd60qmqb7z07bz3kkda4nplg8l2i7y5mwf8li9spknyvq3kdzw") (r "1.51")))

(define-public crate-remoc_macro-0.9.4 (c (n "remoc_macro") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "05i6mg6qcq0amwwr2fysybjmxyq48c1mxgv1zvj0jx2rbrik7d3h") (r "1.51")))

(define-public crate-remoc_macro-0.9.5 (c (n "remoc_macro") (v "0.9.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "12rhpn75pd9sxalzhr6vpggbla7xd3iahnlpap8hp6s5zr8jwipj") (r "1.51")))

(define-public crate-remoc_macro-0.9.6 (c (n "remoc_macro") (v "0.9.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0c9i5w1l8pa5cy6hhff5635ij1pj5rp9wrwlfrfg1dgm8qya1jy6") (r "1.51")))

(define-public crate-remoc_macro-0.9.7 (c (n "remoc_macro") (v "0.9.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0da3wk9ic753alp2f46x6748xpq67vj7x2sz33f5bqhylpvmblp9") (r "1.51")))

(define-public crate-remoc_macro-0.9.8 (c (n "remoc_macro") (v "0.9.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1gkssjrmxyirgcm6zwd35fj3mdkk8ygqma4j0444hlzb6pssc8hp") (r "1.51")))

(define-public crate-remoc_macro-0.9.9 (c (n "remoc_macro") (v "0.9.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "09wyr9m30lykbii21v19nj7gk5ypmpm70sfxwrc8q1azbx8y9877") (r "1.51")))

(define-public crate-remoc_macro-0.9.10 (c (n "remoc_macro") (v "0.9.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0wka8zvsihd8vlsyr54xz12ndsf950874nxj3kmsnxy3a9p5fy7h") (r "1.51")))

(define-public crate-remoc_macro-0.9.11 (c (n "remoc_macro") (v "0.9.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "07k3qgsp7kdzqry95v32752mmfdkgkk1vsnx5nf6capx18wafndn") (r "1.51")))

(define-public crate-remoc_macro-0.9.12 (c (n "remoc_macro") (v "0.9.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0c5cih06z1lwf4ra4aw3rzcwdbn2cb4s3di8xflnps48lq2d8azk") (r "1.51")))

(define-public crate-remoc_macro-0.9.13 (c (n "remoc_macro") (v "0.9.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "002dz2g3f16p727a6dv0zc7ria2pn977k7lxy6yjffqqqaak13py") (r "1.51")))

(define-public crate-remoc_macro-0.9.14 (c (n "remoc_macro") (v "0.9.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0k92yzx6i62s8xwghigvnxw2ppb3n6yvj1bil9af9875h8kn01ny") (r "1.51")))

(define-public crate-remoc_macro-0.9.15 (c (n "remoc_macro") (v "0.9.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1lkycckdgjq04wwyj123nrd8csz8xy0w17jisw67lirjczmag9ri") (r "1.51")))

(define-public crate-remoc_macro-0.9.16 (c (n "remoc_macro") (v "0.9.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0qdlcr6cpvqyi9rlbxpww94nspgib6nrllsrvg0w48cmgfl2z9cq") (r "1.51")))

(define-public crate-remoc_macro-0.10.0 (c (n "remoc_macro") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1wndasn667sga7h79d3xxzv0x6zysppvsnrlghqkw09w3iv2p710") (r "1.59")))

(define-public crate-remoc_macro-0.10.1 (c (n "remoc_macro") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0aj2967ps1nvlphrn9qrvw7baa39k021ayqs3xqz8yr31w2adqjs") (r "1.59")))

(define-public crate-remoc_macro-0.10.2 (c (n "remoc_macro") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0zhgw72q32w8lv7qg09z6n5j5fn0v4jg52npnry3i53y9pn3scb5") (r "1.59")))

(define-public crate-remoc_macro-0.10.3 (c (n "remoc_macro") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0fsxgnm97n9s0a2fksv8jrn52gi42cq2k2w8avkqf2a1njwyda1f") (r "1.59")))

(define-public crate-remoc_macro-0.11.0-pre1 (c (n "remoc_macro") (v "0.11.0-pre1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0pxcgpyrqns09mfabp10hlwrln22f30cl81fm2a39w1n4dadzrrs") (r "1.59")))

(define-public crate-remoc_macro-0.11.0-pre2 (c (n "remoc_macro") (v "0.11.0-pre2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1ca3j36azhifp3ns3qf41dij2z09h8am1d9ighkci5j8w3sjs09k") (r "1.59")))

(define-public crate-remoc_macro-0.11.0-pre3 (c (n "remoc_macro") (v "0.11.0-pre3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1cxi2i93fiynlr8rpxx3h5yzaxqp6h2yxdldnn5rshamdi38sg3j") (r "1.59")))

(define-public crate-remoc_macro-0.11.0 (c (n "remoc_macro") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0kzmwvjp7n8s0zcwif985x73fllzrwqcb0ifbinx6s4x5jv7a6ip") (r "1.72")))

(define-public crate-remoc_macro-0.11.1 (c (n "remoc_macro") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "01akv2zkncf641sk3pinlaxrzc8zch7qm3zr12gd7l9nbjsr5fyg") (r "1.72")))

(define-public crate-remoc_macro-0.11.2 (c (n "remoc_macro") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0mj0d4mxc1g09gl0wxyn0r0r5gc9v5mqlr8m8al3g7004flns7rk") (r "1.72")))

(define-public crate-remoc_macro-0.11.3 (c (n "remoc_macro") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0y0s9vq02s3h9kc6wbn6ppr550c6l384il0nzyvgfw17yl8lmv48") (r "1.72")))

(define-public crate-remoc_macro-0.11.4 (c (n "remoc_macro") (v "0.11.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1r73lx578ymsvb1b3nr3q9ghhk2786x7bbxyn2pl0kj9mf1dg18m") (r "1.72")))

(define-public crate-remoc_macro-0.11.5 (c (n "remoc_macro") (v "0.11.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1ykg3kjsgfmzmbl096vpwsl1ni07nyfc7q9684c4i3vqcr7c51xf") (r "1.72")))

(define-public crate-remoc_macro-0.11.6 (c (n "remoc_macro") (v "0.11.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "08m5aaqvf6a20z1zdnrw0gvsr5312lfkq9cssg21nqr0lawv4d8l") (r "1.72")))

(define-public crate-remoc_macro-0.11.7 (c (n "remoc_macro") (v "0.11.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1x1gll65arw2dg9hyyd7wpfzna83aqvanxrifyjxxhmwxs8f0fnl") (r "1.72")))

(define-public crate-remoc_macro-0.12.0 (c (n "remoc_macro") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0bfx88665izyrdilz2z1ph1kpjifspx0vb65xblmr5xy1w7qhwsh") (r "1.72")))

(define-public crate-remoc_macro-0.13.0 (c (n "remoc_macro") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0aidazf2bng4im49l9wnihvmdjdw6mir1ib072kyhd4nv929j3rv") (r "1.72")))

