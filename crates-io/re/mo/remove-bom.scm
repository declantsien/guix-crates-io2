(define-module (crates-io re mo remove-bom) #:use-module (crates-io))

(define-public crate-remove-bom-0.1.0 (c (n "remove-bom") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1y68y8lda33jq29wf7121wjjw4x0f7565nrvpagf9jkbkzw4l3g9")))

