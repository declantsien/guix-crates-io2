(define-module (crates-io re mo remora) #:use-module (crates-io))

(define-public crate-remora-0.0.1 (c (n "remora") (v "0.0.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)))) (h "0g4gbasllmv8ki8fkrwccw5lkw6ry2241n7wrqy2bf3yg1j8wqqa")))

