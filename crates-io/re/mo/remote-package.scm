(define-module (crates-io re mo remote-package) #:use-module (crates-io))

(define-public crate-remote-package-0.1.0 (c (n "remote-package") (v "0.1.0") (d (list (d (n "debpkg") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cxxid11ccvil4l9vlbgaqq78whp5j15ml0kxg7da1i35khwch9i") (f (quote (("http" "reqwest") ("default" "http" "debian") ("debian" "debpkg"))))))

(define-public crate-remote-package-0.2.0 (c (n "remote-package") (v "0.2.0") (d (list (d (n "debpkg") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "fez") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0a7zc050cqfqn2q5ifvl18fdrqkxiwvfavwj9sin9r9mb0q5s02z") (f (quote (("rpm" "fez") ("http" "reqwest") ("default" "http" "debian" "rpm") ("debian" "debpkg")))) (r "1.56")))

(define-public crate-remote-package-0.2.1 (c (n "remote-package") (v "0.2.1") (d (list (d (n "debpkg") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "fez") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bc04pqknah0dzs1y7apsfqbv4p7myjd5a68myvwzr5amk33zix8") (f (quote (("rpm" "fez") ("http" "reqwest") ("default" "http" "debian" "rpm") ("debian" "debpkg")))) (r "1.56")))

(define-public crate-remote-package-0.3.0 (c (n "remote-package") (v "0.3.0") (d (list (d (n "debpkg") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "fez") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mibl2i4096h1hsx9rlh057lcxh0z52dgrgdkia2mp1mr6n4p0gl") (f (quote (("rpm" "fez") ("http" "reqwest") ("default" "http" "debian" "rpm") ("debian" "debpkg")))) (r "1.56")))

