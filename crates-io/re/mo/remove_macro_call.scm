(define-module (crates-io re mo remove_macro_call) #:use-module (crates-io))

(define-public crate-remove_macro_call-0.1.0 (c (n "remove_macro_call") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "0s7i7p8hqdhqnvxfsj3253b561ddk4d0nx06rac4mgc7yhi3bz03") (r "1.56.1")))

(define-public crate-remove_macro_call-0.1.1 (c (n "remove_macro_call") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "00mmbbr33ajgjj1rxasvmi8fk5fl46zgvsmd36gd4sg0mvjx86p5") (r "1.56.1")))

(define-public crate-remove_macro_call-0.1.2 (c (n "remove_macro_call") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "0dp09b107ny56k6dcaff39bw6ky95lp6x9nrk0s94gv9dkr8av0l") (r "1.56.1")))

(define-public crate-remove_macro_call-0.1.3 (c (n "remove_macro_call") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "10fc7njx8zp2vfihy1iajn0jhf41xkxbm0nm3wpsh5q7kykppl8k") (r "1.56.1")))

