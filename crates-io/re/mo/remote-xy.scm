(define-module (crates-io re mo remote-xy) #:use-module (crates-io))

(define-public crate-remote-xy-0.1.0 (c (n "remote-xy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgh04ac6md448r4i66h43mibyk3c08aqcl7bc01wri5gib44cl7")))

