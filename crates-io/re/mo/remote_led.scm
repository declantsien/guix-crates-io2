(define-module (crates-io re mo remote_led) #:use-module (crates-io))

(define-public crate-remote_led-0.1.0 (c (n "remote_led") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vnsn7fddpwsyj1pbi5kfsa8dhqcjaak01pw87hizgsf3sdfqmgv")))

(define-public crate-remote_led-0.1.1 (c (n "remote_led") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nw6c82v30gzim3ibfx4nlhy61fg4p3kyij1fgz5by23nh9d4qmz")))

