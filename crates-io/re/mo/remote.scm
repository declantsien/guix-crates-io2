(define-module (crates-io re mo remote) #:use-module (crates-io))

(define-public crate-remote-0.0.0 (c (n "remote") (v "0.0.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)))) (h "12x78wa2p34djs0y55y3nw04n1fs136i88z3rmg2l2mhfq66sqz2")))

(define-public crate-remote-1.0.0 (c (n "remote") (v "1.0.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)))) (h "04zch8qx2ydm8qvmj244x2wjar9jbd7b4iyqh9y525scn2gazjgn")))

(define-public crate-remote-1.0.1 (c (n "remote") (v "1.0.1") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)))) (h "0crfshcd3950fbrkjpikv4dasrbkai2kbdgzagl85r0fyai7sq7j")))

