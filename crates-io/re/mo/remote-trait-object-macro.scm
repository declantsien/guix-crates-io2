(define-module (crates-io re mo remote-trait-object-macro) #:use-module (crates-io))

(define-public crate-remote-trait-object-macro-0.2.0 (c (n "remote-trait-object-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "18d2ma8ky46s0y2g60wn3qlq86v3fmnxcd0r8bnrp8n0y3vnf1yq")))

(define-public crate-remote-trait-object-macro-0.3.0 (c (n "remote-trait-object-macro") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "0fbjm5adkc65nb1f199g76zk6i1rq1mwb6b20rxdwd5mi29ajknv")))

(define-public crate-remote-trait-object-macro-0.3.1 (c (n "remote-trait-object-macro") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "150azn1h8vqvj5j18l7lmswbh7cwvzdhfmkjlz6232bn5pd3x4zc")))

(define-public crate-remote-trait-object-macro-0.4.0 (c (n "remote-trait-object-macro") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "1wyiw0as9b75zld487p50ar4vg66sas2pws7rdd5mxlgi9lasgl6")))

(define-public crate-remote-trait-object-macro-0.4.1 (c (n "remote-trait-object-macro") (v "0.4.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "0f6y71axvy99mgx0r5kcln4i66lb300szfq3kj98b9665mmk1p18")))

