(define-module (crates-io re mo remove-async-await) #:use-module (crates-io))

(define-public crate-remove-async-await-1.0.0 (c (n "remove-async-await") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "049w9ixvcp7182nf162wbl8bx6li9qpd0c9x5sz2hfll38pwys8d")))

(define-public crate-remove-async-await-1.0.1 (c (n "remove-async-await") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1lacmalmkadvlc7zx99gkgv5axgdsx2aslv0kjr0ygb8588962ca") (f (quote (("debug" "syn/extra-traits"))))))

