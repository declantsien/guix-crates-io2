(define-module (crates-io re pr repr_c_pub_struct) #:use-module (crates-io))

(define-public crate-repr_c_pub_struct-0.1.0 (c (n "repr_c_pub_struct") (v "0.1.0") (d (list (d (n "parenthesized_c") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.83") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0nydid85hhkm5nfkg60n52aa3m8d95l79s0li6514lc1jqdbkc5n") (r "1.56.1")))

(define-public crate-repr_c_pub_struct-0.1.1 (c (n "repr_c_pub_struct") (v "0.1.1") (d (list (d (n "parenthesized_c") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.83") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03j16iblk17qjhhayf8pjjfmpycssfcn9pgnf2nnydlqdi6pb3zq") (r "1.56.1")))

