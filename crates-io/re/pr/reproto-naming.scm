(define-module (crates-io re pr reproto-naming) #:use-module (crates-io))

(define-public crate-reproto-naming-0.3.14 (c (n "reproto-naming") (v "0.3.14") (h "10rghsdm9pk7vqw7w8vnz12946h5qlm7cpqiiciqyq023xf4przn")))

(define-public crate-reproto-naming-0.3.15 (c (n "reproto-naming") (v "0.3.15") (h "0qcczfzgym3axj5kwybbjdjaqakwj4x48pnvr3v12dznd3yww7ad")))

(define-public crate-reproto-naming-0.3.16 (c (n "reproto-naming") (v "0.3.16") (h "0dr8fp9yq6wi279nslwmpyas4pam0mw0dksjc99crprvf6axq378")))

(define-public crate-reproto-naming-0.3.17 (c (n "reproto-naming") (v "0.3.17") (h "0fqkzbxsv31y61fl12n65gknws9fa8syj6pl0mqvc3g8qdmhanz5")))

(define-public crate-reproto-naming-0.3.18 (c (n "reproto-naming") (v "0.3.18") (h "1kkinpys42cs65zcjrwic4nqsp3bj0rd9ccmhgyx7mpyjb5knsm6")))

(define-public crate-reproto-naming-0.3.19 (c (n "reproto-naming") (v "0.3.19") (h "06s0hycfa3z32wyjli5n3cd0y877n3l8md9dbnik2s22p2wm2wg5")))

(define-public crate-reproto-naming-0.3.21 (c (n "reproto-naming") (v "0.3.21") (h "0ynihhcm8knk77mz33asz3a0829w29ki5wa1lmmlwj5pwr7r2fgm")))

(define-public crate-reproto-naming-0.3.22 (c (n "reproto-naming") (v "0.3.22") (h "1nd2i7l28vk59lyacm3pan3a75m4nibrmsmqafb9s8nvc6mmf3v0")))

(define-public crate-reproto-naming-0.3.23 (c (n "reproto-naming") (v "0.3.23") (h "02kfklq9vicsm5izd6n5jh7h9cmjrbhlch7j9jfipq32lmc2aslw")))

(define-public crate-reproto-naming-0.3.24 (c (n "reproto-naming") (v "0.3.24") (h "1crnsxpxkbys0n85p4ys15dqkjjqs4k74gkcr0gawfrs7gakyvkv")))

(define-public crate-reproto-naming-0.3.25 (c (n "reproto-naming") (v "0.3.25") (h "0khiqylsji99r7s7vp061vsc44d20yvwyvz6l0a34cfs3aw9icgp")))

(define-public crate-reproto-naming-0.3.32 (c (n "reproto-naming") (v "0.3.32") (h "191z29r4m5wfwms7a5zlrl16m1in2miwgvvag7j1nvppsf8anq5y")))

(define-public crate-reproto-naming-0.3.36 (c (n "reproto-naming") (v "0.3.36") (h "1cw4ra66794b3n23s84aa8kdpwb0kwb457z1a228c4195indcm9v")))

