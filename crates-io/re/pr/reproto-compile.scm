(define-module (crates-io re pr reproto-compile) #:use-module (crates-io))

(define-public crate-reproto-compile-0.3.14 (c (n "reproto-compile") (v "0.3.14") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "1aa7k0xnmdk0wmgdia5hnl6ajwkdf8mlkbnrl73s7p805yv4ifg1")))

(define-public crate-reproto-compile-0.3.15 (c (n "reproto-compile") (v "0.3.15") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0z2iqd4l6i6hyigfps9b1xf9wamnjnx60h38amnp8j4xv48d3v6p")))

(define-public crate-reproto-compile-0.3.16 (c (n "reproto-compile") (v "0.3.16") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0b2phjd0agpnn50xa3ygmjmg6mb65xynhlka76db5spk5l5dly9y")))

(define-public crate-reproto-compile-0.3.17 (c (n "reproto-compile") (v "0.3.17") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0plqy54dq4wh10dc2ypjjps8p4ic5lxx60ni2nvy06ja9hq5847y")))

(define-public crate-reproto-compile-0.3.18 (c (n "reproto-compile") (v "0.3.18") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "10rprr770k1j8wppgskrrc5nlmbr3z674lwhz2nl1g56353g9n8j")))

(define-public crate-reproto-compile-0.3.19 (c (n "reproto-compile") (v "0.3.19") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0av7mbscn2rkr4jmmgxpnmpv0ralb6injqy33ji66n53p2cqic30")))

(define-public crate-reproto-compile-0.3.21 (c (n "reproto-compile") (v "0.3.21") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0f5hbfai1vzvp7r3r40f88lzr1wx4cdip4cb9difgdk34796g7c3")))

(define-public crate-reproto-compile-0.3.22 (c (n "reproto-compile") (v "0.3.22") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0a9ajkx866jd8vb6vpmhxnf0y4f7wjbpzbmz5wyachphv7vcrp8l")))

(define-public crate-reproto-compile-0.3.23 (c (n "reproto-compile") (v "0.3.23") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "1crp43dln85i9gfjb5s7xrq49i1sks2kwrnymddfz5fciv52dh02")))

(define-public crate-reproto-compile-0.3.24 (c (n "reproto-compile") (v "0.3.24") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "15bq0xjbax5jv01grp7axa2v1gdlyps9mfyqkdz6bp89plhy1byx")))

(define-public crate-reproto-compile-0.3.25 (c (n "reproto-compile") (v "0.3.25") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "0izfax865c0z8im3pq5hbj963fi7y58p9262bk0vkcs42fx106l1")))

(define-public crate-reproto-compile-0.3.32 (c (n "reproto-compile") (v "0.3.32") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "1n3lq1raqb4bshapp6wwvpcgaqz1lldap72j8hirj35hipydjhb1")))

(define-public crate-reproto-compile-0.3.36 (c (n "reproto-compile") (v "0.3.36") (d (list (d (n "reproto-ast") (r "^0.3") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)) (d (n "reproto-manifest") (r "^0.3") (d #t) (k 0)))) (h "13jqqsjidn0fryzg0pl75zx19qg7bp03bjb6yilmfnyzszsd4y8k")))

