(define-module (crates-io re pr reproto-semck) #:use-module (crates-io))

(define-public crate-reproto-semck-0.3.9 (c (n "reproto-semck") (v "0.3.9") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1w9khvxc78qrhxgx923ksf2546rb9iqsnlaxs9phicl4nyq9gif9")))

(define-public crate-reproto-semck-0.3.10 (c (n "reproto-semck") (v "0.3.10") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0sqfh3vzrsqgyf9iidl4lmd0s026yb22jp5x5s25kf0dfg3935mh")))

(define-public crate-reproto-semck-0.3.11 (c (n "reproto-semck") (v "0.3.11") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1prkrmlw1v267r250dd4k2id5iarirjjcsqlvifl1dxsrhkj4vji")))

(define-public crate-reproto-semck-0.3.12 (c (n "reproto-semck") (v "0.3.12") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "18i08qm6qla018k3wgqqpjnqfi1y69n9lqm7ib5nxkxgwpsci378")))

(define-public crate-reproto-semck-0.3.13 (c (n "reproto-semck") (v "0.3.13") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "19zbwid1if2x1gyx0yfwzcwl6n9izq1h00q7g1gnqyh99gdbzdx2")))

(define-public crate-reproto-semck-0.3.14 (c (n "reproto-semck") (v "0.3.14") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "16m58c4d2pvxfmwzg81hsbj3cvdbf26d9ni97a60xbkwcdsik35c")))

(define-public crate-reproto-semck-0.3.15 (c (n "reproto-semck") (v "0.3.15") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1ndy5kh5yxchk8sx8917v235ji3sqyxmlzx7ydhaad8ng8qij5s7")))

(define-public crate-reproto-semck-0.3.16 (c (n "reproto-semck") (v "0.3.16") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0yjpahzvyxkd306485gnwakzg0z2syfhs2hqwn8ir1vx5pbdibkj")))

(define-public crate-reproto-semck-0.3.17 (c (n "reproto-semck") (v "0.3.17") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0si37civig4axq5j3gwa9azg2m0mq68d7k8lpnh168khq0j139hg")))

(define-public crate-reproto-semck-0.3.18 (c (n "reproto-semck") (v "0.3.18") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0q2l6pgqmr6hazhsgpi4hyxdf43s9x55xpklpqgfiz23rxnamgic")))

(define-public crate-reproto-semck-0.3.19 (c (n "reproto-semck") (v "0.3.19") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1nk9jfrz0nr8rmhny47swryxsvsr2jw6gmnx7dc805ps6hn0rfrl")))

(define-public crate-reproto-semck-0.3.21 (c (n "reproto-semck") (v "0.3.21") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1zp40nz6iq3mbjmp7h62vraxkss10ksygldxcdzi7h8gsphzj9zm")))

(define-public crate-reproto-semck-0.3.22 (c (n "reproto-semck") (v "0.3.22") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1a36y2vpscq9c9dnvq075zy42k336z00dx0bb4mwbrjgh0m9rbkj")))

(define-public crate-reproto-semck-0.3.23 (c (n "reproto-semck") (v "0.3.23") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "05dp81ww7qwwm9lqsfbzpcamn4p39p015lzg53phjc21jyz3bm11")))

(define-public crate-reproto-semck-0.3.24 (c (n "reproto-semck") (v "0.3.24") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0whlvvbjw2ygrhyk5cdcs7ncqidslxj1c43wbq8kgzlagzjd08ck")))

(define-public crate-reproto-semck-0.3.25 (c (n "reproto-semck") (v "0.3.25") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0ywclzbpzgya6yljffkr9c42zwsypdd6v78zk1z8q4v2w9r8w22p")))

(define-public crate-reproto-semck-0.3.32 (c (n "reproto-semck") (v "0.3.32") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1q84vbr908xvzw2vnnj4jjbb81q7d76q9dhkga89jjibi0ynpgl3")))

(define-public crate-reproto-semck-0.3.36 (c (n "reproto-semck") (v "0.3.36") (d (list (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "05w9l1s8f784g6fhvx48gxz76lzvplxnyaifrax0dax80xars24f")))

