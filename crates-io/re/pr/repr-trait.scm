(define-module (crates-io re pr repr-trait) #:use-module (crates-io))

(define-public crate-repr-trait-1.0.0 (c (n "repr-trait") (v "1.0.0") (d (list (d (n "repr-trait-derive") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "024iv68yv3gsb3ly29r8y2q6l4q3yd6znqix5v6bp1fjkk801fqz")))

