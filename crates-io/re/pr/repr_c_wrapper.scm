(define-module (crates-io re pr repr_c_wrapper) #:use-module (crates-io))

(define-public crate-repr_c_wrapper-0.1.0 (c (n "repr_c_wrapper") (v "0.1.0") (h "1kqdrc1ikrypfj2klqgnq900n5dmid455qb414bw9k9lv9fcy6wb")))

(define-public crate-repr_c_wrapper-0.1.1 (c (n "repr_c_wrapper") (v "0.1.1") (h "13hgll3kj2iry924cgdf04rhc5mvih6w6jbzw4h0za6gr85lf89l")))

