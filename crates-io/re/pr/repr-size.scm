(define-module (crates-io re pr repr-size) #:use-module (crates-io))

(define-public crate-repr-size-0.1.0 (c (n "repr-size") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s43l714mvny767gsp3sj6slb1bvv7h5cazsy9f7ghmgkpw955gc") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-repr-size-0.1.1 (c (n "repr-size") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13hx9ajsdvf9qr964vmly1xc4xfrx7akaij5mhhqsjwygr0bdjbl") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-repr-size-0.1.2 (c (n "repr-size") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i1yg7prl1958bvvjz0mg7d4k12nkryqa4ndfk1g8r886m0hh731") (s 2) (e (quote (("serde" "dep:serde"))))))

