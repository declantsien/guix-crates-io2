(define-module (crates-io re pr repr_offset_derive) #:use-module (crates-io))

(define-public crate-repr_offset_derive-0.0.0 (c (n "repr_offset_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17sp9gza9zn1fhwhm7d0gsib154ygzhdpzvc77adk2hdz3c8mvbc")))

(define-public crate-repr_offset_derive-0.1.0 (c (n "repr_offset_derive") (v "0.1.0") (d (list (d (n "as_derive_utils") (r "^0.8.2") (k 0)) (d (n "core_extensions") (r "^0.1.16") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xk0gm06dh6j8l6jgxyyhibrms30miqm6bklmfr2gz8q8y4ir774") (f (quote (("testing" "as_derive_utils/testing"))))))

(define-public crate-repr_offset_derive-0.1.1 (c (n "repr_offset_derive") (v "0.1.1") (d (list (d (n "as_derive_utils") (r "^0.8.3") (k 0)) (d (n "core_extensions") (r "^0.1.16") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xi7z4rlmjr97p1x3pn3jlkp4l50i80p1mvlqy416k20r7qc5jl7") (f (quote (("testing" "as_derive_utils/testing"))))))

(define-public crate-repr_offset_derive-0.2.0 (c (n "repr_offset_derive") (v "0.2.0") (d (list (d (n "as_derive_utils") (r "^0.8.3") (k 0)) (d (n "core_extensions") (r "^0.1.16") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rwkbf12vmgi5v1llmgiirn0yaaiyw821rd7fc9fhpbkdxz95yh9") (f (quote (("testing" "as_derive_utils/testing"))))))

