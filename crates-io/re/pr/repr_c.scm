(define-module (crates-io re pr repr_c) #:use-module (crates-io))

(define-public crate-repr_c-0.0.0 (c (n "repr_c") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.66") (t "cfg(not(target = \"wasm32-unknown-unknown\"))") (k 0)))) (h "0mawkjxqvvxlxp3faq1vyfzxf5agyn8bx5glx5d93shgcyj5gjcz") (f (quote (("default" "alloc") ("alloc"))))))

