(define-module (crates-io re pr repr-with-fallback) #:use-module (crates-io))

(define-public crate-repr-with-fallback-0.1.0 (c (n "repr-with-fallback") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pdwvcrx31cszjwr83300m6jyb943xv1pvl6m3f4wfiq4x2zhfpb")))

(define-public crate-repr-with-fallback-0.1.1 (c (n "repr-with-fallback") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "005jk2izrx1mzhm5ijs9i1iivrh0l0kka9jn1xayswgg1phkc6fx")))

