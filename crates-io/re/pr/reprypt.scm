(define-module (crates-io re pr reprypt) #:use-module (crates-io))

(define-public crate-reprypt-0.1.0 (c (n "reprypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "1mh0y2y5dj8f175v41pza8kfhpmw31ank8qhxnfbnfldwnyvngcr") (y #t)))

(define-public crate-reprypt-0.1.1 (c (n "reprypt") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "0dxpw1pcq9x2zq9ksq4xx977bsyimr1vxgjad7j5l261wl0fg640") (y #t)))

