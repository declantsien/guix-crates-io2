(define-module (crates-io re pr reproto-semver) #:use-module (crates-io))

(define-public crate-reproto-semver-0.3.9 (c (n "reproto-semver") (v "0.3.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mn46k9w6lzms36vyvg8lq6h8mql10hmy1zfc7gjn6qjs8dcivwr")))

(define-public crate-reproto-semver-0.3.10 (c (n "reproto-semver") (v "0.3.10") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pnh7i3npcy93aql2skrxf20vp7c2bpr5xb5srx3fkq11dq1jycm")))

(define-public crate-reproto-semver-0.3.11 (c (n "reproto-semver") (v "0.3.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r9yvpfarwwp0cg9n4vahin9l2n807gvs1dcrzcdkakzrxy9dbi9")))

(define-public crate-reproto-semver-0.3.12 (c (n "reproto-semver") (v "0.3.12") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zis6612dnfss62ayiq1qh36wkalhs28hzavmb1s3yazg4acjph9")))

(define-public crate-reproto-semver-0.3.13 (c (n "reproto-semver") (v "0.3.13") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11vw1y05whqsgzpxf9biz99nalghrlyngnkz5kr38zj55gfdkwh3")))

(define-public crate-reproto-semver-0.3.14 (c (n "reproto-semver") (v "0.3.14") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ljyxa31s0kqyzcm64lh41x27xn7h78gsiaklm9c76sgq6frzl5s")))

(define-public crate-reproto-semver-0.3.15 (c (n "reproto-semver") (v "0.3.15") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0a72am9bgi058s6y4xmcrpmm9ndb0rfmgr7x6ccaz423sjj98nmr")))

(define-public crate-reproto-semver-0.3.16 (c (n "reproto-semver") (v "0.3.16") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jdl6wd1d03xjjvi85pg7q348wgixwm66rw3gz6r3467di027gia")))

(define-public crate-reproto-semver-0.3.17 (c (n "reproto-semver") (v "0.3.17") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0f13rmq19a39l6br2vqd4irl3v39pviv0j21mvay3gvrwpysr65s")))

(define-public crate-reproto-semver-0.3.18 (c (n "reproto-semver") (v "0.3.18") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03djsdx09wg1d38ymndvhscgxnnbnji1n9pwm5frdz087zkc80b4")))

(define-public crate-reproto-semver-0.3.19 (c (n "reproto-semver") (v "0.3.19") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00d7zg9iqxsf9ng09wbyj7w92hazd6j3bzcyk5ml6pmsmyr5rscp")))

(define-public crate-reproto-semver-0.3.21 (c (n "reproto-semver") (v "0.3.21") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03kl0hdgbk3cz81na31pkdy4wjhy3m4zda890zwb7jdmk8wjzg32")))

(define-public crate-reproto-semver-0.3.22 (c (n "reproto-semver") (v "0.3.22") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08i84shpfqirb3c2wjxkpcpfkmj7vj58s5y67h2ld2z2846230mx")))

(define-public crate-reproto-semver-0.3.23 (c (n "reproto-semver") (v "0.3.23") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17nkfln2ra1zldd19wsnmmy883bsr7dpdfs50c9hwy40bifdl6sb")))

(define-public crate-reproto-semver-0.3.24 (c (n "reproto-semver") (v "0.3.24") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1az0rcpa6gcs2p168kb7pg53c902hmplar5qmbn0sdjfsnxa1wd7")))

(define-public crate-reproto-semver-0.3.25 (c (n "reproto-semver") (v "0.3.25") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18w097rq85dwm1pp3hbgjvm9r7hzw8gcwfjfwj3zimcfx6ijlmdb")))

(define-public crate-reproto-semver-0.3.32 (c (n "reproto-semver") (v "0.3.32") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10vscj303df8i015ssdxcjk022x6ima56fnqfkdzr4wy17rmsq51")))

(define-public crate-reproto-semver-0.3.36 (c (n "reproto-semver") (v "0.3.36") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jwb96x8f7ddv98im2lkzb4r14amzvl1n6rq9fd8dai013hz7s9k")))

