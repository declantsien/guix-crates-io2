(define-module (crates-io re pr reproto-lexer) #:use-module (crates-io))

(define-public crate-reproto-lexer-0.3.9 (c (n "reproto-lexer") (v "0.3.9") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0wzrxhfpnfavhm0cc3yhximppkfz6mwn9kf131cspss93hd8kdbv")))

(define-public crate-reproto-lexer-0.3.10 (c (n "reproto-lexer") (v "0.3.10") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "014dcn0r5qqbp2cm18wxi3zrrxj1n8w463b35p75gh42ankjxixh")))

(define-public crate-reproto-lexer-0.3.11 (c (n "reproto-lexer") (v "0.3.11") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0ybvh005c4km1rlqxrp1rvy2klkvc2h4k3hnbji2l38w5fxql3az")))

(define-public crate-reproto-lexer-0.3.12 (c (n "reproto-lexer") (v "0.3.12") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "00nvi084yj61ra1b5y6iz1qrs84vi69rnksnqf04w3s3bgvv2mwf")))

(define-public crate-reproto-lexer-0.3.13 (c (n "reproto-lexer") (v "0.3.13") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "14jl6kyzxy801lm6j7qry1gf6c94c12pxwzg9s992gbqjaks93i6")))

(define-public crate-reproto-lexer-0.3.14 (c (n "reproto-lexer") (v "0.3.14") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0f5qnr8l8c10faqr6vjwz61rdzxvvh777yfbxwxfcm68wh2hqspz")))

(define-public crate-reproto-lexer-0.3.15 (c (n "reproto-lexer") (v "0.3.15") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0y9zz4ziw1mxrsvqz449qcqkv7n082y8azbkvn3a1kvlnvba9hlj")))

(define-public crate-reproto-lexer-0.3.16 (c (n "reproto-lexer") (v "0.3.16") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "18sf5zcm1jslwchq2v6jq6np0l5n5wl0v1mjyrc3m6vsqijwl5gs")))

(define-public crate-reproto-lexer-0.3.17 (c (n "reproto-lexer") (v "0.3.17") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "085v2lf55v9ywrc3shl5pzhqkp5qhlrm28l35cymd7cikq5fz250")))

(define-public crate-reproto-lexer-0.3.18 (c (n "reproto-lexer") (v "0.3.18") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0b81nwjvji2b9z791rg9ryi5hw8p6jfxxql4ksfswk0c3dnwifr5")))

(define-public crate-reproto-lexer-0.3.19 (c (n "reproto-lexer") (v "0.3.19") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0wshz4p2gbjimmbf1yv7y9rsdpiywhyhbkgxcy22ydb2gzldnqmp")))

(define-public crate-reproto-lexer-0.3.21 (c (n "reproto-lexer") (v "0.3.21") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "10gp2qip828czr3y7vdjq6gl6ic3j9sr48xi6zp7rmfay45dmm23")))

(define-public crate-reproto-lexer-0.3.22 (c (n "reproto-lexer") (v "0.3.22") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "18bj022vqjy8fv59wkgc2z38spmjp4d99vq8hhyl8024sfcv4641")))

(define-public crate-reproto-lexer-0.3.23 (c (n "reproto-lexer") (v "0.3.23") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1bczmkda7s72kxwc4xyai78ji6shrv4mjzdcwcr7mavrwfbs3xjs")))

(define-public crate-reproto-lexer-0.3.24 (c (n "reproto-lexer") (v "0.3.24") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0s369ifysm2sl03qnrbhkbzz6b9c292dmd4c9ii71dlfi26zm3p5")))

(define-public crate-reproto-lexer-0.3.25 (c (n "reproto-lexer") (v "0.3.25") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "0l5mbdvbp8a5rh8q57q5aj89xmjhk5xs0jylfa83hrg9gk0sw7ck")))

(define-public crate-reproto-lexer-0.3.32 (c (n "reproto-lexer") (v "0.3.32") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "16s1x23h3wknyiw8yj847hm649bifv0g3plilpfyiahha82ghipq")))

(define-public crate-reproto-lexer-0.3.36 (c (n "reproto-lexer") (v "0.3.36") (d (list (d (n "num-bigint") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)) (d (n "reproto-core") (r "^0.3") (d #t) (k 0)))) (h "1jzkn8ffi4rql4vasvzx0v8gmzabb9iaxk5vwwyv5af52n3cii3r")))

