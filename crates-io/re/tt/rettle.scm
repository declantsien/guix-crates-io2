(define-module (crates-io re tt rettle) #:use-module (crates-io))

(define-public crate-rettle-1.0.0 (c (n "rettle") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wml7kcvdqjs2jwk1rmg1sqp93ipqz1sqdn2pkygx8xks801fhqw")))

(define-public crate-rettle-1.0.1 (c (n "rettle") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hlmvjqg5szd5jydn16bqa0hyjp31srbkdrfkq4l7kifgmzfdgpi")))

(define-public crate-rettle-1.0.2 (c (n "rettle") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0093gdyprbvzh5b2x1r1kq03fqqqvz1n998chx524890wvjjsd3n")))

(define-public crate-rettle-1.0.3 (c (n "rettle") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x281c9wp9jj6s3kqmidb6ralkq8xll7hjj7fiw4w2avzh6j5s48")))

(define-public crate-rettle-2.0.0 (c (n "rettle") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vin8grii8qqy45cw3chj5v7kssj5smm23m9khgiqyy2dfg8c9nz")))

