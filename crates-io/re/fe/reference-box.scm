(define-module (crates-io re fe reference-box) #:use-module (crates-io))

(define-public crate-reference-box-1.0.0 (c (n "reference-box") (v "1.0.0") (h "12dqvkqqf6zvcy4kdr2w42iy0xbpb1b7rfcnqahybgr2l2d6whq0")))

(define-public crate-reference-box-1.1.0 (c (n "reference-box") (v "1.1.0") (h "1akzfa03d95djr0qx5lz3a8jias21mcixsmdjldgzax4brqwlbp9")))

