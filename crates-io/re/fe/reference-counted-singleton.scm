(define-module (crates-io re fe reference-counted-singleton) #:use-module (crates-io))

(define-public crate-reference-counted-singleton-0.1.0 (c (n "reference-counted-singleton") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)))) (h "0n4kiybfv5z6fgc4xbb3d3fmhg357i4fx9krcdwd0x781g1sw0wy")))

(define-public crate-reference-counted-singleton-0.1.1 (c (n "reference-counted-singleton") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)))) (h "16is1ld71ara6wmxhsm80h3xbzr0dlai34cxqr5xrp9gm49m4i7g")))

(define-public crate-reference-counted-singleton-0.1.2 (c (n "reference-counted-singleton") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)))) (h "13r2p2d408ci11s8pjxjaxv8znil0rfpaggcn75xv27bswjvzgzi")))

(define-public crate-reference-counted-singleton-0.1.3 (c (n "reference-counted-singleton") (v "0.1.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)))) (h "0q08piyz7zr2r11w7ic33rh9nsiawzvkm6d8h8m3pcrn1lxziz9z")))

(define-public crate-reference-counted-singleton-0.1.4 (c (n "reference-counted-singleton") (v "0.1.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)))) (h "1fcc50l2xh74r2qn4nx2kk0h4i5ys3v4fhgpg5cz793g00gq8br4")))

