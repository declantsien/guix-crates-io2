(define-module (crates-io re fe reference-counted) #:use-module (crates-io))

(define-public crate-reference-counted-0.1.0 (c (n "reference-counted") (v "0.1.0") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)) (d (n "smart-pointer") (r "^0.1.0") (d #t) (k 0)))) (h "07wlp554zrpkax2m0d3ydvghf8zl7n06nmq8p6m8j3fziw1w9bih") (f (quote (("rc" "maybe-std/alloc") ("default" "rc" "arc") ("arc" "maybe-std/alloc"))))))

