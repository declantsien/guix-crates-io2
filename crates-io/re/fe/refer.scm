(define-module (crates-io re fe refer) #:use-module (crates-io))

(define-public crate-refer-0.1.0 (c (n "refer") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "11qz6i78ylzfmf33qmjzr8zfswa3a5fpsy7f1scb26wcmf06am1w")))

(define-public crate-refer-0.1.1 (c (n "refer") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1237ir85m23qvkcjg6hmxj8h0yn7q00r7ldyshlq3lb3drxy05bw")))

(define-public crate-refer-0.1.2 (c (n "refer") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0gi5myaf5m83hxn74lhrvym5vsqsx5cyl0hvml4929bdd7gk25m0")))

(define-public crate-refer-0.1.3 (c (n "refer") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0900sw9cvbd9bvlic8z1m7ipf1nya2rvn1lyzd7j1lyk2ncdizxv")))

