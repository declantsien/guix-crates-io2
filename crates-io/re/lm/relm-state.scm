(define-module (crates-io re lm relm-state) #:use-module (crates-io))

(define-public crate-relm-state-0.10.0 (c (n "relm-state") (v "0.10.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "relm-core") (r "^0.10.0") (d #t) (k 0)))) (h "00sh10kcb8i52qi24qfr2cdlsxv7pzmpsyckrnd9p7g7wikvzq02") (f (quote (("use_impl_trait"))))))

(define-public crate-relm-state-0.10.1 (c (n "relm-state") (v "0.10.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "relm-core") (r "^0.10.0") (d #t) (k 0)))) (h "190b6hsvi8x1bqm1q3pnxi89hn4gdik1nnc63imbv0pm3001y0z2") (f (quote (("use_impl_trait"))))))

(define-public crate-relm-state-0.11.0 (c (n "relm-state") (v "0.11.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "relm-core") (r "^0.11.0") (d #t) (k 0)))) (h "01991qafsa23kjjcf68g494xz2lf2yxiakvsndanca6ai6sipll7") (f (quote (("use_impl_trait"))))))

(define-public crate-relm-state-0.12.0 (c (n "relm-state") (v "0.12.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "relm-core") (r "^0.12.0") (d #t) (k 0)))) (h "1fck2g0kmfv26hrk806l6pihy4vv5vf36kzwgid1dlwinprs1f6k") (f (quote (("use_impl_trait"))))))

(define-public crate-relm-state-0.13.0 (c (n "relm-state") (v "0.13.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "relm-core") (r "^0.13.0") (d #t) (k 0)))) (h "1d9shgsdcxw052vld8w30263bfjzpfqxmjj22rm5gcr9k11307xw") (f (quote (("use_impl_trait"))))))

(define-public crate-relm-state-0.14.0 (c (n "relm-state") (v "0.14.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "1857v5mk1k7pzrhy91xmkwv3w10s5959l50sfxyc095bbpza11p2")))

(define-public crate-relm-state-0.14.2 (c (n "relm-state") (v "0.14.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "105rk5qxfnndhhlgc5kwy05zzxs929vdznsa24ny7m2ccyirq2wb") (y #t)))

(define-public crate-relm-state-0.14.3 (c (n "relm-state") (v "0.14.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "04i72hhlsgn2ayl6x7jl4cyy6pfk0sn0722j1gv24yw71z3vxd47")))

(define-public crate-relm-state-0.14.4 (c (n "relm-state") (v "0.14.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "0sk5p2wi4wpbrw1bv9r0rlr6787d0flq3a4qykx17lrxhx3bkp20")))

(define-public crate-relm-state-0.14.6 (c (n "relm-state") (v "0.14.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "0npkb85ifq7zfpzch5pqzgpywg4q3psgl76rz2clg5y41h210sbd")))

(define-public crate-relm-state-0.15.0 (c (n "relm-state") (v "0.15.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "0c36pzqfjli0i9w00hlmbaqkp4lw42r1jkfh6gyhx5brinbkbfll")))

(define-public crate-relm-state-0.15.1 (c (n "relm-state") (v "0.15.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "0cnf513mcvp5wkv8qww2xkkazm1lc9ddprhhj1j5dbaa55s2g4s4")))

(define-public crate-relm-state-0.15.2 (c (n "relm-state") (v "0.15.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "1c9csn3f8kswf5x27w2z68an9mj9gmx2l2cd8nz933czl0i86a5g")))

(define-public crate-relm-state-0.15.3 (c (n "relm-state") (v "0.15.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "1b529b3krkrcggx5jxfx44r5sg2iaklwpppfkndpzx5wfyjn8hi8")))

(define-public crate-relm-state-0.16.0 (c (n "relm-state") (v "0.16.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relm-core") (r "^0.16.0") (d #t) (k 0)))) (h "0535w4g83rwr1iwbgdkf6jvccinvd76945nd6sv88hdqsspydcmv")))

