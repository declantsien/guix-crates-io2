(define-module (crates-io re lm relm4-store-view-implementation) #:use-module (crates-io))

(define-public crate-relm4-store-view-implementation-0.1.0-beta.1 (c (n "relm4-store-view-implementation") (v "0.1.0-beta.1") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")))) (h "16cas7b2flilvyyyh6561h7wsx4a9xyvmqaq10a262ddb1yq8z1q")))

(define-public crate-relm4-store-view-implementation-0.1.0-beta.2 (c (n "relm4-store-view-implementation") (v "0.1.0-beta.2") (d (list (d (n "collections") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-collections")) (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")))) (h "01bylqdi2pv27mvjri524zmp2baggpq7fp7caq0p0b3sw5lq4ynv")))

