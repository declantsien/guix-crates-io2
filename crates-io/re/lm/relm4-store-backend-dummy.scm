(define-module (crates-io re lm relm4-store-backend-dummy) #:use-module (crates-io))

(define-public crate-relm4-store-backend-dummy-0.1.0-beta.1 (c (n "relm4-store-backend-dummy") (v "0.1.0-beta.1") (d (list (d (n "gtk-test") (r "^0.14.0") (d #t) (k 2)) (d (n "record") (r "^0.1.0-beta") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "store") (r "^0.1.0-beta") (d #t) (k 0) (p "relm4-store")))) (h "13l21fp7vv6rwkdr33y1xc97rgnl47v9zjyzxqrmyhkvpnq74jyq")))

(define-public crate-relm4-store-backend-dummy-0.1.0-beta.2 (c (n "relm4-store-backend-dummy") (v "0.1.0-beta.2") (d (list (d (n "gtk-test") (r "^0.14.0") (d #t) (k 2)) (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")))) (h "1q4ykm23dlaa2fcch74d04fx8y6nlkw1vg634k87rqzhf63z44p6")))

