(define-module (crates-io re lm relm4-store-collections) #:use-module (crates-io))

(define-public crate-relm4-store-collections-0.1.0-beta.2 (c (n "relm4-store-collections") (v "0.1.0-beta.2") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")))) (h "14yi57bxw62pa0252lxbcnv0bhgjwribbv0z2nym7280xbs9g6va")))

