(define-module (crates-io re lm relm4-store) #:use-module (crates-io))

(define-public crate-relm4-store-0.1.0-beta.1 (c (n "relm4-store") (v "0.1.0-beta.1") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")))) (h "1r8c6szbfpsixw6gkm9mkif11kyp0lb8qwjmyrqw8fg83jsgk8i3")))

(define-public crate-relm4-store-0.1.0-beta.2 (c (n "relm4-store") (v "0.1.0-beta.2") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")))) (h "0xa1amhi57vcydi8196k76549hhzblm2a3a8j5dwh07nqlipa1x7")))

