(define-module (crates-io re lm relm-derive-common) #:use-module (crates-io))

(define-public crate-relm-derive-common-0.10.0 (c (n "relm-derive-common") (v "0.10.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1657x378halczaz76z0a1l60gi0nq4pk00m2ic6xl0wizljpqqdb")))

(define-public crate-relm-derive-common-0.11.0 (c (n "relm-derive-common") (v "0.11.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "16djsyk21sss94j74k8cj9h2gf20d09cq1wqdyccajyz0m6qrxjy")))

(define-public crate-relm-derive-common-0.12.0 (c (n "relm-derive-common") (v "0.12.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "171nxipw5q3pqp9j3j0fy6v2kwv30nhfbk49sh6cbldavk74sxg8") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.13.0 (c (n "relm-derive-common") (v "0.13.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "01z96jqg9958x6ccy8rpb344yc8njwsfxnpjjsp1gykjhmln033p") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.14.0 (c (n "relm-derive-common") (v "0.14.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1ldijkckx3wsig7760pdbzc9qq1pq3dnfz779wgskycq7zg7wf07") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.14.1 (c (n "relm-derive-common") (v "0.14.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0bica0fwqhhnm1kbkgdgs4dhw5d893h931pv65qd3h4qiykss9cf") (f (quote (("unstable" "relm-gen-widget/unstable")))) (y #t)))

(define-public crate-relm-derive-common-0.14.2 (c (n "relm-derive-common") (v "0.14.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1xl0r1dvdkd4pllljgkydphzzrrw8dglq3vvby6xg0ibf6rj5ssp") (f (quote (("unstable" "relm-gen-widget/unstable")))) (y #t)))

(define-public crate-relm-derive-common-0.14.3 (c (n "relm-derive-common") (v "0.14.3") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1ck8as458rwz9h5bv2p99zawymp7wa70y1nwf8dck5xs7bwyk6z6") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.14.4 (c (n "relm-derive-common") (v "0.14.4") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "11k7x5x5xdq0pa3a2yrcdrc5y2pqds9hb6cjx8525x99w3lq372x") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.14.6 (c (n "relm-derive-common") (v "0.14.6") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "11n9pqhj4gphqqa3nrss6hcgmzi1incyh7z2pkwf3r3083cp3yz1") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.15.0 (c (n "relm-derive-common") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1y88ryad57l559mwlcn2z1ajpjvb3kf22h3l3zdlqc3r787gvz0g") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.15.1 (c (n "relm-derive-common") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pb1j78k2xhf35gr6ggczkymr53nhrf1mnacdmwfz2j9asvdq9rg") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.15.2 (c (n "relm-derive-common") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1npcbxagg2xscyjrmjhl24d947sji71ncwdlnldl66sqlky1dkch") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.15.3 (c (n "relm-derive-common") (v "0.15.3") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "076p3m8hcnc1c0gwbi59n4a4f3li9zmwriipc0f6s9ia9p6qkicf") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-common-0.16.0 (c (n "relm-derive-common") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0c32gv3agdrbgnwsq3fj60xz2zmjrz4c9i5dhclwf9jz1rz6z4ff") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

