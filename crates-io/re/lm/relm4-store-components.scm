(define-module (crates-io re lm relm4-store-components) #:use-module (crates-io))

(define-public crate-relm4-store-components-0.1.0-beta.1 (c (n "relm4-store-components") (v "0.1.0-beta.1") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")) (d (n "store-view") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-view-implementation")))) (h "08fmzw6ypi52186mx17p8b8fh0lzpd6b7bnrk223kcm0a0y6yqnh")))

(define-public crate-relm4-store-components-0.1.0-beta.2 (c (n "relm4-store-components") (v "0.1.0-beta.2") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")) (d (n "store-view") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-view-implementation")))) (h "0gizv0zr5s23rcdcnx299y7l9gs31iq3zny7c5ba63hljxh2hyb2")))

