(define-module (crates-io re lm relm-derive) #:use-module (crates-io))

(define-public crate-relm-derive-0.1.0 (c (n "relm-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1i3a3nxijkcw3b0lsqc5gpgv1zq9viazb6ax76b6l0bi5dcl5drs")))

(define-public crate-relm-derive-0.1.1 (c (n "relm-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1mv7pcbdfw1y43zxjv7xlz58li3kqzymknx61vajdnix7hhp07fp")))

(define-public crate-relm-derive-0.1.2 (c (n "relm-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0c3wdjli6jzigzyrlwbskysgmh9837yfapz7jhc9snq2qk3nzn0f")))

(define-public crate-relm-derive-0.1.3 (c (n "relm-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1gnm23w8gqsvd5nk015nj30xd3jlbdjv8ynxr707bwbfdjdirlf0")))

(define-public crate-relm-derive-0.1.4 (c (n "relm-derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1a81fzy9z4ww10sh5jk6i6p4scd84zdmrlddpsdw8ssq37900gnd")))

(define-public crate-relm-derive-0.2.0 (c (n "relm-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0g69hdxkms7jzq6w3sd5j145dlbgzpjgp1p7v689yc0dhgd81bsi")))

(define-public crate-relm-derive-0.2.1 (c (n "relm-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0bg6zqzqdj3s1y1nra03zdn90yjqb001bqzwpn6yr17yq7s33fxy")))

(define-public crate-relm-derive-0.2.2 (c (n "relm-derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0vbw8v6y6jlfkf95c0i4ns3c362261657n1ncxdjs86cizw0myxm")))

(define-public crate-relm-derive-0.6.0 (c (n "relm-derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0v2mz0q5lyccp4r7mpyn1is2yl88w2lcvlwkvsq44a4s683yn8sm")))

(define-public crate-relm-derive-0.7.0 (c (n "relm-derive") (v "0.7.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0am0nrwz5bh1nr9iy5mqc2vklspr60zdz5rbk49iv2h74ry14sqv")))

(define-public crate-relm-derive-0.8.0 (c (n "relm-derive") (v "0.8.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "10hsjkbyym3nzpcg7q428jnm45dqmlci0qr6i19vaix77vvmxjnf")))

(define-public crate-relm-derive-0.9.0 (c (n "relm-derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "076slgd936ihf7h8bnccscjisy4cmrdrqz03p44wl4vybcy0wa62")))

(define-public crate-relm-derive-0.9.1 (c (n "relm-derive") (v "0.9.1") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0z8vfc8yrikxj4fpkhcvqviniw17mb1fyb1mz6dkjw36afgfy027")))

(define-public crate-relm-derive-0.9.2 (c (n "relm-derive") (v "0.9.2") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "04qrx6498v6pdybbmv8110708za0mpl0v6qzgwfz5jv75f53q9h1")))

(define-public crate-relm-derive-0.10.0 (c (n "relm-derive") (v "0.10.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.10.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0gzrq3pi6hkcvjwid02m8dm1jkqnmhjbid5ym9vccjljrx2hjv5c")))

(define-public crate-relm-derive-0.11.0 (c (n "relm-derive") (v "0.11.0") (d (list (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.11.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "002rd68cnsgqpppii7n92fw2br03q9lk8ybq5aa8zn7kys99xzs1")))

(define-public crate-relm-derive-0.12.0 (c (n "relm-derive") (v "0.12.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.12.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1rb7qkh8q8k7sphc14nj3jvsa8gw20d4zv9h2hn6xz265n3nvh57") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.13.0 (c (n "relm-derive") (v "0.13.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.13.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "08975vli7c0adsk2laxp1z7gv45a2a996y0gkdsnxgk2nsm6pcd8") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.14.0 (c (n "relm-derive") (v "0.14.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0c17b4vlnk33qcfh2i65sigdbzxi54vwfsahc111kkc00qsqf4wy") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.14.2 (c (n "relm-derive") (v "0.14.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1c70hb2d0rab9x553ryr258lka42vaq1icxsndgdkfrlvdfn8m29") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable")))) (y #t)))

(define-public crate-relm-derive-0.14.3 (c (n "relm-derive") (v "0.14.3") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0331a7ivp9pxykwnbqprp7jnla7kmiw1gdzwaa0d7i8pw8dfm9rh") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.14.4 (c (n "relm-derive") (v "0.14.4") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1jfxszmip5sd2mycjdbmb2mi5dsmxmvp1j2x512vd07awc1lp5vh") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.14.6 (c (n "relm-derive") (v "0.14.6") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0yjkkw7nc2fp5zcms140g0h4cnxfsp4zn9i4m231pq4yry8rnr5s") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.15.0 (c (n "relm-derive") (v "0.15.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "12jp4bgc3p9qqpkxxxirxaa3s9kk7wrm8v0zyk0gkd3118laajy8") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.15.1 (c (n "relm-derive") (v "0.15.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "059s9kzfxh5if1x01v29zp5kcd8fqal6lhvf20k4qm3k95zn1msa") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.15.2 (c (n "relm-derive") (v "0.15.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0njkmr88jx4wsprvv77yy8a67y64rwxbcj4v77bxlny1jbf3i68p") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.15.3 (c (n "relm-derive") (v "0.15.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "03j8g4f6bv451ndfh7sk6axj20r3kl9r5039790xpy8mcc46ly8k") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.16.0 (c (n "relm-derive") (v "0.16.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-derive-common") (r "^0.16.0") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0crz9l4j6i4d21bqc9z67sywbqhis58i1j68pj8bs7amvs73bzg7") (f (quote (("unstable" "relm-derive-common/unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-derive-0.17.0 (c (n "relm-derive") (v "0.17.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0faqv3dhkqh8ypwir71ynh3p4xmzdln0bf5013aylwnh1riixbvj")))

(define-public crate-relm-derive-0.18.0 (c (n "relm-derive") (v "0.18.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0scx7zkd4d0cm7a5vfjscn86mpcajywnd60mb33ac7nwaiq3jrpc")))

(define-public crate-relm-derive-0.19.0 (c (n "relm-derive") (v "0.19.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0sm1mdxpcclslka027xjx4pkrnbcxmfqg1fb6268349s71w8hk0f")))

(define-public crate-relm-derive-0.19.1 (c (n "relm-derive") (v "0.19.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "126s0j7lwqrm9iflpjdr3pjl9917np120mkgnmdf5kbsabwi5587")))

(define-public crate-relm-derive-0.20.0 (c (n "relm-derive") (v "0.20.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "09sqmy5vrfbxz8qa7vjqcfv3vhbwr9drv83gh1lh8sapwnpj0f6d")))

(define-public crate-relm-derive-0.21.0 (c (n "relm-derive") (v "0.21.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0sd0i3n3mv451agp9ly4dblrjqaqni9w36q8hqrl9z24q00svpzq")))

(define-public crate-relm-derive-0.22.0 (c (n "relm-derive") (v "0.22.0") (d (list (d (n "gtk") (r "^0.14.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "04vk28q8ixxq7skr19hzp6rzl70lgy9rcnhdg03qmkchfaz5mrd9")))

(define-public crate-relm-derive-0.23.0 (c (n "relm-derive") (v "0.23.0") (d (list (d (n "gtk") (r "^0.15.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "132vy72isxdd1vxnsajnqqzik246c47b794gd6dwa87jhyhwpp8k")))

(define-public crate-relm-derive-0.24.0 (c (n "relm-derive") (v "0.24.0") (d (list (d (n "gtk") (r "^0.16.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1qczdi63cjf9n3wz03xhyaw0r0lfwgms9pc5kw69v2mvrrwznzqv")))

