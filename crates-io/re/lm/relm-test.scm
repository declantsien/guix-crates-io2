(define-module (crates-io re lm relm-test) #:use-module (crates-io))

(define-public crate-relm-test-0.1.0 (c (n "relm-test") (v "0.1.0") (d (list (d (n "gdk-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.3") (d #t) (k 0)))) (h "0hv12lbj6jmjbf8c25h2isl9nqlp00qbb02m6p0bypgql0chg8za")))

(define-public crate-relm-test-0.1.1 (c (n "relm-test") (v "0.1.1") (d (list (d (n "gdk-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.3") (d #t) (k 0)))) (h "1pc818lazchqx3z12r26fxa7nj14b2jbbzan7p8kg34g5l1kqip7")))

(define-public crate-relm-test-0.10.0 (c (n "relm-test") (v "0.10.0") (d (list (d (n "gdk-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.3") (d #t) (k 0)))) (h "0lrvkyijkznivk5qfxx9r4qsdrk4nwldir1fman74z77xwiwxyam")))

(define-public crate-relm-test-0.10.1 (c (n "relm-test") (v "0.10.1") (d (list (d (n "gdk-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.4.0") (d #t) (k 0)))) (h "09vcwgf7qq9zjiw7piy5gbxva1jcx2p6ak8yinfw5x87mfky1irr")))

(define-public crate-relm-test-0.11.0 (c (n "relm-test") (v "0.11.0") (d (list (d (n "gdk-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.5.0") (d #t) (k 0)))) (h "18szw3pm02kpm1lnp97ahl39bnwdbizjrinplq8m7irbfw5nm851")))

(define-public crate-relm-test-0.12.0 (c (n "relm-test") (v "0.12.0") (d (list (d (n "gdk-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0nq2bq0j8cgflsfxh83kb02vc5sx23nbnqp06a1nv3ilv33ndfnc")))

(define-public crate-relm-test-0.13.0 (c (n "relm-test") (v "0.13.0") (d (list (d (n "gdk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0f8psxlpl7cprnnisvzs12k964kk2dj8l320d8r3r60qja81vinj")))

(define-public crate-relm-test-0.14.0 (c (n "relm-test") (v "0.14.0") (d (list (d (n "gdk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)))) (h "10kmszzz1zbzpv5h4hzgnrlvs8ryrxsv00z17as48vxk31rbfnrr")))

(define-public crate-relm-test-0.14.2 (c (n "relm-test") (v "0.14.2") (d (list (d (n "gdk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0x8yvqw4x8sak9l8ys2ar6z2wg1h4zgn1x3ayray29wzwr10qgzh") (y #t)))

(define-public crate-relm-test-0.14.3 (c (n "relm-test") (v "0.14.3") (d (list (d (n "gdk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1hd7n5680pzkfw8gbaiw8fnw7cb6arsqcrsg2zyhx54v3mf1pi04")))

(define-public crate-relm-test-0.14.4 (c (n "relm-test") (v "0.14.4") (d (list (d (n "gdk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1qpm3m53m2hr12pjjhyq4r3px0s9y0w2fw2dim5w8wzfrh6h8m20")))

(define-public crate-relm-test-0.14.6 (c (n "relm-test") (v "0.14.6") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "relm-core") (r "^0.14.0") (d #t) (k 0)))) (h "1fsxi6ic51ib18w1f6x1l60imd8xxnk1iqrdil7iv277mpq45dmp") (f (quote (("default" "gtk/v3_10"))))))

(define-public crate-relm-test-0.15.0 (c (n "relm-test") (v "0.15.0") (d (list (d (n "gtk-test") (r "^0.2") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "1hj4rqx0pv181hicg8wzkiizscf9r7qsz1ad2p55nhfdpaaj4a6r")))

(define-public crate-relm-test-0.15.1 (c (n "relm-test") (v "0.15.1") (d (list (d (n "gtk-test") (r "^0.2") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "04b1dwh1ldd6b32svavvdfvxadjsdd12spbszn1cspq68y0vi80d")))

(define-public crate-relm-test-0.15.2 (c (n "relm-test") (v "0.15.2") (d (list (d (n "gtk-test") (r "^0.2") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "1w2rr9wmphp9yzy9gkqa8af0syalmhm22nqfywxmhzbkjhzzi5g3")))

(define-public crate-relm-test-0.15.3 (c (n "relm-test") (v "0.15.3") (d (list (d (n "gtk-test") (r "^0.2") (d #t) (k 0)) (d (n "relm-core") (r "^0.15.0") (d #t) (k 0)))) (h "1vwgb2p5c0wx3zrw4m8vzv7ixfl72dvhx8qx8dmi2a7ylkbk2wxj")))

(define-public crate-relm-test-0.16.0 (c (n "relm-test") (v "0.16.0") (d (list (d (n "gtk-test") (r "^0.3") (d #t) (k 0)) (d (n "relm-core") (r "^0.16.0") (d #t) (k 0)))) (h "0lqsg6kywrm839sammcli8x56idr61mzg1cq6b5nls4g5biqgd25")))

(define-public crate-relm-test-0.17.0 (c (n "relm-test") (v "0.17.0") (d (list (d (n "gtk-test") (r "^0.4") (d #t) (k 0)) (d (n "relm") (r "^0.17.0") (d #t) (k 0)))) (h "0j4aiy6wxl2sqcpqccm86mbkj06pyk2vn40jig41jvgil8byizrc")))

(define-public crate-relm-test-0.18.0 (c (n "relm-test") (v "0.18.0") (d (list (d (n "gtk-test") (r "^0.4") (d #t) (k 0)) (d (n "relm") (r "^0.18.0") (d #t) (k 0)))) (h "1hjg68pnfsy94jfdxa8acin03zgs8yphqvcqp04c8i7399gwmyvv")))

(define-public crate-relm-test-0.19.0 (c (n "relm-test") (v "0.19.0") (d (list (d (n "gtk-test") (r "^0.5") (d #t) (k 0)) (d (n "relm") (r "^0.19.0") (d #t) (k 0)))) (h "06lfd3x38jmmm4azpbn256xq3yssb7j7612qrq7ypbkraqa4bnzr")))

(define-public crate-relm-test-0.20.0 (c (n "relm-test") (v "0.20.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.13.0") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk-test") (r "^0.6") (d #t) (k 0)) (d (n "relm") (r "^0.20.0") (d #t) (k 0)))) (h "1cnn4ckrlqshhgar8gl1al6jljknlj7iajmn17n9dwr0lf64zhh6")))

(define-public crate-relm-test-0.21.0 (c (n "relm-test") (v "0.21.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.13.0") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk-test") (r "^0.6") (d #t) (k 0)) (d (n "relm") (r "^0.21.0") (d #t) (k 0)))) (h "0r0a0i3c70fiy2qk6cgh6c1qrxm3vc1vwx1f8pk7p5v432l5xfz4")))

(define-public crate-relm-test-0.22.0 (c (n "relm-test") (v "0.22.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.14.0") (d #t) (k 0)) (d (n "glib") (r "^0.14.0") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "gtk-test") (r "^0.7") (d #t) (k 0)) (d (n "relm") (r "^0.22.0") (d #t) (k 0)))) (h "0rbcrn8aamwx5g48h5xv6zhq2zy3wl7p67piphjph0935c98ngvc")))

(define-public crate-relm-test-0.23.0 (c (n "relm-test") (v "0.23.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.15.0") (d #t) (k 0)) (d (n "glib") (r "^0.15.0") (d #t) (k 0)) (d (n "gtk") (r "^0.15.0") (d #t) (k 0)) (d (n "gtk-test") (r "^0.15.0") (d #t) (k 0)) (d (n "relm") (r "^0.23.0") (d #t) (k 0)))) (h "0lbshkllv7apj2zrzqid9pza68rdl15422fbswrl7qr606m314hw")))

(define-public crate-relm-test-0.24.0 (c (n "relm-test") (v "0.24.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.16.0") (d #t) (k 0)) (d (n "glib") (r "^0.16.2") (d #t) (k 0)) (d (n "gtk") (r "^0.16.1") (d #t) (k 0)) (d (n "gtk-test") (r "^0.16.0") (d #t) (k 0)) (d (n "relm") (r "^0.24.0") (d #t) (k 0)))) (h "12ww3dmihfazdjs21pdbjxkf7vjdj5jglwpy2c2n3pcjnndm45i4")))

