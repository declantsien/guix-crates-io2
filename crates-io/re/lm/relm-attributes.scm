(define-module (crates-io re lm relm-attributes) #:use-module (crates-io))

(define-public crate-relm-attributes-0.1.0 (c (n "relm-attributes") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0vl254nrngl1mfdyap71xdficv1qab67bjhp16kdsmir0jjg1klj")))

(define-public crate-relm-attributes-0.1.1 (c (n "relm-attributes") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1wafgp40w46j5nyyw94hq4k1yh13cmfjr21hpmvk4s269prjf8ff")))

(define-public crate-relm-attributes-0.1.2 (c (n "relm-attributes") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "015msmiy81ikcdls1c5lk1fhliry67im2ykk2fq1fvyjmi2zdd4v")))

(define-public crate-relm-attributes-0.1.3 (c (n "relm-attributes") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1yd88v40jilfqfzawbhxdlwpaqxcffciqkpqhv4ff87wfkl4pv7l")))

(define-public crate-relm-attributes-0.1.4 (c (n "relm-attributes") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1mxb7lprk7msq9jhia1p40868lnn5w9hpg61hq454kyjss0lqck2")))

(define-public crate-relm-attributes-0.6.0 (c (n "relm-attributes") (v "0.6.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0407g6jk4nwv5g6433zjla3v55f858d26cgddn3kxwmqkj7plmd1")))

(define-public crate-relm-attributes-0.7.0 (c (n "relm-attributes") (v "0.7.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1mbhqkasn2q0036r2n95iz0v9n4dkx6hx33vrn5hvi60s3sv66n8")))

(define-public crate-relm-attributes-0.8.0 (c (n "relm-attributes") (v "0.8.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "01w791qg65j2mkc7yhbsblq5lmyxz7xkkk1flwmaqpa5fgdc04kw")))

(define-public crate-relm-attributes-0.9.0 (c (n "relm-attributes") (v "0.9.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1zp42r0nfvs9xszcsclvqhs9cgqagr0ghj6aydkyf76sbvryn7ar")))

(define-public crate-relm-attributes-0.10.0 (c (n "relm-attributes") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0s8qcdbdbkzd2rlc6zga69nllx080j9pbzz5izc4sfssb6aj3733")))

(define-public crate-relm-attributes-0.11.0 (c (n "relm-attributes") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "04niwbmjs175bz9sgy67ykph9bn06w0dmjqp8xndwy2jwmgb93v9")))

(define-public crate-relm-attributes-0.12.0 (c (n "relm-attributes") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0mldlypzvhb8w0rsmmc0vnjc2mkkrp1jr5mjrwza539gdmsgn56i") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.13.0 (c (n "relm-attributes") (v "0.13.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1vpk6rlgw3dx7yi4bddk3266i1vaysfs7a6cvp1h0v5b5xr5n555") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.14.0 (c (n "relm-attributes") (v "0.14.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1j07pqjjhkgxrbnwix7gyzcaw1g89m4n5hhxiwrywk23nymdyx3n") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.14.2 (c (n "relm-attributes") (v "0.14.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1kl09b1151wr8qblhf8ki38sj4fzwkfrsvmpjrwjn84s500l7a07") (f (quote (("unstable" "relm-gen-widget/unstable")))) (y #t)))

(define-public crate-relm-attributes-0.14.3 (c (n "relm-attributes") (v "0.14.3") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1l4jr89gvq2p0as9mhb6zva7nsmzkjyd2db0jx93lcdrxprabaw5") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.14.4 (c (n "relm-attributes") (v "0.14.4") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1szf7zbil89i3c8kibcwdlby410f9fr9bbc2zwdxn08f3p5lq264") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.14.6 (c (n "relm-attributes") (v "0.14.6") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0xh20l8dwhv37jfh3kkymzdqac12qqd714nk3s8x7fkbc5jslfbm") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.15.0 (c (n "relm-attributes") (v "0.15.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1b8j4pnnl4py7p2kiij4mazf9v1359gaqylfqiz16bzi4jlpvhcv") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.15.1 (c (n "relm-attributes") (v "0.15.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "081ka0w66zif8p244xgwk61dbsmnddpy847550hynkzi712bkk9b") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.15.2 (c (n "relm-attributes") (v "0.15.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3fln1h2xd46z4y15ax63zl7g1wkd0p9q2m58kxnd2wibiyvd31") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.15.3 (c (n "relm-attributes") (v "0.15.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0c7c7dkmf086cafgcj4giqxpwc2l86rnd27j2d6p7s1ig88zfrr3") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

(define-public crate-relm-attributes-0.16.0 (c (n "relm-attributes") (v "0.16.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "relm-gen-widget") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwv0y3fzlx46fwph2w68w4kcvdsm977rb9s3w1bprzy253b53aa") (f (quote (("unstable" "relm-gen-widget/unstable"))))))

