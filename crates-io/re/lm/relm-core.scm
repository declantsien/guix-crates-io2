(define-module (crates-io re lm relm-core) #:use-module (crates-io))

(define-public crate-relm-core-0.1.0 (c (n "relm-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)) (d (n "glib-itc") (r "^0.1.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "1cbpf2bdfz404mp4r229qj4d13xvx7aj59r2awadi63lbdb24rl4")))

(define-public crate-relm-core-0.1.1 (c (n "relm-core") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)) (d (n "glib-itc") (r "^0.1.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "11hpw8j3rq4pllbhkkb0z7by95midzl8gr7zd7d3g6fb1azyg9m3")))

(define-public crate-relm-core-0.1.2 (c (n "relm-core") (v "0.1.2") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)) (d (n "glib-itc") (r "^0.1.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "0g71k3wp617a842rxwxq6dz5z7yyc995l5xhnaac24wf4c1f7jqh")))

(define-public crate-relm-core-0.1.3 (c (n "relm-core") (v "0.1.3") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)) (d (n "glib-itc") (r "^0.1.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "03ag5ym9q4klbl47pa01ll2m8l73qb4fqm6c1k8adg9196pq7rjd")))

(define-public crate-relm-core-0.1.4 (c (n "relm-core") (v "0.1.4") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)) (d (n "glib-itc") (r "^0.1.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "19rws74b5wl4h50l9x0p92zg0fmkipk9ga6inxj71nkvmxg9jn3f")))

(define-public crate-relm-core-0.10.0 (c (n "relm-core") (v "0.10.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.1.0") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)))) (h "0icxk8xcm7ry54m3b453j54p68sk918fh8dz87jgwyrwavnnlpqa")))

(define-public crate-relm-core-0.10.1 (c (n "relm-core") (v "0.10.1") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.2.0") (d #t) (k 0)) (d (n "glib") (r "^0.1.2") (d #t) (k 2)))) (h "0cbhr2zr4wmzj1gxfwi654xn0j1qbpagqklhqrm7daf2s9108lg1")))

(define-public crate-relm-core-0.11.0 (c (n "relm-core") (v "0.11.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 2)))) (h "00m0vzzzq5qn1pyx7wsfpgagv7y6s3yvd0axd6vm7a1qmwwhzqvb")))

(define-public crate-relm-core-0.12.0 (c (n "relm-core") (v "0.12.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 2)) (d (n "gtk") (r "^0.3.0") (d #t) (k 0)))) (h "1qdqr0s83w8z9l3mchxkzrxawf8q6868kk4lxj7i5rpghgk013al")))

(define-public crate-relm-core-0.13.0 (c (n "relm-core") (v "0.13.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "futures-glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)))) (h "1d5vz2ajrqik42zac7h2fpqigvaf902kz4ap7rppyx1w4sdsr96a")))

(define-public crate-relm-core-0.14.0 (c (n "relm-core") (v "0.14.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1mb57xihz1hqjxwj80zw6cq7rrw55kc35rzvnpbvl1bwrap0syiv")))

(define-public crate-relm-core-0.14.2 (c (n "relm-core") (v "0.14.2") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1hfmmawq7xp3pf2ng889lk3sb8yxvqa8nzxm5lq0zsjjnr2h65y1") (y #t)))

(define-public crate-relm-core-0.14.3 (c (n "relm-core") (v "0.14.3") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0q61vxf6wzzki7i6dk8xh9468hdy27x5bx8w7d9ilnz3swbfmdmk")))

(define-public crate-relm-core-0.14.4 (c (n "relm-core") (v "0.14.4") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1gvjshbnx6awwh7kb223dkyq6gzqnr55sv4z6p5n5cip926p3wp7")))

(define-public crate-relm-core-0.14.5 (c (n "relm-core") (v "0.14.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "093k9wjkdc9p1ym21n6x5sw2fgn9ndmy02zdp2c38xy22gj00n0v")))

(define-public crate-relm-core-0.14.6 (c (n "relm-core") (v "0.14.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1wd507yhs6cpw91ayyczika5kl2y46cajy7nys72drrwzi8dldz9")))

(define-public crate-relm-core-0.15.0 (c (n "relm-core") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1pfcvhjm0bh95bk78i0r8mzsmlfv5yrbk88iq7j2mbl5q9aj22wz")))

(define-public crate-relm-core-0.15.1 (c (n "relm-core") (v "0.15.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0rr6aj8qg8ihwzhhqf8mx3bv64zk5qb0a853g00scsj2w7l18m12")))

(define-public crate-relm-core-0.15.2 (c (n "relm-core") (v "0.15.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0vz11xx0ync8p8iqygbwh34my7a77xc64a7r1xrfyx9mmjz33825")))

(define-public crate-relm-core-0.15.3 (c (n "relm-core") (v "0.15.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0mwvjmi8v7sydhmhbknbx7fdgzha1lrmycjk2j0adpzp80cgpdj2")))

(define-public crate-relm-core-0.16.0 (c (n "relm-core") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "glib") (r "^0.7.0") (d #t) (k 0)) (d (n "glib") (r "^0.7.0") (d #t) (k 2)) (d (n "glib-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "15hvb470m8ncwn4mb0540syj255r9f7zfbw5b75zqwbg3f3j7s83")))

