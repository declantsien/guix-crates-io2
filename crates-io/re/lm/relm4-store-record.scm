(define-module (crates-io re lm relm4-store-record) #:use-module (crates-io))

(define-public crate-relm4-store-record-0.1.0-beta.1 (c (n "relm4-store-record") (v "0.1.0-beta.1") (d (list (d (n "reexport") (r "^0.1.0-beta") (d #t) (k 0) (p "relm4-store-reexport")))) (h "1cb2nlfgp0d2q9bshckmgspb0kac7gfmkxrsvhydv0p79apgjhc3")))

(define-public crate-relm4-store-record-0.1.0-beta.2 (c (n "relm4-store-record") (v "0.1.0-beta.2") (d (list (d (n "reexport") (r "^0.1.0-beta") (d #t) (k 0) (p "relm4-store-reexport")))) (h "1j08qla8r06xwvwlzw969icdsd2lsr9l2vrvacmm9f3k7fspxjd3")))

