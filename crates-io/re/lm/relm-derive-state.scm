(define-module (crates-io re lm relm-derive-state) #:use-module (crates-io))

(define-public crate-relm-derive-state-0.10.0 (c (n "relm-derive-state") (v "0.10.0") (d (list (d (n "relm-derive-common") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1z136nkw529r1fyd5w125q5d6r9p5si8n5cnr22fpg0h0byhnvpa")))

(define-public crate-relm-derive-state-0.11.0 (c (n "relm-derive-state") (v "0.11.0") (d (list (d (n "relm-derive-common") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "125gqi1hp5w3fkx86qvy0rvf5wn3swrjydmdcicw2b6mp05ags9c")))

(define-public crate-relm-derive-state-0.12.0 (c (n "relm-derive-state") (v "0.12.0") (d (list (d (n "relm-derive-common") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0qw6m0jjf06x3as5p293b39s8pdpl2kfbqjidnlziwhb1ckhcmxm") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.13.0 (c (n "relm-derive-state") (v "0.13.0") (d (list (d (n "relm-derive-common") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1hk85ncwspgxys6m4rjwj7jq2680kz016aib2ff627gdsj5zrvb7") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.14.0 (c (n "relm-derive-state") (v "0.14.0") (d (list (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0l7cg8lgfk2chc1y8lhyjf0m3qri2n2imjz72xbl4ra0xmxacsh2") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.14.2 (c (n "relm-derive-state") (v "0.14.2") (d (list (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0mdxxga0cqjzh7ijpf12b1gmxg35aydsj4w12fqpkmqbd6blz2lf") (f (quote (("unstable" "relm-derive-common/unstable")))) (y #t)))

(define-public crate-relm-derive-state-0.14.3 (c (n "relm-derive-state") (v "0.14.3") (d (list (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "01r4lwi7kp2fmijlgvg828g2nqw6ndzvqi7ysy68dqdvnkm4qf18") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.14.4 (c (n "relm-derive-state") (v "0.14.4") (d (list (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "12px0z3d4dx21p039ays70iwvpxyan5ii236dniz4v10plghaxds") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.14.6 (c (n "relm-derive-state") (v "0.14.6") (d (list (d (n "relm-derive-common") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1298f31lmczwf4k8jsnspipjba7zy8ch48rwyn59fiv8bm0cz0fc") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.15.0 (c (n "relm-derive-state") (v "0.15.0") (d (list (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1ds7j9ah7sm7dk1kg1mn0rfa5s78qjsp3kjjgihbxylzd3gy8v99") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.15.1 (c (n "relm-derive-state") (v "0.15.1") (d (list (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1r77x867mxzkxwvfdgvfw6ddplpf58jprjhiw6c91z8g8mmcffkp") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.15.2 (c (n "relm-derive-state") (v "0.15.2") (d (list (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18klxxhrkjlg8ippzw195j4haanv41h8lz6dys29s8knfnxnb3qg") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.15.3 (c (n "relm-derive-state") (v "0.15.3") (d (list (d (n "relm-derive-common") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07dq46jhs6zz0wvi00dz0h3sb70hrvrwx459dxvdg0jsbn0jdmkv") (f (quote (("unstable" "relm-derive-common/unstable"))))))

(define-public crate-relm-derive-state-0.16.0 (c (n "relm-derive-state") (v "0.16.0") (d (list (d (n "relm-derive-common") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qpfc3imzq1q54bssiz3b1pm5yk32jxwbrzmp1bwm9fwhv0dlhmm") (f (quote (("unstable" "relm-derive-common/unstable"))))))

