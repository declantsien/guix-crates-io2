(define-module (crates-io re lm relm-gen-widget) #:use-module (crates-io))

(define-public crate-relm-gen-widget-0.1.0 (c (n "relm-gen-widget") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1d3akn2v6s068yaxfc2ljbqvkq46mhd3fln3s2hbk1lbrfhsmwkd")))

(define-public crate-relm-gen-widget-0.1.1 (c (n "relm-gen-widget") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0r3fnzq6yksja1bhv9945n12js2zjqqypilh3b9l12ayc1wdiqbc")))

(define-public crate-relm-gen-widget-0.1.2 (c (n "relm-gen-widget") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0wk0352f93c5fr14wfxkqchy3cfammyvdh70q8hw6slzkakdck2n")))

(define-public crate-relm-gen-widget-0.1.3 (c (n "relm-gen-widget") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0ir09y07agrwdfwkyvrpr05qj9855iwxlbx3ahyc6rzrd14146w3")))

(define-public crate-relm-gen-widget-0.1.4 (c (n "relm-gen-widget") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1amw181174q9mizgmqy0mgvfa7lkqa5s570iwdjxfggr45p8pymk")))

(define-public crate-relm-gen-widget-0.1.5 (c (n "relm-gen-widget") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1yifla7s73nnmrbw8w6gqrg188d5cxvdmnxvxapyw6c1cbr6z5lk")))

(define-public crate-relm-gen-widget-0.1.6 (c (n "relm-gen-widget") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0xbfj73d6mgp6h9i7lgpz1qw9xzwsh3rb0nnb66xhsgyjwk7hlbb")))

(define-public crate-relm-gen-widget-0.1.7 (c (n "relm-gen-widget") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "07m8h63kn190kh7zsi8l3v0xjdlmwhbnpd3s11ljivwfcgq8893j")))

(define-public crate-relm-gen-widget-0.1.8 (c (n "relm-gen-widget") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1ivx1k052wsrarnksxz6ifpxx4wd9qb9ac2w453a2zqn656mvzqc")))

(define-public crate-relm-gen-widget-0.6.0 (c (n "relm-gen-widget") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0jl3ih1r4x8bhf9gq5bcsi09zxr3jdk3fbbh2fh25wkmhr2339yw")))

(define-public crate-relm-gen-widget-0.6.1 (c (n "relm-gen-widget") (v "0.6.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "00sv7ah9j19wj792msqddnkqpljxg57c57aqxfls1329xf60r9bs")))

(define-public crate-relm-gen-widget-0.6.2 (c (n "relm-gen-widget") (v "0.6.2") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1lbaplm1hbmb262a98hrkxv6d4zsxnvsgz9zb3ijsff71qw5qpdi")))

(define-public crate-relm-gen-widget-0.6.3 (c (n "relm-gen-widget") (v "0.6.3") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0dhlz5jv48053ayfh0c1lx0mhq5kh83nccs3qgm0788r3n69ya5s")))

(define-public crate-relm-gen-widget-0.6.4 (c (n "relm-gen-widget") (v "0.6.4") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0nddb80r3rq7mrmfmywjwxz9mzys5sv5f0q41ahsw50hz0pc9j8f")))

(define-public crate-relm-gen-widget-0.6.5 (c (n "relm-gen-widget") (v "0.6.5") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1ycfvz0hhp9xps4krbsmi0pwlk8smk6w7gzh9cj7lgvgaq3sh5c5")))

(define-public crate-relm-gen-widget-0.6.6 (c (n "relm-gen-widget") (v "0.6.6") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0vyfpgv4rjll5v127cp20nhzrd27l433pj0bymlxd4yrr38yki53")))

(define-public crate-relm-gen-widget-0.6.7 (c (n "relm-gen-widget") (v "0.6.7") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1yb4k7rfmy82a37zplhb11kj0kdjzg7y2a8lcc7jzkypckh5kplv")))

(define-public crate-relm-gen-widget-0.6.8 (c (n "relm-gen-widget") (v "0.6.8") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0rw19jnnnm8fsm75xf56yhagsqwalypiyl9bgyykppr1w9grsn18")))

(define-public crate-relm-gen-widget-0.6.9 (c (n "relm-gen-widget") (v "0.6.9") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "16g285ahcgldf0wyyy9d6ppwnxwc05vfrkhj4mqj0m5j3jphpshf")))

(define-public crate-relm-gen-widget-0.6.10 (c (n "relm-gen-widget") (v "0.6.10") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "09gim21ipf2fbz82nw11h2r5ywwjbpzlcmkbd5qnilab9v3h7cbh")))

(define-public crate-relm-gen-widget-0.7.0 (c (n "relm-gen-widget") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1r2bhf6drd91rd30wflkxw78f6q1fqjx99d8ks236kvz0hc1i2cr")))

(define-public crate-relm-gen-widget-0.7.1 (c (n "relm-gen-widget") (v "0.7.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1v2khjcbjz2z8m15j2w0kabc09krg4g0zdrpbp6xb3hpqc3vi9gz")))

(define-public crate-relm-gen-widget-0.7.2 (c (n "relm-gen-widget") (v "0.7.2") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "09b7cc1d7n0xz298bwamx7n7yyg01hrnz4irckfjd0shabn9b76a")))

(define-public crate-relm-gen-widget-0.7.3 (c (n "relm-gen-widget") (v "0.7.3") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1i66h47bfw1gkrwrv6m9isjpdmcaw129j0lzv1lmvyxw3rc2kl05")))

(define-public crate-relm-gen-widget-0.7.4 (c (n "relm-gen-widget") (v "0.7.4") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "16zrl2rvdvry46m2wl5gan17aa4n59jai2fl0z3p7iinljlrcw73")))

(define-public crate-relm-gen-widget-0.7.5 (c (n "relm-gen-widget") (v "0.7.5") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "089dmwyf2s0mqqgpcqhbip6spjd60dyl5nbl03xr8zvgh049m1xz")))

(define-public crate-relm-gen-widget-0.7.6 (c (n "relm-gen-widget") (v "0.7.6") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1kr17fjni2f3knrad9vp4svmdfalx4w8m162l6nc9xx2b03szg6x")))

(define-public crate-relm-gen-widget-0.8.0 (c (n "relm-gen-widget") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0d3qlx81vv0n96dvrfxlfcqab219xndh8lh8wfa40jaxc55jr3la")))

(define-public crate-relm-gen-widget-0.9.0 (c (n "relm-gen-widget") (v "0.9.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1bmxx2zkbqlzfhyy1vagff38asdzald9f811ym9c1bw42iqkx38f")))

(define-public crate-relm-gen-widget-0.9.1 (c (n "relm-gen-widget") (v "0.9.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1cjyxfxds1ckzbrhgkwdf0byf05cv335r6drqfmknhlsvplf11zb")))

(define-public crate-relm-gen-widget-0.9.2 (c (n "relm-gen-widget") (v "0.9.2") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1f6mdfdgzg9vvisbxckh2nawz2v0as9f6nvx25g680by1xc3dr79")))

(define-public crate-relm-gen-widget-0.9.3 (c (n "relm-gen-widget") (v "0.9.3") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "17b8y9g03hshirb86prhjsl66ckdw53siyqiyf8l80hxhfki1b9k")))

(define-public crate-relm-gen-widget-0.9.4 (c (n "relm-gen-widget") (v "0.9.4") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "022pbph52v6zq6npd01svv63flcshca5grswnalwj2w8pcc2xm5s")))

(define-public crate-relm-gen-widget-0.9.5 (c (n "relm-gen-widget") (v "0.9.5") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0kmkrgs6xsnbwgi3kw7b6z6g9g1ak3r0a91p3jx9m3kin08v2p49")))

(define-public crate-relm-gen-widget-0.9.6 (c (n "relm-gen-widget") (v "0.9.6") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "16kp6v7nsswc20q8063di4xkkvkjkp00wp1srdmik4xwskiarb9w")))

(define-public crate-relm-gen-widget-0.9.7 (c (n "relm-gen-widget") (v "0.9.7") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "15dbqqhfrmi8vcwn97bf1jxmh47fk6c0d15acpk4193ml5hs2csy")))

(define-public crate-relm-gen-widget-0.9.8 (c (n "relm-gen-widget") (v "0.9.8") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0dj9fb9makq7699z7l5wnzfb4h5jd2nf7i2wvnqf4m7pvj4hwisw")))

(define-public crate-relm-gen-widget-0.9.9 (c (n "relm-gen-widget") (v "0.9.9") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "10cgg0jjijh733p3lyk5mn1dghz4jfz2baf9v9vv1ph08m2ay8j3")))

(define-public crate-relm-gen-widget-0.9.10 (c (n "relm-gen-widget") (v "0.9.10") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0d3yq1mrp0pxi6j84ix019wqkp4k60y9c9jrwpwm7mg3a94vqsky")))

(define-public crate-relm-gen-widget-0.10.0 (c (n "relm-gen-widget") (v "0.10.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "0w9skgpqdz67cinbmq8k4jdvxn9sxm6fjg0ppyy3j2dyhb7xd48a")))

(define-public crate-relm-gen-widget-0.11.0 (c (n "relm-gen-widget") (v "0.11.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (f (quote ("fold" "full" "visit"))) (d #t) (k 0)))) (h "1kzj84myw3887ajn43i22bf2qsl94zwlcwxyaxdqiws217rb2n9v")))

(define-public crate-relm-gen-widget-0.12.0 (c (n "relm-gen-widget") (v "0.12.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1b22n7z6wb3p2yv53ww4946q4irxw0fs5sf3cqipx77m1h5l0h47") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.13.0 (c (n "relm-gen-widget") (v "0.13.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1pywrnsznjrlr2bkqdyffaxzb4n8nr2z3snkpmlksrpirdaccd7i") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.14.0 (c (n "relm-gen-widget") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1hf8fckqj6x7gxv5aks2ydrj16k3yqx2pym883kwx0nx9q8xzp55") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.14.1 (c (n "relm-gen-widget") (v "0.14.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "18jfr1kqkg8lrbsdamp5i7b2223lqncsn07r8c5fkpv3730pgd04") (f (quote (("unstable" "proc-macro2/nightly")))) (y #t)))

(define-public crate-relm-gen-widget-0.14.2 (c (n "relm-gen-widget") (v "0.14.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1fn12igz4vazd1d4xmbdlrzn816hr9gf9zqrzd918afrnjw43yd4") (f (quote (("unstable" "proc-macro2/nightly")))) (y #t)))

(define-public crate-relm-gen-widget-0.14.3 (c (n "relm-gen-widget") (v "0.14.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0l4vcdkfalaly3yhacmjif6mxwkbf47n85zalqpj55r00d3fj2ij") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.14.4 (c (n "relm-gen-widget") (v "0.14.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0sq26spvkwp3gavzwxrz6xx7xn7546hmx8435f75hxg8rmb09l07") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.14.6 (c (n "relm-gen-widget") (v "0.14.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "17bg6w0c9f97pzlh0cz8xb947bjm2287nvwxjfk82w7fr9qsbbwy") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.15.0 (c (n "relm-gen-widget") (v "0.15.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0ycni163mpaz34f5ggy6ry0w2zkxr2y62x53fd7vfzfz92zvwmay") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.15.1 (c (n "relm-gen-widget") (v "0.15.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1icbvlc3vin0mq1igmgmq7lbj4hzzzx63i245ia1wm6hbpnjzaaz") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.15.2 (c (n "relm-gen-widget") (v "0.15.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "0kd9kkip9gp4iax3ahg2ss957g02dgs2l5hwdzskmf6czzg7qfnv") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.15.3 (c (n "relm-gen-widget") (v "0.15.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "153as9nsn6lix442yakz84d0gcfaqpm77hbxyz3fryz9b2a55ks7") (f (quote (("unstable" "proc-macro2/nightly"))))))

(define-public crate-relm-gen-widget-0.16.0 (c (n "relm-gen-widget") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full" "visit"))) (d #t) (k 0)))) (h "1c7vcqkha9c3n58f3i4ji952mvl5ywgchdvbzf82rhdxajq4gba9") (f (quote (("unstable" "proc-macro2/nightly"))))))

