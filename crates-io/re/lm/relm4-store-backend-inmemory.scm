(define-module (crates-io re lm relm4-store-backend-inmemory) #:use-module (crates-io))

(define-public crate-relm4-store-backend-inmemory-0.1.0-beta.1 (c (n "relm4-store-backend-inmemory") (v "0.1.0-beta.1") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")))) (h "15r6qkcgpw4g982sw7m6l88ymz7aca4dm9b9xag3s6mqxypc805m")))

(define-public crate-relm4-store-backend-inmemory-0.1.0-beta.2 (c (n "relm4-store-backend-inmemory") (v "0.1.0-beta.2") (d (list (d (n "record") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-record")) (d (n "reexport") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store-reexport")) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "store") (r "^0.1.0-beta.1") (d #t) (k 0) (p "relm4-store")))) (h "04skvzrvkwssljn76nfml2ypknnf04wvjsmp2wr94rpagijlxwx7")))

