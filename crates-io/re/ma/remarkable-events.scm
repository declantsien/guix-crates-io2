(define-module (crates-io re ma remarkable-events) #:use-module (crates-io))

(define-public crate-remarkable-events-0.1.0 (c (n "remarkable-events") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("rt" "fs" "io-util"))) (k 0)))) (h "0kbj7r0k692pfh35sw082pnk16af0kg1ddc5i151mdxwh0s2r5j7")))

(define-public crate-remarkable-events-0.1.1 (c (n "remarkable-events") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("rt" "fs" "io-util"))) (k 0)))) (h "0hn59dz4818x2rgv78k59lcrr4m452h8p1fry8bk2jkq3km0gi4j")))

