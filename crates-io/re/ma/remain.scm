(define-module (crates-io re ma remain) #:use-module (crates-io))

(define-public crate-remain-0.1.0 (c (n "remain") (v "0.1.0") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "1s6bjirp5mbah4m2l8vr7dci2ysl8k3sqg7x1wg0273sd3gr9dgq")))

(define-public crate-remain-0.1.1 (c (n "remain") (v "0.1.1") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "1r9pgrdlsw1fdz1bgx12mvg8kd9dw9sami8cskbwcf3y0jnnqr6g")))

(define-public crate-remain-0.1.2 (c (n "remain") (v "0.1.2") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "02g2i42r7zjjbizsg31mjlw4rkqqwa1f1z97vjgm7r0bnm1jbv1v")))

(define-public crate-remain-0.1.3 (c (n "remain") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "select-rustc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0r0d2lm21jwy2zs7w2wpwgl4hj188nyq21sh8wsv2ab0jg6i8p3j")))

(define-public crate-remain-0.1.4 (c (n "remain") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "130f9j0sfl9y7cav9hf5bzy7s8gqx9mc76hvg5s0bdhzr0pf72dc")))

(define-public crate-remain-0.1.5 (c (n "remain") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dsm0d4c2ny8ix33vv6pvzaj121950mm78g8l4h7qqwqc6qks2w2")))

(define-public crate-remain-0.2.0 (c (n "remain") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fk8r2brwya883rwldg1fivfqlmclw4jnaaxl8mv0nzyr8mfl7kv")))

(define-public crate-remain-0.2.1 (c (n "remain") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "125qvj48zhj5glfynhqa10vhak6898a5hgdaznk8s364gwi63j4r")))

(define-public crate-remain-0.2.2 (c (n "remain") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "19rycw933wdsbs3k74zf96z43adr416zshpn7swjqhb8z9w1xfkh")))

(define-public crate-remain-0.2.3 (c (n "remain") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "194z40b98vqqv4pcikjjir373wcnrf5cqcaqi5i1gb44lc72fd8c") (r "1.31")))

(define-public crate-remain-0.2.4 (c (n "remain") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0fz2vpdil9wzggnfqb3h8jyn0jwiw9f0y9ic9an2f6r71nf55pq6") (r "1.31")))

(define-public crate-remain-0.2.5 (c (n "remain") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kvny96nml9mdz3879v97cxvkids0f6dlaxxczvq1cdf79mbr08s") (r "1.31")))

(define-public crate-remain-0.2.6 (c (n "remain") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0fxs4xqifi9fm6h6q93br78af3ms2x9p4c2ly0145m9gm76y412p") (r "1.31")))

(define-public crate-remain-0.2.7 (c (n "remain") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zxaq4v4zga47gds4ddr515jqxj7z3h1glynpb5jx4kn8sdpsjrz") (r "1.31")))

(define-public crate-remain-0.2.8 (c (n "remain") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "032720l3fj6h77raips8a3pd5ym314q2qjyhj2ix7386gljwlg71") (r "1.56")))

(define-public crate-remain-0.2.9 (c (n "remain") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gwivkxfbm8r52sf6ld3i62pj8vw10yqrj4q6ah3w7khqnxm6wva") (r "1.56")))

(define-public crate-remain-0.2.10 (c (n "remain") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hk1k3f8a6yj87vsq9yyjkk4445j0ya1778sjw5zbyznj7aq87qa") (r "1.56")))

(define-public crate-remain-0.2.11 (c (n "remain") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "10m0z95wfc5qdvll6wby7qjbqkrnjylxprcfac3pvrifkl9sgqxw") (r "1.56")))

(define-public crate-remain-0.2.12 (c (n "remain") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "157pzbzkwrj1m111ns3mmr3yla4qnrd0qiij0m6jgb8c4c8y1m8s") (r "1.56")))

(define-public crate-remain-0.2.13 (c (n "remain") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jpbjginxnxd0jbdzg6gi1hp5h11w90d8wv0mbh7x54a568277xd") (r "1.56")))

(define-public crate-remain-0.2.14 (c (n "remain") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hvlmr14qhg7dnwz8lv8zm7514gfh5dwcvnsb9adwdi7hh7zibj6") (r "1.56")))

