(define-module (crates-io re ma remarklib) #:use-module (crates-io))

(define-public crate-remarklib-0.1.0 (c (n "remarklib") (v "0.1.0") (d (list (d (n "function_string_builder") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "079n5j19lcpp5jpwvjvna47y2gwym2p65hswy5x94bgz7nhmvpdg")))

(define-public crate-remarklib-0.2.0 (c (n "remarklib") (v "0.2.0") (d (list (d (n "function_string_builder") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "06v5592xpx4iq1iqgf9nby3l0l3gnxdbw03hhv8v703ir66kq9ak")))

(define-public crate-remarklib-0.3.0 (c (n "remarklib") (v "0.3.0") (d (list (d (n "function_string_builder") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0i28zklm2z1vcclq6bmlpyi871sl22b5ggzshmlbpa1fzdms1asm")))

