(define-module (crates-io re ma remarkable-cloud) #:use-module (crates-io))

(define-public crate-remarkable-cloud-0.1.0 (c (n "remarkable-cloud") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "remarkable-cloud-api") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03a66aj3zb7da2cmkqm2l48idbd8ymmri1x58qzdkxi9m5d2pvrz")))

