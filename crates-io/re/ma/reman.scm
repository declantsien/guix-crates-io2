(define-module (crates-io re ma reman) #:use-module (crates-io))

(define-public crate-reman-0.1.0 (c (n "reman") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1slw1cf2i75cjqa4gifg2xxyqhmz6aw4d16f0sffjn7sc03yxmgp")))

(define-public crate-reman-0.2.0 (c (n "reman") (v "0.2.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "044jfy5sj1j55n0zsygkv51asn38z73238gvg5apkskjgm6iga96")))

(define-public crate-reman-0.3.0 (c (n "reman") (v "0.3.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0d0swfrasagclhykvwc0fini8i2krx2nrwj9img80f154fd4qw24")))

