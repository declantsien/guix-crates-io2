(define-module (crates-io re ma remark-interpreter) #:use-module (crates-io))

(define-public crate-remark-interpreter-0.1.0 (c (n "remark-interpreter") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "remarklib") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0wb57sbnnjd421n9lwdxb4h67yxflswwfni0pyds2l40q38slq5k")))

