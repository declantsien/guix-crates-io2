(define-module (crates-io re ma remake) #:use-module (crates-io))

(define-public crate-remake-0.1.0 (c (n "remake") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.15") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0bxgs42v655bxki7nfswjj560qsfsjv3s9c0942q2xjq2z6fb0jc") (f (quote (("unstable"))))))

