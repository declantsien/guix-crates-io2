(define-module (crates-io re ma remarkable_lines) #:use-module (crates-io))

(define-public crate-remarkable_lines-0.1.0 (c (n "remarkable_lines") (v "0.1.0") (h "1cfn7i45rji8sn4fz1mxyczyrzv7r8pn9nz9rfrakbp3dxc0yqdi") (r "1.69.0")))

(define-public crate-remarkable_lines-0.1.1 (c (n "remarkable_lines") (v "0.1.1") (h "1n3p7rrc0nyr463zii6im778fjpirm74f355rfpykz59pby9v6xg") (r "1.69.0")))

(define-public crate-remarkable_lines-0.1.2 (c (n "remarkable_lines") (v "0.1.2") (h "0616iyw0pjbxqmv2g0d7sykw7jhpkia3qgs9vi4qfg6qw1ss3bqa") (r "1.69.0")))

