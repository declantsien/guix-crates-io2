(define-module (crates-io re ma remarkjs) #:use-module (crates-io))

(define-public crate-remarkjs-0.1.0 (c (n "remarkjs") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.1.3") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "notify") (r "= 5.0.0-pre.1") (d #t) (k 0)))) (h "0w23l6x2ivc9y17ssz09zp2nz5w8pkw5d5kcq26fir5qx2dpk32x")))

(define-public crate-remarkjs-0.1.1 (c (n "remarkjs") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.1.3") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "notify") (r "= 5.0.0-pre.1") (d #t) (k 0)) (d (n "open") (r "^1.3.2") (d #t) (k 0)))) (h "0dj0i34spphj7gxbhlxll35mnyp157fdgzw0099r800djvph0nxb")))

