(define-module (crates-io re mt remtools) #:use-module (crates-io))

(define-public crate-remtools-0.0.0 (c (n "remtools") (v "0.0.0") (h "0lrnpbbiwh6vsw8avfjlb0938lfhiry9agczwqw0qyyv9wai43fm")))

(define-public crate-remtools-1.0.0 (c (n "remtools") (v "1.0.0") (h "0dlvjk0gr2xkq35mncyx4x0q4gsklqm049hmmfhmchiji3h6cxz1")))

