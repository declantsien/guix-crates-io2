(define-module (crates-io re bp rebpf) #:use-module (crates-io))

(define-public crate-rebpf-0.1.0 (c (n "rebpf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rebpf-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1w9m5l8ml87f505g5545kwq6rmyrl7avcnib954z1jx241a5cxad")))

(define-public crate-rebpf-0.1.1 (c (n "rebpf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rebpf-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16qprlx6gbzmhrxivclajjs3glhzq5yccrgavgwynnm7iscfp6la")))

(define-public crate-rebpf-0.1.2 (c (n "rebpf") (v "0.1.2") (d (list (d (n "function_name") (r "^0.2.0") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.0.7-1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0chp6vp03bzhb4xrd101zbjdvk7bmw3lcvg7shdc8mi2i16r5y3d")))

(define-public crate-rebpf-0.1.3 (c (n "rebpf") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "function_name") (r "^0.2.0") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.0.7-1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.1") (d #t) (k 0)))) (h "12dg56mjgzi0y0jzbcb58lqc2p5xg7w0syzbkkyl2994prpj1ril") (f (quote (("userspace") ("default" "bpf" "userspace") ("bpf"))))))

(define-public crate-rebpf-0.1.4 (c (n "rebpf") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "function_name") (r "^0.2.0") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.0.7-1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0in1q0l1qf64gx5znf8l062fqcrp73xps4mfhv5yncg9sx1cxs2p") (f (quote (("userspace") ("default" "bpf" "userspace") ("bpf"))))))

(define-public crate-rebpf-0.1.5 (c (n "rebpf") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "function_name") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.0.7-1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0") (d #t) (k 0)) (d (n "rebpf-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c7z0wkcvm3fjqzz6b3hcrrfl84srmd7821kh5llrx656lv404av") (f (quote (("userspace") ("default" "bpf" "userspace") ("bpf"))))))

