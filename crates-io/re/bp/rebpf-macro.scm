(define-module (crates-io re bp rebpf-macro) #:use-module (crates-io))

(define-public crate-rebpf-macro-0.1.0 (c (n "rebpf-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0pp0c905698xnsn6gznf4l44v0yrdylf7lpcw5v1aj74liwka1vq")))

(define-public crate-rebpf-macro-0.1.1 (c (n "rebpf-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1nljb6lyc1mii5dagvis4s4nc0kxqa93szsh6bkxpkn21q4f81f1")))

