(define-module (crates-io re ca recastnavigation-rs) #:use-module (crates-io))

(define-public crate-recastnavigation-rs-0.1.0 (c (n "recastnavigation-rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "miniz_oxide") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13zn9ic91nlp7mgbqbr63mjccqqq76gfsa99pwp0c69lqfwkjvjr") (f (quote (("default" "rkyv")))) (s 2) (e (quote (("rkyv" "dep:rkyv" "dep:miniz_oxide")))) (r "1.75")))

