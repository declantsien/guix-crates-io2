(define-module (crates-io re ca recase) #:use-module (crates-io))

(define-public crate-recase-0.1.0 (c (n "recase") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1j6ssnz5j38ha3w3gw1sx7znrvfd96ylgyj90m2lxl623xjnhgcp")))

(define-public crate-recase-0.1.1 (c (n "recase") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0nqrcw3pws7x371pcryqk6n5450w0qqr31iszyl10sr6jw3a2fp8")))

(define-public crate-recase-0.2.0 (c (n "recase") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1dpighfxnmbn2hb0rg3mnrf5klyljqdgshbkblvpg02vjc3zw64f")))

(define-public crate-recase-0.3.0 (c (n "recase") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0hdyvwxzi1ylh7zq31w7fb147cr6qxhnmnp4r9fvcqi4s189qm34")))

