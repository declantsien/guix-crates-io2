(define-module (crates-io re ca recast_navigation) #:use-module (crates-io))

(define-public crate-recast_navigation-0.1.0 (c (n "recast_navigation") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "meshopt") (r "^0.1") (d #t) (k 0)) (d (n "rapier3d-f64") (r "^0.16") (f (quote ("simd-stable"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "0nfilan5fry37w5as7pl7bc51ghivrb6df8y8vr8034xfah36aiz")))

