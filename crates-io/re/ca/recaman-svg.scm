(define-module (crates-io re ca recaman-svg) #:use-module (crates-io))

(define-public crate-recaman-svg-1.0.0 (c (n "recaman-svg") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "svg") (r "^0.5.11") (d #t) (k 0)))) (h "0pcxy8cdv57ikzv46g9cvqjs2lsdc4s77spwc9sl8g7dcg6mipb9")))

