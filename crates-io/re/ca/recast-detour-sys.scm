(define-module (crates-io re ca recast-detour-sys) #:use-module (crates-io))

(define-public crate-recast-detour-sys-0.1.0 (c (n "recast-detour-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "insta") (r "^0.7.4") (d #t) (k 2)))) (h "1a0qszhzgprf9k24z7yvgpcrg57wlv7wsjx4viisiz1zwgsdssm0") (f (quote (("skip-build-recast") ("default"))))))

(define-public crate-recast-detour-sys-0.1.1 (c (n "recast-detour-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "insta") (r "^0.7.4") (d #t) (k 2)))) (h "1v05r8wmfd0s4va1ca4gzhlvqv0dv6i3xcabb4x8v675383lpjnr") (f (quote (("skip-build-recast") ("default"))))))

