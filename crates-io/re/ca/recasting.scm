(define-module (crates-io re ca recasting) #:use-module (crates-io))

(define-public crate-recasting-0.1.0 (c (n "recasting") (v "0.1.0") (h "0xvmv8hd2hlncyck5k8ph4hp870mi3b4cjfj0cj9zjib721cz62p") (f (quote (("std") ("default"))))))

(define-public crate-recasting-0.2.0 (c (n "recasting") (v "0.2.0") (d (list (d (n "triple_arena") (r "^0.11") (d #t) (k 2)))) (h "1hc1nckvzr3cbi61hv8jlxd743vl82ziflcv8qjyx1slkg7zhqci") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-recasting-0.2.1 (c (n "recasting") (v "0.2.1") (d (list (d (n "triple_arena") (r "^0.11") (d #t) (k 2)))) (h "0lf6gifrn6smwzp7i5rx9794h8i6xqymaj71g5ygwawj0wdfxbkh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

