(define-module (crates-io re ca recap-derive) #:use-module (crates-io))

(define-public crate-recap-derive-0.1.0 (c (n "recap-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02w80lbmar09f13nvq4g7ibr6ls7qb7mj9dhw9fbhhw7f4vmnnha")))

(define-public crate-recap-derive-0.1.1 (c (n "recap-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1p2k8lwlaphlx18mn80fbm5vb4nhzcv21fpkf34nhzgnk0dm0q9j")))

(define-public crate-recap-derive-0.1.2 (c (n "recap-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wr32l0bjwz5z2jrv6i2d5bnwa8n4h02kkkfb73rilr9l87yfyc5")))

