(define-module (crates-io re ca recap) #:use-module (crates-io))

(define-public crate-recap-0.1.0 (c (n "recap") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "recap-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "187s17sck6nnl6ix4kv880d6qvifd5zwfhp68pplrmvjc73bzdyp") (f (quote (("derive" "recap-derive") ("default" "derive"))))))

(define-public crate-recap-0.1.1 (c (n "recap") (v "0.1.1") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "recap-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bzg0ap8rcvx1j388d6cna23v94qnzhcajas6xlji65rmlv9ka6v") (f (quote (("derive" "recap-derive") ("default" "derive"))))))

(define-public crate-recap-0.1.2 (c (n "recap") (v "0.1.2") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "recap-derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17ircjsq1g9mhzcb5r42n3xn0mk3k78lbqcpw68gcx4m4z3zk10v") (f (quote (("derive" "recap-derive") ("default" "derive"))))))

