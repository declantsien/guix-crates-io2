(define-module (crates-io re ca recaptcha) #:use-module (crates-io))

(define-public crate-recaptcha-0.0.1 (c (n "recaptcha") (v "0.0.1") (d (list (d (n "hyper") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "18795ypd4cs9nqmdl6gmlaf7ss47qslhyr4y08iaqgapvldrbbx0")))

(define-public crate-recaptcha-0.0.2 (c (n "recaptcha") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1nwbvrphrkl2zq8qylpfyg64m5qh2x19r0vdp8z4rybk4i6lczrj")))

(define-public crate-recaptcha-0.0.3 (c (n "recaptcha") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0vgyy3bwz59935l1ql3ls3yljcvzm1nmsvhf4xb7ydd5vxcx5zy4")))

(define-public crate-recaptcha-0.0.4 (c (n "recaptcha") (v "0.0.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j52jdk1psihiqvml1049p0mqyg2xpy6jj7lm1mf9vbyk5psa70j")))

(define-public crate-recaptcha-0.0.5 (c (n "recaptcha") (v "0.0.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0g8yddr9ll8dprl1yzvsnv6f6d64v4mncqimhhvsbr9pc6czs8f2")))

(define-public crate-recaptcha-0.0.6 (c (n "recaptcha") (v "0.0.6") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1y6icc6frxdc6cdn4qkfxnayxf9jfmz07ifipiqv4x6jq334vbdd")))

(define-public crate-recaptcha-0.0.7 (c (n "recaptcha") (v "0.0.7") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1flw70mlrzhrcnhlc3r51ydlwzi6y1j1ljxwkgg29iaxdfaxbrjx")))

(define-public crate-recaptcha-0.1.0 (c (n "recaptcha") (v "0.1.0") (d (list (d (n "hyper") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0l649mxhbx9m1zijfr8b7g3r74cyw7yy35p80fqsrjhxy4rcsff2")))

(define-public crate-recaptcha-0.2.0 (c (n "recaptcha") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 2)))) (h "09kw6ayv738chkbh176yhdidy43cbv9l4m2mj7b4sqzzr7hij6ww")))

(define-public crate-recaptcha-0.3.1 (c (n "recaptcha") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 2)))) (h "1fm740z6yk0bd7nkp4kn7572a1sc80ny798n0fd0w446rrhb0hda")))

(define-public crate-recaptcha-0.4.0 (c (n "recaptcha") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)) (d (n "tokio") (r "^0.2.10") (f (quote ("macros"))) (d #t) (k 2)))) (h "1pr979fi4r0wl30ikq9inyy3cw2vqj63hnibb0b2vgdavd4gbys6")))

(define-public crate-recaptcha-0.4.1 (c (n "recaptcha") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)) (d (n "tokio") (r "^0.2.15") (f (quote ("macros"))) (d #t) (k 2)))) (h "03rw329iaahg4w2lc5fbahi1xj7fp9fv04ann8rznwjy0k2893xq")))

(define-public crate-recaptcha-0.5.0 (c (n "recaptcha") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)) (d (n "tokio") (r "^0.2.15") (f (quote ("macros"))) (d #t) (k 2)))) (h "0gh5nzrsyq6c98cl0w5p5az475sr8wzy20ix83kz9fakl2rwykmv")))

