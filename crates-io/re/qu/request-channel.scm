(define-module (crates-io re qu request-channel) #:use-module (crates-io))

(define-public crate-request-channel-0.1.0 (c (n "request-channel") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.25") (f (quote ("executor"))) (d #t) (k 2)))) (h "0iwv3bnymz50zjhja27am6azb7854hsq4vvnjchbpjsfdyych2mk")))

(define-public crate-request-channel-0.1.1 (c (n "request-channel") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.25") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.25") (f (quote ("executor"))) (d #t) (k 2)))) (h "0k5lp7x9gjp51pqfwxhy4ba3bvyjpil9qcsxm0ls8ynvafiv1i30")))

