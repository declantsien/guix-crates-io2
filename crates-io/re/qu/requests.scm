(define-module (crates-io re qu requests) #:use-module (crates-io))

(define-public crate-requests-0.0.0 (c (n "requests") (v "0.0.0") (h "0hs6ac22j06wl6bz38vrfj80pmiql2422j0dnfc4xas4b1d1yf6s")))

(define-public crate-requests-0.0.1 (c (n "requests") (v "0.0.1") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)))) (h "0h2ys12l4nnmfrkp47amvbcz3sd964kj4rp54bys4k30yrlf5myx")))

(define-public crate-requests-0.0.2 (c (n "requests") (v "0.0.2") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)))) (h "1c23p5f2fqnc9qkyrz4k23qw2kqi0i6sfwqn6xxzj5s8ws9pbwkm")))

(define-public crate-requests-0.0.3 (c (n "requests") (v "0.0.3") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)))) (h "1happ56f5via3pikjijb7nc63fmmldpwyplsxsdc99qc2d9v9vzi")))

(define-public crate-requests-0.0.4 (c (n "requests") (v "0.0.4") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)))) (h "155f5krrx1ayasl2qansjlmbp2cx2a6rv1gv2jz5fjgp6w5q81n4")))

(define-public crate-requests-0.0.5 (c (n "requests") (v "0.0.5") (d (list (d (n "hyper") (r "^0.8.0") (d #t) (k 0)))) (h "1kcg9m06jrm8gixkvbiapr2yr5659c6kq7vzdpm35y5bh0iak7kn")))

(define-public crate-requests-0.0.6 (c (n "requests") (v "0.0.6") (d (list (d (n "clippy") (r "^0.0.63") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (d #t) (k 0)))) (h "1giyzfilvj9js6jcs4p7xbk2g9gmxszfkb7148n01g6ixpd3glf7") (f (quote (("default"))))))

(define-public crate-requests-0.0.7 (c (n "requests") (v "0.0.7") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 0)))) (h "1g06a6syk1zrynf0qbmll1karswa3nhryym94pz0ba01wm9jyshv") (f (quote (("default"))))))

(define-public crate-requests-0.0.8 (c (n "requests") (v "0.0.8") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 0)))) (h "0i5bdk7qhs2b6n36qg210slirhv26vzybnryik3hga1d9iiqd4l7") (f (quote (("default"))))))

(define-public crate-requests-0.0.9 (c (n "requests") (v "0.0.9") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 0)))) (h "0pnvmad37dayqpac6vihiz459svv4gsp45rfz2vryw42ydp03zls") (f (quote (("default"))))))

(define-public crate-requests-0.0.10 (c (n "requests") (v "0.0.10") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^0.7.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.4") (d #t) (k 0)))) (h "1xg8h3g69fl5g4xp790didf202b6ac18waqx07mfn3aj6zc6i2jh") (f (quote (("default"))))))

(define-public crate-requests-0.0.11 (c (n "requests") (v "0.0.11") (d (list (d (n "clippy") (r "0.0.*") (d #t) (k 2)) (d (n "hyper") (r "^0.9.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.5") (d #t) (k 0)))) (h "1w1dznm81bnl1nkg3x9gpz3hhsc3v9skyacjam42hv72gk8bjl2a")))

(define-public crate-requests-0.0.12 (c (n "requests") (v "0.0.12") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.3") (d #t) (k 0)))) (h "1y9r9si5nxis90gwgazh9cichba7sz4wpk0f9n1jm0wcai5zrvxn") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.13 (c (n "requests") (v "0.0.13") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.4") (d #t) (k 0)))) (h "0i6a5gb5bdvpf3h1hmypk044i6568xm5pl2759jdgfrqfsxsismq") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.14 (c (n "requests") (v "0.0.14") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.5") (d #t) (k 0)))) (h "0gcxiv71mh86g0bsahcqlmr25ya0fq22045chcxiwkp59jy2a9a5") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.15 (c (n "requests") (v "0.0.15") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.6") (d #t) (k 0)))) (h "1kaw1ag0miqxykq589pl59qd3an9s55wb15f642rs19kwz0b5xcl") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.16 (c (n "requests") (v "0.0.16") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.7") (d #t) (k 0)))) (h "1b1cxgk42c024ci421k4dqykl6ncbywn7722jxvwibwl50x973bs") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.17 (c (n "requests") (v "0.0.17") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.7") (d #t) (k 0)))) (h "0b38zl0x4i0k9ym6pdxgmsc9b00iwc31nmbkc8pfbfxm4350gdvx") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.18 (c (n "requests") (v "0.0.18") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.8") (d #t) (k 0)))) (h "0ccl1vzsklq4rghamnxg0mj7bk52fyxhczyywbfp9b8y1d7vkq5b") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.19 (c (n "requests") (v "0.0.19") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.9") (d #t) (k 0)))) (h "020ivpvamc4snll8kdf53agzhk4acv6k85jhhrl7lx5i3vj2jqhm") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.20 (c (n "requests") (v "0.0.20") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.9") (d #t) (k 0)))) (h "1g7bb29kgfa09fqnbsb14szz479983bmdw989gx4z2gfswvdrh0m") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.21 (c (n "requests") (v "0.0.21") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.10") (d #t) (k 0)))) (h "0jprwzdkgjhhynaz1x5i0d3g4m22f5n61jhfcgygdwljfhi7y08f") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.22 (c (n "requests") (v "0.0.22") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.10") (d #t) (k 0)))) (h "06ih6a9g3vvyzk7xwl2rp8maxzs1vbg4sqqgppmxnxvcnyamv08b") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.23 (c (n "requests") (v "0.0.23") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0kbb5kqidlfcx53k1hc16izjlh2xv6m6b8ayjlk3m2s62jhxd6xl") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.24 (c (n "requests") (v "0.0.24") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)))) (h "0z3dlbrfn5k61ci5pfhh3dj7zcc2kj5xa0c1mpxz463qcryrkdgs") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.25 (c (n "requests") (v "0.0.25") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "mime") (r "^0.2.2") (d #t) (k 0)))) (h "1zjig6qpy607r4ds70j5i5hx8w8zz0p2fc3lk12826d73q0c5y4b") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-requests-0.0.26 (c (n "requests") (v "0.0.26") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.2.2") (d #t) (k 0)))) (h "1yy1xjg8y77h6pl9m9fnl21jq6yg1hyql9m4p9dz1q54aifvl0hc") (f (quote (("with_json" "json") ("ssl" "hyper-native-tls") ("default" "ssl" "with_json"))))))

(define-public crate-requests-0.0.27 (c (n "requests") (v "0.0.27") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.2.2") (d #t) (k 0)))) (h "0rzhk0bm09r59jqgrss5d8v8w5jwyv3ycyvij7rmiik6np3l7660") (f (quote (("with_json" "json") ("ssl" "hyper-native-tls") ("default" "ssl" "with_json"))))))

(define-public crate-requests-0.0.28 (c (n "requests") (v "0.0.28") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)))) (h "1fd9d12bw3a5sgs799bbz59l0vq0v15vjb4v29xnwwl161vws2s6") (f (quote (("with_json" "json") ("ssl" "hyper-native-tls") ("default" "ssl" "with_json"))))))

(define-public crate-requests-0.0.30 (c (n "requests") (v "0.0.30") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1wyvh3my2rrnwwap2v0fldvjdpsz4nhm6cvmyvcpqi7c515vwa9f") (f (quote (("with_json" "json") ("ssl" "hyper-native-tls") ("default" "ssl" "with_json"))))))

