(define-module (crates-io re qu requiem-rt) #:use-module (crates-io))

(define-public crate-requiem-rt-1.0.0 (c (n "requiem-rt") (v "1.0.0") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "requiem-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "requiem-threadpool") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("rt-core" "rt-util" "io-driver" "tcp" "uds" "udp" "time" "signal" "stream"))) (k 0)))) (h "00sqi82xp4xxvs6v515jgldviqyhkgy05ygsrni8dnp0k2iyx8jh")))

