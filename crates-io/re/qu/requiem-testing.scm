(define-module (crates-io re qu requiem-testing) #:use-module (crates-io))

(define-public crate-requiem-testing-1.0.0 (c (n "requiem-testing") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "requiem-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "requiem-server") (r "^1.0.0") (d #t) (k 0)) (d (n "requiem-service") (r "^1.0.0") (d #t) (k 0)))) (h "1awvl20if79dmflmk9xms53yn42qac1gs6376hgnpk27xvm9h0y8")))

