(define-module (crates-io re qu requiem-router) #:use-module (crates-io))

(define-public crate-requiem-router-0.2.4 (c (n "requiem-router") (v "0.2.4") (d (list (d (n "bytestring") (r "^0.1.2") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bajg1rpvb4slwh13jal6w6gcc5j4379q72jp6wzpw6wr6a58aq6") (f (quote (("default" "http"))))))

