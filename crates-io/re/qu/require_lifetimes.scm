(define-module (crates-io re qu require_lifetimes) #:use-module (crates-io))

(define-public crate-require_lifetimes-0.1.0 (c (n "require_lifetimes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.74") (d #t) (k 2)))) (h "1vz0wnhnz3h5ch2jpgnxpv2ilz5zx1qlmgradbwjykd892gmqal2")))

(define-public crate-require_lifetimes-0.2.0 (c (n "require_lifetimes") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.74") (d #t) (k 2)))) (h "1f88jryggm5rgyrvwgr4ppz9z84a1imkdjbags46js7p3mfznm0j")))

(define-public crate-require_lifetimes-0.3.0 (c (n "require_lifetimes") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.12") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.74") (d #t) (k 2)))) (h "1xrygzxzby5d50pbxavy469sivriqjl53nz38n78yzqgfqy2k333")))

