(define-module (crates-io re qu requiem-cors) #:use-module (crates-io))

(define-public crate-requiem-cors-0.2.0-r1 (c (n "requiem-cors") (v "0.2.0-r1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "requiem-service") (r "^1.0.1") (d #t) (k 0)) (d (n "requiem-web") (r "^2.0.0-r2") (d #t) (k 0)))) (h "06n8cf5lgwfj7489rrw20biszch51w8az4l4x8qsml8gzzhvllza")))

