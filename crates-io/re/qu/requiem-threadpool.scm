(define-module (crates-io re qu requiem-threadpool) #:use-module (crates-io))

(define-public crate-requiem-threadpool-0.3.1 (c (n "requiem-threadpool") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "04g7yv5rpprgprc53rdqk6p5r9g8ismakfj6jrbq3bi9m9smzb4j")))

