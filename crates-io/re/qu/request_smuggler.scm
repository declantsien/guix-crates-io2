(define-module (crates-io re qu request_smuggler) #:use-module (crates-io))

(define-public crate-request_smuggler-0.1.0-alpha.1 (c (n "request_smuggler") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0v55kb4fk0yny3l7331d8qb714fh40ry1qxji8q1dbasd14pqj2a")))

(define-public crate-request_smuggler-0.1.0-alpha.2 (c (n "request_smuggler") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "12p5xjqz3dj5w5fr6qq26k7fafc9m6j9qqmblzzaz01z9ma2zn9z")))

