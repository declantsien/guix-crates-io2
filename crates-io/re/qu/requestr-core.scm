(define-module (crates-io re qu requestr-core) #:use-module (crates-io))

(define-public crate-requestr-core-0.1.0 (c (n "requestr-core") (v "0.1.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1bwa0fhwvcn7y7m2cjjr97rrh3l0msjm9jcdh865sri63355ah2j")))

