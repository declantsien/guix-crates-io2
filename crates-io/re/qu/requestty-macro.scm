(define-module (crates-io re qu requestty-macro) #:use-module (crates-io))

(define-public crate-requestty-macro-0.1.0 (c (n "requestty-macro") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kzj8kw30l8g8xlhb8l69zp9955li24cqm14zgy1aiqvsfdhiqwm")))

(define-public crate-requestty-macro-0.1.1 (c (n "requestty-macro") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m2jz48bl2n52yb3nxc0kp2bwf9qarf1dw37nl34ixmq7kgv9pvb")))

(define-public crate-requestty-macro-0.2.1 (c (n "requestty-macro") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l72z2qsikjq08xjgw9n9qj7nr6rk8g1ykgcq1gj1j65d7j447vm")))

(define-public crate-requestty-macro-0.3.0 (c (n "requestty-macro") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15jq3wglcaf5rzas135545g0qsnhbadln9n6yzsns4iyf3nvyj4h")))

(define-public crate-requestty-macro-0.4.0 (c (n "requestty-macro") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04z8bxxj64d06wz3hzqviy806k60v3c1zwmb62nifhcwpnn9fd8v")))

(define-public crate-requestty-macro-0.4.1 (c (n "requestty-macro") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nd27ag2j48wx8dd4lc4pkd8vn2bzlygvf7lkxffishpqiw4za10")))

(define-public crate-requestty-macro-0.5.0 (c (n "requestty-macro") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vrl1a3dkgn4wa5m6wfnvh7sf3x6i0brwm8fwynwmhlj1y5jd1j7")))

