(define-module (crates-io re qu requestr-cli) #:use-module (crates-io))

(define-public crate-requestr-cli-0.1.0 (c (n "requestr-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "~0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "loggerv") (r "~0.7") (d #t) (k 0)) (d (n "requestr-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)))) (h "0daq2qdqsygjqrayq7v3wn2a2x98xj4qxnwgqag3iqv1f2752d49")))

