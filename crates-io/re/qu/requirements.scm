(define-module (crates-io re qu requirements) #:use-module (crates-io))

(define-public crate-requirements-0.1.0 (c (n "requirements") (v "0.1.0") (d (list (d (n "globwalk") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1mm8jx0z903vw0hwh54gijh95wm9jbiz1k11q8dswgcskr5n6x2y")))

(define-public crate-requirements-0.2.0 (c (n "requirements") (v "0.2.0") (d (list (d (n "globwalk") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1fn733qhdc5374v0kn8299v4b71i5j9y51i6iilirf9svwzvrqcd")))

(define-public crate-requirements-0.3.0 (c (n "requirements") (v "0.3.0") (d (list (d (n "globwalk") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1hwi0yzfi9bb3d8jh1sfs5hkb8qj9nbyy3gf231np3lxyw1yjhr6")))

