(define-module (crates-io re qu requeasy) #:use-module (crates-io))

(define-public crate-requeasy-0.1.0 (c (n "requeasy") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0fcfy6dqqlhrkg503hkhby9qp7nzb95ip9fbw847jglnsasx2l6m") (y #t)))

(define-public crate-requeasy-0.0.1 (c (n "requeasy") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0k4kayz6wwq0awj9zj6ss1vaa7a8h0plriy17qgakwxsx0k38kp1") (y #t)))

(define-public crate-requeasy-0.0.2 (c (n "requeasy") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0l8zy97dv9ng8nld0wc8s0jw653gas6l5nik05x5lq61w9angkcv") (y #t)))

(define-public crate-requeasy-0.0.3 (c (n "requeasy") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0z1li6mpmxkrk22h8jb09abp8i824q3fj1nh04vh294dsgbwl8sv") (y #t)))

(define-public crate-requeasy-0.0.4 (c (n "requeasy") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "08x8z1v46r7mb72r3r8dhk1cfr2bk7v38akdbap3yd23pxnyc3si") (y #t)))

(define-public crate-requeasy-0.0.5 (c (n "requeasy") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "1m955w0l56plvh5nd2n697a21rsrqdckkrn183wc33mzl8zlv77s") (y #t)))

(define-public crate-requeasy-0.0.6 (c (n "requeasy") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "1rfai7hjd5w9vhrnywhczj895h0yb5qxgggk56x59bz3mfw72yka") (y #t)))

(define-public crate-requeasy-0.0.7 (c (n "requeasy") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (d #t) (k 0)) (d (n "webpki") (r "^0.22.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0g5kk51736d569clvxifybyhld8xzjxwf81jjq0nif3yp2wy1ma7")))

