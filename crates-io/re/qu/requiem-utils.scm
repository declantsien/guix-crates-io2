(define-module (crates-io re qu requiem-utils) #:use-module (crates-io))

(define-public crate-requiem-utils-1.0.6 (c (n "requiem-utils") (v "1.0.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "requiem-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "requiem-service") (r "^1.0.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1rfl18ilbslgz1356lrrd4bmxz5ib5hdms6s29a0cmdzvwzs5b34")))

