(define-module (crates-io re qu request-id) #:use-module (crates-io))

(define-public crate-request-id-0.1.0 (c (n "request-id") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("request-id"))) (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (o #t) (d #t) (k 0)))) (h "0j343b4m02dqsmjk9szaqinh91v3rkq9xjpdv008qascw1lnmghy") (f (quote (("uuid" "uuid/v4")))) (s 2) (e (quote (("ulid" "dep:ulid"))))))

(define-public crate-request-id-0.2.0 (c (n "request-id") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tower-http") (r "^0.4.0") (f (quote ("request-id" "util"))) (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (o #t) (d #t) (k 0)))) (h "1xch23syz84yspjyz77s7ym7mhnwkdlnfl25praj8kb9pj0661kh") (f (quote (("uuid" "uuid/v4")))) (s 2) (e (quote (("ulid" "dep:ulid"))))))

(define-public crate-request-id-0.3.0 (c (n "request-id") (v "0.3.0") (d (list (d (n "hyper") (r "^1.0") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.0") (f (quote ("request-id" "util"))) (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (o #t) (d #t) (k 0)))) (h "1a9zxl1arkd83bvp068dy9vcf3p9mdp7c6kyngslklhy7k7pk0cw") (f (quote (("uuid" "uuid/v4")))) (s 2) (e (quote (("ulid" "dep:ulid"))))))

