(define-module (crates-io re qu requiem-tracing) #:use-module (crates-io))

(define-public crate-requiem-tracing-0.1.0 (c (n "requiem-tracing") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0") (d #t) (k 2)) (d (n "requiem-service") (r "^1.0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)))) (h "1drilap0lq634508lsa14aq6jhiaqwy7kc0n7ilbpvqv2zp9x7kr")))

