(define-module (crates-io re qu requiem-web-codegen) #:use-module (crates-io))

(define-public crate-requiem-web-codegen-0.2.0 (c (n "requiem-web-codegen") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "149z7q0cvvvh55nr9hphask58ir297ws5jzs6rsycd1sabw348a9")))

(define-public crate-requiem-web-codegen-0.2.0-r1 (c (n "requiem-web-codegen") (v "0.2.0-r1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "requiem-web") (r "^2.0.0-r2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0acpbhkca8nc89inzl1jz7rg575fpq0rxb16h028lx289irbxda2")))

