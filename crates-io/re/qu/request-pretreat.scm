(define-module (crates-io re qu request-pretreat) #:use-module (crates-io))

(define-public crate-request-pretreat-0.1.0 (c (n "request-pretreat") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "resp-result") (r "^0.1.9") (f (quote ("for-actix"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1282syfghynfw6myjx03wwazkla3cvsv5dsx2x1dnsd38jkb89mc")))

(define-public crate-request-pretreat-0.1.1 (c (n "request-pretreat") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "resp-result") (r "^0.1.9") (f (quote ("for-actix"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0jaznfzb39xi8x6arp6hn86ig2nnd4q1f3dv6raq71y885xrl8fv")))

