(define-module (crates-io re qu requiem-web-actors) #:use-module (crates-io))

(define-public crate-requiem-web-actors-2.0.0-r1 (c (n "requiem-web-actors") (v "2.0.0-r1") (d (list (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "requiem") (r "^0.9.0") (d #t) (k 0)) (d (n "requiem-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "requiem-http") (r "^1.0.1-r1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "requiem-web") (r "^2.0.0-r2") (d #t) (k 0)))) (h "0vxmihmdnbl657b32ibgvzhr6q8dg36gmm47p86a6ifx8f6vyzfs")))

