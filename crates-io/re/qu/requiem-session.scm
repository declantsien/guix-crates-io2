(define-module (crates-io re qu requiem-session) #:use-module (crates-io))

(define-public crate-requiem-session-0.3.0-r1 (c (n "requiem-session") (v "0.3.0-r1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "requiem-service") (r "^1.0.1") (d #t) (k 0)) (d (n "requiem-web") (r "^2.0.0-r2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "134didm1k99jbviqcwvhah56bk1r5xld7xwpk2m8jr982kwpzmlh") (f (quote (("default" "cookie-session") ("cookie-session" "requiem-web/secure-cookies"))))))

