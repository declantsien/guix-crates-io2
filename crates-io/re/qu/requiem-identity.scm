(define-module (crates-io re qu requiem-identity) #:use-module (crates-io))

(define-public crate-requiem-identity-0.2.1-r1 (c (n "requiem-identity") (v "0.2.1-r1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "requiem-http") (r "^1.0.1-r1") (d #t) (k 2)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "requiem-service") (r "^1.0.2") (d #t) (k 0)) (d (n "requiem-web") (r "^2.0.0-r2") (f (quote ("secure-cookies"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "05jcm4xlwcis2yrb3wyzp5yp8ifnc66pc52vi1rgza44m0c7h2ri")))

