(define-module (crates-io re qu requiem-service) #:use-module (crates-io))

(define-public crate-requiem-service-1.0.5 (c (n "requiem-service") (v "1.0.5") (d (list (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "requiem-rt") (r "^1.0.0") (d #t) (k 2)))) (h "1pb30niwsmyayhazrkdpyfd875js3hsdlkm756p60hbphp66ccdd")))

