(define-module (crates-io re qu requiem-codec) #:use-module (crates-io))

(define-public crate-requiem-codec-0.2.0 (c (n "requiem-codec") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (k 0)) (d (n "tokio-util") (r "^0.2.0") (f (quote ("codec"))) (k 0)))) (h "0kklhzwsvqv4l7h94wkwbfccj32nkwk2i9i6k3p4q14b6frjhrs3")))

