(define-module (crates-io re qu request) #:use-module (crates-io))

(define-public crate-request-0.0.1 (c (n "request") (v "0.0.1") (d (list (d (n "openssl") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^0.2.29") (d #t) (k 0)))) (h "1d01zfknkp4xp3yqrgwf3cs3890n6pkpbqyqqmv7v61vx58y8vb2")))

(define-public crate-request-0.0.2 (c (n "request") (v "0.0.2") (d (list (d (n "openssl") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^0.2.29") (d #t) (k 0)))) (h "1gdypykk2hvniirigjpaz11gp9ksl8a19yc09yazblw0zdv3qz2b")))

(define-public crate-request-0.0.3 (c (n "request") (v "0.0.3") (d (list (d (n "openssl") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^0.2.29") (d #t) (k 0)))) (h "1cvq2s363nqd4lfd3ana5s7mw6rr1m575ylxxagvj6ng8d7zk8bm")))

(define-public crate-request-0.0.4 (c (n "request") (v "0.0.4") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "url") (r "^0.2.30") (d #t) (k 0)))) (h "05y2lmkzfz2bx667pj2wpkr1aa6z20l4yl6xc3wvji8l27726hxr")))

(define-public crate-request-0.0.5 (c (n "request") (v "0.0.5") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "url") (r "^0.2.31") (d #t) (k 0)))) (h "03xll9lm8p7hhb6703h4nvcjj7s6qiim38n9nlp52fmkkhn74iy9")))

(define-public crate-request-0.0.6 (c (n "request") (v "0.0.6") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "url") (r "^0.2.31") (d #t) (k 0)))) (h "1ykrcrvdc0gybs3vn7pc3fyvbsdpfbfaa1yvscbl0snqp0hfvawp")))

(define-public crate-request-0.0.7 (c (n "request") (v "0.0.7") (d (list (d (n "openssl") (r "^0.6.2") (d #t) (k 0)) (d (n "url") (r "^0.2.31") (d #t) (k 0)))) (h "1fqadi65yp2x17c8ddb9gv117ma8j631kxwjf4nbyw6932524vz6")))

(define-public crate-request-0.0.8 (c (n "request") (v "0.0.8") (d (list (d (n "openssl") (r "^0.6.2") (d #t) (k 0)) (d (n "url") (r "^0.2.34") (d #t) (k 0)))) (h "1zqwc5asz9xqp2b5swn2c2ypy1icr7fnqhr69imwg833k00f5dgd")))

