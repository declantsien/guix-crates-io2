(define-module (crates-io re as reason-othello) #:use-module (crates-io))

(define-public crate-reason-othello-0.1.0 (c (n "reason-othello") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "0i58p2l21cngj33vk9hblz1s72kyizcb53bdy0313hsv1jgcmscj")))

