(define-module (crates-io re fb refbox) #:use-module (crates-io))

(define-public crate-refbox-0.1.0 (c (n "refbox") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1a8k7cfs42yj17fbpfxpa10p4knh9lf2qmr9yz9z659y8bl7hxac")))

(define-public crate-refbox-0.2.0 (c (n "refbox") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15k2m3s42yb74apy4azyq25n6dv95w443n0249g6zb77p834gkps")))

(define-public crate-refbox-0.3.0 (c (n "refbox") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "038pwm95nnif5r1rprihm29q7n1bmvd8cclybk6vdgn2crf1620z") (f (quote (("cyclic"))))))

