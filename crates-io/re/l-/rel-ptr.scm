(define-module (crates-io re l- rel-ptr) #:use-module (crates-io))

(define-public crate-rel-ptr-0.1.0 (c (n "rel-ptr") (v "0.1.0") (h "1v2nxkzfpndj23b69mblw71aw05i0f2yw6q9j8avii2a43g06vrp") (f (quote (("std") ("default" "std"))))))

(define-public crate-rel-ptr-0.1.1 (c (n "rel-ptr") (v "0.1.1") (h "1vfni7wibaf7jiz56yz1kll9ziz2lv2mma3vfb8da972rpqwhd4d") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1.2 (c (n "rel-ptr") (v "0.1.2") (d (list (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "07fdf95csnsnfh6zspxhjclkib2kzm4lm8p0d61pik56zzjl7hnl") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1.3 (c (n "rel-ptr") (v "0.1.3") (d (list (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "182ncznv33vvmq9001myp3g3z4ah0kkmnjxc08blldkfw54l6sri") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1.4 (c (n "rel-ptr") (v "0.1.4") (d (list (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "1fz48s2q979hp59k9xq6hha35w9w4vy6dan9cvcsczxrgf57mi8l") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2.0 (c (n "rel-ptr") (v "0.2.0") (d (list (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "1lw05dxza9dhvvz1kia4mnihf2x6miw1c3a58d8r1dx28g1fmg0y") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2.1 (c (n "rel-ptr") (v "0.2.1") (d (list (d (n "unreachable") (r "^1") (d #t) (k 0)))) (h "0gjw7w5ns2v6hhiwx0gjx722f43bnkpvz17y4m2fhrnkl6p4a2jg") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2.2 (c (n "rel-ptr") (v "0.2.2") (h "03wljdml7zfd08g7pv6xxk869yzv4sb0w7lziayplaq01pic50h5") (f (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2.3 (c (n "rel-ptr") (v "0.2.3") (h "1wipiji77jr902ykhpiqy2dy2raf4nrszgsnj9mklnfsawznxax5") (f (quote (("no_std") ("nightly") ("default"))))))

