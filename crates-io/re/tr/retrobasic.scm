(define-module (crates-io re tr retrobasic) #:use-module (crates-io))

(define-public crate-retrobasic-0.1.0 (c (n "retrobasic") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dimsum") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1midnmkcamlbzml68q7hb8cd7di3jp8b5ic15b0g3rms69gp63hj")))

