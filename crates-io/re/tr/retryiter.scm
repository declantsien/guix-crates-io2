(define-module (crates-io re tr retryiter) #:use-module (crates-io))

(define-public crate-retryiter-0.1.0 (c (n "retryiter") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f2i15dwz0scivj9b0rpi27vvjy18xm1jy19nfqp0kd754x39sx0")))

(define-public crate-retryiter-0.2.0 (c (n "retryiter") (v "0.2.0") (h "0mqpwgqw8m8bd6065p5jja129f6mz8kncw22ac83dabg3l03b72s")))

(define-public crate-retryiter-0.3.0 (c (n "retryiter") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zr6fj0gi7dpiyah8rwrrzlgq4ya8s8jddlkzlhnmzwsclxym6mp")))

(define-public crate-retryiter-0.4.0 (c (n "retryiter") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1lb0xx23vbvfycf5xqhv0qw7fmab8w51hdnmp2v5gln3d946yijx")))

(define-public crate-retryiter-0.4.1 (c (n "retryiter") (v "0.4.1") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1lwfwk9kgnpysdn8lbm1xwsbfj2x8z6hshkkbaxp9k7dc4yan1fw")))

(define-public crate-retryiter-0.4.2 (c (n "retryiter") (v "0.4.2") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "13ar786jny7r5jyjab9dn1l49na84qbvzf44jbshxli3hnwmcb8f")))

(define-public crate-retryiter-0.4.3 (c (n "retryiter") (v "0.4.3") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s88vsqk62qidnl586v17b6xma5sar22ca0b1ivqyqb3jfc7cffz")))

(define-public crate-retryiter-0.4.4 (c (n "retryiter") (v "0.4.4") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "197znr8w0rkj492bwaf8r4qfrrmjfv047gc3nixd9szvzbhvawlm")))

