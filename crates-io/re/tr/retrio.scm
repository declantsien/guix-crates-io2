(define-module (crates-io re tr retrio) #:use-module (crates-io))

(define-public crate-retrio-0.1.0 (c (n "retrio") (v "0.1.0") (d (list (d (n "partial-io") (r "^0.3.0") (d #t) (k 2)))) (h "1af29m3vldqmy612mxxjsh2gkzasg30268fajdw3yvfkwgaf1fq2")))

(define-public crate-retrio-0.1.1 (c (n "retrio") (v "0.1.1") (d (list (d (n "partial-io") (r "^0.3.0") (d #t) (k 2)))) (h "10vnx1midga7388vhha3f7n7kicr6c1rh62bfdsq3ahbpi33bagx")))

(define-public crate-retrio-0.2.0 (c (n "retrio") (v "0.2.0") (d (list (d (n "partial-io") (r "^0.3.0") (d #t) (k 2)))) (h "1ia87c6xi5sicxh95cs7dvmzb77d3fwn5yrjdajy8ipqz2gs131q")))

