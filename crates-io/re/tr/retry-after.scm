(define-module (crates-io re tr retry-after) #:use-module (crates-io))

(define-public crate-retry-after-0.1.0 (c (n "retry-after") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "httparse") (r "^1.1") (d #t) (k 2)) (d (n "hyper") (r "^0.7") (k 0)))) (h "1f28clvqmd24jy73sfl9nvg7kg6a5fqvx1i7dlsw4d2pqwn6s3hy")))

(define-public crate-retry-after-0.2.0 (c (n "retry-after") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "httparse") (r "^1.1") (d #t) (k 2)) (d (n "hyper") (r "^0.7") (d #t) (k 0)))) (h "1ii4dvzjfnci501mfdb7nrafyyw6c9wb79q3q2y7ad1jm76g6374")))

(define-public crate-retry-after-0.3.0 (c (n "retry-after") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f3fdp5cp1zdgqq0vx6yn7rzxysbcmn2zcv5525imh5h8cscx90f")))

(define-public crate-retry-after-0.3.1 (c (n "retry-after") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ahccb03qcyy8z38yvvhb4s671xndqqphf5mfi4f02drjgf5b38j")))

(define-public crate-retry-after-0.4.0 (c (n "retry-after") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m5fnylp7wbdbs2dyn6iabhr8icmkmk788f9dd0rzf698pza78ph")))

