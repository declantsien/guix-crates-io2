(define-module (crates-io re tr retryable) #:use-module (crates-io))

(define-public crate-retryable-0.1.0 (c (n "retryable") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "proc_macros") (r "^0.1.0") (d #t) (k 0) (p "retryable-proc_macros")))) (h "02n0lcbbv5fg3r5ylijd0nl54w9m4g3libgd7qhk54rb78daylvs") (y #t)))

(define-public crate-retryable-0.1.1 (c (n "retryable") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "proc_macros") (r "^0.1.1") (d #t) (k 0) (p "retryable-proc_macros")))) (h "04hyahs1sppxw2k14xaaqjxspb7d3p7hm6592r1xp62bqf5mhrjx")))

