(define-module (crates-io re tr retro-pixel) #:use-module (crates-io))

(define-public crate-retro-pixel-0.1.0 (c (n "retro-pixel") (v "0.1.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.15") (d #t) (k 2)))) (h "0j6xjfzr2g3p13nmhgdlvwjkyg33585rir1kq9s898bpw8dwjs5b")))

(define-public crate-retro-pixel-0.2.0 (c (n "retro-pixel") (v "0.2.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.15") (d #t) (k 2)))) (h "0rpp0c2h37wmwks65irpkxbh819z6drdnacahlfrnyc74d86493r")))

(define-public crate-retro-pixel-0.2.1 (c (n "retro-pixel") (v "0.2.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.15") (d #t) (k 2)))) (h "1vqzskmx5pggrblir6xnrpbhd9664b5sa7fh2a4w5f0liv1siy89")))

(define-public crate-retro-pixel-0.3.0 (c (n "retro-pixel") (v "0.3.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.15") (d #t) (k 2)))) (h "1s9mpld8jpp999jryg2j2h978bjyrplrnb0dgqvqfk3k6sfnm5q9") (f (quote (("std") ("default" "std"))))))

(define-public crate-retro-pixel-0.3.1 (c (n "retro-pixel") (v "0.3.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.16") (d #t) (k 2)))) (h "0r6lx87455ny13hp720dr7n2kwzx7n2swq3as0ag2bll19h69vkx") (f (quote (("std") ("default" "std"))))))

(define-public crate-retro-pixel-0.3.2 (c (n "retro-pixel") (v "0.3.2") (d (list (d (n "gl") (r "^0.10") (d #t) (k 2)) (d (n "glutin") (r "^0.16") (d #t) (k 2)))) (h "17cx2s4axa52dr5k3lm337l5rmqvpnf1imnvj4lc442xnc21yc2c") (f (quote (("std") ("default" "std"))))))

