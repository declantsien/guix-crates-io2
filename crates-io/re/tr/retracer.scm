(define-module (crates-io re tr retracer) #:use-module (crates-io))

(define-public crate-retracer-0.0.0-TestHistoryPublishing+0 (c (n "retracer") (v "0.0.0-TestHistoryPublishing+0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("serde" "v4" "v7"))) (d #t) (k 0)))) (h "0naq5q7r8i56ziz9mai1knknk6d1938xc39dypdca8k628rpp6bb") (f (quote (("sync") ("default")))) (y #t)))

