(define-module (crates-io re tr retry-policies) #:use-module (crates-io))

(define-public crate-retry-policies-0.1.0 (c (n "retry-policies") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mp5jlv730v1qnzwc5lg22hfnx91krxz59y31sqmjv9mgwbsm8w0")))

(define-public crate-retry-policies-0.1.1 (c (n "retry-policies") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "17azs2zwskv2wf4na731pq9virsyxalsibkhrjbdgkf632dy3ya7")))

(define-public crate-retry-policies-0.1.2 (c (n "retry-policies") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "16sx0y7ca1lq5i963xbqyglcgsbqn90ygfphi2vbr0ij02svr6z0")))

(define-public crate-retry-policies-0.2.0 (c (n "retry-policies") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "11rnra4j8v8shkd0krkbkxydzivrkkkpckd9m62k1sla1r6xq5d7")))

(define-public crate-retry-policies-0.2.1 (c (n "retry-policies") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ggc4g5aqyzwhnhz1vpr6dww3x0v51sl6zflph6w8dypy6zh1p8p")))

(define-public crate-retry-policies-0.3.0 (c (n "retry-policies") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "fake") (r "^2.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "042qs8887sdfvjbyqpmm4fhl6sawwfb8hfcskz9fsvidwd1l4fs9")))

(define-public crate-retry-policies-0.4.0 (c (n "retry-policies") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "070alim5nhp3bvf58cyc729kq4dmgmr8rjqf2ny72a5bdhg4fxaq")))

