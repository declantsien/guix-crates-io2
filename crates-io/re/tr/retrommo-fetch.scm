(define-module (crates-io re tr retrommo-fetch) #:use-module (crates-io))

(define-public crate-retrommo-fetch-0.1.0 (c (n "retrommo-fetch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 0)))) (h "09wwj4w41z47dsw0nbvixsaimhnv2zxgyz57jfsxz6gq0fvajar7")))

(define-public crate-retrommo-fetch-0.1.1 (c (n "retrommo-fetch") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 2)))) (h "0iwa9jjg92pdbyn8375kaad83maxdb89f279wj28ijbqmqwmxdjs")))

(define-public crate-retrommo-fetch-0.2.0 (c (n "retrommo-fetch") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "clock"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v1x4x9f7axd7vb9jf4p2gx9psyb3jbs413bn50c56c3bwh6axcb")))

