(define-module (crates-io re tr retrace) #:use-module (crates-io))

(define-public crate-retrace-0.1.0 (c (n "retrace") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1mhj08a8kq3ar0677ngmkpggc24s6n999dxjr2d1p5qxia9w04bp") (f (quote (("std") ("default" "std") ("bench"))))))

