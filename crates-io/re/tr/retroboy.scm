(define-module (crates-io re tr retroboy) #:use-module (crates-io))

(define-public crate-retroboy-0.1.0 (c (n "retroboy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "retroboy_chip8") (r "^0.1.0") (d #t) (k 0)) (d (n "retroboy_sdl2") (r "^0.1.0") (d #t) (k 0)))) (h "1pjqxrn7xci32kngfjvawcym4csc0qafyidm9zj3nrxy1jbqj5sl")))

