(define-module (crates-io re tr retrogress) #:use-module (crates-io))

(define-public crate-retrogress-0.1.0 (c (n "retrogress") (v "0.1.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)))) (h "0ry7sbzxg4krivxxprb3dj8np00jwl98alzdpp8pgwhhnarhjcbj") (f (quote (("strict") ("default" "strict"))))))

(define-public crate-retrogress-0.2.0 (c (n "retrogress") (v "0.2.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)))) (h "1n32s1h59zkankp0b48960b148qc3bzfxjmzccvj0fprikfrxhfj") (f (quote (("strict") ("default" "strict"))))))

(define-public crate-retrogress-0.2.1 (c (n "retrogress") (v "0.2.1") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)))) (h "14lwlqw8gcyrfmc4njh6cx89nzcv868gpmzaq3j8xrqq3ilji2dg") (f (quote (("strict") ("default" "strict"))))))

