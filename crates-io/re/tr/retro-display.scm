(define-module (crates-io re tr retro-display) #:use-module (crates-io))

(define-public crate-retro-display-0.1.0 (c (n "retro-display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.4") (d #t) (k 0)) (d (n "tinytga") (r "^0.5.0") (d #t) (k 2)))) (h "1k2acgq9m704p0g2afi8mk4xxm8jv550dyim9w4g59jl6cr7vvx7") (f (quote (("docs-rs") ("default" "c64") ("c64"))))))

