(define-module (crates-io re tr retry_durations) #:use-module (crates-io))

(define-public crate-retry_durations-1.0.0 (c (n "retry_durations") (v "1.0.0") (d (list (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "fastrand") (r "^2") (d #t) (k 0)))) (h "0i3ixrq5glz3jhb9lkbclq9aldx0snra8bva1xir25xcsxvlzq2d")))

