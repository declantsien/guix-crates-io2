(define-module (crates-io re tr retry-predicate) #:use-module (crates-io))

(define-public crate-retry-predicate-0.0.0 (c (n "retry-predicate") (v "0.0.0") (h "03zjb5ajgjc9mlgh3p2km8wcj2gnlx8kwqwcgvp7djxyfliyp0ls") (y #t)))

(define-public crate-retry-predicate-0.1.0 (c (n "retry-predicate") (v "0.1.0") (h "17c0ish7brjh4lpcj9n24ylqras7a3rj2ifxcbhsa7hmm6v02kny") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

