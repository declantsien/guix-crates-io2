(define-module (crates-io re tr retry-cli) #:use-module (crates-io))

(define-public crate-retry-cli-0.0.1 (c (n "retry-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "retry") (r "^1.0.0") (d #t) (k 0)))) (h "0qcdz6aayamav0fhd78m5j4w0rqmm97saqfz8fa877xmy9dxd4vv")))

(define-public crate-retry-cli-0.0.2 (c (n "retry-cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "retry") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07izbrnypr7a52hvi8sc0j984d4kxnhlzx3lpz2lf3n8q4361b2h")))

