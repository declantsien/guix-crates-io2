(define-module (crates-io re tr retrying) #:use-module (crates-io))

(define-public crate-retrying-0.1.0 (c (n "retrying") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "retrying-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17yswrg70fqqrflmsfgn5pv2mg9yn9066nl9xxxj7wvdh6wy1km0") (s 2) (e (quote (("tokio" "dep:tokio") ("async_std" "dep:async-std")))) (r "1.64")))

