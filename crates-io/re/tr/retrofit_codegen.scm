(define-module (crates-io re tr retrofit_codegen) #:use-module (crates-io))

(define-public crate-retrofit_codegen-0.1.0 (c (n "retrofit_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0zd24zizj0qs6mcwfkgy8m8aki0fh21gbvxv2k1qlc3czwadd8md")))

(define-public crate-retrofit_codegen-0.1.1 (c (n "retrofit_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "07yij2y23v3i4byiv7sn10n4qn2b7f6kkpm2r8cbmdriyix59hy9") (f (quote (("server") ("default" "server" "client") ("client"))))))

(define-public crate-retrofit_codegen-0.1.2 (c (n "retrofit_codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "049m70gnzl2yz2vv6bss7y5p025g8x4h5g3aiw6ni23d56cgbi27") (f (quote (("server") ("default" "server" "client") ("client"))))))

(define-public crate-retrofit_codegen-0.1.3 (c (n "retrofit_codegen") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "19fxqqps3i1chmyd302lmgh1v0k4l8cylc4xq9qrdwdxmbp5pxcf") (f (quote (("server") ("default" "server" "client") ("client"))))))

(define-public crate-retrofit_codegen-0.1.4 (c (n "retrofit_codegen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0al0g4ys5r5lcw59026rfb3ib79d8p6mr88nbcmpd94g8lwd3qhy") (f (quote (("server") ("default" "server" "client") ("client"))))))

(define-public crate-retrofit_codegen-0.1.5 (c (n "retrofit_codegen") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0srjmcl87nzmih54dhffwrd9s2kz9vnfyqmcsd54d3xmmc89a5bz") (f (quote (("server") ("default" "server" "client") ("client"))))))

(define-public crate-retrofit_codegen-0.1.6 (c (n "retrofit_codegen") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "07pw6ymr5jc0dzyiy0sdslbm9jn5bv01bcpd7j8vb88v56x5kzjx")))

(define-public crate-retrofit_codegen-0.1.7 (c (n "retrofit_codegen") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1y0r235pjmsdw4rr1w63hjfzixy3i3gld6kqcnf5sbgi6pr18hm5")))

(define-public crate-retrofit_codegen-0.1.8 (c (n "retrofit_codegen") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1l49wbjg24ynyrslx9kdw52q01r0h1a60w7jfxrbsfmlnhxqfgxn")))

(define-public crate-retrofit_codegen-0.1.9 (c (n "retrofit_codegen") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1i01id0r8dhd2j7h4v263vjc9apqzbvf1swn3rlpvc1438iry4nx")))

(define-public crate-retrofit_codegen-0.1.10 (c (n "retrofit_codegen") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0wjny69fp6gnh66r3687cx9bgj1jm7fxdlx2c0fqiflj8jw6lkhq")))

(define-public crate-retrofit_codegen-0.2.0 (c (n "retrofit_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1q76jvg7nc4khfvi2zwzxcx6yf2ac1a5sg78l5x7ml1flgf0lsbb")))

(define-public crate-retrofit_codegen-0.2.1 (c (n "retrofit_codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "187nglim27c2nr2h50d1gmxgy3zazmfl400ihv2b7qln88zn03iv")))

(define-public crate-retrofit_codegen-0.2.2 (c (n "retrofit_codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1riwpq645qgc4rcqq7nx43i5ll6ifcw2q3l3pkmbglfnj3n7hdqd")))

(define-public crate-retrofit_codegen-0.2.3 (c (n "retrofit_codegen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0zgd5n5kkfiaa0x5si2dbn6bfwjv83ds4b88a9j1vzzm1s0pknxk")))

(define-public crate-retrofit_codegen-0.2.4 (c (n "retrofit_codegen") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1cm730zpv0qb0b7siiqs1i0yymzg5xhbj4aw554lhkb254kwy2fl")))

(define-public crate-retrofit_codegen-0.2.5 (c (n "retrofit_codegen") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnwa62a3mj43hh3myc9fi3z5xyxa4611cnkj2hgny4pzi7wzzri")))

(define-public crate-retrofit_codegen-0.2.6 (c (n "retrofit_codegen") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1am60wxd1z6pjppqa21m3s4h1ii2drz5hphnjr77klhdv8rvzg18")))

(define-public crate-retrofit_codegen-0.3.0 (c (n "retrofit_codegen") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8469x9m3fr84bbvpyyi5pwmp8h265mvn5936j8bk7ddmp9qnc9") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.1 (c (n "retrofit_codegen") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi1i0bqb2i2ppl3frv4mwz27p1vza8r952h3hf3xfaan233cnjg") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.2 (c (n "retrofit_codegen") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybd95w8nv8qzbiwqg7q1ldjd7gd3rnnl9jkks4bnyc5fj49ql9c") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.3 (c (n "retrofit_codegen") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrjd64ky9ghifb664vhswyygz0is1zls818s9kyjxhzi9a6dnqq") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.4 (c (n "retrofit_codegen") (v "0.3.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnc5f9avfm6c536zb40032r1pywm5d8f950cc9266fgx8nd1iyd") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.5 (c (n "retrofit_codegen") (v "0.3.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0yq8ngskj6rrs3x5fw0v5m5d51lbw1avrb15b58hrnw7hahphkh1") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.3.6 (c (n "retrofit_codegen") (v "0.3.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1nr07jxf1x4ybrnlkxahvmfik3dw20k82ban0xyjc1kg602kgy2k") (f (quote (("rocket") ("default" "axum") ("axum"))))))

(define-public crate-retrofit_codegen-0.4.0 (c (n "retrofit_codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10n4hjss60yypi6qr9bdb3paxhp894r2gq7vn3wq951mi7f8yfcg")))

