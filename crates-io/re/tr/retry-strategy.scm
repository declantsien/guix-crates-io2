(define-module (crates-io re tr retry-strategy) #:use-module (crates-io))

(define-public crate-retry-strategy-0.1.0 (c (n "retry-strategy") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "macros" "net"))) (d #t) (k 0)))) (h "14j7y64s49n87i1zhh56advd3xp6bqvpns5y2d47smz9h13ylnrd")))

(define-public crate-retry-strategy-0.2.0 (c (n "retry-strategy") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "macros" "net"))) (d #t) (k 0)))) (h "0gdsd87xzv3v4vb6dmkl5xdwxhwnkjb6avwxpdnglj8imb231mfm")))

