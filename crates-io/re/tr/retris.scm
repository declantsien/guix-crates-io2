(define-module (crates-io re tr retris) #:use-module (crates-io))

(define-public crate-retris-0.1.0 (c (n "retris") (v "0.1.0") (d (list (d (n "cursive") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1sd38bw5z63533208r7j8vwfgm9130934zlsc1ziy2z6b2323p5y")))

(define-public crate-retris-0.2.0 (c (n "retris") (v "0.2.0") (d (list (d (n "cursive") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0i1vp927s5qzywrgj2cwgdzcm1mdxqhlb7qdi71cw3ap3y5cgmcz")))

(define-public crate-retris-0.3.0 (c (n "retris") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20") (k 0)) (d (n "cursive_buffered_backend") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0h042jamka1yd06zmzijrgwqq16w6njl1in5kyxg621czffcb4c2") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "crossterm-backend") ("crossterm-backend" "cursive/crossterm-backend") ("blt-backend" "cursive/blt-backend"))))))

