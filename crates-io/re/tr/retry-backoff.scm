(define-module (crates-io re tr retry-backoff) #:use-module (crates-io))

(define-public crate-retry-backoff-0.0.0 (c (n "retry-backoff") (v "0.0.0") (h "0cdgpg6asyv8f7dwxj3wcni0mbyz6km99lxksdv0khndh9cm19kb") (y #t)))

(define-public crate-retry-backoff-0.1.0 (c (n "retry-backoff") (v "0.1.0") (d (list (d (n "backoff-rs") (r "^0.1") (o #t) (k 0)) (d (n "exponential-backoff") (r "^1.1") (o #t) (k 0)))) (h "0mmf2zzs9zbc6432akixg7xrqg49nw5zdc96r4jg28hwky5hbmg0") (f (quote (("std" "alloc") ("impl_exponential_backoff" "exponential-backoff" "std") ("impl_backoff_rs" "backoff-rs" "std") ("default" "std" "all_backoffs") ("alloc") ("all_backoffs" "impl_exponential_backoff" "impl_backoff_rs"))))))

