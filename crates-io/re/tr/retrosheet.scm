(define-module (crates-io re tr retrosheet) #:use-module (crates-io))

(define-public crate-retrosheet-0.1.0 (c (n "retrosheet") (v "0.1.0") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "1jlp9prjdxavdn51c4sjv8irxyyv7n3knp8k9mc4v8jm6sfzj6nk")))

(define-public crate-retrosheet-0.1.1 (c (n "retrosheet") (v "0.1.1") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "0zlqp503j15rhzfgp1yiwsn2nq7yzm604nf7ih0nmwp4mbmr78xb")))

(define-public crate-retrosheet-0.1.2 (c (n "retrosheet") (v "0.1.2") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "0a3idf4blnyxw0iw0mzhadm2lchj896l5vj4c334dvb52ia4vqc6")))

(define-public crate-retrosheet-0.1.3 (c (n "retrosheet") (v "0.1.3") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "0f2pmchfgg5r0ifpypp7hgf06lmszlcw5byy3r37zipsj3gndvm6")))

(define-public crate-retrosheet-0.2.0 (c (n "retrosheet") (v "0.2.0") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "08j64wdhf2crd6cxq7p8v9hq41g6b2s0x6nr7qk12miifk7nr7xj")))

(define-public crate-retrosheet-0.2.1 (c (n "retrosheet") (v "0.2.1") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "09sdkzkxlrfvbdqx45zm83lgrzabbkp4ch2hgncv6sv2w8pzxm7l")))

(define-public crate-retrosheet-0.2.2 (c (n "retrosheet") (v "0.2.2") (d (list (d (n "nom") (r "^2.2.1") (d #t) (k 0)))) (h "12mrlyr08md0w4ah5bm8zr5csnklk388a3kri7jcwycfvqzd1clg")))

(define-public crate-retrosheet-0.2.3 (c (n "retrosheet") (v "0.2.3") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1r2sq498g2q0cddrqiqa276j02b6vjdr4x13ybbxhfyjhdy1q1rq")))

(define-public crate-retrosheet-0.2.4 (c (n "retrosheet") (v "0.2.4") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "06q8zpj4i8x42n6j424blc8aa8k9wjr56id47x76yn2qd6p5m0k4")))

