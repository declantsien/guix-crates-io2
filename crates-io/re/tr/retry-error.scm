(define-module (crates-io re tr retry-error) #:use-module (crates-io))

(define-public crate-retry-error-0.0.0 (c (n "retry-error") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)))) (h "1g27qkx7v30431qai7zrvam5hnspd1dhs1d0x4v3nnppsy60whn2")))

(define-public crate-retry-error-0.0.1 (c (n "retry-error") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)))) (h "1z7c8zmapp97gk1xrimcs2hnim0prm5b11gsp1rwnnri4maxsd7v")))

(define-public crate-retry-error-0.0.2 (c (n "retry-error") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)))) (h "13xgzc7mngpskqzpi0gc3wwfz33919zb74p1ch32hacyhpibc32z")))

(define-public crate-retry-error-0.0.3 (c (n "retry-error") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.5") (d #t) (k 2)))) (h "1lhqkp22x2sblf7rk6v1pq8cyiab68dyr7xrjiv0crdyl314ysr0")))

(define-public crate-retry-error-0.1.0 (c (n "retry-error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "1i8swxbyk6pz7nhxfqq2ybal9nyxq248i0ia1l4839ds62gnqcfz")))

(define-public crate-retry-error-0.1.1 (c (n "retry-error") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "02llcpcqf51v0cv9316h5czzz02r8gx2l2s4vck4r4207pp0r1ih")))

(define-public crate-retry-error-0.2.0 (c (n "retry-error") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "010dwq2378ajmajpmcilrp9cyn6krxg3nnwi79xalfxdmk2zvlg2") (r "1.56")))

(define-public crate-retry-error-0.2.1 (c (n "retry-error") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "1lvdzhhln2cd55sy1w52vh42v2mbnjfcp459ak81nd3rnjxsrl61") (r "1.56")))

(define-public crate-retry-error-0.2.2 (c (n "retry-error") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "04jx7n85gkgjf44jv7s287zgqz0vjfgy12x3rv7vj4lfqa97164h") (r "1.56")))

(define-public crate-retry-error-0.3.0 (c (n "retry-error") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "10zndnawkch4g395ghkmppxjbbf7zxagqyfsjldmx54kzn09lvic") (r "1.60")))

(define-public crate-retry-error-0.3.1 (c (n "retry-error") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "1a80bpknd3jh1i8lpfxvsx68w0a6dlihiga3wmr74hknh58l3qlj") (r "1.60")))

(define-public crate-retry-error-0.4.0 (c (n "retry-error") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "0py901rcc536wrbwnmghczhbjzm95kc7arr3dpvi4bzj0ibr4rhc") (r "1.65")))

(define-public crate-retry-error-0.4.1 (c (n "retry-error") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "1scdx97ax1vvhdyjyb6626bkyzfx49ab8alhwarcbqdlcbvb9jzd") (f (quote (("full")))) (r "1.65")))

(define-public crate-retry-error-0.4.2 (c (n "retry-error") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)))) (h "0m8ikxdj3sjb2s57njxis1by6qcf4ymd7lhf3g0yk4rg9ykqzbnc") (f (quote (("full")))) (r "1.65")))

(define-public crate-retry-error-0.5.0 (c (n "retry-error") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0f9ky3kc2dfj56r7v3d0jd57zx52gwpnjmpqcsdai9xj80h3h88b") (f (quote (("full")))) (r "1.65")))

(define-public crate-retry-error-0.5.1 (c (n "retry-error") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "069gb349vcj0242cq03n2ak384wji0fpqi6sdfvz67kbbivh1ry5") (f (quote (("full")))) (r "1.65")))

(define-public crate-retry-error-0.5.2 (c (n "retry-error") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "014ghsmzcmzzhvxibf747pg8kqxqx06wwhgdld8rjci3fz4s1pnp") (f (quote (("full")))) (r "1.70")))

