(define-module (crates-io re tr retry-cmd) #:use-module (crates-io))

(define-public crate-retry-cmd-0.1.0 (c (n "retry-cmd") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0f30sqili1xvsh0hnwa98a2ppxcnyd5qqb737n8jfmkkiprglppz")))

(define-public crate-retry-cmd-0.2.0 (c (n "retry-cmd") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0g8h271rz5f4r19wxkdnnj0zvp134qs4cqdg87zwqzbqx5fgvrmw")))

(define-public crate-retry-cmd-0.2.1 (c (n "retry-cmd") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "09ayi2n1kkrxr5ypf4fhq9pcs9anra03vq606fhhdmg2x21w79by")))

(define-public crate-retry-cmd-0.3.0 (c (n "retry-cmd") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1iqhgygc20r7a1q0j95abhm98kmnww8sk5xa4cs7zc3izjdammr6")))

(define-public crate-retry-cmd-0.4.0 (c (n "retry-cmd") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1ak6sx153n1ni5rnd0sbs8sgbr9mnzfr3cynr5q9z5km9gfc3arn")))

(define-public crate-retry-cmd-0.4.1 (c (n "retry-cmd") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "05c530k9k5jwq7435pq00p4i4v3rnzl4msdszjnvs07iiw47gf41")))

