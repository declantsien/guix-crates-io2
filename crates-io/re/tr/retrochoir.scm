(define-module (crates-io re tr retrochoir) #:use-module (crates-io))

(define-public crate-retrochoir-0.0.1 (c (n "retrochoir") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "leptess") (r "^0.11") (d #t) (k 0)))) (h "13xjs6bvl30l1c48wv35hn85f2psci0s4qphikilkd7ym6xqhf44")))

