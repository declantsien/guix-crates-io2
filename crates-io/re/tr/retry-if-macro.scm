(define-module (crates-io re tr retry-if-macro) #:use-module (crates-io))

(define-public crate-retry-if-macro-0.1.0 (c (n "retry-if-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1c7qn8cz5r9m7xlbi7ivs8fhq2g60lp4ccj5cb6bvkn1s9s6lqlx") (s 2) (e (quote (("tracing" "dep:tracing"))))))

