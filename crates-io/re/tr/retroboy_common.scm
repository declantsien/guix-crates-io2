(define-module (crates-io re tr retroboy_common) #:use-module (crates-io))

(define-public crate-retroboy_common-0.1.0 (c (n "retroboy_common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v7mha20kp090dgc7513x776p8img873wqnxgx8byfvi6yhsbyab")))

