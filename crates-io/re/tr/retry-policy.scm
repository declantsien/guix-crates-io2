(define-module (crates-io re tr retry-policy) #:use-module (crates-io))

(define-public crate-retry-policy-0.0.0 (c (n "retry-policy") (v "0.0.0") (h "071an701x9g7j9dhnvzwzql5w3gygf25nh8m8fck13q503wksnyl") (y #t)))

(define-public crate-retry-policy-0.1.0 (c (n "retry-policy") (v "0.1.0") (d (list (d (n "retry-backoff") (r "^0.1") (k 0)) (d (n "retry-predicate") (r "^0.1") (k 0)))) (h "02kaiz297ss88mhh16bpzm7qyklv7ygi1w2akqby0lymgxfdgr65") (f (quote (("std" "alloc" "retry-backoff/std" "retry-predicate/std") ("default" "std") ("alloc" "retry-backoff/alloc" "retry-predicate/alloc"))))))

(define-public crate-retry-policy-0.2.0 (c (n "retry-policy") (v "0.2.0") (d (list (d (n "retry-backoff") (r "^0.1") (k 0)) (d (n "retry-predicate") (r "^0.1") (k 0)))) (h "04s2fk04czrdzqynkzjbvglkv6ynj2yni1mhqhhs4czw4wmw65qi") (f (quote (("std" "alloc" "retry-backoff/std" "retry-predicate/std") ("default" "std") ("alloc" "retry-backoff/alloc" "retry-predicate/alloc")))) (r "1.58.0")))

(define-public crate-retry-policy-0.2.1 (c (n "retry-policy") (v "0.2.1") (d (list (d (n "retry-backoff") (r "^0.1") (k 0)) (d (n "retry-predicate") (r "^0.1") (k 0)))) (h "16ppbdi8z5lc2kax93md6865y9caa9ajmkssmgcij1rqnv76wqz4") (f (quote (("std" "alloc" "retry-backoff/std" "retry-predicate/std") ("default" "std") ("alloc" "retry-backoff/alloc" "retry-predicate/alloc"))))))

