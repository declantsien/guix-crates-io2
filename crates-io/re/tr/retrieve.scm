(define-module (crates-io re tr retrieve) #:use-module (crates-io))

(define-public crate-retrieve-1.0.0 (c (n "retrieve") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "06g2d6rp0jdlzhl1vk2j7g03yma141r71r37yarg9ainwqxjmx3p") (f (quote (("print") ("default"))))))

(define-public crate-retrieve-1.0.1 (c (n "retrieve") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "06fylqp6a2c816181h580jy4jrvl8q6jxyzs8zy9zn4j431sdn96") (f (quote (("print") ("default"))))))

(define-public crate-retrieve-1.0.2 (c (n "retrieve") (v "1.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1ma4lrd7qr3wnkr08lj2xsfwrv2gsakn3q1mx5gingihh5ii55ra") (f (quote (("print") ("default"))))))

(define-public crate-retrieve-1.1.0 (c (n "retrieve") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "08sr856yq5jlan8l1dl34fm52raz6kwcxmjkyqxliq1n2j94238w") (f (quote (("print") ("default"))))))

(define-public crate-retrieve-1.1.1 (c (n "retrieve") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1ncm6zkfvfayygnp2gbsz3gs7a3sdm2hl342rgj8sjx91m1lxhqp") (f (quote (("print") ("default"))))))

(define-public crate-retrieve-1.1.2 (c (n "retrieve") (v "1.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1cd77svc721mriwp5bnx9fdhd6hz16rnhh0aczysangy95rf6w26") (f (quote (("print") ("default"))))))

