(define-module (crates-io re tr retri) #:use-module (crates-io))

(define-public crate-retri-0.1.0 (c (n "retri") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "zduny-wasm-timer") (r "^0.2.8") (d #t) (k 0)))) (h "1r2fczrxbh9snskqsmkndgyzlln65d4s5cnp3biz47hw4d3229pn")))

