(define-module (crates-io re tr retry_macro) #:use-module (crates-io))

(define-public crate-retry_macro-0.1.0 (c (n "retry_macro") (v "0.1.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "08v406ar0ba2plwkg7jc03whlkpmwqzf3sg2n4cf98a2k574vzl2") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.1.1 (c (n "retry_macro") (v "0.1.1") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "1ddxa7nh8lw3ddp094xrh3a6psjyy46bz1mzc7c6h7r4vy7842l3") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.1.2 (c (n "retry_macro") (v "0.1.2") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "021k9a74cc12vdsvzw0gd1f55bdnhg99mzzsczpnspv6gjhp8dzy") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.1.3 (c (n "retry_macro") (v "0.1.3") (d (list (d (n "shadow-clone") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "0df7iadx9xdij1a35gxv9zv1lly8ml1k8qh2pl26j95169rpzq01") (y #t) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.1.4 (c (n "retry_macro") (v "0.1.4") (d (list (d (n "shadow-clone") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "0g22daq514xjq76vk9dw7mznn9wgki1b7fjn5l9wrfvs22chy0qf") (y #t) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.1.5 (c (n "retry_macro") (v "0.1.5") (d (list (d (n "shadow-clone") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "1b02sgwn61x2aiadrq7hga76y3zb0a8xg1knb3hml07b75znp3d6") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-retry_macro-0.2.0 (c (n "retry_macro") (v "0.2.0") (d (list (d (n "shadow-clone") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)))) (h "14c8fglmp6zzlhgk94jmk6a6m0786zdw07kfxv6hzzkvdb1d3mip") (s 2) (e (quote (("tokio" "dep:tokio"))))))

