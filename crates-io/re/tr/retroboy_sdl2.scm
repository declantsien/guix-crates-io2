(define-module (crates-io re tr retroboy_sdl2) #:use-module (crates-io))

(define-public crate-retroboy_sdl2-0.1.0 (c (n "retroboy_sdl2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "retroboy_common") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.36") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "0gfkg3ksigmnsyb0i7s58f724qfjz3q7xx06bl9bmxhj9bcr6wh5")))

