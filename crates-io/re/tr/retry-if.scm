(define-module (crates-io re tr retry-if) #:use-module (crates-io))

(define-public crate-retry-if-0.1.0 (c (n "retry-if") (v "0.1.0") (d (list (d (n "retry-if-macro") (r "^0.1.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "08xx5ivf36pim2d2nvyh369jb1qy0f6ggmrdsyn9l838sa36fv7f") (f (quote (("tracing" "retry-if-macro/tracing"))))))

(define-public crate-retry-if-0.2.0 (c (n "retry-if") (v "0.2.0") (d (list (d (n "retry-if-macro") (r "^0.1.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "1a4gxb23bx0i9fm46kcj9i593j5fr6i8bli88c16nhjcfixd45h1") (f (quote (("tracing" "retry-if-macro/tracing"))))))

