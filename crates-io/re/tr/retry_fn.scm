(define-module (crates-io re tr retry_fn) #:use-module (crates-io))

(define-public crate-retry_fn-0.1.0 (c (n "retry_fn") (v "0.1.0") (d (list (d (n "async-std") (r ">=1.7.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "version-sync") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "1jfk98gabvx3kx2parrw94hyj1zvy1r10gakdclq1hylalgh95bi") (f (quote (("tokio-runtime" "tokio") ("default") ("async-runtime" "async-std"))))))

(define-public crate-retry_fn-0.2.0 (c (n "retry_fn") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0xsag5xqzh55j9bdrcb7hl9j3zs4mikn7njcd0daf5dnvpvj7dds") (f (quote (("tokio-runtime" "tokio") ("default") ("async-runtime" "async-std"))))))

