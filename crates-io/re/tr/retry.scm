(define-module (crates-io re tr retry) #:use-module (crates-io))

(define-public crate-retry-0.1.0 (c (n "retry") (v "0.1.0") (h "0gvvgw57y5j0waa81mg5ydxybk2a893vqcaniykjl9h5dh75fbkv")))

(define-public crate-retry-0.2.0 (c (n "retry") (v "0.2.0") (h "102vj3axx7l9zj5ribhigai3jar2hwgf3s8vky7w2q4bv9cv4dp8")))

(define-public crate-retry-0.3.0 (c (n "retry") (v "0.3.0") (h "0nng7vsf4cn4a844d7rakxjrqxs5vvhzkssa0xhk0z8hzszwb0sf")))

(define-public crate-retry-0.4.0 (c (n "retry") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "03gc1m8qm18bz6kcn180mb6blc43v5mpj3h1485wfpx225h0yii9")))

(define-public crate-retry-0.5.0 (c (n "retry") (v "0.5.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "02zm8p46vqhkqs0aa6mf9km9403i00hi3blw8hx81hjkwzlf70bn")))

(define-public crate-retry-0.5.1 (c (n "retry") (v "0.5.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0a2hpb2w83ap71y075r1jh9anmb51wbv93b00x5slc9q3frq7b68")))

(define-public crate-retry-1.0.0 (c (n "retry") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1cmr035vqdrgqhn0qz7y2ssjn883mnxq4k8v822vfdvs4jnpm7kq")))

(define-public crate-retry-1.1.0 (c (n "retry") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ra14c3lawwxljc6v62xhggwpdbbddkkwa5qwq7sl5prdajg7bfx")))

(define-public crate-retry-1.2.0 (c (n "retry") (v "1.2.0") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "1nkdyk2ayqbikcm94xa24i8qx6xb1qryrp45zpbndl08j5wg8pn1") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-retry-1.2.1 (c (n "retry") (v "1.2.1") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "0bjwakicmq8idkxhz9kj9flg140fq87iqymyd0vpxpa39djlmvnh") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-retry-1.3.0 (c (n "retry") (v "1.3.0") (d (list (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "01yw0b4ni1dhrrrkjzjykji6adi8rvsxflx5wn9h3j9ygnizfz0x") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-retry-1.3.1 (c (n "retry") (v "1.3.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1xm3p41ygijbjpyj81psqhb2r3rdcqwlk5pl48lgsqwsjh5cd5dc") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-retry-2.0.0 (c (n "retry") (v "2.0.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1x65r4bsa6n6zfvp2sw3qgrgcc3yqkx86xjha3wpadfyc8hxfrli") (f (quote (("random" "rand") ("default" "random"))))))

