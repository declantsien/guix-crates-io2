(define-module (crates-io re ck reckoner) #:use-module (crates-io))

(define-public crate-reckoner-0.1.0 (c (n "reckoner") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "imath-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "060k2w9jgl4yd3cvlplh43c0d3sj8y8g7y1ivixqdingvmfx0x2h")))

(define-public crate-reckoner-0.1.1 (c (n "reckoner") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "creachadair-imath-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1605mp3gvb4yfrvjk3a7jy6320sfagwjvpyacaag3imnzbgimjca")))

(define-public crate-reckoner-0.1.2 (c (n "reckoner") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "creachadair-imath-sys") (r "^0.1.2-alpha.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "08c2inmcri9lgyjg9cg7af64akjksw9mwg1y932ir21qfqqzx423")))

(define-public crate-reckoner-0.2.0 (c (n "reckoner") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "creachadair-imath-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1dp636hnjgl55fka5gz85a9y4wbzblfqvcv3kw3v3drfgii8r416")))

