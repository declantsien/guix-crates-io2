(define-module (crates-io re fr refraction) #:use-module (crates-io))

(define-public crate-refraction-0.1.0 (c (n "refraction") (v "0.1.0") (h "07051aiamybwbkgkh6ly61rpg5snhsllzpazgs556163fr96nfap") (f (quote (("nightly"))))))

(define-public crate-refraction-0.1.1 (c (n "refraction") (v "0.1.1") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)))) (h "1siai8dk691gp8ic5ymr9jjqvcdm8kh3jbv0s174k54plrikmaxs") (f (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-refraction-0.1.2 (c (n "refraction") (v "0.1.2") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)))) (h "0ai2glvjr4pfddcw4zyzyy5q138nqm7958sgx3pkb1xcpnjynsg7") (f (quote (("nightly" "nodrop/use_union"))))))

