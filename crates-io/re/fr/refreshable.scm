(define-module (crates-io re fr refreshable) #:use-module (crates-io))

(define-public crate-refreshable-1.0.0 (c (n "refreshable") (v "1.0.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0h22asfq9xjx2kn5c8nblr64q806p35z8vkshbqi1pcx8ni4p5l4")))

(define-public crate-refreshable-1.0.1 (c (n "refreshable") (v "1.0.1") (d (list (d (n "arc-swap") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "05dra4k8ic67747q6jjfzkrm2bpmxx2g7jcviac5zzfri5a1lsa4")))

(define-public crate-refreshable-1.0.2 (c (n "refreshable") (v "1.0.2") (d (list (d (n "arc-swap") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0zb5cw8lszgycwd4ap8fqhy3zs9vd8dy25lpn4nrza11hswyqsim")))

(define-public crate-refreshable-1.1.0 (c (n "refreshable") (v "1.1.0") (d (list (d (n "arc-swap") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0l41wz0wl883q3gq09zkvj8nv4mg35fpg308cgb8dicp1gwsi1dv")))

(define-public crate-refreshable-2.0.0 (c (n "refreshable") (v "2.0.0") (d (list (d (n "arc-swap") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0kk02k496jr5f2nsm3i3b690pbc31vs7kl023sd4nl9gjfwvypg6")))

