(define-module (crates-io re fr refract) #:use-module (crates-io))

(define-public crate-refract-0.0.0 (c (n "refract") (v "0.0.0") (h "1nncw4zgnv85gdgjvf4kkwqsc244l980ffh71cvw8cxfpxq8qfbf")))

(define-public crate-refract-0.0.1 (c (n "refract") (v "0.0.1") (h "0xhmrjl9m6vk8bkqwd1f77sjfjnhz8959d7yk0hlzg5lphgfkkz1") (y #t)))

