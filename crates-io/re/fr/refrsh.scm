(define-module (crates-io re fr refrsh) #:use-module (crates-io))

(define-public crate-refrsh-0.1.0 (c (n "refrsh") (v "0.1.0") (d (list (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.2") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "0v1gxdh54hqyr1zfa89cv16k3fiwalr4qws8z2py3i2kszs2q1kw")))

(define-public crate-refrsh-0.2.0 (c (n "refrsh") (v "0.2.0") (d (list (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.2") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "1km6adwwqq62bsw3ga70y5ss6zb92rnsbiak40cis444zdx2vxsz")))

(define-public crate-refrsh-0.2.1 (c (n "refrsh") (v "0.2.1") (d (list (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.2") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "09174lip9m51skj21vskvnv20l3isj814rwq3gyzwlhkj5mvkmmf")))

