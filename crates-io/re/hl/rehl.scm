(define-module (crates-io re hl rehl) #:use-module (crates-io))

(define-public crate-rehl-1.0.0 (c (n "rehl") (v "1.0.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07m747nnjnlnys4f9hzx4cbp68zv8ls5v95pmb5dcjhy98mzwx6i")))

(define-public crate-rehl-1.0.1 (c (n "rehl") (v "1.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1n55v4cb8nvpmab77yarhhwydkvndpc6mzgqzmz31x86smskxmjg")))

