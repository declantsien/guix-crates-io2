(define-module (crates-io re _s re_smart_channel) #:use-module (crates-io))

(define-public crate-re_smart_channel-0.2.0-alpha.0 (c (n "re_smart_channel") (v "0.2.0-alpha.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0596svpjll6w8wpsb3hi067caf213il68z8swbwnf6xpcxw85dwa") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.1 (c (n "re_smart_channel") (v "0.2.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1l5wq25c8wdd4v6j3bk3fngykrmw3av27z9rylbhvaijiaavpy8j") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.2 (c (n "re_smart_channel") (v "0.2.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0k8gsl75b53xgliys4wdpcj729rca1nrxpgillzaksqs3kcyjn9a") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.3 (c (n "re_smart_channel") (v "0.2.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "12kjc26zqf9l8kpw88rx5yhk9v11lj6k95wy76mb8rhpckx54a9d") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.4 (c (n "re_smart_channel") (v "0.2.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0cpnjhg70vpkbcrn31shfyvnlc4asrkr74dsa7w5r4pmw5635q9c") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.5 (c (n "re_smart_channel") (v "0.2.0-alpha.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "11dx7983yd54hcr8r8sxg51sw2iy0ksvrncxb93l1f3fdrkynxmk") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.6 (c (n "re_smart_channel") (v "0.2.0-alpha.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1b5pb1wjl4y2l6gsbqw6znn7pk0ifqnqgphlpppyyzx1a8x8cqs4") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0-alpha.8 (c (n "re_smart_channel") (v "0.2.0-alpha.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0v44p7c0shjbl38gmsr4d5g6yk80mspc4jl94ls81mlbmrc61fmc") (r "1.67")))

(define-public crate-re_smart_channel-0.2.0 (c (n "re_smart_channel") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "12zwcbk66c4l6lcwpga9sylm6bycvzkczpqgzv53z5z0kb2xa3g9") (r "1.67")))

(define-public crate-re_smart_channel-0.3.0-alpha.1 (c (n "re_smart_channel") (v "0.3.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1xhlpqx20s3ixi2gps18k1lhqqad5359kzxw58wxaw5wx6xkhhl0") (r "1.67")))

(define-public crate-re_smart_channel-0.3.0 (c (n "re_smart_channel") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "143b6f6xkjbxsxwgfmi95i6a8j89fz7l6llnnklfsjp1bxl19kgl") (r "1.67")))

(define-public crate-re_smart_channel-0.3.1 (c (n "re_smart_channel") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1zx3wap16j9xaqv9gmg8rjk90365d044kykm527vdyw0y24f3j21") (r "1.67")))

(define-public crate-re_smart_channel-0.4.0 (c (n "re_smart_channel") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1gs5mnzx4icjdhjiwlcahgfk4vras0dn5fmkyg53wqnzz21zvddm") (r "1.67")))

(define-public crate-re_smart_channel-0.5.0-alpha.0 (c (n "re_smart_channel") (v "0.5.0-alpha.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1ybb0i9g7nwlvcr48d404cc0nccbxikg3s6b8impw6vg3is0ydfz") (r "1.67")))

(define-public crate-re_smart_channel-0.5.0 (c (n "re_smart_channel") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1bivqxpccfyr89r4l2a58fhc22q09rn65p3kmfds5d3251ydxhjd") (r "1.67")))

(define-public crate-re_smart_channel-0.5.1-alpha.1 (c (n "re_smart_channel") (v "0.5.1-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0c3my3xh2dcz43342w9lr95r6an5fwjccdngimzdi9pwhi9qlfd8") (r "1.67")))

(define-public crate-re_smart_channel-0.5.1 (c (n "re_smart_channel") (v "0.5.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "148pxdw05qwfjb3j83l9gc4sdmjv12xm0q7z9ds834yjxzwglllv") (r "1.67")))

(define-public crate-re_smart_channel-0.6.0-alpha.1 (c (n "re_smart_channel") (v "0.6.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0ry2hi17qmy5skqiji4b5vx2jb5zxpxs813zw98s2ifisr5fr8pc") (r "1.69")))

(define-public crate-re_smart_channel-0.6.0-alpha.2 (c (n "re_smart_channel") (v "0.6.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0pdyawim27xxwml6q9hllg69lk2jvpabz1ksa4ynvq41m2aki491") (r "1.69")))

(define-public crate-re_smart_channel-0.6.0 (c (n "re_smart_channel") (v "0.6.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "13v8dp6nqcdvvwjxffjld9hssgn8h1233i0aqb41bkm9brrprz30") (r "1.69")))

(define-public crate-re_smart_channel-0.7.0-alpha.1 (c (n "re_smart_channel") (v "0.7.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1l65j8w512xb45dkdc11i431ch49dzj4zmdqjxph28992vzawqvl") (r "1.69")))

(define-public crate-re_smart_channel-0.7.0 (c (n "re_smart_channel") (v "0.7.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1chv8cbx53gflp61hihyn188n6a99ddy2rk16ra6scvk86vi2zfb") (r "1.69")))

(define-public crate-re_smart_channel-0.8.0-alpha.2 (c (n "re_smart_channel") (v "0.8.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1zddnr0v19sbcdzpfiwkkl888hrvnf5ps2h9048cij1b7015zybb") (r "1.69")))

(define-public crate-re_smart_channel-0.8.0-alpha.6 (c (n "re_smart_channel") (v "0.8.0-alpha.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1vs3rqjbxcdk6z2x8h276dqs2zl5zfd0608pj43y9cz8sf6kavsz") (r "1.69")))

(define-public crate-re_smart_channel-0.8.0-alpha.7 (c (n "re_smart_channel") (v "0.8.0-alpha.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "03psrg0vf6xvsg2pzb0skpi6nd40fhmnnm0v1fqh09xknihsnw2x") (r "1.69")))

(define-public crate-re_smart_channel-0.8.0 (c (n "re_smart_channel") (v "0.8.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1my35d0bgrj0lmrsrl9894wpl2kiq7z773c9xzdwkazx93zjdhna") (r "1.69")))

(define-public crate-re_smart_channel-0.9.0-alpha.1 (c (n "re_smart_channel") (v "0.9.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "12ng81a3ga1dm9nslj2py0gaji93z3n9qfv64vas91a7mywblnxy") (r "1.69")))

(define-public crate-re_smart_channel-0.9.0-alpha.2 (c (n "re_smart_channel") (v "0.9.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1j63zxcvc5f01by1fjrrl6wlk74353y430c4l2px3ys2q7v6xysy") (r "1.69")))

(define-public crate-re_smart_channel-0.8.1-rc.1 (c (n "re_smart_channel") (v "0.8.1-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "02vqw0lh07yxghrpa8b2k9nbdrmzbzlayk7pvqplhy0v85zs3jpj") (r "1.69")))

(define-public crate-re_smart_channel-0.8.1 (c (n "re_smart_channel") (v "0.8.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "106s0aai5zmmfwcikbn8ka0gjs0lv7vm0gx07ifw7dxszx53c6pk") (r "1.69")))

(define-public crate-re_smart_channel-0.9.0-alpha.3 (c (n "re_smart_channel") (v "0.9.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1v5j2gzbz5f7818wv1ciwqialcrnddg5zd163llb37cwrbsbv1l7") (r "1.72")))

(define-public crate-re_smart_channel-0.8.2 (c (n "re_smart_channel") (v "0.8.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1giavhbl00wv6k560kzk8ly0ilv6g84j9h65kbrin9ij0swkvp85") (r "1.69")))

(define-public crate-re_smart_channel-0.9.0-alpha.5 (c (n "re_smart_channel") (v "0.9.0-alpha.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-alpha.5") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0ws4nhsk670kdjky1x78cfjy8x695977ylfl7kpqn57x0iw1b8z1") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-alpha.6 (c (n "re_smart_channel") (v "0.9.0-alpha.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-alpha.6") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0s6g7g2qxchqc3w0lpdcg7vd1xz1fswkizvky26vgaw2jqx8jxlc") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-alpha.9 (c (n "re_smart_channel") (v "0.9.0-alpha.9") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-alpha.9") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "082l0k84mf36nvykmmacdpn2p38fgn9p1il0fmbm38q0n7dzj17b") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-alpha.10 (c (n "re_smart_channel") (v "0.9.0-alpha.10") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-alpha.10") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0qkajv9l1by3zzzadppw0c9d17dgz4184h1f5q2m26qlvzjvdi3b") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-rc.2 (c (n "re_smart_channel") (v "0.9.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0vimc7q4qdpj40lflkqls1ir88ngxq74zra024vk52k39lkc9i0s") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-rc.3 (c (n "re_smart_channel") (v "0.9.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-rc.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "195dlgz7jyajlpb5xg1pmlqmpghk8x2x1c2xqrchnil2s78rd8q1") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-rc.6 (c (n "re_smart_channel") (v "0.9.0-rc.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-rc.6") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "13d9q80vsrrhlxm2pj91d2n17p4hrpw94wakkc2wp1bpkbvl9zim") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0-rc.7 (c (n "re_smart_channel") (v "0.9.0-rc.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.0-rc.7") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "03555k3bas9ngjxyra9mbdhsrdwfaiqx3bdbl5mpwvra85ax2pr9") (r "1.72")))

(define-public crate-re_smart_channel-0.9.0 (c (n "re_smart_channel") (v "0.9.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.9.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0dl2makb2995850lq2lbm7ymlcpc7l7aqcin72vvsy78nb6j39y6") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.2 (c (n "re_smart_channel") (v "0.10.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "00y4phkbg35h1hw18z3nlncqyimm5y6kirg96dm4kxl4qwn76ghw") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.3 (c (n "re_smart_channel") (v "0.10.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1d4z6a90cv88wfa73kipb5xczswz7ba4yg0lpz8n6kd0njavr97m") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.4 (c (n "re_smart_channel") (v "0.10.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1q6433bd6ccgdw610zprf6g9hjlqr2mpiqzbvb3xi641p5nha0fy") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.5 (c (n "re_smart_channel") (v "0.10.0-alpha.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.5") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1llyxkh48b3ymizv8ypqxfnvby1yn3dsbic9315bx78nva9d504n") (r "1.72")))

(define-public crate-re_smart_channel-0.9.1-rc.3 (c (n "re_smart_channel") (v "0.9.1-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.1-rc.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1lq691x9fspbp4fk44xz62wjg0s3nsjjzcwb60n5p7farldkhmay") (r "1.72")))

(define-public crate-re_smart_channel-0.9.1-rc.4 (c (n "re_smart_channel") (v "0.9.1-rc.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.9.1-rc.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "09ajc2ing1v44xqpandzxqbpkr79fhmj0y76qg7lbpamwimpi31r") (r "1.72")))

(define-public crate-re_smart_channel-0.9.1 (c (n "re_smart_channel") (v "0.9.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.9.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0ak3dkq6d19jqkbs8xv7zwwvx3bxv7idmfk72dzsr0p3d01fc5m0") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.8 (c (n "re_smart_channel") (v "0.10.0-alpha.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.8") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0fdrlcql5q2pgsvbg0h7vw94wmhnd6sdrs9fqczw6s61vwhxhi3z") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-alpha.10 (c (n "re_smart_channel") (v "0.10.0-alpha.10") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-alpha.10") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1ybmsrma6ir7s5m5m6bvrymyjahsl1525bjxcf01jr61yk89npha") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0-rc.1 (c (n "re_smart_channel") (v "0.10.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.0-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1lrv2fbj5f9yfjlshxqn20n9ai5ldf9jf1dk57yacpv0bl96rm8b") (r "1.72")))

(define-public crate-re_smart_channel-0.10.0 (c (n "re_smart_channel") (v "0.10.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.10.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1kilijk678hpzp4zmkgq73wzbk4ar3mpppvzwddjl6i5yp639lr8") (r "1.72")))

(define-public crate-re_smart_channel-0.10.1-rc.1 (c (n "re_smart_channel") (v "0.10.1-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.10.1-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1bhi8xb84mcs0vv0jwdbvb3yh8wyz896wpjvfssbz5bn4fyz4qvg") (r "1.72")))

(define-public crate-re_smart_channel-0.10.1 (c (n "re_smart_channel") (v "0.10.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.10.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1ijvinhzma7bragkh3kf4wlccmb0nvmxc993shqbhslf59ldxwfc") (r "1.72")))

(define-public crate-re_smart_channel-0.11.0-rc.1 (c (n "re_smart_channel") (v "0.11.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.11.0-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1pn9g13vj6dyd6j4471zjwyrsh9a5v31s47q7lg698a0vs0vi5yw") (r "1.72")))

(define-public crate-re_smart_channel-0.11.0-rc.2 (c (n "re_smart_channel") (v "0.11.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.11.0-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0d5ysrjkkyqysv4256c21dxqw6wlgb0nq1qrkfb9c1ga2770p7ci") (r "1.72")))

(define-public crate-re_smart_channel-0.11.0 (c (n "re_smart_channel") (v "0.11.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.11.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1s9drdslrkhj9sfdl8l4rsyvlpny5na4il3ibjm2cfi4hvcn535x") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-alpha.2 (c (n "re_smart_channel") (v "0.12.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-alpha.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0lyb0a6y56kjc04iygarmkyrikcp7wwzy2y811gbmnsx6pypr8g3") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-alpha.3 (c (n "re_smart_channel") (v "0.12.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-alpha.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0fvixi77s71l64xfv7yklvdnn9xjfr3lkhr8zvkapg9d6v59s7my") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-alpha.4 (c (n "re_smart_channel") (v "0.12.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-alpha.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1l8wvsfk2jb14287h055caccg52775fyk29bbr4k9jcgnl6jgmkl") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-rc.1 (c (n "re_smart_channel") (v "0.12.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1zpjs3d74y9fwlgmvz41vpqqjb98kx2rim1p9k46b9cajnk546wn") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-rc.2 (c (n "re_smart_channel") (v "0.12.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1z9vqbvgawiy9yp6n0dif0wz4l9dgiyrqbk6fnwj7dbw3lfmpm9v") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-rc.3 (c (n "re_smart_channel") (v "0.12.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-rc.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "15jx8c936vd9pkgaz0pg1wbi5ad2cwwyrl48m89yfqf9brd15ksn") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-rc.4 (c (n "re_smart_channel") (v "0.12.0-rc.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-rc.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0viqy8hzs6yqbz55myv8aplyfpxvnf59fy5yrnrfp2sgdv0rshyx") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0-rc.6 (c (n "re_smart_channel") (v "0.12.0-rc.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.0-rc.6") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0xdgizxw22plwrxrgdq76izg9qb3dfx1gm023ag9kwmvibpb4jfr") (r "1.72")))

(define-public crate-re_smart_channel-0.12.0 (c (n "re_smart_channel") (v "0.12.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.12.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "18v0c9b804rk4l18yq0k1l8j01y4cl8zlpfxwg6n1j8vypqn899b") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-alpha.2 (c (n "re_smart_channel") (v "0.13.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-alpha.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "06bn0p4ypwjr7vj7v6nq1vcf70a8blmbdgjdv1dii1mnlgmqvinh") (r "1.72")))

(define-public crate-re_smart_channel-0.12.1-rc.1 (c (n "re_smart_channel") (v "0.12.1-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.1-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1cfb6ixspc4vn9qasbi8haz95nkwkx07cz7vlrgcd52g5ghgm3bv") (r "1.72")))

(define-public crate-re_smart_channel-0.12.1-rc.2 (c (n "re_smart_channel") (v "0.12.1-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.12.1-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0052k268k3qchvbd68lknlyflxhdmn0gk0vb6pv7xxxbmpiph2fh") (r "1.72")))

(define-public crate-re_smart_channel-0.12.1 (c (n "re_smart_channel") (v "0.12.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.12.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1pl5ns0i5g768zfjcilcibhlmcis7f7v3w3d79nz517pd6zj1nv9") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-alpha.3 (c (n "re_smart_channel") (v "0.13.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-alpha.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "10aa080kmpbgh1fbn0xa6k4w588dl8jfvpz91yjd9mf4424wmdma") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-alpha.4 (c (n "re_smart_channel") (v "0.13.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-alpha.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1zjxci0qhxp8sjb53n3z6p21hx157rjilcrlbj55bm7ykpikick0") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.1 (c (n "re_smart_channel") (v "0.13.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0d9anmab28rmmrgz8iy308rhzlvy0x1w2s7g22ixqaq6i1rvkmjk") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.2 (c (n "re_smart_channel") (v "0.13.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1hm2h09kiip9mxvgdjk6w2jyln55xqrbarwi0fak7xrnimw3v9z4") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.3 (c (n "re_smart_channel") (v "0.13.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1s388gnb4njpgdgff2l2qg5b85k8ajhzslji8a66c7rmvc97x4am") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.4 (c (n "re_smart_channel") (v "0.13.0-rc.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0bi3qs30sjj4mgipls8bajc91xkqla575l3m9iwr4q987qvqn0sm") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.5 (c (n "re_smart_channel") (v "0.13.0-rc.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.5") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0lhbrilx00jsm3a74i7k29grar4jrjszpfq5615mzgn9msjcdwyy") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.6 (c (n "re_smart_channel") (v "0.13.0-rc.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.6") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0rrl23h6chrg8svh9npbsbn0p8fb5gyilbzymp4wk90bv1kwd4j5") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0-rc.7 (c (n "re_smart_channel") (v "0.13.0-rc.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.13.0-rc.7") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1iy9qwkd1pc87kbfg3fzh79y1jj32m5bl97ml5alqidxghihfgny") (r "1.72")))

(define-public crate-re_smart_channel-0.13.0 (c (n "re_smart_channel") (v "0.13.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.13.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0zlkp9zb53h15sil28c8ljk1i9dacr1d7pk50cigzp7bw2q4yh80") (r "1.72")))

(define-public crate-re_smart_channel-0.14.0-alpha.2 (c (n "re_smart_channel") (v "0.14.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.14.0-alpha.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0k8r9rjqr9f5jpln5i5swwhlkidci3yycxzf5x7dpcfmph7ml8l5") (r "1.74")))

(define-public crate-re_smart_channel-0.14.0-rc.1 (c (n "re_smart_channel") (v "0.14.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.14.0-rc.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1y8j88k0px3lpkjd997rnfx0p3f4q13lyd8bjwf4zab0bfpyysyy") (r "1.74")))

(define-public crate-re_smart_channel-0.14.0-rc.2 (c (n "re_smart_channel") (v "0.14.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.14.0-rc.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "116zrai833hrnwsawgbmvbzlhxlf2bldinl88jc3v7qafrn39nja") (r "1.74")))

(define-public crate-re_smart_channel-0.14.0-rc.3 (c (n "re_smart_channel") (v "0.14.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.14.0-rc.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0a2dcv9n2saxi40vhnd10a504lfmvml1vspz0v1xiblhac86j34i") (r "1.74")))

(define-public crate-re_smart_channel-0.14.0-rc.4 (c (n "re_smart_channel") (v "0.14.0-rc.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.14.0-rc.4") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1krqpciv46k9fj4yr1avh8x3n9qng323wdr2yvkbvgd8gv5j5cxq") (r "1.74")))

(define-public crate-re_smart_channel-0.14.0 (c (n "re_smart_channel") (v "0.14.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.14.0") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0k2qxg51q84lgr7m7yi6jhlf3xdhm4i7blnqjcyda3rdkg6xsp1s") (r "1.74")))

(define-public crate-re_smart_channel-0.14.1 (c (n "re_smart_channel") (v "0.14.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.14.1") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "003ak82wisn7dgdxxxs4qjcdfkgim6ayyxsdjd9amw2mjfyl4ynl") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-alpha.2 (c (n "re_smart_channel") (v "0.15.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-alpha.2") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "01mpxn55w6dny9kdf0yj1dsdppc9m7mva0n3gggp0hfskp95c87j") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-alpha.3 (c (n "re_smart_channel") (v "0.15.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-alpha.3") (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1qyhpvz44zlp7d244dw5ni351pcf60j4275zqfgqkh0phlj6g7md") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-alpha.4 (c (n "re_smart_channel") (v "0.15.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-alpha.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "146i4rd93xcibnbnafhgqp9v80yr7jc1qzyhhgg4vjdljschpn9q") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-alpha.5 (c (n "re_smart_channel") (v "0.15.0-alpha.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-alpha.5") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "04vvm0j0jznijjrb52a0iymbim0pm606dw45wg1qa5z00vr2kbq2") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-rc.1 (c (n "re_smart_channel") (v "0.15.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1cx17vljx4xsbfd5p2cimh0wd4r2njskq207316k20klk4v3zp8m") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-rc.2 (c (n "re_smart_channel") (v "0.15.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-rc.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0b2ppvc8n7wh7hxqnfb571vharb3z1vxq3m2i7qchsrw0bks1smh") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0-rc.3 (c (n "re_smart_channel") (v "0.15.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.0-rc.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "00lmqy68sq85xvw5nkpm04kip5kysmq58im1r70hgn9by8l3p4s5") (r "1.74")))

(define-public crate-re_smart_channel-0.15.0 (c (n "re_smart_channel") (v "0.15.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.15.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0zk72gld88k91ci2h2vpn8a2xh8v5v3gsk6nbdk3qjrlr0lzgxjy") (r "1.74")))

(define-public crate-re_smart_channel-0.15.1-rc.1 (c (n "re_smart_channel") (v "0.15.1-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.15.1-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "01caqprxk58rbhsjwvkib8cgzy8npgw5m3c0wldy9l70f8d21kqi") (r "1.74")))

(define-public crate-re_smart_channel-0.15.1 (c (n "re_smart_channel") (v "0.15.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.15.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1mpmaa6imyhf21m3vjcjj283fnkylp3nbrg1pnf473vw93mk8fa7") (r "1.74")))

(define-public crate-re_smart_channel-0.16.0-alpha.3 (c (n "re_smart_channel") (v "0.16.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-alpha.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0h6skxr8ssjkiyjfv52nxs99zmjqjijh1gz9gi0n9sf1kgihlvm9") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0-alpha.4 (c (n "re_smart_channel") (v "0.16.0-alpha.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-alpha.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1raswkmq8pf5i3z9jnn895f0l6nq8mdmc290mdn5kqihqs1ymk5m") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0-rc.1 (c (n "re_smart_channel") (v "0.16.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1ib3n11ab3rcxlzb3yry3zkagyn29pvk01y5phdyi6qxy9d4k8bc") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0-rc.2 (c (n "re_smart_channel") (v "0.16.0-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-rc.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "03ccanxl2drjfavwi58ihw39i3bwn5xiiw1yklbx6595amjksjvh") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0-rc.3 (c (n "re_smart_channel") (v "0.16.0-rc.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-rc.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "11n8aa3pawiw13c3paj7bd3yyawdyq9lr21wi7s21g0ql1q2mkms") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0-rc.4 (c (n "re_smart_channel") (v "0.16.0-rc.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.0-rc.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1356rymzj50zqh0kgpd2afqjlk3km616s8czkwcvvfqli6xh504q") (r "1.76")))

(define-public crate-re_smart_channel-0.16.0 (c (n "re_smart_channel") (v "0.16.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.16.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1nwraph1gz5mzlj1pfs9d43r9v18z6cwsy7pa4f9wjvv5r830yrq") (r "1.76")))

(define-public crate-re_smart_channel-0.17.0-alpha.2 (c (n "re_smart_channel") (v "0.17.0-alpha.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.17.0-alpha.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0qdfy41vyphpc74y64dmhkxc97xndc4whxs5wn5xp5lqzpxx02i4") (r "1.76")))

(define-public crate-re_smart_channel-0.17.0-alpha.3 (c (n "re_smart_channel") (v "0.17.0-alpha.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.17.0-alpha.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "16k45zpic0cxk9l44xkqi4gwb71s94j56a03a9ba9q67cj5grc8z") (r "1.76")))

(define-public crate-re_smart_channel-0.16.1-rc.1 (c (n "re_smart_channel") (v "0.16.1-rc.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.1-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "1p51glbdlw6l7bw1f2hjdhsl1852g9p5qw5za4fvh4xfscab0wph") (r "1.76")))

(define-public crate-re_smart_channel-0.16.1-rc.2 (c (n "re_smart_channel") (v "0.16.1-rc.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "=0.16.1-rc.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "0a8vm9hyxpg5w6q1km4bs1nkxdin1afnd3r6bkqljapzgf42dgg8") (r "1.76")))

(define-public crate-re_smart_channel-0.16.1 (c (n "re_smart_channel") (v "0.16.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "re_tracing") (r "^0.16.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web-time") (r "^0.2.0") (d #t) (k 0)))) (h "10bm1z16m576f78fsch4r39r1yg1xchdpwax0qv926k7x99f0a8d") (r "1.76")))

