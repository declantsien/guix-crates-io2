(define-module (crates-io re pe repertory) #:use-module (crates-io))

(define-public crate-repertory-0.1.0 (c (n "repertory") (v "0.1.0") (h "1821iiz557asjm4qf6zwsnndaxywvn27fdal2m4zxhh8f88c0jf5") (y #t)))

(define-public crate-repertory-0.0.0 (c (n "repertory") (v "0.0.0") (h "03nbpb9jsi7v5ar41cilg4pilyibd2ghhv6i5paajw0zbn7qx2qa")))

