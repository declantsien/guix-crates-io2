(define-module (crates-io re pe repeat_finite) #:use-module (crates-io))

(define-public crate-repeat_finite-0.1.0 (c (n "repeat_finite") (v "0.1.0") (h "1qxx02b9k4kv1j1mpc6zqc62zkbd2z9x9xy0pv9sfv443wcqjgz4") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-repeat_finite-0.1.1 (c (n "repeat_finite") (v "0.1.1") (h "0c50j6n0zsfpl94161my4hgjcqxf3x8a7fqfhfwz75p4z0p21f6c") (f (quote (("trusted_len") ("default"))))))

