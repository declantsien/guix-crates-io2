(define-module (crates-io re pe repeat-command) #:use-module (crates-io))

(define-public crate-repeat-command-0.1.0 (c (n "repeat-command") (v "0.1.0") (h "0p10axx2cfq6w7ikv7sg48qi8lb443f15pgaidjc3nla22nb0kkx")))

(define-public crate-repeat-command-0.1.1 (c (n "repeat-command") (v "0.1.1") (h "14dvi7zmk5c6nnrc90w0a6g5gbr660vjdhm658864v36y6rqcrli")))

