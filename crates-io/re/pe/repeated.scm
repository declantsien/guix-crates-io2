(define-module (crates-io re pe repeated) #:use-module (crates-io))

(define-public crate-repeated-0.1.0 (c (n "repeated") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10zabn773yfyni8dr5c292wjxm6z1p4kvjavnnx1sx876a806jfa")))

(define-public crate-repeated-0.1.1 (c (n "repeated") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kkv1c48kgk6jgps0qfinvsgka8k9mgl45vm2v6d5lvjkg0m5s09")))

(define-public crate-repeated-0.1.2 (c (n "repeated") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16cis8d7nwr080k4jj88vypzqazrl054wvhsz86sd9pywk35x5v2")))

