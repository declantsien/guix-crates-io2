(define-module (crates-io re pe repeated-assert) #:use-module (crates-io))

(define-public crate-repeated-assert-0.1.0 (c (n "repeated-assert") (v "0.1.0") (h "12kvdm9qsvmng25phmdq6jf08wjmnipjiibbi0nmd6g9a06jby7v")))

(define-public crate-repeated-assert-0.1.1 (c (n "repeated-assert") (v "0.1.1") (h "0vk70niyrnidqqrzf8fqfag32spp3qap0f6pmr4b02nwlrpd6xp4")))

(define-public crate-repeated-assert-0.1.2 (c (n "repeated-assert") (v "0.1.2") (h "1yc6x4v2r6lnqfnwr4nkjdkyj8bjym5f71cyw50l62bjpiwkfp8q")))

(define-public crate-repeated-assert-0.1.3 (c (n "repeated-assert") (v "0.1.3") (h "0fdqq4ir6hb4hmhl5acji2q3cv7qnz8l2krn9yn8dd2bfgfg2zsi")))

(define-public crate-repeated-assert-0.2.0 (c (n "repeated-assert") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0r9dbrh4710mqxz7rjlrp6is1d5kzzmrwinksqgj1if7j446ygz7")))

(define-public crate-repeated-assert-0.3.0 (c (n "repeated-assert") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0znr3dc73a8zfqgcayny8mdx2fc98l1l06dd6wfcmwl2nh86kwmb") (f (quote (("async" "futures" "tokio"))))))

(define-public crate-repeated-assert-0.4.0 (c (n "repeated-assert") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1kpfqwslrq6syxlwy2k743axwfym683s6sm2fz72xykkd6cpx3a7") (f (quote (("async" "futures" "tokio"))))))

