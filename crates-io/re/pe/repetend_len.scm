(define-module (crates-io re pe repetend_len) #:use-module (crates-io))

(define-public crate-repetend_len-0.1.0 (c (n "repetend_len") (v "0.1.0") (d (list (d (n "gcd") (r "^2.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1l9ww24hblvqj5v0bajw2gmvyq207s83avbm6a7m1mbpwgqalbxb") (y #t)))

(define-public crate-repetend_len-0.2.0 (c (n "repetend_len") (v "0.2.0") (d (list (d (n "gcd") (r "^2.3.0") (d #t) (k 0)))) (h "1sms3lc46srajm5p2mqr98l10xi80qzxahgzzyqsssqdvfimgfcs")))

