(define-module (crates-io re sp response-time-analysis) #:use-module (crates-io))

(define-public crate-response-time-analysis-0.3.0 (c (n "response-time-analysis") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "auto_impl") (r "^1.0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0f56205clgr2nk82zs8s7hyqnsn13iq8lj8mdl7znjpsa11fgj5w")))

(define-public crate-response-time-analysis-0.3.1 (c (n "response-time-analysis") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "auto_impl") (r "^1.0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1pcngz0qv0w4x6zgjza4q6p4cqmc19nh7yc6nd41r1cy1pl32vlv")))

(define-public crate-response-time-analysis-0.3.2 (c (n "response-time-analysis") (v "0.3.2") (d (list (d (n "auto_impl") (r "^1.0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1h3f1qsbrxgijy8amz36h025d7bfwb4qdv5ajf6w8pzcgcpyn0gd")))

