(define-module (crates-io re sp resp_brage) #:use-module (crates-io))

(define-public crate-resp_brage-0.1.0 (c (n "resp_brage") (v "0.1.0") (h "0xkay9r3fc0ni8180v8v2yapd4l1sq9i32c6c1hv8sf638nlfbi7")))

(define-public crate-resp_brage-0.1.1 (c (n "resp_brage") (v "0.1.1") (h "0kz99xr2dglzm5ajkz5axn4kn1fzzijvvaykgv91nvsrlkyw61y9")))

(define-public crate-resp_brage-0.1.3 (c (n "resp_brage") (v "0.1.3") (h "01q9q7m41s0wyfs0lb2bx1xbmmkrlq0q9lpx2lk22ywcrgyyx2nd")))

(define-public crate-resp_brage-0.1.4 (c (n "resp_brage") (v "0.1.4") (h "0khbi8llvhrmjrdjbr8xm22jkpp4cz9nh1y6a314fcpkqlv25d2n")))

(define-public crate-resp_brage-0.1.5 (c (n "resp_brage") (v "0.1.5") (h "08x3ip4b3iivwryfwmnzw7jm54n3h80pjr2l55w0bzvij91bzc1f")))

