(define-module (crates-io re sp respk) #:use-module (crates-io))

(define-public crate-respk-0.1.0 (c (n "respk") (v "0.1.0") (d (list (d (n "lz4") (r "^1.21") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "116amiar764jmi7dcdv8c3wjzi8yjn9y7mggqwncvrv044qvyvy6")))

(define-public crate-respk-0.1.1 (c (n "respk") (v "0.1.1") (d (list (d (n "lz4") (r "^1.21") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "0wj279p84q2fldsf25sdj7qmpfjkymwg8jm7p3qpqids81c3swlx")))

(define-public crate-respk-0.1.2 (c (n "respk") (v "0.1.2") (d (list (d (n "lz4") (r "^1.21") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "1vhyyxlz2vz279vjjlc9kpd8bwnmhk1cs36ja2j12vr1pj759ag1")))

