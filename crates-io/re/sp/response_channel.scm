(define-module (crates-io re sp response_channel) #:use-module (crates-io))

(define-public crate-response_channel-0.1.0 (c (n "response_channel") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "07q7bw6ksxlkqvvb5cjc711bp244w0b9gaspcvpdnbc36hzil1ls")))

