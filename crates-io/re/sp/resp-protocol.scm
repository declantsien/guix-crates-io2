(define-module (crates-io re sp resp-protocol) #:use-module (crates-io))

(define-public crate-resp-protocol-0.0.1 (c (n "resp-protocol") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1m8zly83qaq5wda2anr4ca2l7fndxmms4iaqy2gqrjbkv0qmwplk") (y #t)))

(define-public crate-resp-protocol-0.0.2 (c (n "resp-protocol") (v "0.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1m9gya7wp6ys6n9ss2vkgac4i6i1a2jpr2v3xicyzjlqy1rdfm8p") (y #t)))

(define-public crate-resp-protocol-0.0.3 (c (n "resp-protocol") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "06gy5slllq4mr3kx57xj3kg827azzdrg20cawrjfxgrxm8c291p8") (y #t)))

(define-public crate-resp-protocol-0.0.4 (c (n "resp-protocol") (v "0.0.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "19qjvi0x4r19f4rlznnrl10vnrjncv9ff4klypz7pl5njqsrqmn7") (y #t)))

(define-public crate-resp-protocol-0.0.5 (c (n "resp-protocol") (v "0.0.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "09w14rgmq28r7wd8y7vkrjrcr1xylcwb0q8rvzga8ngip9g35mxx") (y #t)))

(define-public crate-resp-protocol-0.0.6 (c (n "resp-protocol") (v "0.0.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1wnwdym58cwlvdw8ak206w6sv5fcb7n6gmdqw6wzlcjpm3vq96gw") (y #t)))

(define-public crate-resp-protocol-0.0.7 (c (n "resp-protocol") (v "0.0.7") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0mv66dxmkyiwmnmnyj97nd2d08g5va4ym73ag04iyg76ipd3sinv") (y #t)))

(define-public crate-resp-protocol-0.0.8 (c (n "resp-protocol") (v "0.0.8") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0slm0i8lpqrkjnkxn4x04favpdpjs9a0sj7x37kdl0wlw29i1vq1") (y #t)))

(define-public crate-resp-protocol-0.0.9 (c (n "resp-protocol") (v "0.0.9") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0zpbph6xz3m2ivmi0azf71pwih79vi14xw4x3pqqn80va8wi2w59") (y #t)))

(define-public crate-resp-protocol-0.0.10 (c (n "resp-protocol") (v "0.0.10") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "17y27dyv7f5ak3gh49vrjm7szzigg1pwa8fk83kp9nj6v0kzy1c9")))

