(define-module (crates-io re sp resp) #:use-module (crates-io))

(define-public crate-resp-0.0.1 (c (n "resp") (v "0.0.1") (h "1g053p3dnmrdsmwcahni2alrx4x7b2dq1dfb7xlvw766lffbzz83")))

(define-public crate-resp-0.1.0 (c (n "resp") (v "0.1.0") (h "1h36w3p4l1kvxxdqv642qgpg8xjwbcbq7pqgjhcaj17wx8vbhy65")))

(define-public crate-resp-0.2.0 (c (n "resp") (v "0.2.0") (h "0jz27fjdqy87cs4k2llknf2wwn261p6wzd64w2l1xwpds23lxpy0")))

(define-public crate-resp-0.2.1 (c (n "resp") (v "0.2.1") (h "10588pa81bm2iqig7vrk2h4637c34f1kj1vkjzwdi2rflkcvgq50")))

(define-public crate-resp-0.2.2 (c (n "resp") (v "0.2.2") (h "1qq1sa8065j9li0rf0sgawnj3f9czf252d4znj024j35q0rhd12x")))

(define-public crate-resp-0.2.3 (c (n "resp") (v "0.2.3") (h "1i51kfb9gimfqn3m95a9w5v4m07m20q0y6acvnp2ak36hp3b8fbk")))

(define-public crate-resp-0.2.4 (c (n "resp") (v "0.2.4") (h "0lr3fj8a296mw9w3d7d8lw4f6a99rljis5j58rnaknjy276lk79h")))

(define-public crate-resp-0.2.5 (c (n "resp") (v "0.2.5") (h "044c8rsbnhhbvixzmgg6rlfj81i5iv9cv956znmgiyskq7pgpvlb")))

(define-public crate-resp-0.2.6 (c (n "resp") (v "0.2.6") (h "19qs9yxi34i1vqsqm7d0yhd12kzssmf7v3rgbvk6v5i4pxnj4qmk")))

(define-public crate-resp-0.3.0 (c (n "resp") (v "0.3.0") (h "103hs3kpnn46kclia6sn7hzw4mkrds8d0s461q40mba73isl2rg9")))

(define-public crate-resp-0.3.2 (c (n "resp") (v "0.3.2") (h "14zfmnsbp9i1pl3l8z5y71yd8xf8l2rjjfx7w5wq63yyyf9kai32")))

(define-public crate-resp-0.3.3 (c (n "resp") (v "0.3.3") (h "1xjvqyp7sharrch1b4v829jpq9943fy9b4l6hcs2bmvd9p28m2bm")))

(define-public crate-resp-0.3.4 (c (n "resp") (v "0.3.4") (h "13a678jbzqvl7flsqxs4gwr822x2sxa4p930adzkkim6zl2p90dd")))

(define-public crate-resp-0.3.5 (c (n "resp") (v "0.3.5") (h "175rv3dcva57n2g087gwhnm6cxfxb24hix1pavjc6hfsxw0aw1db")))

(define-public crate-resp-0.3.6 (c (n "resp") (v "0.3.6") (h "0wwvc7fw98a5gvkggywh3nj5h2q9swijrfjcv9jzzn6cdl319pjr")))

(define-public crate-resp-0.3.7 (c (n "resp") (v "0.3.7") (h "0wn1fw4h9f4zrlwv81dr86vcr76x66rgqpkhg241n1y63xra12j8")))

(define-public crate-resp-0.4.0 (c (n "resp") (v "0.4.0") (h "1bnhgnq5kryn6irinnx1az2dlx8v4vzqr68iainhj7rla94q832n")))

(define-public crate-resp-1.0.0 (c (n "resp") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "15vy225zx5wkj7mrjlrfgb2ybibabn62pr0ir7aqkhhfznwxjr1x")))

(define-public crate-resp-1.0.1 (c (n "resp") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07hq7d2zx69m5mgcsxgj61aw8iqkfa148frmgzn22s16kjchngr4")))

(define-public crate-resp-1.0.2 (c (n "resp") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1sl8kns1743pngaaxbzlilj3mf500qaxrr4ymj2d0qmmiqv5rsbw")))

(define-public crate-resp-1.0.3 (c (n "resp") (v "1.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1apnwb3sq00b57b1dmhm1x9jckjy0pc1i4bsa8ibc61c7hans9rx")))

