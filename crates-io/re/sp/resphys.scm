(define-module (crates-io re sp resphys) #:use-module (crates-io))

(define-public crate-resphys-0.1.0 (c (n "resphys") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "glam") (r "^0.9.4") (f (quote ("mint"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "macroquad") (r "^0.2.9") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 2)))) (h "13h19fd55gxc742cxn5bvy6sk5p1m0vy28l2bgm3av5ralqf82d8")))

