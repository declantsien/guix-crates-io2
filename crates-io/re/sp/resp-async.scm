(define-module (crates-io re sp resp-async) #:use-module (crates-io))

(define-public crate-resp-async-0.0.0 (c (n "resp-async") (v "0.0.0") (d (list (d (n "clippy") (r "^0.0.194") (o #t) (d #t) (k 0)))) (h "09rdzm31g1xkqwsd43wqbfp2i9mfjl3vs1framdw0y1220zh2x0y")))

(define-public crate-resp-async-0.0.1 (c (n "resp-async") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0461b45ylp9dn29y5hivwbkcd1j8jp7yyc1byv6p0glqdj1aclds")))

(define-public crate-resp-async-0.0.2 (c (n "resp-async") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1sih5m72ajc3wxvmwfbxy4gppmssha4c0l3cb5k656vl03imshzz")))

(define-public crate-resp-async-0.0.3 (c (n "resp-async") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1cvs5fxcsw4y287p3i5fj9p77xf6sslab5s7g2y6yc3pyjba2a9i")))

(define-public crate-resp-async-0.0.4 (c (n "resp-async") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1n1lzv6b78mc0rjmznjzna9jawrsx6bcq2wgv87b7vcc5mk3a8y7")))

(define-public crate-resp-async-0.0.5 (c (n "resp-async") (v "0.0.5") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nw5x8ddb1adb1rbrf6khhhkdxmay8fnia31s5wk665rl0mzadl9")))

(define-public crate-resp-async-0.0.6 (c (n "resp-async") (v "0.0.6") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "19yqj4j3zhr4idjba4lcvb9bxij3w6vcmimyvkdi99gr4kck855i")))

