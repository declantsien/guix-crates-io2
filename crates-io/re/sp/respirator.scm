(define-module (crates-io re sp respirator) #:use-module (crates-io))

(define-public crate-respirator-0.1.0 (c (n "respirator") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1y1ql530644qay8ykgpk084kvvq6wmcid7qyil6bnf6zv0as064b")))

(define-public crate-respirator-0.1.1 (c (n "respirator") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "14yar2gg6m4f5bdsx0nni38jk3i9mnj14r80clij34i885pqngjr")))

