(define-module (crates-io re sp respector) #:use-module (crates-io))

(define-public crate-respector-0.1.0 (c (n "respector") (v "0.1.0") (h "1jc2zyc9ly4s0x9rxcibn1h3khv00gs8nrb4syvg16ahivmzpkc8")))

(define-public crate-respector-0.1.1 (c (n "respector") (v "0.1.1") (h "0c4p3hjd7zl6qq51iiprxjja8czp2wr055af9byrl9kjh1jr2slg")))

(define-public crate-respector-0.1.2 (c (n "respector") (v "0.1.2") (h "0xsbaqncsfzs7111xyxxipms498qi0ygi9ykjgpa950agsl8qm1a")))

