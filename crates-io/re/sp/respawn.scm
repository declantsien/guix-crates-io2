(define-module (crates-io re sp respawn) #:use-module (crates-io))

(define-public crate-respawn-0.1.0 (c (n "respawn") (v "0.1.0") (d (list (d (n "quake-util") (r "^0.1.1") (d #t) (k 0)))) (h "1azcw8ds7gzm9fpib7gdi3i0fgwwwavf20cjvsrycysspnd18k91")))

(define-public crate-respawn-0.1.1 (c (n "respawn") (v "0.1.1") (d (list (d (n "quake-util") (r "^0.1.1") (d #t) (k 0)))) (h "0nxq00ch1avzb5m98c7iszh82g7v76x9pi4rc5apmiqx1nv53n9m")))

(define-public crate-respawn-0.1.2 (c (n "respawn") (v "0.1.2") (d (list (d (n "quake-util") (r "^0.1.1") (d #t) (k 0)))) (h "140723agfyqlwl85rs68ybzfih9n7vmgkhb16i78yg2vx17fs8s5")))

