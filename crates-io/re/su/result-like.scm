(define-module (crates-io re su result-like) #:use-module (crates-io))

(define-public crate-result-like-0.1.0 (c (n "result-like") (v "0.1.0") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 0)))) (h "15h1sfrcs0xgq1ymi85rpf6nyfac29x49h7wzsdvcnay62jsfi2k")))

(define-public crate-result-like-0.2.0 (c (n "result-like") (v "0.2.0") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 0)))) (h "0jgxi0ggna7x2ggd5n3i47750db2nxz7j5h0qbb3jb4xrdprzjhn")))

(define-public crate-result-like-0.2.1 (c (n "result-like") (v "0.2.1") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 0)))) (h "16zb9qzi0hsrx8k5a5nzi9knq3js8xdxsqpmih19j3m47mdlqsk5")))

(define-public crate-result-like-0.3.0 (c (n "result-like") (v "0.3.0") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 0)))) (h "0bcgcrfn82aqkff1702k18cbdsvrjh0wgbxfqwi5icy7j4p1aghd")))

(define-public crate-result-like-0.4.0 (c (n "result-like") (v "0.4.0") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0w8qr7c07y1saki9dj1722i56wk553fbxjl5m6cwjif1d3979mn2")))

(define-public crate-result-like-0.4.1 (c (n "result-like") (v "0.4.1") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.1") (d #t) (k 0)))) (h "1dn6j1jyfyy2pcmlm7zzipxnpr9vl9i6n3zw213ayvkxsnak9ay5")))

(define-public crate-result-like-0.4.2 (c (n "result-like") (v "0.4.2") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.2") (d #t) (k 0)))) (h "0halw8i62c9gv89a1hqhlwqzv3p62mbpdfr40400a1hl5r6307rf")))

(define-public crate-result-like-0.4.3 (c (n "result-like") (v "0.4.3") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.3") (d #t) (k 0)))) (h "0g074wl3vs77zxjdxkk15ckql2nxcmhfh42v7splwf7sx5yr4pgr")))

(define-public crate-result-like-0.4.4 (c (n "result-like") (v "0.4.4") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.4") (d #t) (k 0)))) (h "1vdja7h018g1a8pfxi7b4fhr7i30g8gkrqhm8sl6v9iswvis4591")))

(define-public crate-result-like-0.4.5 (c (n "result-like") (v "0.4.5") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.5") (d #t) (k 0)))) (h "0a2486vhzvidv17fhv4qndxihykrdcr5h1g27f8rcnkrjq1gx03v")))

(define-public crate-result-like-0.4.6 (c (n "result-like") (v "0.4.6") (d (list (d (n "is-macro") (r "^0.1") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.4.6") (d #t) (k 0)))) (h "047qjh9d78hrxhyvkvvmd1lvnk10rmw5bs1hg98rhf636mjcxiyc")))

(define-public crate-result-like-0.5.0 (c (n "result-like") (v "0.5.0") (d (list (d (n "is-macro") (r "^0.3") (d #t) (k 2)) (d (n "result-like-derive") (r "^0.5.0") (d #t) (k 0)))) (h "1px9d3n4mfpk5ycg95qnsmi7a9khcn16rgr6bimhazbaxwpigxxb")))

