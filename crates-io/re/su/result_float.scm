(define-module (crates-io re su result_float) #:use-module (crates-io))

(define-public crate-result_float-0.1.0 (c (n "result_float") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.66") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.66") (d #t) (k 2)))) (h "1ip23cbn5vcl9wh0l1p01xaqzqnbi0zmh4jw48nj00qqc4f51556") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-result_float-0.2.0 (c (n "result_float") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.66") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.66") (d #t) (k 2)))) (h "1igb8zqrh90v18qyrya44kh4i8n6lr58krv6n9f7fvf9l8ciswas") (f (quote (("std" "num-traits/std") ("default" "std"))))))

