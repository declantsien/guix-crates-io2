(define-module (crates-io re su result-extensions) #:use-module (crates-io))

(define-public crate-result-extensions-1.0.0 (c (n "result-extensions") (v "1.0.0") (h "1jg55dgqvqvgcgpkpwlmkzq97v6mhhk6v9k6pcyh9vahh54nrn7h") (f (quote (("library"))))))

(define-public crate-result-extensions-1.0.1 (c (n "result-extensions") (v "1.0.1") (h "01rlj9is308wnajlc75zl66d23kn4rgafw9bxwy95wnj5gfcb0mh") (f (quote (("library"))))))

(define-public crate-result-extensions-1.0.2 (c (n "result-extensions") (v "1.0.2") (h "0szx3i1zpdal7m0lm3nrlpiig7df12ym52prni1kzqvxlwknk8g6") (f (quote (("library"))))))

