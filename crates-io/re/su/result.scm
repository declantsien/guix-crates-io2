(define-module (crates-io re su result) #:use-module (crates-io))

(define-public crate-result-0.0.1 (c (n "result") (v "0.0.1") (h "0b30xrxig3qxk8rqcna8acx310clcs17x6n63cmvzyhlsmz8zhlp")))

(define-public crate-result-1.0.0 (c (n "result") (v "1.0.0") (h "0q2mslk9mvpdrl5zr1yvlb8ikmynpq5786c8ybn1wpa03rcqwk8r")))

