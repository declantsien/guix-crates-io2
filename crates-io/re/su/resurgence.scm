(define-module (crates-io re su resurgence) #:use-module (crates-io))

(define-public crate-resurgence-0.1.0 (c (n "resurgence") (v "0.1.0") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "186lgcivdvq1vn951bg74ids7gsslcih2knz9xbgirdfv5gdricc")))

(define-public crate-resurgence-0.1.1 (c (n "resurgence") (v "0.1.1") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "1h8cjsw0h7fvpr24q1a0xa9alc4dzs2qf5f6gpqj11942rhpk1mg")))

(define-public crate-resurgence-0.1.2 (c (n "resurgence") (v "0.1.2") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "0y1y0y6bfd9dasgffcmijgc7lag0nl59gs3bzqmdh7w1y86wpb0v")))

(define-public crate-resurgence-0.1.3 (c (n "resurgence") (v "0.1.3") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "1n52mi59im86k08djwwqgfadb6sva07ky4dxamhfg43hp13vy51q")))

(define-public crate-resurgence-0.1.4 (c (n "resurgence") (v "0.1.4") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "0knwn4hmi4l4j3j3pz3fr3bxnnxjbgc85abk56vs1vy721xg2w3a")))

(define-public crate-resurgence-0.1.5 (c (n "resurgence") (v "0.1.5") (d (list (d (n "smartstring") (r "^1.0.1") (f (quote ("proptest" "serde"))) (d #t) (k 0)))) (h "0lac56zp3c36lma0b9gxkifbvlpdyhya8skkp6mwymp80yslpw44")))

(define-public crate-resurgence-0.1.6 (c (n "resurgence") (v "0.1.6") (h "0539dzrdxkrzv1zfsv8fwlcvc0wnpysyjazhjmyzgvliphfi8ijl")))

(define-public crate-resurgence-0.1.7 (c (n "resurgence") (v "0.1.7") (h "0dv54pmcxrpcc5c3gippmhk14jpfn3372pivvqppx46cbq4papgz")))

(define-public crate-resurgence-0.1.8 (c (n "resurgence") (v "0.1.8") (h "1jsvdx5zg8nd5izrfiqxfclkr0c8jy26i2rgff20hqndivlpbcvb")))

