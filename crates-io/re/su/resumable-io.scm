(define-module (crates-io re su resumable-io) #:use-module (crates-io))

(define-public crate-resumable-io-0.0.1 (c (n "resumable-io") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("sync" "time"))) (k 0)))) (h "0qm05w4kcy9hyzz8kzll27lq1r78ys6kdypl164kxzynmvmy5qlg")))

