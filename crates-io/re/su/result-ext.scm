(define-module (crates-io re su result-ext) #:use-module (crates-io))

(define-public crate-result-ext-0.1.0 (c (n "result-ext") (v "0.1.0") (h "0989y4n7xzhxw6h5s7gh1hdfpwy8cxxxpizcq0ji3db0wlgyik43")))

(define-public crate-result-ext-0.2.0 (c (n "result-ext") (v "0.2.0") (h "1hnw75m67m5kvr39g9iakdjbz191y1fbjkiv3vkxb59bgqla89ak")))

