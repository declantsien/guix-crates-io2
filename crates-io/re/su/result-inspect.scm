(define-module (crates-io re su result-inspect) #:use-module (crates-io))

(define-public crate-result-inspect-0.1.0 (c (n "result-inspect") (v "0.1.0") (h "11kgw2zrdnh59wp1m7z95i7wcbp6vc2czlb27k23pf854xjv893n")))

(define-public crate-result-inspect-0.2.0 (c (n "result-inspect") (v "0.2.0") (h "1dq6xlmbr7cbz178r1fv2fwq3df0nq01ksfqk6l33n04rkg926h2")))

(define-public crate-result-inspect-0.3.0 (c (n "result-inspect") (v "0.3.0") (h "0q5iinq0k1dryxqzfyzn4qwmjbj11agrgvazqfp2j87sxkw51adp")))

