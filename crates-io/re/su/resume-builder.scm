(define-module (crates-io re su resume-builder) #:use-module (crates-io))

(define-public crate-resume-builder-0.1.0 (c (n "resume-builder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "soup") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio-rayon") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1c3yz0af4a48n3rlwpzh6ar16zp2af7aqymhxfas88fhpghpq78g")))

