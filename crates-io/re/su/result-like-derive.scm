(define-module (crates-io re su result-like-derive) #:use-module (crates-io))

(define-public crate-result-like-derive-0.4.0 (c (n "result-like-derive") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g8bz7f4rrhr248z369p4y3y2xwy3y9m5rjxwymw4hyh6gj3h39z")))

(define-public crate-result-like-derive-0.4.1 (c (n "result-like-derive") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.3.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nms70nmbmw49d8n93qh4invrmnlnpaam0qjra7p4zqmr1lzgynw")))

(define-public crate-result-like-derive-0.4.2 (c (n "result-like-derive") (v "0.4.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.3.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1alnx9nsmmk3v6bpbcgwf6hgkazxi7bkn51nazhjmxzqdi3bpsx0")))

(define-public crate-result-like-derive-0.4.3 (c (n "result-like-derive") (v "0.4.3") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1rmrm2qwgq04hrcpykan1w9mjl5jhc6v8q2j93nb990l19ar3b3d")))

(define-public crate-result-like-derive-0.4.4 (c (n "result-like-derive") (v "0.4.4") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "019kfzpk27b94pbyir4jiqm4chn4f0il6lhhzcsl9fa216i5ynv6")))

(define-public crate-result-like-derive-0.4.5 (c (n "result-like-derive") (v "0.4.5") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1h9mgdr264r71n50qc0hy8r6j38ya0xjd6dqp3fg2fbqmjjcha9a")))

(define-public crate-result-like-derive-0.4.6 (c (n "result-like-derive") (v "0.4.6") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-ext") (r "^0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0hlgg3q79wsdmhcgalvknpf7m8x8l54gcj8dqml1qwagwnig1aqz")))

(define-public crate-result-like-derive-0.5.0 (c (n "result-like-derive") (v "0.5.0") (d (list (d (n "pmutil") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r8g5pi0lgvbm4q5pdcgajdbrvcgsvjq3ingf1ixd5780965gmm8")))

