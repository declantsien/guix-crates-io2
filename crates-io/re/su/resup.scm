(define-module (crates-io re su resup) #:use-module (crates-io))

(define-public crate-resup-0.2.0 (c (n "resup") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.21") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "01sq6ll4qn2ifgx1jsj12v0is60bm875kgz9n76may8w7mbg6ng4")))

(define-public crate-resup-0.3.0 (c (n "resup") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1388h08c7cifaj70q5xyib8lms10pb23m74izzdl26q44ylz9jad")))

