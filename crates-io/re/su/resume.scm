(define-module (crates-io re su resume) #:use-module (crates-io))

(define-public crate-resume-0.1.0 (c (n "resume") (v "0.1.0") (h "1mrqacsn6bdvpjdlakch3zn776s0fk331rmfddz1ppgihc15mhl7")))

(define-public crate-resume-0.1.0-1 (c (n "resume") (v "0.1.0-1") (h "1apdbnhlwqg5nkjc08ig1x6sh11izbm8np3qxywwgddfkqklg9xm")))

(define-public crate-resume-0.1.0-2 (c (n "resume") (v "0.1.0-2") (h "0jjr2g1k7z01qwgk8dv6g9fb9pjaxpda3a9bg050xl25bbh6r84q")))

(define-public crate-resume-0.2.0 (c (n "resume") (v "0.2.0") (h "0j6079llzwh42kknnbsljq3ahdy1kfb9i68qjyz6npwllf01cnji")))

(define-public crate-resume-0.2.1 (c (n "resume") (v "0.2.1") (h "0mmj1a65wh767xsvxvj3s09vn38z0czvni5nklv7wq9kv2yzpl1h")))

(define-public crate-resume-0.2.2 (c (n "resume") (v "0.2.2") (h "0sfyp14wf327aza1cfdanvx7phc178bmy903dz2ka7p33r269r3i")))

(define-public crate-resume-0.2.3 (c (n "resume") (v "0.2.3") (h "1jv47nydy1cawj1r2n5gkwbkyb0h7xd1dfq6qxp6gqf68mwv905x")))

(define-public crate-resume-0.2.4 (c (n "resume") (v "0.2.4") (h "03rgqwsagw63snzckfbp9mvhxf178pcjbm9zxymsv60x4g20gpcf")))

(define-public crate-resume-0.2.5 (c (n "resume") (v "0.2.5") (h "1wmc9ma4w1cc94vxccgf6hq2iwly521yp6v3lgaj66kgrp7x0jsz")))

(define-public crate-resume-0.2.6 (c (n "resume") (v "0.2.6") (h "0x32cycwibg148ikkx6nq5ff8lqr9q8kbb44s60syszf1y6w9sar")))

(define-public crate-resume-0.2.7 (c (n "resume") (v "0.2.7") (h "176y74l1l0ksjg2mv5gw698k91pyls6lyi2fybn4byfc1gwg2adc")))

(define-public crate-resume-0.2.8 (c (n "resume") (v "0.2.8") (h "0x2bp0q6w2njm4y6rqx200q9pqhpa4ah6ds7aqx9c4nggi2yr2ji")))

(define-public crate-resume-0.2.9 (c (n "resume") (v "0.2.9") (h "132fccivp56ixwfb5mg85r4vc7ah8mab5zhm55ms6cg8ml5jlfl3")))

(define-public crate-resume-0.2.10 (c (n "resume") (v "0.2.10") (h "1v5x24d4wjnphkrzqm792m3w1qpxr00v9px1i3xw6szl9zqldaax")))

(define-public crate-resume-0.2.11 (c (n "resume") (v "0.2.11") (h "0v0rfdwmhjk0lsb097d1di1ayyl8nfyxnwz3zh4a8pm486rv9vvn")))

(define-public crate-resume-0.2.12 (c (n "resume") (v "0.2.12") (h "0ysq6smfphxq0v34jfi6icrrmwdnd46qs6j9b8bsvgx35sirnfv8")))

(define-public crate-resume-0.2.13 (c (n "resume") (v "0.2.13") (h "0fb78ms86ibjs9i1jr6fblrxmkbqi860nzk0fiw156fcmycwqhgz")))

(define-public crate-resume-0.2.14 (c (n "resume") (v "0.2.14") (h "073gppfgxsvzjkh9j5jp7l81ys21gg8k79dp778kmyc4kxck0wwj")))

(define-public crate-resume-0.2.15 (c (n "resume") (v "0.2.15") (h "1jxkwpjgm18mkarg9915dadkm2ayqqzayvncby4a34h3qbv7iqka")))

(define-public crate-resume-0.2.16 (c (n "resume") (v "0.2.16") (h "1d7nxxw0xwxmrrsmnf5smq6cv16rkk111nmfdy6phjpc2fkwghrd")))

(define-public crate-resume-0.2.17 (c (n "resume") (v "0.2.17") (h "1257paff4i20c308cm337djlhbj3whfi1cg7lpnvn9qs2d31nyjl")))

(define-public crate-resume-0.2.18 (c (n "resume") (v "0.2.18") (h "0hyjikyiwkyawai001439jy4safqkqi0020j1z89r2k906ns5c0h")))

(define-public crate-resume-0.2.19 (c (n "resume") (v "0.2.19") (h "0jf665jz02x9a0hb2gw4v8q7hpsyz5q3nkjqrsbcwj5gkz2l3nz4") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar"))))))

(define-public crate-resume-0.2.20 (c (n "resume") (v "0.2.20") (h "0jh79pi1wr3b158km78z5qkjv2dsily9rdg794mp0mplpakg6x0w") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar"))))))

(define-public crate-resume-0.2.21 (c (n "resume") (v "0.2.21") (h "10zkfvarxz2wasz6spksfpa33gq5hdkwiyiswf6fvkbk4g8slhv4") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar") ("arch"))))))

(define-public crate-resume-0.2.22 (c (n "resume") (v "0.2.22") (h "1fxdxs00byqrkv37iz9qv0nc976ynah1z3d19pqha3clnny2wfdq") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("bar") ("arch"))))))

(define-public crate-resume-0.2.23 (c (n "resume") (v "0.2.23") (h "09cd1ds0m6kchv76h0lbgfb9ly0dm0n2g2kfiyl7hz48d4rhq5s7") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.24 (c (n "resume") (v "0.2.24") (h "1pkb3zyiwm7yrig4mf8qlhj8jyfl5hdkzxhnjgl5jr6m4y731j17") (f (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.25 (c (n "resume") (v "0.2.25") (h "0lhm5dhzh10kcmza2hmivx2k1yky4gmgv5i4zvb9qkis5hczss3p") (f (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.26 (c (n "resume") (v "0.2.26") (h "18d6rbp48p9zr5ckbmsbl27ka2f7s7dygwa79v86dqm4asgww9f6") (f (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.27 (c (n "resume") (v "0.2.27") (h "07qzisbdsh63fn7ja4n30x3hpwbprh9m17xm0mawib0i3nj3cxwn") (f (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.28 (c (n "resume") (v "0.2.28") (h "1d2fiknpxga84r95zqsrk9s7pnvnkb79cwqm4chapvqk7rigmyk8") (f (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.29 (c (n "resume") (v "0.2.29") (h "00mvw2qsx6if395zs7cq96r6lhg8q8g3n7cr21v010zaik5c1asn") (f (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.30 (c (n "resume") (v "0.2.30") (h "0z20wybmdsq2hjzsdwmwbqbvwd0xar2si405mc06k914xd5adp3y") (f (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.31 (c (n "resume") (v "0.2.31") (h "0mgf7h20k5z01s0303ncp058qcxh3a8saspgwzqdj5ydmsknql23") (f (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.32 (c (n "resume") (v "0.2.32") (h "1vfaq3ilmpz6i95xhmimanssq14cishf11i4468vapzkc9axb8m6") (f (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2.33 (c (n "resume") (v "0.2.33") (h "0fw84wdkd397nn0d2h0j09hxphh26m3sjijdwrpyfc4155615dag") (f (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

