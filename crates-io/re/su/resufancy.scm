(define-module (crates-io re su resufancy) #:use-module (crates-io))

(define-public crate-resufancy-0.1.0 (c (n "resufancy") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "filesystem") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pug_tmp_workaround") (r "^0.1") (d #t) (k 0)) (d (n "wkhtmltopdf") (r "^0.3") (d #t) (k 0)))) (h "1vr7gz5yif904hr2xvkq79ywkz0bbgrnl6lv872d0rnkn62imlm8")))

(define-public crate-resufancy-0.1.1 (c (n "resufancy") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "filesystem") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pug_tmp_workaround") (r "^0.1") (d #t) (k 0)) (d (n "wkhtmltopdf") (r "^0.3") (d #t) (k 0)))) (h "1yg971d9325c03w4c6rpmar6lng6hmjrd6zbsrzzs8x232v0qcpp")))

