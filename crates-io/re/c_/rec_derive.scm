(define-module (crates-io re c_ rec_derive) #:use-module (crates-io))

(define-public crate-rec_derive-0.1.0 (c (n "rec_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "0fgpaab887rg4r2fmvsarcszrba8v5dxb9qxyi184iywh9rjwhxl")))

