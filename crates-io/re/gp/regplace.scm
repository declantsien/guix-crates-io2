(define-module (crates-io re gp regplace) #:use-module (crates-io))

(define-public crate-regplace-0.1.0 (c (n "regplace") (v "0.1.0") (d (list (d (n "globset") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "109i00w220k58144831dl2bpm95a318j20l6bjrvsdmrqsfimy7r")))

