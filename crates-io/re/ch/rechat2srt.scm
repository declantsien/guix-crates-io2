(define-module (crates-io re ch rechat2srt) #:use-module (crates-io))

(define-public crate-rechat2srt-0.1.0 (c (n "rechat2srt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "srtlib") (r "^0.1.0") (d #t) (k 0)))) (h "1cpzdfbrj9krz682j1psak71c8d713vy8grl6ahplv4rqmk0dqqx")))

