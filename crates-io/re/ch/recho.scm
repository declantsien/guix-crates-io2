(define-module (crates-io re ch recho) #:use-module (crates-io))

(define-public crate-recho-0.1.0 (c (n "recho") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "03j1z6x6ayi4dx3cmh88r6v75bz8dzxqxywkmr4gk0nbg1hd4hnv")))

(define-public crate-recho-1.0.0 (c (n "recho") (v "1.0.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "14vv53gdynpww4cj85krs8z6j5l6dfnqwhdhg1lkkbfvh42l706p")))

(define-public crate-recho-1.0.1 (c (n "recho") (v "1.0.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "1y9b6mpdhpl30rizrpiz0nyxmy3d4c4xxr90gbci8fl52x64qb0j")))

(define-public crate-recho-1.0.2 (c (n "recho") (v "1.0.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "16lph12cx79mbyf5248p4vs9mn99m7g75jia1zcg457rvffzwsdr")))

