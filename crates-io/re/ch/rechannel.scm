(define-module (crates-io re ch rechannel) #:use-module (crates-io))

(define-public crate-rechannel-0.0.2 (c (n "rechannel") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dmnvhbzbvalhcy67zzny3gmb4i1j4l1aqwnxjxsp28d86233kaa")))

(define-public crate-rechannel-0.0.3 (c (n "rechannel") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s34a98j1fh4a9q8x24snly7hhmlhbgyp2lphcslinfa1d8dq46k")))

(define-public crate-rechannel-0.0.4 (c (n "rechannel") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pzir58q75kqys1gmwvw8c7ri3fji9rcvcqr5xbsvk1fp64spzxj")))

(define-public crate-rechannel-0.0.5 (c (n "rechannel") (v "0.0.5") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "133lmyq7lwpldnqqla8i82s7swxj4j94z9fs29vmkgxdj49fimaw")))

(define-public crate-rechannel-0.0.6 (c (n "rechannel") (v "0.0.6") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gp67xkjqyvppmb4jwvq6z9g411zg2i5z1d6wi0zwkk8mdk3357g")))

(define-public crate-rechannel-0.0.7 (c (n "rechannel") (v "0.0.7") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fdnj5yflv7wmwgls64fzifhwxz155yl80p9kfmh1plhysws2y4s")))

