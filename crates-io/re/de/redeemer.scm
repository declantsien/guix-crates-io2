(define-module (crates-io re de redeemer) #:use-module (crates-io))

(define-public crate-redeemer-1.0.5 (c (n "redeemer") (v "1.0.5") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0bhaxs9ykll8l37hf4gllpq6d3c7d2hnqszr8c71gl7xychb1rss") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.0.6 (c (n "redeemer") (v "1.0.6") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "119qk89a0s4si24hb7q8wbcs9claxbjk2i5lxp98r4zkh11kvs2g") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.0.7 (c (n "redeemer") (v "1.0.7") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "08rs70fvjyiy2wk63l2wvw3485dra90wkqsgpgdpz7y7zpbrsmha") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.0.10 (c (n "redeemer") (v "1.0.10") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1451rlc7g0vqp7v3lbi7w6zkrmhv3510s60jwhfzkvx2m0s5rjpf") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.0.11 (c (n "redeemer") (v "1.0.11") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0is8f06lv9i6f0j8skzp22sshasl40bdk04x9826gkfm451ji42c") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.1.0 (c (n "redeemer") (v "1.1.0") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "16i3n8zqx8d0fkqjyfpap3jf10x6lfnbq7f7qj8ild36v2fzjr3s") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.1.1 (c (n "redeemer") (v "1.1.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "1l2z5h8j34mwc81500fmisvkwri1335fxrmxns9sdavv7clzxgc0") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-redeemer-1.1.2 (c (n "redeemer") (v "1.1.2") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "1051gj718cyq6nd75xgcmm0q8cmfqsgiif2klldkpxp3n85fnjzb") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

