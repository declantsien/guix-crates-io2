(define-module (crates-io re de rede_parser) #:use-module (crates-io))

(define-public crate-rede_parser-0.1.0 (c (n "rede_parser") (v "0.1.0") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0ly9970dz8q567rhirfsd4m8ys9vzzkrrlh8jkb60gjh6vx6awbx")))

(define-public crate-rede_parser-0.1.1 (c (n "rede_parser") (v "0.1.1") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0x4lyk4gw3zzrx7glq4zs4chyfawya26bamiwdl6k2nvif95wa7v")))

(define-public crate-rede_parser-0.1.2 (c (n "rede_parser") (v "0.1.2") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "03gdnkn4cw1k9dkrvggwafl52hs3air19nxr46dsmdbrhhxyp6db")))

(define-public crate-rede_parser-0.1.3 (c (n "rede_parser") (v "0.1.3") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1zlx0wrwrd7zpssssabasq0qmnm5kdri0h4b5hxq8p1vkjzr87fw")))

(define-public crate-rede_parser-0.1.4 (c (n "rede_parser") (v "0.1.4") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "18ycp2h0aal44l4fgil3myy42ivfdqsf9kmk42icrqldigv2b190")))

(define-public crate-rede_parser-0.2.0 (c (n "rede_parser") (v "0.2.0") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "http-serde") (r "^2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0667lrnjlvyc2lafkbxk3xzdnlaz29c454biwjw9hlgwdw124jmm") (f (quote (("input_params"))))))

