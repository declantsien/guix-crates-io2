(define-module (crates-io re ad readable_byte) #:use-module (crates-io))

(define-public crate-readable_byte-0.1.0 (c (n "readable_byte") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14j0rqxbhzjyxw6dgan8dqifcw563g8fxlwrwwhah85q9cv5ksij")))

(define-public crate-readable_byte-0.1.1 (c (n "readable_byte") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qqi1zb2dkr5vh3q7rf5pc8iix2csfn05rprw8fmsayvqv6yi85x")))

(define-public crate-readable_byte-0.1.2 (c (n "readable_byte") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z1nxq0ricdf7s0aazay092m7z9v0rv2jpg05bmbzs46ca6sz0nc")))

(define-public crate-readable_byte-0.1.3 (c (n "readable_byte") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "083k36p3ayh983jhh8dcwkhn1krpzm3haqg25qxgjv4smkah1xlv")))

(define-public crate-readable_byte-0.1.4 (c (n "readable_byte") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03gawwllxw76rpbwvx702sia4zs14cfy7cmqnbyv0v29f35r0drs")))

(define-public crate-readable_byte-0.1.5 (c (n "readable_byte") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d035z7lid62xm114vdca9nljxq0s1afcpax2cb659wrljjzqv2s")))

(define-public crate-readable_byte-0.1.6 (c (n "readable_byte") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ha92fic8qyd6cxqp0y44dp95ar6imc8zl798jp5lqkv9ixmrc98")))

(define-public crate-readable_byte-0.1.7 (c (n "readable_byte") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ak2x93kfi8d0vzbihj9dp66j1wamcmp4h6qpypckrxyw1n8cg09")))

(define-public crate-readable_byte-0.1.8 (c (n "readable_byte") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "164m396wz4vs8fwjrr7n271lsdbi56fcvq11kwlgy3fp2vh4dd6m")))

(define-public crate-readable_byte-0.1.9 (c (n "readable_byte") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fp6q626fq3fg2dml2cclcp7xpvjng78mg9gi6nph2r5n3r42rf9")))

(define-public crate-readable_byte-0.1.10 (c (n "readable_byte") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1y2dhg5md70a0ra9ziw8n55kg7za3hp2kjvxs2wchb5g7sl5jp93")))

(define-public crate-readable_byte-0.1.11 (c (n "readable_byte") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "068rzzsfi4q0bnzgn3mvs5kwkqcxv0fxjyq4k60mplnxhifxhphq")))

(define-public crate-readable_byte-0.1.12 (c (n "readable_byte") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09vq7v5pjgh174dpi3vvlj6is4mg4iik8nbs10a9yh8jgycxyq74")))

(define-public crate-readable_byte-0.1.13 (c (n "readable_byte") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s8arvx9cq42mqqa9rhspr5j3sz8jdgjk65a61qf57a1vj48cbym")))

(define-public crate-readable_byte-0.1.14 (c (n "readable_byte") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rrddpmkrpfzv8515zsmf3jb7rxff74g7cjjhl3kgcvgzans62gi")))

