(define-module (crates-io re ad read_chunks) #:use-module (crates-io))

(define-public crate-read_chunks-0.1.0 (c (n "read_chunks") (v "0.1.0") (h "1wbvvvlgsp3693q8jv1hyi0qmn136lknpbh5xdbxw0wdm31wn4ql")))

(define-public crate-read_chunks-0.1.1 (c (n "read_chunks") (v "0.1.1") (h "0h37wl32cv7bzd2lfa93xbs1iwsfswfwjxd7mbv9rm0a59iq8wbd")))

(define-public crate-read_chunks-0.2.0 (c (n "read_chunks") (v "0.2.0") (h "1gwa7k6xy7zqdbsrhn3jybcqz10nqfiw5czz7n6p2c162yx5mafx")))

