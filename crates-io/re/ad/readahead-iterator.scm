(define-module (crates-io re ad readahead-iterator) #:use-module (crates-io))

(define-public crate-readahead-iterator-0.1.0 (c (n "readahead-iterator") (v "0.1.0") (h "0g4sl0sf0wrd537gazlkshwr2y6fswsbq7kry9kdfp9kybhxxy82")))

(define-public crate-readahead-iterator-0.1.1 (c (n "readahead-iterator") (v "0.1.1") (h "137wnyy3jkq73i6frnzj0500raajs1brv510c0ldy4py69617skk")))

