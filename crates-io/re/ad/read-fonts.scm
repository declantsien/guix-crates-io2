(define-module (crates-io re ad read-fonts) #:use-module (crates-io))

(define-public crate-read-fonts-0.0.1 (c (n "read-fonts") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "font-types") (r "^0.0.1") (d #t) (k 0)))) (h "10rlwy88gfp51zn28grww2gw12cwiyg38yxbhyzb0mmll4q1nrqv") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.0.2 (c (n "read-fonts") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "font-types") (r "^0.0.2") (d #t) (k 0)))) (h "13xx5f7ynf81nxz0ajym0yh1rqxs9l76ssjip0b1llkqzq84vbx3") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.0.3 (c (n "read-fonts") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "font-types") (r "^0.0.3") (d #t) (k 0)))) (h "19jvw0xrzkwiy2jaq0slfdvmdg9idn9lgdp2l5hirij5ybzzhb9v") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal")))) (y #t)))

(define-public crate-read-fonts-0.0.4 (c (n "read-fonts") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "font-types") (r "^0.0.3") (d #t) (k 0)))) (h "046zg36x7wkaf5ylmpppmabqgp1m1xr3d6vrj534hcyfg6wkfg9f") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.0.5 (c (n "read-fonts") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "font-types") (r "^0.0.5") (d #t) (k 0)))) (h "06w6hqnsspvp34lcxa92j9w3ly2rdflk50dmv8rjjxgwxp60d3dv") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.0.6 (c (n "read-fonts") (v "0.0.6") (d (list (d (n "font-types") (r "^0.0.6") (d #t) (k 0)))) (h "17z4yb121an9mj65w01hd391djbwa52phk6r7bdzxx8yy3zr4k1z") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.1.0 (c (n "read-fonts") (v "0.1.0") (d (list (d (n "font-types") (r "^0.1.0") (d #t) (k 0)))) (h "1j333wrcjq88678fwdvc2vdx4m51l9v8yk8kfl5qm5ilql0aa49r") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.1.1 (c (n "read-fonts") (v "0.1.1") (d (list (d (n "font-types") (r "^0.1.0") (d #t) (k 0)))) (h "0241mipir8vb7070n9j7vwvmb0hvlj27fn5zr8x3j9qvx1ndxmij") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.1.2 (c (n "read-fonts") (v "0.1.2") (d (list (d (n "font-types") (r "^0.1.1") (d #t) (k 0)))) (h "08cczi9k81ajkyxw1g0lch9b6hmakhashid9l14g3g5lgai3fsyh") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.1.3 (c (n "read-fonts") (v "0.1.3") (d (list (d (n "font-types") (r "^0.1.2") (d #t) (k 0)))) (h "0l5m902kbnh63x4g274767nlnwyj628y0f96rindz7jahzhghqrj") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.1.4 (c (n "read-fonts") (v "0.1.4") (d (list (d (n "font-types") (r "^0.1.4") (d #t) (k 0)))) (h "09im89pdiggkhs2wayz2rpmcfwryn54n8rfg2gmgfmfkx19ac5la") (f (quote (("traversal" "std") ("test_data") ("std" "font-types/std") ("default" "traversal"))))))

(define-public crate-read-fonts-0.2.0 (c (n "read-fonts") (v "0.2.0") (d (list (d (n "font-types") (r "^0.1.5") (d #t) (k 0)))) (h "0jpsik59h9qbj9pslf7fbmp963dfm36jqx0j7mzxml1zbwwavkz3") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.2.1 (c (n "read-fonts") (v "0.2.1") (d (list (d (n "font-types") (r "^0.1.7") (d #t) (k 0)))) (h "1hdghyds2z4ckkdh5smgd3indml1a606cjdrlw5nbx9lkdx5nykh") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.3.0 (c (n "read-fonts") (v "0.3.0") (d (list (d (n "font-types") (r "^0.1.8") (d #t) (k 0)))) (h "1sppy2sg85i3s44hbxixlmmscd4l9fivij4c7n79g8g10zayci11") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.4.0 (c (n "read-fonts") (v "0.4.0") (d (list (d (n "font-types") (r "^0.2.0") (d #t) (k 0)))) (h "1azfz3z3wasvwx0z0zyr51bshkdsh2c8w2db1sffwrhlpcybnql6") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.5.0 (c (n "read-fonts") (v "0.5.0") (d (list (d (n "font-types") (r "^0.3.0") (d #t) (k 0)))) (h "090fjxvf1kcyzybi4va6f2jbc3jbyv2ss3g0x460ppry8xallf0x") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.6.0 (c (n "read-fonts") (v "0.6.0") (d (list (d (n "font-types") (r "^0.3.0") (d #t) (k 0)))) (h "1c3iakapqvz1xmjysr20haya6hgm6g4wjxwxkm6r202gbikyc2iw") (f (quote (("traversal" "std") ("std" "font-types/std") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.7.0 (c (n "read-fonts") (v "0.7.0") (d (list (d (n "font-types") (r "^0.3.1") (d #t) (k 0)))) (h "1s3la32rl31ggn3x2lc3illsh9gcmcd6z3g479k4a8gm4j65xp8k") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.8.0 (c (n "read-fonts") (v "0.8.0") (d (list (d (n "font-types") (r "^0.3.3") (d #t) (k 0)))) (h "1zqwnqs81sbmafwcl5w8br8aj91172xqckmikvih5dq9vkp4rnnr") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.9.0 (c (n "read-fonts") (v "0.9.0") (d (list (d (n "font-types") (r "^0.3.4") (d #t) (k 0)))) (h "1dn8j8vkk5386z527l8cvf1z6ck91g3xm9zzpm8a7w2r5d37j9rf") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.10.0 (c (n "read-fonts") (v "0.10.0") (d (list (d (n "font-types") (r "^0.3.4") (d #t) (k 0)))) (h "1synnhizabcryhyvhw1lnwiapksb4sgwsm9r1ddzjb9vcha85l47") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.11.0 (c (n "read-fonts") (v "0.11.0") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)))) (h "0hwa2f2bw02nqxgwwmfq5a37mmcl3fwismrqg04m9smxyvfjx7vc") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.11.1 (c (n "read-fonts") (v "0.11.1") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)))) (h "10f5gk8l1ffj579yd18hrggzp5vzffrc4i7placl26qp9bxa90cp") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test"))))))

(define-public crate-read-fonts-0.11.2 (c (n "read-fonts") (v "0.11.2") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17jync5dml7q3agmp8psh0i16xsh46qnn7mf5abgm7ca3srldb3l") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.12.0 (c (n "read-fonts") (v "0.12.0") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vappczqrbsqqdwj3c1c4i85v5j6p6v3xzks2bx9qhvvq38wcyim") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.13.0 (c (n "read-fonts") (v "0.13.0") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11g9w2js76f21jp1014c0d6f22ikww9x625hy1bq0vwr16rvz08j") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.13.1 (c (n "read-fonts") (v "0.13.1") (d (list (d (n "font-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m4zh3ssqpswj82qbm3g1v7r0yiykdhhic502lnf4cybkqd5hwm2") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.13.2 (c (n "read-fonts") (v "0.13.2") (d (list (d (n "font-types") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01aaq7qkwqn9vbn6gppmnhql3kv8i9gsc0bi34qrj5zz7q7acbqy") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.14.0 (c (n "read-fonts") (v "0.14.0") (d (list (d (n "font-types") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ddfxqfjwx7irpfy06p9zhb02gj82ig1hnm4adj8lwnlilimvxda") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.0 (c (n "read-fonts") (v "0.15.0") (d (list (d (n "font-types") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s7crd9nnl63m9iwy7spc03iw3vkcv1bh7np06nkymszfzm7b2xy") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.1 (c (n "read-fonts") (v "0.15.1") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vvhs30lvawswbf416yhcpk6d45pj8dxgq5lvch1y1iz04pgxi75") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.2 (c (n "read-fonts") (v "0.15.2") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ddj7issf8gi45lj74j2irgpm963lbv3854zr6hn9ybjwx9f0mbm") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.3 (c (n "read-fonts") (v "0.15.3") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l2f2zrq6ndjyyy8znm91j0v2i1a0pf95m8x0dq1p04mvf02jdm1") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.4 (c (n "read-fonts") (v "0.15.4") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r3b9qfldwp65qmdbv4i5bydw3kh4kljgyd4f4bx88zv333m40np") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.5 (c (n "read-fonts") (v "0.15.5") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11laxi1l84gyzi4ipmq3vfgnfdl7b49zq5sanc2swbiyqj4ani60") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.15.6 (c (n "read-fonts") (v "0.15.6") (d (list (d (n "font-types") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14pygyadk21mw5aw73m6d9x742268hi34d6lnqqq14sdvgp27shp") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.16.0 (c (n "read-fonts") (v "0.16.0") (d (list (d (n "font-types") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08lpz5fkjpb17w48fingnsd85ghxv5b9fm8z74596xrvimjj9ic1") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.17.0 (c (n "read-fonts") (v "0.17.0") (d (list (d (n "font-types") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cssl5zx987ap1011a67nlvhnlbiw9kl4fnqn16snbbil9mb1n4q") (f (quote (("traversal" "std") ("std" "font-types/std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.18.0 (c (n "read-fonts") (v "0.18.0") (d (list (d (n "bytemuck") (r "=1.13.1") (d #t) (k 0)) (d (n "font-types") (r "^0.5.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i5p8jaddzz6wzykafg6fc91jrvrlaz7vpb1rwap7qq5v9qlgzkj") (f (quote (("traversal" "std") ("std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.19.0 (c (n "read-fonts") (v "0.19.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "font-types") (r "^0.5.2") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dm1a918hg5dn6pr95kjij8bavgq3k1s9xry4r6l6hr80pnbaxga") (f (quote (("traversal" "std") ("std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.19.1 (c (n "read-fonts") (v "0.19.1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "font-types") (r "^0.5.3") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iv8ghcfsgv7vzzahgimrvfvnc3czki5xbm767dm7j6i5gdljixg") (f (quote (("traversal" "std") ("std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

(define-public crate-read-fonts-0.19.2 (c (n "read-fonts") (v "0.19.2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "font-types") (r "^0.5.4") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gif40678ff5dlfc7gy9lqsys80rgwyysbcs61g1kfrk461zwi1d") (f (quote (("traversal" "std") ("std") ("scaler_test") ("default" "traversal") ("codegen_test")))) (s 2) (e (quote (("serde" "dep:serde" "font-types/serde"))))))

