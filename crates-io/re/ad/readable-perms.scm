(define-module (crates-io re ad readable-perms) #:use-module (crates-io))

(define-public crate-readable-perms-0.1.0 (c (n "readable-perms") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1dd80z0p3ksxlwsp9mm6m4s50l1dq3wmmz7h6as1sj9sc01ix698") (f (quote (("speedup_hack_stable"))))))

(define-public crate-readable-perms-0.1.1 (c (n "readable-perms") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "15ir0sppp9a5n4fnf9nnkha4s6khird7i7hh0addabfg36y01dp9") (f (quote (("speedup_hack_stable") ("default" "chmod") ("chmod" "libc"))))))

(define-public crate-readable-perms-0.1.2 (c (n "readable-perms") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "07x4lhf6v6drw9kmvi2pcb7qiclq4xyzisa4k208gqqrfvr9yzh2") (f (quote (("speedup_hack_stable") ("default" "chmod") ("chmod" "libc"))))))

(define-public crate-readable-perms-0.1.3 (c (n "readable-perms") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "068c50q5yladxvgmqhmn2fww1hzp2gj7nl2x36g14kc8r3r548al") (f (quote (("speedup_hack_stable") ("default" "chmod") ("chmod" "libc"))))))

