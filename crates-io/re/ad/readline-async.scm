(define-module (crates-io re ad readline-async) #:use-module (crates-io))

(define-public crate-readline-async-0.1.0 (c (n "readline-async") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.1") (d #t) (k 2)) (d (n "crossterm") (r "^0.23") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("async-await" "std"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "01xl7hb68vb0kz5dsyqn0h6fx88cnx361zkz7wdmkqnknj6bmw3r")))

