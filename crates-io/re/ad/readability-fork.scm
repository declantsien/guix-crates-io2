(define-module (crates-io re ad readability-fork) #:use-module (crates-io))

(define-public crate-readability-fork-0.2.0 (c (n "readability-fork") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1g1i9j38p64pbgq9p9az25g2cw4s5bkpyk41x14jbyrrslgrfn09") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-fork-0.2.1 (c (n "readability-fork") (v "0.2.1") (d (list (d (n "html5ever") (r "^0.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0hiz24df2909wqczxfk94qdd66i0gvhka4kzn87apfxqv45jpv7n") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-fork-0.2.2 (c (n "readability-fork") (v "0.2.2") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "032aw7xzyafn4qjpsmr2zlq1f4bgd1pfp5ikjrxp0hrakyrir5kr") (f (quote (("default" "reqwest"))))))

