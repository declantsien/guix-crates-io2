(define-module (crates-io re ad read-write-ext) #:use-module (crates-io))

(define-public crate-read-write-ext-0.1.0 (c (n "read-write-ext") (v "0.1.0") (d (list (d (n "fixed-buffer") (r "^0.4") (d #t) (k 2)))) (h "0r7wbjyip1qasz9yxjqc8b1ww44px3mr42j2qlxjziyknmizwnmr")))

(define-public crate-read-write-ext-0.1.1 (c (n "read-write-ext") (v "0.1.1") (d (list (d (n "fixed-buffer") (r "^0.4") (d #t) (k 2)))) (h "0b0nz4g0v5anvapclxq2mbrxin2mj0g837qilqpnagqyad3jryzy")))

