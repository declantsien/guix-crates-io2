(define-module (crates-io re ad read-from) #:use-module (crates-io))

(define-public crate-read-from-0.5.0 (c (n "read-from") (v "0.5.0") (h "0ryw0g9fmpyzah4s5hjf1wiril7mxwvvly26v0jy51dc0asqi12d")))

(define-public crate-read-from-0.5.2 (c (n "read-from") (v "0.5.2") (h "0y9dpbqzjyn597zws3898byj0rpvmgag51k5cyzh09wzihyf4p5k")))

