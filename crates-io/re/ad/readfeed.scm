(define-module (crates-io re ad readfeed) #:use-module (crates-io))

(define-public crate-readfeed-0.1.0 (c (n "readfeed") (v "0.1.0") (d (list (d (n "maybe_xml") (r "^0.10.0") (k 0)))) (h "0kfywhhzmamdgskgbyc2d3w115x335slnygn0br17gyi8nlbzxrq") (f (quote (("std" "maybe_xml/std") ("default" "std") ("alloc" "maybe_xml/alloc")))) (y #t) (r "1.71.0")))

(define-public crate-readfeed-0.1.1 (c (n "readfeed") (v "0.1.1") (d (list (d (n "maybe_xml") (r "^0.10.0") (k 0)))) (h "0jhxfrdq4dmn0j1c5dkpqkqc99fwwrr6qh4ygqbh2bjpy3bmmibj") (f (quote (("std" "maybe_xml/std") ("default" "std") ("alloc" "maybe_xml/alloc")))) (y #t) (r "1.71.0")))

(define-public crate-readfeed-0.1.2 (c (n "readfeed") (v "0.1.2") (d (list (d (n "maybe_xml") (r "^0.10.0") (k 0)))) (h "10cclqk9qplbb0fi9327l3ny2p6hwqc6qpi662wsybi8yqfs1qgh") (f (quote (("std" "maybe_xml/std") ("default" "std") ("alloc" "maybe_xml/alloc")))) (y #t) (r "1.71.0")))

(define-public crate-readfeed-0.1.3 (c (n "readfeed") (v "0.1.3") (d (list (d (n "maybe_xml") (r "^0.10.0") (k 0)))) (h "0xwpm02xxkwz0h50qhbsi9dvbi7hbdy18cw6bhr2spcg01hzp63m") (f (quote (("std" "maybe_xml/std") ("default" "std") ("alloc" "maybe_xml/alloc")))) (y #t) (r "1.71.0")))

(define-public crate-readfeed-0.2.0 (c (n "readfeed") (v "0.2.0") (d (list (d (n "maybe_xml") (r "^0.11.0") (k 0)))) (h "104i7lgb138v1prfksw5k71l687fdihizhjkdc71bbzw6crgp6q0") (f (quote (("std" "maybe_xml/std") ("default" "std") ("alloc" "maybe_xml/alloc")))) (r "1.71.0")))

