(define-module (crates-io re ad read) #:use-module (crates-io))

(define-public crate-read-0.1.0 (c (n "read") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winuser" "winbase" "shellapi" "windowsx"))) (d #t) (k 0)))) (h "0jm2kq53a1j2hmdin8p8g9p96q9zfkh8zgspbbapzrddkxwmsxrc")))

(define-public crate-read-0.1.1 (c (n "read") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winuser" "winbase" "shellapi" "windowsx"))) (d #t) (k 0)))) (h "1kfvvcxjlf3mpiz7qqvv66scr539qa7pmq4z8qn72993x78shaw9")))

