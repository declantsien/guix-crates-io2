(define-module (crates-io re ad read-copy-update) #:use-module (crates-io))

(define-public crate-read-copy-update-0.1.0 (c (n "read-copy-update") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "thread_local") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0hz9saw8r6farm4jq7lz2xl634jic6nw74bhjnpm8cx526r1ygka") (f (quote (("thread-local" "thread_local") ("default"))))))

