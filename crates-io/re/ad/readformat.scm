(define-module (crates-io re ad readformat) #:use-module (crates-io))

(define-public crate-readformat-0.1.0 (c (n "readformat") (v "0.1.0") (h "1daq3fcigjryxvllh7gap1il3qdcbxbbs6mawncqq39nfa5hlprl")))

(define-public crate-readformat-0.1.1 (c (n "readformat") (v "0.1.1") (h "0q0a4nxmlxd6k8v52vwrrxc89md31zs25pjpzm928wkk1yx2s0jw")))

(define-public crate-readformat-0.1.2 (c (n "readformat") (v "0.1.2") (h "0lkk62yripgdljh2vwafqk4irhfgpklcr19w2snv7a0a8yypygxh")))

