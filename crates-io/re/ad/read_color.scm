(define-module (crates-io re ad read_color) #:use-module (crates-io))

(define-public crate-read_color-0.0.1 (c (n "read_color") (v "0.0.1") (h "1dzvnbclb0i6mspwy7c1m850y3kms6bw0rya67q0rg0nwqcayxyj")))

(define-public crate-read_color-0.0.2 (c (n "read_color") (v "0.0.2") (h "0gvycln0i6j1gz10zfcvxpp2s9brx3qx27wvvd47gxpxzxf5z39r")))

(define-public crate-read_color-0.1.0 (c (n "read_color") (v "0.1.0") (h "10xdazzbqf8jvig5gg3azmikar64j2v349jgjpr3s69h0qhglav8")))

(define-public crate-read_color-1.0.0 (c (n "read_color") (v "1.0.0") (h "1np0pk31ak7hni4hri3m75mbf8py1wdfjshmrj5krbd4p9c8hk4z")))

