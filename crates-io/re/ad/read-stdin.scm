(define-module (crates-io re ad read-stdin) #:use-module (crates-io))

(define-public crate-read-stdin-1.0.0 (c (n "read-stdin") (v "1.0.0") (h "02qax7wilxmnjz80ap5zmzf2c4zm3i1dlc08xzjhgqjp4y894qwc")))

(define-public crate-read-stdin-1.1.0 (c (n "read-stdin") (v "1.1.0") (h "18yrlpy4mgijsafsisg65kdxl9hgr75qjnx678069f48lwwl2mfn")))

(define-public crate-read-stdin-1.1.1 (c (n "read-stdin") (v "1.1.1") (h "183j285sk814mf22ayv6azixqnh3iqhayjjfydzdka8r1zyi5mjs")))

