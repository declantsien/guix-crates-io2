(define-module (crates-io re ad read-progress) #:use-module (crates-io))

(define-public crate-read-progress-0.1.0 (c (n "read-progress") (v "0.1.0") (h "1pbqwxp3nigsdc5fzs7nvm0x3ds878jwncgnmfafhb9an0n6g2b0")))

(define-public crate-read-progress-0.2.0 (c (n "read-progress") (v "0.2.0") (h "04955c6z2451licarkkz64nv9ryf9imsn2s1655z9bfvm2mfz833")))

(define-public crate-read-progress-0.3.0 (c (n "read-progress") (v "0.3.0") (h "0gzdk97dl46lqiymlgqki3dp6mj06n7d3r5qfif7803nzmx2w3h4")))

(define-public crate-read-progress-0.4.0 (c (n "read-progress") (v "0.4.0") (h "108qa5p4wdlv861bwi6l8dmirw06iz7x5a12c24ywzp6jdyiv8k2")))

(define-public crate-read-progress-0.5.0 (c (n "read-progress") (v "0.5.0") (h "07glzpp5cj9vad2wp5irgp9iqx3gs6lxlmy9xk2pl22afqhzxnjz")))

