(define-module (crates-io re ad readlock) #:use-module (crates-io))

(define-public crate-readlock-0.0.1 (c (n "readlock") (v "0.0.1") (h "1wqs5418i8zwks87va9rn9n7yy86dm5y61f9q86rr23hgmwpgqkv")))

(define-public crate-readlock-0.1.0 (c (n "readlock") (v "0.1.0") (h "04jv05jsba8x2swricn8gqdq80c46pabz1ddbm2f2ym74qld338c")))

(define-public crate-readlock-0.1.1 (c (n "readlock") (v "0.1.1") (h "1nbk9wzz613p8ab9wdfmw4qwx8k8mrx0jqyhw2yxci5s4ns0r3vs")))

(define-public crate-readlock-0.1.2 (c (n "readlock") (v "0.1.2") (h "0mh97c7iqwrwrafifaa3h0r822mnd1gm54yiramqdza2rqkmjc8k")))

(define-public crate-readlock-0.1.3 (c (n "readlock") (v "0.1.3") (h "0bkvabhp8h1sj0ni66akz885km7j4js77hqv3mk1ykah60hs5j1m")))

(define-public crate-readlock-0.1.4 (c (n "readlock") (v "0.1.4") (h "0x9lv3mbvg33l24ilnsxn0m2xlb1i5ij51yqfmw27hifwk674zgs")))

(define-public crate-readlock-0.1.5 (c (n "readlock") (v "0.1.5") (h "06n0wjxpw3fi9j07gh2f5jwrpi877gx200a4jl9rxrh7j8spynzd")))

(define-public crate-readlock-0.1.6 (c (n "readlock") (v "0.1.6") (d (list (d (n "rclite") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0vabvwr87rvd73nykcbhlivzg02ifllfxpac8gvg9qq59fbk3vwj") (s 2) (e (quote (("lite" "dep:rclite"))))))

(define-public crate-readlock-0.1.7 (c (n "readlock") (v "0.1.7") (d (list (d (n "rclite") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "18l0la7zxavbw1ppfigq84f994lyw65rbpl4hlf5gakd37kj7cyp") (s 2) (e (quote (("lite" "dep:rclite"))))))

