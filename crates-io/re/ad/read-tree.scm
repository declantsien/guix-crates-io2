(define-module (crates-io re ad read-tree) #:use-module (crates-io))

(define-public crate-read-tree-0.1.0 (c (n "read-tree") (v "0.1.0") (h "1y39v973c4222gn4jjkwvh89fsj2nby4cps9b9clbfm4j0mssd4r")))

(define-public crate-read-tree-0.2.0 (c (n "read-tree") (v "0.2.0") (h "1ngm0wmlgz8lm53bq1dk9y2ji4823gd79s18f0n7vdf2a4ffdgdg")))

