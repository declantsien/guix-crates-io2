(define-module (crates-io re ad readline-sys) #:use-module (crates-io))

(define-public crate-readline-sys-0.0.1 (c (n "readline-sys") (v "0.0.1") (h "17hz52cqf8f22s2cbs7bg016igmm58xkiklibc8jnvsvd9d5hy0l") (y #t)))

(define-public crate-readline-sys-0.0.2 (c (n "readline-sys") (v "0.0.2") (h "1d3fmwsgjlsffdlrbdxy1h14zwfsp05sws8sinz2mxb1m22scxhw") (y #t)))

(define-public crate-readline-sys-0.0.3 (c (n "readline-sys") (v "0.0.3") (h "0b5dqcik7wf57pz2s65izagz4cm260w5cjg872n6fg44rxc0jpp4") (y #t)))

(define-public crate-readline-sys-0.0.4 (c (n "readline-sys") (v "0.0.4") (h "0mk9cy6840jf40zvsx3hy1hfm7g2wg0s6vd15xwjl4617h67k6b7") (y #t)))

(define-public crate-readline-sys-0.0.5 (c (n "readline-sys") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "04vzdqg8qqjbwls07nky5lar05546pq4p0qm6g3pigq8nsaplmp0") (y #t)))

(define-public crate-readline-sys-0.0.12 (c (n "readline-sys") (v "0.0.12") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "0apj4vxv8jhxjbf5mc68v1wvdylldramfwavd1as2kcmmc39q4fl") (y #t)))

