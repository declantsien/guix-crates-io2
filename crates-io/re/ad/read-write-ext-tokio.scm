(define-module (crates-io re ad read-write-ext-tokio) #:use-module (crates-io))

(define-public crate-read-write-ext-tokio-0.1.0 (c (n "read-write-ext-tokio") (v "0.1.0") (d (list (d (n "fixed-buffer") (r "^0.4") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt" "time"))) (d #t) (k 2)))) (h "1ir6wxvg640bzaq706vlbfn8gnpcc2vcn017fyk8h1vrglwlaqkc")))

