(define-module (crates-io re ad read_char) #:use-module (crates-io))

(define-public crate-read_char-0.1.0 (c (n "read_char") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "0m0nlh806a5zhqw48kxvb6gaqaqp2nvw8ywp1y8f4wzi72sdvpj3")))

(define-public crate-read_char-0.1.1 (c (n "read_char") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "1ar94fw5pcd6vknw04dz5lwdwmy687ipblv5yrga20kmm1jkb00g")))

