(define-module (crates-io re ad read-to-timeout) #:use-module (crates-io))

(define-public crate-read-to-timeout-0.1.1 (c (n "read-to-timeout") (v "0.1.1") (h "1wlg8x7y92xqa3fnzcmkwj5pi4n3klpvz5krngx9dydap7rjb6nx") (y #t)))

(define-public crate-read-to-timeout-0.1.2 (c (n "read-to-timeout") (v "0.1.2") (h "0396n6ym9m2q27r8rbyrz06l273pp87zchzmd7k96hm3xm44nqky") (y #t)))

(define-public crate-read-to-timeout-0.1.3 (c (n "read-to-timeout") (v "0.1.3") (h "1byj9l87kkkrb500xw2bymyph06gs455gpfmdsckyjxc0i0dq197")))

(define-public crate-read-to-timeout-0.1.4 (c (n "read-to-timeout") (v "0.1.4") (h "19f3id9y2hdzayqzinchn0mbp1zcz15z4h7vgxrc93nwahlpi3jq")))

