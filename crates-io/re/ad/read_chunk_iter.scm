(define-module (crates-io re ad read_chunk_iter) #:use-module (crates-io))

(define-public crate-read_chunk_iter-0.1.0 (c (n "read_chunk_iter") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("html_root_url_updated"))) (k 2)))) (h "1h20dj52l96l9ygfw0igzasd0948vcyzhpyli2aw4cs5hkxksjq6")))

(define-public crate-read_chunk_iter-0.2.0 (c (n "read_chunk_iter") (v "0.2.0") (d (list (d (n "atomic-wait") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "plotters" "rayon" "cargo_bench_support"))) (k 2)) (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "poly1305") (r "^0.8.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "1j10w0i39802xn302zcv844y3s2jd11is1fqq60vcs768qh6vyqi") (f (quote (("default") ("autodetect_vectored"))))))

