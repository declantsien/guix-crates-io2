(define-module (crates-io re ad readenv) #:use-module (crates-io))

(define-public crate-readenv-0.1.0 (c (n "readenv") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "1af83z54vxawjnp6n4as5hd79zbbb7rf4y5sji3b4yafdkig4m2j")))

(define-public crate-readenv-0.1.1 (c (n "readenv") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "1grs6wrxn4d6bvns24mp5cbrdj7p4qdq8whl0yvzvqyd4nl5yzyf")))

(define-public crate-readenv-0.1.2 (c (n "readenv") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0jgpzabz6hsk03sh70hk75fndhaxkc6gnis5s5hzs6m84j38y1aj")))

(define-public crate-readenv-0.2.0 (c (n "readenv") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0jks50i31z6084ipc0b2xbl69wji9l7hrz644m7443596pf4zrbr")))

(define-public crate-readenv-0.2.1 (c (n "readenv") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "1pabpri1v84m24h2wdbq6bzv9aisglp7iv4v35z7vv9rks2sjyh6")))

(define-public crate-readenv-0.2.2 (c (n "readenv") (v "0.2.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0l5wslpjqyc078vcqsw1f0gs2k6a1ljcw68fjr725ynwsg9nnxsa")))

(define-public crate-readenv-0.2.3 (c (n "readenv") (v "0.2.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "12i0rqngv5d95m3cp0zxswj4wflk0b06vl3p3363ndqx71p3wrmg")))

(define-public crate-readenv-0.2.4 (c (n "readenv") (v "0.2.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "1zpnr3364shs9w2rqrr18g7jc96rcnwn21qhhj2kim672wsn2wgz")))

(define-public crate-readenv-0.2.5 (c (n "readenv") (v "0.2.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0rfclgjvzqsn98krdlm4nsmdffd1443v9ap4rblvq9bklldsq12x")))

(define-public crate-readenv-0.2.6 (c (n "readenv") (v "0.2.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "1phgk4whdp8nidiysjcacw8ji2h9wkbwx4knj7g40wk10zgxn5i9")))

(define-public crate-readenv-0.3.0 (c (n "readenv") (v "0.3.0") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0a0zsqmqnsfz2nyp0nyc5s8qwxkg8fwygsmj51sz9yi8bgxgp4dl")))

