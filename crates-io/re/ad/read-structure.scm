(define-module (crates-io re ad read-structure) #:use-module (crates-io))

(define-public crate-read-structure-0.1.0 (c (n "read-structure") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ymanh2qwxahkkyxs005n74szbm57idm1p8n06p6mvaqd7iv72sp")))

(define-public crate-read-structure-0.2.0 (c (n "read-structure") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vd28gbvypdqnaj24ialhyq1bb9sx9daj7h5r32bcmd09dkvzvb1")))

(define-public crate-read-structure-0.2.1-rc.1 (c (n "read-structure") (v "0.2.1-rc.1") (d (list (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1dpn4934ww5jvnp6g8iskf3cy7pakbf6gyb0vwznrw1b0bjqzzmi") (y #t)))

