(define-module (crates-io re ad readfilez) #:use-module (crates-io))

(define-public crate-readfilez-0.0.0 (c (n "readfilez") (v "0.0.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0k3kwf2f7ila2ffc4m7sjixph3jjgqibpd2137gl47k9svx2pdgf") (y #t)))

(define-public crate-readfilez-0.0.1 (c (n "readfilez") (v "0.0.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "18bmdf1n64ycc36vjxvlcirrc8wxpjvh0awp3j9k4gi698gdp8dp")))

(define-public crate-readfilez-0.1.0 (c (n "readfilez") (v "0.1.0") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1vniyw0rdd92lb7rrw713qmgz5sckpf7kgmnv9i4d168d0cxqpv3") (f (quote (("seek_convenience") ("default")))) (y #t)))

(define-public crate-readfilez-0.1.1 (c (n "readfilez") (v "0.1.1") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0c8x9gb568kbakxn5538h60vllszwa7pyfwfsyw6im0297qwqzga") (f (quote (("seek_convenience") ("default"))))))

(define-public crate-readfilez-0.1.2 (c (n "readfilez") (v "0.1.2") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0jbwxraim0n0432ywv7kbjsk8ky7ns4dh3g03sr0i5fj095rq082") (f (quote (("seek_convenience") ("default"))))))

(define-public crate-readfilez-0.1.3 (c (n "readfilez") (v "0.1.3") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "114w7a3p5rc7b384galy6g7flkgmif1lg0w3jsjnp08394a6rvbg") (f (quote (("seek_convenience") ("default"))))))

(define-public crate-readfilez-0.2.0 (c (n "readfilez") (v "0.2.0") (d (list (d (n "delegate") (r "^0.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1njzqa1j7ic2l41g4xic7s4xalbwdszj52icnizdy028x74wxs07") (f (quote (("seek_convenience"))))))

(define-public crate-readfilez-0.2.1 (c (n "readfilez") (v "0.2.1") (d (list (d (n "delegate") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0ha7dyaz244ym56mg3a31lcdgfvwi0ffgl7jqb0v4rx62smbmdlc") (f (quote (("seek_convenience"))))))

(define-public crate-readfilez-0.2.2 (c (n "readfilez") (v "0.2.2") (d (list (d (n "delegate-attr") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1lj5f96j4nabr1gkf0a9k1rqxp7jan5d9z1kjb08q9siaq1flxs4") (f (quote (("seek_convenience"))))))

(define-public crate-readfilez-0.2.3 (c (n "readfilez") (v "0.2.3") (d (list (d (n "delegate-attr") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0s0yarb4gis5i6nmzdy5sx0dpaz9xgzb7wm0qv4m1537rhwrn078") (f (quote (("seek_convenience"))))))

(define-public crate-readfilez-0.2.4 (c (n "readfilez") (v "0.2.4") (d (list (d (n "delegate-attr") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.2") (d #t) (k 0)))) (h "08gwrjqik44yx8d2iwr0g7fb6il0kxfdq2lmlr0p94fpm255887j") (f (quote (("seek_convenience"))))))

(define-public crate-readfilez-0.3.0 (c (n "readfilez") (v "0.3.0") (d (list (d (n "delegate-attr") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1z2h2yiz90n7jiipbvirfbn4wk9wm10mdcjqjq8711mcyhm7wgj7")))

(define-public crate-readfilez-0.3.1 (c (n "readfilez") (v "0.3.1") (d (list (d (n "delegate-attr") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r ">=0.5, <=0.7") (d #t) (k 0)))) (h "19a9ir3z85d1a3625zhn9y7jfmipbhca394jzp298q64rhf91fcl")))

(define-public crate-readfilez-0.3.2 (c (n "readfilez") (v "0.3.2") (d (list (d (n "delegate-attr") (r ">=0.2, <=0.4") (d #t) (k 0)) (d (n "memmap2") (r ">=0.5, <=0.10") (d #t) (k 0)))) (h "13xhfzi7l8gbdsj16pwjrlfwj66ibkpp8k46g2wc1wfm9knf1gfs")))

