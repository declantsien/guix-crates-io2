(define-module (crates-io re ad readwrite) #:use-module (crates-io))

(define-public crate-readwrite-0.1.0 (c (n "readwrite") (v "0.1.0") (h "0rh1rbprna8fgk55jagnahglni0ryrli4zmq71pjkglssq0i2nfh")))

(define-public crate-readwrite-0.1.1 (c (n "readwrite") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06kz5zgizf1yjznnw6vv59r5ls760gf8ih5jff5alrws1mqc862p") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-readwrite-0.1.2 (c (n "readwrite") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wz4xlkzzwhki2lsw7vp1s4c86w9xkk42j897b93ds5vvac1p2bk") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-readwrite-0.2.0 (c (n "readwrite") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "tokio_dep") (r "^1") (o #t) (d #t) (k 0) (p "tokio")))) (h "0m7dz5h8xafy4lv22pda39cgj3m5l543l0wl7ya887nfyy1xrzri") (f (quote (("tokio" "tokio_dep" "pin-project-lite") ("default") ("asyncstd" "futures" "pin-project-lite"))))))

