(define-module (crates-io re ad ready) #:use-module (crates-io))

(define-public crate-ready-1.0.0 (c (n "ready") (v "1.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "0c1kqbsmmp6aya1v5izh1vj7k5f73p434jcfp7ls95l3kn1csr68")))

(define-public crate-ready-1.0.1 (c (n "ready") (v "1.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "1vp0wk0ahybvxi2wl0svwdf97pplja2flh0v61kxg3mqv5hdn0n7")))

(define-public crate-ready-1.0.2 (c (n "ready") (v "1.0.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "1705blcrz0wm7qfc3q2q49fh809jsi3xsayqy42sks28jnw1r7pw")))

(define-public crate-ready-1.1.0 (c (n "ready") (v "1.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "0m3cbgrcm66x6rkvxxkw1iys3mii91mbk8v7nb4hym59r5h87xna")))

