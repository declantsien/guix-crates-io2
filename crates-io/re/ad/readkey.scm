(define-module (crates-io re ad readkey) #:use-module (crates-io))

(define-public crate-readkey-0.1.0 (c (n "readkey") (v "0.1.0") (h "15xhbfgmgqnzx5vdpx5p69s11nz3l68pkzv87x5qrkj1z9x66cw9")))

(define-public crate-readkey-0.1.1 (c (n "readkey") (v "0.1.1") (h "0mpixmfnp6iqcdbzy8n2s8dp3ma9xxlczc51fg61vzh0d8pillyh")))

(define-public crate-readkey-0.1.2 (c (n "readkey") (v "0.1.2") (h "0q66gq4157517xzm929vyzkpr757h2p0in2ah5lynlw5la54ig2n")))

(define-public crate-readkey-0.1.3 (c (n "readkey") (v "0.1.3") (h "0zbpc3qxk1y9gllf234chcqq86gbs30b384h1hiw7rj0sjr18zr6")))

(define-public crate-readkey-0.1.4 (c (n "readkey") (v "0.1.4") (h "13k0qaphkkfrl54r3pmzzw30hfpvj8aah7r0c1xjac68l0dzsbrv")))

(define-public crate-readkey-0.1.5 (c (n "readkey") (v "0.1.5") (h "03wg9qhkhgfr2h981xdgagbminb6jg6lfr8qv342dsgkni5vk3fr")))

(define-public crate-readkey-0.1.6 (c (n "readkey") (v "0.1.6") (h "1pdg7bz9r14s2i77qi4sckz36zh1dnxxy9bs86igd0723krpwgjp")))

(define-public crate-readkey-0.1.7 (c (n "readkey") (v "0.1.7") (h "0iiip8bq4yhal5rv6wlws0xgz798blki7s5ly5cmlwm1ssv03m46")))

(define-public crate-readkey-0.2.1 (c (n "readkey") (v "0.2.1") (h "0jfjf03dg2hjz07llf4aiswjr7jppa0bz2jcw0kbpjcvlj6gjxvn")))

