(define-module (crates-io re ad readelf) #:use-module (crates-io))

(define-public crate-readelf-0.0.1 (c (n "readelf") (v "0.0.1") (d (list (d (n "argparse") (r "0.*") (d #t) (k 0)) (d (n "elf") (r "0.*") (d #t) (k 0)))) (h "1ra8w88968h98ldg4d7w6j56g9bqncqva735dl3kxm4q5akw9ici")))

(define-public crate-readelf-0.0.2 (c (n "readelf") (v "0.0.2") (d (list (d (n "argparse") (r "0.*") (d #t) (k 0)) (d (n "elf") (r "0.*") (d #t) (k 0)))) (h "1cqknyszyx6382qmn91pm0y77ls881vl159hmfjc7qvw6xdqqkz5")))

(define-public crate-readelf-0.0.3 (c (n "readelf") (v "0.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "elf") (r "0.0.*") (d #t) (k 0)))) (h "16zn58wcxphx5yj6b3w6jzsqnj3l9knzx3mhjdj3i5d960j7x3k8")))

(define-public crate-readelf-0.0.4 (c (n "readelf") (v "0.0.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.0") (d #t) (k 0)) (d (n "elf") (r "^0.2.0") (d #t) (k 0)))) (h "0fnm7cmsdk7v56vjni8yj7w1gsdl77wcigijd6k71gwi5a8mg44j")))

(define-public crate-readelf-0.0.5 (c (n "readelf") (v "0.0.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)) (d (n "elf") (r "^0.3.0") (d #t) (k 0)))) (h "00kf1l0f7ai5jlvxkmxwzddk5w7qcay31rbwdnnkl86mmyhjsza6")))

(define-public crate-readelf-0.0.6 (c (n "readelf") (v "0.0.6") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)) (d (n "elf") (r "^0.6.0") (d #t) (k 0)))) (h "1mi96y9dsbx7gigwnyfz567y1nfinnz40mjwvxvcg3cxz224fk56")))

(define-public crate-readelf-0.0.7 (c (n "readelf") (v "0.0.7") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)) (d (n "elf") (r "^0.7.1") (d #t) (k 0)))) (h "06zc0q5il5jdqfa772iycql7w9f8xkrlk7mavcysig3ac20p0nls")))

