(define-module (crates-io re ad read_pipe) #:use-module (crates-io))

(define-public crate-read_pipe-0.1.0 (c (n "read_pipe") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0pins0n9hkw35bpngyjdazqyajg07vl3r22jk0i2jclrlg5cy7cv")))

(define-public crate-read_pipe-0.1.1 (c (n "read_pipe") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1az09qzqc31378rjvdwgdn6phqakgvic6a5vn20x9xrm5pq5crhi")))

(define-public crate-read_pipe-0.1.2 (c (n "read_pipe") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1rxibpqs88ahd0fgrsvmapljf42djx788v8c5wgp5n3irhhz5ffy")))

(define-public crate-read_pipe-0.2.0 (c (n "read_pipe") (v "0.2.0") (h "05awrc2jxmsx6flz1in29g5bgkjss71hwlnm5p59sf161v9ai5ya")))

