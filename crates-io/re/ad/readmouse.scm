(define-module (crates-io re ad readmouse) #:use-module (crates-io))

(define-public crate-readmouse-0.1.6 (c (n "readmouse") (v "0.1.6") (h "1ipkwldmdi2sd0vdaj463247bd3b6d88nhkjmwy7vxj8px85l5pd")))

(define-public crate-readmouse-0.2.0 (c (n "readmouse") (v "0.2.0") (h "18yydzfz5iwbhclc2vj0zv3f165ck8b5nl8dd4sawrxnkmnygp2p")))

(define-public crate-readmouse-0.2.1 (c (n "readmouse") (v "0.2.1") (h "154d81zl8ah4c20dgv029avpcmm10ssxbqyfi8cs39g6l5r5q45y")))

