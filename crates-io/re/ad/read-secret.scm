(define-module (crates-io re ad read-secret) #:use-module (crates-io))

(define-public crate-read-secret-0.1.0 (c (n "read-secret") (v "0.1.0") (h "0n26m36m19d31ri8dlyq2mlb6cq07l0v8nkhlmib16anq3mgzgmk")))

(define-public crate-read-secret-0.1.1 (c (n "read-secret") (v "0.1.1") (h "1dvysijv28x4pgkqj8k3h5f986br7mh7rvyrzjn58kavckbkw8gy")))

(define-public crate-read-secret-0.1.2 (c (n "read-secret") (v "0.1.2") (h "17f3bnhwjpdlvx6prsllkyq6ynghifldnimm4bmg4ixawwv8k7jm")))

