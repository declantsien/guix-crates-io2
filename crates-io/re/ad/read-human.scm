(define-module (crates-io re ad read-human) #:use-module (crates-io))

(define-public crate-read-human-0.1.0 (c (n "read-human") (v "0.1.0") (d (list (d (n "error-gen") (r "^0.1.0") (d #t) (k 2)))) (h "0vm89xqg19wm3nr8lhj12nbwmsvix4mhrcisqfndihnlrpprghqw")))

(define-public crate-read-human-0.1.1 (c (n "read-human") (v "0.1.1") (d (list (d (n "error-gen") (r "^0.1.0") (d #t) (k 2)))) (h "1lvifsg32182vhnish64acxl2a1vb7lvgp4d08w8bjv938kwv4v5")))

