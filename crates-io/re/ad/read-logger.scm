(define-module (crates-io re ad read-logger) #:use-module (crates-io))

(define-public crate-read-logger-0.1.0 (c (n "read-logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1c2w5mciyqk2rfmk6ind1sa1w34h5x1qg5bpdkphfqldw6z8njzr")))

(define-public crate-read-logger-0.2.0 (c (n "read-logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "16aab6vi73835ad69hxrkplqj2r1ryln54gbf55q1ny74dd72zqc")))

