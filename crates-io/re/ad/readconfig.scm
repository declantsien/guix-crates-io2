(define-module (crates-io re ad readconfig) #:use-module (crates-io))

(define-public crate-readconfig-0.1.0 (c (n "readconfig") (v "0.1.0") (h "050cljzzfwqvk68v8kw449kcwxs76773rin7pbjfc09i89ippvas")))

(define-public crate-readconfig-0.1.1 (c (n "readconfig") (v "0.1.1") (h "0ll7namy8xqh8pah3h92qa0alr5iz51ajw0sglawj2n7h21l9wp3")))

(define-public crate-readconfig-0.2.1 (c (n "readconfig") (v "0.2.1") (h "0sdkg4qr6q71q9fsj7wsrfapxby48cv02ap6124xr0cmj4qj7bg8")))

(define-public crate-readconfig-0.2.2 (c (n "readconfig") (v "0.2.2") (h "0imm5v3r8201j9x4xp3p83jj3fr4vxy30x07vr0mrsxmshjyg0m2")))

(define-public crate-readconfig-0.2.3 (c (n "readconfig") (v "0.2.3") (h "0y3xj9a9fv59zlhkhlp640511rf1mklvqq4rrxvlx9rf7s122ijw")))

(define-public crate-readconfig-0.2.4 (c (n "readconfig") (v "0.2.4") (h "17s9bl7kab7h460zkjdns5bds8pcnk5ng3ciikk8mpxbn7bgml6v")))

(define-public crate-readconfig-0.2.5 (c (n "readconfig") (v "0.2.5") (h "08yj1h0pg4a8j173bdy69dndbfj1jxcmran60r5vfi3zz7wmaa4c")))

