(define-module (crates-io re ad readlock-tokio) #:use-module (crates-io))

(define-public crate-readlock-tokio-0.1.0 (c (n "readlock-tokio") (v "0.1.0") (d (list (d (n "rclite") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "1k68hv1sn25rh16qhr0539jq17cd2dq2qykjd2sk367h3s8d1gvi") (s 2) (e (quote (("lite" "dep:rclite"))))))

(define-public crate-readlock-tokio-0.1.1 (c (n "readlock-tokio") (v "0.1.1") (d (list (d (n "rclite") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "12dg4sjv8d4p7a8pz0qmqyhwhibvxbcmznp54v61kwnky66kwzvc") (s 2) (e (quote (("lite" "dep:rclite"))))))

(define-public crate-readlock-tokio-0.1.2 (c (n "readlock-tokio") (v "0.1.2") (d (list (d (n "rclite") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "1zc356rp6x0imb4rq2jyq238lv065h4gnyr7zsh3c6aj0s4kd8zv") (s 2) (e (quote (("lite" "dep:rclite"))))))

