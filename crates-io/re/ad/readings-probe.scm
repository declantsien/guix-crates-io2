(define-module (crates-io re ad readings-probe) #:use-module (crates-io))

(define-public crate-readings-probe-0.1.0 (c (n "readings-probe") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0fbrqhg27lnhnbvwx7yf5qzgshmd458z4z686vsyxl4n1w6b68r4")))

(define-public crate-readings-probe-0.1.1 (c (n "readings-probe") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "12rkffkdmvq7ymakjykcn8qd4g5lzbcscywi2vmnvar8g63y280y")))

(define-public crate-readings-probe-0.1.2 (c (n "readings-probe") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("psapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cxfzqag8bx0agywjmggffrn8klj2nnj3sb3xpndgv38mvafwc5i")))

(define-public crate-readings-probe-0.1.3-alpha.0 (c (n "readings-probe") (v "0.1.3-alpha.0") (d (list (d (n "jemallocator") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("psapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00zq6hw16b402vjwhipd7sc50qk87ymsbw4rflspi7vkw3glgfg4") (y #t)))

(define-public crate-readings-probe-0.1.3 (c (n "readings-probe") (v "0.1.3") (d (list (d (n "jemallocator") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("psapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y5yy0ahkkq6lmvlspczbp68x01cia5wwl021zj2xl4jcxni2vkb")))

(define-public crate-readings-probe-0.1.4 (c (n "readings-probe") (v "0.1.4") (d (list (d (n "jemallocator") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("psapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13djcrbzp8ikqmg6k6c7k46nxy88rxca12lslsw19cw38fdw5ss9")))

