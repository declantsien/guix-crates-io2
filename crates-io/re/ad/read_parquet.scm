(define-module (crates-io re ad read_parquet) #:use-module (crates-io))

(define-public crate-read_parquet-0.1.0 (c (n "read_parquet") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_dataframe") (r "^0.1.0") (d #t) (k 0)))) (h "1hxdxw8pppfw06wznw1ymgykkdsdqfcp58968bldgx73l1x18w5n")))

