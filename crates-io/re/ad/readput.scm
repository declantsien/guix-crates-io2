(define-module (crates-io re ad readput) #:use-module (crates-io))

(define-public crate-readput-0.1.0 (c (n "readput") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1qc04p8j4rbn652jhwqwh34v59m2iy4dpamzh5wzs447bp5mxwaj")))

(define-public crate-readput-0.1.1 (c (n "readput") (v "0.1.1") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "0wf3n1kqsmiv6kgmgzz1m0zfgw7f25vcy64k40f1ndfim1w7vmd8")))

(define-public crate-readput-0.1.2 (c (n "readput") (v "0.1.2") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "0zvzzjbsxkns8bhgz0rkczdijl7xzvwsqsgvfkgndgmby2ynpism")))

