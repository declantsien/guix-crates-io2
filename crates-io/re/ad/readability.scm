(define-module (crates-io re ad readability) #:use-module (crates-io))

(define-public crate-readability-0.1.0 (c (n "readability") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0nkh4f31dhf17i6fq1mfns0xly3c3fx5j83g8zqb7vdbn4am5r98")))

(define-public crate-readability-0.1.1 (c (n "readability") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1izqnkq4x7n7vdfzc3xlrq05x54vw8pqz78c29wn3y9155j3vkww")))

(define-public crate-readability-0.1.2 (c (n "readability") (v "0.1.2") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0847i9iahawlly1vnv5yxa44vfwcvpjz6043bk44wxr21j408shr")))

(define-public crate-readability-0.1.3 (c (n "readability") (v "0.1.3") (d (list (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0z3zs3zxwkmmq9krb2g2iiy9yg6gbihy43cy4pi6k0a1jlx1g79b") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-0.1.4 (c (n "readability") (v "0.1.4") (d (list (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "1xi62s47i3k4mq8kpah6qw37xmycsr4qd2sxqi510ljcds7p19km") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-0.1.5 (c (n "readability") (v "0.1.5") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0dah0m9r00pwf9wddfq7v7k3qzjr1yxkk2k3yifs02pdszkdp3ds") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-0.2.0 (c (n "readability") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "16gk8wwi4vyxhmn733fqa2k1g00smq2dj1hgp799sac6j8akp177") (f (quote (("default" "reqwest"))))))

(define-public crate-readability-0.3.0 (c (n "readability") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "08223f12kvhc8404vh6fhplyc88648lni6rd30azfg3d1bi9crg5") (f (quote (("default" "reqwest"))))))

