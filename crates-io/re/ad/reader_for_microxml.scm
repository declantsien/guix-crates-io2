(define-module (crates-io re ad reader_for_microxml) #:use-module (crates-io))

(define-public crate-reader_for_microxml-1.0.0 (c (n "reader_for_microxml") (v "1.0.0") (h "04hrg4c6g4vyfjmi03lf6a4rjk3gyyy9n7503bvf4hb4ibv1cdk2") (y #t)))

(define-public crate-reader_for_microxml-1.0.1 (c (n "reader_for_microxml") (v "1.0.1") (h "144w67fa1lixynx51f07sq7w5psj6xivnazirs46jb3w08zcg8b0") (y #t)))

(define-public crate-reader_for_microxml-1.0.2 (c (n "reader_for_microxml") (v "1.0.2") (h "14914k3xiqimc8y5arhsjq5y4mwy36lspvjmmlydly2c29xs8psw") (y #t)))

(define-public crate-reader_for_microxml-1.0.3 (c (n "reader_for_microxml") (v "1.0.3") (h "1y25adi56bgpf19j3v1xliparp9bljkyghgwklzbzj50s7ifgfyb") (y #t)))

(define-public crate-reader_for_microxml-1.0.4 (c (n "reader_for_microxml") (v "1.0.4") (h "1300gpvnwbcb5bz01ss7ajml7qlgj3gx2rm7f5cjirxai2xid1kr") (y #t)))

(define-public crate-reader_for_microxml-1.1.6 (c (n "reader_for_microxml") (v "1.1.6") (h "1bhr04g6djryvfmpsv2hvsk59c0isgmywd1a52p578jm18b5gyv8") (y #t)))

(define-public crate-reader_for_microxml-1.1.7 (c (n "reader_for_microxml") (v "1.1.7") (h "1rf2lf7v4a4ria4mgsxfyl4019gmh060gkjrwjj52hcwrkh6vpdg") (y #t)))

(define-public crate-reader_for_microxml-1.1.10 (c (n "reader_for_microxml") (v "1.1.10") (h "0b3hlxi841zkbrvp5sy5k4bghxx2frz6733qmvp4zna4gdvqncna") (y #t)))

(define-public crate-reader_for_microxml-1.1.11 (c (n "reader_for_microxml") (v "1.1.11") (h "0hhclczlb54vdi5h6s7ngn50z3dgwi319wykwvmmj6niy2qsnl7x") (y #t)))

(define-public crate-reader_for_microxml-1.1.12 (c (n "reader_for_microxml") (v "1.1.12") (h "1p5dc60czrnrgfcmmabmn7x1pv1f41mcmzl3lhl2hq9qjmnb2l1m") (y #t)))

(define-public crate-reader_for_microxml-2.0.1 (c (n "reader_for_microxml") (v "2.0.1") (h "1qaxl7197zf4iis6zsylphp03ab1z3549bzd0rqz7phi9hznlwhd")))

