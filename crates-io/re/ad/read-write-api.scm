(define-module (crates-io re ad read-write-api) #:use-module (crates-io))

(define-public crate-read-write-api-0.12.0 (c (n "read-write-api") (v "0.12.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "079lkfiggjvkadfwr1vkvsf06i9q8vdva53ycblmvx78rswm1bnj") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.12.1 (c (n "read-write-api") (v "0.12.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "13d92sp729kdis799f2cpn103wb7ymsqnvmqi3h1x4rh6psr6m0l") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.13.0 (c (n "read-write-api") (v "0.13.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "16gz1qcic76v1dqrk5lkbd1wiwjrhfb4060zrbnnqrd1kg39zair") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.14.0 (c (n "read-write-api") (v "0.14.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0kr12nwa7ngk9k3w8a37syyv25fiyvq05zws1ly01yfdylzammcy") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.14.1 (c (n "read-write-api") (v "0.14.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0gf3w44s32qsi5sg6vbkbggrj20hd3cblm0prbknzciblifq6ifx") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.15.0 (c (n "read-write-api") (v "0.15.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "18h7l54fz9mg99swbkzbvyfk45bil7lwfsjpzsda07ixpq3h7nrl") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.15.1 (c (n "read-write-api") (v "0.15.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0gd9hxbj1k4nqxy8v8wz05a2qbmknhc03x2vv54dkyfapmvimisv") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.15.2 (c (n "read-write-api") (v "0.15.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0kp1lipdsn6gm2cz6hbjp13zvny4fl81vpvnqzfrqf792q1shpxr") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.16.0 (c (n "read-write-api") (v "0.16.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1xn58rrd8rfq1havjyi7racsz1j6x3xb8b1yggbdyg7liqc1s8wi") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.16.1 (c (n "read-write-api") (v "0.16.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "18ml1qhb64hswfdbc0a2v71x3y3fhsmjfjidpb5kwnxg7ilf1q7j") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.16.2 (c (n "read-write-api") (v "0.16.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "09j41d5iki36v7xb9amq8ywii051z3ph2bfvcbp2m4172zzgqc0m") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.16.3 (c (n "read-write-api") (v "0.16.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1x8vw9jid71p89z7py5zahzx9x05zbyc7npagw162q47z3lla99v") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.16.4 (c (n "read-write-api") (v "0.16.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1f8phzilcgkdrdmyd0132s8k93p35hifc7r1r99i6vg6537ihqzx") (y #t) (r "1.65")))

(define-public crate-read-write-api-0.17.0 (c (n "read-write-api") (v "0.17.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1ik43wmqz1bpgvnziiq9rjk2b47q5px216c93jwcxb6jhzc3nfpf") (r "1.65")))

(define-public crate-read-write-api-0.17.1 (c (n "read-write-api") (v "0.17.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0jzc7jvnvf91z8fvinljqsrgx340mk247gvnr49zr2zkr167kclg") (r "1.65")))

(define-public crate-read-write-api-0.17.2 (c (n "read-write-api") (v "0.17.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1fa3dzv3vq5b5443d968yvl8i6ka1jnd6g04qkz2ja3dhiw2p0kk") (r "1.65")))

