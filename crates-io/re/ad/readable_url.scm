(define-module (crates-io re ad readable_url) #:use-module (crates-io))

(define-public crate-readable_url-1.0.0 (c (n "readable_url") (v "1.0.0") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.13.0") (d #t) (k 0)) (d (n "html5ever-atoms") (r "^0.2.0") (d #t) (k 0)) (d (n "readability") (r "^0") (d #t) (k 0)) (d (n "tendril") (r "^0.2.3") (d #t) (k 0)))) (h "1zhsfpac0qzg8vqjbwskskma1wglby5gbm2679ix15556z4b9kpr")))

