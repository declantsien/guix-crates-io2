(define-module (crates-io re ad read_buffer) #:use-module (crates-io))

(define-public crate-read_buffer-1.0.0 (c (n "read_buffer") (v "1.0.0") (h "1pa68pdyhy4gmwc5q6dcb1ssgywnr1g11d5hsh5887ih36bmhlpw")))

(define-public crate-read_buffer-1.1.0 (c (n "read_buffer") (v "1.1.0") (h "09s1g5hv0rcpic7zi32lvpvv4cjqzg1h8s2iv4kkvfzak611iqxz")))

(define-public crate-read_buffer-1.2.0 (c (n "read_buffer") (v "1.2.0") (h "13aqacgzcqwadvjaka7xmmlzv9yzyk7d1fdvxfibcpyvwk2y8y3m")))

(define-public crate-read_buffer-1.3.0 (c (n "read_buffer") (v "1.3.0") (h "18k78lk9s0w1vakrgfknky2gr7ldx4af32rb8686mpaza1p122xi") (y #t)))

(define-public crate-read_buffer-1.4.0 (c (n "read_buffer") (v "1.4.0") (h "1i4rja6jwv0nc819pyl6k33ww3nvqspj8byb9xrck3hnnnqiw6xl")))

