(define-module (crates-io re ad readwriteseekfs) #:use-module (crates-io))

(define-public crate-readwriteseekfs-0.1.0 (c (n "readwriteseekfs") (v "0.1.0") (d (list (d (n "fuse") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1v4937ahk7yr8hvrz4bd9l8m8h4rfjigixbb7fvvwdw0nl1vn6wz")))

