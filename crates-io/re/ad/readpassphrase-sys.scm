(define-module (crates-io re ad readpassphrase-sys) #:use-module (crates-io))

(define-public crate-readpassphrase-sys-0.0.1 (c (n "readpassphrase-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1ipd16xiz24jcvgc8r9cnj6calsbygd0iwd8jjhv2xn3hm60a8x1")))

(define-public crate-readpassphrase-sys-0.0.2 (c (n "readpassphrase-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1xarb5m7y4yybiy3vs4kidm9rib3a39q31h37mh0didpk8hag9ij")))

(define-public crate-readpassphrase-sys-0.0.3 (c (n "readpassphrase-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1j9nq5ivgiydp4jwxrf70633dxzsfpc59i4c79rgf52lclk3hlh3")))

(define-public crate-readpassphrase-sys-0.1.0 (c (n "readpassphrase-sys") (v "0.1.0") (h "10960ih97zrc1m145lx0633fm2r3ni6y0hfca33qxhabnkfigr1r")))

