(define-module (crates-io re ad readmem) #:use-module (crates-io))

(define-public crate-readmem-0.1.0 (c (n "readmem") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1mfzl3svh07783gfagp3lihldisw4mbilmkhhh4fw9a4a0wihcz8")))

