(define-module (crates-io re ad readfilter) #:use-module (crates-io))

(define-public crate-readfilter-0.1.0 (c (n "readfilter") (v "0.1.0") (h "0amjsmsjvgj1bhsnl13mqx0ss28n1cznxvdvb5xp0f8zawyp997x")))

(define-public crate-readfilter-0.1.1 (c (n "readfilter") (v "0.1.1") (h "0n7nj9y532g7x53j5bwp90cyvpx6bk87ggjh25fkfz9m4wvw7wah")))

(define-public crate-readfilter-0.1.2 (c (n "readfilter") (v "0.1.2") (h "1ksdxvjzis4s5wyljia9ggdd0qxm9k0r0pfrvzdcnyffmwbf6gaj")))

