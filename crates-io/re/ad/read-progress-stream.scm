(define-module (crates-io re ad read-progress-stream) #:use-module (crates-io))

(define-public crate-read-progress-stream-0.1.0 (c (n "read-progress-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (k 0)))) (h "1clq8bfhn9y7kfd6hy0cms9hccak40iv6wjbsvnjkc0i519axrbz")))

(define-public crate-read-progress-stream-0.2.0 (c (n "read-progress-stream") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (k 0)))) (h "104jww5ihkibjmzqvc7m5j53ahb71hrmsnkzn5xcvwmcwcq6vmkv")))

(define-public crate-read-progress-stream-0.3.0 (c (n "read-progress-stream") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (k 0)))) (h "1plapq25mzjm7v4p9xfd0s80kh5d88zhq9jkr5dsbxlpgjj222sc")))

(define-public crate-read-progress-stream-1.0.0 (c (n "read-progress-stream") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pbr") (r "^1.0.4") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 2)))) (h "1a6jv07kbn53q3hgn6dnynr1pg1v40r8rsqrhx94p97yq8pq8db4")))

