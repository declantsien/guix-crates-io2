(define-module (crates-io re ad readonly) #:use-module (crates-io))

(define-public crate-readonly-0.1.0 (c (n "readonly") (v "0.1.0") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "00928x56kb218zwdn6m7qgf3d7nxvarsycizbdf6r87n5lbhpw7s") (y #t)))

(define-public crate-readonly-0.1.1 (c (n "readonly") (v "0.1.1") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "0n87yxhnp6dglkigwkzhr57xwha08lfnzddh9k6zw8l2fyw4wdxw")))

(define-public crate-readonly-0.1.2 (c (n "readonly") (v "0.1.2") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "1v17cl0vp2p047x2hihl4k0jdgcsck3wrpp7mbrkrjd4y7yqpwvr")))

(define-public crate-readonly-0.1.3 (c (n "readonly") (v "0.1.3") (d (list (d (n "compiletest") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "19j86v5y30zqgv2wjg40fg71ng9v1lx8gazh08irxdcxadvspj5i")))

(define-public crate-readonly-0.1.4 (c (n "readonly") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 2)))) (h "08g4rbl29id88hzndj0q078fvc39c3ksyvb46xxmr4q50lsxn3fh")))

(define-public crate-readonly-0.1.5 (c (n "readonly") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06aj2zhfm35rrklyyvflpj0w1v7vlpxcm383x1zjbcvf04jk3zf9")))

(define-public crate-readonly-0.1.6 (c (n "readonly") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "10xqmmgxlcdldppzmyg7cb6fs6x35b5dqzcyxq999qfbglwgph2l")))

(define-public crate-readonly-0.2.0 (c (n "readonly") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "15ipfnrsap7nxfqig3xi3wzvnd0ca7f5q1sbj565fsc6l71w3as5")))

(define-public crate-readonly-0.2.1 (c (n "readonly") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0pc27z3g5237xqvvbwms2dyk66al238apgqwz2dg0hxx1v6xi58i") (r "1.31")))

(define-public crate-readonly-0.2.2 (c (n "readonly") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "066wx2jv805b6rs9ac8rgqp99g5mp7hl3hysw5gfbjhgn31hh3kc") (r "1.31")))

(define-public crate-readonly-0.2.3 (c (n "readonly") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v0ixqs5ss34a2d7c20ss3l6mz625ndz93k2d10h309pwpj2b1yp") (r "1.31")))

(define-public crate-readonly-0.2.4 (c (n "readonly") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0di91wqbmwmm178s8qxhnxajgnz6yqj3c4fgams16r5vnvbmxzsq") (r "1.31")))

(define-public crate-readonly-0.2.5 (c (n "readonly") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mmpqfanzf334ddmyqyg0rj99b3gc0p6m0p96sa1796a5cy3hnn7") (r "1.31")))

(define-public crate-readonly-0.2.6 (c (n "readonly") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0blsa04gdsr3h8106a6q24xz3dv8fg0dnnxbwbslyi728cvv51zx") (r "1.56")))

(define-public crate-readonly-0.2.7 (c (n "readonly") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.1") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1h14ynxlxfxpz74lrgaj2w3xg5bq6kl477xww9sqz24z2jvvzw65") (r "1.56")))

(define-public crate-readonly-0.2.8 (c (n "readonly") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.1") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qrrpfz2vczw1dmb7b6s4khi4vq9bsp6qs2j8halfp1bq8knsrgb") (r "1.56")))

(define-public crate-readonly-0.2.9 (c (n "readonly") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.1") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m6q960hb2anpfhbpdyc18d4r2rj9ypmg6w3fr5x8g6ghgzxg1i2") (r "1.56")))

(define-public crate-readonly-0.2.10 (c (n "readonly") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "18ly32hxlzayck04gq6j4f7x7aibln6myc0ilbaigfahssb1xs6z") (r "1.56")))

(define-public crate-readonly-0.2.11 (c (n "readonly") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0x7masgw57xyymvax2w7xdh1fv1fic2sckcmd7h2z5362zd3kx5q") (r "1.56")))

(define-public crate-readonly-0.2.12 (c (n "readonly") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sws48fymmjvfrinbqhizyiszw7pc9gj3qnlw76zvddz84g66pd2") (r "1.56")))

