(define-module (crates-io re ad readext) #:use-module (crates-io))

(define-public crate-readext-0.0.1 (c (n "readext") (v "0.0.1") (h "0ana36vvhwdggcxl8li1n551y6grszwhqklqpgfz9688wr3s638k")))

(define-public crate-readext-0.1.0 (c (n "readext") (v "0.1.0") (h "19kjd8gvrb82b47ic8wjkbzzxc0q8vnk9fyfamxk9kwby7smip5b")))

