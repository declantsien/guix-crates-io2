(define-module (crates-io re ad read-byte-slice) #:use-module (crates-io))

(define-public crate-read-byte-slice-0.1.0 (c (n "read-byte-slice") (v "0.1.0") (d (list (d (n "fallible-streaming-iterator") (r "^0.1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.12.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.12.3") (d #t) (k 2)))) (h "00r4z40vxiffn9xqlif0dwj3m5mv8la27vb9ybd6p2grmsmkl5rv")))

(define-public crate-read-byte-slice-0.1.1 (c (n "read-byte-slice") (v "0.1.1") (d (list (d (n "fallible-streaming-iterator") (r "^0.1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.12.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.12.3") (d #t) (k 2)))) (h "0zmw7lizzy1lv5sb27n1pvcp4w5x3f7hamhp5yjai3pdkwam7cyg")))

(define-public crate-read-byte-slice-0.1.2 (c (n "read-byte-slice") (v "0.1.2") (d (list (d (n "fallible-streaming-iterator") (r "^0.1.5") (d #t) (k 0)))) (h "0glki9z2jmkmnrb8vl0g80alqry9yxfazxf4v9x5pii8fjwj03ry")))

