(define-module (crates-io re ad read_chars) #:use-module (crates-io))

(define-public crate-read_chars-0.1.0 (c (n "read_chars") (v "0.1.0") (h "162yg3jzbi7nkghjrzzrr55i1qnw5b4n86swyzhnbfcq6xl6akar")))

(define-public crate-read_chars-0.2.0 (c (n "read_chars") (v "0.2.0") (h "05g4k023ymw9fmsvxxh0ga5h2pkij7j4w140db74dvwsiiznyks9")))

(define-public crate-read_chars-0.3.0 (c (n "read_chars") (v "0.3.0") (h "1kpvsqr8k64g8clzcs0qzxk3i2v00wignswasrn8zy8iwvdnpxn5")))

