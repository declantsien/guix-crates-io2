(define-module (crates-io re ad readifchange) #:use-module (crates-io))

(define-public crate-readifchange-0.1.0 (c (n "readifchange") (v "0.1.0") (h "0f8j835f2sivxvqjxc5v64346c31mpyl131pg9qipj2gzv26s52y") (y #t)))

(define-public crate-readifchange-0.2.0 (c (n "readifchange") (v "0.2.0") (h "14zni8vxkmz6znys7l9dml2zab5wblnjkmji6r5rhjq1b1axg54f") (y #t)))

(define-public crate-readifchange-0.3.0 (c (n "readifchange") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03g5jf6sva8d0zxvn2f61y0vhln7qm3b3sg7q1zp9cj2d6wy5r10") (y #t)))

(define-public crate-readifchange-0.4.0 (c (n "readifchange") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8gmcgrlshmy81c18hfqcvlxsjwicjqx5c8jf4bfmvld7mrb59m") (y #t)))

