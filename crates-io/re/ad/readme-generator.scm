(define-module (crates-io re ad readme-generator) #:use-module (crates-io))

(define-public crate-readme-generator-0.1.0 (c (n "readme-generator") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "12a0wndrcr5s7lwdrkaf5dl2l1d0nnrlqd2nzda563hqck9mvnbx")))

(define-public crate-readme-generator-0.1.1 (c (n "readme-generator") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0f21iryw0nh56grm9p225dkw5anv9hf729s2hgd3ji04qwrhaifi")))

(define-public crate-readme-generator-0.1.3 (c (n "readme-generator") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0x5jzdf3ab7kjjp632yfkxphravpj9f54vrvsacjrmbmpl2bljzb")))

