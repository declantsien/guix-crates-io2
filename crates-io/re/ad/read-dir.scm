(define-module (crates-io re ad read-dir) #:use-module (crates-io))

(define-public crate-read-dir-0.1.0 (c (n "read-dir") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1xh18x1fs84bxddzgn74jrmrdp8c0igiv0v9d9wcsmhxvn0mccwl")))

(define-public crate-read-dir-0.2.0 (c (n "read-dir") (v "0.2.0") (h "1y1wwzsihnh44pnc370mxrsawi8b160cx90v787pidqyv4p3g14p")))

