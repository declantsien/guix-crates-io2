(define-module (crates-io re ad read-restrict) #:use-module (crates-io))

(define-public crate-read-restrict-0.1.0 (c (n "read-restrict") (v "0.1.0") (h "02dxc85hjzr9mj9i47k1kcdw3sv0dpw2p71dd0fsv8s58ir1h973")))

(define-public crate-read-restrict-0.1.1 (c (n "read-restrict") (v "0.1.1") (h "1892v62djxjnb1fq62vg1iqc1hcci4cdz3nmnv2xycb4j074nx8q")))

(define-public crate-read-restrict-0.2.0 (c (n "read-restrict") (v "0.2.0") (h "1djglccnyw6rlj8g7f2rja0658ij9sgfih37s10cx7p31ggwd2zm")))

(define-public crate-read-restrict-0.3.0 (c (n "read-restrict") (v "0.3.0") (h "16a99dd44b5739h8mvi1izzxrinrk2rdf6lq87yc8andx7dmi0lg")))

