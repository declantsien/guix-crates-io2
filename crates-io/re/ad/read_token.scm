(define-module (crates-io re ad read_token) #:use-module (crates-io))

(define-public crate-read_token-0.0.0 (c (n "read_token") (v "0.0.0") (d (list (d (n "range") (r "^0.0.1") (d #t) (k 0)))) (h "0qmyz4n6frlicr0zgg0qqik2agf45nc6psb7xhcmjx9yvb9x40hx")))

(define-public crate-read_token-0.0.11 (c (n "read_token") (v "0.0.11") (d (list (d (n "range") (r "^0.0.9") (d #t) (k 0)))) (h "162knd8rr5zvi7jclnjcbjxxi8ra9mv5c3y7wqc1j0nscs4wjgki")))

(define-public crate-read_token-0.1.0 (c (n "read_token") (v "0.1.0") (d (list (d (n "range") (r "^0.1.0") (d #t) (k 0)))) (h "1za3w63d61lkgg6wpp4fbid258ll14xfl634yh6zvazgwm7rzc9x")))

(define-public crate-read_token-0.1.1 (c (n "read_token") (v "0.1.1") (d (list (d (n "range") (r "^0.1.0") (d #t) (k 0)))) (h "0fnq4zrc9jjy5p4jq1bp6g6vjfhawl1cqch7vxkshd82lf5xxncb")))

(define-public crate-read_token-0.1.2 (c (n "read_token") (v "0.1.2") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "0s3qbbmx5x14d57f1ydfppwxjrdb92vh2i4zxvca40d1pyqdi8b9")))

(define-public crate-read_token-0.1.3 (c (n "read_token") (v "0.1.3") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "1bp99kiba4ssvbv5r4p1f4q2azg1wi0gjpcp2x2jvq7g6f7k6x3x")))

(define-public crate-read_token-0.2.0 (c (n "read_token") (v "0.2.0") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "0v4yzplsxgv7cywd24j64v428jqpd6kmmv9hc3l3d1npqijppilv")))

(define-public crate-read_token-0.2.1 (c (n "read_token") (v "0.2.1") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "111y0q64i6gjc2z4dil1hcil6yrjpsbr3im593v8wz593qlmkxjb")))

(define-public crate-read_token-0.2.2 (c (n "read_token") (v "0.2.2") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "0scpih4a8flw8jw8dk8iq16p41x4lky8qbcv2xmi00xkjm5ldbnc")))

(define-public crate-read_token-0.3.0 (c (n "read_token") (v "0.3.0") (d (list (d (n "range") (r "^0.2.0") (d #t) (k 0)))) (h "01a6mq7qsx6xj7c1clw8qk476sj07ka133cpgd9awv43zcydprby")))

(define-public crate-read_token-0.4.0 (c (n "read_token") (v "0.4.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "0f4qpnayfdwpqmymb431ap1n4gyz11i70fz2yqjnbgi5xy2x19li")))

(define-public crate-read_token-0.5.0 (c (n "read_token") (v "0.5.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "1jhdkqxxybmshizr5z015zkb6q4vlp95zjya3dza05swi77mzgxs")))

(define-public crate-read_token-0.6.0 (c (n "read_token") (v "0.6.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "1an3q4nz10m6qysrgx308dsp0mm57przpf4hwrnbqzwyv8dz1cyw")))

(define-public crate-read_token-0.6.1 (c (n "read_token") (v "0.6.1") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "0zd06j4ril9ajkp0nws5l0b8dycy5cm0rs6kiqlr9adf89zmj0ni")))

(define-public crate-read_token-0.6.2 (c (n "read_token") (v "0.6.2") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "1i9x0144h153783jh2vcvr8918r3r7pc03zzj59kprx98an0hs3x")))

(define-public crate-read_token-0.7.0 (c (n "read_token") (v "0.7.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "14s6cf0ji6v8kcq7p3azbva5bb99ykqwkixbr1d0zxhlyy9j1zlr")))

(define-public crate-read_token-0.8.0 (c (n "read_token") (v "0.8.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "14p8sj82690pj6bxgcpbhs8vf9qfgqaklznqdlw0q53c9sfliigb")))

(define-public crate-read_token-0.9.0 (c (n "read_token") (v "0.9.0") (d (list (d (n "range") (r "^0.3.0") (d #t) (k 0)))) (h "0jmh9gq5q463gpa4zl746c5ab3f12j8lmik578851zxl6hhh7gfl")))

(define-public crate-read_token-1.0.0 (c (n "read_token") (v "1.0.0") (d (list (d (n "range") (r "^1.0.0") (d #t) (k 0)))) (h "0f0ni4g3rf1ywykb4w1df9mxy5lhj0zf8kfnrzmajl5cycbfy7q5")))

