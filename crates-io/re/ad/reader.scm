(define-module (crates-io re ad reader) #:use-module (crates-io))

(define-public crate-reader-0.1.0 (c (n "reader") (v "0.1.0") (h "0zzigdhr3khfg3g921chhx7x63cp041h1v9a83zwzhdhigkwsc45")))

(define-public crate-reader-0.1.1 (c (n "reader") (v "0.1.1") (h "1jw25hs9pzmdzd7lz0p39h6whmqm3cavjj19ir5jzcl97yr812iz")))

(define-public crate-reader-0.1.2 (c (n "reader") (v "0.1.2") (h "0dmygxfqjzdzg9bcc1ny6j22afkkiqbv2wv0dfxg2czi9w69s23m")))

(define-public crate-reader-0.1.3 (c (n "reader") (v "0.1.3") (h "1z9xrb1csklis8sajdjwa6z8rzgavxvlzmwj36ignapria7kdb14")))

(define-public crate-reader-0.1.4 (c (n "reader") (v "0.1.4") (h "1dm7nw4q65ybn413s1nv1lrrwmi4x770z1hkhs69lcd1614xd7nv")))

