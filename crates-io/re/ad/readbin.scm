(define-module (crates-io re ad readbin) #:use-module (crates-io))

(define-public crate-readbin-0.1.0 (c (n "readbin") (v "0.1.0") (h "0nwwfazx0mlc4rxi5dvylwhgpzi5hd4llny5zx0cnw2zmy459594")))

(define-public crate-readbin-0.2.0 (c (n "readbin") (v "0.2.0") (h "0rdp935cjqc5spq6d5ckc4mky1zharzzmxbvsaw4f6gz7nxg74fr")))

