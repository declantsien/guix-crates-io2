(define-module (crates-io re ad read-write-store) #:use-module (crates-io))

(define-public crate-read-write-store-0.1.0 (c (n "read-write-store") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1jx4a50awlbdwp4wl84rds270fg1rdiban8ymjlj0b6almi31z4s")))

(define-public crate-read-write-store-0.2.0 (c (n "read-write-store") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "parking_lot") (r "^0.2") (d #t) (t "cfg(not(loom))") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13gsdidw71f91r7dwandiyp6jvz8gpgi61ly124db3q7d68s7jji")))

