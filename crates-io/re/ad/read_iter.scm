(define-module (crates-io re ad read_iter) #:use-module (crates-io))

(define-public crate-read_iter-0.1.0 (c (n "read_iter") (v "0.1.0") (h "1m1633ak82s6k7fdas0hl5kc8mf6dmacq0zn92zr574xc6sl60pa")))

(define-public crate-read_iter-0.1.1 (c (n "read_iter") (v "0.1.1") (h "1cv65khi3hskcd2iv5l10cai4sr60ph3jzp5lm3sgsx1z6mx18wy")))

(define-public crate-read_iter-0.1.2 (c (n "read_iter") (v "0.1.2") (h "1hnvsqsiyzcrxwa7lw1fc3l98wy7a1n3xx957657b9npsb01j0j6")))

