(define-module (crates-io re ad readwise) #:use-module (crates-io))

(define-public crate-readwise-0.0.0 (c (n "readwise") (v "0.0.0") (h "1kl8yv77gmxrklgk3m5flgsjpihaiiakby6whcyw2mbdpk3qvxr3")))

(define-public crate-readwise-0.1.0 (c (n "readwise") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v3wf040s1r6s73wdyzn9vbh59ym90gg4baymyd19c2h0prz76f7")))

(define-public crate-readwise-0.2.0 (c (n "readwise") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07mip7j0x7yfpympvbpmcwwmg0iya5bn57zfrbmih8ypwd8vy13w")))

(define-public crate-readwise-0.3.0 (c (n "readwise") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xaa90a7bx0a7h3a6hlv602j1vadnfmf24v3fk5iy7gig2br75pw")))

(define-public crate-readwise-0.3.1 (c (n "readwise") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "0if88qmxzc060cv80zjd494542p1pi4dnr59vajg3691dn8g0366")))

(define-public crate-readwise-0.4.0 (c (n "readwise") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "snafu") (r "^0.7.2") (d #t) (k 0)))) (h "10khdbj1w75ds3vsp1n7y2wgiby10jzdwcij293jvdbdlddq8kwb")))

