(define-module (crates-io re ad read_until_slice) #:use-module (crates-io))

(define-public crate-read_until_slice-0.1.0 (c (n "read_until_slice") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "net"))) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0868mr20vmw54b8mag2wicydkqjw1hh2azf7kanm1nfd610pb66a")))

(define-public crate-read_until_slice-0.1.1 (c (n "read_until_slice") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "net"))) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0vf24997hxh4gip2i6fqv76sajhghagns3zz84827y3iz1idivj3")))

(define-public crate-read_until_slice-0.1.2 (c (n "read_until_slice") (v "0.1.2") (d (list (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "net"))) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0bhv93vncga3mvy8m35bipcwq1khsgglp4z0h56g53zwj1dhb67p")))

(define-public crate-read_until_slice-0.1.3 (c (n "read_until_slice") (v "0.1.3") (d (list (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "net"))) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0gxnsa94xnzl1qdjz51zp1hwzclbkga105ba1ygxbb227h30vhmv")))

(define-public crate-read_until_slice-0.1.4 (c (n "read_until_slice") (v "0.1.4") (d (list (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("macros" "rt" "net"))) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0qwbr1ikp0y7i2fjrd25cl3d0rqg6ra3ajwfnxz39lp4qhy4fds1")))

