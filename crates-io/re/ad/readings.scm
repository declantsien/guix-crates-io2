(define-module (crates-io re ad readings) #:use-module (crates-io))

(define-public crate-readings-0.1.0 (c (n "readings") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotters") (r "^0.2") (d #t) (k 0)))) (h "1k3vw065bh2vbs0jm0ffw6b09m6im7bgsbsdpvsd3dsy46xv9188")))

(define-public crate-readings-0.1.1 (c (n "readings") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotters") (r "^0.2") (d #t) (k 0)))) (h "0hks373c1iy16mr7g6fgyhnkgh6ndyhlwzi67j2gq04fv1v473ri")))

(define-public crate-readings-0.1.2 (c (n "readings") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotters") (r "^0.2") (d #t) (k 0)))) (h "0cgzrff1rkgqsi49g8i9z8blnlxcpw1rj1142d74cil4pqk0kkm1")))

(define-public crate-readings-0.1.3-alpha.0 (c (n "readings") (v "0.1.3-alpha.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotters") (r "^0.2") (d #t) (k 0)))) (h "0vsxx3id9zh46rh7hpdl7s2yh0yfwslplah7l9q6aj3mgk5byfdb")))

(define-public crate-readings-0.1.3 (c (n "readings") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotters") (r "^0.2") (d #t) (k 0)))) (h "1svbl1v1wn4rnilpacz3ixlcxmprsakc12v30an1agkhlglbi0b0")))

