(define-module (crates-io re ad readervzrd) #:use-module (crates-io))

(define-public crate-readervzrd-0.1.0 (c (n "readervzrd") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10z91hc7c060sg1z8hlrfaw2hdvc4y383j1kwgzkd499062bv984")))

