(define-module (crates-io re ad read-process-memory-ptrace) #:use-module (crates-io))

(define-public crate-read-process-memory-ptrace-0.1.4 (c (n "read-process-memory-ptrace") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mach") (r "^0.3.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "basetsd" "minwindef" "handleapi" "memoryapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qx9rzfb7b2whcly9sxr59acrys3a1bxxabnayvwv6kdv27p0k8n") (r "1.56")))

