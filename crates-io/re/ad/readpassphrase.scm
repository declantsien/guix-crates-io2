(define-module (crates-io re ad readpassphrase) #:use-module (crates-io))

(define-public crate-readpassphrase-0.0.1 (c (n "readpassphrase") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "readpassphrase-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0kxawwczwvsswa4p31w60yw7zsbyy8g5wm6i232flr23mxx239qz")))

(define-public crate-readpassphrase-0.1.0 (c (n "readpassphrase") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "readpassphrase-sys") (r "^0.0.1") (d #t) (k 0)))) (h "194a0f0y1pfkzdchw0ymjrs1pz84rpf5w420gd41yr8ljp0ashgq")))

(define-public crate-readpassphrase-0.2.0 (c (n "readpassphrase") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "readpassphrase-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0c7piy62zk9s5fl0bng602j1bhp2dfhvb9p9a5b4h0iv1k3n7azi")))

