(define-module (crates-io re ad read-primitives) #:use-module (crates-io))

(define-public crate-read-primitives-0.1.0 (c (n "read-primitives") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "19c76sv4hj43zhpwfb9h9yz85l2jj9byfkbsmgdggrah3g7llq4v")))

(define-public crate-read-primitives-0.1.1 (c (n "read-primitives") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0f7akdf4bnwmd6vgwii46mq1p3aj6gp4bi5krr6hgjw8xcin0y6c")))

