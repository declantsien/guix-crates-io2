(define-module (crates-io re ad readable-regex) #:use-module (crates-io))

(define-public crate-readable-regex-0.1.0-alpha1 (c (n "readable-regex") (v "0.1.0-alpha1") (d (list (d (n "fancy-regex") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0y18whjw4y8ghhxzm0kix8maim7ncqw35snav6qijc1j6p1r0kzk") (f (quote (("re-fancy" "fancy-regex") ("re" "regex") ("presets" "once_cell") ("default" "re"))))))

