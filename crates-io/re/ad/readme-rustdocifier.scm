(define-module (crates-io re ad readme-rustdocifier) #:use-module (crates-io))

(define-public crate-readme-rustdocifier-0.1.0 (c (n "readme-rustdocifier") (v "0.1.0") (h "1n5chamxvxwvzx4lp3k58a4a9hsnsbh5f0v6hp0b8vyikpbjw08y")))

(define-public crate-readme-rustdocifier-0.1.1 (c (n "readme-rustdocifier") (v "0.1.1") (h "1b6faqngdahbkx1kryxypir3g8l8j5qm5q6wbj71m2x045dpdb88")))

