(define-module (crates-io re ad readme-api) #:use-module (crates-io))

(define-public crate-readme-api-0.1.0 (c (n "readme-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1g9hdq5wyx399kv6spdd2sj79dbkqlgglbnb1bml8lj60yl9z348")))

