(define-module (crates-io re ad readable-inlined-runtime) #:use-module (crates-io))

(define-public crate-readable-inlined-runtime-0.1.0 (c (n "readable-inlined-runtime") (v "0.1.0") (h "1spj4xzbifgry6q2rmqyb198s2gx6vd4asrkbywr72yrj5y8sbfa")))

(define-public crate-readable-inlined-runtime-0.1.1 (c (n "readable-inlined-runtime") (v "0.1.1") (h "02lwvjyf336rmni6r3vnl1zxk4vb44bzkh4k58p9g69scvd257r4")))

(define-public crate-readable-inlined-runtime-0.1.2 (c (n "readable-inlined-runtime") (v "0.1.2") (h "1286bc58kracr55ccbxc75s3zzxgqs2f06qgzm221ii8kqzg63nq")))

