(define-module (crates-io re ad read_write) #:use-module (crates-io))

(define-public crate-read_write-0.1.0 (c (n "read_write") (v "0.1.0") (h "0mgp1qi4a5f5468zrf8l161rawisnlcp21pdwrhlqi1k7rbbj7bs")))

(define-public crate-read_write-0.1.1 (c (n "read_write") (v "0.1.1") (h "18h0fspzxfzhqjjrzb6xb1gvcrg6avmrahj7p973akqrm3415z1f")))

(define-public crate-read_write-0.1.2 (c (n "read_write") (v "0.1.2") (h "1rpysfb16c8fp5qf2crjq8ypyhsc99rqywzwcq6gvlf4617n25j5")))

