(define-module (crates-io re ad read_input) #:use-module (crates-io))

(define-public crate-read_input-0.1.0 (c (n "read_input") (v "0.1.0") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "0fg40ljmll5fq0hjm6g0hv033rwv5lmnq9zhdj32fw74qf9b48ib") (y #t)))

(define-public crate-read_input-0.0.1 (c (n "read_input") (v "0.0.1") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "0v7p1r7q9ack46p95wfy9h20skdabrif5n6341wgycqcxl8sx36w")))

(define-public crate-read_input-0.0.2 (c (n "read_input") (v "0.0.2") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "1cggfxwrkprgg49db50s2493wnw12h39hfwcq8xm7qqv77zgdx94")))

(define-public crate-read_input-0.1.1 (c (n "read_input") (v "0.1.1") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "04apc766za5pi8915bk1axhnyjsw4n9wq0xcacwdz3vr4pqqcd3k")))

(define-public crate-read_input-0.2.1 (c (n "read_input") (v "0.2.1") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "0la53hqh5s2lfr8s6gz133l3i5kgy8iawp1lrq22l6401aa6c4dh")))

(define-public crate-read_input-0.3.0 (c (n "read_input") (v "0.3.0") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)))) (h "0z2mmr1j3m1yjv8y90ikxn7flfcpnl6i310ffq8fz7wg34pdqi1f")))

(define-public crate-read_input-0.3.1 (c (n "read_input") (v "0.3.1") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1n9la3vgs1nigj6y8g5rmvqqn51xpx7sx2j4s6f80ds71kvfjn4g")))

(define-public crate-read_input-0.3.2 (c (n "read_input") (v "0.3.2") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "089z65199xxkxcg02w3ilyvhjdad9ig7zmh2jr0km0azgwgm2ywx")))

(define-public crate-read_input-0.3.3 (c (n "read_input") (v "0.3.3") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "16qg285ja5v1hmysxva1al9rnx3247a2cc20dm9q6hrilsw4hbk7")))

(define-public crate-read_input-0.3.4 (c (n "read_input") (v "0.3.4") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0akn29szxln2rf3kv7wk9yf92pq9q8qmvj4pzai88jmnwfk1yxpk")))

(define-public crate-read_input-0.3.5 (c (n "read_input") (v "0.3.5") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "11jdkk0imkiydq3xi10vnf13xd13jwfhcvl236pqhcda0wq4qcwz")))

(define-public crate-read_input-0.3.6 (c (n "read_input") (v "0.3.6") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1xpyjrwbs0y9w1k9cj47y90vmnl04lckzr78m8mvwkx937h9y9wc")))

(define-public crate-read_input-0.3.7 (c (n "read_input") (v "0.3.7") (d (list (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1xzlqi1x2wd711bwa0k8zznry39lgziyygbd30md2i03m9gs414p")))

(define-public crate-read_input-0.3.8 (c (n "read_input") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0jr5dxx1bbx1x29lpwb77g3255g433ycw9p1ql4lwvywjmam30gk")))

(define-public crate-read_input-0.4.0 (c (n "read_input") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "17qjk9cv2pd4pb815ic1jacxww2156lx5jwn1znbpngiqc13llv2")))

(define-public crate-read_input-0.4.1 (c (n "read_input") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1walc1zm27yxvxplybsk23406jdhnn6wy3qp5q620pg7y1dr27m6")))

(define-public crate-read_input-0.4.2 (c (n "read_input") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "10zqnn4irzk3yrhwygqdjkk9smpqbzn143xjy4sz6fzk1sh1nkg8")))

(define-public crate-read_input-0.4.3 (c (n "read_input") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0ighc5hwx8rznl11whxmdkf8h8y1mfgxxhlswlxfjsdccjlrqrc7")))

(define-public crate-read_input-0.4.4 (c (n "read_input") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0kr8q5nzkldjkvfxqwx53n2jxnfj59c7qa20avwb3vxyj2p9ws4l")))

(define-public crate-read_input-0.4.5 (c (n "read_input") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0nijn4ki55lszldi8m2xxy0gq82m0qxsfc22jl8vx59lj8c8sdgr")))

(define-public crate-read_input-0.5.0 (c (n "read_input") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1hx5z6kzq55179kbf4d023kizs12ch7slwa2yzhy91kvyds6qig1")))

(define-public crate-read_input-0.5.1 (c (n "read_input") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0gbaxdhwd08faddn6hid07j50552fch88qvxwn0698lznrsm19j0")))

(define-public crate-read_input-0.5.2 (c (n "read_input") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "190bidi8asx0i1vn721a02nbpfq83vychzlsjz22rqgnsn0gc80s")))

(define-public crate-read_input-0.5.3 (c (n "read_input") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0908lib42vmphxnd3hqiscw8qvnjgbch27p0qssapd44z9hcgsp3")))

(define-public crate-read_input-0.5.4 (c (n "read_input") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "1pc3waa7zydrnqgfbrizj11dyxhxni11q38yff00f6iyw4x9kka0")))

(define-public crate-read_input-0.6.0 (c (n "read_input") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "0g7slpyk1hi7lii03w2jk0kgjc792kbn9a5mrwgpl7qqj1ncrggh")))

(define-public crate-read_input-0.6.1 (c (n "read_input") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "12zppblv0c19wqxb807fmn71wff057q2f043iqff2hjzcrsg2wfy")))

(define-public crate-read_input-0.6.2 (c (n "read_input") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "1i21wfg66i4kgmgdkq4qkzg8w23787q3m73zfqpkp7r99r6jrncg")))

(define-public crate-read_input-0.7.0 (c (n "read_input") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "0sx6s6vr75grdapdqri5ig6dl885pldkh5rwcic4xbfpa3h06cmc")))

(define-public crate-read_input-0.7.1 (c (n "read_input") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "1547mlj3j1v7912knl2b4s2jmdkbmb5mpjhfg26myfs5rydia5hp")))

(define-public crate-read_input-0.7.2 (c (n "read_input") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "17w46knsq2rg04483wzbz7ncrr2v9xnn7k9c56vgra8ybyksmirh")))

(define-public crate-read_input-0.8.0 (c (n "read_input") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "0k1xzp7fjk3rxpm21g74mbpd90cc9pq99r8q5whh6ajq9dg9b3zl")))

(define-public crate-read_input-0.8.1 (c (n "read_input") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "0qkcnq1v1ang1wfr1fh9h0585cq6xmh2rp2r86mkdxy2v65xl2ik")))

(define-public crate-read_input-0.8.2 (c (n "read_input") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 2)))) (h "0zhyx17cjgi43mkapvr7466bv2li8424jvmi9klayy89nddnh20k")))

(define-public crate-read_input-0.8.3 (c (n "read_input") (v "0.8.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 2)))) (h "0cqm7ac0a0yddan2nqys45q9wpv8y3nla7pg1pmp0sk2c3qk5g1y")))

(define-public crate-read_input-0.8.4 (c (n "read_input") (v "0.8.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 2)))) (h "1rah032dx715arzhrqaffj7czdyha7x28vl2rryyp8iqcp61hxdm")))

(define-public crate-read_input-0.8.5 (c (n "read_input") (v "0.8.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 2)))) (h "09fcn9qnyk0k0w5qkbpmbix5k5vfqwfip11l161qyfwmhxjs451m")))

(define-public crate-read_input-0.8.6 (c (n "read_input") (v "0.8.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 2)))) (h "026n4c932bkgxkd3fqxq6zd95ml9m4snf3dkc2vms01xv9s8c5rg")))

