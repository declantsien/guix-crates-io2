(define-module (crates-io re ad readline-rs) #:use-module (crates-io))

(define-public crate-readline-rs-0.1.0 (c (n "readline-rs") (v "0.1.0") (h "178rzqvn0q9djdn70vlj054958g2n983y7f31v2g0qm5kz7sdwgg") (y #t) (l "readline")))

(define-public crate-readline-rs-0.1.1 (c (n "readline-rs") (v "0.1.1") (h "0jy4j3vzlsacp51njn60vx640nv8wpm779s51phb9advsyky9a26") (y #t) (l "readline")))

(define-public crate-readline-rs-0.1.2 (c (n "readline-rs") (v "0.1.2") (h "168cv3q38yxhm25m5xhnz3mdlql1gbgqg4r6dav565shjami0m7k") (y #t) (l "readline")))

