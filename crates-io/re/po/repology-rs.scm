(define-module (crates-io re po repology-rs) #:use-module (crates-io))

(define-public crate-repology-rs-0.1.0 (c (n "repology-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0xm1fs2kavlb47k0aywmrqjdlg58b73i4w0hlv6c493cjbm9l7dn")))

