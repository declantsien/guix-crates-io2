(define-module (crates-io re po repomon) #:use-module (crates-io))

(define-public crate-repomon-0.1.0 (c (n "repomon") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0jkgz4jz1zikfs0185sbh5paninlzhkg8a0clxasgjkkpjpl3106")))

(define-public crate-repomon-0.1.1 (c (n "repomon") (v "0.1.1") (d (list (d (n "bincode") (r "^0") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("serde" "use_std" "v5"))) (d #t) (k 0)))) (h "0sqfd888ynk0r05cajp3km1dz9l1hblymk9cw0807fhiz45apvnz")))

