(define-module (crates-io re po repoutil) #:use-module (crates-io))

(define-public crate-repoutil-0.5.0 (c (n "repoutil") (v "0.5.0") (h "0n2r1cmkcpiir3c0cr5bav0p317ma4c78dzwypkyizj3k24h9and")))

(define-public crate-repoutil-0.11.0 (c (n "repoutil") (v "0.11.0") (h "16xvdi3g3lxnb0mdnvinx6iq1gb2dd4m8l9kdc7zq0jp6axmj5q0")))

(define-public crate-repoutil-0.11.1 (c (n "repoutil") (v "0.11.1") (h "1np9vnfghclhcc6wkwkyxshdy6wn4j3vw2xb5qbw5grpwm84jrmn")))

(define-public crate-repoutil-0.12.0 (c (n "repoutil") (v "0.12.0") (d (list (d (n "shellexpand") (r "^1.0") (d #t) (k 0)))) (h "0zk7ypric3siih1lnr4mz9i76iay1d8zz3wkcvz0a9qzcw2wn1i9")))

(define-public crate-repoutil-0.15.0 (c (n "repoutil") (v "0.15.0") (d (list (d (n "shellexpand") (r "^1.0") (d #t) (k 0)))) (h "04rhvh8awqcxxf7haj5rhv45s7zb4pizp4qhr2d9045l5286c8l2")))

(define-public crate-repoutil-0.16.0 (c (n "repoutil") (v "0.16.0") (d (list (d (n "shellexpand") (r "^1.0") (d #t) (k 0)))) (h "1ljagzw9ggjhd63427qnhgy1sirjwfaaf9gi827cdclqsqaz11c8")))

(define-public crate-repoutil-0.16.1 (c (n "repoutil") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)))) (h "1kj49brrc8z95qfxq9dksppvjd85kyg5w4calln9pyfll86b8qmv")))

(define-public crate-repoutil-0.17.1 (c (n "repoutil") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17vdkzsb1j924kr2sxvkbr0spmjkl4vlrgsfi72wl17xh801474k")))

