(define-module (crates-io re po reporters) #:use-module (crates-io))

(define-public crate-reporters-0.1.0 (c (n "reporters") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lettre") (r "^0.10.0-rc.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1h98rdpc1c68lap3zl0c1w0y6b15mvh3pp6bkhp4w3imd5myi9n2")))

