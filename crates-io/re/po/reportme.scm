(define-module (crates-io re po reportme) #:use-module (crates-io))

(define-public crate-reportme-0.1.0 (c (n "reportme") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("tls" "json"))) (k 0)))) (h "02s44i36rkm9xy61fh7qcvgwjccpfmyw41bgmbrf3ggrwp2a07d5")))

(define-public crate-reportme-0.1.1 (c (n "reportme") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("tls" "json"))) (k 0)))) (h "125clxkyraisr1n03f689rca84s89lw535y02gbag67sy5mdq6gv")))

(define-public crate-reportme-0.2.0 (c (n "reportme") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("tls" "json"))) (k 0)))) (h "12iigap46ask6w8mf6ccmb60c4n0ikbm8fs55wc98nwf82viiz7m")))

(define-public crate-reportme-0.2.1 (c (n "reportme") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("tls" "json"))) (k 0)))) (h "0423lv3zwsbmg3yr3v3a72la30ibnwc7smf8f57zy40k2ba23svy")))

