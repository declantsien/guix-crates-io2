(define-module (crates-io re po report-test) #:use-module (crates-io))

(define-public crate-report-test-0.0.0 (c (n "report-test") (v "0.0.0") (h "10qc85s8gzz0v3k23h62bbgh4czzi8y58fvkrnyj1bcj66c87k1l")))

(define-public crate-report-test-0.1.0 (c (n "report-test") (v "0.1.0") (d (list (d (n "enclave-runner") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.2.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.6.0") (d #t) (k 0)))) (h "1b5z10lzz31b75anr5hrdj4irzcmp8l07k6rr6ajqdqwgy7z219x")))

(define-public crate-report-test-0.2.0 (c (n "report-test") (v "0.2.0") (d (list (d (n "enclave-runner") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.7.0") (d #t) (k 0)))) (h "18zf665rs2x6y0ab548g3fnlij3n6bdz8pg9af5qj38c41wpf8j1")))

(define-public crate-report-test-0.3.0 (c (n "report-test") (v "0.3.0") (d (list (d (n "enclave-runner") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.7.0") (d #t) (k 0)))) (h "0ic9d60qrd3fj9kpqk6an8jjbrafkdgwhw0fzzh0s4rs3mj6dvjj")))

(define-public crate-report-test-0.3.1 (c (n "report-test") (v "0.3.1") (d (list (d (n "enclave-runner") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.7.0") (d #t) (k 0)))) (h "1hpmx4r8fspra7j9hlwpcagns0lw3d49mlyfqykfpb8i6qgf67z0")))

(define-public crate-report-test-0.3.2 (c (n "report-test") (v "0.3.2") (d (list (d (n "enclave-runner") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.3.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.7.0") (d #t) (k 0)))) (h "12vvqsj29zqrylddxib6p82ffcqxihxy90ai0yq0dq2nm08q1shn")))

(define-public crate-report-test-0.3.3 (c (n "report-test") (v "0.3.3") (d (list (d (n "enclave-runner") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.4.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.7.0") (d #t) (k 0)))) (h "0z2w599bngbqwf5k9pxlvqr3mqyamlnjbq6ldlg6pkc0kp8pw8n8")))

(define-public crate-report-test-0.4.0 (c (n "report-test") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "enclave-runner") (r "^0.6.0") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.4.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.8.0") (d #t) (k 0)))) (h "0gqsf5lz5dzn77yk1sz1wzqil0b5r2g2s7vm497ga5dl6j3dgcy4")))

