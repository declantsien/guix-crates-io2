(define-module (crates-io re po reportportal_common_api) #:use-module (crates-io))

(define-public crate-reportportal_common_api-0.1.0 (c (n "reportportal_common_api") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.6") (d #t) (k 1)))) (h "1qhdhy50kfl8rpr7rakz1kwfrf3avkbh9xn0zwdfgn9lv9kyjs4s") (y #t)))

(define-public crate-reportportal_common_api-0.1.1 (c (n "reportportal_common_api") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.6") (d #t) (k 1)))) (h "0hm2j1fq0410jrm33vgp4vwpda87nsb4is4lchcn00ad0yd4qf2h") (y #t)))

(define-public crate-reportportal_common_api-0.2.0 (c (n "reportportal_common_api") (v "0.2.0") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "1a68wn6gzqhxr8qj6r9sgysiirr05202r2z7db9mv61hdndjy2g1") (y #t)))

(define-public crate-reportportal_common_api-0.2.1 (c (n "reportportal_common_api") (v "0.2.1") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "0i5rk5rklz4n14qa27rqw012kmqdy507amc1xs2nsv3ir6n3aarg") (y #t)))

(define-public crate-reportportal_common_api-0.2.2 (c (n "reportportal_common_api") (v "0.2.2") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "1avf0brbp49gv4d2jvr6x6hdg08w334jl343fxni41vfsfxj7fr7") (y #t)))

(define-public crate-reportportal_common_api-0.2.3 (c (n "reportportal_common_api") (v "0.2.3") (d (list (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.6") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "1h99wqhskhvw3c6f04rs9618skp4qplpbbhv4ip2g7blz87jf7sz")))

