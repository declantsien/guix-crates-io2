(define-module (crates-io re po repo-utils) #:use-module (crates-io))

(define-public crate-repo-utils-0.3.0 (c (n "repo-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^4.0.13") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0nhn4j1zabxywhsh2v8620i8q2bqp8f2vkapa049nx33smh41s6r")))

