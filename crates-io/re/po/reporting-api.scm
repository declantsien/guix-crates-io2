(define-module (crates-io re po reporting-api) #:use-module (crates-io))

(define-public crate-reporting-api-0.1.0 (c (n "reporting-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0gn72hx3d068v923raipbanzgysxr0w53xl5b8xgpmxz1ls04kac")))

(define-public crate-reporting-api-0.2.0 (c (n "reporting-api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0srpkpdg66v4w0ab3pjkws8p5dspd12qdwbrzr02hc9k4n0x7ja2")))

(define-public crate-reporting-api-0.3.0 (c (n "reporting-api") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07shfiklmcdfhfh1p17f2sp0gy31fdlqnvfrrmwjlpln76vlkhch")))

(define-public crate-reporting-api-0.3.1 (c (n "reporting-api") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bqxdlsafwkkvirn4gsh0zl8zf1lxrjrcjh1nv54bvpxi8hckkqf")))

