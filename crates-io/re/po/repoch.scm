(define-module (crates-io re po repoch) #:use-module (crates-io))

(define-public crate-repoch-0.1.0 (c (n "repoch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0ddz4mfj80s5g8g38nrzw6nibcdq9b2a5fvvhd1hc6hhvra15pvk")))

(define-public crate-repoch-0.1.1 (c (n "repoch") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "16876pa4vqg1cm2cdqkbg7cfhv88dzjb3wakdx9ajcmxvfr0vdcb")))

