(define-module (crates-io re po repoman) #:use-module (crates-io))

(define-public crate-repoman-0.0.1 (c (n "repoman") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1s9xf4ypmr23dhlrlk8mykivn5dy1lx0vqzw1m64km10r4a5f32v")))

(define-public crate-repoman-0.0.2 (c (n "repoman") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "ha-utils") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "074mkip88hfrs4ys802gy9hn0aqc55nzzvb979g0iw01nicvy8an")))

