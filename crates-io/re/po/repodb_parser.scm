(define-module (crates-io re po repodb_parser) #:use-module (crates-io))

(define-public crate-repodb_parser-0.1.0 (c (n "repodb_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "196663gyxj6b8mkpw9jrckdc4mgp8nzsx2ff53mvdiws5j9j5frz")))

(define-public crate-repodb_parser-0.2.0 (c (n "repodb_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "0p41pxh7in74lkqmjb1rxflf26pmqz8jj3k1hd0jlm6pgzhlnvij")))

(define-public crate-repodb_parser-0.2.2 (c (n "repodb_parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "11i6sr5z98wdi2drz8k5l3ag5bah0nqivfxlyc45fbbrgqxfhmig")))

