(define-module (crates-io re po repose) #:use-module (crates-io))

(define-public crate-repose-0.1.0 (c (n "repose") (v "0.1.0") (d (list (d (n "base64") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libarchive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)))) (h "1blwv42dbqy1q2wvrh2i2g40svzc0w5ywx60mpq22wzp2xmpjxnl")))

