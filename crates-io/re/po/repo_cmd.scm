(define-module (crates-io re po repo_cmd) #:use-module (crates-io))

(define-public crate-repo_cmd-0.1.0 (c (n "repo_cmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "16gp93diyzp8ypvx2fdlzk563y0qcifih2a83rfikpg0nayif5l1")))

(define-public crate-repo_cmd-0.1.1 (c (n "repo_cmd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1n4qzwg8001pcicg95k7a1hmf4zxn1nssqa1l6ml5lnnfild5vw6")))

(define-public crate-repo_cmd-0.1.2 (c (n "repo_cmd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0q7qjr7rd4x9sm5ki3mal7q149r8n3y2x30986jrk535lxw5d2xw")))

