(define-module (crates-io re po repomon-config) #:use-module (crates-io))

(define-public crate-repomon-config-0.1.0 (c (n "repomon-config") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0llb73q1h2fwq9kzh1rmazlka5gchgzwy21dsn587k62f36fvpkk")))

(define-public crate-repomon-config-0.2.0 (c (n "repomon-config") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1z9pwipbav8mx1fqa27c6bya3ghi6w3mymnm6h71jzwipa4blngy")))

(define-public crate-repomon-config-0.3.0 (c (n "repomon-config") (v "0.3.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1rdl7apv34bam153vpnyy2wamcg3wkl1hhkp9wvda94zg6nrdr4q")))

(define-public crate-repomon-config-0.3.1 (c (n "repomon-config") (v "0.3.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0w52jaasp0clr0x4vydkfhldld7kihhw79mdb0hj2kv3z6c9nrwn")))

