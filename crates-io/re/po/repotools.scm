(define-module (crates-io re po repotools) #:use-module (crates-io))

(define-public crate-repotools-0.0.0 (c (n "repotools") (v "0.0.0") (h "0d7163nl086h42rcda0mahib5dzsbgwn0qq88p65zfrjmpr58wv3") (y #t)))

(define-public crate-repotools-0.0.9000 (c (n "repotools") (v "0.0.9000") (h "0a5xjmwlmcal5sx3vmpsf470f4clr32pya3zg4hyqszbnsabb9ay") (y #t)))

(define-public crate-repotools-0.1.0 (c (n "repotools") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0mxf1vly52vi66nk5yd3c98jddaapic6506qmpxvq6pr1c8wq49b")))

(define-public crate-repotools-0.2.0 (c (n "repotools") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0i4xjxkcilnwy823ia88anzibcf0zalmip6p1gy6akh6jckf3ks1")))

(define-public crate-repotools-0.3.0 (c (n "repotools") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "148aq2s3149x3c4gvvafrdixd56mv4v2l9yhlqqklmjmpf23x7c4")))

(define-public crate-repotools-0.4.0 (c (n "repotools") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0f8lg7pg9hb5y34ql6g4rb8yrqv01lg7darxrcmi29iapz212ky6")))

(define-public crate-repotools-0.5.0 (c (n "repotools") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0gcskykp9pnkz4nbihkmzagmr46nd21vb7pkxzv9nl5phrjd3l7a")))

(define-public crate-repotools-0.6.0 (c (n "repotools") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1vx2vf33d0bzlrg2mmlwpgz9lifbn2rqngnjz9chdy53v3qh6wm2")))

(define-public crate-repotools-0.7.0 (c (n "repotools") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1k575ybb7cf7flbkhwqy7dm2285fdabasvr8p89z2g4pl679d49p")))

(define-public crate-repotools-0.7.1 (c (n "repotools") (v "0.7.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1asbg1n3s041n5hgrdl920xw6fp2s08qapc0p4a28w21gi9nkws2") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le") ("cm") ("aa"))))))

(define-public crate-repotools-0.7.3 (c (n "repotools") (v "0.7.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1q61ip17p6681rhqbnfgaq10hna8q7y00jiw4b7a35iib8jpfyyj") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le") ("cm") ("aa"))))))

(define-public crate-repotools-0.7.4 (c (n "repotools") (v "0.7.4") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1z33wqva9darkji1y8zj77zm8vhs4l8lxwda8hlzq4qlhw5mzgkr") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le") ("cm") ("aa")))) (y #t)))

(define-public crate-repotools-0.7.5 (c (n "repotools") (v "0.7.5") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1nhb5a5grs92y98bfbr007pdkm9xjxgx2086xzmb284z2nsxllfy") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le") ("cm") ("aa"))))))

(define-public crate-repotools-0.8.0 (c (n "repotools") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "152r34ykfys65f2l4w8iw1rqx23c9w2yl7102p0rzsmkhr1gvn9v") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le") ("cm") ("aa"))))))

(define-public crate-repotools-0.9.0 (c (n "repotools") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)))) (h "0s15h3yac604fa446xdlnzmrs1jwyxm29jjgfrbqggwswj4khqf5") (f (quote (("ud") ("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le" "ud") ("cm") ("aa"))))))

(define-public crate-repotools-0.10.0-rc1 (c (n "repotools") (v "0.10.0-rc1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)))) (h "1823hg055i7lq14h9pl74ms8ig3gq29zrncbhwnakbv89scdcz7v") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le" "sq") ("cm") ("aa")))) (s 2) (e (quote (("sq" "dep:ulid"))))))

(define-public crate-repotools-0.10.0-rc2 (c (n "repotools") (v "0.10.0-rc2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)))) (h "0v8qansjk2l1zb9v4fcapa7zx6w4rbglg7zs3pxw6rvvvdw295f3") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le" "sq") ("cm") ("aa")))) (s 2) (e (quote (("sq" "dep:ulid"))))))

(define-public crate-repotools-0.10.0-rc3 (c (n "repotools") (v "0.10.0-rc3") (d (list (d (n "clap") (r "^3") (f (quote ("std" "yaml"))) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)))) (h "0ii7c7ilrw1svhs7kkmnbznmypm04m119hjkkcsi01yjc9bwqpvz") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le" "sq") ("cm") ("aa")))) (s 2) (e (quote (("sq" "dep:ulid"))))))

(define-public crate-repotools-0.10.0 (c (n "repotools") (v "0.10.0") (d (list (d (n "clap") (r "^3") (f (quote ("std" "yaml"))) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "ulid") (r "^1") (o #t) (d #t) (k 0)))) (h "0c9xrsc98mkv6cc4749g9a90rsy33f1s2v1vl88kwqlxv0dznpkr") (f (quote (("st") ("pu") ("le") ("dp") ("di") ("default" "dp" "aa" "st" "di" "cm" "pu" "le" "sq") ("cm") ("aa")))) (s 2) (e (quote (("sq" "dep:ulid"))))))

