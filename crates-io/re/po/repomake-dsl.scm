(define-module (crates-io re po repomake-dsl) #:use-module (crates-io))

(define-public crate-repomake-dsl-0.0.0-reserve (c (n "repomake-dsl") (v "0.0.0-reserve") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fakemap") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pd1lzn84cr9vfdmrn2bx1iyf2ivi4fqm5mbyqnibfqn4nzwrk96")))

