(define-module (crates-io re po repos) #:use-module (crates-io))

(define-public crate-repos-0.1.0 (c (n "repos") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "093r0p2z4q8j19psqah03l4ivhn7l82d5p8y7jz32m14y6mdqwl9")))

(define-public crate-repos-0.1.1 (c (n "repos") (v "0.1.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02r560pxg0kk5dhc13fsacdaw2l0rjs430zmrafyg5j6cnv5yfby")))

(define-public crate-repos-0.1.2 (c (n "repos") (v "0.1.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1k26ijbcspslpqkigdl6jm93fgbbaf93djhygcz09qkakvqlyq41")))

(define-public crate-repos-0.1.3 (c (n "repos") (v "0.1.3") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jpvc9nz44wr2paa0vw9zqjqhqcb0pqizrcyalr1vnv6p4y5ly2c")))

(define-public crate-repos-0.1.4 (c (n "repos") (v "0.1.4") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0f7gfnyp5kr1ayjg4jrbigy2igk2mqc0x5mgn271xm9fjr031id8")))

(define-public crate-repos-0.1.5 (c (n "repos") (v "0.1.5") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jbsz0qqqwv4ahvp7474393x5jmrk5ngpz0kpqcgj6i4dx7vfqb7")))

(define-public crate-repos-0.1.6 (c (n "repos") (v "0.1.6") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1nbc1aas09wjlmiipvfjk3bjhjamhx6jsxiyry2lga50i1mlhadr")))

(define-public crate-repos-0.1.7 (c (n "repos") (v "0.1.7") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1abvwyphh0dn9r6qygi3g70pcxbpmp13c5q1w1fgyx7nddgc99pl")))

(define-public crate-repos-0.1.8 (c (n "repos") (v "0.1.8") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bw88frm1sgszxwcksvxifyqx5y2ymaspbs85pb3n9l9y2i8sb0w")))

