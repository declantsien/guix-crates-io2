(define-module (crates-io re po repos-tool) #:use-module (crates-io))

(define-public crate-repos-tool-0.1.0 (c (n "repos-tool") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "remove_empty_subdirs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0x6ic3hq3bbx2b1zwplh178hnxddyi609iaqpyl9r8lhda051z29")))

(define-public crate-repos-tool-0.1.1 (c (n "repos-tool") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "remove_empty_subdirs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1ph28wh70wk98akfjxg4g6lfavy9wq2ffmbydwx0jwsvw8vd3s5j")))

(define-public crate-repos-tool-0.1.2 (c (n "repos-tool") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "remove_empty_subdirs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "08cyx636m8a3xcpc5n7jaqa59qmkj41qwzf6js6c0lqvh2r2bp4d")))

(define-public crate-repos-tool-0.1.3 (c (n "repos-tool") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "remove_empty_subdirs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0n61sl1p4ll3b5hg4pslbp293p938qa0da8bd4krv02l71ycll7s")))

(define-public crate-repos-tool-0.1.4 (c (n "repos-tool") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "remove_empty_subdirs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "01gnpqjqwg212p4r1jxdh0flj02lqjwxr9axv18dkadqm7ccycnh")))

