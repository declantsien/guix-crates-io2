(define-module (crates-io re qo reqores-client-surf) #:use-module (crates-io))

(define-public crate-reqores-client-surf-0.1.0 (c (n "reqores-client-surf") (v "0.1.0") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)))) (h "00k0x1g5m3sdm9baf6v1f5jr1q46a2n2kwbrfxps592g5sj488sa")))

(define-public crate-reqores-client-surf-0.1.1 (c (n "reqores-client-surf") (v "0.1.1") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)))) (h "0hbxrn0zk59qrwpzmw44m229hswal072x6wsp2lw12cr98aagyga")))

(define-public crate-reqores-client-surf-0.1.2 (c (n "reqores-client-surf") (v "0.1.2") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)))) (h "1kx8iyfs7y2c0834ppsybpwzgkjkbbn8dg77924gy8h7lvdg6mcz")))

(define-public crate-reqores-client-surf-0.1.3 (c (n "reqores-client-surf") (v "0.1.3") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "surf") (r "^2") (k 0)))) (h "0p37pigijf551sva601d20sbqp12a2jipa87ky57zi0r0vr1rmi6") (f (quote (("wasm-client" "surf/wasm-client") ("middleware-logger" "surf/middleware-logger") ("hyper-client" "surf/hyper-client") ("h1-client-rustls" "surf/h1-client-rustls") ("h1-client" "surf/h1-client") ("encoding" "surf/encoding") ("default" "curl-client" "middleware-logger" "encoding") ("curl-client" "surf/curl-client"))))))

