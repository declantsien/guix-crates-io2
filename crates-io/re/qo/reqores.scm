(define-module (crates-io re qo reqores) #:use-module (crates-io))

(define-public crate-reqores-0.1.0 (c (n "reqores") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06fw3jy24y26x39fv5a0bzmd71rhmncx1dssa56p5aivwv457szh")))

(define-public crate-reqores-0.1.1 (c (n "reqores") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fan1vqd6fqjwnpv3kzxmafl89dh5j55jv3z0rdrnrqy8j01bgq1")))

(define-public crate-reqores-0.1.2 (c (n "reqores") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "19kkqbrl10r2vbmqis228iyi7z4fm4g5vi8f220prfv5wx9fkzg4")))

(define-public crate-reqores-0.1.3 (c (n "reqores") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08wvxs2ygfnxq70chy9wm7aliabmhwii52fkwjdwrprfwzfn0b5z")))

(define-public crate-reqores-0.1.4 (c (n "reqores") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vad17gkwhr4mr64wg0wdgmpfnc7f2h52hngxrnxfk2sd7yq19xw")))

(define-public crate-reqores-0.1.5 (c (n "reqores") (v "0.1.5") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0lhp2nbri6qs28mwlb2p2jymarklddirv00689lz3lpkfgz303nb")))

(define-public crate-reqores-0.1.6 (c (n "reqores") (v "0.1.6") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15xlbmcg50xs3znma6yhf4hl8i4n3vr1sd3mkzag842ldzxphg3m")))

(define-public crate-reqores-0.1.7 (c (n "reqores") (v "0.1.7") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v0z2a4mvrx9zmfvscc7nkw3l2q33314nk1gs6yhlca8n08w5jky")))

