(define-module (crates-io re qo reqores-universal-cf-worker) #:use-module (crates-io))

(define-public crate-reqores-universal-cf-worker-0.1.0 (c (n "reqores-universal-cf-worker") (v "0.1.0") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "worker") (r "^0.0.10") (d #t) (k 0)))) (h "0b7qyhgw787c7n2yisfy4q5i818y0np83lb2si2lrhshyjwbp3b8") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1.1 (c (n "reqores-universal-cf-worker") (v "0.1.1") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "worker") (r "^0.0.10") (d #t) (k 0)))) (h "0h4ra8ai0imp73k8vs45jf2c2wr4mwhmidcya09icwis9whqyw59") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1.2 (c (n "reqores-universal-cf-worker") (v "0.1.2") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "worker") (r "^0.0.10") (d #t) (k 0)))) (h "01as2gbdi03lpa1bf7nh7d1vl1vc1b6kbd09kmdjv9n6f4pq36n6") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1.3 (c (n "reqores-universal-cf-worker") (v "0.1.3") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "worker") (r "^0.0.11") (d #t) (k 0)))) (h "0pmd8azpvy7rkfrwwgn812b5800b36d8755889wcxlvhqwh96qx4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1.4 (c (n "reqores-universal-cf-worker") (v "0.1.4") (d (list (d (n "reqores") (r "^0.1") (d #t) (k 0)) (d (n "worker") (r "^0.0.11") (d #t) (k 0)))) (h "12lfz8washh1xig0991ziq5hl0imxv6xxzac8cvyn4aa2fpag05p") (f (quote (("server") ("default" "client" "server") ("client"))))))

