(define-module (crates-io re ga regalloc) #:use-module (crates-io))

(define-public crate-regalloc-0.0.1 (c (n "regalloc") (v "0.0.1") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "08z4lh2pyginwys6y9rh702ssvlr1cai7hs6idkfh9wg1di9pk5x")))

(define-public crate-regalloc-0.0.2 (c (n "regalloc") (v "0.0.2") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "09f1hyac9vwqdhff9my2wwhww2n6q6rbbppfzg2zz4h5myxblhjv")))

(define-public crate-regalloc-0.0.3 (c (n "regalloc") (v "0.0.3") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "003iv2svpzl5s4f568s8f25qa82h638p1b7pfr0h81ymhz6mx24m")))

(define-public crate-regalloc-0.0.4 (c (n "regalloc") (v "0.0.4") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "10j7nz5g5khm7ckp62jnh2200dy25h4ad1f4y7a4x1x9lkmr6rgd")))

(define-public crate-regalloc-0.0.5 (c (n "regalloc") (v "0.0.5") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0f1gmb3a4pi03pwxzr8yz49vah4v9hy7d4sldi2skskk4kjx8mya")))

(define-public crate-regalloc-0.0.6 (c (n "regalloc") (v "0.0.6") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "133k9dlb999kv8j7d17dbs2l0gdwb3dqh8cj9v0abrrkdbxcikyd")))

(define-public crate-regalloc-0.0.7 (c (n "regalloc") (v "0.0.7") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "14dkyfrz2k645qdmh6128sgxhnf93qa18l81w70m9wp7rb08pswc")))

(define-public crate-regalloc-0.0.8 (c (n "regalloc") (v "0.0.8") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "1ql1dhy311ww4p41n1k8wv6lhd6fjg014jbq73ya1kimlzdbk5d5")))

(define-public crate-regalloc-0.0.9 (c (n "regalloc") (v "0.0.9") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "1wk1dp0nzdffxf85pd7nlsw5la4xmdj6ahnrr97hjr8n8axh2811")))

(define-public crate-regalloc-0.0.10 (c (n "regalloc") (v "0.0.10") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "01jvsw7p5kbhdfq4znf08acj8qidq3gqsmqisfry4381jmj90ldx")))

(define-public crate-regalloc-0.0.11 (c (n "regalloc") (v "0.0.11") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0zdvwrjx2j6cbxwg6ya033n0cwhy4d17g8m4vq5pg3qzp5nhycva") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.12 (c (n "regalloc") (v "0.0.12") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0xi75bj052dwyipbw31frhnnslh8ms83qz4hwwkavrfqb5yn7mmz") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.13 (c (n "regalloc") (v "0.0.13") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "15agdgbhnszc3cvv62yz5qbwh065d8an7bcvcvbkz87fnxmnwp0f") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.14 (c (n "regalloc") (v "0.0.14") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0p74k7w4hr4ax2q2fzk39wxkf62gq46vcy9qgr0wkxywg8k9wihs") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.15 (c (n "regalloc") (v "0.0.15") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0vc5ycqxfn00qv8hn3bml824kxp4azfxp81ynswgcp531r24czbn") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.16 (c (n "regalloc") (v "0.0.16") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "1r0pknzcr53zfnh13w7sdqya8l4c8sqk7bq1phd5y90vjzj8v35v") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.17 (c (n "regalloc") (v "0.0.17") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "1mmxhgy2q4iik8wkrqfm99zfkhnh37pbw46hynxr2vps6pc0rkl9") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.18 (c (n "regalloc") (v "0.0.18") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1a800i0zrmlcijbdn86flrgbyv7ik0rjxv5ypn54h4yk1qcdzmy6") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.19 (c (n "regalloc") (v "0.0.19") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0fpipvg7hfcd3kqlg4y55ls6skvqq2pdc061b90rd6k53zbbb18h") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.20 (c (n "regalloc") (v "0.0.20") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "11nv82klg8f0s0rrr3vrl6i3f1ysxzijbar2c30b9mg0zsfnbn3m") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.21 (c (n "regalloc") (v "0.0.21") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "017qwh3hwwpnb8jyp0n18vszn56khnxbpf1p3cacaslq85mjayxj") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.22 (c (n "regalloc") (v "0.0.22") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1pnn9yvwzqjn876kdni8hz94qdcf90nxq2qa95f3bzl1chzrb4wr") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.23 (c (n "regalloc") (v "0.0.23") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0k0hnj7965rklryxmq663z66f8h540k2v6xjxgfn7qzn17m2frxk") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.24 (c (n "regalloc") (v "0.0.24") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1lmd2s6gxyi1l703cyikwhg8ap8x3h45knfrlvzr05jbib7bwhjq") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.25 (c (n "regalloc") (v "0.0.25") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1ygi1fnqdphigi5w0jq7jqd5b2zy1i60nrp4hjh5nv5nkn6b99fc") (f (quote (("fuzzing") ("default"))))))

(define-public crate-regalloc-0.0.26 (c (n "regalloc") (v "0.0.26") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "158x5zihv539dq2a05ykpk9hv353jlwdb7nq684n3zg0g4nhj0vw") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.27 (c (n "regalloc") (v "0.0.27") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1a17y2ljzf8ikqbfbgp34ngd4qc98y2yvbmxdmy31kz7bypqmfmr") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.28 (c (n "regalloc") (v "0.0.28") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0jinwryr5ify8qh2y6s0m1wgk2iw7agfy00bkqmggq2zi78bx61m") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.29 (c (n "regalloc") (v "0.0.29") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1fhfzsds47151vvnhbqn7k5vdrpzzibd67kfkhswv2ikd08cay61") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.30 (c (n "regalloc") (v "0.0.30") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1lfq924d9cayvi0bcsw11jdnf4mzv01lz5r8ykb4dwvg9z9w4h90") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.31 (c (n "regalloc") (v "0.0.31") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1ic1jdjpk4wh2ms90jj28g172cp0x0zqadydhm97bi31glwpy7sp") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.32 (c (n "regalloc") (v "0.0.32") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1x11qcxqvd5anzgklv4rp3bi73bv23m5bhrj7pd23nafaml48c56") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.33 (c (n "regalloc") (v "0.0.33") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "06mbx3x5rp5f8al1k7hx5f4r9pcacax75fa0klippjnzj7zqr03x") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

(define-public crate-regalloc-0.0.34 (c (n "regalloc") (v "0.0.34") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "00ncljiasvlw66avy9756327pcvp3nph0xw3d3f0p65z7qfnni32") (f (quote (("fuzzing") ("enable-serde" "serde") ("default"))))))

