(define-module (crates-io re re rererouter) #:use-module (crates-io))

(define-public crate-rererouter-0.1.0 (c (n "rererouter") (v "0.1.0") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0gwrrvqpshkbb0rrz20nblq6nqc3pkhh2sy2f6bn29wvyg6yi65q")))

(define-public crate-rererouter-0.1.1 (c (n "rererouter") (v "0.1.1") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "08969byanrrqwkp83yyz69c1ri5zrqsbh0bwlypzrrxvd1ija0b1")))

