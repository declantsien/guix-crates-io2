(define-module (crates-io re re reref) #:use-module (crates-io))

(define-public crate-reref-0.2.0 (c (n "reref") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-lock") (r "^9") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0jm5vsg3cwfjps3mli0zwsnhbpvqzmff652r8pqsql6s44gy6apd")))

