(define-module (crates-io re tz retz) #:use-module (crates-io))

(define-public crate-retz-0.1.0 (c (n "retz") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo" "error-context" "help" "std" "suggestions" "usage"))) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)))) (h "0p1171gx4c5dmn4iz21xy9jli49iwdh0bvwk8bid9wd7432p3c8n")))

(define-public crate-retz-0.1.1 (c (n "retz") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo" "error-context" "help" "std" "suggestions" "usage"))) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)))) (h "0y975whr2yjj2n4nz2nz1hn9kknvil7sx573037d848ba52bl8gc")))

(define-public crate-retz-0.1.2 (c (n "retz") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~4.4.18") (f (quote ("derive" "cargo" "error-context" "help" "std" "suggestions" "usage"))) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)))) (h "12fa23bi6v3kmcmx240lycbbd6aypkq967aj5rv8vxg115lp5prr") (r "1.70.0")))

