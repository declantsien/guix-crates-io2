(define-module (crates-io re te retest) #:use-module (crates-io))

(define-public crate-retest-0.2.3 (c (n "retest") (v "0.2.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "1szzxb2h2bbcs4fms0cx8qr33v9hql3dhs03j65bhfa460j744a1")))

