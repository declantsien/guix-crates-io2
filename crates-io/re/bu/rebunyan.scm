(define-module (crates-io re bu rebunyan) #:use-module (crates-io))

(define-public crate-rebunyan-0.1.0 (c (n "rebunyan") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.19") (f (quote ("serde-human-readable" "parsing"))) (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0pwjwy9q4vyn0p50qb1lxpic21mkakpv8nkz3a9cb7a992waj14a")))

