(define-module (crates-io re fu refuse) #:use-module (crates-io))

(define-public crate-refuse-0.0.1 (c (n "refuse") (v "0.0.1") (d (list (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "intentional") (r "^0.1.1") (d #t) (k 0)) (d (n "kempt") (r "^0.2.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1h2m1mlvsawr0x2ag1fx6lx0vk9hkxngkjcg4agpn5qrsg1q2l6r") (r "1.73.0")))

(define-public crate-refuse-0.0.2 (c (n "refuse") (v "0.0.2") (d (list (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "intentional") (r "^0.1.1") (d #t) (k 0)) (d (n "kempt") (r "^0.2.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "refuse-macros") (r "=0.0.2") (d #t) (k 0)))) (h "1bnyd5madwhnqk9bjda2wdzkymzzxh8bshc4164pq28cs7mrwbpd") (r "1.73.0")))

(define-public crate-refuse-0.0.3 (c (n "refuse") (v "0.0.3") (d (list (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "intentional") (r "^0.1.1") (d #t) (k 0)) (d (n "kempt") (r "^0.2.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "refuse-macros") (r "=0.0.3") (d #t) (k 0)))) (h "00jynp75838jfhlr6cgzqfa8ai10anf27p234qzbg8vdilh1p8z2") (r "1.73.0")))

