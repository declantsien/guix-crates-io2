(define-module (crates-io re fu refuse-macros) #:use-module (crates-io))

(define-public crate-refuse-macros-0.0.2 (c (n "refuse-macros") (v "0.0.2") (d (list (d (n "attribute-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "manyhow") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1zg3n2y8cfymkj7i3qpip23f4q1n4jq27m1qbz9jscsfvycjhr6l") (r "1.73.0")))

(define-public crate-refuse-macros-0.0.3 (c (n "refuse-macros") (v "0.0.3") (d (list (d (n "attribute-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "manyhow") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0gj7q7ihc717hydm237c0zknzf5vp437gpahs134l7a883mj6hg5") (r "1.73.0")))

