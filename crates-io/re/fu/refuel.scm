(define-module (crates-io re fu refuel) #:use-module (crates-io))

(define-public crate-refuel-0.1.0 (c (n "refuel") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hdw56adpnpy3vpxpvnm9vwr32bcnjjm6mwgyvb8f7w3iwzz8f4w") (y #t)))

(define-public crate-refuel-0.1.1 (c (n "refuel") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1amp0n99x0q1v5yhdx3vlvn3pf78cg7my4apm06vq53ld1ij4ipz")))

(define-public crate-refuel-0.1.2 (c (n "refuel") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diesel") (r "^2.1.6") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qyg0g32i9s5f9y46mi3a4f12a5swbxf0bgxl9q0lwcf20pjarfb")))

