(define-module (crates-io re fu refuse-pool) #:use-module (crates-io))

(define-public crate-refuse-pool-0.0.2 (c (n "refuse-pool") (v "0.0.2") (d (list (d (n "ahash") (r "^0.8.11") (f (quote ("runtime-rng"))) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "refuse") (r "=0.0.2") (d #t) (k 0)))) (h "1x25fw7bvvwg481lqq1kdd5y0h0ha3gb419ls5d3wqais4zrh9zl") (r "1.73.0")))

(define-public crate-refuse-pool-0.0.3 (c (n "refuse-pool") (v "0.0.3") (d (list (d (n "ahash") (r "^0.8.11") (f (quote ("runtime-rng"))) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "refuse") (r "=0.0.3") (d #t) (k 0)))) (h "1birp98c8062bn07dvrhnlas49rv8kbwhraxx6103p7qcadfs88s") (r "1.73.0")))

