(define-module (crates-io re fu refute) #:use-module (crates-io))

(define-public crate-refute-0.1.0 (c (n "refute") (v "0.1.0") (h "1knj6c8vp6d1nldacazwn9nv8s9yy60zyj6bs5bvfy3cjgnxfrwp")))

(define-public crate-refute-1.0.0 (c (n "refute") (v "1.0.0") (h "04wdy6xbv54z96ncdbjmgark2k1qp5yn0v440bgibfi4hlxfbizy")))

(define-public crate-refute-1.0.1 (c (n "refute") (v "1.0.1") (h "0y01vc3yj29bjzlph0yx745y9m4g42injd1cdkxmk437vxm8g7c2")))

