(define-module (crates-io re cg recgen) #:use-module (crates-io))

(define-public crate-recgen-0.1.1 (c (n "recgen") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "recgen-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1y5fzmpv7qqhpdc087h90xsr7ax93cabcp8rrmym8dm7b1mqql5w")))

(define-public crate-recgen-0.1.2 (c (n "recgen") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "recgen-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mqsf5bkv72vgpc5l45q7ckk563wqz16qqiyffwjzq38jaibig04")))

