(define-module (crates-io re cg recgen-sys) #:use-module (crates-io))

(define-public crate-recgen-sys-0.1.1 (c (n "recgen-sys") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1j7syqljsg1c4s3j2i3rlmdbs8vm12rwla9gbl77dl35qkh7vc7f")))

(define-public crate-recgen-sys-0.1.2 (c (n "recgen-sys") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1ky2lbbw7s046bxniqbw0gjys4588rl2mhr347j6yqxkf7wrpynf")))

