(define-module (crates-io re vc revc) #:use-module (crates-io))

(define-public crate-revc-0.1.0 (c (n "revc") (v "0.1.0") (h "17yf247vxwfiamg9i18kxxlnc9z2ygbrk4kdhh6ksyyicl2bqk4n")))

(define-public crate-revc-0.1.1 (c (n "revc") (v "0.1.1") (h "1x9wz00iwb8bm3qkmkzqc3yics3zhil4jvmmydma1lnk0aky7q2q")))

(define-public crate-revc-0.1.2 (c (n "revc") (v "0.1.2") (d (list (d (n "clap") (r "^2") (o #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zhh9902iph30b3lndzrcjjxmchbj6mlw6s5qi4h4jbm930fkhdb") (f (quote (("trace_resi" "trace") ("trace_reco" "trace") ("trace_pred" "trace") ("trace_dbf" "trace") ("trace_coef" "trace") ("trace_bin" "trace") ("trace") ("default" "binaries") ("binaries" "clap"))))))

(define-public crate-revc-0.1.3 (c (n "revc") (v "0.1.3") (d (list (d (n "clap") (r "^2") (o #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "y4m") (r "^0.6") (o #t) (d #t) (k 0)))) (h "059fvkzvy611gj720n4qc2b3c84xjp6jgy85k3cl2x8y1hc4xvy9") (f (quote (("trace_resi" "trace") ("trace_reco" "trace") ("trace_pred" "trace") ("trace_me" "trace") ("trace_dbf" "trace") ("trace_cudata" "trace") ("trace_coef" "trace") ("trace_bin" "trace") ("trace") ("default" "binaries") ("binaries" "clap" "y4m"))))))

(define-public crate-revc-0.2.0 (c (n "revc") (v "0.2.0") (d (list (d (n "clap") (r "^2") (o #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "y4m") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1942baxbjn772c0w374bw48ax9zdfa152a8bh6k019yq3am3n6jm") (f (quote (("trace_resi" "trace") ("trace_reco" "trace") ("trace_pred" "trace") ("trace_me" "trace") ("trace_dbf" "trace") ("trace_cudata" "trace") ("trace_coef" "trace") ("trace_bin" "trace") ("trace") ("default" "binaries") ("binaries" "clap" "y4m"))))))

