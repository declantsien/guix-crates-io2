(define-module (crates-io re ep reepal) #:use-module (crates-io))

(define-public crate-reepal-0.1.0 (c (n "reepal") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b02asmr25c0gdgi9i56pb3w54awqk8v97qaw982cgmal6kqq1y3")))

(define-public crate-reepal-0.1.1 (c (n "reepal") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0klfb8wkgvac4pd96j6ysb5wi54vf6wr014r8mx4vhq2vkbcs3fp")))

(define-public crate-reepal-0.1.2 (c (n "reepal") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "17yc6ijqwfhj2hdhljf8zwnhdb2vc8y9dw0aslkgrzc2nrfk2gzs")))

(define-public crate-reepal-0.1.3 (c (n "reepal") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "19msbzplbknl4swzqb67niiqjh82k6im5pacrhnz8rv5wxslf6ah")))

(define-public crate-reepal-0.1.4 (c (n "reepal") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mc4r3l0vv9icppv0h8xnya7nrsjxa9yxpvs1whxjpl0mgcrwm01")))

(define-public crate-reepal-0.1.5 (c (n "reepal") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z92adrdvzhgwfya3ay9bq52953sjxancsfjlrqg1lr73y1vmhg9")))

(define-public crate-reepal-0.1.6 (c (n "reepal") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0324s8218xaxqg8g3990x8q9p90vm97dk6a29b7kzl6j2lbcfv4w")))

(define-public crate-reepal-0.1.7 (c (n "reepal") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1959zhqq29qsl1rfflkln5a55nw4l4pxyjv9j05jmgz9gk5hkvg2")))

(define-public crate-reepal-0.1.8 (c (n "reepal") (v "0.1.8") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n7blfc35v15ibzry8xq4m3zg0v1yqjf7hsw9zr9vy7y1dxc6fs6")))

(define-public crate-reepal-0.1.9 (c (n "reepal") (v "0.1.9") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qhcb1dr97javjqi7r2rlixg4a23parlid1ivb39743w1vfcaadh")))

(define-public crate-reepal-0.2.0 (c (n "reepal") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dd8rm66v6gh1398gj5y3bdym0m28a8wjxiy398c403yi3k2kh5l")))

