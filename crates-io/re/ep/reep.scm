(define-module (crates-io re ep reep) #:use-module (crates-io))

(define-public crate-reep-0.1.1 (c (n "reep") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.2") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "18fcbwir174q0ykkxb6irrsfxdkr7w2vm4f8wiwxhhdnb20nlc2b")))

(define-public crate-reep-0.2.0 (c (n "reep") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "router") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "1.*") (d #t) (k 0)))) (h "07y1aa636i8x6c46qraizwy4dihyq9vgjzgd19ars5gjj2qdg17p")))

