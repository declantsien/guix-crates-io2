(define-module (crates-io re ep reep-id-string) #:use-module (crates-io))

(define-public crate-reep-id-string-0.1.0 (c (n "reep-id-string") (v "0.1.0") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "reep") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1blw8rgzivlfg9r1g2h7ly4qxf3mgyldvksp316dvcvmzmc0gqf5") (f (quote (("serde-support" "serde") ("rustc-serialize-support" "rustc-serialize") ("default"))))))

(define-public crate-reep-id-string-0.2.0 (c (n "reep-id-string") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "reep") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0gqj9dqwxlmpa6sg7mky01ir5z9dxxyfi899vqrb5wb7w4dxnfj1") (f (quote (("serde-support" "serde") ("rustc-serialize-support" "rustc-serialize") ("default"))))))

