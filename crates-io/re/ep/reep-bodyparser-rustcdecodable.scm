(define-module (crates-io re ep reep-bodyparser-rustcdecodable) #:use-module (crates-io))

(define-public crate-reep-bodyparser-rustcdecodable-0.1.0 (c (n "reep-bodyparser-rustcdecodable") (v "0.1.0") (d (list (d (n "bodyparser") (r "*") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "reep") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1pvikcnngxh2p6znacmlx3zdvg8ndjg64xc3bdmzj6cwxqpdqyqz")))

(define-public crate-reep-bodyparser-rustcdecodable-0.2.0 (c (n "reep-bodyparser-rustcdecodable") (v "0.2.0") (d (list (d (n "bodyparser") (r "^0.4") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "reep") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "00myb29f414kqpgqf1six4ns5gfy1yban32acpmcaz9a3ajvmk19")))

