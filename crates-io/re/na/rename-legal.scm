(define-module (crates-io re na rename-legal) #:use-module (crates-io))

(define-public crate-rename-legal-0.1.1 (c (n "rename-legal") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "16ndv6g63mi621n1i1x0fxm81p1rh8fvingnmf2sa4iq0nd1rl8h")))

(define-public crate-rename-legal-0.1.2 (c (n "rename-legal") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "067pkvn02gf7b5dahrl93204kli6m54yrflx2dawky1ddwlhg04i")))

