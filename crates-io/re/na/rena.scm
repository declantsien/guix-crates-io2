(define-module (crates-io re na rena) #:use-module (crates-io))

(define-public crate-rena-1.0.0 (c (n "rena") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "03swb2nqd3zs0kdmy4rvn11lg1jj9n91hij5c0r7nmrxhn23jbfd")))

(define-public crate-rena-1.0.1 (c (n "rena") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1ncb844l1cn8ml7npyfxfb6570i226mby5pq26jsg7yxfxigf4y3")))

(define-public crate-rena-1.0.2 (c (n "rena") (v "1.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0ym5a0z898cxxsxrgpa2day83r5lnwj2d8yw58jxa63r9rfms5x1")))

(define-public crate-rena-1.0.3 (c (n "rena") (v "1.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "118l4gkzxlcymiqqdyb0l495p5k1bs8mfqxkr5fzvscqack1b9wi")))

(define-public crate-rena-1.0.4 (c (n "rena") (v "1.0.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0lnv37zr85r72zd5s0d8j1pjdya73wn7c5fw2j3nfb84yk465cgr")))

(define-public crate-rena-1.1.0 (c (n "rena") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1s48bd6i86yrcs1ysmjzjizmn5vny1p9i5acl24vjlbl9pi3a6dw")))

(define-public crate-rena-1.1.1 (c (n "rena") (v "1.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0mn97mz38l0dr36yz6ch4byjljkqmbjii6gn5w28sbqwpl53g6g6")))

(define-public crate-rena-1.2.0 (c (n "rena") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "paris") (r "^1.5") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0and38xyr8rrxxng6z289gkwg7z1mfhbij946m3kkd8bzbmn1p35")))

(define-public crate-rena-1.3.0 (c (n "rena") (v "1.3.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("suggestions" "color" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.0") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "paris") (r "^1.5.9") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "03l8l1gb4pkpvf13kn7149v7605jb0zn0fwj7c9d8bmzam3l7vnm")))

(define-public crate-rena-1.3.1 (c (n "rena") (v "1.3.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("suggestions" "color" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "paris") (r "^1.5.11") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1d2xq2f1706gy6nh05blcpmrnawpkkyajprgl92k8ad9xyk0djs4")))

(define-public crate-rena-1.4.0 (c (n "rena") (v "1.4.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("suggestions" "color" "wrap_help" "cargo" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "paris") (r "^1.5.15") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "strfmt") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "1gxhz7361km6w50vwnywx3lc5cxdx81ya52zw96lg4741vwwh4a0")))

