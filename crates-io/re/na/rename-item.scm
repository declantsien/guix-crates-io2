(define-module (crates-io re na rename-item) #:use-module (crates-io))

(define-public crate-rename-item-0.1.0 (c (n "rename-item") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (k 0)) (d (n "quote") (r "^1.0.18") (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("parsing" "proc-macro" "full"))) (k 0)))) (h "0hsa40hjaismq46y1s7gimnh8laq58wc0dvsxxlgxpnbgsz1yawg")))

(define-public crate-rename-item-0.1.1 (c (n "rename-item") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.1") (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (k 0)) (d (n "quote") (r "^1.0.28") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("parsing" "proc-macro" "full"))) (k 0)))) (h "18b6jrhfp2x9jpbvi5g4q66nwksr41pgnripi7lbnag554bja2zq")))

