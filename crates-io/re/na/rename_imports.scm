(define-module (crates-io re na rename_imports) #:use-module (crates-io))

(define-public crate-rename_imports-0.1.0 (c (n "rename_imports") (v "0.1.0") (h "0lpf12hn97jp1b2n6syh2q6zg1frv7dbkkxxz11mjih3sxdmg8s4")))

(define-public crate-rename_imports-0.1.1 (c (n "rename_imports") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1mla2zgrs5mh885zgaigzbnv4sjfz1jm1r77fg5zsg4piiss3vi9")))

