(define-module (crates-io re na renamore) #:use-module (crates-io))

(define-public crate-renamore-0.1.0 (c (n "renamore") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "077d6c7v2br5p93gqmx6nbxgd26f5146vzvjhfhcpphiyvnf8jg0")))

(define-public crate-renamore-0.2.0 (c (n "renamore") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00hwxndn2cm6gidz7k3brgl5ivx2g8l5bxmd5jx0pwkfk5n49c29")))

(define-public crate-renamore-0.3.0 (c (n "renamore") (v "0.3.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08571vvhy9vmz1hbifyda0fgbvvm1nlrqi9g2cv3q6brr2mckdzq")))

(define-public crate-renamore-0.3.1 (c (n "renamore") (v "0.3.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zj2c8bm959nda6z3xw6k6a2qmb26scmsx5048pcvr9ah5bnzzb5") (f (quote (("always-supported") ("always-fallback"))))))

(define-public crate-renamore-0.3.2 (c (n "renamore") (v "0.3.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0w1npjj8q26c7js5kvhabdq1ibakv0ag7a31i846vwldl7ccwnqg") (f (quote (("always-supported") ("always-fallback"))))))

