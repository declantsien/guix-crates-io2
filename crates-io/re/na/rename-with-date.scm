(define-module (crates-io re na rename-with-date) #:use-module (crates-io))

(define-public crate-rename-with-date-0.1.0 (c (n "rename-with-date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "018jg8fkh05vg611sxxx0d936fyj9ynb09vfcdri2vyw4mbnd5m4")))

(define-public crate-rename-with-date-0.1.1 (c (n "rename-with-date") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "03avnrihy1lvn7fp2idg8n9zq8a5zdajp6xbb71i47ihjxg4q6k5")))

(define-public crate-rename-with-date-0.1.2 (c (n "rename-with-date") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1dbhhz1mzfii9mk5mbhgvyn7byj64ss4llqgwvljfxb1f6v2fhhi")))

