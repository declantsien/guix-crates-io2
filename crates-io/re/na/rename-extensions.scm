(define-module (crates-io re na rename-extensions) #:use-module (crates-io))

(define-public crate-rename-extensions-0.2.0 (c (n "rename-extensions") (v "0.2.0") (d (list (d (n "clap") (r "~2.26.0") (d #t) (k 0)) (d (n "glob") (r "~0.2.11") (d #t) (k 0)))) (h "1aa94v76m9s65cwh6ckmqqshglzkbxck52d0ck7cy442vdh15ia9")))

(define-public crate-rename-extensions-0.3.0 (c (n "rename-extensions") (v "0.3.0") (d (list (d (n "clap") (r "~2.26.0") (d #t) (k 0)) (d (n "glob") (r "~0.2.11") (d #t) (k 0)))) (h "15mxf0z5gjcqj9p4ql840w31r03zwrjybz2p5zlqr3jr6179llwa")))

