(define-module (crates-io re na rename) #:use-module (crates-io))

(define-public crate-rename-0.1.0 (c (n "rename") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0ajfsi241d6mdn260k41kvwwf55mckamanah18m3nn7igvrw14s1")))

(define-public crate-rename-0.1.1 (c (n "rename") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5shz59zbsvswav0q6pkabnl972nj5nb9xbi5m31raivj4gd93a")))

