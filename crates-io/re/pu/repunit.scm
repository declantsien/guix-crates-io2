(define-module (crates-io re pu repunit) #:use-module (crates-io))

(define-public crate-repunit-0.1.0 (c (n "repunit") (v "0.1.0") (h "0bfdwp7m2dzib9hnkm0js6h0fb8fy93hcpvfjzyfaibr23iqyl1f")))

(define-public crate-repunit-0.1.1 (c (n "repunit") (v "0.1.1") (h "15lv7idr91y42z69j5c2msn5ilhhp9jhklszygzwrk6swdxnhvhh")))

(define-public crate-repunit-0.1.2 (c (n "repunit") (v "0.1.2") (h "1wnddx7cibzq1pral6i50cqg0pbvqadsl3kf92s6q5qp176lskmx")))

(define-public crate-repunit-0.1.3 (c (n "repunit") (v "0.1.3") (h "04brzpv8q6wqkbxsc72ximrx8q24vqc5j55blmzmr1w2l9liysxc")))

(define-public crate-repunit-0.1.4 (c (n "repunit") (v "0.1.4") (h "1rp8x1xx31xrd8w9kmvz0dh0g6svycr5dlhc0b2a4h3imkwk5p6s")))

(define-public crate-repunit-0.1.5 (c (n "repunit") (v "0.1.5") (h "1909m96i9a9r0bngii1isr2qn569zrryrgbxy341zbwk1mc22xhs")))

(define-public crate-repunit-0.1.6 (c (n "repunit") (v "0.1.6") (h "1axl36ha42gjx4p11rrfyd7y9xqa5m65f3n024imsihmsarsg8h2")))

