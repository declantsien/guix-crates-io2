(define-module (crates-io re pu repugnant-pickle) #:use-module (crates-io))

(define-public crate-repugnant-pickle-0.0.1 (c (n "repugnant-pickle") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0slspw61z8dpc4wrdb78cv3hkmy0w744c0k5gb991kmjp1gsd96l") (f (quote (("torch" "zip") ("default"))))))

