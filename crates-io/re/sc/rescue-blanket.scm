(define-module (crates-io re sc rescue-blanket) #:use-module (crates-io))

(define-public crate-rescue-blanket-0.1.0 (c (n "rescue-blanket") (v "0.1.0") (h "09bywhqmndxpqbmv40qbg151b2kwcd1rn3gq5l6damlwnm4pv7y5")))

(define-public crate-rescue-blanket-0.2.0 (c (n "rescue-blanket") (v "0.2.0") (h "1qqndn3phwg94chpxi9hfjsg8al7qvxmk4f9sp75z8xq28pdg7xw") (f (quote (("std") ("default" "std"))))))

