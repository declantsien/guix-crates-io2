(define-module (crates-io re sc resctrlfs) #:use-module (crates-io))

(define-public crate-resctrlfs-0.8.0 (c (n "resctrlfs") (v "0.8.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0dsnja3dna740hgrhdmkpfkrrs50w58hvddr7n6gyv2280i18gwh")))

(define-public crate-resctrlfs-0.8.1 (c (n "resctrlfs") (v "0.8.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0kll1gdr4mf1rsq3hn1xxwfg9p834h0nbl12m1i01hwab9540f98")))

