(define-module (crates-io re td retdec) #:use-module (crates-io))

(define-public crate-retdec-0.1.0 (c (n "retdec") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)) (d (n "multipart") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0y2p0cbcnfmmqr3h3s79w2zkrb8drxk7qxgpyh5s3cjps4jjkqh3")))

