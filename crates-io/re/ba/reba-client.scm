(define-module (crates-io re ba reba-client) #:use-module (crates-io))

(define-public crate-reba-client-0.1.0 (c (n "reba-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rlp") (r "^0.5.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.21.3") (f (quote ("alloc" "recovery"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (f (quote ("signing"))) (d #t) (k 0)))) (h "1g5jf61jdymk4f5j5n8ii2klgkdxkph83phj6v3q2r2nvqdsf5wl")))

