(define-module (crates-io re ba rebackup) #:use-module (crates-io))

(define-public crate-rebackup-1.0.0 (c (n "rebackup") (v "1.0.0") (d (list (d (n "atomic") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1wqwkq2j8whpw56047b73kbm6vymklpg0zchprwziy9qdmaksypq") (f (quote (("default" "cli") ("cli" "clap" "glob"))))))

(define-public crate-rebackup-1.0.1 (c (n "rebackup") (v "1.0.1") (d (list (d (n "atomic") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "064jv9i966whg2fy0xsny1jirymf9agfbgpznhpl26w7pyymnzhp") (f (quote (("default" "cli") ("cli" "clap" "glob"))))))

(define-public crate-rebackup-1.0.2 (c (n "rebackup") (v "1.0.2") (d (list (d (n "atomic") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1yxigdmnamr0fg12qzcnxi5s64rgzyp5i5ysh2l29qnjlf7sx2vn") (f (quote (("default" "cli") ("cli" "clap" "glob"))))))

