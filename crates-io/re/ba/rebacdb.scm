(define-module (crates-io re ba rebacdb) #:use-module (crates-io))

(define-public crate-rebacdb-0.1.0 (c (n "rebacdb") (v "0.1.0") (d (list (d (n "tokio") (r "^1.34") (f (quote ("io-util" "sync"))) (k 0)))) (h "15qn77lx0nif43w42f8im4hcbvl7gqkhpibrs32s529myiinc5kw")))

(define-public crate-rebacdb-0.1.1 (c (n "rebacdb") (v "0.1.1") (d (list (d (n "tokio") (r "^1.34") (f (quote ("io-util" "sync"))) (k 0)))) (h "0qkvqs75y8vvfyhlyckwgmknqqb98xap08q4nlvmlmbl50sq3wkc")))

