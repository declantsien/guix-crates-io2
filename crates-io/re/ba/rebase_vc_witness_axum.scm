(define-module (crates-io re ba rebase_vc_witness_axum) #:use-module (crates-io))

(define-public crate-rebase_vc_witness_axum-0.1.0 (c (n "rebase_vc_witness_axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.2") (d #t) (k 0)) (d (n "axum-macros") (r "^0.3.8") (d #t) (k 0)) (d (n "rebase_witness_sdk") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "0pwi10rhxk57pk46753ii4xz15b209f2g149ss10gmm7jmz70wiv")))

