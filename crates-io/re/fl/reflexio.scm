(define-module (crates-io re fl reflexio) #:use-module (crates-io))

(define-public crate-reflexio-0.1.0 (c (n "reflexio") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "05lkccrc92xjqdxmbjyfbvfh8d67j80smqphfprvfaacxgcjm9fg")))

