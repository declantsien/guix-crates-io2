(define-module (crates-io re fl reflective_pe_dll_loader) #:use-module (crates-io))

(define-public crate-reflective_pe_dll_loader-0.1.0 (c (n "reflective_pe_dll_loader") (v "0.1.0") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "libloaderapi"))) (d #t) (k 0)))) (h "0pdyvy6gfmphsnz7snnhvrn0q5vyih5p7wkjpxwdhyjik3yrp1z3")))

(define-public crate-reflective_pe_dll_loader-0.1.1 (c (n "reflective_pe_dll_loader") (v "0.1.1") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "libloaderapi"))) (d #t) (k 0)))) (h "1s0bl4vbjnz6gf7709d4qcqim7pqnx9sa8ahjzcfdpfcv6hmlzzc")))

(define-public crate-reflective_pe_dll_loader-0.1.2 (c (n "reflective_pe_dll_loader") (v "0.1.2") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "libloaderapi"))) (d #t) (k 0)))) (h "19yva5xl662yyxm6lrsj3qa53gnknvh0ky8xajqx0i2kh87011px")))

