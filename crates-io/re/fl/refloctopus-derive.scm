(define-module (crates-io re fl refloctopus-derive) #:use-module (crates-io))

(define-public crate-refloctopus-derive-0.0.1 (c (n "refloctopus-derive") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16xqpkvbp9q98yn7q7hc10fnnkly5w5az1vhjzc8pc6hk5z0d1vr") (f (quote (("default") ("attrs"))))))

