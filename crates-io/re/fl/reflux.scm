(define-module (crates-io re fl reflux) #:use-module (crates-io))

(define-public crate-reflux-0.1.0 (c (n "reflux") (v "0.1.0") (h "1jda9f03y5ck6gqix1n0q81pslkyayldk76imka9cs3jhxf6zpqk")))

(define-public crate-reflux-0.1.1 (c (n "reflux") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)))) (h "08lj47xnbxg0ph57h5p8wi3z203bflwl4w0bplz40448rrwcajrc")))

(define-public crate-reflux-0.1.2 (c (n "reflux") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)))) (h "1nkja9x5y5zfg5cn7hnxmi8lj6jyln93v7fgqdlshba4izgl7zvp")))

