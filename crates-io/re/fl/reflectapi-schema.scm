(define-module (crates-io re fl reflectapi-schema) #:use-module (crates-io))

(define-public crate-reflectapi-schema-0.1.0 (c (n "reflectapi-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wlz9syylzm0vs7hzjc4mkyr41xvgcl6cbq6qg25l56yfc2ylyy4")))

(define-public crate-reflectapi-schema-0.2.0 (c (n "reflectapi-schema") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "09zd5qcagyzqvrqdaakihyvv8k5w0f060rc856fxs9cwck1v9has")))

(define-public crate-reflectapi-schema-0.3.0 (c (n "reflectapi-schema") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nvbkc0hm6v8lsy7jvr6s0vnnmvzqaxmyw597d1a25y7zi39jkj0")))

(define-public crate-reflectapi-schema-0.4.0 (c (n "reflectapi-schema") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "16hk6cci53bamdzxs3c1mjg6l5hjpn6b6bqj3n7qxr1lvyj7j6af")))

(define-public crate-reflectapi-schema-0.4.1 (c (n "reflectapi-schema") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v4pnd27lf7y0d16v3ljjaaswgqn77qscxjwrbisa4nr72c7y2gs")))

(define-public crate-reflectapi-schema-0.4.2 (c (n "reflectapi-schema") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nz1k36sgb9hr58g6krm9zxwqx5hs9ixib39jb9qgwkxv973bkvj")))

(define-public crate-reflectapi-schema-0.5.0 (c (n "reflectapi-schema") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a7xa4dzg18rm5fbnz881847mx35z4j4dpih6z2r2kl43a7jpyfl")))

(define-public crate-reflectapi-schema-0.5.1 (c (n "reflectapi-schema") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "135w2slvx2ggma792p53fci2c0nmgd4d3rzhfh03l7x2pffcxc0m")))

(define-public crate-reflectapi-schema-0.5.2 (c (n "reflectapi-schema") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jq2gw3brimjdzp837721l3jgv984pp76gvy9lrbryyxl843xgy3")))

(define-public crate-reflectapi-schema-0.5.3 (c (n "reflectapi-schema") (v "0.5.3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wai6djdl8yqk5w5vliypbnhviqbcsix9j2pprkw2yc3ma7gn9sl")))

(define-public crate-reflectapi-schema-0.5.4 (c (n "reflectapi-schema") (v "0.5.4") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pciiyxm3b9yr2dx3jnsn2q3fvh7q59ylfhjj5n1amkpph2yxc56")))

(define-public crate-reflectapi-schema-0.5.5 (c (n "reflectapi-schema") (v "0.5.5") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "04p2n60n6qjfxh8dhh199vs6hvh650dr50dmdrqj5py7l3491vlh")))

