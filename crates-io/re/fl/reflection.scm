(define-module (crates-io re fl reflection) #:use-module (crates-io))

(define-public crate-reflection-0.1.0 (c (n "reflection") (v "0.1.0") (d (list (d (n "trees") (r "^0.1") (d #t) (k 0)))) (h "1aga90j8bis4nbh764zk0wm6x1baf46pbzw8hw16sqh6jbldl2gf")))

(define-public crate-reflection-0.1.1 (c (n "reflection") (v "0.1.1") (d (list (d (n "trees") (r "^0.1") (d #t) (k 0)))) (h "1y3pmlzqnavrpddmjswrsdnslyk7714b20rk0mxns9xpjnw04q20")))

(define-public crate-reflection-0.1.2 (c (n "reflection") (v "0.1.2") (d (list (d (n "trees") (r "^0.1") (d #t) (k 0)))) (h "0j78vl655pdi6b4qyl0jzhnzadxh8x3yn2hdwkcv05ay29g32652")))

(define-public crate-reflection-0.1.3 (c (n "reflection") (v "0.1.3") (d (list (d (n "trees") (r "^0.2") (d #t) (k 0)))) (h "0565hfa47g9xv99pnxjbc49cm5y03bcxw1saqrlnxmm4pmqs8xzl")))

