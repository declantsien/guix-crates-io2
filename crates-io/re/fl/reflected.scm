(define-module (crates-io re fl reflected) #:use-module (crates-io))

(define-public crate-reflected-0.1.0 (c (n "reflected") (v "0.1.0") (h "1xv9x1n5hmi4px39k5hbmpk33ri0nrkcqljcnji2hswnnimr8ak2")))

(define-public crate-reflected-0.1.1 (c (n "reflected") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reflected_proc") (r "^0.1.1") (d #t) (k 0)))) (h "13dxdqzinpl1fs9f357g1626s2wjibyyvz9wgh2fm6x0simzmbpy")))

(define-public crate-reflected-0.1.2 (c (n "reflected") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reflected_proc") (r "^0.1.3") (d #t) (k 0)))) (h "0h1synyyshbzl3jhyrlfdd20sfanz23maddv903yc5rhd59hl0cf")))

(define-public crate-reflected-0.1.3 (c (n "reflected") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h0y2b3a1vy6sxa6vkvzrpsqyq0lgbcaclkn6i1rr1afmapjkxv0")))

(define-public crate-reflected-0.1.4 (c (n "reflected") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nam3m7by3nhr0fckzkf628pshff6rqy38jq2zcpx9a389i2jgsd")))

(define-public crate-reflected-0.1.5 (c (n "reflected") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g113himg95k0v8cbyjif59ayx6l23d0np9hkfqn1bndvyzjx3nf")))

(define-public crate-reflected-0.1.6 (c (n "reflected") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19hmkllmwmj0wr8km178byf6i49d0w3ypndydnnbcz929bif3cxj")))

(define-public crate-reflected-0.1.7 (c (n "reflected") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kggnd5xd25p0nswg2zhki3awfq9mbid3k1lpywgfp9kakassx2d")))

(define-public crate-reflected-0.2.0 (c (n "reflected") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fj1v2za44vw0arsyzn2vkp9i9cr5211bjnpkjxm9cx9y5wq13hf")))

(define-public crate-reflected-0.2.1 (c (n "reflected") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fg5pwgyjgbz2qmpbx4yqqg7wkgffva1zk4vvy704pnw5rrcn6jh")))

(define-public crate-reflected-0.2.2 (c (n "reflected") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z646qdy141kin5hyf9x89wdqjz6pzwgppwg8d23skgwny0hd9jb")))

(define-public crate-reflected-0.2.3 (c (n "reflected") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0f19s6f4dnlfxyddwfbnw9lfdsgkly4211gipfrfv26h9a2n9dbi")))

(define-public crate-reflected-0.2.4 (c (n "reflected") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zf3zhnmx98p58f9hsr5ik6ba6y2d7aw9xdmk8fqjyl04jiyf5z2")))

(define-public crate-reflected-0.2.5 (c (n "reflected") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nrf075zyyakripwqf276p0g6p5013bnqcl8zdcp4mv1asa228lm")))

(define-public crate-reflected-0.2.6 (c (n "reflected") (v "0.2.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hivckvwrihf2w0f5f1k09sxvzs5a6kfw7caxvd96z1j15d0w7dl")))

(define-public crate-reflected-0.2.7 (c (n "reflected") (v "0.2.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rgc41aj78j5lw64g5w6wmyrrfci9axfyyl4pipicsa1ylzlfbsi")))

(define-public crate-reflected-0.2.8 (c (n "reflected") (v "0.2.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03r57jmx849nn8g1djhbghshpfjdyah13mxa255n91496n6vsd7q")))

(define-public crate-reflected-0.2.9 (c (n "reflected") (v "0.2.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "015xxy0yvk7f3adnn2zh4m82rysw4vr5f6dwkp1hlb3njwszbq9y")))

(define-public crate-reflected-0.3.0 (c (n "reflected") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1aqw74kd85misqz4rnjf0313a2y8l60d1n7kzplwz6kh860hskxd")))

(define-public crate-reflected-0.3.1 (c (n "reflected") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mwdn92lhid7zkyzmhc4lx0xrqi3s124gbgaza4zw26f84pff927")))

(define-public crate-reflected-0.3.2 (c (n "reflected") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1im6v49i89crri5pykxwsg3ngx5w6iahfnkgz5ikvbak1qalw4p4")))

(define-public crate-reflected-0.4.0 (c (n "reflected") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0jmvxlz7ghwkxs3va0knwzj6mp9gxf19g3iar8jdyx8mgjwrr2pb")))

(define-public crate-reflected-0.4.1 (c (n "reflected") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1bjw2iay8hzxdwv9jaj0pscjdcmx6np5n9xbx2374ss5bwrgy3cg")))

(define-public crate-reflected-0.4.2 (c (n "reflected") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1g2y86701g7bi64n4amwb7vk0fv8v0grqpry6r51c863l26s490h")))

(define-public crate-reflected-0.4.3 (c (n "reflected") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0rpqg819infbmm2l0f5kjsq0wilyip9y789nnvrwg7s6gcfqp17l")))

(define-public crate-reflected-0.4.4 (c (n "reflected") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "08897gf4733ralj5bxqjnscahrc5apvs4brn5id5hnn6k4j616y1")))

(define-public crate-reflected-0.5.0 (c (n "reflected") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "13791drvfqag0k9fcvqkdvykvklj5a6axzfsr540sxs00zwmazqg")))

(define-public crate-reflected-0.5.1 (c (n "reflected") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0xqnzn33sshsys0ikh622xvixvrw7578jqydw35r8kic0d7hpmrh")))

(define-public crate-reflected-0.5.2 (c (n "reflected") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1fh796n6qr2lw1i5jicrhqj35wz6m2pcmly6z43ckgzm76fni1my")))

(define-public crate-reflected-0.5.3 (c (n "reflected") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "15c590l9x53jj6agavkwp0yv8wzdyc3zmbwcm61j2jj660lycdap")))

(define-public crate-reflected-0.5.4 (c (n "reflected") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "175s7sz6f31gsa97alpj7dl6yjjpm0yyqjpr6kabrvx6hkaisrlj")))

(define-public crate-reflected-0.5.5 (c (n "reflected") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "05y76bh30z10jkxkh7jiw4s1n81qlxwg26xqa696rl82fcz7j11c")))

(define-public crate-reflected-0.6.0 (c (n "reflected") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0ikf0fbrdv7iq9jdjbfvam9iypx9n70l9cym0s9vqzhdw3dbzlxm")))

(define-public crate-reflected-0.6.1 (c (n "reflected") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "16yc6qa1crqv9xjlmbdak7bcbmvgx66his26ql3w3b3sxr322zhg")))

(define-public crate-reflected-0.6.2 (c (n "reflected") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1c7633km7mj8vzrjx7wynjc708j02p55sif8y5vgvfzv93ghjndv")))

(define-public crate-reflected-0.6.3 (c (n "reflected") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0xc18511mv9w2hlfjfll3x3ccf251zk0a2ssx1q2hysx3xirx03v")))

(define-public crate-reflected-0.6.4 (c (n "reflected") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1lknnf2p2sk3ysv7w3l9v7jywawkcqm9rccgcn94c6iyj9fkwfbm")))

(define-public crate-reflected-0.6.5 (c (n "reflected") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1jkwjcs55bzfy2k1fycnnzld9fvk0csv4q0q98xhvnlmpgk49fza")))

(define-public crate-reflected-0.6.6 (c (n "reflected") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1l3vjirhx3sdgs4qv7zxg09r2y1k5chzk7jlpaqq2nf06z8q4qm9")))

(define-public crate-reflected-0.6.7 (c (n "reflected") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1zh2p24k4kbxzw8p2d8q560qmgbfwc819sns4j5ygzz3g63513a7")))

(define-public crate-reflected-0.6.8 (c (n "reflected") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0bprbdh06kc5mjhhrkqllylrc5zr9aqckq99fid98mqh46s62cxj")))

(define-public crate-reflected-0.6.9 (c (n "reflected") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "0f3z36fjqdpdkap1wiv7z15bqgl8h60k5ks6n6z71pj03i37cbgn")))

(define-public crate-reflected-0.6.10 (c (n "reflected") (v "0.6.10") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "1b19qcqrp42q8ky786q7a6cx9yzkqp8clwd3a2nmqspmr8h5qa3v")))

(define-public crate-reflected-0.7.0 (c (n "reflected") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)))) (h "12y7pj5zyl41x1xrq1002jvbdi26i3qghiyljkvr5kvps0crkx7k")))

(define-public crate-reflected-0.7.1 (c (n "reflected") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "0af640ljcfvff2i1hi456w94wcgwfdg5waxk91qgrvmpr68h5kyp")))

(define-public crate-reflected-0.8.0 (c (n "reflected") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "17z37iwmqh539jr4ja572596cj0my7qpq0drznyghjr72gsgsgmm")))

(define-public crate-reflected-0.8.1 (c (n "reflected") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "1qsqqchl1bqzhkbj2dxg7cvxgx4gymb26pqy7yh7wxss9sdxmljf")))

(define-public crate-reflected-0.9.0 (c (n "reflected") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reflected_proc") (r "^0.9.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "1jmm0c7c3r0npp8jz1gzli592dq6m4c2f9c7jy8jnf2657k6712j")))

(define-public crate-reflected-0.9.1 (c (n "reflected") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reflected_proc") (r "^0.9.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "0vwf20hxipi7a93yj88wccl2jgcvmwqi4skwxbhv6xmj2j63s3nw")))

