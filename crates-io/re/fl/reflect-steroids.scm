(define-module (crates-io re fl reflect-steroids) #:use-module (crates-io))

(define-public crate-reflect-steroids-0.1.0 (c (n "reflect-steroids") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.8.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0z0vvb89l0r9f6kxwhj85abl5sfkrkc3swrmp3gckxs8vgdx5cs1")))

(define-public crate-reflect-steroids-0.1.1 (c (n "reflect-steroids") (v "0.1.1") (d (list (d (n "bevy_reflect") (r "^0.8.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0akjlwwpd86scpzd6kix7xmj5m9hkk4slphccglfzv1hgpn5hgcm")))

(define-public crate-reflect-steroids-0.2.0 (c (n "reflect-steroids") (v "0.2.0") (d (list (d (n "bevy_reflect") (r "^0.8.0") (d #t) (k 0)) (d (n "inventory") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1p8p0ripks34myflv997vr79f23irw003d2zfn1vdx227c9dp2n6") (f (quote (("default" "inventory"))))))

