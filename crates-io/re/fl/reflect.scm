(define-module (crates-io re fl reflect) #:use-module (crates-io))

(define-public crate-reflect-0.0.0 (c (n "reflect") (v "0.0.0") (h "1l6s9sbbzfbc0alg1j1q3ay2j6n1pl1a1j8ilck99mnw59kyslf1") (y #t)))

(define-public crate-reflect-0.0.1 (c (n "reflect") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1vqvqyx6mic84vlld8rzmwb280lpfnd7mjj1r05v7pm102myjc2r")))

(define-public crate-reflect-0.0.2 (c (n "reflect") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0r8bgsi3jk7sas5p1ca6m85zmd7m53cjcwj3g4f6aac5dzqvcgp3")))

(define-public crate-reflect-0.0.3 (c (n "reflect") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0xxvqbwg4mn2n88d6pz7hxlb6xg9rwsi6gi6zya4dac5v173m3ac")))

(define-public crate-reflect-0.0.4 (c (n "reflect") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^0.2") (d #t) (k 0)) (d (n "reflect-internal") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1la18jg8n2zdd13843n3mb3ghdx5x17kn413n27s28f51g72q553")))

(define-public crate-reflect-0.0.5 (c (n "reflect") (v "0.0.5") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "^0.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qsbcgy76xdb6dsn7yc4xf2jrqzxq94w0hxyy8zfzyra4ijx0isq")))

(define-public crate-reflect-0.0.6 (c (n "reflect") (v "0.0.6") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "^0.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dx8212f21idsd9qmpbrv2565fqkf6gxn40fqynl5pggfwf1b6ca")))

(define-public crate-reflect-0.0.7 (c (n "reflect") (v "0.0.7") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "000886igfk9a4y9bj7vw58jkijkfh98q5cfi6kdnn620942kbkxz")))

(define-public crate-reflect-0.0.8 (c (n "reflect") (v "0.0.8") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0phqcla2szy94qldhh2a9k5bx0si53macgmrv35w6d4friglflcc")))

(define-public crate-reflect-0.0.9 (c (n "reflect") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bziqpl4il7g7hl2cwk36swad6lvyfasfrpv8ni1l565a8nsi1y5") (r "1.33")))

(define-public crate-reflect-0.0.10 (c (n "reflect") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03lpflrwwc6f6lzbhczzx1byd7hzzfp1d931m64jml02z1dgsvvd") (r "1.33")))

(define-public crate-reflect-0.0.11 (c (n "reflect") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.11") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vn6li4ldzvxg1szfligpj6ml87rmm8n6phpgrbi9fkx5sgyqdmn") (r "1.56")))

(define-public crate-reflect-0.0.12 (c (n "reflect") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.12") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1fzi1z9rc0qvfpmfdhhbjw7flyp2qcdychm5a9m0vc6qadmg34g6") (r "1.56")))

(define-public crate-reflect-0.0.13 (c (n "reflect") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.13") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "03fmfi3p06yhwd72a4r663yjs2ra87ipjdcfj06rfappnqkj4qz3") (r "1.56")))

(define-public crate-reflect-0.0.14 (c (n "reflect") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 0)) (d (n "reflect-internal") (r "=0.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1hbrarrh1xh6sby7pv6f75w3fxc9nhpj12581v8jpmr9ig7z94gq") (r "1.56")))

