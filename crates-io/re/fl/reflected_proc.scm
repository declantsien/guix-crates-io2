(define-module (crates-io re fl reflected_proc) #:use-module (crates-io))

(define-public crate-reflected_proc-0.1.1 (c (n "reflected_proc") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j5yda4gb7pm2xn5njbbh3qz767jwc953w9myzfk7gg6pxcldd7s")))

(define-public crate-reflected_proc-0.1.2 (c (n "reflected_proc") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w94c57v5wxlwas5indwwyxf8j6vpnp85d1gfz4d1w3mrkq8fh5n")))

(define-public crate-reflected_proc-0.1.3 (c (n "reflected_proc") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j62w6g2nimrxwwm0idzx5y2blwmi8qhvg8yacaccdgk2rimcwl2")))

(define-public crate-reflected_proc-0.1.4 (c (n "reflected_proc") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cmzk8bvy0miwwx9fnvq8s9lsfyz9dpx4r8xidmxrf48qqwrx523")))

(define-public crate-reflected_proc-0.1.5 (c (n "reflected_proc") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pxd1kb5qx52mxg5iprjcbnj7dcv273h2k7c81d7k617cx4wpvjj")))

(define-public crate-reflected_proc-0.1.6 (c (n "reflected_proc") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dmbdiv1lzd4z2cc0l8m8qypp47h9sfppvay52vw0vhavyiccva9")))

(define-public crate-reflected_proc-0.1.7 (c (n "reflected_proc") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xz534nkxl02ryddixrx9hzhb6xkawg7qashvb5msvfxiwbf2agn")))

(define-public crate-reflected_proc-0.2.0 (c (n "reflected_proc") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02bic3chfawk81lcfazzs982mb7sg6kws4wnw9imjfvw7xkxjyy0")))

(define-public crate-reflected_proc-0.2.1 (c (n "reflected_proc") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dpa9vv2aj4ybjc40a5n4wlysdcs4ikh4gp195v118kd8dmrhds4")))

(define-public crate-reflected_proc-0.2.6 (c (n "reflected_proc") (v "0.2.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m04ddf3j7b39f4cpppcyvzy3z3pixmfnayg8vvw6yjan18syi02")))

(define-public crate-reflected_proc-0.2.7 (c (n "reflected_proc") (v "0.2.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cvh5yaavbvdkrifjpn9nkvknl1db74mscp36zai7zzpmy71haiv")))

(define-public crate-reflected_proc-0.2.8 (c (n "reflected_proc") (v "0.2.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0792qw26kj7f8nq412cx4y6k62hzcyxxdrs4nl9y2n7bpmfgrg1i")))

(define-public crate-reflected_proc-0.2.9 (c (n "reflected_proc") (v "0.2.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n8342l5k89srlbdjv0g0blqawhhxx1bysj60fldv4mlbpl4cwzb")))

(define-public crate-reflected_proc-0.3.0 (c (n "reflected_proc") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nji6nr0c2cjkkp44b273zp0y1hv7w652k4fdcn6rkdw1pcwn318")))

(define-public crate-reflected_proc-0.3.2 (c (n "reflected_proc") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cmhyv0xklvrvg74nbmcz0z0crd67nmzn4ib8hh9amzp2b72fapn")))

(define-public crate-reflected_proc-0.4.0 (c (n "reflected_proc") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1myyj7zjaz1vj13zczpfdx9yxl70cy0wmh3kkrnhgci3cc7g8ibz")))

(define-public crate-reflected_proc-0.4.2 (c (n "reflected_proc") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1czh6hkamj1m5bs67k4samb8719h89p1xbrvwy44qk9cyvqk750a")))

(define-public crate-reflected_proc-0.4.3 (c (n "reflected_proc") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05ypm93pxv0q8x8hfqw6668pggg6kplyg6by9v21gb7y91p505sg")))

(define-public crate-reflected_proc-0.4.4 (c (n "reflected_proc") (v "0.4.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "175876d6hvd9awb55fcqvrqanhkw3s9nypg23rnj32f1fjbp0y6n")))

(define-public crate-reflected_proc-0.5.0 (c (n "reflected_proc") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n3vzdis4r72s8ri76kfc8wzc22dirwkvc96p13qdm48xq0vf4gd")))

(define-public crate-reflected_proc-0.5.1 (c (n "reflected_proc") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0byac0kn89vnqa752g3khvf9hfq0381hwrvzf67qdljc9ccq1971")))

(define-public crate-reflected_proc-0.5.2 (c (n "reflected_proc") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a7h3nkbwi88sn68bsa8f01m6j0qb6nw8ff9pqzl63v1kqq036jd")))

(define-public crate-reflected_proc-0.5.3 (c (n "reflected_proc") (v "0.5.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wxyh5xaz4a8mnxs7j7y4fa3iq99kcrn8mdqdwxyij8xxgpsgmmp")))

(define-public crate-reflected_proc-0.5.4 (c (n "reflected_proc") (v "0.5.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "185x1mygz0id33by4i2xafpdpn6z2v7jav7m854bmy38m3iqy4f4")))

(define-public crate-reflected_proc-0.5.5 (c (n "reflected_proc") (v "0.5.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1df7x7jn1izd8h4si7rznh0xl0aj00y534k25yrsgz8yb8pd4m6f")))

(define-public crate-reflected_proc-0.6.0 (c (n "reflected_proc") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d11h765626ag6mk52n3ha2354n4y5dznxpw9j41d7vy4w0b149l")))

(define-public crate-reflected_proc-0.6.1 (c (n "reflected_proc") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07nvi6yny5flsk3rvzxydcqrlzagsvg3f02w63b01p746bkp7z8x")))

(define-public crate-reflected_proc-0.6.2 (c (n "reflected_proc") (v "0.6.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "128j3sa67yln4wy03gi8rx2x3mww45r4kpj2z5wh8mwiwhjqfagc")))

(define-public crate-reflected_proc-0.6.3 (c (n "reflected_proc") (v "0.6.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06madgwpw65zs9iixr7v779fnq8fr8lv2qhz475bjjrvx112ki3d")))

(define-public crate-reflected_proc-0.6.4 (c (n "reflected_proc") (v "0.6.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03v25mk70y5lbkqcaskrpj7hmnn23v7xcdl77nn2282qsxcj81a9")))

(define-public crate-reflected_proc-0.6.5 (c (n "reflected_proc") (v "0.6.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rlaqz3zm2p696imakv1hlpn1ynppb4drh4s4bip27gf8w0kyay5")))

(define-public crate-reflected_proc-0.6.6 (c (n "reflected_proc") (v "0.6.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ahp9a7f7zxwll1lqhl29jjsdwx32w0a7bjknk146nm7nkjlgb1d")))

(define-public crate-reflected_proc-0.6.7 (c (n "reflected_proc") (v "0.6.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1684001k9d41ny2kzpv9qmbpcsgphsimx9zdj48syr2cqi78w02p")))

(define-public crate-reflected_proc-0.6.8 (c (n "reflected_proc") (v "0.6.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i1s0snlnlrzmsrhnldsl1272ab4yrpkah9l1gzm2a92arwbb4q6")))

(define-public crate-reflected_proc-0.6.9 (c (n "reflected_proc") (v "0.6.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kihc6c2fyh1shhw6z4hw12m0mf1z9hp5hk805iwnkmxcvq84lsd")))

(define-public crate-reflected_proc-0.6.10 (c (n "reflected_proc") (v "0.6.10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15s2cd5ayglf8wafwfzkq7kph4371434ywbgdqxlfylwzr5lh803")))

(define-public crate-reflected_proc-0.7.0 (c (n "reflected_proc") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pqy2r4pwq95vnrlz5153v87hmwx4d9hzicn0jj6i558mmxisyjg")))

(define-public crate-reflected_proc-0.7.1 (c (n "reflected_proc") (v "0.7.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1na50dm0l72var017g9s49wb94vxzmnhz3qr8xcwaz5j24z3yygk")))

(define-public crate-reflected_proc-0.8.0 (c (n "reflected_proc") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w99pmv02n2mlqmlyl4214ja19c1b4vbyzs10mmdkwknl8cpcqnc")))

(define-public crate-reflected_proc-0.8.1 (c (n "reflected_proc") (v "0.8.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p45mkfgmqbrs15qa9znhm96ywvjwfgsmx3gc1n2dq1rh1wpchql")))

(define-public crate-reflected_proc-0.9.0 (c (n "reflected_proc") (v "0.9.0") (d (list (d (n "quote") (r "=1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sr7jhqbrjc8pi5x8b9lmf7gsniz1kj3hynjm5mlds8lavzfb7d9")))

(define-public crate-reflected_proc-0.9.1 (c (n "reflected_proc") (v "0.9.1") (d (list (d (n "quote") (r "=1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ws4qq5sif0dilg95m8frqycjq85g6kzqkxqzaf9klcgg0bbpc95")))

