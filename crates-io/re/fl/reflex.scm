(define-module (crates-io re fl reflex) #:use-module (crates-io))

(define-public crate-reflex-0.1.0 (c (n "reflex") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ysaqkjd1dvbp6c7aq4z38mmxm74sv8g8nrh488r5863h779pxql")))

(define-public crate-reflex-0.1.1 (c (n "reflex") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0k5pxw8i0gn8np57z23fhsqic0534gjy4j0q6liqycphscsq52zn")))

(define-public crate-reflex-0.1.2 (c (n "reflex") (v "0.1.2") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1qifihw3550ca70gm0yddxfb1d7mzhjpa95jxr5jqnfq3n6bbl9d")))

