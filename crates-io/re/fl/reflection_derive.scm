(define-module (crates-io re fl reflection_derive) #:use-module (crates-io))

(define-public crate-reflection_derive-0.1.0 (c (n "reflection_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.21") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)) (d (n "trees") (r "^0.1") (d #t) (k 0)))) (h "1iv5b5yqiyi00km5g2vdppp4pjp08nx000c1zk5mw7b9jcwrqh1w")))

(define-public crate-reflection_derive-0.1.1 (c (n "reflection_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.21") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0abpps4g2p64xqck39hf7sc00g17jyy1v8f888hqjx60ldscq85l")))

