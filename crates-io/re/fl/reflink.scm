(define-module (crates-io re fl reflink) #:use-module (crates-io))

(define-public crate-reflink-0.1.0 (c (n "reflink") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("ioapiset" "winioctl" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "141l48jhs88b14frcfr1zx627kfc1fmyfbs3xnsb655gs8r191sc")))

(define-public crate-reflink-0.1.1 (c (n "reflink") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("ioapiset" "winioctl" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zpya97m105jhhqsijri629m08zhb4nvhv9ywnkkcf2mjqhc26ld")))

(define-public crate-reflink-0.1.2 (c (n "reflink") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("ioapiset" "winioctl" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ppkq7y860g6r07g6bkhp8iqf0a5f9wq2kbnyr9zsqf4w4qmqf4l")))

(define-public crate-reflink-0.1.3 (c (n "reflink") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("ioapiset" "winioctl" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1glcyqvryv2zj6kjbfji0cldrkincqx3ds3wjwl4qnsnig15wn5w")))

