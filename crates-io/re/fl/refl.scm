(define-module (crates-io re fl refl) #:use-module (crates-io))

(define-public crate-refl-0.1.0 (c (n "refl") (v "0.1.0") (h "1zmy5wcn48jbdgrgbgh3cn67awhk3qdkmq1yjvmi398b4lfkxsxy") (f (quote (("no_std"))))))

(define-public crate-refl-0.1.1 (c (n "refl") (v "0.1.1") (h "11lypdshp9zw6al32i9vvi3n907ihlaai5qcqxay9a7122mycfgc") (f (quote (("no_std"))))))

(define-public crate-refl-0.1.2 (c (n "refl") (v "0.1.2") (h "1dqc5mi6g6q64vv42p6z9vawlflaccdgrpdpsxpzpgb7hynra9h3") (f (quote (("no_std"))))))

(define-public crate-refl-0.1.3 (c (n "refl") (v "0.1.3") (h "1y1shw15n9pr9gbv8cgn6sm9h64jl5mhw05p29650c2935rnlcnq") (f (quote (("no_std"))))))

(define-public crate-refl-0.2.0 (c (n "refl") (v "0.2.0") (h "0k60gv9f0a3739b5vw37z56bfwxcfsbidav1jin00vzhxmv6kl9q")))

(define-public crate-refl-0.2.1 (c (n "refl") (v "0.2.1") (h "09jhxvfqn8lbd5i9097sigs3nkk1l0b72k2pl1h4yzbdwf72rzpm")))

