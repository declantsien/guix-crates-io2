(define-module (crates-io re fl reflect_tools) #:use-module (crates-io))

(define-public crate-reflect_tools-0.2.0 (c (n "reflect_tools") (v "0.2.0") (d (list (d (n "reflect_tools_meta") (r "~0.2.0") (f (quote ("enabled" "enabled"))) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0pnjx81v4qic55v911sj8i3mdbjv3f00gxzgrhg4mix17nw9ldqc") (f (quote (("reflect_reflect") ("full" "enabled" "reflect_reflect") ("enabled") ("default" "enabled" "reflect_reflect"))))))

