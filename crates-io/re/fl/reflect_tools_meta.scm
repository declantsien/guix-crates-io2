(define-module (crates-io re fl reflect_tools_meta) #:use-module (crates-io))

(define-public crate-reflect_tools_meta-0.2.0 (c (n "reflect_tools_meta") (v "0.2.0") (d (list (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1vxwcwhj4hfscd85js2p9vhbxjfsg5ny5whm8h5xm6s3rmpa374b") (f (quote (("reflect_reflect") ("full" "enabled" "reflect_reflect") ("enabled") ("default" "enabled" "reflect_reflect"))))))

