(define-module (crates-io re fl reflexive) #:use-module (crates-io))

(define-public crate-reflexive-0.1.0 (c (n "reflexive") (v "0.1.0") (h "0162vmrw80nrw55hzdkrqib5gzkrrls4wvpizf47ljxvpw987yn9")))

(define-public crate-reflexive-0.4.1 (c (n "reflexive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "086lnncan1gv6xzxy6fj4xzkph166a876n2z5h7svbb3gmgg13m8") (r "1.56.1")))

