(define-module (crates-io re fl reflect-internal) #:use-module (crates-io))

(define-public crate-reflect-internal-0.0.4 (c (n "reflect-internal") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vky2mgqf8rmj4lvn4q0578vfs9g3p1kjh12bffnaadk1dssxzvs")))

(define-public crate-reflect-internal-0.0.5 (c (n "reflect-internal") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0256b51ihc5ak7k780kb929vjxyz2py9155rwxa8sn23rfql4hy1")))

(define-public crate-reflect-internal-0.0.6 (c (n "reflect-internal") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03vqx5hzzw3phxfv71wqsahrgrrg9mrdnkv3rmliv1fgzgc32v31")))

(define-public crate-reflect-internal-0.0.7 (c (n "reflect-internal") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g9m2f6yk510lklyq3c56s6967hm1zkq4jm086qngdzs93ik95xf")))

(define-public crate-reflect-internal-0.0.8 (c (n "reflect-internal") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1aw6mfxssh7rfnmdd0cvmj6d6i256ljq0xjd8rhq5488ss0r9g01")))

(define-public crate-reflect-internal-0.0.9 (c (n "reflect-internal") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g1kl3kbfyg450gsvb8zr2hrs91a5gjx29q9izg12s6sq14z75mx") (r "1.33")))

(define-public crate-reflect-internal-0.0.10 (c (n "reflect-internal") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1av0qil2qq6fld4ix8qy1zkkbb592x3p07s2svnd4x64vz8vi3zl") (r "1.33")))

(define-public crate-reflect-internal-0.0.11 (c (n "reflect-internal") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07ldqs1a6hwj2ryy1fyjxn6xycc2qkpxsqdsvr6rybq2236gvs3m") (r "1.56")))

(define-public crate-reflect-internal-0.0.12 (c (n "reflect-internal") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1m3gnarpq2k39938yk70hv4lifannbl35y351dgdhni71pcqcmyr") (r "1.56")))

(define-public crate-reflect-internal-0.0.13 (c (n "reflect-internal") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1jp0aqzq9kzch75z2r35fakch7pn58mw42vljrram376dp1czmfx") (r "1.56")))

(define-public crate-reflect-internal-0.0.14 (c (n "reflect-internal") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1g3d17izv5qfqvwyix2bzr5frm2c59pygy79xbj0p5mvbsqh0dcd") (r "1.56")))

