(define-module (crates-io re gu regular) #:use-module (crates-io))

(define-public crate-regular-0.1.0 (c (n "regular") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1f79rlx3lp288r9f36v4hik2hnya4c1bf7inpk2l19gmx06f1qdj") (r "1.64")))

(define-public crate-regular-0.1.1 (c (n "regular") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "084vrjrbb3nlp39bylmg6hw2lmqz54d8vsyi4nqwk2npizmhxf27") (r "1.64")))

