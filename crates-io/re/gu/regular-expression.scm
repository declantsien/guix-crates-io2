(define-module (crates-io re gu regular-expression) #:use-module (crates-io))

(define-public crate-regular-expression-0.1.0 (c (n "regular-expression") (v "0.1.0") (d (list (d (n "finite-automata") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regular-expression-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-lexer-bootstrap") (r "^0.1.0") (d #t) (k 0)) (d (n "simple-parser-bootstrap") (r "^0.1.0") (d #t) (k 0)))) (h "1nspcymam60i8mj1d0d6668gz5c32a2j6vwk72l68kcwgxgs6l8i")))

