(define-module (crates-io re nd renderling_build) #:use-module (crates-io))

(define-public crate-renderling_build-0.1.0 (c (n "renderling_build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.7.2") (d #t) (k 0)))) (h "09rwai4ckqcpmiaaz5bk2c63acmcmqhq52nwlrhjyhwr51knl8fj")))

(define-public crate-renderling_build-0.1.1 (c (n "renderling_build") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.0") (d #t) (k 0)))) (h "1wyar76k85vw1lpkmy0d299773lkrs8h9wsm7qlhbm4kk0qy0dsk")))

