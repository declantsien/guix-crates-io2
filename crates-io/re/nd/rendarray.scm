(define-module (crates-io re nd rendarray) #:use-module (crates-io))

(define-public crate-rendarray-0.1.0 (c (n "rendarray") (v "0.1.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.4") (o #t) (d #t) (k 0)))) (h "02pkxa7zhsacvg9z8rlmkcg919n45wmlk3njcjalz0401d46fcfd") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.1.1 (c (n "rendarray") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1c04r41g2siaj6mqmdf3bjrs8ljj6g3ga438r1069b5j588czs6i") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha (c (n "rendarray") (v "0.2.0-alpha") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1nb311i1l083qpp9iwps0ln523iylvqlq36qsm454kl1s25dnfig") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.2 (c (n "rendarray") (v "0.2.0-alpha.2") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "19wbk061znqlmjisk07hqag4hkfv54441173wj2kp8v4ncqcfxkp") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.3 (c (n "rendarray") (v "0.2.0-alpha.3") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "10gmjbgdvpdfh3k9cnri3dj37qbynkfjk87jrgjgwk0j34zpq92j") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.4 (c (n "rendarray") (v "0.2.0-alpha.4") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rblas") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1x3fyx24qabqh3zp0c235kh92w62k1zjya03bz6054cw9pbmx2p0") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.5 (c (n "rendarray") (v "0.2.0-alpha.5") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rblas") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "14xryjvr8yskb8g8bwah9c5yw648hjdsm2qqlvkfm5gc0r5g0k65") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.6 (c (n "rendarray") (v "0.2.0-alpha.6") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rblas") (r "^0.0.12") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0y6pa80hf5nj1kgpq9pzqywln6wfjqlbzpi93iy7asrxvhnjd9p0") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.7 (c (n "rendarray") (v "0.2.0-alpha.7") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rblas") (r "^0.0.13") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1750sff8xrhwfh3g43janhzw2nhwjsg01vi8klw9zybkgk308dpn") (f (quote (("assign_ops")))) (y #t)))

(define-public crate-rendarray-0.2.0-alpha.8 (c (n "rendarray") (v "0.2.0-alpha.8") (d (list (d (n "itertools") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "rblas") (r "^0.0.13") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "09pz6nkxbd3hj78myw0x6s43qvbdph00k1564npg2lhgqv7cfq26") (f (quote (("assign_ops")))) (y #t)))

