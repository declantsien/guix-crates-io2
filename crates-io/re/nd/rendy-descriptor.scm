(define-module (crates-io re nd rendy-descriptor) #:use-module (crates-io))

(define-public crate-rendy-descriptor-0.1.0 (c (n "rendy-descriptor") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "16vbhah4808xck109mzg4rvyh049xyq60yxyys9fm2sr9pnf6nlz")))

(define-public crate-rendy-descriptor-0.2.0 (c (n "rendy-descriptor") (v "0.2.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1h7n17mplaszzs1jw7m5i7q8rvkbvmc74lnyiy2f6bxg4930156r")))

(define-public crate-rendy-descriptor-0.3.0 (c (n "rendy-descriptor") (v "0.3.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1f2xcdb1al62v90rv033vyr33n2715gqy58kbdqy2p5zkvm9y943")))

(define-public crate-rendy-descriptor-0.4.0 (c (n "rendy-descriptor") (v "0.4.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1c38vzf0y0rdzbcbjslvny63ydmb3zzhc700vya07zjc5ci24g96")))

(define-public crate-rendy-descriptor-0.4.1 (c (n "rendy-descriptor") (v "0.4.1") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1n2ihqrki1yczbkablsp7jf9gdd08wg15mzhqpn4aybap0ycqyya")))

(define-public crate-rendy-descriptor-0.5.0 (c (n "rendy-descriptor") (v "0.5.0") (d (list (d (n "gfx-hal") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log" "backtrace"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "02f8hkq3si2cs6hkbwx5q06n5aqpavdknwsjcryxp1hyw119rcy8")))

(define-public crate-rendy-descriptor-0.5.1 (c (n "rendy-descriptor") (v "0.5.1") (d (list (d (n "gfx-hal") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (f (quote ("log"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "07k2igpqsqz6ghx3a6sa2xcvbx1fqm2ha7qgb6cfjijra30bqxgl")))

