(define-module (crates-io re nd renderkit) #:use-module (crates-io))

(define-public crate-renderkit-1.0.0 (c (n "renderkit") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "printf") (r "^0.1.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "vsprintf") (r "^2.0.0") (d #t) (k 0)))) (h "1khipbdd6gm3jkl8b8zazgn6x8nwb0lr8ml6gfr4hdcqy048x80g")))

