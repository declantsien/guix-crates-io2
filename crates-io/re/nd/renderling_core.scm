(define-module (crates-io re nd renderling_core) #:use-module (crates-io))

(define-public crate-renderling_core-0.1.0 (c (n "renderling_core") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.10") (d #t) (k 0)))) (h "1jk4z5kr01v3qgnb0nn3zagbmdxxx83llgxyzvb4blp4sabglsd1")))

(define-public crate-renderling_core-0.1.1 (c (n "renderling_core") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "1pmyv3w486rhgzzbfmiqrxd36agm40v20g3ddfdr1dpbvxnldhc2")))

(define-public crate-renderling_core-0.1.2 (c (n "renderling_core") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "0snnacv6c0g7a1dh8mc88flvm9qdcr3ijcxbaq4m9hxr8w3xpmlr") (f (quote (("wasm" "wgpu/webgl") ("default"))))))

(define-public crate-renderling_core-0.1.3 (c (n "renderling_core") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.13") (d #t) (k 0)))) (h "0rzhki9ixvjsaxvg70zs3bbf0h31ny65lv47a2qw5sfvbiak49x6") (f (quote (("wasm" "wgpu/webgl") ("default"))))))

(define-public crate-renderling_core-0.1.4 (c (n "renderling_core") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)))) (h "1as03dqhbdd4959ld2n0ql72l57g4bybcqx6f4xhzszcl2k1627g") (f (quote (("wasm" "wgpu/webgl") ("default"))))))

(define-public crate-renderling_core-0.1.5 (c (n "renderling_core") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)))) (h "0l9xv3k9dvl0y8r664q8ii2dp53k8ls5qs33aphnjzzmhqb1pjh8") (f (quote (("wasm" "wgpu/webgl") ("default"))))))

