(define-module (crates-io re nd rend3-list) #:use-module (crates-io))

(define-public crate-rend3-list-0.0.1 (c (n "rend3-list") (v "0.0.1") (d (list (d (n "rend3") (r "^0.0.1") (d #t) (k 0)))) (h "036h6w0z03048w5i71bih8w0g4rc5p7fl9cvf1vdpgiza8hixwi9")))

(define-public crate-rend3-list-0.0.2 (c (n "rend3-list") (v "0.0.2") (d (list (d (n "rend3") (r "^0.0.2") (d #t) (k 0)))) (h "1hm0686w6xqc5ws5kffb36awp2nf5fpb4hwm7fk9r0l763nwnbc0")))

(define-public crate-rend3-list-0.0.3 (c (n "rend3-list") (v "0.0.3") (d (list (d (n "rend3") (r "^0.0.3") (d #t) (k 0)))) (h "13z61jixyvz761izrcchdzhh0lp7p77kbirjslwy7nxqsxn21zlf")))

(define-public crate-rend3-list-0.0.4 (c (n "rend3-list") (v "0.0.4") (d (list (d (n "rend3") (r "^0.0.4") (d #t) (k 0)))) (h "12qhc1cpnqncmiy0zbw2d0sicljdxym80hb79wpsy2bwmd8apk5a")))

(define-public crate-rend3-list-0.0.5 (c (n "rend3-list") (v "0.0.5") (d (list (d (n "rend3") (r "^0.0.5") (d #t) (k 0)))) (h "18bsnfvy3bk9c38lgrpnihgk63x36s0w4rfgcmzlph2jhvykb15p")))

