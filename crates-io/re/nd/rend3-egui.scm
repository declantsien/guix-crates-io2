(define-module (crates-io re nd rend3-egui) #:use-module (crates-io))

(define-public crate-rend3-egui-0.3.0 (c (n "rend3-egui") (v "0.3.0") (d (list (d (n "egui") (r "^0.16.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.16.0") (d #t) (k 0)) (d (n "epi") (r "^0.16.0") (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (d #t) (k 0)) (d (n "rend3") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "1jx9zny3yn4c135pdz1khlc0cmj6h6nwgpsaf8cv47hzqbsrx194") (r "1.57")))

