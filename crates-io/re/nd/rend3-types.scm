(define-module (crates-io re nd rend3-types) #:use-module (crates-io))

(define-public crate-rend3-types-0.0.6 (c (n "rend3-types") (v "0.0.6") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.17") (d #t) (k 0)) (d (n "wgt") (r "^0.10") (d #t) (k 0) (p "wgpu-types")))) (h "0cwdfn07k2c93lxvqqzgzwkcsnr9g1w5d6205l0pf5kwm9iv5fd5")))

(define-public crate-rend3-types-0.1.0 (c (n "rend3-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "wgt") (r "^0.10") (d #t) (k 0) (p "wgpu-types")))) (h "1dzkm807hdg0k69d57104pg2n2qkafcf4dv48v6hj6mayvvz7kph")))

(define-public crate-rend3-types-0.1.2 (c (n "rend3-types") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "wgt") (r "^0.10") (d #t) (k 0) (p "wgpu-types")))) (h "1xdjk1gpdj06h1iagiqn6gnlb48x7l42pi0y7s5qswjbcwnyp3j9")))

(define-public crate-rend3-types-0.2.0 (c (n "rend3-types") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.19") (d #t) (k 0)) (d (n "wgt") (r "^0.11") (d #t) (k 0) (p "wgpu-types")))) (h "1kh8k2i6nivzw8iq97ak499bjdyxkwa064xw4zm3nff8bsagg62s")))

(define-public crate-rend3-types-0.3.0 (c (n "rend3-types") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgt") (r "^0.12") (d #t) (k 0) (p "wgpu-types")))) (h "1zibw4fkvbh1jyl59579a7j1zwgzjmi1nny0817cjcaxg9h7mbw4") (r "1.57")))

