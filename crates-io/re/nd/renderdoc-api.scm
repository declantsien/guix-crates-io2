(define-module (crates-io re nd renderdoc-api) #:use-module (crates-io))

(define-public crate-renderdoc-api-0.1.0 (c (n "renderdoc-api") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (t "cfg(unix)") (k 0)) (d (n "renderdoc-api-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0817plpjirlnybqvlagpfqgx8pmyxlacyrxb3hw0r92qvh3vhn7m")))

