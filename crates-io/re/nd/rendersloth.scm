(define-module (crates-io re nd rendersloth) #:use-module (crates-io))

(define-public crate-rendersloth-0.2.0 (c (n "rendersloth") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^3.2.4") (d #t) (k 0)))) (h "0kbrsfa2f00d5641pddppyjpxpbq0g2zjj6pfjjl7fv5hn6jaijc") (f (quote (("build-cli" "clap"))))))

(define-public crate-rendersloth-0.2.1 (c (n "rendersloth") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^3.2.4") (d #t) (k 0)))) (h "026nmbvgizp35n651pvh09ifx850vpdc76jlq9l8vhnyskg7j0x8") (f (quote (("build-cli" "clap"))))))

(define-public crate-rendersloth-0.2.2 (c (n "rendersloth") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "stl_io") (r "^0.7") (d #t) (k 0)) (d (n "tobj") (r "^3.2.4") (d #t) (k 0)) (d (n "tui") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0f61qnp66hjyn6i8x356r4fn3i6s5qwxgz5id05503xlpmawsyhb") (f (quote (("tui-widget" "tui") ("build-cli" "clap"))))))

(define-public crate-rendersloth-0.2.3 (c (n "rendersloth") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "stl_io") (r "^0.7") (d #t) (k 0)) (d (n "tobj") (r "^3.2.4") (d #t) (k 0)) (d (n "tui") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0v9kww198n76cxh07x8c60hwn9pbp3c896822pbhbw3gh2j9f4ks") (f (quote (("tui-widget" "tui") ("build-cli" "clap"))))))

(define-public crate-rendersloth-0.2.4 (c (n "rendersloth") (v "0.2.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "stl_io") (r "^0.7") (d #t) (k 0)) (d (n "tobj") (r "^3.2.4") (d #t) (k 0)) (d (n "tui") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1fgg6q52wva2kw6i2yz3jk8ji361mvm1ffw2lqm2pbxkizknrhpy") (f (quote (("tui-widget" "tui") ("build-cli" "clap"))))))

