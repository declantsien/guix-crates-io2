(define-module (crates-io re nd rendy-chain) #:use-module (crates-io))

(define-public crate-rendy-chain-0.1.0 (c (n "rendy-chain") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "133d8nxkisfv926fxriw55rldhrmfnybzgv75kfg5ngb9yv701fl")))

(define-public crate-rendy-chain-0.2.0 (c (n "rendy-chain") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rendy-util") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "1vh65ahxn03dd472dyg96r12fs3agbnmz3sgnvhi80nkgdif7ga5") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.3.0 (c (n "rendy-chain") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "1j717ay7askmzkjf01xm47xxpgrpy56pnvbcvhjjmbid5swv97ng") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.3.1 (c (n "rendy-chain") (v "0.3.1") (d (list (d (n "gfx-hal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "044mh8s3qm4l6k2lz0mxis7y42jw5nnsr7187anycjh9ryi0d5q2") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.4.0 (c (n "rendy-chain") (v "0.4.0") (d (list (d (n "gfx-hal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "0nda3pqq84s2m17d2rnqpi9d4npdk7m2lqdzz18nf67ka4yz0b3i") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.4.1 (c (n "rendy-chain") (v "0.4.1") (d (list (d (n "gfx-hal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "1bbd9h7cjc06fcmzk4yaja5rpxahnba050z0wy562818hlcj0b8c") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.5.0 (c (n "rendy-chain") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rendy-core") (r "^0.5.0") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "03kmn9ch54p989h55a6vcwz2hbrq7lr9ziwzgykj1x2ysqqwqmhl") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

(define-public crate-rendy-chain-0.5.1 (c (n "rendy-chain") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rendy-core") (r "^0.5.1") (d #t) (k 0)) (d (n "thread_profiler") (r "^0.3") (d #t) (k 0)))) (h "1y1minp9qb5ky8kvknrf6sa5f56swml7kqpddbx7xk2k3xmw1gbx") (f (quote (("profiler" "thread_profiler/thread_profiler"))))))

