(define-module (crates-io re nd render) #:use-module (crates-io))

(define-public crate-render-0.1.0 (c (n "render") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "render_macros") (r "^0.1") (d #t) (k 0)))) (h "0hnjy5d27xawcnnkqjgq9wx66xhx67lsckz8kynz703nrrziivzm")))

(define-public crate-render-0.1.1 (c (n "render") (v "0.1.1") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "render_macros") (r "^0.1") (d #t) (k 0)))) (h "1k441f7gsh6mxh14m7y7js45lxqrgfmjdn2wf51grlv94jqnmxbz")))

(define-public crate-render-0.2.0 (c (n "render") (v "0.2.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "render_macros") (r "^0.2") (d #t) (k 0)))) (h "0b4ssrav5fr3kmiq6mp6w68m0vdq54hjdpd8vg0jbljgf3wjkshq")))

(define-public crate-render-0.3.0 (c (n "render") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "render_macros") (r "^0.3") (d #t) (k 0)))) (h "0pxg1bjlwzl5fyijzjx4n1g7fzdxcjw6l92jc5jz3r4f59ib4dpd")))

(define-public crate-render-0.3.1 (c (n "render") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "render_macros") (r "^0.3.1") (d #t) (k 0)))) (h "00p28x5c02pgsbcn9xwan1qnk4j6pva2xj0r2ixp5d5ihc28slh7")))

