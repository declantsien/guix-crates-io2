(define-module (crates-io re nd rendersvg-meetmangukiya) #:use-module (crates-io))

(define-public crate-rendersvg-meetmangukiya-0.9.0 (c (n "rendersvg-meetmangukiya") (v "0.9.0") (d (list (d (n "fern") (r "= 0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.2") (d #t) (k 0)) (d (n "resvg") (r "^0.9.0") (d #t) (k 0)))) (h "015byvr12x2rar063hxjrsbp503w14k0gkgi6wj4493q9g2343k1") (f (quote (("text" "resvg/text") ("skia-backend" "resvg/skia-backend") ("raqote-backend" "resvg/raqote-backend") ("qt-backend" "resvg/qt-backend") ("default" "text") ("cairo-backend" "resvg/cairo-backend"))))))

(define-public crate-rendersvg-meetmangukiya-0.9.1 (c (n "rendersvg-meetmangukiya") (v "0.9.1") (d (list (d (n "fern") (r "= 0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.2") (d #t) (k 0)) (d (n "resvg") (r "^0.9.0") (d #t) (k 0)))) (h "1365fyamfvp3023xbys6mv6iilcqyd3fbimhnxj0c5vn6zjyhvgz") (f (quote (("text" "resvg/text") ("skia-backend" "resvg/skia-backend") ("raqote-backend" "resvg/raqote-backend") ("qt-backend" "resvg/qt-backend") ("default" "text") ("cairo-backend" "resvg/cairo-backend"))))))

