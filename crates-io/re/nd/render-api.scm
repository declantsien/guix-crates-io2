(define-module (crates-io re nd render-api) #:use-module (crates-io))

(define-public crate-render-api-0.1.0 (c (n "render-api") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1li0a3zcggj3jkakj8dhv95jfjzabdspss7shb5bsq2c1fh6fp2b")))

(define-public crate-render-api-1.0.0 (c (n "render-api") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0lcq1kjr05l1njiyhxj72qnny4xq793q17yfkp5lxhkb2zzhg1ws")))

(define-public crate-render-api-1.0.1 (c (n "render-api") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0a4wh3i8j84jpmgc9xshyxs7k0manm02qlij0lnszp7r4dfbq78p")))

(define-public crate-render-api-1.0.3 (c (n "render-api") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0irdhrbk9lxg3ig9jyfkdby1g4bg1f31a1kq1qw9ryx1y3kn6f4c")))

