(define-module (crates-io re nd render_macros) #:use-module (crates-io))

(define-public crate-render_macros-0.1.0 (c (n "render_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j3cw46bk737dz69agbaz6abrvz20h5jp0xhd5pm8y0hsz8whscx")))

(define-public crate-render_macros-0.1.1 (c (n "render_macros") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "render") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jgs60n8s7pc8mrz2j0z15wsh10d5k7h5wvh0yhkl995w65gfvpn")))

(define-public crate-render_macros-0.2.0 (c (n "render_macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dinc4jm2wfb7fhk6ck9045fzm6aqlfz0phj4fhgkmwvqsfawsm1")))

(define-public crate-render_macros-0.3.0 (c (n "render_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02ci0gv59dz5axwrpl4i3ava3iddq7c031sn9w7val7gq58fmgdq")))

(define-public crate-render_macros-0.3.1 (c (n "render_macros") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "render") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19x2lhd3j7jss5iz3nxpwigl7j58fimmci85qmknzxm1j7a4iv3f")))

