(define-module (crates-io re nd renderdoc-sys) #:use-module (crates-io))

(define-public crate-renderdoc-sys-0.4.0 (c (n "renderdoc-sys") (v "0.4.0") (h "08vrzrnmbvs7vghpwrpajnw9f9byn7p4004mivcz5h6i062yjpyq")))

(define-public crate-renderdoc-sys-0.5.0 (c (n "renderdoc-sys") (v "0.5.0") (h "1gprk28sz458a2mqhygriw3fl698zc0sq2hjpvzrwwglzr8b6j3x")))

(define-public crate-renderdoc-sys-0.6.0 (c (n "renderdoc-sys") (v "0.6.0") (h "146kl0p73j5ssx93rb3yy9k1fss1q5r8p9igzrjyghs9i02skm30")))

(define-public crate-renderdoc-sys-0.7.0 (c (n "renderdoc-sys") (v "0.7.0") (h "0cc3h0fvxlnfj7bhxknfs4ykk8qyqrgklvib51gj3pymlh0j16d7")))

(define-public crate-renderdoc-sys-0.7.1 (c (n "renderdoc-sys") (v "0.7.1") (h "0mx1crv83mwmfl7yvhnpmhjb01fx5yd9f3f2gpwlnb1518gjsf7i")))

(define-public crate-renderdoc-sys-1.0.0 (c (n "renderdoc-sys") (v "1.0.0") (h "12v23c9z5xnpjgb0zdzwbj7kaj2cip0p6s58vls2569b72mq0q11")))

(define-public crate-renderdoc-sys-1.1.0 (c (n "renderdoc-sys") (v "1.1.0") (h "0cj8zjs7k0gvchcx3jhpg8r9bbqy8b1hsgbz0flcq2ydn12hmcqr")))

