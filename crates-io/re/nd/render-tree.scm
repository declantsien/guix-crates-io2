(define-module (crates-io re nd render-tree) #:use-module (crates-io))

(define-public crate-render-tree-0.1.0 (c (n "render-tree") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.1") (d #t) (k 0)))) (h "1wcq0hff83m6wdrqidccwysz3lfi1nidjnvmrji1gbnaqjnax2qs")))

(define-public crate-render-tree-0.1.1 (c (n "render-tree") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.5") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0r0nwdkyqj5rpz62d4q45hi9y8fv4izggs3gpkhprywwy1ymivb8")))

