(define-module (crates-io re nd renderdoc-derive) #:use-module (crates-io))

(define-public crate-renderdoc-derive-0.4.0 (c (n "renderdoc-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0v2argllws7ls2jxpcqd3cg9d5qx8afw5msj2pkqz2n0mcl0iyzw")))

(define-public crate-renderdoc-derive-0.5.0 (c (n "renderdoc-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "0lw7wpvm8swq85azq443rnii3ijypnfh2ipg5rfkxd4jk92aj2sb")))

(define-public crate-renderdoc-derive-0.6.0 (c (n "renderdoc-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "1yy3yaxz2x6a3mb1gs6hf8551jag19np53iykjgcg9207472vy7d")))

(define-public crate-renderdoc-derive-0.7.0 (c (n "renderdoc-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "0bv042z7rylrb5iqvq3n86cspzy23f24zkdqwwj6pky6grzac4cy")))

