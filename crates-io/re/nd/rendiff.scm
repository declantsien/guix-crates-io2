(define-module (crates-io re nd rendiff) #:use-module (crates-io))

(define-public crate-rendiff-0.1.0 (c (n "rendiff") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (k 0)) (d (n "itertools") (r "^0.10.5") (f (quote ("use_alloc"))) (k 0)))) (h "0932djb2c9i9kpnlfakdvsxi1m84aw56fy55b5wbip1a16m9hd67") (r "1.69.0")))

