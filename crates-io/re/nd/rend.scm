(define-module (crates-io re nd rend) #:use-module (crates-io))

(define-public crate-rend-0.1.0 (c (n "rend") (v "0.1.0") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "08s2ywzfv0vapkl4253m3b050y37p37b6wvn01qrhzcqiykhwxmp") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.1.1 (c (n "rend") (v "0.1.1") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0ny3j5m4gk435m50x4ba3www77hgb3kv8a3brmh66dbf1a9865yq") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.2.0 (c (n "rend") (v "0.2.0") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0jyvpbxyjvsh2jf5zi0c1apf579hspxijl4prm1jbvx06f2s1yxz") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.2.1 (c (n "rend") (v "0.2.1") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "05h10hl2y3yrq55w9gca4p0q6jy6i79c4l0qi5i12nldhkgm4bqb") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.2.2 (c (n "rend") (v "0.2.2") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "08qmf0p6qx8gcasbcy21rhkc6z0kvrz6a2j9irsc37q904l0lbln") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.2.3 (c (n "rend") (v "0.2.3") (d (list (d (n "bytecheck") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1piif7fqzbsy3rzcn01ginscd5nc6npzs719gj10qsqqy045fy2j") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.0 (c (n "rend") (v "0.3.0") (d (list (d (n "bytecheck") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1ncrrhpvvd9z6lz8ima4js6x7sy224xy92cw8gxd2bz860c96xrl") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.1 (c (n "rend") (v "0.3.1") (d (list (d (n "bytecheck") (r "^0.6.0") (o #t) (k 0)))) (h "1cga0w8vsvfl3hhiwwvwvw26s4w1xmdq4r99abr1rvhnl5hd1cch") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.2 (c (n "rend") (v "0.3.2") (d (list (d (n "bytecheck") (r "^0.6.0") (o #t) (k 0)))) (h "1is1ks0d487vgsvrfxh8mpdavfg0ljjzlcggf7ak1vi9wni520vd") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.3 (c (n "rend") (v "0.3.3") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)))) (h "0v88kmw6ryg5ib0iwyp5ihdgihqwg92vgj8s118fjiwb7y6p553p") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.4 (c (n "rend") (v "0.3.4") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)))) (h "0ln1nzlwkbm0f99wqw0g5y4xfghnwycwmjajllvjhan7ahypxafh") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.5 (c (n "rend") (v "0.3.5") (d (list (d (n "bytecheck") (r "^0.6") (o #t) (k 0)))) (h "06pilvkn9pqf4j1li33j14x6lpg86fwr2a21wlrq7374gkzgccqh") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.3.6 (c (n "rend") (v "0.3.6") (d (list (d (n "bytecheck") (r "~0.6.7") (o #t) (k 0)))) (h "15fz3rw8c74586kxl6dcdn4s864ph884wfpg9shgnbrnnss69bvr") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.4.0 (c (n "rend") (v "0.4.0") (d (list (d (n "bytecheck") (r "~0.6.7") (o #t) (k 0)))) (h "1av8mfxrc6dvm0hmn2ymi56jrv6a7dqssxwdn1zx6h4j1790h42q") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.4.1 (c (n "rend") (v "0.4.1") (d (list (d (n "bytecheck") (r "~0.6.7") (o #t) (k 0)) (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1z88kx2iwx5yql3sg5znp8zpli837y9h4i7r686dasrshrii8mx2") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.5.0-pre1 (c (n "rend") (v "0.5.0-pre1") (d (list (d (n "bytecheck") (r "^0.8.0-pre1") (o #t) (k 0)))) (h "0qjryhbjscaqmm5aj6fcf0h077h0nnm8nxqvk7n7v8v0ms18dyb5") (f (quote (("default"))))))

(define-public crate-rend-0.5.0-pre2 (c (n "rend") (v "0.5.0-pre2") (d (list (d (n "bytecheck") (r "^0.8.0-pre1") (o #t) (k 0)))) (h "017115kf3hwqzdwx3hvfzy3mc3mpyzg0hcvia7kybksw64jwwz48") (f (quote (("default"))))))

(define-public crate-rend-0.5.0-pre3 (c (n "rend") (v "0.5.0-pre3") (d (list (d (n "bytecheck") (r "^0.8.0-pre3") (o #t) (k 0)))) (h "1yvb74v16hi79i06p545qgx9gkyxyj0m4qxya1hzsmcw3ynwxfda") (f (quote (("default"))))))

(define-public crate-rend-0.5.0-pre4 (c (n "rend") (v "0.5.0-pre4") (d (list (d (n "bytecheck") (r "^0.8.0-pre4") (o #t) (k 0)))) (h "0w8xf6a68gbl79ikw84vbszik943ga0asnw9wiz0c0rf3hv0y4l4") (f (quote (("default"))))))

(define-public crate-rend-0.5.0-pre5 (c (n "rend") (v "0.5.0-pre5") (d (list (d (n "bytecheck") (r "^0.8.0-pre5") (o #t) (k 0)))) (h "1r46b826k1nkbgg97lnbdh5izwx8s9mv9fiya7fc0nvgh2dv83kn") (f (quote (("default"))))))

(define-public crate-rend-0.5.0-pre6 (c (n "rend") (v "0.5.0-pre6") (d (list (d (n "bytecheck") (r "^0.8.0-pre6") (o #t) (k 0)))) (h "1k643ckzgwx4zbsvpxwaqmy04prl9qjrxy6jhncn86dh4wrgq0kz") (f (quote (("default"))))))

(define-public crate-rend-0.4.2 (c (n "rend") (v "0.4.2") (d (list (d (n "bytecheck") (r "~0.6.7") (o #t) (k 0)) (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0z4rrkycva0lcw0hxq479h4amxj9syn5vq4vb2qid5v2ylj3izki") (f (quote (("validation" "bytecheck") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-rend-0.5.0-alpha.7 (c (n "rend") (v "0.5.0-alpha.7") (d (list (d (n "bytecheck") (r "^0.8.0-alpha.9") (o #t) (k 0)))) (h "0p4fp665gmmabhwr7b6cc9w9gym59gwcskfzqh5xvkdh5nwhwl20") (f (quote (("default"))))))

