(define-module (crates-io re nd rend3-imgui) #:use-module (crates-io))

(define-public crate-rend3-imgui-0.3.0 (c (n "rend3-imgui") (v "0.3.0") (d (list (d (n "glam") (r "^0.20.0") (d #t) (k 0)) (d (n "imgui") (r "^0.8") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.19") (d #t) (k 0)) (d (n "rend3") (r "^0.3.0") (d #t) (k 0)))) (h "08fw67k16cp77clkd0d9g08wdk7m6skxh6rp85r295vdjxjmih75") (r "1.57")))

