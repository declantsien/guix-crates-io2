(define-module (crates-io re nd rendezvous) #:use-module (crates-io))

(define-public crate-rendezvous-0.1.0 (c (n "rendezvous") (v "0.1.0") (h "0ijr441g49qzlm2kq836yyh2p5fbpwka6waimnnpgbpyj2hcbxcx") (y #t)))

(define-public crate-rendezvous-0.1.1 (c (n "rendezvous") (v "0.1.1") (h "1vnka7gpvnyhawq4z1z7jyqk638b8yhij15yjz01h3kgxw4hrfd7")))

(define-public crate-rendezvous-0.2.0 (c (n "rendezvous") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0y5xanysy1yhik380ykgdqg3j6vm5vikaq6w2cwbgdf8ccrga7dw") (s 2) (e (quote (("tokio" "dep:tokio") ("log" "dep:log"))))))

(define-public crate-rendezvous-0.2.1 (c (n "rendezvous") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "17ih126xb0g6azsrv6jk5r79xr3qjlwg4zq38kvxd7rlvc9rn2qk") (s 2) (e (quote (("tokio" "dep:tokio") ("log" "dep:log"))))))

(define-public crate-rendezvous-0.2.2 (c (n "rendezvous") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "1j599qk6c39i1qrdpwqxzxjy2qc30l449rdgr0ni73pq00jqddbc") (s 2) (e (quote (("tokio" "dep:tokio") ("log" "dep:log"))))))

(define-public crate-rendezvous-0.2.3 (c (n "rendezvous") (v "0.2.3") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0wq3bmfd675ya1gib2gdg3ryxvvsm2pj6dcjyqk59s4i83w6gvvs") (s 2) (e (quote (("tokio" "dep:tokio") ("log" "dep:log"))))))

