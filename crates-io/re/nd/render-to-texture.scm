(define-module (crates-io re nd render-to-texture) #:use-module (crates-io))

(define-public crate-render-to-texture-0.1.0 (c (n "render-to-texture") (v "0.1.0") (d (list (d (n "basis-universal") (r "^0.3.1") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("basis-universal" "bevy_render" "bevy_asset" "bevy_core_pipeline"))) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0dmva6rlirygs2nmqjc1716kcpzrpj94hpg60y9p56mr16rsy837") (y #t) (r "1.76.0")))

(define-public crate-render-to-texture-0.1.1 (c (n "render-to-texture") (v "0.1.1") (d (list (d (n "basis-universal") (r "^0.3.1") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("basis-universal" "bevy_render" "bevy_asset" "bevy_core_pipeline"))) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "120nm2rc1a9y0il7q0hrdqd801pph37g6lvqb6kfpdkqn0g9h25l") (r "1.76.0")))

(define-public crate-render-to-texture-0.13.0 (c (n "render-to-texture") (v "0.13.0") (d (list (d (n "basis-universal") (r "^0.3.1") (d #t) (k 0)) (d (n "bevy") (r "^0.13.1") (f (quote ("basis-universal" "bevy_render" "bevy_asset" "bevy_core_pipeline"))) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-lite") (r "^2.3.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1sx5n6ai7k89p517n3pn3lngsrf6nq62q6a0s9alr0pxrqykvnlc") (r "1.76.0")))

