(define-module (crates-io re nd render_cerke_board) #:use-module (crates-io))

(define-public crate-render_cerke_board-0.1.0 (c (n "render_cerke_board") (v "0.1.0") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1zaxj7ryf9dagg7kja7msyvx7x08y0wi686ckaksqkmhydwjlcv1")))

(define-public crate-render_cerke_board-0.1.1 (c (n "render_cerke_board") (v "0.1.1") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0vzhc6im5m8zk7fdr6dfpsc2ljxp5lxc7qrj0wmbmxh6ak6xw2rb")))

(define-public crate-render_cerke_board-0.1.2 (c (n "render_cerke_board") (v "0.1.2") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1pyc46x4z3ai694i31dcbi8jh3a5bhynab24nflwfaj551f2v075")))

(define-public crate-render_cerke_board-0.1.3 (c (n "render_cerke_board") (v "0.1.3") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0xn89vjrcg413fhn3hj26r320p66vp3cmfpn8gzz2cv4x6bi16ip")))

(define-public crate-render_cerke_board-0.1.4 (c (n "render_cerke_board") (v "0.1.4") (d (list (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0s31z8m5xgkhkxsp934j169kbq839ym42xz9q837fxpw9lqm1pkk")))

(define-public crate-render_cerke_board-0.1.5 (c (n "render_cerke_board") (v "0.1.5") (d (list (d (n "cetkaik_core") (r "^0.1.0") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "1ipsmwrfcp64j6wirvfvc8qib3q29l2kz2yfymrv1485vvrgh7jm")))

(define-public crate-render_cerke_board-0.1.6 (c (n "render_cerke_board") (v "0.1.6") (d (list (d (n "cetkaik_core") (r "^0.1.1") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "0zz5hsi8flqp7d75rbwff9ba7izsrapi51c44h460g3x2j87kl6p")))

(define-public crate-render_cerke_board-0.1.7 (c (n "render_cerke_board") (v "0.1.7") (d (list (d (n "cetkaik_core") (r "^0.1.1") (d #t) (k 0)) (d (n "fastblur") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "1gyf5mqxif3wamkh02fimxwhi7bdy0mii9fy33jjb8mlpm67yzi3")))

(define-public crate-render_cerke_board-0.1.8 (c (n "render_cerke_board") (v "0.1.8") (d (list (d (n "cetkaik_core") (r "^0.1.10") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "0pwdr4b3hblgm9n4k4sm5j1c86c6w85m89zzn25kk403adhhh3vl")))

(define-public crate-render_cerke_board-0.1.9 (c (n "render_cerke_board") (v "0.1.9") (d (list (d (n "cetkaik_core") (r "^0.1.10") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.0") (d #t) (k 0)))) (h "04f479hxqnl7ld43rr65ngpc0jyvgpc8yzr12pjfzgi8qf7r2mbf")))

(define-public crate-render_cerke_board-0.1.10 (c (n "render_cerke_board") (v "0.1.10") (d (list (d (n "cetkaik_core") (r "^0.1.10") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.2") (d #t) (k 0)))) (h "0d4kfggpkmzbcz4z25k4zfs62n3y8mj6sc2kk7fln2m3ga1capns")))

(define-public crate-render_cerke_board-0.1.11 (c (n "render_cerke_board") (v "0.1.11") (d (list (d (n "cetkaik_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.2") (d #t) (k 0)))) (h "0n0sahn2d5791izd3zq4dnbdd9144id7q5cn1km82ny02y45sxj8")))

(define-public crate-render_cerke_board-0.1.12 (c (n "render_cerke_board") (v "0.1.12") (d (list (d (n "cetkaik_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.3") (d #t) (k 0)))) (h "0zylqq0hjmffhx3kig1r65hnss1g9xb08nxndcjm8i0j916z7ly6")))

(define-public crate-render_cerke_board-0.1.13 (c (n "render_cerke_board") (v "0.1.13") (d (list (d (n "cetkaik_core") (r "^0.2.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.3") (d #t) (k 0)))) (h "0w6b721h9zhlflh07vrdrs1gaxgnpygyypavm3gpjh6snz4j02h9")))

(define-public crate-render_cerke_board-0.1.14 (c (n "render_cerke_board") (v "0.1.14") (d (list (d (n "cetkaik_core") (r "^0.3.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.3") (d #t) (k 0)))) (h "1c7is76d1y88daxvm3yv4yrmzvyds527yqivfjan4ysy6lhygz9r")))

(define-public crate-render_cerke_board-0.1.15 (c (n "render_cerke_board") (v "0.1.15") (d (list (d (n "cetkaik_core") (r "^0.3.4") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.1.4") (d #t) (k 0)))) (h "1sbxbd1q1prvbbxifb9yps8mk9f2r404igci1rn32h1g9phij0j7")))

(define-public crate-render_cerke_board-0.1.16 (c (n "render_cerke_board") (v "0.1.16") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.2.1") (d #t) (k 0)))) (h "1a0skm7z5x1jd6029n6d17jf3vk9jij80yp766r1b4m1sq2mbvgq")))

(define-public crate-render_cerke_board-0.1.17 (c (n "render_cerke_board") (v "0.1.17") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.2.1") (d #t) (k 0)))) (h "1pdm9c7lvjj6qrhzxyxh986bkdak6piz951fphx3jw4cmn16ms1f")))

(define-public crate-render_cerke_board-0.1.18 (c (n "render_cerke_board") (v "0.1.18") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_naive_representation") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "wood_grain") (r "^0.2.1") (d #t) (k 0)))) (h "1w1zfh3yybydnfxgv31x22nijnrv5vlr9il46q5hznx8qnyxza47")))

