(define-module (crates-io re it reiterator) #:use-module (crates-io))

(define-public crate-reiterator-0.1.0 (c (n "reiterator") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0ybi6ylslkkcifyps87qhgsq3f7czin0b6b3h4mb1mxgzm7p1agw")))

(define-public crate-reiterator-0.1.1 (c (n "reiterator") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1g2zmn7mb43vf7qpa1xbahc5idfl2rir5ar47vp1qxmqc2jm8q3h")))

(define-public crate-reiterator-0.1.2 (c (n "reiterator") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1bf388k4adw6vpkliy6chpx6fx565lpvs5lafcdc7gklyvjp6d23")))

(define-public crate-reiterator-0.1.3 (c (n "reiterator") (v "0.1.3") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1adbdlf23y693hrfvq08j2mll8jgxr74lcjxffdixdgbnswvh05k")))

(define-public crate-reiterator-0.3.0 (c (n "reiterator") (v "0.3.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "01a67qjk793yz5sm2mf88dq3gjw7hwhm6435jwa7bwlmx2nj72q2")))

