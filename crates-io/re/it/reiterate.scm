(define-module (crates-io re it reiterate) #:use-module (crates-io))

(define-public crate-reiterate-0.1.0 (c (n "reiterate") (v "0.1.0") (d (list (d (n "elsa") (r "^0.1.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1b58d5m8zdb4ansvbbw9vz0jzip27vyma1zr8j0divdpjyr5blhp")))

(define-public crate-reiterate-0.1.1 (c (n "reiterate") (v "0.1.1") (d (list (d (n "elsa") (r "^0.1.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "17s0r2nf8v5adf5j2ahwf325qm6pr346pdjx1kpd84sivliz9qic")))

(define-public crate-reiterate-0.1.2 (c (n "reiterate") (v "0.1.2") (d (list (d (n "elsa") (r "^1.0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0px4mwnj7mrm1mc474w2n5scgsj8j55sa2m8ggzx1r87c6hxvvs9")))

(define-public crate-reiterate-0.1.3 (c (n "reiterate") (v "0.1.3") (d (list (d (n "elsa") (r "^1.0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0ql27xghr49425k5bxs8s71346ijxaz45vd7b667mis1584g0bbh")))

