(define-module (crates-io re cp recp) #:use-module (crates-io))

(define-public crate-recp-0.2.0 (c (n "recp") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "07741am0z5mjybsj9l0kzaxd080yf6933d3l233xvijjs3llhbdm")))

(define-public crate-recp-0.2.1 (c (n "recp") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.1") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0hkcvlzimp7gvhv8cb3aw7h0v0k07jl35ww95bbbbsawsipwckzy")))

(define-public crate-recp-0.2.2 (c (n "recp") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0q9n45v4djkac221b9a844jgramx6spiwnnax07f9i68v69y6vcr")))

(define-public crate-recp-0.2.3 (c (n "recp") (v "0.2.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1kxmn7mp9lpvjk410py5bgxk4zpiy7h9rijkjh51cklkk2k06xfm")))

(define-public crate-recp-0.2.4 (c (n "recp") (v "0.2.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.4") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "013gmx4zx72b9i4jzsymkh5lpp9w8y6qprqp8rp86pd8c53c9kxd")))

(define-public crate-recp-0.2.7 (c (n "recp") (v "0.2.7") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.7") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1nf9xf4pqa6b46zglnn17dfrmr7ska0j601j615am1a1sqi3539c")))

(define-public crate-recp-0.2.8 (c (n "recp") (v "0.2.8") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1z79jmfv7jar9mw8wb6chv1iwlipc8661ss8c4j7wh13b3pvcp9f")))

(define-public crate-recp-0.2.9 (c (n "recp") (v "0.2.9") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.2.9") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0s1vah4kr45rm41dragsagmx3l34dwy8nw1bm4nizvwal3qjsmnc")))

(define-public crate-recp-0.3.0 (c (n "recp") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0sgkzjcmn3jvdn45bsrkc2xk4632s849q95vw4xvmgqi1wfsd60y")))

(define-public crate-recp-0.3.1 (c (n "recp") (v "0.3.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1pl1iviik9g5s9474a59avrpzs54d8py0yzy708ji0b0mgc3wcxb")))

(define-public crate-recp-0.4.0 (c (n "recp") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1dwfwhshf2f44qjr3mhz8f41hr8z07aw90pjkzincvfbp9s25z0d")))

(define-public crate-recp-0.4.1 (c (n "recp") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.4.1") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0pp6lia1g9fkzfffpsh1jhysxw8jfmdnjrn6n7v0va2mpn8jsmxs")))

(define-public crate-recp-0.4.2 (c (n "recp") (v "0.4.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.4.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0zwywsk0a1xf4k6yp52m5i4cik52s6xz5ksw05kcnp771h36lifs")))

(define-public crate-recp-0.4.3 (c (n "recp") (v "0.4.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.4.3") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "00wzfa9hn2mq1vr844k6a6p6s61yi163blj3ia9dvvgjmd9mgwn0")))

(define-public crate-recp-0.5.0 (c (n "recp") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "1jckfxg8caph28872ii50a6r7w49499mz63nyrbigw97bdm4b4zh")))

(define-public crate-recp-0.6.0 (c (n "recp") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.6.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0z70a3w52lkp4nypr4dwlnr3yd9mkl07dzhj7mp4ziqfmvmmkksf")))

(define-public crate-recp-0.7.0 (c (n "recp") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0nqp43zjn349zf0a85326v370gj60b2xylz4jz2imj178sc27w7a")))

(define-public crate-recp-0.8.0 (c (n "recp") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0ail7pfzmhnx2i1pzmpyz1afw2wz4csj5ba5vkd6ma97b3mxjqw2")))

(define-public crate-recp-0.8.1 (c (n "recp") (v "0.8.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "recipe-parser") (r "^0.8.1") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "12hmahal28xpyk45dbb1k68xfaa4iq9ggrg3z86xgscqs6wjcci2")))

