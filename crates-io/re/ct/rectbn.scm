(define-module (crates-io re ct rectbn) #:use-module (crates-io))

(define-public crate-reCTBN-0.1.0 (c (n "reCTBN") (v "0.1.0") (d (list (d (n "approx") (r "~0.5") (d #t) (k 2) (p "approx")) (d (n "bimap") (r "~0.6") (d #t) (k 0)) (d (n "enum_dispatch") (r "~0.3") (d #t) (k 0)) (d (n "itertools") (r "~0.10") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "ndarray") (r "~0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "rand") (r "~0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "~0.3") (d #t) (k 0)) (d (n "rayon") (r "~1.6") (d #t) (k 0)) (d (n "statrs") (r "~0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1hyikxxk3b9mba9mjzq5kis0m1yjhwffrc8kh535vwqki495fbf0")))

