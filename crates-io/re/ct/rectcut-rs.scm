(define-module (crates-io re ct rectcut-rs) #:use-module (crates-io))

(define-public crate-rectcut-rs-0.1.0 (c (n "rectcut-rs") (v "0.1.0") (h "0japr9k6x0bvigyzcdjndy9r2gccn37990icpicqj3dvcx434dyb")))

(define-public crate-rectcut-rs-0.1.1 (c (n "rectcut-rs") (v "0.1.1") (h "1zbnc156q2d8psgv9x05gqnhn6mdibm9msnk09k49chk1r83s7vm")))

(define-public crate-rectcut-rs-0.1.2 (c (n "rectcut-rs") (v "0.1.2") (h "10d1x4zrm3ak7aszhphc5ml3hbs2ki2jk0iad2x8msq38vnyxa83")))

