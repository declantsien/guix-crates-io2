(define-module (crates-io re ct rectangle-pack) #:use-module (crates-io))

(define-public crate-rectangle-pack-0.1.0 (c (n "rectangle-pack") (v "0.1.0") (h "0sq0mamfr5m2g44132yg46qxwza32r40nvqk82qdlqqh4d5qnr3l")))

(define-public crate-rectangle-pack-0.1.1 (c (n "rectangle-pack") (v "0.1.1") (h "1yy4vjfic1b7sn8avc2b8c7wgsrl6cnyvikv1jh5adgfvvfpimq2")))

(define-public crate-rectangle-pack-0.1.2 (c (n "rectangle-pack") (v "0.1.2") (h "0fq1sh9hifn67w33f4qnh8i55fq78qdsgdrj6bbrkbywliss5ld9")))

(define-public crate-rectangle-pack-0.1.4 (c (n "rectangle-pack") (v "0.1.4") (h "0znvb5ac242vx1n7s0qvjs4376zd94cx1c89y1zz1a1drbi3z285")))

(define-public crate-rectangle-pack-0.1.5 (c (n "rectangle-pack") (v "0.1.5") (h "1ac93wzlzvffnniz5lc36gfaq7164blz5qdhmsa9lnz5nqy322pl")))

(define-public crate-rectangle-pack-0.1.6 (c (n "rectangle-pack") (v "0.1.6") (h "066v804iplc06fpw5yybzzdkbhr3ynnq2ydjg854s7xmwsi6jsqb")))

(define-public crate-rectangle-pack-0.2.0 (c (n "rectangle-pack") (v "0.2.0") (h "1mqca3320sbh07f7vd2c5gyx8977xhf1dqni19v8866am7mvh2g5")))

(define-public crate-rectangle-pack-0.2.1 (c (n "rectangle-pack") (v "0.2.1") (h "09axgbgmh77ix8j9wngn8cdw9pswsddv1yndr275zv255wgjn9r0")))

(define-public crate-rectangle-pack-0.2.2 (c (n "rectangle-pack") (v "0.2.2") (h "0h3bwgqx3r9sk4l4c9hl5akb275y6kbj27hd5nbn2pqg1l0pbwwx")))

(define-public crate-rectangle-pack-0.3.0 (c (n "rectangle-pack") (v "0.3.0") (h "0spiaw2w3fnrfi3cs9r8a639imr8kb028b9z5b3hjaxpnpyb47l3")))

(define-public crate-rectangle-pack-0.4.0 (c (n "rectangle-pack") (v "0.4.0") (h "1q5rf0xrrv8zwws32l2ihbjql3scapqmvw8ymb9bs6f7nw7wcf97")))

(define-public crate-rectangle-pack-0.4.1 (c (n "rectangle-pack") (v "0.4.1") (h "1qdp7kv06vs00rcdqz1p3f8v4h0rq0h9jslklyg4h5yiyww11fx1")))

(define-public crate-rectangle-pack-0.4.2 (c (n "rectangle-pack") (v "0.4.2") (h "1fzr1k7yir4w15vr7iskxaqdaa0hz5k539a96hayfj20i3r67m50") (f (quote (("std") ("default" "std"))))))

