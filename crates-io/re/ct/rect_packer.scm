(define-module (crates-io re ct rect_packer) #:use-module (crates-io))

(define-public crate-rect_packer-0.1.0 (c (n "rect_packer") (v "0.1.0") (d (list (d (n "image") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0mh1akk0g6lgjmq160k59wcxvmqkjmcbpsac2pa49n9y1k2af62j")))

(define-public crate-rect_packer-0.1.1 (c (n "rect_packer") (v "0.1.1") (d (list (d (n "image") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1b1s5dl4a04jjrnmzymcjkkh94avz2b5fbi40b1fz0k0b85az12z")))

(define-public crate-rect_packer-0.2.0 (c (n "rect_packer") (v "0.2.0") (d (list (d (n "image") (r "^0.12.3") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0b03kzdnz2airr9bkkpgnqs5gl5z1665sykmphny8s7m6kfw5vb0")))

(define-public crate-rect_packer-0.2.1 (c (n "rect_packer") (v "0.2.1") (d (list (d (n "image") (r "^0.12.3") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0i8wrfdgwcpdcqm7gwl6w5n5j3bnh325srj7111cq0abvbgv9zyq")))

