(define-module (crates-io re ct rectangle-device-blocks) #:use-module (crates-io))

(define-public crate-rectangle-device-blocks-0.1.0 (c (n "rectangle-device-blocks") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libipld") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "1cqricmpkcx11s66wj17c084f80r2pqv0l3pr1kfap246zyqk187")))

