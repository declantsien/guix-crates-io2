(define-module (crates-io re ct rectangle-device-media) #:use-module (crates-io))

(define-public crate-rectangle-device-media-0.1.0 (c (n "rectangle-device-media") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "m3u8-rs") (r "^1.0") (d #t) (k 0)) (d (n "mpeg2ts") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rectangle-device-blocks") (r "^0.1.0") (d #t) (k 0)) (d (n "rectangle-device-player") (r "^0.1.0") (d #t) (k 0)) (d (n "rectangle-device-sandbox") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10igshanfm7rcmyxw58zvm3m5y8jqarf5rvw2r5f1nbnsplqlqs7")))

