(define-module (crates-io re ct rectutils) #:use-module (crates-io))

(define-public crate-rectutils-0.1.0 (c (n "rectutils") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1gl1yxz675f1m2p24bhhia6jml18dy8s81q9cvz00hji0kv026vv") (r "1.72")))

