(define-module (crates-io re ct rectangle-device) #:use-module (crates-io))

(define-public crate-rectangle-device-0.1.0 (c (n "rectangle-device") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "file_limit") (r "^0.0.2") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rectangle-device-media") (r "^0.1.0") (d #t) (k 0)) (d (n "rectangle-device-network") (r "^0.1.0") (d #t) (k 0)))) (h "02jpim7rjrk9l6s69rm5n5rljg3azvgimds50w4p2a79yajrdg8p")))

