(define-module (crates-io re ct rectree3) #:use-module (crates-io))

(define-public crate-rectree3-0.1.0 (c (n "rectree3") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0m557wcc2cnb5mq8ikzccss6ngknra41gfm4b6yiw3m26hnkgfy8")))

(define-public crate-rectree3-0.2.0 (c (n "rectree3") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "15ai0lrsw5wfyzpbsq5zqgargd1sabv48fa75fis37l0f86f26fn")))

(define-public crate-rectree3-0.3.0 (c (n "rectree3") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1y0qnpcvdbph026z6gk56n6q1y16brnzalamjixing9gps5ap2q7")))

(define-public crate-rectree3-0.3.1 (c (n "rectree3") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.18.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1g0i7ihmligillzragxzhnpw3iwqs67cd1r5rv67cwkxbl6h0izf")))

(define-public crate-rectree3-0.3.2 (c (n "rectree3") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.18.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1shhliq11aij8pvh3vf6pjv89mrf96ghs5jgydhk5a74f9bb0qbl")))

(define-public crate-rectree3-0.3.3 (c (n "rectree3") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.18.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "13fwf5zyz89v548j3nva75q9pym65nqpk1w8f23mz9l4y2c5v9w7")))

(define-public crate-rectree3-0.3.4 (c (n "rectree3") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0qppjki0lbwxs3v2w8nrf1kyah0mvs4719zrg727cs5ls1qymiy5")))

(define-public crate-rectree3-0.4.0 (c (n "rectree3") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "02galaq5ds6a9q7ng056yxbs0mr844m8msbjs3xjxj92n3l6yd4n")))

(define-public crate-rectree3-0.5.0 (c (n "rectree3") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "06yrnqgv0synzrmd62bv2543q0hm78s474h86lifaixn0x15jpxf")))

(define-public crate-rectree3-0.5.1 (c (n "rectree3") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "07zpd3plp4hp85m1jbjg3k4v6dib8hpzp3d3953mgyy152flzdvy")))

(define-public crate-rectree3-0.5.2 (c (n "rectree3") (v "0.5.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0217xc1dbgxxl6lg7v9dh0r2abj7bx4pgwmbfc3iwhv2rdss786i")))

(define-public crate-rectree3-0.5.3 (c (n "rectree3") (v "0.5.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1hdwi8hi2j75hf8qxh6y0464nyikprplqhkc2l7cy13c9912dqg5")))

