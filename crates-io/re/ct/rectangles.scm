(define-module (crates-io re ct rectangles) #:use-module (crates-io))

(define-public crate-rectangles-0.1.0 (c (n "rectangles") (v "0.1.0") (h "0ahw6rl1p63df6pbyrkzf0a2ysc23aix7i62hx078xh7b83wbnk1")))

(define-public crate-rectangles-0.2.0 (c (n "rectangles") (v "0.2.0") (h "1gi5dfmf5q3y2cdsp1mb00rnl6w0ksxj06gl8vyjmcpmlmhbl6n3")))

(define-public crate-rectangles-0.3.0 (c (n "rectangles") (v "0.3.0") (h "0byh903j4jjmy1h82pcskap9dn4di9q560scijgvrq0wqcq099yc")))

