(define-module (crates-io re jo rejoin_slice) #:use-module (crates-io))

(define-public crate-rejoin_slice-1.0.0 (c (n "rejoin_slice") (v "1.0.0") (h "1mijrvqqzxn9g2v60z8ry0aha0d1xn5df6c4g470dab7xasvzd64") (y #t)))

(define-public crate-rejoin_slice-1.0.1 (c (n "rejoin_slice") (v "1.0.1") (h "0rg3i7snmkqlkjlqjp5728qygl9z4jllaiv3y9d3vsc8n8wdnb9p") (y #t)))

(define-public crate-rejoin_slice-1.0.2 (c (n "rejoin_slice") (v "1.0.2") (h "0rzhv3ms1fczyg52bqzcfi80rpxi7j7kbawaaxgs26lgk8j7w3sd") (y #t)))

(define-public crate-rejoin_slice-1.0.3 (c (n "rejoin_slice") (v "1.0.3") (h "15cz5378zldsbqs0ldbhvcm7wi0r69wa7m762dmkjqm9gz79zf4y") (y #t)))

