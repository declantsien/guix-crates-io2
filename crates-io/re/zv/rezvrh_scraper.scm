(define-module (crates-io re zv rezvrh_scraper) #:use-module (crates-io))

(define-public crate-rezvrh_scraper-0.1.0 (c (n "rezvrh_scraper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("full"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fs2yhmdm1i3qnr161nvh1hx6ivmpafbw6g6mprczp2x1skmln5s")))

