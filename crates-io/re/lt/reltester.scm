(define-module (crates-io re lt reltester) #:use-module (crates-io))

(define-public crate-reltester-0.1.0 (c (n "reltester") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1z484gq3lqf0a2fq1hlr5lz08amh3vcgc6cr29jp7abl0d7z43s1")))

(define-public crate-reltester-1.0.0 (c (n "reltester") (v "1.0.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "18xvkiwdl6s20n79fka2szjd6q4awba0rq5y9n29nk0yavzwav5s")))

(define-public crate-reltester-1.0.1 (c (n "reltester") (v "1.0.1") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0mkpwbjf2ff3mm77lwcbhjziy2sbav6n2rkzbkxsj40gwaqdhp7z")))

(define-public crate-reltester-2.0.0 (c (n "reltester") (v "2.0.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07qd8vzdg71zfzp2v6gy3v01z5dw1v3r9vwqcaw5la0n770nm4nd") (r "1.56")))

