(define-module (crates-io re ru rerup) #:use-module (crates-io))

(define-public crate-rerup-1.0.7 (c (n "rerup") (v "1.0.7") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "env") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnjkyb6zy8wfx0a00am33dy7v1n9ca14sdar2w69ds1nx7cyr9p")))

(define-public crate-rerup-1.0.8 (c (n "rerup") (v "1.0.8") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "env") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kij4006jwwc53p395cpy7cw0djzrf26dy8azpxhihgwryraxlkp")))

(define-public crate-rerup-1.0.9 (c (n "rerup") (v "1.0.9") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "env") (r "^0.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18wyk1hbldj6frh9ibcdyhi8ji8rw19xldpr4drgybh44j7vhqax")))

