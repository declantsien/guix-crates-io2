(define-module (crates-io re ru rerust) #:use-module (crates-io))

(define-public crate-rerust-0.1.0 (c (n "rerust") (v "0.1.0") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-signals") (r "^0.3") (d #t) (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0drllw89zyc29q8pyzqzj0r0flp18v2s9lvfdw5zx5yd3zihv92b")))

