(define-module (crates-io re ru reru) #:use-module (crates-io))

(define-public crate-reru-0.1.0 (c (n "reru") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1k2n54w9q7agfgd73xg5qnj68cbhsh76pni0fypg7b5zd9rvgl9f") (f (quote (("json" "serde" "serde_json") ("default" "json")))) (y #t)))

(define-public crate-reru-0.1.1 (c (n "reru") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1ig4izvl0c0596iw64sdp4yssgw7wwrnd5g0m78lh65q960j8hjk") (f (quote (("json" "serde" "serde_json") ("default" "json")))) (y #t)))

(define-public crate-reru-0.1.2 (c (n "reru") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "12c5ib9n5v78ln64gjncvrvkv4zr6r2mnjliknzjry0d7b2772yc") (f (quote (("json" "serde" "serde_json") ("default" "json")))) (y #t)))

