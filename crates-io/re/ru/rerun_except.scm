(define-module (crates-io re ru rerun_except) #:use-module (crates-io))

(define-public crate-rerun_except-0.1.0 (c (n "rerun_except") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0094pyl5pp9fpgr02wkzcn0isnrwr7pz58qmfgmwap30cl1wkk6n")))

(define-public crate-rerun_except-0.1.1 (c (n "rerun_except") (v "0.1.1") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "07iw19m6qg0xmvs4mhlxzyikhr431vis8nn1r13a5vk0jv770hzp")))

(define-public crate-rerun_except-0.1.2 (c (n "rerun_except") (v "0.1.2") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "04jbdijs0jv1pxpfvqffhxa7s93xvfysalv1l31q9y6j37j9dlp7")))

(define-public crate-rerun_except-1.0.0 (c (n "rerun_except") (v "1.0.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "1casvdbwf0xzcrxcqpc7dxvv6m45iqbapjw57y22pn01xxdlm5w9")))

