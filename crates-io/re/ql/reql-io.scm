(define-module (crates-io re ql reql-io) #:use-module (crates-io))

(define-public crate-reql-io-0.0.0 (c (n "reql-io") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "ql2") (r "^1.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "reql") (r "^0.0.6-alpha1") (d #t) (k 0)) (d (n "scram") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1m26ch27k48l5cpc53jkv5wz9sc4mbdpbbdix452dr2dgsj6dij8") (y #t)))

(define-public crate-reql-io-0.1.0 (c (n "reql-io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "scram") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1a1bvlhywxrrd6x8rpklrcmpfhf0xd4d0fh02vrhvcq05ald9h2m") (y #t)))

(define-public crate-reql-io-0.2.0 (c (n "reql-io") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "scram") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0d1dsghsq39bn7dkid2mzyqkj2wjcsj4vq8hh0d4v4hn3jf4jg14") (y #t)))

