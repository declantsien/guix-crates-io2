(define-module (crates-io re ql reqly) #:use-module (crates-io))

(define-public crate-reqly-0.0.0 (c (n "reqly") (v "0.0.0") (d (list (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0p634bdnl4gbcjyy317dahy5j6myrhrlalj9vy2lx3vaycqq4m01")))

