(define-module (crates-io re ql reql-derive) #:use-module (crates-io))

(define-public crate-reql-derive-0.0.6-alpha1 (c (n "reql-derive") (v "0.0.6-alpha1") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "07jf88p4q9wzhx6xr6xrzvmsbgy3klivc6rm4g20shmnk0y6ajk0") (y #t)))

(define-public crate-reql-derive-0.0.6-alpha2 (c (n "reql-derive") (v "0.0.6-alpha2") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1m1gfpsxhj37gfnbqmp7iqjwcabxn67kv9ks6nch2r52y8nmlqpp") (y #t)))

(define-public crate-reql-derive-0.0.6-alpha3 (c (n "reql-derive") (v "0.0.6-alpha3") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1cp9sfvddh2c1rcg291k82c46zpyijcpakgygfkcd12xx925hviq") (y #t)))

(define-public crate-reql-derive-0.0.6-alpha4 (c (n "reql-derive") (v "0.0.6-alpha4") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "14c5jgpx9kr4hws2r1swp0m8sz71yw6gma6yqkwqib0ml9v4hlca") (y #t)))

(define-public crate-reql-derive-0.0.6 (c (n "reql-derive") (v "0.0.6") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18ncc8x52jfq1axw3vsfsbam8c42kyk5lalhnss4n1qm00326ajz") (y #t)))

(define-public crate-reql-derive-0.0.7 (c (n "reql-derive") (v "0.0.7") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0gcxk26dq66wa7pz1ma3s7jqqsbrbyg6zgy5dgvj27rdmq44ajw7") (y #t)))

(define-public crate-reql-derive-0.0.8 (c (n "reql-derive") (v "0.0.8") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "093qfqq06fwfyd4qrxmr863zf2xvildm3xmr3lkspng6ir7slv4x") (y #t)))

(define-public crate-reql-derive-0.0.9 (c (n "reql-derive") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g4h8332m7rrp54iqz1d82nf0bihdd7rmpmdcihigrv8rwjz0knb") (y #t)))

(define-public crate-reql-derive-0.0.10 (c (n "reql-derive") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xb68s2drmvccrvh8dcmcdmmj8jrk5y04bf1pkgzm8lfz50bycjn") (y #t)))

