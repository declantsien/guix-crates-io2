(define-module (crates-io re ql reql-types) #:use-module (crates-io))

(define-public crate-reql-types-0.0.1 (c (n "reql-types") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std" "v4" "serde"))) (d #t) (k 0)))) (h "0x7zhyyqx7r6js4cs8s6a809lvclvkh87jn9194d8rnk8yaskfbk")))

(define-public crate-reql-types-0.0.2 (c (n "reql-types") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std" "v4" "serde"))) (d #t) (k 0)))) (h "0ildh0mhpr4kf122bn84zad2b28yk5j6kk3nb90h88jfdah2zbsx")))

(define-public crate-reql-types-0.0.3 (c (n "reql-types") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1np6q1k5bhm7vy7pypykigmzswyjrny57j9lhicxsg89k9vm8lz4")))

(define-public crate-reql-types-0.0.4 (c (n "reql-types") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1mm2xs4sg4l4mm3klv4190njk1r0p1fmkx47mymf2r4hwfya08mh")))

(define-public crate-reql-types-0.0.5 (c (n "reql-types") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1lsb3ki98mzqfv4xv0yqcbcvhm9p0sbzmz1mvshh07bqndjp3myi")))

(define-public crate-reql-types-0.0.6 (c (n "reql-types") (v "0.0.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "16kkinhyxxd6vl1clwc3iy16yd987my1q5712xavrl9y900l7bh8")))

(define-public crate-reql-types-0.1.0 (c (n "reql-types") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1imqghfspy6x36bxmrlw7d05v63gd1kaggm1sz04517c5fwrj8sl")))

(define-public crate-reql-types-0.2.0 (c (n "reql-types") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0v8r8h6h2m93bsrx95apipck09zfd8b729ysgkxf7x1x9lzjzpzb")))

(define-public crate-reql-types-0.2.1 (c (n "reql-types") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1bkmlmx15hcw9150i5vvx6h1h46j22r5wn3n0pm4s58y4nnjhmj4")))

(define-public crate-reql-types-0.3.0 (c (n "reql-types") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0cd8dg2z2vg8i14glymvnyk822ymf6x0ga74xm9vmy3aznnxq0mb")))

(define-public crate-reql-types-0.3.1 (c (n "reql-types") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0rxy7774ffpy1zqsxcvsxqh98s3h641b7f5hkxzv09zgc35fyv66")))

(define-public crate-reql-types-0.3.2 (c (n "reql-types") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1n5vhxmfxbraa5xfy962axy4wfc732ps0pbd7c014dc9bm761sza")))

(define-public crate-reql-types-0.3.3 (c (n "reql-types") (v "0.3.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0w5f194gl85dc0ak9zykf55s4dixg5ivpjbbqjimc0ajakzzgcln")))

