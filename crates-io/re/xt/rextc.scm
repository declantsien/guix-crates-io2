(define-module (crates-io re xt rextc) #:use-module (crates-io))

(define-public crate-rextc-0.1.0 (c (n "rextc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "021v6hkcq7ib2xmyc1lxmxz64m9fi68vf0prz334wv07bkbam37m")))

(define-public crate-rextc-0.1.1 (c (n "rextc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0y7hs2wxd1bly9bznncafn2c0cr6na7ab38azjdhqjw1k26giv5q")))

(define-public crate-rextc-2.0.0 (c (n "rextc") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "03lglaph03kr9asc3lkizncgv209ajczgsxv49i65p9mgp8l1yd0")))

(define-public crate-rextc-2.0.1 (c (n "rextc") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1rlrb7vpqrifclpkain2l40rlw021p231r2flhba79ybvgaa3q95")))

