(define-module (crates-io re xt rexturl) #:use-module (crates-io))

(define-public crate-rexturl-0.1.0 (c (n "rexturl") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1yrplplhgxz4lpnzzg9npaq0dqb8cbav1kg48gvb3cgxldqqf8j4")))

(define-public crate-rexturl-0.1.1 (c (n "rexturl") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0cf518vb529wwwp7cjkf949bqdxqfmp0vzf36rzdsyr3v2cvgn0m")))

(define-public crate-rexturl-0.1.2 (c (n "rexturl") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1syjifgvyx8nih8k4hqsm027hfkbp87h55wpck232mgc99lxnsg6")))

(define-public crate-rexturl-0.2.0 (c (n "rexturl") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "19f67ng1as9nql0y8nxccdw3k2ifpxl42hvwr4dgasq09jga3gd2")))

(define-public crate-rexturl-0.2.1 (c (n "rexturl") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1vhjqwx597i9a5yrc5m500xir44637j810jzyn91vgg7l5p9ji3l")))

(define-public crate-rexturl-0.2.2 (c (n "rexturl") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1n1l2ivk9magrkv8fc4y9c3ghal9ha53r7d9z74k46jr19fmyl66")))

(define-public crate-rexturl-0.3.0 (c (n "rexturl") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("cargo" "env" "derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0s3w3bv7p9wspyrnra3xz6m5zrxkcqmdk4dh6vglhl3hksd4rr8c")))

