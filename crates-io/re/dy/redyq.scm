(define-module (crates-io re dy redyq) #:use-module (crates-io))

(define-public crate-redyq-0.1.0 (c (n "redyq") (v "0.1.0") (d (list (d (n "redis") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "0q4ymcslmrmnrddyhmc9y34mc0vfj21c631kji3yln90aa3is0py")))

(define-public crate-redyq-0.1.1 (c (n "redyq") (v "0.1.1") (d (list (d (n "redis") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1idip1i85w6f22jyh74xmpzhkpn6azhsjqr0g9akp76dbn9px4dh")))

