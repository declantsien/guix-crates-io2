(define-module (crates-io re dp redpanda-transform-sdk-sr) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-sr-1.0.0 (c (n "redpanda-transform-sdk-sr") (v "1.0.0") (d (list (d (n "redpanda-transform-sdk-sr-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.0") (d #t) (k 0)))) (h "0r0kb13hjqk8mvy03s76pqhnk8krlqh4g6x2ah2qhr0ba5nkmsgy") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-1.0.1 (c (n "redpanda-transform-sdk-sr") (v "1.0.1") (d (list (d (n "redpanda-transform-sdk-sr-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.1") (d #t) (k 0)))) (h "18zys73q0fgd81wdmzzhsqf64lbd3cy1k030klgzq4n66g978bq3") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-1.0.2 (c (n "redpanda-transform-sdk-sr") (v "1.0.2") (d (list (d (n "redpanda-transform-sdk-sr-sys") (r "^1.0.2") (d #t) (k 0)) (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.2") (d #t) (k 0)))) (h "1x8lcfmmgg3s89kp6vl7kzkfsqfawncbhj3xbxam12bmngra60fk") (r "1.72.0")))

