(define-module (crates-io re dp redpanda-http-rust) #:use-module (crates-io))

(define-public crate-redpanda-http-rust-0.0.1 (c (n "redpanda-http-rust") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1q2ilrhi4dg8qbvc2bszlg651ysrnxdz7pmk7qiqb13arhi909yr") (f (quote (("defaults")))) (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

