(define-module (crates-io re dp redpine) #:use-module (crates-io))

(define-public crate-redpine-0.1.0 (c (n "redpine") (v "0.1.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "polling") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 0)))) (h "1ybz3b4aj3xxhqh1kn83l2p16v3yj3zmwrwv1jq7hd57wvvzhj1c")))

(define-public crate-redpine-0.2.0 (c (n "redpine") (v "0.2.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "polling") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 0)))) (h "1kg1qd6hacdw2nvy74yhair71i6p6cfnq7rdnj0yck3177m2w9cx")))

(define-public crate-redpine-0.2.1 (c (n "redpine") (v "0.2.1") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "polling") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 0)))) (h "123k2mv9cfw5fdi8cwx55pwxrgjma2klk0f0dl32a6rrc63gcsli")))

