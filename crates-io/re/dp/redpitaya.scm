(define-module (crates-io re dp redpitaya) #:use-module (crates-io))

(define-public crate-redpitaya-0.0.0 (c (n "redpitaya") (v "0.0.0") (h "1a4xf9yfrapdc4adq771cgjqdf62c5cliwc3kp1l5cbhk93w3690")))

(define-public crate-redpitaya-0.1.0 (c (n "redpitaya") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.3") (d #t) (k 0)))) (h "0zn5cfpqsnf6fgn55sfbxv5gf10bb78zvpiw7r8fk48y27dl54dl")))

(define-public crate-redpitaya-0.2.0 (c (n "redpitaya") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.4") (d #t) (k 0)))) (h "1f57ibdxsx361zpcd12v4pvzanri7v5ivcix0l3n2cjb11cwky55")))

(define-public crate-redpitaya-0.3.0 (c (n "redpitaya") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.6") (d #t) (k 0)))) (h "09f80bj3l1kqjxpnib3asvj54rjfndjrblg8s4l7134c8lzi11ik")))

(define-public crate-redpitaya-0.6.0 (c (n "redpitaya") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.9") (d #t) (k 0)))) (h "0i667vpx21iyayx74v9amras95j5acihhy53zdzia7gwnp8mr997")))

(define-public crate-redpitaya-0.8.0 (c (n "redpitaya") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.9") (d #t) (k 0)))) (h "0xb16vlppacddizjjkz76c1r45likb1h9w1q1vz72zkjb0w3275y")))

(define-public crate-redpitaya-0.8.1 (c (n "redpitaya") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.10") (d #t) (k 0)))) (h "09wxkjiwjaammpnhxzsb0x4l2r59yiwhsbj8gxkdrvqqwp97rdvw")))

(define-public crate-redpitaya-0.9.0 (c (n "redpitaya") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.11") (d #t) (k 0)))) (h "1ga2cim1dh65xv76ljhf0g55raa5wxcj5mkz4107rd7ql1dv13g3")))

(define-public crate-redpitaya-0.10.0 (c (n "redpitaya") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.13") (d #t) (k 0)))) (h "0gx00c1hh3dyzwwhm6x1chli61bszs5xyli5xg1z75899hjai6h7")))

(define-public crate-redpitaya-0.10.1 (c (n "redpitaya") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.13") (d #t) (k 0)))) (h "1yrlbaf5prnwc4hgghi9pyq48sm9l1aanqwn3m766i8jyf7q73sp")))

(define-public crate-redpitaya-0.11.0 (c (n "redpitaya") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.14") (d #t) (k 0)))) (h "1x72rnhkrghhcjwpl793kvbmv6mliw60implf0ml2l3j8s6v04xy")))

(define-public crate-redpitaya-0.13.0 (c (n "redpitaya") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.16") (d #t) (k 0)))) (h "14vdfyk1r1pfkd5asp7rcgq2xgslgv20n2w791iibr64nikbdpm8")))

(define-public crate-redpitaya-0.14.0 (c (n "redpitaya") (v "0.14.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.17") (d #t) (k 0)))) (h "0fz3b4xlvh8ygg3xyvbrkprj4ds4ih5agz8nx0nc07swddcib09q")))

(define-public crate-redpitaya-0.15.0 (c (n "redpitaya") (v "0.15.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.18") (d #t) (k 0)))) (h "08qqxkjga2fw5dplfx0bn8ssy3d9ga8i3haw1p3l57iny227bk8g")))

(define-public crate-redpitaya-0.16.0 (c (n "redpitaya") (v "0.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.18") (d #t) (k 0)))) (h "0lrfvbhk9h2laj390jv3k2shr95f6pqggp4c7pl7nbgzv84qsjyq")))

(define-public crate-redpitaya-0.17.0 (c (n "redpitaya") (v "0.17.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.18") (d #t) (k 0)))) (h "0hsz3sfw7ai0ckhv35zkw62bna26l997ph8il1kc6gcggaf9qji2")))

(define-public crate-redpitaya-0.18.0 (c (n "redpitaya") (v "0.18.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.19") (d #t) (k 0)))) (h "0qah5f1qz8csa9gjsn6kpcqb8kayi8bv9bafgkpfgrkn0k0vb6xb")))

(define-public crate-redpitaya-0.19.0 (c (n "redpitaya") (v "0.19.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "03rcvf8ds5djmq07ypq0slrj3b7xndf0hqzqxd74r588gck6qfdm")))

(define-public crate-redpitaya-0.20.0 (c (n "redpitaya") (v "0.20.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "0l12h050z3msa5vpki7valirfckgjawx8hch498lqfba18q2148c")))

(define-public crate-redpitaya-0.21.0 (c (n "redpitaya") (v "0.21.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "1sl56gd5cpxri2cfd6hk6sy8nklp603848bcfchlqjjhrbx6a4vr")))

(define-public crate-redpitaya-0.22.0 (c (n "redpitaya") (v "0.22.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "0fy0n1fww7ybx0s7a6jm64pa85q9vf8hdaq2qr01jxi3gkgvx2p9")))

(define-public crate-redpitaya-0.22.1 (c (n "redpitaya") (v "0.22.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "1jf3y4jj8ybbh2745g08vslx2vcyzxa523l0kikv3mnvj133iv5j")))

(define-public crate-redpitaya-0.22.2 (c (n "redpitaya") (v "0.22.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "1z858904mfhp1rpnngipcxsf9wj31sxnjsi2i7164ql40kdf6q8g")))

(define-public crate-redpitaya-0.22.3 (c (n "redpitaya") (v "0.22.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.20") (d #t) (k 0)))) (h "04npi74kvifzivc3925dil01a1yszi3ar36hyzax71k13j5vri61")))

(define-public crate-redpitaya-0.23.0 (c (n "redpitaya") (v "0.23.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.21") (d #t) (k 0)))) (h "1sgn26z1k1q9y4fjpllaq2b23dj417vk9f0g7z7yhl5ja94sl9s9")))

(define-public crate-redpitaya-0.23.1 (c (n "redpitaya") (v "0.23.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.21") (d #t) (k 0)))) (h "1pmyhyqq4pilj2kkrl6kwdps9cns78hgcj569r5xcghcr87fqgvg")))

(define-public crate-redpitaya-0.23.2 (c (n "redpitaya") (v "0.23.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.21") (d #t) (k 0)))) (h "1shyp4wxgiyzfchqlyklsmka2mvzag5lc40zdja9ryr10z3viy63")))

(define-public crate-redpitaya-0.24.0 (c (n "redpitaya") (v "0.24.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.22") (d #t) (k 0)))) (h "1sxs70zka9bbnkb7w78gxi9nhscsxh8bng4bfmxir1vh7fszgrzi")))

(define-public crate-redpitaya-0.25.0 (c (n "redpitaya") (v "0.25.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.23") (d #t) (k 0)))) (h "0v60vicfbn1hv2d5yril5jb54xxfv93swh8c03cl6mscc1kmiih6")))

(define-public crate-redpitaya-0.26.0 (c (n "redpitaya") (v "0.26.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.24") (d #t) (k 0)))) (h "1rijk62269zbdw1lznrgbrnw779rwbw41yqb6g630qf91nycl3wj")))

(define-public crate-redpitaya-0.27.0 (c (n "redpitaya") (v "0.27.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-mock") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rp-sys") (r "^0.25") (o #t) (d #t) (k 0)))) (h "01c7ih74186lnhkblnp4lwn0db9mzk3ranj1y7aaa2f0zfnb5ri1") (f (quote (("mock" "rp-mock") ("default" "rp-sys")))) (y #t)))

(define-public crate-redpitaya-0.28.0 (c (n "redpitaya") (v "0.28.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.26") (d #t) (k 0)))) (h "0j86vsf6bq4584i7prww07jd8rxh1a64kz50006i3ns5lx8f1czm") (f (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.29.0 (c (n "redpitaya") (v "0.29.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.27") (d #t) (k 0)))) (h "02xk54pgj8bbyryfa1w1v1zrpc6gcy0c3q860qiz82njhz64i42q") (f (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.30.0 (c (n "redpitaya") (v "0.30.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.27") (d #t) (k 0)))) (h "0hghci1dfjzq8vbh906ph2im5sml388vpwhg75x8ir7p34bc4d4s") (f (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.31.0 (c (n "redpitaya") (v "0.31.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rp-sys") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1vwlx2qd22v4lpk17yg7jxjrlpbbp3xzaiszn8zwpgvibgb682k6") (f (quote (("mock" "rp-sys/mock") ("default" "rp-sys"))))))

