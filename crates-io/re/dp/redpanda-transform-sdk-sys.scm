(define-module (crates-io re dp redpanda-transform-sdk-sys) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-sys-0.0.1-dev+edeac7a3ef (c (n "redpanda-transform-sdk-sys") (v "0.0.1-dev+edeac7a3ef") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=0.0.1-dev") (d #t) (k 0)))) (h "1hn7bxvvxgrbqyvh1lrfl31ykd2kn69qwf3fsa6p6bfyps2cgcjd") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.1-rc4 (c (n "redpanda-transform-sdk-sys") (v "23.3.1-rc4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.1-rc4") (d #t) (k 0)))) (h "1gkb9as1dwc8dgv67apkx0lpgf1asxqz626xgfjv9z1mny3nx6fv") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.2-rc1 (c (n "redpanda-transform-sdk-sys") (v "23.3.2-rc1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.2-rc1") (d #t) (k 0)))) (h "1bpai5k0sjyvwdpmqifh1ppad9m1wwbmnimh5wpggr9z58ndcb1s") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.2-rc2 (c (n "redpanda-transform-sdk-sys") (v "23.3.2-rc2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.2-rc2") (d #t) (k 0)))) (h "05rbx1rybc8s2yrwafap2y7qbqiwc5wsa2war3vlihnbpzrnzcva") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.2-rc3 (c (n "redpanda-transform-sdk-sys") (v "23.3.2-rc3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.2-rc3") (d #t) (k 0)))) (h "15klcw184sdskmppw29hc3pqy3djzlqa1xxi1ad0fmvjl794h6ma") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.2 (c (n "redpanda-transform-sdk-sys") (v "23.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.2") (d #t) (k 0)))) (h "0fdl9dppbb7sp95bxia3j7jixgbwpkn4lzg2idhsr58zsm0hwlb5") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-0.1.0 (c (n "redpanda-transform-sdk-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=0.1.0") (d #t) (k 0)))) (h "1sq2hjmyi2hrmw1qrk442zkl0rgkx9x10vyib5p1qad9v28b94vv")))

(define-public crate-redpanda-transform-sdk-sys-23.3.3-rc1 (c (n "redpanda-transform-sdk-sys") (v "23.3.3-rc1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.3-rc1") (d #t) (k 0)))) (h "1s21al2vpd4wbl64maxwrjm95qhzhfs1nsp2zq3r8vabi66np0ha") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.3 (c (n "redpanda-transform-sdk-sys") (v "23.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.3") (d #t) (k 0)))) (h "1gs3p9c1i0r0sx7dw4lk2ana82kb9cpkrcrhclmb45gppdxv4rsi") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.4-rc1 (c (n "redpanda-transform-sdk-sys") (v "23.3.4-rc1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.4-rc1") (d #t) (k 0)))) (h "15353nlc51037pjmaksqx0zx5r30b36vccfk2jjk8w6zw5cbjf17") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-23.3.4 (c (n "redpanda-transform-sdk-sys") (v "23.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=23.3.4") (d #t) (k 0)))) (h "0hs42m27ssmb1v2jwnb4z8zilk94bcqyn5ha9f2xr254l983sypg") (y #t)))

(define-public crate-redpanda-transform-sdk-sys-0.2.0 (c (n "redpanda-transform-sdk-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "=0.2.0") (d #t) (k 0)))) (h "078w9n9mrhhkmqnxpxx3hb2vc96j4vqqs9dwkml5p8f3h5hzc2b2")))

(define-public crate-redpanda-transform-sdk-sys-1.0.0 (c (n "redpanda-transform-sdk-sys") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "^1.0.0") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.0") (d #t) (k 0)))) (h "0g7nlh810l8h5r03qc54b8bpxmsiazqn2zjy7kzlw9a709rkcxfy")))

(define-public crate-redpanda-transform-sdk-sys-1.0.1 (c (n "redpanda-transform-sdk-sys") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "^1.0.1") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.1") (d #t) (k 0)))) (h "0fc3hp3ka0rdvwhq2a1g92yaddnn7lmlnwgyf66sygcgd8djmkhp")))

(define-public crate-redpanda-transform-sdk-sys-1.0.2 (c (n "redpanda-transform-sdk-sys") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redpanda-transform-sdk-types") (r "^1.0.2") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.2") (d #t) (k 0)))) (h "1mcchp1sdbwbn417r7p3vl68br3hy9hi89rg5v4ma3gpaid6mkm2")))

