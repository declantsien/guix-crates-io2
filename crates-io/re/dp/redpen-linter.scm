(define-module (crates-io re dp redpen-linter) #:use-module (crates-io))

(define-public crate-redpen-linter-0.1.0 (c (n "redpen-linter") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.10") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "1dpqyspx8zlsbrwbi4di6pk1gxba2h6rb25shqd4x9k9vgkybm3j") (r "1.72.0")))

(define-public crate-redpen-linter-0.2.0 (c (n "redpen-linter") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.10") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "0zm410mzbdiw75cpfkbh4c9q4bgic63lswr2q09mhv0gdjz4kn7i") (r "1.72.0")))

(define-public crate-redpen-linter-0.3.0 (c (n "redpen-linter") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.10") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "1h7gy1h9njnkzja4zv5wq90jdwdi0jsqzcp4xkp0z0d3dcwz4057")))

(define-public crate-redpen-linter-0.4.0 (c (n "redpen-linter") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.10") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "17gjw3q7hj7a7cq888bjkwpva0907px9cak7bwrfr7yzm5sgbf4h")))

