(define-module (crates-io re dp redpanda-transform-sdk-types) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-types-0.0.0-dev+6956980246 (c (n "redpanda-transform-sdk-types") (v "0.0.0-dev+6956980246") (h "0370047gc3k2ix1izyfxfk2rv2lnn398x97pwj6j3a8kx9zdpli3") (y #t)))

(define-public crate-redpanda-transform-sdk-types-0.0.1-dev+edeac7a3ef (c (n "redpanda-transform-sdk-types") (v "0.0.1-dev+edeac7a3ef") (h "1rcxmi1srinqdyvkch546kbbk0amwrpf4zrnaabgfq4j0kbp5vq8") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.1-rc4 (c (n "redpanda-transform-sdk-types") (v "23.3.1-rc4") (h "084q7wr1zssg705d45nkg7ksl847r3yh4lrlynqr3a4cxsmz3z90") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.2-rc1 (c (n "redpanda-transform-sdk-types") (v "23.3.2-rc1") (h "191chhm672yca0jwycg0m8y0b1l538a76jw6n0lqwjshpqj8473h") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.2-rc2 (c (n "redpanda-transform-sdk-types") (v "23.3.2-rc2") (h "17l7s65rpkgxa0x2514p2r997phgdc8ayavcz7zqhfciy787gp1b") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.2-rc3 (c (n "redpanda-transform-sdk-types") (v "23.3.2-rc3") (h "1pqk6c1rg45s8a7qscp7pnss3jimwfmsn447hsdyrxq2lrzmhfbv") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.2 (c (n "redpanda-transform-sdk-types") (v "23.3.2") (h "08rjmimjhk0wgwa29cp6gbl562964cha3p6r796j05mlfja3ms9p") (y #t)))

(define-public crate-redpanda-transform-sdk-types-0.1.0 (c (n "redpanda-transform-sdk-types") (v "0.1.0") (h "1g75pl89q10dqxw43lb683v14h1a504pxx993xf2c4a2sfrpfn9d")))

(define-public crate-redpanda-transform-sdk-types-23.3.3-rc1 (c (n "redpanda-transform-sdk-types") (v "23.3.3-rc1") (h "168p4if8qvdh7316lsnxn5f7xhyw2xi4nz147imnh814ngdxw9sw") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.3 (c (n "redpanda-transform-sdk-types") (v "23.3.3") (h "1bj534z8aqkjbqd6fq94aj11xdxfpqfpan97za2x1w7xcx1fp2cc") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.4-rc1 (c (n "redpanda-transform-sdk-types") (v "23.3.4-rc1") (h "0fd3my5pw3450qjmijr1ad1vnrjvcr6a8gyv1jy0p7vswzd6k7cl") (y #t)))

(define-public crate-redpanda-transform-sdk-types-23.3.4 (c (n "redpanda-transform-sdk-types") (v "23.3.4") (h "0syjp2y2x7s5g9zv3ldz8y56b98ipkrsl0qfdjnx1pxxwkaaahys") (y #t)))

(define-public crate-redpanda-transform-sdk-types-0.2.0 (c (n "redpanda-transform-sdk-types") (v "0.2.0") (h "0fqi7ln1nszhsxk6xwg03yqjjb9r06scfsglq15w33rh00f43dr2")))

(define-public crate-redpanda-transform-sdk-types-1.0.0 (c (n "redpanda-transform-sdk-types") (v "1.0.0") (h "0s3g7i3h2rryfkj22g4ixvly87fff7q9drka35kdf7jm1sd3kr3v")))

(define-public crate-redpanda-transform-sdk-types-1.0.1 (c (n "redpanda-transform-sdk-types") (v "1.0.1") (h "0k9s0vwcisc2l2mjg15yddl4ak78rx27af8jf7v9ifi6mnwwrgv8")))

(define-public crate-redpanda-transform-sdk-types-1.0.2 (c (n "redpanda-transform-sdk-types") (v "1.0.2") (h "1gzybw9w6lg7rm7svvz5yq0d4b16plczshy1l4gwdk6ka795vz9s")))

