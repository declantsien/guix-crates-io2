(define-module (crates-io re dp redpen-shim) #:use-module (crates-io))

(define-public crate-redpen-shim-0.1.0 (c (n "redpen-shim") (v "0.1.0") (h "02z60vyx8lqvx9blysl7pnxqdi9h6v61xmj1kx0hh547c23gvgcy")))

(define-public crate-redpen-shim-0.2.0 (c (n "redpen-shim") (v "0.2.0") (h "05s905lc53ignh1xrxb4zrwsf45hq4j0biz7b3wl0qkgif88x5gg")))

(define-public crate-redpen-shim-0.4.0 (c (n "redpen-shim") (v "0.4.0") (h "0zji92s4k6c4kdcp1jwh60zfq1xjssqizmai514l5vxs67fys3jk")))

