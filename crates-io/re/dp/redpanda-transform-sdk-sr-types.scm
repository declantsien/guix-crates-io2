(define-module (crates-io re dp redpanda-transform-sdk-sr-types) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-sr-types-1.0.0 (c (n "redpanda-transform-sdk-sr-types") (v "1.0.0") (d (list (d (n "redpanda-transform-sdk-varint") (r "^1.0.0") (d #t) (k 0)))) (h "16y36k7hfa7anl7bs92a5p39dqfr3nk6awz9mf7skpgfz3p4z1y8") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-types-1.0.1 (c (n "redpanda-transform-sdk-sr-types") (v "1.0.1") (d (list (d (n "redpanda-transform-sdk-varint") (r "^1.0.1") (d #t) (k 0)))) (h "16bbfg3vhfb26ynw21v47c0qv76qycc5n8044hdj8c2dl0xpr1j7") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-types-1.0.2 (c (n "redpanda-transform-sdk-sr-types") (v "1.0.2") (d (list (d (n "redpanda-transform-sdk-varint") (r "^1.0.2") (d #t) (k 0)))) (h "17iqw99c3fbz3gqxvkrpcv16pkgnwxy22hgkc3x1ww49nfdp0n01") (r "1.72.0")))

