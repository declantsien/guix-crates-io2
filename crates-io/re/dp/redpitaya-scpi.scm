(define-module (crates-io re dp redpitaya-scpi) #:use-module (crates-io))

(define-public crate-redpitaya-scpi-0.1.0 (c (n "redpitaya-scpi") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1141cis3p5kzqk7yjh16i1bim4dw2mc1ivxb4097zgdhvvpzi1ld")))

(define-public crate-redpitaya-scpi-0.2.0 (c (n "redpitaya-scpi") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1vdfr75h9fk7bgisgp60vmbs743sz6xmzq1vwh7322iqprdjnqq0")))

(define-public crate-redpitaya-scpi-0.3.0 (c (n "redpitaya-scpi") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0vna7zmx7i4vq63kjllnnw5g16bkg61qw4p5m4qwph67vvmxzqps")))

(define-public crate-redpitaya-scpi-0.4.0 (c (n "redpitaya-scpi") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1kp0xdg3zk8mknhsqvkmzf31prnycb5s13qgi55lcpwy1hpr8kl2")))

(define-public crate-redpitaya-scpi-0.5.0 (c (n "redpitaya-scpi") (v "0.5.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rd43qff9qdn6ww168d90x34f6av98cmnl0w1h617g407r0jsspy")))

(define-public crate-redpitaya-scpi-0.6.0 (c (n "redpitaya-scpi") (v "0.6.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1jxmxmpzisf3s63sy9v8p8iaa38qwcyrcb94ds8fchjikvw6w3yf")))

(define-public crate-redpitaya-scpi-0.7.0 (c (n "redpitaya-scpi") (v "0.7.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "129x2xjm71100k5vhn4af747hrah2w7hkgdbfilk2s1d9l3hfaki")))

(define-public crate-redpitaya-scpi-0.8.0 (c (n "redpitaya-scpi") (v "0.8.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0sfm37kp33iv8n8xld64ql8jgki26608yxf7cp7bgah4vc0w9153")))

(define-public crate-redpitaya-scpi-0.9.0 (c (n "redpitaya-scpi") (v "0.9.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "10qpf0fdhbh16k8ykv9k2jhpmakb3cb0mlwg2s1yv2qmqm539bsw")))

(define-public crate-redpitaya-scpi-0.10.0 (c (n "redpitaya-scpi") (v "0.10.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1vl2vcp80r0yi2x47zmq9dmqzx4k9s8lppgf566jgi8fki2k53yv")))

(define-public crate-redpitaya-scpi-0.10.1 (c (n "redpitaya-scpi") (v "0.10.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0mjs6rk5d1k6jfigkcypbj6ppqkw1isj204jyc8mq7hrfww6vy80")))

(define-public crate-redpitaya-scpi-0.11.0 (c (n "redpitaya-scpi") (v "0.11.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0fm87li2gvzl3d33rpmf158wf3szz3l5c8v0hkp83phphyzpy0rc")))

(define-public crate-redpitaya-scpi-0.12.0 (c (n "redpitaya-scpi") (v "0.12.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0jsjb9hm9az333gz4pw84l58xcawhksjgr3waxl7qmi92k5ac87m")))

(define-public crate-redpitaya-scpi-0.13.0 (c (n "redpitaya-scpi") (v "0.13.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "12m9179p05bl4v5m2a55smix00mnksy60zwqsx21sh4ml1ly1rg9")))

(define-public crate-redpitaya-scpi-0.14.0 (c (n "redpitaya-scpi") (v "0.14.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0i6llv4bbsjq4lyqphglaxb4fxk9xlxq9d93im4p1sphp96sq9zz")))

(define-public crate-redpitaya-scpi-0.15.0 (c (n "redpitaya-scpi") (v "0.15.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1swcg4akaygp13i82cysq621bjv3ifp8a4g816abbny1fjd84067")))

(define-public crate-redpitaya-scpi-0.15.1 (c (n "redpitaya-scpi") (v "0.15.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0zg2jh14a5gxipbmiacif9856gfsafqvbjlij8znydy0g7fwl3zy")))

(define-public crate-redpitaya-scpi-0.16.0 (c (n "redpitaya-scpi") (v "0.16.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0rhqh7p351s3kn9p47s7xv134w4qg6ys4bq9nvdhrm95bir2ysky")))

(define-public crate-redpitaya-scpi-0.17.0 (c (n "redpitaya-scpi") (v "0.17.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1dd03sdv703gln5vvhslp9bhhskbyivfmdk5xwlz55h92ph2vnms")))

(define-public crate-redpitaya-scpi-0.18.0 (c (n "redpitaya-scpi") (v "0.18.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0ll5x8d2kccwdhq5kga6i501mhlq4jfp56jhjahdj18yqlgs6cly")))

(define-public crate-redpitaya-scpi-0.19.0 (c (n "redpitaya-scpi") (v "0.19.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1iqs9qfk80qnhxnmhn0zaads2fi8xar0k4szr40w7fv9jjd8jdpp")))

(define-public crate-redpitaya-scpi-0.20.0 (c (n "redpitaya-scpi") (v "0.20.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1pqnn0bh18ymkvavpyisv93ckzjfk35hjgyikhk1s4jq5whwhbh9")))

(define-public crate-redpitaya-scpi-0.21.0 (c (n "redpitaya-scpi") (v "0.21.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "12kfpq2npc2wc9hjbj9rmxzyhyi8lmlng9izg18mh3bwf1gnrkxw")))

(define-public crate-redpitaya-scpi-0.22.0 (c (n "redpitaya-scpi") (v "0.22.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "09n0d2yjkdya4065zmhwr3aq9qbr6r8wpspxbjla6sxpyhykfjsk")))

(define-public crate-redpitaya-scpi-0.23.0 (c (n "redpitaya-scpi") (v "0.23.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1br7v8401v96qdk2k6s5sm9s47g9fmcbc93d6f7vwssnrx5cpbha") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.23.1 (c (n "redpitaya-scpi") (v "0.23.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0hkb0m0bj7iry5zq27j01i6ghix8lkn7dfcf7939xlyx203sf1dy") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.24.0 (c (n "redpitaya-scpi") (v "0.24.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1zjd5m2ckqxrgndfipmg29xis6r17aq5hfd3wvffz99r4clnnzhj") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.25.0 (c (n "redpitaya-scpi") (v "0.25.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rqq33fbi4dv8xvgq9kxb50fb1fzjcgazqy1vf8s04yczs8vp712") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.26.0 (c (n "redpitaya-scpi") (v "0.26.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1sk7rrr3fp72a7prkwjzxmffkp44nh05m7664b6angw8d31cznpc") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.27.0 (c (n "redpitaya-scpi") (v "0.27.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "16p095njbxcdfi068zm22vw123nvnq759wp71vlsm89z3x1bbxi9") (f (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.27.1 (c (n "redpitaya-scpi") (v "0.27.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0w7xrw6jvhm4ci6lppslv32rgkqk1vq8g2m72g89zc09v1xrr4mr") (f (quote (("mock") ("default"))))))

