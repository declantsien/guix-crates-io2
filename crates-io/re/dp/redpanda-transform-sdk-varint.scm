(define-module (crates-io re dp redpanda-transform-sdk-varint) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-varint-1.0.0 (c (n "redpanda-transform-sdk-varint") (v "1.0.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1aa1x2v1ch3d8plzhmlhlv5vm42lg5rsxss54hbhriciwssv4g6v") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-varint-1.0.1 (c (n "redpanda-transform-sdk-varint") (v "1.0.1") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0k121rajk2dxhw7dkh3s7pi28nj4ndi16y507mv7m6ll0b79xzcn") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-varint-1.0.2 (c (n "redpanda-transform-sdk-varint") (v "1.0.2") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "027bdac4341drflw8v5hzp3jyny0ba9q8wkxwyxfgid908yvxwzf") (r "1.72.0")))

