(define-module (crates-io re dp redpanda-http) #:use-module (crates-io))

(define-public crate-redpanda-http-0.0.1 (c (n "redpanda-http") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "simplehttp") (r "^0.0.1") (d #t) (k 0)))) (h "0q9324a0x1s06c5wpc40kmpc1fmbhpfs5dl2i9j2lmlcgvn3814i")))

(define-public crate-redpanda-http-0.0.2 (c (n "redpanda-http") (v "0.0.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "simplehttp") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0zk83q7scqgnwh3ncdxhig1zxa1rdhqxcy0nhqbdsmdbf6881iji")))

(define-public crate-redpanda-http-0.0.4 (c (n "redpanda-http") (v "0.0.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "simplehttp") (r "^0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0sa23lzgbfa78z7737cvksqv84hsd316v0f4q6mf3bwr3xpfq8sb")))

