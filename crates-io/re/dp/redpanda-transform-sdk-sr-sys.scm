(define-module (crates-io re dp redpanda-transform-sdk-sr-sys) #:use-module (crates-io))

(define-public crate-redpanda-transform-sdk-sr-sys-1.0.0 (c (n "redpanda-transform-sdk-sr-sys") (v "1.0.0") (d (list (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.0") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.0") (d #t) (k 0)))) (h "0gfiyjkmmizpilqnclw888zdmrmgbxw4kkwpk2dgdmrqlk52cwf2") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-sys-1.0.1 (c (n "redpanda-transform-sdk-sr-sys") (v "1.0.1") (d (list (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.1") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.1") (d #t) (k 0)))) (h "1bw1vpi4zfvxpmwc2ww68j0cqqik3d5w3rx8vwwy076cglkd7vdw") (r "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-sys-1.0.2 (c (n "redpanda-transform-sdk-sr-sys") (v "1.0.2") (d (list (d (n "redpanda-transform-sdk-sr-types") (r "^1.0.2") (d #t) (k 0)) (d (n "redpanda-transform-sdk-varint") (r "^1.0.2") (d #t) (k 0)))) (h "10g32phmqbvdhm7pi2w8dn275j587nb3w5fbi7xl03lzp4lsz2vv") (r "1.72.0")))

