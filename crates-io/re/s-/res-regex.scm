(define-module (crates-io re s- res-regex) #:use-module (crates-io))

(define-public crate-res-regex-0.1.0-beta.1 (c (n "res-regex") (v "0.1.0-beta.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "ress") (r "^0") (d #t) (k 0)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "0h245sjy51jc55yxi5g389118xfkfjwfd9qrgasw0fv7cz4ayyj3")))

(define-public crate-res-regex-0.1.0-beta.2 (c (n "res-regex") (v "0.1.0-beta.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "ress") (r "^0") (d #t) (k 0)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "1p1wm11z4aazlas6yiyz26j9l3ik8ki6z5prl0kycc7kfds1k9cn")))

(define-public crate-res-regex-0.1.0-beta.3 (c (n "res-regex") (v "0.1.0-beta.3") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "ress") (r "^0") (d #t) (k 0)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "18q6gziggapdfi6y2c9wdk6nnbf6x1k33sk5brga9jgqc95jlgmx")))

(define-public crate-res-regex-0.1.0-beta.4 (c (n "res-regex") (v "0.1.0-beta.4") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "19z66rr8qhh4rf72mvlbvplkqz867mvs580vap35mmf5mvci47in")))

(define-public crate-res-regex-0.1.0 (c (n "res-regex") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "139dmrlwklnb44cmgf36pym5zasa543mwnvyhmrv9vmlbx8n9kma")))

(define-public crate-res-regex-0.1.1 (c (n "res-regex") (v "0.1.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "1wjhmihwvpp9sf2r7ws87xhkyszmnrddrv1zvlk1xj75y21p85kv")))

(define-public crate-res-regex-0.1.2 (c (n "res-regex") (v "0.1.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "10dhmzd0iqha6xzrnjpyhc1z0ifk0jxa3ma9115lhdb8q7pkl747")))

(define-public crate-res-regex-0.1.3 (c (n "res-regex") (v "0.1.3") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "0453zh95v3r87ixyd2xx8g8prs7i2d01jj766ph7f5rhbx40vim4")))

(define-public crate-res-regex-0.1.4 (c (n "res-regex") (v "0.1.4") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "unic-ucd-ident") (r "^0.9.0") (f (quote ("id"))) (k 0)))) (h "1ca7ib8hn3qcqrlyqyvbh6zny584qpydia5g64zzkxi856v7z5vv")))

