(define-module (crates-io re n3 ren3) #:use-module (crates-io))

(define-public crate-ren3-0.1.0 (c (n "ren3") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1sbhh703q1m2yw3cinprj8ipv8vyhbfa38r84a8clnzvc0qvjm0c")))

