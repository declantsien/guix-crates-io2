(define-module (crates-io re bl reblessive) #:use-module (crates-io))

(define-public crate-reblessive-0.1.0 (c (n "reblessive") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1frlj8fxxaz12q8hj5blfsbysv48g6h10w7swwm51msv715j2kj4") (r "1.65")))

(define-public crate-reblessive-0.1.1 (c (n "reblessive") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "107c2yv4j6sxnv930yc32b9l63fcf398iyx4lxcfwf8rikmnrnqi") (r "1.66")))

(define-public crate-reblessive-0.1.2 (c (n "reblessive") (v "0.1.2") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1310fkga8crxf5339nqzj4vb6ybfkwdwx63swg0fn5bw4nzm8d6n") (r "1.66")))

(define-public crate-reblessive-0.2.0 (c (n "reblessive") (v "0.2.0") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0h8ynfrsck7kq0mp896xy10mnlvkiii1blfql4kr85xprpgmj6bm") (r "1.77")))

(define-public crate-reblessive-0.3.0 (c (n "reblessive") (v "0.3.0") (d (list (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "01s742wxivwqf4i60dbcpmijgzv0xbjmc7kcdk9g4wm0hp3p7a4g") (s 2) (e (quote (("tree" "dep:futures-util")))) (r "1.77")))

(define-public crate-reblessive-0.3.1 (c (n "reblessive") (v "0.3.1") (d (list (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1lgh85vzxxkihwxz2hfdgqvaa82haqvdbyp49a05qbkbvd04sz3l") (s 2) (e (quote (("tree" "dep:futures-util")))) (r "1.77")))

(define-public crate-reblessive-0.3.2 (c (n "reblessive") (v "0.3.2") (d (list (d (n "atomic-waker") (r "^1.1.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0idscli03zfqcp3zpbv9h3z9rnxwzga88si7w14w7fngnwnq727b") (s 2) (e (quote (("tree" "dep:futures-util")))) (r "1.77")))

(define-public crate-reblessive-0.3.3 (c (n "reblessive") (v "0.3.3") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "19w3idcvx9qlxg250ziw2p36i2a0wak48qn8yz7rsl5hy2yqzdpz") (f (quote (("tree")))) (r "1.77")))

(define-public crate-reblessive-0.3.4 (c (n "reblessive") (v "0.3.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0nwsnq9908abi4f3mrcz5r2sppqyhnqjl8avs51dqkd59mz9sa63") (f (quote (("tree")))) (y #t) (r "1.77")))

(define-public crate-reblessive-0.3.5 (c (n "reblessive") (v "0.3.5") (d (list (d (n "ciborium") (r "^0.2.2") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0amgmks2mpzb3g2ip1nmx8fm4h95ak6hgyd2r9n0y7njbgddwja1") (f (quote (("tree")))) (r "1.77")))

