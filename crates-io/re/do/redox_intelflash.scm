(define-module (crates-io re do redox_intelflash) #:use-module (crates-io))

(define-public crate-redox_intelflash-0.1.0 (c (n "redox_intelflash") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)))) (h "1n6vl5gi6acj6v51yhdmz4swflkgfw5rx1h5j1c90007iifz9rj9")))

(define-public crate-redox_intelflash-0.1.1 (c (n "redox_intelflash") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)))) (h "17j6262mdl2fn7fay9vxmim3s3660bxrh3lxi1dz55aagan8vqzw")))

(define-public crate-redox_intelflash-0.1.2 (c (n "redox_intelflash") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)))) (h "07v4n0zg4ni1jbzbc6qr2bs8dkrsh5wascx10sgdhij1jqzc1jbv")))

(define-public crate-redox_intelflash-0.1.3 (c (n "redox_intelflash") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)))) (h "1xa337n5qwk8bz9xgzfxpd2lmkhzz2nwcjfajjhjzn28fp8m33kx")))

