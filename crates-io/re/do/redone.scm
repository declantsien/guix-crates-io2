(define-module (crates-io re do redone) #:use-module (crates-io))

(define-public crate-redone-0.1.0 (c (n "redone") (v "0.1.0") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0a76bg8zz3sm19awyccyjq478j0h2lfy66yzby1pbafn3bblpr3c")))

