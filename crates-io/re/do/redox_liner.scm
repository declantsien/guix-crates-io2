(define-module (crates-io re do redox_liner) #:use-module (crates-io))

(define-public crate-redox_liner-0.5.1 (c (n "redox_liner") (v "0.5.1") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "09qk255j8vnhhhmj489lwgcxsh5q04psnxv3v2yla7sr2jqgc57h")))

(define-public crate-redox_liner-0.5.2 (c (n "redox_liner") (v "0.5.2") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "073713s7vjafzz9jj33166f1sqdrlpv6216nvb4hh5fyjrcxrhv3")))

