(define-module (crates-io re do redox) #:use-module (crates-io))

(define-public crate-redox-0.0.0 (c (n "redox") (v "0.0.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sha1") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "14knh671flsqm33vq4lyjsf2yjx0bslc6h2gva4432qqnx4dc5yy")))

(define-public crate-redox-0.0.1 (c (n "redox") (v "0.0.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sha1") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0n9zbxy3g5yn7p2av1dfdgg2rr3a0w6jcnf7iivkk3y3mavr5vaf")))

(define-public crate-redox-0.0.2 (c (n "redox") (v "0.0.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sha1") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1pgjw3fbmcpp7wzszjjx4ifvmnpxh408zssd2qb9kfisij8qgxij")))

(define-public crate-redox-0.0.3 (c (n "redox") (v "0.0.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "sha1") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0wyph0dlcxi7c1x6pvm8g99hky2pisapwbaliazfblwvwhs0l104") (f (quote (("unstable"))))))

