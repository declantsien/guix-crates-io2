(define-module (crates-io re do redox_termios) #:use-module (crates-io))

(define-public crate-redox_termios-0.1.0 (c (n "redox_termios") (v "0.1.0") (d (list (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "19szlcrp4kgkz2646vn7fah7jz8var9cxykchla0ycwdvqq5jjdw")))

(define-public crate-redox_termios-0.1.1 (c (n "redox_termios") (v "0.1.1") (d (list (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "0xhgvdh62mymgdl3jqrngl8hr4i8xwpnbsxnldq0l47993z1r2by")))

(define-public crate-redox_termios-0.1.2 (c (n "redox_termios") (v "0.1.2") (d (list (d (n "redox_syscall") (r "^0.2") (d #t) (k 0)))) (h "13zyscvmv0ib7ydw0prv9f0qvvlayfk03gdl4mqjfggxnjndhh44")))

(define-public crate-redox_termios-0.1.3 (c (n "redox_termios") (v "0.1.3") (h "1jzifsj7fqyksz4325l3azfzpyv027kjabf93zcmass3p9q5c510")))

