(define-module (crates-io re do redocs) #:use-module (crates-io))

(define-public crate-redocs-0.1.0 (c (n "redocs") (v "0.1.0") (d (list (d (n "build_html") (r "^2.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)))) (h "0s05zh0klw7885is790gqbzrqm4725hsxm837h7gkkvadrnx0agw")))

