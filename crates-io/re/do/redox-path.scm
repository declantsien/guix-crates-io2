(define-module (crates-io re do redox-path) #:use-module (crates-io))

(define-public crate-redox-path-0.1.0 (c (n "redox-path") (v "0.1.0") (h "01gpm2rdchmp848qhcbq67qiigi3p0lvz47i0bjzf20za4ahr2ws")))

(define-public crate-redox-path-0.1.1 (c (n "redox-path") (v "0.1.1") (h "1ilr0sgplpsrlrgbl1pyly5ajbw7x5m7c09cahbylrqlzrsp4p7l")))

(define-public crate-redox-path-0.2.0 (c (n "redox-path") (v "0.2.0") (h "19xy5d1n8pvmrq0iqzwyjgs59f0i31n5shnmbzzxwhh929jjc1v4")))

(define-public crate-redox-path-0.3.0 (c (n "redox-path") (v "0.3.0") (h "06cfac3yi95j54mr7378h8145brjaardfymxa6imxfhnr7pg85fm")))

(define-public crate-redox-path-0.3.1 (c (n "redox-path") (v "0.3.1") (h "05yppz2f9b8n4avm9mjhalyllfmy4lmyc2577pa5kcd5nv14ava3")))

