(define-module (crates-io re do redox_users) #:use-module (crates-io))

(define-public crate-redox_users-0.1.0 (c (n "redox_users") (v "0.1.0") (d (list (d (n "argon2rs") (r "^0.2") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "1i1j14v6200m1wy2gy69532yc1z2hvhywyvq8w8dh812yn4pcfcg")))

(define-public crate-redox_users-0.2.0 (c (n "redox_users") (v "0.2.0") (d (list (d (n "argon2rs") (r "^0.2") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "09ig904ck8fhd8xjfnz97p2jqzbm5k5x13andz4d4kz6kgj9fji1")))

(define-public crate-redox_users-0.3.0 (c (n "redox_users") (v "0.3.0") (d (list (d (n "argon2rs") (r "^0.2") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "0a1q5jv76vj1mwmqf2mmhknmkpw5wndx91gjfgg7vs8p79621r9z")))

(define-public crate-redox_users-0.3.1 (c (n "redox_users") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.5") (d #t) (k 0)))) (h "0vdn688q9wg997b1x5abx2gf7406rn1lvd62ypcgh1gj7g5dpkjf")))

(define-public crate-redox_users-0.3.2 (c (n "redox_users") (v "0.3.2") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.5") (d #t) (k 0)))) (h "0mxmzc1kp6ii1niqb91cw432pahn1icany7kmwz01lj9dd201ir4")))

(define-public crate-redox_users-0.3.3 (c (n "redox_users") (v "0.3.3") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.6") (d #t) (k 0)))) (h "0xc524xn2561ks586llpvv1klhqmaqd6h21cdq3cqr6ppiy8ih8x")))

(define-public crate-redox_users-0.3.4 (c (n "redox_users") (v "0.3.4") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.7") (d #t) (k 0)))) (h "0cbl5w16l3bqm22i4vszclf6hzpljxicghmllw7j13az4s9k1ch9")))

(define-public crate-redox_users-0.3.5 (c (n "redox_users") (v "0.3.5") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)))) (h "179fxmyqaqzibp533ajgbn4ljah9lrzpqvd3i73h55bs7qrkf1yy") (f (quote (("default" "auth") ("auth" "rust-argon2"))))))

(define-public crate-redox_users-0.4.0 (c (n "redox_users") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0r5y1a26flkn6gkayi558jg5dzh2m2fdsapgkpn7mj01v3rk51aj") (f (quote (("default" "auth") ("auth" "rust-argon2"))))))

(define-public crate-redox_users-0.4.1 (c (n "redox_users") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2.11") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0jnfr5pkdwa3w63b2z7yk92vm5hma4scjlpal8h2nwa4xdxbgnvi") (f (quote (("default" "auth") ("auth" "rust-argon2" "zeroize"))))))

(define-public crate-redox_users-0.4.2 (c (n "redox_users") (v "0.4.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2.11") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0mgyn1pyipqks0bm2c140jh9cwpl28rfhw01dd6amwcn4qz24xkp") (f (quote (("default" "auth") ("auth" "rust-argon2" "zeroize"))))))

(define-public crate-redox_users-0.4.3 (c (n "redox_users") (v "0.4.3") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2.11") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0asw3s4iy69knafkhvlbchy230qawc297vddjdwjs5nglwvxhcxh") (f (quote (("default" "auth") ("auth" "rust-argon2" "zeroize"))))))

(define-public crate-redox_users-0.4.4 (c (n "redox_users") (v "0.4.4") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "libredox") (r "^0.0.1") (f (quote ("call"))) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "1d1c7dhbb62sh8jrq9dhvqcyxqsh3wg8qknsi94iwq3r0wh7k151") (f (quote (("default" "auth") ("auth" "rust-argon2" "zeroize"))))))

(define-public crate-redox_users-0.4.5 (c (n "redox_users") (v "0.4.5") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "libredox") (r "^0.1.3") (f (quote ("std" "call"))) (k 0)) (d (n "rust-argon2") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "1498qyfyc2k3ih5aaffddvbhzi36na8iqg54hcm4pnpfa6b3sa5x") (f (quote (("default" "auth") ("auth" "rust-argon2" "zeroize"))))))

