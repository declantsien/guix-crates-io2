(define-module (crates-io re do redo) #:use-module (crates-io))

(define-public crate-redo-0.1.0 (c (n "redo") (v "0.1.0") (h "13cgjrmk1fdy8an2dwwc7cpgabir1isfk08kcl5ppg1i9kw0q17q")))

(define-public crate-redo-0.2.0 (c (n "redo") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0y7zs1khgk071klkwvf6zpiyzs634ph5b5pbipdnsakl4a3pn98c")))

(define-public crate-redo-0.2.1 (c (n "redo") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0zq7gi1n808wpd1ybflb7pnwzmhyd789pcrrinfsk0xy2p0ns23w") (f (quote (("no_state"))))))

(define-public crate-redo-0.3.0 (c (n "redo") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0zs30hapn1imi60kbiycjk26jqhwnv8jjm1xa2l9zisskslaypql")))

(define-public crate-redo-0.4.0 (c (n "redo") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1rrv9lk97a8p5pps1dyr8qwkjyg9pnj64gz87pj95lvr08qinznb")))

(define-public crate-redo-0.5.0 (c (n "redo") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1cdn5ql93zkbazscxa61s31qp3n44zfh6sq7mj466j2yy2x0whx8")))

(define-public crate-redo-0.6.0 (c (n "redo") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "05sfa0qd35i824pjixa1a4xryv742yil3fkwlrw5bvaj8qhf402w")))

(define-public crate-redo-0.7.0 (c (n "redo") (v "0.7.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1awwl9riz3vlyiam9s3g8qvr3pl0105hfk0rlnwz38f82lrgbbah")))

(define-public crate-redo-0.7.1 (c (n "redo") (v "0.7.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "08smpykc9p24nsxfid4jbbzjbfmqqfhrd0ancnkxpmidp49aaal2")))

(define-public crate-redo-0.7.2 (c (n "redo") (v "0.7.2") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1y9sc6c83mx91r3gbwph695bda6p7ii3716vx381gzbdwbkr6hz5")))

(define-public crate-redo-0.7.3 (c (n "redo") (v "0.7.3") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "08qsj2fn6b2rbidm2dijidps5h31slzm8v4d6vvarkbk2f4hm6y9")))

(define-public crate-redo-0.7.4 (c (n "redo") (v "0.7.4") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0kgkgsxdgczbgrxmzm2fsl5g8b5fqj8n5f71ad32rs3xwmhaifmq")))

(define-public crate-redo-0.7.5 (c (n "redo") (v "0.7.5") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0wlxrjizxzl4yb40hig695yw5j7pjnwkr7100f8zr0bjk3hlq8hb")))

(define-public crate-redo-0.8.0 (c (n "redo") (v "0.8.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "12l13487y200phxw6gr67z18wry3riqmy9cl7bi198w921zyw8p9")))

(define-public crate-redo-0.8.1 (c (n "redo") (v "0.8.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "18ziaib2jnk75pdsx0jxpfhddbba6n7gsfmr08ww9594dkwisdm7")))

(define-public crate-redo-0.9.0 (c (n "redo") (v "0.9.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1fwk16kw3v749pqwshrx15iisxx9lvhx7khibv5b6czcizc61rwz")))

(define-public crate-redo-0.9.1 (c (n "redo") (v "0.9.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "12snffp1fkm585vikvixkr93vlh534zrgm0dzgixnxhc6ny0fkdb")))

(define-public crate-redo-0.10.0 (c (n "redo") (v "0.10.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0634mrwlyfwlz527khfp94ryhg43xkldcjlpnhj2kp0pg2j89lc5")))

(define-public crate-redo-0.14.0 (c (n "redo") (v "0.14.0") (h "1119dz5slnl98zyajqlzjgf57nf7g5q1a10fjxz4whjlzwallagw")))

(define-public crate-redo-0.15.0 (c (n "redo") (v "0.15.0") (h "0blrxxk8xrni45s7n62qzkh1rim0rdxficgzgmxzzn8d9fkk4i8w")))

(define-public crate-redo-0.15.1 (c (n "redo") (v "0.15.1") (h "16h1a3dbrz036f06sx89x8afmvgj7nd48x8d5vysr1yk9jd1v4gn")))

(define-public crate-redo-0.15.2 (c (n "redo") (v "0.15.2") (h "0q6nflwh0jbfd1v53milqchmda3raq8g2bgsibxvim8hbs7qb9g9")))

(define-public crate-redo-0.16.0 (c (n "redo") (v "0.16.0") (h "1z3v0sqaf2jrlm2f8kbc4v7kja3a174f9c08jjbd8rw6gx62vhg3")))

(define-public crate-redo-0.17.0 (c (n "redo") (v "0.17.0") (h "0kkswvfky6jcgrflb4ihz3j19v2jcp15qgqnhj8kwv68dz782qn5")))

(define-public crate-redo-0.17.1 (c (n "redo") (v "0.17.1") (h "1c4lnvswzqk31bs8vd6snl0ckkx9hlqdvvrzjclnb2yinrlnxjrd")))

(define-public crate-redo-0.17.2 (c (n "redo") (v "0.17.2") (h "11jm8rdamagsrl4146sq0sjm8rfcw15xy637cdcf5hi02s4s0ji8")))

(define-public crate-redo-0.18.0 (c (n "redo") (v "0.18.0") (h "0rjkl1d2x0xh3bsdqh3zr2k43qfxsvr6siri75fyz7sracalynbk")))

(define-public crate-redo-0.19.0 (c (n "redo") (v "0.19.0") (h "11hbff6wb0063as5n6bacdml3vl8q93y9h4hkaljc447kfdbcblv")))

(define-public crate-redo-0.19.1 (c (n "redo") (v "0.19.1") (h "1q47pw7zkrrpwhxl4psn4zs8l3a8gy1wdlcizfq7mmhzhyd4g7ng")))

(define-public crate-redo-0.20.0 (c (n "redo") (v "0.20.0") (h "1w4w5jmy4jx6c53fvvrzcxq3266iq55bxaamjv5vw8icjil7jyr0")))

(define-public crate-redo-0.21.0 (c (n "redo") (v "0.21.0") (h "0clfvyxnskgqhpbdms9q8rq7bx3jj4wlrvr5xsycymd4rcya68mg")))

(define-public crate-redo-0.22.0 (c (n "redo") (v "0.22.0") (h "1i43218akjcpyvbp2gx8d9gk1046hgqijpcjvqwz2z9njaln8yyc")))

(define-public crate-redo-0.23.0 (c (n "redo") (v "0.23.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "17vlx916ss5gxh08c2vfmkbbqv1shghzs4wx47nhjq7sc52ghmik")))

(define-public crate-redo-0.24.0 (c (n "redo") (v "0.24.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0nzfd488qycqg8d82yvdasdnik42mvfbjyw036kh2gi58wl4zkqh")))

(define-public crate-redo-0.24.1 (c (n "redo") (v "0.24.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "12a2xwlkvmdx5l62ajq0jf5fzkagvn5wbqiwjg0d0qz64ijl6m0g")))

(define-public crate-redo-0.24.2 (c (n "redo") (v "0.24.2") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qm5qkfw318zj43cyln5rf1ba7bv72vfqhwa2f0jiwkbkh70jqf9")))

(define-public crate-redo-0.24.3 (c (n "redo") (v "0.24.3") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kdljwyabs71j98fhwv6ac5iwrxnfdi9gn711jk0qw4pk3rm1h0g")))

(define-public crate-redo-0.25.0 (c (n "redo") (v "0.25.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nx4j7sf19plp1wsgpv46669ahg79pgrnssbx344aaz4vr71l16x")))

(define-public crate-redo-0.26.0 (c (n "redo") (v "0.26.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yksl7rvwmp71iyjjv7kcgdglx6p41l1jlcpxc6mmn5bd6agnfrs")))

(define-public crate-redo-0.27.0 (c (n "redo") (v "0.27.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hhprw7zaxrijzrgp05dmcviddc8xf8ra6339a5iz237cqifz00l") (f (quote (("default" "chrono"))))))

(define-public crate-redo-0.27.1 (c (n "redo") (v "0.27.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q7kjr5z90m61d0gfx51z86a95cp257magrnkfxsrkr2izs6hf95") (f (quote (("default" "chrono"))))))

(define-public crate-redo-0.28.0 (c (n "redo") (v "0.28.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19ks92h50fqx1afyl6352rdmp497hb2zjppsrdz8bzx05cdpfshz")))

(define-public crate-redo-0.28.1 (c (n "redo") (v "0.28.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gz36dfdfzvpkv1i2b0hdf8jcbj6xp3vgy97vw5wcp5qc6kc4svn")))

(define-public crate-redo-0.28.2 (c (n "redo") (v "0.28.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ds29dw60835248f5vh1zp712a1nvx7cf0vjylwarvrwm1jvbi47")))

(define-public crate-redo-0.28.3 (c (n "redo") (v "0.28.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b913niycmfi1pi2ir75lpalz9acvbaxnpwfjdlcxk6sh1my4hbs")))

(define-public crate-redo-0.29.0 (c (n "redo") (v "0.29.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m4l8xpgs5b5h3k9i13h3n17w3sx38qasqm27x34p5hfqsl72vk8")))

(define-public crate-redo-0.29.1 (c (n "redo") (v "0.29.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11nyxqf97gx5h46gsd68lfrxrffryxy0blnbzlzh78dh1fqxlhzm")))

(define-public crate-redo-0.29.2 (c (n "redo") (v "0.29.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g0ghzsr7dngq9jav4yllvy2slk7x360p82cynfarschyml50m59")))

(define-public crate-redo-0.29.3 (c (n "redo") (v "0.29.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1frzb2r9a812ww54q79rqk7zjfl1cy8f1hiykhnivvmhf47p1yvn")))

(define-public crate-redo-0.29.4 (c (n "redo") (v "0.29.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b2n5ngwh56z7g3w0yjzwm4ajryar0zf8a7s5706chb1v8g14qng")))

(define-public crate-redo-0.29.5 (c (n "redo") (v "0.29.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16qn25qywdz373ap24jm34zd5lyswlciss95b38ggqp6y9dvn898")))

(define-public crate-redo-0.29.6 (c (n "redo") (v "0.29.6") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nkhrmiilxkng4aiqhg0d0qzj8699k6ydb49q6m31v9jlnzlrcjn")))

(define-public crate-redo-0.29.7 (c (n "redo") (v "0.29.7") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dwqymwwbf4jqzrr2221mjz7rxr1lv9pj5n1m10z3hmv5gis6vi4")))

(define-public crate-redo-0.29.8 (c (n "redo") (v "0.29.8") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vfxqiys9p6i7s92ah32am72yg2qplqxsycjpnxq51h05wyc4ll7")))

(define-public crate-redo-0.30.0 (c (n "redo") (v "0.30.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1aim615hx4idjz457w6i9vap4d7wwadk1qphlvm3s1avlzgrrhri")))

(define-public crate-redo-0.30.1 (c (n "redo") (v "0.30.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0aljmk9sf34py3cwlynr46ziqvafnqr0shs3pzrz7372wvnnc1sr")))

(define-public crate-redo-0.31.0 (c (n "redo") (v "0.31.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zcal2p43ak0j8vrbcwxsvcb6xxvc2nz84lkmrrav9smran5h1i9") (f (quote (("std") ("default" "std"))))))

(define-public crate-redo-0.31.1 (c (n "redo") (v "0.31.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kpvhski7c5ww8hcpvqwk991kb8wnabf3ipmy3r23zbc9pvdd52j") (f (quote (("std") ("default" "std"))))))

(define-public crate-redo-0.31.2 (c (n "redo") (v "0.31.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0snl6yb8flhv8af9scagl58pdp40ya4mazz4xnsj0w042qswd5i9") (f (quote (("std") ("default" "std"))))))

(define-public crate-redo-0.32.0 (c (n "redo") (v "0.32.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d3d13h3vdr91k6bynj0qj43181fjshj7n0cwxnh6s5l4pgvbwcn") (f (quote (("default"))))))

(define-public crate-redo-0.32.1 (c (n "redo") (v "0.32.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09x229sla3jz2jsn1gn61asb50v7428qchm518yv4kbm01jnn3jz") (f (quote (("default"))))))

(define-public crate-redo-0.33.0 (c (n "redo") (v "0.33.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13lz5kc7v8i9j1xhyglrp1zq372km7zi9184r991pfl5ihy7rqbs")))

(define-public crate-redo-0.34.0 (c (n "redo") (v "0.34.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "083b35wz7s52f9j27fncg0zca33faq6b1kndzqy0pnscw0lzh10q")))

(define-public crate-redo-0.35.0 (c (n "redo") (v "0.35.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gqnljr1mq90vrhjpm11kf0nzfrbm5ipmvq9c2mwjgwzcqz8cgvn")))

(define-public crate-redo-0.36.0 (c (n "redo") (v "0.36.0") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ci7m5s5v5828yb4lzrb2wqgs9iakihnjazz6k1n7j3543kijw9k")))

(define-public crate-redo-0.37.0 (c (n "redo") (v "0.37.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qmvr99q750z06rg9nv9723rs54sl1b5by7a75shi62v1dqk6hb1") (f (quote (("display" "colored") ("default"))))))

(define-public crate-redo-0.37.1 (c (n "redo") (v "0.37.1") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "090p1qlm2jqkkkv39wnpd149pvj4hfs735c1mx3q4gjxpmwha60g") (f (quote (("display" "colored") ("default"))))))

(define-public crate-redo-0.38.0 (c (n "redo") (v "0.38.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vh7sh2yllhkbbds076z4h0l1n5691dsghyzwak6bizqlys48cp9") (f (quote (("display" "colored") ("default"))))))

(define-public crate-redo-0.39.0 (c (n "redo") (v "0.39.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "111isvpqdy9a9vxx55nr3k3nzc52pk2j1pjgdkdm8i4bnqsx8zmz") (f (quote (("display" "colored") ("default"))))))

(define-public crate-redo-0.40.0 (c (n "redo") (v "0.40.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1f7ckfapk80mw1igbizr3s8dm944g1hav1k82yxas5sxixddxpm9")))

(define-public crate-redo-0.41.0 (c (n "redo") (v "0.41.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "030zgchk7zsmj262b6hr6m5dpcvr5ggs43b0s20g3iaf9aiiq0gr")))

(define-public crate-redo-0.41.1 (c (n "redo") (v "0.41.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "08xh6jlr4ccx0qcb7nw7dci3ai1q661fkyvjhfvkfilx7c766fk8")))

