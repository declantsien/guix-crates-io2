(define-module (crates-io re do redox_uefi_alloc) #:use-module (crates-io))

(define-public crate-redox_uefi_alloc-0.1.0 (c (n "redox_uefi_alloc") (v "0.1.0") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)))) (h "0r75519x14mrczxrdi0xcxk3az46yhjqiqrkbaiw5z8645725rvj")))

(define-public crate-redox_uefi_alloc-0.1.1 (c (n "redox_uefi_alloc") (v "0.1.1") (d (list (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)))) (h "143apz17agwlnngv4gdnykyvp5h1kx8p9w6l0qjq6afbmw8p0rvp") (y #t)))

(define-public crate-redox_uefi_alloc-0.1.2 (c (n "redox_uefi_alloc") (v "0.1.2") (d (list (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)))) (h "0z1wfw2dapgjgvq8y489gcq8glhlrwflqwsf5nxd1dh8hyjk2ksj")))

(define-public crate-redox_uefi_alloc-0.1.3 (c (n "redox_uefi_alloc") (v "0.1.3") (d (list (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)))) (h "1darnbwd19bn5dz0x4janyrk786d6fv540srrgdahw23gbs3sgk5")))

(define-public crate-redox_uefi_alloc-0.1.4 (c (n "redox_uefi_alloc") (v "0.1.4") (d (list (d (n "redox_uefi") (r "^0.1.5") (d #t) (k 0)))) (h "1qgd4x76mshfxnfl8ya0d5h8cv6bjkyhy5ilc9khg0hgmd5bxvh9")))

(define-public crate-redox_uefi_alloc-0.1.8 (c (n "redox_uefi_alloc") (v "0.1.8") (d (list (d (n "redox_uefi") (r "^0.1.8") (d #t) (k 0)))) (h "0xd74dsd0d8f4ar00bciaj7vqcggrjmn3bxpha5d6zkzbnrscklz")))

(define-public crate-redox_uefi_alloc-0.1.9 (c (n "redox_uefi_alloc") (v "0.1.9") (d (list (d (n "redox_uefi") (r "^0.1.8") (d #t) (k 0)))) (h "03q1n25dmdv5q50f3w8y78nhv6bm3imixcdbbf835i280kwycrqm")))

(define-public crate-redox_uefi_alloc-0.1.10 (c (n "redox_uefi_alloc") (v "0.1.10") (d (list (d (n "redox_uefi") (r "^0.1.10") (d #t) (k 0)))) (h "08jhnjlhzlbzzib0k369znd86nwqpqw79ryqpd86b8vda2y589ni")))

(define-public crate-redox_uefi_alloc-0.1.11 (c (n "redox_uefi_alloc") (v "0.1.11") (d (list (d (n "redox_uefi") (r "^0.1.11") (d #t) (k 0)))) (h "0if4nwqxx18yxsw32qaihvmkjcm0b8979fi6w8gh6446j5j03b74")))

(define-public crate-redox_uefi_alloc-0.1.12 (c (n "redox_uefi_alloc") (v "0.1.12") (d (list (d (n "redox_uefi") (r "^0.1.12") (d #t) (k 0)))) (h "1xninbj91y5yinf0z2d57y62daaj3basvj8hwmyprk0fzmbh7zhj")))

