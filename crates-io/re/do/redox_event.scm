(define-module (crates-io re do redox_event) #:use-module (crates-io))

(define-public crate-redox_event-0.1.0 (c (n "redox_event") (v "0.1.0") (d (list (d (n "redox_syscall") (r "^0.1") (d #t) (k 0)))) (h "0ajpm76088mrjjgin13krnnibcp7xq76926hbzb3lnpl706s9qcq")))

(define-public crate-redox_event-0.2.0 (c (n "redox_event") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libredox") (r "^0.0.1") (f (quote ("call"))) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "07b2c0x42d6lww2h5ghvmx0idyh9yzh0h4mc73pclwpagb0yv3xp")))

(define-public crate-redox_event-0.2.1 (c (n "redox_event") (v "0.2.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libredox") (r "^0.0.1") (f (quote ("call"))) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "04i73vnbxvsndf0snzj7qm53c8iyscqdaj12bm02aj6xg8njapj7")))

(define-public crate-redox_event-0.3.0 (c (n "redox_event") (v "0.3.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libredox") (r "^0.0.4") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "0wh25yjcd5cdhmi5vfa15yj2hx938gad9q3a6babib3s93295srm")))

(define-public crate-redox_event-0.4.0 (c (n "redox_event") (v "0.4.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libredox") (r "^0.1.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.5") (d #t) (k 0)))) (h "0jm0czh3xal7m4zrz0pina04853xzv7ikscvvyrfcv94ihl9ir0g")))

(define-public crate-redox_event-0.4.1 (c (n "redox_event") (v "0.4.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libredox") (r "^0.1.2") (k 0)) (d (n "redox_syscall") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0vwm1d02j94l9z6hqsjx820y56z3ndxr2ygk9rx294jrbnm9yq39") (f (quote (("wrappers") ("default" "wrappers"))))))

