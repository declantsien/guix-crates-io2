(define-module (crates-io re do redox-scheme) #:use-module (crates-io))

(define-public crate-redox-scheme-0.1.0 (c (n "redox-scheme") (v "0.1.0") (d (list (d (n "libredox") (r "^0.0.3") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4.1") (d #t) (k 0)))) (h "1a717a3kd6akc9d1wl8krb142w667141vrkii7cvgycp8yv8jlj2")))

(define-public crate-redox-scheme-0.2.0 (c (n "redox-scheme") (v "0.2.0") (d (list (d (n "libredox") (r "^0.1.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.5") (d #t) (k 0)))) (h "1h8497vygij4wrwf48pnvi1h5fmjafki8ahqkg6dg5bkd5hl2kaa")))

