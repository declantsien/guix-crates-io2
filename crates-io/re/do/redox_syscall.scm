(define-module (crates-io re do redox_syscall) #:use-module (crates-io))

(define-public crate-redox_syscall-0.1.0 (c (n "redox_syscall") (v "0.1.0") (h "1510hdc768qgchqyrjahiajn72brr6wxrl9b40bypnqhmcqq391m")))

(define-public crate-redox_syscall-0.1.1 (c (n "redox_syscall") (v "0.1.1") (h "0fkldscw9p41jw8za6i2njkgdrz7g724dn0pj5yy04dx0ypdkfmn")))

(define-public crate-redox_syscall-0.1.2 (c (n "redox_syscall") (v "0.1.2") (h "1wabiy1zn22s2p60hvycqjgfbbkbd4rgic5vjvk6kdij073sw9mp")))

(define-public crate-redox_syscall-0.1.3 (c (n "redox_syscall") (v "0.1.3") (h "034y7kczksd0jw61qqbqz2gvg6y4hi8b0hzfvfab38pf1lp2c3jp")))

(define-public crate-redox_syscall-0.1.4 (c (n "redox_syscall") (v "0.1.4") (h "0vx5vfl95w47m85m40zcg8g2h6nw11h14w2aw92vx0yvnmhiiawj")))

(define-public crate-redox_syscall-0.1.5 (c (n "redox_syscall") (v "0.1.5") (h "05k05bi5p3fs9fs8gyihwjzswaikh834f2bzhcnhwydmk0jdc5l0")))

(define-public crate-redox_syscall-0.1.6 (c (n "redox_syscall") (v "0.1.6") (h "0j6w09ryf9z18p94dnjk1sn7bdibpi4hfxh3zbz8x65m7kf2pp4b")))

(define-public crate-redox_syscall-0.1.7 (c (n "redox_syscall") (v "0.1.7") (h "07mynv1jk88q32bqbpx117jz1848hja2s91bv8gjsqp4a8xzdwir")))

(define-public crate-redox_syscall-0.1.8 (c (n "redox_syscall") (v "0.1.8") (h "12pbpf1ps83dphhy9xjdwv3i7wcrwi86avcbcwnpa4apvasrwi3g")))

(define-public crate-redox_syscall-0.1.9 (c (n "redox_syscall") (v "0.1.9") (h "0xfgfwlk892id8llyydq1rinqcsmcm3p8v83d09ang10hh039z5j")))

(define-public crate-redox_syscall-0.1.10 (c (n "redox_syscall") (v "0.1.10") (h "010ql7mazzxj889wqwc954cp118hvp3jfij2z2n47mg9zfam9690")))

(define-public crate-redox_syscall-0.1.11 (c (n "redox_syscall") (v "0.1.11") (h "0v3cbfxnj9jy5y5ralfbb1l7wmkgqv6rs94n6rl2mw17ichqwlpg")))

(define-public crate-redox_syscall-0.1.12 (c (n "redox_syscall") (v "0.1.12") (h "0ys65nnixrvdzn82wbyxac754w1dw2d0ixhc5mcw1jizfp7z2249")))

(define-public crate-redox_syscall-0.1.13 (c (n "redox_syscall") (v "0.1.13") (h "0zcvrqb5swngpdhjhdx2qcpv7l9i476iwrpyn5bpc8wnrkapr96h")))

(define-public crate-redox_syscall-0.1.14 (c (n "redox_syscall") (v "0.1.14") (h "05z0zd44ipxvsn8k7295sfp0f8w12sdhjy94hnbnhqimafaf7hqn")))

(define-public crate-redox_syscall-0.1.15 (c (n "redox_syscall") (v "0.1.15") (h "01h260lgjzwvrn3ciavkji0zivyad2lx3rbvqh92ls1fg612psz7")))

(define-public crate-redox_syscall-0.1.16 (c (n "redox_syscall") (v "0.1.16") (h "0lq7amb5bhjv0qaj6gp2ack6sp3bab0l6gbyfln5dv5xm34mrlwd")))

(define-public crate-redox_syscall-0.1.17 (c (n "redox_syscall") (v "0.1.17") (h "0nq7va6jia7vivgf8dgkfskz9jqkgdxhiij7xhfv72nzp7adznr9")))

(define-public crate-redox_syscall-0.1.18 (c (n "redox_syscall") (v "0.1.18") (h "0kcvk31p1dvs2qrxffr14d5l07vfa8zl6lf7r7927c8d02vawh9h")))

(define-public crate-redox_syscall-0.1.19 (c (n "redox_syscall") (v "0.1.19") (h "1s0qb064myc31xqvjj2r1dsh91a6wvq5swjqsqvhms8j9b8mg8z4")))

(define-public crate-redox_syscall-0.1.20 (c (n "redox_syscall") (v "0.1.20") (h "0j827572cvxnynkspplihds8qy790vlm2y433rl9574yp2bvgdhy")))

(define-public crate-redox_syscall-0.1.21 (c (n "redox_syscall") (v "0.1.21") (h "1bd1swqb88v76ggb14akfjay9v950f3yx2y0pb6wbvw3j76j0hcg")))

(define-public crate-redox_syscall-0.1.22 (c (n "redox_syscall") (v "0.1.22") (h "0gp3zdh9wxcl4kj0l7x73yvqpaml4jwdwv7yfjrxndddkjp2zhmk")))

(define-public crate-redox_syscall-0.1.23 (c (n "redox_syscall") (v "0.1.23") (h "0mhxvh0mhym7qxkx9apnsmjk9bgdy729xz55v2qf82zli3rcmm6c")))

(define-public crate-redox_syscall-0.1.24 (c (n "redox_syscall") (v "0.1.24") (h "1xdg4z1vq3wcbwfcvpn8mj745gj9czxyxpky65gxdkr8gmh9784s")))

(define-public crate-redox_syscall-0.1.25 (c (n "redox_syscall") (v "0.1.25") (h "03hn0hfn7g846l53mfwf94iivncz996mk2xj0riiwrnj58b892pq")))

(define-public crate-redox_syscall-0.1.26 (c (n "redox_syscall") (v "0.1.26") (h "0yhlcwlc04zz57b95rz9sz0q7qxqkwwb4dh784223gk73qdagxlx")))

(define-public crate-redox_syscall-0.1.27 (c (n "redox_syscall") (v "0.1.27") (h "1sb87w2i1m0sm58dhdsiak9saprfl47b7gf7pywjj9amviizdp40")))

(define-public crate-redox_syscall-0.1.28 (c (n "redox_syscall") (v "0.1.28") (h "0s0xdnkp175l59kvzq365zmjj8wrkchc32ppznfy9wvviv6pmayx")))

(define-public crate-redox_syscall-0.1.29 (c (n "redox_syscall") (v "0.1.29") (h "0975f0g48i57l8ywsd48zr2bb336wfc72fbynkzknc1m39ihk4rw")))

(define-public crate-redox_syscall-0.1.30 (c (n "redox_syscall") (v "0.1.30") (h "0yydb2v9495cs37r4xbsgxhxi55jz4sk2bxnny8g7754fskzn4l3")))

(define-public crate-redox_syscall-0.1.31 (c (n "redox_syscall") (v "0.1.31") (h "02a5n1zf89mj303dbwl4c2jrvr6w0j566sa4yaz8k0hhikqi3pld")))

(define-public crate-redox_syscall-0.1.32 (c (n "redox_syscall") (v "0.1.32") (h "187f61ha332m4jh17s0y8qhxj0jhlv470w2vsicxwkl8apv5s45b")))

(define-public crate-redox_syscall-0.1.33 (c (n "redox_syscall") (v "0.1.33") (h "16cck16slbhca83pjvjcmv4h204s81nmkplg66dmlk95wc8z1f07")))

(define-public crate-redox_syscall-0.1.34 (c (n "redox_syscall") (v "0.1.34") (h "0svh45q15fyz636ir3458vanmv6my9c16bzspc63pbq7i1c5q6nz")))

(define-public crate-redox_syscall-0.1.36 (c (n "redox_syscall") (v "0.1.36") (h "1amci4zif11d3qh574yagm2h96ysgy16rlj0sxx63ll9s1w6ss17")))

(define-public crate-redox_syscall-0.1.37 (c (n "redox_syscall") (v "0.1.37") (h "1z8pch9kyds4p3910j2spzdrdvi8kzw2kmg42mwpcayjpb7fx4hd")))

(define-public crate-redox_syscall-0.1.38 (c (n "redox_syscall") (v "0.1.38") (h "1hkk03xclifh067i035ngvyh9yjvd1vmiwapqzk01mszbcdda4ha")))

(define-public crate-redox_syscall-0.1.39 (c (n "redox_syscall") (v "0.1.39") (h "0gmp0h2cff2q71agg0p16rxdfkczjylxjzzggww13zqq1far713b")))

(define-public crate-redox_syscall-0.1.40 (c (n "redox_syscall") (v "0.1.40") (h "1c8j05dgfyhj5ms5is5yy9sb857bmmrqjmqywjjfjhyg7qfyj562")))

(define-public crate-redox_syscall-0.1.41 (c (n "redox_syscall") (v "0.1.41") (h "1z2jw35rrfas1vvccqabddccd9580qap9m2rn08150092s2xcsc9")))

(define-public crate-redox_syscall-0.1.42 (c (n "redox_syscall") (v "0.1.42") (h "14zy712a1dj7za8sgjgn2m1znpqzaidllmvcqbqji6qw9lmbi3yg")))

(define-public crate-redox_syscall-0.1.43 (c (n "redox_syscall") (v "0.1.43") (h "0pddspcj4vg6i8nds112zrrlpipx02l05gbzmyp90qwsir8ag7b7")))

(define-public crate-redox_syscall-0.1.44 (c (n "redox_syscall") (v "0.1.44") (h "0w7b3jxvydfa5zjzvagh6464j7ranyza099d1ac4b9c7gclwsjx8")))

(define-public crate-redox_syscall-0.1.45 (c (n "redox_syscall") (v "0.1.45") (h "18v4b7d8ia36wmy00fdwziljjm78pyqjdxwp7jqjiy8wl6bsqykv")))

(define-public crate-redox_syscall-0.1.46 (c (n "redox_syscall") (v "0.1.46") (h "1mhpbll85ff9ayifr79lnim2pbahaw82as6ah6ci17g7rn4kdn03")))

(define-public crate-redox_syscall-0.1.47 (c (n "redox_syscall") (v "0.1.47") (h "0q4qz9a4ckms7xvjcdx484q7vpnxcmqlirl6ikhm45ll1yig774q")))

(define-public crate-redox_syscall-0.1.48 (c (n "redox_syscall") (v "0.1.48") (h "0rp4q6iq5dnz7z053rp7izhm6rq5nh5mpmhhagy1z4mk8w9dwiz3")))

(define-public crate-redox_syscall-0.1.49 (c (n "redox_syscall") (v "0.1.49") (h "1bdgy0ahdklb25c2i13yjqj5lpkwd9znjivbpcga7w7kvjpm0b7j")))

(define-public crate-redox_syscall-0.1.50 (c (n "redox_syscall") (v "0.1.50") (h "1hkga2axgq4knmp7wdfkqgcb2f9c5nllynzlxxv1fc619m9rmvjj")))

(define-public crate-redox_syscall-0.1.51 (c (n "redox_syscall") (v "0.1.51") (h "11bbrjxgjr5z3p89fqvfrjrsybr8rnf0lyg9r43glgfazxpkfgj2")))

(define-public crate-redox_syscall-0.1.52 (c (n "redox_syscall") (v "0.1.52") (h "0s9x850ypvl4935nc3h0dgsjnlw9ffcyq7s1q15nxn6fwm9k0ayk")))

(define-public crate-redox_syscall-0.1.53 (c (n "redox_syscall") (v "0.1.53") (h "0a0r86rg5d4cr4rw26qkq27v6yapm8d4hj6vqgavjvpfnw8qb12k")))

(define-public crate-redox_syscall-0.1.54 (c (n "redox_syscall") (v "0.1.54") (h "0lp22pjvmj33a2fa7y1h9cgxmnfd8whbb8s6n0f4yp7nl0a9q8hj")))

(define-public crate-redox_syscall-0.1.55 (c (n "redox_syscall") (v "0.1.55") (h "0g183ppd21dsbwfxwx1pjlhrp27b1kq90i3qpwg1lr07lymr7l6b")))

(define-public crate-redox_syscall-0.1.56 (c (n "redox_syscall") (v "0.1.56") (h "110y7dyfm2vci4x5vk7gr0q551dvp31npl99fnsx2fb17wzwcf94")))

(define-public crate-redox_syscall-0.1.57 (c (n "redox_syscall") (v "0.1.57") (h "1kh59fpwy33w9nwd5iyc283yglq8pf2s41hnhvl48iax9mz0zk21")))

(define-public crate-redox_syscall-0.2.0 (c (n "redox_syscall") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0nv7gnbw9dn5556sz60w7wi32qyddya0cmp46373fkykk77c9ids")))

(define-public crate-redox_syscall-0.2.1 (c (n "redox_syscall") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0nhgrbahr6c6m54flmnh6am74h5xpsyf53b0vszy3dlf3qm2rf28")))

(define-public crate-redox_syscall-0.2.2 (c (n "redox_syscall") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "065z1q9sw9mzlqj5fv5jfcgcxhza3xkwq5n14naqnhlicw72mn7k") (y #t)))

(define-public crate-redox_syscall-0.2.3 (c (n "redox_syscall") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1xg72800fxc8j51aa8rbd5bv1ka1gkz1g4v4zg992nf8q1vm07kc")))

(define-public crate-redox_syscall-0.2.4 (c (n "redox_syscall") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0w254gyccyinrzhgd562ddrhgcpwswy700mmc9qa6pkc86lqrv05")))

(define-public crate-redox_syscall-0.2.5 (c (n "redox_syscall") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1n878lpw577avdr5dzbkil02xwbx0a57mr2r3dcnnkz28i71wd4l")))

(define-public crate-redox_syscall-0.2.6 (c (n "redox_syscall") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0hchadrvdfvsrxg7zxz808nj521bnzq552apgs753sycbi5k2w42")))

(define-public crate-redox_syscall-0.2.7 (c (n "redox_syscall") (v "0.2.7") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1why6kw3mbilpnca2xmxn6qh8byiydvizq8i75imqdgphvjr5pc5")))

(define-public crate-redox_syscall-0.2.8 (c (n "redox_syscall") (v "0.2.8") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1z27f6rzm0lcpszbr2dmfyna7qmpmx4yb98yhzg1956l3kj3j9vl")))

(define-public crate-redox_syscall-0.2.9 (c (n "redox_syscall") (v "0.2.9") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1vpyfnfrw5ki262720yks8m7vn1a2mg89s4rqi5ir7izvyx9md2s")))

(define-public crate-redox_syscall-0.2.10 (c (n "redox_syscall") (v "0.2.10") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1zq36bhw4c6xig340ja1jmr36iy0d3djp8smsabxx71676bg70w3")))

(define-public crate-redox_syscall-0.2.11 (c (n "redox_syscall") (v "0.2.11") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0g2gcdqp462827ywwk51jx7sgy706wbz86vvfkq484jma80zx043")))

(define-public crate-redox_syscall-0.2.12 (c (n "redox_syscall") (v "0.2.12") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1w7ccknxglmqpx49x3nl8vz4pc688ys1wgkrq7q4kh863gy87qca")))

(define-public crate-redox_syscall-0.2.13 (c (n "redox_syscall") (v "0.2.13") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0hpgwvgjlg1j9z7bjf5y18fkd8ag7y4znhqxg85hnpp5qz25pwk2")))

(define-public crate-redox_syscall-0.2.14 (c (n "redox_syscall") (v "0.2.14") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0d8jf516cb6qq28cfy4c5nw6bm8nq0vhz1ym1i25qsixygqghykl")))

(define-public crate-redox_syscall-0.2.15 (c (n "redox_syscall") (v "0.2.15") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "0nkxilbjdlwvvxj115adzv0r2iniadfn7x7v41qw3351srcgwk2k")))

(define-public crate-redox_syscall-0.2.16 (c (n "redox_syscall") (v "0.2.16") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "16jicm96kjyzm802cxdd1k9jmcph0db1a4lhslcnhjsvhp0mhnpv")))

(define-public crate-redox_syscall-0.3.0 (c (n "redox_syscall") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1vcgpxgaa1j5cl8nvsyaig5fa5q25c1887x7c1psm7v2nrlyk36a")))

(define-public crate-redox_syscall-0.3.1 (c (n "redox_syscall") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1fscqlbkzn11nndjhwsw0bam92sw4xz0baqrq0d38kvbb22h8d3q")))

(define-public crate-redox_syscall-0.3.2 (c (n "redox_syscall") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "03rni7cvfcxiy57fxqxlp5hffpdfybyh5xz0fmgdi4rmhajqz1ys")))

(define-public crate-redox_syscall-0.3.3 (c (n "redox_syscall") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "1x5zcgad8isiyksd55nd1b019v2s4fad8rxmdybsh7mja37icqsd")))

(define-public crate-redox_syscall-0.3.4 (c (n "redox_syscall") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "00xzpjc7rm9iiiwg67wnr7zvn2wynhb1w3w9hs6sviz8x2paj0pv")))

(define-public crate-redox_syscall-0.3.5 (c (n "redox_syscall") (v "0.3.5") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0acgiy2lc1m2vr8cr33l5s7k9wzby8dybyab1a9p753hcbr68xjn") (f (quote (("rustc-dep-of-std" "core" "bitflags/rustc-dep-of-std"))))))

(define-public crate-redox_syscall-0.4.0 (c (n "redox_syscall") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0k34zl5n2y5xfazdni8pn2n696i4xn671152xxxcbhqwskibrl6y") (f (quote (("rustc-dep-of-std" "core" "bitflags/rustc-dep-of-std"))))))

(define-public crate-redox_syscall-0.4.1 (c (n "redox_syscall") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1aiifyz5dnybfvkk4cdab9p2kmphag1yad6iknc7aszlxxldf8j7") (f (quote (("rustc-dep-of-std" "core" "bitflags/rustc-dep-of-std"))))))

(define-public crate-redox_syscall-0.5.0 (c (n "redox_syscall") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "15hf2s5hspyd5j4jvpn9qkdhpb6ia6cbs91i3wwsqzncabwpih8k") (f (quote (("rustc-dep-of-std" "core" "bitflags/rustc-dep-of-std"))))))

(define-public crate-redox_syscall-0.5.1 (c (n "redox_syscall") (v "0.5.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0zja6y3av9z50gg1hh0vsc053941wng21r43whhk8mfb9n4m5426") (f (quote (("rustc-dep-of-std" "core" "bitflags/rustc-dep-of-std"))))))

