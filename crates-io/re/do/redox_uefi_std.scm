(define-module (crates-io re do redox_uefi_std) #:use-module (crates-io))

(define-public crate-redox_uefi_std-0.1.0 (c (n "redox_uefi_std") (v "0.1.0") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "09fpl81mjdqg9gwq04bhh5nhpjgqds5c6whwnn1zx9xy1bq0rgnl")))

(define-public crate-redox_uefi_std-0.1.1 (c (n "redox_uefi_std") (v "0.1.1") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "0fw4wjd7zm38p78xagzpvix7i59zdsg68qblk7apwcaxhvnfbxg3")))

(define-public crate-redox_uefi_std-0.1.2 (c (n "redox_uefi_std") (v "0.1.2") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "0zvwn7lhrlxf9anb1534pk6jclmlclnh0r9hz6r4c1k5i8xc3mga")))

(define-public crate-redox_uefi_std-0.1.3 (c (n "redox_uefi_std") (v "0.1.3") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "1skdkvn6a1i6s4197m43rlgpih71cyj0h5jgby33qlpr24j6k4cj")))

(define-public crate-redox_uefi_std-0.1.4 (c (n "redox_uefi_std") (v "0.1.4") (d (list (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "0qzc4vcf8yq17dyf8pw6vfkfhvaymsmarhq8i9q3dcwfv6ii4rwn")))

(define-public crate-redox_uefi_std-0.1.5 (c (n "redox_uefi_std") (v "0.1.5") (d (list (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.1") (d #t) (k 0)))) (h "0b9s3nc28d1c898j6jyrcbmaxbqgamx6w6fx0ac7rfcv3mw2199j")))

(define-public crate-redox_uefi_std-0.1.6 (c (n "redox_uefi_std") (v "0.1.6") (d (list (d (n "redox_uefi") (r "^0.1.2") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.1") (d #t) (k 0)))) (h "1w2sfixdnq1nvfyxvnc14j6vw2cgfz9xpw5yxhyxc1nxy8qmg1nq")))

(define-public crate-redox_uefi_std-0.1.7 (c (n "redox_uefi_std") (v "0.1.7") (d (list (d (n "redox_uefi") (r "^0.1.5") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.4") (d #t) (k 0)))) (h "1bh1wkrdvnkc54h8xp6cfr6ihp7lir2ihq8v8830dp6l74rjbk2y")))

(define-public crate-redox_uefi_std-0.1.8 (c (n "redox_uefi_std") (v "0.1.8") (d (list (d (n "redox_uefi") (r "^0.1.8") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.8") (d #t) (k 0)))) (h "00265lkba9rhvbzyq3394ymkdfa7fzps036vwhq2zaivfcdapb81")))

(define-public crate-redox_uefi_std-0.1.9 (c (n "redox_uefi_std") (v "0.1.9") (d (list (d (n "redox_uefi") (r "^0.1.8") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.8") (d #t) (k 0)))) (h "1dmb9j0bjb7i2xms65z0gd35ikwdqkc160dwa6c42i8mjsy2j3c4")))

(define-public crate-redox_uefi_std-0.1.10 (c (n "redox_uefi_std") (v "0.1.10") (d (list (d (n "redox_uefi") (r "^0.1.10") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.10") (d #t) (k 0)))) (h "1g8f1h23vyfk3zzlwxvgr1rz64da4wxri9102mhy8ri37gfabpmr")))

(define-public crate-redox_uefi_std-0.1.11 (c (n "redox_uefi_std") (v "0.1.11") (d (list (d (n "redox_uefi") (r "^0.1.11") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.11") (d #t) (k 0)))) (h "0sj6dcfvqx6pgq2gq62abjf571h86v6mf5pqmgdix3czf2kpvq6q")))

(define-public crate-redox_uefi_std-0.1.12 (c (n "redox_uefi_std") (v "0.1.12") (d (list (d (n "redox_uefi") (r "^0.1.12") (d #t) (k 0)) (d (n "redox_uefi_alloc") (r "^0.1.12") (d #t) (k 0)))) (h "1hlyq5h24a51z8pq376s8qm34326fk7cc68qzwpiajjclv1dy69a")))

