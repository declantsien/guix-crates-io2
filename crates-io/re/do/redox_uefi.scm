(define-module (crates-io re do redox_uefi) #:use-module (crates-io))

(define-public crate-redox_uefi-0.1.0 (c (n "redox_uefi") (v "0.1.0") (h "05qjbv3i4icirqjd0bmvmb3wpa72j5nmlr17knhmri97bij51z6s")))

(define-public crate-redox_uefi-0.1.1 (c (n "redox_uefi") (v "0.1.1") (h "1jny5k9zv2a7qcnncibm7h5iy0r4nif4v5wl05pqgamcv8vypcbn")))

(define-public crate-redox_uefi-0.1.2 (c (n "redox_uefi") (v "0.1.2") (h "0dc9xdqrqpm1irq6j9k8pz98gpzv2lb1rci9h0blgfjs1hca6qww")))

(define-public crate-redox_uefi-0.1.3 (c (n "redox_uefi") (v "0.1.3") (h "1hzqsczmb2l1rdgkzlgxrmbx3rwdh66kf8ac0nv0chxc29gfmxcs")))

(define-public crate-redox_uefi-0.1.4 (c (n "redox_uefi") (v "0.1.4") (h "09g7n2a7zq3h6k3mi8i3zri9zmd71bk5l3ycwpp323izkdacxr1d")))

(define-public crate-redox_uefi-0.1.5 (c (n "redox_uefi") (v "0.1.5") (h "1nix5qfz4k7s21axi1q3gfryj6697y7yp9fi985daycyi613wj0c")))

(define-public crate-redox_uefi-0.1.8 (c (n "redox_uefi") (v "0.1.8") (h "091dv4s7k2b6zb3k2dwlaz8vrn31bplyy478hw4hcaz1abv7qccb")))

(define-public crate-redox_uefi-0.1.9 (c (n "redox_uefi") (v "0.1.9") (h "11njp3kjxilhj47psp1g50wskkmnggx7ir1n3zf3j5pjj75jzdj1")))

(define-public crate-redox_uefi-0.1.10 (c (n "redox_uefi") (v "0.1.10") (h "19b8xdzp2lw96gbbaw7gr10ng4zldah5jy5k5k99zxy94afychs1")))

(define-public crate-redox_uefi-0.1.11 (c (n "redox_uefi") (v "0.1.11") (h "1r94dz0id1dwcgcl9n6zlqq8ki2gv1bkksmnxx9nj2l59q9bknx3")))

(define-public crate-redox_uefi-0.1.12 (c (n "redox_uefi") (v "0.1.12") (h "0kc3sm5barj95159iyn429q5w5qg1lv7z170az1dynmymz9gh7md")))

