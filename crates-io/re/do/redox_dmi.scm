(define-module (crates-io re do redox_dmi) #:use-module (crates-io))

(define-public crate-redox_dmi-0.1.1 (c (n "redox_dmi") (v "0.1.1") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "1dvzm9rvn86hljlyl4wmwn09cs3bzdqmhpz6sgn4csbvrwdzc02g")))

(define-public crate-redox_dmi-0.1.2 (c (n "redox_dmi") (v "0.1.2") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "06xb6d932cmbfrf98x79sxjn1rc4a5mwfcpaggzlmg0jymhmnmrm")))

(define-public crate-redox_dmi-0.1.3 (c (n "redox_dmi") (v "0.1.3") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "1vl1rxjdd4r4b24h3kzc7ibmz7yz0kayf5ff36jnw53zks5wbgq8")))

(define-public crate-redox_dmi-0.1.4 (c (n "redox_dmi") (v "0.1.4") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "0r21awr9q8150xa6gsmwbj7w88vl2lm8sb5c4lpik7k34cdm2hpp") (f (quote (("std") ("default"))))))

(define-public crate-redox_dmi-0.1.5 (c (n "redox_dmi") (v "0.1.5") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "0px46ljhj4vkbw45krwyg1x0fv8gh2nbcvb45bxf1bz45qza3759") (f (quote (("std") ("default"))))))

(define-public crate-redox_dmi-0.1.6 (c (n "redox_dmi") (v "0.1.6") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "173qyz3n0j8f3k3b06v4gxbfvf4z6mni6zpxsd70vya1bnpihqi1") (f (quote (("std") ("default"))))))

