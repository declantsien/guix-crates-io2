(define-module (crates-io re do redox_hwio) #:use-module (crates-io))

(define-public crate-redox_hwio-0.1.0 (c (n "redox_hwio") (v "0.1.0") (h "0xs49p901ivajdzlg0jxn1y6l7h3c1y8yrimmprb2m2hzn2k0p4w")))

(define-public crate-redox_hwio-0.1.1 (c (n "redox_hwio") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0pdjhfc8hg30b0rxxxcijqw9jj657a3pvhyvfm5wnahmpr48nzc4") (f (quote (("stable" "lazy_static") ("default"))))))

(define-public crate-redox_hwio-0.1.2 (c (n "redox_hwio") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0sfhd6rjc20symnnxnhhw7blsvx5ayqlv7ngnad4ci19kdvc40cv") (f (quote (("stable" "lazy_static") ("default"))))))

(define-public crate-redox_hwio-0.1.3 (c (n "redox_hwio") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0w40dp775m1r0fqnzl3kzpnav9ca4cvd7jj4cq8096ijcx62raj1") (f (quote (("stable" "lazy_static") ("default"))))))

(define-public crate-redox_hwio-0.1.4 (c (n "redox_hwio") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "05ph3bwzyg4wfcan56nki46k5f0i775rfgn43c7n1qjnmxsq0frb") (f (quote (("stable" "lazy_static") ("default"))))))

(define-public crate-redox_hwio-0.1.5 (c (n "redox_hwio") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "13bp2rr8l928nwfxgv2y0kj3iy0fjvsmm7sydh6clc2yp7hxk2cl") (f (quote (("stable" "lazy_static") ("default"))))))

(define-public crate-redox_hwio-0.1.6 (c (n "redox_hwio") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0x6fgwyja25i47skd1cwccd91finy5fa798mbcmkg10s6jniddcf") (f (quote (("std" "lazy_static") ("stable") ("default" "std"))))))

