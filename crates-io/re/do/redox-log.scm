(define-module (crates-io re do redox-log) #:use-module (crates-io))

(define-public crate-redox-log-0.1.1 (c (n "redox-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0mqmi91ingmrpyqxsr19b4vg40vkg5y0rw8ixja0np1s00xd7xnb")))

(define-public crate-redox-log-0.1.2 (c (n "redox-log") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "1wjlb8k5f0mlwh6n5ypb39xzhhihrpp6f7lg9aki1rnqn9zcdmyv")))

