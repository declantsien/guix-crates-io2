(define-module (crates-io re ui reui) #:use-module (crates-io))

(define-public crate-reui-0.0.0 (c (n "reui") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)))) (h "1p5b531xz2f2xn9ly1v5af0sj6jkm01x6sm5cfqg8gnk7lvzr77a")))

(define-public crate-reui-0.0.1 (c (n "reui") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "144893bz6n6k2xd5klzwqayj0xc17gs7jih3frh4rxqmjgvzgh1a")))

(define-public crate-reui-0.0.2 (c (n "reui") (v "0.0.2") (d (list (d (n "bevy") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "0vlhh14h1lii3cbx9gy8l0j76ryzs5z1mwaxixi17rdxdf3c0srg")))

(define-public crate-reui-0.2.0 (c (n "reui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.16") (d #t) (k 0)))) (h "0mg4dkwyn6mpqw18b7qipqw83ghqrsgl7sx6i9n6lfmf1407kyqw")))

(define-public crate-reui-0.3.0 (c (n "reui") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)))) (h "1017gj8wgr5vw40lbi15m3x2wa99ckknmalfn1fifq42bwl21n9h")))

