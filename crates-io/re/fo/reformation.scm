(define-module (crates-io re fo reformation) #:use-module (crates-io))

(define-public crate-reformation-0.2.1 (c (n "reformation") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0rv9cpqpcv35whx2avvbcfdciyhwafxg0sp9nv0la8myx995jgs9")))

(define-public crate-reformation-0.2.2 (c (n "reformation") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "029qmzpl9mi0s4lhk3zzmpqhmp42vmmvscxplxgy73n2d43sa0qp")))

(define-public crate-reformation-0.2.3 (c (n "reformation") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1ciz6zyr367bp2izrbyx07y6c6qfkwnw9b5zcv1hlfzdncp3mz8n")))

(define-public crate-reformation-0.3.0 (c (n "reformation") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1fjbajxz0sxdz8dxc5n716f8qwmcf3j783ynfxn2zakahm7h67h9")))

(define-public crate-reformation-0.3.1 (c (n "reformation") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1i78ywcfh883hfjv30wxq1fdyhigz126zhndn7f3v087hgl7vdyy")))

(define-public crate-reformation-0.4.0 (c (n "reformation") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00v0bsbpf2fmvdhzl21bwjjhhid9w9cnvjnqwlqy2a47i0sflx2y")))

(define-public crate-reformation-0.4.1 (c (n "reformation") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00w4c1sdlzakan26zd40v3v7jcf6x3mva22vny0288w4ghilfwyg")))

(define-public crate-reformation-0.5.0 (c (n "reformation") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "once_cell") (r "^0.2") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1nrg6s977znc632vkqry8q93v6ha50mq1fkrgmvq2wajc6gx191f")))

(define-public crate-reformation-0.5.2 (c (n "reformation") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1wzazs2s2z4gw1zl6i047c7azia7hrbhqmqlaxqrc7pnd4m3ipr8") (f (quote (("containers"))))))

(define-public crate-reformation-0.5.3 (c (n "reformation") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "reformation_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "10p964935ls5bs3in5srqj9jhkd5bqnappqdjbjyfi6hnd0r4ybz") (f (quote (("containers"))))))

