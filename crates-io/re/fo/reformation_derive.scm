(define-module (crates-io re fo reformation_derive) #:use-module (crates-io))

(define-public crate-reformation_derive-0.2.1 (c (n "reformation_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pvrygarkyizwgdals434nvb41sgpms1dx450s8qav5fa291cm8j")))

(define-public crate-reformation_derive-0.2.2 (c (n "reformation_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1x8gcs018mk2jwyzfvb95a0incrnmy6ai1a0v779q8g0p2j8x64r")))

(define-public crate-reformation_derive-0.2.3 (c (n "reformation_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ma8drbgc6pvzyv14kk6y71hb8qx1hql2k9lkmgr6w14pdnxsqx7")))

(define-public crate-reformation_derive-0.3.0 (c (n "reformation_derive") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1fp7rhfzniva1jz6bbfx4fp68xw2hn9iqrcr6i0wkwx66n295jpd")))

(define-public crate-reformation_derive-0.3.1 (c (n "reformation_derive") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ig3fszbfzq39wxlshm8w8j3rffxfaz8hs1xkqi2h15sxk0pfbq6")))

(define-public crate-reformation_derive-0.4.0 (c (n "reformation_derive") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1r0adlsv6pq4mny2pb9xpls6zw4lggcfh0w0ibv89aklyrx58l65")))

(define-public crate-reformation_derive-0.4.1 (c (n "reformation_derive") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "14gd2bywmffhz08yyyrysn80cxjssb71qn9sh4g8l9ak2m7jcdbr")))

(define-public crate-reformation_derive-0.5.0 (c (n "reformation_derive") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1clwbj5bb3w8hy0ckcgxvmc9hhrmp2i455dm9ca47pda2p0sl4bk")))

(define-public crate-reformation_derive-0.5.2 (c (n "reformation_derive") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gzb33bpdc09qldlv33pjp4sxb4h5fdjkg1v13lxjzmk44jy7p05")))

