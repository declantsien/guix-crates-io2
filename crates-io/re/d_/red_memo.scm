(define-module (crates-io re d_ red_memo) #:use-module (crates-io))

(define-public crate-red_memo-0.1.0 (c (n "red_memo") (v "0.1.0") (h "1vir3f0l33dvgzwwysf2cpywscc5lrpnck6lc29h23sx4mpm1230")))

(define-public crate-red_memo-0.1.1 (c (n "red_memo") (v "0.1.1") (h "0xd36r6xh0g3m59c0hdvbkfcgvm7r4iq0sy687x403qp93hh5534")))

