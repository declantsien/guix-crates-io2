(define-module (crates-io re d_ red_spnego) #:use-module (crates-io))

(define-public crate-red_spnego-0.0.1 (c (n "red_spnego") (v "0.0.1") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04phc89l0mb22y9gz4j8zraxgrgdskw24hf18y24jbrz0aa8sabs")))

