(define-module (crates-io re d_ red_primality) #:use-module (crates-io))

(define-public crate-red_primality-0.1.0 (c (n "red_primality") (v "0.1.0") (d (list (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "0yi8plclcbwl0qmhz0j2dqs4p6dlykz5070fxz8y5h8zxb7a5blx")))

(define-public crate-red_primality-0.1.1 (c (n "red_primality") (v "0.1.1") (d (list (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "0rvvjq924bvxbdq5iqkypwbfwpysw7kmj9arla8hv4k6dpy8bpd5")))

(define-public crate-red_primality-0.1.2 (c (n "red_primality") (v "0.1.2") (d (list (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "025i2pbfwxa9v24xpzfbsv7x77npldvbjgcxzdg1ya843pghdiwx")))

(define-public crate-red_primality-0.1.3 (c (n "red_primality") (v "0.1.3") (d (list (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "0mr8n1dj63gndbviswk297p5qmsqndmlwaf4l73ld2mz1c9d428a")))

(define-public crate-red_primality-0.1.4 (c (n "red_primality") (v "0.1.4") (d (list (d (n "num-integer") (r "^0.1.41") (d #t) (k 2)) (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "0ahkqd84hnzp4ng8766mawld1zm4l10aal5cdw73hjc62vfn6zk9")))

(define-public crate-red_primality-0.1.5 (c (n "red_primality") (v "0.1.5") (d (list (d (n "num-integer") (r "^0.1.41") (d #t) (k 2)) (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "0510175lw100nv9d0pd2hh85m92qm62paas494y8dgjfx0018n14")))

(define-public crate-red_primality-0.1.6 (c (n "red_primality") (v "0.1.6") (d (list (d (n "num-integer") (r "^0.1.41") (d #t) (k 2)) (d (n "primal") (r "^0.2.3") (d #t) (k 2)))) (h "04k716hlpjcgqqxyyqngcdsc2gk1m8k27zhmv1xw6p7abzxpklcr")))

(define-public crate-red_primality-0.2.0 (c (n "red_primality") (v "0.2.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 2)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 2)))) (h "1m1bf838wyhclqfxs9z384h9yv4wqr1ijqzfaga8hlik2yjab25d")))

