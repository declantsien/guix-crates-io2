(define-module (crates-io re d_ red_union_find) #:use-module (crates-io))

(define-public crate-red_union_find-0.1.0 (c (n "red_union_find") (v "0.1.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0yqqbmq10bgkr6q2q4pwrac8nnjrnc92f3mlvpi781qiylgda6gw")))

(define-public crate-red_union_find-0.2.0 (c (n "red_union_find") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0xqmhil35dp3rhdhrgf25xi1n3gpgfcijy7v5mhkzvazxbb5d6pd")))

(define-public crate-red_union_find-0.2.1 (c (n "red_union_find") (v "0.2.1") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1h51pkpl6hlq58xrhq35izsc66bxb9bjiqsjpqk4il99dknmx7fx")))

(define-public crate-red_union_find-0.2.2 (c (n "red_union_find") (v "0.2.2") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0i1pz9dxwyincfr63yyfpz4f2rd0z1y78znv39cqbs8lkknfdfpv")))

(define-public crate-red_union_find-0.2.4 (c (n "red_union_find") (v "0.2.4") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1biwlk77sll1wzavzx1f2r6vs8cy44xlakwh5c5pqxf27yr68kj9")))

(define-public crate-red_union_find-0.2.5 (c (n "red_union_find") (v "0.2.5") (d (list (d (n "num-integer") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "num-traits") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "1hgk6chypjxdbq0bh1f5h09r325lfx4dcnfflhlirgnvw3wjhlhq")))

(define-public crate-red_union_find-0.3.0 (c (n "red_union_find") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0hz3pjy3iijf3fl2l5qsbsrfdhhffcpa3fppa9p6prdfdijd69h4")))

