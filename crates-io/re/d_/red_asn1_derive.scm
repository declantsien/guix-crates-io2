(define-module (crates-io re d_ red_asn1_derive) #:use-module (crates-io))

(define-public crate-red_asn1_derive-0.0.8 (c (n "red_asn1_derive") (v "0.0.8") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1srhfc1iby0wncdk49n42h16x7masnw36qm4a7ywnc5sa1fajxwd")))

(define-public crate-red_asn1_derive-0.0.9 (c (n "red_asn1_derive") (v "0.0.9") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0969qy033p7j2vzmccwilfgzhg3c6xdisywh3yiziwmlbxzwsjva")))

(define-public crate-red_asn1_derive-0.1.0 (c (n "red_asn1_derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0k083iihfcwslfi3nzpb821sjc5wkm96jd1yjm3rd3q7gnx8xqx1")))

(define-public crate-red_asn1_derive-0.1.1 (c (n "red_asn1_derive") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0knlfxj0hpvn687xa88dwdkzf69xbwcygly6gm2d92cjc938xv5m")))

(define-public crate-red_asn1_derive-0.1.2 (c (n "red_asn1_derive") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0g741cqzfrc7cayxzwfjpczla1rmsafzzljg8mfk26shhfl3b9v5")))

(define-public crate-red_asn1_derive-0.2.0 (c (n "red_asn1_derive") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "114lvr939i8qza2vb8j6frd9qfna3yvxygbpnnv04mws0pmark45")))

(define-public crate-red_asn1_derive-0.2.1 (c (n "red_asn1_derive") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qklls32r5h98vp2c8i384x3p7l10zv5fyfkfb7wwzzj9vkggxfj")))

