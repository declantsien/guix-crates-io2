(define-module (crates-io re d_ red_asn1) #:use-module (crates-io))

(define-public crate-red_asn1-0.0.9 (c (n "red_asn1") (v "0.0.9") (d (list (d (n "ascii") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)))) (h "0vdl8g2rnfj64qxn2lmg32xglqmkwfw6qsnizsb35npi8zxkk9k1")))

(define-public crate-red_asn1-0.1.0 (c (n "red_asn1") (v "0.1.0") (d (list (d (n "ascii") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.0") (d #t) (k 0)))) (h "1rxal1nljl76z6c0l3mah44d85l6431j9d1z8aqqzw51qfbb89bl")))

(define-public crate-red_asn1-0.1.1 (c (n "red_asn1") (v "0.1.1") (d (list (d (n "ascii") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.1") (d #t) (k 0)))) (h "00dp7y1q0j86c91c34nj2l8ij0l38z5bifwpvq7zngp7s4gjg8qd")))

(define-public crate-red_asn1-0.1.2 (c (n "red_asn1") (v "0.1.2") (d (list (d (n "ascii") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.1") (d #t) (k 0)))) (h "0wn1jkm2xnim9wdww2cc88a30zn1p7dnhl0qvw7gqqwrw4a8k04s")))

(define-public crate-red_asn1-0.1.3 (c (n "red_asn1") (v "0.1.3") (d (list (d (n "ascii") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.1") (d #t) (k 0)))) (h "0q2v4knhk36l8sdlp8cpn52rdj6rha1sxi3gb63f1b1gldxab72b")))

(define-public crate-red_asn1-0.2.0 (c (n "red_asn1") (v "0.2.0") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0j30r3i6j3kdi1pqx1fassr27kzpgab6k0mjkl5shyagncyv99z8")))

(define-public crate-red_asn1-0.2.1 (c (n "red_asn1") (v "0.2.1") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "002wwx8jp6zsa5mxh84n5cl64z8wqnpcsg022z9yv55mql3nsbkl")))

(define-public crate-red_asn1-0.2.2 (c (n "red_asn1") (v "0.2.2") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "0d2w1w43b0j7j005f9ydzb3s75cn9c60ci544b51kdk6ylf1lb09")))

(define-public crate-red_asn1-0.3.0 (c (n "red_asn1") (v "0.3.0") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "0v9bj857hixawkcjiskxvqvsr41qwh73mxp9h95d5nv78xpm6dbz")))

(define-public crate-red_asn1-0.3.1 (c (n "red_asn1") (v "0.3.1") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "1rx67xcccsls0ibbfyj8vcqrbn9iwxwca7882qgaa46aamvngilw")))

(define-public crate-red_asn1-0.3.2 (c (n "red_asn1") (v "0.3.2") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "0fq9d6spswbffww0mkhqpws78ix1wh6dm8m0clrayswr9b77s5lj")))

(define-public crate-red_asn1-0.3.3 (c (n "red_asn1") (v "0.3.3") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "1ak67k8mmwxdghx56pv7b69h809s1c4h4crx2334pz0fmnwqi0gz")))

(define-public crate-red_asn1-0.3.4 (c (n "red_asn1") (v "0.3.4") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "1gsm3qjmyn6rwcibv8hz72x6c7h4329jp2nzkcabk4saprscrgs2")))

(define-public crate-red_asn1-0.3.5 (c (n "red_asn1") (v "0.3.5") (d (list (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 2)))) (h "04078yxxvmnjz1x41a0k9iv72lbjji1by61jkxpf5p9f7vrglaqy")))

