(define-module (crates-io re d_ red_ntlm) #:use-module (crates-io))

(define-public crate-red_ntlm-0.0.1 (c (n "red_ntlm") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "md4") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z8wmw0ga38rv4jvh8cyy4vkgrl969q7lf2vdizwilzd0q6aimm9")))

