(define-module (crates-io re m- rem-constraint) #:use-module (crates-io))

(define-public crate-rem-constraint-0.1.0 (c (n "rem-constraint") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mktemp") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rem-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("extra-traits" "full" "visit" "fold" "visit-mut" "printing" "parsing"))) (d #t) (k 0)))) (h "1pb1n75va899y8s0m5hxiq92c63kxn9jllb7lq5y69yr3d5z05wj")))

