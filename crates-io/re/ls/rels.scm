(define-module (crates-io re ls rels) #:use-module (crates-io))

(define-public crate-rels-0.1.0 (c (n "rels") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "duration-str") (r "^0.7.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1b9rnwwhhajk143aswb4gs6q0clq9cv9xb8rjf3wqr04a9znnkgs")))

(define-public crate-rels-0.1.1 (c (n "rels") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "duration-str") (r "^0.7.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0bfqlx78pwmpapwb89nmj1x1ndhg5l0h69pplpidw4fsqid6lrig")))

(define-public crate-rels-0.1.2 (c (n "rels") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "duration-str") (r "^0.7.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "049wjxnc4lph8pr13j5niyq6bkblv0yac9dc3md5dmxy189g7h9d")))

