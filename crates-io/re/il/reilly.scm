(define-module (crates-io re il reilly) #:use-module (crates-io))

(define-public crate-reilly-0.0.1 (c (n "reilly") (v "0.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.21") (f (quote ("dtype-slim"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 2)))) (h "1fa67zl2kbnb3nv14fphadid1yjlpf2qz9wb9n1awxs7xx3p6r3c")))

