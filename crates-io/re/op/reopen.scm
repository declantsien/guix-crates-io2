(define-module (crates-io re op reopen) #:use-module (crates-io))

(define-public crate-reopen-0.1.0 (c (n "reopen") (v "0.1.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "simple-signal") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "1dyls0y7ary32v27xxqgxr73kg127k6wz0hqzdyb6g37mxqhr1il")))

(define-public crate-reopen-0.1.1 (c (n "reopen") (v "0.1.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "simple-signal") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "07913c7qg83k6v9ic49yjzasr1scx8r40y8gvqkknd6pi3qcl73x")))

(define-public crate-reopen-0.1.2 (c (n "reopen") (v "0.1.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "simple-signal") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "09ffbl5vbsxnnzavkqjr9dp9a8lhdrrf6nz8z4gwc2zi0dpqkgd7")))

(define-public crate-reopen-0.2.0 (c (n "reopen") (v "0.2.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "simple-signal") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "164w2yv2f7s1wcfjavsn63cp1frqmi8xyl4azpl988z4mbvzxq4i")))

(define-public crate-reopen-0.2.1 (c (n "reopen") (v "0.2.1") (d (list (d (n "arc-swap") (r "~0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "~0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "simple-signal") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0wvayjg0rmbgdg83x3wl1x5zj7s5a327bpwfiysr3y7187fql4bh")))

(define-public crate-reopen-0.2.2 (c (n "reopen") (v "0.2.2") (d (list (d (n "arc-swap") (r "~0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "~0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "1mzcwp5fr0fwnbc6z48nzn8hdxcf4qn7hgk8qh17zv9v8yp9w8ad")))

(define-public crate-reopen-0.3.0 (c (n "reopen") (v "0.3.0") (d (list (d (n "libc") (r "~0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "signal-hook") (r "~0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "12b3mfxkwb8akdfa701nzvqr6lsc6n84vrq088gmjy8lxlmr4an6")))

(define-public crate-reopen-1.0.0 (c (n "reopen") (v "1.0.0") (d (list (d (n "autocfg") (r "~1") (d #t) (k 1)) (d (n "libc") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "partial-io") (r "~0.3") (d #t) (k 2)) (d (n "signal-hook") (r "~0.1.9") (o #t) (d #t) (k 0)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "12aa3fd0h9kyl50k5gy3dcxfgr164asglqyir79igy843in2cp78") (f (quote (("signals" "signal-hook"))))))

(define-public crate-reopen-1.0.1 (c (n "reopen") (v "1.0.1") (d (list (d (n "autocfg") (r "~1") (d #t) (k 1)) (d (n "libc") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "partial-io") (r "~0.3") (d #t) (k 2)) (d (n "signal-hook") (r "~0.1.9") (o #t) (d #t) (k 0)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "07b744xs1y9lg6q8mynrnjyxgfw1vd4fzrhjx0412swlgvhsh8db") (f (quote (("signals" "signal-hook" "libc"))))))

(define-public crate-reopen-1.0.2 (c (n "reopen") (v "1.0.2") (d (list (d (n "autocfg") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.0, <0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 2)) (d (n "partial-io") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "signal-hook") (r ">=0.2.0, <0.3.0") (o #t) (k 0)) (d (n "simple-logging") (r ">=2.0.0, <3.0.0") (d #t) (k 2)) (d (n "version-sync") (r ">=0.9.0, <0.10.0") (d #t) (k 2)))) (h "0ccnw5b8g4jrrqijz4b0gbkxx1shc059ns3lg0sfqvbn03nv7p2y") (f (quote (("signals" "signal-hook" "libc"))))))

(define-public crate-reopen-1.0.3 (c (n "reopen") (v "1.0.3") (d (list (d (n "autocfg") (r "~1") (d #t) (k 1)) (d (n "libc") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 2)) (d (n "partial-io") (r "~0.3") (d #t) (k 2)) (d (n "signal-hook") (r "~0.3") (o #t) (k 0)) (d (n "simple-logging") (r "~2") (d #t) (k 2)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "14w2v7a4dav23jzfrxj9lgacp731xizvng4bn7slan7qmk1wwhpz") (f (quote (("signals" "signal-hook" "libc"))))))

