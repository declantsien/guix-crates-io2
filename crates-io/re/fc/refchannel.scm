(define-module (crates-io re fc refchannel) #:use-module (crates-io))

(define-public crate-refchannel-0.0.1 (c (n "refchannel") (v "0.0.1") (h "0jwp44lhpsjg9k0g9yn3n13dpvblncnlnzrfjpqc49vfy4r96821")))

(define-public crate-refchannel-0.0.2 (c (n "refchannel") (v "0.0.2") (h "1im2f8f58j64w6rw2d1lykazgb7ia0xcki4f637lp7567advjq6i")))

