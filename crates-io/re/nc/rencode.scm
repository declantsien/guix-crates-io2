(define-module (crates-io re nc rencode) #:use-module (crates-io))

(define-public crate-rencode-0.1.0 (c (n "rencode") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)))) (h "1mvicshqlzky2nh6svqgcc78l3hpfimk3hjcj5qv8k4y0wscqp59")))

(define-public crate-rencode-0.1.1 (c (n "rencode") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)))) (h "0yyzyc2hmpccbg5aaw97xjmxsymi67knvrz532jqhlq3075ba3qx")))

(define-public crate-rencode-0.2.0 (c (n "rencode") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)))) (h "0xnfgi5688xr1s84larv2gy6l6xawzj34pj18z3vghqk6lirfnqm")))

(define-public crate-rencode-0.2.1 (c (n "rencode") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)))) (h "11p5hdz9fcr8c9jkq9ynkdyslpvvdkd4nx11r4arnyxjmxcn9mir")))

