(define-module (crates-io re nc rench) #:use-module (crates-io))

(define-public crate-rench-0.1.0 (c (n "rench") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1fsgfciqxfm893mkk4i2rvynl0b2gx0w762z780rr66241j7y827")))

(define-public crate-rench-0.2.0 (c (n "rench") (v "0.2.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "19zk961l1bz741lrv70hq278vjzxc7x96sjpcm018vzhjv7yv43q")))

(define-public crate-rench-0.3.0 (c (n "rench") (v "0.3.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "022m1233334gh6655sakf9sdb8f1kpg0xjwdipcpgbfi2rafjdvw")))

