(define-module (crates-io re se resend-rs) #:use-module (crates-io))

(define-public crate-resend-rs-0.1.0 (c (n "resend-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "136ymhdj09ynvvm5gqpzn0aapklz943v31cnra79gvhbxpbp1zx2") (f (quote (("async"))))))

(define-public crate-resend-rs-0.2.0 (c (n "resend-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1jy255999lz2nr9xks9dxl3w4lrwa9m1rwrbgkzl9yn4s4hmingm") (f (quote (("async"))))))

(define-public crate-resend-rs-0.3.0 (c (n "resend-rs") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1fl7abfwkizms3xwvkqcr7b2q46awx7j7cb8jsd0ds3z996mb3jq") (f (quote (("async"))))))

(define-public crate-resend-rs-0.4.0 (c (n "resend-rs") (v "0.4.0") (d (list (d (n "ecow") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "maybe-async") (r "^0.2.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "test-util" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f95ki8x13w6sdy48sqibvbz3wmwyjldxaxb3341mgwna8ks0jf5") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "native-tls") ("blocking" "reqwest/blocking" "maybe-async/is_sync"))))))

