(define-module (crates-io re se reservoir-sampler) #:use-module (crates-io))

(define-public crate-reservoir-sampler-0.0.1 (c (n "reservoir-sampler") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0sn5r6xqj3xx3li9qvvk07k4v83nfr62d1q7nzhf163z1lv6chrw")))

(define-public crate-reservoir-sampler-0.1.0 (c (n "reservoir-sampler") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1yssl4kp5x9b8br1lr9y3sp0mczg21p9cs8slwlrkw785ppz9mn7")))

