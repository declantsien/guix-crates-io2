(define-module (crates-io re se reset-recognizer) #:use-module (crates-io))

(define-public crate-reset-recognizer-0.7.0 (c (n "reset-recognizer") (v "0.7.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ji5pqkc8k2421dfwn33337d7d8s8xsdpjyskslbra0a3paqgfzf")))

(define-public crate-reset-recognizer-0.7.1 (c (n "reset-recognizer") (v "0.7.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k1ayj1dfn1dzrs7xxs9mfpg4qdhkkg127mw2ixwcv64yip8rjmy")))

(define-public crate-reset-recognizer-0.7.2 (c (n "reset-recognizer") (v "0.7.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07ll0f9ca9bsqc9x5p8lnkv94ip2x5zkcag3b587d6j5dkqr20q2")))

(define-public crate-reset-recognizer-0.8.0 (c (n "reset-recognizer") (v "0.8.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1j142hybhj9vicw6flwcm8brqqa09105zfnkki2vg9m1z5f16lv9")))

