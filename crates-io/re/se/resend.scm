(define-module (crates-io re se resend) #:use-module (crates-io))

(define-public crate-resend-0.1.0 (c (n "resend") (v "0.1.0") (d (list (d (n "resend_derive") (r "^0.1") (d #t) (k 0)))) (h "04pv4fwmyp09zknf1bq33x5nlzqxji4vrxbac085hzx2b0bxgfd0") (f (quote (("unstable") ("little") ("len_vlq") ("len_16") ("default") ("big") ("MAX_LEN_500M") ("MAX_LEN_2G") ("MAX_LEN_100M"))))))

(define-public crate-resend-0.1.1 (c (n "resend") (v "0.1.1") (d (list (d (n "resend_derive") (r "^0.1") (d #t) (k 0)))) (h "10ac6n8wimzblv34m17v04wi0jhkhysalj8yskzg1azk68jjmj6f") (f (quote (("unstable") ("little") ("len_vlq") ("len_16") ("default") ("big") ("MAX_LEN_500M") ("MAX_LEN_2G") ("MAX_LEN_100M"))))))

(define-public crate-resend-0.1.2 (c (n "resend") (v "0.1.2") (d (list (d (n "resend_derive") (r "^0.1") (d #t) (k 0)))) (h "0zb1dv0g893xadfmkv200nbnf43szb9zjcl59dl99ar19y827fjl") (f (quote (("unstable") ("little") ("len_vlq") ("len_16") ("default") ("big") ("MAX_LEN_500M") ("MAX_LEN_2G") ("MAX_LEN_100M"))))))

(define-public crate-resend-0.1.3 (c (n "resend") (v "0.1.3") (d (list (d (n "resend_derive") (r "^0.1") (d #t) (k 0)))) (h "1di1l2gl8jvcxcqx95cd35jscjdf42iz7msj049yq9l1zmp43sj3") (f (quote (("unstable") ("little") ("len_vlq") ("len_16") ("default") ("big") ("MAX_LEN_500M") ("MAX_LEN_2G") ("MAX_LEN_100M"))))))

