(define-module (crates-io re se reservoir) #:use-module (crates-io))

(define-public crate-reservoir-0.1.0 (c (n "reservoir") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "0q4mycafglm0v1f1kfj6va0p3jdlpvf4xfnnd8bxnrmg2mv0fn6k")))

(define-public crate-reservoir-0.2.0 (c (n "reservoir") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "0zakmvvvsafpzvacj9c8wm8j4n529zsn6dxh7b1d82kb1b1wxgi5")))

