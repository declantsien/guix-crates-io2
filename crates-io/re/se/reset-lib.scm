(define-module (crates-io re se reset-lib) #:use-module (crates-io))

(define-public crate-ReSet-Lib-0.1.0 (c (n "ReSet-Lib") (v "0.1.0") (h "04w5wjf9g4mdpnv0gjg9zxpq5k6q6ir2wg0p0r4ac55xblm7la28")))

(define-public crate-ReSet-Lib-0.1.1 (c (n "ReSet-Lib") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)))) (h "1xn4cbhnqc4rvcpy3hyq9xzzhix20qsdqfzj8ra5myimznc3vm43")))

(define-public crate-ReSet-Lib-0.1.2 (c (n "ReSet-Lib") (v "0.1.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1415h48c41xqayrzwi5gfp8ah5c9g352p7h33v1sxfwzn31gjj9w")))

(define-public crate-ReSet-Lib-0.1.3 (c (n "ReSet-Lib") (v "0.1.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "054j9xqnqblldgbwprn58m9f4jrrj6jip1p6xcr7wsavw4g1x9yw")))

(define-public crate-ReSet-Lib-0.1.4 (c (n "ReSet-Lib") (v "0.1.4") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1wbnwwmg43fa23qsjyndpz8cz8ivbdi1q4kknxflhy18pvngg4gd")))

(define-public crate-ReSet-Lib-0.1.5 (c (n "ReSet-Lib") (v "0.1.5") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1qld32hkci8l7hhkphs33a5jpff95zpnnxki6i623fgdjxa051nw")))

(define-public crate-ReSet-Lib-0.1.6 (c (n "ReSet-Lib") (v "0.1.6") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1g6knfkbh16zy1x9nzi52b9mbi0pwx2hxwzssw0v7phx32fj6lji")))

(define-public crate-ReSet-Lib-0.1.7 (c (n "ReSet-Lib") (v "0.1.7") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0cx31z5szj6jk63h0l76himfl5rmmm25pj5ld5a6njcl44s6d9g6")))

(define-public crate-ReSet-Lib-0.1.8 (c (n "ReSet-Lib") (v "0.1.8") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1443r6ah40gn4vqd85s9d27s89jpx28zgc4gwq9avcdww5z8zhch")))

(define-public crate-ReSet-Lib-0.1.9 (c (n "ReSet-Lib") (v "0.1.9") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0gbrh4dhpjbd4ybwijcwp0c8pw6vbkbc81k6n50pzvikszai9d2y")))

(define-public crate-ReSet-Lib-0.2.0 (c (n "ReSet-Lib") (v "0.2.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "09pjlwdgiii2zvdvr8p3zrf728lx4wdf9x8w0hrw12m23c0nw1kg")))

(define-public crate-ReSet-Lib-0.2.1 (c (n "ReSet-Lib") (v "0.2.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0xj4ccdvy25p7q2srfg2qbkbpg09c8pzddhikmn6cjf5r7h9slqs")))

(define-public crate-ReSet-Lib-0.2.2 (c (n "ReSet-Lib") (v "0.2.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "07ssirg8bhlk5yyls3vh94rdg3v3avvbzgk596qh6azp21igwrsz")))

(define-public crate-ReSet-Lib-0.2.3 (c (n "ReSet-Lib") (v "0.2.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0q09pqb3mbni9734lr6yq8357cfa9arcjanqzr7k58p26hvz2giy")))

(define-public crate-ReSet-Lib-0.2.4 (c (n "ReSet-Lib") (v "0.2.4") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0kzbqfmdf3xc4x81zzh9nwd3af7ylxvzj4srnadikm2656rjk7q2")))

(define-public crate-ReSet-Lib-0.2.5 (c (n "ReSet-Lib") (v "0.2.5") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0y9hawbkam9ps6xgs7k4vpjaprgpw0016a33cghsi7qyw5mmsalb")))

(define-public crate-ReSet-Lib-0.2.6 (c (n "ReSet-Lib") (v "0.2.6") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1w6r05rpagqsan67cg7hhxxf4m41ck4b1wsdr6raaaswg06z2j4h")))

(define-public crate-ReSet-Lib-0.2.7 (c (n "ReSet-Lib") (v "0.2.7") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1nm8fzw4631cz23lvxb221p6wcfl7bcjkjf1dlazmp1k9llgw50g")))

(define-public crate-ReSet-Lib-0.2.8 (c (n "ReSet-Lib") (v "0.2.8") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0fl72zlag9z63x4s085sh9942v0bhif64rdjsf53yra17mxhjqb7")))

(define-public crate-ReSet-Lib-0.2.9 (c (n "ReSet-Lib") (v "0.2.9") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0asb0zhf21y535cvq628093sjad5d3y119q3l8wsfgr0xxp4j9sl")))

(define-public crate-ReSet-Lib-0.3.0 (c (n "ReSet-Lib") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "02djhb1i9sgnxqm6757jv44586f4xpm3mbi0k8nd8r1999565hl8")))

(define-public crate-ReSet-Lib-0.3.1 (c (n "ReSet-Lib") (v "0.3.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0wmblz8wllh3i0f484dd7ydlnzlpaga8cyl0568464q35r3bhhzi")))

(define-public crate-ReSet-Lib-0.3.2 (c (n "ReSet-Lib") (v "0.3.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "18kxxv4zysqgfnjdzwc1i5l6r6l769vp86pvnbivclx85n35k490")))

(define-public crate-ReSet-Lib-0.3.3 (c (n "ReSet-Lib") (v "0.3.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0wvmgp1ll2ji3wz96l5q37zsvp9qrl8ldg4rd1g0zmnd2jv1zpp6")))

(define-public crate-ReSet-Lib-0.3.4 (c (n "ReSet-Lib") (v "0.3.4") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1z8d7hnzym323pdp60yb5xngd0a4l1icyl8m1h74nm8r0n0c7nr3")))

(define-public crate-ReSet-Lib-0.3.5 (c (n "ReSet-Lib") (v "0.3.5") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "17dic9a9g6rgrmjj2jlnrnnl326fnd2847a6afrh8rha8s7l5wm0")))

(define-public crate-ReSet-Lib-0.3.6 (c (n "ReSet-Lib") (v "0.3.6") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1bs9va10rwx2iwf3yx9f1rcd626a05psl5m9mq6fmmwh2hxq5p2r")))

(define-public crate-ReSet-Lib-0.3.7 (c (n "ReSet-Lib") (v "0.3.7") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0lsn1bz2vrsf7gdrymqmnsbi6279hghd22rrvpw3w9wvrmf1sp4w")))

(define-public crate-ReSet-Lib-0.3.8 (c (n "ReSet-Lib") (v "0.3.8") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1hpv79s8v94rxp3x3al15z1kia032pcjw3447jbhy4zclkwifivf")))

(define-public crate-ReSet-Lib-0.3.9 (c (n "ReSet-Lib") (v "0.3.9") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0l37npajnpmw9rjb4wrp1d6zricjlzkbxav33628q6qzas5rbz74")))

(define-public crate-ReSet-Lib-0.4.0 (c (n "ReSet-Lib") (v "0.4.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1d1v1ch9wa91zvhx3pics2xz326h1gbkxryraxi21s823iyg2p03")))

(define-public crate-ReSet-Lib-0.4.1 (c (n "ReSet-Lib") (v "0.4.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1cflyc68637p8d5aph04yylcpyvz5ayyfz9wk2xl5hclzplnrd5r")))

(define-public crate-ReSet-Lib-0.4.2 (c (n "ReSet-Lib") (v "0.4.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "107jb4y19ajxkp1gnak6cm4prn88hia0b6a1klzk1b31ji3h2c11")))

(define-public crate-ReSet-Lib-0.4.3 (c (n "ReSet-Lib") (v "0.4.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0z0cdznhmfzm95k30c1qn9d880hh6qyggmcn5jff5f05hjsi45sm")))

(define-public crate-ReSet-Lib-0.4.4 (c (n "ReSet-Lib") (v "0.4.4") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "048zsmbpmgxdk6gr58zxxg26k3rmm3p2jjgdvkn0qyyn0m9031wk")))

(define-public crate-ReSet-Lib-0.4.5 (c (n "ReSet-Lib") (v "0.4.5") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0np4sj5n00gxp5sy5z9xx6v9s4qbf766ykp3kyh9nfy1nxvf0ihc")))

(define-public crate-ReSet-Lib-0.4.6 (c (n "ReSet-Lib") (v "0.4.6") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1zbpvxkqz08nrhgkynz50cf7rkz4mzqp6589h7jdvrs7ch8y3cnw")))

(define-public crate-ReSet-Lib-0.4.7 (c (n "ReSet-Lib") (v "0.4.7") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1v6ag6caqydi0wx0adb9y78kc6qjsj7psqb97qxs6kc2jwn8qv13")))

(define-public crate-ReSet-Lib-0.4.8 (c (n "ReSet-Lib") (v "0.4.8") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1pa126vwn9hkfn9rqi1hvzvi09dk69p166cy3a1s2xjzywb9y2ax")))

(define-public crate-ReSet-Lib-0.4.9 (c (n "ReSet-Lib") (v "0.4.9") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "12g2bgm5ajfaz2b0qm99lw8s661vqjkc5ldrn90rm72q3aahd93p")))

(define-public crate-ReSet-Lib-0.5.0 (c (n "ReSet-Lib") (v "0.5.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0r4fcjffzjxf2hz1isqsrsp3q3biisizlq7ssqnghzzj373dqwfl")))

(define-public crate-ReSet-Lib-0.5.1 (c (n "ReSet-Lib") (v "0.5.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1ydl85kcvp0jgv9029p8nscs42r8z2mhpp0n27blp0avj8hncyr3")))

(define-public crate-ReSet-Lib-0.5.2 (c (n "ReSet-Lib") (v "0.5.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0ajkgwbcndj6892avkfd2bcd5frb5c799c0rgmncsgm7gjvbq8rm")))

(define-public crate-ReSet-Lib-0.5.3 (c (n "ReSet-Lib") (v "0.5.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1s6ywslm8f53l0fd3cj14bln6xr37095xlapfr7vy8sm2wyjs5zz")))

(define-public crate-ReSet-Lib-0.5.4 (c (n "ReSet-Lib") (v "0.5.4") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1d2kib1phpblw7zaily5vfa5xhbzs9va909xbvalaz5swawppak7")))

(define-public crate-ReSet-Lib-0.5.5 (c (n "ReSet-Lib") (v "0.5.5") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0hjgmrl9rszp4i4pi7wjnyz48hdadr6jv3zahpr8k9dpx8ghl79d")))

(define-public crate-ReSet-Lib-0.5.6 (c (n "ReSet-Lib") (v "0.5.6") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0hyblhby1cc84avmb24q3yqsqzsn06bkf9240xh2z2fwn9bnmhb1")))

(define-public crate-ReSet-Lib-0.5.7 (c (n "ReSet-Lib") (v "0.5.7") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0iqyshi8xs7p7sa3gqkdphmr58gmrsran86cgz9jpd10gpg1556v")))

(define-public crate-ReSet-Lib-0.5.8 (c (n "ReSet-Lib") (v "0.5.8") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "09mf7xwwjyhfqf8p93d071cxqpdmwv7sqdvijg88a6gmqcxmqdl7")))

(define-public crate-ReSet-Lib-0.5.9 (c (n "ReSet-Lib") (v "0.5.9") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1flihgfkl19y3j8f4mpwqzq9q0zb52c6cjnw7p3fzl0nl1zx68ym")))

(define-public crate-ReSet-Lib-0.6.0 (c (n "ReSet-Lib") (v "0.6.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "0c0k4dhn38dkbiw6s3plw78ap11pk2wqh512j7rdv6knv57prr7m")))

(define-public crate-ReSet-Lib-0.6.1 (c (n "ReSet-Lib") (v "0.6.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1xamnjr38d2g12r4wavn3bhrgxwcl0a17115y5wcw2cw30v9jczh")))

(define-public crate-ReSet-Lib-0.6.2 (c (n "ReSet-Lib") (v "0.6.2") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1p74wchm6h7mc0pcg1np11xkpj22wvw74grvp07s4xwhizn4391m")))

(define-public crate-ReSet-Lib-0.6.3 (c (n "ReSet-Lib") (v "0.6.3") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "pulse") (r "^2.0") (d #t) (k 0) (p "libpulse-binding")))) (h "178dchsymgr9nzb3jka2zk0m6p297w81nm5dngzid59c3z5q5619")))

