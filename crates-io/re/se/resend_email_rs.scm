(define-module (crates-io re se resend_email_rs) #:use-module (crates-io))

(define-public crate-resend_email_rs-0.1.0 (c (n "resend_email_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.198") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0rhvf7n6k9xkq3iandqp1mccl82hng7wz19lz71az8gyh0myhcd5")))

