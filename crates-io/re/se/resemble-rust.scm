(define-module (crates-io re se resemble-rust) #:use-module (crates-io))

(define-public crate-resemble-rust-0.1.0 (c (n "resemble-rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1g9fj2gqixzzdaaalhb9rxidhk9cna8mb0i8ipagpfi71h8z316s")))

(define-public crate-resemble-rust-1.0.0 (c (n "resemble-rust") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "17kvv0kshcqhxkj2aaq5hg1s9f7h2yc79vlmd20anyz800mgd475")))

(define-public crate-resemble-rust-1.1.0 (c (n "resemble-rust") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1ydlq4b0n4jrjgb9bmwplqcn2vmdn34vzj3flnaywyy39f0glcj8")))

