(define-module (crates-io re se resend_derive) #:use-module (crates-io))

(define-public crate-resend_derive-0.1.0 (c (n "resend_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x65jzvmzp09f4iac1ypk8ayvbnvmhdlmy37vcv6mazhh42ci6ds")))

(define-public crate-resend_derive-0.1.1 (c (n "resend_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0py6bnnly4miw1v7x6i9lil76vqnqqdwghnq07if1g18v8z95gny")))

