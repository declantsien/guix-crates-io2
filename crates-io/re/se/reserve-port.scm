(define-module (crates-io re se reserve-port) #:use-module (crates-io))

(define-public crate-reserve-port-1.0.0 (c (n "reserve-port") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "09gq0b0cbhzr5j1q49hv709mp1q0x4lcsfkgrgy4sym5qaf2ajb1")))

(define-public crate-reserve-port-1.1.0 (c (n "reserve-port") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1v12pxfsagkp23xl60ayv40qpg2k9clflanxnw2cldy1qxlalywr")))

(define-public crate-reserve-port-1.2.0 (c (n "reserve-port") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1q0n2vnscrpkinfqvmmcv829adypbb2qfc1s7ackq57nb4yzivj6")))

(define-public crate-reserve-port-1.3.0 (c (n "reserve-port") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0pwa14vd3w2j8imxsi946pp0ivxbmz8fvzj905cwv1h28v9yy4mj")))

(define-public crate-reserve-port-2.0.0 (c (n "reserve-port") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "183jqf8n0ps4b0dd8br5ik8vnw78k2164qd7l4mm7hf62pzffs9r")))

(define-public crate-reserve-port-2.0.1 (c (n "reserve-port") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10x21rdb1hjzp6n5flbbw3hfd7brmirckz1q0zsf3a7s5d516f4q")))

