(define-module (crates-io re se reser) #:use-module (crates-io))

(define-public crate-reser-0.1.0 (c (n "reser") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_any") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0y0ly1199bynkwqma5ja3g8g9pv7jl1c9vfl40bfphdixchs9z2b")))

(define-public crate-reser-0.1.1 (c (n "reser") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_any") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0np8k5vz0v1z72l8p78lb29q2pp4p3ym7pqjjyvic4s9i66arz62")))

