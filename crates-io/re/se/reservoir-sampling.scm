(define-module (crates-io re se reservoir-sampling) #:use-module (crates-io))

(define-public crate-reservoir-sampling-0.1.0 (c (n "reservoir-sampling") (v "0.1.0") (d (list (d (n "binary-heap-plus") (r "^0.4.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)))) (h "1nv4hpdk2639vb90r3vdvss21cagd86r0xn6145584m93icgi38s") (y #t)))

(define-public crate-reservoir-sampling-0.1.1 (c (n "reservoir-sampling") (v "0.1.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)))) (h "16wggzj21z5kknx37hly9hh46lm607rd1ghwfsnx6rkaag50imd8") (y #t)))

(define-public crate-reservoir-sampling-0.2.0 (c (n "reservoir-sampling") (v "0.2.0") (d (list (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1z64qr8y0q65ik7zs1gg7zxfkl6nk2j7s9m2gvi80116barvhl3n") (f (quote (("streaming_iterator_support" "streaming-iterator")))) (y #t)))

(define-public crate-reservoir-sampling-0.2.1 (c (n "reservoir-sampling") (v "0.2.1") (d (list (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0sk10dn0frxgcnc9k4ciga6b7ldyb90rj9g9zp7wfcvkix05kv7h") (f (quote (("streaming_iterator_support" "streaming-iterator"))))))

(define-public crate-reservoir-sampling-0.2.2 (c (n "reservoir-sampling") (v "0.2.2") (d (list (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1fzckk1v9ccf2q7h04ds3jvagxi3isjbi9awjzc2l6yzmyvy874q") (f (quote (("streaming_iterator_support" "streaming-iterator"))))))

(define-public crate-reservoir-sampling-0.2.3 (c (n "reservoir-sampling") (v "0.2.3") (d (list (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1v3s96c0ksp6559qklab9y6d36rplxh5ckpn5gjp1byzfpj6dh1l") (f (quote (("streaming_iterator_support" "streaming-iterator"))))))

(define-public crate-reservoir-sampling-0.3.0 (c (n "reservoir-sampling") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "02ap3asfd4p2nw2djyn4bjccnxg5kzipcps6cw7xp4hs4dxc80gi") (f (quote (("unweighted") ("default" "unweighted"))))))

(define-public crate-reservoir-sampling-0.3.1 (c (n "reservoir-sampling") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1xwdc0g81sf8s7pql2dqd2xy6jblli3rr2iphz071k8580b9gbh9") (f (quote (("unweighted") ("default" "unweighted"))))))

(define-public crate-reservoir-sampling-0.4.0 (c (n "reservoir-sampling") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "143d05j3sxs6vqdvwbncg9l4rpypl1566ggcn03wqw5y5y58qx6l") (f (quote (("weighted") ("unweighted") ("default" "unweighted" "weighted"))))))

(define-public crate-reservoir-sampling-0.4.1 (c (n "reservoir-sampling") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0s89i8y9m6s3m911b6w3g47as9y92h71cx1qyzjwjhrg5441fcsy") (f (quote (("weighted") ("unweighted") ("default" "unweighted" "weighted"))))))

(define-public crate-reservoir-sampling-0.4.2 (c (n "reservoir-sampling") (v "0.4.2") (d (list (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "18xi3qk6ywak9aim7jay8mvra6brgg3y5ia6lngg26nidl3706vr") (f (quote (("weighted") ("unweighted") ("default" "unweighted" "weighted"))))))

(define-public crate-reservoir-sampling-0.5.0 (c (n "reservoir-sampling") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "06k6zjnnpqy8909h6cnfnqcp60x6jp43ghg8x9d97gwj4hag4qap") (f (quote (("weighted") ("unweighted") ("default" "unweighted" "weighted"))))))

(define-public crate-reservoir-sampling-0.5.1 (c (n "reservoir-sampling") (v "0.5.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0vnyl7k7qab6832an4sj2r6vh6k74b6klz28j1qwalch2v0dn9x5") (f (quote (("weighted") ("unweighted") ("default" "unweighted" "weighted"))))))

