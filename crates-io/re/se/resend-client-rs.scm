(define-module (crates-io re se resend-client-rs) #:use-module (crates-io))

(define-public crate-resend-client-rs-0.1.0 (c (n "resend-client-rs") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0l2ahgfj4wrbdslzkr6n9zq8lp58vxq76149gni9v5rb68qzkdrh")))

