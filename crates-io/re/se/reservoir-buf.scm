(define-module (crates-io re se reservoir-buf) #:use-module (crates-io))

(define-public crate-reservoir-buf-0.0.1 (c (n "reservoir-buf") (v "0.0.1") (h "1hl1gpj4nwg6x069n7bngxy4vkc7p5q4q72nr50pfvd8gq59zdng")))

(define-public crate-reservoir-buf-0.0.2 (c (n "reservoir-buf") (v "0.0.2") (h "0ckjb2xv44fxa9z1nfq2n5yqqn18a01yhphvsd3yiqh7cch0ympr")))

(define-public crate-reservoir-buf-0.0.3 (c (n "reservoir-buf") (v "0.0.3") (h "0klksg1srypi0051f7f18zcpp6qrmw6rn2g9512d75p476kr5xlk")))

