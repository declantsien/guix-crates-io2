(define-module (crates-io re dw redwood-wiki) #:use-module (crates-io))

(define-public crate-redwood-wiki-0.1.0 (c (n "redwood-wiki") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)) (d (n "rusqlite") (r "^0.24.1") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "1a0a25y80l25i3zdwfzcjijz380lj60q3y4s4xqla9135dfwflkk")))

