(define-module (crates-io re dw redwood_cli) #:use-module (crates-io))

(define-public crate-redwood_cli-0.1.0 (c (n "redwood_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "redwood") (r "^0.1.0") (d #t) (k 0)))) (h "0npnw5y5wajfs4fxl4h39c6pm3i7y28q82lssn7lhqykrzsmxy90")))

