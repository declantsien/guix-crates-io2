(define-module (crates-io re dw redwm) #:use-module (crates-io))

(define-public crate-redwm-0.0.1 (c (n "redwm") (v "0.0.1") (h "1ar7k4b5xfa6vp20ppn2n2qn8fvqynarmdd72sgzczxg327li779")))

(define-public crate-redwm-0.0.2 (c (n "redwm") (v "0.0.2") (d (list (d (n "x11rb") (r "^0.8.1") (f (quote ("all-extensions"))) (d #t) (k 0)))) (h "0g7cqi3480q1l2rn67dhfalg60gf0p2aasrrhghmb14nsh9d3b9a")))

