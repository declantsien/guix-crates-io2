(define-module (crates-io re dw redwood) #:use-module (crates-io))

(define-public crate-redwood-0.1.0 (c (n "redwood") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "crossbeam-utils") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0wa19faz2nnig51h1pbqgvyr0pcz8iky7h74wadvrdnhb1qpsjkd") (f (quote (("default" "asm") ("asm"))))))

