(define-module (crates-io re qr reqrnpdno) #:use-module (crates-io))

(define-public crate-reqrnpdno-0.1.0 (c (n "reqrnpdno") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "cookies" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "0kp9wb3d27ajpjvs0vcj2wkmjh7xxnv09f4bcrwc907s9w6z3az7")))

(define-public crate-reqrnpdno-0.1.1 (c (n "reqrnpdno") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "cookies" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "030zmyhir8snvnz5y16gsi0kwk3yiwd14mpsns7ri5yh38bgpyd4")))

(define-public crate-reqrnpdno-0.1.2 (c (n "reqrnpdno") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "cookies" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "055jibsjik7d5vydik0npzj0akizm85mz7gm5hgl82q5hp08kqh7")))

(define-public crate-reqrnpdno-0.1.3 (c (n "reqrnpdno") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "cookies" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "1v1hxrq95q2rgzq20h1pynf6wb9yb5h92994074c282n25myl9ww")))

