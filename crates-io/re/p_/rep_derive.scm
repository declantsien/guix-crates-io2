(define-module (crates-io re p_ rep_derive) #:use-module (crates-io))

(define-public crate-rep_derive-0.1.0 (c (n "rep_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "07gp3z35ribn485xl18m19r16qwwczkn3vj6sc32xisi3ff3cv5k")))

(define-public crate-rep_derive-0.2.0 (c (n "rep_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksn5y27a2kj91nmxssg83g58v0sakmmdf8w1z4an5gqxgdhviqq")))

(define-public crate-rep_derive-0.3.0 (c (n "rep_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0gydy4d1r0s08pndx0wj5nxy7vch7q3snfzi8s6095b7kwqlc5ki")))

