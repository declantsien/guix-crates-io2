(define-module (crates-io re sl reslab) #:use-module (crates-io))

(define-public crate-reslab-0.0.0 (c (n "reslab") (v "0.0.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "sharded-slab") (r "^0") (d #t) (k 0)))) (h "1r4326czs8bmr4s8lmqx5gr3dq6q5pigdf02w4xvi1hjh4xm7hgv")))

