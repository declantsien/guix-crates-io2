(define-module (crates-io re fi refined_type) #:use-module (crates-io))

(define-public crate-refined_type-0.1.0 (c (n "refined_type") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1lcmpkisyd8br2wk084r982rnbdwi2r02dwa29xwcmc6j6k3zfd6") (y #t)))

(define-public crate-refined_type-0.1.1 (c (n "refined_type") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0ax4648r2wxygky2if4fl3j97m0s065ri0lfqr8k7fdzdkq19b91") (y #t)))

(define-public crate-refined_type-0.1.2 (c (n "refined_type") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "05iyhnw8b2v7q8dpb6gszkffkm5917gfxfkqz6i4giv3p4hgzh7d") (y #t)))

(define-public crate-refined_type-0.1.3 (c (n "refined_type") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "10xb8sf2lbhgqv65dcgn2v487figmwm3v6chqxgb8chkb0cdpchd") (y #t)))

(define-public crate-refined_type-0.1.4 (c (n "refined_type") (v "0.1.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ch04nybmjcrlv02lh5hdhqmp1i3vx156b94fqvy19vx0lg0yd0x") (y #t)))

(define-public crate-refined_type-0.1.5 (c (n "refined_type") (v "0.1.5") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0qfwwdz2wadi2rllz9j49x52l573ah3qxkv4bv55926224mqkx70") (y #t)))

(define-public crate-refined_type-0.2.0 (c (n "refined_type") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1yvd40b403wiavrwl5aa01s5cg4pyn1654smqwn6j6dp4gc7jcg6") (y #t)))

(define-public crate-refined_type-0.2.1 (c (n "refined_type") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "19gdzxyl2qy8lm5yp49q2wlq1jimxpahqp55yjd5asfnjad47s7b") (y #t)))

(define-public crate-refined_type-0.2.2 (c (n "refined_type") (v "0.2.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1mjcqiif4dm8innz15xy9972s95pbx4q9s975ayy2blkcy9xgkw2") (y #t)))

(define-public crate-refined_type-0.2.3 (c (n "refined_type") (v "0.2.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0c24j9bzy9pzj54xnwp7pnwfnghrwkjv2vn6z6vhr5y9jx940fy2") (y #t)))

(define-public crate-refined_type-0.2.4 (c (n "refined_type") (v "0.2.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1lpkl6hsl6vz8k46l8ifr3kaxgyc7yx8zygszh9ljnyz1injixs5") (y #t)))

(define-public crate-refined_type-0.3.0 (c (n "refined_type") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "10pkf3ybmq9lpj8dhywz32r14rcxhnwvih6hqg071xk604cvjikw") (y #t)))

(define-public crate-refined_type-0.3.1 (c (n "refined_type") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "19m04x5x00lf5fc9mmkgb4xzf3i5lc4l68bybld7ncbqb7df89ky") (y #t)))

(define-public crate-refined_type-0.3.2 (c (n "refined_type") (v "0.3.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "14iw3fy94azqy0j5scijrzs43m9zfr8qflf47vdb512h65sgjmm8") (y #t)))

(define-public crate-refined_type-0.3.3 (c (n "refined_type") (v "0.3.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "04z1bhwvyh16b58p5n12majlg7j3m9fm81cwx49m4dyqpb6fqjhf")))

(define-public crate-refined_type-0.3.4 (c (n "refined_type") (v "0.3.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "099rc1a1gr20raqw9af8j93snv7n91j0ndcdvk0h8765pf3y3mr5")))

(define-public crate-refined_type-0.3.5 (c (n "refined_type") (v "0.3.5") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1my9jz4s3fv89yskca8gqam8a98qk3i11iq0v43rj3zf8bxiwyzj")))

(define-public crate-refined_type-0.3.6 (c (n "refined_type") (v "0.3.6") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1nxqjhbl68hi31278nibg03nm27z6n8pyzvyfwj9xwyzs80krx4j")))

(define-public crate-refined_type-0.3.7 (c (n "refined_type") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "18jvxbwziiby48kg70zplq675mgf8p17fmmrhb3g2zvg9df7rw5g")))

(define-public crate-refined_type-0.4.0 (c (n "refined_type") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "038pym6mxc2s7m92njv4b88z7cw2lg6k3l4y82rr1qgs9y2aylj4")))

(define-public crate-refined_type-0.4.1 (c (n "refined_type") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1js0s489qidlc6wn8bzd6pcr2h750k4k9h2an2dr900cpziv40gq")))

(define-public crate-refined_type-0.4.2 (c (n "refined_type") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0a8gdl9rql3l85mbm0s4av475cvllxb3qgla1371zvgbq48mw3g1")))

(define-public crate-refined_type-0.4.3 (c (n "refined_type") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "092cbjvf7gxbwr7wsz6ws3vs2vq4pqiq1ckqxsiw1zwb1lsml6xj")))

(define-public crate-refined_type-0.4.4 (c (n "refined_type") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0jqqz49k2xw8hsgzjlgi40lm47viqi9p30r4ly3irrwdq68h33r2")))

(define-public crate-refined_type-0.4.5 (c (n "refined_type") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "04zwhirhwhl7k82vdqh87ymh70k34lk01qr7cgi9h9mxrppqsp6g") (y #t)))

(define-public crate-refined_type-0.4.6 (c (n "refined_type") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0xcc112qmww32rflnr7pvayk7v5b2xk6s0p458sdjihy8738fww4") (y #t)))

(define-public crate-refined_type-0.4.7 (c (n "refined_type") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0af4k26x76zriig1ynkq8lkqwn32wd6bmdc3lp9npy7dfppxf60r")))

(define-public crate-refined_type-0.4.8 (c (n "refined_type") (v "0.4.8") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "12rz2zvd5lj7zfylzhl2hgrb64fz2hcpwf1cgzpgkc1ab9h6mggg")))

(define-public crate-refined_type-0.4.9 (c (n "refined_type") (v "0.4.9") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0w4rda6b6cac4c70rqbjpm4ghhcfma3zjdhpf7282j2d80d1ivyb")))

(define-public crate-refined_type-0.4.10 (c (n "refined_type") (v "0.4.10") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1cxw1y3vnsnj6q9bpwm1kwf61mx1wxrqpadm0zg4p53d8241knnq") (y #t)))

(define-public crate-refined_type-0.4.11 (c (n "refined_type") (v "0.4.11") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "14dvf5f4g59f9syyh8i2892wiwm3iwnmh344gj13cgvpz70blf1i") (y #t)))

(define-public crate-refined_type-0.4.12 (c (n "refined_type") (v "0.4.12") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0fkpli0q43vvj8pl5ahjg0jr8v6jziq4z7skkjgkmp83sx87njiy") (y #t)))

(define-public crate-refined_type-0.4.13 (c (n "refined_type") (v "0.4.13") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0svxzdbjdzkdpsk9185mrpbskbf8fcn0lkxf5qszak6w7gjdbp2n")))

(define-public crate-refined_type-0.4.14 (c (n "refined_type") (v "0.4.14") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "11kxvai9msxnnb6r06ljhmlzakdfl50vgnipsivdzfahlmir78w6")))

(define-public crate-refined_type-0.4.15 (c (n "refined_type") (v "0.4.15") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "03fq5hw6qi7lasary288glyp9p19ydd0kx4lzkgd390f9vsmil3d")))

(define-public crate-refined_type-0.4.16 (c (n "refined_type") (v "0.4.16") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "00d0vrbrris843sz63brfglyaj1mf1imb58yinp3dm7rycl5mnds")))

(define-public crate-refined_type-0.5.0 (c (n "refined_type") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0xx5f1hsiiqrarsmda6jms2xbfzkjxg4l9m90dwwknas3hnc2iz4") (y #t)))

(define-public crate-refined_type-0.5.1 (c (n "refined_type") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0dvrsgr7s7nwgar7s8hnpd4z6brwh5q2q43sgday1pc3vzvr6dgz")))

(define-public crate-refined_type-0.5.2 (c (n "refined_type") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 2)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "084kravgca488cj2wfqjqhd8gyipq1scpv5rs6pl08w1dw5jk685")))

(define-public crate-refined_type-0.5.3 (c (n "refined_type") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 2)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "1gpyxzg9wc3iiyj33scs1mjmzvj6apiijwrrwajazlqd0nfgrwr7")))

(define-public crate-refined_type-0.5.4 (c (n "refined_type") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 2)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "0c17fi58h0fdfr0a6ygliyqjwi0s6yagiyqsbrrxryv8rppnhc7i")))

