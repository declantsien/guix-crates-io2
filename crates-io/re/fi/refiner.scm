(define-module (crates-io re fi refiner) #:use-module (crates-io))

(define-public crate-refiner-0.1.1 (c (n "refiner") (v "0.1.1") (d (list (d (n "error_tools") (r "~0.4.0") (f (quote ("full"))) (k 0)) (d (n "meta_tools") (r "~0.5.0") (f (quote ("full"))) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "11xpddy9nkrx9mj4r43qvqac0nfaf3qv09qz1f2z4d84aaghgalr") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

