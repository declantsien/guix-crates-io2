(define-module (crates-io re fi refinement) #:use-module (crates-io))

(define-public crate-refinement-0.1.0 (c (n "refinement") (v "0.1.0") (h "1qvmdb6cmp545l9lws9l1ndrf88s54rfqhh9dcmdqivyfvzlb623")))

(define-public crate-refinement-0.2.0 (c (n "refinement") (v "0.2.0") (h "125n4xs80ryrqdgj6qfxzjd2fq80nxdn3cp7bmhqcqp3fv8wnv7k")))

(define-public crate-refinement-0.3.0 (c (n "refinement") (v "0.3.0") (h "163c9ag0r1916xkamspxi9cqrbifbsg1mn511dr5sskxmpjw3mc0")))

(define-public crate-refinement-0.4.0 (c (n "refinement") (v "0.4.0") (h "0wp7gg19akfk0vvhvf0jpl24sz1ky5v2qw6prgxnv7zys7bb4f76")))

(define-public crate-refinement-0.5.0 (c (n "refinement") (v "0.5.0") (h "01l5jl8ipchrnqv5klmhhd6ik2r075ffdg936hvwl5i9cm4gdha1")))

