(define-module (crates-io re fi refined-float) #:use-module (crates-io))

(define-public crate-refined-float-0.1.0 (c (n "refined-float") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "0bdk2y9in5iv5rz7wgarzmm9by6ma81ksv2n4w7dk3yvff4izrld")))

(define-public crate-refined-float-0.1.1 (c (n "refined-float") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "03x57sdhgqqfcg371vkz7xw1bw2s256r2gc7hfs6lv234gxjpd5w") (f (quote (("std") ("default" "std"))))))

(define-public crate-refined-float-0.2.0 (c (n "refined-float") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "0i96sglv7vpp46ch3qiaadzvfdd83kllff7wzfbnfbmj816203n2") (f (quote (("std") ("default" "std" "approx"))))))

