(define-module (crates-io re fi refid) #:use-module (crates-io))

(define-public crate-refid-0.0.1 (c (n "refid") (v "0.0.1") (h "0m4cnlshw7knkpjdd694zzpirnxxkv9ps0vpr51mnd3zgwszvnic")))

(define-public crate-refid-0.0.2 (c (n "refid") (v "0.0.2") (h "1masrkbij17b0vizbzplkaw81fa01i88mpjnj5fnls0kxqy3sv2v") (y #t)))

(define-public crate-refid-0.0.3 (c (n "refid") (v "0.0.3") (h "1r2njjg7q6cp5l7glb0l4c296qzihvi1m5rnc5pd1y1x5i22mfh5")))

(define-public crate-refid-0.1.0 (c (n "refid") (v "0.1.0") (h "1bk84msy3km844xhdlf8d1ridbnvrkh7ysi2bf7ciwhjmgq7rwxp")))

(define-public crate-refid-0.2.0 (c (n "refid") (v "0.2.0") (h "1mz99ls1lmbraq9whggbb1rnxi012ryglbbdm6isfq7p8pdhmmwl")))

(define-public crate-refid-0.2.1 (c (n "refid") (v "0.2.1") (h "03x3sl0zsvqwxzrwlv429wrgvl3q9n1wh83v8fkj7mvjlwz55ayi")))

