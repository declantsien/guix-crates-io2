(define-module (crates-io re fs refset) #:use-module (crates-io))

(define-public crate-refset-0.1.0 (c (n "refset") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1dysax7vy702mdcfmsq47ncjnii0li5p6ycmyiz8g0l5snqvyw7q")))

(define-public crate-refset-0.1.1 (c (n "refset") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0lvlax91dpxynwzh3qi6zij41drlrb7wpcwpnnj75975rpa62dnr")))

(define-public crate-refset-0.2.0 (c (n "refset") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "smallmap") (r "^1.1.6") (f (quote ("serde"))) (o #t) (d #t) (k 0)))) (h "054vm8z1ksfpf9xk01gdqid29696zdkwy57zhib2xz68kc5pp3v6")))

