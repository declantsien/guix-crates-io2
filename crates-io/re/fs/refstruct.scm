(define-module (crates-io re fs refstruct) #:use-module (crates-io))

(define-public crate-refstruct-0.1.0 (c (n "refstruct") (v "0.1.0") (d (list (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1v4byjva3m30rl73d0s6fxsqbyj9szhaq04c68dizy7bzj247dxd")))

(define-public crate-refstruct-0.1.1 (c (n "refstruct") (v "0.1.1") (d (list (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1kz6jl45ra21f84fj3s6rsx1l7ixpmrq8kiap4d664z11f6gdwy9")))

