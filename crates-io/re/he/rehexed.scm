(define-module (crates-io re he rehexed) #:use-module (crates-io))

(define-public crate-rehexed-0.1.0 (c (n "rehexed") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "1krfxgij9jbsgdflpzk0lwvax2cwd0qzbrnf6d4dvl6izyzndc2b")))

(define-public crate-rehexed-0.1.1 (c (n "rehexed") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "03k6qnmsk1bwm2js0m3nnbdymljl3gzmxxg5ban3mi4p4w55h0vp")))

