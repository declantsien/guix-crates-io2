(define-module (crates-io re g- reg-analyzer-rs) #:use-module (crates-io))

(define-public crate-reg-analyzer-rs-0.1.0 (c (n "reg-analyzer-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0d5pfsjsn20lwwj3g05ihfvlvz4brr9zzd2ygyv78jr4vw7xfyj5")))

