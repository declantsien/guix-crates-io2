(define-module (crates-io re g- reg-watcher) #:use-module (crates-io))

(define-public crate-reg-watcher-0.1.0 (c (n "reg-watcher") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "widestring") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("synchapi" "winbase" "errhandlingapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5.1") (d #t) (k 0)))) (h "1i5f98iq551zzq3kqwpjc8qghvrfsrmf6vsrsr8sr0mrcak24k9z")))

(define-public crate-reg-watcher-0.1.1 (c (n "reg-watcher") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "widestring") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("synchapi" "winbase" "errhandlingapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5.1") (d #t) (k 0)))) (h "1s1zrs4fa2qpidqh0q1hh7c1h9b7wmxryv39jhl1iw9x3d23qv6c")))

