(define-module (crates-io re vs revshell) #:use-module (crates-io))

(define-public crate-revshell-1.0.0 (c (n "revshell") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1bpkv53i75h7q9w3shik7aaxl4sahmvnk13hjmfr9izvyab5kapp")))

