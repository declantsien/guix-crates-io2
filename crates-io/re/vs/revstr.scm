(define-module (crates-io re vs revstr) #:use-module (crates-io))

(define-public crate-revstr-1.0.0 (c (n "revstr") (v "1.0.0") (h "0v2v456fgkc6ildrd568nmmyyr9mm8pmc56xhhcn7k3d7mjq85vi")))

(define-public crate-revstr-1.0.1 (c (n "revstr") (v "1.0.1") (h "088323qznzllv37hcrxailbrzq602gkq323qnbgk1ksckb3hdp56")))

(define-public crate-revstr-1.0.2 (c (n "revstr") (v "1.0.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "05z47drwk207xjalifkng67a368nk7059zyzpydy9fscviph8g9m")))

