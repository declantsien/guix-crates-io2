(define-module (crates-io re rc rercon) #:use-module (crates-io))

(define-public crate-rercon-0.1.0 (c (n "rercon") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0qvp8gxifw5m0x1q5gmd7wp87rwcvi42pn8jds5w3nqimm6qdjhf") (y #t)))

(define-public crate-rercon-0.1.1 (c (n "rercon") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0pi8sgwzb52c28ha3565jk6k4z3h5n1317j2j80h3z9ncz4966xd")))

(define-public crate-rercon-1.0.0 (c (n "rercon") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "18cj55cf4rcs4slflqbcmx2mwl7q3arpzdx3zj34x3cn5da650nz")))

(define-public crate-rercon-1.1.0 (c (n "rercon") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "190kmcg81cc7wdqdvwdcnliq5wlbwszmfy7ypkq3k8adm289hhd9") (y #t)))

(define-public crate-rercon-1.0.1 (c (n "rercon") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "06pc1kvyp32mwbwmcnp0bnzq4zm74a47vb4irzx7pxb6y8bgjrai")))

(define-public crate-rercon-1.0.2 (c (n "rercon") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0mi9r5hzajqnrm06zag6gmh51cqvilnzwcxzfs6srfhfp480lq28")))

(define-public crate-rercon-1.1.1 (c (n "rercon") (v "1.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0l6hvmfirhd84bndjk2k6791j6zs0ncj68adgjcyafxdpa7z3zin")))

(define-public crate-rercon-1.2.0 (c (n "rercon") (v "1.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("io-util" "time" "macros" "net" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "08rbyvidc4qg3s90jclp0lvvk4zy3ynlw41g7rlf3pbd7y7b1wb9") (f (quote (("reconnection" "tokio/sync") ("default" "reconnection"))))))

