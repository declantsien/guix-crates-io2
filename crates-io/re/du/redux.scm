(define-module (crates-io re du redux) #:use-module (crates-io))

(define-public crate-redux-0.1.0 (c (n "redux") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1bs43kfldg3259fbyxsrqkyixmd63m40541s5darfl4ciisn4cs1")))

(define-public crate-redux-0.2.0 (c (n "redux") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ilphsf6pmmdbrcwa40prp7yzjdrccksb4q7wlchn82b5l4pr0cx")))

