(define-module (crates-io re du reduct-macros) #:use-module (crates-io))

(define-public crate-reduct-macros-1.6.0 (c (n "reduct-macros") (v "1.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "1phg1fvwf19hw1khsw3ymzdwg16vxx3lr0ki23kir69xfv6x6yzs")))

(define-public crate-reduct-macros-1.6.1 (c (n "reduct-macros") (v "1.6.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xwnmmah5x22brqp6i6asfzm2f5kcghbd07vgh002w8vk6sycpi1") (r "1.61.0")))

(define-public crate-reduct-macros-1.6.2 (c (n "reduct-macros") (v "1.6.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cikr312r450cwg2xks51ch11r3z3q2k7py095b4m8hza7lz4z0r") (r "1.61.0")))

(define-public crate-reduct-macros-1.7.0 (c (n "reduct-macros") (v "1.7.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "03965z625z6zfpv2la79g115yf2vvfpq68w0sj0qfbmn0nm6bass") (r "1.70.0")))

(define-public crate-reduct-macros-1.7.1 (c (n "reduct-macros") (v "1.7.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "177ch4gli0408iim88hw2ybmmc8nw43qwl1cix7kiscb3j4kn23j") (r "1.70.0")))

(define-public crate-reduct-macros-1.7.2 (c (n "reduct-macros") (v "1.7.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gsfl2197bdb7jf7vszws23ga9scdsq848zbn8bfry7d8zzvkaf8") (r "1.70.0")))

(define-public crate-reduct-macros-1.7.3 (c (n "reduct-macros") (v "1.7.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xv05xvkyjx8jlw6555z7ggfhyzx2cygq9vkhpbsk1pgxsi8a82z") (r "1.70.0")))

(define-public crate-reduct-macros-1.8.0 (c (n "reduct-macros") (v "1.8.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xckpdv4n9r5j4iq23zvc2wr561dysg9v1cxkhzsacc3vx528iif") (r "1.75.0")))

(define-public crate-reduct-macros-1.8.1 (c (n "reduct-macros") (v "1.8.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("derive"))) (d #t) (k 0)))) (h "12dn7d9rr5wpi9cas91b315a7gm303iby8mkrd0xdxv5vc8mwvmc") (r "1.75.0")))

(define-public crate-reduct-macros-1.8.2 (c (n "reduct-macros") (v "1.8.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("derive"))) (d #t) (k 0)))) (h "15dm3y73vxx5cwi7bq26y96qz9c7gzakb93d4h7niszvjy5a6faf") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.0 (c (n "reduct-macros") (v "1.9.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m1h942w58b1qcgikgh1ydj1l155clbghij6g2plsx25h8y5ywp0") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.1 (c (n "reduct-macros") (v "1.9.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qbygzf869q19ls5vr5bm2h4ll9129j9wmyz8br782l5prh0fc7x") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.2 (c (n "reduct-macros") (v "1.9.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "14mkmcr40khyh4iav125jk6vm7pk6595m19p1dmp89bh12g3sf8h") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.3 (c (n "reduct-macros") (v "1.9.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "162shmljnpq0g0d6jhnkv4lx952hqfnaq1da1j8s3zwph83lrxcb") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.4 (c (n "reduct-macros") (v "1.9.4") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xiaab3z56m9l6iilay2pcf0xll07ybb67l8z7mpi7kd3bfxk3ck") (r "1.75.0")))

(define-public crate-reduct-macros-1.9.5 (c (n "reduct-macros") (v "1.9.5") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("derive"))) (d #t) (k 0)))) (h "1al9d7kn7d7gpq3lw8f77q6fxync26b50fdkg9bi3s5bizlv4am5") (r "1.75.0")))

