(define-module (crates-io re du reduce) #:use-module (crates-io))

(define-public crate-reduce-0.1.0 (c (n "reduce") (v "0.1.0") (h "0izg03b08mphm2nhhhkwk86y12pgzlqnk2pkwlyrcg4si0ai0jm8")))

(define-public crate-reduce-0.1.1 (c (n "reduce") (v "0.1.1") (h "12dxxj4blzahc5dbgm79d9b2q9vb641184csjbbz94aj84bvfxsz")))

(define-public crate-reduce-0.1.2 (c (n "reduce") (v "0.1.2") (h "0g1zkanp4a0jb7cpyn1s29xhik4qiy7mbfkbfiljlbxhd1bgmcbm")))

(define-public crate-reduce-0.1.3 (c (n "reduce") (v "0.1.3") (h "0ha3ahh270fq7a5gxjrgpyqpqfl7nzcs466b972ch7wwnyg59j54")))

(define-public crate-reduce-0.1.4 (c (n "reduce") (v "0.1.4") (h "1mrmicr43hx3s9556zbzg0lys4nmsw0vwznd52imxhcanr3xrlhn")))

(define-public crate-reduce-0.1.5+deprecated (c (n "reduce") (v "0.1.5+deprecated") (h "1k9f1ag8wb7vsk6zdbzzp8qim9l70hc5l2i4vk69cjmwbwkprzzy")))

