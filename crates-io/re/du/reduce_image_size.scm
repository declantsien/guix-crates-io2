(define-module (crates-io re du reduce_image_size) #:use-module (crates-io))

(define-public crate-reduce_image_size-0.1.0 (c (n "reduce_image_size") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fast_image_resize") (r "^2.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "oxipng") (r "^9.0") (f (quote ("parallel"))) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "turbojpeg") (r "^0.5") (f (quote ("image"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zf2pqis9dgq7zq0sm1zabhdncf4daj8lnj4fws482k8j975xpri")))

(define-public crate-reduce_image_size-0.2.0 (c (n "reduce_image_size") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fast_image_resize") (r "^2.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "oxipng") (r "^9.0") (f (quote ("parallel"))) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "turbojpeg") (r "^0.5") (f (quote ("image"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ickg3d2wx085hx811cv5llmnfxl8462pf9gc0ykacbva3p078hk")))

