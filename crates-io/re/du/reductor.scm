(define-module (crates-io re du reductor) #:use-module (crates-io))

(define-public crate-reductor-0.0.1-alpha.1 (c (n "reductor") (v "0.0.1-alpha.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0y9bks80r45g8pggxqsfsxhirzfmh0absqxcmrd5y98jlp09ccp7")))

(define-public crate-reductor-0.0.1 (c (n "reductor") (v "0.0.1") (h "1w28c55l7cl1a60hmc60imqcj0mggns75vgx7hkiycql5dy2fqss")))

(define-public crate-reductor-0.0.2 (c (n "reductor") (v "0.0.2") (h "00jx5x30xhjwqas85rwk7rpycgpmivx956yszb0av5slq3279g3r")))

(define-public crate-reductor-0.0.3 (c (n "reductor") (v "0.0.3") (h "0mvbcljz4952352lyqsh3jxc0rspqac8sa650n31f867kmpz74hp")))

(define-public crate-reductor-0.0.4 (c (n "reductor") (v "0.0.4") (h "0v8l214fyir7x9cmavh2qa6gl5247asyzs4dq5mlcbawdz9g8gj7")))

(define-public crate-reductor-0.0.5 (c (n "reductor") (v "0.0.5") (h "0z346rk6whqlvjf4hykkjyn27fjr9bmir25bgbjc22p6h7k0hln7")))

(define-public crate-reductor-0.0.6 (c (n "reductor") (v "0.0.6") (h "010x6ijs4im07g34dc6wy6lan4c3cvw6yqmx54v69rrjbl9zqnd9")))

(define-public crate-reductor-0.0.7 (c (n "reductor") (v "0.0.7") (h "1vj059r22lnglx6p2y42smgb6jfihy4pwphmcwllx6djhxcdh0ib")))

(define-public crate-reductor-0.0.8 (c (n "reductor") (v "0.0.8") (h "007hn5ppwn6pv8mkhjxa7wa2ihy9276i8pdawvbgf62y0ymykhhp")))

(define-public crate-reductor-0.0.9 (c (n "reductor") (v "0.0.9") (h "1z2a4dz1h55b6jy3mayxig9i1yxdizjynligmbjlw83r84x2vg8r")))

(define-public crate-reductor-0.0.10 (c (n "reductor") (v "0.0.10") (h "0mbnspc8rsldw9yw03wman0xp4qzgr6d5sqjd1wkk2kww3rb5n09")))

(define-public crate-reductor-0.0.11 (c (n "reductor") (v "0.0.11") (h "17alqprvw1wia6ix36k7x7zi9mmmvwrhpmr5rw6fyfj0nszy1nvd")))

(define-public crate-reductor-0.0.12 (c (n "reductor") (v "0.0.12") (h "11hhyhm5vyysf6gm76b43zyyrjdl7c65glm0r1c5bn6c7ykd2m6w")))

