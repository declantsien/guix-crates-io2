(define-module (crates-io re du reduxr) #:use-module (crates-io))

(define-public crate-reduxr-0.1.0 (c (n "reduxr") (v "0.1.0") (h "0s87xbszxjlyq6gmm3094rl9s09v1f04qinr8bq13bpc0n18lkaj") (y #t)))

(define-public crate-reduxr-0.1.1 (c (n "reduxr") (v "0.1.1") (h "048p9hipk8lnxcj9fd8y9n8jbv2n40qbv7bxw7laybps9x9sq3la") (y #t)))

(define-public crate-reduxr-0.1.3 (c (n "reduxr") (v "0.1.3") (h "1q6r01cm5jminhw463sh9p71c7dn06025ihwam86v9bp8h7wg5dm") (y #t)))

(define-public crate-reduxr-0.1.4 (c (n "reduxr") (v "0.1.4") (h "1qaqp6522zc8rch0vhdddh6cl2fss4xvs954334za7az5rdyh2km") (y #t)))

(define-public crate-reduxr-0.1.5 (c (n "reduxr") (v "0.1.5") (h "0ga2y863ycsl71xnsra3f2b9894s793xir2j8mn9a84c3cghmyyw") (y #t)))

(define-public crate-reduxr-0.2.2 (c (n "reduxr") (v "0.2.2") (h "1bq4qma6011499d8nawpz013gg613gs393swfw0m3vnjfm6jz3pm")))

