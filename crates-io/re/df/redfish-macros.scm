(define-module (crates-io re df redfish-macros) #:use-module (crates-io))

(define-public crate-redfish-macros-0.1.0 (c (n "redfish-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1nz26mjdh4nrsq7c0ycpgycajr7b8z4ar888pk9jykx9d3f9blya")))

(define-public crate-redfish-macros-0.1.1 (c (n "redfish-macros") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0bdpfvdqh9gncpz7k6iwff9p4fl4z9k4dqh7571y2kr2nk3498vz")))

(define-public crate-redfish-macros-0.3.1 (c (n "redfish-macros") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0z643ak668fcc4zz4xdnqan0nifgflslml0siir85dh01b5rrxap")))

