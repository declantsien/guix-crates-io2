(define-module (crates-io re df redfish-axum) #:use-module (crates-io))

(define-public crate-redfish-axum-0.3.1 (c (n "redfish-axum") (v "0.3.1") (d (list (d (n "axum") (r "^0.6.18") (k 0)) (d (n "redfish-codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "redfish-core") (r "^0.3.1") (d #t) (k 0)))) (h "0p4ffb452fqq0rnxv9m3jfprgidfn36yrg77hw5k08drsls9x4bi")))

