(define-module (crates-io re df redfish-core) #:use-module (crates-io))

(define-public crate-redfish-core-0.3.1 (c (n "redfish-core") (v "0.3.1") (d (list (d (n "axum") (r "^0.6.18") (f (quote ("json"))) (o #t) (k 0)) (d (n "redfish-codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1341lxmkvgi71bdsi7sb438xizm78fjd8s02vwiyx063ki2f0znx") (f (quote (("default" "axum" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("axum" "dep:axum"))))))

