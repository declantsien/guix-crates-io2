(define-module (crates-io re df redfa) #:use-module (crates-io))

(define-public crate-redfa-0.0.1 (c (n "redfa") (v "0.0.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "10wq5wdx1vfhdh96pwb91ymnayxkpkyq0w6g4z6dji6bjam5i7x4") (f (quote (("unstable"))))))

(define-public crate-redfa-0.0.2 (c (n "redfa") (v "0.0.2") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0822ikq6grx642cviipyvya4c9bhf6ihbp6y3h3b0pwzriqjgk19") (f (quote (("unstable"))))))

(define-public crate-redfa-0.0.3 (c (n "redfa") (v "0.0.3") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)))) (h "0zl5lzmfvf6jkv2qmvx8x05kj09g3p28l31k1f1375722bpb8hds")))

