(define-module (crates-io re t_ ret_ty_attr) #:use-module (crates-io))

(define-public crate-ret_ty_attr-0.1.0 (c (n "ret_ty_attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1w87dfwf70fh8i17ishb735jywksd7f6j0nmf6l6y1acm75wxfdp")))

(define-public crate-ret_ty_attr-0.1.1 (c (n "ret_ty_attr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1s1achdjzy7zrmb0c0hvc9l8y6gbwynxmhia6z07z81jfb149jbp")))

(define-public crate-ret_ty_attr-0.1.2 (c (n "ret_ty_attr") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1rzqww800kwqck09pic3bdhssdyaz85x0si1wlsxqwx0nmd2yphk")))

