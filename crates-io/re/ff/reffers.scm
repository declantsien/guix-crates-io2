(define-module (crates-io re ff reffers) #:use-module (crates-io))

(define-public crate-reffers-0.2.0 (c (n "reffers") (v "0.2.0") (h "06av9lwpdmqnz7jdl6acz4hvqf43v6gb2f0152nvh202d4hzdlqx")))

(define-public crate-reffers-0.3.0 (c (n "reffers") (v "0.3.0") (h "0w5jjcmr2r9xzfzf75q75pmyb5q8aknrn6xnjlwd4i5wkz38s59k")))

(define-public crate-reffers-0.4.0 (c (n "reffers") (v "0.4.0") (h "18nrjwpafqwanwyc50rlx5366avc153a7gm4cz9lk7xrnyw06zyp")))

(define-public crate-reffers-0.4.1 (c (n "reffers") (v "0.4.1") (h "1qc0wlv5y6ckxhs203rm0psbpk63g6bbf0syp7wc7mwxfh0m95hh")))

(define-public crate-reffers-0.4.2 (c (n "reffers") (v "0.4.2") (h "0jnq87ikgikg79rzi8i1n582978y9kmzm2f9hy9p5ga6fralj9hi")))

(define-public crate-reffers-0.5.0 (c (n "reffers") (v "0.5.0") (d (list (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "1jwm59z93iwl1hvwi9bw89z0gjzkwc3x5sv2ak98jy6if384mjfs")))

(define-public crate-reffers-0.5.1 (c (n "reffers") (v "0.5.1") (d (list (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "0cpwlsamf6ckrjr7i5jwic5r6qyrw737f72zp1nbb623aif7cfqc")))

(define-public crate-reffers-0.6.0 (c (n "reffers") (v "0.6.0") (d (list (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "0xvvp9jhrx53w7nf1q617j2cmw8q8fg3mii6b51w99dd5a7qzc10")))

(define-public crate-reffers-0.6.1 (c (n "reffers") (v "0.6.1") (d (list (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "0v9mlvjnviil5rdwcxcpm7kds2crybqcfzi83v4vq0zfx1ix76xf")))

(define-public crate-reffers-0.7.0 (c (n "reffers") (v "0.7.0") (d (list (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "0j6jzqx6dklcimlgvhq837n5pywd5v6aqds62wg9iw16a6f6ab8n")))

