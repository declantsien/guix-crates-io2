(define-module (crates-io re wr rewrite) #:use-module (crates-io))

(define-public crate-rewrite-0.1.0 (c (n "rewrite") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "009d98cx6q975fwbz1z527r3bk0ps9xx5z49pkjibdrihfz6kdwl")))

(define-public crate-rewrite-0.2.0 (c (n "rewrite") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f5qh3m8wgb87b7361rh41cb0xrr73sr9cs1mysgl0wr75n6i0r8")))

(define-public crate-rewrite-1.0.0 (c (n "rewrite") (v "1.0.0") (h "1px2iy7birdf6s0w131kdaccqrmq3l90s45larxij1k29axg7yif")))

