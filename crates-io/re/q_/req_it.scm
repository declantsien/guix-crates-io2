(define-module (crates-io re q_ req_it) #:use-module (crates-io))

(define-public crate-req_it-0.1.0 (c (n "req_it") (v "0.1.0") (d (list (d (n "act_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "gtk_estate") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "1.0.*") (d #t) (k 0)) (d (n "pretty_goodness") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)))) (h "1iqnja6c82bsrwvmn36885yq8as85blf1glfdyi0069fqc5pbbpq")))

