(define-module (crates-io re tw retworkx) #:use-module (crates-io))

(define-public crate-retworkx-0.0.1 (c (n "retworkx") (v "0.0.1") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0rpsch2vycx22gzb9q1819ywwshsi8wdl1mz8psqinq6cxbzq4n7")))

(define-public crate-retworkx-0.0.2 (c (n "retworkx") (v "0.0.2") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0y64dn68gf9f2i2jhninzhfj3m4crxmbdcp32arxjb334dibpdhh")))

(define-public crate-retworkx-0.0.3 (c (n "retworkx") (v "0.0.3") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0g5svdwf5yp60wixzss2p9z6vhf0y0brfirzmf6b9gb1yx333gr8")))

(define-public crate-retworkx-0.0.5 (c (n "retworkx") (v "0.0.5") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0chyba4d2bji1hb6kmhx9cpa4wvchzdbxhzgmhy07fkgg1xvd9aw")))

(define-public crate-retworkx-0.0.6 (c (n "retworkx") (v "0.0.6") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0jw92548pl82l6b6zi4mghb3x54hyvakrhq5zr16n2ybapaacf0x")))

(define-public crate-retworkx-0.0.7 (c (n "retworkx") (v "0.0.7") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "101nn49v1nii2b5q5khxxy9nxl9ffcsxh8mm11hpls8ydv57ciz8")))

(define-public crate-retworkx-0.0.8 (c (n "retworkx") (v "0.0.8") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "06qndawb2kfl8l1phrngirbx0byas17cfmdj0rwmk6wcz0s8sd7x")))

(define-public crate-retworkx-0.0.9 (c (n "retworkx") (v "0.0.9") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0y8514p1nhvzmnsi8r3v5wk89q1jb66qcviz4s4mxqgl0ncdrnbs")))

(define-public crate-retworkx-0.1.1 (c (n "retworkx") (v "0.1.1") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1fa2s3r576vnngdl5pp57hd0889pb1xinbr4wj1f224kf99d1zl2")))

(define-public crate-retworkx-0.2.0 (c (n "retworkx") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "13drxvcprxnskqf8wpzb4zkabvps633ajrihw2br64jwh2vikdhc")))

(define-public crate-retworkx-0.3.0 (c (n "retworkx") (v "0.3.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0yyqz2yw6f1acfzjdf71f3nmn7kgpz4gz6zzvpjdbyp06zvkiqni")))

(define-public crate-retworkx-0.3.1 (c (n "retworkx") (v "0.3.1") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0s16cvim7qzb95k6bnk9phd6ccl4nkkc4hrwwjgzzlzh998lnqc5")))

(define-public crate-retworkx-0.3.2 (c (n "retworkx") (v "0.3.2") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1i9s93dky3y2b44m2aackf2mjyjjb6s6p4q73pyrlriyvdcc5rh6")))

(define-public crate-retworkx-0.3.3 (c (n "retworkx") (v "0.3.3") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.5") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1qksldwwq6bkvdwfgwpj1fwi0kmzx4brgfv9v4h7q9xvd69misrz")))

(define-public crate-retworkx-0.3.4 (c (n "retworkx") (v "0.3.4") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1limn6mf1xskpwqd0srylpfi4d0kp810hna83fi8ckkq3lw0i3a0")))

(define-public crate-retworkx-0.4.0 (c (n "retworkx") (v "0.4.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "numpy") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.11.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)))) (h "1fdici9kf520psv3fgy64kfwv82ksimargyp52528pgslk64125z")))

(define-public crate-retworkx-0.5.0 (c (n "retworkx") (v "0.5.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "numpy") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.11.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)))) (h "0l9llwi282vgz4nn85i0xgkwicgkmjbgx7qrffsrakjpxzf6s7l3")))

(define-public crate-retworkx-0.6.0 (c (n "retworkx") (v "0.6.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (f (quote ("extension-module" "hashbrown"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1b4vlmdg1q6avnxklsdbim5084m9cy4pzd424gvcsz6y19kli3v8")))

(define-public crate-retworkx-0.8.0 (c (n "retworkx") (v "0.8.0") (d (list (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "numpy") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (f (quote ("extension-module" "hashbrown"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "14hykz16q1djgimkifi49qsxpp6jy01mcj4wj4dcvsqqm1wwry9n")))

