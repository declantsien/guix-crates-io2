(define-module (crates-io re tw retworkx-core) #:use-module (crates-io))

(define-public crate-retworkx-core-0.11.0 (c (n "retworkx-core") (v "0.11.0") (d (list (d (n "ahash") (r "^0.7.6") (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0s2v1p5zn57xpr4dd38sr4b3r2iwfrxfwfff5filwxbcmg6wsfrm")))

