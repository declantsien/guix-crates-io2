(define-module (crates-io re mu remutex) #:use-module (crates-io))

(define-public crate-remutex-0.1.0 (c (n "remutex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0lha8gz589vqxx17w00bfkfh8yr9m3hj9hbwy0x2d99f0fb69hj3")))

(define-public crate-remutex-0.1.1 (c (n "remutex") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0didiri8v2ypckp1j78cf883j3pglrjpclk79nvpzcmk4hdrr3rg")))

