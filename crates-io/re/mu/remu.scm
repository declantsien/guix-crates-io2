(define-module (crates-io re mu remu) #:use-module (crates-io))

(define-public crate-remu-1.0.0 (c (n "remu") (v "1.0.0") (h "0d1jzfbapwyj0czmqk6kc9j0zfh9j1g48l899i3yb22alivm78r6")))

(define-public crate-remu-1.0.1 (c (n "remu") (v "1.0.1") (h "04gr7w7v0zfm78yzz20ms3axwm9hlzxf08qgfd8y0kbf1a6r67b8")))

