(define-module (crates-io re al realtor-rs) #:use-module (crates-io))

(define-public crate-realtor-rs-0.1.0 (c (n "realtor-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "17qrq62zvy9mfwzch5ba0i5z7xicmbna4dviip2idha7kxg7cryd")))

(define-public crate-realtor-rs-0.2.0 (c (n "realtor-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0prdnqviphfin21csib46bl13nqgyxxdflxl8crj6j7jwn3a9am5")))

(define-public crate-realtor-rs-0.3.0 (c (n "realtor-rs") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1fqxn1nsk9lpiwv0k57nldyc80c4rl25lbrb069nf59h58jvcz31")))

(define-public crate-realtor-rs-0.4.0 (c (n "realtor-rs") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0xypiwkkfczxm6wl6ss59i0pxxm27lh0svhrh3ap275qaraypmbd")))

(define-public crate-realtor-rs-0.5.0 (c (n "realtor-rs") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0slhiwvk0ywx04w4n68hj0mydx4v1zmzbi0nrq550jb29rpsyx6n")))

(define-public crate-realtor-rs-0.6.0 (c (n "realtor-rs") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0z8l9hyfw2kd0q4mlzqdy9ina7zmyfvzzy6ks6cm2q87yfq2symp")))

