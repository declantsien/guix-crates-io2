(define-module (crates-io re al realm_syscall) #:use-module (crates-io))

(define-public crate-realm_syscall-0.1.0 (c (n "realm_syscall") (v "0.1.0") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "0hb4fkyqvgdxhpf3sfbj1hqk2g4hg8rngpf2z0dwrpg5xf9zr88i")))

(define-public crate-realm_syscall-0.1.1 (c (n "realm_syscall") (v "0.1.1") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "1zmgk4x6qjia8b2xsnd33xghyy3ghshxw8mrkgrqw4glyjcz5grf")))

(define-public crate-realm_syscall-0.1.2 (c (n "realm_syscall") (v "0.1.2") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "0yyjfwn6qx8kan1yfahk17757gxhcm5drq8xrvgv0paymnzqmchf")))

(define-public crate-realm_syscall-0.1.3 (c (n "realm_syscall") (v "0.1.3") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "1b0xjqfg5m3bjzgkpix9qpzs2cll36lmf2bi8w1whsfjkq0mrwby")))

(define-public crate-realm_syscall-0.1.4 (c (n "realm_syscall") (v "0.1.4") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "1yv87738w90qf34jq2j0935d3s2hl0qn835q32zflr7dfcbv9kka")))

(define-public crate-realm_syscall-0.1.5 (c (n "realm_syscall") (v "0.1.5") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "1f093bmnifwxm555lrip1q881i7hlwwyz1fb8vl3yng476ylks7a")))

(define-public crate-realm_syscall-0.1.6 (c (n "realm_syscall") (v "0.1.6") (d (list (d (n "daemonize") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "16kw9blyz72d2p59lsazy9a13xahjbwi3s86d6almvsmxbbly75x")))

(define-public crate-realm_syscall-0.1.7 (c (n "realm_syscall") (v "0.1.7") (d (list (d (n "daemonize") (r "^0.5") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)))) (h "0s4qdw7pdrm7qkjj0xnksyjb0qs2f2w87m5gr7qm49b7na9zkpwf")))

(define-public crate-realm_syscall-0.1.8 (c (n "realm_syscall") (v "0.1.8") (d (list (d (n "daemonize") (r "^0.5") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)))) (h "0z6i0g3mfzf767p2m02fhyi4ngfnxg6l7sx9dlhy8xpv84asd6p5")))

