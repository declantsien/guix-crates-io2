(define-module (crates-io re al real-async-trait) #:use-module (crates-io))

(define-public crate-real-async-trait-0.0.0 (c (n "real-async-trait") (v "0.0.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 2)))) (h "1haskm2bk0dgf12ajv4kilpl3m5x4113snl20rwf44kqhpwahpkz")))

(define-public crate-real-async-trait-0.0.1 (c (n "real-async-trait") (v "0.0.1") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 2)))) (h "1nlc9jh8ibh18k403c2ibmdn8g6prlnlf4c26wfvmbyy28qnp65v")))

(define-public crate-real-async-trait-0.0.2 (c (n "real-async-trait") (v "0.0.2") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 2)))) (h "14icxms9dx1124mgy91d8ysfkw7wb1lfr3wr5bakk7nldqa72whl")))

