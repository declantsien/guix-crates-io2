(define-module (crates-io re al real_parent) #:use-module (crates-io))

(define-public crate-real_parent-0.1.0 (c (n "real_parent") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "02g9sbmqgl71kvfj3q1f9xniniiz1h70d3l2r209plyaqgy458p4")))

(define-public crate-real_parent-0.2.0 (c (n "real_parent") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "0hq1ks9irbjykvxh00b0rqfmdfx1ncxy4bn9av731bh0as84if81")))

