(define-module (crates-io re al realtime) #:use-module (crates-io))

(define-public crate-realtime-0.1.0 (c (n "realtime") (v "0.1.0") (h "0sh2q4brsw4m4k3i53hfbaxjzihzdcnnnbrybbgcrq20ka8b143s")))

(define-public crate-realtime-0.1.1 (c (n "realtime") (v "0.1.1") (d (list (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "rt-rtai") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rt-thread") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rt-xenomai") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1lhr48ldc8hfmv3gs9xkwxw81i9dzg9p4j0a6nq6ir7irygwxphr") (f (quote (("xenomai" "rt-xenomai") ("rtai" "rt-rtai") ("preempt" "rt-thread"))))))

