(define-module (crates-io re al real-memory) #:use-module (crates-io))

(define-public crate-real-memory-0.1.0 (c (n "real-memory") (v "0.1.0") (h "1834n8czvpwlhca8r0y6zsmdq575wvpx2rdjrsmfr7cyxwyycwl3")))

(define-public crate-real-memory-0.1.1 (c (n "real-memory") (v "0.1.1") (h "1igppc0flk6mzlszrbivckbhsf9vvnvjslc009xndiwn2b37z6dx")))

(define-public crate-real-memory-0.1.2 (c (n "real-memory") (v "0.1.2") (h "147azhcafgw0hkm3ydvj9svid5il6jzj5wlixv9jysn2iwc0l4yc")))

