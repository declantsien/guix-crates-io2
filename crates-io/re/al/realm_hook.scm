(define-module (crates-io re al realm_hook) #:use-module (crates-io))

(define-public crate-realm_hook-0.1.0 (c (n "realm_hook") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0jszs2b2hgrnp8zripkq5pmn7d2ks7rrxmlyr18fggsfg7app92r")))

(define-public crate-realm_hook-0.1.1 (c (n "realm_hook") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0565j5dx7wj7q9y3k50pfvhlghpd8q1vgbizhndmx2qhd60dh2vq")))

(define-public crate-realm_hook-0.1.2 (c (n "realm_hook") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0rn26m08m8g0jnq0j1aqvcwwv5p4ga6h59fckij3zmzl1yq9q9hg")))

(define-public crate-realm_hook-0.1.3 (c (n "realm_hook") (v "0.1.3") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0fwsxfgbdm4nsjljbw301swsrig6r46s00di0bdr9kjyvxzld94z")))

(define-public crate-realm_hook-0.1.4 (c (n "realm_hook") (v "0.1.4") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0150079q00ysgxfy05q1zrgacsr3v8lw23w346sjkklrfv9mi5b4")))

