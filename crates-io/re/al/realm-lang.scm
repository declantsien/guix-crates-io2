(define-module (crates-io re al realm-lang) #:use-module (crates-io))

(define-public crate-realm-lang-0.1.0 (c (n "realm-lang") (v "0.1.0") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ac7sl6dxcms4hfy3hm70iqdb6cw3x29hmz1ryvh9a98z2mpd8m6")))

