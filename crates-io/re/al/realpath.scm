(define-module (crates-io re al realpath) #:use-module (crates-io))

(define-public crate-realpath-0.1.0 (c (n "realpath") (v "0.1.0") (h "1ll7w7pllq3iyj1kpifrfs48b5q449lx28hddnyahdqpcdk00a6a")))

(define-public crate-realpath-0.1.1 (c (n "realpath") (v "0.1.1") (h "1pf14sbmjl4n8x0vfax7r4225y7r7zlrzp3f8y22rlslb8x1wlbq")))

