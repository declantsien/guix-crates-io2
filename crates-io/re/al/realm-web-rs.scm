(define-module (crates-io re al realm-web-rs) #:use-module (crates-io))

(define-public crate-realm-web-rs-0.1.0 (c (n "realm-web-rs") (v "0.1.0") (d (list (d (n "bson") (r "^2.5.0") (d #t) (k 0)) (d (n "builder-pattern") (r "^0.4.2") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "stream" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0np418mv28dvl8k7zps2dbl47jgv356n82kjg04nqr61bpxqrlgz")))

