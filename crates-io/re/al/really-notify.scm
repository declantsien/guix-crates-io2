(define-module (crates-io re al really-notify) #:use-module (crates-io))

(define-public crate-really-notify-0.1.0 (c (n "really-notify") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "bitmask-enum") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^6.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xpqkp45kjwpnbc1bvdfbdcxhcd601mwihw6a2bcc0c4ia3vn3fc") (f (quote (("inotify" "libc" "bitmask-enum" "async-stream") ("default" "inotify")))) (s 2) (e (quote (("notify" "dep:notify"))))))

