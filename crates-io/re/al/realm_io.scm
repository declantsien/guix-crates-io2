(define-module (crates-io re al realm_io) #:use-module (crates-io))

(define-public crate-realm_io-0.1.0 (c (n "realm_io") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0afwrjv6wkv0x07d3bxa0b15sb2njjg1f5cwg66pmjqb5cfsz98n")))

(define-public crate-realm_io-0.2.0 (c (n "realm_io") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0112pz6b8wjsw85frskaz9m8wba7d0kl980rvy8d6pmm4i2jchlp")))

(define-public crate-realm_io-0.2.1 (c (n "realm_io") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1gbnw6f7hdsk7152vn2m4vp056p20rwcgqap2wbrrkidz6dawsi6")))

(define-public crate-realm_io-0.2.2 (c (n "realm_io") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1z3cis4n2nmzck446xghj3rl7sa374v44a6m4cbfaaljp8vjpc26")))

(define-public crate-realm_io-0.2.3 (c (n "realm_io") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1yafsy571m1c67wzslha9csliwzpxdwhwy0fxm84n9c65zk7bmql")))

(define-public crate-realm_io-0.2.4 (c (n "realm_io") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1jp0zcwrpxn9lcrwzlvyq4hv8lniqbzl6yl901x5h14fvcb9r6ap")))

(define-public crate-realm_io-0.2.5 (c (n "realm_io") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1cpa8k7lkjcq1jjyk7hbbqb6mza5ab7v9aq73xp6h2bkz6nijj91")))

(define-public crate-realm_io-0.2.6 (c (n "realm_io") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "058j82ss77n3d7z9zch8bbwkrnm6kzky6k4l9kq7dw6p901a0743") (f (quote (("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.2.7 (c (n "realm_io") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1n5aimbbiwqhr1b2z72jjfhi02xjcvwaw8r0kjmiq48b0kxi9652") (f (quote (("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.3.0 (c (n "realm_io") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0dg503jnawlx1pwjwl6260pqrkd417bqzlpji29nzdihf20c4ipw") (f (quote (("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.3.1 (c (n "realm_io") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1labx92dl13klv3l51ihnjhcg0nj3kkz3by6zs9jg6ikc53wpd0s") (f (quote (("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.3.2 (c (n "realm_io") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "06q89nnzci334hd9gvj3ksd4c4fkcg78npkdni46ryar9p40ikva") (f (quote (("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.3.4 (c (n "realm_io") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0mp28hjilp7gk6a2gv2k8mhi2mviqcin1xn7k1fxc21pivb32pkn") (f (quote (("statistic") ("peek") ("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.3.5 (c (n "realm_io") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1fdih19ym0di4pnmaaxapl2k5nyp73mqx2j3qp2m5hzjlx9r1hfv") (f (quote (("statistic") ("peek") ("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.4.0 (c (n "realm_io") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0sdgvfxjb25xgzlnl4pnkbyagmbpr92qidi0mj9y71drj1h116s1") (f (quote (("statistic") ("peek") ("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.5.0 (c (n "realm_io") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "122jlv5aa2k6lm907bm027jxbcm6wwljznmm1w0vhhzdwbxgsfvd") (f (quote (("statistic") ("peek") ("default") ("brutal-shutdown"))))))

(define-public crate-realm_io-0.5.1 (c (n "realm_io") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("net"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0haa3rfvrndnqg0xgfx7n4f50628na0n8j91inh67fr0i5aaqn15") (f (quote (("statistic") ("peek") ("default") ("brutal-shutdown"))))))

