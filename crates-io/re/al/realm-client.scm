(define-module (crates-io re al realm-client) #:use-module (crates-io))

(define-public crate-realm-client-0.1.0 (c (n "realm-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sorted-json") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0a1q5wdy9wkv8ybhjfp3zsdppi0mjaf0fr7zfpdcfrhal5k4r7kh")))

