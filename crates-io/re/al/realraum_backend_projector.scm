(define-module (crates-io re al realraum_backend_projector) #:use-module (crates-io))

(define-public crate-realraum_backend_projector-0.0.0 (c (n "realraum_backend_projector") (v "0.0.0") (d (list (d (n "axum") (r "^0.6.20") (f (quote ("http2" "ws"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.4.3") (f (quote ("fs" "cors" "compression-full"))) (d #t) (k 0)))) (h "0a2iqmdpj0pv7ia87a5xhgq33871pqsdgl3iwpkf8l8ss69209gc")))

