(define-module (crates-io re al real-proc) #:use-module (crates-io))

(define-public crate-real-proc-0.0.1 (c (n "real-proc") (v "0.0.1") (d (list (d (n "const-frac") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1n7iy1bqcd320rck8n82ap91skdkcjzygxb7l8sjgb2z9irpg0wj")))

(define-public crate-real-proc-0.0.2 (c (n "real-proc") (v "0.0.2") (d (list (d (n "const-frac") (r "^0.0") (f (quote ("tokenize"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1xrgf29mczjy12ic2ljxvydz8ac3j2rhmd7vpkbj33ilm7505yl5")))

(define-public crate-real-proc-0.0.3 (c (n "real-proc") (v "0.0.3") (d (list (d (n "const-frac") (r "^0.0") (f (quote ("tokenize"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "109wffp30lp0g8wv756dk1zw6vy5s9cvg6cffpmqa68k7whcdmzm")))

