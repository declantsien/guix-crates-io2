(define-module (crates-io re al realfft) #:use-module (crates-io))

(define-public crate-realfft-0.1.0 (c (n "realfft") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "0g6zgkddcq8bxhlz0l3hxks0vcgrfhkwvm8kilzcamg4ww1h78d1")))

(define-public crate-realfft-0.2.0 (c (n "realfft") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "17q2f68flfyk1ym8cizzrd4ml773ijxrz59002b8hz3axfrg79zv")))

(define-public crate-realfft-0.2.1 (c (n "realfft") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "10pvhkwy0v298m9186hl9js6bymas6b0i6yair4l1nc99pg1ay34")))

(define-public crate-realfft-0.3.0 (c (n "realfft") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustfft") (r "^4.0.0") (d #t) (k 0)))) (h "1kr8y44mahzmfp2cch463gsmji60l2x1nkb131ri669hvwv818r9")))

(define-public crate-realfft-0.4.0 (c (n "realfft") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustfft") (r "^5.0.0") (d #t) (k 0)))) (h "0zhhrcb9762pj95388r0hb9j781336k69q5bxsxqjkg510w0wmvq")))

(define-public crate-realfft-1.0.0 (c (n "realfft") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "rustfft") (r "^5.0.0") (d #t) (k 0)))) (h "1bp9qpz6vjib36j3z86iixq2mgk0hx4c5xb99qcwimpls7p92qjq")))

(define-public crate-realfft-1.1.0 (c (n "realfft") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "rustfft") (r "^5.0.1") (d #t) (k 0)))) (h "1kp0999sxra4id5bzg3bvswbi46c4148ixwvd4453j45x6lvi5za")))

(define-public crate-realfft-2.0.0 (c (n "realfft") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "0xmnl6jf68n1bn6g3in2vc9ybmiixl3v4kcm10ik2din6h1fhqdv")))

(define-public crate-realfft-2.0.1 (c (n "realfft") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "0hi3f6g3pw0is7r6rxxb0fbnaizd7ydbagzjc13n9hqxyf3mqsfp")))

(define-public crate-realfft-3.0.0 (c (n "realfft") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "0fh5553p6ry0g5z4j6hfp8ggiv8ichyvkb7fbnzy39sxwmpqffx8")))

(define-public crate-realfft-3.0.1 (c (n "realfft") (v "3.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "1f4igfys89db32rgj86qfvi0964kbb1ajjvl3wrxz3fnmczyna40")))

(define-public crate-realfft-3.0.2 (c (n "realfft") (v "3.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "13zjpvs4j5q42bqg2g5fnlrqgq35p2qvhs47m0nikn84xbrhq0rh")))

(define-public crate-realfft-3.1.0 (c (n "realfft") (v "3.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1k545v76p0dpya4kwwx2j7x1v3d574fw8xbpc14q1zxydrnyclih")))

(define-public crate-realfft-3.2.0 (c (n "realfft") (v "3.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1lp1yba6miiihg6qha7x6v6r5gv01hlxfj40lm527ln6y3lbimlk")))

(define-public crate-realfft-3.3.0 (c (n "realfft") (v "3.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0w854bh695cxhrng7jz3sip65hpgjq92amml8wsrd06xbiz9ygcm")))

