(define-module (crates-io re al realpath-ext) #:use-module (crates-io))

(define-public crate-realpath-ext-0.1.0 (c (n "realpath-ext") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)))) (h "1z5mbh6y6bha4fc12kamz3xb2zyz7gw1c8qb6fypyv5m7749k4yf") (f (quote (("std") ("default" "std"))))))

(define-public crate-realpath-ext-0.1.1 (c (n "realpath-ext") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "errno") (r "^0.2.8") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1wq54s0bx7w9g5y9al1gsfj3nm7pb039dsmf3avnyb7gxp3jfir5") (f (quote (("std") ("default" "std"))))))

(define-public crate-realpath-ext-0.1.2 (c (n "realpath-ext") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "errno") (r "^0.2.8") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "0gisjhiw5mvmczs2pqwwbkh3j8b6ril36dc33pqqa09k6wlf1z8i") (f (quote (("std") ("default" "std"))))))

(define-public crate-realpath-ext-0.1.3 (c (n "realpath-ext") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "errno") (r "^0.3.0") (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0bq9pmwag74q7k7d496zmsg6brqp1yqmz5pll122nlqd5a374bv9") (f (quote (("std") ("default" "std"))))))

