(define-module (crates-io re al realjump) #:use-module (crates-io))

(define-public crate-realjump-0.1.0 (c (n "realjump") (v "0.1.0") (h "0rbz57zgvj0rs16inqz5jaibqzr742gmlrb6fxh4l8cfvqf2sp7c")))

(define-public crate-realjump-0.2.0 (c (n "realjump") (v "0.2.0") (h "1bw94yc5p2q0jfijbm785xxjiygrqdzpzm3pwgbvrpvb6k4hh4mz")))

