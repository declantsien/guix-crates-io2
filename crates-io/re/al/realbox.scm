(define-module (crates-io re al realbox) #:use-module (crates-io))

(define-public crate-realbox-0.1.0 (c (n "realbox") (v "0.1.0") (h "19dn3d0506m3qjdlhqy01i1bfxx2avrm5x0cn0r0chpz931pz8z1")))

(define-public crate-realbox-0.1.1 (c (n "realbox") (v "0.1.1") (h "1y1d1xlnq9niqsghjj0hnpsya2pgx4qy2w5b0yiwy7qh6b2ax4p6")))

