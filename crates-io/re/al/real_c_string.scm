(define-module (crates-io re al real_c_string) #:use-module (crates-io))

(define-public crate-real_c_string-0.1.0 (c (n "real_c_string") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "18g4bsqmmb9s8f69jjcbk362kddhnzf5p8z4qsngfpbg26izlgxp")))

(define-public crate-real_c_string-0.1.1 (c (n "real_c_string") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "1p9j5hn1rnz8hvq25b8wcs1zgn91pvf1qjqgdfqwzf9qyw73wv4l")))

(define-public crate-real_c_string-1.0.0 (c (n "real_c_string") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pvdp8l0gdxmf82q92z3m6mrc3p969hmqdqgw2f4yp9l4sw0v3x5")))

(define-public crate-real_c_string-1.0.1 (c (n "real_c_string") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "04xgf62bncc0fp0n36g2x9q7x2hc6hsqyjmvhg7wjrbn6pqlq95f")))

