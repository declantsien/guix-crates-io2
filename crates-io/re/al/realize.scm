(define-module (crates-io re al realize) #:use-module (crates-io))

(define-public crate-realize-0.1.0 (c (n "realize") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.4.2") (d #t) (k 0)) (d (n "sha1") (r "^0.2.0") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("release_max_level_trace" "max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-journald") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "1hir06grj6b7hnxhrs8nwaq99g28zr3rq3nf60ldwdbbpgilcbmp")))

