(define-module (crates-io re al realia) #:use-module (crates-io))

(define-public crate-realia-0.1.0 (c (n "realia") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "1z2y4l16bj6h0522z1r31rgi5mjvm37mwl8x00l99c68sh0biwap")))

(define-public crate-realia-0.1.1 (c (n "realia") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "0d3q3a6z3wlgrwjcvy6laadm2zwm7m0v4zjf0g1xydf4lzp0672h")))

(define-public crate-realia-0.2.0 (c (n "realia") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "target-spec") (r "^0.4.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "074r9aq87h8hddx9agaqkmdpbmkng3dpablr80ii384m7425f8ih")))

