(define-module (crates-io re al real_float) #:use-module (crates-io))

(define-public crate-real_float-0.1.0 (c (n "real_float") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1qh6c9zjkcf000fqwgf2jr415ksqp4pkv505rnbs2qrsfg7a39q6") (f (quote (("strict"))))))

(define-public crate-real_float-0.1.1 (c (n "real_float") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0vc1k495k5fip4iskcdxzzbp584iisrw860y23ihna0600i6chs3") (f (quote (("strict"))))))

(define-public crate-real_float-0.2.0 (c (n "real_float") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bzffaydyfjfqvh3s8cqgv9fngsf5sr7b5prv7f8xf2iyr9gv46f") (f (quote (("strict")))) (s 2) (e (quote (("num" "dep:num-traits"))))))

(define-public crate-real_float-0.3.0 (c (n "real_float") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0bslnzgv57y9ga65lq5q9c3gnrgy0gzw586r33006xibyn731vix") (f (quote (("strict")))) (s 2) (e (quote (("num" "dep:num-traits"))))))

