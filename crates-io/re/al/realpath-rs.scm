(define-module (crates-io re al realpath-rs) #:use-module (crates-io))

(define-public crate-realpath-rs-0.1.0 (c (n "realpath-rs") (v "0.1.0") (d (list (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "0bas0f018ydfyib43cqgz0pivw1iivf91n1yniczza082ijva9mz")))

(define-public crate-realpath-rs-0.1.5 (c (n "realpath-rs") (v "0.1.5") (d (list (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "0pbxidpl2njns1zmz4b19j4xdchbsxp5h8wwphznzcnczmiq132n")))

(define-public crate-realpath-rs-0.1.6 (c (n "realpath-rs") (v "0.1.6") (d (list (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "0lpgzgm6wdxlin80qlafrchj7crfrrlnqgv0apnjkxqmqf2qr1p2")))

(define-public crate-realpath-rs-0.2.0 (c (n "realpath-rs") (v "0.2.0") (d (list (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "1ky5y5fy536zdvlz8dwzir5svxl7cd2b80b9g1pmrsgcv7601rpr")))

