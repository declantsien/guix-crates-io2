(define-module (crates-io re qi reqif-rs) #:use-module (crates-io))

(define-public crate-reqif-rs-0.1.0 (c (n "reqif-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "yaserde") (r "^0.10.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.10.0") (d #t) (k 0)))) (h "0ns9hhdywpx1x63z7laj7zzsix721km2zhs5agrrvh6hgq12kxkj") (y #t)))

(define-public crate-reqif-rs-0.1.1 (c (n "reqif-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "yaserde") (r "^0.10.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.10.0") (d #t) (k 0)))) (h "1x1ng2j6rikc18gn3m580mc85s5vfi2zdayhrvyn09nin9g3lla0")))

(define-public crate-reqif-rs-0.1.2 (c (n "reqif-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "yaserde") (r "^0.10.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.10.0") (d #t) (k 0)))) (h "1wfncra3zh3gaxmdljjwx3hwhgcrik2bds0jynjmrdd422bckncl")))

