(define-module (crates-io re nl renls) #:use-module (crates-io))

(define-public crate-renls-0.1.0 (c (n "renls") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lw7ghjc01gpzdng09yxs9sls51z24c0pnlqgdv879fqrq6bgq59")))

(define-public crate-renls-0.1.2 (c (n "renls") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "16d3dq6x9b4mas6znr0y5wmsbivr2n4s8vqj7cnrhx4myazg90v6")))

