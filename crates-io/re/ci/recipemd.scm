(define-module (crates-io re ci recipemd) #:use-module (crates-io))

(define-public crate-recipemd-0.1.0 (c (n "recipemd") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^3.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.9.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "17sia0cir0slqgnqpapvky7gp2d25crk9i67a9ngd831wc6l2csk") (f (quote (("tests" "diagnostics" "miette/fancy") ("diagnostics" "miette") ("default" "diagnostics"))))))

