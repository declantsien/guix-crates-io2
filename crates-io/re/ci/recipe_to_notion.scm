(define-module (crates-io re ci recipe_to_notion) #:use-module (crates-io))

(define-public crate-recipe_to_notion-0.1.0 (c (n "recipe_to_notion") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0hgfn8iym5f42jnlf1cg86rmp3prbrydx41hjdcil2m16gs1f50j")))

(define-public crate-recipe_to_notion-0.1.1 (c (n "recipe_to_notion") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0lc47f7jc0133a3fwm8394p1rii38wr48s10ym14zjg9am0aagn6")))

