(define-module (crates-io re ci recipe-reader) #:use-module (crates-io))

(define-public crate-recipe-reader-0.9.5 (c (n "recipe-reader") (v "0.9.5") (h "1r2zb2zbmfvw8c7jr1nsy66awkkcvgnqy03kazy0r7w6sn8mfskb")))

(define-public crate-recipe-reader-0.9.6 (c (n "recipe-reader") (v "0.9.6") (h "0i0qapq2qzy1kkfsw3q1gg11imlddmd6svhhkf966q9mx6p20k39")))

(define-public crate-recipe-reader-0.9.8 (c (n "recipe-reader") (v "0.9.8") (h "1rwdsvblr7vrd8lzsnsj5a9v336yml2k69ijvn2lyjy1gzc6sx2k")))

(define-public crate-recipe-reader-0.9.9 (c (n "recipe-reader") (v "0.9.9") (h "0q4k94lvfk3van6wqas8242pnf2rcgngd0rshwx8daabr540by87")))

(define-public crate-recipe-reader-0.9.10 (c (n "recipe-reader") (v "0.9.10") (h "1av1a3rgx5h15hjh8cnfs199bgrw4xvqvrgf10hgwhgbgcgp84y6")))

(define-public crate-recipe-reader-0.9.11 (c (n "recipe-reader") (v "0.9.11") (h "00rcv2ydkz913zk5pll5psw964mivdbgbx5blikwnj43biacigk1")))

