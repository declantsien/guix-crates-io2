(define-module (crates-io re ci reciter) #:use-module (crates-io))

(define-public crate-reciter-0.1.0 (c (n "reciter") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0rzz5hpac2ps7p0949hl01p0x535zki8jzs77h7mwmxkjcm8fypv")))

(define-public crate-reciter-0.1.1 (c (n "reciter") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jqnwyyqbmycr6g0qwrwf3m8kn3wqg8ad0zmmg0lqmz41qqir4dh")))

(define-public crate-reciter-0.1.2 (c (n "reciter") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0jfcnciyp9mdnns5b9ws5i81gqc14fmzzkkapkzybi9g8nhphwfa")))

