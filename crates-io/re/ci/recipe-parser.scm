(define-module (crates-io re ci recipe-parser) #:use-module (crates-io))

(define-public crate-recipe-parser-0.2.0 (c (n "recipe-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x5n3iy70qjdvha19mlr13vgppi68aaaa1cblhdv3si54r80y1cz")))

(define-public crate-recipe-parser-0.2.1 (c (n "recipe-parser") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09aqxvmzb9si0h8bwdw59vjdl22vlhjzpxz8n7jh63af5d1g8sb4")))

(define-public crate-recipe-parser-0.2.2 (c (n "recipe-parser") (v "0.2.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gwqipnx5ydz4wg5cjzfbwqf3z39n53rz1iyjp0n3awvglz4zbs1")))

(define-public crate-recipe-parser-0.2.3 (c (n "recipe-parser") (v "0.2.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nk2kcywdi6vil61z70lqv66whr5ih58sdq0072rnmms0l7zi025")))

(define-public crate-recipe-parser-0.2.4 (c (n "recipe-parser") (v "0.2.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cxh9mh6j6idvvh6d84mc076mi8xpfdpsk16sx17cmd3qwp69p9r")))

(define-public crate-recipe-parser-0.2.7 (c (n "recipe-parser") (v "0.2.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10a92y4rimnhqbvg1wr6vipkxz0vbag7xfmrzdf807cx3fjlrmig")))

(define-public crate-recipe-parser-0.2.8 (c (n "recipe-parser") (v "0.2.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dc3z75l3jw223xncja4gxain5rihk8j8f7czwih1a068jlwpk2m")))

(define-public crate-recipe-parser-0.2.9 (c (n "recipe-parser") (v "0.2.9") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yqajh85855g5qy1rqjrqh0wwdlw3znig2141l5akhsfm0fs1qdv")))

(define-public crate-recipe-parser-0.3.0 (c (n "recipe-parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y58q195540j6xsqjsphdx3dipv5iwjxdpangy29698m36y93vcg")))

(define-public crate-recipe-parser-0.3.1 (c (n "recipe-parser") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qca5dr61a0mh3v0cyk4h7w8msn70jicaj6b60x4ql810gwxhq9m")))

(define-public crate-recipe-parser-0.4.0 (c (n "recipe-parser") (v "0.4.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "0zr9nags5fabvgw08mgsx05wmflb69wxgwsmilpjsm5sllfp67f4")))

(define-public crate-recipe-parser-0.4.1 (c (n "recipe-parser") (v "0.4.1") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "0037h8mg485c9hd0k4invw981dkxxcr3vrp5pdpqza7ir3hg48dk")))

(define-public crate-recipe-parser-0.4.2 (c (n "recipe-parser") (v "0.4.2") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "1ba77y12yhqk33c6d4f99gf0p8yai3rlk60wkiffx42qm0hpf24i")))

(define-public crate-recipe-parser-0.4.3 (c (n "recipe-parser") (v "0.4.3") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "01ywdhf8pa8l0af5wgq77ccg5wj4xyinf6hx5r5qf9kgyg765a6g")))

(define-public crate-recipe-parser-0.5.0 (c (n "recipe-parser") (v "0.5.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "0q5fnlm5ihm3gniczj5x7iarf9zmam1nss8k2axr1v2rfk31ii6h") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-recipe-parser-0.6.0 (c (n "recipe-parser") (v "0.6.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "0hqsxwyrg3wpn5xbn7zkfa23x5smhxbafp7p9zb9296hq6zlazn7") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars"))))))

(define-public crate-recipe-parser-0.7.0 (c (n "recipe-parser") (v "0.7.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winnow") (r "^0.6.6") (d #t) (k 0)))) (h "1b0rlqpbynb2j3df1bvfjmy7xibwqm769zqkiizclipi9d1wdfqa") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars"))))))

(define-public crate-recipe-parser-0.8.0 (c (n "recipe-parser") (v "0.8.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tsify") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "19haqa9jp117rsgwx70gkzz3yajxmya03p8556g7x07n6cgrxqdm") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:tsify") ("serde" "dep:serde") ("schemars" "dep:schemars"))))))

(define-public crate-recipe-parser-0.8.1 (c (n "recipe-parser") (v "0.8.1") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tsify") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "0farlh7jvb67mchchnysfa64wk7s1hfs3l9d8rlrfavq0sb83m4w") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:tsify") ("serde" "dep:serde") ("schemars" "dep:schemars"))))))

