(define-module (crates-io re ci reciprocal) #:use-module (crates-io))

(define-public crate-reciprocal-0.1.0 (c (n "reciprocal") (v "0.1.0") (h "0768g2xcp12ywzp7hgl27wdi5nywdr36ps4pha25knmgymbsappy")))

(define-public crate-reciprocal-0.1.1 (c (n "reciprocal") (v "0.1.1") (h "0965fiix3jnk5y5vc4v5w3hdpvvdvb2rc2cni6rfl967agzvrxsg")))

(define-public crate-reciprocal-0.1.2 (c (n "reciprocal") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastdivide") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "strength_reduce") (r "^0.2.3") (d #t) (k 2)))) (h "1gwrgmwgpjy56iwbm3imh2g0pjsxppb3ir004k37iqyga945nfss") (f (quote (("nightly"))))))

