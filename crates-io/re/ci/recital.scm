(define-module (crates-io re ci recital) #:use-module (crates-io))

(define-public crate-recital-0.1.0 (c (n "recital") (v "0.1.0") (d (list (d (n "nom") (r "1.2.*") (d #t) (k 0)))) (h "0z04xl6ycw9q0vw9d2a79w6cwhq50bwnlxv4vnj1va0kn2xrqbnv")))

(define-public crate-recital-0.2.0 (c (n "recital") (v "0.2.0") (d (list (d (n "nom") (r "1.2.*") (d #t) (k 0)))) (h "0hvq8fwz9aq315dlx5d2jf7p90k39if4vxgnmwk0k86k9ldqjcnx")))

(define-public crate-recital-0.3.0 (c (n "recital") (v "0.3.0") (d (list (d (n "nom") (r "1.2.*") (d #t) (k 0)))) (h "0fincwfndwspg1kp5f5yjy5x4jbxarcdkvjx3ix80jl8a4db07wg")))

