(define-module (crates-io re sm resman) #:use-module (crates-io))

(define-public crate-resman-0.1.0 (c (n "resman") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)))) (h "1hlvda39z7w8mr7c9grmzjnr3ffj4lajjq94f243a3kws4axly6l")))

(define-public crate-resman-0.2.0 (c (n "resman") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.1.0") (d #t) (k 0)))) (h "1g7zv95y0rjxdxi0byhhkf2anmh6z2x7qmgi6224l95r8rl2j5h1")))

(define-public crate-resman-0.3.0 (c (n "resman") (v "0.3.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.1.0") (d #t) (k 0)))) (h "1a795hrn41rdkianm13lz243gah49diyq9x3x33piiwnz6245mv7")))

(define-public crate-resman-0.4.0 (c (n "resman") (v "0.4.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.2.0") (d #t) (k 0)))) (h "138vr9saxgxsgxfinmmbni179nyv1l59ljvvxi87afyb61khn30r")))

(define-public crate-resman-0.5.0 (c (n "resman") (v "0.5.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.4.0") (d #t) (k 0)))) (h "10c5d4p1naz9k258xz0am3wpdy4qbd31qb9s4lgh61cy5whc7i9s") (f (quote (("debug"))))))

(define-public crate-resman-0.6.0 (c (n "resman") (v "0.6.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "0m8gvpa7gcm4yg8dmmaijn9x5ix8lrs5maiz3jgzkds9c2nfmvr0") (f (quote (("debug"))))))

(define-public crate-resman-0.7.0 (c (n "resman") (v "0.7.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "1y75yn078syif53vvkbnzkdg1lkirkbqqi39im92sxdgjclxd9r1") (f (quote (("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.8.0 (c (n "resman") (v "0.8.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "1sa3lll9zipkggxcaxh3vpnvgzspw8aq1aafkq325qpzkdvv7y3z") (f (quote (("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.9.0 (c (n "resman") (v "0.9.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.3.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "01kcf3a80k9w3hgcyd7gmniidqq21jv4xdvvsrlsssdpz0vqmyd0") (f (quote (("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.10.0 (c (n "resman") (v "0.10.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.4.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "05h5h8ix9s1gvwa2ffilc5hwkmnwqz7pr9jlwgnj9i2pnm130jhc") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.11.0 (c (n "resman") (v "0.11.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.4.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "1avqjqmk929p5s6icgxn53ckqhdh8j5sdk866mzcnga2zllg3wmh") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.12.0 (c (n "resman") (v "0.12.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.5.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "14j9ag7mkgd3d7ypf2svnzqf5p2dv555c5ccd4qpsmsn9nv3bpaf") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.13.0 (c (n "resman") (v "0.13.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.6.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "1434h8v5qxrjrqrzhbwshg7f9kx2qpyaagy0206ql99k9d46p917") (f (quote (("high_arg_count" "fn_meta" "fn_meta/high_arg_count") ("fn_res_once" "fn_meta") ("fn_res_mut" "fn_meta") ("fn_res" "fn_meta") ("default") ("debug"))))))

(define-public crate-resman-0.14.0 (c (n "resman") (v "0.14.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.7.0") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.0") (d #t) (k 0)))) (h "1a4kz2qf94jldmdhzfzbr6m5c61a9ya732kx6r02a8ic18ci4bvj") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.15.0 (c (n "resman") (v "0.15.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.7.1") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.1") (d #t) (k 0)))) (h "04qnv6w0lfc3ja3sn8lj6xy3qq98190fdkvbsrf2b6a9krrv1y4q") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.16.0 (c (n "resman") (v "0.16.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.7.3") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.2") (d #t) (k 0)))) (h "0a07y9z26dsvrbmx3c2j4fv8xzk65x4pqw1qh7g3wh1iv7nfq7cz") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.16.1 (c (n "resman") (v "0.16.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.7.3") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.2") (d #t) (k 0)))) (h "1n0r45vs7ks224qz9n893bqz9blcp4i4qn9127p0463v2p0h6bil") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

(define-public crate-resman-0.17.0 (c (n "resman") (v "0.17.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fn_meta") (r "^0.7.3") (f (quote ("fn_meta_ext"))) (o #t) (d #t) (k 0)) (d (n "rt_map") (r "^0.5.2") (d #t) (k 0)) (d (n "tynm") (r "^0.1.8") (d #t) (k 0)))) (h "0hjbh8mwm8cqkq4lzspisr82zmbjnjfnh8324v7325g1j6d86gkg") (f (quote (("high_arg_count" "fn_meta/high_arg_count") ("fn_res_once") ("fn_res_mut") ("fn_res") ("default") ("debug"))))))

