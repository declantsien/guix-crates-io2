(define-module (crates-io re vo revolt-rs) #:use-module (crates-io))

(define-public crate-revolt-rs-0.0.1 (c (n "revolt-rs") (v "0.0.1") (h "0ywrmpi3gva8lhlxi88skzgq82wdj4jzn9bh3qxkpbz7mpsagjbm") (y #t)))

(define-public crate-revolt-rs-0.0.2 (c (n "revolt-rs") (v "0.0.2") (h "0q0795vbhkxrdghm5bwig6y87750bw2rm94pxhfw2jxq676di1y6") (y #t)))

(define-public crate-revolt-rs-0.0.3 (c (n "revolt-rs") (v "0.0.3") (h "1h40phxsxd1f37b86946r9bsvxb4zxj1r0ascki43lpmckw46pi8")))

