(define-module (crates-io re vo revolt-rs-gateway) #:use-module (crates-io))

(define-public crate-revolt-rs-gateway-0.0.1 (c (n "revolt-rs-gateway") (v "0.0.1") (h "0sxq68cq6yfrbf5c81j4sldw91fn5psd6r7c6wvr72bnnp71zbpc") (y #t)))

(define-public crate-revolt-rs-gateway-0.0.2 (c (n "revolt-rs-gateway") (v "0.0.2") (h "1aw2zvjp0q1z1x6sy2rakcpjc6v51vlcnyp375vqx9zdyygkmf3j")))

