(define-module (crates-io re vo revolt_clamav-client) #:use-module (crates-io))

(define-public crate-revolt_clamav-client-0.1.4 (c (n "revolt_clamav-client") (v "0.1.4") (h "1wrd9szap14zhv50366zv34y1xsk22qjrvizhvc1vk2fnm8pa79l")))

(define-public crate-revolt_clamav-client-0.1.5 (c (n "revolt_clamav-client") (v "0.1.5") (h "1a6fcpmkw8jwk9l23vjgqxh23szx239q5z81pxff1qk1x2grww9x")))

