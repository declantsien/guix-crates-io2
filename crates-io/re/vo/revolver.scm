(define-module (crates-io re vo revolver) #:use-module (crates-io))

(define-public crate-revolver-0.1.0 (c (n "revolver") (v "0.1.0") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "stdio-override") (r "^0.1.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0d9daf4rpv0nd9xsaj9l2dlgsrvpg43d7c1k8dp28k3hhz6l8rfg")))

(define-public crate-revolver-0.2.0 (c (n "revolver") (v "0.2.0") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "stdio-override") (r "^0.1.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1nsv1v078lprbg1qcvj3z7hw337mq8jqib2kj87qnxvaap26gjvq")))

