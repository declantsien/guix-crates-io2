(define-module (crates-io re vo revolt_okapi) #:use-module (crates-io))

(define-public crate-revolt_okapi-0.7.0-rc.1 (c (n "revolt_okapi") (v "0.7.0-rc.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d587ixirf78r54drxn3zhjlqpz1qdp6ip4rlp62zcdflr4xgkp7") (f (quote (("preserve_order" "schemars/preserve_order") ("impl_json_schema" "schemars/impl_json_schema"))))))

(define-public crate-revolt_okapi-0.9.0 (c (n "revolt_okapi") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hcz2dvhbd5rm87bgrf8kn759pjjqj4w7cvqj8x0d7vxn1jqrnv5") (f (quote (("preserve_order" "schemars/preserve_order") ("impl_json_schema" "schemars/impl_json_schema"))))))

(define-public crate-revolt_okapi-0.9.1 (c (n "revolt_okapi") (v "0.9.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nw3yxnv4wsz7q6dcfmknij2mc33fg0ybxi7wwph9hv9wxxdzgr3") (f (quote (("preserve_order" "schemars/preserve_order") ("impl_json_schema" "schemars/impl_json_schema"))))))

