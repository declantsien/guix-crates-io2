(define-module (crates-io re vo revolute) #:use-module (crates-io))

(define-public crate-revolute-0.1.0 (c (n "revolute") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "revolt_api") (r "^0.5") (d #t) (k 0)))) (h "1z8cs0cwbvbgf95xfqsaax26xxnwlsmnyag3cj081l5i14lyrkhg")))

