(define-module (crates-io re vo revolt-rs-http) #:use-module (crates-io))

(define-public crate-revolt-rs-http-0.0.1 (c (n "revolt-rs-http") (v "0.0.1") (h "1yfj6xvw62zpl3fgv3xvbshim4mgvfl1jwgwn935g0rl1nzgxk1f") (y #t)))

(define-public crate-revolt-rs-http-0.0.2 (c (n "revolt-rs-http") (v "0.0.2") (h "0x7b2kh1zjk2yavp6gbnj1jsmhcyl5mfmdbq0727r2qc7q2im3hd")))

