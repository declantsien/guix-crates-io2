(define-module (crates-io re vo revolt_optional_struct) #:use-module (crates-io))

(define-public crate-revolt_optional_struct-0.2.0 (c (n "revolt_optional_struct") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0gnm2ly21arpwg1j7d7rkcfls28rp8v7lbxlfafzw8w6clwjfd0d")))

