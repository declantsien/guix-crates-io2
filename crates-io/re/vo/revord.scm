(define-module (crates-io re vo revord) #:use-module (crates-io))

(define-public crate-revord-0.0.1 (c (n "revord") (v "0.0.1") (h "086wdcsgdqkqawwkpwcnh0474kayslrab1d7rc81084s0bx1w8qx")))

(define-public crate-revord-0.0.2 (c (n "revord") (v "0.0.2") (h "1g0jy4zqjdbkkc012jbjpb86l992w1yaq929f09xlpjwk827kcnq")))

