(define-module (crates-io re vo revonet) #:use-module (crates-io))

(define-public crate-revonet-0.1.0 (c (n "revonet") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1v2fd4nbijdhr3454bwnhfivssnxxzibzb6xmdnpky4h9m7bqaw1")))

(define-public crate-revonet-0.2.0 (c (n "revonet") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "16bnmfdk1q9jxmfy9ln14mhhxgh5ik3icxn1y8y4d22bv1hgp53r")))

(define-public crate-revonet-0.2.1 (c (n "revonet") (v "0.2.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1a5jb0rs4z1bs7g6vv34xh5lqmvlh62l2q3m7rkzsam2c7sp2zwv")))

