(define-module (crates-io re vo revoker) #:use-module (crates-io))

(define-public crate-revoker-1.0.0 (c (n "revoker") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12ihbqjb3zxkgdxcfv8yczy19qwh5s88c63ksl2k089nnljxxxrv")))

(define-public crate-revoker-1.0.1 (c (n "revoker") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "11pkk9vklsfpi79b2ibnjc2m4q3824b34h6rij64absrvgj2fmms")))

(define-public crate-revoker-2.0.0 (c (n "revoker") (v "2.0.0") (d (list (d (n "attohttpc") (r "^0.26.1") (f (quote ("basic-auth"))) (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)))) (h "0fhfyd4f61bkq0ia7796krwh8im0c8mnb8cyr94vwyppvjkqcj3k")))

(define-public crate-revoker-2.1.0 (c (n "revoker") (v "2.1.0") (d (list (d (n "attohttpc") (r "^0.26.1") (f (quote ("basic-auth"))) (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)))) (h "1l046rijllfi1qjwm69aykjwaxq9mgm9lgqh2dzjjf75z25l39fh")))

