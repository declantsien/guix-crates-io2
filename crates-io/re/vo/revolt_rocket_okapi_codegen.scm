(define-module (crates-io re vo revolt_rocket_okapi_codegen) #:use-module (crates-io))

(define-public crate-revolt_rocket_okapi_codegen-0.8.0-rc.1 (c (n "revolt_rocket_okapi_codegen") (v "0.8.0-rc.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05rvw13ld7nrissg9nxln6nq4fabr4hlin8apmd6090b98r32jfy")))

(define-public crate-revolt_rocket_okapi_codegen-0.9.0 (c (n "revolt_rocket_okapi_codegen") (v "0.9.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wacsl04ys78890b2mbnnnmcvhrczi2mbh7z7i4kxnzn2bimc4x9")))

(define-public crate-revolt_rocket_okapi_codegen-0.9.1 (c (n "revolt_rocket_okapi_codegen") (v "0.9.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ydy3p59mda9jxrbyvp9c1x800w590zs3k0gd6hz1j4akmb20rnc")))

