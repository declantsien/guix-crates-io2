(define-module (crates-io re vo revolt-rs-util) #:use-module (crates-io))

(define-public crate-revolt-rs-util-0.0.1 (c (n "revolt-rs-util") (v "0.0.1") (h "1c1mpgnm97215l8n7v9vz5hlys50r0rqpyxp4mxxswkg28v5hwqv") (y #t)))

(define-public crate-revolt-rs-util-0.0.2 (c (n "revolt-rs-util") (v "0.0.2") (h "0d5ny6qqpnrn97x5b0bzl79a149jjrh1vf53jg4bdhl9cw7xa6mh")))

