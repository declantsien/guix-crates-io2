(define-module (crates-io re vo revolt_api) #:use-module (crates-io))

(define-public crate-revolt_api-0.5.5 (c (n "revolt_api") (v "0.5.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "160f485a8jga73ilalz42fzxlgjk4gf009x4jznwk66hij0r306j") (y #t)))

(define-public crate-revolt_api-0.5.5-rev.1 (c (n "revolt_api") (v "0.5.5-rev.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pic0bfspcfhmwhqp9dn6qlqil1q1cq41sd1bc6wsmxcijnbja1q")))

(define-public crate-revolt_api-0.5.5-rev.2 (c (n "revolt_api") (v "0.5.5-rev.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1c7kck4cs994mc5zdcq70hsfinq652ha9vzjpp7fzmkirfcyldjz")))

(define-public crate-revolt_api-0.6.5 (c (n "revolt_api") (v "0.6.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1m33sdp340ggm6f963bjf1c9zrxl3rz24b77r1r7vrjhi9nw2bn7")))

