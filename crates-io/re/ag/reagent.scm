(define-module (crates-io re ag reagent) #:use-module (crates-io))

(define-public crate-reagent-0.1.0 (c (n "reagent") (v "0.1.0") (d (list (d (n "ramhorns") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "14c36g062g4kwczpr41hm0whdvv5rp43c1j23sl12vd0wzrlj0j2")))

(define-public crate-reagent-0.1.1 (c (n "reagent") (v "0.1.1") (d (list (d (n "ramhorns") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1mpc08shd4nj281ki312jl39hfg2b89fkwkjn600l98rv90vwhy9")))

