(define-module (crates-io re ig reign_boot) #:use-module (crates-io))

(define-public crate-reign_boot-0.1.1 (c (n "reign_boot") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "05h7y9chrf09vb93r0zrvasrk7ka2w7m9xa1xxyxlbh9may5286j")))

(define-public crate-reign_boot-0.1.2 (c (n "reign_boot") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)))) (h "0sdj0wm2z91dmrvhb8h13h0n6n8rvxxaak1gwilh09n89jcbz0ah")))

(define-public crate-reign_boot-0.2.0 (c (n "reign_boot") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)))) (h "00v9f444pvn3g0jrzqar6qccphjp5pz85gprk104pq9i63wcsxaq")))

(define-public crate-reign_boot-0.2.1 (c (n "reign_boot") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "envy") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "19bck2hsdcghqxf2wynh1yzrwm922lfl9z0rh99bmr5a83b86igi") (f (quote (("doc") ("default"))))))

