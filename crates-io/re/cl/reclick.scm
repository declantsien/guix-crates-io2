(define-module (crates-io re cl reclick) #:use-module (crates-io))

(define-public crate-reclick-0.1.0 (c (n "reclick") (v "0.1.0") (d (list (d (n "device_query") (r "^0.2") (d #t) (k 0)) (d (n "tfc") (r "^0.6") (d #t) (k 0)))) (h "1s5vyh9yf3h97kz4s4xs741irknfmz3sm66vg60clzanryhdyndh")))

(define-public crate-reclick-0.1.1 (c (n "reclick") (v "0.1.1") (d (list (d (n "device_query") (r "^0.2") (d #t) (k 0)) (d (n "tfc") (r "^0.6") (d #t) (k 0)))) (h "1l792b1yyjjwg4817k6zkz672b65r54816xyz29fn1i5nb7zwr92")))

