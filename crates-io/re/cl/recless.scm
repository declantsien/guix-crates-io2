(define-module (crates-io re cl recless) #:use-module (crates-io))

(define-public crate-recless-0.1.0 (c (n "recless") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "0cvwbvv7qhkkjz8l06gdq8xm8jnaf7b7pr1f4i9s8cd91vsgam4j") (f (quote (("blas" "ndarray/blas"))))))

(define-public crate-recless-0.1.1 (c (n "recless") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "02hxg3q1aswqjx0xj0mqkc65q93rbcvp1m8i2j949bc67lvp5gcy") (f (quote (("blas" "ndarray/blas"))))))

(define-public crate-recless-0.1.2 (c (n "recless") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "01h3ihryn23wlgmyambl0k1nmxjk1npn8pbmalbrfbwg0pzb3gi5") (f (quote (("blas" "ndarray/blas"))))))

(define-public crate-recless-0.1.3 (c (n "recless") (v "0.1.3") (d (list (d (n "blas") (r "^0.15") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (f (quote ("blas"))) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1ziq1fvlvxpv4iwa4bjifizmmad61j0cs95yzff7cd0avrczljsf") (f (quote (("serde_support" "serde" "serde_derive" "ndarray/serde"))))))

(define-public crate-recless-0.2.0 (c (n "recless") (v "0.2.0") (d (list (d (n "blas") (r "^0.15") (d #t) (k 0)) (d (n "ndarray") (r "^0.10") (f (quote ("blas"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1d120751zjv9x0711vja3aknwz4brgmhli6901sl42wix4j5523a") (f (quote (("serde_support" "serde" "serde_derive" "ndarray/serde"))))))

(define-public crate-recless-0.3.0 (c (n "recless") (v "0.3.0") (d (list (d (n "blas") (r "^0.16") (k 0)) (d (n "ndarray") (r "^0.10") (f (quote ("blas"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cngqfkklvkjycaxy8ywzfb0fv8ljr30gpnk88pa3xj7aimdr0nq") (f (quote (("serde_support" "serde" "serde_derive" "ndarray/serde"))))))

