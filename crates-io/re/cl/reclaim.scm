(define-module (crates-io re cl reclaim) #:use-module (crates-io))

(define-public crate-reclaim-0.1.0 (c (n "reclaim") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "memoffset") (r "^0.2.1") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1riczn60svjklc209dyc9zj6vazykkvvpg7sfj3ad16faa9hzxc1") (f (quote (("std") ("default" "std"))))))

(define-public crate-reclaim-0.2.0 (c (n "reclaim") (v "0.2.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "memoffset") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "11a40vyh3ggsp0br7fw83v3zg8778z97yjl5fbw04j2x216cyp10") (f (quote (("std") ("default" "std"))))))

(define-public crate-reclaim-0.2.1 (c (n "reclaim") (v "0.2.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "memoffset") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0iag8bbvifv5jygri4wfhxfxp5bnk01zipppinxli6p3js9by80n") (f (quote (("std") ("default" "std"))))))

(define-public crate-reclaim-0.2.2 (c (n "reclaim") (v "0.2.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "02m617dyfsrf3xcn2l4mcm7vd2qq9c1ikmrqp2y1mmvhm7wsx9ms") (f (quote (("std") ("default" "std"))))))

