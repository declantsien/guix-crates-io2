(define-module (crates-io re cl reclaim-solana) #:use-module (crates-io))

(define-public crate-reclaim-solana-0.1.0 (c (n "reclaim-solana") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0pix5z16biy8b5iq56cmm72183bb7n6n9lhvq8ddqcaqydi1nmmw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

