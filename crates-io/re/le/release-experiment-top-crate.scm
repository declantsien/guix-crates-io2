(define-module (crates-io re le release-experiment-top-crate) #:use-module (crates-io))

(define-public crate-release-experiment-top-crate-0.3.1 (c (n "release-experiment-top-crate") (v "0.3.1") (h "1p1iv84gy76wi9gzp7w0k8pjfyb4wmilsk2jvil91s8ikazgh4kq")))

(define-public crate-release-experiment-top-crate-0.3.5 (c (n "release-experiment-top-crate") (v "0.3.5") (h "0l0p9wxb2ailh0rqqz594gihb7hnyfgmxjk5r4jav4cc0hz2my41")))

