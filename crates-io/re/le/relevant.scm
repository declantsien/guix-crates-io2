(define-module (crates-io re le relevant) #:use-module (crates-io))

(define-public crate-relevant-0.1.0 (c (n "relevant") (v "0.1.0") (h "0mmv51lh23crvqgyqldlpqc7y6v5lhkhzi5g04ldk052zqvmj0hm")))

(define-public crate-relevant-0.1.1 (c (n "relevant") (v "0.1.1") (h "00h3ig8q1jyvmcglqcnzl7agz5hij6blmi12pg33cvdacb38gc62")))

(define-public crate-relevant-0.2.0 (c (n "relevant") (v "0.2.0") (h "0xkibraysw4xwblccab1l40f74b1dwp788z9hkqcwip7lyjircz6") (f (quote (("std") ("default" "std"))))))

(define-public crate-relevant-0.3.0 (c (n "relevant") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ld7ymg40yrx4pcig3rqgxmwa48jpk5sh731zpnsvymi0dairbg6") (f (quote (("std") ("serde-1" "serde") ("default" "log"))))))

(define-public crate-relevant-0.4.0 (c (n "relevant") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ib94nx8px0v2xi1hi7im36sfjb9493z4x8qny6z6livbk7zfsm2") (f (quote (("std") ("serde-1" "serde") ("panic"))))))

(define-public crate-relevant-0.4.1 (c (n "relevant") (v "0.4.1") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yhpdmgfhy879qljm2yxgxcra46sijvf9mw02l8r3cs2cykicn14") (f (quote (("std") ("serde-1" "serde") ("panic"))))))

(define-public crate-relevant-0.4.2 (c (n "relevant") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0203mfr401jqjfwgh1qqpifvxjl0qgpsahlvbdzm9x1p7phk5hmv") (f (quote (("std") ("serde-1" "serde") ("panic") ("default" "std"))))))

