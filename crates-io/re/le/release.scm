(define-module (crates-io re le release) #:use-module (crates-io))

(define-public crate-release-0.5.0 (c (n "release") (v "0.5.0") (d (list (d (n "git2") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "1n85qr0b9xp3ix8sif6ynwvjcgcd7l2rji0jz8maj19cb6k1jjyd")))

(define-public crate-release-0.8.0 (c (n "release") (v "0.8.0") (d (list (d (n "git2") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "1k59xh6dnh1d2ng6mmnp0xzam0c92a0x0ssy8p0bcycwqw7bpq2s")))

(define-public crate-release-0.8.1 (c (n "release") (v "0.8.1") (d (list (d (n "git2") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "0y7hbmcfm0k5lab9mjyk0zm7kij9wv1rxgll0qi5ssvbbawrbkb0")))

