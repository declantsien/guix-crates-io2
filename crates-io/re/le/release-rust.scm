(define-module (crates-io re le release-rust) #:use-module (crates-io))

(define-public crate-release-rust-0.1.0 (c (n "release-rust") (v "0.1.0") (h "1rsfim4xq1502qwnnima02mc9vad3k6b4gfdbdjxvfhs6llgmj10")))

(define-public crate-release-rust-0.0.5 (c (n "release-rust") (v "0.0.5") (h "0hcy74iv65hiszyhrha3bq5wr29lwvznknpjnchswxdchm8wr0mb")))

(define-public crate-release-rust-0.0.6 (c (n "release-rust") (v "0.0.6") (h "1vwl9x8by84mk4zv231jyh3f8ya7pappidgz2zf357mdbh8589dw")))

