(define-module (crates-io re le releasable) #:use-module (crates-io))

(define-public crate-releasable-0.1.0 (c (n "releasable") (v "0.1.0") (h "0mhdl081kdzmid7ngp3yxvswlvsg0xk3fyfk85wn1zsmymy80gzz") (y #t)))

(define-public crate-releasable-0.1.1 (c (n "releasable") (v "0.1.1") (h "0hbx5hy0g865cvjvnck18qd5myz8h8pk1bdi4j8qf9lb9brvx6ba") (y #t)))

(define-public crate-releasable-0.1.2 (c (n "releasable") (v "0.1.2") (h "013lkz4hzpg2kzl910m40d94hz3mafpqn8bd460492gwxknsj952") (y #t)))

(define-public crate-releasable-0.1.3 (c (n "releasable") (v "0.1.3") (h "0nm47slskcqfcrj823pz4yqsxa82xkhdsrz4yzv1krbjjf6svrih") (y #t)))

(define-public crate-releasable-0.1.4 (c (n "releasable") (v "0.1.4") (h "0mxd6b4y0lim6vly1x5j7cxhvsc7i98hlbqanax8lsv984ccnr59") (y #t)))

(define-public crate-releasable-0.1.5 (c (n "releasable") (v "0.1.5") (h "1xi3lixig3qk7zzq416q73naswqs7ma2hkpl4rnszniw2higq1cw") (y #t)))

(define-public crate-releasable-0.1.6 (c (n "releasable") (v "0.1.6") (h "0c0i33hws5jam3xwpib3nzgfwsas5ai73vmdhaihijzqq18dwc1w") (y #t)))

