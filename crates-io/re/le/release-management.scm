(define-module (crates-io re le release-management) #:use-module (crates-io))

(define-public crate-release-management-0.0.0 (c (n "release-management") (v "0.0.0") (h "146iz2jvi0rgspfz1g4hyzvpgxska86akfnxdqhfnj6aykh7s118") (y #t)))

(define-public crate-release-management-0.0.1 (c (n "release-management") (v "0.0.1") (h "10m60vfympcwyfy431s1b65kbn64bxciisgib81g5nxln9k9lki5") (y #t)))

(define-public crate-release-management-0.0.2 (c (n "release-management") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0jzbxw48kdj4hnkic03dmkvp132mkf8f6jk57dxh38dd0yvgkp8j") (y #t)))

(define-public crate-release-management-0.0.3 (c (n "release-management") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0jnmd6g0x9nryyh1ldrwpzs6kr2lc3l6px92h4dy08kdijrls1li") (y #t)))

(define-public crate-release-management-0.0.4 (c (n "release-management") (v "0.0.4") (h "0rrg5cm7hcqrj4y5y45nd4iajbh1p1h3ix6nif8ghh3lylk6yrk2") (y #t)))

