(define-module (crates-io re le relexer-derive) #:use-module (crates-io))

(define-public crate-relexer-derive-0.1.0 (c (n "relexer-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.1") (d #t) (k 0)))) (h "1z8bbm0fpaibqq9kdkqrc8xibikzkk1dfzczd4z4bvfgln49vnkj")))

