(define-module (crates-io re le release-me) #:use-module (crates-io))

(define-public crate-release-me-0.1.0 (c (n "release-me") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "gitmoji-changelog") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0bsx7nxrj0v1pb6ib0jmria5lzj2rjkc7jz6879s44w2f9bapx6p")))

