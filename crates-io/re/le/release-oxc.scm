(define-module (crates-io re le release-oxc) #:use-module (crates-io))

(define-public crate-release-oxc-0.0.0 (c (n "release-oxc") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (f (quote ("derive" "batteries"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "git-cliff-core") (r "^2.2.0") (f (quote ("repo"))) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (f (quote ("parse"))) (d #t) (k 0)))) (h "12pw5mqwnx8a40ky3c9hz8mpnnq0cbxl2s10qml5lc8sfyirhzfq") (y #t)))

(define-public crate-release-oxc-0.0.1 (c (n "release-oxc") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (f (quote ("derive" "batteries"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "git-cliff-core") (r "^2.2.0") (f (quote ("repo"))) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (f (quote ("parse"))) (d #t) (k 0)))) (h "00p0025rvs07vb10xjk109848ciz3shji3dfv9n2d18hp8hqlvyw") (y #t)))

