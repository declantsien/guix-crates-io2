(define-module (crates-io re le relearn_derive) #:use-module (crates-io))

(define-public crate-relearn_derive-0.1.0 (c (n "relearn_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ca7l2cl4mmmvmqv3bikd203xjiaj9h5lfp7nrfvs15qjfg0pf66")))

(define-public crate-relearn_derive-0.2.0 (c (n "relearn_derive") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "relearn") (r "<=0.2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zhjr6mnr9j0ppfxciaia316mr6m7igbziim8yrgyp6ksffhxb5z") (f (quote (("doc-only" "relearn/doc-only"))))))

(define-public crate-relearn_derive-0.3.0 (c (n "relearn_derive") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "relearn") (r "^0.2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0818frrhdgcdhwr0jhzqh0a7byf9028yb82rwza3xkxhw1rxxxhw") (f (quote (("doc-only" "relearn/doc-only"))))))

