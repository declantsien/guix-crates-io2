(define-module (crates-io re le releasetag) #:use-module (crates-io))

(define-public crate-releasetag-0.1.0 (c (n "releasetag") (v "0.1.0") (h "1fx7cr7w8a5zg4nzk2k0w36pvflm4xap4r7071c0lwdbic1ifcj6") (y #t)))

(define-public crate-releasetag-0.2.0 (c (n "releasetag") (v "0.2.0") (d (list (d (n "bytestool") (r "^0.1.0") (d #t) (k 0)))) (h "178q4kv51k41skhp5zkj165xcilid2mxqzbg52ksfnii1pbvhps8") (y #t)))

(define-public crate-releasetag-0.3.0 (c (n "releasetag") (v "0.3.0") (d (list (d (n "bytestool") (r "^0.2.0") (d #t) (k 0)))) (h "16y9w1nmlw4mxca59614hwxb2r0g5z5d7r51671dl1jzhvshc6z6") (y #t)))

(define-public crate-releasetag-0.4.0 (c (n "releasetag") (v "0.4.0") (d (list (d (n "bytestool") (r "^0.2.0") (d #t) (k 0)))) (h "0knj2r9wzfcv2hg09agpkyq78f87az7chddra3g41wxnp8a84fyk") (y #t)))

(define-public crate-releasetag-0.5.0 (c (n "releasetag") (v "0.5.0") (d (list (d (n "bytestool") (r "^0.2.0") (d #t) (k 0)))) (h "03fy861c4qmxqa0yr9f6lfmvnadhaivdj3izfsgrk934fzzgdanz") (y #t)))

(define-public crate-releasetag-0.6.0 (c (n "releasetag") (v "0.6.0") (d (list (d (n "bytestool") (r "^0.2.0") (d #t) (k 0)))) (h "01c9agfhqqri5d1ksqypyy27wx0y9rsxbmlsh11y01iwbdprp4q6") (y #t)))

(define-public crate-releasetag-0.7.0 (c (n "releasetag") (v "0.7.0") (d (list (d (n "bytestool") (r "^0.3.0") (d #t) (k 0)))) (h "1809vczfi8by6frh15n9v2a36hszmagw2h2rgqly5fh33ia4flcv") (y #t)))

(define-public crate-releasetag-0.8.0 (c (n "releasetag") (v "0.8.0") (h "0hrrnkl40xpalahg3y7knx7d8i60lg1al4w0lb9sgcmsq39ql1rz")))

(define-public crate-releasetag-1.0.0 (c (n "releasetag") (v "1.0.0") (h "1cjhrrf6a1283yqlgq61r2x4vgig5gjz60z6plgjji3s58lbxdyv")))

(define-public crate-releasetag-1.0.1 (c (n "releasetag") (v "1.0.1") (h "030rwpgasjnk4jdcvkz351nnb49pcx3n1rprhxq20rrphv0z7jcx")))

(define-public crate-releasetag-1.0.2 (c (n "releasetag") (v "1.0.2") (h "1fyvh2s3qgv60skq710d92ba8xwmdckjwbkyxwdslybzn60nnwbm")))

(define-public crate-releasetag-1.1.0 (c (n "releasetag") (v "1.1.0") (h "0lf9lxv9snqcs2m54zfi5ywwqc8p3s12yi8625v0b7y21mcyz09f")))

