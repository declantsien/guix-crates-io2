(define-module (crates-io re le release-script) #:use-module (crates-io))

(define-public crate-release-script-0.1.1 (c (n "release-script") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.6") (d #t) (k 0)))) (h "07z2ia5yrjmljlfk0ay0ywnymr32yc43bpzr002xxdp5z36nph9i")))

