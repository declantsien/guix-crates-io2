(define-module (crates-io re le release-utils) #:use-module (crates-io))

(define-public crate-release-utils-0.2.4 (c (n "release-utils") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("http-interop"))) (d #t) (k 0)))) (h "0blj6rwdd6rjc1l77bipd46dz126a3bhpmzy75p88d1x3h9rp5cj") (r "1.70")))

(define-public crate-release-utils-0.3.0 (c (n "release-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "ureq") (r "^2.8.0") (f (quote ("http-interop"))) (d #t) (k 0)))) (h "1y9jm5118say3221jws9zjnmaz23mx8fbsc2rfjlmk6m7zwj35yf") (r "1.70")))

(define-public crate-release-utils-0.4.0 (c (n "release-utils") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)) (d (n "ureq") (r "^2.8.0") (f (quote ("http-interop"))) (d #t) (k 0)))) (h "0k51iyxsfypvxcml23pap8wmmhaxxkspq8534814w07dm9d3ma8a") (r "1.70")))

(define-public crate-release-utils-0.4.1 (c (n "release-utils") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)) (d (n "ureq") (r "^2.8.0") (f (quote ("http-interop"))) (d #t) (k 0)))) (h "1krw6vbd5dbl5mndcqw57zy6y58s3xig8m6ahgvsj7czgl42x4h2") (r "1.70")))

(define-public crate-release-utils-0.5.0 (c (n "release-utils") (v "0.5.0") (d (list (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1a34i8g57qnjb3k1qbl5ix96152ggav1h7zymwxjf2jzsrfn5djj") (r "1.70")))

