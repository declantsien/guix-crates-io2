(define-module (crates-io re le relex) #:use-module (crates-io))

(define-public crate-relex-0.1.0 (c (n "relex") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ah7xil2r1g900krsm20jxm56r9r3dq98fmi6bsr6mkgqqzfi50f")))

(define-public crate-relex-0.1.1 (c (n "relex") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1iaqrikzn6scgid1906mxc0msf3cy84fy9jwhdkdn6rl0jrwdqz9")))

(define-public crate-relex-0.1.2 (c (n "relex") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "06gfj053ywik447sf3mwh861lis1a8y2l1zwsqbbfvv102w57xz9")))

(define-public crate-relex-1.0.0 (c (n "relex") (v "1.0.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1g6kiwpcq159qilzrbiyb3yqjbjyyhil0pzsach2zlr42iy7fskg")))

