(define-module (crates-io re le release-experiment-inner-crate) #:use-module (crates-io))

(define-public crate-release-experiment-inner-crate-0.1.2 (c (n "release-experiment-inner-crate") (v "0.1.2") (h "0g2k8qnbfd69ijyhqj8w0rd9dafgphraf7c4s4k0qib94s20hihg")))

(define-public crate-release-experiment-inner-crate-0.3.3 (c (n "release-experiment-inner-crate") (v "0.3.3") (h "1hrc3c19lyrxdpl8c3sw0zd370604pz86xq6d7l6c7rakfgrq065")))

