(define-module (crates-io re le relexer) #:use-module (crates-io))

(define-public crate-relexer-0.1.0 (c (n "relexer") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)) (d (n "relexer-derive") (r "^0.1") (d #t) (k 0)))) (h "0lhjnx6lj0i6pmcam01nm3immn644b2zwwws12q2g85bkgk2s68k")))

