(define-module (crates-io re dd reddit-search) #:use-module (crates-io))

(define-public crate-reddit-search-0.1.0 (c (n "reddit-search") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0m301xq79z7z37fqqspmff6jfzzvc7vpq07fxs1vy1szdibcp6hm")))

(define-public crate-reddit-search-0.2.0 (c (n "reddit-search") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0q4kvgz6g2sl4g6ma1ndpmsgm3awmqc3id4ivkad18agc12vd0bf")))

(define-public crate-reddit-search-0.2.1 (c (n "reddit-search") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0hd3gchs2ibaxq2ww4wv3krsxaysl1m29s6wa17rpwm6yjp6brs2")))

(define-public crate-reddit-search-0.2.2 (c (n "reddit-search") (v "0.2.2") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "07yr9anr9gcpy3fjis7hs9hz8kkagz1npjgchvp829m7pcjchpiq")))

(define-public crate-reddit-search-0.3.0 (c (n "reddit-search") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1i4c2v5pp0l495isaim5z3rnvq5qcfx934c6d0bdmjv3ny1j93k1")))

(define-public crate-reddit-search-0.4.0 (c (n "reddit-search") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1hd2l0hmn3v1yazhc5dv278rbywvnpgvl46a79bfya1jnj9qnnri")))

(define-public crate-reddit-search-0.5.0 (c (n "reddit-search") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0h3d5xfa55lg4ha98788vpvmg41x7hkdssbpxi2m8spdka9ici6y")))

(define-public crate-reddit-search-0.6.0 (c (n "reddit-search") (v "0.6.0") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0nvs4pgwlq0w55f1kvccig89qj1ywppd4bspyq9s56an0balk6s1")))

(define-public crate-reddit-search-0.6.1 (c (n "reddit-search") (v "0.6.1") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "12qyzgdvsg6hcv90yg8qn3fxq06kpmh1z44mj6b5hzhziw9mmbcf")))

(define-public crate-reddit-search-0.6.2 (c (n "reddit-search") (v "0.6.2") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "13slxnna0rb0741c2lkr1j60d1c3619v8x3jdris931ra4p13s2f")))

(define-public crate-reddit-search-0.6.3 (c (n "reddit-search") (v "0.6.3") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0z1nfyc73jggribk480kwvgd51wxds43ddrb00k61l1m0mkwy6hd")))

(define-public crate-reddit-search-0.6.4 (c (n "reddit-search") (v "0.6.4") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0s91ybs3rz11xs3axa1li1i952sj5jwncp2g8g337d0xdam5nd1r")))

(define-public crate-reddit-search-0.6.6 (c (n "reddit-search") (v "0.6.6") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0bx4wsi3k7x3iay8p03ywrgkkx129c33pharrbfayk5yxc0fs6kh")))

(define-public crate-reddit-search-0.6.7 (c (n "reddit-search") (v "0.6.7") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "13vflrm70jjykf6c59bpmllj9vfnb0xwbmf7gww3ljsqn93p8vra")))

(define-public crate-reddit-search-0.6.8 (c (n "reddit-search") (v "0.6.8") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "110qx8yz0b01sc57r9r3gwrhq1hpccphjqls3akqwir2xm5hwh8k")))

(define-public crate-reddit-search-0.6.9 (c (n "reddit-search") (v "0.6.9") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0w4385s1jlf87axcs8ah86dlp3sq1rxrhc6dvra3x9ff75lrry3d")))

(define-public crate-reddit-search-0.6.10 (c (n "reddit-search") (v "0.6.10") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1zz67z66iax2afv7gc9pabvvyg6g017i2gvv2py8ggcir4amryp0")))

(define-public crate-reddit-search-0.6.11 (c (n "reddit-search") (v "0.6.11") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0v6rc5mk17xakjwaxrl500sx2rkcxnxfgdixrqynfpl6bbdcpfvr")))

(define-public crate-reddit-search-0.6.12 (c (n "reddit-search") (v "0.6.12") (d (list (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0b5skmsvp9gr5wz9vjd1dclmqrzkkgmslbg1aa88p7wvn1hkni6y")))

(define-public crate-reddit-search-0.7.0 (c (n "reddit-search") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0msi1x60xm7i0r8ndsckcf1wkz6spgq93m9c8w1jn6fmhpfg89d8")))

(define-public crate-reddit-search-0.7.1 (c (n "reddit-search") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1v97m2jidlbvckxr9czdcixrc80fdrf7wyy6v6qjdns5bpy406yb")))

(define-public crate-reddit-search-0.7.2 (c (n "reddit-search") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0gh6pvy0apafv40b4g4ryq9l7ss10hc8nlwb0z36jhxnrw01hqdl")))

(define-public crate-reddit-search-0.7.3 (c (n "reddit-search") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "1xmj89z6fszk0k239sixardzai4b4s8gi8ii264d9qqxjfbajxzj")))

(define-public crate-reddit-search-0.7.4 (c (n "reddit-search") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "05dd41d06bsv2j2h73hsbil6k5z089675ncvvw8ysnlhg214b9ll")))

(define-public crate-reddit-search-0.7.5 (c (n "reddit-search") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "0c1bbx4ipq1xzalh2y7av1aas5n091sbpcac7j1v7dpnv4yny8z3")))

(define-public crate-reddit-search-0.7.6 (c (n "reddit-search") (v "0.7.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "0wxlqr45rcpkzaxgjdw67gdpyjgksvc6672sxy9di02jjmd2fi0p")))

(define-public crate-reddit-search-0.8.0 (c (n "reddit-search") (v "0.8.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "15rvdcw0khpbk5b99lgpiq014y882wki0igi2w664wlgyqh4d7gj")))

(define-public crate-reddit-search-0.8.1 (c (n "reddit-search") (v "0.8.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "13c6z4rddrrlj0c2kfb67j17izw24z17i91p3nmgp7jb0q26pafs")))

(define-public crate-reddit-search-0.8.3 (c (n "reddit-search") (v "0.8.3") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "1x05hn8ik28nwlsn6rp9hpz1m94yjfgmqfspmp89hp6ndvg2p5mx")))

(define-public crate-reddit-search-0.8.4 (c (n "reddit-search") (v "0.8.4") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "18axgcgfj4rfw5gsq7yk8vb4xb05jk3fm50vky8mj18413bdf91l")))

(define-public crate-reddit-search-0.9.0 (c (n "reddit-search") (v "0.9.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "09z6677vjpddkl18qhx6dhxpvd6rnd2a5ag9x0qpd9v0qsh52nhn")))

(define-public crate-reddit-search-0.9.1 (c (n "reddit-search") (v "0.9.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "1dqm9nhp0h49x9fk865nfzw287akbw4d9nbp2xxdhdi00m59hfha")))

(define-public crate-reddit-search-0.9.2 (c (n "reddit-search") (v "0.9.2") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "0hx0xkg7q5ajqdr9n5iik139qr2hvcqqy6vddpmnvl4qhz84gz2c")))

(define-public crate-reddit-search-0.10.0 (c (n "reddit-search") (v "0.10.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "0jzw3dwjajaqvzrmcy3glqh5b9wr38v327sd4812n2y1793jsf7p")))

