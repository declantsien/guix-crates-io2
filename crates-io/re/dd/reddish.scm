(define-module (crates-io re dd reddish) #:use-module (crates-io))

(define-public crate-reddish-0.0.0 (c (n "reddish") (v "0.0.0") (h "12c8j9wv64rd5f6z46225ynbmk6bb6qsys4v2xq0km8szdhqwvja")))

(define-public crate-reddish-0.1.0 (c (n "reddish") (v "0.1.0") (h "0vjjgdfxiaykb046cxng05s343v9174493syxfhbhmm3lf6925sk")))

(define-public crate-reddish-0.2.0 (c (n "reddish") (v "0.2.0") (h "06gz26mjywp3i4gv3vl6srf5xprh9rca1cyiilqhhqg19p6sq9k4")))

