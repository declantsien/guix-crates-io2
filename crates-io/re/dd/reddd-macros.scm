(define-module (crates-io re dd reddd-macros) #:use-module (crates-io))

(define-public crate-reddd-macros-0.1.0 (c (n "reddd-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01g2ddxsg3bc6zrl19czazc9vbn275909by4mzwrg3clb178ib0s")))

(define-public crate-reddd-macros-0.2.0 (c (n "reddd-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qjrh5hqs9rz0cibmm0dim1kka1a146x2m45npwhqz2yzlz7zdm3")))

(define-public crate-reddd-macros-0.2.1 (c (n "reddd-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "084h0a2m5rllpvbpjnhsh5dvikrs5gssk4y6v1qzdig3x0jnbsd4")))

