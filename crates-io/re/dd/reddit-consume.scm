(define-module (crates-io re dd reddit-consume) #:use-module (crates-io))

(define-public crate-reddit-consume-0.1.1-rc.2 (c (n "reddit-consume") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "casey") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mpvipc") (r "^1") (d #t) (k 0)) (d (n "roux") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "0mm1b8ywmrfh6v5wvkk222nxfw5q8lidik6b0fx0z8q5v1jnai82")))

