(define-module (crates-io re cs recs) #:use-module (crates-io))

(define-public crate-recs-1.0.0 (c (n "recs") (v "1.0.0") (h "0xwllfzx16g980f9dhhkc38wjiy34dv4zsjsfmc6ajlab3nbv5f0") (y #t)))

(define-public crate-recs-1.0.1 (c (n "recs") (v "1.0.1") (h "0dxxx4qzhsc69lnnl3hf8yn7gbs4i5sb7k0808ach4irbl77y7b5")))

(define-public crate-recs-1.1.0 (c (n "recs") (v "1.1.0") (h "0dx78dmqnmaalzxq8hgail303jjmbs72b7l0bnzjw6s1hh8435sd")))

(define-public crate-recs-2.0.0 (c (n "recs") (v "2.0.0") (h "17kxdrxib65hjwhxggcablqrl30arpma1k1inzvlc8abx3rgzhlp")))

(define-public crate-recs-2.0.1 (c (n "recs") (v "2.0.1") (h "1nk0ygj13rgybq00gs5bgc3mjj8hqp0nqw9jxvlp5pzpy4hvjzxf")))

