(define-module (crates-io re cs recstrm) #:use-module (crates-io))

(define-public crate-recstrm-0.0.1 (c (n "recstrm") (v "0.0.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "testtools") (r "^0.1.3") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1cx1l3qbz52msgks8rbizv49n2yvr0yyqpzjpg2kag7psp5ynxp2") (r "1.56")))

