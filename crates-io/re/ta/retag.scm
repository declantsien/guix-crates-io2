(define-module (crates-io re ta retag) #:use-module (crates-io))

(define-public crate-retag-0.0.1 (c (n "retag") (v "0.0.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "16jchib39qf132x50y4dprcv89fj9qj0jqbq12bxbdj5zwycvqmq")))

(define-public crate-retag-0.0.2 (c (n "retag") (v "0.0.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "0kvkig24xd0wvlpfc63mvlcjpc2cmcbgk7nm0mq2wv4zyknylzb5")))

(define-public crate-retag-0.0.3 (c (n "retag") (v "0.0.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "19xvp33px1840iwkmmn3v8g32cpv4sbmlaiqzrhg1hbsxc6yzncs")))

