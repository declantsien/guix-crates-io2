(define-module (crates-io re ta retaker) #:use-module (crates-io))

(define-public crate-retaker-0.1.0 (c (n "retaker") (v "0.1.0") (d (list (d (n "anylist") (r "^0.2.2") (d #t) (k 0)))) (h "0sm2gksdqws6ndrvwq85nxaa4shf6p7l56ldib053ggajp1r0dwj") (r "1.56")))

(define-public crate-retaker-0.1.1 (c (n "retaker") (v "0.1.1") (d (list (d (n "anylist") (r "^0.2.3") (d #t) (k 0)))) (h "1kka4y896v1w510zfw86iyaph30vc7p0az0nf2xq1ii7s9rkszbh") (r "1.56")))

(define-public crate-retaker-0.1.2 (c (n "retaker") (v "0.1.2") (d (list (d (n "anylist") (r "^0.2.3") (d #t) (k 0)))) (h "0a9bjdaw7h52w4nvlv0wsli6j4xcbsvpf08mr22yrj1hddmyg5zg") (r "1.56")))

