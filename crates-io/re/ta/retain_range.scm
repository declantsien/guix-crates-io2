(define-module (crates-io re ta retain_range) #:use-module (crates-io))

(define-public crate-retain_range-0.1.0 (c (n "retain_range") (v "0.1.0") (h "0nqjv7wn3wss2bizmx9ynzplvbdfz1si1dial7vhcy4f995m40sf")))

(define-public crate-retain_range-0.1.1 (c (n "retain_range") (v "0.1.1") (h "0k7k84cazxg9aanxwq6kkq5x20469dx03b1f6r9dv60n3ilj9b72")))

