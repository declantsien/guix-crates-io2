(define-module (crates-io re sa resa) #:use-module (crates-io))

(define-public crate-resa-0.1.0 (c (n "resa") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "extractor") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "1l1mbg5zyfq9ld5l9qkykxv6r39vd0sq55lb7gl1a19ffcz6331c") (y #t)))

(define-public crate-resa-0.1.1 (c (n "resa") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "extractor") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "1pd07ni11ki41y0f0mglr6wzps26q9770f7lq1h52h5pwqw0p483")))

