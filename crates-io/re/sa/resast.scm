(define-module (crates-io re sa resast) #:use-module (crates-io))

(define-public crate-resast-0.1.0 (c (n "resast") (v "0.1.0") (h "1pdwb65l4kj5zj7m3wvqibqgpiysihpyscy5wj76a4qpqvd77rzd")))

(define-public crate-resast-0.2.0 (c (n "resast") (v "0.2.0") (h "12pp7xry8kamgjsxfcpwcnslhqykb3vabrkf4v264agvqcxxjjsp")))

(define-public crate-resast-0.2.1 (c (n "resast") (v "0.2.1") (h "147b2q7likjbwnw0qw2dbgdc5jvxl9k5h12ixdcf7lj0wzcif64a")))

(define-public crate-resast-0.3.0 (c (n "resast") (v "0.3.0") (h "1rdiybcn9dgycsxjz4dlp43b0rgc8d879svdiwx3acg09h6nx0ss")))

(define-public crate-resast-0.4.0-beta-1 (c (n "resast") (v "0.4.0-beta-1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "021ywahkwpb2bhyp1iz5c8gjs5qf8b7pxarpj1200jrfv2fn4ib1")))

(define-public crate-resast-0.4.0-beta-2 (c (n "resast") (v "0.4.0-beta-2") (d (list (d (n "ressa") (r "^0.7.0-beta-1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "1qb38vc2wghj2ipn32m8456wkn7z58dkxj4xy2xrbprx0j0sm2pr")))

(define-public crate-resast-0.4.0-beta-3 (c (n "resast") (v "0.4.0-beta-3") (d (list (d (n "ressa") (r "^0.7.0-beta-4") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "04dhnhxvp57zyf8qh7cwjvdphjrlcd4hns7jsng2clwxkfjzl473")))

(define-public crate-resast-0.4.0-beta-4 (c (n "resast") (v "0.4.0-beta-4") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1r4xc5di573ica0y5bvbwlzcpx1lhzm309n5mj1kxyh119vrbnl3") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.4.0 (c (n "resast") (v "0.4.0") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0srmgkaiy5ygqgwpawc5zn7cvcs9z0qfgml3c9pdgvi71xwvw94m") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default")))) (y #t)))

(define-public crate-resast-0.4.1 (c (n "resast") (v "0.4.1") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1dfxksjsgfanqmfn0gwzklq90bp8gar4vyh614r7py8wnyi2cf17") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.5.0-alpha.1 (c (n "resast") (v "0.5.0-alpha.1") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gdnavxs8mygpm6fm4h79bapdp7k36q8yxm9fvgvnx2yz0fwazf0") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.5.0-alpha.2 (c (n "resast") (v "0.5.0-alpha.2") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yhfmksq432vqhzjmi65k4m5ixcjvskzzfjbvyzp5cy7j42rgcfb") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.5.0-alpha.3 (c (n "resast") (v "0.5.0-alpha.3") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "07z9hf0klqg8iiag2slq03zrb5pjxf4rr807gd06b8xsjvxg3l4m") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.5.0 (c (n "resast") (v "0.5.0") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09q83z7i4dmlazmgiq3j784yq0a075nzv0y8ylnxs6azp7wiiq6w") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.6.0-alpha.1 (c (n "resast") (v "0.6.0-alpha.1") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0hk7lhm9lb92c2pkdadyqm2wrbapahr6i00avazd6falbmr16a92") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.6.0-alpha.2 (c (n "resast") (v "0.6.0-alpha.2") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "18mx9m3c61183nwy82fvl2379hzpan72ffdc00ssrqd6kb2r6z2q") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.6.0-alpha.3 (c (n "resast") (v "0.6.0-alpha.3") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "ressa") (r "^0.7.0-beta-6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0r0w82jv7f1rdl7mvy8cmjm1i161c8i6zjwxwfdllaa25xkdq0x2") (f (quote (("serialization" "serde" "serde_derive") ("esprima" "serialization") ("default"))))))

(define-public crate-resast-0.6.0-alpha.4 (c (n "resast") (v "0.6.0-alpha.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gjkcac6vr4gi4h8ay3376vdinpv0lhhs93l8jisfnliyv9l7bgh") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-resast-0.6.0-alpha.5 (c (n "resast") (v "0.6.0-alpha.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y49y9mn7cgfdmfv0328j9bfapbj6jfbsj0v9q2hgpfdhkz30gmn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-resast-0.6.0-alpha.6 (c (n "resast") (v "0.6.0-alpha.6") (h "1za2k1m9cflhw82498k1jzg4sqsypg9ibf5k1zfy4xzhfbp9vkfp")))

