(define-module (crates-io re si resize_slice2) #:use-module (crates-io))

(define-public crate-resize_slice2-0.1.0 (c (n "resize_slice2") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "19z5w2pi9g5306mdl92ka76ks83z4glxrb29h2ra7hkli642sxr6")))

(define-public crate-resize_slice2-0.1.1 (c (n "resize_slice2") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0cqppiri2rhz6agdgf2y4xgdlmrfvl7zp2473xhiiwwnba0iwxrr")))

(define-public crate-resize_slice2-0.1.2 (c (n "resize_slice2") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "07pbvxv7lyi5fyj5sxw426h4m5xcfb7rmad0qqa34rfdk1j5ja22")))

(define-public crate-resize_slice2-0.2.0 (c (n "resize_slice2") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "04pi1w4jnxxr3vkwl7qgd751yc14071jyapib47sh4nip9hdp27q")))

