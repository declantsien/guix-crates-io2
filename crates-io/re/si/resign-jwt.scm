(define-module (crates-io re si resign-jwt) #:use-module (crates-io))

(define-public crate-resign-jwt-0.1.0 (c (n "resign-jwt") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "frank_jwt") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0a1118jl53r6dqz497gxcnwm2pnrpnmyhb3cvwipf1hjkhh52rfw")))

