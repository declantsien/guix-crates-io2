(define-module (crates-io re si resizr) #:use-module (crates-io))

(define-public crate-resizr-0.1.0 (c (n "resizr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "0nyxd6cvn3cz5ixh0bsdvwm6dhhprmw5ga3wh04ddj4j0ri3rg82")))

(define-public crate-resizr-0.1.1 (c (n "resizr") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "1zsibspdgijgmxbpz1g5dyvlvc3y7rgcq859p4vik8g5rivpfnma")))

