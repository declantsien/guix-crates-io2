(define-module (crates-io re si resistor) #:use-module (crates-io))

(define-public crate-resistor-0.0.0 (c (n "resistor") (v "0.0.0") (h "0cg8sfdpxsrkpabxldc36893zk3dgrqcgdhjbkfyvyc9mj32mv2q") (f (quote (("default"))))))

(define-public crate-resistor-0.1.0 (c (n "resistor") (v "0.1.0") (h "0lsc1f67fmxyz0qnbrwz3ky0viril5zbnf06rdrkmc0acqghj3jv") (f (quote (("default"))))))

