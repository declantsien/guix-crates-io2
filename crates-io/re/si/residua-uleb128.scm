(define-module (crates-io re si residua-uleb128) #:use-module (crates-io))

(define-public crate-residua-uleb128-0.1.0 (c (n "residua-uleb128") (v "0.1.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1bkp202rh1w0h95jjmkdxdw52kxm9rwyiam9jz8f25a0gwqangjh")))

(define-public crate-residua-uleb128-0.2.0 (c (n "residua-uleb128") (v "0.2.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1d7nw8a20pfk8h0famhx1mp00pv3af7by5g0wzyqwhb4imbjib6y")))

