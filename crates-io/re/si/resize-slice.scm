(define-module (crates-io re si resize-slice) #:use-module (crates-io))

(define-public crate-resize-slice-0.0.1 (c (n "resize-slice") (v "0.0.1") (h "0mbh60gi79fzh7wf5m0ww2iqhwv87dlnd95cg6llw0nxxwkimh1k") (f (quote (("unstable"))))))

(define-public crate-resize-slice-0.1.0 (c (n "resize-slice") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "1b7cysf24bvmmqj9kkjd81ndl03yzri4vv0zx51sx3f1hjkf2sdn") (f (quote (("unstable"))))))

(define-public crate-resize-slice-0.1.2 (c (n "resize-slice") (v "0.1.2") (d (list (d (n "smallvec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "0835hxmy2mfwdjini71n87r78110az3zqhfbn5ilyvf809m3xvrx")))

(define-public crate-resize-slice-0.1.3 (c (n "resize-slice") (v "0.1.3") (d (list (d (n "smallvec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "0jlc614745fvf7ykxfz379n4cnx2d79crq5rb1lyg4cq9bvv4g4a")))

