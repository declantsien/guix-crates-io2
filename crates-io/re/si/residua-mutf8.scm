(define-module (crates-io re si residua-mutf8) #:use-module (crates-io))

(define-public crate-residua-mutf8-0.1.0 (c (n "residua-mutf8") (v "0.1.0") (d (list (d (n "residua-cesu8") (r "^0.1") (d #t) (k 0)))) (h "1iwldizswcrzg1zhx45cgpaszrax80j431qwc9lfsj2c5dzd9dlf")))

(define-public crate-residua-mutf8-0.3.0 (c (n "residua-mutf8") (v "0.3.0") (d (list (d (n "residua-cesu8") (r "^0.3.0") (d #t) (k 0)))) (h "1fbq228w7s2br6ms4fmap8k0qvjv4ql35i7xfcnsyxffx8nhgb4j")))

(define-public crate-residua-mutf8-0.4.0 (c (n "residua-mutf8") (v "0.4.0") (d (list (d (n "residua-cesu8") (r "^0.4.0") (d #t) (k 0)))) (h "1sfz276wc73m2i6p963k1bgrx85kpamwjwzf60jbl0f2gq23hr81")))

(define-public crate-residua-mutf8-0.4.1 (c (n "residua-mutf8") (v "0.4.1") (d (list (d (n "residua-cesu8") (r "^0.4.1") (d #t) (k 0)))) (h "1m6b65m2k811yi5czxbhzxbk0yxpnm09cjdwzkda2dmjzsczpzx4")))

(define-public crate-residua-mutf8-0.4.2 (c (n "residua-mutf8") (v "0.4.2") (d (list (d (n "residua-cesu8") (r "^0.4") (d #t) (k 0)))) (h "0wvs7k38cf2va5i801xaphz38g4fl68cj1xzpq4pi85kh2fan1g1")))

(define-public crate-residua-mutf8-0.5.0 (c (n "residua-mutf8") (v "0.5.0") (d (list (d (n "residua-cesu8") (r "^0.6.0") (d #t) (k 0)))) (h "1707l3dqg4248lbsxcf119dnlg0ahgs54im21cvp3zr4520alp0c")))

(define-public crate-residua-mutf8-1.0.0 (c (n "residua-mutf8") (v "1.0.0") (d (list (d (n "residua-cesu8") (r "^1") (d #t) (k 0)))) (h "09h9zvs9m6xfbm8skqjhrhq40yx1y22vl0lx89mn5fm6wlzfw5w3")))

(define-public crate-residua-mutf8-2.0.0 (c (n "residua-mutf8") (v "2.0.0") (d (list (d (n "residua-cesu8") (r "^2") (d #t) (k 0)))) (h "03b3bq1izpchdc657x6cxa3qg0agg8v9q7kdmmyhwllfli1sinra") (f (quote (("std") ("default" "std"))))))

