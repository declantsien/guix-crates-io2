(define-module (crates-io re si resizing-vec) #:use-module (crates-io))

(define-public crate-resizing-vec-0.1.0 (c (n "resizing-vec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "04r3130zp4hz8vk6x81l8l8x7gi98zrjf2zm2fwjajqym1dgb48k")))

(define-public crate-resizing-vec-0.1.1 (c (n "resizing-vec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0mpyw0pf25mnma29fgbdfa2bmh1dbybv2v26wbp62i7zqrsqss1m")))

(define-public crate-resizing-vec-0.1.3 (c (n "resizing-vec") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "118c24miwhmn7ssqsnajv43l33habgaf15kyfyhszrjb86lfyqzk")))

(define-public crate-resizing-vec-0.1.4 (c (n "resizing-vec") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "10dcri5kdxjdipw3icah8z4xqb1dfbk4cizybiq26rmbm9yjv3j4")))

