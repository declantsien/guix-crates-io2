(define-module (crates-io re si resize) #:use-module (crates-io))

(define-public crate-resize-0.0.1 (c (n "resize") (v "0.0.1") (d (list (d (n "png") (r "^0.3") (d #t) (k 2)))) (h "1lx275kz0dwkd36a70n7czyhwzgjsxn67dxr2079n5nahk033bg2") (y #t)))

(define-public crate-resize-0.1.0 (c (n "resize") (v "0.1.0") (d (list (d (n "png") (r "^0.3") (d #t) (k 2)))) (h "1rc3kwydn76gcf8w80sam7cxja1xlvgf9amy2ndldsqfrapgvdg6") (y #t)))

(define-public crate-resize-0.2.0 (c (n "resize") (v "0.2.0") (d (list (d (n "png") (r "^0.3") (d #t) (k 2)))) (h "13k07dhsvrghl92921285dnv0jwbf595070y2bnpd5ypyif5r3ry") (y #t)))

(define-public crate-resize-0.3.0 (c (n "resize") (v "0.3.0") (d (list (d (n "png") (r "^0.6") (d #t) (k 2)))) (h "1f4zg009n4xb1pq046wdax0ijr0rx3s9cbci27n1krqrqa46gl8f") (y #t)))

(define-public crate-resize-0.3.1 (c (n "resize") (v "0.3.1") (d (list (d (n "png") (r "^0.15") (d #t) (k 2)))) (h "1ai83laz5yvg4r7z9q8d1m0vq0fqj2ycyprw5fxzxyzaj3im7rmr")))

(define-public crate-resize-0.4.0 (c (n "resize") (v "0.4.0") (d (list (d (n "png") (r "^0.16.1") (d #t) (k 2)))) (h "1rdnaja7hy450gq33n9va072ysyy4hsxylwb7qaa4fn05k1854f1") (y #t)))

(define-public crate-resize-0.4.1 (c (n "resize") (v "0.4.1") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "0ggf37wfm1a4q66iism8kf8chl3bfvnmspcs7ial0ph0p3346ki6") (y #t)))

(define-public crate-resize-0.4.2 (c (n "resize") (v "0.4.2") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "117gfdjf2xm2k9xl73jh260bnc6048mj3inrgrqlvp2i3h8z6nii") (y #t)))

(define-public crate-resize-0.5.0-alpha.1 (c (n "resize") (v "0.5.0-alpha.1") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.22") (d #t) (k 0)))) (h "0vngs02qfmsfd7piy8kn4vq0sbyhjn2rj3564brch6499f9v0gba") (y #t)))

(define-public crate-resize-0.4.3 (c (n "resize") (v "0.4.3") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "0bamrw2m37l8q46mcy6snp6106d93dq7x67hbbj32w88pjdhxn84")))

(define-public crate-resize-0.5.0-alpha.2 (c (n "resize") (v "0.5.0-alpha.2") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.22") (d #t) (k 0)))) (h "0bwiip10gfwl3bjqn1j6ic1771km0xv4y6gbas5bahjgvcpb431x") (y #t)))

(define-public crate-resize-0.5.0-beta.1 (c (n "resize") (v "0.5.0-beta.1") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.22") (d #t) (k 0)))) (h "1gvc2xbqyjjz18vxxl0rlcpj0i95kqy6wqk3acklqv6d1srbppq2") (y #t)))

(define-public crate-resize-0.5.0-beta.2 (c (n "resize") (v "0.5.0-beta.2") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.22") (d #t) (k 0)))) (h "19vgbff79qzwzg9c030lxz32z3pws91gn3vzh1zypqc5qkfi0b01") (y #t)))

(define-public crate-resize-0.5.0 (c (n "resize") (v "0.5.0") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "0v8g11d5s0ry8w11yfwsy2hgwj3rr4ma0wz20qdrqnicbiar60h4") (y #t)))

(define-public crate-resize-0.5.1 (c (n "resize") (v "0.5.1") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "1lk7y6h0wx6whzgxja1mlry6d78g8ryw1rrk22x61il07q4mmfhx") (y #t)))

(define-public crate-resize-0.5.2 (c (n "resize") (v "0.5.2") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "0fc9vgkzvxyxdx1bdpi8n9f6cx87bhyrc1ail9r0ybn60abfszl2") (y #t)))

(define-public crate-resize-0.5.3 (c (n "resize") (v "0.5.3") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "0f6h7m3hpzc1dfg8qw9y7lm32jbvvfxl5n8677py57bsx63xbm2h") (y #t)))

(define-public crate-resize-0.5.4 (c (n "resize") (v "0.5.4") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "15qm7dxd53sz3brwiisb12xydxp1rcakp4yi0pq75c6kcmdbjgjm")))

(define-public crate-resize-0.5.5 (c (n "resize") (v "0.5.5") (d (list (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "0achcqlpyw8a0q2hrifs1n4hfixmp174qjan0b04ss46x918r87j")))

(define-public crate-resize-0.6.0 (c (n "resize") (v "0.6.0") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "1xp2yhrff1lx9q6h7p3gh2x23387c2y7f7s0xfzv5q0wm280s11q") (y #t)))

(define-public crate-resize-0.6.1 (c (n "resize") (v "0.6.1") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "17zdvsd0cq5q812lz27n3rwgdrmk1nv1xd6v112y82b5h1bi7fj2")))

(define-public crate-resize-0.6.2 (c (n "resize") (v "0.6.2") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "05vsb3210bjn55k0j25kyv3d66j6jzld398pgyx9sq7z67vqsymk")))

(define-public crate-resize-0.7.0 (c (n "resize") (v "0.7.0") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "1y6aqaf17ga4mnhkc823bdsng4x90bjdyj60i7xj01ndvlbliy9h")))

(define-public crate-resize-0.7.1 (c (n "resize") (v "0.7.1") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "17sfm26wmcr3nzgwpqp4xsz502h5r493bq11yf7cpj5iar97ivc2") (y #t)))

(define-public crate-resize-0.7.2 (c (n "resize") (v "0.7.2") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.24") (d #t) (k 0)))) (h "1vxlj9nqaspj9nw5hkhvc7acvqpzfwd0xp3lxs8gckfdlcsj38x2")))

(define-public crate-resize-0.7.3 (c (n "resize") (v "0.7.3") (d (list (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.31") (d #t) (k 0)))) (h "046k2mi1lw3aiddc1mx6ishzs1hmp3fsai4lwwxi4vb6g3kx0pnh")))

(define-public crate-resize-0.7.4 (c (n "resize") (v "0.7.4") (d (list (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)))) (h "0hdd5r2m1700y6r88v5hq3q28xixrsbfhbzqz26409jyy3zvvrw7") (r "1.57")))

(define-public crate-resize-0.8.0 (c (n "resize") (v "0.8.0") (d (list (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "1l4r8k9dqbdrm29p37ivldvwl9hglrgfc32cxwwk1cz1h1rg238w") (f (quote (("default" "rayon")))) (r "1.57")))

(define-public crate-resize-0.8.1 (c (n "resize") (v "0.8.1") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (k 0)))) (h "0zphjvan364xg7p4kjarclqls9x3733wvxcvv0i9r2jn008kiv1q") (f (quote (("std") ("default" "std" "rayon")))) (s 2) (e (quote (("rayon" "dep:rayon" "std") ("no_std" "dep:libm" "dep:hashbrown")))) (r "1.60")))

(define-public crate-resize-0.8.2 (c (n "resize") (v "0.8.2") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (k 0)))) (h "1j4prbjga4nwvhhp15c2r2irnj81xs4zy86140x7kx7g4013rr4c") (f (quote (("std") ("default" "std" "rayon")))) (s 2) (e (quote (("rayon" "dep:rayon" "std") ("no_std" "dep:libm" "dep:hashbrown")))) (r "1.60")))

(define-public crate-resize-0.8.3 (c (n "resize") (v "0.8.3") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (k 0)))) (h "1qrd4dbnwidjs4n96df8cqjsjlp5wjxwv6k23jhl62zsihn6fslv") (f (quote (("std") ("default" "std" "rayon")))) (s 2) (e (quote (("rayon" "dep:rayon" "std") ("no_std" "dep:libm" "dep:hashbrown")))) (r "1.60")))

(define-public crate-resize-0.8.4 (c (n "resize") (v "0.8.4") (d (list (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (k 0)))) (h "105hlp9c2bpx0qw37hm3w3cwiidfszc0pvifbrn3ka079ic9zqn3") (f (quote (("std") ("default" "std" "rayon")))) (s 2) (e (quote (("rayon" "dep:rayon" "std") ("no_std" "dep:libm" "dep:hashbrown")))) (r "1.60")))

