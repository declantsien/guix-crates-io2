(define-module (crates-io re si resistor-calc) #:use-module (crates-io))

(define-public crate-resistor-calc-0.1.0 (c (n "resistor-calc") (v "0.1.0") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "meval") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0z9l46r2a7mhwg7kp2xxnn21frhxqsmzdc3i7i95mr4zfg567ncw") (f (quote (("expr_builder" "meval") ("default" "expr_builder"))))))

