(define-module (crates-io re si residua-cesu8) #:use-module (crates-io))

(define-public crate-residua-cesu8-0.1.0 (c (n "residua-cesu8") (v "0.1.0") (h "0hqsa2hq4k3m2l53l9x02v6s7ax7mvqq24r92gzyr13vjwsnl06d") (y #t)))

(define-public crate-residua-cesu8-0.2.0 (c (n "residua-cesu8") (v "0.2.0") (h "03ygnqqxwh9q3rhs8q2yd88rr981nf40f70frhzlk4lw9xwz6vpp") (y #t)))

(define-public crate-residua-cesu8-0.3.0 (c (n "residua-cesu8") (v "0.3.0") (h "0n84jp440arb1k317a3x1dgya45b94r928798d07siyaz0kaqnkj") (y #t)))

(define-public crate-residua-cesu8-0.4.0 (c (n "residua-cesu8") (v "0.4.0") (h "0b7lgm705pb89lb6x8qzy9107yd572gwgldiq9d6x6nrdwmb84yj")))

(define-public crate-residua-cesu8-0.4.1 (c (n "residua-cesu8") (v "0.4.1") (h "0hxifx07wz6d2wdxvhxdrppah7j1f4ihc1lzlq52ip0k57ysjz06")))

(define-public crate-residua-cesu8-0.4.2 (c (n "residua-cesu8") (v "0.4.2") (h "04vz9xync8xbjbxb3rdqkzh8v53l7sjk6nc9smyw5n66v7hkx97f")))

(define-public crate-residua-cesu8-0.4.3 (c (n "residua-cesu8") (v "0.4.3") (h "0dgaa48p5w5yl2caqh7lyglkl4r170917a1xc8mw213qy730ah0v")))

(define-public crate-residua-cesu8-0.5.0 (c (n "residua-cesu8") (v "0.5.0") (h "1kblvs8zcvl0glgmra3g42a3k0x9k8vmf1pmqmbw228mr5sxym41")))

(define-public crate-residua-cesu8-0.5.1 (c (n "residua-cesu8") (v "0.5.1") (h "1gim7wqks5lwmj54h1iwaqrxxcd4mdc0ci3ilhnw81i6zdz99lfb")))

(define-public crate-residua-cesu8-0.5.2 (c (n "residua-cesu8") (v "0.5.2") (h "1ni366hv9f4y6m237k94h5i68gigq3n0aanqx8lwfmysbki2fjma")))

(define-public crate-residua-cesu8-0.6.0 (c (n "residua-cesu8") (v "0.6.0") (h "1i740v5pn1dp1dmzj5ls76fxa9wg4x4nhl39dayvmd0gps64jknk")))

(define-public crate-residua-cesu8-1.0.0 (c (n "residua-cesu8") (v "1.0.0") (h "0lwjs1v1pixqp6940q4jlpj673846npjm2dixmy4h422sgciw8ca")))

(define-public crate-residua-cesu8-2.0.0 (c (n "residua-cesu8") (v "2.0.0") (h "1siq56z0425wkp556h8l01970ipw3a42sq05andp2qcqbla9p8kc") (f (quote (("std") ("default" "std"))))))

