(define-module (crates-io re si resiter) #:use-module (crates-io))

(define-public crate-resiter-0.1.0 (c (n "resiter") (v "0.1.0") (h "1z73frvq7cqbdl7ymzxz72h3392550h3275sjfg4xy6wn2vpizi4")))

(define-public crate-resiter-0.2.0 (c (n "resiter") (v "0.2.0") (h "0fsa03632jw38d7w6i8sjy5xd9kj2yihk80acr8a0ja7qdiaffjg")))

(define-public crate-resiter-0.3.0 (c (n "resiter") (v "0.3.0") (h "114jrmnqfrycc6r1p12f2r0ad077yq5rb1cr745qfff2mpysgx1r")))

(define-public crate-resiter-0.4.0 (c (n "resiter") (v "0.4.0") (h "1gp19ms1ns6mp6j4rvbwf380d0ibh3ynpi5my1lpg2r5j0gansdx")))

(define-public crate-resiter-0.5.0 (c (n "resiter") (v "0.5.0") (h "0xmjs9580x82fdvfdp38h7k0xmkr1365jmwli29gcr8qxdb5vjfb")))

