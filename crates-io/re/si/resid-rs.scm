(define-module (crates-io re si resid-rs) #:use-module (crates-io))

(define-public crate-resid-rs-0.1.0 (c (n "resid-rs") (v "0.1.0") (h "1mav5qx1qabrwa55yrhs48diw7myy4a1qp7pqd495gkhwbg59l9h")))

(define-public crate-resid-rs-0.2.0 (c (n "resid-rs") (v "0.2.0") (h "1s62dk03ndm1vsq93i27m7vcvn8m6b4rxj6flzh90svc9pmnsxz2")))

(define-public crate-resid-rs-0.3.0 (c (n "resid-rs") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)))) (h "06vvscqi5jvvkw9wxm9hl41pdxdlrjp76j7zyj9mmvw8447mngms")))

(define-public crate-resid-rs-0.4.0 (c (n "resid-rs") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)))) (h "03fg411qgsg91vmrq28flk65637fp5aaila23rynqy5vpx86bpm6")))

(define-public crate-resid-rs-0.4.1 (c (n "resid-rs") (v "0.4.1") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)))) (h "0hi422fki69v17qyn1yi9n4af9l6k2ii37q9r42avkk1wcnxr5d9")))

(define-public crate-resid-rs-0.4.2 (c (n "resid-rs") (v "0.4.2") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)))) (h "1sg9b5g9fbw8vhspyyqcw0cv2p0vk46i1dzxhr6iz8qyl4191s6c")))

(define-public crate-resid-rs-0.5.0 (c (n "resid-rs") (v "0.5.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)))) (h "0yhgb2na6cjw5jj45b1i08j1yb5c4657m0qj9pm7sk8zh6nfw5nr")))

(define-public crate-resid-rs-0.6.0 (c (n "resid-rs") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0fidcb3pwb6i8c9f31gwclpawlr67jyd2cxzydamw42ccsn695qp")))

(define-public crate-resid-rs-0.7.0 (c (n "resid-rs") (v "0.7.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "11rcp1bsz58fnib563q8x5spir5403ya8fbgbh775rlf3vw8dd9b")))

(define-public crate-resid-rs-0.8.0 (c (n "resid-rs") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1zy4wndn86n38gvmjhsm78prkhpcz9nvmg8w846rq7xivk4d4s8z")))

(define-public crate-resid-rs-0.9.0 (c (n "resid-rs") (v "0.9.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1qiwlrn6vn69590acnk25hpsqz2v445jns48jk56i3dlddk2immg")))

(define-public crate-resid-rs-1.0.0 (c (n "resid-rs") (v "1.0.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "1055arj8dagplygvfgmz04rkkqa6sj9a9bvfwrzpjzn4bq861a0y") (f (quote (("std") ("default" "std"))))))

(define-public crate-resid-rs-1.0.1 (c (n "resid-rs") (v "1.0.1") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0h8x38a6z82rgkvdfvs4s9glw62sp4riab2m1wm01ap5kh21wxpk") (f (quote (("std") ("default" "std"))))))

(define-public crate-resid-rs-1.0.2 (c (n "resid-rs") (v "1.0.2") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0gr9hmw31ivax7fwqqapd372bp265npz2ar1l93h3sg49aali5hj") (f (quote (("std") ("default" "std"))))))

(define-public crate-resid-rs-1.0.3 (c (n "resid-rs") (v "1.0.3") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0fyi5s1s8cbz9c6fw21ghrwyb0yfhgpvrwl6bigf82kf7z2jxich") (f (quote (("std") ("default" "std"))))))

(define-public crate-resid-rs-1.0.4 (c (n "resid-rs") (v "1.0.4") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "1ascpc5qmkgsciidrk4paw7zyd3dxdl6c9yi7d8krz5nks8p79sz") (f (quote (("std") ("default" "std"))))))

(define-public crate-resid-rs-1.1.0 (c (n "resid-rs") (v "1.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1p3rv0rm7v80b6www0zdlvgkjrazj2bsf6g9gmlm9wsha15asn8v") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-resid-rs-1.1.1 (c (n "resid-rs") (v "1.1.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1slr654s57zikm862cshywv5jjp13b2as7cynznd6c9y44qhmky4") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

