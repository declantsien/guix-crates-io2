(define-module (crates-io re gi regiface-macros) #:use-module (crates-io))

(define-public crate-regiface-macros-0.1.1 (c (n "regiface-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nd9vq4mcsnyhi8m89wyif4kbzs7xd635sk3b70djiisn6vvin5q") (f (quote (("default"))))))

(define-public crate-regiface-macros-0.1.2 (c (n "regiface-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gk03sn4lhmn6v6k8pabsdcf8wjki3yw2q4q3j5rg92s3xscrcli") (f (quote (("default"))))))

