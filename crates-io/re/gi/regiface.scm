(define-module (crates-io re gi regiface) #:use-module (crates-io))

(define-public crate-regiface-0.1.0 (c (n "regiface") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cqibifri7dqxq18c7fwk036jrjkz4zxhcpqnxrl2gfbinwg8l5n") (s 2) (e (quote (("packed_struct" "dep:packed_struct"))))))

(define-public crate-regiface-0.1.1 (c (n "regiface") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ms51hh043sx21swzrnhvfp1049hqfzkyks6nxrq9vwazxs64grp") (s 2) (e (quote (("packed_struct" "dep:packed_struct"))))))

(define-public crate-regiface-0.1.2 (c (n "regiface") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (o #t) (d #t) (k 0)) (d (n "regiface-macros") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "087gv4xdyysadp8qa673s4lraixkafqv8zramvr87r205rzl17bb") (f (quote (("default")))) (s 2) (e (quote (("packed_struct" "dep:packed_struct"))))))

(define-public crate-regiface-0.1.3 (c (n "regiface") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (o #t) (k 0)) (d (n "regiface-macros") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lfqyxzha9ia126hmk5p59rvb6cwhjmw0bhw72j0l2v5hwwm3v4l") (f (quote (("default")))) (s 2) (e (quote (("packed_struct" "dep:packed_struct"))))))

(define-public crate-regiface-0.1.4 (c (n "regiface") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (o #t) (k 0)) (d (n "regiface-macros") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n37ak0rwy8rmgc3h9j8lhbqfghmvwvglh10w2kkzikaazfk2970") (f (quote (("default")))) (s 2) (e (quote (("packed_struct" "dep:packed_struct"))))))

