(define-module (crates-io re gi regi_object) #:use-module (crates-io))

(define-public crate-regi_object-0.1.0 (c (n "regi_object") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1rv7sbakbsnl86da7fp86axy3jmm50gmnjvmdl3xncg01nw40rcg")))

