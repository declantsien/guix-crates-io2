(define-module (crates-io re gi register-bits) #:use-module (crates-io))

(define-public crate-register-bits-0.1.0 (c (n "register-bits") (v "0.1.0") (h "191j2w2s6wsz7hqc1lnqzx0mgbm3myq91aqmcqabjxgllah5mxhz") (f (quote (("no-std") ("default" "32bit") ("all-regs" "8bit" "16bit" "32bit" "64bit") ("8bit") ("64bit") ("32bit") ("16bit"))))))

(define-public crate-register-bits-0.2.0 (c (n "register-bits") (v "0.2.0") (h "0fcv7v2j2983asny2mdgnwhlrc82qa5m7knhblw8vra866md4y3m") (f (quote (("no-std") ("default" "32bit") ("all-regs" "8bit" "16bit" "32bit" "64bit") ("8bit") ("64bit") ("32bit") ("16bit"))))))

(define-public crate-register-bits-0.2.1 (c (n "register-bits") (v "0.2.1") (h "1y7nmh32zalc3am4pjn2cpwnp9bwy9c1qxssfhv6synckgalmmay") (f (quote (("no-std") ("default" "32bit") ("all-regs" "8bit" "16bit" "32bit" "64bit") ("8bit") ("64bit") ("32bit") ("16bit"))))))

(define-public crate-register-bits-0.2.2 (c (n "register-bits") (v "0.2.2") (h "017107wq2x5111k9ss4ah827j3w6hkcxcl16100ykmzzmjql5d9f") (f (quote (("no-std") ("default" "32bit") ("all-regs" "8bit" "16bit" "32bit" "64bit") ("8bit") ("64bit") ("32bit") ("16bit"))))))

(define-public crate-register-bits-0.2.3 (c (n "register-bits") (v "0.2.3") (h "15vnwz6c9mxwgjc9jizyhs5zwmim6x4fagfwkbxpif9z8h2y4rgg") (f (quote (("no-std") ("default" "32bit") ("all-regs" "8bit" "16bit" "32bit" "64bit") ("8bit") ("64bit") ("32bit") ("16bit"))))))

