(define-module (crates-io re gi register-interface) #:use-module (crates-io))

(define-public crate-register-interface-0.1.0 (c (n "register-interface") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvky3k1zz4gim9l1h6apdb9bm9q8ad4zi7cf0hzqz652dlzgydk")))

