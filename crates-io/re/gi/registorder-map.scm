(define-module (crates-io re gi registorder-map) #:use-module (crates-io))

(define-public crate-registorder-map-0.1.0 (c (n "registorder-map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (k 2)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "11ccfdjpk2nx3400hv16rh34axf4pg6r0r3ksag5nxkb8ryzsj89")))

