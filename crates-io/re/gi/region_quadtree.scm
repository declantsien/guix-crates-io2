(define-module (crates-io re gi region_quadtree) #:use-module (crates-io))

(define-public crate-region_quadtree-0.1.0 (c (n "region_quadtree") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sgdw8l8n10f3xjlkfq0q3nh3zlc9mkqqsrsn0r1m073g7z8mwps")))

