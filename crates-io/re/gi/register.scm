(define-module (crates-io re gi register) #:use-module (crates-io))

(define-public crate-register-0.1.0 (c (n "register") (v "0.1.0") (d (list (d (n "tock-registers") (r "^0.1.0") (d #t) (k 0)))) (h "11602nxmafhw9w13wyprj3mpxix2ybd0qfsnshg3znvq5wlqyady")))

(define-public crate-register-0.1.1 (c (n "register") (v "0.1.1") (d (list (d (n "tock-registers") (r "^0.1.0") (d #t) (k 0)))) (h "0yb401cwh1g67np8a8i23bs7rn3v1nhz4kj4c9mk673yp91m560f")))

(define-public crate-register-0.2.0 (c (n "register") (v "0.2.0") (d (list (d (n "tock-registers") (r "^0.2.0") (d #t) (k 0)))) (h "1hbgqcgdd30qdz1s1apscxrz3398vl8zk4ksa95gz0hq1fn12yhm")))

(define-public crate-register-0.2.1 (c (n "register") (v "0.2.1") (d (list (d (n "tock-registers") (r "^0.2.0") (d #t) (k 0)))) (h "1nycsn97bpxfz41z9x1asxd14gj6cvfzrnbak0h5d7i9sav323z1")))

(define-public crate-register-0.3.0 (c (n "register") (v "0.3.0") (d (list (d (n "tock-registers") (r "^0.3.0") (d #t) (k 0)))) (h "12y2m7i0l1jc99sk3gll8bwb7zd1h7lyfylb5i9wdid69szsmhvy") (y #t)))

(define-public crate-register-0.3.1 (c (n "register") (v "0.3.1") (d (list (d (n "tock-registers") (r "^0.3.0") (d #t) (k 0)))) (h "1124vacvg9dmn2dc90nm7n04ixr1pafyikbbhvyxx5jk99iz0cmm") (y #t)))

(define-public crate-register-0.3.2 (c (n "register") (v "0.3.2") (d (list (d (n "tock-registers") (r "^0.3.0") (d #t) (k 0)))) (h "1yqbawrxa7gk7107l7cwhwq0lqz3yipc9821am8mk0x9r5nlmx50")))

(define-public crate-register-0.3.3 (c (n "register") (v "0.3.3") (d (list (d (n "tock-registers") (r "^0.3.0") (d #t) (k 0)))) (h "08sqs0lzxkli0m9ygavps37nd0cvd1vlvqd4pfw7zml1vvfvb6s6")))

(define-public crate-register-0.4.0 (c (n "register") (v "0.4.0") (d (list (d (n "tock-registers") (r "0.4.*") (d #t) (k 0)))) (h "1z77c43k5q8vf87jchj5scyz5cm7fidp9c9nkr94zsgssdm58jr7")))

(define-public crate-register-0.4.1 (c (n "register") (v "0.4.1") (d (list (d (n "tock-registers") (r "0.4.*") (d #t) (k 0)))) (h "0ma934b3qlapl8ifixg890dqsy49hxkkrkkp6wl0yzf6c76gg4kw")))

(define-public crate-register-0.4.2 (c (n "register") (v "0.4.2") (d (list (d (n "tock-registers") (r "0.4.*") (d #t) (k 0)))) (h "0mcfr5w31wj16dlx1zp6fsal5qm57dbhv1qzxnd7vakw4nfi87x5")))

(define-public crate-register-0.5.0 (c (n "register") (v "0.5.0") (d (list (d (n "tock-registers") (r "0.5.*") (d #t) (k 0)))) (h "00gl33id02jj1vzdpzc9xkpx2b7l0ikwmdmrxiq15j1vaaxc443f") (f (quote (("no_std_unit_tests" "tock-registers/no_std_unit_tests") ("default"))))))

(define-public crate-register-0.5.1 (c (n "register") (v "0.5.1") (d (list (d (n "tock-registers") (r "0.5.*") (d #t) (k 0)))) (h "05dqk3y1q74p71yla2rh63g8c7jaxxfbn13mlmhizlkpwjqabayy") (f (quote (("no_std_unit_tests" "tock-registers/no_std_unit_tests") ("default"))))))

(define-public crate-register-1.0.0 (c (n "register") (v "1.0.0") (d (list (d (n "tock-registers") (r ">=0.6.0, <0.7.0") (d #t) (k 0)))) (h "0ab8zzpb23lff9q47kw7j5gs6b802j00z63ap0bis6h3phygypqq") (f (quote (("no_std_unit_tests" "tock-registers/no_std_unit_tests") ("default"))))))

(define-public crate-register-1.0.1 (c (n "register") (v "1.0.1") (d (list (d (n "tock-registers") (r ">=0.6.0, <0.7.0") (d #t) (k 0)))) (h "0zjiakrdk7jws6yvlfq4b5y2xmmis4r1rxxvgwmi7slgqk08dwzs") (f (quote (("no_std_unit_tests" "tock-registers/no_std_unit_tests") ("default"))))))

(define-public crate-register-1.0.2 (c (n "register") (v "1.0.2") (d (list (d (n "tock-registers") (r "0.6.*") (d #t) (k 0)))) (h "0csl2d1ycqk8d5wjlpjcj7wh9iw199f7gzkc03swhz5b57g4g8pl") (f (quote (("no_std_unit_tests" "tock-registers/no_std_unit_tests") ("default"))))))

