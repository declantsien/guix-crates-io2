(define-module (crates-io re gi regigigas) #:use-module (crates-io))

(define-public crate-regigigas-0.1.0 (c (n "regigigas") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "lasso") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stupiderators") (r "^0.1.0") (d #t) (k 0)))) (h "1gzhbfg4nw03874s3s1gs85czj76hwxk9aykv1dzpaay47cxik4l")))

(define-public crate-regigigas-0.2.0 (c (n "regigigas") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "lasso") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stupiderators") (r "^0.1.0") (d #t) (k 0)))) (h "1fdfw8yvxcrypjkbk79b3546i313yiqfwlink5zb2dirzrxpikgc")))

