(define-module (crates-io re gi region_buffer) #:use-module (crates-io))

(define-public crate-region_buffer-0.1.0 (c (n "region_buffer") (v "0.1.0") (h "1gzfc66bcgb0rvy9b7rvlxz0v5v9clyirnfivnxy9i8km4k50anq")))

(define-public crate-region_buffer-0.1.1 (c (n "region_buffer") (v "0.1.1") (h "0ibiyspcldh4bhbq47wac6nphbg7ki9g4slkwq75jissplrybkl5")))

(define-public crate-region_buffer-0.1.2 (c (n "region_buffer") (v "0.1.2") (h "01lbg8xaaln03nsfw12sr50sjwv274bmah3z2kkcqrk8xkxja8d1")))

(define-public crate-region_buffer-0.1.3 (c (n "region_buffer") (v "0.1.3") (h "0r2726xlsd1zr9h5j8rg9lkjap9z3qack3x8bq5pki706ya8lbpw")))

(define-public crate-region_buffer-0.1.4 (c (n "region_buffer") (v "0.1.4") (h "1r467qwngdsif09pmmwqmz9z0cc6rdr85cra44ppiym9v57hi8vs")))

(define-public crate-region_buffer-0.1.5 (c (n "region_buffer") (v "0.1.5") (h "1dii55qg4a2idg4kjnpclk0d7274vd10087ycjynn7micigblg02")))

