(define-module (crates-io re gi regindex) #:use-module (crates-io))

(define-public crate-regindex-0.1.0 (c (n "regindex") (v "0.1.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "11vfpkk4sg3myby79wiw2hryc9mdhn5k19yc6ch88cm2fx1yj23q")))

(define-public crate-regindex-0.2.0 (c (n "regindex") (v "0.2.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "0rh5akwj66wwya8lhvlx7znhc9amcvq64jmqir9j00n88mprqb84")))

(define-public crate-regindex-0.2.1 (c (n "regindex") (v "0.2.1") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0kzxqsrc5zpyi53j7adr2mnk2iwjhdd15gldbmr4rn1mzf4iqrz1")))

