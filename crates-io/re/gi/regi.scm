(define-module (crates-io re gi regi) #:use-module (crates-io))

(define-public crate-regi-0.1.0 (c (n "regi") (v "0.1.0") (h "1km8gbwyhs66qv820nsgac736sb4ngifjaclbv7096izfpzs7ph9")))

(define-public crate-regi-0.1.1 (c (n "regi") (v "0.1.1") (h "1kwyg1lnh9q0jwwik368bk3mvr90r0ffilqz6bygqi6k6zx9g2ki")))

(define-public crate-regi-0.1.2 (c (n "regi") (v "0.1.2") (h "1zrbyj42k01hgi32d19l13rfbkc9kilzvm70iwypc5n2wk8fihfd")))

(define-public crate-regi-0.1.3 (c (n "regi") (v "0.1.3") (h "0ywg0rq4k7pzldmr5nknsfz2cqgfb1a32frwcmgmcr9rq93a9kcl")))

(define-public crate-regi-0.1.4 (c (n "regi") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w3qrl6fj6zf4a6v39w3aqg0055avs8hqwlhpj8vwrhihqq3y06v")))

