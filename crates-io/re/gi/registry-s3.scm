(define-module (crates-io re gi registry-s3) #:use-module (crates-io))

(define-public crate-registry-s3-0.2.0 (c (n "registry-s3") (v "0.2.0") (d (list (d (n "base64") (r "=0.13.1") (d #t) (k 0)) (d (n "chrono") (r "=0.4.23") (d #t) (k 0)) (d (n "hmac") (r "=0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha-1") (r "=0.10.1") (d #t) (k 0)))) (h "1difb7kpri87b7cd9gl8racdla5j0jxgk7l764h95cf12gdh5ma1")))

