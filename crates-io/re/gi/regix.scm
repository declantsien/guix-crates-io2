(define-module (crates-io re gi regix) #:use-module (crates-io))

(define-public crate-regix-0.1.0 (c (n "regix") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.8") (d #t) (k 0)) (d (n "regix-crates-io") (r "^0") (d #t) (k 0)) (d (n "regix-npm") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02xfpf2qwqdknrmy3almqf7kchsgc1sajq2b71j37d66a0imlafs")))

