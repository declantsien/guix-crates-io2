(define-module (crates-io re gi registry-distro) #:use-module (crates-io))

(define-public crate-registry-distro-0.8.4 (c (n "registry-distro") (v "0.8.4") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "00xpvcsx00hlh43ld51jl5jklny1qhb0bbhda3ayarzvgsj4d0yw")))

(define-public crate-registry-distro-0.8.5 (c (n "registry-distro") (v "0.8.5") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1v2csib0p094jqiq344h6hqab9qz8kvrk4vhjsgmk4n61sz3jmn8")))

(define-public crate-registry-distro-0.8.7 (c (n "registry-distro") (v "0.8.7") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "12ymda53916slfnhsyckk8gg66fvppzcg52wc7fzxz195yyxjmm9")))

(define-public crate-registry-distro-0.9.0 (c (n "registry-distro") (v "0.9.0") (d (list (d (n "built") (r "^0.7.1") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1d3dbvnk3gzyvjwh0sccqg3x33nxlrngarfv3a5cvh4f0a25fcp7")))

(define-public crate-registry-distro-0.9.2 (c (n "registry-distro") (v "0.9.2") (d (list (d (n "built") (r "^0.7.1") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0x072dlfllza6ws3m2kld4wn98fwf9hzwpl3h43h1zikvxn07yxq")))

(define-public crate-registry-distro-0.9.4 (c (n "registry-distro") (v "0.9.4") (d (list (d (n "built") (r "^0.7.1") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1x18v9hhg55dv8azhxa7vzi2cp7h2w0cn9sy0cwy3rnmfsind23g")))

