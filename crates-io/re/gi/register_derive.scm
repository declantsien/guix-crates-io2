(define-module (crates-io re gi register_derive) #:use-module (crates-io))

(define-public crate-register_derive-0.1.0 (c (n "register_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yfvk51wp0bgqgy0xa67q6kgaxzzxvmkcvf5sgspgk8c0yl6w8an")))

(define-public crate-register_derive-0.1.1 (c (n "register_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yydr0j44m5f5k91mc4jf2n2ja8sl642sz9q4mvlqmjqrzllwfgl")))

