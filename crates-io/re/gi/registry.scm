(define-module (crates-io re gi registry) #:use-module (crates-io))

(define-public crate-registry-0.0.2 (c (n "registry") (v "0.0.2") (h "0x4zvj1k966cyibh4vp56i3l8nm03ijnx8q9hscb292n9n9snj7z") (y #t)))

(define-public crate-registry-0.1.0 (c (n "registry") (v "0.1.0") (h "0bzmm3cxxrl47m9s8j0acnmih4vagryw5z92l5yh7bzyyls9pi9z") (y #t)))

(define-public crate-registry-1.0.0-alpha.1 (c (n "registry") (v "1.0.0-alpha.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "widestring") (r "^0.1.0") (d #t) (k 0) (p "utfx")) (d (n "winapi") (r "^0.3.8") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "06f9zqg0bfpymj2rng2md09rkzi31ppbn9rphgxnkv98m7lbvfhi")))

(define-public crate-registry-1.0.0-alpha.2 (c (n "registry") (v "1.0.0-alpha.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "161hs9kv3hv330962bzc2awisycs74sc0mlhnhk358hg1907hy1v")))

(define-public crate-registry-1.0.0-alpha.3 (c (n "registry") (v "1.0.0-alpha.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "1vi2p545p7k1g8jkk027l71568vvbqrrscvl25hy170v8sdxvarc")))

(define-public crate-registry-1.0.0-alpha.4 (c (n "registry") (v "1.0.0-alpha.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "1wyf6wc07v2b3mq6lya25hi5jzhji0l9i6vi8na55570scm402bi")))

(define-public crate-registry-1.0.0-alpha.5 (c (n "registry") (v "1.0.0-alpha.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "16azz7izdzy2vrkbnzh5714s7zngs0iina3vip4r4786brz714zh")))

(define-public crate-registry-1.0.0 (c (n "registry") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg"))) (d #t) (k 0)))) (h "1mqhj1m2vxa6nyaxc4nz4saadm16mvlqjckxhw8ya41qacdbbpph")))

(define-public crate-registry-1.1.0 (c (n "registry") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "0m2hzs3rh774c68chnfav1w8xi9a4nnaqar0gss8cl6j3fq4s3y6")))

(define-public crate-registry-1.1.1 (c (n "registry") (v "1.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "1665wbddsmw7f321q08c9ycbijwzmmd4qcy7khag391l3i6ggwcy")))

(define-public crate-registry-1.2.0 (c (n "registry") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "0k8r5jsraf48kbspvfcr96awcksq1rm779z9ddmbrmfqh6nfsdji")))

(define-public crate-registry-1.2.1 (c (n "registry") (v "1.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "1agsb9vvw9138fa6jy2wny46qy43g4jvpkq30n6q53m56xm1g4ym")))

(define-public crate-registry-1.2.2 (c (n "registry") (v "1.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "1i0jkxs159i0y0lvdnvcsb7y9396j9n671rl040d1c29pxcb3r43")))

(define-public crate-registry-1.2.3 (c (n "registry") (v "1.2.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "utfx") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winreg" "processthreadsapi" "winnt" "winbase" "securitybaseapi" "ntdef"))) (d #t) (k 0)))) (h "154z6izcv7s1asal4vm9d2i972rffrfhryciqbbddfa61mc6sfyg")))

