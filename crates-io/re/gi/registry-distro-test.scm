(define-module (crates-io re gi registry-distro-test) #:use-module (crates-io))

(define-public crate-registry-distro-test-0.8.4 (c (n "registry-distro-test") (v "0.8.4") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "18yrps20zgqjdf4zrapwiaajx9l4qhpilp50if6yfs9y4c13r477")))

(define-public crate-registry-distro-test-0.8.4-1 (c (n "registry-distro-test") (v "0.8.4-1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0yhdaph3xqm019hlzsnzpmwm0f5k4abj2z3lqlxnharcwyimmidk")))

(define-public crate-registry-distro-test-0.8.4-2 (c (n "registry-distro-test") (v "0.8.4-2") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1wnzb993m0z4cj0qnnylyw1bhmcsjllvidl5ggki74ph4gcxv9n1")))

