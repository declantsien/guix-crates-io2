(define-module (crates-io re gi regit) #:use-module (crates-io))

(define-public crate-regit-0.1.0 (c (n "regit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "06p8pd7g1ylvyzpk8bhd70x921msgv3a669wfa70f1qmdd99gd42")))

