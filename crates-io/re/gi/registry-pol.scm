(define-module (crates-io re gi registry-pol) #:use-module (crates-io))

(define-public crate-registry-pol-0.1.0 (c (n "registry-pol") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "12mzlh4mskswjp49lmizjbqlwal6p1w0nr4jqgghfph3q0f9qw9s")))

(define-public crate-registry-pol-1.0.0 (c (n "registry-pol") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (t "cfg(target_os = \"windows\")") (k 2)))) (h "1gn9k973sx93jg363mk7mpnf6lwxylvrd2adivy3s20qmqiwk1pv")))

