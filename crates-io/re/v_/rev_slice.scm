(define-module (crates-io re v_ rev_slice) #:use-module (crates-io))

(define-public crate-rev_slice-0.1.0 (c (n "rev_slice") (v "0.1.0") (h "0dglpq7w6y18ai34wqmf352ipfxx361mf9bpqxv6vbvcwp08jnlr") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-rev_slice-0.1.1 (c (n "rev_slice") (v "0.1.1") (h "012sa9i2rc3qh835vpwp90b6r9jzgfdadwdz7ychqvm7hb3n22gb") (f (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1.2 (c (n "rev_slice") (v "0.1.2") (h "195hc5hmckvxpn31xk7a64mq3hgk2b4abhab2bb6bs6gsbd6049s") (f (quote (("std") ("docs_rs_workarounds") ("default" "std"))))))

(define-public crate-rev_slice-0.1.3 (c (n "rev_slice") (v "0.1.3") (h "1s6jd70lyjvi8vg1430skzbynbscip6khbkw5ihl5fk8sa461yjs") (f (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1.4 (c (n "rev_slice") (v "0.1.4") (h "1rphlva8imqcc8gzi5k85q0q451zdp0943h30mkgin9264p09f97") (f (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1.5 (c (n "rev_slice") (v "0.1.5") (h "0m1r9sybn1wavn57bbigmz1sj2mvhjphnz38qg5z1aanbblh5n3f") (f (quote (("std") ("default" "std"))))))

