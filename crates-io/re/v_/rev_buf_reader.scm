(define-module (crates-io re v_ rev_buf_reader) #:use-module (crates-io))

(define-public crate-rev_buf_reader-0.1.0 (c (n "rev_buf_reader") (v "0.1.0") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "03pnkfrm8bqzpjghma07wbg31qxxydlrirx91x3q9z0frfq860xk") (f (quote (("read_initializer") ("iovec") ("default"))))))

(define-public crate-rev_buf_reader-0.1.1 (c (n "rev_buf_reader") (v "0.1.1") (d (list (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "177l0alryc1fvm071qy0qzgnhkiw6ni9vcba9y0mzbmacjcrb5qa") (f (quote (("read_initializer") ("iovec") ("default"))))))

(define-public crate-rev_buf_reader-0.2.0 (c (n "rev_buf_reader") (v "0.2.0") (d (list (d (n "memchr") (r "^2.3.0") (d #t) (k 0)))) (h "0hgplkmyg59yych6l33ssxl4yfbh3kdw8hyginhls8as87wfmrsv") (f (quote (("read_initializer") ("default"))))))

(define-public crate-rev_buf_reader-0.3.0 (c (n "rev_buf_reader") (v "0.3.0") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)))) (h "047rfp65hwi5wmxj9157s603g2kjhhbjwvl2b44r4a8fw13jw3wc") (f (quote (("read_initializer") ("default"))))))

