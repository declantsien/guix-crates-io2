(define-module (crates-io re v_ rev_bits) #:use-module (crates-io))

(define-public crate-rev_bits-0.1.0 (c (n "rev_bits") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0nvfgqb3di9zf2iz3xipq66wdlhp8z9qljlcl9v5k32mmvvrrb74")))

(define-public crate-rev_bits-0.1.1 (c (n "rev_bits") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "18q0wqr9b45hqfkp57whiz6wqqk0jmv56ijim2ix7riq5dwhsf7g")))

(define-public crate-rev_bits-0.1.2 (c (n "rev_bits") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0glghw40s8mmina7wcgww79d12aanywsbn2w448mrynj0nq8z0m5")))

