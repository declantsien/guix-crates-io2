(define-module (crates-io re v_ rev_lines) #:use-module (crates-io))

(define-public crate-rev_lines-0.1.0 (c (n "rev_lines") (v "0.1.0") (h "061ci18bp1rqq8k6ishlk0rajl0hbcjnz517krpmkvcyyzlabg4v")))

(define-public crate-rev_lines-0.2.0 (c (n "rev_lines") (v "0.2.0") (h "0bandfl4fqa77x6dg698ln5k4p2wx7xfczjmyrr5qg7pn89p4dcq")))

(define-public crate-rev_lines-0.2.1 (c (n "rev_lines") (v "0.2.1") (h "1cw8mf0gpskm1wxsnsjaq1gnzp1vi3jcgjkg2d9i0csdcsv55sqq")))

(define-public crate-rev_lines-0.2.2 (c (n "rev_lines") (v "0.2.2") (h "0k9hd109aabx13k2pi6cgjd91fyxapzqwsm9vv9fxvl0fbw9g0mg")))

(define-public crate-rev_lines-0.3.0 (c (n "rev_lines") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pzh5g31ljhkzinpahg8zq05c0gz54q307ayz89vzk55qxm92qpd")))

