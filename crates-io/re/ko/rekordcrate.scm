(define-module (crates-io re ko rekordcrate) #:use-module (crates-io))

(define-public crate-rekordcrate-0.1.0 (c (n "rekordcrate") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0wyg4fl8gk41jp8yllkhw8mn8l70917h2yiclqjb67mimfixci16")))

(define-public crate-rekordcrate-0.2.0 (c (n "rekordcrate") (v "0.2.0") (d (list (d (n "binrw") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "1a929h06ibsmih30klyqbpk1piwm1ym4alcw1zrqsaa63dfjg4xa") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

(define-public crate-rekordcrate-0.2.1 (c (n "rekordcrate") (v "0.2.1") (d (list (d (n "binrw") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)) (d (n "parse-display") (r "^0.6.0") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z892g58cww6ixalamxd6gzwrc9v3r1bvfahm4kz1p9xkhp1gbdd") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

