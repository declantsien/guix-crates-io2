(define-module (crates-io re db redbcli) #:use-module (crates-io))

(define-public crate-redbcli-0.1.0 (c (n "redbcli") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "redb") (r "^1.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "0a7w9p5id05sg8xlysp1dp02hlf2pbzvbkrwfljf9jnlqgr94gwv")))

