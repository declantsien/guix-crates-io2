(define-module (crates-io re db redblocks) #:use-module (crates-io))

(define-public crate-redblocks-0.1.2 (c (n "redblocks") (v "0.1.2") (h "0m0k6jbmdcn6cwnmfk68rq02v25bkkbx8bglfypnd8r32x0bkk6d")))

(define-public crate-redblocks-0.1.3 (c (n "redblocks") (v "0.1.3") (h "0vckf58hccfd3jvd7yy8z08bhk465f6x186pabwsph6gmfaj9bpr")))

(define-public crate-redblocks-0.1.4 (c (n "redblocks") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0y5n7cnwj7f6vjh0n2y5wv7qpnjp6ncigzgcyyrzw3xdz49z6pzx")))

(define-public crate-redblocks-0.1.41 (c (n "redblocks") (v "0.1.41") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0pv81nqcigb2pkmwa0xq6666vsnp3skv8vppnwjl785qziy3181g")))

(define-public crate-redblocks-0.1.42 (c (n "redblocks") (v "0.1.42") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0bcc9q0k241x54pzf5ab2lybpsrjg8lr4iizh1yqhlzzpml633sx")))

(define-public crate-redblocks-0.2.0 (c (n "redblocks") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "1j3gfnz5wrfclfgc3572aaax1p126hzbbfckfrpwq34q9d8xvz1m")))

(define-public crate-redblocks-0.2.1 (c (n "redblocks") (v "0.2.1") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0qxvw0k5612sssq0kjraalgbhznw9p7dnh73vqi1kh0fx5zzh06r")))

(define-public crate-redblocks-0.2.2 (c (n "redblocks") (v "0.2.2") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "17m26msb8w9qpicaywlsfgqkbkdw4dahbn25yaqcff51ihiw6vv1")))

(define-public crate-redblocks-0.2.3 (c (n "redblocks") (v "0.2.3") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "1hqydklmjnamk3rwi17h8si1w3f8q2hx7n4mw05gzrqbw6wk91sf")))

(define-public crate-redblocks-0.2.31 (c (n "redblocks") (v "0.2.31") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "1pl98nbq4iq7750zg289a5lv35wi0fsgs560pyl86jbmxkgi9kd0") (y #t)))

(define-public crate-redblocks-0.2.4 (c (n "redblocks") (v "0.2.4") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openweathermap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0h6b0rbx6waji06lrk730m8x1lfn34av89pz1jyniny8fxja1mkd") (f (quote (("weather" "openweathermap") ("all" "weather"))))))

