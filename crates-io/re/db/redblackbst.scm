(define-module (crates-io re db redblackbst) #:use-module (crates-io))

(define-public crate-redblackbst-0.1.0 (c (n "redblackbst") (v "0.1.0") (h "1583r4p8c6z11m6gcdzxqwdkmpz91r8hs1673wmydl03dy07ir7r")))

(define-public crate-redblackbst-0.1.1 (c (n "redblackbst") (v "0.1.1") (h "1xx3q7117bm1isv4bf91viaa0iv1hffzd2jk2a4n2sawdd1l1xal")))

