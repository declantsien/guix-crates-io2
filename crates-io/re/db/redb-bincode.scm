(define-module (crates-io re db redb-bincode) #:use-module (crates-io))

(define-public crate-redb-bincode-0.1.0 (c (n "redb-bincode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "redb") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0yddzappc6in8vgds87zdjw3351k75wz7xbnc872zhkfsssdq68r")))

(define-public crate-redb-bincode-0.2.0 (c (n "redb-bincode") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "redb") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0v8injlvh89vkmaz6d386bhqbhdsb8f91cw26940dmn1l1yc1c9i")))

(define-public crate-redb-bincode-0.2.1 (c (n "redb-bincode") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "redb") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0b5nramkyiv4jprmaybww7k861g158qk5p2pl445ga0vrs72r2c4")))

