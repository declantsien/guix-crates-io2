(define-module (crates-io re db redbot) #:use-module (crates-io))

(define-public crate-redbot-0.1.0 (c (n "redbot") (v "0.1.0") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mockito") (r "^0.17.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "09175p655dxzfl4sbng27m7gy7yb2m2q8whxi111aav1jswf446b")))

(define-public crate-redbot-0.1.1 (c (n "redbot") (v "0.1.1") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mockito") (r "^0.17.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "021f6b3h02fva67sx8crkpkl3lkpasx9vwb8snzs9wc14y6ajxvm")))

(define-public crate-redbot-0.1.2 (c (n "redbot") (v "0.1.2") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mockito") (r "^0.17.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "1vfafjwha5m4m9fhy85w8fasprrdr4hpfyxhlp8k5vxcghhnxhw3")))

(define-public crate-redbot-0.1.3 (c (n "redbot") (v "0.1.3") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mockito") (r "^0.17.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "085ics1yidxxgwq5pp09182zq59dcqrk0yw7n95rz6gaik6dqkzv")))

