(define-module (crates-io re am ream) #:use-module (crates-io))

(define-public crate-ream-0.3.1 (c (n "ream") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)))) (h "007pbmfjkdvqs3p9ilqvxdasbl622glv9kcw3pvsx4drsc0fxh7s")))

(define-public crate-ream-0.3.2 (c (n "ream") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)))) (h "09s0n24r20ql9jpn1cg26d96y9xib9xlmbsylirn1npgqkbswwbm")))

(define-public crate-ream-0.3.3 (c (n "ream") (v "0.3.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)))) (h "0dgkf83pdrg5w0ijf6rqpvymg398cvxi1p2pb793z6v6ixhcnqzl")))

(define-public crate-ream-0.4.0 (c (n "ream") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)))) (h "1qy2f87ndrbpydjc3cniswq2v0ps7000nf99gpi97jqxm9pf7f6i")))

(define-public crate-ream-0.4.2 (c (n "ream") (v "0.4.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)))) (h "0i2apyxmfigc4fvdalhh5vnldkndyw6g72bv5vdccgm39abd3sif")))

