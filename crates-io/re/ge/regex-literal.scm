(define-module (crates-io re ge regex-literal) #:use-module (crates-io))

(define-public crate-regex-literal-1.0.0 (c (n "regex-literal") (v "1.0.0") (d (list (d (n "regex-automata") (r "^0.4.6") (d #t) (k 0)))) (h "020mhws2iqzmhd7vsi55qx9mq8wh7zj91gm291qryzh9cc84b2yh") (f (quote (("w"))))))

(define-public crate-regex-literal-1.0.1 (c (n "regex-literal") (v "1.0.1") (d (list (d (n "regex-automata") (r "^0.4.6") (d #t) (k 0)))) (h "01vq7fvl0xymm2f391sx26ixf6bw405c9khlacbir81jwwa9wxcz") (f (quote (("w"))))))

(define-public crate-regex-literal-1.0.2 (c (n "regex-literal") (v "1.0.2") (d (list (d (n "regex-automata") (r "^0.4") (d #t) (k 0)))) (h "17zww9f2c2x71qxig9hhwn3fkrzsmp50barli4mmv0krh9ivn5vk") (f (quote (("w"))))))

(define-public crate-regex-literal-1.1.0 (c (n "regex-literal") (v "1.1.0") (d (list (d (n "regex-automata") (r "^0.4") (d #t) (k 0)))) (h "1hcmxhbn0hrnya1ja41v6kgvvsa7b2wg95zvk5g5yhhagapk56z8") (f (quote (("w") ("default" "w"))))))

(define-public crate-regex-literal-1.1.1 (c (n "regex-literal") (v "1.1.1") (d (list (d (n "regex-automata") (r "^0.4") (d #t) (k 0)))) (h "11rgg0zjj5v4gdbbv3fjcijvpdrlibmddd3nv00shjl966ian0z7") (f (quote (("w") ("default" "w"))))))

(define-public crate-regex-literal-1.2.0 (c (n "regex-literal") (v "1.2.0") (d (list (d (n "regex-automata") (r "^0.4") (d #t) (k 0)))) (h "0q9lnmq1yqr0y2sa4bmdhpw203yyykfvpik1niv3a85za37kbdls") (f (quote (("w") ("default" "w"))))))

