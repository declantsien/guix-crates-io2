(define-module (crates-io re ge regex_router) #:use-module (crates-io))

(define-public crate-regex_router-0.1.0 (c (n "regex_router") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1qaa31q21g0yr2g1i8pk762b3dwdrgx971lxqp3cicr6f8846k9a") (y #t)))

(define-public crate-regex_router-1.0.0 (c (n "regex_router") (v "1.0.0") (d (list (d (n "hyper") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "168ilq7m17qd3ys03vvpp6bvk6z231jr641vg2yc44gdc8q560yd") (y #t)))

(define-public crate-regex_router-1.0.1 (c (n "regex_router") (v "1.0.1") (d (list (d (n "hyper") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1yay0a3s156l04ci0icxkdnkns9vi80iphzmrpv7khq7bm45n4rk") (y #t)))

(define-public crate-regex_router-1.0.2 (c (n "regex_router") (v "1.0.2") (d (list (d (n "hyper") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "14h5wk453fnq67sdkwha8dxlqzn80f84nd4b6ng6k2lb7f80ln76") (y #t)))

