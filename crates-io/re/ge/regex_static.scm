(define-module (crates-io re ge regex_static) #:use-module (crates-io))

(define-public crate-regex_static-0.1.0 (c (n "regex_static") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex_static_macro") (r "^0.1") (d #t) (k 0)))) (h "05ydl650drijqs6ids73c35a044wchjqpmdkwws3s5x7ljpfh38i")))

(define-public crate-regex_static-0.1.1 (c (n "regex_static") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex_static_macro") (r "^0.1") (d #t) (k 0)))) (h "0880riviynaiihj3k5yijp61cw953py44fzpk2894habbqfdc9k1")))

