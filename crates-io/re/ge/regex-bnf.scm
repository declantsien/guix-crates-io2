(define-module (crates-io re ge regex-bnf) #:use-module (crates-io))

(define-public crate-regex-bnf-0.1.0 (c (n "regex-bnf") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-bnf-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1m3pa50ss99rjf11g86nq0q6hbikb6nfvipsr8agdb107cmmcn62")))

(define-public crate-regex-bnf-0.1.1 (c (n "regex-bnf") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-bnf-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r7h451nnfsn6mhb4ny0v3qlb5masikw2lb7bpg9hyzzv534g9m8")))

(define-public crate-regex-bnf-0.1.2 (c (n "regex-bnf") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-bnf-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1zvy1hsijsb31ld4ynflr2b6ac87a58hh778bbxr9qvz5hid9n6c") (f (quote (("debug-logs"))))))

