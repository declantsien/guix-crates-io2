(define-module (crates-io re ge regex-mel) #:use-module (crates-io))

(define-public crate-regex-mel-0.7.0-rc1 (c (n "regex-mel") (v "0.7.0-rc1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "11fbff6bgayh19h0mkj9xqb62i7dmqnfh59ig8y8i8ax475kgnfw") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.7.0 (c (n "regex-mel") (v "0.7.0") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "01prd5ci71wjcmmbzj9hln615cfgmd4wkvyppma23338mr1iqds4") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.7.1 (c (n "regex-mel") (v "0.7.1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "09znp774jfr7d1sm4ph3dw0vl2pcwjylspq76b1jiggs3isxdgm6") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.8.0-rc1 (c (n "regex-mel") (v "0.8.0-rc1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc1") (d #t) (k 0)))) (h "0n6ylivr8z45acnafpl5w0jzw913n3a5lczklpy8hy407jnc74xb") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.8.0-rc2 (c (n "regex-mel") (v "0.8.0-rc2") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc2") (d #t) (k 0)))) (h "1flqp4dwgbmk5zq1cwdy6i9ipqzqy6cj59zv2fi4vbfmswbjhi09") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.8.0-rc3 (c (n "regex-mel") (v "0.8.0-rc3") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc3") (d #t) (k 0)))) (h "106b3m129f1bfb5k2r0qb6fl0xhimal402fpdhhnx4ndanbfx7yb") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-regex-mel-0.8.0 (c (n "regex-mel") (v "0.8.0") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.8.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "std-mel") (r "^0.8.0") (d #t) (k 0)))) (h "1rf7cdhvvcb5sq4fpaqmi8xjcpri3ilbj4scjhj904hawy226sss") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

