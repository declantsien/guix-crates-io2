(define-module (crates-io re ge regenboog) #:use-module (crates-io))

(define-public crate-regenboog-0.1.0 (c (n "regenboog") (v "0.1.0") (h "1aj7bjgmb5y06bi6wib0i83n039633ib14kj477hk3qan7mmrybq")))

(define-public crate-regenboog-0.1.1 (c (n "regenboog") (v "0.1.1") (h "0gvnghg56k7cs22j8939j0l7xi8syk8r54hsnp5cw9pbxa3h9wwv")))

