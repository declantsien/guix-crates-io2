(define-module (crates-io re ge regex-lexer-lalrpop) #:use-module (crates-io))

(define-public crate-regex-lexer-lalrpop-0.1.0 (c (n "regex-lexer-lalrpop") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14hm20hk5kks52ib96ss399hf8391z3c3xfnx4ngb68ppq8za37z")))

(define-public crate-regex-lexer-lalrpop-0.2.0 (c (n "regex-lexer-lalrpop") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vpn8zh7k2a18d635fxy6nm6aqq2x5bkqrhr2hml77j9pwycgqsn")))

(define-public crate-regex-lexer-lalrpop-0.2.1 (c (n "regex-lexer-lalrpop") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10xyyk1v5vv2rjwy284f35m5k7v406f44qpa2jllcpipiskrcjjc")))

(define-public crate-regex-lexer-lalrpop-0.3.0 (c (n "regex-lexer-lalrpop") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rw1q5gf86g3l32lnd7d5hja5ymmhpr27gsnrasv6vqanikl9gn")))

