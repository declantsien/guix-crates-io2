(define-module (crates-io re ge regex_static_impl) #:use-module (crates-io))

(define-public crate-regex_static_impl-0.1.0 (c (n "regex_static_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.22") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i9b5k5rxrcpm843x98pvkvmrmq1hnsv0q335rvhx9w6k00madww")))

