(define-module (crates-io re ge regen-lang) #:use-module (crates-io))

(define-public crate-regen-lang-0.0.1 (c (n "regen-lang") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "06ywkq5wvyl0xzlxd6rcc07w6jvmm8ibvqb434s8p6p9qlk27j3r")))

(define-public crate-regen-lang-0.0.2 (c (n "regen-lang") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1gik9bvxj0kf108cpp6wh8p56zy3aawby0blfhirrf8n4liii0f3")))

(define-public crate-regen-lang-0.0.4 (c (n "regen-lang") (v "0.0.4") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "codize") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1r5amgdjm006qkj0fq8zckw6kw6zfa5m55hk9vscc91p43yrgrxy") (f (quote (("build-binary" "clap"))))))

(define-public crate-regen-lang-0.0.5 (c (n "regen-lang") (v "0.0.5") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "codize") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "160fzskagbn6hb96dig2s84wm1mgj7skmn34lk0snc8yvhzyjvc9") (f (quote (("default" "clap"))))))

(define-public crate-regen-lang-0.0.6 (c (n "regen-lang") (v "0.0.6") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "codize") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0q83izz3a72m22xr3mw392sjxjra6pnaqa8kqwvsdadypsgmwil6") (f (quote (("default" "clap"))))))

(define-public crate-regen-lang-0.0.7 (c (n "regen-lang") (v "0.0.7") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "codize") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0kbzp8ll65xi9as4qvsr96g66w8x0v3k8s3nkkxrimq7dc90qdrl") (f (quote (("default" "clap"))))))

