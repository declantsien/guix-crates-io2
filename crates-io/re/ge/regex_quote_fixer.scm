(define-module (crates-io re ge regex_quote_fixer) #:use-module (crates-io))

(define-public crate-regex_quote_fixer-0.1.0 (c (n "regex_quote_fixer") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1znd4gyvv9yf786x7fjk187g1yw9dapz03v2win6lsiib6saadl3")))

(define-public crate-regex_quote_fixer-0.1.1 (c (n "regex_quote_fixer") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "11pb59rk4i4pbz98kpc2d01df3pnicmh71x4xyq3lj1iqq4d1jar")))

(define-public crate-regex_quote_fixer-0.1.2 (c (n "regex_quote_fixer") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1k5j22wchnmzim710vvgv177dc871ys2r1k2spchrszzjxsz5p2z")))

(define-public crate-regex_quote_fixer-0.1.3 (c (n "regex_quote_fixer") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1awa0vbf5qlhj2aax1vi0qnb6vncv6lhncr60pxzh8bshg56ny9k")))

(define-public crate-regex_quote_fixer-0.1.4 (c (n "regex_quote_fixer") (v "0.1.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0zr8ph50vbm3rlr8lcnf7qdx4zpyyw8r6sylm244d7wdg51lvl0n")))

(define-public crate-regex_quote_fixer-0.1.5 (c (n "regex_quote_fixer") (v "0.1.5") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0p43srgqb9034rfci06kan3smb3wa09c3zklsv17g9qxl00w8rw4")))

(define-public crate-regex_quote_fixer-0.1.6 (c (n "regex_quote_fixer") (v "0.1.6") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1dd7fnxvcd51gi8ydm9d3m9gdq8vmj06ps24k93kk32czmimdqsb")))

(define-public crate-regex_quote_fixer-0.2.0 (c (n "regex_quote_fixer") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "104skqparmm5yq37dz8fznxcs3ry0gh1bnr8ncq44dpxrs685dnq")))

(define-public crate-regex_quote_fixer-0.2.1 (c (n "regex_quote_fixer") (v "0.2.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1kgw5xchb5lkr293jhrz2cqzli4qzdaw8s53cs11g4jwib80pyny")))

