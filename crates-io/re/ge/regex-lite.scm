(define-module (crates-io re ge regex-lite) #:use-module (crates-io))

(define-public crate-regex-lite-0.0.0 (c (n "regex-lite") (v "0.0.0") (h "1rnkvhzvxz0si4bv294pps1kl456rbsp8s00fqww8gk66lf2nz8d")))

(define-public crate-regex-lite-0.1.0 (c (n "regex-lite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "18jwv478fvdlsrza1gmy5bkarkvn870csz1f148fk9kb71zxwvpr") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.60.0")))

(define-public crate-regex-lite-0.1.1 (c (n "regex-lite") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "0zc2d55bbnbqbpwfxb7chr8nggc2f91lqyn57w7h9mhcjc6dif2f") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.65")))

(define-public crate-regex-lite-0.1.2 (c (n "regex-lite") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "14a2dwm0c6rigacvzlm72i261rxvrl7vnmxfwr1hxkw4ixw2pal3") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.65")))

(define-public crate-regex-lite-0.1.3 (c (n "regex-lite") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "0y38d4jmak1f5v5dydawib8n3l3fl08ri6pm81hpx52kav8vqvls") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.65")))

(define-public crate-regex-lite-0.1.4 (c (n "regex-lite") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "0y89bq9vp3nkv8gqj7b3bc5kh7fzq8w5b4z2yr66b4k4a0wwm4kr") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.65")))

(define-public crate-regex-lite-0.1.5 (c (n "regex-lite") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "regex-test") (r "^0.1.0") (d #t) (k 2)))) (h "13ndx7ibckvlasyzylqpmwlbp4kahrrdl3ph2sybsdviyar63dih") (f (quote (("string") ("std") ("default" "std" "string")))) (r "1.65")))

