(define-module (crates-io re ge regex_mutator) #:use-module (crates-io))

(define-public crate-regex_mutator-0.1.0 (c (n "regex_mutator") (v "0.1.0") (d (list (d (n "regex-syntax") (r "^0.6.17") (d #t) (k 0)))) (h "0p7gwb8px5wx6fplgmf54887yl667921vw3hyg1kprglvmwlhj5c")))

(define-public crate-regex_mutator-0.2.0 (c (n "regex_mutator") (v "0.2.0") (d (list (d (n "regex-syntax") (r "^0.6.17") (d #t) (k 0)))) (h "0s4cybcgn51fv72xd0zarf1sgkzbcs6nml0qsfg0zgy8f96sq6lf")))

(define-public crate-regex_mutator-0.3.0 (c (n "regex_mutator") (v "0.3.0") (d (list (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0gn90cg10aza3kw3nr360l1dq3gffh3yn5ap6zvblrfhjpv5f0gb")))

(define-public crate-regex_mutator-0.3.1 (c (n "regex_mutator") (v "0.3.1") (d (list (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0xwb16ndmlkzm1qqriy31hlr238gzibzar6nnck11sgs9nmpjq7n")))

