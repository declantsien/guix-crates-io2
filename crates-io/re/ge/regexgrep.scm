(define-module (crates-io re ge regexgrep) #:use-module (crates-io))

(define-public crate-regexgrep-1.0.0 (c (n "regexgrep") (v "1.0.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1cvlx1j3c89v6lxms8av5fm5018jslwzm5xjqg7iak20k48kpgpr")))

(define-public crate-regexgrep-1.0.1 (c (n "regexgrep") (v "1.0.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1n3ggig456xm3in8g7kcr7ywa2j57gkblg3mq1lfjn9ibga6v00d")))

(define-public crate-regexgrep-1.0.2 (c (n "regexgrep") (v "1.0.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1slsznrbbr3pmn003i9l544af4c33xw9kl1b42v4gx7y52dq9x61")))

(define-public crate-regexgrep-1.0.3 (c (n "regexgrep") (v "1.0.3") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0yfb81s1s1xan8n17bdd4z5sqa381khibgh2gsx4shmkf8cxzalm")))

