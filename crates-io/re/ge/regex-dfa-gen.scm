(define-module (crates-io re ge regex-dfa-gen) #:use-module (crates-io))

(define-public crate-regex-dfa-gen-0.1.0 (c (n "regex-dfa-gen") (v "0.1.0") (d (list (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1sd3j6k6az68pmpfvr7gi0mnclc6lr54mvgjdcxdg3aiij4is85g")))

