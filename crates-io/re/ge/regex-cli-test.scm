(define-module (crates-io re ge regex-cli-test) #:use-module (crates-io))

(define-public crate-regex-cli-test-0.1.0 (c (n "regex-cli-test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1khhpz07pr17v3584rlz8njmb7fkj3j1p153cfg7nl9h6v7nljnv")))

