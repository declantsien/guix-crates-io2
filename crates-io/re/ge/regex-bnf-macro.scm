(define-module (crates-io re ge regex-bnf-macro) #:use-module (crates-io))

(define-public crate-regex-bnf-macro-0.1.0 (c (n "regex-bnf-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1kffjsrdx1272hiq5qz3vn0lgkanga8807n1dhva8z4gq1pxmcga")))

(define-public crate-regex-bnf-macro-0.1.1 (c (n "regex-bnf-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1p2h4sxf5ydz2c4i8062j7g9082n2551q8hj00x28nzw06g165b6")))

