(define-module (crates-io re ge regex-parse-proc-macro) #:use-module (crates-io))

(define-public crate-regex-parse-proc-macro-0.1.0 (c (n "regex-parse-proc-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "114kfzw2l2w405bfwk38hcigyd8jmwzpk7yqd2dl0zhqhr3a904w")))

