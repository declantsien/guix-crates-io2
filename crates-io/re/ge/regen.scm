(define-module (crates-io re ge regen) #:use-module (crates-io))

(define-public crate-regen-0.1.0 (c (n "regen") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0jhpr6wc2dxhbnc1dvcqx4y2mdahfms22dpbmx9dk3kvbdkfpnjc")))

