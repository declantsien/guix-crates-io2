(define-module (crates-io re ge regex_env) #:use-module (crates-io))

(define-public crate-regex_env-0.1.0 (c (n "regex_env") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0km6q09zjzniskxr7qkdshbfb02vr5x8iipf2vljvlsqvmfbifi5")))

(define-public crate-regex_env-0.2.1 (c (n "regex_env") (v "0.2.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0kn4d1k60mnywl373597k7k0dyj060jga10qsqji9sdcfvvmx4si")))

