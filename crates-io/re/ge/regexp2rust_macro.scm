(define-module (crates-io re ge regexp2rust_macro) #:use-module (crates-io))

(define-public crate-regexp2rust_macro-0.1.0 (c (n "regexp2rust_macro") (v "0.1.0") (d (list (d (n "jsexec") (r "^0.1.6") (d #t) (k 0)))) (h "16rcmxpk3m47nagiszcdsya45fq3wckzl19frcbj57s0gjpn4gvg")))

(define-public crate-regexp2rust_macro-0.1.1 (c (n "regexp2rust_macro") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jsexec") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)))) (h "08j06z0y39mikxjaqwxy7yqzlqjiai19z958ywdyijl5wprr45w8")))

(define-public crate-regexp2rust_macro-0.1.3 (c (n "regexp2rust_macro") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jsexec") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)))) (h "02cpn66zgr46kqpajkw87jvrjwgj122ysihjj001gvl2nfv3cqsd")))

(define-public crate-regexp2rust_macro-0.1.4 (c (n "regexp2rust_macro") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jsexec") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)))) (h "1zz08k94a47zm7c7qx4fsgca3729yf5fj74ad2hv4rnfr52kxh6z")))

(define-public crate-regexp2rust_macro-0.1.5 (c (n "regexp2rust_macro") (v "0.1.5") (d (list (d (n "jsexec") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (f (quote ("perf-literal"))) (k 2)))) (h "1106v09x7h3wasfi1dg5rvc3fv84j4gd0chxxfp0lpnj2a1ljcxy")))

