(define-module (crates-io re ge regex-lexer) #:use-module (crates-io))

(define-public crate-regex-lexer-0.1.0 (c (n "regex-lexer") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19y23v5l7hy45yc5jrcvwmzhjvqcrpcd8j6rdd7a32crdisky01g")))

(define-public crate-regex-lexer-0.1.1 (c (n "regex-lexer") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "055s96n29rwhp8r5vhb1b84zzms5r30z4223qcrp7q0i9kdx4w6y")))

(define-public crate-regex-lexer-0.2.0 (c (n "regex-lexer") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08znd66grp57zj2ri1a8bp9c2yasbm30bzvshv1v6422vcp108c0")))

