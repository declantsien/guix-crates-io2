(define-module (crates-io re ge regex-decode) #:use-module (crates-io))

(define-public crate-regex-decode-0.1.0 (c (n "regex-decode") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1g1ami7kiv0bblwcl1s27l67pz7sn4r0wminizrj9yaay8jgq2q3")))

