(define-module (crates-io re ge regex-generator) #:use-module (crates-io))

(define-public crate-regex-generator-0.1.0 (c (n "regex-generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1fcq8kp7g6rx09sa1ljjil4jgh8yx4wbdv1igv7dbaiw9zyl02y8")))

