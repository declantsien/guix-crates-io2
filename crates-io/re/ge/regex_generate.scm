(define-module (crates-io re ge regex_generate) #:use-module (crates-io))

(define-public crate-regex_generate-0.1.0 (c (n "regex_generate") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.4.1") (d #t) (k 0)))) (h "0xnj65glv2g7vbhbk6wbvix9738hbb0miyinf4wr2sqg2dlg3nil")))

(define-public crate-regex_generate-0.2.0 (c (n "regex_generate") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.4.1") (d #t) (k 0)))) (h "0rb4f7s97s21msnb07a343sywfryq7ik26416n8jy2l6d7k6xjjl")))

(define-public crate-regex_generate-0.2.1 (c (n "regex_generate") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.11") (d #t) (k 0)))) (h "1ihbyhy0g79qmd16r1ab0pwbi57iymlyyk78v6f50169j4a0lm68")))

(define-public crate-regex_generate-0.2.2 (c (n "regex_generate") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.11") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 2)))) (h "1wlqlpn342f3zddxsn63nrpw5rxrck5srk3d68azczmgnk7zgikl")))

(define-public crate-regex_generate-0.2.3 (c (n "regex_generate") (v "0.2.3") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)))) (h "1b0a915wpjzfdvgnb6qcb7pi69xb02dngpbrqak2v59844lqmmy1")))

