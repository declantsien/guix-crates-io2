(define-module (crates-io re ge regex_static_macro) #:use-module (crates-io))

(define-public crate-regex_static_macro-0.1.0 (c (n "regex_static_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "regex_static_impl") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "188inq1ipw5bzyhysalrsrj3ij6z6ahn5dy8bdimhh83znam9cbr")))

