(define-module (crates-io re ge regex-macro) #:use-module (crates-io))

(define-public crate-regex-macro-0.1.0 (c (n "regex-macro") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yf5x1jpdlks7ssq9vbhl0z1fgwspay45xz5h0qbsjx9wb6vjhz4")))

(define-public crate-regex-macro-0.1.1 (c (n "regex-macro") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jdikhhkxmrcyq1nn31ivr28796cq4vpi9amnswpiwk35wwyxbrl")))

(define-public crate-regex-macro-0.2.0 (c (n "regex-macro") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0nql20vvvgy86pwxap4fi84a1qnsclx9kfhbcjbb4vfimpkkdyhj")))

