(define-module (crates-io re ge regex_prime) #:use-module (crates-io))

(define-public crate-regex_prime-0.1.0 (c (n "regex_prime") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "0fg6qwdg962wr3p6q3gwwakwzrm56znpm85q21rrr3r5h4hy11m7")))

(define-public crate-regex_prime-0.1.1 (c (n "regex_prime") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "015k0habyrafakjflfxpaqinf24in3z44vkbgi9w20106mnish7f")))

