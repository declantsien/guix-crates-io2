(define-module (crates-io re ge regex-tokenizer-impl) #:use-module (crates-io))

(define-public crate-regex-tokenizer-impl-0.1.0 (c (n "regex-tokenizer-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mh1a3pjpaw4xd0hsq88gh7y3600bay9yw4addkvyyzkj286f8ys")))

