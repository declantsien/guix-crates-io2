(define-module (crates-io re ge regexp) #:use-module (crates-io))

(define-public crate-regexp-0.1.0 (c (n "regexp") (v "0.1.0") (h "0rn8jdaa7d0afvv36243fsiwhjwz93ypa4cvkqimmiyywg3pv1p2")))

(define-public crate-regexp-0.1.1 (c (n "regexp") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0g85ik5hn7s5smwy1p3y94kj38pm84bvlr0m3dnmyvjr04zspq21")))

(define-public crate-regexp-0.1.2 (c (n "regexp") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0bpcjfvlr04wcgaa6d2izwkvfg26m9cnbb2ps7flm41sikrdjp23")))

(define-public crate-regexp-0.2.0 (c (n "regexp") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "08kd6f46pbhzf5zhjcl9kzlyk6bk73azwr5l18vb3aww8mylz3hr")))

(define-public crate-regexp-0.2.1 (c (n "regexp") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0njxpw73sw6gwrw416fncihwwc2nwgdlv2n2y2544vsxpr2y81im")))

(define-public crate-regexp-0.2.2 (c (n "regexp") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "02ni5ypin43pra4w1w4lqpzal4c836h56s3bb11lxxzmcynjnfkc")))

(define-public crate-regexp-0.2.3 (c (n "regexp") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "02qr8liq7705rnnad0w3pfyvcshy4naxahky0kvj99904q048m9a")))

(define-public crate-regexp-0.2.4 (c (n "regexp") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0j1np916i82d1jw8610dwiy92bi6frrkk99c7i09sdjfblndicli")))

(define-public crate-regexp-0.2.5 (c (n "regexp") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1sbhp0nzx2ylr439hynhh6l0mnry9y01aycmf121kxdzfd81c1c1")))

(define-public crate-regexp-0.2.7 (c (n "regexp") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1brkwk4mbi4l53rysk54fdmbhbpk44vhwilymkvhsp99mpvmham8")))

(define-public crate-regexp-0.3.0 (c (n "regexp") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0vxccf80ikpv3fhmi7b0rhi8cp7a4scxywajkhl9nxd85ns4wf8c")))

(define-public crate-regexp-0.3.1 (c (n "regexp") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "16sxmsfyz7yg2cscbsaylcby21p894wanmag0mgvad7w943mzxvl")))

(define-public crate-regexp-0.3.2 (c (n "regexp") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0zkjskflmgr47nly6k8hnvhq7i4b2pxl56jrb8l2wnzwv6p5n14v")))

