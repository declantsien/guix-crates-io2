(define-module (crates-io re ge regex-cache) #:use-module (crates-io))

(define-public crate-regex-cache-0.1.0 (c (n "regex-cache") (v "0.1.0") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "1ln1bqq9lq0kpgnpi7yjmxs5wxqxl4jpv5qxypy58q1772qm1yfd")))

(define-public crate-regex-cache-0.1.1 (c (n "regex-cache") (v "0.1.1") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "1w5hlqjmp3vlkli7vlff2a6kxhg6fxv91kbz0wkv57852ig0v19c")))

(define-public crate-regex-cache-0.1.2 (c (n "regex-cache") (v "0.1.2") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "00adqw65qrgmqf9xspqn10kmzs6ac127j9zz7gwqa1f9rfgp74w9")))

(define-public crate-regex-cache-0.1.4 (c (n "regex-cache") (v "0.1.4") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "oncemutex") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)))) (h "0ls65xiv3hqq63da8b9siy310vfx3h7ysjbm9ns4wsc04yid442i")))

(define-public crate-regex-cache-0.1.5 (c (n "regex-cache") (v "0.1.5") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "oncemutex") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)))) (h "1jmm6v1fn8h1amb6wxcg3mbv02nixx3ikqq1rnqb9v7dh17bm6kp")))

(define-public crate-regex-cache-0.1.6 (c (n "regex-cache") (v "0.1.6") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "oncemutex") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.5") (d #t) (k 0)))) (h "00sys909r92qm19yr75kn3hn64mi1q8ngg9a0h549xb51swsirjq")))

(define-public crate-regex-cache-0.2.0 (c (n "regex-cache") (v "0.2.0") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "oncemutex") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "16c6wfmlqjb99zm751gjzncd54pyzpj3xxy5nhivysxgyr8n7j1j")))

(define-public crate-regex-cache-0.2.1 (c (n "regex-cache") (v "0.2.1") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "oncemutex") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "14w7h1rl68fzzvavig18hf184macxcypqsrv6m7vkf23jzb64yrg")))

