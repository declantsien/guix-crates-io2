(define-module (crates-io re ge regecs) #:use-module (crates-io))

(define-public crate-regecs-0.1.0 (c (n "regecs") (v "0.1.0") (h "0xwvfx0qsfw8kcvh3jzwppzw0ps4r9frc5y032wnxkkl8amsybbs")))

(define-public crate-regecs-1.0.0 (c (n "regecs") (v "1.0.0") (h "0zmgcgcfzvmvfg65mgg635nzn13241nqw321kmw9d5jhgvbq1470")))

(define-public crate-regecs-2.0.0-alpha-1 (c (n "regecs") (v "2.0.0-alpha-1") (d (list (d (n "bpx") (r "^3.0.0") (d #t) (k 0)))) (h "0kzgpl0vx684rjxyh89awrn4g1v78w614pgkzn142040lcl2n33y")))

(define-public crate-regecs-2.0.0-rc.2.0.0 (c (n "regecs") (v "2.0.0-rc.2.0.0") (d (list (d (n "bpx") (r "^3.0.0") (d #t) (k 0)))) (h "1qd6cl3h0mc5ipq7m1yysmkm4q2hh73hdcv14560889vy8z4xry3")))

