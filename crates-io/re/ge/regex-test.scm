(define-module (crates-io re ge regex-test) #:use-module (crates-io))

(define-public crate-regex-test-0.1.0 (c (n "regex-test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "bstr") (r "^1.3.0") (f (quote ("std" "serde"))) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (f (quote ("parse"))) (k 0)))) (h "012nj2qjkxlv5zmnk53hc5a95kdsz8ss469s0a6fp5xdqbpi9f8l")))

