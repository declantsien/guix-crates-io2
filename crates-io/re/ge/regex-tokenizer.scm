(define-module (crates-io re ge regex-tokenizer) #:use-module (crates-io))

(define-public crate-regex-tokenizer-0.1.0 (c (n "regex-tokenizer") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-tokenizer-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xxivqfai4y5m99l3z7mcf7svi0g9nzmv6nv8p0gkjqbjzk9a1ly")))

(define-public crate-regex-tokenizer-0.1.1 (c (n "regex-tokenizer") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-tokenizer-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0svdknkjcri10ffsx8vl7ybag3xjr3v9l5pj21h1n7i1xcywg75r")))

