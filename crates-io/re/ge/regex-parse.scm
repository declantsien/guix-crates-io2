(define-module (crates-io re ge regex-parse) #:use-module (crates-io))

(define-public crate-regex-parse-0.1.0 (c (n "regex-parse") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "regex-parse-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0x25flj93s6arfpl0wchf88ik56rrydiwmi0wkisjcqbbnjjk2d0")))

