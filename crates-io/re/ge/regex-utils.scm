(define-module (crates-io re ge regex-utils) #:use-module (crates-io))

(define-public crate-regex-utils-0.1.0 (c (n "regex-utils") (v "0.1.0") (d (list (d (n "regex-automata") (r "^0.3.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "04anh3bvxn7ximhacpy5mz1clshwm49ypfjl4nzyqvn35iprs7wl")))

