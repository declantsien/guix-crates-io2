(define-module (crates-io re ge regexm) #:use-module (crates-io))

(define-public crate-regexm-0.1.0-beta.0 (c (n "regexm") (v "0.1.0-beta.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "051v7v2b3yphybb3lkdc241aids52i8ilgp6and0hlpm72zd2aw2")))

(define-public crate-regexm-0.1.0-beta.1 (c (n "regexm") (v "0.1.0-beta.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r084navwjkz496b0i69k3syq0xchl85ajawla5v6p5kddhdq52w")))

(define-public crate-regexm-0.1.0 (c (n "regexm") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vmfhavkgj6j9mbqwn2alvqbc9zy708n9br9gj600j7x7b2520zn")))

(define-public crate-regexm-0.2.0-beta.0 (c (n "regexm") (v "0.2.0-beta.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06agq6ja7ll0nf2gz08d9m4rpplqrmhppl7zklwbkkb1xlhmdh0p")))

(define-public crate-regexm-0.2.0-beta.1 (c (n "regexm") (v "0.2.0-beta.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gdb6l902agmzr5ya4vhqw503am0ah1f57frzyihgbqibz90n63s")))

(define-public crate-regexm-0.2.0 (c (n "regexm") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w2k9m6zh9izs5qka2whcz3nsak80lgyydfxrisqjyf61q8ii20a")))

(define-public crate-regexm-0.2.1 (c (n "regexm") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04wikviz2kmllhgzi9c5hh44958djbjfvdalih51vqzhbsg6r59v")))

