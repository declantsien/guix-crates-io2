(define-module (crates-io re ge regecs-codegen) #:use-module (crates-io))

(define-public crate-regecs-codegen-0.1.0 (c (n "regecs-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "090vz7xf7b6nmm0pwhsfp31mivabk7pjd91mf737x1xi21ikfjmn")))

(define-public crate-regecs-codegen-1.0.0 (c (n "regecs-codegen") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybg4bbs7dnvir60yg2zcmbjzfkziwyagq391hdybw99xc41vrsx")))

(define-public crate-regecs-codegen-2.0.0-rc.1.0.0 (c (n "regecs-codegen") (v "2.0.0-rc.1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w5w5npsnpqgnasibg3ign5701yayrv3k9mxzy9n0ymbn70v2pdg")))

