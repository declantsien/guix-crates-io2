(define-module (crates-io re ge regex_macros) #:use-module (crates-io))

(define-public crate-regex_macros-0.1.0 (c (n "regex_macros") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "0awglsa70i4yyb9ql288flikfrq5970p96an6bw1699m0vgwjrvq")))

(define-public crate-regex_macros-0.1.1 (c (n "regex_macros") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "18v6nxi13sfsnrzwlz402dgs22bf43ax3kc7md6qv791xaxk373q")))

(define-public crate-regex_macros-0.1.2 (c (n "regex_macros") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "1x2m1yy4ssinaqgyba11i6nxghk85dgwv88nw1ri06zjyx28b2gi")))

(define-public crate-regex_macros-0.1.3 (c (n "regex_macros") (v "0.1.3") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "16hxgw6ih2zph3dzfd08nr35izicash24mk2sar1in1b27a1cd5x")))

(define-public crate-regex_macros-0.1.4 (c (n "regex_macros") (v "0.1.4") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "041in0yd2775qqk6z4qw4yi065rbg06i2h8z6ij25f0i52a9rwqn")))

(define-public crate-regex_macros-0.1.5 (c (n "regex_macros") (v "0.1.5") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "1fn5pkcvc5224s1hx8sijsnijs4kx5dpvax717y75pyz3vkb69ij")))

(define-public crate-regex_macros-0.1.6 (c (n "regex_macros") (v "0.1.6") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "037i6fjvbg02lasiyqcyhsfgv04v9jihl3ki9ywya0x4vf1i53r3")))

(define-public crate-regex_macros-0.1.8 (c (n "regex_macros") (v "0.1.8") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "06c5pl3vv78464ibjjqic7jj8ch2pvxqdqf8j2m1q1mmf2xh419b")))

(define-public crate-regex_macros-0.1.9 (c (n "regex_macros") (v "0.1.9") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "05vahqcw3257lz3zzbcib0mvjdghbigdca1kcxcvj2w2wfgs6wsl")))

(define-public crate-regex_macros-0.1.10 (c (n "regex_macros") (v "0.1.10") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "014b3qv5fkl5lnrmgj82rnsj4ampwl7wx9jvv7ghv20hshfhz7vz")))

(define-public crate-regex_macros-0.1.11 (c (n "regex_macros") (v "0.1.11") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "11vs8mmvhzprgy17gnm2mcdi1qiaxh31abs7m01dnhakg17kh58a")))

(define-public crate-regex_macros-0.1.12 (c (n "regex_macros") (v "0.1.12") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "19r9hcikqrglc11abcvv5m65d5jpqah6xr89mddkchgrh0ppxnzh")))

(define-public crate-regex_macros-0.1.13 (c (n "regex_macros") (v "0.1.13") (d (list (d (n "rand") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0cwrfbh539b0w90wqn5x8yrkw1lm0g8jvhdswwzaf861w2rqp6p2")))

(define-public crate-regex_macros-0.1.14 (c (n "regex_macros") (v "0.1.14") (d (list (d (n "rand") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "017lr754mrm8w0i5nw53l9ajpna993w91js0cylz1kb0md9rn3cp")))

(define-public crate-regex_macros-0.1.15 (c (n "regex_macros") (v "0.1.15") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "1s0pqxgpyc6z8sbjj6k74v5dyfwywviyzf7wgsh7y7zz809bg8g2")))

(define-public crate-regex_macros-0.1.16 (c (n "regex_macros") (v "0.1.16") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0jlj7396q4mkyp8slc69r1d9pq4hjkclhi62cxbyqmrfycg7xamh")))

(define-public crate-regex_macros-0.1.17 (c (n "regex_macros") (v "0.1.17") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0fkzvlnv3crrc27yfna8ngic249pid2g160a2dbp9s9421y58rrj")))

(define-public crate-regex_macros-0.1.18 (c (n "regex_macros") (v "0.1.18") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0grc6w42cw8sxiqlx8a285gp0wvdl87n89xns7vahc60k4hm1w8y")))

(define-public crate-regex_macros-0.1.19 (c (n "regex_macros") (v "0.1.19") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0h2zy9r2ng82sb2ns9pqsq0m12a6wl9f3399l5iahf8bxiw9zcg0")))

(define-public crate-regex_macros-0.1.20 (c (n "regex_macros") (v "0.1.20") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "07732y413wc93z2nzl2bfj8jzdbqvw49j6gfzr6xzajbr2f9aq86")))

(define-public crate-regex_macros-0.1.21 (c (n "regex_macros") (v "0.1.21") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "12qy7zli0p2yb6l1vfgmng1c388m2scnrrlhrmpkdr7dgw2nkyp2")))

(define-public crate-regex_macros-0.1.22 (c (n "regex_macros") (v "0.1.22") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "1cw3xar7hx1p3wlnh90gkydz2l9w7c36ajyf3cfm6q9r553vycgx")))

(define-public crate-regex_macros-0.1.23 (c (n "regex_macros") (v "0.1.23") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "1k83hdg7y422n4h6ilikm0cz9i1hpjmi1pl2hk356qr2c8a5zbxl")))

(define-public crate-regex_macros-0.1.24 (c (n "regex_macros") (v "0.1.24") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.41") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0h1l8qgbz0b42n7mjnpyvkrkwv3x3q8m60070x2zsl0gfi1gjpwi")))

(define-public crate-regex_macros-0.1.25 (c (n "regex_macros") (v "0.1.25") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0zaj5djnlrxchi52pi8bqq38hlqijxhx3nvcz37196m5ypq8p7jq")))

(define-public crate-regex_macros-0.1.26 (c (n "regex_macros") (v "0.1.26") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.0") (f (quote ("pattern"))) (d #t) (k 0)))) (h "17ixfwlrznrzmx2kqi2ribsx0dy0flz01i4al8si4g7pkwp929m6")))

(define-public crate-regex_macros-0.1.27 (c (n "regex_macros") (v "0.1.27") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0nb95p9kpjinjk00741gsxsx4srr76ay1v2qkip9qnmnf774vddy")))

(define-public crate-regex_macros-0.1.28 (c (n "regex_macros") (v "0.1.28") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1") (f (quote ("pattern"))) (d #t) (k 0)))) (h "1mwgk0s43zk5drwy6ifjiyg70byxyasqv7z6hmajmyv9zbh137s4")))

(define-public crate-regex_macros-0.1.29 (c (n "regex_macros") (v "0.1.29") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 2)))) (h "0ks3mxcrjmn4q2kzg7fs4mfhzriry27ys3bp0zvl2m17y0zx1b6m")))

(define-public crate-regex_macros-0.1.30 (c (n "regex_macros") (v "0.1.30") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 2)))) (h "1zasv12kh7nba66hgmgxhs9yz8fh5z88xbgkg1ki2973sng1hs1d")))

(define-public crate-regex_macros-0.1.31 (c (n "regex_macros") (v "0.1.31") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.53") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 2)))) (h "0b2i75g9bfiriaync58hx9qpazgc7rvs6hhyis3m9mpf0a85ckjv")))

(define-public crate-regex_macros-0.1.32 (c (n "regex_macros") (v "0.1.32") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.53") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.2") (d #t) (k 2)))) (h "00wmnwf3yrviid96c1w6q5sa8pxwggg30dxikkk8mpfzzqkxrif9")))

(define-public crate-regex_macros-0.1.33 (c (n "regex_macros") (v "0.1.33") (d (list (d (n "regex") (r "^0.1.56") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.0") (d #t) (k 0)))) (h "0yyfrlaim2zlrjwsqxksyqf0ii30a1d66r9n1rh2aahp451jd7zr")))

(define-public crate-regex_macros-0.1.34 (c (n "regex_macros") (v "0.1.34") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.59") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.1") (d #t) (k 0)))) (h "1c6rm3fkbckkp7kjlj0zjj3lfny1nbs7w4ld470f4sgy2jfp8cs6")))

(define-public crate-regex_macros-0.1.35 (c (n "regex_macros") (v "0.1.35") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.63") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.1") (d #t) (k 0)))) (h "1sgdfqmlgd40jf5y47rcvci687psidwl69z7583sb4p6y1zbsnhz")))

(define-public crate-regex_macros-0.1.36 (c (n "regex_macros") (v "0.1.36") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.63") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.1") (d #t) (k 0)))) (h "1an78miggsqb1zc2r2bd59814icx1xz0dwj2vmn7lkvzi7k7948s")))

(define-public crate-regex_macros-0.1.37 (c (n "regex_macros") (v "0.1.37") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.63") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.1") (d #t) (k 0)))) (h "0vp7szmgrbfkkvj8sfjl3kz5prg0f2lv5xp178kj95ibx29awa0x")))

(define-public crate-regex_macros-0.1.38 (c (n "regex_macros") (v "0.1.38") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1.63") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.1") (d #t) (k 0)))) (h "11h11ainpmrb01wfwvbgz2ardix2w5y1j5501f9zc7p3cp970n4y")))

(define-public crate-regex_macros-0.2.0 (c (n "regex_macros") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "regex") (r "^0.2.0") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.4.0") (d #t) (k 0)))) (h "12z0vv20spvi698p328xnkiwhawsmr48hyrf7lpdnzqzxn2892ax")))

