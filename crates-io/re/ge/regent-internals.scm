(define-module (crates-io re ge regent-internals) #:use-module (crates-io))

(define-public crate-regent-internals-0.1.0 (c (n "regent-internals") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "08z6zwfvxcskfjafwdg93xrl7a9r51snscl5gkcllxji48xf8ijz")))

(define-public crate-regent-internals-0.2.0 (c (n "regent-internals") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "1pdnmf6agdrbf4m97hpwv0jz687lx2a9137clgbvivcdfhhfrjgr") (f (quote (("nightly") ("default"))))))

