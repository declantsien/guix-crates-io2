(define-module (crates-io re ge regenerator) #:use-module (crates-io))

(define-public crate-regenerator-0.1.0 (c (n "regenerator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "fuzzerang") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex-automata") (r "^0.3.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.3") (d #t) (k 0)))) (h "1w0w9dxf9mvvjxassczwr30har4ji6p7hva24xnz4739kq09gpw2")))

(define-public crate-regenerator-0.1.1 (c (n "regenerator") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "fuzzerang") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex-automata") (r "^0.3.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.3") (d #t) (k 0)))) (h "1spzh9yn8jgf70cnjrsd21pnck6x43bwbmgs1frk5cs9p9m8d1m2")))

(define-public crate-regenerator-0.1.2 (c (n "regenerator") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "fuzzerang") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex-automata") (r "^0.3.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.3") (d #t) (k 0)))) (h "178gcnpymfz22793ll5rw42qd4di0hbw58dnnbs8h043qzqnvjxl")))

