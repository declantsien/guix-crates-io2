(define-module (crates-io re ge regent) #:use-module (crates-io))

(define-public crate-regent-0.1.0 (c (n "regent") (v "0.1.0") (d (list (d (n "regent-internals") (r "^0.1.0") (d #t) (k 0)))) (h "1yccmyikphgnig71mjqpqimhni6jf70a48pxy541ijqirsydbgkr")))

(define-public crate-regent-0.2.0 (c (n "regent") (v "0.2.0") (d (list (d (n "regent-internals") (r "^0.2.0") (d #t) (k 0)))) (h "03g85mfmgr9k8qr6cxw04rgyxk2fzxnr04dk5vzibgpp0zydf9rb")))

(define-public crate-regent-0.3.0 (c (n "regent") (v "0.3.0") (h "1r5xs3bqhyqhbmjmqqzifwranp5gc0s07xh79wvnjym2gpshml5s")))

