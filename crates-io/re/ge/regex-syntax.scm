(define-module (crates-io re ge regex-syntax) #:use-module (crates-io))

(define-public crate-regex-syntax-0.1.0 (c (n "regex-syntax") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0i5r5pzqyjnqrpjxx002vmmg6b7310w6dzbl2yy247fdysi5529r")))

(define-public crate-regex-syntax-0.1.1 (c (n "regex-syntax") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1y3bbvb3g10qjpg5i663152yz2p6019mn8ljwkw16k750aiaaq90")))

(define-public crate-regex-syntax-0.1.2 (c (n "regex-syntax") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0l9hm2jxzbxlhfgak08sqpms4b679gfzyp3bcpf1nayyivp00cfh")))

(define-public crate-regex-syntax-0.1.3 (c (n "regex-syntax") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1fqz6blk1qgzaxdfk87p32rlwsmbjmv4ydj2c3razrrn79s26xda")))

(define-public crate-regex-syntax-0.2.0 (c (n "regex-syntax") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0nmj9xxycflkz3rr2zjfjqajzxm5sa725xyhgx9fsdfzl2ixvkvy")))

(define-public crate-regex-syntax-0.2.1 (c (n "regex-syntax") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "152cj0g63nk19fac6sxabf4d23w72g3yafvss0kqqz45wz15px4n")))

(define-public crate-regex-syntax-0.2.2 (c (n "regex-syntax") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07whxx99a1rdqcgizg178wl1258ahini60x3w9dq8g52n02yiwgw")))

(define-public crate-regex-syntax-0.2.3 (c (n "regex-syntax") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1c2vlw2fv04gk936mnr315r67kcnf06vx0a2asy1nv02hmcpj2a7")))

(define-public crate-regex-syntax-0.2.4 (c (n "regex-syntax") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1f2ixykk6sk56ql4yx6kyfgila4nlaxvpbgxvzdwwsn0ksv742aw")))

(define-public crate-regex-syntax-0.2.5 (c (n "regex-syntax") (v "0.2.5") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14wyh2qw2fm6l37gxyb162azsfj0gix1cnisdml1r5ll5wr89xjg")))

(define-public crate-regex-syntax-0.3.0 (c (n "regex-syntax") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0h5mj9pxlywsbyyvwvd6jvd1dh69r9pma7zbn62a1b345icjbw4i")))

(define-public crate-regex-syntax-0.3.1 (c (n "regex-syntax") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1afmjc30hyyl7b6xslbipbd03xq4rrgh9l5lwd1sc2anw2qr25c4")))

(define-public crate-regex-syntax-0.2.6 (c (n "regex-syntax") (v "0.2.6") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1irxgdwh1vwb2sys0gqdicjspf9z6nzgpbnivviliyjdbb73a6d2")))

(define-public crate-regex-syntax-0.3.2 (c (n "regex-syntax") (v "0.3.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "10w4iivrq4kc9qz9i9c9x2qnggghjk0724gfgnh4v71k2rrbwx50")))

(define-public crate-regex-syntax-0.3.3 (c (n "regex-syntax") (v "0.3.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "02yhwfhcgiq452pwaca99sf9s093p73h81rxxl5yvrvvp8ili85s")))

(define-public crate-regex-syntax-0.3.4 (c (n "regex-syntax") (v "0.3.4") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "16aim04l3dipdv6y78jxra4l5frplgxwyb9h8s69vbbhfjnhl11i")))

(define-public crate-regex-syntax-0.3.5 (c (n "regex-syntax") (v "0.3.5") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1zf8jazgs1k9j3pqjwh6f0g5qbhfbl43zaj4aghz8773g80h3517")))

(define-public crate-regex-syntax-0.3.6 (c (n "regex-syntax") (v "0.3.6") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1p8037rgjzc9w7vk2fymncrzzn6yc9rrr4rwqxnfskjsz75b1s9j")))

(define-public crate-regex-syntax-0.3.7 (c (n "regex-syntax") (v "0.3.7") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "04df1qz5n7lrr9x40mca52p1yajzbd32723gg3d4hnp9rqxmgw28")))

(define-public crate-regex-syntax-0.3.8 (c (n "regex-syntax") (v "0.3.8") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1lq1pal3p45mnzahpxqh88hf4rkc5i0c2apll648bqgk8x2g3b01")))

(define-public crate-regex-syntax-0.3.9 (c (n "regex-syntax") (v "0.3.9") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ms9hgdhhsxw9w920i7gipydvagf100bb56jbs192rz86ln01v7r")))

(define-public crate-regex-syntax-0.4.0 (c (n "regex-syntax") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0ms4s2zdr9lr79isjphqqg2wkc8rblvpwcax21ghj0vnynqr349g")))

(define-public crate-regex-syntax-0.4.1 (c (n "regex-syntax") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1nv2gwaks8ndd25h4axx79jl2a64h337a1f54xagalvrxxg0m2dd")))

(define-public crate-regex-syntax-0.4.2 (c (n "regex-syntax") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "03p24bsfg2rw2cc5h8ri4fp7j06xwyyd5grlqy0g11ixp5c1r4wf")))

(define-public crate-regex-syntax-0.5.0 (c (n "regex-syntax") (v "0.5.0") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0xrylqw9digzfyzz61j8qvzfqls18r94dgpg7apnxq4hgqg3kms8")))

(define-public crate-regex-syntax-0.5.1 (c (n "regex-syntax") (v "0.5.1") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "19lyza9cqy77mb7sd735djqkhmbvwrsa0121kxsazc2npxpbp3fy")))

(define-public crate-regex-syntax-0.5.2 (c (n "regex-syntax") (v "0.5.2") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "1a8cvx2s9k07qc7f85z0ips23c6vxv8f871a8r4wyqivqf97jcyq")))

(define-public crate-regex-syntax-0.5.3 (c (n "regex-syntax") (v "0.5.3") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0mip1ciw0lg05yfrgbn8jfdagywarv5h2bkcd9li9j8xqdv0hmdj")))

(define-public crate-regex-syntax-0.5.4 (c (n "regex-syntax") (v "0.5.4") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "041p6qsakg21zikkhgwsfzkpx0q0dkrjah36sbhpy458l2lpyvh9") (y #t)))

(define-public crate-regex-syntax-0.5.5 (c (n "regex-syntax") (v "0.5.5") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "1yzvkhv1qv0c6w3ksm4a5rpkqxzx43i4lwqlj90c797l8n9hg45x")))

(define-public crate-regex-syntax-0.5.6 (c (n "regex-syntax") (v "0.5.6") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "19zp25jr3dhmclg3qqjk3bh1yrn7bqi05zgr5v52szv3l97plw3x")))

(define-public crate-regex-syntax-0.6.0 (c (n "regex-syntax") (v "0.6.0") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0sxzf43dfr030pfxa6h5a4jlfm42arvc03m22g7wcp371pvc06lg")))

(define-public crate-regex-syntax-0.6.1 (c (n "regex-syntax") (v "0.6.1") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0m0xbqprkl1fhpbcd9jngrw49gsh4yj54nchbvy80y11ymsnmc05")))

(define-public crate-regex-syntax-0.6.2 (c (n "regex-syntax") (v "0.6.2") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0b91mky5sf8jzi1c88fb8g1bgpbks3fbra6zcwpnw7v56nra6yvl")))

(define-public crate-regex-syntax-0.6.3 (c (n "regex-syntax") (v "0.6.3") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0vmcaaibhipb89z36940jxqysbnc8q9jdbqw2a2gw25pqam5gigv")))

(define-public crate-regex-syntax-0.6.4 (c (n "regex-syntax") (v "0.6.4") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "1l8n99813xfs08xqlc5y95nxqvifk1x9wqz1c0crwyns57ns4isf")))

(define-public crate-regex-syntax-0.6.5 (c (n "regex-syntax") (v "0.6.5") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0q88mg3ibbadfw3fqd5lj8zp4dc14d5psgd603gmyafmvbp3abwc")))

(define-public crate-regex-syntax-0.6.6 (c (n "regex-syntax") (v "0.6.6") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "15ha1mlyp77h49lbkmqsclvj21df9afqd644v60pnadyxs0qdzfw")))

(define-public crate-regex-syntax-0.6.7 (c (n "regex-syntax") (v "0.6.7") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "0j0x5vdz3xvs1j138349pdq5hxf5xkh6528j0rzs3qzrhq342xlx")))

(define-public crate-regex-syntax-0.6.8 (c (n "regex-syntax") (v "0.6.8") (d (list (d (n "ucd-util") (r "^0.1.0") (d #t) (k 0)))) (h "088fwhmg1v4ynk093vbvqvjpln6d9kb5x7i05rmir711rq6360cv")))

(define-public crate-regex-syntax-0.6.9 (c (n "regex-syntax") (v "0.6.9") (d (list (d (n "ucd-util") (r "^0.1.4") (d #t) (k 0)))) (h "02kv0j62sfzrgj7mjj4ygsn2z1vsv3jmayzpc3xxy6cyfj7aib5z")))

(define-public crate-regex-syntax-0.6.10 (c (n "regex-syntax") (v "0.6.10") (d (list (d (n "ucd-util") (r "^0.1.4") (d #t) (k 0)))) (h "0p47lf38yj2g2fnmvnraccqlxwk35zr76hlnqi8yva932nzqam6d")))

(define-public crate-regex-syntax-0.6.11 (c (n "regex-syntax") (v "0.6.11") (h "0grli4djafrkckh4ilvcw5z3wwqmasqzi3lqf5b6vrd55kmwqhxi")))

(define-public crate-regex-syntax-0.6.12 (c (n "regex-syntax") (v "0.6.12") (h "05pplicvzvgkb2wb4i98p2mrpgc8gws6vdl8xlpyyr6f3h6y59qi") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.13 (c (n "regex-syntax") (v "0.6.13") (h "141xi6yiji0bc04c4skamd4ny9vnd1jrwc7qzfga425lyn8yhd77") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.14 (c (n "regex-syntax") (v "0.6.14") (h "01myl8xqpbnird23xnsb92sjmz1cmp69r6m7y3dwbpmsx4zzx3dj") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.15 (c (n "regex-syntax") (v "0.6.15") (h "157jw0rhxy3vhxdmz3yjqvfa0ks07xpb3ch5ajd27hkf185csikj") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.16 (c (n "regex-syntax") (v "0.6.16") (h "1hggxbahkkahjn4wxyrzd2jn84sdqvmkp720hid7703nj12zhchi") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.17 (c (n "regex-syntax") (v "0.6.17") (h "1blmlgzcg7in3kcxqabpfzzrbnamr2i671flbrmlqhfps5bvvrbz") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.18 (c (n "regex-syntax") (v "0.6.18") (h "1s648w7rwpxnq9iqwbyy43ar4al07906jpz0jxlql23bgjwjwh96") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.19 (c (n "regex-syntax") (v "0.6.19") (h "0qcps29hsxg5pc34zz43x404cvi0di0cqj2ym9q8i1daks6yhyy1") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.20 (c (n "regex-syntax") (v "0.6.20") (h "076irs10yk2nlw7iyq0s7s22mqn4sfi6cxr60pjy3k8m9lv7mawc") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.21 (c (n "regex-syntax") (v "0.6.21") (h "12d176jkgw9749g07zjxz0n78nyvb2nqx3j4sp5aqyphvji1n61v") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.22 (c (n "regex-syntax") (v "0.6.22") (h "10b56ylil35jkb4nwqxm8hbyx3zq7fws0wpydjln165s8xql3sxm") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.23 (c (n "regex-syntax") (v "0.6.23") (h "0j25v3pbfaprpr1k7h8smw5xrs6j5kzznddq50nzcq1f2n4z1m94") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.24 (c (n "regex-syntax") (v "0.6.24") (h "1cmgiysghm32vxf85rhf4phqhmjmcn2zc84x2sr6ykxsb5sbivq0") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.25 (c (n "regex-syntax") (v "0.6.25") (h "16y87hz1bxmmz6kk360cxwfm3jnbsxb3x4zw9x1gzz7khic2i5zl") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.26 (c (n "regex-syntax") (v "0.6.26") (h "0r6vplrklxq7yx7x4zqf04apr699swbsn6ipv8bk82nwqngdxcs9") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.27 (c (n "regex-syntax") (v "0.6.27") (h "0i32nnvyzzkvz1rqp2qyfxrp2170859z8ck37jd63c8irrrppy53") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.28 (c (n "regex-syntax") (v "0.6.28") (h "0j68z4jnxshfymb08j1drvxn9wgs1469047lfaq4im78wcxn0v25") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.6.29 (c (n "regex-syntax") (v "0.6.29") (h "1qgj49vm6y3zn1hi09x91jvgkl2b1fiaq402skj83280ggfwcqpi") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("default" "unicode"))))))

(define-public crate-regex-syntax-0.7.0 (c (n "regex-syntax") (v "0.7.0") (h "1x9291yil7li639dy96pv6rxh8hm33jqsn9kiwj359cvhyb8i1mn") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.7.1 (c (n "regex-syntax") (v "0.7.1") (h "0g1s6ra0ra8xy1fxscspd406c3pn53bjm1is8phamlwvy6a656d5") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.7.2 (c (n "regex-syntax") (v "0.7.2") (h "0y4czjqxlw97j9bw5lpa18drxf8y3iv5jah3dwih6agdfq70ass3") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.7.3 (c (n "regex-syntax") (v "0.7.3") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0in8p3lrrwryws7xpygi46qk8hss0lh5rzbhirqs9r1hfb37vc1a") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.7.4 (c (n "regex-syntax") (v "0.7.4") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qjczlc2w92kamn9ipjdr5pjql0jnccahpi9l3r6wp0rnsjr5sp5") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.7.5 (c (n "regex-syntax") (v "0.7.5") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nhjmqdlakfi4yb8lh7vbbh71dsy90jjvrjvvnrih6larldgpdfv") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.60.0")))

(define-public crate-regex-syntax-0.8.0 (c (n "regex-syntax") (v "0.8.0") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ggq1dhdrxr37hzh9avjs4a4inw67xcch948xg60fjvqp60v1jy3") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.65")))

(define-public crate-regex-syntax-0.8.1 (c (n "regex-syntax") (v "0.8.1") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cwd69syyk8141p3cjhzsgai1ava1g8k7lqdr27h6sq38zflzn2n") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.65")))

(define-public crate-regex-syntax-0.8.2 (c (n "regex-syntax") (v "0.8.2") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17rd2s8xbiyf6lb4aj2nfi44zqlj98g2ays8zzj2vfs743k79360") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.65")))

(define-public crate-regex-syntax-0.8.3 (c (n "regex-syntax") (v "0.8.3") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mhzkm1pkqg6y53xv056qciazlg47pq0czqs94cn302ckvi49bdd") (f (quote (("unicode-segment") ("unicode-script") ("unicode-perl") ("unicode-gencat") ("unicode-case") ("unicode-bool") ("unicode-age") ("unicode" "unicode-age" "unicode-bool" "unicode-case" "unicode-gencat" "unicode-perl" "unicode-script" "unicode-segment") ("std") ("default" "std" "unicode")))) (s 2) (e (quote (("arbitrary" "dep:arbitrary")))) (r "1.65")))

