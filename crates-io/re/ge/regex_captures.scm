(define-module (crates-io re ge regex_captures) #:use-module (crates-io))

(define-public crate-regex_captures-0.1.0 (c (n "regex_captures") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1din8bd53wa2g3ax660jf0qgl8bkazhi24yqdk4s9mpbs7qy9gxb")))

(define-public crate-regex_captures-0.1.1 (c (n "regex_captures") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bxhvq7dn56prarfd296scp13miz83f7ia3s4j8fxm776ginij6d")))

(define-public crate-regex_captures-0.2.0 (c (n "regex_captures") (v "0.2.0") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0dpdklsxvv6fxxw1rk9xcby8zpa6hwf2d4cy1f6zsl1cdzv51sin")))

