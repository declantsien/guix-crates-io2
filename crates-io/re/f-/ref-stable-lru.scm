(define-module (crates-io re f- ref-stable-lru) #:use-module (crates-io))

(define-public crate-ref-stable-lru-0.1.0 (c (n "ref-stable-lru") (v "0.1.0") (h "0gi9s5ya254nqqqyqs2v7s7nm144nzz5hlqk2vf5f7ppb203ich2")))

(define-public crate-ref-stable-lru-0.2.1 (c (n "ref-stable-lru") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1k3mr8yz0prsw6kzm6mc839y02c03a4b8fx5fhbsx3rdmi9k3xkk")))

(define-public crate-ref-stable-lru-0.2.2 (c (n "ref-stable-lru") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1alri9ni9z1iw06nyavjjbrm4c425c68s9rs3i1g6qnrhh3k3c7h")))

