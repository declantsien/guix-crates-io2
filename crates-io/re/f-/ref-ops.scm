(define-module (crates-io re f- ref-ops) #:use-module (crates-io))

(define-public crate-ref-ops-0.1.0 (c (n "ref-ops") (v "0.1.0") (h "1746z1z5fd58nl1b35ma4i5w6bx9j8maa7h1244ya85ii1dpvcf6") (y #t)))

(define-public crate-ref-ops-0.1.1 (c (n "ref-ops") (v "0.1.1") (h "0lbzi7zdpyfd6py155y9gvwy58b3cskqkn2l2djmhd0c2k283lkp") (y #t)))

(define-public crate-ref-ops-0.1.2 (c (n "ref-ops") (v "0.1.2") (h "0gispkwk14js319kmp70crm6cdcjinj0wnnva6v5igqb882y52rp") (y #t)))

(define-public crate-ref-ops-0.2.0 (c (n "ref-ops") (v "0.2.0") (h "0h2hfja2mmqrr1placy91crnw5bnh5r9phasidvvgi5d1pwi7wqw") (y #t)))

(define-public crate-ref-ops-0.2.1 (c (n "ref-ops") (v "0.2.1") (h "00km6qqzf75iwh4k0fd2lbzfblkbwkcbxby0whn3l5apl1clsg0l") (y #t)))

(define-public crate-ref-ops-0.2.2 (c (n "ref-ops") (v "0.2.2") (h "0fqmdkwqn5zkfjyaqwaidxy5y5wpj982pkb6kchlc5j8wqc7ad05") (y #t)))

(define-public crate-ref-ops-0.2.3 (c (n "ref-ops") (v "0.2.3") (h "047rplr064zciv47igfz42ljvz5siidfrfq9y6xlcxmpwmdi9fwb") (y #t)))

(define-public crate-ref-ops-0.2.4 (c (n "ref-ops") (v "0.2.4") (h "0jszd2qq2pgrj97wfpi4v2bmfw1f4bz4ix0lsiws939jwd8086b8") (y #t)))

(define-public crate-ref-ops-0.2.5 (c (n "ref-ops") (v "0.2.5") (h "00yi4gkwga6pp1d3rnnx5lzaxd1ic1zn58q33411hvscmag2chx7")))

