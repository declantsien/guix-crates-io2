(define-module (crates-io re f- ref-mut-n) #:use-module (crates-io))

(define-public crate-ref-mut-n-1.0.0 (c (n "ref-mut-n") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jh1mzh1jnh21pcvqp58101a92s9rjb8cn4d4rv2vsybaasgxd5i")))

(define-public crate-ref-mut-n-1.0.1 (c (n "ref-mut-n") (v "1.0.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yl7ph9fig4xrb2wzk1sw4077rs6srvvxsh0s2czlyd918g0yj3d")))

