(define-module (crates-io re f- ref-cast-impl) #:use-module (crates-io))

(define-public crate-ref-cast-impl-0.1.0 (c (n "ref-cast-impl") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0vv563kgj0p7gqcrh2g3xid1f28yb27zhhb35y1wkh7h91lrh8n6")))

(define-public crate-ref-cast-impl-0.2.0 (c (n "ref-cast-impl") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1dwx22fd4cy2d24p6c9f72vzggiq1d4jq9yqkmigyc48z8dn587j")))

(define-public crate-ref-cast-impl-0.2.1 (c (n "ref-cast-impl") (v "0.2.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0sra3jwar40q28h5cvgkjzq615wzw4dpp4wz82l0yl4yx8vbsdpb")))

(define-public crate-ref-cast-impl-0.2.2 (c (n "ref-cast-impl") (v "0.2.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1i0xxj98i7am3b087sm22ss9lv2h2x8nypd2p590c0r2kwspxa7q")))

(define-public crate-ref-cast-impl-0.2.3 (c (n "ref-cast-impl") (v "0.2.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0xdlfrda137i8pf0qs1yn4hmiwqw59jy4a4sk5zh5kpvh5ba897d")))

(define-public crate-ref-cast-impl-0.2.4 (c (n "ref-cast-impl") (v "0.2.4") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0w5z7d0k46pflwaz4gnymdcjv0bw0w576hb6lgn5dzsxm2vhl84y")))

(define-public crate-ref-cast-impl-0.2.5 (c (n "ref-cast-impl") (v "0.2.5") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0q91br61nr890skwyq5w0g293cpwwnhyy51p8acjkfv5fxd37jcb")))

(define-public crate-ref-cast-impl-0.2.6 (c (n "ref-cast-impl") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0hw0frpzna5rf5szix56zyzd0vackcb3svj94ndj629xi75dkb32")))

(define-public crate-ref-cast-impl-0.2.7 (c (n "ref-cast-impl") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0av43xxjlinfqklb67rpj217cmaxfjsf8151gs0hbs4hnr5664ck")))

(define-public crate-ref-cast-impl-1.0.0 (c (n "ref-cast-impl") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07rc752npmkyc5b8xcqk2ydbl3gxi1n4fzrq0wx9wz5qd4mvavn3")))

(define-public crate-ref-cast-impl-1.0.1 (c (n "ref-cast-impl") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j7fyfv7safs51871pfwh91b7lh1pivgnhaqqapbkz36vafbabk0")))

(define-public crate-ref-cast-impl-1.0.2 (c (n "ref-cast-impl") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i1i3an8si070aqg2mvz6yqc6y2pl9zhd6dd2piz17l7mdsv88bx")))

(define-public crate-ref-cast-impl-1.0.3 (c (n "ref-cast-impl") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wjgpcabdpa0da5lag214xc07lrp1csrm128i4lb1i4axp53qlhc")))

(define-public crate-ref-cast-impl-1.0.4 (c (n "ref-cast-impl") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0c81jh5mih29lvs7mmq29rbzg0r0h7sp1r8la96iwnfpnhdn5rf2")))

(define-public crate-ref-cast-impl-1.0.5 (c (n "ref-cast-impl") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1l5w1fqx9wm8lg8wq5j2r8nc7323kpbag353cd9mk9ksq0zigmcr")))

(define-public crate-ref-cast-impl-1.0.6 (c (n "ref-cast-impl") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1lndalzfwhrpknibjs027zjcfjvincxqhyv3b4wwn89brnpf6f2c")))

(define-public crate-ref-cast-impl-1.0.7 (c (n "ref-cast-impl") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "07zs83c6nj07h1b1jssmqi6p5xa3xp03l66598vnjhf955784hx0") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.8 (c (n "ref-cast-impl") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0m0zy2d298n5wr96kxxahsygwdyig3xs2bpnpw1n2a308iqc89jz") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.9 (c (n "ref-cast-impl") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1ykqlp3jc0vvyq8gdhddr10hafh4mjvb2lrvj0r5x2i5cdhcsd2j") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.10 (c (n "ref-cast-impl") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1dh1halb6igad1x3kjbjipksypmi8r553khj0pa99qjwlzx9iaya") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.11 (c (n "ref-cast-impl") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1yrqw58psbfwg4x3r19xmwnaxkpx21awxjsa0jql2dlwx98jjg11") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.12 (c (n "ref-cast-impl") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0s03qqqw3kglg5kya14qwxsp5s762dhvwqqlhqhx5bxc055dx1sq") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.13 (c (n "ref-cast-impl") (v "1.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0dlg5b3qs3mgh298454qvibda7p0036x78w5cpgd6kz9x48qbymb") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.14 (c (n "ref-cast-impl") (v "1.0.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0pzfrixs4vnhcrr70jp71aycb2h65vz82qhh0r44wr03my90r74z") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.15 (c (n "ref-cast-impl") (v "1.0.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "06w8kkb1qn2pbcn5gmcwd7kh0vxfsvkmvi9v8dsy50ir740i4l4w") (r "1.31")))

(define-public crate-ref-cast-impl-1.0.16 (c (n "ref-cast-impl") (v "1.0.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1rzrfk7hl28gvc44ws06d5yywra8c131lk1nlyr0l1c3njm7a8ld") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.17 (c (n "ref-cast-impl") (v "1.0.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "14psk784nnvh70mmgza88drgfsmp0vs7zfqp8f1ybzpgv48b0c59") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.18 (c (n "ref-cast-impl") (v "1.0.18") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "05kjg9g9akzb0yns255a5zvdkbwk0wa9kp1cf8k6h25nv7d57gv8") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.19 (c (n "ref-cast-impl") (v "1.0.19") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1yn91c66nrl94ypljc6m85mi3gfmqrdiyygkjz3pcqknbg4g1yid") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.20 (c (n "ref-cast-impl") (v "1.0.20") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "09a9cscyfpdmbcmk5klz39hcnmclb4b3w3nxjf0h146grz176x3z") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.21 (c (n "ref-cast-impl") (v "1.0.21") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1i4x0wdbbgq9q06w1mip567vmmgwblzkq1177glc5wj5d2zw8ri5") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.22 (c (n "ref-cast-impl") (v "1.0.22") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1k5w51zyy06kcnr0vw71395mx1xrlxlpma35zjx2w2lvv7wb9paz") (r "1.56")))

(define-public crate-ref-cast-impl-1.0.23 (c (n "ref-cast-impl") (v "1.0.23") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1rpkjlsr99g8nb5ripffz9n9rb3g32dmw83x724l8wykjgkh7hxw") (r "1.56")))

