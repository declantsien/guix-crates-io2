(define-module (crates-io re f- ref-extended) #:use-module (crates-io))

(define-public crate-ref-extended-0.1.0 (c (n "ref-extended") (v "0.1.0") (h "04p8jcrm9kprsxbnhaz0fjarlvcy38kzd19cj94wi5pi4pf57cxw") (y #t)))

(define-public crate-ref-extended-0.1.1 (c (n "ref-extended") (v "0.1.1") (h "0sxgcihczqd5w329kcjksmrph2yj468h302cdykqpi0yh6is03sx") (y #t)))

(define-public crate-ref-extended-0.1.2 (c (n "ref-extended") (v "0.1.2") (h "1xzr325smfayzg8w73hmv4yxawjjjr5c0kf9xisnd0rgz51bvcvx") (y #t)))

(define-public crate-ref-extended-0.1.3 (c (n "ref-extended") (v "0.1.3") (h "02qf90ks74dyqpab864x3819cb9ixyahkay3r6zxch8nprdvjywj") (y #t)))

(define-public crate-ref-extended-0.2.0 (c (n "ref-extended") (v "0.2.0") (h "041264x022748fv8h1sh3n3fynk3y11k6f2zqgzd25ddsyllazd1")))

(define-public crate-ref-extended-0.2.1 (c (n "ref-extended") (v "0.2.1") (h "1z7bwi9bkhxbxkncmxb7sjnqndnxhv41q4m5psd3sn7npk6pvf78")))

