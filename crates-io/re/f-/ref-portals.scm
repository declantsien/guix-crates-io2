(define-module (crates-io re f- ref-portals) #:use-module (crates-io))

(define-public crate-ref-portals-1.0.0-beta.1 (c (n "ref-portals") (v "1.0.0-beta.1") (d (list (d (n "assert-impl") (r "^0.1.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)) (d (n "wyz") (r "^0.2.0") (d #t) (k 0)))) (h "0a1871nwrs2idd5fw2pz05y54y16i6dd4y63wyfmsvh3f4ajr3gs")))

(define-public crate-ref-portals-1.0.0-beta.2 (c (n "ref-portals") (v "1.0.0-beta.2") (d (list (d (n "assert-impl") (r "^0.1.3") (d #t) (k 2)) (d (n "assert-panic") (r "=1.0.0-preview.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)) (d (n "wyz") (r "^0.2.0") (d #t) (k 0)))) (h "1lmq1gmrfgcdhh1mbvrkwgqki08vialb14c00nilddhkrd2fcv73")))

