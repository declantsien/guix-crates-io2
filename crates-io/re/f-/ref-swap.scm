(define-module (crates-io re f- ref-swap) #:use-module (crates-io))

(define-public crate-ref-swap-0.1.0 (c (n "ref-swap") (v "0.1.0") (h "1k6gcnf9kjx9l98l9zx4x534dz1g5pgk8wglsfzrbh5i6dj4lvmh")))

(define-public crate-ref-swap-0.1.1 (c (n "ref-swap") (v "0.1.1") (h "1sr69h63zalkbmgm7v8fa3wic95iqzgw66dh0ysi1pv7drprxssr")))

(define-public crate-ref-swap-0.1.2 (c (n "ref-swap") (v "0.1.2") (h "0g7v535w9vqm08gvar3i2ah92p25lc5dbmc8y05b9rgyvxa0rhq9")))

