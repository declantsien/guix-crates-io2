(define-module (crates-io re f- ref-cast) #:use-module (crates-io))

(define-public crate-ref-cast-0.1.0 (c (n "ref-cast") (v "0.1.0") (d (list (d (n "ref-cast-impl") (r "^0.1") (d #t) (k 0)))) (h "1474lhc1zif901321nirp3zcfswzxq07w34pvh3m54qvhxizxzn6")))

(define-public crate-ref-cast-0.2.0 (c (n "ref-cast") (v "0.2.0") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "0r3mvbnp60b7lynpvvv6gggijjbisxwn2434q2ws42ihk61651gz")))

(define-public crate-ref-cast-0.2.1 (c (n "ref-cast") (v "0.2.1") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "0w3pbihc2dxd6q5b7chvjv8x3rgcwhn31pdl2h4wk48khg621viz")))

(define-public crate-ref-cast-0.2.2 (c (n "ref-cast") (v "0.2.2") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "168nv0vcj7lbz202pwhfz5lq0391jd9c0r7vxhlcz70saalm5cra")))

(define-public crate-ref-cast-0.2.3 (c (n "ref-cast") (v "0.2.3") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "02b2d2r5c43sqvdb6hcb3f08xvr6953b0cb5bpyx9d13in7f4cqb")))

(define-public crate-ref-cast-0.2.4 (c (n "ref-cast") (v "0.2.4") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "01m40ip334bx248h9bxyxcpvgfihwa7h5jb2jp8gh8rcwy5sd9vh")))

(define-public crate-ref-cast-0.2.5 (c (n "ref-cast") (v "0.2.5") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "0bd8q0vi6fxydbaxfkfq94fjz7zw7w13qi7a6kp6jwqf7xfsa081")))

(define-public crate-ref-cast-0.2.6 (c (n "ref-cast") (v "0.2.6") (d (list (d (n "ref-cast-impl") (r "^0.2") (d #t) (k 0)))) (h "0jgj1zxaikqm030flpifbp517fy4z21lly6ysbwyciii39bkzcf1")))

(define-public crate-ref-cast-0.2.7 (c (n "ref-cast") (v "0.2.7") (d (list (d (n "ref-cast-impl") (r "= 0.2.7") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fcbpfb7xhr992qvyfg9hr5p63xqykjp48pm3f7a1q21vmhzksvv")))

(define-public crate-ref-cast-1.0.0 (c (n "ref-cast") (v "1.0.0") (d (list (d (n "ref-cast-impl") (r "= 1.0.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vy378bdzb4kcz13kh96c5n5qw1jinhfrya5j4bf9rxz65x1jzq7")))

(define-public crate-ref-cast-1.0.1 (c (n "ref-cast") (v "1.0.1") (d (list (d (n "ref-cast-impl") (r "= 1.0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lfdmiay4fchzncpdw4zdhazcm09zh7cidwdc70kzdp1fmw4q88a")))

(define-public crate-ref-cast-1.0.2 (c (n "ref-cast") (v "1.0.2") (d (list (d (n "ref-cast-impl") (r "=1.0.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "08r6qz7228k55nlyl5v7ykdzxrasnawgzmb1jrbfbnkx2s3ifp3l")))

(define-public crate-ref-cast-1.0.3 (c (n "ref-cast") (v "1.0.3") (d (list (d (n "ref-cast-impl") (r "=1.0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.32") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kmyva8qifql967b6n5fqv1ybkr8dsk7541ppy25pwxwyjr2cxp1")))

(define-public crate-ref-cast-1.0.4 (c (n "ref-cast") (v "1.0.4") (d (list (d (n "ref-cast-impl") (r "=1.0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.32") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g8akn898qfckrzc90v7npiizxl7m2zvqqpsbfnawhl7ph9iqgyx")))

(define-public crate-ref-cast-1.0.5 (c (n "ref-cast") (v "1.0.5") (d (list (d (n "ref-cast-impl") (r "=1.0.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.32") (f (quote ("diff"))) (d #t) (k 2)))) (h "0pgrgmr72dmb7xh0w0nssi316jih1yj894mm24y8jf6xfwy8ljz8")))

(define-public crate-ref-cast-1.0.6 (c (n "ref-cast") (v "1.0.6") (d (list (d (n "ref-cast-impl") (r "=1.0.6") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.32") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nj1wp530yd1rx545qisvllvp7nbv9x00iax57p391w0bn1jl3rh")))

(define-public crate-ref-cast-1.0.7 (c (n "ref-cast") (v "1.0.7") (d (list (d (n "ref-cast-impl") (r "=1.0.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1arjfjs0b52wa4z43p6v2rmppnssyr5pr8l8rkj86avcbdi5hpb8") (r "1.31")))

(define-public crate-ref-cast-1.0.8 (c (n "ref-cast") (v "1.0.8") (d (list (d (n "ref-cast-impl") (r "=1.0.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "12l29aj44iwkdg69w538dkf0d4yk3hfi3yckdbv67x8c8d08jv3p") (r "1.31")))

(define-public crate-ref-cast-1.0.9 (c (n "ref-cast") (v "1.0.9") (d (list (d (n "ref-cast-impl") (r "=1.0.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0arkgnl28zalp4r1h8h415rh1lji2qlr0r59014v8jj9079bq4zd") (r "1.31")))

(define-public crate-ref-cast-1.0.10 (c (n "ref-cast") (v "1.0.10") (d (list (d (n "ref-cast-impl") (r "=1.0.10") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1haf4wjh5vjyv39hyi81zgh9mj993x4cy87n6d8z6az3ycrgdsxq") (r "1.31")))

(define-public crate-ref-cast-1.0.11 (c (n "ref-cast") (v "1.0.11") (d (list (d (n "ref-cast-impl") (r "=1.0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "059k3pqyyhw6pfgh479kqsn9igljj0vxpg4dmflm29r75y4r5lvh") (r "1.31")))

(define-public crate-ref-cast-1.0.12 (c (n "ref-cast") (v "1.0.12") (d (list (d (n "ref-cast-impl") (r "=1.0.12") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hj9mzzashgvkykm38jb63imb76ggxlzhj7y2d4rp4kcfkqk79qj") (r "1.31")))

(define-public crate-ref-cast-1.0.13 (c (n "ref-cast") (v "1.0.13") (d (list (d (n "ref-cast-impl") (r "=1.0.13") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i1ca3rwgvrd0pzp2ii1yci2jidjmgvrxjnqgiv0vmprnkmmvcak") (r "1.31")))

(define-public crate-ref-cast-1.0.14 (c (n "ref-cast") (v "1.0.14") (d (list (d (n "ref-cast-impl") (r "=1.0.14") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1vdryz081qiyi1d10vny209g5shcjp5b9rzwys7d9g4kja6gny4c") (r "1.31")))

(define-public crate-ref-cast-1.0.15 (c (n "ref-cast") (v "1.0.15") (d (list (d (n "ref-cast-impl") (r "=1.0.15") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1m9xrvanqh7mmy2l2g3lj856cxkzbc4q0pjijw0623pqkvq2rbx9") (r "1.31")))

(define-public crate-ref-cast-1.0.16 (c (n "ref-cast") (v "1.0.16") (d (list (d (n "ref-cast-impl") (r "=1.0.16") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0b5iyxdxwrmysi36injr2zi7mlv9m2473sbhxr0nicy8n68slgzl") (r "1.56")))

(define-public crate-ref-cast-1.0.17 (c (n "ref-cast") (v "1.0.17") (d (list (d (n "ref-cast-impl") (r "=1.0.17") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hv7cqpqn3i5x1kqv7pkfgzim5sr8b4piaam8a7m9d8nbwd7pl45") (r "1.56")))

(define-public crate-ref-cast-1.0.18 (c (n "ref-cast") (v "1.0.18") (d (list (d (n "ref-cast-impl") (r "=1.0.18") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0sxzy3f0zmclsmi1z17n16xbjbp99d5c6nh7592yy6f3fya82h8n") (r "1.56")))

(define-public crate-ref-cast-1.0.19 (c (n "ref-cast") (v "1.0.19") (d (list (d (n "ref-cast-impl") (r "=1.0.19") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1w9x0ris7ds9krfqg5rr181i1100z1a5116gn7fl46c4x0c7xvv1") (r "1.56")))

(define-public crate-ref-cast-1.0.20 (c (n "ref-cast") (v "1.0.20") (d (list (d (n "ref-cast-impl") (r "=1.0.20") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "102j2yqipkizkc85p2k469l35j0rv7p88nrb1yh9viz9fg85ipmc") (r "1.56")))

(define-public crate-ref-cast-1.0.21 (c (n "ref-cast") (v "1.0.21") (d (list (d (n "ref-cast-impl") (r "=1.0.21") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ir9gd0mkpp0rnr0wqsz19cmz8ksqwx2qij3zgpsx1i6y74kwcak") (r "1.56")))

(define-public crate-ref-cast-1.0.22 (c (n "ref-cast") (v "1.0.22") (d (list (d (n "ref-cast-impl") (r "=1.0.22") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gs7m6rikdf1k0vk6irnf9g0vwpf4ilzg2pg7cd1nwnia166v164") (r "1.56")))

(define-public crate-ref-cast-1.0.23 (c (n "ref-cast") (v "1.0.23") (d (list (d (n "ref-cast-impl") (r "=1.0.23") (d #t) (k 0)) (d (n "ref-cast-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ca9fi5jsdibaidi2a55y9i1k1q0hvn4f6xlm0fmh7az9pwadw6c") (r "1.56")))

