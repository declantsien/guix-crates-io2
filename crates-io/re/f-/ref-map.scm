(define-module (crates-io re f- ref-map) #:use-module (crates-io))

(define-public crate-ref-map-0.1.1 (c (n "ref-map") (v "0.1.1") (h "1bgb8ljwp9svlgdyvnsq5cpqdf0y86wsmd0n9s47fvv2gqk46wg0")))

(define-public crate-ref-map-0.1.2 (c (n "ref-map") (v "0.1.2") (h "0pgajd5lak3bq2j4qh9ihxibb79gdxbpdbv3rr0jlh993z9q2ckh")))

(define-public crate-ref-map-0.1.3 (c (n "ref-map") (v "0.1.3") (h "0xbhpw62xajvb0h4h6y94bjk0yqif3a8mq558l22d7rnbsc76ayj")))

