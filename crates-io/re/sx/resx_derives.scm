(define-module (crates-io re sx resx_derives) #:use-module (crates-io))

(define-public crate-resx_derives-0.1.0 (c (n "resx_derives") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "resx") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0qhh477j0vfyr86wnhlrwgmri8z6wkld1nswi4gdc2xyrqcy1nk4")))

(define-public crate-resx_derives-0.1.1 (c (n "resx_derives") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "resx") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ri8mdp2ssra14d9x6sfq4pv63by3wspm2dlidc89j80f69qxlm4")))

