(define-module (crates-io re _t re_tensor_ops) #:use-module (crates-io))

(define-public crate-re_tensor_ops-0.2.0-alpha.0 (c (n "re_tensor_ops") (v "0.2.0-alpha.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08xgc3aqinkrsh08njdmnzm315h7m6kjb1nrdhblkink004y7jf3") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.1 (c (n "re_tensor_ops") (v "0.2.0-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cq22cl8is28clknxzwcpddd1bvbkjk1jaiiaxhgfvj0xcw4cbff") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.2 (c (n "re_tensor_ops") (v "0.2.0-alpha.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yr4q23xa40230imjr70zmp2bqcvxvrdh70d6qjz9sn5rxfdqsmb") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.3 (c (n "re_tensor_ops") (v "0.2.0-alpha.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fpf92qyz63yrdjgahfd8a01q6hywdjsb2dvb02b267a7m10d6mp") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.4 (c (n "re_tensor_ops") (v "0.2.0-alpha.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01l82imwzr61dh6hm07yhkl674l1yd18m6bsh0n5qgrx3gf5agpv") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.5 (c (n "re_tensor_ops") (v "0.2.0-alpha.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0388pqb3fi80ky6hcjl2bkpl8vgwfcnwqxzfl7j4b956mr3l9h1y") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.6 (c (n "re_tensor_ops") (v "0.2.0-alpha.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06l7dsj51qrkqs5r3f29ycrqrck37irfkjsd1lff91j60bqgaszl") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0-alpha.8 (c (n "re_tensor_ops") (v "0.2.0-alpha.8") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0-alpha.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lhawxr8gxrqrc6riqn2y5dmnfcm7gsfzh72y1nrlhyd94jn8nla") (r "1.67")))

(define-public crate-re_tensor_ops-0.2.0 (c (n "re_tensor_ops") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0axx9b4zq4i8nbh64icsa939fj7q078i29rp36i8sw7i6xxisry1") (r "1.67")))

(define-public crate-re_tensor_ops-0.3.0-alpha.1 (c (n "re_tensor_ops") (v "0.3.0-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "=0.3.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zl0727hmy2jq25n7x8lh918r5qi5jdr3s0c7asjpjnxqvz7a2w9") (r "1.67")))

(define-public crate-re_tensor_ops-0.3.0 (c (n "re_tensor_ops") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ggs1gv1wl47044j4fvp20jfhlp5mh2xlpl8ihsb2s3v2dg33sc0") (r "1.67")))

(define-public crate-re_tensor_ops-0.3.1 (c (n "re_tensor_ops") (v "0.3.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qs0idrnvqf2nsak3qb225s60nq1qjwss19i29j2ihavla6br5kj") (r "1.67")))

(define-public crate-re_tensor_ops-0.4.0 (c (n "re_tensor_ops") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("extern_crate_alloc"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hhl0sw8ypk607j10yshiwwvsqwnl7nnmdn404glbmj2f75a2l2n") (r "1.67")))

(define-public crate-re_tensor_ops-0.5.0-alpha.0 (c (n "re_tensor_ops") (v "0.5.0-alpha.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.5.0-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qqvdj0q0r4yd40j2kdmz5ggvmmbypdylvfdwx2kq5v2nmwal25x") (r "1.67")))

(define-public crate-re_tensor_ops-0.5.0 (c (n "re_tensor_ops") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qh9ii41ig3p6rz06mhs6msjmah878b8vc2ch9mdshlfh2qvc9hh") (r "1.67")))

(define-public crate-re_tensor_ops-0.5.1-alpha.1 (c (n "re_tensor_ops") (v "0.5.1-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "=0.5.1-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g1g8kip1gwdyvy601mn6vlfc9qpprr5mgqndsbc2b2vw5vr7srn") (r "1.67")))

(define-public crate-re_tensor_ops-0.5.1 (c (n "re_tensor_ops") (v "0.5.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jm3z1lfkkfh4i6w1v32amp6xg65by7q4sr08p1kajvrm5ysjgdh") (r "1.67")))

(define-public crate-re_tensor_ops-0.6.0-alpha.1 (c (n "re_tensor_ops") (v "0.6.0-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "=0.6.0-alpha.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a45fy2h6simgb9mp56kj01sjil05lwq6ri0r1wxaj6ph9xw282i") (r "1.69")))

(define-public crate-re_tensor_ops-0.6.0-alpha.2 (c (n "re_tensor_ops") (v "0.6.0-alpha.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "=0.6.0-alpha.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10vfwnv7m2lp0bc1ad8jlx0swpwfy2zab035qjpwnaq3syx8l6bk") (r "1.69")))

(define-public crate-re_tensor_ops-0.6.0 (c (n "re_tensor_ops") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zifmvw8gp8qnch7bcfkl0cbn5fvhqfp9n39ha53rwvf7qv20cd6") (r "1.69")))

(define-public crate-re_tensor_ops-0.7.0-alpha.1 (c (n "re_tensor_ops") (v "0.7.0-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "=0.7.0-alpha.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17qvh4lg8g19mx10hdzy048xnqjw3s436pji8laya8nifdikzajz") (r "1.69")))

(define-public crate-re_tensor_ops-0.7.0 (c (n "re_tensor_ops") (v "0.7.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_log_types") (r "^0.7.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qx6lvya5y7alb4a84k93ciydcm4gklw26833p23q03nprq4idcl") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.0-alpha.2 (c (n "re_tensor_ops") (v "0.8.0-alpha.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "=0.8.0-alpha.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04f5chqyj0vjxnm9zbbk524zcg0y45ivmgmsfsl6y27ywv81h0nx") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.0-alpha.6 (c (n "re_tensor_ops") (v "0.8.0-alpha.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "=0.8.0-alpha.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1skf625mgq32wflcyl405ika0qy2m4la0wmp1dxh0ym9xkfps3fj") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.0-alpha.7 (c (n "re_tensor_ops") (v "0.8.0-alpha.7") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "=0.8.0-alpha.7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nyxs71d0cqak7xkzv0x4xz33xh5jqqyhx5xpyhdyq93n9r8yz5p") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.0 (c (n "re_tensor_ops") (v "0.8.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "^0.8.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03dbkaq62j2813fqy7jxwpq8140gz0vbbyvak9c9vk2g6gkak950") (r "1.69")))

(define-public crate-re_tensor_ops-0.9.0-alpha.1 (c (n "re_tensor_ops") (v "0.9.0-alpha.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "=0.9.0-alpha.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mxl9mwp0r50dbbfxbf6m34iahl0ylyfw5nln85ahvw9i9jl0m1d") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.1 (c (n "re_tensor_ops") (v "0.8.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "^0.8.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wp0n59rjmjklnw72r55p0ys71ddfmlr0khjg1kc4k9a0vm4j3gs") (r "1.69")))

(define-public crate-re_tensor_ops-0.8.2 (c (n "re_tensor_ops") (v "0.8.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_components") (r "^0.8.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nmfg8sivds6dxzxvx968zmgsr3liwjgdh741j89mfymv2qx2ldf") (r "1.69")))

(define-public crate-re_tensor_ops-0.9.0-alpha.5 (c (n "re_tensor_ops") (v "0.9.0-alpha.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-alpha.5") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0barqj67l0fvri5nq0ypc5cw6z59zlfd7xzzgxk2p82yyw1y5nh4") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-alpha.6 (c (n "re_tensor_ops") (v "0.9.0-alpha.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-alpha.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r05jv49688a2g8i66f0c7s1i4dkkryk1gdqxx4s22m12bvmrf20") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-alpha.9 (c (n "re_tensor_ops") (v "0.9.0-alpha.9") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-alpha.9") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bvn1cq9imnvn7plqqqa7lvj17fh5ig2mdl6mi0i4nz7m0r0gshz") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-alpha.10 (c (n "re_tensor_ops") (v "0.9.0-alpha.10") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-alpha.10") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11rrhxbwq45h4gk2f2m2nc3v8had4fhb315dr7pdmzpnf4axqfq4") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-rc.2 (c (n "re_tensor_ops") (v "0.9.0-rc.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-rc.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jixz54zrhylqs0llw6bqylbd3m8hfmbwrpnvnj1764vk3w3z5ql") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-rc.3 (c (n "re_tensor_ops") (v "0.9.0-rc.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-rc.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ggr7z64ygigh4iz0m14dclxfq1fa4a3v521gb3342ckaf0hh9x8") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-rc.6 (c (n "re_tensor_ops") (v "0.9.0-rc.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-rc.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c1gbz2qnjba0g298zv5jr0cn614dq2fhpabcw32wa9iifqy85a0") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0-rc.7 (c (n "re_tensor_ops") (v "0.9.0-rc.7") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.0-rc.7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h5q3lpb2f1icmgzs9y0hz9i71ivj84yx44va5xx4qv8x919k7f") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.0 (c (n "re_tensor_ops") (v "0.9.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "^0.9.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yx3fpvaccf6qrdqaqcixpnpcsvk55yy671rwq8dd2r5c0z3rkdk") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.2 (c (n "re_tensor_ops") (v "0.10.0-alpha.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n7k4pzbc5a1cmyj19dn1dqpkb4l0zrghb0whyzb9hmpb2bdppwk") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.3 (c (n "re_tensor_ops") (v "0.10.0-alpha.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x0znkvh3pr6mr2z3ij7vij6lws509yi5kxd1ashrzcc5fzmbr4p") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.4 (c (n "re_tensor_ops") (v "0.10.0-alpha.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "029491jbbrprffqdw6y90844sa245j24lk70wxssyk1b3cs2bar5") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.5 (c (n "re_tensor_ops") (v "0.10.0-alpha.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.5") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g8m1dngxxz2balf9j0yk4r95yk15jl9w3d2vsidmyn3bvr416j2") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.1-rc.3 (c (n "re_tensor_ops") (v "0.9.1-rc.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.1-rc.3") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03i1aa36nc9sk9fbsqvxhbwyqk8x01vd4ssfrx9llm3la2i7snil") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.1-rc.4 (c (n "re_tensor_ops") (v "0.9.1-rc.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.9.1-rc.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pdwq4cych9g7mlkrw14kkn4jwjxk2vqkdjmz03cpi4mxxsxk5di") (r "1.72")))

(define-public crate-re_tensor_ops-0.9.1 (c (n "re_tensor_ops") (v "0.9.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "^0.9.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sjr8hrjx9zgjvwn17z99mqlb7k0lvx80xaqzcla7ki6xz8a1jq5") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.8 (c (n "re_tensor_ops") (v "0.10.0-alpha.8") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.8") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c0w0lwkczizrnkhjkr6vn70rzrrdsbg2lck7p9fyqg3ll39nifn") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-alpha.10 (c (n "re_tensor_ops") (v "0.10.0-alpha.10") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-alpha.10") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wa93cpgyqsp8d30b9k0m7263abzdqsvhg85ijb358ki9phampls") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0-rc.1 (c (n "re_tensor_ops") (v "0.10.0-rc.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.0-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bjx8a36wvaddwwd2r2n9pc3npl6z2fl8qj1j62af6wh22vjcj4h") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.0 (c (n "re_tensor_ops") (v "0.10.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "^0.10.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vriv50p2imfcpr85qdirs0g1wbr4b11ws4a6hfsjsalp4d1xcll") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.1-rc.1 (c (n "re_tensor_ops") (v "0.10.1-rc.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.10.1-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k8xg8pyywrzmxsfd3ib7dzgm1rl0vn28r45d8znc0av99pbaxk9") (r "1.72")))

(define-public crate-re_tensor_ops-0.10.1 (c (n "re_tensor_ops") (v "0.10.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "^0.10.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18l1ghfdrrcvs2bwcgd6f8rs0cv4ssv65wgp0l91jg6l09wc2gaf") (r "1.72")))

(define-public crate-re_tensor_ops-0.11.0-rc.1 (c (n "re_tensor_ops") (v "0.11.0-rc.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.11.0-rc.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10av6h79sv2svs0ghlksa1xy1md0pnfxw8gjp56zhza87lgj6qmg") (r "1.72")))

(define-public crate-re_tensor_ops-0.11.0-rc.2 (c (n "re_tensor_ops") (v "0.11.0-rc.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "=0.11.0-rc.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zli2nl583gjnbk22wqqra848dxmzwjp84vnqq69ki3y5qw1d6mr") (r "1.72")))

(define-public crate-re_tensor_ops-0.11.0 (c (n "re_tensor_ops") (v "0.11.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "re_types") (r "^0.11.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a92by4pi4x7b4ywfdscl56zm27y9zbmph4dcrw2gc7dny9yvy66") (r "1.72")))

