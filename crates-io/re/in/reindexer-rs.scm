(define-module (crates-io re in reindexer-rs) #:use-module (crates-io))

(define-public crate-reindexer-rs-0.1.0 (c (n "reindexer-rs") (v "0.1.0") (d (list (d (n "reindexer-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1lw2b8jmvvkrf4f7dxmcci5f05wc5rhyc6fhrjrniirb4sg55nx2")))

(define-public crate-reindexer-rs-0.1.1 (c (n "reindexer-rs") (v "0.1.1") (d (list (d (n "reindexer-sys") (r "^0.1") (d #t) (k 0)))) (h "1rzyijyxwav162m1j1pif26b2gb3ka6xzgjvhca3q9g8izl64h0a")))

(define-public crate-reindexer-rs-0.2.0 (c (n "reindexer-rs") (v "0.2.0") (d (list (d (n "reindexer-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0k3a6acgmzqyvcwynw42k28m3rqyzwzs8vnff7cnk6idld6qyaz4")))

(define-public crate-reindexer-rs-0.2.2 (c (n "reindexer-rs") (v "0.2.2") (d (list (d (n "reindexer-sys") (r "^0.2") (d #t) (k 0)))) (h "0i0p7l86yl9mwby6129gfjyzc6m610i2yqqxh0f029g53j4m951h")))

