(define-module (crates-io re in reinterpret) #:use-module (crates-io))

(define-public crate-reinterpret-0.1.0 (c (n "reinterpret") (v "0.1.0") (h "0nbkbll849pq8mpsanr1m2x1ipmfqh59failcyzz21mg0jahrd49")))

(define-public crate-reinterpret-0.1.1 (c (n "reinterpret") (v "0.1.1") (h "1imia1qqrm6x5fgck3gfpvqsy3pfmbvs81m6c5wcx9dda23nni5y")))

(define-public crate-reinterpret-0.1.2 (c (n "reinterpret") (v "0.1.2") (h "0p2va75jliys8b8m9mqlfm0vk80vpizxdzpsyvwib0iylb2aa9hw")))

(define-public crate-reinterpret-0.2.0 (c (n "reinterpret") (v "0.2.0") (h "14xkw5p1p7zm5qn63skxb3i4hnfgwn8rifjbc48igdggnqwq99kz")))

(define-public crate-reinterpret-0.2.1 (c (n "reinterpret") (v "0.2.1") (h "06ljaa44s6glf37zyl0nsbrl37ik6i31f1ndbpx4n95gprw0139i")))

