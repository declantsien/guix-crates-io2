(define-module (crates-io re in reindexer-sys) #:use-module (crates-io))

(define-public crate-reindexer-sys-0.1.0 (c (n "reindexer-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "0m8abavlaj8s8x8j0gcrxdbpsq4s2fp1kdj0gy4k6sfj9c82ks4j")))

(define-public crate-reindexer-sys-0.1.1 (c (n "reindexer-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "0csix5d1bc0596ivw91prji6gilxrvbxyxa0vp5yr7j675p3r0c5")))

(define-public crate-reindexer-sys-0.2.0 (c (n "reindexer-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "1ngdhwszcxnd38davm3d92h4v80971fv1v4lz50iqs0bkaia4fi1")))

(define-public crate-reindexer-sys-0.2.2 (c (n "reindexer-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)))) (h "10v7n6zfl5j9iky8d61b1zlnbihnm9kwyzphj21xvqi0j3iq1343")))

