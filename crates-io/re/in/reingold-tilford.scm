(define-module (crates-io re in reingold-tilford) #:use-module (crates-io))

(define-public crate-reingold-tilford-1.0.0 (c (n "reingold-tilford") (v "1.0.0") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "04psv121267d4c7v339i1wqkp0jr9pi7w6ckqmjlm501csk887zp")))

