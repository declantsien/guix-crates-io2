(define-module (crates-io re in reinda-core) #:use-module (crates-io))

(define-public crate-reinda-core-0.0.1 (c (n "reinda-core") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1271y5gg6jj4i4yxdag6apl6j29jdi4y2mw3gcd4i5npmdgrijf4")))

(define-public crate-reinda-core-0.0.2 (c (n "reinda-core") (v "0.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twoway") (r "^0.2.1") (d #t) (k 0)))) (h "0mriq5zpxibqm78lqvg3g1dwlx61jm4zsyslgdkbgwcw4alccjga") (f (quote (("debug-is-prod"))))))

(define-public crate-reinda-core-0.0.3 (c (n "reinda-core") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18wipb6vf4mw6axdchab1ky2lp1jxrvhjzrfv0ra2gawg3nixdqj") (f (quote (("debug-is-prod"))))))

