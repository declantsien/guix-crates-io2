(define-module (crates-io re in reinhold) #:use-module (crates-io))

(define-public crate-reinhold-0.4.0 (c (n "reinhold") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "087bryg459ahgsl1sbzkbkxy7n8id06d1f6a3aq6x956hfikzb3x")))

(define-public crate-reinhold-0.5.0 (c (n "reinhold") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0p93naq3yjwc1ky0fbpb55smjykjikssqhy4xwx4y4wi8spiv7gk")))

