(define-module (crates-io re in reindeer-macros) #:use-module (crates-io))

(define-public crate-reindeer-macros-0.3.0 (c (n "reindeer-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.78") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cvcl05sq4pxl2a4l5yb7p2k69pdfqmgcz7n4mjmv060gnar26l5")))

