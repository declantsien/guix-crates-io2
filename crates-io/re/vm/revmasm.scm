(define-module (crates-io re vm revmasm) #:use-module (crates-io))

(define-public crate-revmasm-0.1.0 (c (n "revmasm") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1r66jhmnd5s6j1ixz76gfdgrr5sz97gd8v5y0pyjdd0jjl7zig8n") (r "1.66")))

(define-public crate-revmasm-0.1.1 (c (n "revmasm") (v "0.1.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dmfl82jpc0dqkc8hhxyycx2bc3y4p0kj9b83r7b83lzr21f4kis") (y #t) (r "1.66")))

(define-public crate-revmasm-0.1.2 (c (n "revmasm") (v "0.1.2") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zcqkjhs7jwabi1b7yw5yf1c6ha79zl26nx7bahbl51mlbkn4ccb") (r "1.66")))

