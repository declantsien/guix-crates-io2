(define-module (crates-io re ss ressa-path) #:use-module (crates-io))

(define-public crate-ressa-path-0.0.1 (c (n "ressa-path") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "1mz32iz6c9bi0pn1sj3pacf8qn6cqw31plabggl837k57jq2sbks") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

