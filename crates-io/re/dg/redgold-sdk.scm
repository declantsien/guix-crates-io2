(define-module (crates-io re dg redgold-sdk) #:use-module (crates-io))

(define-public crate-redgold-sdk-0.1.9 (c (n "redgold-sdk") (v "0.1.9") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1vggfbkx493n8bfmsgq2sgz3ncdpix5gx5fqb1f4107wcfhc5393")))

(define-public crate-redgold-sdk-0.1.10 (c (n "redgold-sdk") (v "0.1.10") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1bkg7r880mx08jhk1yrp6gisbric87dm8zj76v9yvph2a59ardxl")))

(define-public crate-redgold-sdk-0.1.11 (c (n "redgold-sdk") (v "0.1.11") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1irc086yfrv3km6kszx7p32km9lxrd9x7a7ivsfaxh1zx9zmxsc9")))

(define-public crate-redgold-sdk-0.1.12 (c (n "redgold-sdk") (v "0.1.12") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1f3a9wlcsbm6l3g3lm6l3ah08pc1q6v6jn63m8jc0k78d9faxhkr")))

(define-public crate-redgold-sdk-0.1.14 (c (n "redgold-sdk") (v "0.1.14") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1kymdq30jsygxjv875alwwwj2154fsnn7zk62dlpkgd6sv5n27db")))

(define-public crate-redgold-sdk-0.1.15 (c (n "redgold-sdk") (v "0.1.15") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0idqyy4bl3zgkw9zyrrhg7g5vw84d3gfa9j8hx1yxprwxywc5hzj")))

(define-public crate-redgold-sdk-0.1.16 (c (n "redgold-sdk") (v "0.1.16") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "16j5f4x2krvc9gliwy58xm9nfpb43g92j0fswravnf9wfwsw516k")))

(define-public crate-redgold-sdk-0.1.17 (c (n "redgold-sdk") (v "0.1.17") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1zs5ylg495nwy80ihmhxsl4aksp62ybm2592y5g569khv76cfh0v")))

(define-public crate-redgold-sdk-0.1.18 (c (n "redgold-sdk") (v "0.1.18") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1gy5yc9vghbvklcsxjq7822w6c70spf4xalz4wjxxdlac30b6ncv")))

(define-public crate-redgold-sdk-0.1.20 (c (n "redgold-sdk") (v "0.1.20") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "10yza0idn8q7ddxb064651i43ks3fzchj398wskk0h8x7nqx8vas")))

(define-public crate-redgold-sdk-0.1.21 (c (n "redgold-sdk") (v "0.1.21") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1zihrnny2g5axiwkbcags7aka4j06m1q0rwz0v4r9bxxk7b2vyqk")))

(define-public crate-redgold-sdk-0.1.22 (c (n "redgold-sdk") (v "0.1.22") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "13a4z1gmn0wrvlss9ka8bb2bkfhxsazra3vl3nl2p3n144pwp12g")))

(define-public crate-redgold-sdk-0.1.23 (c (n "redgold-sdk") (v "0.1.23") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "17k3c02gf9694fhdng9g218h143vpp29vd41wkm66ivpqcffdwqz")))

(define-public crate-redgold-sdk-0.1.24 (c (n "redgold-sdk") (v "0.1.24") (d (list (d (n "extism-pdk") (r "^0.3.3") (d #t) (k 0)) (d (n "redgold-schema") (r "^0.1.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0x77y004hg4iwyfyjia19n5d5ba681adwj9yh8mhw608wnpiirhc")))

