(define-module (crates-io re dg redgold-sdk-client) #:use-module (crates-io))

(define-public crate-redgold-sdk-client-0.1.9 (c (n "redgold-sdk-client") (v "0.1.9") (d (list (d (n "redgold-schema") (r "^0.1.9") (d #t) (k 0)))) (h "01syhkfknhz6rgb9701h59yip5334c5rhz08ninbkpcisaiyscdc")))

(define-public crate-redgold-sdk-client-0.1.10 (c (n "redgold-sdk-client") (v "0.1.10") (d (list (d (n "redgold-schema") (r "^0.1.10") (d #t) (k 0)))) (h "1razx6dhpbayhfhfa1ap5kyya9gia933g7v5njcs7ahdm2fxisf8")))

(define-public crate-redgold-sdk-client-0.1.11 (c (n "redgold-sdk-client") (v "0.1.11") (d (list (d (n "redgold-schema") (r "^0.1.11") (d #t) (k 0)))) (h "1871q47x7a5q3fsn70zfwns3gx5940b5glbw2b4akv6sj2izkssh")))

(define-public crate-redgold-sdk-client-0.1.12 (c (n "redgold-sdk-client") (v "0.1.12") (d (list (d (n "redgold-schema") (r "^0.1.12") (d #t) (k 0)))) (h "0wwg4s4j8n033q4rjmg8iz1fvavaqi3m6mnpqdmls2qhcixczr60")))

(define-public crate-redgold-sdk-client-0.1.14 (c (n "redgold-sdk-client") (v "0.1.14") (d (list (d (n "redgold-schema") (r "^0.1.14") (d #t) (k 0)))) (h "1nc2jm9nknpd3ahx4q6d35pz17v13psyxb77h8p2dvb7gn063z6n")))

(define-public crate-redgold-sdk-client-0.1.15 (c (n "redgold-sdk-client") (v "0.1.15") (d (list (d (n "redgold-schema") (r "^0.1.15") (d #t) (k 0)))) (h "16bz8hsdymnnaaf28mfivnrpsmf41pjd5vbp18rxpci67pc1mfzj")))

(define-public crate-redgold-sdk-client-0.1.16 (c (n "redgold-sdk-client") (v "0.1.16") (d (list (d (n "redgold-schema") (r "^0.1.16") (d #t) (k 0)))) (h "1mr12r67ks2bx8h17fajqv2acsk74sh26abz2vfswcdha7f7b1r4")))

(define-public crate-redgold-sdk-client-0.1.17 (c (n "redgold-sdk-client") (v "0.1.17") (d (list (d (n "redgold-schema") (r "^0.1.17") (d #t) (k 0)))) (h "0k9lvcwfwl2hj96gr99grryir42fq9xg2p8h7qyd9204zrpmm425")))

(define-public crate-redgold-sdk-client-0.1.18 (c (n "redgold-sdk-client") (v "0.1.18") (d (list (d (n "redgold-schema") (r "^0.1.18") (d #t) (k 0)))) (h "0jzx59hr7lmzpzx73p8871iywdqhkvm91hjb7vq5qyricpydghx8")))

(define-public crate-redgold-sdk-client-0.1.20 (c (n "redgold-sdk-client") (v "0.1.20") (d (list (d (n "redgold-schema") (r "^0.1.20") (d #t) (k 0)))) (h "0hcsy5yxvpxp3422cwjq2wmzfs3g1y2hmrvadrmnpjv8x2ssj5rc")))

(define-public crate-redgold-sdk-client-0.1.21 (c (n "redgold-sdk-client") (v "0.1.21") (d (list (d (n "redgold-schema") (r "^0.1.21") (d #t) (k 0)))) (h "1m13xhlbvz4s48lfvc7g37wcnrxflkxj6mwq0j1lwl1v0swqrlh2")))

(define-public crate-redgold-sdk-client-0.1.22 (c (n "redgold-sdk-client") (v "0.1.22") (d (list (d (n "redgold-schema") (r "^0.1.22") (d #t) (k 0)))) (h "0v35wzkd5l01r62hzacsgcl8l48w8ifq4gwbwz4n060d9mqvfip8")))

(define-public crate-redgold-sdk-client-0.1.23 (c (n "redgold-sdk-client") (v "0.1.23") (d (list (d (n "redgold-schema") (r "^0.1.23") (d #t) (k 0)))) (h "04vfdg9wzszf1qangwpl9946925af5rcj5lmwydis47lsarra508")))

(define-public crate-redgold-sdk-client-0.1.24 (c (n "redgold-sdk-client") (v "0.1.24") (d (list (d (n "redgold-schema") (r "^0.1.24") (d #t) (k 0)))) (h "1aybqljn9byywprcdfg9qvsg6v57waffcxhfnfa2003m688vgffm")))

