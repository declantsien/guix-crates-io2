(define-module (crates-io re la relational_types_procmacro) #:use-module (crates-io))

(define-public crate-relational_types_procmacro-1.0.0 (c (n "relational_types_procmacro") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "11s3nb40zh13m05hgpwbdhggjjxrl19d9pziha65jyibsn5cq4xg")))

(define-public crate-relational_types_procmacro-1.1.0 (c (n "relational_types_procmacro") (v "1.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "06xr17r85fa3pvpzgdxb1bhx2a4q9rqpbr7b428a885cs80xfgz2")))

(define-public crate-relational_types_procmacro-2.0.0 (c (n "relational_types_procmacro") (v "2.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "13hji1phj4fk9vynkajbx44zc9faqmchj3jskwh7y71cwb7f86yh")))

