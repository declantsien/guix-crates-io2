(define-module (crates-io re la relaunch) #:use-module (crates-io))

(define-public crate-relaunch-0.1.0 (c (n "relaunch") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (d #t) (k 2)))) (h "1rpb6bbb986l481ll57zzhwf91v4vv3qj1dnfpsncgpisxwp0p0j")))

(define-public crate-relaunch-0.1.1 (c (n "relaunch") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (d #t) (k 2)))) (h "0dkdd1lsf5kdd1zg7ph23wksf9zh6zdgcl91x78nbwl7bk6xf66k")))

(define-public crate-relaunch-0.1.2 (c (n "relaunch") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (d #t) (k 2)))) (h "0k42s8ql00i83cwrrwlxn3wgifvcj5629pskgf97pq6qi7rixn05")))

