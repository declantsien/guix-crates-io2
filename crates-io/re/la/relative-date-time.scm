(define-module (crates-io re la relative-date-time) #:use-module (crates-io))

(define-public crate-relative-date-time-0.1.0 (c (n "relative-date-time") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)))) (h "0v14299d6kmjsb63jjw065f6lh5rkxxz9dgbaj523jhscqcy3ris")))

