(define-module (crates-io re la relativedelta) #:use-module (crates-io))

(define-public crate-relativedelta-0.1.0 (c (n "relativedelta") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde1") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "1d2ls7pnfnpgs80vrh8glyqwr5qphgbj8qwy6s806li63spgckk8") (f (quote (("serde" "serde1")))) (y #t)))

(define-public crate-relativedelta-0.2.0 (c (n "relativedelta") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18qn57hhw82b8qvvw4krzknpdv5370j8nr5mdc39599vxy95rn31") (f (quote (("serde1" "serde")))) (y #t)))

(define-public crate-relativedelta-0.2.1 (c (n "relativedelta") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02f43q29z59wmnhgf8hb2wzxngk7g1xd0rlfrd7cga0i6nqkwfrp") (f (quote (("serde1" "serde"))))))

(define-public crate-relativedelta-0.2.2 (c (n "relativedelta") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vl50diaigqdc6naaywq65r4g5jgpzprjnbhcz7q4ych1mzfxd6b") (f (quote (("serde1" "serde"))))))

