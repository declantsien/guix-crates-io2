(define-module (crates-io re la relay-sr201) #:use-module (crates-io))

(define-public crate-relay-sr201-0.1.0 (c (n "relay-sr201") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wvj6cbyqcyhj1brd30xm0fxn80hymr4b2r9i0sl4mzpgw52nn5v")))

(define-public crate-relay-sr201-0.2.0 (c (n "relay-sr201") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qw6hr78mrr21sbjf8rclsxg9v3yriay3h8z7gn4mmc43ycmfihg")))

