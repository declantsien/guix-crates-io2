(define-module (crates-io re la relay8x) #:use-module (crates-io))

(define-public crate-relay8x-0.0.1 (c (n "relay8x") (v "0.0.1") (h "0mh69jhz6nis5c4y11kmqn7ndlidq1vn267g6c7r3s0z0g7zssfl")))

(define-public crate-relay8x-0.0.2 (c (n "relay8x") (v "0.0.2") (h "04yhcrs10f4h4pm0rhnrv870kif37svlwwvhlqccmmdgkny9aljr")))

(define-public crate-relay8x-0.1.0 (c (n "relay8x") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0n50pklpmc5kap29y1mav4xkd21f0v4jwc3hs3mcn8msnh6gr75c")))

(define-public crate-relay8x-0.1.1 (c (n "relay8x") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "1xpr7sbi36afn92pk54nx0dssl3ja3p9ryn9k70334zlwk48saby")))

