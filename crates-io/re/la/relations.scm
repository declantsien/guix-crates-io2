(define-module (crates-io re la relations) #:use-module (crates-io))

(define-public crate-relations-0.1.0 (c (n "relations") (v "0.1.0") (h "1zv93bg09d0iih8w0hf56z7x9hzcvm17i8m21d8k7szwaw7p60nv")))

(define-public crate-relations-0.2.0 (c (n "relations") (v "0.2.0") (h "1gy9mymq5g1lxq8gcd6mxcvdvxagy4w6s9z4grim9ilsx619d1zf")))

(define-public crate-relations-0.3.0 (c (n "relations") (v "0.3.0") (h "1ycxwbn6z52zsdz4lgpfxlraqbcvjjjncn3ajzavq6bzrhf5lrj0")))

