(define-module (crates-io re la relational_types) #:use-module (crates-io))

(define-public crate-relational_types-1.0.0 (c (n "relational_types") (v "1.0.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "relational_types_procmacro") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed_index_collection") (r "^1") (d #t) (k 0)))) (h "0gnj0rbwb9x80b4dnbxpwqq19fslkfjjm870japj0sf2vydn0gaj") (f (quote (("default" "relational_types_procmacro"))))))

(define-public crate-relational_types-1.0.1 (c (n "relational_types") (v "1.0.1") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "relational_types_procmacro") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed_index_collection") (r "^1") (d #t) (k 0)))) (h "1ic5qai7890sd1ypzgvql3myrx5yxhqsihkf848g4dp2nmbv93sq") (f (quote (("default" "relational_types_procmacro"))))))

(define-public crate-relational_types-1.1.0 (c (n "relational_types") (v "1.1.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "relational_types_procmacro") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed_index_collection") (r "^1") (d #t) (k 0)))) (h "0vqpn5001qawxjwf2in04nbrqzprcn5h1y82x6vynjhja6za0hkz") (f (quote (("default" "relational_types_procmacro"))))))

(define-public crate-relational_types-2.0.0 (c (n "relational_types") (v "2.0.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "relational_types_procmacro") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed_index_collection") (r "^2") (d #t) (k 0)))) (h "0ps59mw74f5bv6a4fawqa21kl934p7r3lp19gmf0zbscfyxnz0jm") (f (quote (("default" "relational_types_procmacro"))))))

