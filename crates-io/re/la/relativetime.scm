(define-module (crates-io re la relativetime) #:use-module (crates-io))

(define-public crate-relativetime-0.1.0 (c (n "relativetime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "1gvca3mlk41rblk6711d9nqx577cnqdadbg9ylfdv8jvpl33b4na") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-relativetime-0.1.1 (c (n "relativetime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "03fjfqhbmc76kmh48x2yq1l953iakk19n1j56rfhgvccm6qpl1if") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-relativetime-0.1.3 (c (n "relativetime") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "0widjdbcagsv0chknmxrf3r5bgmp1rpafvdxqj744qlnfn2n1xwp") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-relativetime-0.1.4 (c (n "relativetime") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "138w9q45094d31z50ki1wzalncy8k0x39qakabdyhbkv3zl97c47") (s 2) (e (quote (("chrono" "dep:chrono"))))))

