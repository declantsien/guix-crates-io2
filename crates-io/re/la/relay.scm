(define-module (crates-io re la relay) #:use-module (crates-io))

(define-public crate-relay-0.0.0 (c (n "relay") (v "0.0.0") (h "1vrhlichhrj5ymvw01cz25gpg6zl74ikwdjxp8cr955zblvbl319")))

(define-public crate-relay-0.1.0 (c (n "relay") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1ic1yzlx8n3a25xrxq5f6cgh9j14rwpxn6q3f18whrq8nvzbl0gk")))

(define-public crate-relay-0.1.1 (c (n "relay") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "16j8y57rjrfy3h5xfi9fwfbjs1nka3iifi52rvp9szldd21f6xhm")))

