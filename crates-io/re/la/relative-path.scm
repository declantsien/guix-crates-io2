(define-module (crates-io re la relative-path) #:use-module (crates-io))

(define-public crate-relative-path-0.1.0 (c (n "relative-path") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1c8w9m9n914vwb3v23f8vrx88sfhm6ris2hl5kr0gp2pvy5m0vq6") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.1.1 (c (n "relative-path") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xj7w6g9q6r4n1b1zi7raylp4idpw6d43l31xy67v107jmh4pc91") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.1.2 (c (n "relative-path") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0j059wr6jpjl1706ikcaz5fini0vv4dm9dly4idni0zbg5594gbs") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.1.3 (c (n "relative-path") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xbgz4kkc0lxqx8mk9bis86vha78d81l4m8fq9dhahwlagjg5j0s") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.1.4 (c (n "relative-path") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pc59vkb69rgs83m3bxmn13ybiiip327786jyimlz87g0v20pslh") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.1.5 (c (n "relative-path") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "144pimhsrpsn9llkx6zqmdhx00h1dfj9ciwj3nxay1yrwvm2wgay") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.0 (c (n "relative-path") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yps4pbm3dni49kjy8zabmhwwrhchma85byg8pkcainw4z5sldld") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.1 (c (n "relative-path") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06gs0vdn6qgl0w6zf1s88md0pksvkx48xfk4z9anwp3shpcg3mpc") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.2 (c (n "relative-path") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "164dqprxk8hpbkvv8j56w8y1q599ncrlyhn2g5vwhkxhvvlw4gda") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.3 (c (n "relative-path") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09x7q1n7kpakylk5vbj08b0psbha8vdfp6sv1k4xsgnv1kwd11nc") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.4 (c (n "relative-path") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0c574nwrj9dda9aw2jlcmdbc80l0dfl6vcb6agnxn8r5nsfi6l8c") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.5 (c (n "relative-path") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pkdi59hqrqnihv90bhhdr2fd160yn46bl84wzb5ywc5jz2dni44") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.2.6 (c (n "relative-path") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02np9yzig0sjq20x0m1s4cqvh3fijhpi32f1plx1pki26cri391y") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.0 (c (n "relative-path") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0xa9qg9bw7iandvn5fyagd46wn6ljlqrm52y6fh0lqapgwq29yrm") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.1 (c (n "relative-path") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1y89q19xn1qscsl2cfj67sn6wjksk1zsy785gqrn1cf4jr53y8vr") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.2 (c (n "relative-path") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lcpm9dhnnvhqp79jkj0dcc7jrq3dz6rp41w94gb2739k6s8sx7h") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.3 (c (n "relative-path") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1anpzp3v5c12ba7q705wcc0444vkaxjl74ys1s80hcw3yycnyvsl") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.4 (c (n "relative-path") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cbxmd837mrngx2zdwc669880shd2xkrm8a4gp2zrhr544kwqmqi") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.5 (c (n "relative-path") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hmp3ya2wi9ljs84y1zlq09d2b7xgapj0razhjbjfz2bvdlw4s35") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.6 (c (n "relative-path") (v "0.3.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bgq0j07zivlnlry4n10a61jaizpdjpdm4mgqc59ysv2lka00pks") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.3.7 (c (n "relative-path") (v "0.3.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qrmcs2h2241gfhfpd1l0rrjsrf1i2g02b5dyys30mv095jgj576") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-0.4.0 (c (n "relative-path") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "077vw2ls046vgi37yslfhkkhd80nnggagicds8qxhwycy73r0xqf") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.0.0 (c (n "relative-path") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m0rlasarkiaw9r0q50ml6zjkmizqp4nb8cy8g722a8gyh0f1pdy") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.1.0 (c (n "relative-path") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ybfrqv3syca8y0bbgsyc1ndcxym73fgl4nmwdkb02m70s5pcqqy") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.1.1 (c (n "relative-path") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zkkfzcvm2xdp76nd2kzgvzlwx5gwvan6y6wd6j1v13jz8dkni0d") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.2.0 (c (n "relative-path") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07i6sjrs5xm559p1n9yr2s3hpj3psy9b62yhakkxsxqnsi04p6k2") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.2.1 (c (n "relative-path") (v "1.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18g2zbfm4a3nsg6hj9lcbpa3ah0q9c46m1hhbc2cv0mk8wn140n6") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.3.0 (c (n "relative-path") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0n537m5h1k7fjr6c3yfxk809fhvd41wqdsfj7x9jnbs72c644ra7") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.3.1 (c (n "relative-path") (v "1.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1lyi6kj7pnrqkyi070cv91blxpz6ymnj1n9jpfw3lwgr5qdd5ihs") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.3.2 (c (n "relative-path") (v "1.3.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "152zdks8chgsq4vmp562bx6whvixm7gzivab1cf8rs1r634ggbv5") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.4.0 (c (n "relative-path") (v "1.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1013ag52p01ghfb84lkx84hpx66m088ccyfaj8y1z0zdgqyxayd4") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.5.0 (c (n "relative-path") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mjsiyylr7fx859zkqykasj3vpbyh1mphdj7d2brrmjgjzl9sqpr") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.6.0 (c (n "relative-path") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mrvdk90lhrawnxqcq2mqrasnmd5jhap4nsjs14sn0mihvqcmm3k") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.6.1 (c (n "relative-path") (v "1.6.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "11kw0f4rxrbpf5la3rnzpcxh7c2c7lrn5cb0ns996g71q4fq76m4") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.7.0 (c (n "relative-path") (v "1.7.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ndnqgwgl5d5cncxpvb5wizgxlja6la58fyzcm9z5fwmvkni5qdl") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.7.1 (c (n "relative-path") (v "1.7.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mpm4x86dkj55472628sbn3acv20n3vmzraanzc6xn8djyqb3c17") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.7.2 (c (n "relative-path") (v "1.7.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0hhxvlbyz101qy1x220bx1nq07vjx3mn5c3pd0w9j56xrs12vwqd") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-relative-path-1.7.3 (c (n "relative-path") (v "1.7.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0j76xl2qzkkvghm0qz6dviq5xpg4v9z8nj8zmhri6dj94hvnpgyk") (f (quote (("default"))))))

(define-public crate-relative-path-1.8.0 (c (n "relative-path") (v "1.8.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "162n4lb1wplj05pr3whhjg3l6xmx4891an8sxmxkqb4kf0955wjb") (f (quote (("default")))) (r "1.56")))

(define-public crate-relative-path-1.9.0 (c (n "relative-path") (v "1.9.0") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)))) (h "1jl32g41ww8pm8lbdmxm6ahagzwkz8b02q1gxzps47g1zj52j1y7") (f (quote (("default")))) (r "1.56")))

(define-public crate-relative-path-1.9.1 (c (n "relative-path") (v "1.9.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)))) (h "09095az60qnsa90wm1gbpbvyqjs0r1ljf5fnffvmcxjqrs78vpzl") (f (quote (("default")))) (r "1.66")))

(define-public crate-relative-path-1.9.2 (c (n "relative-path") (v "1.9.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)))) (h "1g0gc604zwm73gvpcicn8si25j9j5agqz50r0x1bkmgx6f7mi678") (f (quote (("default")))) (r "1.66")))

(define-public crate-relative-path-1.9.3 (c (n "relative-path") (v "1.9.3") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)))) (h "1limlh8fzwi21g0473fqzd6fln9iqkwvzp3816bxi31pkilz6fds") (f (quote (("default")))) (r "1.66")))

