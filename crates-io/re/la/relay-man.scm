(define-module (crates-io re la relay-man) #:use-module (crates-io))

(define-public crate-relay-man-0.0.9 (c (n "relay-man") (v "0.0.9") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "0fb0jyb2gky57blpbzkxkp53bi5qr1ajq2n12h0l5jykrsx820nl") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.1.0 (c (n "relay-man") (v "0.1.0") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "1l41f6x5kizk2xx7ablg0jqs69ir976k913wwnxmnb4bpccny206") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.1.1 (c (n "relay-man") (v "0.1.1") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "1y3nfhy8681bihmj65p86v8cyiga9l7341pr0zk3c6hizix7jy61") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.0 (c (n "relay-man") (v "0.2.0") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "1rsf4y667j03zdwyazn9wlkm68ba7crqqnjfabdi3zx5q6wk3b06") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.1 (c (n "relay-man") (v "0.2.1") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "03x8j60q1b9q7hswjl6i7gmsqipmnz3sx6fhq1m6b7rj299d3w0q") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.2 (c (n "relay-man") (v "0.2.2") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "0p496yf7l4z2y3skvpgyx4a6v3fgzd8sb1f6l1f5aibb392mjpkk") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.3 (c (n "relay-man") (v "0.2.3") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "1s70769r0imzd0m3pcprh7sd3f3wvmygqrxq7pyjb8j0q9hmqy1f") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.4 (c (n "relay-man") (v "0.2.4") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "natpmp") (r "^0.4.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "0a9gpg6m5m4y3zdh7wa7vbc2dnijbvgrzbzbi1mcd5s15h6dqcf4") (f (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-relay-man-0.2.5 (c (n "relay-man") (v "0.2.5") (d (list (d (n "bytes-kman") (r "^0.1.7") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "natpmp") (r "^0.4.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "0s5frdpqhnn6rdnpzid4f4asx4xan5ncbn3jz9pf5rqsdfwmgwfs") (f (quote (("server") ("default" "client" "server") ("client"))))))

