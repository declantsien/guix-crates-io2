(define-module (crates-io re up reups) #:use-module (crates-io))

(define-public crate-reups-0.1.1 (c (n "reups") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0cpdipb7ljj4r29c2khvwfgiz7cd7s67addbb56z5mxfryvfpg0l")))

(define-public crate-reups-0.2.0 (c (n "reups") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "048marxf0wbsw5pfvali4p1ymnabzal7x1dq3zh0wk88ylf3x28v")))

(define-public crate-reups-0.2.1 (c (n "reups") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1bncs1rfz7naq5ypwjg32xm0cdsmpnwi0pmqyad6ysa1dppvv65n")))

(define-public crate-reups-0.3.0 (c (n "reups") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1jsa9p3h3l81w43hl5nx72fa575jq0hs5ryxdb0x9kch41ipin74")))

