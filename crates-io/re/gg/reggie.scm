(define-module (crates-io re gg reggie) #:use-module (crates-io))

(define-public crate-reggie-0.1.0 (c (n "reggie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.0") (d #t) (k 0)) (d (n "winreg") (r "^0.50") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1661xfvk83rxcf0zmav0xzpzgjnncal1m6dbyvdka36aiqyvjlc3")))

(define-public crate-reggie-0.1.1 (c (n "reggie") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.0") (d #t) (k 0)) (d (n "winreg") (r "^0.50") (f (quote ("chrono"))) (d #t) (k 0)))) (h "0dhif5k51zygj44gqcvjxrb6nmv3c1r3jxpy0004rdpjd279yi4a")))

