(define-module (crates-io re me remedy) #:use-module (crates-io))

(define-public crate-remedy-0.1.1 (c (n "remedy") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "remediate") (r "^0.2.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1gv46wrzyqkqdxwswkw0yihcf1zi0mn0kj861hcpdcs6hj45qg58")))

