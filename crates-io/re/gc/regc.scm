(define-module (crates-io re gc regc) #:use-module (crates-io))

(define-public crate-regc-0.1.0 (c (n "regc") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0pk3hz9ksg628xms87nb2bdv58xmd9jw8ri1p6xbrilpb3d8h0rn")))

(define-public crate-regc-0.1.1 (c (n "regc") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "15kfham27mqsf9aq2jhfpb3bcpjs0gldsz7d6sk5k50zd7zfn00s")))

(define-public crate-regc-0.1.2 (c (n "regc") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0a7gjmfxh7wdg6d7vnm6vf1s95kkqkgknsnxb0d0ykqnbpr22svf") (y #t)))

(define-public crate-regc-0.1.3 (c (n "regc") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0jldh8hygfh67np9a8z8n29ah7i5c8y26i592rcx0rxkl71rra2d") (y #t)))

(define-public crate-regc-0.1.4 (c (n "regc") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1bnmmb90h55cjvxdw3ckrrvkys275l9b70c892b7fi67wc2d11ys") (y #t)))

(define-public crate-regc-0.1.5 (c (n "regc") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1hrnkqk5jksv9c1b2iqcyi5rf06z3d185c63nj7vlkn9m0zhzr8x") (y #t)))

(define-public crate-regc-0.2.0 (c (n "regc") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "03xa2k1v1sdph4vij0alikp40wlflm7s7lvkm3in2rzmix4f36r5") (y #t)))

(define-public crate-regc-0.2.1 (c (n "regc") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0cxg2c5lzz09imlgrn2zkjlvqd2zzvxjhsd6jg4a65lwpylah0rx")))

(define-public crate-regc-0.2.2 (c (n "regc") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "08fy3rnwvhchf6gyn5726s19ljaj46bch0va19afhzz72axvdpl6")))

(define-public crate-regc-0.2.3 (c (n "regc") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0wx5hx1cgvys1p3m3297q0ndk4qg6hacliw5r6sgib5vf2lrfk32")))

(define-public crate-regc-0.2.4 (c (n "regc") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "15jq30xddz24b9b5xjlhapqxpi1ssgzgvh38hffwd4kalx2qizqr")))

(define-public crate-regc-0.2.5 (c (n "regc") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1bg5pz30939mriaz6rpvyl4qn1kcgrqgyhfzqk5yzyipkl2fskm9")))

(define-public crate-regc-0.2.6 (c (n "regc") (v "0.2.6") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1i8xx90x2y90fdwnva3mfb87xjvgzbikblq14rw40krh3qd0jw3h")))

(define-public crate-regc-0.2.7 (c (n "regc") (v "0.2.7") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1zamam5cdj7j1jaaqk5kn4szdwkpaximibk0i3bqrn6diqwp1ksa")))

(define-public crate-regc-0.2.8 (c (n "regc") (v "0.2.8") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "17dyimx6xfb2si91jsx72widfpdlil9px2rlzzgx8zlpnvv83xdn")))

