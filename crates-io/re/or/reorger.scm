(define-module (crates-io re or reorger) #:use-module (crates-io))

(define-public crate-reorger-0.1.0 (c (n "reorger") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "007wv6fyrbb6vvs54ri63scflaxa5kj1k8x1gzmdc9b0m6z4p6qx")))

(define-public crate-reorger-0.2.0 (c (n "reorger") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1s3qkf4s2m9az3fpy1q5mgi539hcnxcn5bwydx25cskj44shfqp3")))

(define-public crate-reorger-0.3.0 (c (n "reorger") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0yk5avnn3qb4nssw8q0b2n15yhb1k5lg4jpsmib2ny29z8s5c24j")))

(define-public crate-reorger-0.3.1 (c (n "reorger") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0v74x78cp5jm1ff1f67bvd940ffmc7d81qc0z8sjvi37cvlr8bqc")))

(define-public crate-reorger-0.5.0 (c (n "reorger") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0ph5laksfqwdxzjsd1l1xykaq0vxj93y1xd54slm9nwqgg87r5k8")))

(define-public crate-reorger-0.5.1 (c (n "reorger") (v "0.5.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0zrsf6pn6sdd5knzdhqpr59nqnqk93pbjg0g071lragzmrhfkvvq")))

(define-public crate-reorger-0.5.2 (c (n "reorger") (v "0.5.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1k6qxkl8bpxy0pmdb97gzqyaa2zsb0zbg2k3zk9alp2wh9i29l7m")))

