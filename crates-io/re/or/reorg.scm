(define-module (crates-io re or reorg) #:use-module (crates-io))

(define-public crate-reorg-0.1.0 (c (n "reorg") (v "0.1.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.0") (d #t) (k 0)))) (h "116df5g4q9yln6ayarj2fnay7xf9872ap18ka7avb5wk0wyw0k4v")))

