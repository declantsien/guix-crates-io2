(define-module (crates-io re or reorder) #:use-module (crates-io))

(define-public crate-reorder-1.0.0 (c (n "reorder") (v "1.0.0") (h "087zb7dzb9k57ly51x4iqbmhz2fywki4n1jsl8s2jd520ggxr81w") (y #t)))

(define-public crate-reorder-1.0.1 (c (n "reorder") (v "1.0.1") (d (list (d (n "unchecked-index") (r "^0.2.2") (d #t) (k 0)))) (h "07ydjg6ly6b88s5bimsf0xf68s9kzq5vw6c17fwajj1hqdj58mn5") (y #t)))

(define-public crate-reorder-1.0.2 (c (n "reorder") (v "1.0.2") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "unchecked-index") (r "^0.2.2") (d #t) (k 0)))) (h "07bnpfny6a49pwilgsmvvzwdmd2wwwpcgcxz4n5rq9n5r240wmch") (y #t)))

(define-public crate-reorder-1.0.3 (c (n "reorder") (v "1.0.3") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0sba8gvql62yq0bdqk4078v82gpy3bsh545lp4aacd6v52iwk45y") (y #t)))

(define-public crate-reorder-1.1.0 (c (n "reorder") (v "1.1.0") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1hbhb9y38f1hgjr7jwrsjimhciwdiyw0nf6hcs2h9z9ddvpqakmx") (y #t)))

(define-public crate-reorder-1.2.0 (c (n "reorder") (v "1.2.0") (d (list (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "013747j61n0wibn3ybg7v0ncv2bkb6jpb2l5j873s70wc50xrq9p")))

(define-public crate-reorder-2.0.0 (c (n "reorder") (v "2.0.0") (h "0095i4bvnhnxfk1dfybs7s5wpby6f0xq5y9n65ic1ys83qa76whn")))

(define-public crate-reorder-2.0.1 (c (n "reorder") (v "2.0.1") (h "125ddgzkrzkxzng7qsxkcg24vb0a099pyc88h6wdww6xr3qr8k4c")))

(define-public crate-reorder-2.1.2 (c (n "reorder") (v "2.1.2") (h "00kr1aq3vqk3fjy2xk59q3ynb10j42is2dih5ikcvrrrg653b5n7")))

(define-public crate-reorder-2.1.3 (c (n "reorder") (v "2.1.3") (h "0vdmqmmw8hv0x703idcnk7i7aigc1salvgnqkf1mpp073q1p4rnd")))

