(define-module (crates-io re pc repc) #:use-module (crates-io))

(define-public crate-repc-0.1.0 (c (n "repc") (v "0.1.0") (d (list (d (n "repc-impl") (r "=0.1.0") (d #t) (k 0)))) (h "1121dlb9k4kdkhxvin0w7fnadx7xmw81d5gr3zkwdzp71gyj2a60")))

(define-public crate-repc-0.1.1 (c (n "repc") (v "0.1.1") (d (list (d (n "repc-impl") (r "=0.1.1") (d #t) (k 0)))) (h "1rlqzrf4yzd1v5y7xh3sbcxxm58vn29l13x4ywqb66z8s7hrfm7x")))

