(define-module (crates-io re pc repc-impl) #:use-module (crates-io))

(define-public crate-repc-impl-0.1.0 (c (n "repc-impl") (v "0.1.0") (h "038zk8fwdad8kh6hpyrsjlrrwg5y4x3ajcz2gmq92659k6hh8n2j")))

(define-public crate-repc-impl-0.1.1 (c (n "repc-impl") (v "0.1.1") (h "0s431dgymmpvppch9qfyxhzc41d94sk44v0p07lc4d8kb9yhn4cy")))

