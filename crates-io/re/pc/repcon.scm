(define-module (crates-io re pc repcon) #:use-module (crates-io))

(define-public crate-repcon-0.1.0 (c (n "repcon") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0asy3hymlch3cfddc2sqx6rkd26gjmfr1y6l48rykrlnn64gmz1a")))

(define-public crate-repcon-0.1.1 (c (n "repcon") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0iib0n2863zijkahh347ikdcgklb3mm5jyn68vswk0mjndjzlsgv")))

(define-public crate-repcon-0.1.2 (c (n "repcon") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qn9zfnazvidblp6bb7rlprdhjjwfl5pn25byghqkwi6mxkvbzwq")))

(define-public crate-repcon-0.1.3 (c (n "repcon") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fm72fac0zc4hmhlxqfmphy4pvacnc2xw5h84vcz4194qz3cfvnf")))

