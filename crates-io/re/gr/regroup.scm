(define-module (crates-io re gr regroup) #:use-module (crates-io))

(define-public crate-regroup-0.1.0 (c (n "regroup") (v "0.1.0") (h "0mpifbsgjal370z7qkz627gmmv6lx9swk72zlfyz9jdasl8d05js") (f (quote (("std" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("alloc")))) (y #t)))

(define-public crate-regroup-0.1.1 (c (n "regroup") (v "0.1.1") (h "01774qapcfg7l03qhd9dn11mcg38dckhcvlylbazi816n59c8kdj") (f (quote (("std" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("alloc")))) (y #t)))

(define-public crate-regroup-0.2.0 (c (n "regroup") (v "0.2.0") (h "1ihgccb2zyna1pv8vwmwjkyjbnh8r5fdsmrssr0w9v0r2wdhdmlz") (f (quote (("std" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("alloc"))))))

(define-public crate-regroup-0.3.0 (c (n "regroup") (v "0.3.0") (h "1rx06dir86yvr03djfwv15q8bkajpxd09glq955648ddym93c4ca") (f (quote (("std" "alloc" "rc" "arc") ("rc" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("arc" "alloc") ("alloc"))))))

(define-public crate-regroup-0.4.0 (c (n "regroup") (v "0.4.0") (h "04k7f5zpzmwns5yfq3ph9kmrb7v83hafsfp310sv56yg5yd7gsk1") (f (quote (("std" "alloc" "rc" "arc") ("rc" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("arc" "alloc") ("alloc"))))))

(define-public crate-regroup-0.4.1 (c (n "regroup") (v "0.4.1") (h "06zc9mvf71w7840nvhv5rbvn3a429pr7pmg6lpbkjqr5m3fzlrwy") (f (quote (("std" "alloc" "rc" "arc") ("rc" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("arc" "alloc") ("alloc"))))))

(define-public crate-regroup-0.4.2 (c (n "regroup") (v "0.4.2") (h "1mhl3k9z1c8i568ji512m0yh6s4is14w3w3xxdl3wwqss92d5c8g") (f (quote (("std" "alloc" "rc" "arc") ("rc" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("arc" "alloc") ("alloc"))))))

(define-public crate-regroup-0.4.3 (c (n "regroup") (v "0.4.3") (h "074lz9qwp4i3qix0axh80lzshqnzibp2qa3z1zyvx23cpvy8l8ns") (f (quote (("std" "alloc" "rc" "arc") ("rc" "alloc") ("primitives") ("depth_7" "depth_6") ("depth_6" "depth_5") ("depth_5" "depth_4") ("depth_4" "depth_3") ("depth_3" "depth_2") ("depth_2") ("default" "depth_3" "std") ("arc" "alloc") ("alloc"))))))

