(define-module (crates-io re gr regrate) #:use-module (crates-io))

(define-public crate-regrate-0.3.0 (c (n "regrate") (v "0.3.0") (d (list (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-rc.7") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kq8bnmml3gxpvwfgqan1xazf3p8q9bbh2fmi42z3sffrblc5pk9")))

