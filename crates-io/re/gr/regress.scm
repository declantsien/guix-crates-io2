(define-module (crates-io re gr regress) #:use-module (crates-io))

(define-public crate-regress-0.1.0 (c (n "regress") (v "0.1.0") (h "0zrvn0204b8p86xh4jqs9mrsaqiqm22c00ywxdp31p1g03c5piq8") (f (quote (("prohibit-unsafe") ("dump-phases") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.1.1 (c (n "regress") (v "0.1.1") (h "1bgz38ysmv43vm5qsynrmmyp5ijwwb8a4794dddpy1xwwz0kd6ln") (f (quote (("prohibit-unsafe") ("dump-phases") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.1.2 (c (n "regress") (v "0.1.2") (d (list (d (n "memchr") (r "^2.3.3") (d #t) (k 0)))) (h "0r84579jk64k1h6d1b1j9lg6iq1qhcgdn4bhmriaf4ikg4xszg94") (f (quote (("prohibit-unsafe") ("dump-phases") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.1.3 (c (n "regress") (v "0.1.3") (d (list (d (n "memchr") (r "^2.3.3") (d #t) (k 0)))) (h "16nk6ir62cb84d9qbiis105gh0pgl6fymwrrjj4l40cgav297ah8") (f (quote (("prohibit-unsafe") ("dump-phases") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.1.4 (c (n "regress") (v "0.1.4") (d (list (d (n "memchr") (r "^2.3.3") (d #t) (k 0)))) (h "1ig4zrr1i456a8cl0c12cj7xzc1y6xs2imcjgwnxfd632vdzjsfn") (f (quote (("prohibit-unsafe") ("dump-phases") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.2.0 (c (n "regress") (v "0.2.0") (d (list (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0kjvgbx2b851sl2ajpcpvxsyhim8b9mag546dv00q4h214l8rpla") (f (quote (("prohibit-unsafe") ("default" "backend-pikevm" "cli") ("cli" "structopt") ("backend-pikevm"))))))

(define-public crate-regress-0.3.0 (c (n "regress") (v "0.3.0") (d (list (d (n "memchr") (r "^2.3.3") (d #t) (k 0)))) (h "1p7ji2mcsznvxjw8i1h9yygqb20wxkd914n1zqcr9mld8r4mx8gj") (f (quote (("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.4.0 (c (n "regress") (v "0.4.0") (d (list (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "0vb3nngb2iwxg6p9k707ddhlwc3wrcryzy8gx24bw5fzxqmwlaaa") (f (quote (("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.4.1 (c (n "regress") (v "0.4.1") (d (list (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "0bnrfm7q06hq4zqcrjfv2jq6f2vgc11szyi74qzww9l0zqhzz4ha") (f (quote (("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm") ("backend-pikevm"))))))

(define-public crate-regress-0.5.0 (c (n "regress") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "1sg8wb4g4vknp515d6pdkr6fxc78yprlnh9zi78rdh4fpn8db5fr") (f (quote (("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.6.0 (c (n "regress") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "1ysh7apdh2spk2rr9xixsbgwhsznyk6bi6fvvl20nkdh1kxfrac2") (f (quote (("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.7.0 (c (n "regress") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "0wdxrn2212hfqc4axff4zg0gjw44m317nzlp4kz2srdd3zmpfz5n") (f (quote (("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.7.1 (c (n "regress") (v "0.7.1") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "1hnjh32dzy7fn5j7j7kky46806wzc94zassr26034lc0mnf9dnaf") (f (quote (("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.8.0 (c (n "regress") (v "0.8.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "186ywpjcmrgds1c2z69psqf0j7v7xkv2mdsp4qdnr48k8nx3jpsg") (f (quote (("utf16") ("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.9.0 (c (n "regress") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "07pmszkvgzc2m68bldj3jp1gyp4wbzrhqj0sp88kciyqghgrlvyh") (f (quote (("utf16") ("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.9.1 (c (n "regress") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "0y94aac9vss2zw4vzxb2fxibx6ijck9wry1fjpzqmigcpwg2mbhf") (f (quote (("utf16") ("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

(define-public crate-regress-0.10.0 (c (n "regress") (v "0.10.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (k 0)))) (h "0hj8hn9j6jz8ld5ivk5s3xgbyvv4s8pks8clfaaaxajxmwj0mzhn") (f (quote (("utf16") ("std" "memchr/std") ("prohibit-unsafe") ("index-positions") ("default" "backend-pikevm" "std") ("backend-pikevm"))))))

