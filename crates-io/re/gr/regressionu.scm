(define-module (crates-io re gr regressionu) #:use-module (crates-io))

(define-public crate-regressionu-0.1.0 (c (n "regressionu") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0fx4nv2kwxfz0ik3iziyf4v3f68zrx9lyxqqv8hn8dya2izbwpnh") (y #t) (r "1.63.0")))

(define-public crate-regressionu-0.1.1 (c (n "regressionu") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0989pfmds7wswjc3j8ab1rdaws04dm5zhg5n5pzgs409rd30d70z") (r "1.63.0")))

(define-public crate-regressionu-0.2.0 (c (n "regressionu") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "180y7mn2b4bs9iy33cf369bjl52iw37n46mzhl80c9vwvr83ksdn") (y #t) (r "1.63.0")))

(define-public crate-regressionu-0.2.1 (c (n "regressionu") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1fgymk416pfqx17wyzj1gn4pdkypqd7vszxgrdk5hjgzqrcbhzix") (y #t) (r "1.63.0")))

(define-public crate-regressionu-0.2.2 (c (n "regressionu") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0b8lpprx94w062h38q2xdpqpdh8g9b20fl4w06x4jg04d7fdx4dw") (y #t) (r "1.63.0")))

(define-public crate-regressionu-0.2.3 (c (n "regressionu") (v "0.2.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1cjap623jg00y84n3z0fgai3djhxvnflb7xv6kawil73iq34zqdv") (y #t) (r "1.63.0")))

(define-public crate-regressionu-0.2.4 (c (n "regressionu") (v "0.2.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "00vdmqj8wc9gb9h24qjhyxjkyz6kwzcc3xw7dx9plzwdrgpilznz") (r "1.63.0")))

(define-public crate-regressionu-0.3.0 (c (n "regressionu") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "08qgb1lb23kzayjpcbh7fs0cbqh6zgs8fv3swcwlkcprkjh56r61") (r "1.63.0")))

(define-public crate-regressionu-0.3.1 (c (n "regressionu") (v "0.3.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "02rdaykwmmyzqp6xaq4dfqyx3m71vgn56x195s3vkrg07yy9xljx") (r "1.63.0")))

