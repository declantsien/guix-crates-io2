(define-module (crates-io re ef reefdb) #:use-module (crates-io))

(define-public crate-reefdb-0.1.0 (c (n "reefdb") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1lz28aykli6zinr2yiczgh1fc8bsfgbs7yma4hnqxhk0cnpml8d6")))

