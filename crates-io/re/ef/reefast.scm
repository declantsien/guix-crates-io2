(define-module (crates-io re ef reefast) #:use-module (crates-io))

(define-public crate-reefast-0.0.1 (c (n "reefast") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j1paqh15h793fc5krv4qxw4qyxp6nky325mb91l1h7nmxnvmg3z")))

