(define-module (crates-io re no reno) #:use-module (crates-io))

(define-public crate-reno-0.1.0 (c (n "reno") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "caps") (r "^0.5.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "oci-spec") (r "^0.5.8") (d #t) (k 0)) (d (n "prctl") (r "^1.0.0") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (d #t) (k 0)) (d (n "rlimit") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1wmh4rppip4q5aahsps9lxwfx4y2wf87lj4vc0y0pr2bxp32kcnr")))

