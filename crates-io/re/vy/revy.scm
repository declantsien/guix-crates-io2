(define-module (crates-io re vy revy) #:use-module (crates-io))

(define-public crate-revy-0.14.0 (c (n "revy") (v "0.14.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "rerun") (r "^0.14.1") (f (quote ("sdk" "image" "glam"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)))) (h "08n50v33vb3vs55pwkjnz6p5mh2nq6c1z6dir71s6bs8m5w6qwdb") (r "1.76")))

(define-public crate-revy-0.15.0 (c (n "revy") (v "0.15.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "rerun") (r "^0.15.0") (f (quote ("sdk" "image" "glam"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)))) (h "016717gdir3j51qva9xjc1zf5y2a9q4672xjwkffq63016z5pyw3") (r "1.76")))

