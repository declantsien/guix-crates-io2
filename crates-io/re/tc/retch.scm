(define-module (crates-io re tc retch) #:use-module (crates-io))

(define-public crate-retch-1.0.0 (c (n "retch") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fspp") (r "^1.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "1cxyjwdpysfplq8cx2z0k5bj6h2rbqpndfr47qr913z4phmcrwd2")))

