(define-module (crates-io re tc retc-raft) #:use-module (crates-io))

(define-public crate-retc-raft-0.0.0 (c (n "retc-raft") (v "0.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "sync" "time" "macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "16fn4wvqs8rznls1q4jcrwzzf8qxz9xv8yqdbp2ij6g1la4w2c0x")))

