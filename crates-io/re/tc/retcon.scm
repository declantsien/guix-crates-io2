(define-module (crates-io re tc retcon) #:use-module (crates-io))

(define-public crate-retcon-0.0.0-reserved (c (n "retcon") (v "0.0.0-reserved") (h "0pvi63wsicv015i9ap64yz2slay1419d1kgx7lg8v3grd50hfry7") (y #t)))

(define-public crate-retcon-0.0.0-- (c (n "retcon") (v "0.0.0--") (h "16g9jxr0vb67l68faqa64wf39h9344j4f70q16g6b4vrizwr9rpq") (y #t)))

