(define-module (crates-io re tc retc) #:use-module (crates-io))

(define-public crate-retc-0.0.0 (c (n "retc") (v "0.0.0") (d (list (d (n "retc-raft") (r "^0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "macros" "time"))) (d #t) (k 0)))) (h "01fw7qaxkrxp6z9908zx8983l34hq3y04jb5jfsc8awpl5hhyyly")))

