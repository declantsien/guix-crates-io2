(define-module (crates-io re zi rezip) #:use-module (crates-io))

(define-public crate-rezip-0.0.0 (c (n "rezip") (v "0.0.0") (h "1wh52k8bjl6ank42rbwpy9w1wljld1yzqras7iiwyam47aivhml4")))

(define-public crate-rezip-0.1.0 (c (n "rezip") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "18m4v66dwj7rb6f0nrzcd505wrlkavq0j72yw17jgrsl5mazh5r3")))

(define-public crate-rezip-0.1.1 (c (n "rezip") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1wir9pqgd9z7yjjz9bw469zfssbkgd5vakjbcl6fs6q8y3wcryrz")))

(define-public crate-rezip-0.1.2 (c (n "rezip") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0bbq056iy8k2plh85sba3azhfk75firlgxaml5xwjbcmlixvcc89")))

(define-public crate-rezip-0.1.3 (c (n "rezip") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "19jrndm6yspycncvbmwxl96nf0ngv865zxsygcxslydwx9n3nrjh")))

