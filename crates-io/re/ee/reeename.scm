(define-module (crates-io re ee reeename) #:use-module (crates-io))

(define-public crate-reeename-0.1.0 (c (n "reeename") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0pdgvflb1gk0kp5sac86fiqqw3xdnnafp754kfyzfngrhqal4n6a")))

