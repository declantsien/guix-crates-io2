(define-module (crates-io re di redis-stream) #:use-module (crates-io))

(define-public crate-redis-stream-0.1.1 (c (n "redis-stream") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 2)))) (h "0sr15n48v54qsgmj4a632s5ia8zlxjxv0hjdhmg75687fqgymnzj")))

(define-public crate-redis-stream-0.1.2 (c (n "redis-stream") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 2)))) (h "10lqbwq1iv1hfmm7bzx8rpm4m2maz4l5x04i6qmg2jv9a1a70kbv")))

