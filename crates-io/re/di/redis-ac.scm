(define-module (crates-io re di redis-ac) #:use-module (crates-io))

(define-public crate-redis-ac-0.1.0 (c (n "redis-ac") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0scbaixyydddsk7650gi82v4561i584x94p032hmp2102nvrzvjh") (f (quote (("geospatial" "redis/geospatial") ("default"))))))

(define-public crate-redis-ac-0.2.0 (c (n "redis-ac") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "17rbznnwhpkdqxib7mbg94jq2cjp9nidjmdyvbnqdhxd674bbbci") (f (quote (("geospatial" "redis/geospatial") ("default"))))))

(define-public crate-redis-ac-0.2.1 (c (n "redis-ac") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1jdl4l3a7rdbcz9vc8j5x89y1687vi43a0lx5szy6ij58y8df69f") (f (quote (("readme") ("geospatial" "redis/geospatial") ("default"))))))

(define-public crate-redis-ac-0.1.1 (c (n "redis-ac") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1r99b7h5i98d0mf4wky2yq6y96p77vq7m3jfhwpg325fzv788hdl") (f (quote (("geospatial" "redis/geospatial") ("default"))))))

