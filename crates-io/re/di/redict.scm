(define-module (crates-io re di redict) #:use-module (crates-io))

(define-public crate-redict-0.1.0 (c (n "redict") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.0") (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (d #t) (k 0)))) (h "16kmc0xc3zy5qjhrng9lpfl91vharw6ydhcl18cb6fr82c7mpzbz")))

(define-public crate-redict-0.2.0 (c (n "redict") (v "0.2.0") (d (list (d (n "clap") (r ">=2.33.0") (o #t) (d #t) (k 0)) (d (n "termion") (r ">=1.5.0") (o #t) (d #t) (k 0)) (d (n "tui") (r ">=0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r ">=1.7.0") (o #t) (d #t) (k 0)) (d (n "url") (r ">=2.2.0") (d #t) (k 0)))) (h "0k611gp1fx73pq7lkx7rhqzngzls48hq3n3rwg4qjyf6jj0ynr2w") (f (quote (("default" "cli") ("cli" "tui" "termion" "clap" "unicode-segmentation"))))))

(define-public crate-redict-0.2.1 (c (n "redict") (v "0.2.1") (d (list (d (n "clap") (r ">=2.33.0") (o #t) (d #t) (k 0)) (d (n "termion") (r ">=1.5.0") (o #t) (d #t) (k 0)) (d (n "tui") (r ">=0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r ">=1.7.0") (o #t) (d #t) (k 0)) (d (n "url") (r ">=2.2.0") (d #t) (k 0)))) (h "1yvsn0c951qd5x6jm60m8l6cb2pa4g6d2da3l4ki80lp1j7gjcby") (f (quote (("default" "cli") ("cli" "tui" "termion" "clap" "unicode-segmentation"))))))

(define-public crate-redict-0.2.2 (c (n "redict") (v "0.2.2") (d (list (d (n "clap") (r ">=2.33.0") (o #t) (d #t) (k 0)) (d (n "termion") (r ">=1.5.0") (o #t) (d #t) (k 0)) (d (n "tui") (r ">=0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r ">=1.7.0") (o #t) (d #t) (k 0)) (d (n "url") (r ">=2.2.0") (d #t) (k 0)))) (h "0six0r0fbrgbbwk64cigv6j7hy5n3avpnzyl8y3q3phwfcbb74nx") (f (quote (("default" "cli") ("cli" "tui" "termion" "clap" "unicode-segmentation"))))))

(define-public crate-redict-0.2.3 (c (n "redict") (v "0.2.3") (d (list (d (n "clap") (r ">=2.33.0") (o #t) (d #t) (k 0)) (d (n "termion") (r ">=1.5.0") (o #t) (d #t) (k 0)) (d (n "tui") (r ">=0.14.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r ">=1.7.0") (o #t) (d #t) (k 0)) (d (n "url") (r ">=2.2.0") (d #t) (k 0)))) (h "1784bf16ahncgm0mw6704kkdfz0ra6w2vk2f6qvh75rpnbj1b6ya") (f (quote (("default" "cli") ("cli" "tui" "termion" "clap" "unicode-segmentation"))))))

