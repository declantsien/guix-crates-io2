(define-module (crates-io re di redis-zset-ts) #:use-module (crates-io))

(define-public crate-redis-zset-ts-0.1.0 (c (n "redis-zset-ts") (v "0.1.0") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f7wg17grhps0qidzxyiwyg39grwsx8lmx47khry5rnklhahja5h") (r "1.63")))

(define-public crate-redis-zset-ts-0.2.0 (c (n "redis-zset-ts") (v "0.2.0") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11qn1ir3qvaz3krk54xbmsi5ac7x6b2pxfdkh5bb85nx0hs2ypkm") (r "1.63")))

