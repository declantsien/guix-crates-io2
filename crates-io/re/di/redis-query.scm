(define-module (crates-io re di redis-query) #:use-module (crates-io))

(define-public crate-redis-query-0.1.0 (c (n "redis-query") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "15nm3ai26yaqxzsjjir1lazh57p52ywk3icnb0r4dzqb5yp9mi5n")))

(define-public crate-redis-query-0.1.1 (c (n "redis-query") (v "0.1.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1p5b9065j2yci01s2srfksysbgszsmzcpl6vi3sii34cr3l02m0l")))

(define-public crate-redis-query-0.1.2 (c (n "redis-query") (v "0.1.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1qpzgfphk3bxzzqqkxbw05kfhyxkwjir9c7k18kcsrg36l0sb4fx")))

(define-public crate-redis-query-0.1.3 (c (n "redis-query") (v "0.1.3") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "106sjxn6q1vp8m96607xl6ign0my3ng0qv6lwqdr7n62phpikh87")))

(define-public crate-redis-query-0.1.4 (c (n "redis-query") (v "0.1.4") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0ryg525qyp7342w220dlfxs8l4vg154sqzqsxbbzjan5qic6413p")))

(define-public crate-redis-query-0.1.5 (c (n "redis-query") (v "0.1.5") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "17ih30awa5aswf8qxzac233nzbs03pn653j2p1hs60lwg7pbndhg")))

(define-public crate-redis-query-0.1.6 (c (n "redis-query") (v "0.1.6") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0wz6k62ziqq57qmglfllp7m8vlx5iy9y16g0xqsflva1kikv6575")))

(define-public crate-redis-query-0.1.7 (c (n "redis-query") (v "0.1.7") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "00jh6cjkdansg79lsd7k37dz19ms7r680296q8445g2863i8ia8b")))

(define-public crate-redis-query-0.1.8 (c (n "redis-query") (v "0.1.8") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1axki785p8vx9dxhdcyjqvlww064ai7v0s250bdkcvv3vpkq13i7")))

(define-public crate-redis-query-0.1.9 (c (n "redis-query") (v "0.1.9") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "0h6r5j814zs1h3pz9pppdbqvfjjxcc6zika2ap3dgk5hgyn010k6")))

(define-public crate-redis-query-0.1.10 (c (n "redis-query") (v "0.1.10") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "redis") (r "^0.21.6") (d #t) (k 0)))) (h "0pn7ja1bb376ysn1j3zs6xijmxgznq7ivdccgnzfcbwrwqrl3wzq")))

