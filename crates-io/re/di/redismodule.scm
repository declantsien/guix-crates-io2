(define-module (crates-io re di redismodule) #:use-module (crates-io))

(define-public crate-redismodule-0.1.0 (c (n "redismodule") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "009zgzrglargrra1wijiaqhkbfzpd07df86iz7d2jjd798nd020g")))

(define-public crate-redismodule-0.1.1 (c (n "redismodule") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "0k0bxs3l4srnh321066qzfxknhkwas9569s63n78ppcyxkykv7bd")))

(define-public crate-redismodule-0.1.2 (c (n "redismodule") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "00if66fyqzcrsqrb8nn4kqv1nskry37wal33w353wfr3yzmvs4zf")))

