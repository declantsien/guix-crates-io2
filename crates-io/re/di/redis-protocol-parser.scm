(define-module (crates-io re di redis-protocol-parser) #:use-module (crates-io))

(define-public crate-redis-protocol-parser-0.1.0 (c (n "redis-protocol-parser") (v "0.1.0") (h "13r01pl727b5dk0fnydcalqqcv0fk9fd8n43ix5ap4xhh1vrwvvm")))

(define-public crate-redis-protocol-parser-0.1.1 (c (n "redis-protocol-parser") (v "0.1.1") (h "0ccnwxl2zgvidkqf4jf94dv19g36xpq2hx0fqjzfp71fya48q6yz")))

(define-public crate-redis-protocol-parser-0.1.2 (c (n "redis-protocol-parser") (v "0.1.2") (d (list (d (n "bytes") (r "^0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s4w4y30v89wqjf3cgrny61sipddwpx5q6p50qrbz8ca1zshww31")))

