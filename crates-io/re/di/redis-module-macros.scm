(define-module (crates-io re di redis-module-macros) #:use-module (crates-io))

(define-public crate-redis-module-macros-2.0.3 (c (n "redis-module-macros") (v "2.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0if3g7vvvnlsn7wff9nlrzx6c7v9cfxqk601y8n0596rmd9xi1hb")))

(define-public crate-redis-module-macros-2.0.4 (c (n "redis-module-macros") (v "2.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybmkx603bf46n0ycmyhrdzf1wjg5h0ym2yas54dgy3hr7nbgbgk")))

(define-public crate-redis-module-macros-2.0.5 (c (n "redis-module-macros") (v "2.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w8406gczwp72ynxynia1vmv9l1g2fafz7rakl17aq1003fcxinf")))

(define-public crate-redis-module-macros-2.0.7 (c (n "redis-module-macros") (v "2.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15qip13g0papf1jjjxjmjnkqzz1l5vmwwi19kvws1azarcw29jd9")))

