(define-module (crates-io re di redis-universal-client) #:use-module (crates-io))

(define-public crate-redis-universal-client-0.0.1 (c (n "redis-universal-client") (v "0.0.1") (d (list (d (n "redis") (r ">=0.17") (f (quote ("cluster"))) (d #t) (k 0)))) (h "0967pivhcfdj94n14f2i4pszz1865x6nv8i9ah8n96xinfcp3yjf")))

(define-public crate-redis-universal-client-0.0.2 (c (n "redis-universal-client") (v "0.0.2") (d (list (d (n "redis") (r ">=0.17") (f (quote ("cluster"))) (d #t) (k 0)))) (h "0cbdq4ikvdkkw9hapbxz71dsqji38iin6wjnxv07h17j06431q8z")))

(define-public crate-redis-universal-client-0.0.3 (c (n "redis-universal-client") (v "0.0.3") (d (list (d (n "redis") (r ">=0.17, <=0.20") (f (quote ("cluster"))) (d #t) (k 0)))) (h "12zzalkzal90navpxj0xmrc4j02mnadnjkziyc6zmqh23scw7nln")))

