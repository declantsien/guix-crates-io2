(define-module (crates-io re di redis-macros-derive-bincode) #:use-module (crates-io))

(define-public crate-redis-macros-derive-bincode-0.1.0 (c (n "redis-macros-derive-bincode") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "redis") (r "^0.22.2") (f (quote ("tokio-comp" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1v43jzy51irvp2lg5wfbg1syr1zrc7x7d7vn3rgix5gzdfqaidcp")))

