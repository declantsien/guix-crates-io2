(define-module (crates-io re di redisesh) #:use-module (crates-io))

(define-public crate-redisesh-0.1.0 (c (n "redisesh") (v "0.1.0") (d (list (d (n "redis") (r "^0.21.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "06jh6f6wqww505mji0b9panqy9zgl2m07ini032clij6f8mys736")))

