(define-module (crates-io re di redis-mq) #:use-module (crates-io))

(define-public crate-redis-mq-0.1.0 (c (n "redis-mq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("connection-manager" "tokio-comp"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("full"))) (d #t) (k 0)))) (h "1hww2kyxzy1yvsqfhyxqjf3vps7c7mqik5jmwmz8c41j3v5fq3sz")))

