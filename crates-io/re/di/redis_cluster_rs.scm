(define-module (crates-io re di redis_cluster_rs) #:use-module (crates-io))

(define-public crate-redis_cluster_rs-0.1.0 (c (n "redis_cluster_rs") (v "0.1.0") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1dmpgxfxx7zd8njrnhjasivb4dmmdxdkijjma14q8hjg8g38qry7")))

(define-public crate-redis_cluster_rs-0.1.1 (c (n "redis_cluster_rs") (v "0.1.1") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0na5mig1fbaq031waxdca71qbr7h5vfdmxz77yhavf20bxj0dxhs")))

(define-public crate-redis_cluster_rs-0.1.2 (c (n "redis_cluster_rs") (v "0.1.2") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1ycfdcrl6zrz6cbljsb7qzl716lqldfvkg0qyf57z11mrfiv2yyi")))

(define-public crate-redis_cluster_rs-0.1.3 (c (n "redis_cluster_rs") (v "0.1.3") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1w89w9b0fby1bg0h0gv5sfidy06kw2x7miibifbqkm2v8kp9lzv3")))

(define-public crate-redis_cluster_rs-0.1.4 (c (n "redis_cluster_rs") (v "0.1.4") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)))) (h "1y70dwp3iin2hzjla64yicszjvd69bknjps9iw7jdvg9szhnbmgq")))

(define-public crate-redis_cluster_rs-0.1.5 (c (n "redis_cluster_rs") (v "0.1.5") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)))) (h "1rzyggi5nb3qy7md7nq7mbv9d6ibbv5bgrnv3mbz2h628w2iv9c8")))

(define-public crate-redis_cluster_rs-0.1.6 (c (n "redis_cluster_rs") (v "0.1.6") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)))) (h "0sv35sdikrqakx275qlxdqy8d2ic5h06q6fwngvfl6h51f4j6csv")))

(define-public crate-redis_cluster_rs-0.1.7 (c (n "redis_cluster_rs") (v "0.1.7") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)))) (h "1l9bv98d425z6664jkhbczi4qyg7jiyf0vis9rdmxyqbsqn9c519")))

(define-public crate-redis_cluster_rs-0.1.8 (c (n "redis_cluster_rs") (v "0.1.8") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)))) (h "1bd8hfsybfb2pr845zlnw2qqwv8gdjayq5wj1w02pvwh5gyhj5vb")))

(define-public crate-redis_cluster_rs-0.1.9 (c (n "redis_cluster_rs") (v "0.1.9") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)))) (h "1hdp1qw50cn54qp2x4mf299h42g4nw36vfc7fijhiz1m6g6ddx2c")))

(define-public crate-redis_cluster_rs-0.1.10 (c (n "redis_cluster_rs") (v "0.1.10") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)))) (h "03cwvikswpbc37z5nrzga489hcc1akdapfsn29wbb3ll56c2rz11")))

