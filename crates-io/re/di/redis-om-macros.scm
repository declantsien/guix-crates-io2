(define-module (crates-io re di redis-om-macros) #:use-module (crates-io))

(define-public crate-redis-om-macros-0.1.0 (c (n "redis-om-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "1qy676yzsqfmv8j0sjk9vf2a9vyw38bww9k6w0g8x7iqfdgcjdzh") (f (quote (("json") ("default") ("aio"))))))

