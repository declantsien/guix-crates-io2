(define-module (crates-io re di redismodule_cmd_procmacros) #:use-module (crates-io))

(define-public crate-redismodule_cmd_procmacros-0.1.0 (c (n "redismodule_cmd_procmacros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a91j3vqhsb6ivqmi6bwjw5mf5442xzgkw6kwi86496cvc2lczk8")))

(define-public crate-redismodule_cmd_procmacros-0.1.1 (c (n "redismodule_cmd_procmacros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zgb1wvszdibxwj6bn7xkni847bp9c3c5yldh11qrvnadcar6946")))

(define-public crate-redismodule_cmd_procmacros-0.1.2 (c (n "redismodule_cmd_procmacros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03vw9arhx013q6i7v5247ijwhgy9dfvvr23iy2sj7bwpzs6kkpnr")))

