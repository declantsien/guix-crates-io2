(define-module (crates-io re di redis-args) #:use-module (crates-io))

(define-public crate-redis-args-0.8.0-rc.1 (c (n "redis-args") (v "0.8.0-rc.1") (d (list (d (n "redis") (r "^0.23") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0r7w3jnsbz08yfrmpqc6cqvwli60jqra25wvl1lirnv3kvi3mll1")))

(define-public crate-redis-args-0.9.0-alpha (c (n "redis-args") (v "0.9.0-alpha") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.9.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sgmbav7pw7f047s2ibxs6vlnq4dyvg6p7jmakjl1mr8jyv1kz9y")))

(define-public crate-redis-args-0.9.0 (c (n "redis-args") (v "0.9.0") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bs9g0085j33f8jr5amq9y9kix48gxkdh2h1mki58zg5w6r8gpjf")))

(define-public crate-redis-args-0.10.0-rc.1 (c (n "redis-args") (v "0.10.0-rc.1") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.10.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kk19bfv816l1h114ph0px507f8w178bgaqd1x0k2cafnbr2pbyy")))

(define-public crate-redis-args-0.11.0-alpha (c (n "redis-args") (v "0.11.0-alpha") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.11.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0630lph5zrgmrvr728n9l10bkxn5ncfp4a6z76yy8hmw3w7hxff6")))

(define-public crate-redis-args-0.12.0-alpha (c (n "redis-args") (v "0.12.0-alpha") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.12.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ifscpa7ny74yncbd0a70sj0ar25w9vj0kwl86sb00yzl50g4v6z")))

(define-public crate-redis-args-0.12.0-alpha.1 (c (n "redis-args") (v "0.12.0-alpha.1") (d (list (d (n "redis") (r "^0.25") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g510apz8zy7k7jwwy4zg6772jl00hapz0y5shfdpjc9fqr3cr56")))

(define-public crate-redis-args-0.11.0-rc.2 (c (n "redis-args") (v "0.11.0-rc.2") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1frp2h2xmzpyh71ngdpfxvp9fhylfxrl7r6nj9qglhgcd4rdgzzl")))

(define-public crate-redis-args-0.10.0 (c (n "redis-args") (v "0.10.0") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1n19lsrmj1cd7j6mmkj71yd8ibkqpn230lf09gx05p1msrfp61ia")))

(define-public crate-redis-args-0.13.0-alpha (c (n "redis-args") (v "0.13.0-alpha") (d (list (d (n "redis") (r "^0.25") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0sjz5nv4rs80v0yr230sxng1h514rk1rb9wg0v7gmzk3hq1migwf")))

(define-public crate-redis-args-0.11.0 (c (n "redis-args") (v "0.11.0") (d (list (d (n "redis") (r "^0.24") (d #t) (k 0)) (d (n "redis-args-impl") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ssb4nws349p4q3sr338bxm3idqvnh744y1bm4hj0c67z5fykl2y")))

