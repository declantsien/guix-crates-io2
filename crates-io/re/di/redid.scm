(define-module (crates-io re di redid) #:use-module (crates-io))

(define-public crate-redid-0.2.0 (c (n "redid") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "enum-map") (r "^2.7.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.1") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (d #t) (k 2)))) (h "0l0mgjxv45nq4mfhv600b2mdhas3z95cwc72xjjadn5qksh6iapv")))

