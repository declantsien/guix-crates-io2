(define-module (crates-io re di redisgraph) #:use-module (crates-io))

(define-public crate-redisgraph-0.1.0 (c (n "redisgraph") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "00f7w4klxpl01495v6n1vjcl0192bhd87wyk3v97flvvyf4abdpp")))

(define-public crate-redisgraph-0.2.0 (c (n "redisgraph") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "095r95yb3d90z9sr59a4y8m5s2b7gnbgjvg3yk8x6jyilljlm15s")))

(define-public crate-redisgraph-0.3.0 (c (n "redisgraph") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "10m2bb9c0q20lp84pgl778cvcs9kw39382rwdjfjazz4c27gg7r3")))

(define-public crate-redisgraph-0.4.0 (c (n "redisgraph") (v "0.4.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "1frzr0662787jp76zs154qq33a7mbm63mymb28cg1ji8cql09l0n")))

(define-public crate-redisgraph-0.5.0 (c (n "redisgraph") (v "0.5.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "redis") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "07fzgj614z2hbybm3sk2jbk6g146gw9fgwsdhbnx8n84awh3x2pc")))

