(define-module (crates-io re di redis_ts) #:use-module (crates-io))

(define-public crate-redis_ts-0.1.0 (c (n "redis_ts") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1pc6w2gsfn2bphay2kn93764gwhdywvk8g2dvcnngqli89m5vh65")))

(define-public crate-redis_ts-0.1.1 (c (n "redis_ts") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0vwq63784sk1195ga87dziggg17ifpxf5xdm8chfzmd6ngbafh9y")))

(define-public crate-redis_ts-0.2.0 (c (n "redis_ts") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1cg3nnkgiv6y650ca4m5rrmz358xbwvwmiv6gba54jvcayl2rcna")))

(define-public crate-redis_ts-0.2.1 (c (n "redis_ts") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "136fsvzafsrbk9p7wjwa91wpwsa2clrmpj8nar49yd95fyjmj471")))

(define-public crate-redis_ts-0.2.2 (c (n "redis_ts") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0c3371v9889fzdcwqjkfdzc28spmfh8r1xz4nnw0lxw20i6y1980")))

(define-public crate-redis_ts-0.3.0 (c (n "redis_ts") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0a4h0i7a3ij1acv3avyvm7ppjvdifr4kp0smkni4i3086bcf2qhd") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.4.0 (c (n "redis_ts") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0lcbm9hmdyfxgxdgs4717gy6h4pmciif3hsii5mjv6qzgaaxvziw") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.4.1 (c (n "redis_ts") (v "0.4.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1fbw6l9mnkr4mr5j1496mwhgzfwkgvhma75jqfff32scma2s7idc") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.4.2 (c (n "redis_ts") (v "0.4.2") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1gn3fxji3mhzxq78afz4q1yn702h8bn2z3fd4hk0z2kwdjvnriaj") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.5.0 (c (n "redis_ts") (v "0.5.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1l1ya8vnkqhdwx6jx2yij2482m2is7r2ddpq1id9sm6rkhyl4csq") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.5.1 (c (n "redis_ts") (v "0.5.1") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0cl2wlhln814ywfdq8kj2yl6x4y6fac0jwmmsr65sclw23zhp97y") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.5.2 (c (n "redis_ts") (v "0.5.2") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0g8dfm0wmakmlyh6fx1a636d7qkxfhxr35q9ss922fhnpjcj59fr") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.5.3 (c (n "redis_ts") (v "0.5.3") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0za2cpwjf3xjl8fqa6qmajgk69hpnn78mlypl4djzmik4lxyv3kw") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis_ts-0.5.4 (c (n "redis_ts") (v "0.5.4") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("tokio1"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "redis") (r "^0.25.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1wyc9dq7ff7h2xbvg1z43i15viy5s29rn99f46dwd4liglck23nr") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

