(define-module (crates-io re di redis-cell) #:use-module (crates-io))

(define-public crate-redis-cell-0.2.2 (c (n "redis-cell") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ggcxca40ibb1fraq8yfgkpvjhi88v42jrbkmwbz7kspc8xcjzws")))

