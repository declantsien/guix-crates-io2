(define-module (crates-io re di redify-macros) #:use-module (crates-io))

(define-public crate-redify-macros-0.0.1 (c (n "redify-macros") (v "0.0.1") (d (list (d (n "derive-elves") (r "^0.1.1") (d #t) (k 0)) (d (n "num2words") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (d #t) (k 0)))) (h "1fhnl9230y1vxbxrw6v5knlmaml6yz1zlhns3kaa5hmfzgxafyax")))

