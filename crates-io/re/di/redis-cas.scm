(define-module (crates-io re di redis-cas) #:use-module (crates-io))

(define-public crate-redis-cas-1.0.0 (c (n "redis-cas") (v "1.0.0") (d (list (d (n "redis-module") (r "^1.0.1") (d #t) (k 0)))) (h "1w1w20di7nx08q7y0cydkz29j1zdqpv4fgspi6dcxqj1ygsy0nv9") (y #t)))

(define-public crate-redis-cas-1.0.1 (c (n "redis-cas") (v "1.0.1") (d (list (d (n "redis-module") (r "^1.0.1") (d #t) (k 0)))) (h "01qn4swkzn365iqg7hqw99pvkkscah44ijajd1rhpnv09r0gq5jx")))

(define-public crate-redis-cas-1.1.0 (c (n "redis-cas") (v "1.1.0") (d (list (d (n "redis-module") (r "^2.0") (d #t) (k 0)))) (h "1cyhs7rh8cfcm4f5s2syhi4479m3083kxqicr2n0ypxn4k0zfqms")))

