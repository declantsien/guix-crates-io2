(define-module (crates-io re di redif) #:use-module (crates-io))

(define-public crate-redif-0.1.0 (c (n "redif") (v "0.1.0") (d (list (d (n "amy") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0l7fpfas3ki2pkf8j273j8mwpzcy9ga62aildf4611jb0szawv11")))

(define-public crate-redif-0.1.1 (c (n "redif") (v "0.1.1") (d (list (d (n "amy") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "08l1chi068qhl4fa1xc2h8m67qx3d3dbdk8lvfa8l6skr06d1psf")))

