(define-module (crates-io re di redis-module-test-rs) #:use-module (crates-io))

(define-public crate-redis-module-test-rs-0.1.0 (c (n "redis-module-test-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "redis") (r "^0.25.3") (d #t) (k 0)) (d (n "redis-module") (r "^2.0.7") (d #t) (k 0)))) (h "02gx9qgfai5p1klg09p9ikzp1ljlqqsqjkfy38n9c2xjqa2znrdh")))

