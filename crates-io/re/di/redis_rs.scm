(define-module (crates-io re di redis_rs) #:use-module (crates-io))

(define-public crate-redis_rs-0.1.0 (c (n "redis_rs") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1.7") (d #t) (k 0)))) (h "1bx31bsq77krzz3l68gvxva6x5bsdgi4x6crpsp4rb43vf6ws0kk")))

(define-public crate-redis_rs-0.2.0 (c (n "redis_rs") (v "0.2.0") (h "1j4hypkzq6616q7wmnbl6bk4jf9jkzsg9s5fv2hma40zxhjgrjdr")))

(define-public crate-redis_rs-0.3.0 (c (n "redis_rs") (v "0.3.0") (h "11vjwqlrr6l9b58x9nygri2rrky85arqh3ffwm9n70x014db4141")))

(define-public crate-redis_rs-0.4.0 (c (n "redis_rs") (v "0.4.0") (h "09qn7abczfgs3wylf86fmgj68vpwp639f578iwxbmqxpvqzy6izf")))

(define-public crate-redis_rs-0.4.1 (c (n "redis_rs") (v "0.4.1") (h "1p77nknd3grq7h6bb9yb082pflxh7h030p8cn77xjnhvvilc8zxm")))

(define-public crate-redis_rs-0.5.0 (c (n "redis_rs") (v "0.5.0") (h "17z8lzjrsmga4830ad3vc2k0rfd1j6x7zg7ki8mjzw13vg1fp3qm")))

(define-public crate-redis_rs-0.6.0 (c (n "redis_rs") (v "0.6.0") (h "1m7mhh8kiaqj4kdlg3yn8lfm9m4rq46lww1iv3ss5mnis52480p4")))

(define-public crate-redis_rs-0.6.1 (c (n "redis_rs") (v "0.6.1") (h "0i657fml0x3j9x1v1wrvzv8zywzjfr1rlvvn0mqa1kxl94xgp9qy")))

(define-public crate-redis_rs-0.7.0 (c (n "redis_rs") (v "0.7.0") (h "1wnbv3gm7pbcx32fnwafbqgpx0l0hbydaz8l8ygq327d6hli5yxi")))

(define-public crate-redis_rs-0.7.1 (c (n "redis_rs") (v "0.7.1") (h "18l590dg7igpcy9b5ii77913vr1gvq6l2ci1dvsf5y7nvqk8sdry")))

(define-public crate-redis_rs-0.7.2 (c (n "redis_rs") (v "0.7.2") (h "01zhyk0za1vxlfq6llds8wyaijyv3klwh35p3ngsrfha04gn20wc")))

(define-public crate-redis_rs-0.8.0 (c (n "redis_rs") (v "0.8.0") (h "01blq2n5cnk6firlhqwxrippr4qxnz0psq7w3861qyj6wvra6l7q")))

(define-public crate-redis_rs-0.8.1 (c (n "redis_rs") (v "0.8.1") (h "0k7fbq9krvbip858vkqk85yzlxpgwvdx87xiy4rizxlq80brc725")))

(define-public crate-redis_rs-0.8.2 (c (n "redis_rs") (v "0.8.2") (h "1hq89lk4m93jmc664xjr5r2pavkgzwpkkbzxdp0ld90ngdz4ygw5")))

(define-public crate-redis_rs-0.8.3 (c (n "redis_rs") (v "0.8.3") (h "1kgskasxgzqk87xm85zgxnjfqnqnyckrb0qxc7apyxw9xyh8xvfr")))

(define-public crate-redis_rs-0.8.4 (c (n "redis_rs") (v "0.8.4") (h "001k6dk0xlz6nzgws0i8c5m12q0kvxha1mp239lvn1y5d07iddmk")))

(define-public crate-redis_rs-0.8.5 (c (n "redis_rs") (v "0.8.5") (h "144dz805c9mipiczs2vlzhbx6wx41y4qkvnsz2qx4q49r50z4pxc")))

(define-public crate-redis_rs-0.8.6 (c (n "redis_rs") (v "0.8.6") (h "05sw5qlqwaijwd6cam5w3wn9r4ys7sp3rfry55acyn93dxvdjxdf")))

(define-public crate-redis_rs-0.8.7 (c (n "redis_rs") (v "0.8.7") (h "0maqfz1cshgq2b6cxyh15qkg1agb4rmwxnd0lilirzm0rfafnyg8")))

(define-public crate-redis_rs-0.8.8 (c (n "redis_rs") (v "0.8.8") (h "1750a7jhqp49vbkwvch7ccp78g8x104rxwak6ifwbldiq78af3hr")))

(define-public crate-redis_rs-0.9.0 (c (n "redis_rs") (v "0.9.0") (h "011kcgzq520r3yygjd9klkg5kx0hhs1zgb2i1aybjn4aq9ykira4")))

