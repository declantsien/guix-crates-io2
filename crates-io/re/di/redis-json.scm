(define-module (crates-io re di redis-json) #:use-module (crates-io))

(define-public crate-redis-json-0.0.1-beta-1 (c (n "redis-json") (v "0.0.1-beta-1") (d (list (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00ax6npiv86da4b677xqb3hfp9fbambcr58f0s2zgh5hg2y61r42")))

