(define-module (crates-io re di redify) #:use-module (crates-io))

(define-public crate-redify-0.0.1 (c (n "redify") (v "0.0.1") (d (list (d (n "redify-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "redis") (r "^0.22.3") (f (quote ("aio" "tokio-comp"))) (d #t) (k 0)))) (h "1fv6002lm8inbgjmvz1sb3scchhfqnraafqzpbakh2cyj4hnqpkr")))

