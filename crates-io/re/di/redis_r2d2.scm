(define-module (crates-io re di redis_r2d2) #:use-module (crates-io))

(define-public crate-redis_r2d2-0.17.0 (c (n "redis_r2d2") (v "0.17.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.17") (d #t) (k 0)))) (h "0khswmsjk7pxg5vpckasr00l80afn5sn99r4jyjvchlm56slr6vw")))

