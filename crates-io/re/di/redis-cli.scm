(define-module (crates-io re di redis-cli) #:use-module (crates-io))

(define-public crate-redis-cli-0.0.1 (c (n "redis-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.0.6") (d #t) (k 0)) (d (n "resp") (r "^0.2.0") (d #t) (k 0)))) (h "0s15d0br6cx6fw12pzv1zf1zgknsixzzpl3xwxhzinwbwcrhs2fp")))

(define-public crate-redis-cli-0.1.0 (c (n "redis-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.0") (d #t) (k 0)) (d (n "resp") (r "^0.2.1") (d #t) (k 0)))) (h "17s1fhz7h3xa9bpldgi2r02jvjxh4sg43v5mphxszvnj2s0p1pmr")))

(define-public crate-redis-cli-0.1.2 (c (n "redis-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.1.0") (k 0)) (d (n "resp") (r "^0.2.4") (d #t) (k 0)))) (h "1s3wpl6qai61ib899j2bhhbz2ckbsf1madpidav9rxf0mig45iwk")))

(define-public crate-redis-cli-0.1.3 (c (n "redis-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.1.0") (k 0)) (d (n "resp") (r "^0.2.5") (d #t) (k 0)))) (h "0n1zx6g6f4bg665mvhg1bv0yxr92a3pd34lbcj25pjjksyfb8665")))

(define-public crate-redis-cli-0.2.0 (c (n "redis-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.2.6") (d #t) (k 0)))) (h "0v637w5q15vc1pf53znpdj0yxp87mrx2azsq1r233b5hjzla7p72")))

(define-public crate-redis-cli-0.2.2 (c (n "redis-cli") (v "0.2.2") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.2") (d #t) (k 0)))) (h "1qkh48xvnq4xf6nmjw63y80lf05ws2xd7qy408x5k5lk1cgmx7iy")))

(define-public crate-redis-cli-0.3.0 (c (n "redis-cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.2") (d #t) (k 0)))) (h "1nyg789bc22zgy3yr7dz22m80vb0h7r8ciai1qj5wg76wv950k8b")))

(define-public crate-redis-cli-0.3.1 (c (n "redis-cli") (v "0.3.1") (d (list (d (n "clap") (r "^2.5.2") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.6") (d #t) (k 0)))) (h "0856gw74vzjr5wnbcrh2mjs5if9bk1rjnh5kg5jxab63gw3rk7wd")))

