(define-module (crates-io re di redis-actor) #:use-module (crates-io))

(define-public crate-redis-actor-0.1.2 (c (n "redis-actor") (v "0.1.2") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0vfv7dfcsnfx8d4j09gnqj8mwfjqfjrh3smwm8mv18i1m74xfq4l")))

(define-public crate-redis-actor-0.1.3 (c (n "redis-actor") (v "0.1.3") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1fb4pb0llcz78b0a1n8fdc3jjnkvzim4900nizqbi79zny1ip85y")))

(define-public crate-redis-actor-0.1.4 (c (n "redis-actor") (v "0.1.4") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0nai9fsxfcj889s90vrnhlkg76cggj8ygwwz28qf6cjqiw7207jm")))

(define-public crate-redis-actor-0.1.5 (c (n "redis-actor") (v "0.1.5") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0bjpww5q62sijqihcw08i1sxwv0gjwsbgzilvzfp0yvgyaqh8w2s")))

(define-public crate-redis-actor-0.2.0 (c (n "redis-actor") (v "0.2.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0dawl5kyl8brjc7ff1fgy3fwxivxahhj1aa0hnjwb6d9w3ksax60")))

(define-public crate-redis-actor-0.3.0 (c (n "redis-actor") (v "0.3.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "13p2jqkvmz2r8w1b1al6fawkadk48v56dqh0qyyng80yk0z54fkm")))

(define-public crate-redis-actor-0.4.0 (c (n "redis-actor") (v "0.4.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0g2m7bbyh7swj98inji61hly8br7hwzqh9zy93n4mn0hv2zkz01z")))

