(define-module (crates-io re di redisclient) #:use-module (crates-io))

(define-public crate-redisclient-0.1.0 (c (n "redisclient") (v "0.1.0") (h "0qysz3xw5akz2l57z8drnhpfrgy01y5clwplx0q5h6bggiwa1fi7")))

(define-public crate-redisclient-0.1.1 (c (n "redisclient") (v "0.1.1") (h "12h2wibn1784rx4xkmm2iflynhd457pachjq8lhybxzzx8g8402q")))

(define-public crate-redisclient-0.1.2 (c (n "redisclient") (v "0.1.2") (h "1pdb29ggmamfz7bg4d3jyg890j6gkm4fqlkmhh3rm84vc43bhqxw")))

