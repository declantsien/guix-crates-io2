(define-module (crates-io re di redis_raw) #:use-module (crates-io))

(define-public crate-redis_raw-1.0.0 (c (n "redis_raw") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "macros" "rt-core" "dns" "io-util"))) (d #t) (k 0)))) (h "1nhf8bl8fdcwyhmhfgxwbd07jsby6zscry5vwy8b5mfh7h836lc1")))

(define-public crate-redis_raw-1.0.1 (c (n "redis_raw") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "macros" "rt-core" "dns" "io-util"))) (d #t) (k 0)))) (h "1yi1flashs8zrhf2bdqdkxxw10myfxzk2zis9v0fxn21849xaa7f")))

