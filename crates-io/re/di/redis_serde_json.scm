(define-module (crates-io re di redis_serde_json) #:use-module (crates-io))

(define-public crate-redis_serde_json-0.1.0 (c (n "redis_serde_json") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0s5xk5kpccrc80hr31sym3p3dx20sx5i2z88pl3p3cwbaah2nnhz")))

