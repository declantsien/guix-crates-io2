(define-module (crates-io re di redislock) #:use-module (crates-io))

(define-public crate-redislock-1.3.0 (c (n "redislock") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "testcontainers") (r "^0.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.7.1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0ccyxb8dv02ivmd3nk44cg52fq4kcp2c0mlp7nrik71cgf8jfwla") (f (quote (("tls" "redis/tls") ("default") ("async" "tokio"))))))

