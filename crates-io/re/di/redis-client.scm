(define-module (crates-io re di redis-client) #:use-module (crates-io))

(define-public crate-redis-client-0.1.0 (c (n "redis-client") (v "0.1.0") (h "05f2n8jm6qahckjlmls03a8r91ihpffs040v3bvbk0656q59ndp6") (y #t)))

(define-public crate-redis-client-0.1.1 (c (n "redis-client") (v "0.1.1") (h "07rahx6mam94y88m0zkqmvmn523pq4nwp31xcqbkplj07cwijx6n") (y #t)))

(define-public crate-redis-client-0.2.0 (c (n "redis-client") (v "0.2.0") (h "1mr3lvvhfckmb3x7zlkhz9h15g22iz1jshl7sdd9ycclnczwz8mz")))

(define-public crate-redis-client-0.3.0 (c (n "redis-client") (v "0.3.0") (h "12bm3n8j375b8sffcigzvrz56kgm3bkxfc1218mly59yanz53rv4")))

(define-public crate-redis-client-0.4.0 (c (n "redis-client") (v "0.4.0") (h "0khjq05s8a06svfqw9sgpdfz4rdc4g6aq4kca4l3il3f1cnd03jp")))

(define-public crate-redis-client-0.4.1 (c (n "redis-client") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0pm7ql8vq3xsl689f0q1222xlbv9fb2rjlsg2ag4vazkyvabn0lz")))

(define-public crate-redis-client-0.4.2 (c (n "redis-client") (v "0.4.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1j04lbvwzpxrif2n7ph95y8qiyarimyv3s14yskphfhiy7fpzc5y")))

(define-public crate-redis-client-0.4.3 (c (n "redis-client") (v "0.4.3") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "01ajvyf3826mj748ra3sidhyfxbxym60r3il6rxy3wk30zwmhcbk")))

(define-public crate-redis-client-0.5.0 (c (n "redis-client") (v "0.5.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "08pfb61w3jknjdbwz0pqlalqjir201h3k5bixrx1xpg440hkmq8y")))

