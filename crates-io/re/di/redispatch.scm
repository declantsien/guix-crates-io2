(define-module (crates-io re di redispatch) #:use-module (crates-io))

(define-public crate-redispatch-0.0.1 (c (n "redispatch") (v "0.0.1") (h "1ci7zs2gdfmgw6w4jrdsr9s4bwrjxhhk97qp30s8bgz3gkmh48fg")))

(define-public crate-redispatch-0.0.2 (c (n "redispatch") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("aio" "tokio-comp"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0m324ia84ncym5ry5skz3n5xh0vc9c036dalyi0gm57z3kn2lv9g")))

