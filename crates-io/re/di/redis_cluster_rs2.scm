(define-module (crates-io re di redis_cluster_rs2) #:use-module (crates-io))

(define-public crate-redis_cluster_rs2-0.23.3 (c (n "redis_cluster_rs2") (v "0.23.3") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)))) (h "1lnmprvylbikscdlbbgba5h3r9kaipximhby2v04g050qxldsq0s")))

