(define-module (crates-io re di redis-derive) #:use-module (crates-io))

(define-public crate-redis-derive-0.1.0 (c (n "redis-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0381warmcgq82yic05q34nkaq3wc4r6z5y829v2rih2k442lpjwg") (y #t)))

(define-public crate-redis-derive-0.1.1 (c (n "redis-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x6nlhg61znsga6k1pasp5gp00r3ywp69g7rcn05457ca238svw1") (y #t)))

(define-public crate-redis-derive-0.1.2 (c (n "redis-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07skvx35yjyj9b2skjy4nfz4smdmw57hwsxsdaxfraaj6axvj66b") (y #t)))

(define-public crate-redis-derive-0.1.3 (c (n "redis-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k2qd1gjnfnrwyhwf4pvdvjjs56br16im92wqdsji1k259025lx1") (y #t)))

(define-public crate-redis-derive-0.1.4 (c (n "redis-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jvkla8sw5xpl3iscid8rlziiaqmmm4qzr68g27rzi5z0sqf0br6") (y #t)))

(define-public crate-redis-derive-0.1.5 (c (n "redis-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1l8n0ahwxcv0afg07jqq2kqas8cfb3w8ahfac3yl4y50h8jyfnvw")))

(define-public crate-redis-derive-0.1.6 (c (n "redis-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07msazpnf8js629bdrsvb10lp34jm5hlav2sy4lanr76i3f9c997")))

(define-public crate-redis-derive-0.1.7 (c (n "redis-derive") (v "0.1.7") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04pgk3bcfjvnslnljmcnnsk4xgnm15nhvd3ydz1lx26xb90alm6x")))

