(define-module (crates-io re di redis-cli-tool) #:use-module (crates-io))

(define-public crate-redis-cli-tool-0.1.0 (c (n "redis-cli-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "redis-wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8slgnwnwc6aksl1xcjfs9a8gjvmmw075qf70kz23ny44ihvrjd")))

