(define-module (crates-io re di redis-wrapper) #:use-module (crates-io))

(define-public crate-redis-wrapper-0.1.0 (c (n "redis-wrapper") (v "0.1.0") (d (list (d (n "redis") (r "^0.25.2") (f (quote ("tokio-comp" "tokio-native-tls-comp"))) (d #t) (k 0)))) (h "0hza10vlvzp7vlv383jr5z7ywqj9r290ajrfbphv0bldy536a72z")))

