(define-module (crates-io re di redis-broker) #:use-module (crates-io))

(define-public crate-redis-broker-0.1.0 (c (n "redis-broker") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1bpaf94lv9560l09g75ih6pgagb73y5v1acmnybi88gaiw9rrjsh")))

(define-public crate-redis-broker-0.1.1 (c (n "redis-broker") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1ncsargr2kax13pim5am90vp84dfbiyv38hjpmajm9r8nma55w8f")))

