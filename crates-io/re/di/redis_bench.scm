(define-module (crates-io re di redis_bench) #:use-module (crates-io))

(define-public crate-redis_bench-0.1.0 (c (n "redis_bench") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "1za090qq5kmz2jpbcjbqxm3jnq30rxa8n0hgijv64cf4wxyxjh7m")))

