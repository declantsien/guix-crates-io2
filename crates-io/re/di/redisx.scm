(define-module (crates-io re di redisx) #:use-module (crates-io))

(define-public crate-redisx-0.0.1 (c (n "redisx") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-tls") (r "^0.6.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.7") (d #t) (k 0)) (d (n "atoi") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (k 0)))) (h "1y2140sjh6krpp6jdpfkbzw0bbjyqhlc15xslyxxx0pwb9jsllhc")))

(define-public crate-redisx-0.0.2 (c (n "redisx") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "async-std") (r "^1.0.1") (d #t) (k 0)) (d (n "async-tls") (r "^0.6.0") (d #t) (k 0)) (d (n "atoi") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (k 0)))) (h "04qphzzy86rrpmjm4l9ln0r35xd5r3rw7nmvp49fwfc69rsi7gk6")))

