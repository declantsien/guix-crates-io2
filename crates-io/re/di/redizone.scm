(define-module (crates-io re di redizone) #:use-module (crates-io))

(define-public crate-redizone-0.1.0 (c (n "redizone") (v "0.1.0") (d (list (d (n "redcon") (r "^0.1.2") (d #t) (k 0)) (d (n "tzf-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1zm0miabwf3qm8h1m40ffmgdwmzvq8q32b5lip97sgmaqkd4a7ny")))

(define-public crate-redizone-0.1.2 (c (n "redizone") (v "0.1.2") (d (list (d (n "redcon") (r "^0.1.2") (d #t) (k 0)) (d (n "tzf-rs") (r "^0.4.4") (d #t) (k 0)))) (h "1cjfqiir351szhf1w940m8c4g0as9n5dlvd2hq5k28mdkjv205gp")))

(define-public crate-redizone-0.1.4 (c (n "redizone") (v "0.1.4") (d (list (d (n "redcon") (r "^0.1.2") (d #t) (k 0)) (d (n "tzf-rs") (r "^0.4.5") (d #t) (k 0)))) (h "0ndg6ksg2mp3b6mvdnxr7a1sj3ag7q34ykjhl0vb3ga6x8r914yr")))

