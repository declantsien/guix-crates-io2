(define-module (crates-io re di redisgraphio) #:use-module (crates-io))

(define-public crate-redisgraphio-0.1.0 (c (n "redisgraphio") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "redis") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "1w0rrgr4h2hkvlgxj4gadn7kv15hsv6m5vzb8idi9sasnk7nj2lx") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redisgraphio-0.2.0 (c (n "redisgraphio") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "redis") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "0p41dkdxz33zxg93jh8k7807fglqi7ff8j3zsisn5drimpqsbmpq") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "redis") ("async-std-comp" "redis/async-std-comp"))))))

