(define-module (crates-io re di redis-parser) #:use-module (crates-io))

(define-public crate-redis-parser-0.1.0 (c (n "redis-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)))) (h "0z0asj7hkkdq88p20ajgb18i6ghisv8aygafbsz72cp60cdpmr6f")))

(define-public crate-redis-parser-0.1.1 (c (n "redis-parser") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 2)))) (h "0k3zlmvnzsc5rxd20sjkhbhkcnfksrfynvnrffn4c5nng00srdkm")))

(define-public crate-redis-parser-0.1.2 (c (n "redis-parser") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 2)))) (h "1w12w61s5f0ka75vr4r89m3yc4pg3qlqlqcpy68h8ci93qbhr4x1")))

