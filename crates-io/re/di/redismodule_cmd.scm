(define-module (crates-io re di redismodule_cmd) #:use-module (crates-io))

(define-public crate-redismodule_cmd-0.0.1 (c (n "redismodule_cmd") (v "0.0.1") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (f (quote ("test"))) (d #t) (k 2)))) (h "0xfx574nmhr9www28fmf8ic9h1vhzw68hp9kgvhfz7ir0gvl3dbv")))

(define-public crate-redismodule_cmd-0.1.0 (c (n "redismodule_cmd") (v "0.1.0") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (f (quote ("test"))) (d #t) (k 2)) (d (n "redismodule_cmd_procmacros") (r "^0.1.0") (d #t) (k 0)))) (h "00kigmmz6y2klrlxr84ypifagjw9api37vnsnc9vyxc2rd66d651") (f (quote (("docgen") ("default"))))))

(define-public crate-redismodule_cmd-0.1.1 (c (n "redismodule_cmd") (v "0.1.1") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (f (quote ("test"))) (d #t) (k 2)) (d (n "redismodule_cmd_procmacros") (r "^0.1.0") (d #t) (k 0)))) (h "0mb8fdwapy5y6ax4al319vl2717cq2ihqp91afzg81gd0gwblwaw") (f (quote (("docgen") ("default"))))))

(define-public crate-redismodule_cmd-0.1.2 (c (n "redismodule_cmd") (v "0.1.2") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (d #t) (k 0)) (d (n "redis-module") (r "^0.10.0") (f (quote ("test"))) (d #t) (k 2)) (d (n "redismodule_cmd_procmacros") (r "^0.1.2") (d #t) (k 0)))) (h "1c3l77fz7hbp0l31swac09riif4vpkgkxq0dg0fpky9km7cynnsf") (f (quote (("docgen") ("default"))))))

