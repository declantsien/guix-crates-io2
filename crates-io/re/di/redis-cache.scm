(define-module (crates-io re di redis-cache) #:use-module (crates-io))

(define-public crate-redis-cache-0.1.0 (c (n "redis-cache") (v "0.1.0") (d (list (d (n "casbin") (r "^2.0.1") (f (quote ("cached"))) (k 0)) (d (n "redis") (r "^0.17.0") (f (quote ("script"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "08sbcys1kdfgwasjda1qn6kmvjjn432xy6jlsrh5izrk4vgyqg1b") (f (quote (("runtime-tokio" "casbin/runtime-tokio") ("runtime-async-std" "casbin/runtime-async-std") ("default" "runtime-async-std"))))))

(define-public crate-redis-cache-0.2.0 (c (n "redis-cache") (v "0.2.0") (d (list (d (n "casbin") (r "^2.0.1") (f (quote ("cached"))) (k 0)) (d (n "redis") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1ia9jna62fv016yflvn4mzhyakwabzcirdv5al8x3gpshpl2a55z") (f (quote (("runtime-tokio" "casbin/runtime-tokio") ("runtime-async-std" "casbin/runtime-async-std") ("default" "runtime-async-std"))))))

(define-public crate-redis-cache-0.3.0 (c (n "redis-cache") (v "0.3.0") (d (list (d (n "casbin") (r "^2.0.1") (f (quote ("cached"))) (k 0)) (d (n "redis") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "04smg6iin6j6g53ly2xv648bfsx7bjwk61kfg1ryxmcf7m3hw3jb") (f (quote (("runtime-tokio" "casbin/runtime-tokio") ("runtime-async-std" "casbin/runtime-async-std") ("default" "runtime-async-std"))))))

