(define-module (crates-io re di redisfs) #:use-module (crates-io))

(define-public crate-redisfs-0.1.0 (c (n "redisfs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "0vxffbcf1kh6wf6y0b0ks1zm88y28sx6v5plszkvzx9y0mamndqa")))

(define-public crate-redisfs-0.1.1 (c (n "redisfs") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "012fs72yk7dbpzxicrypyv3a1q3xdzv8il62vwilwb1ajg6sqwfy")))

(define-public crate-redisfs-0.1.2 (c (n "redisfs") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)))) (h "05768haj6w5axg7i12wvlnabar31pf0wqqs239lfdmwfjiggg03r")))

