(define-module (crates-io re di redis-zero-protocol-parser) #:use-module (crates-io))

(define-public crate-redis-zero-protocol-parser-0.1.0 (c (n "redis-zero-protocol-parser") (v "0.1.0") (h "102wgp87cshb2sc14vaqxrfxi4x9m60j3s306zswfzqlfinldf7n")))

(define-public crate-redis-zero-protocol-parser-0.1.1 (c (n "redis-zero-protocol-parser") (v "0.1.1") (h "0c9sd6vph1nd1bgcfbcqdy8ha2gs4kjml9gpniyk7bi6flvcmhv9")))

(define-public crate-redis-zero-protocol-parser-0.2.1 (c (n "redis-zero-protocol-parser") (v "0.2.1") (h "1xfv5i7m9979zxv1x0g6fnikh9bpa5ww9lwfkcrn8l89r7v7k799")))

(define-public crate-redis-zero-protocol-parser-0.3.0 (c (n "redis-zero-protocol-parser") (v "0.3.0") (h "1dgjpd48mff9dx9rq59yj9shjs2hj9046gc9b7clqv18l7r8ygpl")))

(define-public crate-redis-zero-protocol-parser-0.3.1 (c (n "redis-zero-protocol-parser") (v "0.3.1") (h "1mbgl8axp140ciz65svdigm9rbadvia3pihg83n16nd7zz04gmai")))

(define-public crate-redis-zero-protocol-parser-0.3.2 (c (n "redis-zero-protocol-parser") (v "0.3.2") (h "0ab6j9fx9gfc7w3pbpabvicl2n1rpcgq6pks87s07r0fw3cy6clr")))

(define-public crate-redis-zero-protocol-parser-0.3.3 (c (n "redis-zero-protocol-parser") (v "0.3.3") (h "1s0z4z14fzgm6ck455q7ls3vhn8g77hd4bvmyxlha35238a1cs1s")))

(define-public crate-redis-zero-protocol-parser-0.3.4 (c (n "redis-zero-protocol-parser") (v "0.3.4") (h "0ww7a9w28slam0rwsg4sq0kdy4nsv528468z1cvmgsjdprksq2ff")))

