(define-module (crates-io re di redis_regular_expression_module) #:use-module (crates-io))

(define-public crate-redis_regular_expression_module-0.1.0 (c (n "redis_regular_expression_module") (v "0.1.0") (d (list (d (n "redis-module") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cfcd3m0jdh3q01dd45x8m33x9lxmb57qgjwpnsl3j13c2a4f2jf")))

(define-public crate-redis_regular_expression_module-0.1.1 (c (n "redis_regular_expression_module") (v "0.1.1") (d (list (d (n "redis-module") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1943k8lz44d8467xm9fznhwlmzk9xxmxfvk05hr4y3fmi9hzv741")))

(define-public crate-redis_regular_expression_module-0.2.0 (c (n "redis_regular_expression_module") (v "0.2.0") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c83j00dsm3h13fjy4b9m5yq8kznbfs1zscbjgs3h9s7x8h1lxgf")))

(define-public crate-redis_regular_expression_module-0.2.1 (c (n "redis_regular_expression_module") (v "0.2.1") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "055w67fq16n95znfkqm5aa9pqh23bznx7vingfk0df04wdc9is0n")))

(define-public crate-redis_regular_expression_module-0.2.2 (c (n "redis_regular_expression_module") (v "0.2.2") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gsydyzc5mgzyvfaqb8pzbiqb89c6nqcqcxv78dlhhf73pq4mmfp")))

(define-public crate-redis_regular_expression_module-0.2.3 (c (n "redis_regular_expression_module") (v "0.2.3") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b3j0f7q80nmdfjz1zc2k510p03j1z7fj24h8wif5xa43qh8mdmv")))

(define-public crate-redis_regular_expression_module-0.2.4 (c (n "redis_regular_expression_module") (v "0.2.4") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1inwmprkapfxs3z9mv034b49n9qbgr3zgv0p8fahfwfif1flz5yw")))

(define-public crate-redis_regular_expression_module-0.3.0 (c (n "redis_regular_expression_module") (v "0.3.0") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0zpw1v09g85syppgbznmp49svsmnlx0wmvpmdkrlmi741rsv3ab6")))

(define-public crate-redis_regular_expression_module-0.4.0 (c (n "redis_regular_expression_module") (v "0.4.0") (d (list (d (n "redis-module") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0078ymfaglnqlh8xfmjnavmif8968zh3f2nplyjhvnfnxclr2r50")))

(define-public crate-redis_regular_expression_module-0.4.1 (c (n "redis_regular_expression_module") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "redis") (r "^0.20.2") (d #t) (k 2)) (d (n "redis-module") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "07dk9yrsfldwp39q9pbjv331l78h0l8msqnhbhb1dh49vwaik6yd")))

