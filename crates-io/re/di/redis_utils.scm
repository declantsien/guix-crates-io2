(define-module (crates-io re di redis_utils) #:use-module (crates-io))

(define-public crate-redis_utils-0.1.0 (c (n "redis_utils") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("tokio-comp"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1xinhlh20y4savy3qamsbfy33zyvrl5qrzd4b369s2sdk2js4y40")))

(define-public crate-redis_utils-0.1.1 (c (n "redis_utils") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("tokio-comp"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0ddvpgfn0fsfh396ir36hrjhzks8rbwcgpmg8i3mc1ph7yqymhn3")))

(define-public crate-redis_utils-0.1.2 (c (n "redis_utils") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("tokio-comp"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0f3qj8h1zcr7snwn7ypq97br95vxz88qgscjdbk5zk8fqlm8nj9p")))

(define-public crate-redis_utils-0.1.3 (c (n "redis_utils") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("tokio-comp"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "18r9cd59clhzqg01jv0c83hc81xfwkmq9xkkav24kmaklrcmza0n")))

