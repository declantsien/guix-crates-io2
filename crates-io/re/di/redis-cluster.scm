(define-module (crates-io re di redis-cluster) #:use-module (crates-io))

(define-public crate-redis-cluster-0.1.0 (c (n "redis-cluster") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0i0n214crqsphi5lq0khqlppx029s6k686q2b7fqvvafq3si23l6")))

(define-public crate-redis-cluster-0.1.1 (c (n "redis-cluster") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0i8f15r1jfhg80ricqdlqbgnm26zj9sqpmxngbdswyqp6d67bjm5")))

(define-public crate-redis-cluster-0.1.2 (c (n "redis-cluster") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "0dp8ak8dchy1v57a51wpw8078v11lckdxr9f03dzfxkr8q7gv2np")))

(define-public crate-redis-cluster-0.1.3 (c (n "redis-cluster") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1zrzypsyzqvvfljapm48hnpy32l3vrf5zrpas2xa1d5zsr19nd87")))

(define-public crate-redis-cluster-0.1.4 (c (n "redis-cluster") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1k7zc3i44zdczpb7pb3wfyx1sjnp42yn059jiigck5y2xqrzfz98")))

