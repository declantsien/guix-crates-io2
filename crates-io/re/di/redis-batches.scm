(define-module (crates-io re di redis-batches) #:use-module (crates-io))

(define-public crate-redis-batches-0.1.0 (c (n "redis-batches") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("script" "aio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0q80xln6qvpdl7n44f24n0bsq0wd0jpp7sm0dazk21i4mirbq012") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "tokio-comp") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis-batches-0.1.1 (c (n "redis-batches") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("script" "aio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0hkfy9ps0a9pvd3yznh5w5aflmnc4yxwc2rsicn5h9z9g1i50lan") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "tokio-comp") ("async-std-comp" "redis/async-std-comp"))))))

(define-public crate-redis-batches-0.1.2 (c (n "redis-batches") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("script" "aio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0vh9cxm97d6c6wck6cqdlq5rc07r7cbzjxbc5fn6pvlvlzv1liid") (f (quote (("tokio-comp" "redis/tokio-comp") ("default" "tokio-comp") ("async-std-comp" "redis/async-std-comp"))))))

