(define-module (crates-io re di redis-pubsub) #:use-module (crates-io))

(define-public crate-redis-pubsub-0.0.1 (c (n "redis-pubsub") (v "0.0.1") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "redis") (r "^0.21") (f (quote ("aio" "tokio-comp"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("rt" "rt-multi-thread" "macros" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("rt-multi-thread" "test-util"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0pqpwf52pgp75mhzaymhlgiyx9qi2gffj16pivhs8pc4vzbhavqb") (r "1.56")))

