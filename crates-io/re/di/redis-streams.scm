(define-module (crates-io re di redis-streams) #:use-module (crates-io))

(define-public crate-redis-streams-0.1.0 (c (n "redis-streams") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "net2") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "redis") (r "^0.11.0") (d #t) (k 0)))) (h "1fmj66f50lpdlm5jry5ngp0cdj3zh0zjvvy999ms96llg88i04m2")))

(define-public crate-redis-streams-0.1.1 (c (n "redis-streams") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "net2") (r "^0.2.34") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "redis") (r "^0.16.0") (d #t) (k 0)))) (h "1ha5075c4x4wi0a0amz6m881h21jl5fm6b4z9mvi3i7xr7fa1y8v")))

