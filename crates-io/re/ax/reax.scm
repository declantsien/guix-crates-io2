(define-module (crates-io re ax reax) #:use-module (crates-io))

(define-public crate-reax-0.1.0 (c (n "reax") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0lcmiksf48k6x6xkbzc1vx426yyjlw489lmw0nqvvwlin7pg797y")))

(define-public crate-reax-0.2.0 (c (n "reax") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1bl9ynwrnlq42idpp80xnsp6653xzqw5ip0hg2q8davyndflmkp6")))

