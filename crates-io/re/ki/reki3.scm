(define-module (crates-io re ki reki3) #:use-module (crates-io))

(define-public crate-reki3-0.0.1 (c (n "reki3") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "hyper") (r "^0.8.0") (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "redis") (r "^0.5.2") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "1hj950kb9168w6lrkb1rhzixpsnaf9mdscap36va1wz4brlkrbsa")))

(define-public crate-reki3-0.0.2 (c (n "reki3") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "hyper") (r "^0.8.0") (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "redis") (r "^0.5.2") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "0lnnz87dciap6rvpnlzjgr1vvymbpw05ic55fffywcng17akvd79")))

