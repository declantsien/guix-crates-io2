(define-module (crates-io re pn repng) #:use-module (crates-io))

(define-public crate-repng-0.1.0 (c (n "repng") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "16p6ikcz6k2af9sm3bfa71xkbj8fah432xnds3w8x926wy291c3q")))

(define-public crate-repng-0.2.0 (c (n "repng") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0c12miziix3nh9k2l340jnd3f1l4z6g28rz209gdgijcsf4rnq7b")))

(define-public crate-repng-0.2.1 (c (n "repng") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1zx79dix3298wfnzhqcbn5jznjmh9m04kr0zv9cqqrmhrwqhf2cl")))

(define-public crate-repng-0.2.2 (c (n "repng") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1nmvba099zkidyba8vbn2g03nbx3wmadc928xfrrkijwrg97rm8d")))

