(define-module (crates-io re dc redc) #:use-module (crates-io))

(define-public crate-redc-0.1.0 (c (n "redc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (f (quote ("integer"))) (k 0)) (d (n "twoword") (r "^0.1.0") (d #t) (k 0)))) (h "077dxl1f3hb7bc5fx5d9kmx0qr1mhqhz7l55661bj46zdfd9wxfa")))

