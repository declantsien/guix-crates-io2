(define-module (crates-io re dc redcon) #:use-module (crates-io))

(define-public crate-redcon-0.1.0 (c (n "redcon") (v "0.1.0") (h "0kvkjzv1qac22fmq92mkh82dp9ssnp9wzvhwdr5665z344k2fjmr")))

(define-public crate-redcon-0.1.1 (c (n "redcon") (v "0.1.1") (h "0givdg984zxy3v8bnapa0dyz0j313xisyp139wcnijmdainykbdn")))

(define-public crate-redcon-0.1.2 (c (n "redcon") (v "0.1.2") (h "19zdsm0h51khdrx0ypqhqbzjjg4s5d5id3km0i4m59k37vw8r1di")))

