(define-module (crates-io re so resolve) #:use-module (crates-io))

(define-public crate-resolve-0.0.1 (c (n "resolve") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "097kq4l6fsv29d3qhcnfn7ppiqdb2xmax9rkf84bsplx790g9spi")))

(define-public crate-resolve-0.0.2 (c (n "resolve") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14453rnmq5lxm6mav47nxb24kvfdy7p9znq9xwb8la9b2nfmjqbr")))

(define-public crate-resolve-0.1.0 (c (n "resolve") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1m81c389sa7fxgvxldpgz89b0j0achgbhnjqvr854x7szwvq6jff")))

(define-public crate-resolve-0.1.1 (c (n "resolve") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gjza9vxa5z2d323645y1vnljphij4020rw0ydzhbhxnzwd8yfjn")))

(define-public crate-resolve-0.1.2 (c (n "resolve") (v "0.1.2") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0j7m9yzykp4rqh0cpgyyl0y6rja95a6clwimaksjbxgnkdaxyh3a")))

(define-public crate-resolve-0.2.0 (c (n "resolve") (v "0.2.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jk1b0lv755xcsmy07ijjj29a4sk9yk7inpd4rgsdglrb0q6nlhr")))

