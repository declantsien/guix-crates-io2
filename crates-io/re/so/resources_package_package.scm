(define-module (crates-io re so resources_package_package) #:use-module (crates-io))

(define-public crate-resources_package_package-0.0.1 (c (n "resources_package_package") (v "0.0.1") (h "18zfdidk647bs30snnkggw0pjk9scgdyf09ybx2bwzi1rm212f3r")))

(define-public crate-resources_package_package-0.0.2 (c (n "resources_package_package") (v "0.0.2") (h "0yms0f1c06pk28ahpai550nvkhgm67456h0ras652szihmnyxmg2")))

(define-public crate-resources_package_package-0.0.3-pre (c (n "resources_package_package") (v "0.0.3-pre") (h "1n5h8v3b7cgfmv8r829ygkfd0hf0q3m7swj2xl3r8p8nnzqkpbvp") (y #t)))

(define-public crate-resources_package_package-0.0.4 (c (n "resources_package_package") (v "0.0.4") (h "1iarqfwl9njq3h4nm406dbhdmpbp7pdnq1jvxgfddqrmb0dla50w")))

(define-public crate-resources_package_package-0.0.5 (c (n "resources_package_package") (v "0.0.5") (h "0n3cwm5mwmcz6i82srx46k1vd2gk3gdc24yrz6hndb9zbal6nh2q")))

(define-public crate-resources_package_package-0.0.6 (c (n "resources_package_package") (v "0.0.6") (h "1zs6qx317z5vlb4dj54hm073z988lb99v7g32xdsnkgrind0qrcd")))

