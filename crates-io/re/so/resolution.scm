(define-module (crates-io re so resolution) #:use-module (crates-io))

(define-public crate-resolution-0.1.0 (c (n "resolution") (v "0.1.0") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xrandr") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0rk0c50bqw7c0gc4dd3rn92zb1ia00sx9w13i0d01g0ayprbcizg") (y #t)))

(define-public crate-resolution-0.1.1 (c (n "resolution") (v "0.1.1") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xrandr") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1z47i0gagkr7iy87x1a2d3swj91ydp00zy7qfhxbb7yi8fk72qc2")))

(define-public crate-resolution-0.1.2 (c (n "resolution") (v "0.1.2") (d (list (d (n "core-graphics") (r "^0.23") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xrandr") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "13f789150gw99li443ps2favcykqb7mbqyvvwhb7dwlkfqxinymp") (y #t)))

(define-public crate-resolution-0.1.3 (c (n "resolution") (v "0.1.3") (d (list (d (n "core-graphics") (r "^0.23") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xrandr") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "00wjlx64z357qj5l47bi9j9dn9s01yzpmn85imccvdfbvc93j8lp")))

(define-public crate-resolution-0.1.4 (c (n "resolution") (v "0.1.4") (d (list (d (n "core-graphics") (r "^0.23") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "xrandr") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0mzsy5b2p95bjjfnd296c7an991mnyhmjgqnfrbwyzzk6rf6y0ma")))

