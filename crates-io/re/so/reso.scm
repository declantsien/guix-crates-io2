(define-module (crates-io re so reso) #:use-module (crates-io))

(define-public crate-reso-0.0.1 (c (n "reso") (v "0.0.1") (h "14qq1ngpksbn44z3m3d3xj2sbqa8yl3czplcpc5z44nlhb75mwji")))

(define-public crate-reso-0.0.2 (c (n "reso") (v "0.0.2") (h "1m7jzxg0xarp9vq6cz0n1h92bc5khjr0rns4kpjys67snl3vb24h")))

(define-public crate-reso-0.0.3 (c (n "reso") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0sci6ql5ddy6ij3i3x46dsmwc7gqwh6mnhbr2dzqx98rbxxrfg38")))

(define-public crate-reso-0.0.4 (c (n "reso") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0jkibrnj7kdavhibln4lni7x1l1sszy31ai1cqnxlmgcq06rx6yz")))

(define-public crate-reso-0.0.5 (c (n "reso") (v "0.0.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0jsxr3ddngknmk6075x97lxfllri7f24gwsj0ldm9yrfzal4zica")))

(define-public crate-reso-0.0.6 (c (n "reso") (v "0.0.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1isnfs4y9j4038s81mp3ilhigd2xfcmzfx2rhyfkzvisxj0f3n5f")))

(define-public crate-reso-0.0.7 (c (n "reso") (v "0.0.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0bb9hsnv7spylk66jg0zaivqzhai2kan04hir9zi5aw7jn2v7lqw")))

