(define-module (crates-io re so resonata) #:use-module (crates-io))

(define-public crate-resonata-0.1.0 (c (n "resonata") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0m8id8sb0n2yl7y86d4i630g5j8xrc6cf49bgc37vc2gxa71z23h") (y #t)))

(define-public crate-resonata-0.1.1 (c (n "resonata") (v "0.1.1") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "11gsqpl5imk790ni5r2qj728if0x41h64cmasfpd2bfx9v2059p9") (y #t)))

(define-public crate-resonata-0.2.0 (c (n "resonata") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "07848j6nxgm17a4mb3ll0k29yrh52i5qlcln4lb3f8lwxyh7kjdn")))

(define-public crate-resonata-0.2.1 (c (n "resonata") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1wyj6h3f5ps7zvqz1hhslwdlddvw5bzym0g42f7d6kz8v1j66kjd")))

(define-public crate-resonata-0.3.0 (c (n "resonata") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0g9v6k08h3av29kqszmf4mqlp3qs4qc3qgh94rd7yddm377asaip")))

