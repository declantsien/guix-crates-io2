(define-module (crates-io re so resourcetrack) #:use-module (crates-io))

(define-public crate-resourcetrack-0.1.0 (c (n "resourcetrack") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1dsif43pxyd5pji64c11s0g18vs61w2hg4935bhazybnrf82mwz0")))

(define-public crate-resourcetrack-0.3.0 (c (n "resourcetrack") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0x9b5sj9fh5dw4y3y11dip4fqv7w1qddp4chb6dwc2skag3z8fpv")))

(define-public crate-resourcetrack-0.4.0 (c (n "resourcetrack") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "18nwycar1lyiiklwiryhd2grjdkxg3bibf89cmsr6nvb1iqdjrsm")))

