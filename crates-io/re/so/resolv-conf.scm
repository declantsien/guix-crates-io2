(define-module (crates-io re so resolv-conf) #:use-module (crates-io))

(define-public crate-resolv-conf-0.1.0 (c (n "resolv-conf") (v "0.1.0") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0c3y2gnz3m02i3f8k4j6wrna3cy1cgdrcd8ai23c4y0zzkbha128")))

(define-public crate-resolv-conf-0.2.0 (c (n "resolv-conf") (v "0.2.0") (d (list (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "181a9djg50g7yz11s1g3fjrfkfdp41iqjclbbxrj6imxh3bawqnw")))

(define-public crate-resolv-conf-0.3.0 (c (n "resolv-conf") (v "0.3.0") (d (list (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^0.2.2") (d #t) (k 0)))) (h "01p4r2my8lxh88asqxqi3cshzvxc945a10zpccgxlclnazav8bla")))

(define-public crate-resolv-conf-0.4.0 (c (n "resolv-conf") (v "0.4.0") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1v8wbxn6wj7dr8h4xfqqxvhm7il98d63mrs29qhmma9np7d3xwia")))

(define-public crate-resolv-conf-0.5.0 (c (n "resolv-conf") (v "0.5.0") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1w5r5lcs7k41ir02k8lg7p458hryrkyrywh2605w2dkcighk2k4v")))

(define-public crate-resolv-conf-0.6.0 (c (n "resolv-conf") (v "0.6.0") (d (list (d (n "hostname") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1aswycqa7hxxhc5wimbwb01yqlwavqds4jkflrmrsrd2nrmhh6wf") (f (quote (("system" "hostname"))))))

(define-public crate-resolv-conf-0.6.1 (c (n "resolv-conf") (v "0.6.1") (d (list (d (n "hostname") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "14hs950j56j8rvc9d8axgsp8x3aawr8rkbm2zkvzs7l485ddjay6") (f (quote (("system" "hostname"))))))

(define-public crate-resolv-conf-0.6.2 (c (n "resolv-conf") (v "0.6.2") (d (list (d (n "hostname") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1jvdsmksdf6yiipm3aqahyv8n1cjd7wqc8sa0p0gzsax3fmb8qxj") (f (quote (("system" "hostname"))))))

(define-public crate-resolv-conf-0.6.3 (c (n "resolv-conf") (v "0.6.3") (d (list (d (n "hostname") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0jlzifww1h7j23jnjj49xz8q0fpd9rqpd0ks8c4y651vgw9lx0qi") (f (quote (("system" "hostname"))))))

(define-public crate-resolv-conf-0.7.0 (c (n "resolv-conf") (v "0.7.0") (d (list (d (n "hostname") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "005sk8r1php2g41yn7fdf1sn8cafyaqm6jxia42h2v88saa47r2j") (f (quote (("system" "hostname"))))))

