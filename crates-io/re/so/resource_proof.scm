(define-module (crates-io re so resource_proof) #:use-module (crates-io))

(define-public crate-resource_proof-0.1.0 (c (n "resource_proof") (v "0.1.0") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "0ap3j7z4grc49gisc14swskb3wg2bb38c674n8f5dnx48dc5dd87")))

(define-public crate-resource_proof-0.2.0 (c (n "resource_proof") (v "0.2.0") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "0a3l45m9qq1c8g6si3xzhwz19nw4xfz982i1qpygwsjvi3rzvb8p")))

(define-public crate-resource_proof-0.2.1 (c (n "resource_proof") (v "0.2.1") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "0z2f5x792kv1jds7kjjknvzgj6d9sx935vlr0dpg3f4m5bm6zsnn")))

(define-public crate-resource_proof-0.2.2-pre (c (n "resource_proof") (v "0.2.2-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "031i58qpnblnsqr5mcw9n4p938zj3lrjli96gnckpqagbagajv8a")))

(define-public crate-resource_proof-0.2.3-pre (c (n "resource_proof") (v "0.2.3-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "18ci9ixky5r072g2mrslyariw54dysga572ln3f2w1rmaigahknz")))

(define-public crate-resource_proof-0.2.4-pre (c (n "resource_proof") (v "0.2.4-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "1f724a7c06inl8li7c91p11vy0qczq9vs0j8gl06811pwh2zl4ai")))

(define-public crate-resource_proof-0.2.5-pre (c (n "resource_proof") (v "0.2.5-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "1gnwdglsp1a3kxsnjpnvwmbsgrzl6bassn14iqvb5jn342nabclc")))

(define-public crate-resource_proof-0.2.7-pre (c (n "resource_proof") (v "0.2.7-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "1hphnyqsdi9114cf0ixw2ba0986hys6jynpqpkgrd1b10j32z2zv")))

(define-public crate-resource_proof-0.3.0-pre (c (n "resource_proof") (v "0.3.0-pre") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.1.1") (d #t) (k 0)))) (h "074mlgqa7xg30r47ah96bbw3hmxz4c5acq0mqsvhqkc1ibrxy45w")))

(define-public crate-resource_proof-0.3.1 (c (n "resource_proof") (v "0.3.1") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.86") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.2.0") (d #t) (k 0)))) (h "0n3x3spmkaiz8a392hl9xaglwh1kia75qy4rx1nbpi8wdx9vx5vy")))

(define-public crate-resource_proof-0.4.0 (c (n "resource_proof") (v "0.4.0") (d (list (d (n "clap") (r "~2.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "termion") (r "~1.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.2.0") (d #t) (k 0)))) (h "151h9wvn6zyza4kfghng8sbd38p50ysilvnzy2gzmbkjbz9l60s1")))

(define-public crate-resource_proof-0.5.0 (c (n "resource_proof") (v "0.5.0") (d (list (d (n "clap") (r "~2.25.1") (d #t) (k 0)) (d (n "rand") (r "~0.3.15") (d #t) (k 0)) (d (n "termion") (r "~1.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.3.0") (d #t) (k 0)))) (h "05c269jn9n4ha9f7hlqxlipqwrb69hiwbcyz68dd9l0c7ah73aly")))

(define-public crate-resource_proof-0.6.0 (c (n "resource_proof") (v "0.6.0") (d (list (d (n "clap") (r "~2.29.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.4.0") (d #t) (k 0)))) (h "1q236lf8095mjih9hxbr6xlv729kjhf0md7bqqhrn1l2lhd660fb")))

(define-public crate-resource_proof-0.7.0 (c (n "resource_proof") (v "0.7.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.4.0") (d #t) (k 0)))) (h "0znkr84fx9c9w9ix3f04dncn2bk229mmq053p3gx5v6q8ckj6s2c")))

(define-public crate-resource_proof-0.8.0 (c (n "resource_proof") (v "0.8.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "1ccknplpbdw08hzp1svyl520bs6w1lfrnsddsypafi8zg565sv76")))

(define-public crate-resource_proof-1.0.31 (c (n "resource_proof") (v "1.0.31") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "0x2bkmnjkimdpbqimh25v58nqwx0fpi3hm91hvali54vbh478q43")))

(define-public crate-resource_proof-1.0.32 (c (n "resource_proof") (v "1.0.32") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "1y1k1qnlcx8r0ip5bmmlb4w3ma6m6vq1x8qv5afyypfcvk3q94ms")))

(define-public crate-resource_proof-1.0.33 (c (n "resource_proof") (v "1.0.33") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "1jin2yffmgwwkjdgxinqgyhi907m8d24baq1k9x6dg87z1mzy75b")))

(define-public crate-resource_proof-1.0.34 (c (n "resource_proof") (v "1.0.34") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "1l0n0f12720p1xn0s82lbblnvb02jmsrks4ivqcblw2by7plgf8z")))

(define-public crate-resource_proof-1.0.35 (c (n "resource_proof") (v "1.0.35") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "~1.5.0") (d #t) (k 0)))) (h "0fx6s795pnw5xv0mhpprbgyc5aqljdwnvplf80ry2mr0v71bmjzj")))

(define-public crate-resource_proof-1.0.36 (c (n "resource_proof") (v "1.0.36") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1b5pa5gai3vpw1si1srss2s2v4zgqkj5l5jlijrdd9jzi9d1cp53")))

(define-public crate-resource_proof-1.0.37 (c (n "resource_proof") (v "1.0.37") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1j4ja4zc7r661wgya4kw9bp7szspi8ns9x5lpq9f2l77wy20h2bv")))

(define-public crate-resource_proof-1.0.38 (c (n "resource_proof") (v "1.0.38") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "rand") (r "~0.4.1") (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "01qb8ck2bjbgy6kjdh5vrwq9zy1bp0fh1lr97xa08dzkj3jk8zr9")))

(define-public crate-resource_proof-1.0.39 (c (n "resource_proof") (v "1.0.39") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "~1.5.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1z9nrx497ky26mszpbyb8h6q5x82b7rzr41dv43dk5c1m32c6rvf")))

