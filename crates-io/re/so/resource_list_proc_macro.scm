(define-module (crates-io re so resource_list_proc_macro) #:use-module (crates-io))

(define-public crate-resource_list_proc_macro-0.5.0 (c (n "resource_list_proc_macro") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "~0.5.15") (d #t) (k 0)) (d (n "quote") (r "~1.0.3") (d #t) (k 0)))) (h "1g6flm8v8gc7g9d4zcc9r8xqkng2acagn96x5rm33gssffgjd34x")))

