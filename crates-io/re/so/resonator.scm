(define-module (crates-io re so resonator) #:use-module (crates-io))

(define-public crate-resonator-0.1.0 (c (n "resonator") (v "0.1.0") (h "0z2zzp3bivrpk6193hjsjgybylfyxfgz6i6qns39kl7a33nikap7")))

(define-public crate-resonator-0.2.0 (c (n "resonator") (v "0.2.0") (h "05ahil1dswfyqv3hm6ih31mpvbf2d85aszqj5pxnpzbzrzwal5pn")))

(define-public crate-resonator-0.2.1 (c (n "resonator") (v "0.2.1") (h "1qnp0955vwqj07m2kracs5y1465z659dvp9d9lkavfkixhiqb7v1")))

(define-public crate-resonator-0.2.11 (c (n "resonator") (v "0.2.11") (h "02zxz7djwk2nh128ig1q632z0dy6061xykphdarj7lw7v79nmpb2")))

(define-public crate-resonator-0.2.12 (c (n "resonator") (v "0.2.12") (h "11xl3sfh040s7vcvlq80kmxd49xcnjh9548y5si1m80h8s5xvlly")))

(define-public crate-resonator-0.3.0 (c (n "resonator") (v "0.3.0") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "http-body") (r "^1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "socketioxide") (r "^0.10") (f (quote ("extensions" "state"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower-http") (r "^0.5.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "0db808dr117cc5wqbmb73p7c4rm0mnimmi3b72136az58y0z0kh5")))

(define-public crate-resonator-0.3.1 (c (n "resonator") (v "0.3.1") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "http-body") (r "^1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "socketioxide") (r "^0.10") (f (quote ("extensions" "state"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower-http") (r "^0.5.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "10g0lqfbn8i7r5c59by5basyiryzy734qyw3lnayv1r4iha6yb2s")))

(define-public crate-resonator-0.3.2 (c (n "resonator") (v "0.3.2") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "http-body") (r "^1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "socketioxide") (r "^0.10") (f (quote ("extensions" "state"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower-http") (r "^0.5.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "1mba3qqm0xvlkwmp7anvi4s60sy6cpnmjb767mpnwrd10znabhq6")))

