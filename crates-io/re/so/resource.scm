(define-module (crates-io re so resource) #:use-module (crates-io))

(define-public crate-resource-0.2.0 (c (n "resource") (v "0.2.0") (h "0zn60vmv4fbsd3ilzgdfvbny4i990knbh8fsxl92l6rr71yrl5lk") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-resource-0.2.1 (c (n "resource") (v "0.2.1") (h "1cjbmppylh9xmyca57mlf4fz7z008jk8qfbwgksa1n5rgia46dn9") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-resource-0.3.0 (c (n "resource") (v "0.3.0") (h "08m0xgscjvl126gfqygbz3dpnnmpvrbniy8z71gg1555lzlv03dy") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-resource-0.4.0 (c (n "resource") (v "0.4.0") (h "1dhsbf40scg3sn33hdlpl75w2j45ji1iiwmm8s89csl263kanrkr") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-resource-0.5.0 (c (n "resource") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "~0.5.15") (o #t) (d #t) (k 0)) (d (n "resource_list_proc_macro") (r "=0.5.0") (o #t) (d #t) (k 0)))) (h "08jn1jydw9kx5xgvjv0lqly7zhrqj7j9b4d95nh3fq1r7iqgd9qi") (f (quote (("force-static") ("force-dynamic") ("experimental-resource-list" "resource_list_proc_macro" "proc-macro-hack") ("default"))))))

