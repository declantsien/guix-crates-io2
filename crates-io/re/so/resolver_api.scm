(define-module (crates-io re so resolver_api) #:use-module (crates-io))

(define-public crate-resolver_api-0.1.0 (c (n "resolver_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04wkz8ah898gzd4h5ppyiqrh98pj0k33127lwwv6apw02pclz3gm")))

(define-public crate-resolver_api-0.1.1 (c (n "resolver_api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cazxvjm4iipr8da4wc3p1x01w0xgdr0gh1yykgdbszhz18cbpyx")))

(define-public crate-resolver_api-0.1.2 (c (n "resolver_api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15v59sa93s9sby5v9wsy3xsxai0159admzgcn899sk6z6zal63yy")))

(define-public crate-resolver_api-0.1.3 (c (n "resolver_api") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pydx7hnprknvqd2sfm5wjgn9l179lgh9qm0rjm8wj706yyh3p21")))

(define-public crate-resolver_api-0.1.4 (c (n "resolver_api") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sa3ffmjijjvdc8dp9gm1mhdgqfak52jsqf9maip7y27yx33xv5l")))

(define-public crate-resolver_api-0.1.5 (c (n "resolver_api") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10b7iidbc5lvyapa24zriyb39jz003lcpfh6p74hm0q2m4pn2k1a")))

(define-public crate-resolver_api-0.1.6 (c (n "resolver_api") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mrpf9yxa671ndnylz7r83sifmkypy6zj63vvni885sn3dbdgqnm")))

(define-public crate-resolver_api-0.1.7 (c (n "resolver_api") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0z81xqkqqwl2py0z1c0wd8g6ij63i3kcvlhbnnq12higqklsljv8")))

(define-public crate-resolver_api-0.1.8 (c (n "resolver_api") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1kvr3zlndf3v98a6jqrknkr9shsirqpn9nm1360021ggalf9ficj")))

(define-public crate-resolver_api-0.1.9 (c (n "resolver_api") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "167ry4ca6dlpwyjmyrxv4izwvqr3b35cm2ykzxr10px8r5mix623")))

(define-public crate-resolver_api-1.0.0 (c (n "resolver_api") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.85") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "116li6jyvshyd03nz3knb2wsf6kxd8rpkgws5d1fh6kl1x8vrr39")))

(define-public crate-resolver_api-1.1.0 (c (n "resolver_api") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.85") (d #t) (k 0)) (d (n "resolver_api_derive") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1da52rfj2vwrmnlky6sy55d1l2hz7ljad36fvdldwqi6fhg87s5l")))

