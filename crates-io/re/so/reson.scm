(define-module (crates-io re so reson) #:use-module (crates-io))

(define-public crate-reson-0.1.0 (c (n "reson") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "realfft") (r "^3.3.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.3") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1gsjxjm72nld0l1azaws4g4c5rsdzqlxlvlllvzcfjychmbsfckc")))

