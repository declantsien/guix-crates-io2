(define-module (crates-io re so resolved-pathbuf) #:use-module (crates-io))

(define-public crate-resolved-pathbuf-0.1.0 (c (n "resolved-pathbuf") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wwlzlbqbx16b88z2p42c9p6hyvvzgzxgnz0xagjlxwsvh2ji9l3")))

(define-public crate-resolved-pathbuf-0.1.1 (c (n "resolved-pathbuf") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gkkby2iyy9f39r24iqg6lsfb6aflvq78yn8767sk3sskzal3w5z")))

(define-public crate-resolved-pathbuf-0.1.2 (c (n "resolved-pathbuf") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0inm7wli05jmd6xwsfnlvk6jyha4zh0rrz2ciddfxpzr1fk99hcj")))

(define-public crate-resolved-pathbuf-0.1.3 (c (n "resolved-pathbuf") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g7lmszw2nzla0jw2zdm1gkva0fk2nwc7khdr3pnmfp3z4mj4li2")))

