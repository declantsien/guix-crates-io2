(define-module (crates-io re so resolver-cli) #:use-module (crates-io))

(define-public crate-resolver-cli-0.1.0 (c (n "resolver-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)))) (h "152jw8p67w0jbg514fvx0qfpn17wcmk65fsv2vsnr54x4imislh2")))

(define-public crate-resolver-cli-0.1.1 (c (n "resolver-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)))) (h "1h167i110gicwxg80bzl359azcm249622d5p22290j2mdj3bz8i7")))

(define-public crate-resolver-cli-0.1.2 (c (n "resolver-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)))) (h "0rdy7hlsf2ndj11h7w21kc77i33kzv9a6b0q80kkg7z2dmdbfb4a")))

