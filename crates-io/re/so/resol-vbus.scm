(define-module (crates-io re so resol-vbus) #:use-module (crates-io))

(define-public crate-resol-vbus-0.1.0 (c (n "resol-vbus") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "0j9l1xbhppyq5r604mwvxaqbc4c1pr4ncfr0b1zzagmksvkpaq4d")))

(define-public crate-resol-vbus-0.1.1 (c (n "resol-vbus") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "0dikxdhna5ksr6rk6b2w0ijv8q98nnvkz9glqzikvbgxr6iz70py")))

(define-public crate-resol-vbus-0.2.0 (c (n "resol-vbus") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1scrq4z46sq925xr34ys1rac134qlm5q1b0m3mr52rkakga3zdid")))

(define-public crate-resol-vbus-0.2.1 (c (n "resol-vbus") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0ayy1387y9xx5f71i2g415hgzvnf31gwljv23cxcbxxjwqz2a5cl") (f (quote (("no-default-spec"))))))

