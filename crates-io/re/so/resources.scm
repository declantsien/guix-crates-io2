(define-module (crates-io re so resources) #:use-module (crates-io))

(define-public crate-resources-0.1.0 (c (n "resources") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.3.2") (d #t) (k 0)))) (h "07gcjd1h54n9mingvjjkksabnhkfqjysgx13yvjz7rrzww6p4jcd")))

(define-public crate-resources-0.2.0 (c (n "resources") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.3.2") (d #t) (k 0)))) (h "0wdyangp2mab27saz5h1wvhfh6wl50s3x2lf6vngd8730h1fsr5x")))

(define-public crate-resources-0.2.1 (c (n "resources") (v "0.2.1") (d (list (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0x6mwjhkid7ygk3q2n9l13mbrazkmn2806a6lrxv8kf4zacliigq")))

(define-public crate-resources-1.0.0 (c (n "resources") (v "1.0.0") (d (list (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "095p1v5jndsf94l4vgk6pjb7mpgsqm8fz8nzv91d983nfx5vwwnq")))

(define-public crate-resources-1.1.0 (c (n "resources") (v "1.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "0rjh9nhnw3ghs7w4hhp152inz1j4pp9wfj3vjz995sq96yhhw1s2") (f (quote (("fetch"))))))

