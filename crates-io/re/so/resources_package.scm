(define-module (crates-io re so resources_package) #:use-module (crates-io))

(define-public crate-resources_package-0.0.1-pre.1 (c (n "resources_package") (v "0.0.1-pre.1") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1pj1j71a9nj1fkg6qmhb9b8jal8ha1iq7ilz214qyr0nwkkd99cn")))

(define-public crate-resources_package-0.0.1 (c (n "resources_package") (v "0.0.1") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "06s5gfnmjk9psa9l3k2b8cqfg3sjv4811h9lxncwnan6i9cssg7d")))

(define-public crate-resources_package-0.0.2 (c (n "resources_package") (v "0.0.2") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "0mbamfpj3arb4x2r3iwgj387qirwzllvmvg6kbp95s3sns4v0fap")))

(define-public crate-resources_package-0.0.3 (c (n "resources_package") (v "0.0.3") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "0qvi3pcgrlacp1iw1k1m6nd1rs0456kyc6jk9bw1kpiiigkm5w1m")))

(define-public crate-resources_package-0.0.4 (c (n "resources_package") (v "0.0.4") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1m2dg86kgkv3v1zyarp386556wpqwcfzfpdlsmssvia9xdv9abzg")))

(define-public crate-resources_package-0.0.5 (c (n "resources_package") (v "0.0.5") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1sji8j3zmdxl7bdzn22misjiyh00b3xp4zhycbbvkafmkwa01z5q")))

(define-public crate-resources_package-0.0.6 (c (n "resources_package") (v "0.0.6") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1xizxjndqk6vnkxk1bpvs6sqicbsrzr7s0kasvi3wvnj5ad8bjcq")))

(define-public crate-resources_package-0.0.7 (c (n "resources_package") (v "0.0.7") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "0ylsgrnyfzxn7imp3n0dvffd58rmgb8gv2zsbxndv6ifws2s4rsn")))

(define-public crate-resources_package-0.0.8 (c (n "resources_package") (v "0.0.8") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1qs8vd9p11607x09ck81d9vrb97drf01vdyq932izlrnlvvcz008")))

(define-public crate-resources_package-0.1.0 (c (n "resources_package") (v "0.1.0") (d (list (d (n "resources_package_package") (r "*") (d #t) (k 0)))) (h "1qqz9zr2ngnrh5jxsinndhaxnm35zg3p7chgs605vh6dzmhrv9wi")))

