(define-module (crates-io re so resource-fork) #:use-module (crates-io))

(define-public crate-resource-fork-0.1.0 (c (n "resource-fork") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1gcmsy3343dh2p7ch2fdihwdxp0xglb3yn0b2rc8nmj4nvddkah6")))

(define-public crate-resource-fork-0.2.0 (c (n "resource-fork") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "13jv193gpkxlvan5k28pglwhrpiai1p2ffm7z1vvl4hyzqb4342j")))

(define-public crate-resource-fork-0.3.0 (c (n "resource-fork") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1ycaqv58mwyf5r1hm0qxdb29mrhl44226li7b7kzyva4hfqg1m0f")))

