(define-module (crates-io re so resolver) #:use-module (crates-io))

(define-public crate-resolver-0.1.0 (c (n "resolver") (v "0.1.0") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "14f2dcym9dm8sbk9j30acm7q125zb6cr1yij7p91ysmcwm5ajbh4") (r "1.61.0")))

(define-public crate-resolver-0.1.1 (c (n "resolver") (v "0.1.1") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0m4fzwqhqc3vk2pz5jl3670h0nxqwz5838ygca2sab93wmf5w09c") (r "1.61.0")))

(define-public crate-resolver-0.1.2 (c (n "resolver") (v "0.1.2") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1ibcgaik24a5fhl8r91csxc8mvwjbgc8k7bsmaywyc08nqn22www") (r "1.61.0")))

