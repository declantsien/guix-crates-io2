(define-module (crates-io re so resource-path) #:use-module (crates-io))

(define-public crate-resource-path-0.0.0 (c (n "resource-path") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "165i75n0kb1glibb7skmqqhmwa5bngqafpq0sdqj1aj6ni1shj7h") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-resource-path-0.1.0 (c (n "resource-path") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trauma") (r "^2.2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1m23zhspi1bc7fhwma2pzlclj9w5cpwrs954ds55gq2blnkvzwv7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

