(define-module (crates-io re so resolvconffs) #:use-module (crates-io))

(define-public crate-resolvconffs-0.1.0 (c (n "resolvconffs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (k 0)) (d (n "fuser") (r "^0.10.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "trait-set") (r "^0.2.0") (d #t) (k 0)))) (h "1fyvp9bvfhzag388qi0sax2vlzwmiy13sb369n4kcnq787dqy555") (f (quote (("mini" "log/release_max_level_off") ("logging") ("default" "logging"))))))

(define-public crate-resolvconffs-0.1.1 (c (n "resolvconffs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (k 0)) (d (n "fuser") (r "^0.10.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "trait-set") (r "^0.2.0") (d #t) (k 0)))) (h "07xv1vjzx21jvimcr5zqg2jfazphdk5g48l6s6lks18hnx7vhfa6") (f (quote (("mini" "log/release_max_level_off") ("logging") ("default" "logging"))))))

