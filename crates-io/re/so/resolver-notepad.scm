(define-module (crates-io re so resolver-notepad) #:use-module (crates-io))

(define-public crate-resolver-notepad-0.1.0 (c (n "resolver-notepad") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1phndzq0zxx7x3lblwagxga9vmr2k7p0sc72vw7jg3xfsiv8qvh6")))

(define-public crate-resolver-notepad-0.2.0 (c (n "resolver-notepad") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1f5qq23mdzzkkg6sw8n6h0gw8rlsjkxda8h6d1p9y0yvm0r7anv5")))

(define-public crate-resolver-notepad-0.3.0 (c (n "resolver-notepad") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "01zz4sky5abz3wqna0bi9lw4y7svb057010mkpcagwsgfrkrk7q0")))

