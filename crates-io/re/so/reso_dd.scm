(define-module (crates-io re so reso_dd) #:use-module (crates-io))

(define-public crate-reso_dd-0.1.0 (c (n "reso_dd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gmvd67s1c0wnyrnlap1q22d1bn1krr6rd7gslsh6s93y8skrbbc") (y #t)))

(define-public crate-reso_dd-0.2.0 (c (n "reso_dd") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15d3zj3xc808s9v7w9a5yfh293x9mjvnf1nb9y8qpnhz9p5fx3kg") (y #t)))

