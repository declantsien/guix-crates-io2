(define-module (crates-io re so resolv) #:use-module (crates-io))

(define-public crate-resolv-0.1.0 (c (n "resolv") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0sqsypw0qqy27d5fva2hjgi84wgikfqyvpylvsrvvm2fwdvb07n9")))

(define-public crate-resolv-0.1.1 (c (n "resolv") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ngqmp6yh521qgcrfpi31s84038nsbjh5rmwfy6008fz8q7fdaz7")))

(define-public crate-resolv-0.1.2 (c (n "resolv") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hjnnwmbfv783dx22h3rrwj0iwjkr199dwcjbxrv1kjc6833kg8y")))

(define-public crate-resolv-0.1.3 (c (n "resolv") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16yxa9kr9r59m13ppbvahfx81iz1r9h5i5p8a8blg1i34hjld6ys")))

(define-public crate-resolv-0.1.4 (c (n "resolv") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0vzpalymf25zyvl4kzx3nkprqjiwpymwbx0z7rlm845nyfnlnv87")))

(define-public crate-resolv-0.1.5 (c (n "resolv") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hacdic881yfqqv3x2clby1jklr6ai754hv1k9qalxljxycfa10g")))

(define-public crate-resolv-0.2.0 (c (n "resolv") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0c9v0v87xnxncm07wyf7d4rym9k7swshs06yk19q55idvkw9pwp6")))

(define-public crate-resolv-0.3.0 (c (n "resolv") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libresolv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1dda7n5l2vxsfxqrp1wy07xzlwszp3nz4zb120n238x1q7943yjh")))

