(define-module (crates-io re ds redstone-near-connector-rs) #:use-module (crates-io))

(define-public crate-redstone-near-connector-rs-0.0.1 (c (n "redstone-near-connector-rs") (v "0.0.1") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0-pre.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0iakcz5z09a59jhn06ikvsqbgy49ygh8nx6h6dq3yc81685ppw90")))

(define-public crate-redstone-near-connector-rs-0.0.2 (c (n "redstone-near-connector-rs") (v "0.0.2") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0-pre.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0g2mkkiyfgkncawc9rd0bj6zyqmjzplnnhmjf0bnqxida0mbq6lr")))

(define-public crate-redstone-near-connector-rs-0.0.3 (c (n "redstone-near-connector-rs") (v "0.0.3") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.0-pre.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0xngz39sqnfn0kwfc8zqfg4z4lsw9r59q4m6cazi0cf75s9jbsd8")))

