(define-module (crates-io re ds redsync) #:use-module (crates-io))

(define-public crate-redsync-0.1.0 (c (n "redsync") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13k5annm3zmjggvayfqxjdycxdn328zhsb6s2hmhc3i4vc370080")))

(define-public crate-redsync-0.1.1 (c (n "redsync") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fb2s8ghz2lvqg4q465kgxa8q2kcgnd7vx1z69k4zlzvrbhm5x54")))

(define-public crate-redsync-0.1.2 (c (n "redsync") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jq3zncax59lgnv0nnb7nxzka59x6z6viar3j6r1pbzwj5rcg0bm")))

(define-public crate-redsync-1.0.0 (c (n "redsync") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j9wklcjbqkzk6wqbb8ghidq8jm49qln1ac62al0cyb14hs90n8z")))

(define-public crate-redsync-1.0.1 (c (n "redsync") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i913z2yh4xb37hchr95b1nj9gcws11v1slwbh0qyi4ax9m384nh")))

