(define-module (crates-io re ds redseat-crypto) #:use-module (crates-io))

(define-public crate-redseat-crypto-0.1.0 (c (n "redseat-crypto") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16c5z5qx0mmfzga0prmcf75pmk6jxmrdvq30w4hi4fwk7pmmlyrc")))

