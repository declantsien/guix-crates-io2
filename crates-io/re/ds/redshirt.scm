(define-module (crates-io re ds redshirt) #:use-module (crates-io))

(define-public crate-redshirt-0.1.0 (c (n "redshirt") (v "0.1.0") (d (list (d (n "ring") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0w7m3y6mshwh143k4a5ldh1g7102lxxian33l3qvn9j30njrsa89") (f (quote (("redshirt2" "ring") ("redshirt1") ("default" "redshirt1" "redshirt2")))) (y #t)))

(define-public crate-redshirt-0.1.1 (c (n "redshirt") (v "0.1.1") (d (list (d (n "ring") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "07xh0k42p10n7k16ln24fvb4ar3gl55drv8wml529brjyf1310y1") (f (quote (("redshirt2" "ring") ("redshirt1") ("default" "redshirt1" "redshirt2"))))))

(define-public crate-redshirt-0.1.2 (c (n "redshirt") (v "0.1.2") (d (list (d (n "ring") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1g1ic3g138gikqqcpvxbz0y6jjr42a16is6adhi1z8mg9v5qba9h") (f (quote (("redshirt2" "ring") ("redshirt1") ("default" "redshirt1" "redshirt2"))))))

(define-public crate-redshirt-0.1.3 (c (n "redshirt") (v "0.1.3") (d (list (d (n "ring") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0ik60r49hignzbs07mxwwznjfv6wnj0hjsd5p50ds8xvwmc09zf2") (f (quote (("redshirt2" "ring") ("redshirt1") ("default" "redshirt1" "redshirt2"))))))

