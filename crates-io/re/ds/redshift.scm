(define-module (crates-io re ds redshift) #:use-module (crates-io))

(define-public crate-redshift-1.0.0 (c (n "redshift") (v "1.0.0") (d (list (d (n "csv") (r "^0.14.4") (d #t) (k 0)))) (h "1jypsdjycybmhsmfr52dswwq7nx925q9xd8xaw2igb49hqw4yrx4")))

(define-public crate-redshift-1.0.1 (c (n "redshift") (v "1.0.1") (d (list (d (n "csv") (r "^0.14.4") (d #t) (k 0)))) (h "1q1b9qacfvv9pznvrwamb9my9kl6b2vp8ihhf0vcrimfbfs6lx44")))

(define-public crate-redshift-1.0.2 (c (n "redshift") (v "1.0.2") (d (list (d (n "csv") (r "^0.14.4") (d #t) (k 0)))) (h "07iylmf34hq83477i2m3dpf9lrjaml2idb8jcs36i9chjhnrggfa")))

(define-public crate-redshift-1.0.3 (c (n "redshift") (v "1.0.3") (d (list (d (n "csv") (r "^0.14.4") (d #t) (k 0)))) (h "1rbr6n6agzwgapgnfcf8syxpf6affzql9j0brqmrpj5adn9lfdsy")))

