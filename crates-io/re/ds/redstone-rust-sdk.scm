(define-module (crates-io re ds redstone-rust-sdk) #:use-module (crates-io))

(define-public crate-redstone-rust-sdk-0.1.0 (c (n "redstone-rust-sdk") (v "0.1.0") (h "1xm62w33001k9igfjf8z42fz7y5k674wlpgjqxlp2qwpmrc9dw0m")))

(define-public crate-redstone-rust-sdk-0.1.1 (c (n "redstone-rust-sdk") (v "0.1.1") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "ink_env") (r "^3.4.0") (d #t) (k 0)))) (h "01rjjds8mgcl14bags8qk9zikg4ppqn7d1qlrmhz2s95w0dz1iy5")))

(define-public crate-redstone-rust-sdk-0.1.2 (c (n "redstone-rust-sdk") (v "0.1.2") (d (list (d (n "bitmaps") (r "^3.2.0") (d #t) (k 0)) (d (n "ink_env") (r "^3.4.0") (d #t) (k 0)))) (h "0hfxyg9pr5f6vwmx7878spglm3n5yj4v4sm5v5axlkszldm74l1p")))

