(define-module (crates-io re xr rexrocksdb) #:use-module (crates-io))

(define-public crate-rexrocksdb-0.3.2 (c (n "rexrocksdb") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "librocksdbsys") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0wzh2gx1qn7jcla4g9skjpz60x5lbrhv9d4z2ipfxffdplg1bgc0") (f (quote (("valgrind") ("sse" "librocksdbsys/sse") ("portable" "librocksdbsys/portable") ("default"))))))

