(define-module (crates-io re _b re_build_build_info) #:use-module (crates-io))

(define-public crate-re_build_build_info-0.3.0-alpha.1 (c (n "re_build_build_info") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "0cj8pdcysdlx804889c8pf4hak04z9acha1g2ss21dsigkqjjyaf") (r "1.67")))

(define-public crate-re_build_build_info-0.3.0 (c (n "re_build_build_info") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "12r3sisr2sc1j8qav1if13hc9gh9ias57ldqx4ni84nkz0clsy3x") (r "1.67")))

(define-public crate-re_build_build_info-0.3.1 (c (n "re_build_build_info") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "0r1zz9xx0qk165vsa5m72jr77i8526wzc8swxs3b6liyhv21hbmw") (r "1.67")))

(define-public crate-re_build_build_info-0.4.0 (c (n "re_build_build_info") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "0k30apqshh87b2i323409ipmg883gi4g41dw24cx8rmfb61c970y") (r "1.67")))

(define-public crate-re_build_build_info-0.5.0-alpha.0 (c (n "re_build_build_info") (v "0.5.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "1q35aaadbi4fj7wrryrn7zzm35363jynchhwslv9afggivxfj0sa") (r "1.67")))

(define-public crate-re_build_build_info-0.5.0 (c (n "re_build_build_info") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "0abf4pbfh6pwhzd3a1mwqhil0ibl6ap79nb084qfy5rrvwa2d4p5") (r "1.67")))

(define-public crate-re_build_build_info-0.5.1-alpha.1 (c (n "re_build_build_info") (v "0.5.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "0ksvbf882j99m0dvdln4x4pqvqasyyby8ygr5nnw8gval04w7cv9") (r "1.67")))

(define-public crate-re_build_build_info-0.5.1 (c (n "re_build_build_info") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (d #t) (k 0)))) (h "1qwv0h6812cvbizyd8nk06idwvk8w3jxi5lygj5hsphghaqbm0s7") (r "1.67")))

(define-public crate-re_build_build_info-0.6.0-alpha.1 (c (n "re_build_build_info") (v "0.6.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (k 0)))) (h "17h2qglc9dx7vpy2qys4ck951qyrpikhxfsd0fxxrdralkdzajal") (r "1.69")))

(define-public crate-re_build_build_info-0.6.0-alpha.2 (c (n "re_build_build_info") (v "0.6.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (k 0)))) (h "08fx0kpy5j429ixjf5vqi192vpv8z1qidfl744syz64k9v5ynk8l") (r "1.69")))

(define-public crate-re_build_build_info-0.6.0 (c (n "re_build_build_info") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (k 0)))) (h "16rvdk44mn73nnf6bvdi7nag0h64mlfw8p42xaw4ysn5pmmhfak1") (r "1.69")))

(define-public crate-re_build_build_info-0.7.0-alpha.1 (c (n "re_build_build_info") (v "0.7.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (k 0)))) (h "147m27b9bxiwr35nh90sgwbd7g4dz575fwj720id0v1snnv0qnss") (r "1.69")))

(define-public crate-re_build_build_info-0.7.0 (c (n "re_build_build_info") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("wasm-bindgen" "formatting"))) (k 0)))) (h "1d1z7jm4q1r36xrklc9cki7jnfly9832shp9nawpp86hznvmc0lf") (r "1.69")))

