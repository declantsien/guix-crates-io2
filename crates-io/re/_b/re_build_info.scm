(define-module (crates-io re _b re_build_info) #:use-module (crates-io))

(define-public crate-re_build_info-0.3.0-alpha.1 (c (n "re_build_info") (v "0.3.0-alpha.1") (h "02sfq76dchs4kvwlqbqbk7p2ypj9al8plb2mw684c0rga7lrchm0") (r "1.67")))

(define-public crate-re_build_info-0.3.0 (c (n "re_build_info") (v "0.3.0") (h "0g91wmxvd7ib9367pmrq9xkcy5xmhz12m515zdxrmfzk8czc7775") (r "1.67")))

(define-public crate-re_build_info-0.3.1 (c (n "re_build_info") (v "0.3.1") (h "015ivp6zfrc9kgy5m2np2zqcbdhcl3xxlf7mvcx0cvaqq9vp84sb") (r "1.67")))

(define-public crate-re_build_info-0.4.0 (c (n "re_build_info") (v "0.4.0") (h "11l0knh28zcyx5iaxd13gmvnzc0h4f19k5nvxfwz6zp6y23q85ry") (r "1.67")))

(define-public crate-re_build_info-0.5.0-alpha.0 (c (n "re_build_info") (v "0.5.0-alpha.0") (h "1qkxhg2psirqgxlryzw6hjklrwpwbyfi75h4cf5zm0wgx693342q") (r "1.67")))

(define-public crate-re_build_info-0.5.0 (c (n "re_build_info") (v "0.5.0") (h "1gb9ii6pz0j7pyi639fma45aa15kdg863mnhb3zjnkk5354m283f") (r "1.67")))

(define-public crate-re_build_info-0.5.1-alpha.1 (c (n "re_build_info") (v "0.5.1-alpha.1") (h "10y3vgw15rnng1cwp0fi9pk4dcpaqpix2xkl5sn4wl2cj91g63jp") (r "1.67")))

(define-public crate-re_build_info-0.5.1 (c (n "re_build_info") (v "0.5.1") (h "10cfapsfd87fy8dmwpf0vbjzk4gsamsmyqp26r4kxv03g04wigj3") (r "1.67")))

(define-public crate-re_build_info-0.6.0-alpha.1 (c (n "re_build_info") (v "0.6.0-alpha.1") (h "0dr1m8m73vdxs7mh6z5b9wlaxisrhpsbmvj34xyks3aal4scz3gm") (r "1.69")))

(define-public crate-re_build_info-0.6.0-alpha.2 (c (n "re_build_info") (v "0.6.0-alpha.2") (h "1qjkf619x34vajql1zrcimjfjln5mskrnkamrynlm2wan35iq5ic") (r "1.69")))

(define-public crate-re_build_info-0.6.0 (c (n "re_build_info") (v "0.6.0") (h "02s0yyh5nd12kndajqjhzwcfcdjxczl55mmkxsc8641859fjv5hc") (r "1.69")))

(define-public crate-re_build_info-0.7.0-alpha.1 (c (n "re_build_info") (v "0.7.0-alpha.1") (h "0ivmhdg63i61k4w5b46ms4qk2ylxf562mvjg2kh6gyws3dv8y6s4") (r "1.69")))

(define-public crate-re_build_info-0.7.0 (c (n "re_build_info") (v "0.7.0") (h "1z1s1w9lx1kmf090yw0kqxl8vy87vzldylwdv7kcyh8im6xmp8hl") (r "1.69")))

(define-public crate-re_build_info-0.8.0-alpha.1 (c (n "re_build_info") (v "0.8.0-alpha.1") (h "1ii39kdsvqliibq5yz5sgdrnb0iq6qcgnzj2973r7ycv2ijbqria") (r "1.69")))

(define-public crate-re_build_info-0.8.0-alpha.2 (c (n "re_build_info") (v "0.8.0-alpha.2") (h "1dgmdawd7rhsd39v8s2hfanzwb0w0in7mn8qwrzwc990hqaz8p1y") (r "1.69")))

(define-public crate-re_build_info-0.8.0-alpha.6 (c (n "re_build_info") (v "0.8.0-alpha.6") (h "08gm60xlrhlwf7x5smpyyy62w36np8zyli353zpf17622nnygd4i") (r "1.69")))

(define-public crate-re_build_info-0.8.0-alpha.7 (c (n "re_build_info") (v "0.8.0-alpha.7") (h "0h9wkml3lg1dndn51x74ycxwps6nm78fs0zdvn4a8w4qiihdrx1n") (r "1.69")))

(define-public crate-re_build_info-0.8.0 (c (n "re_build_info") (v "0.8.0") (h "1xpl2ygm9dg19vfq4s7qc2r1zg463bwa16i8cxia1lpqak9p1isi") (r "1.69")))

(define-public crate-re_build_info-0.9.0-alpha.1 (c (n "re_build_info") (v "0.9.0-alpha.1") (h "1fnqsv1blrancfc3bn3gx6kv67rrmzl25gmlm6zafl8ag5kq3blc") (r "1.69")))

(define-public crate-re_build_info-0.9.0-alpha.2 (c (n "re_build_info") (v "0.9.0-alpha.2") (h "1xlzcfxnzcnv1bpxx4i9cpdfr6ahbxwqnq4psr0sqcava1sqq8z0") (r "1.69")))

(define-public crate-re_build_info-0.8.1-rc.1 (c (n "re_build_info") (v "0.8.1-rc.1") (d (list (d (n "const_panic") (r "^0.2.8") (f (quote ("rust_1_64"))) (d #t) (k 0)))) (h "02cdi0abdg44icydd7kmc9769y81ffm82y1fb6ringiar7kxqcw4") (r "1.69")))

(define-public crate-re_build_info-0.8.1 (c (n "re_build_info") (v "0.8.1") (h "0z1l5vhvjsiz3m7m3c1pi0hcspakdcqmr0mksdyk2s1p3d1zk7hp") (r "1.69")))

(define-public crate-re_build_info-0.9.0-alpha.3 (c (n "re_build_info") (v "0.9.0-alpha.3") (h "1h4778q12z5vckjg1d5kjk7b8vsacs37cfylqx8jlgk5i2w4rlh5") (r "1.72")))

(define-public crate-re_build_info-0.8.2 (c (n "re_build_info") (v "0.8.2") (h "1divy7z77f5kv7m2sf3crrvg0llf4vmbnbnwm5xksal236l7qkdv") (r "1.69")))

(define-public crate-re_build_info-0.9.0-alpha.4 (c (n "re_build_info") (v "0.9.0-alpha.4") (h "1y0qrc6rrw9klp9lgl2qr3nl6900yq95lqh8ld7dix4pskj6gm53") (r "1.72")))

(define-public crate-re_build_info-0.9.0-alpha.5 (c (n "re_build_info") (v "0.9.0-alpha.5") (h "1965zplvvzn6020rw6s4j224rpab4frx26kn4c4ddncs46i5pj0q") (r "1.72")))

(define-public crate-re_build_info-0.9.0-alpha.6 (c (n "re_build_info") (v "0.9.0-alpha.6") (h "02gyckpk3hldd735sid7c84cmkv0f11lyka1agrb4w8ja6jhjv61") (r "1.72")))

(define-public crate-re_build_info-0.9.0-alpha.9 (c (n "re_build_info") (v "0.9.0-alpha.9") (h "13dgnpccwdfdc0741h9jxiy3n21k2i16abixyzywshkg4m7gmsf9") (r "1.72")))

(define-public crate-re_build_info-0.9.0-alpha.10 (c (n "re_build_info") (v "0.9.0-alpha.10") (h "10zf1635vspxxa5sxbmb59npqr8llb0rr4djk3xvx7xxf2blarbw") (r "1.72")))

(define-public crate-re_build_info-0.9.0-rc.2 (c (n "re_build_info") (v "0.9.0-rc.2") (h "08j5f5ljndzz73wb53ch17d615rpy5s4wzr7fhg5b8qzrjcqj4ac") (r "1.72")))

(define-public crate-re_build_info-0.9.0-rc.3 (c (n "re_build_info") (v "0.9.0-rc.3") (h "0v18rnjw36dakyjn243rb9wacn0g1p035n7bw5kcl7c9vbn141pr") (r "1.72")))

(define-public crate-re_build_info-0.9.0-rc.6 (c (n "re_build_info") (v "0.9.0-rc.6") (h "116cnvxzaa330qimmmy2jy7pkyyxq3pwqv3lha4ckhc12j3cjx7c") (r "1.72")))

(define-public crate-re_build_info-0.9.0-rc.7 (c (n "re_build_info") (v "0.9.0-rc.7") (h "1az4rymdsyy2vgrmhl6p37yp9hmn97l4gljsqqdyswhflflwf4fp") (r "1.72")))

(define-public crate-re_build_info-0.9.0 (c (n "re_build_info") (v "0.9.0") (h "1jp6w3zqrx8wzaaks2xf7bdmnlqyh19gy10jkr5x4m43d5vjrpjl") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.2 (c (n "re_build_info") (v "0.10.0-alpha.2") (h "1x10hqh39i2xdplmcklmj96pv9fx64wd9ddqancj9fvxlchz5wxg") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.3 (c (n "re_build_info") (v "0.10.0-alpha.3") (h "01x9qv1b96z1wl0fp7vnpgafk3q8gshglwyms9ig67xr099l0syi") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.4 (c (n "re_build_info") (v "0.10.0-alpha.4") (h "12ryg43h4j00hf62qy48flky6p4j7s61zwaw7vmp71ddwg531ahi") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.5 (c (n "re_build_info") (v "0.10.0-alpha.5") (h "0bd04n8kx22k0qw7vgpy975rh7gd85xyk0chsy1k1ks2jszq5s54") (r "1.72")))

(define-public crate-re_build_info-0.9.1-rc.3 (c (n "re_build_info") (v "0.9.1-rc.3") (h "11rs97n4f8bi0ddkihdlr0b2v2als5v47gqlqspln5h13g60ylgh") (r "1.72")))

(define-public crate-re_build_info-0.9.1-rc.4 (c (n "re_build_info") (v "0.9.1-rc.4") (h "02nn07npxqlqrnkljq033qj5l3c3ai0j8w6zfq9zn4mqw6ip53i0") (r "1.72")))

(define-public crate-re_build_info-0.9.1 (c (n "re_build_info") (v "0.9.1") (h "0sggn7ijqbfjyf2f2rxlal5z768zykpkrnapcbg0w2aw0wwn3xv6") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.8 (c (n "re_build_info") (v "0.10.0-alpha.8") (h "0d5z9hfwx5xfjpz4dhfrf11dhl4nwdyxzvq7g9gn56hxsgpc15im") (r "1.72")))

(define-public crate-re_build_info-0.10.0-alpha.10 (c (n "re_build_info") (v "0.10.0-alpha.10") (h "1i6nsmgc8hfc3ww524w7sspv748l3rngz2k2m52q13r2a8hck8xb") (r "1.72")))

(define-public crate-re_build_info-0.10.0-rc.1 (c (n "re_build_info") (v "0.10.0-rc.1") (h "0nl8cjgs6pqic6kdwx7n5sw15spnw5j3m8lv19slmaw6i27l7xk4") (r "1.72")))

(define-public crate-re_build_info-0.10.0 (c (n "re_build_info") (v "0.10.0") (h "1g47vri0qhhzra3agm7bwrjkx781psrbhj1w04cmilksp3r2jpb5") (r "1.72")))

(define-public crate-re_build_info-0.10.1-rc.1 (c (n "re_build_info") (v "0.10.1-rc.1") (h "08z1n3iy9sqcjalnm18848ydy82nk897nkwvxx5w0s685mngbh6h") (r "1.72")))

(define-public crate-re_build_info-0.10.1 (c (n "re_build_info") (v "0.10.1") (h "1vyv3ah9hn5y9lsq5fx72j7yij5amq50yipf5nfcqq49hx3x2j8v") (r "1.72")))

(define-public crate-re_build_info-0.11.0-rc.1 (c (n "re_build_info") (v "0.11.0-rc.1") (h "06zsbchzb554waf2ba0qa3ws4hzj92h203jwwvkc7x99y85x754l") (r "1.72")))

(define-public crate-re_build_info-0.11.0-rc.2 (c (n "re_build_info") (v "0.11.0-rc.2") (h "05p30ymq9dskqrj61rqsg61mfs5wk39ikq8hszl7bfnv89nz4api") (r "1.72")))

(define-public crate-re_build_info-0.11.0 (c (n "re_build_info") (v "0.11.0") (h "1ps53dryjhlxsk370a8hnh0vf018y7ln7jmmm9bj4dzgjinvfxln") (r "1.72")))

(define-public crate-re_build_info-0.12.0-alpha.2 (c (n "re_build_info") (v "0.12.0-alpha.2") (h "0dcsmmn5qa7k6klrbmsv7j86kq9zn79lzdkw7ga6da7x0w43z0s1") (r "1.72")))

(define-public crate-re_build_info-0.12.0-alpha.3 (c (n "re_build_info") (v "0.12.0-alpha.3") (h "0x09ds5b477prfksbkxhw2wvvqvl5fijzxsqc770hljfcg6v8m6s") (r "1.72")))

(define-public crate-re_build_info-0.12.0-alpha.4 (c (n "re_build_info") (v "0.12.0-alpha.4") (h "1q6jv3frpp4qyldxldqr8ybmnwnjdpknhgi44ibxckfmdggq6dcf") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.1 (c (n "re_build_info") (v "0.12.0-rc.1") (h "0z3kf9mrigrplf0gyikb9apx03k4qj0s01g8rq2y6xpg5r34faqw") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.2 (c (n "re_build_info") (v "0.12.0-rc.2") (h "09y8cm40c6pzsc0y75l01scg55blhy0xwgs087bvvri5qsqnljn5") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.3 (c (n "re_build_info") (v "0.12.0-rc.3") (h "14lplwnn98qy9sgvklamhrw5x3490p5k95ak80z8hidp8b4qyisv") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.4 (c (n "re_build_info") (v "0.12.0-rc.4") (h "1ibyjlzq56mcxydgmjl6pb2aarqnzxjml1bbncz2i9yfljwvcnr6") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.5 (c (n "re_build_info") (v "0.12.0-rc.5") (h "0ni37zvg8kabhp06d4ljvw9bsas3c23fifqag4jk19irsrwybpgp") (r "1.72")))

(define-public crate-re_build_info-0.12.0-rc.6 (c (n "re_build_info") (v "0.12.0-rc.6") (h "05jzgg4kppr3x49rympm6vyxxhyfcl7cdyc72qpjbjfnfnsdnm2b") (r "1.72")))

(define-public crate-re_build_info-0.12.0 (c (n "re_build_info") (v "0.12.0") (h "1n1bbcwfb8mwlmr99vssb0nak5rwm2mrixr87xkl8rzm96nvbh3c") (r "1.72")))

(define-public crate-re_build_info-0.13.0-alpha.2 (c (n "re_build_info") (v "0.13.0-alpha.2") (h "0pyg782323a16c0y7dv61jzjxqxz3a5wp4cs4wlxbcshn0wwrsvz") (r "1.72")))

(define-public crate-re_build_info-0.12.1-rc.1 (c (n "re_build_info") (v "0.12.1-rc.1") (h "1zmhc5zaksnp7cakjcl1x3czah1d071m9bcgd5a3h2505grycw5f") (r "1.72")))

(define-public crate-re_build_info-0.12.1-rc.2 (c (n "re_build_info") (v "0.12.1-rc.2") (h "1aln38m98mq4cd7f0jfjb7vwvd89va3wqgnq5ikipx2jqq96k5g4") (r "1.72")))

(define-public crate-re_build_info-0.12.1 (c (n "re_build_info") (v "0.12.1") (h "1bk32xs7l9fz5q60jlrlanrb0xhs0qd2zbsnapqxmr874dy5isgn") (r "1.72")))

(define-public crate-re_build_info-0.13.0-alpha.3 (c (n "re_build_info") (v "0.13.0-alpha.3") (h "1sqirgc7y90474san92i7w6bm2gqqs4hc90wmq7mn0ckjlkpli71") (r "1.72")))

(define-public crate-re_build_info-0.13.0-alpha.4 (c (n "re_build_info") (v "0.13.0-alpha.4") (h "00lwxmr4290n14acs5wssaxbb0dgfhb5mbp0w31vrbcibz2f7klw") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.1 (c (n "re_build_info") (v "0.13.0-rc.1") (h "01pgw3scds6sbhag0kwxa5i1bjaqgacb81rfcydw07qxb4ad884q") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.2 (c (n "re_build_info") (v "0.13.0-rc.2") (h "0bdacann3yjwxrl7gwzpqiwk47ijn6l3wihpcgqx7l1gz3dknbzm") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.3 (c (n "re_build_info") (v "0.13.0-rc.3") (h "1rdqsqdc2h4al6whs3dqfivpi9rxvf7is9bj7qy9bzc18cc9h3lp") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.4 (c (n "re_build_info") (v "0.13.0-rc.4") (h "0v5ab51xa1ys4ajf7wq28l1sn7pxj86l29ahf4cchbsv2aj87wnk") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.5 (c (n "re_build_info") (v "0.13.0-rc.5") (h "07r7fnpay5znbc7y82vfdimyv8swpy0a8nbgnyhy8f80hg64s47y") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.6 (c (n "re_build_info") (v "0.13.0-rc.6") (h "16sjrnwrnw1i4ry70kxia13yrdgvwk31jaf7b8k1ad9w109a7wa1") (r "1.72")))

(define-public crate-re_build_info-0.13.0-rc.7 (c (n "re_build_info") (v "0.13.0-rc.7") (h "1vx2im8qnh845v54m18frfjmwpxp4z93q9b6xvi5y72d6a3y645q") (r "1.72")))

(define-public crate-re_build_info-0.13.0 (c (n "re_build_info") (v "0.13.0") (h "0ixmgv8hgxjkm98bpip5wrp8gd2f8ynwl5v5lwmclh1i2wp05fxl") (r "1.72")))

(define-public crate-re_build_info-0.14.0-alpha.2 (c (n "re_build_info") (v "0.14.0-alpha.2") (h "1r07cgq4izisfnwb7py522443xxpfigg4r7pjm6wzsw6k3037mp5") (r "1.74")))

(define-public crate-re_build_info-0.14.0-rc.1 (c (n "re_build_info") (v "0.14.0-rc.1") (h "1bnn56rkxaivw65nimaf8ivnsna6wlakiss95h0vw87vkxpykhmd") (r "1.74")))

(define-public crate-re_build_info-0.14.0-rc.2 (c (n "re_build_info") (v "0.14.0-rc.2") (h "16g3yriv3amhhhnabdvdqf009zc63xsyv13cw762vc68b22vwr4v") (r "1.74")))

(define-public crate-re_build_info-0.14.0-rc.3 (c (n "re_build_info") (v "0.14.0-rc.3") (h "18dwlz31k6h14dsfp6nlimzb09dvdra51zlx8hzp0akl2r8593cf") (r "1.74")))

(define-public crate-re_build_info-0.14.0-rc.4 (c (n "re_build_info") (v "0.14.0-rc.4") (h "0si5a6pk5bxl2bqpavjc6p1nkparl3my7xnwkpa1h691ra1k9rmg") (r "1.74")))

(define-public crate-re_build_info-0.14.0 (c (n "re_build_info") (v "0.14.0") (h "1yns8zz4mjk21k5w3l3b18dcfji6kp605l5mhwpv2gh99z6x771n") (r "1.74")))

(define-public crate-re_build_info-0.14.1 (c (n "re_build_info") (v "0.14.1") (h "0fkyg395wxdmlngmi1762jxa139dr23m55d7xp4xcpgvmmh6n1k9") (r "1.74")))

(define-public crate-re_build_info-0.15.0-alpha.2 (c (n "re_build_info") (v "0.15.0-alpha.2") (h "1h6r1z8pszfyhb23xl3wspd6lmhvnmcny4my9szkk6wadk0khkqg") (r "1.74")))

(define-public crate-re_build_info-0.15.0-alpha.3 (c (n "re_build_info") (v "0.15.0-alpha.3") (h "1hii4q55y543p54xgbkj2mkxhrr7mcr5r659xmqigf936cxfz7fr") (r "1.74")))

(define-public crate-re_build_info-0.15.0-alpha.4 (c (n "re_build_info") (v "0.15.0-alpha.4") (h "0cjpcpw8bx3qi8lvn3jspvinlsqgqd3rlj3cl07xz1nhnf93ziyl") (r "1.74")))

(define-public crate-re_build_info-0.15.0-alpha.5 (c (n "re_build_info") (v "0.15.0-alpha.5") (h "0rxk7v88614rr9wn1x8vzi1f6rf1l22yfjs4v2hsfwn6q4m35ya3") (r "1.74")))

(define-public crate-re_build_info-0.15.0-rc.1 (c (n "re_build_info") (v "0.15.0-rc.1") (h "0z02cmqr0rlnrngpp57xqdvnzjlil8bjfqcvyx8bxql9a78p95s7") (r "1.74")))

(define-public crate-re_build_info-0.15.0-rc.2 (c (n "re_build_info") (v "0.15.0-rc.2") (h "1sav0nz7r20ivdmxzppms8v2p7i27bj6pxssqzcyl681l23jf13d") (r "1.74")))

(define-public crate-re_build_info-0.15.0-rc.3 (c (n "re_build_info") (v "0.15.0-rc.3") (h "0gq9qmrrbdhaahidpvipfz09ycs0jgajayd2s10c2v7k3x78rkhz") (r "1.74")))

(define-public crate-re_build_info-0.15.0 (c (n "re_build_info") (v "0.15.0") (h "0dp5i858z3j8gq6a91aqpbx5px15nqdvnngv2zbrfwd6vd1vznz6") (r "1.74")))

(define-public crate-re_build_info-0.15.1-rc.1 (c (n "re_build_info") (v "0.15.1-rc.1") (h "079ylxvzwfvik1gshd6hag0mgxvkm3gc2a2y9r5v9bpqhl36d1ql") (r "1.74")))

(define-public crate-re_build_info-0.15.1 (c (n "re_build_info") (v "0.15.1") (h "04l6vr6mm2hrvqgg8acgzl62agj0jp3xnfj1cqv87vyqv5wjwzql") (r "1.74")))

(define-public crate-re_build_info-0.16.0-alpha.3 (c (n "re_build_info") (v "0.16.0-alpha.3") (h "0hvcqjbwdiywd6nlmwiamhyavrs9l9lv2pmlmkk52xx4mxynkmni") (r "1.76")))

(define-public crate-re_build_info-0.16.0-alpha.4 (c (n "re_build_info") (v "0.16.0-alpha.4") (h "05kx2cjljycg6949npsslarvxzq5bqi44595d30qb56lcaf3ydc1") (r "1.76")))

(define-public crate-re_build_info-0.16.0-rc.1 (c (n "re_build_info") (v "0.16.0-rc.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0m1rampac9rxrj7qr37g9igph22p2fq61ma6ba3rqg34yjqbvk47") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.0-rc.2 (c (n "re_build_info") (v "0.16.0-rc.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "10qy4frvkxvj6xviay1bdpd1kgvyhg7qk9s2fa4v7vp7s0h293ap") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.0-rc.3 (c (n "re_build_info") (v "0.16.0-rc.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0qyb2g63f2vlqz6nqi1jyijdvp4zrwsfb3l2kzh40vsb8ygkhbnq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.0-rc.4 (c (n "re_build_info") (v "0.16.0-rc.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1s2h0y46vm4y7p1dh115dyl93ky6pzzblyvkln2sqrd3p9kbpj6i") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.0 (c (n "re_build_info") (v "0.16.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1l9hi57lp7rbnxxarpgsvjyvx0k341b102dr2c4n9adnw697hi3y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.17.0-alpha.2 (c (n "re_build_info") (v "0.17.0-alpha.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "03whb58p6xif0bkmypvhgy3050chgncplngrf6y684skdswwyg59") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.17.0-alpha.3 (c (n "re_build_info") (v "0.17.0-alpha.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1zpcwj83cx0pnaxgf7ip05s4i3k232rc0bv0jh6prg9mik05gh4q") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.1-rc.1 (c (n "re_build_info") (v "0.16.1-rc.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0r907qmjd8jv89i6r9kdvxzh1k8w5by99nbpag17bkv3c34h8wlq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.1-rc.2 (c (n "re_build_info") (v "0.16.1-rc.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "08biqcz5lwvv4sj51d4hf7mwy29dz8chx9fwkcr0hhsk3wcsl2q2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

(define-public crate-re_build_info-0.16.1 (c (n "re_build_info") (v "0.16.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0mw0ys6pl8lv0czas3jn7jia3xj2dnn5yb38926a54fl4mjzykhp") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.76")))

