(define-module (crates-io re kk rekker) #:use-module (crates-io))

(define-public crate-rekker-0.1.0 (c (n "rekker") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.23.5") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26.1") (d #t) (k 0)))) (h "1l5p5s7689alva77vp6c805sgfnszr1m7nr7il7576npf4fg405s")))

(define-public crate-rekker-0.1.1 (c (n "rekker") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.23.5") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26.1") (d #t) (k 0)))) (h "1iwc10hby7b9s7vmdkp2ri6z1h21bjsl3f7ikds122ni2snp8jyr")))

