(define-module (crates-io re dm redmaple) #:use-module (crates-io))

(define-public crate-redmaple-0.1.0 (c (n "redmaple") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "12vxa526g5mkd2ki9j6k9mzbavzyz6333k7k3lmfz94bixxp0n6s")))

(define-public crate-redmaple-0.2.0 (c (n "redmaple") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "154gd1cfjkr8rwkh4pk0l28a14yvyf12qynkzfby48zsawanvqgp")))

(define-public crate-redmaple-0.3.0 (c (n "redmaple") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dyvbnig1s881zq9fn44crjmzlx6a6ijviy1g0cvpjgh9ay1lj4c")))

(define-public crate-redmaple-0.3.1 (c (n "redmaple") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m64lmpry789pxi2n37490mf1wilmg2ymn4chrslcd916khycyx0")))

(define-public crate-redmaple-0.4.0 (c (n "redmaple") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sjq9ri81jbc3n5kxrhbr2jay9z8wb7ik3zxkz0i2yhphi16fc7y")))

(define-public crate-redmaple-0.4.1 (c (n "redmaple") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vbkfn7nv2cnpigqzwq352gl995wrdz6f504m6p3gh1rvv424l5z")))

(define-public crate-redmaple-0.4.2 (c (n "redmaple") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xrqhhs0rps6rblay6zad37x6rrc0qcybn6ck2ddndn9s8m7r9c5")))

(define-public crate-redmaple-0.5.0 (c (n "redmaple") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zzv240pfg5rr8bvi9hv7hzhnbn65vn9x34639ml05m1avzbpkqr")))

(define-public crate-redmaple-0.5.1 (c (n "redmaple") (v "0.5.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lxwwj3qx12gmyk5jxcar5wh79d3kklzxqb0lb7mz2ilisxb15q1")))

(define-public crate-redmaple-0.6.0 (c (n "redmaple") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yymjwfrab3m4l4r2bwk4vg6f1rzjj2pgl39xw392bh42b329mpv")))

(define-public crate-redmaple-0.7.0 (c (n "redmaple") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "14ivl8sarpk37gkjg9alb2m6c07bab34b737jgdhcfh46gpkd87h")))

(define-public crate-redmaple-0.8.0 (c (n "redmaple") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1az6f79gg8hcg3h5nbjbd6j2xyk31iv484cxy7ggm88yk9qjvj0v")))

(define-public crate-redmaple-0.8.1 (c (n "redmaple") (v "0.8.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "13chfb8njmvxn2kz5arp91nqgh1nmajsk5cdsh7yvmkq5pk69ilb")))

(define-public crate-redmaple-0.8.2 (c (n "redmaple") (v "0.8.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ll7f7lvsnkv3wv0h683bdlsmvm551rw8dsdxkw0s3v6bmfpzc63")))

(define-public crate-redmaple-0.8.3 (c (n "redmaple") (v "0.8.3") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bbv2w3k0v7vqkqqndcy85yf6a029zh4pcqg6lmv3rhn9arq237k")))

(define-public crate-redmaple-0.9.0 (c (n "redmaple") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z982fgj1dzfjiz4blynzz30kk1gf65fi66l5vihj345fik80y06")))

(define-public crate-redmaple-0.10.0 (c (n "redmaple") (v "0.10.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "053nnc7c48mpyc1dkas4pd37xd1l3dvzjhx7sfdxh3qgpcprysar")))

(define-public crate-redmaple-0.10.1 (c (n "redmaple") (v "0.10.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1d65qardfzyxqbzp8j13l8jrhlhh8bb0zw1y7ials1kgwap7nakh")))

(define-public crate-redmaple-0.11.0 (c (n "redmaple") (v "0.11.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "03dm68slrkm6vdacgmhcf4lcrkgayq0pcwhvck72q8dm8lypbxj9")))

(define-public crate-redmaple-0.11.1 (c (n "redmaple") (v "0.11.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0i8348k91b3qp96wzih2j1vvid4m59zcrjg0hd56ybjgb21dldd1")))

(define-public crate-redmaple-0.12.0 (c (n "redmaple") (v "0.12.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "07shhjhkfvxsrzzw15dp1xik4slyrbgliampnw960nd86wldxvla")))

(define-public crate-redmaple-0.12.1 (c (n "redmaple") (v "0.12.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "06arh6w6la1rshx3kg6d893xlvwq30z5pyc3inw4z1071x2hm2xp")))

(define-public crate-redmaple-0.12.2 (c (n "redmaple") (v "0.12.2") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1i6ngs6h9gn51lxn96vy6w2sg8gr37rhwnry9vrrhnf13gmljadq")))

(define-public crate-redmaple-0.12.3 (c (n "redmaple") (v "0.12.3") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ip15clzk97svpj1w9c02nrfavr1i0gr7xrzsn3lvggyg3r0nk1l")))

(define-public crate-redmaple-0.13.0 (c (n "redmaple") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "09fh1v6zjabk1y3n3viyyarvnlkjv2ml0rq9rimx0m3b2fw67hwj")))

(define-public crate-redmaple-0.13.1 (c (n "redmaple") (v "0.13.1") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pz1mc43cjl3w32r7c4c8bpz1vi22laa96k149gcpacwxkkf01x8")))

(define-public crate-redmaple-0.14.0 (c (n "redmaple") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "19271824hi70ws6m73bn8888w776k4sy1by586hrdzwwz4gmcsnk")))

(define-public crate-redmaple-0.14.1 (c (n "redmaple") (v "0.14.1") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "10aqgzipk4ydhxg64sbhj41fqmbaqp42r8dsbdd3i8d4vphw3h11")))

(define-public crate-redmaple-0.14.2 (c (n "redmaple") (v "0.14.2") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "0xlra2phkmzglbjrl3snb0gj8cfikzxgszlr1q62jvr40pfw7qnn")))

(define-public crate-redmaple-0.15.0 (c (n "redmaple") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "165yimnpgqnk3mj4xf6891q6l9rz9980qc6hchgmw3m34idjjyiz")))

(define-public crate-redmaple-0.16.0 (c (n "redmaple") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "1g3iyv50anj0xlnnxzbsj2bxbxs1dq9l84sqi6xfkl6jrcfsbs26")))

(define-public crate-redmaple-0.16.1 (c (n "redmaple") (v "0.16.1") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "1sv2kawixaf7i1isr6drgppx8h9dqf6f0jkzadkg88j6jr60l1d1")))

(define-public crate-redmaple-0.17.0 (c (n "redmaple") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 2)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "1855b6hd9qljxnybdz6kgi5rh9gqc584x8lam7cddy3rqafwxb0n")))

(define-public crate-redmaple-0.18.0 (c (n "redmaple") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 2)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "16c6k1k81srm738cn4vzsi15r26gk9yph30aargjma1b8v0i3wha")))

(define-public crate-redmaple-0.18.1 (c (n "redmaple") (v "0.18.1") (d (list (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 2)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "105z349qpvyacj4x9ywzgs8142p2mn6nxlnk40zd7zshm15bzj8i")))

(define-public crate-redmaple-0.18.2 (c (n "redmaple") (v "0.18.2") (d (list (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 2)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "1wcq5hvw6x2c4cpbmp4mazjhs962793b605wv6h026whlq6jvx60")))

