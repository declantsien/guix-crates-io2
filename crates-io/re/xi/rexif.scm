(define-module (crates-io re xi rexif) #:use-module (crates-io))

(define-public crate-rexif-0.2.0 (c (n "rexif") (v "0.2.0") (h "0y01s75m8d36s5j3swrwh967zvzql1a2mn8an5zzl286nh477s72") (y #t)))

(define-public crate-rexif-0.3.0 (c (n "rexif") (v "0.3.0") (h "1cvl4qvp3b5kvaa1nd7a13pxw9c01g98f5z9s13kiyy2p6h8x8gk") (y #t)))

(define-public crate-rexif-0.3.1 (c (n "rexif") (v "0.3.1") (h "0mh7p7gkfx45f5vrrk8qcin5awr15xzfz3zyrqhxgrx61lg975z4") (y #t)))

(define-public crate-rexif-0.3.2 (c (n "rexif") (v "0.3.2") (h "143lldpmrx5c3fx3jifpk67syl11hy8ayb4kbr9vhw2kr5pp5894") (y #t)))

(define-public crate-rexif-0.3.3 (c (n "rexif") (v "0.3.3") (h "0sja0xm3bkwmxcpgvjfpd8wjr0j6pl2wq8y0ysb2ag2zxklrl0wy") (y #t)))

(define-public crate-rexif-0.3.4 (c (n "rexif") (v "0.3.4") (h "1rk5m79palxm3n49wsk65ganqjh33z4jfhbb5w5rg1ihqq18acyf") (y #t)))

(define-public crate-rexif-0.3.5 (c (n "rexif") (v "0.3.5") (h "0np0133z1wwshgpqaywq3z0qd61fymq10xqjilhyrvi4rnsadhnm") (y #t)))

(define-public crate-rexif-0.3.6 (c (n "rexif") (v "0.3.6") (h "1ckh82h3khlsi7i37yd51f0zhybk76b5xcxglflwrzpw1dwi2hcw") (y #t)))

(define-public crate-rexif-0.3.7 (c (n "rexif") (v "0.3.7") (h "1whnq3x8rf4476jfwikfsfg0qkclj4j7jia7635fl758qdk3i3wm")))

(define-public crate-rexif-0.4.0 (c (n "rexif") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0vnx4gn0827fy071hnd0rsrx6sl1bdwpqhwbc5zh29x95gnvf1ps")))

(define-public crate-rexif-0.4.1 (c (n "rexif") (v "0.4.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1rrrw7cljxdc62m5j6xkx51bnsqh8a73mj58ll5nyfzm10wb9d5v")))

(define-public crate-rexif-0.5.0 (c (n "rexif") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1pf1yrxhf1smg47vmkbmpkb07f8wsfw71zclq1hbmmch1bp431di")))

(define-public crate-rexif-0.5.1 (c (n "rexif") (v "0.5.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "0m617fvrqysz7mjcin99mw31b5zfw85nr0d1f9l84hknqlw6brwm")))

(define-public crate-rexif-0.5.2 (c (n "rexif") (v "0.5.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "13rhc32sjg6sx6y4rciwfvrwsj78y19bm2a7m9ibla85gxxkmix7")))

(define-public crate-rexif-0.5.3 (c (n "rexif") (v "0.5.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0zzzmj0lyvxs1z7w8v5nzm9allvbsal738gp2sb1cb9f7k1fb0n9")))

(define-public crate-rexif-0.7.0 (c (n "rexif") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1bbbvq0p475ry16b52clc74yhgj3n9jnwv73apgdb1fnn8fv7mz6") (y #t)))

(define-public crate-rexif-0.7.1 (c (n "rexif") (v "0.7.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0y89bday7sfcb44ca53b6kdvlyllk32b74cpa398krvxdd3994bi")))

(define-public crate-rexif-0.7.3 (c (n "rexif") (v "0.7.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "10qlras7zzxw0apvjn5ccwy7353lvykc5a6phn8ay8h5nxjjjda9")))

