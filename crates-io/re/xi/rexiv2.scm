(define-module (crates-io re xi rexiv2) #:use-module (crates-io))

(define-public crate-rexiv2-0.1.0-pre (c (n "rexiv2") (v "0.1.0-pre") (h "169csq3s3jan21n0mfbqb0qp4111i0i0gvzvbl7q8q1xzmrpivdf")))

(define-public crate-rexiv2-0.1.0 (c (n "rexiv2") (v "0.1.0") (h "1i2szjqvs9m53myj3fh2k1lpn1lsafpzwflfvf456dd7gnk3z28j")))

(define-public crate-rexiv2-0.2.0 (c (n "rexiv2") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "030fj69fghc72yfrmwnpryj4bnva28s42kidaxgxmm69sq06lnn8")))

(define-public crate-rexiv2-0.2.1 (c (n "rexiv2") (v "0.2.1") (d (list (d (n "gexiv2-sys") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 2)))) (h "1xwmiazmsaq0g1i0iiwh4a8gi1wxfdhwllwi62k52yinqpn1y7m5")))

(define-public crate-rexiv2-0.2.2 (c (n "rexiv2") (v "0.2.2") (d (list (d (n "gexiv2-sys") (r "> 0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0d8j75h7cyr7wnycy9syiq72w6xi3i6478pbw1k7d99grhfd0bsv")))

(define-public crate-rexiv2-0.2.3 (c (n "rexiv2") (v "0.2.3") (d (list (d (n "gexiv2-sys") (r "> 0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1krwm83ba5zw1ql1sq71yaj6whz6z4miq0ay66b5141g7l0s9jlp")))

(define-public crate-rexiv2-0.3.0 (c (n "rexiv2") (v "0.3.0") (d (list (d (n "gexiv2-sys") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1ybz5h74aklcpbda83107rr954mp1qx76vvrszav3448107zkkil")))

(define-public crate-rexiv2-0.3.1 (c (n "rexiv2") (v "0.3.1") (d (list (d (n "gexiv2-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0jda1z77gvlbvfrqd0z5rm8nxfdvcr3nmivxh4y5fwbamznfqid5")))

(define-public crate-rexiv2-0.3.2 (c (n "rexiv2") (v "0.3.2") (d (list (d (n "gexiv2-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0cpwqn63nhpjfn458szma0v74z4nxxcq8sxm1i39l0f63nh6ajxk")))

(define-public crate-rexiv2-0.3.3 (c (n "rexiv2") (v "0.3.3") (d (list (d (n "gexiv2-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "18ls7qa80vds7yi52lv7ajcqwq6y4gpc4q17vf4x65q3998cvgrz")))

(define-public crate-rexiv2-0.4.0 (c (n "rexiv2") (v "0.4.0") (d (list (d (n "gexiv2-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0q729wfyhzisaxhbjrwrpchi305n9w6hn21rh3kv5firiwzdhrw8")))

(define-public crate-rexiv2-0.4.1 (c (n "rexiv2") (v "0.4.1") (d (list (d (n "gexiv2-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0pq9fqzpnd2dcfv691rg57li7cqb0l1fd3s5nhj6jjm43982l0fk")))

(define-public crate-rexiv2-0.4.2 (c (n "rexiv2") (v "0.4.2") (d (list (d (n "gexiv2-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (k 0)))) (h "00dd8ilqjqajg8kx1hrfaia38qziy1hxld4ip8vwwk7i1gk5dll3")))

(define-public crate-rexiv2-0.4.3 (c (n "rexiv2") (v "0.4.3") (d (list (d (n "gexiv2-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "= 0.1.36") (k 0)) (d (n "num-traits") (r "= 0.1.37") (k 0)))) (h "1gziqcqgg5xrcclhnswy7mb6dzf0k57cd44q0lx7clp33i8ka8mm")))

(define-public crate-rexiv2-0.5.0 (c (n "rexiv2") (v "0.5.0") (d (list (d (n "gexiv2-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (k 0)))) (h "1gmmq8jr2m0bb2q4q004xv9pmq4236zpkshldnnpz09anw9ij6yg")))

(define-public crate-rexiv2-0.6.0 (c (n "rexiv2") (v "0.6.0") (d (list (d (n "gexiv2-sys") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (k 0)))) (h "110jynvx6s8a45lwgpm422jl65ny9csyd0pfy0nkzpyb46hd8brk")))

(define-public crate-rexiv2-0.7.0 (c (n "rexiv2") (v "0.7.0") (d (list (d (n "gexiv2-sys") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (k 0)))) (h "1x2yk0pywvaqy3zqjd38hfpy930waqkm3iwq3rs6m4x66vbzldxx")))

(define-public crate-rexiv2-0.8.0 (c (n "rexiv2") (v "0.8.0") (d (list (d (n "gexiv2-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (k 0)))) (h "019zb761shq5gzw9v20xr0v26lrqm8b254ag4h80ssn7m7jg6jk9") (f (quote (("raw-tag-access" "gexiv2-sys/raw-tag-access" "glib-sys"))))))

(define-public crate-rexiv2-0.9.0 (c (n "rexiv2") (v "0.9.0") (d (list (d (n "gexiv2-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (k 0)))) (h "1a6gg1dcky4f4v7zv2g1r3ynip4jhcrv6z28fclw0pmhbaxk89cq") (f (quote (("raw-tag-access" "gexiv2-sys/raw-tag-access" "glib-sys"))))))

(define-public crate-rexiv2-0.9.1 (c (n "rexiv2") (v "0.9.1") (d (list (d (n "gexiv2-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (k 0)))) (h "1996k2qb1zx6qs76xkw2ybcz8413qph0inpyk8phryfw6irk0d5b") (f (quote (("raw-tag-access" "gexiv2-sys/raw-tag-access" "glib-sys"))))))

(define-public crate-rexiv2-0.10.0 (c (n "rexiv2") (v "0.10.0") (d (list (d (n "gexiv2-sys") (r "^1.4") (d #t) (k 0)) (d (n "glib-sys") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (k 0)))) (h "11zv4vpdf936vn5gf05am2z4ci316j7bp9wfyldylgylrbfbkv0i") (f (quote (("raw-tag-access" "gexiv2-sys/raw-tag-access" "glib-sys")))) (r "1.63")))

