(define-module (crates-io re co record-screen) #:use-module (crates-io))

(define-public crate-record-screen-0.3.0 (c (n "record-screen") (v "0.3.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "env-libvpx-sys") (r "^4.0.9") (d #t) (k 0)) (d (n "quest") (r "^0.3") (d #t) (k 0)) (d (n "scrap") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vpx-encode") (r "^0.3") (f (quote ("vp9"))) (d #t) (k 0)) (d (n "webm") (r "^1") (d #t) (k 0)))) (h "09mkaq8cjjampc05pblsy5bip9vpyh9cfql0dpv6i4gbxcy3fdqw")))

(define-public crate-record-screen-0.4.0 (c (n "record-screen") (v "0.4.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "env-libvpx-sys") (r "^5") (d #t) (k 0)) (d (n "quest") (r "^0.3") (d #t) (k 0)) (d (n "scrap") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vpx-encode") (r "^0.5") (f (quote ("vp9"))) (d #t) (k 0)) (d (n "webm") (r "^1") (d #t) (k 0)))) (h "1qayfw8b6qanmwdik985y0vxyi8v51kzvziwiiynicj9h8dik3v1")))

