(define-module (crates-io re co recordkeeper) #:use-module (crates-io))

(define-public crate-recordkeeper-0.1.0 (c (n "recordkeeper") (v "0.1.0") (h "0ijqsiz09mmavmgm778zq2zijw8apz7gsj14367mbyvapn7wf7a6")))

(define-public crate-recordkeeper-0.1.1 (c (n "recordkeeper") (v "0.1.1") (h "168kfsc4zh9m0b1ga68qasiv3s0508bx7hbpfxfwm3hmzrfs116j")))

(define-public crate-recordkeeper-0.2.0-alpha (c (n "recordkeeper") (v "0.2.0-alpha") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "recordkeeper-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1bzgd8flj8bb6av3wan095f3hawzvlyaz0d62vz41ry3dac56xj4") (f (quote (("map-bitmaps")))) (s 2) (e (quote (("strum" "dep:strum"))))))

