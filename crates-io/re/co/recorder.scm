(define-module (crates-io re co recorder) #:use-module (crates-io))

(define-public crate-recorder-0.1.0 (c (n "recorder") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq0lg0449sl4045r52jrlw3wswlyqqimmck7slmls494h50fzsh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-recorder-0.1.1 (c (n "recorder") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhy6ajm34hll8zrbl6hgai959kmxrcaagq209iwnp0a8qzjba8s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-recorder-0.2.1 (c (n "recorder") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlgb6py7lpdpabxfms1jn7wjjvh23cnb6nllrbkzg1i4j64gc2j") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

