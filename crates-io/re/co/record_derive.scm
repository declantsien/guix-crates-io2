(define-module (crates-io re co record_derive) #:use-module (crates-io))

(define-public crate-record_derive-0.1.0 (c (n "record_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1dxawq977c4h20lqriqfsv79zvrkfz0i4fk66arh1zrdx2kkx967")))

(define-public crate-record_derive-0.2.0 (c (n "record_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0gaz2dnw5rp1ppv883jdsp12y3wd0i8kn9sbsp45xlyv5xp8g9d9")))

(define-public crate-record_derive-0.2.1 (c (n "record_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1hhrr3rnfcd7rqj07ry987fpkwa8l2bck9lncmh3f3xzfrjmigvs")))

(define-public crate-record_derive-0.2.2 (c (n "record_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1jprqm8h8alvq6x5d08b494i4rafpf4j91divgk90lxsgawmq2yv")))

(define-public crate-record_derive-0.2.3 (c (n "record_derive") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0hkbc8fdv35g0c6g4f4r3p2gzjawjxzqlkhcy5mz6h2lv8mm93np")))

(define-public crate-record_derive-0.2.5 (c (n "record_derive") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1ci6i2b6c1s4fsw1394a8y957ba44z4c4zimpg17lh4g19d8ggdz")))

(define-public crate-record_derive-0.2.6 (c (n "record_derive") (v "0.2.6") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0rxcldc3nin2p74ajxdhnsvp3dnj79mjfbw64qnspx97qfrkhjd8")))

