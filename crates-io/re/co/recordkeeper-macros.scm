(define-module (crates-io re co recordkeeper-macros) #:use-module (crates-io))

(define-public crate-recordkeeper-macros-0.1.0 (c (n "recordkeeper-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1in0lmshz9g1dmywsgxra81h71daf3jhl28nv6d16ad1zc3k8bjk")))

