(define-module (crates-io re co recode_rs) #:use-module (crates-io))

(define-public crate-recode_rs-1.0.3 (c (n "recode_rs") (v "1.0.3") (d (list (d (n "encoding_rs") (r "^0.7.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "092l4bvzi9ccxs5w1mg2y1dqzb7dgxy3c429vnxjb29cs477zxpn") (f (quote (("simd-accel" "encoding_rs/simd-accel") ("no-static-ideograph-encoder-tables" "encoding_rs/no-static-ideograph-encoder-tables"))))))

(define-public crate-recode_rs-1.0.4 (c (n "recode_rs") (v "1.0.4") (d (list (d (n "encoding_rs") (r "^0.7.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0pgq04z959q10n977h5iflhcrcxrqfk6p3d3s659xcw8lpy57v5h") (f (quote (("simd-accel" "encoding_rs/simd-accel") ("no-static-ideograph-encoder-tables" "encoding_rs/no-static-ideograph-encoder-tables"))))))

(define-public crate-recode_rs-1.0.5 (c (n "recode_rs") (v "1.0.5") (d (list (d (n "encoding_rs") (r "^0.8.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1v7xp7ly58f03hsni7l94j4palbakimay3vdv6s939g7jwq423y5") (f (quote (("simd-accel" "encoding_rs/simd-accel"))))))

(define-public crate-recode_rs-1.0.6 (c (n "recode_rs") (v "1.0.6") (d (list (d (n "encoding_rs") (r "^0.8.11") (f (quote ("fast-legacy-encode"))) (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "17ykwhlmdx0s2d7yqjqcm8pa924ia5r8c0rp3m2s0rjmfakrgp4x") (f (quote (("simd-accel" "encoding_rs/simd-accel"))))))

