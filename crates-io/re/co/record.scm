(define-module (crates-io re co record) #:use-module (crates-io))

(define-public crate-record-0.1.0 (c (n "record") (v "0.1.0") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0l8b96y5fl8nfllwpva7kvlddfm41ngqxj9wsvwzjfs8pn4zw89d")))

(define-public crate-record-0.1.1 (c (n "record") (v "0.1.1") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1rb8svibppsyg0nwbl348ln0llqrfdrbslzy1mncwkrz62wg1588")))

(define-public crate-record-0.2.0 (c (n "record") (v "0.2.0") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0vfqz4k4vsc3v5rrb93mwchz09mmjn5ppfnp8hrf9j2zl2gqa6cq")))

(define-public crate-record-0.2.1 (c (n "record") (v "0.2.1") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "16ky9y2610jdz18a2lr7nln73j7714j1nxcsddkk8wyx88qb7bc4")))

(define-public crate-record-0.2.2 (c (n "record") (v "0.2.2") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0sci0l403g0k7g10w225879ax5s4dzwb0rdwzy3y248sbqq416ir")))

(define-public crate-record-0.2.3 (c (n "record") (v "0.2.3") (d (list (d (n "clap") (r "^2.16.1") (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "pty-shell") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "05hj3qbnxnk5yjbn3cv88aw8k4jgdkc02g6xihami6pp53d6kzh0")))

