(define-module (crates-io re co records) #:use-module (crates-io))

(define-public crate-records-0.1.0 (c (n "records") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "029lzhhjj1njfrp1125g5k7dml9n6zr27l0brwnkpfc9439y07g4")))

(define-public crate-records-0.1.1 (c (n "records") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxdspwnvbm6ms76x7rhv9avqy8x5y7x4rxak5bgcws32gcv94lp")))

(define-public crate-records-0.2.0 (c (n "records") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aw90gvphk1gskn8j80la3qqchm9gpq92jj209737zy5hgi35j09")))

