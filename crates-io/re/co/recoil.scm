(define-module (crates-io re co recoil) #:use-module (crates-io))

(define-public crate-recoil-0.1.0 (c (n "recoil") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "axum") (r "^0.6.12") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1xpq165flblp73m867srxrf6hi2mzbqm7sg8p1kh6wgyk2rbv67q")))

(define-public crate-recoil-1.0.0 (c (n "recoil") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "axum") (r "^0.6.12") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0hinflrfc7qp4czw2a04mdlblj01pnyhlbv2qkynkxbdwykjna86")))

