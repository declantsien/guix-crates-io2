(define-module (crates-io re co recode-util) #:use-module (crates-io))

(define-public crate-recode-util-0.1.0 (c (n "recode-util") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "recode") (r "^0.5.0-alpha.3") (d #t) (k 0)) (d (n "tokio-util") (r "^0") (o #t) (d #t) (k 0)))) (h "1pds9qcknryyhc33rnydhkd5mvazk9iggrclyzlqwlvg7625whd0") (f (quote (("framed" "tokio-util/codec") ("default" "framed"))))))

