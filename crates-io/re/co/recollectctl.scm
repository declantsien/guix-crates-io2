(define-module (crates-io re co recollectctl) #:use-module (crates-io))

(define-public crate-recollectctl-0.1.0 (c (n "recollectctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "librecollect") (r "^0.1.0") (d #t) (k 0)))) (h "1hxciq5bdycl6hf0lv1a0xq9790rpq2c44m0rkizlj22i5sj0lgj")))

(define-public crate-recollectctl-0.2.0 (c (n "recollectctl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "librecollect") (r "^0.2.0") (d #t) (k 0)))) (h "05lwh6jwynbh2jbyzjd40jaf71j93i7gfrpsfn74qh5cz060bydn")))

(define-public crate-recollectctl-0.3.0 (c (n "recollectctl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "librecollect") (r "^0.3.0") (d #t) (k 0)))) (h "0ijqghwh2x98jp9m3sl2sv0wgvq74vissrr86hck2l1y0dnvs0xz")))

(define-public crate-recollectctl-0.3.1 (c (n "recollectctl") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "librecollect") (r "^0.3.0") (d #t) (k 0)))) (h "0x6k1azk294qqn7id1izxc4p1a6vcs3lm90grsqhpydyyqrnhny9")))

(define-public crate-recollectctl-0.3.2 (c (n "recollectctl") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "librecollect") (r "^0.3") (d #t) (k 0)))) (h "1wjm0hag4bli4dps25xp8lgaq4ay3yzmg04pk002h8hfnis6jh15")))

