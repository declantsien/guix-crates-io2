(define-module (crates-io re co recordbox) #:use-module (crates-io))

(define-public crate-recordbox-0.1.0 (c (n "recordbox") (v "0.1.0") (d (list (d (n "crypto_api_chachapoly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1n8c6lj5pma68j4s3z386sk7nm4g4cdqqdmdlz8lbs7bh1yf2khg") (f (quote (("impl_chachapoly" "crypto_api_chachapoly") ("default" "impl_chachapoly")))) (y #t)))

(define-public crate-recordbox-0.1.1 (c (n "recordbox") (v "0.1.1") (d (list (d (n "crypto_api_chachapoly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "crypto_api_osrandom") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ebacktrace") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1wdqwk9mdx2clb0zbg7san6jszvs3fd2bd6c53cfp3mdlwm7xjmz") (f (quote (("random" "crypto_api_osrandom") ("impl_chachapoly" "crypto_api_chachapoly") ("default" "random" "impl_chachapoly")))) (y #t)))

