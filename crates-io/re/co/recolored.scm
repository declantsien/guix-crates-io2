(define-module (crates-io re co recolored) #:use-module (crates-io))

(define-public crate-recolored-1.9.3 (c (n "recolored") (v "1.9.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "13cnnyhq9mm6y54mmr41gk290fi65nkb6svnkwi8cxm8v0nwk10m") (f (quote (("no-color"))))))

