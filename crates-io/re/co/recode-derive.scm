(define-module (crates-io re co recode-derive) #:use-module (crates-io))

(define-public crate-recode-derive-0.1.0 (c (n "recode-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07mhcynjki0j7nqki0lgqrwd6nxrs4gwmgvh08ymankm0iim91p5")))

(define-public crate-recode-derive-0.2.0 (c (n "recode-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0br7wxm8yr68fx3xqk3x8sfgg70k6089g1ywc2kz76vd91m9asw7")))

(define-public crate-recode-derive-0.2.1 (c (n "recode-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rsday4am09aqgwmgiv4vhs052cd9rk2qlzkr2hw8qlyvxc73k24")))

(define-public crate-recode-derive-0.2.2 (c (n "recode-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnb1ni6ac5bpndnn0smkl9s0qbp746p2vlqacw7dghnlz4payvj")))

(define-public crate-recode-derive-0.2.3 (c (n "recode-derive") (v "0.2.3") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13774l5bq7pswvlxri2c307wkrdl2lcrpgkahsmgqvl0f9xvzb6w")))

(define-public crate-recode-derive-0.3.0 (c (n "recode-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0whzxw6n62qv4bmclqfbhhsznqiycch99fbwxwyghvpa65rhkwg7")))

(define-public crate-recode-derive-0.4.0 (c (n "recode-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11jxcqa31vn4z4ipa6l1nq3b03h7y2ifhp9gzqsyxq2sr82mh871")))

(define-public crate-recode-derive-0.4.1 (c (n "recode-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h7fq33fl7bdy37m6vpwlv1q4sxn8xzirdnbxzshcmr9nl9zxqr0")))

(define-public crate-recode-derive-0.5.0-alpha.1 (c (n "recode-derive") (v "0.5.0-alpha.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd23804aqjsj0fnav90qxw8sr9cg9s3zjhia1q4c4f0r400d6hk")))

