(define-module (crates-io re fm refmove) #:use-module (crates-io))

(define-public crate-refmove-0.1.0 (c (n "refmove") (v "0.1.0") (h "1fdcvz559n92180vbv6zy6rlcgmgb2g9zln9q9z04fi7wfxnn8jb") (f (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1.1 (c (n "refmove") (v "0.1.1") (h "0c9j4gcw4xb5yz34vaaqb72j132qqir3xz2sj98z9xd70skrdskv") (f (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1.2 (c (n "refmove") (v "0.1.2") (h "0vc152z04xmwjcdyqgarv3bsn2bnipymd4shdnjmafag628m7858") (f (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1.3 (c (n "refmove") (v "0.1.3") (h "1q0nb4lif2crxn6lmhf98ps9lb1ihsanmhkkffas940c2q0c3q5g") (f (quote (("std") ("default" "std"))))))

