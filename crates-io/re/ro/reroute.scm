(define-module (crates-io re ro reroute) #:use-module (crates-io))

(define-public crate-reroute-0.1.0 (c (n "reroute") (v "0.1.0") (d (list (d (n "hyper") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0fnk9r086h8bky6390im3xh4pdm0gnmmkvl769cydgipnh12vnyc")))

(define-public crate-reroute-0.1.1 (c (n "reroute") (v "0.1.1") (d (list (d (n "hyper") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "102y4xkdwzyv04ywjy2n153ir832bv2mhrn0wx0nzll5yf6xa8rq")))

(define-public crate-reroute-0.1.2 (c (n "reroute") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "10h0i9wmpgh45vmlnrzg3iwiiv8naa76lr1xwgamz641y9djvs78")))

(define-public crate-reroute-0.2.0 (c (n "reroute") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "09p166imxcdahrrj64fb310nvfg83jrykikm09ai2yin4ayynh05")))

(define-public crate-reroute-0.2.1 (c (n "reroute") (v "0.2.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0rkz5qsvgfcsap9rmvhm0rliavlx5phyvvbhxifby5szfhdl18rd")))

(define-public crate-reroute-0.2.2 (c (n "reroute") (v "0.2.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0m03sjpab6bvdxrhybs6xm4c5cnqq7lpzx6f7zi81flr2qf3dri1")))

(define-public crate-reroute-0.2.3 (c (n "reroute") (v "0.2.3") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0cwm6zn5xkldz8b3xspn3j4q3gqxcwbrha0w7k3ia7dawi9kyww4")))

(define-public crate-reroute-0.2.4 (c (n "reroute") (v "0.2.4") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0bijsxrp6ksz5c04x6r1r0i4wbp18377bv0hq39riisgg585kzfy") (f (quote (("ssl" "hyper/ssl") ("serde-serialization" "hyper/serde-serialization") ("default" "ssl"))))))

(define-public crate-reroute-0.3.0 (c (n "reroute") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "regex") (r "^0.1.59") (d #t) (k 0)))) (h "0jbrkrchvfhg0aiscamdvyzlz4lyf359088k5rnr4wvfaranb87r") (f (quote (("ssl" "hyper/ssl") ("serde-serialization" "hyper/serde-serialization") ("default" "ssl"))))))

(define-public crate-reroute-0.3.1 (c (n "reroute") (v "0.3.1") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17w6c58iqsq0dlm5g0kkl447rlxc191pzvra9klg6lw9yk1vh7w4") (f (quote (("ssl" "hyper/ssl") ("serde-serialization" "hyper/serde-serialization") ("default" "ssl"))))))

(define-public crate-reroute-0.3.2 (c (n "reroute") (v "0.3.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zddjy4g6l2pxilgcn8jdzy72prqvi7z5inlvvj7pwxsq884vrv0")))

(define-public crate-reroute-0.4.0 (c (n "reroute") (v "0.4.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0w8k9lf4p7a062scgz8rmcc51dnyy0fjw6k12x43hldrvarz21fb")))

(define-public crate-reroute-0.4.1 (c (n "reroute") (v "0.4.1") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14vi176n5pl7yc2jn58ki3n63c8fpdaig3hfn1szj0arxbaarrfz")))

