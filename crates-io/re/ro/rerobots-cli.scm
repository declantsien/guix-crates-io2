(define-module (crates-io re ro rerobots-cli) #:use-module (crates-io))

(define-public crate-rerobots-cli-0.11.3 (c (n "rerobots-cli") (v "0.11.3") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("color"))) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "insta") (r "^1.16.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rerobots") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0bcj6p030irrzvxrgp3583n93752chjwmrilskkvd280a86a0z4d")))

