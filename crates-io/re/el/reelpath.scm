(define-module (crates-io re el reelpath) #:use-module (crates-io))

(define-public crate-reelpath-1.0.0 (c (n "reelpath") (v "1.0.0") (h "14aww8c2zvralfn41n91qfzmwk7g4xfsxwsmkkrs83gaw1bsq9y5")))

(define-public crate-reelpath-1.0.1 (c (n "reelpath") (v "1.0.1") (h "1aqk5zpsqqvjh92crfq6py0f6cd0k3w163a9f9f16yx3br1j5i0g")))

(define-public crate-reelpath-1.0.2 (c (n "reelpath") (v "1.0.2") (h "1dp7x751nd60kxqgbqnsn3m9cfvq3iqq6qxjhyg6w6vygckqcknr")))

(define-public crate-reelpath-1.1.0 (c (n "reelpath") (v "1.1.0") (h "1ksw7g3y3hjfcnl7l0m5vpgi2f4nlfdfxp9jwl3hyq56dhw9jlxb")))

(define-public crate-reelpath-1.2.0 (c (n "reelpath") (v "1.2.0") (h "0d6xa265ik6ngs5bklbjqw9wdagv1fzbxd7bj0zkc2bqfmpxjjzj")))

(define-public crate-reelpath-1.2.1 (c (n "reelpath") (v "1.2.1") (h "0mjqa9pmgl318ydyw847vmvqszh7lpnl86ajcp1a0pxjf5xlrsfr")))

(define-public crate-reelpath-1.2.2 (c (n "reelpath") (v "1.2.2") (h "13rd8263ymj718m1zbk5wq2c7fs1a27jqdklbyv6kk1jzk0zp7bq")))

