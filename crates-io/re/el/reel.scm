(define-module (crates-io re el reel) #:use-module (crates-io))

(define-public crate-reel-0.0.0 (c (n "reel") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "reef") (r "^0.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0ggg8xvx5559vrg1gi78sjbaz300jgjjxgllp4m4qji0hz7a412l")))

