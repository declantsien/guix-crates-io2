(define-module (crates-io re _f re_format_arrow) #:use-module (crates-io))

(define-public crate-re_format_arrow-0.16.0-alpha.3 (c (n "re_format_arrow") (v "0.16.0-alpha.3") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-alpha.3") (k 0)) (d (n "re_types_core") (r "=0.16.0-alpha.3") (k 0)))) (h "0grk093lw0dq1qfvdhhhd74wvjz1n92rx04fz2pmrrzws8qddyq9") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0-alpha.4 (c (n "re_format_arrow") (v "0.16.0-alpha.4") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-alpha.4") (k 0)) (d (n "re_types_core") (r "=0.16.0-alpha.4") (k 0)))) (h "1rklxwqb0fg1a5qk1ydsy28qk3acck6dxn4cq61q3w3qyivlj94f") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0-rc.1 (c (n "re_format_arrow") (v "0.16.0-rc.1") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-rc.1") (k 0)) (d (n "re_types_core") (r "=0.16.0-rc.1") (k 0)))) (h "0yw1k9cvfcdyinlh4j7v2lzqi3q9v0xz0axmqiyz3clxsbdsd6sj") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0-rc.2 (c (n "re_format_arrow") (v "0.16.0-rc.2") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-rc.2") (k 0)) (d (n "re_types_core") (r "=0.16.0-rc.2") (k 0)))) (h "10gbag3k34x17hjy1jlsk8d979vbpbq4jx6vz88sq6wxjdamqi1l") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0-rc.3 (c (n "re_format_arrow") (v "0.16.0-rc.3") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-rc.3") (k 0)) (d (n "re_types_core") (r "=0.16.0-rc.3") (k 0)))) (h "18b0ry9rbbhdf2zfflwskx2gbxzzvbv0wps7y4x02q5pia995gkw") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0-rc.4 (c (n "re_format_arrow") (v "0.16.0-rc.4") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.0-rc.4") (k 0)) (d (n "re_types_core") (r "=0.16.0-rc.4") (k 0)))) (h "0kswbl7nxks2x2j8l8b0hbyzm9s5cpgrx829n54dx5hvlrwsyck8") (r "1.76")))

(define-public crate-re_format_arrow-0.16.0 (c (n "re_format_arrow") (v "0.16.0") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "^0.16.0") (k 0)) (d (n "re_types_core") (r "^0.16.0") (k 0)))) (h "0j70dca4x0ra1rkxsgxmdfwxglkakivl3shrwss8s20dhhzyhbhk") (r "1.76")))

(define-public crate-re_format_arrow-0.17.0-alpha.2 (c (n "re_format_arrow") (v "0.17.0-alpha.2") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.17.0-alpha.2") (k 0)) (d (n "re_types_core") (r "=0.17.0-alpha.2") (k 0)))) (h "1b575k39zzm3d1jmap8s1vs236z875vvay8586wi7yjvsy5rykv0") (r "1.76")))

(define-public crate-re_format_arrow-0.17.0-alpha.3 (c (n "re_format_arrow") (v "0.17.0-alpha.3") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.17.0-alpha.3") (k 0)) (d (n "re_types_core") (r "=0.17.0-alpha.3") (k 0)))) (h "037wwmmid3770bv3xlvmxcxldm00jl25c60xh7acpqqv5qmmd9xj") (r "1.76")))

(define-public crate-re_format_arrow-0.16.1-rc.1 (c (n "re_format_arrow") (v "0.16.1-rc.1") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.1-rc.1") (k 0)) (d (n "re_types_core") (r "=0.16.1-rc.1") (k 0)))) (h "048iq0r8db6z7g5d7phjml3bd9i71cfn4yipsdnvh2qc5v4mxq22") (r "1.76")))

(define-public crate-re_format_arrow-0.16.1-rc.2 (c (n "re_format_arrow") (v "0.16.1-rc.2") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "=0.16.1-rc.2") (k 0)) (d (n "re_types_core") (r "=0.16.1-rc.2") (k 0)))) (h "04arncx67in6f2ad07bma37yy7rczkniahvzcmq8y5y2q0szp7zd") (r "1.76")))

(define-public crate-re_format_arrow-0.16.1 (c (n "re_format_arrow") (v "0.16.1") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0) (p "re_arrow2")) (d (n "comfy-table") (r "^7.0") (k 0)) (d (n "re_tuid") (r "^0.16.1") (k 0)) (d (n "re_types_core") (r "^0.16.1") (k 0)))) (h "0mb96qdhw8xzqdr3xghg81x07iixs7z5qbq4gldcswr6v3h5fx7n") (r "1.76")))

