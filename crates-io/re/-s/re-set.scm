(define-module (crates-io re -s re-set) #:use-module (crates-io))

(define-public crate-re-set-0.0.0 (c (n "re-set") (v "0.0.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "1jykziymqjqv78px377q7rd9yfxaak2s5kshspgn50aa22k434qv")))

(define-public crate-re-set-0.0.2-alpha.0 (c (n "re-set") (v "0.0.2-alpha.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "1xr51q3lyapm71blhh73vl5lr23k52vqbrws8yz37nwfvb0liq4x")))

(define-public crate-re-set-0.0.3-alpha.0 (c (n "re-set") (v "0.0.3-alpha.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "0cq00c57k8bp9lhgximpycf00rb4dlw89x08nblxaxkf8fl0pxy1")))

(define-public crate-re-set-0.0.3 (c (n "re-set") (v "0.0.3") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "16hf6laxvqymbqd9jpnd4476x88wil2l64b7fmdqdk73mx5353vw")))

(define-public crate-re-set-0.1.0 (c (n "re-set") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "13r4vn2500d6aqywfk31wz7xn0glm3agkb0h6yj1sb37v1aa5hhr")))

(define-public crate-re-set-0.1.1 (c (n "re-set") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "1pcm5m0hwn4asxq9k17qdwwk3ygqfw87xpp451060lhlj5f42svl")))

(define-public crate-re-set-0.2.0 (c (n "re-set") (v "0.2.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "12vsl7l1wsnkcx0kz7d7iclymdykllf7md2nckw2fzkixd98sdkp")))

