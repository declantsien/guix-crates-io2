(define-module (crates-io re sr resrap) #:use-module (crates-io))

(define-public crate-resrap-0.1.0 (c (n "resrap") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0wzlimyvrpq141qbcn36n3pbbjvpd92cxhvaik1jg6k29412lkbs")))

(define-public crate-resrap-0.2.0 (c (n "resrap") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.2.0") (d #t) (k 0)))) (h "077zsj3nkfrs8jn5jrjq5shym2zlgg43qn3zp019rsxx16dawqz5")))

(define-public crate-resrap-0.3.0 (c (n "resrap") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.3.0") (d #t) (k 0)))) (h "01ack4hdnr53483jgbzssxd97ixssziz3ki6937ph47k5ljihacm")))

(define-public crate-resrap-0.4.0 (c (n "resrap") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.4.0") (d #t) (k 0)))) (h "16dcxnn4r541isdphg3l2swr78f0cbh9diwbmgmqpfagklw982cb")))

(define-public crate-resrap-0.5.0 (c (n "resrap") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "0yy0ikmfpqww4znirk51gh33vn1dh8d0z7v4s3xc37bga5h3pry7")))

(define-public crate-resrap-0.6.0 (c (n "resrap") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "05y06n61r638z6dgdpgvsyj1swvsbk3mbn4j1ag3dx6g49v82qi4")))

(define-public crate-resrap-0.8.0 (c (n "resrap") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "1d1r39ihgpdxx4anim4qqxadasmfq0vkm57v7yx8ws4wn5qgda2d")))

(define-public crate-resrap-0.9.0 (c (n "resrap") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "1b1j98lkdmsw3d3256waxzliqsqrc5a9h3hhk0sajrm5kswwli75")))

(define-public crate-resrap-0.10.0 (c (n "resrap") (v "0.10.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "0d9nhz59y4did6q1hlbpzglq6cm4pxykdsimbypvmn77hxw4m04a")))

(define-public crate-resrap-0.11.0 (c (n "resrap") (v "0.11.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.5.0") (d #t) (k 0)))) (h "0p0v8qsq704ln1j5r5xs2y3i3phial1y3lqybyq2pyvsabfmmiqv")))

(define-public crate-resrap-0.12.0 (c (n "resrap") (v "0.12.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.6.0") (d #t) (k 0)))) (h "1s7m376pn6whhih77s3i9i7xljfcgb21krczdq9ms0ihpnhm6g4f")))

(define-public crate-resrap-0.13.0 (c (n "resrap") (v "0.13.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.6.0") (d #t) (k 0)))) (h "0p7jm5l25di3spg88cjxllcrfz0h5n85837vjivh8m4hh105jjmk")))

(define-public crate-resrap-0.14.0 (c (n "resrap") (v "0.14.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.7.0") (d #t) (k 0)))) (h "18grmnz964k88fp8vj7iffdl8jcz3gqqxr0g4vz94mqwimjhqi4l")))

(define-public crate-resrap-0.15.0 (c (n "resrap") (v "0.15.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.8.0") (d #t) (k 0)))) (h "17syb9vdm63qqfnghvlpaic7wpr56z6srwldy4lfykmz6ci4k5bp")))

(define-public crate-resrap-0.16.0 (c (n "resrap") (v "0.16.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.8.0") (d #t) (k 0)))) (h "1dhca2wgzm3i76q4588qf8jlis3lfalmcjmdkkw9dlbzimcfg5r9")))

(define-public crate-resrap-0.17.0 (c (n "resrap") (v "0.17.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "resrap_proc_macro") (r "^0.8.0") (d #t) (k 0)))) (h "138hc5iz2ry33mgnd265716ppg6qfzlmsapaaf647f2pcr2r7i2i")))

