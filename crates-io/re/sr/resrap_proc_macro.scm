(define-module (crates-io re sr resrap_proc_macro) #:use-module (crates-io))

(define-public crate-resrap_proc_macro-0.1.0 (c (n "resrap_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3fgj5dclrghzrfhm121v0sjnmg0rs6rf6bmp0ss4ki9qddj11l")))

(define-public crate-resrap_proc_macro-0.2.0 (c (n "resrap_proc_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1qs9s8d8n5r4yf7n4cc8w775iacahvar3qrr73xqdh6klx6fyaj1")))

(define-public crate-resrap_proc_macro-0.3.0 (c (n "resrap_proc_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdia1qr59sjygbm9iqykrb31mmbsbgg09vsk1d37k83k4kb738s")))

(define-public crate-resrap_proc_macro-0.4.0 (c (n "resrap_proc_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0nywsn69l0r2aj9knxrqf45kfvra2j8dpkq0fy684p2qz0ynbgs0")))

(define-public crate-resrap_proc_macro-0.5.0 (c (n "resrap_proc_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvk24qxxy2rg7rp01rjf00d469nsb4h6h82bawn3qrzpy95xwhi")))

(define-public crate-resrap_proc_macro-0.6.0 (c (n "resrap_proc_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1nas1n1b7wri1q9dphsj6z9xdnzrlaaysvh4l6q9r743kndfcg")))

(define-public crate-resrap_proc_macro-0.7.0 (c (n "resrap_proc_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1m9jx2jkhwrb624pqzhnn65nm6q8x0fsmgbwx26i4r36pkisrzpq")))

(define-public crate-resrap_proc_macro-0.8.0 (c (n "resrap_proc_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "14nwl56ss99y3w39cx316fwksgbz45kqv8idln8l2v14fnvv8jys")))

