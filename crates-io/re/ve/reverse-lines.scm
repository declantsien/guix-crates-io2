(define-module (crates-io re ve reverse-lines) #:use-module (crates-io))

(define-public crate-reverse-lines-0.1.0 (c (n "reverse-lines") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "1fnby04lxwh78fqy9p8kl3144x6wpzfw4pldv2hlccdzbh3c8xnk")))

(define-public crate-reverse-lines-0.1.1 (c (n "reverse-lines") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "14484zm64yzy4bb1mqg17vchfc768xyqws9bjwvqg6z188vd4qv3")))

