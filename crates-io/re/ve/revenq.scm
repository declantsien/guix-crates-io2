(define-module (crates-io re ve revenq) #:use-module (crates-io))

(define-public crate-revenq-0.0.0 (c (n "revenq") (v "0.0.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "triomphe") (r "^0.1") (d #t) (k 0)))) (h "1lrd89108rfllwa3blq8jj5g30mflaqgkfykb2ns9ljnl18xlw5d")))

(define-public crate-revenq-0.0.1 (c (n "revenq") (v "0.0.1") (h "12ffh41dpfg49dmw5swx98kgpafwyqr2jj23sywi2l1l8d0zhfs0")))

(define-public crate-revenq-0.0.2 (c (n "revenq") (v "0.0.2") (h "155mimb0llm8pcl96aq11qdia4sdsdnsx8cmvjvd9sxk43q6wz9v")))

(define-public crate-revenq-0.0.3 (c (n "revenq") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x3g072g9l3s1rrwj82wpl41clhjc09yqanl4a8qwrbbj2y3rwvf") (y #t)))

(define-public crate-revenq-0.0.4 (c (n "revenq") (v "0.0.4") (d (list (d (n "async-task") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "161wdl7ar6ly67lz14chf8mgbpy6rz7y53a02vnvcwnsc0xirmja") (f (quote (("woke-queue" "async-task" "crossbeam-utils" "futures-core" "pin-utils") ("default" "woke-queue")))) (y #t)))

(define-public crate-revenq-0.0.5 (c (n "revenq") (v "0.0.5") (d (list (d (n "async-task") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "1n9p4l46qfg9l7b5xlg742g72gvda688y8xkk2wdz938cb0jhrzc") (f (quote (("woke-queue" "async-task" "crossbeam-utils" "futures-core" "pin-utils") ("default" "woke-queue"))))))

(define-public crate-revenq-0.1.0 (c (n "revenq") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r "^2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (o #t) (k 0)))) (h "08r33rmv132gaj5x8dj5gzbzy8b1d6f0111zyyaqcsj28i7g1gn6")))

(define-public crate-revenq-0.1.1 (c (n "revenq") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r "^2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (o #t) (k 0)))) (h "0rkkqkjlq20dz2i0f7jfjywyw5srw88wi54wzp8112i2y2k4mxrh") (f (quote (("std") ("default"))))))

(define-public crate-revenq-0.2.0 (c (n "revenq") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r "^2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1") (o #t) (k 0)))) (h "0rjv2bki6c7bc2mh78izwp9zh8zs4w3qr0mr8mz8wqv9nbn4hv0k") (f (quote (("std") ("default"))))))

(define-public crate-revenq-0.3.0 (c (n "revenq") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r "^2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (k 0)))) (h "0cn48bz734727zl7h1i47vjgdsa9qlziwazpaiagpkkv9l4a71d4") (f (quote (("std") ("default"))))))

(define-public crate-revenq-0.4.0 (c (n "revenq") (v "0.4.0") (d (list (d (n "async-channel") (r "^1.5") (d #t) (k 2)) (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r "^2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (k 0)))) (h "1i6505h1bsp9r7xcg5p603v6y0zqaf1nbv2likcqc9w6nri5iar4") (f (quote (("default"))))))

(define-public crate-revenq-0.4.1 (c (n "revenq") (v "0.4.1") (d (list (d (n "async-channel") (r "^1.5") (d #t) (k 2)) (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "event-listener") (r ">=2.2, <2.6") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "once_cell") (r ">=1.4, <1.6") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1") (k 0)))) (h "1s97v8ka266zsq1lksmb3kch5n45n7g7wkj6gdlanrz53in5j4rd") (f (quote (("default"))))))

