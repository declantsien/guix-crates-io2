(define-module (crates-io re ve reversi) #:use-module (crates-io))

(define-public crate-reversi-0.1.0 (c (n "reversi") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "01wc6sgf605ccksj3nzn95vs56093c9afxli68dacg8r1qs1dsca")))

