(define-module (crates-io re ve revec) #:use-module (crates-io))

(define-public crate-revec-0.1.0 (c (n "revec") (v "0.1.0") (h "0sq59pka96cy2z7pc13yd74p263dwjmwvprbi2ccq9y374lmn8pp")))

(define-public crate-revec-0.2.0 (c (n "revec") (v "0.2.0") (h "0nzaj4zvyay459yqwk49vfb9v3cck2n71v83bx5bnk70hk3gwkgn")))

