(define-module (crates-io re ve reventsource) #:use-module (crates-io))

(define-public crate-reventsource-0.0.0 (c (n "reventsource") (v "0.0.0") (h "1zb1m59kir4wazxd7b7vcm1if4w8q4zyvz6gyc8b1k8qrgfa5sxm")))

(define-public crate-reventsource-0.0.1 (c (n "reventsource") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.9") (d #t) (k 2)))) (h "19idhx0q5cwql1h9r7xjnp8nd64xk6far5jdczqk81yxfl3fbck9")))

(define-public crate-reventsource-0.0.2 (c (n "reventsource") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.9") (d #t) (k 2)))) (h "0ps71k5isnrwm2byhf5cqdsiwqgwvb6c3j9xpf0v1wqp7c8nzc3x")))

