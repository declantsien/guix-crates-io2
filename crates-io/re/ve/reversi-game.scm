(define-module (crates-io re ve reversi-game) #:use-module (crates-io))

(define-public crate-reversi-game-0.3.3 (c (n "reversi-game") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)))) (h "02rw7v5hcj1h95g7xr2zbxpcrllfflwmfgywmjnphgm2ziabkbvx")))

