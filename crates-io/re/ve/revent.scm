(define-module (crates-io re ve revent) #:use-module (crates-io))

(define-public crate-revent-0.1.0 (c (n "revent") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0gxn7szd9w8gp4fk80v57wnl5bwpvg8h0r6z7kjpbsj058mdcrgm")))

(define-public crate-revent-0.2.0 (c (n "revent") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03a1vzwppr6sma755jjn9ai46kvqvd0xixkcsfvwcfxizpfa4xlv")))

(define-public crate-revent-0.3.0 (c (n "revent") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12k23gpny1csjyq69fm377y2pgn43wdxs1d5yq1fw9bqvcx5f6jj")))

(define-public crate-revent-0.4.0 (c (n "revent") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)))) (h "05vwln9rl9bvq9383mspmsai36k3mfbay5al6yvy6mimv0q3pw67")))

(define-public crate-revent-0.5.0 (c (n "revent") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)))) (h "1w9iscq1rlcianvb7s4lfmv5rgy8v2ydyqk62jzp7gcd7v3xi6w9")))

(define-public crate-revent-0.5.2 (c (n "revent") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1b1nclg02c338r924rgid8icvpwmdvhlyq7xm98lzvbzrz17pnw8")))

(define-public crate-revent-0.6.0-rc.1 (c (n "revent") (v "0.6.0-rc.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "123hb9nzqsvmhydmkj2y9j3f1n72q7a4a624qd8g70nph5d3wc2p")))

(define-public crate-revent-0.6.2 (c (n "revent") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "14rrghk6yx1j3n7qlj6n07z9yy6gqbv8schkdfqa6d0r274z5kr2")))

(define-public crate-revent-0.7.0-rc.1 (c (n "revent") (v "0.7.0-rc.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1p6nz25z50n33y88n6yfblymngby7lhhg6g6xpb9jjynr1c9691n")))

(define-public crate-revent-0.7.0 (c (n "revent") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "08jg5fapxjd0ja9af15g5rv5l7wnyqc9wvrdc90bp88p69cm4jsv")))

(define-public crate-revent-0.7.1 (c (n "revent") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "08k4mhkj2pdgdnq1jgfgrbkd4vv6fw8aljiqm9nzncnxdabk4gyc")))

(define-public crate-revent-0.8.0 (c (n "revent") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "16l0g662bmvc7fiv816mbvnb4p587xg4v4aqjxap8gjcvnw5wg5y")))

(define-public crate-revent-0.8.1 (c (n "revent") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1r4lmbsq31yjdzh1ybnhabyyrqnlag9jhnf9j040hbvqi25sgnh4")))

(define-public crate-revent-0.9.0 (c (n "revent") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "00lkab8z8i1mac7cb3ps58y26gyq1yjmzvqra5jq4xa0wpvndx9j")))

(define-public crate-revent-0.9.1 (c (n "revent") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1nc0qiznvxnmvrn12j7y6drvx975z9hzldskark4pwfqq3i1082x")))

(define-public crate-revent-0.10.0 (c (n "revent") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1p776bqxq1ydpxw8n2vgi9kxi07yg7l1f9gmfhm7dkf2mrh9hnll")))

(define-public crate-revent-0.10.1 (c (n "revent") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1yzvii0nnkqshv6a0rnga361rx67fk4vf2pbhbl7jvyhg0il0bb8")))

(define-public crate-revent-0.10.2 (c (n "revent") (v "0.10.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1zcj4x5h912pfyxx7kh3hhba1nvycbn94l4bgpmx16ka4gr4mjqm")))

(define-public crate-revent-0.10.3 (c (n "revent") (v "0.10.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)))) (h "1l91h48g9ngiigw192lln6jvzwfhxf0hmzywfj3qhjr3lacj8vlq")))

(define-public crate-revent-0.10.4 (c (n "revent") (v "0.10.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wjhc4sisiwhbbrvmvphni3fqqzkp8gmac028s8fmq2m8l8bpqmp")))

(define-public crate-revent-0.10.5 (c (n "revent") (v "0.10.5") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0ngbw5npph375psciavzlpss9iz1vain3dqlvzz9mlzn6f2wdfa1")))

(define-public crate-revent-0.10.6 (c (n "revent") (v "0.10.6") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0sjkj3aq9hbvn6bc591k9h9lwlky688fq8iyglj94mjbp964jjcf")))

(define-public crate-revent-0.11.0 (c (n "revent") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0gq5kms4rvk7khzn2adrlmy726nmxcybkx2x7csdmdsbjil39djk")))

(define-public crate-revent-0.13.1 (c (n "revent") (v "0.13.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "04r9kkmchhll7v5996rxzaywfcqb8nj8d4srr0mxylhd90d8sf80")))

(define-public crate-revent-0.14.0 (c (n "revent") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "03nl3vkp5r5dxyqi6l0gn8ksiy8csjvv2db487c4g4mzbin6zvpn")))

(define-public crate-revent-0.14.1 (c (n "revent") (v "0.14.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0hjkii782d5x64mhb3l8l5k7slbyx18yd6qnmnyyih3h4i8lhs5s")))

(define-public crate-revent-0.14.2 (c (n "revent") (v "0.14.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1xz4nshrpjbg13rj7wx2sxj2c0c19n1grgg2bsmyf8a5vfwj4dfa")))

(define-public crate-revent-0.15.0 (c (n "revent") (v "0.15.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1n2zylacj3aw6sx9ybab67aw7q71db48dcb4wc1fz1nwny5y9pjf")))

(define-public crate-revent-0.16.0 (c (n "revent") (v "0.16.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1jki32yyhbdvxbm3lyzjarxwnlzz58gd3y9qpddg3m2s2npvr1nh")))

(define-public crate-revent-0.17.0 (c (n "revent") (v "0.17.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1ibj1vl0520sljjyfk8bwn41f3xdai6pr5jbdbbmssrdw7lwzh85")))

(define-public crate-revent-0.18.0 (c (n "revent") (v "0.18.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1dc2ih5qgd8c56z2kd2lmdxalyirm6gqgnwqbd9x9xh9iqkg173p")))

(define-public crate-revent-0.19.0 (c (n "revent") (v "0.19.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0snk64xb4ay7ahwkp42pb730w8md0pqn5yyasdrcs1w0aix151dc")))

(define-public crate-revent-0.20.0 (c (n "revent") (v "0.20.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1yzl2gzfff597582ps8sdd73aiv1xry7n5k668c70vzzr0zj6lnq")))

(define-public crate-revent-0.20.1 (c (n "revent") (v "0.20.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0g5x8lsgg6sbwy8d1qnf3g3i8kw9a1v7qkdla34paahcs03wbx9j")))

(define-public crate-revent-0.20.2 (c (n "revent") (v "0.20.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "077nd0ki7cqi3gnz2697wn6c7rsp9vs8y667m5aiqqxih0wqrzig")))

(define-public crate-revent-0.21.0 (c (n "revent") (v "0.21.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1g2gr760q0ihl4akyzzyxb82k0rf32ymc9p6jkcj7rsb1yi0dzl0")))

(define-public crate-revent-0.21.1 (c (n "revent") (v "0.21.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1csj8ray4frx56flh1y66jzlypbsyp8awixswdmsac4y3bbwnz15")))

(define-public crate-revent-0.22.0 (c (n "revent") (v "0.22.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "11rl1jkvz8i79h6yb7rhjrb6aywsa5wkqi32f8si39jss5m5kdms")))

(define-public crate-revent-0.22.1 (c (n "revent") (v "0.22.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1f7a9gp3qfd0jzqr9090q377ki09iwrkqb93669ra1zcv8jjxzac")))

(define-public crate-revent-0.23.0 (c (n "revent") (v "0.23.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1pbq8sqy47x2m0gqpipv6h42bhgnmyiihi91bzy5g4cq7635ih3d")))

(define-public crate-revent-0.23.1 (c (n "revent") (v "0.23.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0x0894nh9rjvn8xyzi3xsbjifdzm4kpdy2frj3jdpqkfyx6ci2gd")))

(define-public crate-revent-0.23.2 (c (n "revent") (v "0.23.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1bs0m8l8xs24sqnpw5md9r3f44yb4icq95p4c7b7px530kb8v8mx")))

(define-public crate-revent-0.23.3 (c (n "revent") (v "0.23.3") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1amvaxxcyfnv5ba5xjsq9wqm0js5vhm5wwi1sgv74cmq5csycy1d")))

(define-public crate-revent-0.24.0 (c (n "revent") (v "0.24.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "slog-term") (r "^2.5") (d #t) (k 2)))) (h "11ln0j05s695cxb7k69lkry16k4sbba7kl96mf1cbfrh0w6qqvj5") (f (quote (("logging" "slog"))))))

(define-public crate-revent-0.24.1 (c (n "revent") (v "0.24.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "slog-term") (r "^2.5") (d #t) (k 2)))) (h "056kyp3yhc42inw8hszmjzhmfys5rkm4nynz5sscjwnlhmfpqadh") (f (quote (("logging" "slog"))))))

(define-public crate-revent-0.25.0 (c (n "revent") (v "0.25.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "slog-term") (r "^2.5") (d #t) (k 2)))) (h "19zb6sl0ys7bmh6iykj0j3rb7ii6q4fm51xkpq832v98438d1r6h") (f (quote (("logging" "slog"))))))

(define-public crate-revent-0.26.0 (c (n "revent") (v "0.26.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "slog-term") (r "^2.5") (d #t) (k 2)))) (h "0d5i32zrpzkh8h1205dkpbr501v2zj3y0wwqg62lplvb7kh062a0") (f (quote (("logging" "slog"))))))

(define-public crate-revent-0.27.0 (c (n "revent") (v "0.27.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.5.2") (o #t) (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 2)) (d (n "slog-term") (r "^2.5") (d #t) (k 2)))) (h "14cj8qwa52l3ywk92pqc0cq4w0xsbkw211jqj01r30galbknngr3") (f (quote (("logging" "slog"))))))

(define-public crate-revent-0.28.0 (c (n "revent") (v "0.28.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "031gznbhdmj7hfpwwgsn9mmjzdzzw23j8mkpdysqdlpn62xhqm30") (f (quote (("trace"))))))

(define-public crate-revent-0.28.1 (c (n "revent") (v "0.28.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "isize-vec") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1adq46ji2klrm68ixx9x8gvdyacpfvahp1533npqpasnd7zkg5hh") (f (quote (("trace"))))))

(define-public crate-revent-0.29.0 (c (n "revent") (v "0.29.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "isize-vec") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1r1fflf558fr7mq56n2yg4bmp673sin6g0psz33vrkvkh1wi4l1a") (f (quote (("trace") ("asynchronous" "crossbeam-channel"))))))

