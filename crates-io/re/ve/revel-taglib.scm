(define-module (crates-io re ve revel-taglib) #:use-module (crates-io))

(define-public crate-revel-taglib-1.0.0 (c (n "revel-taglib") (v "1.0.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1fnkjpkzyclnlhz5r9zlsr3ywaxklsp5pvlli6ir7hp34lk0wk23") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-revel-taglib-1.1.0 (c (n "revel-taglib") (v "1.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1xcgzv2w1gip0s3r6svf04zbjf8m1cwk65q56zmhgm7y02f7hrxb") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-revel-taglib-2.0.0 (c (n "revel-taglib") (v "2.0.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1fpm58dcliwp9sg3jys6zxy1wdwjq89y278nw3i34yyqr9wjpf0s") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-revel-taglib-2.0.1 (c (n "revel-taglib") (v "2.0.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "171162lwp05si9gry5qzvrqgc0261m9h0i0ifwc33l4pyk4rvcq4") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

