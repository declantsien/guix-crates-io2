(define-module (crates-io re ve rever) #:use-module (crates-io))

(define-public crate-rever-0.0.1 (c (n "rever") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1mgr4gb44xix1vfh7bvn8b11wh2bzb0r03mpy1ahl1mn42xl52pd")))

