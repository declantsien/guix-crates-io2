(define-module (crates-io re ve reverse_proxy) #:use-module (crates-io))

(define-public crate-reverse_proxy-0.1.0 (c (n "reverse_proxy") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (f (quote ("multipart" "tls"))) (d #t) (k 0)) (d (n "warp-reverse-proxy") (r "^0.3") (d #t) (k 0)))) (h "1hryjiyc92ck40kpzri2zrri9scj37dpzj9wawdw72bgsnkzxi8g")))

(define-public crate-reverse_proxy-0.2.0 (c (n "reverse_proxy") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (f (quote ("multipart" "tls"))) (d #t) (k 0)) (d (n "warp-reverse-proxy") (r "^0.3") (d #t) (k 0)))) (h "19pbb4vrf6f9d9l8a47kbiwaqwl625fkwdbxnspddg8wmygl23xp")))

(define-public crate-reverse_proxy-0.3.0 (c (n "reverse_proxy") (v "0.3.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (f (quote ("multipart" "tls"))) (d #t) (k 0)) (d (n "warp-reverse-proxy") (r "^0.4") (d #t) (k 0)))) (h "18vf3lk3ry50yx788bz9xa4j6w0d0lrl06qdykm95f0lcxlypwmk")))

