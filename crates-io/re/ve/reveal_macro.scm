(define-module (crates-io re ve reveal_macro) #:use-module (crates-io))

(define-public crate-reveal_macro-0.1.0 (c (n "reveal_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "030854x8hw83lgl4lsa1y3x9daqr86v56s1sdjvcn2lzzp9jjhkh") (y #t)))

