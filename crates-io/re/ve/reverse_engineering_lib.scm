(define-module (crates-io re ve reverse_engineering_lib) #:use-module (crates-io))

(define-public crate-reverse_engineering_lib-0.1.0 (c (n "reverse_engineering_lib") (v "0.1.0") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1r774aqmp6p514ij3s54gddy2kljf2ijs6zh040yf5qya75wk7j9")))

(define-public crate-reverse_engineering_lib-0.2.0 (c (n "reverse_engineering_lib") (v "0.2.0") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0g5k8320plgdn2aw2fgyyf7gml9jkz470qwknn227s3p0hnq4vb8")))

(define-public crate-reverse_engineering_lib-0.2.2 (c (n "reverse_engineering_lib") (v "0.2.2") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "12b463i5qk9zila2wjy6iaazk87kgfi6hanidlxg9pdr1bbvffic")))

(define-public crate-reverse_engineering_lib-0.3.0 (c (n "reverse_engineering_lib") (v "0.3.0") (d (list (d (n "capstone") (r "^0.12.0") (d #t) (k 0)) (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0gknzf16apj0qhd87va42xchirk9yj8a0jmw9g0cw0v9aa8ha7b5")))

