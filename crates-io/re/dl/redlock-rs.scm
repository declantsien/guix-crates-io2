(define-module (crates-io re dl redlock-rs) #:use-module (crates-io))

(define-public crate-redlock-rs-0.2.0 (c (n "redlock-rs") (v "0.2.0") (d (list (d (n "redis") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ay32gs9niy2akg1z8n38mqlyjsmk624qdkpac41dfc49svf9nfb")))

(define-public crate-redlock-rs-0.2.1 (c (n "redlock-rs") (v "0.2.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "redis") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "04nkcy988gk8mv32gya2g6ay8zrl2gvh834hddghylwx7vg05k4s")))

