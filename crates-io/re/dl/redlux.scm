(define-module (crates-io re dl redlux) #:use-module (crates-io))

(define-public crate-redlux-0.1.0 (c (n "redlux") (v "0.1.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.13.0") (k 0)))) (h "0cyfmq7k3wfxhhn88lgfrxxxvl924w93jjl77f3nmm5jz7na710f")))

(define-public crate-redlux-0.2.0 (c (n "redlux") (v "0.2.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.13.0") (k 0)))) (h "05z3q1zcp2gb1nrzvs14b5nkgjh0ikr9khs8xv2fda1n6zxh1c0f")))

(define-public crate-redlux-0.3.0 (c (n "redlux") (v "0.3.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.8.2") (d #t) (k 0)) (d (n "rodio") (r "^0.13.0") (k 0)))) (h "0mqy8gmg5sq09r5sn310akbiganh48r9nijnpcjbh00fky6l0yky")))

(define-public crate-redlux-0.4.0 (c (n "redlux") (v "0.4.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.8.2") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (k 0)))) (h "16h5nh2gb0fp5c4fifsn1qaxrwidvg29bmn3cj83b21bak24w5d8")))

(define-public crate-redlux-0.5.0 (c (n "redlux") (v "0.5.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.10.0") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (k 0)))) (h "1z5ybmn8synvick6azi9g460akvbz5366am2wf99zb6aim73x9ya")))

(define-public crate-redlux-0.6.0 (c (n "redlux") (v "0.6.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.12.0") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (k 0)))) (h "0wfiv8b1w8f9hzxvmzg4r145bz8rqchx0ibaxf949mpqr9qx87x7")))

(define-public crate-redlux-0.7.0 (c (n "redlux") (v "0.7.0") (d (list (d (n "fdk-aac") (r "^0.4.0") (d #t) (k 0)) (d (n "mp4") (r "^0.12.0") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (o #t) (k 0)))) (h "1v9gm8hzymv9zyb1hgdhfdrh2vz7yrjyjccy1qlpv1qaznffjh57") (f (quote (("default" "rodio"))))))

