(define-module (crates-io re dl redlock) #:use-module (crates-io))

(define-public crate-redlock-0.3.0 (c (n "redlock") (v "0.3.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "redis") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "06hsvp3jpb7n1dxqc4z4bsq6c6d2va4vahdbf8jqs0s6gqh40qqx")))

(define-public crate-redlock-0.3.1 (c (n "redlock") (v "0.3.1") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "redis") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1z7zcyc9wiqdbxqmb1d9y2ycbfyz9r62n1pz5k1qf9378j49ykjq")))

(define-public crate-redlock-1.0.0 (c (n "redlock") (v "1.0.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "redis") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0m1g40882a23wanx5pdjxs5brvpd0zrzhrpkm42hpwwjchic7a2b")))

(define-public crate-redlock-1.1.0 (c (n "redlock") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)))) (h "0a43b9y11q5ml55rdqp6xn9awq3039qs3sgd5w0nw5m8cskw6pkb")))

(define-public crate-redlock-1.2.0 (c (n "redlock") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "testcontainers") (r "^0.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.7.1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0j40j36qlkixfwrzkh99v3f8kp7c7wknrkxrxfbl8ncckhrbzl8z") (f (quote (("tls" "redis/tls") ("default") ("async" "tokio"))))))

(define-public crate-redlock-2.0.0 (c (n "redlock") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "testcontainers") (r "^0.14.0") (d #t) (k 2)))) (h "18laicccqxq4zdkxi69hfxpr5adnlvxhgq5h476x2fljnlkhncff") (f (quote (("default") ("async" "tokio"))))))

