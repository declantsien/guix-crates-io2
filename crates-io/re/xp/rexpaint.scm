(define-module (crates-io re xp rexpaint) #:use-module (crates-io))

(define-public crate-rexpaint-0.1.0 (c (n "rexpaint") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "codepage-437") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "noise") (r "^0.5") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textwrap") (r "^0.10") (d #t) (k 2)))) (h "1bfg2gblnphyi474pj8fskixrg6gbnrvbkbc63wwr0zlw8jybrwf")))

(define-public crate-rexpaint-0.1.1 (c (n "rexpaint") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "codepage-437") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "noise") (r "^0.5") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "tcod") (r "^0.12") (d #t) (k 2)) (d (n "textwrap") (r "^0.10") (d #t) (k 2)))) (h "0j4saqphjfc6w11dbkxrma23a6hy6v2xn8ghh03lqdjbn3yxdlbp")))

