(define-module (crates-io re xp rexpect) #:use-module (crates-io))

(define-public crate-rexpect-0.1.0 (c (n "rexpect") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "1h2k807y9d9ljm2ly5597d99xb95hjsa828c5rzaxm49sx6b0g4p")))

(define-public crate-rexpect-0.1.1 (c (n "rexpect") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "1nq4k8nwnw5jxdx7yqskwzkkxra158jwb8bjyc5ph9f8kl81yrff")))

(define-public crate-rexpect-0.2.0 (c (n "rexpect") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "083iv2bkh86m99rjsrhrpv3ahcap3l06kmg8gxybd5qnkmwr0h55")))

(define-public crate-rexpect-0.3.0 (c (n "rexpect") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "174s099xjrj7g531vr5d7qqjw4ppl1376mwb1vk87jnz1a127wn9")))

(define-public crate-rexpect-0.4.0 (c (n "rexpect") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "11kihz6dcri4gprd8pj0axf3z48rz5d6ml6x4nrglq8lv54h2b46")))

(define-public crate-rexpect-0.5.0 (c (n "rexpect") (v "0.5.0") (d (list (d (n "comma") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1y6zb8ckqn88gqp72bc7q1cfsy35pwhishmymm45mywnixvn1zq1") (r "1.60")))

