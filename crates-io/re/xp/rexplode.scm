(define-module (crates-io re xp rexplode) #:use-module (crates-io))

(define-public crate-rexplode-1.0.0 (c (n "rexplode") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)))) (h "0d0iaxlqf6a92svwy0i3qv3ngld2g0wfxvq151h1pp4ryjhaslp6")))

(define-public crate-rexplode-1.0.1 (c (n "rexplode") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)))) (h "1wxjzg6dgwh52kzz7w974wkripjfar858cskjg00xbq1qs116js2")))

