(define-module (crates-io re xp rexpr) #:use-module (crates-io))

(define-public crate-rexpr-1.0.0 (c (n "rexpr") (v "1.0.0") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "17frld9salcbzrwkzxn94b1gdfvz3jrsnspjvv76hllgdxvy5552")))

(define-public crate-rexpr-1.0.1 (c (n "rexpr") (v "1.0.1") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "162l96xdsw6504pcy959mihr8vr0kh72naylvb63qy3zlq7gfbgh")))

(define-public crate-rexpr-1.0.2 (c (n "rexpr") (v "1.0.2") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "03wbs8x1g9gl5bcd99z9z5d8aqsz3w12jfrh262161w6vmyw4y41")))

(define-public crate-rexpr-1.0.3 (c (n "rexpr") (v "1.0.3") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1s8n4fgllnp2cybrnlvgvcqxh7aimw4gs0q6kxhlfc1bm34g3rgd")))

(define-public crate-rexpr-1.0.4 (c (n "rexpr") (v "1.0.4") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "00hpa5gymksz0mipqmhqx15rscvz1bhyhcv4iks2pk1nhmd3h0mr")))

(define-public crate-rexpr-1.0.5 (c (n "rexpr") (v "1.0.5") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16n311fapbiypm3527m5pc50ikh9zfcipj8qlj4r04prqy6317x5")))

(define-public crate-rexpr-1.0.6 (c (n "rexpr") (v "1.0.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "18zrza5wczjas9zpb609jgh3ybbprjykk2gg08ml1bwkz9my81zp")))

(define-public crate-rexpr-1.0.7 (c (n "rexpr") (v "1.0.7") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1npy67mhsf666mlsq32xp53gldg14pfpk8h243v4h0s089gcqls7")))

