(define-module (crates-io re cy recycler) #:use-module (crates-io))

(define-public crate-recycler-0.1.0 (c (n "recycler") (v "0.1.0") (h "1zfib9s33880axjvgqybql9sg2nbsvi9ffvrjdfdlf86xc3p0xg9")))

(define-public crate-recycler-0.1.1 (c (n "recycler") (v "0.1.1") (h "1dq4mg7lprp12n62qd0wrh33mvqb7ix09syv76hibfapnx3rvmbx")))

(define-public crate-recycler-0.1.2 (c (n "recycler") (v "0.1.2") (h "1fh2mdbrf87ry70amwr93l5ngipwyg5vqw2n5hh6m63wf8izxyma")))

(define-public crate-recycler-0.1.3 (c (n "recycler") (v "0.1.3") (h "0w215ybplrky4bjm7466n3b9hc0a9v6xfg003h9vfvs1rzjamdx0")))

(define-public crate-recycler-0.1.4 (c (n "recycler") (v "0.1.4") (h "1yll0sqswy6afk9ik7r22djqafa3wfgvgdzqqh7jbczyiqr2gp4q")))

