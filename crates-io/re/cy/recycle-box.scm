(define-module (crates-io re cy recycle-box) #:use-module (crates-io))

(define-public crate-recycle-box-0.1.0 (c (n "recycle-box") (v "0.1.0") (h "1zf0y8a2m6czppix77vbcw0p9c4frmv513wr1kf7068vhj4hdgnj") (r "1.56")))

(define-public crate-recycle-box-0.2.0 (c (n "recycle-box") (v "0.2.0") (h "0i66zlzbxdlc92q3kxlgsywp9418dzdh2an2j9fs10v11wf0n605") (r "1.56")))

