(define-module (crates-io re cy recycle_vec) #:use-module (crates-io))

(define-public crate-recycle_vec-1.0.0 (c (n "recycle_vec") (v "1.0.0") (h "0a5qaryypk0n6kpvrww1c6cwhxbs1f48hpr50933swm4vsc96vmb")))

(define-public crate-recycle_vec-1.0.1 (c (n "recycle_vec") (v "1.0.1") (h "18nc7x9haipn8g038m7b0s2j1mr6lr8bz8wzqawf174ybyl2mn2w")))

(define-public crate-recycle_vec-1.0.2 (c (n "recycle_vec") (v "1.0.2") (h "0b6ariyc4xjkbb31n7r75nv5c422dax2k9i47k9r66jy98867dj1")))

(define-public crate-recycle_vec-1.0.3 (c (n "recycle_vec") (v "1.0.3") (h "07dwjhidzq5m7xj8z56s7gyj08l4ywj2nvwx8zgpwqg6vqp24i14")))

(define-public crate-recycle_vec-1.0.4 (c (n "recycle_vec") (v "1.0.4") (h "0g0kaac9wb2hb88lgr9dwa1zbml59dwqr49d5b3ib5k2zz01ngy9")))

(define-public crate-recycle_vec-1.1.0 (c (n "recycle_vec") (v "1.1.0") (d (list (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0922qy5nyb5dc7hynab344gn8x1q108qkwj03f69vq9d64mnbfg4")))

(define-public crate-recycle_vec-1.1.1 (c (n "recycle_vec") (v "1.1.1") (d (list (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1i3fq3frvmpbgrvnc4h7n73njwl2vabcymy979sfigmwf204m41d")))

