(define-module (crates-io re to retour-utils-impl) #:use-module (crates-io))

(define-public crate-retour-utils-impl-0.1.0 (c (n "retour-utils-impl") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z9rzx7349v77zv9kb41x5nn77s4d9ryszk1zagkhlwm04cjmcn8")))

(define-public crate-retour-utils-impl-0.1.1 (c (n "retour-utils-impl") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12fpaavcghmxfxz0ik2jgz6qwc4y3h6lmyq0xw6c0r62jphwivjc")))

(define-public crate-retour-utils-impl-0.1.2 (c (n "retour-utils-impl") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03s6li9xjg2wk2ikckxgs2hqq9fxfc3cb4vfwsjzakbzd3b2jn4f")))

(define-public crate-retour-utils-impl-0.2.0 (c (n "retour-utils-impl") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0iwnarkvvxl7xx2dzidb6aviw0ljfzazqngcwl9kq0hagdf7d1il")))

