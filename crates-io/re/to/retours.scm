(define-module (crates-io re to retours) #:use-module (crates-io))

(define-public crate-retours-0.0.1 (c (n "retours") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "01gv3yvsxi7szcdn3h7qnd6j3y6vsxs8k73482vgyff0xvd4rmkv")))

(define-public crate-retours-0.0.2 (c (n "retours") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "054h9ljl393rb453x6r6mqh8vdsnqyxd8zllj9c1c42dxay1rkij") (y #t)))

(define-public crate-retours-0.0.3 (c (n "retours") (v "0.0.3") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "087v5grfphn9pc3q2q1vjbd4zz57wra2dmmn4gc25ji672qhgnsy")))

