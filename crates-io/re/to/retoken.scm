(define-module (crates-io re to retoken) #:use-module (crates-io))

(define-public crate-retoken-0.1.0 (c (n "retoken") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1n96acxwi2pbapax5yn944q5nx9swn9lvq180a7ffarnm89zfn1r")))

(define-public crate-retoken-0.1.1 (c (n "retoken") (v "0.1.1") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1c9zrkcpz3y6m2z93jn3hqabvxjihsngczdxjbknpf9nj1i414rb")))

(define-public crate-retoken-0.1.2 (c (n "retoken") (v "0.1.2") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0dqx5hzlxlk3sbcbhs7yzxwwp80sfxss6mnpckmakfqpghcfa41f")))

(define-public crate-retoken-0.1.3 (c (n "retoken") (v "0.1.3") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1vb5g4snwyna8j0m96i33qiz0bj3d9pilvi88lglzn49rknakz7h")))

(define-public crate-retoken-0.1.4 (c (n "retoken") (v "0.1.4") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1axn0b1hk0bjggb1lqicgq65jln62qw7k6all8k91qbklsaba605")))

(define-public crate-retoken-0.1.5 (c (n "retoken") (v "0.1.5") (d (list (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "retoken-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0qgc9yjifhplv8bxll3s6g2a5yxyhzc95rnckp69jafzwmwkf3q9")))

