(define-module (crates-io re to retouch) #:use-module (crates-io))

(define-public crate-retouch-0.1.0 (c (n "retouch") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.30") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0yn1fb3dmbn4bazbcp3lk2wgnfdlaxn07p5cp31pnkcp5f284065")))

