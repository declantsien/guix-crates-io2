(define-module (crates-io re to retoken-macros) #:use-module (crates-io))

(define-public crate-retoken-macros-0.1.0 (c (n "retoken-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16l3wm8v8fyr4rll8jpiz5dim42z5ij7q68yfvzv6v5pl6yzpbcj")))

(define-public crate-retoken-macros-0.1.1 (c (n "retoken-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1ma151i36bd7pr7lpq51si7gh0jy46a0nx16j93xq63mk8gmzm6i")))

(define-public crate-retoken-macros-0.1.2 (c (n "retoken-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1ml7qr298xkaqaxzscnzsf10k4018id7gkbkpf0a5ia5fvpnc28z")))

