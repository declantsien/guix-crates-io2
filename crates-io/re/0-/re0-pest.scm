(define-module (crates-io re #{0-}# re0-pest) #:use-module (crates-io))

(define-public crate-re0-pest-0.1.0 (c (n "re0-pest") (v "0.1.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "19zc05i8601ycjbysi9188c4i8d21y3nrm1qk519dbl9a83fgfxa")))

(define-public crate-re0-pest-0.2.0 (c (n "re0-pest") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1j7myks83ympxkcgr43cdjngphxk5lfgixn14nrv3kg7535gcx8f")))

