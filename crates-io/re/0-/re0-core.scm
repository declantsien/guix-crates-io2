(define-module (crates-io re #{0-}# re0-core) #:use-module (crates-io))

(define-public crate-re0-core-0.0.0 (c (n "re0-core") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0q820ifpg2ybw3vz94dlzdcfc1k91f3gz51d4x4wdbix1nv81h3m") (f (quote (("default"))))))

