(define-module (crates-io re bo rebootinto-cli) #:use-module (crates-io))

(define-public crate-rebootinto-cli-0.1.0 (c (n "rebootinto-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "embed-resource") (r "^1") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rebootinto-core") (r "^0.1") (d #t) (k 0)))) (h "14i9m080nv57yxa8ipnqjl6cv4rxpybxb1n5y437wnsns144xxzy")))

