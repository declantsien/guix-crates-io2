(define-module (crates-io re bo reborrow-derive) #:use-module (crates-io))

(define-public crate-reborrow-derive-0.3.0 (c (n "reborrow-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18l4i8vh4ymi9723rs1w3w0iw4hkhhbngj73s018pvi9igm7wlfj")))

(define-public crate-reborrow-derive-0.4.0 (c (n "reborrow-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a44azzz7z9rxi69frcayybg2znsz8k7xg54lfhrl30wsn4i8cq1")))

(define-public crate-reborrow-derive-0.5.0 (c (n "reborrow-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12ki1jww9q0f6184r0ljkranq4x46f9bffzkah9v6q3f590icbdl")))

(define-public crate-reborrow-derive-0.5.2 (c (n "reborrow-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fxv9k8sscc9898yzzhnlc27i6qr9g2lb07scvcblnl34swabsjq")))

