(define-module (crates-io re bo rebootinto-tui) #:use-module (crates-io))

(define-public crate-rebootinto-tui-0.1.0 (c (n "rebootinto-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "embed-resource") (r "^1") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rebootinto-core") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.8") (o #t) (k 0)))) (h "10084alqv6f0ik2dxldb7w20xlm50q5rw6kbdgx52m63kw8bwsns") (f (quote (("termion_backend" "termion" "tui" "tui/termion") ("default" "crossterm_backend") ("crossterm_backend_sync_input" "crossterm_backend") ("crossterm_backend" "crossterm" "tui" "tui/crossterm"))))))

