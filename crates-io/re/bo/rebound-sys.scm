(define-module (crates-io re bo rebound-sys) #:use-module (crates-io))

(define-public crate-rebound-sys-0.1.0 (c (n "rebound-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0hq36virwqy3wcnsc1d3b1gnna2z7ijbc9jq3lc9jz324mbmabf0") (l "rebound")))

(define-public crate-rebound-sys-0.1.1 (c (n "rebound-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0gvjwqh0vkl4zha65livl5iqbqg9vgy27mgil5wlly1w8dsizy6j") (l "rebound")))

(define-public crate-rebound-sys-0.2.0 (c (n "rebound-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1p0f5azsds7sgm3r0j1wql7g175fz0b09kx7i8wdl8cby5vlpga5") (l "rebound")))

