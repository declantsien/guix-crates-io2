(define-module (crates-io re bo rebound-proc) #:use-module (crates-io))

(define-public crate-rebound-proc-0.0.1 (c (n "rebound-proc") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06vf9siwldr349h5251hdfh289zqmm2qf1g4v69d6ayq6w2bmvd7")))

(define-public crate-rebound-proc-0.1.0 (c (n "rebound-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15hrf10203sg1smlnm6mas274r0v5n8bj05j6g5zwx45ka5dswrb")))

(define-public crate-rebound-proc-0.2.0 (c (n "rebound-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kfqz1545q2p21clj935dl9hqwwhk230yyrpkdz0x41yczjl8fa6")))

(define-public crate-rebound-proc-0.3.0 (c (n "rebound-proc") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qg17hlw99wg046fqcz2g1l9fri1knfncjbz5lx6nh35kjphh18i")))

(define-public crate-rebound-proc-0.4.0 (c (n "rebound-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10lsrzdws8mgix2xsbriyh2w37533bcgjm64a9750sw96slh4cjx")))

