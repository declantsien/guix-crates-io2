(define-module (crates-io re bo rebop) #:use-module (crates-io))

(define-public crate-rebop-0.3.0 (c (n "rebop") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1mq777fprynl9jzkwj4r2lvx5klvpga7hriwnybsx6jmc00jszx4")))

(define-public crate-rebop-0.3.1 (c (n "rebop") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0h4gdw6ibwbwwq0l1kcanya5xzf7mqzp1ddhdb5v1yldw9wwhdqs")))

(define-public crate-rebop-0.4.0 (c (n "rebop") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "pyo3") (r "^0.14.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0vxsl1jnd0ph066avfkij25jmv2yffnikv05n8fvmflxj3qkmvq2")))

(define-public crate-rebop-0.5.0 (c (n "rebop") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0jhkaw8arcnx556qdmbdgk2kvl3wx5vqyayciljyv0dgi9fsgmks")))

(define-public crate-rebop-0.5.1 (c (n "rebop") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1mfxmhbqidlgsrk35bh4zj1hwiyh5ha1lkvhm15nihl2aa1r8z6x")))

(define-public crate-rebop-0.6.0 (c (n "rebop") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module" "abi3-py39"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0vb2qbq7r454kxks785yz44vmnzpi499f297nm471a764lvri73z")))

