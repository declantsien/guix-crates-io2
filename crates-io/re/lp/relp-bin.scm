(define-module (crates-io re lp relp-bin) #:use-module (crates-io))

(define-public crate-relp-bin-0.0.1 (c (n "relp-bin") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "relp") (r "^0.0.6") (d #t) (k 0)) (d (n "relp-num") (r "^0.0.5") (d #t) (k 0)))) (h "1jhfmlj0pbnmzaigrknnabnz8c1i00wxbcjz6icaxlxhknv5qdng")))

(define-public crate-relp-bin-0.0.2 (c (n "relp-bin") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "relp") (r "^0.0.6") (d #t) (k 0)) (d (n "relp-num") (r "^0.0.5") (d #t) (k 0)))) (h "1vd14vnkq3hphfbafr1glb29vsk1lsbn49hznds1hvwskbn1i0ir")))

(define-public crate-relp-bin-0.0.3 (c (n "relp-bin") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "relp") (r "^0.0.8") (d #t) (k 0)) (d (n "relp-num") (r "^0.0.5") (d #t) (k 0)))) (h "0c2xvw0nsnkc9a6qb5lxkziy6s9f27absbsms2k3jnmx9a8v80a4")))

(define-public crate-relp-bin-0.0.4 (c (n "relp-bin") (v "0.0.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "relp") (r "^0.2.1") (d #t) (k 0)) (d (n "relp-num") (r "^0.1.2") (d #t) (k 0)))) (h "05dlmi5kf4yjawapjcjhaga97zvdbrg2hiw7padpbyryasdk1n13")))

(define-public crate-relp-bin-0.0.5 (c (n "relp-bin") (v "0.0.5") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "relp") (r "^0.2.1") (d #t) (k 0)) (d (n "relp-num") (r "^0.1.2") (d #t) (k 0)))) (h "1msx21wgsljp5hx4nh4w9ylm8zlyh9jvzlgp4f90w539nc7vjf8a")))

(define-public crate-relp-bin-0.0.6 (c (n "relp-bin") (v "0.0.6") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "relp") (r "^0.2.3") (d #t) (k 0)) (d (n "relp-num") (r "^0.1.10") (d #t) (k 0)))) (h "08y9h3dl1l9afgzlrh3308y5w8ddlvfcdl7g3vjkgsck3iszincx")))

