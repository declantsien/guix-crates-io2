(define-module (crates-io re ne renet_steam) #:use-module (crates-io))

(define-public crate-renet_steam-0.0.1 (c (n "renet_steam") (v "0.0.1") (d (list (d (n "bevy_app") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "renet") (r "^0.0.15") (d #t) (k 0)) (d (n "steamworks") (r "^0.11") (d #t) (k 0)))) (h "1nlgvs3c19xhsgxwzk12aqbqzb6dd7varvl93fg8sciy6n1wywl3") (s 2) (e (quote (("bevy" "dep:bevy_app" "dep:bevy_ecs" "dep:bevy_renet"))))))

