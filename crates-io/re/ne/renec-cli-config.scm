(define-module (crates-io re ne renec-cli-config) #:use-module (crates-io))

(define-public crate-renec-cli-config-1.8.14 (c (n "renec-cli-config") (v "1.8.14") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0cg9lc5hh1j85f2c3shvp2q363s7hhzsvbvnq55hp8apabi7v72m")))

