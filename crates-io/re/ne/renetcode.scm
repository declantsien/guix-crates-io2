(define-module (crates-io re ne renetcode) #:use-module (crates-io))

(define-public crate-renetcode-0.0.1 (c (n "renetcode") (v "0.0.1") (d (list (d (n "aead") (r "^0.4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "120nf9w45cy8i8cvz45hy4nyb3zxrh3z0hjkq7l8zdg6zpj42nyc")))

(define-public crate-renetcode-0.0.2 (c (n "renetcode") (v "0.0.2") (d (list (d (n "aead") (r "^0.4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "09saprz6cmzzdhk27d0pdgwaj8di76gwn84jv3sbzblwxzl5nzf9")))

(define-public crate-renetcode-0.0.3 (c (n "renetcode") (v "0.0.3") (d (list (d (n "aead") (r "^0.4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1ghcbff3qrabmkgcz7l3y01z3x4i4j1l79k31bl8vpvgax57flvi")))

(define-public crate-renetcode-0.0.4 (c (n "renetcode") (v "0.0.4") (d (list (d (n "aead") (r "^0.4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0055fjfqfv0p20jmbjw4kfram0d08x5vplsdw5lzvfhhngc4rxny")))

(define-public crate-renetcode-0.0.5 (c (n "renetcode") (v "0.0.5") (d (list (d (n "aead") (r "^0.4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "14fv9vdak677bifps6i4qnz9lijj18gd637zmv8r3fkm7rkdb6ff")))

(define-public crate-renetcode-0.0.6 (c (n "renetcode") (v "0.0.6") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15r755lnf7867gr1hyczg25gfwadipbz0mjxgjk7hhrxbrfzj9y6")))

(define-public crate-renetcode-0.0.7 (c (n "renetcode") (v "0.0.7") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "18w5nnfchyj3v88sj0jm2v6f259zivpk1357gxqzg058sya0scxl")))

(define-public crate-renetcode-0.0.8 (c (n "renetcode") (v "0.0.8") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1yz9dland4ma5wjywkka5y5mz8x079p608ngxmmqcvwswx3gqq9h")))

(define-public crate-renetcode-0.0.9 (c (n "renetcode") (v "0.0.9") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kq9fsias7yhqnkgjgmphpvbnffcm7gcfjb03p380zfd0r6cc1pd")))

(define-public crate-renetcode-0.0.10 (c (n "renetcode") (v "0.0.10") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xwzf87aa3csy2rcqccpjdhz9swkilp93rmvm4bi5qvh8pvwf80d")))

(define-public crate-renetcode-0.0.11 (c (n "renetcode") (v "0.0.11") (d (list (d (n "chacha20poly1305") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "19rjh917xvgqpdbhpvga5chdhd019cark8daiz667mmicbv4q5is")))

