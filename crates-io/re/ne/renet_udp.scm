(define-module (crates-io re ne renet_udp) #:use-module (crates-io))

(define-public crate-renet_udp-0.0.1 (c (n "renet_udp") (v "0.0.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "renet") (r "^0.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08vyszzb8smd3x99d86lssaz9ps7s0cvz89m1jc2ajmkkwk802yg")))

(define-public crate-renet_udp-0.0.2 (c (n "renet_udp") (v "0.0.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "renet") (r "^0.0.4") (d #t) (k 0)))) (h "07kjfzw6n0js5hn8xvfzqh2bdika0ip7frlxn3lgbrbwwk8ny7mm")))

