(define-module (crates-io re ne renet) #:use-module (crates-io))

(define-public crate-renet-0.0.1 (c (n "renet") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18x08x8wm56bwdf2cyv302k51kzid5y5ma90ifyflpgp679wfkmi")))

(define-public crate-renet-0.0.2 (c (n "renet") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r2s24cry8yl4kdizgmbdy8r79bswr8q8jb2v18s3l3ymg6sd1nw")))

(define-public crate-renet-0.0.3 (c (n "renet") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xa7ssh2l0bn2gja9qwfb8s8mr00p1qqw1c7bap9xz6sa9zbybq1")))

(define-public crate-renet-0.0.4 (c (n "renet") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "029ds72dg8s0mp66r3q2sld57qy8dbhqpnqfyc0pndrasd1x4269")))

(define-public crate-renet-0.0.6 (c (n "renet") (v "0.0.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.2") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.2") (d #t) (k 0)))) (h "16f449n211af2qf3s9c30l937886z1mj79cm9lg4ncdq62mfrqmw")))

(define-public crate-renet-0.0.7 (c (n "renet") (v "0.0.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.3") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.3") (d #t) (k 0)))) (h "17apz0c02f6ba7is486yl6f2mi583639p00fmqnnr8lww7mqafva")))

(define-public crate-renet-0.0.8 (c (n "renet") (v "0.0.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.4") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.4") (d #t) (k 0)))) (h "1bs2ydryan1lb4cgcqhcszw3l8yxzzwz8vpl6a320xm3akbfxi3n")))

(define-public crate-renet-0.0.9 (c (n "renet") (v "0.0.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.5") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.5") (d #t) (k 0)))) (h "15g9g5ys3swv2f9vb6djx5z1kxjq9vslspgm0af50y9fg6ym6hsw")))

(define-public crate-renet-0.0.10 (c (n "renet") (v "0.0.10") (d (list (d (n "bevy_ecs") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.6") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.6") (d #t) (k 0)))) (h "1wxqavlcvgazxh7ggf2813n4syqmiqbj7h4qv11d1vg9a93l7fk3") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet-0.0.11 (c (n "renet") (v "0.0.11") (d (list (d (n "bevy_ecs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rechannel") (r "^0.0.7") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.7") (d #t) (k 0)))) (h "15fvp42x0qjr1kj8v7ll1w1zr9pr7w6byv7c1snhhkgwdjb4385f") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet-0.0.12 (c (n "renet") (v "0.0.12") (d (list (d (n "bevy_ecs") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "octets") (r "^0.2") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.8") (o #t) (d #t) (k 0)))) (h "18bf0xs0xzhimnv8nazhaica2kj7lzigknx33s1hnmism05i57bp") (f (quote (("default" "transport")))) (s 2) (e (quote (("transport" "dep:renetcode") ("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet-0.0.13 (c (n "renet") (v "0.0.13") (d (list (d (n "bevy_ecs") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "octets") (r "^0.2") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "0vmpfmjwbg2ngi6fwbc7kfga5cci0hjnkfd83k20nr58n2i2853m") (f (quote (("default" "transport")))) (s 2) (e (quote (("transport" "dep:renetcode") ("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet-0.0.14 (c (n "renet") (v "0.0.14") (d (list (d (n "bevy_ecs") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "octets") (r "^0.2") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i4n898n5dfxcw4cnmnkgqpiza1yfyz7wfxvl66asz2nsx1ngb73") (f (quote (("default" "transport")))) (s 2) (e (quote (("transport" "dep:renetcode") ("serde" "dep:serde") ("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet-0.0.15 (c (n "renet") (v "0.0.15") (d (list (d (n "bevy_ecs") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "octets") (r "^0.2") (d #t) (k 0)) (d (n "renetcode") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16mnkbj2i1iw802a41bkwxah4sg21wv7dr2c78h8g9kcphw64a4k") (f (quote (("default" "transport")))) (s 2) (e (quote (("transport" "dep:renetcode") ("serde" "dep:serde") ("bevy" "dep:bevy_ecs"))))))

