(define-module (crates-io re ne renegades_sqlite) #:use-module (crates-io))

(define-public crate-renegades_sqlite-0.1.1 (c (n "renegades_sqlite") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0nwvq9nplm2cvp43g0kjw3l4k9g6fdymqvpwnkdwa97z2kq5hwq0") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-renegades_sqlite-0.1.2 (c (n "renegades_sqlite") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1fvmwrwlyf27lbim6yx57bzw4vyv75zz5iqyv94cj909s51w0sis") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-renegades_sqlite-0.1.3 (c (n "renegades_sqlite") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0fnp9sriwxqr12x9l0kqm7pdrixmq6sfc5pblhvn74h7an41fkji") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

