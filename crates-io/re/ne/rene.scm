(define-module (crates-io re ne rene) #:use-module (crates-io))

(define-public crate-rene-0.0.0 (c (n "rene") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.16") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.16") (d #t) (k 1)))) (h "0msg4ka1y7x5w1n1z6cxl1ma6fylh6zprbi9zy9hjjzvic4086kh")))

(define-public crate-rene-0.1.0 (c (n "rene") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.19.0") (d #t) (k 1)) (d (n "pyo3-ffi") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rithm") (r "^14.0.0") (d #t) (k 0)) (d (n "traiter") (r "^4.0.0") (f (quote ("numbers"))) (d #t) (k 0)))) (h "0z3m590cy6ysr6kbyd58rhx612f2nwv3r5w4vy3rynxpayqjd29f") (r "1.71.0")))

