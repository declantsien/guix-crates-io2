(define-module (crates-io re ne renet_visualizer) #:use-module (crates-io))

(define-public crate-renet_visualizer-0.0.1 (c (n "renet_visualizer") (v "0.0.1") (d (list (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "renet") (r "^0.0.8") (d #t) (k 0)))) (h "1d80nvyrzg48gbcpcg9qy9iyr5ac20wxrbf333a82a20pf3azfm7")))

(define-public crate-renet_visualizer-0.0.2 (c (n "renet_visualizer") (v "0.0.2") (d (list (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "renet") (r "^0.0.9") (d #t) (k 0)))) (h "05zj2q3iyskq1lgpi0m5m9w360zih9pskw7r5lq13pnax54z479p")))

(define-public crate-renet_visualizer-0.0.3 (c (n "renet_visualizer") (v "0.0.3") (d (list (d (n "bevy_ecs") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "renet") (r "^0.0.10") (d #t) (k 0)))) (h "117538cingclaqqcgcl1x2y1maqza8vr7215sm7zjkx9lrca7fl1") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet_visualizer-0.0.4 (c (n "renet_visualizer") (v "0.0.4") (d (list (d (n "bevy_ecs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.21") (d #t) (k 0)) (d (n "renet") (r "^0.0.11") (d #t) (k 0)))) (h "1sb3l7a2jagq467fmk19va93c9barl8wjgdm84pg2zcfxm5sa4fg") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet_visualizer-0.0.5 (c (n "renet_visualizer") (v "0.0.5") (d (list (d (n "bevy_ecs") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.21") (d #t) (k 0)) (d (n "renet") (r "^0.0.12") (d #t) (k 0)))) (h "0s8km5sjpkdqmn7wyhld0z5wh9jdm0s7nklravgqrhp760q4y0qy") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet_visualizer-0.0.6 (c (n "renet_visualizer") (v "0.0.6") (d (list (d (n "bevy_ecs") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)))) (h "155xj1phjmxlkw5grqmk5p3y1g9qij6yaw7w6xb54h9a6xycpk4y") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet_visualizer-0.0.7 (c (n "renet_visualizer") (v "0.0.7") (d (list (d (n "bevy_ecs") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "renet") (r "^0.0.14") (d #t) (k 0)))) (h "0sfh8x3wczv2h3sxfmzvw884f4bbifz9cxykkxfb34n184mriqbf") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

(define-public crate-renet_visualizer-0.0.8 (c (n "renet_visualizer") (v "0.0.8") (d (list (d (n "bevy_ecs") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "renet") (r "^0.0.15") (d #t) (k 0)))) (h "0f829xz130qf6ljrgkbhng7gblxchij9xjlqzlkhzgr0zjl432nk") (s 2) (e (quote (("bevy" "dep:bevy_ecs"))))))

