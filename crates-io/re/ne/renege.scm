(define-module (crates-io re ne renege) #:use-module (crates-io))

(define-public crate-renege-0.1.0 (c (n "renege") (v "0.1.0") (h "1xlv338c2vwbbabncigxg9fbnxaczpw61hn49ym675aclf8q5r7q")))

(define-public crate-renege-0.1.1 (c (n "renege") (v "0.1.1") (h "1abjv3s6mld1ykf9k7jcccv2sw5pzrsa2l5a4wr1shkyv84zwwfv")))

(define-public crate-renege-0.1.2 (c (n "renege") (v "0.1.2") (h "0y7pwacs7ar48z88x423zka9w76f79pcm9lvy52d0xx1wds72pas")))

