(define-module (crates-io re da redact) #:use-module (crates-io))

(define-public crate-redact-0.0.1 (c (n "redact") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1idf9z5ln4v0g2667ijcfxvi2kg1qszrmkm0hh1z7da1i7mh7d71")))

(define-public crate-redact-0.0.2 (c (n "redact") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ryvppvfdni10azww4pbv9cdm2dzr4g0zn5f3rxfbmizxmdgf92g")))

(define-public crate-redact-0.0.3 (c (n "redact") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "106099jfbny775r5nyz0jparmawy5r3mpab30x0apprfy7w1f0lp")))

(define-public crate-redact-0.0.4 (c (n "redact") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0xkdz0faki27kppxai6rv1mq7nqiznzvyiad2qxv4039dllhlr0l")))

(define-public crate-redact-0.0.5 (c (n "redact") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hcq9j9yn5gpniakpz5cr76n6y7rz5lxj0wihl5gvjnwcfwy15zi")))

(define-public crate-redact-0.0.6 (c (n "redact") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yarnmq7m5w61vy9d528bk5z54vf1gq90yma80hfknz3w7bk0q5w")))

(define-public crate-redact-0.0.7 (c (n "redact") (v "0.0.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "11f0qrkqx4xzw74mn8ngcgjvl7x3nnil9r7xfw0hcxy98b6f92ha")))

(define-public crate-redact-0.0.8 (c (n "redact") (v "0.0.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16p7ymdl5cr25yn9v95k8p8n2gy2xiddxy03lww9aa0vhv2iakhq")))

(define-public crate-redact-0.0.9 (c (n "redact") (v "0.0.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1x4nr5nyc3hdzxsym6a1xlikvj39ssd0qxpa5kcphagl0c939gbf")))

(define-public crate-redact-0.0.10 (c (n "redact") (v "0.0.10") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0g7kn3mb05350ni9ww7i6bywka5r3wadm4mzkw5zalddx98dr343")))

(define-public crate-redact-0.0.11 (c (n "redact") (v "0.0.11") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0awr13fygpx7c73b3ms20wlymixdl801r1h9g9ba0zqirxd8cjxp") (f (quote (("std") ("default" "std"))))))

(define-public crate-redact-0.1.0 (c (n "redact") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1c9fwbk30jdcbsz57mcf064cs5zazhndimxd82q7bc1fyz96ki45") (f (quote (("std") ("default" "std"))))))

(define-public crate-redact-0.1.1-pre0 (c (n "redact") (v "0.1.1-pre0") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1syl695958nab3clbdibk7dcj10hrkbicr6vnans0chfwfhp3yid") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand"))))))

(define-public crate-redact-0.1.1 (c (n "redact") (v "0.1.1") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0wfvlhh2dbi4s8pw8c17yxcsiv9pkdj8s9mq09a3qx9jyr4p04sn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand"))))))

(define-public crate-redact-0.1.2 (c (n "redact") (v "0.1.2") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d59gafbnbk7mw6f5903339fc7hr1p0w0q937j4v20i8vkvcsv80") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand"))))))

(define-public crate-redact-0.1.3 (c (n "redact") (v "0.1.3") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j4r4pyazvqhyw5rd27jh3416h6c3g2nrpbd2x5ggacakikw35bb") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.4 (c (n "redact") (v "0.1.4") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11z4xvmmsxs660bd9qgpvsxlp068aghr0lla86880x4bdh13q2da") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.5 (c (n "redact") (v "0.1.5") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19gjschp21sd2bq8gwnnbcx0lqq2f3wqwg11xd0m7rcs66kc15qb") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.6 (c (n "redact") (v "0.1.6") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vf7r3bjabk7dp73vsn82cwjds3pr23zf175kyysdsy1y8wvqmsv") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.7 (c (n "redact") (v "0.1.7") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ga9m5vxwjlwqv4q43mzi263mnzjvcl4wwcwkrh04y5pd6xmnlh9") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.8 (c (n "redact") (v "0.1.8") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s0i4mwpnbq2pj8xjyr3xs3dvnz0v0sy6y5vgbnfh74jhc8r7k5v") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.9 (c (n "redact") (v "0.1.9") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "03s3akqdccqr0bx8kbjn63gmz6nfxldcq31j32gnqj75nmiihc5p") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize") ("serde" "dep:serde") ("fake" "dep:fake" "dep:rand")))) (r "1.65")))

(define-public crate-redact-0.1.10 (c (n "redact") (v "0.1.10") (d (list (d (n "fake") (r "^2.5") (o #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("std"))) (d #t) (k 2)))) (h "0sarksb146nspz2dky3h44q66y4m3la0y3zdn7nmcv89kd3fq800") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize") ("serde" "dep:serde") ("fake" "dep:fake" "dep:rand")))) (r "1.65")))

