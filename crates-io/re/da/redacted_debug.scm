(define-module (crates-io re da redacted_debug) #:use-module (crates-io))

(define-public crate-redacted_debug-0.1.0 (c (n "redacted_debug") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0zhi35hq3pcvj8r1hc0q3rqnpp5mim3g60q6x0igz2rrry85xmgk")))

(define-public crate-redacted_debug-0.2.0 (c (n "redacted_debug") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0i0nsvsqmbp86jb4yp948348nf483c9ihhzzva1rp6vsgq88zwjy")))

