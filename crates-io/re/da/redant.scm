(define-module (crates-io re da redant) #:use-module (crates-io))

(define-public crate-redant-0.1.0 (c (n "redant") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03nsw6zmxangghsbwndx4x4755s4hqn9ks7ppw42nj0386hhxxhw")))

(define-public crate-redant-0.1.1 (c (n "redant") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0wjl78k9iwk7n5js10msab21fn5dl7wag07mzkmgf428y42x5w92")))

(define-public crate-redant-0.1.2 (c (n "redant") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mygr2lfwk0w5ygsr94na7hhz9s2fgdgcwvgd1jmj9bqlwy5zn7z")))

(define-public crate-redant-0.1.3 (c (n "redant") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1z899nrj0dnf5z6i46jj2nq3hyblqwaff44q1928jdkxa2nfpl5k")))

(define-public crate-redant-0.1.4 (c (n "redant") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0qwhcvrf072ifb3si9il3hk5vm4gnamm5asrswf7mkblpihlkn3m")))

(define-public crate-redant-0.1.5 (c (n "redant") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0py5sm5sbb6g1b926jk2v4wv82pgz45i58cyscphn6r3xs25g3rz")))

(define-public crate-redant-0.1.6 (c (n "redant") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fb39fz5dv63x5i247blsajd14b7j4g03b1a0pv7ja6gngp76vky")))

(define-public crate-redant-0.1.7 (c (n "redant") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1w7906hpxzspnhws8x8p14d7xqnv02j72nqkd9p0wrv7yz96fr9p")))

(define-public crate-redant-0.1.8 (c (n "redant") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "regex" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "08wm76x8cdnr4diwd402c18m7czswk3nci9k6kl73plcrw6yiyjk")))

(define-public crate-redant-0.1.9 (c (n "redant") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo" "regex" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "11hxrpnnln3bbfmm56k0p2j8rv5mibsngv7pgkgi4y6m71hzwryh")))

