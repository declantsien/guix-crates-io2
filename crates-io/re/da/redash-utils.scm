(define-module (crates-io re da redash-utils) #:use-module (crates-io))

(define-public crate-redash-utils-0.1.0 (c (n "redash-utils") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ky6l2ylsnwmnjzrynfc4j8134l64nig424bvd0har7dxz4i843j")))

(define-public crate-redash-utils-0.1.1 (c (n "redash-utils") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f5b4mc5vhrls3cnl8wcp7nhg72c7ma6q7n0p7g5c735x2x19ih8")))

