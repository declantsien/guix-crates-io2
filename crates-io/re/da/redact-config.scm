(define-module (crates-io re da redact-config) #:use-module (crates-io))

(define-public crate-redact-config-1.0.0 (c (n "redact-config") (v "1.0.0") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lny1m9j67l5k6j7smbzhc7rxxg2hna2bgqdwgqy6664gca68gh0")))

(define-public crate-redact-config-1.0.1 (c (n "redact-config") (v "1.0.1") (d (list (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4xlflvxhk7pcd7y9qbdp7i6pjng22d13h6cqkx54qx9xma1v2w")))

(define-public crate-redact-config-1.0.2 (c (n "redact-config") (v "1.0.2") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ig3zxrmgfk94i87m5rccbs3aqv9zmiykvm5vdc0wp5ndx2ndsnm")))

