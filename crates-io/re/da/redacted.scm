(define-module (crates-io re da redacted) #:use-module (crates-io))

(define-public crate-redacted-0.0.0 (c (n "redacted") (v "0.0.0") (h "0c8hhg2a521sfym9d4pcw9jd851kxjiz9p2l8rpkjdjhp5gsindg")))

(define-public crate-redacted-0.1.0 (c (n "redacted") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (o #t) (d #t) (k 0)))) (h "1ih7aq727n49blxh5jqyzwyiy7aaxz20nq383r619l49n23wpv2p") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize"))))))

(define-public crate-redacted-0.1.1 (c (n "redacted") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (o #t) (d #t) (k 0)))) (h "048abckz61hswjb9057803yl9gvv65vppc7m2xy02l7ja8bd18v4") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize"))))))

(define-public crate-redacted-0.1.2 (c (n "redacted") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (o #t) (d #t) (k 0)))) (h "0ixsl9a7h25741cxnzczalwlflyzm0hvdh0rcx51lva9ajlxkxp1") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize"))))))

(define-public crate-redacted-0.2.0-alpha.0 (c (n "redacted") (v "0.2.0-alpha.0") (d (list (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (o #t) (d #t) (k 0)))) (h "024x7pr42wziri0byiyvrpqxc9nkpncazshpwws6w49px2hhh812") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize"))))))

(define-public crate-redacted-0.2.0-alpha.1 (c (n "redacted") (v "0.2.0-alpha.1") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (o #t) (d #t) (k 0)))) (h "0chi6fxx4d01yj7nflqr7b199z2f7mnr62r8rf2kn2xjjlxs2n4d") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize" "hex"))))))

(define-public crate-redacted-0.2.0-alpha.2 (c (n "redacted") (v "0.2.0-alpha.2") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (o #t) (d #t) (k 0)))) (h "0rr6vah506gpgsd2xh6afh31nci3hp33bxdx4j9vxyry14qvljcl") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize" "hex"))))))

(define-public crate-redacted-0.2.0-alpha.3 (c (n "redacted") (v "0.2.0-alpha.3") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (o #t) (d #t) (k 0)))) (h "04k5drd4n9g4asqhnnasazha4b5bz20qd779pl0s7c10n3mi12vb") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize" "hex"))))))

(define-public crate-redacted-0.2.0 (c (n "redacted") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (o #t) (d #t) (k 0)))) (h "0w9a287grja9vraj09hbjii711ijnv1zcgxag5s3hwn6sdpn5z3j") (f (quote (("serde_support" "serde" "serde_bytes") ("default" "serde_support" "zeroize" "hex"))))))

