(define-module (crates-io re da redactedsecret) #:use-module (crates-io))

(define-public crate-redactedsecret-0.4.0 (c (n "redactedsecret") (v "0.4.0") (d (list (d (n "bytes_crate") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0) (p "bytes")) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (k 0)))) (h "0iisi28hqv04iy2ya6l5zqndsj97pzhfgcvqwmj7058hmc2i0s7f") (f (quote (("default" "alloc") ("bytes" "bytes_crate" "zeroize/bytes") ("alloc" "zeroize/alloc"))))))

(define-public crate-redactedsecret-0.4.1 (c (n "redactedsecret") (v "0.4.1") (d (list (d (n "bytes_crate") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0) (p "bytes")) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (k 0)))) (h "18ygwjxplkpbn3d89vy0mh8v45pk3s6jfigrli0jmz16kl0038jn") (f (quote (("default" "alloc") ("bytes" "bytes_crate" "zeroize/bytes") ("alloc" "zeroize/alloc"))))))

