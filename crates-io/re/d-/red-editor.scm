(define-module (crates-io re d- red-editor) #:use-module (crates-io))

(define-public crate-red-editor-0.1.0 (c (n "red-editor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1bm4ln42jifj15bghjnq5glbd6ygcpcys383i61ahlfk51bpxks5")))

(define-public crate-red-editor-0.1.1 (c (n "red-editor") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1jdxgmg5j3dcjx43nly5ri3prngs4bb3s06v96wkcbbda28yj452")))

(define-public crate-red-editor-0.2.0 (c (n "red-editor") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "11k88z6k9zvkl7z399dg63kyjfnp8f6jya2jv5a1dwf7zfhy4w2b")))

(define-public crate-red-editor-0.2.1 (c (n "red-editor") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0bik9x9s8v4mz968y2hxw81x7axf76ss3x0bj2hgyv21liv9ri38")))

