(define-module (crates-io re d- red-it) #:use-module (crates-io))

(define-public crate-red-it-0.1.0 (c (n "red-it") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0y9bcazigh2qqdzkws20g1kmybfsjjnlqh0cm8mxv872h59vd37x")))

(define-public crate-red-it-0.1.1 (c (n "red-it") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0g59rrcg785w2bhpv8av4926mi3cd2sfmaw7qp0p6iyxxih8rzac")))

