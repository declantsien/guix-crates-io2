(define-module (crates-io re fp refptr-macros) #:use-module (crates-io))

(define-public crate-refptr-macros-0.1.0 (c (n "refptr-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n4rglbpaz07sxpgkyk0j10d7nyirw45k5b6zb1d25wz42palb9a")))

(define-public crate-refptr-macros-0.2.0 (c (n "refptr-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13igpfh24884yaqz2n8akcz1ysi699x3b7jpavg22a2jqvad4gph")))

