(define-module (crates-io re fp refptr) #:use-module (crates-io))

(define-public crate-refptr-0.1.0 (c (n "refptr") (v "0.1.0") (d (list (d (n "refptr-macros") (r "^0.1.0") (d #t) (k 0)))) (h "038i63c871aw49618pw23aclslqsjqacvrv6yl9g93ai8n94qrly") (f (quote (("default" "atomic") ("atomic"))))))

(define-public crate-refptr-0.2.0 (c (n "refptr") (v "0.2.0") (d (list (d (n "refptr-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1l4wnxxypgj20j3x7v7xai23c7x1b3xgcp0h8jv19dmk26gwzq8r") (f (quote (("default" "atomic") ("atomic"))))))

