(define-module (crates-io re fp refparser) #:use-module (crates-io))

(define-public crate-refparser-0.1.0 (c (n "refparser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1whxr5gaflafmnydy70qp7g46pd2yhxwrx4d0byikgbn2iz0r9l4")))

