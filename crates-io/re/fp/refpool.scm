(define-module (crates-io re fp refpool) #:use-module (crates-io))

(define-public crate-refpool-0.1.0 (c (n "refpool") (v "0.1.0") (h "1qw3f4l8jcby12lc47s0s7w9njkawya6xzzfs5c07wjixk0ffwwm")))

(define-public crate-refpool-0.2.0 (c (n "refpool") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0vrmc8d5x7a0cpwlf42yjyd7y2gd1zic7crp53c8pq71zka14xh3") (f (quote (("sync" "crossbeam-queue"))))))

(define-public crate-refpool-0.2.1 (c (n "refpool") (v "0.2.1") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1wss2ldxc9wc1szcmhdyxy4dmsfz3nnlpx1nm0i1rv1nk9j2wam1") (f (quote (("sync" "crossbeam-queue"))))))

(define-public crate-refpool-0.2.2 (c (n "refpool") (v "0.2.2") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0cckmalm4ls6rkgmvyxdzb3wxvzgibiyw7bshm6p4vfqj55462xm") (f (quote (("sync" "crossbeam-queue"))))))

(define-public crate-refpool-0.2.3 (c (n "refpool") (v "0.2.3") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0kd2qv9fnaln5ih8ggwpqgl6ygzr6gf1hpmf68ck8g36d3wpjs4m") (f (quote (("sync" "crossbeam-queue"))))))

(define-public crate-refpool-0.3.0 (c (n "refpool") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1y9vsk5qv62n9f34agx5lfg8s68sg7zqbxr1r0y6x3m6myvxfs3i")))

(define-public crate-refpool-0.3.1 (c (n "refpool") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "19pmjg8cvh0yz2s6qmgh167p8pgqh4asf3pjrqvcx2lhcj8zmprp")))

(define-public crate-refpool-0.4.0 (c (n "refpool") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "13d9whn5790s3safiwnnvpcz01m7kv8n324zhmzlcrzafw6shnyf") (f (quote (("default_impl") ("default"))))))

(define-public crate-refpool-0.4.1 (c (n "refpool") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1nf4jy7ch871wjg2950xalybn3lwvgxqdx0vz2izck3pj8jz2ikb") (f (quote (("default_impl") ("default"))))))

(define-public crate-refpool-0.4.2 (c (n "refpool") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1dagmgdhxvldxvphhibzb4ncw8m9avvvffxwca6kky2qr6yizfky") (f (quote (("default_impl") ("default"))))))

(define-public crate-refpool-0.4.3 (c (n "refpool") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0n6b1qpq0fcyzm3nrmiw0z9x4nawy9dklqfr3mb8rp571yw8d7in") (f (quote (("default_impl") ("default"))))))

