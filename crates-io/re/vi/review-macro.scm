(define-module (crates-io re vi review-macro) #:use-module (crates-io))

(define-public crate-review-macro-0.1.0 (c (n "review-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bwj6n2ka8x7pmp2bvya9bd5hphdb7h5b2zg15mjphs040nmfcp9")))

(define-public crate-review-macro-0.2.0 (c (n "review-macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "09vj7sqswdywv9v49h7p2026r37pxv56cd2nmh6d2qw2ddig78xi")))

(define-public crate-review-macro-0.3.0 (c (n "review-macro") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "03nbpc5a10p7hlm37w9k8fbqp9glbyl21dicj8n2apwb5gr6bf28") (r "1.56.0")))

(define-public crate-review-macro-0.4.0 (c (n "review-macro") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "05g6n2wyz8nsv5y0wb1nqf38pdisfnbxzq9y7sjhd5yam947d9bs") (r "1.56.0")))

(define-public crate-review-macro-0.4.1 (c (n "review-macro") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "15v5najfjvjv932fwhskzj3d5x4pv8yvk1w7khgvdyvzxa5n2abl") (r "1.56.0")))

