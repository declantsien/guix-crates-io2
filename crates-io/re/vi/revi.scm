(define-module (crates-io re vi revi) #:use-module (crates-io))

(define-public crate-revi-0.0.0-beta-0.1 (c (n "revi") (v "0.0.0-beta-0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "0x6341bhkfz976n91300zjn965c917prrl1nb42vlwj9qqw9vfy5")))

(define-public crate-revi-0.0.0-beta-0.2 (c (n "revi") (v "0.0.0-beta-0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "15l5fx1z9z1bdqyygj6vl84d6qcv67s6ybhzwc09rb09nrz8z6bm")))

(define-public crate-revi-0.0.0-beta-0.3 (c (n "revi") (v "0.0.0-beta-0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "0p4j3sjfqywn4qbx0dssrbqna7iif7hsl74abypcigvyrr9sq83i")))

(define-public crate-revi-0.0.0-beta-0.4 (c (n "revi") (v "0.0.0-beta-0.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "1nq355wpn5vrff0cj3qkgsljzhn8lxkkjwg1xkcarzxflfk17gnk") (f (quote (("debug_bar"))))))

(define-public crate-revi-0.0.0-beta-0.4.1 (c (n "revi") (v "0.0.0-beta-0.4.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "1wv41jac3q8wpmgj5n05x2hn2nbip5cbggy7z915pbya18ily1bg") (f (quote (("debug_bar"))))))

(define-public crate-revi-0.0.0-beta-0.5 (c (n "revi") (v "0.0.0-beta-0.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mlua") (r "^0.6.0-beta.3") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "ropey") (r "^1.2.0") (d #t) (k 0)))) (h "0ghpq8kkgm0b1q4rdww1k6xkzpagvin6mg9765qqwis3axgdfvwf") (f (quote (("debug_line") ("debug_bar"))))))

(define-public crate-revi-0.0.0-beta-0.6 (c (n "revi") (v "0.0.0-beta-0.6") (d (list (d (n "revi-core") (r "^0.0.0-beta-0.6") (d #t) (k 0)) (d (n "revi-ui") (r "^0.0.0-beta-0.6") (d #t) (k 0)))) (h "1jvqkljfl0ja0cfzv61spk0d1f4k2fpchlj3f86ll6fdgxvk0lxc") (f (quote (("debug_line_words" "revi-core/debug_line_words") ("debug_line" "revi-core/debug_line") ("debug_input_number" "revi-core/debug_input_number") ("debug_bar" "revi-core/debug_bar"))))))

(define-public crate-revi-0.0.0-beta-1 (c (n "revi") (v "0.0.0-beta-1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mlua") (r "^0.6.1") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "revi-core") (r "^0.0.0-beta-1") (d #t) (k 0)) (d (n "revi-ui") (r "^0.0.0-beta-1") (d #t) (k 0)))) (h "0arpp0r23p20cy88j86zrdjl5a5w9yv009y5ryvv3f4vcvnhx7d6")))

(define-public crate-revi-0.0.0-beta-2 (c (n "revi") (v "0.0.0-beta-2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mlua") (r "^0.6.1") (f (quote ("lua53" "vendored"))) (d #t) (k 0)) (d (n "revi-core") (r "^0.0.0-beta-2") (d #t) (k 0)) (d (n "revi-ui") (r "^0.0.0-beta-2") (d #t) (k 0)))) (h "1pl6hcm2hb14n5w1452dx48m4kapkw1wbqm32w46adh7n29g4zv0")))

