(define-module (crates-io re vi revi-ui) #:use-module (crates-io))

(define-public crate-revi-ui-0.0.0-beta-0.6 (c (n "revi-ui") (v "0.0.0-beta-0.6") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0qdhrdb6pxghflnxgrzgw48j899cc4w4k1s6wvjf0phyaclxpmh5")))

(define-public crate-revi-ui-0.0.0-beta-1 (c (n "revi-ui") (v "0.0.0-beta-1") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0jm8nv8kh9zqg5b580hdflkm6y5ai4rdwrsl3wd74y45jjngxny2")))

(define-public crate-revi-ui-0.0.0-beta-2 (c (n "revi-ui") (v "0.0.0-beta-2") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0pqvjl3bdj0dqrza0izwyg3as7s6h9pvkap5wwm06yd3mb85arkk")))

