(define-module (crates-io re vi revise) #:use-module (crates-io))

(define-public crate-revise-0.0.0-beta (c (n "revise") (v "0.0.0-beta") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "14lbrf4xml4zbanxk2pdwvxp3ya47l77ck2rk7zhj5nr2h8q4r5m")))

(define-public crate-revise-0.1.0 (c (n "revise") (v "0.1.0") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "088akkf2sgx7973xml7b6mq4zhajq2jwcj8bf8pmhmjw6c4nmhx5")))

(define-public crate-revise-0.1.1 (c (n "revise") (v "0.1.1") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0rfphdcn3psh1skrs3bnfhwlyf4q7q1malcz7sh0fd9yzfw9n9jp")))

(define-public crate-revise-0.1.2 (c (n "revise") (v "0.1.2") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1m6lr2qwzknf896ll6snaz6n7blm853bngzlv18978b540340w5l")))

(define-public crate-revise-0.1.3 (c (n "revise") (v "0.1.3") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1hkvw7f9ql40vy8kpbiv6zwjw5b6j06xw245p439i8979xy653gj")))

(define-public crate-revise-0.1.4 (c (n "revise") (v "0.1.4") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1wh6alm3n2mpwbmrwrqym2vg00k39fksrn3jpj9ppqhv9rwnkckw")))

(define-public crate-revise-0.1.5 (c (n "revise") (v "0.1.5") (d (list (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "15438ni8cg57sc60i1vzxgxrbbj2vy97f0c98gzsswgf29ydgmlf")))

