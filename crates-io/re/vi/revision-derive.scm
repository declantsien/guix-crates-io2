(define-module (crates-io re vi revision-derive) #:use-module (crates-io))

(define-public crate-revision-derive-0.1.0 (c (n "revision-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1lpa3bvm1vikvr6ppgmcyy2yy69815j0d64qpqllnm0cdk8yk6sq")))

(define-public crate-revision-derive-0.2.0 (c (n "revision-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0ksqxxphxx07yl5wxnmsq337kyr51y4ggxmyrfmhhw5yj23n6wgf")))

(define-public crate-revision-derive-0.2.1 (c (n "revision-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0zzmbxwzrh4z1w5k9m34xl2drcs8dv5ix4c0m3gywy4f337lxsbc")))

(define-public crate-revision-derive-0.2.2 (c (n "revision-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0vgmm9d87w9jgqw0vf7v4ijzwdl665y2ckcvm3szi1mvy6j2g2bf")))

(define-public crate-revision-derive-0.2.3 (c (n "revision-derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1knvgnwal1l9sfz77z96pxvb7hq33r0rhdafv0qdcyg2ps8923v9")))

(define-public crate-revision-derive-0.3.0 (c (n "revision-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1yj6kgiizgwi1v9jiz6iprvqnp9k2gxms7nw0cfkb6z6aj7kf7kr")))

(define-public crate-revision-derive-0.4.0 (c (n "revision-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "11f72fj1c2rfi8l82wnphn85xx6nr2z2rg82jnhkj1ilnkqbmhhl")))

(define-public crate-revision-derive-0.5.0 (c (n "revision-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1lqfws9043g14knq5dwc5s3fh4nsdl6c1dcraz1vw78zyv2nz6dz")))

(define-public crate-revision-derive-0.6.0 (c (n "revision-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "12cign66jligwgxnpm058j55pz0a3702nqzrc6njxmv1g06ckdf5")))

(define-public crate-revision-derive-0.7.0 (c (n "revision-derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "11bw6w1676ml44pssg76b3rfkvya844qg1j4bsmhlkjdg6vg0kw5")))

