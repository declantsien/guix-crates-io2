(define-module (crates-io re mi remit) #:use-module (crates-io))

(define-public crate-remit-0.1.0 (c (n "remit") (v "0.1.0") (h "0ajj50xpgm628szfgcmgp6dwicp7fmpn1j5m1m7fi2f3bpin53kg")))

(define-public crate-remit-0.1.1 (c (n "remit") (v "0.1.1") (h "1rhwmjnvrqi6slclz6llpvlxjlbxicy45bl94xj0ha87d0z0p1nr")))

(define-public crate-remit-0.1.2 (c (n "remit") (v "0.1.2") (h "18jps59a63qd2sl7yhqcpbc090wsyfbik7560b3dzkxhll78z08i") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-remit-0.1.3 (c (n "remit") (v "0.1.3") (h "0b7gxmc0xnyvmghzds8csaabx08vjjmgmif6flxm7hdfj702w3qr") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-remit-0.1.4 (c (n "remit") (v "0.1.4") (h "1m0npv81hbg53p2lpxlap87xzdxyr91igz3pkvzkqq0jp4vjfcpf") (f (quote (("default" "alloc") ("alloc"))))))

