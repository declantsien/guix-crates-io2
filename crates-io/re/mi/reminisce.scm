(define-module (crates-io re mi reminisce) #:use-module (crates-io))

(define-public crate-reminisce-0.0.1 (c (n "reminisce") (v "0.0.1") (h "17laz3g5b1xkdgzr6sh1bsbhyxx0wlrvkj0ni3bdj9bmjmvlypd1")))

(define-public crate-reminisce-0.0.2 (c (n "reminisce") (v "0.0.2") (h "1fl0p091s5krxa44j2kih4wmblbivh09rgvqjfjxcdgnd0cjr7kc")))

(define-public crate-reminisce-0.0.3 (c (n "reminisce") (v "0.0.3") (h "1q9aaqxfl7zr3jp8sa9yw1k0v3v28g3y0648n9r8qqcyxvyz2yz7")))

(define-public crate-reminisce-0.0.4 (c (n "reminisce") (v "0.0.4") (h "0axbkd7afpppzzxngmzr3q3d8f4pi4c7wwx2n1a7pvxkl0srrahp")))

(define-public crate-reminisce-0.0.5 (c (n "reminisce") (v "0.0.5") (h "15azv2w8vrsgvahin27l4735p2gc901y3zzqswn48zbjfq4w07mk")))

(define-public crate-reminisce-0.0.6 (c (n "reminisce") (v "0.0.6") (h "1zi7pdyx4cnxvrdqfrbyfnk5c0lz9sv8cg2lx3xkcap381yahdjf")))

(define-public crate-reminisce-0.0.7 (c (n "reminisce") (v "0.0.7") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "1bmrm50w2k18bg5kg2cav7knifm6724cvfzqscq58ki4c6vmn7m9") (f (quote (("sdl" "sdl2") ("default"))))))

(define-public crate-reminisce-0.0.8 (c (n "reminisce") (v "0.0.8") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "1zlqgy32pm0mqr9nyk45gn9x1f6lc2y3iqcfvk4p0y7zlvkda7d7") (f (quote (("sdl" "sdl2") ("default"))))))

(define-public crate-reminisce-0.0.9 (c (n "reminisce") (v "0.0.9") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "06l8y3a7pfs1fdmj3fhh5rx2jlysvhmmpa5fz5n863c2hlzac2cg") (f (quote (("sdl" "sdl2") ("default"))))))

(define-public crate-reminisce-0.1.0 (c (n "reminisce") (v "0.1.0") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "18k2243bq6gi14kicr6jnrjjdzzm8fyd340qnpj6wgcyn5q72m4v") (f (quote (("sdl" "sdl2") ("default"))))))

(define-public crate-reminisce-0.2.0 (c (n "reminisce") (v "0.2.0") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "147vxai82sln48wqnp3569sm8mhczz531ngiq5g7mngj59qp7la2") (f (quote (("sdl" "sdl2") ("default"))))))

(define-public crate-reminisce-0.2.1 (c (n "reminisce") (v "0.2.1") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "1yrbfk9l3ph0lff5fcaq5529adrj7rs6nilvi8p1qfih7ajx27ng") (f (quote (("sdl" "sdl2") ("mappings") ("default"))))))

(define-public crate-reminisce-0.9.0 (c (n "reminisce") (v "0.9.0") (d (list (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "0y7y86gzxin0mjwcq2p0ydb7lrhvcrj22v1hmfnylvi89g0sh6aq") (f (quote (("sdl" "sdl2") ("mappings") ("default"))))))

(define-public crate-reminisce-0.9.1 (c (n "reminisce") (v "0.9.1") (d (list (d (n "libc") (r "*") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "*") (o #t) (d #t) (k 0)))) (h "15fzqr32lz7w86qg4a13vii5a53ipvfiaw5xvqin5y4maszb4ygc") (f (quote (("sdl" "sdl2") ("mappings" "libc") ("default" "libc"))))))

