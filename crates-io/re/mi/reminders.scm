(define-module (crates-io re mi reminders) #:use-module (crates-io))

(define-public crate-reminders-0.1.0 (c (n "reminders") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)))) (h "0h0yjvjm448msyvzsdjdgx31nh7n0jzh63kpynzi7z6xckpazas3")))

(define-public crate-reminders-0.1.1 (c (n "reminders") (v "0.1.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)))) (h "05nz2pkdgqdmwbpnnxhkx45gls6i0p8479q1val6yiyxg5ivfk1x")))

(define-public crate-reminders-0.1.2 (c (n "reminders") (v "0.1.2") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)))) (h "09vh61wcp90jzlrnlyq9cy2957jva2i73cy2ggb8663zvk3gnhdq")))

(define-public crate-reminders-0.1.3 (c (n "reminders") (v "0.1.3") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)))) (h "1gx6g7r9k0n8dgkcr413imi7ynvy487kiv7ng6iy1079pgjf9irf")))

