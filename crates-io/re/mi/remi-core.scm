(define-module (crates-io re mi remi-core) #:use-module (crates-io))

(define-public crate-remi-core-0.1.0 (c (n "remi-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)))) (h "1asq7hcp5i2yj94jn9d44ia1rar9vc08sk5r1xivnvyz01as15vq") (y #t)))

(define-public crate-remi-core-0.2.0 (c (n "remi-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)))) (h "1wwbgqyxmw66dywmi1y5h6y4cxs9dmyzs6qrkpi2jlacd1y4j757") (y #t)))

(define-public crate-remi-core-0.2.1 (c (n "remi-core") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "146cwm2hpmhyyl0r64n9f3q6dpdxr1xqj5khq9yara746cyk0vnn") (y #t)))

(define-public crate-remi-core-0.3.0 (c (n "remi-core") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "0l714ajnmiw3qga86xhbv1qzk4hwg89pm3cnbq901xdslsgn6nsr") (y #t)))

(define-public crate-remi-core-0.4.0 (c (n "remi-core") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "1xygkkj27yljn1j0ygh7gax0jcgklpldv1h2smddbsw8smvm4p3j") (y #t) (r "1.67.0")))

(define-public crate-remi-core-0.4.1 (c (n "remi-core") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "14qdbz2m9jwd3yij0mhsjhkzbk63knr0vvnp9q5amh1r0sldfg71") (y #t) (r "1.67.0")))

(define-public crate-remi-core-0.4.2 (c (n "remi-core") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)))) (h "1pzgqcnqfyihg2d4wd0jbfvc3vjg7c8njll752hr0ngrb2jm1mak") (y #t) (r "1.67.0")))

(define-public crate-remi-core-0.4.3 (c (n "remi-core") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)))) (h "05d9f5xfkcw90jxlbxafv1i8m0rmpy4c1qi58r82hzfmgmvsb69s") (y #t) (r "1.67.0")))

