(define-module (crates-io re mi remi) #:use-module (crates-io))

(define-public crate-remi-0.1.0 (c (n "remi") (v "0.1.0") (d (list (d (n "remi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-gridfs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-s3") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1isxgknggjww34sk9jfqvfgvf21nkci5d6ijkxn5mmqkdbly340b") (f (quote (("default" "fs")))) (s 2) (e (quote (("s3" "dep:remi-s3") ("gridfs" "dep:remi-gridfs") ("fs" "dep:remi-fs"))))))

(define-public crate-remi-0.1.1 (c (n "remi") (v "0.1.1") (d (list (d (n "remi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "remi-gridfs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-s3") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0x230y3bwvp3ns457ckw5aavgwi0wjsgvy6yi10akm2ip6b7227l") (f (quote (("default" "fs")))) (s 2) (e (quote (("s3" "dep:remi-s3") ("gridfs" "dep:remi-gridfs") ("fs" "dep:remi-fs"))))))

(define-public crate-remi-0.1.2 (c (n "remi") (v "0.1.2") (d (list (d (n "remi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "remi-gridfs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-s3") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "094bdvjkhifi03hlmsa938781m5hqjf03m6w6m1kp7f015gnnd0f") (f (quote (("default" "fs")))) (s 2) (e (quote (("s3" "dep:remi-s3") ("gridfs" "dep:remi-gridfs") ("fs" "dep:remi-fs"))))))

(define-public crate-remi-0.1.3 (c (n "remi") (v "0.1.3") (d (list (d (n "remi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "remi-gridfs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-s3") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1rs57bczqh9crnds8ycal6z3ap9g4ki9p5q0j70jlwxx4f6sjd7q") (f (quote (("default" "fs")))) (s 2) (e (quote (("s3" "dep:remi-s3") ("gridfs" "dep:remi-gridfs") ("fs" "dep:remi-fs"))))))

(define-public crate-remi-0.1.4 (c (n "remi") (v "0.1.4") (d (list (d (n "remi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "remi-gridfs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "remi-s3") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0hmr75h90ylhny02qnhml9gxdm09vi6hi6b0addmv4y8k3a1c93b") (f (quote (("default" "fs")))) (s 2) (e (quote (("s3" "dep:remi-s3") ("gridfs" "dep:remi-gridfs") ("fs" "dep:remi-fs"))))))

(define-public crate-remi-0.2.0 (c (n "remi") (v "0.2.0") (d (list (d (n "remi-core") (r "^0.2.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.2.0") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.2.0") (o #t) (k 0)) (d (n "remi-s3") (r "^0.2.0") (o #t) (k 0)))) (h "18276s9fs3qdka7slcwh2qrx5rqafi0hipipzv9s3vqpxqh6r5h2") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs"))))))

(define-public crate-remi-0.2.1 (c (n "remi") (v "0.2.1") (d (list (d (n "remi-core") (r "^0.2.1") (d #t) (k 0)) (d (n "remi-fs") (r "^0.2.1") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.2.1") (o #t) (k 0)) (d (n "remi-s3") (r "^0.2.1") (o #t) (k 0)))) (h "06aslb3j1zs5290sj1hayzrl200niyyrn88v9j4l9kgrjc35k6ni") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs"))))))

(define-public crate-remi-0.3.0 (c (n "remi") (v "0.3.0") (d (list (d (n "remi-core") (r "^0.3.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.3.0") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.3.0") (o #t) (k 0)) (d (n "remi-s3") (r "^0.3.0") (o #t) (k 0)))) (h "1497s05xyc5iyxi999rgx71v25j6fcqsajhyfr7fc41r73r3xzw9") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs"))))))

(define-public crate-remi-0.4.0 (c (n "remi") (v "0.4.0") (d (list (d (n "remi-core") (r "^0.4.0") (d #t) (k 0)) (d (n "remi-fs") (r "^0.4.0") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.4.0") (o #t) (k 0)) (d (n "remi-s3") (r "^0.4.0") (o #t) (k 0)))) (h "0qgj5x773ni4qyimzpksikzk74vgpygzr4mmqfsy09i871wdambv") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs")))) (r "1.67.0")))

(define-public crate-remi-0.4.1 (c (n "remi") (v "0.4.1") (d (list (d (n "remi-core") (r "^0.4.1") (d #t) (k 0)) (d (n "remi-fs") (r "^0.4.1") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.4.1") (o #t) (k 0)) (d (n "remi-s3") (r "^0.4.1") (o #t) (k 0)))) (h "07qa6gnd5fcgs2l8rps55iv7pp7f7pnyphqvgg8qjs3xmr4vlk7y") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs")))) (r "1.67.0")))

(define-public crate-remi-0.4.2 (c (n "remi") (v "0.4.2") (d (list (d (n "remi-core") (r "^0.4.2") (d #t) (k 0)) (d (n "remi-fs") (r "^0.4.2") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.4.2") (o #t) (k 0)) (d (n "remi-s3") (r "^0.4.2") (o #t) (k 0)))) (h "196av6wjr1lmc06r83rawwgdy1bp7jxsnxsw2smjmbnrlcdblxms") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs")))) (r "1.67.0")))

(define-public crate-remi-0.4.3 (c (n "remi") (v "0.4.3") (d (list (d (n "remi-core") (r "^0.4.3") (d #t) (k 0)) (d (n "remi-fs") (r "^0.4.3") (o #t) (k 0)) (d (n "remi-gridfs") (r "^0.4.3") (o #t) (k 0)) (d (n "remi-s3") (r "^0.4.3") (o #t) (k 0)))) (h "0lvr6cxbcsa250mb0ap6dx93shwx8ipr4dvfkk73yj4nm6wlhczi") (f (quote (("serde" "remi-fs/serde" "remi-s3/serde" "remi-gridfs/serde") ("s3" "remi-s3") ("gridfs" "remi-gridfs") ("fs" "remi-fs") ("default" "fs")))) (r "1.67.0")))

(define-public crate-remi-0.5.0 (c (n "remi") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "1aqa9mlzcm01qnx67fij381vinkj5azsjvwj6qgs77i2rq3py943") (r "1.71.0")))

(define-public crate-remi-0.6.0 (c (n "remi") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "051wgc0mb6iyhddkpa2ij99w40n1svmjzv60z55ydalb22pgjac9") (r "1.74.0")))

(define-public crate-remi-0.6.1 (c (n "remi") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "0c3scbqc096b55aivbhvjv1xhkkv29bd92i2i03b5fln6rz46a8i") (r "1.74.0")))

(define-public crate-remi-0.6.2 (c (n "remi") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "1qxhsadlq8g58iap0sayl15h4pf6pcdbv4v5g2cxhkly9cgldlxc") (r "1.74.0")))

(define-public crate-remi-0.6.3 (c (n "remi") (v "0.6.3") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "06i24lg9a68rpgfd2qyq4ch6p9wxs3pjn2gb4jgq1v1pgacg1r4m") (r "1.74.0")))

(define-public crate-remi-0.7.0 (c (n "remi") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "0jmdb83cbsb66bqmc49pvllbiramljrs7ivj1mkpnq2afkpswwmy") (r "1.74.0")))

(define-public crate-remi-0.7.1 (c (n "remi") (v "0.7.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "0jskydwji7vkr82pl07v21bch2dn3bnkf9vkd4vcw4w1hh9lp7cn") (r "1.74.0")))

(define-public crate-remi-0.8.0 (c (n "remi") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "0xajxfh3b3lq303b2bhiw22dr4fmlqnjkrxpgnhnr2rplb7x36ga") (r "1.76")))

