(define-module (crates-io re kt rekt-protocol-common) #:use-module (crates-io))

(define-public crate-rekt-protocol-common-0.1.0 (c (n "rekt-protocol-common") (v "0.1.0") (h "1avr00lv7w3fkjghms47ykfb2zggj9kmfxnpiffyfxbxlqjcqblg")))

(define-public crate-rekt-protocol-common-0.1.1 (c (n "rekt-protocol-common") (v "0.1.1") (h "0vqpl5ki9nziss6czk18wjzjxban1cciwa6v7kma9y2k73y2r85m")))

