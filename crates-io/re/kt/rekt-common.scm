(define-module (crates-io re kt rekt-common) #:use-module (crates-io))

(define-public crate-rekt-common-0.1.2 (c (n "rekt-common") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "17m59b9185kbkf1rzkr6c8mnsfbl9w3r3dbbqlyidfg7y9a6qlz7")))

