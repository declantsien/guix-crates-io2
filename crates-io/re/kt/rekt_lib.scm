(define-module (crates-io re kt rekt_lib) #:use-module (crates-io))

(define-public crate-rekt_lib-0.1.3 (c (n "rekt_lib") (v "0.1.3") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0kxkfns1n4pxkrgs71i8rcqqbznml01gsq4g2ks5cajq3lysiz4f")))

(define-public crate-rekt_lib-0.1.4 (c (n "rekt_lib") (v "0.1.4") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0p3024p75dg16a5y4lzj8q6clf61ljkwk1j74d10jx9qv1gjrd0l")))

(define-public crate-rekt_lib-0.1.5 (c (n "rekt_lib") (v "0.1.5") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0ph7xix61nwcni79mcpwi1f6cc7cggflbq15sb9h4r9k1b9jh7bc")))

(define-public crate-rekt_lib-0.1.6 (c (n "rekt_lib") (v "0.1.6") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1fi59f2zvzlrcx0cmrl4d15553yms2ihlvk9k7wlrl2w1jidfawn")))

