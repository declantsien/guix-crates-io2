(define-module (crates-io re va revault-gui) #:use-module (crates-io))

(define-public crate-revault-gui-0.0.1 (c (n "revault-gui") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "iced") (r "^0.2") (f (quote ("svg"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-embed") (r "^5.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uds_windows") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)))) (h "12d3pyac0n28xxsh1y8d52gqkn36gidn67cqj6q6yfs18cyj9k8h")))

