(define-module (crates-io re ti retina-common) #:use-module (crates-io))

(define-public crate-retina-common-0.3.0 (c (n "retina-common") (v "0.3.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)))) (h "01hxbqbws74lhk6rwk84vxnp5s5dysh6s1nzh9ic63lpw83jg18j")))

