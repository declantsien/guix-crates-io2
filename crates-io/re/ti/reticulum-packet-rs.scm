(define-module (crates-io re ti reticulum-packet-rs) #:use-module (crates-io))

(define-public crate-reticulum-packet-rs-0.1.0 (c (n "reticulum-packet-rs") (v "0.1.0") (h "18ns033nrrg4hxy8a1zwxz6qqn0jwhfnxm3c5wspq9lsfhqa6gad")))

(define-public crate-reticulum-packet-rs-0.2.0 (c (n "reticulum-packet-rs") (v "0.2.0") (h "0xznf7gkmdzh79m1m3i8zxa3ssa3777q3ap9xw9vp7zrmy0l4hy9")))

