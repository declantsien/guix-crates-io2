(define-module (crates-io re ti reti) #:use-module (crates-io))

(define-public crate-reti-0.2.0 (c (n "reti") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "config") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "2.*") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "18hvg5225fnzm7j43c2mk9jq9c99k7lrl2d36chkqdm0ngl95dmz")))

