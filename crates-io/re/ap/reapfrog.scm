(define-module (crates-io re ap reapfrog) #:use-module (crates-io))

(define-public crate-reapfrog-0.1.0 (c (n "reapfrog") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "15g2m3nb0pcsm506s96ivjz2nhb8hgrm7a2wzjsplqx46cyllhc0")))

(define-public crate-reapfrog-0.1.1 (c (n "reapfrog") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "0blqlpj8ldvdrvp9w191sivnhcfrxnm8k4j3p19x64wry9dzil9i")))

(define-public crate-reapfrog-0.2.0 (c (n "reapfrog") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "0h22chnxsvkl9aw7hfvlqgk4s15sv48jza1j00r9v6nrc8lkny5c")))

