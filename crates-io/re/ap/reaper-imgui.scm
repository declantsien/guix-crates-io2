(define-module (crates-io re ap reaper-imgui) #:use-module (crates-io))

(define-public crate-reaper-imgui-0.1.1 (c (n "reaper-imgui") (v "0.1.1") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "rea-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "rea-rs-low") (r "^0.1.2") (d #t) (k 0)) (d (n "rea-rs-macros") (r "^0.1.2") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)))) (h "1dpcvyj5hgwdknijyfjyhsn6b0pwhxj3wpvc4bw9sy3zyiagnwxk")))

(define-public crate-reaper-imgui-0.1.2 (c (n "reaper-imgui") (v "0.1.2") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "rea-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "rea-rs-low") (r "^0.1.2") (d #t) (k 0)) (d (n "rea-rs-macros") (r "^0.1.2") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)))) (h "0jpmmwaj2dn18s1vxda85g0jw0l2n2b1rcnq01gvyqndkipg8sjh")))

