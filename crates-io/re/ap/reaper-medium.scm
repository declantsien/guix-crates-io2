(define-module (crates-io re ap reaper-medium) #:use-module (crates-io))

(define-public crate-reaper-medium-0.1.0 (c (n "reaper-medium") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6") (f (quote ("not_literal"))) (d #t) (k 0)) (d (n "helgoboss-midi") (r "^0.1.0") (d #t) (k 0)) (d (n "reaper-low") (r "^0.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "12z82kx7v6xk280wk8nwvi4c32cp5xdq4ha0pghc1pv688jl94zl")))

