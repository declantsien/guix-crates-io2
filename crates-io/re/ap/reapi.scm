(define-module (crates-io re ap reapi) #:use-module (crates-io))

(define-public crate-reapi-0.1.0 (c (n "reapi") (v "0.1.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tonic") (r "^0.11") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (f (quote ("cleanup-markdown"))) (d #t) (k 1)))) (h "1bdlqps2dsiz1bpa6xlkd3wz8skjndwya53ka7vfabbr40q5ncah") (f (quote (("eq") ("default" "serde" "eq" "buildgrid") ("buildgrid"))))))

