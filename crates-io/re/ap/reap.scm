(define-module (crates-io re ap reap) #:use-module (crates-io))

(define-public crate-reap-0.1.0 (c (n "reap") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09pxsa9zrp7y68ifv7k56yvfjjll0bb5cq2165hlbrhp7kjqm2d1")))

(define-public crate-reap-0.2.0 (c (n "reap") (v "0.2.0") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "0mjdnrdr32d17l24801d21g47yzca7f6xngnx4axzk74zza7lia3")))

(define-public crate-reap-0.2.1 (c (n "reap") (v "0.2.1") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "16qa3nishk9pd2w3s5y2rxps6958zn4m9d4qmgxdq7wlihs8fy7j")))

(define-public crate-reap-0.3.0 (c (n "reap") (v "0.3.0") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "inferno") (r "^0.8") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "09xh7al18ia09iqskgm1dw744qsm5km7qqvdmj8bx6fa7i8a6i02")))

(define-public crate-reap-0.3.1 (c (n "reap") (v "0.3.1") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "1zdvhgq117llwi3yrad093x83hfw4zgkq9wykdah7sag97qj68jx")))

(define-public crate-reap-0.3.2 (c (n "reap") (v "0.3.2") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "0yfy6rrx3v6g3yq5jsz5338hycj8k292d6b1d1z89p3hijy7sq6n")))

(define-public crate-reap-0.3.3 (c (n "reap") (v "0.3.3") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "timed_function") (r "^0.1") (d #t) (k 0)))) (h "1rdmsgnizz96fd7vrpgk7b8sykliqlh3d5qhbzdcczrrn9izy30a")))

