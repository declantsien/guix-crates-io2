(define-module (crates-io re ap reaper-macros) #:use-module (crates-io))

(define-public crate-reaper-macros-0.1.0 (c (n "reaper-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "reaper-low") (r "^0.1.0") (d #t) (k 2)) (d (n "reaper-medium") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "vst") (r "^0.2.0") (d #t) (k 2)))) (h "1bsp37n8killrjpig89ryqn1f386s2kqyy31avj7l0088n6vk9fa")))

