(define-module (crates-io re ap reaper) #:use-module (crates-io))

(define-public crate-reaper-2.0.0 (c (n "reaper") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "fern") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "ratelimit") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "1bawhlmvrc4gvm48mhbbqicv4146xcw303jrcxrp5c5zkhj8w7pm")))

