(define-module (crates-io re ap reaper-low) #:use-module (crates-io))

(define-public crate-reaper-low-0.1.0 (c (n "reaper-low") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (o #t) (d #t) (k 1)) (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.8") (o #t) (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (o #t) (d #t) (k 1)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (o #t) (d #t) (k 1)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "vst") (r "^0.2.0") (d #t) (k 0)))) (h "1wksr97p8s24ab0w6xlk04jgs7jxdr130999wr6ssjhj5ll27dgd") (f (quote (("generate" "bindgen" "quote" "syn" "proc-macro2") ("default"))))))

