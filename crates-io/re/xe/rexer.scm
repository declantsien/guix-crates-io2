(define-module (crates-io re xe rexer) #:use-module (crates-io))

(define-public crate-rexer-0.1.0 (c (n "rexer") (v "0.1.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "fake") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1xw32765nahzib69aby3xwkm9ydfdn89spq2g4aww65vrl350f4q")))

(define-public crate-rexer-0.1.1 (c (n "rexer") (v "0.1.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "fake") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1v9zkca1yms3ncnmah7ndlfbf55g638pfrpm0cr6z61gahrhq8kg")))

(define-public crate-rexer-0.1.2 (c (n "rexer") (v "0.1.2") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "fake") (r "^2") (d #t) (k 2)) (d (n "futures") (r "^0") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0") (o #t) (d #t) (k 0)))) (h "0wscvw81w9qamd58im9gwb93y4cyl1067f4p7xi6g0wjxf6ma5iq") (f (quote (("util" "futures" "pin-project-lite" "tokio-util") ("default" "util"))))))

