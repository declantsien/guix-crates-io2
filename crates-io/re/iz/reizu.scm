(define-module (crates-io re iz reizu) #:use-module (crates-io))

(define-public crate-reizu-0.1.0 (c (n "reizu") (v "0.1.0") (h "04bbqx7s0mvp5fl2vdbxqxr0wm3wm6lxssqvhga97kx1dm4mzviq")))

(define-public crate-reizu-0.1.1 (c (n "reizu") (v "0.1.1") (h "0kab7s7jybnxgxqqx1xjyznh8yd5l6r5l1ghpzr5v5wv6n2i6gsd")))

