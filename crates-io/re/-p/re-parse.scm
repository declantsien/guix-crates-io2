(define-module (crates-io re -p re-parse) #:use-module (crates-io))

(define-public crate-re-parse-0.1.0 (c (n "re-parse") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "re-parse-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.5.4") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1l2j32njfss099ndsxhallqrzbbpgbrha6h2lihq869cyrr7gwxg")))

