(define-module (crates-io re ac reachable) #:use-module (crates-io))

(define-public crate-reachable-0.2.0 (c (n "reachable") (v "0.2.0") (d (list (d (n "dns-lookup") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "sync" "time" "macros"))) (o #t) (d #t) (k 0)))) (h "0d4z7rq9a17piv0jja9b1dlf3xqancvpirin0qawzk8qwsicars3") (f (quote (("default" "async") ("async" "futures" "tokio"))))))

(define-public crate-reachable-0.2.1 (c (n "reachable") (v "0.2.1") (d (list (d (n "dns-lookup") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "sync" "time" "macros"))) (o #t) (d #t) (k 0)))) (h "15k5x14s1k64hilqvzl765gagpcycq3q4n3gl16p7sby4cg3dxfc") (f (quote (("default" "async") ("async" "futures" "tokio"))))))

(define-public crate-reachable-0.2.2 (c (n "reachable") (v "0.2.2") (d (list (d (n "dns-lookup") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "sync" "time" "macros"))) (o #t) (d #t) (k 0)))) (h "09vizw6yyyd8ln07xm14clljwgljykj6606v5if996avs6j2zg1v") (f (quote (("default" "async") ("async" "futures" "tokio"))))))

