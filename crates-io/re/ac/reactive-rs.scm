(define-module (crates-io re ac reactive-rs) #:use-module (crates-io))

(define-public crate-reactive-rs-0.1.1 (c (n "reactive-rs") (v "0.1.1") (d (list (d (n "slice-deque") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "slice-deque") (r "^0.1") (d #t) (k 2)))) (h "1avyshcvzp0nx6n0z3mm47xvrbih2m5f8v1i29nnzynag7h1zr6y") (f (quote (("default" "slice-deque"))))))

