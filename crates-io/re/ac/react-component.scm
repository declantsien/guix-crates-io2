(define-module (crates-io re ac react-component) #:use-module (crates-io))

(define-public crate-react-component-0.1.0 (c (n "react-component") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "10pgp6y8hixx8sjg1x8d6qj3z8vcznr8wyh6rrrcpwwv4zcps5zi")))

(define-public crate-react-component-0.1.1 (c (n "react-component") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "0pkylnc0zhi7syazkfrj9fy56a0cxp6ykl096b3qrmv8md6zh77j")))

(define-public crate-react-component-0.1.2 (c (n "react-component") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1l5165yia7xz2l34m8acy7s1ihg9a42k6vq8ba7v4sy0a5l5pinv")))

(define-public crate-react-component-0.2.2 (c (n "react-component") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "03qhc0c4fi9rqdkjb1vnwgvrpkwqacb9vwcaks0ywgvrzckjxj0x")))

(define-public crate-react-component-0.2.1 (c (n "react-component") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1i2bamzwy26n3sz67g8xacnwkz0yfmc67s1pb4c02sd0jsjprfl2")))

(define-public crate-react-component-0.2.3 (c (n "react-component") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0myknmnrj9pyyj1z1mwhlkhy3gim6hsqp30kvqsncmkxhfk0vs42")))

(define-public crate-react-component-0.2.4 (c (n "react-component") (v "0.2.4") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "1yi5wqn1axahfni6zmfc45ggcqdizy9xnlfasxwc4156zw1b5wn9")))

(define-public crate-react-component-0.2.5 (c (n "react-component") (v "0.2.5") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0y7r742b4y0m952q9502l5ai4825l37l8cz38aa5ghb4ipyhd0d8")))

(define-public crate-react-component-0.2.6 (c (n "react-component") (v "0.2.6") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0x30w8g4y5xcl0w9sqhgn8y40j7zmv01f7kyi872py8lz58m5ayk")))

