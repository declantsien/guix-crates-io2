(define-module (crates-io re ac reacty_yew) #:use-module (crates-io))

(define-public crate-reacty_yew-0.1.0 (c (n "reacty_yew") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s8a1i0djyd1gmm4mhqm7chhlr0r4bllj7pw5mskrpli07cj8qy2")))

