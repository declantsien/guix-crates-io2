(define-module (crates-io re ac reactor-trait) #:use-module (crates-io))

(define-public crate-reactor-trait-0.0.0 (c (n "reactor-trait") (v "0.0.0") (h "1bpxzj5rfdbn4y3ddhniv4czmxhwl76wj3k4piq6x7ynn7qyzy55")))

(define-public crate-reactor-trait-0.1.0 (c (n "reactor-trait") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "1xvyg2l6x7sxh32wpvy9k0jh4lcj3mg45qfklg0fyycgqzc62dh0")))

(define-public crate-reactor-trait-0.1.1 (c (n "reactor-trait") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "1pdr8cywsiavqfxn1lclxcjvqscyd4fn86xrsn4wwcrvyh5f31ag")))

(define-public crate-reactor-trait-0.2.0 (c (n "reactor-trait") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "10gsi2lixjym6mkm6746wbd2qjyva02n6jfcydd1ql746m5gfggb")))

(define-public crate-reactor-trait-1.0.0 (c (n "reactor-trait") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "0n3n83z0cm5bcbjrgsbfypf6frji5whrai6wvghd1wg13a0qzp5f")))

(define-public crate-reactor-trait-1.0.1 (c (n "reactor-trait") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "1mgidkizczvkg1qjhvlik8fg3r8gx0780z7b8wch2xpxb4ka7vz9")))

(define-public crate-reactor-trait-1.1.0 (c (n "reactor-trait") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "0n6w1nyd4qgrw24p64ww00pj65ll32c1jwgl61kmb5yhwj9l52j3") (r "1.56.0")))

