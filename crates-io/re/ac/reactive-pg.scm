(define-module (crates-io re ac reactive-pg) #:use-module (crates-io))

(define-public crate-reactive-pg-0.2.0 (c (n "reactive-pg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "sqlparser") (r "^0.26.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.0") (f (quote ("with-serde_json-1"))) (d #t) (k 0)))) (h "1s5gik6cpflqvzbsyazkcf463hrcl6lxnazz05k95bvyffhgyjdp")))

