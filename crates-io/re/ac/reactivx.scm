(define-module (crates-io re ac reactivx) #:use-module (crates-io))

(define-public crate-reactivx-0.1.0 (c (n "reactivx") (v "0.1.0") (h "1ph636w7wkmaqm4n0h23n96xcirfila25ag6ljdvqi1zxbn33f89") (y #t)))

(define-public crate-reactivx-0.1.1 (c (n "reactivx") (v "0.1.1") (h "0dpxj0fl51xixr067vxzls9hq64fnwjnhvmhdqm1nxmm1fwxi52v") (y #t)))

(define-public crate-reactivx-0.1.2 (c (n "reactivx") (v "0.1.2") (h "1rj1ly5nngllbq6x6xf64d08p9z1j8dk44djwyyhnrn262prgl49") (y #t)))

(define-public crate-reactivx-0.2.0 (c (n "reactivx") (v "0.2.0") (h "057zsfgd7iblpyycbqmn429zbrrfx6dy6cv28rlir416gn8fy4ps") (y #t)))

(define-public crate-reactivx-0.2.1 (c (n "reactivx") (v "0.2.1") (h "0hnwkqrzb99zls0fs9lh4gqsgl2hkhx00wlmdsj44pybpmc0fxvx") (y #t)))

(define-public crate-reactivx-0.2.2 (c (n "reactivx") (v "0.2.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0g25d0qcxzhi4lq0lqa29igppzjdfdwfb524h9gasksv0fnx0iy2") (y #t)))

(define-public crate-reactivx-0.2.3 (c (n "reactivx") (v "0.2.3") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "13kpybs7xzlcmf9z6p3205gh9k7kxwksjvn1b4h6wx1akk1j06zb") (y #t)))

(define-public crate-reactivx-0.2.4 (c (n "reactivx") (v "0.2.4") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "150p5xhcw3kwy9nd5mzgv3lxb6fps6xp4az0y23vf24r12905l9g") (y #t)))

(define-public crate-reactivx-0.3.0 (c (n "reactivx") (v "0.3.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "157qp6b4xgfsw1zw7xp9ld2mfnxnyxifflj6ycywx0p15snk78pw") (y #t)))

(define-public crate-reactivx-0.3.1 (c (n "reactivx") (v "0.3.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0c5pqfs1yhvn2wim3d61xvsnm17ma7apq6zgxdf1bw9qj3r7aq1i") (y #t)))

(define-public crate-reactivx-0.3.2 (c (n "reactivx") (v "0.3.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1yssbj29ryjym34j2yhmnywi2l38j2g92c4glmcl3m18wznznpsx") (y #t)))

(define-public crate-reactivx-0.3.3 (c (n "reactivx") (v "0.3.3") (h "1rfhnpxqigxr2jgsznpjz35nv1hck24ydabj4944p6g00v49bmhv") (y #t)))

