(define-module (crates-io re ac reactor-cache) #:use-module (crates-io))

(define-public crate-reactor-cache-0.1.0 (c (n "reactor-cache") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1h4aj6fpsj9mafkms3m2q44znlk3jfsms0yha64lmr6lfyqd5yf3")))

(define-public crate-reactor-cache-0.1.1 (c (n "reactor-cache") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "14zzhayq696q4gfpflhw1dc1g9msfxc6z1nzpz4y88frqwd20i90")))

