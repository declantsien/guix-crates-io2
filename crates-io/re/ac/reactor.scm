(define-module (crates-io re ac reactor) #:use-module (crates-io))

(define-public crate-reactor-0.1.0 (c (n "reactor") (v "0.1.0") (d (list (d (n "block_allocator") (r "*") (d #t) (k 0)) (d (n "iobuf") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1vkj5af656cdx2n9zx2si01lk4k91fbx5ngx4b6b9xva2d623wry")))

(define-public crate-reactor-0.1.4 (c (n "reactor") (v "0.1.4") (d (list (d (n "log") (r "^0.3.3") (d #t) (k 0)) (d (n "mio") (r "^0.4.3") (d #t) (k 0)) (d (n "tendril") (r "^0.1.6") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1gyksjwmkk6d1km1rpbjl95bhn1fbym72jyxbzdrm9nym35qznsy") (f (quote (("unstable"))))))

