(define-module (crates-io re ac reactor_rs) #:use-module (crates-io))

(define-public crate-reactor_rs-0.0.1 (c (n "reactor_rs") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1fv047xqg1bnwf1dc5hc18dnddpxi29rxc7xh4myb14myyvll3fp")))

(define-public crate-reactor_rs-0.0.2 (c (n "reactor_rs") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0qp40pgbh244ngr81v58zzvsf8a73zk2qd266kxfsrcdlay87dzx")))

(define-public crate-reactor_rs-0.0.3 (c (n "reactor_rs") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "09hwnamjsa6x57q50v9449p3zwpxw6ry36yip4w7974cm3zwz6r4")))

(define-public crate-reactor_rs-0.0.4 (c (n "reactor_rs") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "1vfs5q5jmah7s4c4zh7jib4d4cj1q9263v0sxv0w8flyxi6n7lzz")))

(define-public crate-reactor_rs-0.0.5 (c (n "reactor_rs") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "19cg5i77cndk9mx69d4wnsczncy9bam404z9i594zfp1zvkch9qf")))

