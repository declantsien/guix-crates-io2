(define-module (crates-io re ac reactivate) #:use-module (crates-io))

(define-public crate-reactivate-0.1.0 (c (n "reactivate") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1a3vbhg7bsf6nd17x8gk6bjkfqz6apd44dcb83948ak63rry739g")))

(define-public crate-reactivate-0.1.1 (c (n "reactivate") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1kqkm0j8jab6hzld7r8dlfx7z3sxykdbbx8zvfhb07rfhaz2vagq")))

(define-public crate-reactivate-0.2.0 (c (n "reactivate") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0g8ahrc6dz8898a73nfvcz7dna4q3x8mfz6jgajx2lc3l22cbjg2")))

(define-public crate-reactivate-0.2.1 (c (n "reactivate") (v "0.2.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1fk979yr1wbn5czxnl2nr7ba3lyx8cq9xn1a8qr12najy9bk4gcv")))

(define-public crate-reactivate-0.2.2 (c (n "reactivate") (v "0.2.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "19ilzmbmnf0kn56kz7116pc6r74zb7q3kxq564jvx7gww5va7di0")))

(define-public crate-reactivate-0.2.3 (c (n "reactivate") (v "0.2.3") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0594b8k9gcissh3wb20wycmlcijdf1avfii0w2hd6qczfzlzgkva")))

(define-public crate-reactivate-0.2.4 (c (n "reactivate") (v "0.2.4") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0cmzf8zzrcsbmsayn6gzcy0a5zqybn99k9l15pncci7ms2xp9bx6")))

(define-public crate-reactivate-0.2.5 (c (n "reactivate") (v "0.2.5") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0130rcfj37b9r4qr19zxmdbh6jyrsm4qkk1k5jzsq5kvf5ivm067")))

(define-public crate-reactivate-0.2.6 (c (n "reactivate") (v "0.2.6") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1812wq67ahw01bwvw9ami5ijwh01v8bnipqpzpj2b9cid0x8k4ml")))

(define-public crate-reactivate-0.2.7 (c (n "reactivate") (v "0.2.7") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0396z881p6b4hg1cplkgmdz0xsdb062wyqm0ry20drlhb29p552m")))

(define-public crate-reactivate-0.2.8 (c (n "reactivate") (v "0.2.8") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "17i8vsyydih28w6xpr946bssxvnsw8srq3vn2x88ingrx3rbxrlv")))

(define-public crate-reactivate-0.2.9 (c (n "reactivate") (v "0.2.9") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "13ymwnm4hvn9k4hzalyzkziyqlj7n18wqayiyym9i7cfr0q3v0qg")))

(define-public crate-reactivate-0.2.10 (c (n "reactivate") (v "0.2.10") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1bhlaip0f4lkkmapwg78i3ppzjj0zria9jmxrnfwy1wsa03p13vm") (y #t)))

(define-public crate-reactivate-0.2.11 (c (n "reactivate") (v "0.2.11") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0ib5k6kk550g7wnv9ndcal4qxpp5sa5xd6hdy4sj9wy0cv4gwylw")))

(define-public crate-reactivate-0.2.12 (c (n "reactivate") (v "0.2.12") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1zsi2cm2im22r7vl7sahgg3lrni2srabppvzsg3kv3q430v0yf5s")))

(define-public crate-reactivate-0.2.13 (c (n "reactivate") (v "0.2.13") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0dz5d1mpps946bvbyb66hh3p5pkdrhk8hkxrlky73zhjiv3q4crf")))

(define-public crate-reactivate-0.2.14 (c (n "reactivate") (v "0.2.14") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "00arzsdmhjra80y422rdwiinm6bj7pnb66vb323jl5qvw1m3xvi8")))

(define-public crate-reactivate-0.3.0 (c (n "reactivate") (v "0.3.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "09havx361dh9aics48dm5yxb2w8aqi7zxwi0s4jgbrp5jciflh18") (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-reactivate-0.3.1 (c (n "reactivate") (v "0.3.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0v1n85j90dp7knaa3rrs2b20kk3m32jjbq9wxakcrcz9fhzq3c5v") (s 2) (e (quote (("parallel-notification" "dep:rayon"))))))

(define-public crate-reactivate-0.4.0 (c (n "reactivate") (v "0.4.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0pr7qq64x838hhv070bmwdblpn9pqn0qgdxxy4dlap0zpbsqx0l5") (f (quote (("threadsafe"))))))

(define-public crate-reactivate-0.4.1 (c (n "reactivate") (v "0.4.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0748221v99hjrc67z567qka9qwpbkb72w3s7a7l068skyaf896zz") (f (quote (("threadsafe"))))))

(define-public crate-reactivate-0.4.2 (c (n "reactivate") (v "0.4.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1lga39zvhf7bs0pjp3x434f7vg3fpk7mnqha8blfssv8ld05gxkp") (f (quote (("threadsafe"))))))

