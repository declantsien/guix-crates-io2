(define-module (crates-io re ac reaction-sdk) #:use-module (crates-io))

(define-public crate-reaction-sdk-0.1.0 (c (n "reaction-sdk") (v "0.1.0") (d (list (d (n "scsys") (r ">=0.1.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1crkczzv3bwxhnx2dcnkm0hkhdqzczi3q35rmhi5vvs8b1g39qqw") (f (quote (("full") ("default"))))))

