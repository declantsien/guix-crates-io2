(define-module (crates-io re ac reactionary) #:use-module (crates-io))

(define-public crate-reactionary-0.1.0 (c (n "reactionary") (v "0.1.0") (h "0gj8jmsjh114w5b1b332bfgdm444vycn474xhc89ir8bi157qyaa") (y #t)))

(define-public crate-reactionary-0.1.1 (c (n "reactionary") (v "0.1.1") (d (list (d (n "scsys") (r ">=0.1.4") (f (quote ("full"))) (d #t) (k 0)))) (h "05rqy1d33807z43xj13b72zgbhiapwb24xr0bj0syx8lp3wrwich") (f (quote (("full") ("default"))))))

