(define-module (crates-io re ar rearch-tokio) #:use-module (crates-io))

(define-public crate-rearch-tokio-0.2.1 (c (n "rearch-tokio") (v "0.2.1") (d (list (d (n "rearch") (r "0.2.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "1lpb0ry04adsgab1nv6c9fpb6ixqqcb77idgblab7rv9x9lmmywf")))

(define-public crate-rearch-tokio-0.4.0 (c (n "rearch-tokio") (v "0.4.0") (d (list (d (n "rearch") (r "0.4.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "1dsyvd382gfzk5cn1nck3nph5j8ppchvlhdzykhgcjfcjh0l0rd1")))

(define-public crate-rearch-tokio-0.5.0 (c (n "rearch-tokio") (v "0.5.0") (d (list (d (n "rearch") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "085pm9gs1hkdirpfn18avj33cjx2fmzkjrapkfc2hxgfyii13qav")))

(define-public crate-rearch-tokio-0.5.1 (c (n "rearch-tokio") (v "0.5.1") (d (list (d (n "rearch") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "031nhribhm2y5q7p8vplngh86lvxmz4k0i1z0h0dccj3g63i3srd")))

(define-public crate-rearch-tokio-0.6.0 (c (n "rearch-tokio") (v "0.6.0") (d (list (d (n "rearch") (r "^0.7.0") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "187rq9hy42hbxcnf51rl1h13i8sjpxiybqmvxf8iga0pkjaq0nan")))

(define-public crate-rearch-tokio-0.7.0 (c (n "rearch-tokio") (v "0.7.0") (d (list (d (n "rearch") (r "^0.8.0") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "00dizc817mrg6sc73p5qfk15438wazg2knirhb1ldh6l62cyg3h8")))

(define-public crate-rearch-tokio-0.8.0 (c (n "rearch-tokio") (v "0.8.0") (d (list (d (n "rearch") (r "^0.9.1") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "08jlpis1sjvilnqf6vmnc4nzkc7cla06qnah8d0l8xb1ka51xyv7")))

(define-public crate-rearch-tokio-0.9.0 (c (n "rearch-tokio") (v "0.9.0") (d (list (d (n "rearch") (r "^0.9.2") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.4.0") (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt"))) (k 0)))) (h "0f3p62i5kjrs3wiwd08gjxmsy4r507v69bjh52nhwlijngp2zjj7")))

(define-public crate-rearch-tokio-0.10.0 (c (n "rearch-tokio") (v "0.10.0") (d (list (d (n "rearch") (r "^0.10.0") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.5.0") (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (k 0)))) (h "0q1khs707hdh88c3rx8cha580g8vwcr4mfhdwi114cywpnqhhgq5")))

(define-public crate-rearch-tokio-0.10.1 (c (n "rearch-tokio") (v "0.10.1") (d (list (d (n "rearch") (r "^0.10.0") (d #t) (k 0)) (d (n "rearch-effects") (r "^0.5.0") (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (k 0)))) (h "1k25sq8w6xy23if39wdb3rxbrjxna3fk9kbkn2yldy16440pddzy")))

