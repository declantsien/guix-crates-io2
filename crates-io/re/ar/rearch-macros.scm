(define-module (crates-io re ar rearch-macros) #:use-module (crates-io))

(define-public crate-rearch-macros-0.0.1 (c (n "rearch-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1d5pk43nxvjwy6pc2hx5qx8jaza8faig27jfkw9in9frrh7z3r9x")))

(define-public crate-rearch-macros-0.1.0 (c (n "rearch-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "032bs68w69npv6yxx0rf7803a64m6xv4l0ckr2lgkhidh9mkw8dp")))

(define-public crate-rearch-macros-0.2.0 (c (n "rearch-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkx2njs3xh9zkrr38mysv028m9r4wd9qg22nvj9j0pbzaj20y72")))

(define-public crate-rearch-macros-0.2.1 (c (n "rearch-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "11rmqwv4l6pyvvflwkbxbl2d30nj8lgrjrsx2bw5s7kz6fdmv6lw")))

(define-public crate-rearch-macros-0.4.0 (c (n "rearch-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "069mg410pcsdqcrcx7ai009bw4rhfg9cdk9jv13rjzvmmwffnaal")))

(define-public crate-rearch-macros-0.5.0 (c (n "rearch-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1nrwpmkpbglh1sb3jl33gfj9ydg5lw8vd8ni30yg7944b5k9qgqm")))

(define-public crate-rearch-macros-0.6.0 (c (n "rearch-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0cv6jw3nnfd5j7p4nb8grbmgymmv9x4hv9mn9jw6vb31l1p9fc4q")))

