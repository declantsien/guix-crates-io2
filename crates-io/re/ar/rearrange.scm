(define-module (crates-io re ar rearrange) #:use-module (crates-io))

(define-public crate-rearrange-0.1.0 (c (n "rearrange") (v "0.1.0") (h "1y9jdzrlx4ds3isgsnivii34ks1vl3ah03fhwdygz88m197ci43p")))

(define-public crate-rearrange-0.2.0 (c (n "rearrange") (v "0.2.0") (h "02cblv32yy6zmc8jvzi017wdg7418cqcssxfgkmfksfdp7zi2f9f")))

