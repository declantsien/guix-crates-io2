(define-module (crates-io re ar rearch-effects) #:use-module (crates-io))

(define-public crate-rearch-effects-0.1.0 (c (n "rearch-effects") (v "0.1.0") (d (list (d (n "rearch") (r "^0.7.0") (d #t) (k 0)))) (h "0sqw3psmrqz7v0n9yghj2v2g3alpxw702nw4am3q68d141d3hi77")))

(define-public crate-rearch-effects-0.2.0 (c (n "rearch-effects") (v "0.2.0") (d (list (d (n "rearch") (r "^0.8.0") (d #t) (k 0)))) (h "0n1f92ym9imnjk9685r6pkm7gq3z4hpsqhpijdd7jllpr5qgpr1q")))

(define-public crate-rearch-effects-0.2.1 (c (n "rearch-effects") (v "0.2.1") (d (list (d (n "rearch") (r "^0.9.0") (d #t) (k 0)))) (h "0zln5vmh38w8i48fc7ic2zxahs6nakzykd2570r1vcihd5a6203z")))

(define-public crate-rearch-effects-0.3.0 (c (n "rearch-effects") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.19.0") (k 0)) (d (n "rearch") (r "^0.9.1") (d #t) (k 0)))) (h "0mcblm15dvh51v546x6mzpkk8g5y9wchv4b6wdizxigcnwag5g3b")))

(define-public crate-rearch-effects-0.4.0 (c (n "rearch-effects") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.19.0") (o #t) (k 0)) (d (n "rearch") (r "^0.9.2") (d #t) (k 0)))) (h "0pavw6fc0zkvrrjhinbz41j09p46jl07hshdkbp994qpaqii7n59") (f (quote (("default" "lazy-state-transformers")))) (s 2) (e (quote (("lazy-state-transformers" "dep:once_cell"))))))

(define-public crate-rearch-effects-0.5.0 (c (n "rearch-effects") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.19.0") (o #t) (k 0)) (d (n "rearch") (r "^0.10.0") (d #t) (k 0)))) (h "1fp3wmv3l6d0asfblfvx20x6dmfq54zzpwnhm45vi8gfvg7bvhxy") (f (quote (("default" "lazy-state-transformers")))) (s 2) (e (quote (("lazy-state-transformers" "dep:once_cell"))))))

(define-public crate-rearch-effects-0.5.1 (c (n "rearch-effects") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.19.0") (o #t) (k 0)) (d (n "rearch") (r "^0.10.0") (d #t) (k 0)))) (h "1rjbbi6wlym98vmr3l380lp278q1nl8dss5q03g58vfagas7i7jl") (f (quote (("default" "lazy-state-transformers")))) (s 2) (e (quote (("lazy-state-transformers" "dep:once_cell"))))))

