(define-module (crates-io re dz redzone) #:use-module (crates-io))

(define-public crate-redzone-0.1.0 (c (n "redzone") (v "0.1.0") (d (list (d (n "soloud") (r "^1.0.1") (d #t) (k 0)))) (h "18bdjm38ha4njmv7487lizg17nk4646clj18d17ryqg0mhp1h8ln")))

(define-public crate-redzone-0.1.1 (c (n "redzone") (v "0.1.1") (d (list (d (n "soloud") (r "^1.0.1") (d #t) (k 0)))) (h "1drlsqnl2wnr5g472i8phws7fq956ffsrym4vw64favxcj0fv8kq")))

