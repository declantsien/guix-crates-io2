(define-module (crates-io re ng rengine) #:use-module (crates-io))

(define-public crate-rengine-0.0.0 (c (n "rengine") (v "0.0.0") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1m0916acnvc20q4kf14b4kklh7azswcc7hypcmi8q3f0klymq4qd") (y #t) (r "1.66")))

(define-public crate-rengine-0.0.1 (c (n "rengine") (v "0.0.1") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "13wswy5cdgi2cdqk0qyhmsla8whggpdxxhnc5szkhbnm5vh9kyra") (y #t) (r "1.66")))

(define-public crate-rengine-0.0.2 (c (n "rengine") (v "0.0.2") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "10xpq746qkf9v4c6bq6j1dlpqjxbxmdzv0iazidx6fcw6ikdqhv5") (y #t) (r "1.66")))

