(define-module (crates-io re we rewe) #:use-module (crates-io))

(define-public crate-rewe-0.1.0 (c (n "rewe") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1dzx1qxndfqc552bc62ar719pcyx0fmcywf8sxhvvday4fg7gfp4")))

(define-public crate-rewe-0.2.0 (c (n "rewe") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0l3m9dx1gm1123as8jy9706qz6g4wkcq2ksslrl03hs3ilrdygdy")))

(define-public crate-rewe-0.2.1 (c (n "rewe") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "106ay5bcay8skbix8lms2il9l2cvv9ikkb221msdqkm466qci8ss") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "native-tls"))))))

