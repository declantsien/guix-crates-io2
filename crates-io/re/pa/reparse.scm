(define-module (crates-io re pa reparse) #:use-module (crates-io))

(define-public crate-reparse-0.1.0 (c (n "reparse") (v "0.1.0") (h "0h9jnb2g06npjj6w0bp21jhri37hprrpvnby1xjl6vx6nvygywyy") (y #t)))

(define-public crate-reparse-0.1.1 (c (n "reparse") (v "0.1.1") (h "0jy3mb25jfr281zllwapy8ipmdrkiq6zkgi5kjk9m8cq3g41qw5b") (y #t)))

