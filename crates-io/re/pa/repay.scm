(define-module (crates-io re pa repay) #:use-module (crates-io))

(define-public crate-repay-0.1.0 (c (n "repay") (v "0.1.0") (d (list (d (n "ramn-currency") (r "^0.4.0") (d #t) (k 0)))) (h "0db9501k8gsf9bsk9fkii4v13scm3sixgx4aa1gm2c5r2dh3p2x2")))

(define-public crate-repay-0.1.1 (c (n "repay") (v "0.1.1") (d (list (d (n "ramn-currency") (r "^0.4.1") (d #t) (k 0)))) (h "051nrw76cr3iv03qx7wgqffvpm2mfjxj0r7b3zvlibnx6bdxg0pq")))

(define-public crate-repay-0.1.2 (c (n "repay") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0nb5x8vx5gc8kwxsfb6057pzb5p6c32k2kn5gbapi4d15b938ksh")))

(define-public crate-repay-0.2.0 (c (n "repay") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1q5bgwz6cb9rza6k5m0qyd4mp8zd2waav5njh862k3dhqdayhfki")))

(define-public crate-repay-0.2.1 (c (n "repay") (v "0.2.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1j18n9qr6f5a753wy8dhgqpj45jpnbc5xc1b07qz88c4g80xba02")))

