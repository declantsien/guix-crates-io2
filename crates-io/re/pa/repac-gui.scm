(define-module (crates-io re pa repac-gui) #:use-module (crates-io))

(define-public crate-repac-gui-0.1.0 (c (n "repac-gui") (v "0.1.0") (d (list (d (n "repac-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "slint") (r "^1.0.2") (d #t) (k 0)) (d (n "slint-build") (r "^1.0") (d #t) (k 1)))) (h "0i8170y8p8cfagmcigd5x5rwj3aim6fx5hgyfj3k52hx1470rz4s") (y #t)))

