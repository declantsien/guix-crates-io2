(define-module (crates-io re pa repac-cli) #:use-module (crates-io))

(define-public crate-repac-cli-0.1.0 (c (n "repac-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "pbr") (r "^1.1.1") (d #t) (k 0)) (d (n "repac-lib") (r "^0.1") (d #t) (k 0)))) (h "0phfz4l3s3by2lrq7iiqrivp5ah4cfi0anylbknjdsw6hlrfbqmb") (y #t)))

