(define-module (crates-io re pa repac-lib) #:use-module (crates-io))

(define-public crate-repac-lib-0.1.0 (c (n "repac-lib") (v "0.1.0") (d (list (d (n "libflate") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "tar") (r "^0.4.29") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.1") (d #t) (k 0)))) (h "03a6yc09hwpjynqhmgvs45nc64jwmyzs2b4l1pjaj7jva17ripy3") (y #t)))

