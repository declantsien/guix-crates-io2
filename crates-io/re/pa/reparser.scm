(define-module (crates-io re pa reparser) #:use-module (crates-io))

(define-public crate-reparser-0.1.0 (c (n "reparser") (v "0.1.0") (d (list (d (n "lalrpop-util") (r "^0.19.5") (f (quote ("lexer"))) (d #t) (k 0)))) (h "17wnmyfxjzqj7qhzvr82nm9a4ldfwddg2f1sgwfi9g7c0d221lpj")))

(define-public crate-reparser-0.1.1 (c (n "reparser") (v "0.1.1") (d (list (d (n "lalrpop-util") (r "^0.19.5") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1vj8j1q8szcm5dn77706dhfivc5iv1v92w30j9f88w1hlhdr1b83")))

(define-public crate-reparser-0.1.11 (c (n "reparser") (v "0.1.11") (d (list (d (n "lalrpop-util") (r "^0.19.5") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0m3a4jic8g384z0kj3kkz2inq0ibhjzzv7g82x2cv44rf7k63fa7")))

