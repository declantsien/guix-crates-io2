(define-module (crates-io re pa repak-lib) #:use-module (crates-io))

(define-public crate-repak-lib-0.1.0 (c (n "repak-lib") (v "0.1.0") (d (list (d (n "libflate") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.29") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)))) (h "0115s2pzmr0a0zxz6f8n8b0n8c9swp5d2rmdgfxm6m76raadajgw")))

