(define-module (crates-io re pa repackage) #:use-module (crates-io))

(define-public crate-repackage-0.1.0 (c (n "repackage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.8.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.3") (f (quote ("zlib"))) (k 0)) (d (n "tar") (r "^0.4.32") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0gmka4ykkbvgwcvgd18636ln0l5ik97srvmfnm5v8qxm57yq8yvf")))

(define-public crate-repackage-0.1.1 (c (n "repackage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.3") (f (quote ("zlib"))) (k 0)) (d (n "tar") (r "^0.4.32") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1yd7ynkm527wbq0mgb9j5gmila4plbvqb4k9r9hyzp83jkyxsja9")))

