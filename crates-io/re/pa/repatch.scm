(define-module (crates-io re pa repatch) #:use-module (crates-io))

(define-public crate-repatch-0.1.0 (c (n "repatch") (v "0.1.0") (d (list (d (n "anstyle") (r "^1.0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bstr") (r "^1.9.0") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "diffy") (r "^0.3.0") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.7") (d #t) (k 0)) (d (n "grep-regex") (r "^0.1.12") (d #t) (k 0)) (d (n "grep-searcher") (r "^0.1.13") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1apxvpx8c5nk1vk8pm0jg5rs5yax983iqk9mys5paps42aslka1a")))

