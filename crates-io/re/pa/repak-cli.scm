(define-module (crates-io re pa repak-cli) #:use-module (crates-io))

(define-public crate-repak-cli-0.1.0 (c (n "repak-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (d #t) (k 0)) (d (n "pbr") (r "^1.1.1") (d #t) (k 0)) (d (n "repak-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1319k34n4d3rqbl9vwqisjcqdd7f18kypnvjypkbrf41avz5q1wd") (y #t)))

