(define-module (crates-io re fa refactory_string) #:use-module (crates-io))

(define-public crate-refactory_string-0.1.0 (c (n "refactory_string") (v "0.1.0") (h "06n43y8sy76yxhf855kpl0vhvvimc8gp9j35wddpdh6kfrxz8lj3")))

(define-public crate-refactory_string-0.1.1 (c (n "refactory_string") (v "0.1.1") (h "1823nmnvymqkc5c0g3wq2sfx8x7b06s3iv9w1y8ixzcw2534ifga")))

(define-public crate-refactory_string-0.1.2 (c (n "refactory_string") (v "0.1.2") (h "1382f3faa6kiviz7b0ipw0fpbbs6kf7dfmra0islxnnd8m3l12zy")))

(define-public crate-refactory_string-0.1.3 (c (n "refactory_string") (v "0.1.3") (h "1x9hygbpnyid4ak9x3243g82fc90wf19brrdxi76yn2vj7vxidmq")))

(define-public crate-refactory_string-0.1.4 (c (n "refactory_string") (v "0.1.4") (h "108w2nbl2q14i1dd8abg8yklk6hxzxlgj5zwhfbramir7cvn92dm")))

