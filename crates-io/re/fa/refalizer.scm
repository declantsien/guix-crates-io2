(define-module (crates-io re fa refalizer) #:use-module (crates-io))

(define-public crate-refalizer-0.1.0 (c (n "refalizer") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0dfs6slqgvqrhlr01az9k0z6sdk8vz25w6yw7fkczdzgxdv36jh8")))

