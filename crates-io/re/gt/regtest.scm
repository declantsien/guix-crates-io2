(define-module (crates-io re gt regtest) #:use-module (crates-io))

(define-public crate-regtest-0.1.0 (c (n "regtest") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "13df4x0y2v472qzjmcjx39r05ck02kv9v5i2vh3x5h6c2lcwfqz3")))

(define-public crate-regtest-0.1.1 (c (n "regtest") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1iqwqkcflczld2gjng2syp9b2a5rpx1f02vi8whcd9f4cxkxmv6b")))

(define-public crate-regtest-0.2.0 (c (n "regtest") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1cwv2m35hrym2q7ja39wvjc097g294ck0rv8csq3dx3sn3l87nvs")))

(define-public crate-regtest-0.2.1 (c (n "regtest") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "02mcy2x6sh7aizh9rpmp7ri3xqg8hlz3b94np79kqkx1bkn58v02")))

(define-public crate-regtest-0.2.2 (c (n "regtest") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0ij4xkmgj208b3i302alcvmxixhx915q60j48wncan9r0ca9s9d8")))

(define-public crate-regtest-0.3.0 (c (n "regtest") (v "0.3.0") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "11ai9q7jr7a6z6fyhbsrqf33vzavmwdq40knl97j4ij9vk6q3pni")))

(define-public crate-regtest-0.3.1 (c (n "regtest") (v "0.3.1") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "109rpib45kqnw3pn83lwmzx4aghbd96ppvccybr14dm08lfbj2zd")))

(define-public crate-regtest-0.3.2 (c (n "regtest") (v "0.3.2") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1rv0r45acp0qk2qxb660pmn1id1m0kzg588rjy73qjqy7igfa5xy")))

(define-public crate-regtest-0.3.3 (c (n "regtest") (v "0.3.3") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1hzgkcqgb2dc9iv2jxwfvwn2mq4fyhbdmhb92cxkbhm7rwb8gh6m")))

(define-public crate-regtest-0.3.4 (c (n "regtest") (v "0.3.4") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "026hphzdnzwm0digwmibkp7jfp66i0rfir0kjlfvilvz99qdjpvh")))

(define-public crate-regtest-0.3.5 (c (n "regtest") (v "0.3.5") (d (list (d (n "app_dirs") (r "^1.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1yhy7rh6kr47x705rfgicirj63vv172zgfqza3vkgcx61jml87a8")))

