(define-module (crates-io re im reim) #:use-module (crates-io))

(define-public crate-reim-0.0.0 (c (n "reim") (v "0.0.0") (h "00j58qqmn1gh3hxnm3bxq5g32w629jlplrf2h08yfpjfjwadzvkv")))

(define-public crate-reim-0.0.1 (c (n "reim") (v "0.0.1") (h "1zc5bpd2811nir9ppbq4i3pqam9wkcn9acszb50ma8g07nzjy219")))

(define-public crate-reim-0.0.2 (c (n "reim") (v "0.0.2") (h "1wjbffnvp2jfsnpi7j4q9xs6x1dd1c39pw333bzzmh5xq90q3m8i")))

(define-public crate-reim-0.0.3 (c (n "reim") (v "0.0.3") (h "11k12vn56la4rfgxm01s3n2sr1z71l7dvansldcj90xx5iprlbxy")))

(define-public crate-reim-0.0.4 (c (n "reim") (v "0.0.4") (h "07z2kxbi6wi6wvvdsa5w2idxwzr9i4qa78qida93rph0rds8h96z")))

(define-public crate-reim-0.0.5 (c (n "reim") (v "0.0.5") (h "0wvf8h1ykh7sgf3sa1i67p5x95fwhxx4nyi4hx2v3v8hpkqr0xd5")))

(define-public crate-reim-0.0.6 (c (n "reim") (v "0.0.6") (h "1slq6fi2lh8dw1d82s36ammvbmk2aph8dk8pnhlvr1ng7nchl5y4")))

(define-public crate-reim-0.0.7 (c (n "reim") (v "0.0.7") (h "1l5w5dvgzc2yqkpxyvrzx1mgz56dwx9hyr5cpdyj90i3h1nzsj17")))

(define-public crate-reim-0.0.8 (c (n "reim") (v "0.0.8") (d (list (d (n "disp") (r "^0") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)))) (h "1nbxxdqv455l3fxg06p3nl3n1bgl0zn7dsl8hc4kkrb1m39bk6zz")))

