(define-module (crates-io re wo rework) #:use-module (crates-io))

(define-public crate-rework-0.0.1 (c (n "rework") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pandet") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "1hc8syzzln12y4rbag0k0aq9ggqw2av055y10iiqncrrwnwqkq7f") (y #t)))

(define-public crate-rework-0.0.2 (c (n "rework") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pandet") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "0vz4rknbjb60l82g1z5f5hc3jwh8r5d8rdkx7jp15zfms1iiibbg")))

(define-public crate-rework-0.1.0 (c (n "rework") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pandet") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "1y1dki4mcg5568lpr6i91wl5rrhjvz8s2sllk750jabsq7sx5sdl")))

