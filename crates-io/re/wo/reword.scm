(define-module (crates-io re wo reword) #:use-module (crates-io))

(define-public crate-reword-0.1.0 (c (n "reword") (v "0.1.0") (h "0kd3jpxa8jksi6wvfy0gc2bbrw1f13wjwmvyijfzdv08vr0f5a7v")))

(define-public crate-reword-0.1.1 (c (n "reword") (v "0.1.1") (h "0hkwlp076jglcisyrgxdvpdbh62lx6qpilmha1p9j5syxyb36gqy")))

(define-public crate-reword-0.1.2 (c (n "reword") (v "0.1.2") (h "0kaz5d011mpgqm91d4zpj7fh96l4vgwlxrfyqa8xphf927k8k1s8")))

(define-public crate-reword-0.1.3 (c (n "reword") (v "0.1.3") (h "1m4k32pglg44kwji2br5n23gydx7r62dfn5b5lrzjsa67smacp70")))

(define-public crate-reword-0.1.4 (c (n "reword") (v "0.1.4") (h "0h81sk29fzwvwxi6avmw1y03yh4mc03dir0pr6af2nwzzid89q9a")))

(define-public crate-reword-0.2.0 (c (n "reword") (v "0.2.0") (h "1zg45knjc3bnca03d98714mb0xflwmvnbcdxjyrry7s3g0rjl1d0")))

(define-public crate-reword-0.2.1 (c (n "reword") (v "0.2.1") (h "1pl2vqg4xlj0jvy7hkrn2li83kvwxr92aiqbjzg82bzjr6pqvlhc")))

(define-public crate-reword-0.3.0 (c (n "reword") (v "0.3.0") (h "1rya8fg0661m66p1wvk7lh142675gv3m4n0vl2g4jndi2s4gib9y")))

(define-public crate-reword-1.0.0 (c (n "reword") (v "1.0.0") (h "11jaxh6zjzjfzc3a3psmgcd3l6ppzkll16v1b8hjw762lyc6i45m")))

(define-public crate-reword-1.0.1 (c (n "reword") (v "1.0.1") (h "05yxh04fjrmx20bvdnqhvqv5d53izm838gid9hgipqd3z9cxnaxi")))

(define-public crate-reword-1.0.2 (c (n "reword") (v "1.0.2") (h "0jj6rm1bpbp1kscrqc014d20xda39igfgh7r8aipszn76wpfih0r")))

(define-public crate-reword-1.0.3 (c (n "reword") (v "1.0.3") (h "022vdc4bpb4wmpafnjmr56vk83dgjdr16v2fraw4837h94gsnk8z")))

(define-public crate-reword-1.0.4 (c (n "reword") (v "1.0.4") (h "0wpajpcbj8khgrdpclk7d5jzy0v25l7qfq76mhxm46lb5w4m5g19")))

(define-public crate-reword-1.0.5 (c (n "reword") (v "1.0.5") (h "10f1dl8433nzka1lzb67lcy7bw55pqcfzqj0glmmwyd0fgqi5fbc")))

(define-public crate-reword-1.0.6 (c (n "reword") (v "1.0.6") (h "1jpszm5iia9yx218z303rby004cibw8jvjs89qbg4hh9zxndxv7m")))

(define-public crate-reword-1.0.7 (c (n "reword") (v "1.0.7") (h "1kdprg37g4ivha5r8yqz8fljlrbh3iljqhjyqg47pgwkbq55d9zn")))

(define-public crate-reword-2.0.0 (c (n "reword") (v "2.0.0") (h "18fnm1r6xwjxglwcvq69p2nw2im7yg85y3cbwjj6nwm6188b1ds5")))

(define-public crate-reword-3.0.0 (c (n "reword") (v "3.0.0") (h "0ds0dcw6kndlj6fsa73bcadnf4c5n5dq91ncaabclxp4w0r5bnba")))

(define-public crate-reword-3.0.1 (c (n "reword") (v "3.0.1") (h "19m0nffsd4wj1z2z21hjdgwkww58n40gicjzrxhjphvignf41iyh")))

(define-public crate-reword-3.0.2 (c (n "reword") (v "3.0.2") (h "0wg621rvlpnx4054006kgsy57hic3hgn3njlpfgjpwyvlj3j19kk")))

(define-public crate-reword-4.0.0 (c (n "reword") (v "4.0.0") (h "187dszrf32rbq32xahs1sr2wijw6x1as2f6xsp9b9n6bhy19rvvf")))

(define-public crate-reword-4.0.1 (c (n "reword") (v "4.0.1") (h "1mc3my6517fgilq6qla5bd9adnjhq24i48gd1acmvmlcy5bszpr4")))

(define-public crate-reword-5.0.0 (c (n "reword") (v "5.0.0") (h "1sk1gy0cbcjxyk5pxama2mmrdp27h8c89hl92i77qjgj4z41m1zs")))

(define-public crate-reword-6.0.0 (c (n "reword") (v "6.0.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0x4jl10p7ixyimh9ssqxzxyx3l3slyqg6wqlrdc64xp94z154hml")))

(define-public crate-reword-6.1.0 (c (n "reword") (v "6.1.0") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "127six38a8zrmphyqmivkgm17wsqyfb76gzjfkzgj45g5vk5gi1d")))

(define-public crate-reword-6.2.0 (c (n "reword") (v "6.2.0") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0bq0l893ibg4xv5pi2xc8pbsmi55fm27pvi0hn8nq9v6yxb0zcrk")))

(define-public crate-reword-6.3.0 (c (n "reword") (v "6.3.0") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0mbggz7jimc7j7nk017pwkmk50s43k46a11z7ryx88pzww3abnsp")))

(define-public crate-reword-6.4.0 (c (n "reword") (v "6.4.0") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "13hk9xiwqzd657g7x6i5scqbzjxh8jigqx20n1xhplli9p17vs4z")))

(define-public crate-reword-7.0.0 (c (n "reword") (v "7.0.0") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0r873xd8i731zyhgh20b75hi51fi93vm6ncrg6s7dvg9vjc209zy")))

