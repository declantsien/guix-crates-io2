(define-module (crates-io re un reunion) #:use-module (crates-io))

(define-public crate-reunion-0.1.0 (c (n "reunion") (v "0.1.0") (h "0chaf2iyipgplswlapnrb5zlp5i6q2jywg2yvs2bcbmwvkigrk20")))

(define-public crate-reunion-0.1.1 (c (n "reunion") (v "0.1.1") (h "05vrs7s77q31sbk3mrjb6wlcggsqdsfbxc0pbbbmxj6vjfvhn2wb")))

(define-public crate-reunion-0.1.2 (c (n "reunion") (v "0.1.2") (h "0z4y8h99q3akg4cbvggi6hjd5awvq33wxsv6275zrwggrkxw1yr8")))

(define-public crate-reunion-0.1.4 (c (n "reunion") (v "0.1.4") (h "17w2lr0aswngarxssdnjjqckxbizqyq1yh5nl9rk0vcbqqa0scs4")))

(define-public crate-reunion-0.1.5 (c (n "reunion") (v "0.1.5") (h "1ag4qfn99wh0w1rm7szmwd450sa1di4wb3pb6i52p53fhpm9a0yj")))

(define-public crate-reunion-0.1.6 (c (n "reunion") (v "0.1.6") (h "0z0ig9496f8ba45n5lqlynz0zv3f1hkswpinvl0c3c3g4g1307kn")))

(define-public crate-reunion-0.1.7 (c (n "reunion") (v "0.1.7") (h "0rf9ijg9440ajicm1qrs1nnfz2kn81b5lczvlp0jar4qrpnccca4")))

(define-public crate-reunion-0.1.8 (c (n "reunion") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fwnz8jpi98nl0j2gy24zhaj13qn2l425izmrq4cklz03dn8lvm8")))

(define-public crate-reunion-0.1.9 (c (n "reunion") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "00vxc8ddibcpj1riw4xhn1gimswda293idkfjjnp138vjgsxv4x6")))

(define-public crate-reunion-0.1.10 (c (n "reunion") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ff5ssgmhzcgv4w5x5s9l256vvcpm9rhk6z3p062lvd03c6rcpgy")))

(define-public crate-reunion-0.1.11 (c (n "reunion") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0m12bzwp15g93dj2vrryviqfkya39xik9s9dkhl53nckvmnlz5ya")))

(define-public crate-reunion-0.1.12 (c (n "reunion") (v "0.1.12") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07mv5qv3yk4inj0qm0n12j4zhiik86gp52xva696yh3cvs22yc4g")))

(define-public crate-reunion-0.1.13 (c (n "reunion") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n99g2032fl91yxvs971gjwffjv76mj4b56ywdfz0cmfdaynn6ym")))

(define-public crate-reunion-0.1.14 (c (n "reunion") (v "0.1.14") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10sd47429b6kdwaljl0hg6d3f272cvr5dymjqfmmfgj2qka82km7")))

