(define-module (crates-io re sv resvg-qt) #:use-module (crates-io))

(define-public crate-resvg-qt-0.1.0 (c (n "resvg-qt") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bj5xw703ywzixkd79l11vwdarb7g5pmfqs5w3lj1y0lxcn55qm4")))

(define-public crate-resvg-qt-0.2.0 (c (n "resvg-qt") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1sqrcfcgh4n6mhm60wnflrfp5lz3y4ndfqixpcw2qzcfagm7y5d9")))

(define-public crate-resvg-qt-0.2.1 (c (n "resvg-qt") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dqw8s25k3zq65parn6fil5bj16cplag94z7aima237czbk2jbn2")))

(define-public crate-resvg-qt-0.3.0 (c (n "resvg-qt") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05lqsnvk14sbcy6xhm06i1arlb9cmr1pmf0jr9w8fgmmh5r0f3in")))

(define-public crate-resvg-qt-0.4.0 (c (n "resvg-qt") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c3zxgyznidyfavzpzzl9qszy74slqiwj0lwpzrryqzw876glbhc")))

(define-public crate-resvg-qt-0.5.0 (c (n "resvg-qt") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jyyi37lll0rv4r1av6z86cgz5hx306l48jhv9g1ladazqxixh62")))

(define-public crate-resvg-qt-0.6.0 (c (n "resvg-qt") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a57s8khy0k7gh30j2g3xwpsqyxqsixv89365yipcrg0j6sv35ac")))

(define-public crate-resvg-qt-0.6.1 (c (n "resvg-qt") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qax2vl0cvrm5vz14g0zq69dyrava4lg665qb0yxv7xz8lwf5d6g")))

(define-public crate-resvg-qt-0.7.0 (c (n "resvg-qt") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ll3fdz1zd8b8rinzvrxy0swgl30qc2zlfx9rsjjlqy0pxg4b1vf")))

(define-public crate-resvg-qt-0.8.0 (c (n "resvg-qt") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "091dbwc9akry0p1laly7phg79wgsbx0cc8l563hgvs6jd4ydafx0")))

(define-public crate-resvg-qt-0.9.0 (c (n "resvg-qt") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v04xn0lkvc96qlx1qkbb1jlhlm3pds5sr8jclrd1m44yqlnsm0y")))

(define-public crate-resvg-qt-0.9.1 (c (n "resvg-qt") (v "0.9.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vbqkg0rh3rfvxd7bazdjkwpksxhwpa13gwcnb4a7gc3rmwr9mb5")))

(define-public crate-resvg-qt-0.10.0 (c (n "resvg-qt") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "1vhaq5zibc3mch4wj84jw2rjz8cl31n5x1dxmcs4hvf5v73srly1") (f (quote (("text" "usvg/text") ("default" "text"))))))

(define-public crate-resvg-qt-0.10.1 (c (n "resvg-qt") (v "0.10.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "1ki0y5hchyiyi9xwqv6cg2pgnzc07i83cfc7gj654fwlk61dvq2f") (f (quote (("text" "usvg/text") ("default" "text"))))))

