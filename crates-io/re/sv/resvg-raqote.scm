(define-module (crates-io re sv resvg-raqote) #:use-module (crates-io))

(define-public crate-resvg-raqote-0.10.0 (c (n "resvg-raqote") (v "0.10.0") (d (list (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.15") (k 0)) (d (n "raqote") (r "^0.8") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "06mymji0r7294j4fsrj79g4ykw5vscniw5mf747hzkxb8iw8f0fh") (f (quote (("text" "usvg/text") ("default" "text"))))))

(define-public crate-resvg-raqote-0.10.1 (c (n "resvg-raqote") (v "0.10.1") (d (list (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.15") (k 0)) (d (n "raqote") (r "^0.8") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "09mc77f74l8hzpdyb7vq273cnaj1p8q7w2x9sccv3fyc6b1h52g9") (f (quote (("text" "usvg/text") ("default" "text"))))))

