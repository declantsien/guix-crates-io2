(define-module (crates-io re sv resvg-cairo) #:use-module (crates-io))

(define-public crate-resvg-cairo-0.10.0 (c (n "resvg-cairo") (v "0.10.0") (d (list (d (n "cairo-rs") (r "^0.8") (f (quote ("png"))) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "0kqrk7r2skca0cd2n8yb2nigppak1cxqj2p4fvhw8194gbk92apf") (f (quote (("text" "usvg/text") ("default" "text"))))))

(define-public crate-resvg-cairo-0.10.1 (c (n "resvg-cairo") (v "0.10.1") (d (list (d (n "cairo-rs") (r "^0.8") (f (quote ("png"))) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "0m86za85sh6bzph3b3m57s24qifwx1jgz2iqkiq90vhfrcqjkj0x") (f (quote (("text" "usvg/text") ("default" "text"))))))

