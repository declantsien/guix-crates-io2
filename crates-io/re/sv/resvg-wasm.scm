(define-module (crates-io re sv resvg-wasm) #:use-module (crates-io))

(define-public crate-resvg-wasm-0.1.3 (c (n "resvg-wasm") (v "0.1.3") (d (list (d (n "fontdb") (r "^0.7") (d #t) (k 0)) (d (n "resvg") (r "^0.20") (k 0)) (d (n "tiny-skia") (r "^0.6") (d #t) (k 0)) (d (n "usvg") (r "^0.20") (f (quote ("text"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0j15rdg62nyg9i7qnrzywhlsgdriig88zk25awha97mcgf26c4nf")))

