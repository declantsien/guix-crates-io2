(define-module (crates-io re sv resvg-skia) #:use-module (crates-io))

(define-public crate-resvg-skia-0.8.0 (c (n "resvg-skia") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13131838drdjx3pgbl1hhh9khj152yg2ry12jgcxf8hy8c837n56")))

(define-public crate-resvg-skia-0.9.0 (c (n "resvg-skia") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01km1l05jb8xhy5cd3j9wy3np8l65wymr8npbl1ir7v5z98q64w7")))

(define-public crate-resvg-skia-0.9.1 (c (n "resvg-skia") (v "0.9.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kmg94npcr4m9rgw3cf3259wb357mc839yb5b85aya328ms0kh1j")))

(define-public crate-resvg-skia-0.10.0 (c (n "resvg-skia") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.10") (k 0)))) (h "1r7hj3blxwx42w8lg4msv0r8rriilmd4yzykmn4w890kinlzjfcg") (f (quote (("text" "usvg/text") ("default" "text"))))))

