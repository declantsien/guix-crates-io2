(define-module (crates-io re x- rex-ucph) #:use-module (crates-io))

(define-public crate-rex-ucph-0.1.0 (c (n "rex-ucph") (v "0.1.0") (d (list (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "1bsbsdmmw3xxzvi9cb4rwp43g3523246bhgwjqmaszqh551409if")))

(define-public crate-rex-ucph-0.1.1 (c (n "rex-ucph") (v "0.1.1") (d (list (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "0hswk8s7sl7zsncm7y3mra38ic4dpcv31gpvgfzzs2cq6kkpsssn")))

(define-public crate-rex-ucph-0.1.2 (c (n "rex-ucph") (v "0.1.2") (d (list (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "1yp551g8b0v8qlnqdz1ykwn3ac7q9hdsb63i5k3bjvgsaf21v38s")))

(define-public crate-rex-ucph-0.1.3 (c (n "rex-ucph") (v "0.1.3") (d (list (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "0ypd7ldwv4vsqv9zbzmvvwgb6z4fli6xkr9jcpqmnp52nxq738bd")))

