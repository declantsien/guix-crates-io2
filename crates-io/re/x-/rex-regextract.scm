(define-module (crates-io re x- rex-regextract) #:use-module (crates-io))

(define-public crate-rex-regextract-0.1.0 (c (n "rex-regextract") (v "0.1.0") (d (list (d (n "eyre") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1fs5x9w0khvpp7lvrq3px693fiwdacj5sq46bvpnh7a4gr12ndhb")))

(define-public crate-rex-regextract-0.1.1 (c (n "rex-regextract") (v "0.1.1") (d (list (d (n "eyre") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1bjhmrqbbba1s0g0q1jfl1x39zcyfs1znyvpn1pcfpz4xcwjjjgr")))

