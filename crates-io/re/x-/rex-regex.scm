(define-module (crates-io re x- rex-regex) #:use-module (crates-io))

(define-public crate-rex-regex-0.1.1 (c (n "rex-regex") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 2)))) (h "0i6sblhw7cwqmq7bb4jf23anv046ws6m1gkh20x7i25fmv9vw789")))

(define-public crate-rex-regex-0.1.2 (c (n "rex-regex") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 2)))) (h "134wg33g9dwyq0vy3hkscnlpx9kpxzyr3jh6hg6sq3066zbfjqc9")))

(define-public crate-rex-regex-0.1.3 (c (n "rex-regex") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 2)))) (h "0cadwyfr6bvxys04zfc3r3jf3rwcvg4jml4fqvyq364p6f8mxrqx")))

(define-public crate-rex-regex-0.1.4 (c (n "rex-regex") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 2)))) (h "0sks8l1yh33ghg4x22makhk7jrbj34wq8n8gdr2z1g36hija9m7h")))

