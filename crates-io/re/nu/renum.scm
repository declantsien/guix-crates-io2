(define-module (crates-io re nu renum) #:use-module (crates-io))

(define-public crate-renum-0.1.0 (c (n "renum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "1604kpw3d15hxigky92mm05y3q4r6h5m0j6b1lfyzcq7910clwm2")))

(define-public crate-renum-0.1.1 (c (n "renum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "1si40s3jna7i3y8bp1fgyinn8dnm2m6b578lfiw67xjvydrph6cx")))

