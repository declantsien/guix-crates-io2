(define-module (crates-io re ce recently-used-xbel) #:use-module (crates-io))

(define-public crate-recently-used-xbel-1.0.0 (c (n "recently-used-xbel") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05vxia2l3hc6b075vm7r3l0xsqa500qxls4cbirk8c2jf71k75dx")))

