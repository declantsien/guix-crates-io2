(define-module (crates-io re pi repite) #:use-module (crates-io))

(define-public crate-repite-0.0.1 (c (n "repite") (v "0.0.1") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "arraydeque") (r "^0.4.5") (d #t) (k 0)) (d (n "espera") (r "^0.0.1") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sixbit") (r "^0.5.0") (d #t) (k 0)))) (h "02l9s5j0avrp13zix93cnmcgx010cgh0x90qj72y8n6x19r05cpf") (f (quote (("std" "espera/std") ("safe" "espera/safe") ("default" "std" "safe")))) (r "1.65.0")))

(define-public crate-repite-0.0.2 (c (n "repite") (v "0.0.2") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "arraydeque") (r "^0.4.5") (d #t) (k 0)) (d (n "espera") (r "^0.0.1") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sixbit") (r "^0.5.0") (d #t) (k 0)))) (h "0wxnihkspp371nl6nakvf9fb51gdpl4wqp9pdypi19vnvy1xgxsn") (f (quote (("std" "espera/std") ("safe" "espera/safe") ("default" "std" "safe")))) (r "1.65.0")))

(define-public crate-repite-0.0.3 (c (n "repite") (v "0.0.3") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "arraydeque") (r "^0.4.5") (d #t) (k 0)) (d (n "espera") (r "^0.0.3") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sixbit") (r "^0.5.0") (d #t) (k 0)))) (h "16sxn3aa768iqvdmai9vj3l47dajibklhc8g359crchhgwrzqq3x") (f (quote (("std" "espera/std") ("safe" "espera/safe") ("nightly") ("default" "std" "safe")))) (r "1.65.0")))

