(define-module (crates-io re v- rev-up-your-harley) #:use-module (crates-io))

(define-public crate-rev-up-your-harley-0.1.0 (c (n "rev-up-your-harley") (v "0.1.0") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "0yp4wl3rbhyqh8biqnbw46js909g7vrbypr0n4i2f4m8r00pnj8j")))

(define-public crate-rev-up-your-harley-0.1.1 (c (n "rev-up-your-harley") (v "0.1.1") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "0q6q17ml9ijrxjj4gx4yf6g3sxz5y3hq3wxsmwnzlwa2ifz82lkn")))

(define-public crate-rev-up-your-harley-0.1.2 (c (n "rev-up-your-harley") (v "0.1.2") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "1kxazxhplq94djd6r9i733zd2h19pcap21fy3nmigvq2s7jyl36l")))

(define-public crate-rev-up-your-harley-0.1.3 (c (n "rev-up-your-harley") (v "0.1.3") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "168x4a0p0mmm7hng9c00ddb1nv7w7mjf9pyb5032djgbncmh451i")))

(define-public crate-rev-up-your-harley-1.0.0 (c (n "rev-up-your-harley") (v "1.0.0") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "0vgsk4vdg56azwwvdaxpvbjy5s2s7x8m05scz4b8wbb94qlapsz4")))

(define-public crate-rev-up-your-harley-1.0.1 (c (n "rev-up-your-harley") (v "1.0.1") (d (list (d (n "unic") (r "^0.7") (d #t) (k 0)))) (h "1jz5cggk4q3gvrbic71f26wpjwx050hdy8akf17l5rsdsdgamqcq")))

