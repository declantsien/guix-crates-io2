(define-module (crates-io re pl replacer) #:use-module (crates-io))

(define-public crate-replacer-0.1.0 (c (n "replacer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1yysczjc14p6g6ci51797algzrb5xcnicav4wx3aa8z23zldbyp5")))

(define-public crate-replacer-0.1.1 (c (n "replacer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1zb1wvqdmz21va030mn8pnkvp87vsdrmnmd1nr4z732zy2ig4iqp")))

(define-public crate-replacer-0.1.2 (c (n "replacer") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "066h4wdxgclwqrmbmn805h66bc1fqkz7fl343844836szgairyag")))

(define-public crate-replacer-0.1.3-alpha.0 (c (n "replacer") (v "0.1.3-alpha.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1v1ygrhyz66nzb3a0xlbdb2zfrbdbi5z8rc28w4fmgk68l89n1px")))

(define-public crate-replacer-0.1.3 (c (n "replacer") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "07dnyjaz63fbbln6yz4qfx0asdsj41dzqj70x3cy5m03r24kwjil")))

(define-public crate-replacer-0.2.0 (c (n "replacer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0lnlaz8170v14wml48jyq0iziyq68rhsgbz3d0vxrn0g7l2cghi1")))

(define-public crate-replacer-0.3.0 (c (n "replacer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "00y76n6675wxbqww519xpz2mjf8mgyhmhq878cdmbidizdnmmbc9")))

(define-public crate-replacer-0.3.1 (c (n "replacer") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "02xcfjr7mja7kysqfbx1z5nl2hf4mif63azng3wbrz6n7vkwbzq7")))

(define-public crate-replacer-0.4.0 (c (n "replacer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1cfrciwgqkw6k5c5yw2gdr290v5ckcsnwc7kym5lb9klgx9n72c3")))

