(define-module (crates-io re pl replman_derive_impl) #:use-module (crates-io))

(define-public crate-replman_derive_impl-0.1.0 (c (n "replman_derive_impl") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15h6ijv1vx7f9ix45px5sm35gykl9azfhnk5nf10yl76d3j0y81b")))

