(define-module (crates-io re pl replace_err) #:use-module (crates-io))

(define-public crate-replace_err-0.1.0 (c (n "replace_err") (v "0.1.0") (h "0x42swyly24kvs2j9ah2pkzd3rlc0cf6dkcf0yl8cd18xm6wiszy")))

(define-public crate-replace_err-0.2.0 (c (n "replace_err") (v "0.2.0") (h "1bj20z2dflhyzvz6sckiln2nnpancdh9np5v69qgcf77bqymkyw5")))

(define-public crate-replace_err-1.0.0 (c (n "replace_err") (v "1.0.0") (h "1qf8bn0fbjq3c3sns48d26nygfkqd5vkvv3np90ml45qkmkkjk7m")))

