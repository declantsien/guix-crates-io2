(define-module (crates-io re pl replace-types) #:use-module (crates-io))

(define-public crate-replace-types-0.1.0 (c (n "replace-types") (v "0.1.0") (d (list (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x1iz8nffqla964447v7c5z1i5gvjd7k28b0fphr6x7mwrxrmqn1")))

(define-public crate-replace-types-0.1.1 (c (n "replace-types") (v "0.1.1") (d (list (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zvijww84fsh6i8xqjvg11gvv25yvp8q557jacx6b0zh3k3ysam0")))

(define-public crate-replace-types-0.2.0 (c (n "replace-types") (v "0.2.0") (d (list (d (n "syn") (r "^2") (f (quote ("extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1vp97yl5p44322nqnh3lsx857p61h1xzc0ha0lbz8pkwdvxlmvzd")))

