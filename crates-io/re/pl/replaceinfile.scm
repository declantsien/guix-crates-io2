(define-module (crates-io re pl replaceinfile) #:use-module (crates-io))

(define-public crate-ReplaceInFile-0.1.0 (c (n "ReplaceInFile") (v "0.1.0") (h "02pk7782ixh805i0fpsfn7bzgz676b6qqpb1njj1v0kryjcclj42")))

(define-public crate-ReplaceInFile-0.1.1 (c (n "ReplaceInFile") (v "0.1.1") (h "07g5vclnxhfgm1h0l7xagr1723q2j60br7jin05vdiqzmylpcqfy")))

(define-public crate-ReplaceInFile-0.1.2 (c (n "ReplaceInFile") (v "0.1.2") (h "0x7722hqx9wjrgs2vcrgk30k1rvfbzgcnir6fg4kk3wlb2d34nz5")))

(define-public crate-ReplaceInFile-0.1.3 (c (n "ReplaceInFile") (v "0.1.3") (h "00d1f9qriprqsz65iz37yq0rdp15gyxfdx9j8xnw4jshlh77j3r2")))

