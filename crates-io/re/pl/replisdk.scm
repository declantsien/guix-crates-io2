(define-module (crates-io re pl replisdk) #:use-module (crates-io))

(define-public crate-replisdk-0.1.0 (c (n "replisdk") (v "0.1.0") (d (list (d (n "replisdk-proc") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01fbpafaqspp3gvsfjvsi05fg0szj1c2vmc6qz672g4pxjkpijxi") (f (quote (("platform-models") ("platform" "platform-models")))) (r "1.60")))

