(define-module (crates-io re pl replicante_util_upkeep) #:use-module (crates-io))

(define-public crate-replicante_util_upkeep-0.1.0 (c (n "replicante_util_upkeep") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "humthreads") (r "^0.1.3") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 2)) (d (n "replicante_util_failure") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.9") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "0rqikwwab857db3a08440nibbfyszflhgzmvp8nb8fkdipdb1p76")))

(define-public crate-replicante_util_upkeep-0.2.0 (c (n "replicante_util_upkeep") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "humthreads") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.16.0") (d #t) (k 2)) (d (n "replicante_util_failure") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.9") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "14jlrl3494hzzhajmvllapv9syzg9xs1jrrpf6xky0m917xvlgmr")))

(define-public crate-replicante_util_upkeep-0.2.1 (c (n "replicante_util_upkeep") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "humthreads") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "replicante_util_failure") (r "^0.1.3") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.9") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "0psz53ln2yjjl3b8ys2936dd8mb5ppl90hlap323k1lqnh81zpry")))

(define-public crate-replicante_util_upkeep-0.2.2 (c (n "replicante_util_upkeep") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "humthreads") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "replicante_util_failure") (r "^0.1.3") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.9") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "0sgc7z3ym0ss8gycakl52y0g19g807jbkpj2w1a7idm7kxf8dk85")))

