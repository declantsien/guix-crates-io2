(define-module (crates-io re pl replace-map) #:use-module (crates-io))

(define-public crate-replace-map-0.0.1 (c (n "replace-map") (v "0.0.1") (h "18jd9ffb74zv5qvqd9c1i9mndxqlhxy07q4mlks91al8sak199md")))

(define-public crate-replace-map-0.0.2 (c (n "replace-map") (v "0.0.2") (h "0rly5dz4q343rx3j5dvvibd1aq61ns00d31vzir6qjkk8n0ppa2w")))

(define-public crate-replace-map-0.0.3 (c (n "replace-map") (v "0.0.3") (h "0vwfng3csqvw1zlmh25lakknnpq5y8mr95xsfbsc7wmwirgcgzqv")))

(define-public crate-replace-map-0.0.4 (c (n "replace-map") (v "0.0.4") (h "0nk42w6dnv1ji1j7dwj8afpmk9dmfx3ci8y488yk0kbyxs1y6iqs")))

(define-public crate-replace-map-0.0.5 (c (n "replace-map") (v "0.0.5") (h "1b2jwm6y04vga65lsl85xal2c9vg9nf04217zsp4nl0xkly73v57")))

(define-public crate-replace-map-0.0.6 (c (n "replace-map") (v "0.0.6") (h "0av04zi9jmvcfyf97r7iwrpxwxk59h3khi9v3wc8ci5xm6kasmvx")))

(define-public crate-replace-map-0.0.7 (c (n "replace-map") (v "0.0.7") (h "1c4rc2cwk7hqq64zj426jbwvx401q8f743ji4c4xdabqkfz85qa5")))

