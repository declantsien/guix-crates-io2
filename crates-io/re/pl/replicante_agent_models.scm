(define-module (crates-io re pl replicante_agent_models) #:use-module (crates-io))

(define-public crate-replicante_agent_models-0.1.1 (c (n "replicante_agent_models") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 2)))) (h "07rkgdl5wlb6sf25s5q8kpfk8rry6njc6i54bmhjazzggvr7z9fr")))

(define-public crate-replicante_agent_models-0.2.0 (c (n "replicante_agent_models") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 2)))) (h "19jpb2h8k9g4lip7daw19l5wj4cs92cirp4jh7m8n9vaqq9rnm09")))

(define-public crate-replicante_agent_models-0.2.1 (c (n "replicante_agent_models") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 2)))) (h "12swwcrj4l38jymrprizmsj2nzl1c3ww0i9sbw5aaiv8fsamdi1w")))

