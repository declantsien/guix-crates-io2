(define-module (crates-io re pl replit_audio) #:use-module (crates-io))

(define-public crate-replit_audio-0.1.0 (c (n "replit_audio") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jakrxwjgpc6wcw8yxsabp3bdvbhxaanbr8px8dqhd0qsw1kkyrk")))

