(define-module (crates-io re pl replicate) #:use-module (crates-io))

(define-public crate-replicate-0.1.0 (c (n "replicate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "palaver") (r "^0.2.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0gl00sa32ajg0fbm7ir7c3z7y2r3xaq9bzqmyxki0dy4g6m3fjxw") (r "1.56")))

(define-public crate-replicate-0.1.1 (c (n "replicate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "palaver") (r "^0.2.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1rzmf884ip49c47dpbw6a4pbsypngipzcp76xp6mva7kbxpfcvpa") (r "1.56")))

