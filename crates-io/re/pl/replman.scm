(define-module (crates-io re pl replman) #:use-module (crates-io))

(define-public crate-replman-0.1.0 (c (n "replman") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "replman_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "test-case") (r "^1.2") (d #t) (k 2)))) (h "11w5dpffxypixn4xz4gicyscxsycxsjawizvzssn4sp416lrk2ja")))

