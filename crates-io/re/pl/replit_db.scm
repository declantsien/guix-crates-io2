(define-module (crates-io re pl replit_db) #:use-module (crates-io))

(define-public crate-replit_db-0.1.0 (c (n "replit_db") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "01zv1rf1r14z7w5865yvdn2bh8bkb6qkhkxggnpbpd8r006jk6zg")))

(define-public crate-replit_db-0.1.1 (c (n "replit_db") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1hkjkclmrvcn4v838xz1vv9w1xp9jrjla7bp5vbwv7ng4167hf61")))

(define-public crate-replit_db-0.1.2 (c (n "replit_db") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rdqgk5w5gwq2fq6cm4p6wvsq7vk2zcbgs5lspr4k0i9bmgivhb2")))

(define-public crate-replit_db-0.1.3 (c (n "replit_db") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1g7j234bc0cxhdrxpswhp99iin5ic2jq87rnrv0cqgklxg2sx0a3")))

(define-public crate-replit_db-1.0.0 (c (n "replit_db") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "02cv4518r6jg7ppwjd0zjl8ml29g6jwrzr5w8d87bb9bi2h718rj")))

(define-public crate-replit_db-1.0.1 (c (n "replit_db") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0kmnhgfji5rw02zl0bkfgra49kgpsbg2phdcvinpjh323p0hm9a8")))

(define-public crate-replit_db-1.0.2 (c (n "replit_db") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0qwlw4kq0bhgcznkaww689i0b130sp2kwgfy8sh42f9xwn3big2r")))

(define-public crate-replit_db-1.1.0 (c (n "replit_db") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1sb026a658cwaiqj8860ac6sp6vfwbn8vjh4lfllg2rpacb8nfz7")))

(define-public crate-replit_db-1.1.1 (c (n "replit_db") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1c02fqpii2387nyncmvyw4pcaqnc42282gvgnl9f8pndrl9h1hsb")))

(define-public crate-replit_db-1.1.2 (c (n "replit_db") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0gixkfm39lgmifwnbzqg7sf04kl6qglgmzqq3xxp13nsc62q0hh4")))

(define-public crate-replit_db-1.1.3 (c (n "replit_db") (v "1.1.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "155wbimk4kpfi47s6ds2pfsk974qv4mcfd9rqpkaq5xyb81bwmgd")))

(define-public crate-replit_db-1.1.4 (c (n "replit_db") (v "1.1.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0w9ghlc89y4pyzqxff4ghhg2rvqb4iw313bc2yl3jz8npikfmrni")))

(define-public crate-replit_db-1.2.0 (c (n "replit_db") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1vzdr1ssld22hrcmp2a0bp1ji0hbf0ksikag3bcz1klnphf0i3x5")))

