(define-module (crates-io re pl replitdb) #:use-module (crates-io))

(define-public crate-replitdb-0.1.1 (c (n "replitdb") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "137fm3p4wi8gvb524kbgjcbrhqla4lbcyx4dswz7yxyfwrahgs90")))

(define-public crate-replitdb-0.1.2 (c (n "replitdb") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "088ks4a8zqz991zjbq64fiq1hn6qk4gp85cwf4bpfjfjrd1srwh3")))

(define-public crate-replitdb-0.1.3 (c (n "replitdb") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1pjychhcs0q2dyqk0ggn7qrj8w1h269n1klc6ml37cwj94rmq5yx")))

