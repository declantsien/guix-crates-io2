(define-module (crates-io re pl replace_with) #:use-module (crates-io))

(define-public crate-replace_with-0.1.0 (c (n "replace_with") (v "0.1.0") (h "0kikhywmv9ajq7q60mmiv1gsw62mvgkfpbphs8w1nyavrbr18ng0") (y #t)))

(define-public crate-replace_with-0.1.1 (c (n "replace_with") (v "0.1.1") (h "0a1r5j2b7xydda971z0nijlcp8i6pmxii6kqiqq5vshr91sdb3m5") (y #t)))

(define-public crate-replace_with-0.1.2 (c (n "replace_with") (v "0.1.2") (h "1nvdh708yjd98d2dfv5vzcclnizlzvv1lnsl6rxq5n9x5zl7iylb") (y #t)))

(define-public crate-replace_with-0.1.3 (c (n "replace_with") (v "0.1.3") (h "1vnz00d1gn39a2ifsl7ybahmdbw9kdwx2kq2vkd9gs90962wnlnl") (f (quote (("std") ("panic_abort") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-replace_with-0.1.4 (c (n "replace_with") (v "0.1.4") (h "1f0x8a1bi8vlnnhj4bqicppgndm0v8416fffi3k1ky0wvhkanni7") (f (quote (("std") ("panic_abort") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-replace_with-0.1.5 (c (n "replace_with") (v "0.1.5") (h "1ap76cw88vh5as3mrgm6wifnlihvg10jhqnm3ksf5siy9qw8cqp7") (f (quote (("std") ("panic_abort") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-replace_with-0.1.6 (c (n "replace_with") (v "0.1.6") (h "1amgpwak0n6lyhnncljc8h901bz74hwm2gxg5r73hwg96hxsca2g") (f (quote (("std") ("panic_abort") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-replace_with-0.1.7 (c (n "replace_with") (v "0.1.7") (h "142n16d280wiwwi5g2j2y55mkcwidnkg9z6g5gk1ss9mwi763a73") (f (quote (("std") ("panic_abort") ("nightly") ("default" "std"))))))

