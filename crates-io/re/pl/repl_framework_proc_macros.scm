(define-module (crates-io re pl repl_framework_proc_macros) #:use-module (crates-io))

(define-public crate-repl_framework_proc_macros-0.1.0 (c (n "repl_framework_proc_macros") (v "0.1.0") (h "10iq33j174pcrwzjl8yc6nwvfaad7s3c3s92iy54rgvq3j1kwk68") (y #t)))

(define-public crate-repl_framework_proc_macros-0.1.1 (c (n "repl_framework_proc_macros") (v "0.1.1") (h "1rx18ilbx6dgwmcy4zkkjgx946j9xhbpf0nzpc9rgxvqi6hvrdkj") (y #t)))

