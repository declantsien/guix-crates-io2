(define-module (crates-io re pl replaces) #:use-module (crates-io))

(define-public crate-replaces-0.0.0 (c (n "replaces") (v "0.0.0") (d (list (d (n "aoko") (r "^0.3.0-alpha.25") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "000yrhj0x0h9v389apgzdj1by682fi6rcnms5xir7hc55297vn18")))

(define-public crate-replaces-0.0.1 (c (n "replaces") (v "0.0.1") (d (list (d (n "aoko") (r "^0.3.0-alpha.25") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q5gc7iqj1gl26ks39zbsf5q65ygnr2ma32sncdmrij7bwh10x8m")))

(define-public crate-replaces-0.0.2 (c (n "replaces") (v "0.0.2") (d (list (d (n "aoko") (r "^0.3.0-alpha.25") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0824900gs04iad1jrqr7l5asmbd8r9231fb6p1sx5035690r0ncv")))

