(define-module (crates-io re pl replace_ident) #:use-module (crates-io))

(define-public crate-replace_ident-0.1.0 (c (n "replace_ident") (v "0.1.0") (h "1c7mxh4psmn33i2yzhbzmkrhprc0ww3pyx8xfdzbbr8qggzk0ifp") (r "1.56")))

(define-public crate-replace_ident-0.1.1 (c (n "replace_ident") (v "0.1.1") (h "0b615flp3lp5gs42jb950ddw07vl9zinpb4ialshjnzisfiy0m57") (r "1.56")))

