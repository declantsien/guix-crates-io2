(define-module (crates-io re pl replicante_util_failure) #:use-module (crates-io))

(define-public crate-replicante_util_failure-0.1.0 (c (n "replicante_util_failure") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "slog") (r "^2.2.0") (d #t) (k 0)))) (h "0xwp4q7zipz97ya9zwi6b2i1yifnl2qfwqr5n460n1a2p77qn9c4")))

(define-public crate-replicante_util_failure-0.1.1 (c (n "replicante_util_failure") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "sentry") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "slog") (r "^2.2.0") (d #t) (k 0)))) (h "180d9gz21cz4qv9ljimz0gvds3nkdpwzh0yix5pv94kjjwmxhi51")))

(define-public crate-replicante_util_failure-0.1.2 (c (n "replicante_util_failure") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "sentry") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "slog") (r "^2.2.0") (d #t) (k 0)))) (h "1mpks34s52k5ahl1gb9wwzir5gpz6chvkdaz3qlnm4qjhw9kjda5")))

(define-public crate-replicante_util_failure-0.1.3 (c (n "replicante_util_failure") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "sentry") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "slog") (r "^2.2.0") (d #t) (k 0)))) (h "0nhxclbgi6mkq94rwb1d02nh5avrvyqf2c2n8hv31l8nfgc20r9s")))

(define-public crate-replicante_util_failure-0.1.4 (c (n "replicante_util_failure") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "sentry") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "slog") (r "^2.2.0") (d #t) (k 0)))) (h "19fyqjcdmdc1l1s8n7jggm5497pflc93cj98qiiw5457hgjyypad")))

