(define-module (crates-io re pl repl_framework) #:use-module (crates-io))

(define-public crate-repl_framework-0.1.5 (c (n "repl_framework") (v "0.1.5") (h "106b3ijjn7rilkmbzcvfpj2frr5nnkqa3pyfxwy54any4xww2jj5")))

(define-public crate-repl_framework-0.1.6 (c (n "repl_framework") (v "0.1.6") (h "0jar6sl702idn3djyacjy8kcagq06s28yzaa18rcznn82mf4n50b")))

(define-public crate-repl_framework-0.1.7 (c (n "repl_framework") (v "0.1.7") (h "0mdjaasb1xwsfqv035fk21v02lab83xrkpy6wp5wi8w5hwzvm8i7") (y #t)))

(define-public crate-repl_framework-0.1.71 (c (n "repl_framework") (v "0.1.71") (h "1nwqqwxxgfljwv3r0i1m1vipcbc59d6y7b7nnpl0c60203g761f5") (y #t)))

(define-public crate-repl_framework-0.1.72 (c (n "repl_framework") (v "0.1.72") (d (list (d (n "repl_framework_proc_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0346isrhwgn9jzmg07rgf82jbb3g3mmlsyh5p3r7fqyizhrvkqhw")))

(define-public crate-repl_framework-0.1.73 (c (n "repl_framework") (v "0.1.73") (d (list (d (n "repl_framework_proc_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1ssm6ks7fzaxpl5qpwggd155klr7glxr19xi1har26na6sddja1a")))

(define-public crate-repl_framework-0.1.74 (c (n "repl_framework") (v "0.1.74") (d (list (d (n "repl_framework_proc_macros") (r "^0.1.1") (d #t) (k 0)))) (h "029y4pvrqq2dz35qrn0gqvkz561591h6v9vnhz76hbpg9x4is8g7")))

(define-public crate-repl_framework-0.2.0 (c (n "repl_framework") (v "0.2.0") (h "1awrq37vy64w1iwjcdwg7ll7rbjp01xk9qmn2zldwch584m0a3kc")))

(define-public crate-repl_framework-0.2.1 (c (n "repl_framework") (v "0.2.1") (h "1mkdw8vsk0y2vl02gwyhrxli2xiviiinlgxlafw06bwkmakm7xmw")))

(define-public crate-repl_framework-0.3.0 (c (n "repl_framework") (v "0.3.0") (h "1pjbhqn6mw6x8f8a65bmlgb89520jp2ws03q5a5qhr1bj5sjswj1")))

