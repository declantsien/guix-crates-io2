(define-module (crates-io re pl replacing-buf-stream) #:use-module (crates-io))

(define-public crate-replacing-buf-stream-0.1.0 (c (n "replacing-buf-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1.5") (d #t) (k 2)) (d (n "tower-web") (r "^0.3.7") (d #t) (k 0)))) (h "004x18jm2k6c9208fyril06cbaxhrr4wha588k5655zy8g5pw936")))

