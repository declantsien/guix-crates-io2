(define-module (crates-io re pl replace_html) #:use-module (crates-io))

(define-public crate-replace_html-0.1.0 (c (n "replace_html") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "HtmlElement"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "189yrih526ywfd48xpf6shbz6khwllav6clk4d9fc2ayd6dakfii")))

