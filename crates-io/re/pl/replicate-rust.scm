(define-module (crates-io re pl replicate-rust) #:use-module (crates-io))

(define-public crate-replicate-rust-0.0.1 (c (n "replicate-rust") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0s79jb6vbjhvqyhr5hlg9kman2cfdz08cz0m7p99d46m5i8jkw1h") (y #t)))

(define-public crate-replicate-rust-0.0.2 (c (n "replicate-rust") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "00xx433l40qyswlk8xh5vjnlhfd4jjn1gg76242gbx3gzyl6ysd8")))

(define-public crate-replicate-rust-0.0.3 (c (n "replicate-rust") (v "0.0.3") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0c2xv9fsshr0948qlkhyay0w3qjbiia60iis7ag5gks8j50alpgb")))

(define-public crate-replicate-rust-0.0.4 (c (n "replicate-rust") (v "0.0.4") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0cdy6blzl485qhq5svnjd8pabph5ji6l9nx689ymmw7v0gykg4fj")))

(define-public crate-replicate-rust-0.0.5 (c (n "replicate-rust") (v "0.0.5") (d (list (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0a9qc59qz59ihh1gn7gw1avbf6nkrgi5519blpkvvl181xy2m4v1")))

