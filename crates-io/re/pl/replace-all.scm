(define-module (crates-io re pl replace-all) #:use-module (crates-io))

(define-public crate-replace-all-0.1.0 (c (n "replace-all") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0wxzzla9h9vzjfs2gssxsrm54zjgx2grfba7kvfa6zh6q241j2x6")))

(define-public crate-replace-all-0.1.1 (c (n "replace-all") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1a6zw013zarbm2w9iyda83a4yvdpxdlzjkiw9kzbfp2ddpsl77qh")))

