(define-module (crates-io re pl replay-channel) #:use-module (crates-io))

(define-public crate-replay-channel-0.1.0 (c (n "replay-channel") (v "0.1.0") (h "0rja83bw41iv8czd6kgv9k70i5lgbwg7w1j6dx2z5balbwnsmra0")))

(define-public crate-replay-channel-0.1.1 (c (n "replay-channel") (v "0.1.1") (h "1cyy0cd90kw8kbsvxyvxm42avvqv9zmdr9rd17q3v5n797vwnh3b")))

(define-public crate-replay-channel-0.1.2 (c (n "replay-channel") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0rlrmmvhsah884x4aqb6w2md9hrjlfirb2ljzdgw3lzzmxh5w2a3")))

(define-public crate-replay-channel-0.1.4 (c (n "replay-channel") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0j4cfcyfdwlvpr3pl7blc1xj0cd5jkk1s4l1zqrx67gk7nswrzff")))

(define-public crate-replay-channel-0.1.5 (c (n "replay-channel") (v "0.1.5") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "10qnmk186f6jy62m3dvpd10ym8y2kl6g8d5rnd0jg4cgqxwaiavh")))

(define-public crate-replay-channel-0.1.6 (c (n "replay-channel") (v "0.1.6") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "15z1s19j9g22y3zplcf68agzaag8h4pi44q0yi37fdqckrlslxw8")))

(define-public crate-replay-channel-0.1.7 (c (n "replay-channel") (v "0.1.7") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0rg7xgqqpm6aqk2jrjpg7prcbm3vyrjhp746m4xikd52621grd1n")))

(define-public crate-replay-channel-0.1.8 (c (n "replay-channel") (v "0.1.8") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "1p82h9f2z97fbandyxdb9bx65wrf0mw120p6pf58sb96fzphh54i")))

(define-public crate-replay-channel-0.1.9 (c (n "replay-channel") (v "0.1.9") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0bmjikd039ix3nidpkwfnjdc43nyvw5hn1f07pl9apk8w6sswhn0")))

(define-public crate-replay-channel-0.1.10 (c (n "replay-channel") (v "0.1.10") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0mip710jp15k8sq54as34fzhh31jnlkk2d09s6m6gxly5lpg08nn")))

(define-public crate-replay-channel-0.1.11 (c (n "replay-channel") (v "0.1.11") (d (list (d (n "append-only-vec") (r "^0.1.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0b25qyif3i2plffanjrnwwrcbfjwy9gz8px7fis9fp6zpyl7hpy0")))

(define-public crate-replay-channel-0.1.12 (c (n "replay-channel") (v "0.1.12") (d (list (d (n "append-only-vec") (r "^0.1.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("parking_lot" "full"))) (d #t) (k 0)))) (h "0vnhwk0nmq5j6qwhz9pzyab2rvja36jyykv07d9zqy92kap6a4sy")))

