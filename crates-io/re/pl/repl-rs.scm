(define-module (crates-io re pl repl-rs) #:use-module (crates-io))

(define-public crate-repl-rs-0.1.0 (c (n "repl-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0hzkdgplvyl4b699za7h6gmxm5fb6dlhna20jal5rwq5mmhcf49j")))

(define-public crate-repl-rs-0.2.0 (c (n "repl-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1rmirj5js002g95hg7syxdnvdhy3fyj3n9qqnzhps9gxclk6r23n")))

(define-public crate-repl-rs-0.2.1 (c (n "repl-rs") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1x7vvz4m7sxky1ry4d4c0w304hhhs73d14kkckpck74ygi6awxql")))

(define-public crate-repl-rs-0.2.2 (c (n "repl-rs") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "07f2ip4nv8zmn9bb7sw8vrm1zjm1nxymb5lhdp98315zz8lwd8dz")))

(define-public crate-repl-rs-0.2.3 (c (n "repl-rs") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0jpi4mc3bk7pyy530yx6jmmds9w02sddnbzv4f8v4nmb4gyk7lpv")))

(define-public crate-repl-rs-0.2.4 (c (n "repl-rs") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.1.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1ibgrzlvr2w5jgfnghs3ka41q8gqlsiz8jlvjjsa8sgmha7is3sc")))

(define-public crate-repl-rs-0.2.5 (c (n "repl-rs") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "14p631sjrmgnpivf4hvq58hpgvq2qdpnlwdskz56nnrmr4dasr17")))

(define-public crate-repl-rs-0.2.6 (c (n "repl-rs") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0hz934slm1ihbml13i5c1grj2h5zr1g2jgjf67hir4fcl4xyvflk")))

(define-public crate-repl-rs-0.2.7 (c (n "repl-rs") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1c6lx355qn4wwax4k0xmcnmazpjn0hafmf6bf051mkyk8r0r9kp3")))

(define-public crate-repl-rs-0.2.8 (c (n "repl-rs") (v "0.2.8") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "05vms9iamsmkjjpm8vc0y71nh3i2ddnwsnd06yik1yz0k02hhxcq")))

