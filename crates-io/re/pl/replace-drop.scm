(define-module (crates-io re pl replace-drop) #:use-module (crates-io))

(define-public crate-replace-drop-0.1.0 (c (n "replace-drop") (v "0.1.0") (h "1mp5zbv4bv5lr4n52vwfgxcs563zz3nz5r52aq91hph7vvdlp48q")))

(define-public crate-replace-drop-0.2.0 (c (n "replace-drop") (v "0.2.0") (h "1189ssi70q0gwy4hvajb490jp8aq35j9ggzfhswzh18xmlb8rfbc")))

(define-public crate-replace-drop-0.2.1 (c (n "replace-drop") (v "0.2.1") (h "0lik6ri1vkb6h6mdb6gybzcl4wz66r75li4fnzvbaambz9y7mhaj")))

