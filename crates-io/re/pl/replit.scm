(define-module (crates-io re pl replit) #:use-module (crates-io))

(define-public crate-replit-0.1.0 (c (n "replit") (v "0.1.0") (h "195xrgg1lkv7zar8gzdw8p7hh6ddl4hh0anjx55ivyzgzrf4idhz") (y #t)))

(define-public crate-replit-0.1.1 (c (n "replit") (v "0.1.1") (h "1p5ahnl66sfh0g0apg57r8zmrj5kir2kggl9d4p7ljmm6s6l64h6")))

