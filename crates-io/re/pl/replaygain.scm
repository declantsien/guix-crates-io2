(define-module (crates-io re pl replaygain) #:use-module (crates-io))

(define-public crate-replaygain-1.0.0 (c (n "replaygain") (v "1.0.0") (h "0843fr5qnv3rrcv9z1jjw2xy9a76ji46z2vz17ng12pgjgba8ghh")))

(define-public crate-replaygain-1.0.1 (c (n "replaygain") (v "1.0.1") (h "1k6yqhakf3qqd5ndw1apnb55jc41xg4lbapi8hmk3cr28xf78iny")))

