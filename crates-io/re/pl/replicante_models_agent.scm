(define-module (crates-io re pl replicante_models_agent) #:use-module (crates-io))

(define-public crate-replicante_models_agent-0.3.0 (c (n "replicante_models_agent") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 2)))) (h "1w3bvibsvzdcavmw8lc5jraq1lyilxyflakwbq7kxwfbddc4lcrc")))

(define-public crate-replicante_models_agent-0.3.1 (c (n "replicante_models_agent") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "04ay13glvlb1xv7bixqd70im8m76n006cwf58qkqfya39n0iysin")))

(define-public crate-replicante_models_agent-0.3.2 (c (n "replicante_models_agent") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1736a0q43ik1f9y86mrrx6l0aasnj5xjh7xp6y1dbp2h4qgdvah8")))

