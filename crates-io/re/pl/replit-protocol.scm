(define-module (crates-io re pl replit-protocol) #:use-module (crates-io))

(define-public crate-replit-protocol-0.1.0 (c (n "replit-protocol") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "10aswd5v0wk3x1v96cp73xjni14hn25p8dri4pkgx1sdb9pbvwan")))

(define-public crate-replit-protocol-0.1.1 (c (n "replit-protocol") (v "0.1.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "0hfwqjcdkm6shp09bgq03rgi3z8mwaf9whrxg5rsm75na1qmw561")))

(define-public crate-replit-protocol-0.2.0 (c (n "replit-protocol") (v "0.2.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "0j5n9nz0awy1ffskz3c4frplqrn0c43qf1l5m4xr3mqp02vs8fal")))

(define-public crate-replit-protocol-0.2.1 (c (n "replit-protocol") (v "0.2.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "1vhyrz2lc1x1kmlwg7m21vfmyb2wbnbcdhah33rszxs6j76id0f2")))

(define-public crate-replit-protocol-0.2.2 (c (n "replit-protocol") (v "0.2.2") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "0avarsfm42r24yhza89qc0ikahzzyvvi3hb444j82rpgp3103k41")))

(define-public crate-replit-protocol-0.2.3 (c (n "replit-protocol") (v "0.2.3") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "104ymc7h5dyckmla5rr5c2v78akzs6idwjqyj8rc5syic32mcgd5")))

(define-public crate-replit-protocol-0.2.4 (c (n "replit-protocol") (v "0.2.4") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "0gwkwm9yksy65i6lfjhxfx73jgcj65hyip7mlz0h9ilvw3pc8n33")))

