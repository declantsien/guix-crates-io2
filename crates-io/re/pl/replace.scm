(define-module (crates-io re pl replace) #:use-module (crates-io))

(define-public crate-replace-0.1.0 (c (n "replace") (v "0.1.0") (d (list (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1y2p6n2fy86hvx1ff3yk6n9xga0ca9m38190cl3v933g8ag3yprj")))

(define-public crate-replace-0.1.1 (c (n "replace") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "10vjnqsz8ijakaz574scw52da9dzm97pkliwx9zgpa82bk7l892f")))

(define-public crate-replace-0.1.2 (c (n "replace") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "051sbpy3nxgl62f0s3gpjks15vsnsfrs9k8gry4pf9fjwjb377cf")))

