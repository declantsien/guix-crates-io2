(define-module (crates-io re pl replay_filter) #:use-module (crates-io))

(define-public crate-replay_filter-0.1.1 (c (n "replay_filter") (v "0.1.1") (d (list (d (n "honggfuzz") (r "^0.5") (d #t) (t "cfg(fuzzing)") (k 0)))) (h "0jialvg1kwmxwjrljzaj830qpvmg2jxbgmiw1qdg1gb5y2anp8rb")))

(define-public crate-replay_filter-0.1.2 (c (n "replay_filter") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "honggfuzz") (r "^0.5") (d #t) (t "cfg(fuzzing)") (k 0)))) (h "1sh0p6pfs4rwypazpswb34p3v30fpmx35x3b7lb78lznbns14bz9")))

