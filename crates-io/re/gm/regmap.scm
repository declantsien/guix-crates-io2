(define-module (crates-io re gm regmap) #:use-module (crates-io))

(define-public crate-regmap-0.1.0 (c (n "regmap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)) (d (n "yaxpeax-x86") (r "^0.1.5") (k 0)))) (h "03rb2afq25vlfm4iimqmgc3sy5yx56a07m9pmbfs2m9x16f77q72") (f (quote (("std") ("default" "std"))))))

