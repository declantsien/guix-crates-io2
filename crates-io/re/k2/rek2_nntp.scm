(define-module (crates-io re k2 rek2_nntp) #:use-module (crates-io))

(define-public crate-rek2_nntp-0.1.0 (c (n "rek2_nntp") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)))) (h "0r8hm2zlvss0lwikb6nj729i6qdxs95z1i9k5h4sdmg87hkifryh")))

(define-public crate-rek2_nntp-0.1.1 (c (n "rek2_nntp") (v "0.1.1") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)))) (h "0qjg9zg1vcgf127ch773fzfzplki3dkv3khjivip903zrcr6i8zf")))

(define-public crate-rek2_nntp-0.1.2 (c (n "rek2_nntp") (v "0.1.2") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.1") (d #t) (k 0)))) (h "1rysjadv7zbqspf2ccgwqsa6cs347giwam6bh4i47m9jpkimlzpb")))

(define-public crate-rek2_nntp-0.1.3 (c (n "rek2_nntp") (v "0.1.3") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.1") (d #t) (k 0)))) (h "0x8mw2764nz7i13xim6dwi4w8rks2hynpaxjbjn35bjmam7z5xyb")))

(define-public crate-rek2_nntp-0.1.4 (c (n "rek2_nntp") (v "0.1.4") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.1") (d #t) (k 0)))) (h "0gmq6yivax0czcx03p65x4ilg0zmmj4x05136xp1jx49ajqxh1hw")))

(define-public crate-rek2_nntp-0.1.5 (c (n "rek2_nntp") (v "0.1.5") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rustls") (r "^0.21.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.1") (d #t) (k 0)))) (h "1pxv5mzn422sl6m00mcar7w3hix0ydk1r9v6f8s0gmgp0qipbd5m")))

