(define-module (crates-io re sh reshape_helper) #:use-module (crates-io))

(define-public crate-reshape_helper-0.1.0 (c (n "reshape_helper") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06myanql62n1b6ni66gdn1n70qqqkzrazsrpkmxz6za9p2i0px6v")))

(define-public crate-reshape_helper-0.1.1 (c (n "reshape_helper") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15667r2f7n714any7291qfcc9452g3w5g3ifv7p4sgavg9y6sydc")))

