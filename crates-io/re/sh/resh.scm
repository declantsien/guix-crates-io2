(define-module (crates-io re sh resh) #:use-module (crates-io))

(define-public crate-resh-0.1.1 (c (n "resh") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0y209yqhfw47p4g51xngsnykzvzqgpp462pwm4gii1rvy3rqxzsc")))

(define-public crate-resh-0.1.2 (c (n "resh") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "059yk8ikg5l2j0miahhl5i7avfc2g0zicxhmndmjza16vai269j1")))

(define-public crate-resh-0.1.3 (c (n "resh") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1fqadcsa2ggk27qpzghalqvrl810wyj362nch439jvjj3339kzd9")))

