(define-module (crates-io re ts retstat) #:use-module (crates-io))

(define-public crate-retstat-1.0.0 (c (n "retstat") (v "1.0.0") (d (list (d (n "rocket") (r "^0.4.4") (d #t) (k 0)))) (h "02z1albm3ax9mr6g4b3l2hvpaqvg9f2rnbrs6k929ivl4hzyprva")))

(define-public crate-retstat-1.0.1 (c (n "retstat") (v "1.0.1") (d (list (d (n "rocket") (r "^0.4.4") (d #t) (k 0)))) (h "0af5dkj3pbhhp86h7001rqpjvzcbzng0a05qr29bf0kqz61j7mzm")))

(define-public crate-retstat-1.1.0 (c (n "retstat") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7.0") (d #t) (k 0)))) (h "0vw0x1232r2y7wddysx4dwpasqjixydpjm68zbkgr8b2696m2r85")))

(define-public crate-retstat-1.1.1 (c (n "retstat") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7.0") (d #t) (k 0)))) (h "0rbfy1g71cms6d0fqaiyifmz6n5blgm98kisvcr0n4w3f3al48sb")))

