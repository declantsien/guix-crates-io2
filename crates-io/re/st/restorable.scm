(define-module (crates-io re st restorable) #:use-module (crates-io))

(define-public crate-restorable-0.1.0 (c (n "restorable") (v "0.1.0") (h "0hcmipzfp58py9a2p5ndgqz0alw85x651lfsaagnppn3rvl9bga6")))

(define-public crate-restorable-0.2.0 (c (n "restorable") (v "0.2.0") (h "0swddfnxvzk1kkbbwj76aji9wxds29vfa6h18871yc38qff893q5")))

