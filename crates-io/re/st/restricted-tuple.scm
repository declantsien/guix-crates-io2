(define-module (crates-io re st restricted-tuple) #:use-module (crates-io))

(define-public crate-restricted-tuple-0.1.0 (c (n "restricted-tuple") (v "0.1.0") (d (list (d (n "integral-exponential-polynomial") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.37") (k 0)) (d (n "num-integer") (r "^0.1.33") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.33") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1bz9imbr83sgcbs2ylpcbrnr0bnc1mnkic5n5ivzf2bsfl5wy3hz")))

(define-public crate-restricted-tuple-0.2.0 (c (n "restricted-tuple") (v "0.2.0") (d (list (d (n "integral-exponential-polynomial") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.39") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "15ivsf4girvh5mywg8lwgakspi1xx9yvzccm1y9h816aqwldlc0a")))

