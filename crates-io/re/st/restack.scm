(define-module (crates-io re st restack) #:use-module (crates-io))

(define-public crate-restack-0.6.0 (c (n "restack") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 2)) (d (n "indoc") (r "^1.0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "02aibrr7cfr7qj6imhm89szh6wrcjq4v9vv7mpklkgiw1jflyszm")))

(define-public crate-restack-0.6.1 (c (n "restack") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 2)) (d (n "indoc") (r "^1.0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "04w57pxpn6v4agzvs8pjx4ihrd6siylh889f38nwn111ccd3irm6")))

(define-public crate-restack-0.7.0 (c (n "restack") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "03h2s42agwf4gni81fm1mdppsr3rpm63gs3npk0z17dv410cvd59")))

