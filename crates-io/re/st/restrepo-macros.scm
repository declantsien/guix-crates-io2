(define-module (crates-io re st restrepo-macros) #:use-module (crates-io))

(define-public crate-restrepo-macros-0.0.2 (c (n "restrepo-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1c899f5z6ljijp74g8q1ixzjjgwalqqs0kb40cfkwqn6h18jns52")))

