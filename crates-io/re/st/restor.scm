(define-module (crates-io re st restor) #:use-module (crates-io))

(define-public crate-restor-0.1.0 (c (n "restor") (v "0.1.0") (h "0yysv9v4bxpkm3c9x1ff0i3l0gagq6p105zi194l8mq44nps7g3h")))

(define-public crate-restor-1.0.0 (c (n "restor") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "179dhf3bypbviyhk6ly7i3g05jn31bl1in8dqqqi347amg7x11br")))

(define-public crate-restor-2.0.0 (c (n "restor") (v "2.0.0") (d (list (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "1bdq0xh9mb7z1rr4fz2j513icigddb6wsz54cd9pxnmfjlq5cswp")))

(define-public crate-restor-2.0.1 (c (n "restor") (v "2.0.1") (d (list (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "1mnkfjwr2q5wgayhk5d5g6ia6gw9p0vhlaamrfd3kc4ag8inh2i3")))

(define-public crate-restor-2.0.2 (c (n "restor") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "13l6v7b5w9pzqr1mwqslbs1q6vmzs9skm369q5zzrcp0argdwqn9")))

