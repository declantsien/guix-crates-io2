(define-module (crates-io re st restartables) #:use-module (crates-io))

(define-public crate-restartables-0.1.0 (c (n "restartables") (v "0.1.0") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0lfbk00m9bdwa4pxhiqzd80cb6pr3h3lmgsll6i0p1fp0qqfx1bq") (f (quote (("use_reqwest" "reqwest"))))))

(define-public crate-restartables-0.2.0 (c (n "restartables") (v "0.2.0") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0q6jajzylrhn88fxnw4673wn2zcgi4s4vywk12df7d2pwk72ax6b") (f (quote (("use_reqwest" "reqwest"))))))

(define-public crate-restartables-0.3.0 (c (n "restartables") (v "0.3.0") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ww9bj2di4hb9mxdsabv8c2lc1lri7xq0bqgdww0vv7iky131dpf") (y #t)))

(define-public crate-restartables-0.4.0 (c (n "restartables") (v "0.4.0") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1lg6xd2v3wacckl6zzn2p1xrsixwsm7fyvifk12kzmckc6ymxwcm")))

(define-public crate-restartables-0.4.1 (c (n "restartables") (v "0.4.1") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0bym1sd62sjwg129s1lvc039y82m6qfsw328ws1b6yp1k8h6bpc7")))

(define-public crate-restartables-0.4.2 (c (n "restartables") (v "0.4.2") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1z5rj0qgv195n5qy61zl19pzbslarx2wm3qv26p84kgvy85zv1za")))

