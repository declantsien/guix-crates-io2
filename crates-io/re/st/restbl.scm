(define-module (crates-io re st restbl) #:use-module (crates-io))

(define-public crate-restbl-0.1.0 (c (n "restbl") (v "0.1.0") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "lexical-core") (r "^0.8.5") (f (quote ("write-integers"))) (o #t) (k 0)) (d (n "memoffset") (r "^0.9.0") (d #t) (k 0)) (d (n "sa") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0") (d #t) (k 0)))) (h "105kl97a6wdcg5ybf54z0rxwgnb30p3vjwhn3p1np3qir2d2ly41") (f (quote (("yaml" "lexical-core") ("std" "alloc" "thiserror-no-std/std") ("default" "std") ("alloc"))))))

