(define-module (crates-io re st restless-web) #:use-module (crates-io))

(define-public crate-restless-web-0.1.0 (c (n "restless-web") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ivcmrnwa7akhg1b608m5ywsv7d65axc7y4fjnfm5233jzlqbnig")))

(define-public crate-restless-web-0.1.1 (c (n "restless-web") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gz0i807726k8g242bp155wzdljyfs5i4adcn705q2d3lfxja5k5")))

