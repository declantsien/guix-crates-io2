(define-module (crates-io re st restruct) #:use-module (crates-io))

(define-public crate-restruct-0.1.0 (c (n "restruct") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.2") (o #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r20na1b29m6ks0jv8dqnamh86zykvhbckbq50g39c8di7fsy7yr") (f (quote (("rustfmt" "rustfmt-nightly"))))))

(define-public crate-restruct-0.1.1 (c (n "restruct") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.2") (o #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1syvr40zgzjzywp7zrl3n95rqxa01blv7w7lb0xpn1vp1i7bqxfl") (f (quote (("rustfmt" "rustfmt-nightly"))))))

(define-public crate-restruct-0.1.2 (c (n "restruct") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.2") (o #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cn9pzf6gmw1y4c9ikmqyv8q33azikzhrg4411gcq6fcy326cp4d") (f (quote (("rustfmt" "rustfmt-nightly"))))))

