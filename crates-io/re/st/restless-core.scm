(define-module (crates-io re st restless-core) #:use-module (crates-io))

(define-public crate-restless-core-0.1.0 (c (n "restless-core") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1cwx454nbmd9csya6xbkw0cjg963myv1xx24kqgrzid9rqcrhv3b") (f (quote (("full" "bytes" "http")))) (s 2) (e (quote (("http" "dep:http") ("bytes" "dep:bytes"))))))

