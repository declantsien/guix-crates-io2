(define-module (crates-io re st restless-axum) #:use-module (crates-io))

(define-public crate-restless-axum-0.1.0 (c (n "restless-axum") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "restless-core") (r "^0.1.0") (d #t) (k 0)) (d (n "restless-hyper") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0v6vhlvcjy4vyxldf7zk0yp986wh2fg6fvcyrbzx7wcwmwmplrrk")))

