(define-module (crates-io re st resty) #:use-module (crates-io))

(define-public crate-resty-0.1.0 (c (n "resty") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cdy7x9z1nbxigy7bjnaj91748bralzppzs7jpmbprxk7yrpjam3")))

