(define-module (crates-io re st restless-gloo) #:use-module (crates-io))

(define-public crate-restless-gloo-0.1.0 (c (n "restless-gloo") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "gloo-net") (r "^0.3.1") (d #t) (k 0)) (d (n "restless-core") (r "^0.1.0") (f (quote ("http"))) (d #t) (k 0)) (d (n "restless-data") (r "^0.1.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1hn9qzg2d3mik4psjwla10qx16kwijdm8gfaqfmk5b71k69di9vh")))

