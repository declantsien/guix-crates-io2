(define-module (crates-io re st restrust) #:use-module (crates-io))

(define-public crate-restrust-0.1.0 (c (n "restrust") (v "0.1.0") (h "11mflpbgcwg07n2gr14f6679cc9rzwphazrfrr1swr80fxqx6x8a")))

(define-public crate-restrust-0.2.0 (c (n "restrust") (v "0.2.0") (h "0k1l4hjw51y1xhxgp0724zjxydgdlxj6r0idh7ixga4shwww8gcs")))

(define-public crate-restrust-0.2.1 (c (n "restrust") (v "0.2.1") (h "160794b5s7rqiycafkscmx3v2mjq54zzxjm8kshnjvfhinzbflqv")))

(define-public crate-restrust-0.2.2 (c (n "restrust") (v "0.2.2") (h "1b82h8c13syhf4xrgp4fx25n2mcrnr7lx49an54cc06cv9b8hdxd")))

(define-public crate-restrust-0.3.0 (c (n "restrust") (v "0.3.0") (h "0kycy41mnkglikf1svnm9r2zn1iyh911fvlacshjgrd8d6mrc7rf")))

(define-public crate-restrust-0.4.0 (c (n "restrust") (v "0.4.0") (h "0v1dcjj4j68s0d5q2ssljrbpqr9q8l470rbzcqx68dmbifz4mizp")))

(define-public crate-restrust-0.4.1 (c (n "restrust") (v "0.4.1") (h "0zm8gyb3bx7v4w5pnlw0pbdw64rqgh9d55y2gp7jf8nfabmy8ym5")))

(define-public crate-restrust-0.5.0 (c (n "restrust") (v "0.5.0") (h "1lfhyflqfh4wz2hahysnzsn8wy0mkwh9144rj0j52c2mlj7kqg42")))

(define-public crate-restrust-0.5.1 (c (n "restrust") (v "0.5.1") (h "0x16fn61c4x3cyl4nn94v6vgr65bkl519ii0hf1jv59k0cjcn1pn")))

(define-public crate-restrust-0.6.0 (c (n "restrust") (v "0.6.0") (h "1kqik3z3xifk5wbix800bzfpacw34fq9p4nfnq9gqla95kk7nwwz")))

(define-public crate-restrust-0.6.1 (c (n "restrust") (v "0.6.1") (h "13qbl596l1d9xavp70mqc053ivlrppafh91j4bbckcsbwh440vvr")))

(define-public crate-restrust-0.6.2 (c (n "restrust") (v "0.6.2") (h "0ngsa74y7w4xwqrxd3ywq3qk9178f0wbg58rrdy3m1yrwdqzn8xl")))

(define-public crate-restrust-0.6.3 (c (n "restrust") (v "0.6.3") (h "0i904lx82f2h0zwrs2h6ynyf5pzvn0kppr67n9jppd8mmzn32l51")))

(define-public crate-restrust-0.6.4 (c (n "restrust") (v "0.6.4") (h "19d6rdbgc4kq3zz4nshx66n16m4465ax4qxivbm2j31s52lx5wsj")))

(define-public crate-restrust-0.7.0 (c (n "restrust") (v "0.7.0") (h "150nj7rrzqh55cgg90pfpi24mzpp50d4m3q6cd7006sd0278320f")))

