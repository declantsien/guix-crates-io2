(define-module (crates-io re st restless-yew) #:use-module (crates-io))

(define-public crate-restless-yew-0.1.0 (c (n "restless-yew") (v "0.1.0") (d (list (d (n "restless-gloo") (r "^0.1.0") (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)) (d (n "yew-hooks") (r "^0.2.0") (d #t) (k 0)))) (h "1zmj0zdijq85ziqp7l9sm0d057lfva2qykxjmdb5icsq2gwgyjxz")))

