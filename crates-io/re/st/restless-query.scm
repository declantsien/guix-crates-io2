(define-module (crates-io re st restless-query) #:use-module (crates-io))

(define-public crate-restless-query-0.1.0 (c (n "restless-query") (v "0.1.0") (d (list (d (n "restless-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "1f59ll81jfr6nbszsdkcg7d2g3b2fi1racgf9pdvszb2gz3vhm3n") (s 2) (e (quote (("urlencoded" "dep:serde" "dep:serde_urlencoded") ("qs" "dep:serde" "dep:serde_qs"))))))

