(define-module (crates-io re st restricted-text) #:use-module (crates-io))

(define-public crate-restricted-text-0.0.0 (c (n "restricted-text") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "basic-text") (r "^0.0.0") (d #t) (k 0)) (d (n "duplex") (r "^0.1.0") (d #t) (k 0)) (d (n "layered-io") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.2") (d #t) (k 1)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)) (d (n "unsafe-io") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "utf8-io") (r "^0.1.0") (f (quote ("layered-io"))) (d #t) (k 0)))) (h "0fsnz2q1g71p9l65bvin61w5cdjgwvsczynikhlyvj3pvdfkx746")))

