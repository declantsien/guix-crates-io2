(define-module (crates-io re st restart) #:use-module (crates-io))

(define-public crate-restart-0.1.0 (c (n "restart") (v "0.1.0") (d (list (d (n "kill_tree") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (k 0)))) (h "0lgb0mjsknz1nghjd95w0qrp982m6q4y09awbrljvnr3qbsp4rcc")))

