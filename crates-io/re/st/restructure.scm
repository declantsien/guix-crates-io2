(define-module (crates-io re st restructure) #:use-module (crates-io))

(define-public crate-restructure-0.1.0 (c (n "restructure") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "0lvqandha4261pqz5d5g2yvfw9rr8a7vkny9jc6ybsr1k22x7gcn")))

(define-public crate-restructure-0.1.1 (c (n "restructure") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "1mxzyc6fnhgpdrfm6v03aqc105jy7fvhb9bzhaykalibql4izsnk")))

(define-public crate-restructure-0.2.0 (c (n "restructure") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "10khmg6ryhl5iwaic7p8gwa2bskflhw67fzkf2khidsrv97c0xi6")))

