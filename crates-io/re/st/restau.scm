(define-module (crates-io re st restau) #:use-module (crates-io))

(define-public crate-restau-0.1.0 (c (n "restau") (v "0.1.0") (h "0i7kyvh2430p2b35hzxsigf41fnvnsybl4vgw8kymdqs0izjrjsw")))

(define-public crate-restau-0.2.0 (c (n "restau") (v "0.2.0") (h "13lsb9j00m4zdwnfhwgydpqc0jh2wprr47gmq7z1zwhlm0yslzlv")))

