(define-module (crates-io re st rest-client) #:use-module (crates-io))

(define-public crate-rest-client-0.1.0 (c (n "rest-client") (v "0.1.0") (d (list (d (n "rest-client_codegen") (r "^0.1") (d #t) (k 0)))) (h "1khl0sxldsbq6dx495zfrb72s5vhmxlld8135w211y0vq5v6rrar")))

(define-public crate-rest-client-0.1.1 (c (n "rest-client") (v "0.1.1") (d (list (d (n "rest-client_codegen") (r "^0.1") (d #t) (k 0)))) (h "1d3w21rkjs3yks7q5g0vhadci3fj4m76xx91lsyx5pa8vzkjmck3")))

(define-public crate-rest-client-0.2.0 (c (n "rest-client") (v "0.2.0") (d (list (d (n "rest-client_codegen") (r "^0.2") (d #t) (k 0)))) (h "15jxszsn2kp9ccyc280bsgbn7biafr355s2s0hi8r680hy22g89p")))

(define-public crate-rest-client-0.3.0 (c (n "rest-client") (v "0.3.0") (d (list (d (n "rest-client_codegen") (r "^0.3") (d #t) (k 0)))) (h "04c04gmfykkz6hbva1sq3yi7bn61rnrnp7zpamifxvwxha69gacc")))

