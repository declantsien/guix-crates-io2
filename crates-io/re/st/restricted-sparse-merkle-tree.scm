(define-module (crates-io re st restricted-sparse-merkle-tree) #:use-module (crates-io))

(define-public crate-restricted-sparse-merkle-tree-0.4.0-rc2 (c (n "restricted-sparse-merkle-tree") (v "0.4.0-rc2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "blake2b-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nwqiblbjgifghxw82r1jwcbq7skbikl3sn9p73ibjvjvbns6rps") (f (quote (("std") ("default" "std" "blake2b") ("blake2b" "blake2b-rs"))))))

