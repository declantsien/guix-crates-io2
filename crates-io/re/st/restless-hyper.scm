(define-module (crates-io re st restless-hyper) #:use-module (crates-io))

(define-public crate-restless-hyper-0.1.0 (c (n "restless-hyper") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "restless-core") (r "^0.1.0") (f (quote ("http" "bytes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0rhijqkkdb5az4sy5d2njj542jg574ay6jh8fpw0k3k4259f9im8")))

