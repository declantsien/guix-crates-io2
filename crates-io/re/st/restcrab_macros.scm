(define-module (crates-io re st restcrab_macros) #:use-module (crates-io))

(define-public crate-restcrab_macros-0.1.0 (c (n "restcrab_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "014wr3c42wjj67cc779hakkvyj6864gfkcsy9591wns0ggpprsfy")))

(define-public crate-restcrab_macros-0.1.1 (c (n "restcrab_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gzws3z8hyxaf0f90gkfdzbnrnk0flk92297b8i8qz15mdk2mbnp")))

(define-public crate-restcrab_macros-0.1.2 (c (n "restcrab_macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sbbx7rg4cffpa3nhk24bn01nqfhizwdwh2c0lqqhiw0b2wfwhj1")))

(define-public crate-restcrab_macros-0.2.0 (c (n "restcrab_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g8fw46scy51z0zk0dl01sljg4cfisc7fb5s574m0scahpb6jywn")))

