(define-module (crates-io re st restq-http) #:use-module (crates-io))

(define-public crate-restq-http-0.1.0 (c (n "restq-http") (v "0.1.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1ajgvav748rhflgb9w6qsybb7fb09d3d93ns71ii2mgvrwq7gzah")))

(define-public crate-restq-http-0.2.0 (c (n "restq-http") (v "0.2.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1911gmvr3k24iaj55a2clkfkldrgkd5cy2bpbs6gnkvk7d71g2ba")))

(define-public crate-restq-http-0.3.0 (c (n "restq-http") (v "0.3.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0cxawnpb01jpd6vx85f6h35wwdr6ysb6h8l5px35q3arn9fdc0fj")))

(define-public crate-restq-http-0.3.2 (c (n "restq-http") (v "0.3.2") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0dmrrp86l2vm4ad96sa3innbqzj694fsqfijsq4ypf70d2xy445v")))

(define-public crate-restq-http-0.3.3 (c (n "restq-http") (v "0.3.3") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0gqpcaph94vlwbrhlrxk4xir5l59awp91ni4bz2b2cr05508vm5a")))

(define-public crate-restq-http-0.4.0 (c (n "restq-http") (v "0.4.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0jjzgl5ydlbd70m2yh4wwq4c42dknvj1178zi9wlhcavplrhhicw")))

(define-public crate-restq-http-0.5.0 (c (n "restq-http") (v "0.5.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "restq") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1lrv205pwkz5zs6kzz8hzfj5gbx8j3vmq3qlxd4ky304b65kda9s")))

(define-public crate-restq-http-0.6.0 (c (n "restq-http") (v "0.6.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "restq") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0iqfpfmmjmkl3qy2in7pqxjhcav5fw1m03wjlgf5li93bn6v2ch0")))

(define-public crate-restq-http-0.7.0 (c (n "restq-http") (v "0.7.0") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "restq") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ipzcrnbw0blvkk8wnvxk7m52y75wqbfzw4rq6hf1yj720la8v85")))

(define-public crate-restq-http-0.8.0 (c (n "restq-http") (v "0.8.0") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "restq") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q9m6rq55rng4jwlrjj5ixxic8xyxn30sp5q2dmrgamslwbxpchz")))

(define-public crate-restq-http-0.9.0 (c (n "restq-http") (v "0.9.0") (d (list (d (n "http") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "restq") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s5m4j43ksqbdgb9azs26gi45l8g3742dj5akri52wiv73cqqqr8")))

