(define-module (crates-io re st rest_cli) #:use-module (crates-io))

(define-public crate-rest_cli-0.1.0 (c (n "rest_cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "colored_json") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1j848nqyan4252hz7d0jszw991sscnx2m2mxy1f8y6alpf3cmm6f")))

