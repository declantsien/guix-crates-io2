(define-module (crates-io re st restless-wasm-cache) #:use-module (crates-io))

(define-public crate-restless-wasm-cache-0.1.0 (c (n "restless-wasm-cache") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "restless-gloo") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-cache") (r "^0.2.1") (d #t) (k 0)))) (h "11lspmqrzhmspbwwyam2hiqmaraww2maaz9xlwyc1lgcigp849zv")))

