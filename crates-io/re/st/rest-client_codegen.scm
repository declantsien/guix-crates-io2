(define-module (crates-io re st rest-client_codegen) #:use-module (crates-io))

(define-public crate-rest-client_codegen-0.1.0 (c (n "rest-client_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0rkfkyy2y903pwypgpncha8fj4jkq9rl207ww161ris3hzaibbhr")))

(define-public crate-rest-client_codegen-0.1.1 (c (n "rest-client_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "00lj1f8bm7zj6fjs6g10c6v4mm0l3viipxmfi15s2pi9hsbf0krj")))

(define-public crate-rest-client_codegen-0.2.0 (c (n "rest-client_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "10mxz7wyikmffmajnrd6nffwni4fk9s9z0mjnqfvnk32l4nzglpq")))

(define-public crate-rest-client_codegen-0.3.0 (c (n "rest-client_codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "09n350z3b2m8ghhrhf4vlk8ijvsicc37la7y1h6da8qq7d1w0kb0")))

