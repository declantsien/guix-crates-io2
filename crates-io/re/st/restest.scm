(define-module (crates-io re st restest) #:use-module (crates-io))

(define-public crate-restest-0.1.0 (c (n "restest") (v "0.1.0") (d (list (d (n "dep_doc") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "restest_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0sl3c4gp3h7pi14q6lg1373a48438srpnp1117azmq5ppi3df1vy")))

