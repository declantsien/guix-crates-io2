(define-module (crates-io re st rest-api-utils) #:use-module (crates-io))

(define-public crate-rest-api-utils-0.1.0 (c (n "rest-api-utils") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vkwyajli06bm79wbsy70jrk2s3jk4nr0wasxvgs9hpy7jczga2w") (y #t)))

(define-public crate-rest-api-utils-0.1.1 (c (n "rest-api-utils") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1rqy8mh8cr494cnmm262b52fw2pn0l140f9bs210m30dlmsvyad1") (y #t)))

(define-public crate-rest-api-utils-0.1.2 (c (n "rest-api-utils") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pgnixa2vdg24q2rvzbpivr76gmpfradv67b42k5s0ji5n1r0zgs") (y #t)))

(define-public crate-rest-api-utils-0.1.3 (c (n "rest-api-utils") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05mqhk1wkfzznmixkk63affa4fapg8i1i0lxi4zb2n9sxdg4ly91") (y #t)))

(define-public crate-rest-api-utils-0.1.4 (c (n "rest-api-utils") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ivc5vav444ddd97mvyzzc2gdr6kww0kqljbyxjz615852hjn0jq") (y #t)))

(define-public crate-rest-api-utils-0.1.5 (c (n "rest-api-utils") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13cxxw1hpg524wpkqfbmnns4sh0f32rg83b8f68q73f5mbscivhb") (y #t)))

(define-public crate-rest-api-utils-0.1.6 (c (n "rest-api-utils") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yxp3izj7pd85y7g3dp86cnwv08m5r1kcqvwp7dyx8cica7phjwq") (y #t)))

(define-public crate-rest-api-utils-0.1.7 (c (n "rest-api-utils") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hj4c7whiqlkd8034r7xas2d4g9h3d15lmgqb4vmps83xg733874") (y #t)))

(define-public crate-rest-api-utils-0.1.8 (c (n "rest-api-utils") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0499b4nnn9yy5mf3gpvmiwia81pja1bcrid2n5blcka99fjgpm2a") (y #t)))

(define-public crate-rest-api-utils-0.1.9 (c (n "rest-api-utils") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1brf09d95i83rwx09pcjh3mfwqnjmvmy155266hrk309vbxjbxns") (y #t)))

