(define-module (crates-io re st resters) #:use-module (crates-io))

(define-public crate-resters-0.1.0 (c (n "resters") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3.18") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.6") (d #t) (k 0)) (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "13d45xsf2gbbm4z9v1s6dn1wnv805cjql4zpxslywjjnnh5gab8a")))

(define-public crate-resters-0.1.1 (c (n "resters") (v "0.1.1") (d (list (d (n "fltk") (r "^1.3.18") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.6") (d #t) (k 0)) (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "05hb7y6wp4rlhz090nw83icciaj9mv013xx33b5qwj871qfgc0rn")))

