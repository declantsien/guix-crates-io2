(define-module (crates-io re is reis) #:use-module (crates-io))

(define-public crate-reis-0.1.0 (c (n "reis") (v "0.1.0") (d (list (d (n "calloop") (r "^0.10.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "rustix") (r "^0.38.3") (f (quote ("fs" "net"))) (d #t) (k 0)))) (h "0q17rds800q5yfsdrqnaws4bxgabx2rfa4ysgljx46vfb0j60jpy")))

(define-public crate-reis-0.2.0 (c (n "reis") (v "0.2.0") (d (list (d (n "ashpd") (r "^0.8.1") (d #t) (k 2)) (d (n "calloop") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.29") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "rustix") (r "^0.38.3") (f (quote ("event" "fs" "net"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.31.0") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "xkbcommon") (r "^0.7.0") (d #t) (k 2)))) (h "18v7w1bxpyg3l07k85qa9brs7iwsc40r8bwpz1kx1s3jixhf6pb3") (s 2) (e (quote (("tokio" "dep:tokio" "futures") ("calloop" "dep:calloop"))))))

