(define-module (crates-io re is reishi) #:use-module (crates-io))

(define-public crate-reishi-0.0.1 (c (n "reishi") (v "0.0.1") (d (list (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1n2wlcc8lz5cigll8cx1f1vsr2vcqwr6kkmm55kjhlxb6smjmlf9")))

