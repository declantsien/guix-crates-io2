(define-module (crates-io re ho rehost) #:use-module (crates-io))

(define-public crate-rehost-0.2.0 (c (n "rehost") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1cfp6kbqp0nhgg9l32mpwhxqf01jnmbb1slbrd9fs2gzix8nab8b")))

