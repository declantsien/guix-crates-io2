(define-module (crates-io re ik reikna) #:use-module (crates-io))

(define-public crate-reikna-0.5.0 (c (n "reikna") (v "0.5.0") (h "1swwfayb4ywn4d1gsqc0y3hnbssz1srjkx777wcai4a8rxamyc4r")))

(define-public crate-reikna-0.6.0 (c (n "reikna") (v "0.6.0") (h "0cc1vik4asc2pr1ncyhbgbwf7y6pyapp64l0s6bamsy3kbsfj4xa")))

(define-public crate-reikna-0.10.0 (c (n "reikna") (v "0.10.0") (h "1h51mivxxncy24rmy987v22ih6c4zzv18g4sdw12b1fjakb3qi3j")))

(define-public crate-reikna-0.12.3 (c (n "reikna") (v "0.12.3") (h "1jhfk2frdiffclkn557jvasw9qxzbi9ifa0rv1b9v0i5qw92nzsq")))

