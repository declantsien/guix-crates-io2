(define-module (crates-io re nv renvar) #:use-module (crates-io))

(define-public crate-renvar-0.1.0 (c (n "renvar") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)))) (h "13qsa6ff3mgvccilx3swbkbbqf9aw12fpllbpj3hch968a4kszk9") (f (quote (("with_trimmer") ("prefixed") ("postfixed") ("full" "prefixed" "postfixed" "with_trimmer") ("default") ("case_insensitive_prefixed") ("case_insensitive_postfixed"))))))

