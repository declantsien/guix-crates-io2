(define-module (crates-io re nv renvsubst) #:use-module (crates-io))

(define-public crate-renvsubst-0.1.0 (c (n "renvsubst") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rg6q9rah9blnkldi2slh5y4c4nljq13i6frnjca546cldb1yk9l")))

(define-public crate-renvsubst-0.1.1 (c (n "renvsubst") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lip6mk4cvg2d57m9arzr2zdy5qaqv2kq2pqj3f2qa0a0s461yzl")))

(define-public crate-renvsubst-0.1.2 (c (n "renvsubst") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "05phzjgwz56ib37pg32izfgr0m3jiwp24wbx3vcsm4wln4vq5lkv")))

