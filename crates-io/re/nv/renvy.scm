(define-module (crates-io re nv renvy) #:use-module (crates-io))

(define-public crate-renvy-0.1.3 (c (n "renvy") (v "0.1.3") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5.7") (d #t) (k 2)) (d (n "librenvy") (r "~0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "11j67yjvhy897ikxl7jhimw3jbshng7g1fxq7s6vp5mx5q0cz2sr")))

(define-public crate-renvy-0.1.5 (c (n "renvy") (v "0.1.5") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5.7") (d #t) (k 2)) (d (n "librenvy") (r "~0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0x2glh5m799j0qrp70xcxwgnax607p2xs58nm7iq011riq3k0m6b")))

(define-public crate-renvy-0.1.6 (c (n "renvy") (v "0.1.6") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)) (d (n "clap") (r "~3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5.7") (d #t) (k 2)) (d (n "librenvy") (r "~0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "03v62na1hhcwk7qph5s4j5i2vfaybjc4qp3cn1747i8fmzx84rfr")))

