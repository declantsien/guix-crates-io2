(define-module (crates-io re nv renv) #:use-module (crates-io))

(define-public crate-renv-0.1.0 (c (n "renv") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^0.1.1") (d #t) (k 0)))) (h "0zq972m5q58nbx4xa8l1nczqqsg1a7ik5h5w15nj280q4xpwvp7b") (f (quote (("unstable"))))))

(define-public crate-renv-0.2.0 (c (n "renv") (v "0.2.0") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^0.1.1") (d #t) (k 0)))) (h "1n8lxfvlcdbd3sg7i1f0h7ha2zl4752sk9qg2chnmnq4i78dqsqm") (f (quote (("unstable"))))))

(define-public crate-renv-0.2.1 (c (n "renv") (v "0.2.1") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)))) (h "1vsspznppfqsgzxpj4arpa19a2phc8wf39kbslvz6bh7v8j2rx1v") (f (quote (("unstable"))))))

(define-public crate-renv-0.2.2 (c (n "renv") (v "0.2.2") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "java-properties") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "104v9q8jwz7xnbxy08wg7cmzhlya4pd3f868hb0fwsg3h48p2z5r") (f (quote (("unstable"))))))

(define-public crate-renv-0.2.3 (c (n "renv") (v "0.2.3") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "java-properties") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.55") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0c7adc88yk4zl1ahj5s0rkpv01xvb9xgafzqc1yqf7cfxi8x28nr") (f (quote (("unstable"))))))

