(define-module (crates-io re lc relclis) #:use-module (crates-io))

(define-public crate-Relclis-0.1.0 (c (n "Relclis") (v "0.1.0") (h "1j1z5z4ddzh7qdpicvhxikb20608fn2bqlydy0xqfbqhh30dfgfc") (y #t)))

(define-public crate-Relclis-0.0.1 (c (n "Relclis") (v "0.0.1") (h "1zwkjprgjs471xrdzz82sawl8azrpi2jd7kb269dkbwm3f5m4qk3") (y #t)))

(define-public crate-Relclis-0.0.2 (c (n "Relclis") (v "0.0.2") (h "1a9672lp1bdyj704h67qg8mwlvk5wp156xv3p0npgjqb7jmmlxc4") (y #t)))

(define-public crate-Relclis-0.0.3 (c (n "Relclis") (v "0.0.3") (h "09191ca61g7zdv5x0dxh73ll7awh1df7x34sa9nd90wspljzcxbs") (y #t)))

(define-public crate-Relclis-0.1.1 (c (n "Relclis") (v "0.1.1") (h "068h4z2f8bcm1v6v87h2s9si4s1ys02rg8pv4knpqsy0m8xv4asy")))

