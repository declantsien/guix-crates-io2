(define-module (crates-io re pv repvar) #:use-module (crates-io))

(define-public crate-repvar-0.1.0 (c (n "repvar") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09b4hg2944gqyvvl5z3hbp2b5p6hml893a225cnmd2lcskd852lv")))

(define-public crate-repvar-0.1.1-beta (c (n "repvar") (v "0.1.1-beta") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b8s6lhk5s1s996xf7j96q13fnxsd8x3vwqmr6y4x8lcq74wf1ib")))

(define-public crate-repvar-0.1.9 (c (n "repvar") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ffjjn78987wr8qz00xx49lbsn3j1q7qpr6wx9njjj00c47wj01f")))

(define-public crate-repvar-0.1.11 (c (n "repvar") (v "0.1.11") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17ppn2iqccvwk68a3fynpgd6d3832r8p02xw4k4kqzl8rg9k2sb5")))

(define-public crate-repvar-0.1.12 (c (n "repvar") (v "0.1.12") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02bv6sizca9apqk5w7xzwwmq2aly65hla2fzbd899q79bah1kzrs")))

(define-public crate-repvar-0.1.13 (c (n "repvar") (v "0.1.13") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pj2vifnyrh0bbp8q4nrhqbvgj97jr3rs239y73qc953x3xskqp3")))

(define-public crate-repvar-0.1.14 (c (n "repvar") (v "0.1.14") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15z9iqxdbizx6ms1dm2fjsc10ixly6czcyizsshcjayqiabnv9zq") (y #t)))

(define-public crate-repvar-0.1.15 (c (n "repvar") (v "0.1.15") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wyzsp9vw5hdvr6c9ifngcipgzhxrcggqvvms7gdwr51bdwhxp6d") (y #t)))

(define-public crate-repvar-0.1.16 (c (n "repvar") (v "0.1.16") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pj4zyq8b2xzcxbq7637q9gca36174bq28w48p30r11d64d8sac9") (y #t)))

(define-public crate-repvar-0.1.17 (c (n "repvar") (v "0.1.17") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17dvy89fsaap6milryj96g15cwjlmw5hzdk02ipsj4dcnhqb79lx")))

(define-public crate-repvar-0.13.0 (c (n "repvar") (v "0.13.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "cli_utils") (r "^0.6") (d #t) (k 0) (p "cli_utils_hoijui")) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "git-version") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "0b4i1dvndq29fg68djgsrb55im9q52vq6m27zifls4s87lzvzlwf")))

(define-public crate-repvar-0.13.1 (c (n "repvar") (v "0.13.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "cli_utils") (r "^0.7") (d #t) (k 0) (p "cli_utils_hoijui")) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "git-version") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "1g1g2v3giaha7rs1nj7x4i2c1i9w5z1hx7xa3xkyidk51851k4gp")))

