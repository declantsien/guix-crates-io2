(define-module (crates-io r8 li r8lib) #:use-module (crates-io))

(define-public crate-r8lib-0.1.0 (c (n "r8lib") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0byjnlvnq5md7f8zrhyxafvjn3yc4jz6d0gzk6zy23f6d7g59mw4")))

