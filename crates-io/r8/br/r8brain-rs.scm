(define-module (crates-io r8 br r8brain-rs) #:use-module (crates-io))

(define-public crate-r8brain-rs-0.3.2 (c (n "r8brain-rs") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16bqixjwx7dq4lib2nkhgf25q6nlfhfailpi9frbw4bd6j32alvf")))

(define-public crate-r8brain-rs-0.3.3 (c (n "r8brain-rs") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0hq8jibq0i7516wjmykvrggnb2v37lssi12ysfm1132mj82c85bk")))

(define-public crate-r8brain-rs-0.3.4 (c (n "r8brain-rs") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0p07vcrqna27f0rzvds9v3nnrmgkmq551d16z0x84md6ibdhngls")))

(define-public crate-r8brain-rs-0.3.5 (c (n "r8brain-rs") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0i4bm0nwc4hl71yhd73l3hd1pr04yz6z300z0lvma21ma9dakn78")))

