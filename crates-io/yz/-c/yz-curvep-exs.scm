(define-module (crates-io yz -c yz-curvep-exs) #:use-module (crates-io))

(define-public crate-yz-curvep-exs-0.1.0 (c (n "yz-curvep-exs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("parse"))) (o #t) (d #t) (k 0)))) (h "03dm6mkvpj9mk7mwflxj1x7729hqa0q9nj075vyxmi81ikfccags") (f (quote (("cli" "serde" "toml"))))))

