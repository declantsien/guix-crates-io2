(define-module (crates-io yz -d yz-diary-date) #:use-module (crates-io))

(define-public crate-yz-diary-date-0.1.0 (c (n "yz-diary-date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std"))) (k 0)))) (h "0r34qmk29bbl034q0jkq4psi4lzkqy77qw7vr78z2fvcfhjjpqa9")))

(define-public crate-yz-diary-date-0.1.1 (c (n "yz-diary-date") (v "0.1.1") (d (list (d (n "camino") (r "^1.0") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std"))) (k 0)))) (h "1i8q96mx2cb8pn8fpb7p2zbjzamji7mmvn2qbswx9mc78fhycm23")))

