(define-module (crates-io yz -s yz-string-utils) #:use-module (crates-io))

(define-public crate-yz-string-utils-0.1.0 (c (n "yz-string-utils") (v "0.1.0") (h "0mwj04y1f55jyhica6ch8y9i3q8irfp0dc615mlipc3s7dy6v4pj")))

(define-public crate-yz-string-utils-0.1.1 (c (n "yz-string-utils") (v "0.1.1") (h "1y7wcfzfsalxs1swmfswkffjmv4ys0x2ig5481j18cc4g0bxmsnr")))

(define-public crate-yz-string-utils-0.1.2 (c (n "yz-string-utils") (v "0.1.2") (h "1lnbvpy16i8y0mbcmhp7qh3yp69b3s2h4f3kllabnl4z5c4j0k0z")))

(define-public crate-yz-string-utils-0.1.3 (c (n "yz-string-utils") (v "0.1.3") (h "1z5cbcmkz8r8l39jfwrzxa0hd5c7bs9v5d8knc0k6mj67pa6v82z")))

(define-public crate-yz-string-utils-0.2.0 (c (n "yz-string-utils") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0sv5gvr0pdxaqrrxazsj8p12zgn54422krb2jgm68kiqkbmfphi0")))

(define-public crate-yz-string-utils-0.3.0 (c (n "yz-string-utils") (v "0.3.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1ys4j5i8bcv7vr3g76wsqqigm7a1gn0scl8l1lm88xjkphpvmhc7")))

(define-public crate-yz-string-utils-0.3.1 (c (n "yz-string-utils") (v "0.3.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0qhqcifmhkxp4fsrjyd7pvhm8q75bfnm3gg9613r2wawr2p0gpy5")))

(define-public crate-yz-string-utils-0.4.0 (c (n "yz-string-utils") (v "0.4.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1b7y5mm7k6m9fjfwlc2p3bypwb3592wc2wk3cjpfflh151l0jdlm") (f (quote (("consume-ident" "unicode-ident" "unicode-normalization"))))))

