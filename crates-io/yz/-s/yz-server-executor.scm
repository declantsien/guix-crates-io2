(define-module (crates-io yz -s yz-server-executor) #:use-module (crates-io))

(define-public crate-yz-server-executor-0.2.0 (c (n "yz-server-executor") (v "0.2.0") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)))) (h "0bl1ka59afwqbvfjd45h6hlffr1716n3c0269b8njz5qf8n9p6qy")))

(define-public crate-yz-server-executor-0.3.0 (c (n "yz-server-executor") (v "0.3.0") (d (list (d (n "async-executor") (r "^1.8") (d #t) (k 0)) (d (n "event-listener") (r "^4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (o #t) (d #t) (k 0)))) (h "0wxyz1ixl9ksxj4qcs929lfcyjg0ygkdn0f14y2ymdhmvb5s3nmb")))

