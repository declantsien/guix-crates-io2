(define-module (crates-io yz -n yz-nomstr) #:use-module (crates-io))

(define-public crate-yz-nomstr-0.1.0 (c (n "yz-nomstr") (v "0.1.0") (d (list (d (n "nom") (r "^5") (k 0)))) (h "1qzq4zi9aydzqzvb0ra29rgqkckygqvi2nk6yd59blphcl4in9mh")))

(define-public crate-yz-nomstr-0.2.0 (c (n "yz-nomstr") (v "0.2.0") (d (list (d (n "nom") (r "^5") (k 0)))) (h "01xqw83ywzydhmdaai3js211r9ismrphlgrpl5smi24mf6salhrq") (y #t)))

(define-public crate-yz-nomstr-0.2.1 (c (n "yz-nomstr") (v "0.2.1") (d (list (d (n "nom") (r "^5") (k 0)))) (h "0023p631vz2fw0z211iz2hy788bpd9dc4zrxqb2v7jjxz2gd5zd2") (y #t)))

(define-public crate-yz-nomstr-0.3.0 (c (n "yz-nomstr") (v "0.3.0") (d (list (d (n "nom") (r "^5") (k 0)))) (h "053vq17icillqr9fa5jdcb29i5qz73zmmpmhl0xzfbi0xgc1pqky")))

