(define-module (crates-io yz -p yz-posix-mode) #:use-module (crates-io))

(define-public crate-yz-posix-mode-0.0.0 (c (n "yz-posix-mode") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (t "cfg(unix)") (k 2)) (d (n "num-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "umask") (r ">=0.1, <1.1") (o #t) (d #t) (k 0)))) (h "0k4jq30yy5p4337makr7xnw7ni7l4rrmnxf2j2pa8isdmb30a94k") (f (quote (("num" "num-derive" "num-traits"))))))

