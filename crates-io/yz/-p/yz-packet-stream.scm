(define-module (crates-io yz -p yz-packet-stream) #:use-module (crates-io))

(define-public crate-yz-packet-stream-0.1.0 (c (n "yz-packet-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1pf89n7ib626mi4c6drm66yjch7lxpabk7sbmmyridkqlnf6pjji")))

(define-public crate-yz-packet-stream-0.1.1 (c (n "yz-packet-stream") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "103891wwjrsvca6lggwly3x22fygi07ir4ci0l8w1r25hfbp0q71")))

(define-public crate-yz-packet-stream-0.1.2 (c (n "yz-packet-stream") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "poll-buf-utils") (r "^0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0f1b3jmv94yw5n6ppnwbgycf49wxib18k22vf19ipj6yyz3z1vrr")))

(define-public crate-yz-packet-stream-0.1.3-alpha1 (c (n "yz-packet-stream") (v "0.1.3-alpha1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "poll-buf-utils") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10q933kx5q34kd7z75x4a554z2dlkv2y7w3ny6nn2nkxqk8xyzic")))

(define-public crate-yz-packet-stream-0.1.3 (c (n "yz-packet-stream") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "poll-buf-utils") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "04zz22bw9i6x2wbizh6jm4j46j6l84yljms55ap5mgyn73wrphiz")))

(define-public crate-yz-packet-stream-0.2.0 (c (n "yz-packet-stream") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wfbxl5rpd5lx98jb8hynri7ar55162xijbrlmbdrf0s519bidzd")))

(define-public crate-yz-packet-stream-0.2.1 (c (n "yz-packet-stream") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0mng6s96rz6im30b4j0g64inypsmb11rip7k3iag467v12qi9ym2")))

