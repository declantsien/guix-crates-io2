(define-module (crates-io yz b6 yzb64) #:use-module (crates-io))

(define-public crate-yzb64-0.1.0 (c (n "yzb64") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0-alpha.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "1cpmyc78fa1zyix3mn284pf4np4x0cdhqaskwkgscqzxcjibijrg")))

(define-public crate-yzb64-0.1.1 (c (n "yzb64") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0f5yywq0pn117imzw79ijg43hw7sd7k6308fl5qdafshqkfcxs13")))

