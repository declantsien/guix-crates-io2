(define-module (crates-io m_ le m_lexer) #:use-module (crates-io))

(define-public crate-m_lexer-0.0.1 (c (n "m_lexer") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19fd9mc4qa81prh6g7mpas8n6bq94k8mpm4ysnqgjhj01dvrhfnb")))

(define-public crate-m_lexer-0.0.2 (c (n "m_lexer") (v "0.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1j579izdmyv4yxhyvh6w8pzfjsmg79bq88d2dyj5zm97yk05k4cd")))

(define-public crate-m_lexer-0.0.3 (c (n "m_lexer") (v "0.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wz9saa4wdv4q2vq23g2ab2jbs48a5wjhzb23w6qczjpqnyrvys9")))

(define-public crate-m_lexer-0.0.4 (c (n "m_lexer") (v "0.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19v7hk4i3avgvmhhv26bf5hjfjpwkrvy81dfbdd5hb8nj6zixrd7")))

