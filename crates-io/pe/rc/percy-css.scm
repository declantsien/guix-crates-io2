(define-module (crates-io pe rc percy-css) #:use-module (crates-io))

(define-public crate-percy-css-0.1.0 (c (n "percy-css") (v "0.1.0") (h "1kyim64fmp3jn2vrxaysywr7jmh7fm0b3w3n60lqij18g333v370")))

(define-public crate-percy-css-0.1.1 (c (n "percy-css") (v "0.1.1") (d (list (d (n "percy-css-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1giimbypyfhc5bh6km3hd30nwrk2ys9znksyr4lvskbicbpx7vdl")))

