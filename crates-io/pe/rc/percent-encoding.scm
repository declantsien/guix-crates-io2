(define-module (crates-io pe rc percent-encoding) #:use-module (crates-io))

(define-public crate-percent-encoding-1.0.0 (c (n "percent-encoding") (v "1.0.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)))) (h "0mp3d9md35vsnk0gznvvngk68gck923p7d6r87g6nw47h5ily5fy")))

(define-public crate-percent-encoding-1.0.1 (c (n "percent-encoding") (v "1.0.1") (h "0cgq08v1fvr6bs5fvy390cz830lq4fak8havdasdacxcw790s09i")))

(define-public crate-percent-encoding-2.0.0 (c (n "percent-encoding") (v "2.0.0") (h "0m6rkp3iy11la04p6z3492rns6n693pvmx585dvfmzzlzak2hkxs")))

(define-public crate-percent-encoding-2.1.0 (c (n "percent-encoding") (v "2.1.0") (h "0bp3zrsk3kr47fbpipyczidbbx4g54lzxdm77ni1i3qws10mdzfl")))

(define-public crate-percent-encoding-2.2.0 (c (n "percent-encoding") (v "2.2.0") (h "13nrpp6r1f4k14viksga3094krcrxgv4b42kqbriy63k7ln5g327") (f (quote (("default" "alloc") ("alloc")))) (r "1.51")))

(define-public crate-percent-encoding-2.3.0 (c (n "percent-encoding") (v "2.3.0") (h "152slflmparkh27hprw62sph8rv77wckzhwl2dhqk6bf563lfalv") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.51")))

(define-public crate-percent-encoding-2.3.1 (c (n "percent-encoding") (v "2.3.1") (h "0gi8wgx0dcy8rnv1kywdv98lwcx67hz0a0zwpib5v2i08r88y573") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.51")))

