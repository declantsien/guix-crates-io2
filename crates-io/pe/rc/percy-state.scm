(define-module (crates-io pe rc percy-state) #:use-module (crates-io))

(define-public crate-percy-state-0.1.0 (c (n "percy-state") (v "0.1.0") (h "11ws132dbcyw66h7ym6gks6nk7dx72prdlnqb2hf3q1snyknfmr9")))

(define-public crate-percy-state-0.1.1 (c (n "percy-state") (v "0.1.1") (h "0pjpqyv47c6b5k4qxqjrzdrk8bc5i22na5ah27qk9bnad2xgsilx")))

(define-public crate-percy-state-0.1.2 (c (n "percy-state") (v "0.1.2") (h "12yqacsvnffiqmgkm8z8hp1ckyfzyzz83qqiyzahil3hpc8g2zci")))

