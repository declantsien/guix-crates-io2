(define-module (crates-io pe rc percent-encoding-rfc3986) #:use-module (crates-io))

(define-public crate-percent-encoding-rfc3986-0.1.0 (c (n "percent-encoding-rfc3986") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1a26xwqqp3f6ykhqi6m7dsi421bhk8azjfgl2z3d434zf4pj89w7") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-percent-encoding-rfc3986-0.1.1 (c (n "percent-encoding-rfc3986") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "07nirii7sq64s4ddaliz04ixfwz80cqbwr8k5gmhx865y40anvyf") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-percent-encoding-rfc3986-0.1.2 (c (n "percent-encoding-rfc3986") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0q8c6w9amas4z6xy5l2ygbgwy39bs1c8z0k81znmp8mmfhkyyi3w") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-percent-encoding-rfc3986-0.1.3 (c (n "percent-encoding-rfc3986") (v "0.1.3") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0rhi36mn6pylfaa3syzhzq775nl7d1ddrsb4i9b2g08nfxaw0drn") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

