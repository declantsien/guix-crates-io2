(define-module (crates-io pe rc percy-router) #:use-module (crates-io))

(define-public crate-percy-router-0.2.1 (c (n "percy-router") (v "0.2.1") (d (list (d (n "percy-router-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "percy-vdom") (r "^0.6.0") (d #t) (k 0)))) (h "12d66345dzz6li5ajkpy09731djf885l9lyay8i2v2ifsj3mxpcq")))

(define-public crate-percy-router-0.2.2 (c (n "percy-router") (v "0.2.2") (d (list (d (n "percy-dom") (r "^0.6.0") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.1") (d #t) (k 0)))) (h "02pawswvsiykm3djm36r5fadq0nnvvrz029f72bp5phq2m93p96v")))

(define-public crate-percy-router-0.2.3 (c (n "percy-router") (v "0.2.3") (d (list (d (n "percy-dom") (r "^0.6.17") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1rx0swxq6f2ghqsrfk3d1vkqa2m9zcmivk7b47vf9nmrx7wha1iz")))

(define-public crate-percy-router-0.2.4 (c (n "percy-router") (v "0.2.4") (d (list (d (n "percy-dom") (r "^0.6.17") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.3") (d #t) (k 0)))) (h "0ygn4qa3rdqj17df0cb6sdsfzf0hrnf7d167xmp8j6kqqfijsag9")))

(define-public crate-percy-router-0.2.6 (c (n "percy-router") (v "0.2.6") (d (list (d (n "percy-dom") (r "^0.6.28") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0kakxhya5s5d0h309mszr2m428ffrxrxnyrlw9y0ckl2x2ly5l3h")))

(define-public crate-percy-router-0.3.0 (c (n "percy-router") (v "0.3.0") (d (list (d (n "percy-dom") (r "^0.7.0") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0q4nxcs565p771gmlhpg4gfqr1165s6gy5vrx9caqk41ny06hm53")))

(define-public crate-percy-router-0.3.1 (c (n "percy-router") (v "0.3.1") (d (list (d (n "percy-dom") (r "^0.7.1") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "066q9msby02ggpd2m0yqwav9mqbwh50ssv8qglfq5cmg234372bz")))

(define-public crate-percy-router-0.3.2 (c (n "percy-router") (v "0.3.2") (d (list (d (n "percy-dom") (r "^0.7.2") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "04ffsfjmnbyy4sp4ris28aracxlcv1f5adqrp3sqwgabmv28bdqx")))

(define-public crate-percy-router-0.3.3 (c (n "percy-router") (v "0.3.3") (d (list (d (n "percy-dom") (r "^0.7.3") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "058y9bqpl111m4r6d5421dbihn76xzjsivqff0lgw1d4fldfamwv")))

(define-public crate-percy-router-0.3.4 (c (n "percy-router") (v "0.3.4") (d (list (d (n "percy-dom") (r "^0.7.3") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "05jllkccczmkjdvbvwqb60wr9qr8a8h14c55b295mrlbslbg5x7w")))

(define-public crate-percy-router-0.3.5 (c (n "percy-router") (v "0.3.5") (d (list (d (n "percy-dom") (r "^0.7.5") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1n1lp29r179yz8s7g366p36diz8g6v1gmghi5n0cd9yhg83wfhgx")))

(define-public crate-percy-router-0.3.6 (c (n "percy-router") (v "0.3.6") (d (list (d (n "percy-dom") (r "^0.7.5") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "06j5rv0n3jyasi9s97imva6yrfsw0f7y08i1p0p3p3pv5jpnmb1h")))

(define-public crate-percy-router-0.3.7 (c (n "percy-router") (v "0.3.7") (d (list (d (n "percy-dom") (r "^0.7.5") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1qwm720a4mb21qd620a2qcrhqql7gqx8fgvccvgc5n2pyf6dmg7a")))

(define-public crate-percy-router-0.4.0 (c (n "percy-router") (v "0.4.0") (d (list (d (n "percy-dom") (r "^0.8.0") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "04v5rqr9gwv12xy2agg8bpn0z806cc1kr5yv1zy57g03b1nrvadi")))

(define-public crate-percy-router-0.5.0 (c (n "percy-router") (v "0.5.0") (d (list (d (n "percy-dom") (r "^0.9") (d #t) (k 0)) (d (n "percy-router-macro") (r "^0.1.5") (d #t) (k 0)))) (h "14mi8w3nir36jmlkhq3mjv7vwdiyd0nh91xmjf2vax31189j966k")))

