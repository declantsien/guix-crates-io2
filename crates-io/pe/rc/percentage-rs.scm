(define-module (crates-io pe rc percentage-rs) #:use-module (crates-io))

(define-public crate-percentage-rs-0.1.0 (c (n "percentage-rs") (v "0.1.0") (h "1a3gxnk9g51ill9rw3h0nzmkis3ald04907mn9wkcqdlv1c1mdsk")))

(define-public crate-percentage-rs-0.1.1 (c (n "percentage-rs") (v "0.1.1") (h "1bwjif300d23zj0plalv944ys593z9v8mi3k2p5im4r6nicwbxx4")))

(define-public crate-percentage-rs-0.1.2 (c (n "percentage-rs") (v "0.1.2") (h "1kb707p2ckg5cf78anhfmxx7hdc69xrh8l8a7b2phgyldqqx2lkw")))

(define-public crate-percentage-rs-0.1.3 (c (n "percentage-rs") (v "0.1.3") (h "0dbfcgf2wl20dwf1f99i8sk9wvh6362i2drdkgi3bng79bd8lrsb")))

(define-public crate-percentage-rs-0.1.4 (c (n "percentage-rs") (v "0.1.4") (h "0njhl9l835xa0w7jbmj0y3qvjx2r3yjkdlcwahdniagskv9av931")))

(define-public crate-percentage-rs-0.1.5 (c (n "percentage-rs") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0623ia3vryx58wmsplyzy83wsgrc6f9wfh41n8vkvd1xp26iflib")))

(define-public crate-percentage-rs-0.1.6 (c (n "percentage-rs") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a66v84i5nqag341dpdprq1g5rq4nwxhw6s9n8wz67qxvmdvhrkb") (f (quote (("serde_support" "serde"))))))

