(define-module (crates-io pe rc percy-css-macro) #:use-module (crates-io))

(define-public crate-percy-css-macro-0.1.0 (c (n "percy-css-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1lzgsvcgc41hv3d1m2i7xj7gjs5yvmbkvifds9l3hc0ccfqx16mg")))

(define-public crate-percy-css-macro-0.1.1 (c (n "percy-css-macro") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikkczigldq5g6bcx6868knhq2395k61qppnb1xma7sqgnpbsyrf")))

