(define-module (crates-io pe rc perchance) #:use-module (crates-io))

(define-public crate-perchance-0.1.1 (c (n "perchance") (v "0.1.1") (d (list (d (n "macaw") (r "^0.16.0") (f (quote ("with_serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "=2.2") (d #t) (k 0)))) (h "08aa7djg2bhiifljg8dngq3pl3lmidfdds9wy9w7isdhjkqjhy8v") (r "1.56")))

(define-public crate-perchance-0.2.0 (c (n "perchance") (v "0.2.0") (d (list (d (n "macaw") (r "^0.16.0") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "=2.2") (o #t) (d #t) (k 0)))) (h "1wcgdgiydxza2l31qinigznk86lb5mb4bzd4rq9md7vixfy9p7ba") (f (quote (("std" "macaw/std" "once_cell" "ordered-float") ("libm" "macaw/libm") ("default" "std")))) (r "1.56")))

(define-public crate-perchance-0.2.1 (c (n "perchance") (v "0.2.1") (d (list (d (n "macaw") (r "^0.16.0") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "=2.2") (o #t) (d #t) (k 0)))) (h "1qsrds4dp1nm2am4vzbgp82np68wwlvpbsiskz4imxiigbwrxjjd") (f (quote (("std" "macaw/std" "once_cell" "ordered-float") ("libm" "macaw/libm") ("default" "std")))) (r "1.56")))

(define-public crate-perchance-0.2.2 (c (n "perchance") (v "0.2.2") (d (list (d (n "macaw") (r "^0.16.0") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "=2.2") (o #t) (d #t) (k 0)))) (h "19zccd6znjm6njfgb2ijcmjaw0x63ryip6ii08shpnih3xxwx6h8") (f (quote (("std" "macaw/std" "once_cell" "ordered-float") ("libm" "macaw/libm") ("default" "std")))) (r "1.56")))

(define-public crate-perchance-0.3.0 (c (n "perchance") (v "0.3.0") (d (list (d (n "macaw") (r "^0.17.0") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "=2.2") (o #t) (d #t) (k 0)))) (h "0crxh50y489fn2ph5v163v1a7x3yk45nj05siv2zh8divxws182z") (f (quote (("std" "macaw/std" "once_cell" "ordered-float") ("libm" "macaw/libm") ("default" "std")))) (r "1.56")))

(define-public crate-perchance-0.3.1 (c (n "perchance") (v "0.3.1") (d (list (d (n "macaw") (r "^0.17.0") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (o #t) (d #t) (k 0)))) (h "0fs4ifrk27rn5g0bq9zlc6b9vngl4r47w2bjsxkby9ivgnqnhqjv") (f (quote (("std" "macaw/std" "once_cell" "ordered-float") ("libm" "macaw/libm") ("default" "std")))) (r "1.56")))

(define-public crate-perchance-0.3.2 (c (n "perchance") (v "0.3.2") (d (list (d (n "macaw") (r "^0.17.0") (o #t) (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (o #t) (d #t) (k 0)))) (h "01jdhcb9jkp0pkk78ghziysyz6kiqrgrc8w76cc1x3yf7i4vlpbs") (f (quote (("default" "std" "macaw")))) (s 2) (e (quote (("std" "macaw?/std" "once_cell" "ordered-float") ("libm" "macaw?/libm")))) (r "1.64")))

(define-public crate-perchance-0.4.0 (c (n "perchance") (v "0.4.0") (d (list (d (n "macaw") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)))) (h "1sdcpgjp4vcyjlaxjd9x83ycdbk4gab5c2rj3n8qsp6gjfda8bi6") (f (quote (("default" "macaw")))) (r "1.65.0")))

(define-public crate-perchance-0.5.0 (c (n "perchance") (v "0.5.0") (d (list (d (n "macaw") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)))) (h "098l9f07smrh0dgnkmqsq1n81z9ski280z1nmnl9hnnyfchhmnsy") (f (quote (("default" "macaw")))) (r "1.66.0")))

(define-public crate-perchance-0.5.1 (c (n "perchance") (v "0.5.1") (d (list (d (n "macaw") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)))) (h "030w465a3iwfrw98xxq8i73n9j079vqh32b4l9aiz5lylqm92l3i") (f (quote (("default" "macaw")))) (r "1.71.1")))

(define-public crate-perchance-0.5.2 (c (n "perchance") (v "0.5.2") (d (list (d (n "macaw") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.0") (d #t) (k 0)))) (h "1nd1nm6wrlwgvdmlkrl6cjsvqgplz32zsc0m5ixm6pmbp3kpzjnk") (f (quote (("default" "macaw")))) (r "1.75.0")))

(define-public crate-perchance-0.5.3 (c (n "perchance") (v "0.5.3") (d (list (d (n "macaw") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.0") (d #t) (k 0)))) (h "167xnv9i3bm9lx4nbq19kbbpvr3aiavxgdw4jybnapaa7dd7m55z") (f (quote (("default" "macaw")))) (r "1.76.0")))

