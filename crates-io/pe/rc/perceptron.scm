(define-module (crates-io pe rc perceptron) #:use-module (crates-io))

(define-public crate-perceptron-0.1.0 (c (n "perceptron") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zlh0cqjg96zayiggwyn6smw4s4an7wmc11dm596xlyip1xyh3kz")))

(define-public crate-perceptron-0.1.1 (c (n "perceptron") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12665wh5ngxggkq0snrwv894hvk6s47b97sf8cn2vv90ni4ifd5b")))

