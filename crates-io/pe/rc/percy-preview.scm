(define-module (crates-io pe rc percy-preview) #:use-module (crates-io))

(define-public crate-percy-preview-0.0.1 (c (n "percy-preview") (v "0.0.1") (d (list (d (n "virtual-node") (r "^0.3.1") (d #t) (k 0)))) (h "1pscjhr2md5v17arrh3syhbiqbi7380n9l5gfwpcrqjy9cx8brj5")))

(define-public crate-percy-preview-0.0.2 (c (n "percy-preview") (v "0.0.2") (d (list (d (n "virtual-node") (r "^0.3.1") (d #t) (k 0)))) (h "0dg2n49p91ciqx04vr0q3jj6v7bks0fq9vi3h7myfrx7ma8d8scb")))

(define-public crate-percy-preview-0.0.3 (c (n "percy-preview") (v "0.0.3") (d (list (d (n "virtual-node") (r "^0.3.1") (d #t) (k 0)))) (h "1shyccah7chz20jljsnysdvhkms6sk1ly17r95p12xirxnm8k1x9")))

