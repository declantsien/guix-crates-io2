(define-module (crates-io pe rc percy-webapis) #:use-module (crates-io))

(define-public crate-percy-webapis-0.0.1 (c (n "percy-webapis") (v "0.0.1") (d (list (d (n "wasm-bindgen") (r "^0.2.14") (d #t) (k 0)))) (h "0hqdzqyqhpsgc35riyrmv237jirdq86vmhhyyh2088474dfmg0rg")))

(define-public crate-percy-webapis-0.0.2 (c (n "percy-webapis") (v "0.0.2") (d (list (d (n "wasm-bindgen") (r "^0.2.21") (d #t) (k 0)))) (h "1nx0ghdriq58j3sr0ch1rrkqdqp6drj89s0df0njh7g0iryp7fk7")))

