(define-module (crates-io pe tr petra_grid) #:use-module (crates-io))

(define-public crate-petra_grid-1.0.0 (c (n "petra_grid") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0l3m6h8k5vk42pysbsra7s71z2n5sbw06pklkwar4msaym514qsq")))

