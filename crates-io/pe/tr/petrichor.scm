(define-module (crates-io pe tr petrichor) #:use-module (crates-io))

(define-public crate-petrichor-0.1.0 (c (n "petrichor") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("msgpack"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05dn0sm2nqgkjz9ms7sprkbhc48zij2qgc2gln9zxfxk9jfz76ng")))

(define-public crate-petrichor-0.1.1 (c (n "petrichor") (v "0.1.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("msgpack"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nsdzds66sgw9x59qq2xicms3n73j59qaqki1fv72vxc7rlw0qlk")))

