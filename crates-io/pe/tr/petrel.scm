(define-module (crates-io pe tr petrel) #:use-module (crates-io))

(define-public crate-petrel-0.0.1 (c (n "petrel") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diameter") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2qll1gm5qz64dphkx1gblp0v5h6al291si5yj07xnfdfxpmng3")))

