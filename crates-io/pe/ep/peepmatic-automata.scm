(define-module (crates-io pe ep peepmatic-automata) #:use-module (crates-io))

(define-public crate-peepmatic-automata-0.1.0 (c (n "peepmatic-automata") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "1ckn02kbfy49hhfjrp698xg39ngba4j11r8nsdw8f23rbwlnk5yn") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.2.0 (c (n "peepmatic-automata") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "02d9qp02abwd54xilk5gpcs1jv8gciq7dwlmjhm8vamjvbxld3k7") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.67.0 (c (n "peepmatic-automata") (v "0.67.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0blp84rjc7y5ydbj242g4kbkaf5ddrwivv17jbrmxjbdig43wz02") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.68.0 (c (n "peepmatic-automata") (v "0.68.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "07717zsk5k0wy09vm73m9figl1ifbp69q30gmw8112a6cxfxhj3j") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.69.0 (c (n "peepmatic-automata") (v "0.69.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0wr3gxjyfifpqv9l9h3a637lhj0c37hq91vgv7qiw8wks19qqvpw") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.70.0 (c (n "peepmatic-automata") (v "0.70.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0xpmgywyzkp3a3qz67dzrsckj74cki4wh4m47b1k5nv0csl1fg8x") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.71.0 (c (n "peepmatic-automata") (v "0.71.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0lrfh9jpm6hxcw809gs8pf47d83qiqybp3xnk04aydnwsrg5k4ra") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.72.0 (c (n "peepmatic-automata") (v "0.72.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0fqrk87qki1njbifs3l3abcmp889p7dmpzzcdzi41yl7krb4ny78") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.73.0 (c (n "peepmatic-automata") (v "0.73.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "14ga657bqhshf8nf6l2qh2irxm8ccp0kmk2g7sy3i412xi714ip4") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.74.0 (c (n "peepmatic-automata") (v "0.74.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0yhlcnsp4cq1yp0chqym72v2sxcy5zzpkzx4kdnzh1j8v7vhnjv0") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.73.1 (c (n "peepmatic-automata") (v "0.73.1") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0fc0szxhwr47dsvc6qy4j7xdmsix5dalm97zzxwch33rl1js44zv") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.75.0 (c (n "peepmatic-automata") (v "0.75.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "02a172ag9rax5cnb30m2gdai7v6n5ksfm0av3nshnx3vsig9g38c") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.76.0 (c (n "peepmatic-automata") (v "0.76.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0lh3rshpzvlfkh236cxm34r40h40w01j5vb78nmq8z2l7k20mva1") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.77.0 (c (n "peepmatic-automata") (v "0.77.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "1xghh93ny8inlc89dhqg6jm593q5inm20jqzyd7rvwfv586h7qrm") (f (quote (("dot"))))))

(define-public crate-peepmatic-automata-0.78.0 (c (n "peepmatic-automata") (v "0.78.0") (d (list (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)))) (h "0jzr0h5d02mlzx8xami1m82p5nj2ylsb58289nxzyxpyzzb43fmn") (f (quote (("dot"))))))

