(define-module (crates-io pe ep peepable) #:use-module (crates-io))

(define-public crate-peepable-0.1.0 (c (n "peepable") (v "0.1.0") (h "0v86rla94ys7hwbilb4srvw7r3ir5shp7bfwy27j0909brhbmpya")))

(define-public crate-peepable-0.1.1 (c (n "peepable") (v "0.1.1") (h "0wj5gn81q4n42p6n4f3m4kmsk786463f1in65f3spzqj0yhvnbs5")))

