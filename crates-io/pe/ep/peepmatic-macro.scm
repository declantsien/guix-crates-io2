(define-module (crates-io pe ep peepmatic-macro) #:use-module (crates-io))

(define-public crate-peepmatic-macro-0.1.0 (c (n "peepmatic-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dqf4sshj8d8ald8iw5hn3s6dw9l7l3xy7gzicas7xy8igwmq9jy")))

(define-public crate-peepmatic-macro-0.2.0 (c (n "peepmatic-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mxxd8f9fnp141k850rgq5vcrnrhxqg44y5gassw7g1aiyyfskm4")))

(define-public crate-peepmatic-macro-0.67.0 (c (n "peepmatic-macro") (v "0.67.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "154mfxdl8y2kkzjb3zy9m7lyzlmwpdmgi8i0rla1r8n5snsw1iaw")))

(define-public crate-peepmatic-macro-0.68.0 (c (n "peepmatic-macro") (v "0.68.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "026ijr0c64cv92xjncnk3lxvma56vrpfjqk6s98rikq62fjq8jcq")))

(define-public crate-peepmatic-macro-0.69.0 (c (n "peepmatic-macro") (v "0.69.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15x4z08asfgiqjpvh3pgr02mp6knzbjvnl4gxghf0k6pkmfarcwh")))

(define-public crate-peepmatic-macro-0.70.0 (c (n "peepmatic-macro") (v "0.70.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0an3y9fhwbd3cgzpckkdrf4m8y5cr5kk8f2v9dam1sbs52m7af5c")))

(define-public crate-peepmatic-macro-0.71.0 (c (n "peepmatic-macro") (v "0.71.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0npkp0xjspxvrfxbwq66cdyqq1ffb5g4lp10a1l9122r9nq071c1")))

(define-public crate-peepmatic-macro-0.72.0 (c (n "peepmatic-macro") (v "0.72.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m65a2dyfqnx7ggyhk94mgig7hnsyvmw3mnfyfxs29hqp4wkmkgq")))

(define-public crate-peepmatic-macro-0.73.0 (c (n "peepmatic-macro") (v "0.73.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14ddxyhkihz59icbs4qk57pg7lb6ycj49xg7bn0afma0jvgp53mk")))

(define-public crate-peepmatic-macro-0.74.0 (c (n "peepmatic-macro") (v "0.74.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19vf9ya32mm42ja9r04184nk2c0wrk734yjmjqljrh3x9ad0j0h9")))

(define-public crate-peepmatic-macro-0.73.1 (c (n "peepmatic-macro") (v "0.73.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1022gh79sqhkr1523wzz8zch9bdspjyp92qsrayl4rr1bcg4ibww")))

(define-public crate-peepmatic-macro-0.75.0 (c (n "peepmatic-macro") (v "0.75.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qy2149mjambl3pzw2amfjirsf5c007b0mzg8vwl4mbb30pdrq10")))

(define-public crate-peepmatic-macro-0.76.0 (c (n "peepmatic-macro") (v "0.76.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vcc8lvabjai1ckx1f236k2jl9acl017f6vb1qkhy58rd9hjcgq3")))

(define-public crate-peepmatic-macro-0.77.0 (c (n "peepmatic-macro") (v "0.77.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02nh7xkb1xmpx9vfg6q3x9q4m54bxhssj8farnxjwfm4kgmvkx1i")))

(define-public crate-peepmatic-macro-0.78.0 (c (n "peepmatic-macro") (v "0.78.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "159l9pr636pkp1hwz7jzxdwr9dz3asf74msc3xiqx8cl7m2ihrn2")))

