(define-module (crates-io pe ep peepmatic-test-operator) #:use-module (crates-io))

(define-public crate-peepmatic-test-operator-0.69.0 (c (n "peepmatic-test-operator") (v "0.69.0") (d (list (d (n "peepmatic-traits") (r "^0.69.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^29.0.0") (d #t) (k 0)))) (h "1bj5zzbd2vg344mqm28f2v09s8fjbzy1mq626f7girz614kvwr7f")))

(define-public crate-peepmatic-test-operator-0.70.0 (c (n "peepmatic-test-operator") (v "0.70.0") (d (list (d (n "peepmatic-traits") (r "^0.70.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^32.0.0") (d #t) (k 0)))) (h "104npc0v9aq5pl8d048jqj5rlbw21xdcysjsiih484lxxn7vj4r5")))

(define-public crate-peepmatic-test-operator-0.71.0 (c (n "peepmatic-test-operator") (v "0.71.0") (d (list (d (n "peepmatic-traits") (r "^0.71.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "0jdkyirjimfm9l8k1xvv3nspbih096l5zgnk5i4x6gfn7gd5n175")))

(define-public crate-peepmatic-test-operator-0.72.0 (c (n "peepmatic-test-operator") (v "0.72.0") (d (list (d (n "peepmatic-traits") (r "^0.72.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "01xnlcq7f4x0ddy9xxv4lcszj6jqc81scm7hawa09d4w4bzqmzxh")))

(define-public crate-peepmatic-test-operator-0.73.0 (c (n "peepmatic-test-operator") (v "0.73.0") (d (list (d (n "peepmatic-traits") (r "^0.73.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "1rs0i1x50fg32w6gyc0xqd72smsaidxws8k9hbb8aa3kscdahf6q")))

(define-public crate-peepmatic-test-operator-0.74.0 (c (n "peepmatic-test-operator") (v "0.74.0") (d (list (d (n "peepmatic-traits") (r "^0.74.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "03hr6q81qr81qgjqlissvj0jvsm1db3izfmqg09vhmbrr6m17zzn")))

(define-public crate-peepmatic-test-operator-0.73.1 (c (n "peepmatic-test-operator") (v "0.73.1") (d (list (d (n "peepmatic-traits") (r "^0.73.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "0qdp1zn13yswmzp082bcm3gj42f6djfyr73z3ys9xc676vj87d68")))

(define-public crate-peepmatic-test-operator-0.75.0 (c (n "peepmatic-test-operator") (v "0.75.0") (d (list (d (n "peepmatic-traits") (r "^0.75.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "0sq0avpbkhpfh41xswi11qg3b9k1khgki7w3x3mf3ak8m87w9q0j")))

(define-public crate-peepmatic-test-operator-0.76.0 (c (n "peepmatic-test-operator") (v "0.76.0") (d (list (d (n "peepmatic-traits") (r "^0.76.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^36.0.0") (d #t) (k 0)))) (h "14mxmkarn97h5m6rkf5hpmqzrq65mzvzvczzy6ikn1hkd08asyjg")))

(define-public crate-peepmatic-test-operator-0.77.0 (c (n "peepmatic-test-operator") (v "0.77.0") (d (list (d (n "peepmatic-traits") (r "^0.77.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^37.0.0") (d #t) (k 0)))) (h "1j1p79ll46aq7qq7baiy6n6b2dl7lcka3ajaqvw90b9x7g97vfvh")))

(define-public crate-peepmatic-test-operator-0.78.0 (c (n "peepmatic-test-operator") (v "0.78.0") (d (list (d (n "peepmatic-traits") (r "=0.78.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "1vq6s2zxj1vslxrf1vss71dmx2mqwmnkh727ayz24bn8jm1rl5x2")))

