(define-module (crates-io pe ct pectin) #:use-module (crates-io))

(define-public crate-pectin-0.0.2 (c (n "pectin") (v "0.0.2") (d (list (d (n "git-testament") (r "^0.2.5") (d #t) (k 1)) (d (n "quake-util") (r "^0.3.2") (d #t) (k 0)) (d (n "tcl") (r "^0.1.9") (d #t) (k 0)))) (h "1j27cqwbz4bf7nknc2aqvppyhc3a97xcvsiaaakggxpjzxl3psz3")))

(define-public crate-pectin-0.0.3 (c (n "pectin") (v "0.0.3") (d (list (d (n "git-testament") (r "^0.2.5") (d #t) (k 1)) (d (n "quake-util") (r "^0.3.2") (d #t) (k 0)) (d (n "tcl") (r "^0.1.9") (d #t) (k 0)))) (h "044pja5sifhdrxdfbclb9wygrskdsdksxf9r3qfgzrir8n24yx2r")))

