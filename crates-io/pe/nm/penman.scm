(define-module (crates-io pe nm penman) #:use-module (crates-io))

(define-public crate-penman-0.1.0 (c (n "penman") (v "0.1.0") (d (list (d (n "api") (r "^0.1.0") (d #t) (k 0) (p "ledgers-api")) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2.1") (d #t) (k 0)))) (h "0fm2a775mmla5v8lp0kwzkm59qvcn02d0x0c42jaja5vyf7c7wky")))

(define-public crate-penman-0.1.1 (c (n "penman") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "ledgers") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2.1") (d #t) (k 0)))) (h "1c9ciz367x9d8x3rs8j3nrpn8if6i8b2jawzck3jywcyfm69f9a2")))

