(define-module (crates-io pe m- pem-rfc7468) #:use-module (crates-io))

(define-public crate-pem-rfc7468-0.0.0 (c (n "pem-rfc7468") (v "0.0.0") (h "0bqaxacrrldxy05nmfpx4g1vg23091rc77hkai9p15kqvqayh47g") (y #t)))

(define-public crate-pem-rfc7468-0.1.0 (c (n "pem-rfc7468") (v "0.1.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "0q90pz0vb3vcl9dfzy5j0j0s4gs106yrkxg2vfrppfp8dw58wdan") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-pem-rfc7468-0.1.1 (c (n "pem-rfc7468") (v "0.1.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "0ns0i0arlcpkh3ygsgx9xxrfy2jwkd85gr4xi9rxs7x2p0dgh7l0") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2.0 (c (n "pem-rfc7468") (v "0.2.0") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "04vc2kxzy9wvq7nlhivrw6qbljyjvi2il6m4clk485wsik3r1zmq") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2.1 (c (n "pem-rfc7468") (v "0.2.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "1mdgdknswfqkp8nh7006wilzm95whj9racianjs79q2kf9ay1a1c") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2.2 (c (n "pem-rfc7468") (v "0.2.2") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "0r82g7gy2wp8j8zm19bdm01xdsml1x0xkrmd5bap2lm107ab47z7") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2.3 (c (n "pem-rfc7468") (v "0.2.3") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "15yljsciaz07wmgl391s34zx8lmpyr6b5x4zkslr8cjr7h7fn8lg") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2.4 (c (n "pem-rfc7468") (v "0.2.4") (d (list (d (n "base64ct") (r ">=1, <1.2") (d #t) (k 0)))) (h "1m1c9jypydzabg4yscplmvff7pdcc8gg4cqg081hnlf03hxkmsc4") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.3.0 (c (n "pem-rfc7468") (v "0.3.0") (d (list (d (n "base64ct") (r "^1.2") (d #t) (k 0)))) (h "1vijz4iyw842iwpw9vbbsk22sqksbwqnzwxr3nprmz12h19m84mg") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.3.1 (c (n "pem-rfc7468") (v "0.3.1") (d (list (d (n "base64ct") (r "^1") (d #t) (k 0)))) (h "0c7vrrksg8fqzxb7q4clzl14f0qnqky7jqspjqi4pailiybmvph1") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.4.0-pre.0 (c (n "pem-rfc7468") (v "0.4.0-pre.0") (d (list (d (n "base64ct") (r "=1.4.0-pre.0") (d #t) (k 0)))) (h "0zvznxwzfal4zxwra8jwaghjxm0cvqrjdvignwnpjsb3g7vcxwpz") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.4.0-pre.1 (c (n "pem-rfc7468") (v "0.4.0-pre.1") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "1mjfjqnp13y87xvjmq15d24c11f850l3k37q9826g5knsjb8yhh6") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.4.0 (c (n "pem-rfc7468") (v "0.4.0") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "1489q0axcw7i16f1rgi4k4p1sfz6sk5gbs4nxnfd6c330zk19xv2") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.5.0 (c (n "pem-rfc7468") (v "0.5.0") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "0hlmq7hsgcdg6k6blwrcmdiirwyzc858hbzpjzs8s8f0chiyn9kf") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (y #t) (r "1.56")))

(define-public crate-pem-rfc7468-0.5.1 (c (n "pem-rfc7468") (v "0.5.1") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "0cmrnp8q5x66ldgvgp7p6mv0nrvdyf3x3ypgwm4dmb5a7420fglp") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.6.0 (c (n "pem-rfc7468") (v "0.6.0") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "1b5d8rvc4lgwxhs72m99fnrg0wq7bqh4x4wq0c7501ci7a1mkl94") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.56")))

(define-public crate-pem-rfc7468-0.7.0 (c (n "pem-rfc7468") (v "0.7.0") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "04l4852scl4zdva31c1z6jafbak0ni5pi0j38ml108zwzjdrrcw8") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.60")))

(define-public crate-pem-rfc7468-1.0.0-pre.0 (c (n "pem-rfc7468") (v "1.0.0-pre.0") (d (list (d (n "base64ct") (r "^1.4") (d #t) (k 0)))) (h "0zf23n156jp6jsc8q25xdifsmmh8kyacky5k0n40ys6i4wf5x9kn") (f (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (r "1.60")))

