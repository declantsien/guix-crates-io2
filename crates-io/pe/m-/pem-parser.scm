(define-module (crates-io pe m- pem-parser) #:use-module (crates-io))

(define-public crate-pem-parser-0.1.0 (c (n "pem-parser") (v "0.1.0") (d (list (d (n "openssl") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1qf7w4jyzmj0hlgpymy04kj08m40vgzdwvq1af70hwx6sn3vvppv")))

(define-public crate-pem-parser-0.1.1 (c (n "pem-parser") (v "0.1.1") (d (list (d (n "openssl") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1l3splkmb125vqrgj2agi6047afaa45id3mrs45drhn16aj9hda4")))

