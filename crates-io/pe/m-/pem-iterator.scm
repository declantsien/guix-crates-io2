(define-module (crates-io pe m- pem-iterator) #:use-module (crates-io))

(define-public crate-pem-iterator-0.1.0 (c (n "pem-iterator") (v "0.1.0") (d (list (d (n "pem") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "124vh94631q98sl97kq6320g6w4axkqy14sjikpxzjqmwxvrsqqa") (f (quote (("store_label") ("std") ("default" "std" "store_label"))))))

(define-public crate-pem-iterator-0.2.0 (c (n "pem-iterator") (v "0.2.0") (d (list (d (n "pem") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "09kzqsh9zkyib9gavgy2gc7pjvfznmpk505dfyq0qa9yll0dmja5") (f (quote (("std") ("generators") ("default" "std"))))))

