(define-module (crates-io pe rk perkv) #:use-module (crates-io))

(define-public crate-perkv-0.0.0 (c (n "perkv") (v "0.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "persy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1wwh9lgd5lfxr3wsnl1jwcszhpyk2h0jvgk8nb4qh8yb2in4rnfp") (y #t)))

(define-public crate-perkv-0.0.1 (c (n "perkv") (v "0.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "persy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0vjpv8s4zssdh3lfk6yddgjb78myj164ys7mcmla295rqpf9r2qc") (y #t)))

(define-public crate-perkv-0.0.2 (c (n "perkv") (v "0.0.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "delegate") (r "^0.3.0") (d #t) (k 0)) (d (n "persawkv") (r "^0.0.0-alpha.1") (d #t) (k 0)) (d (n "persy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1w6n4r8y8l1ikcpqb12n90lpbyrk0s98f5326jjxlbyqh6gd1ynx") (y #t)))

