(define-module (crates-io pe ro peroxide-num) #:use-module (crates-io))

(define-public crate-peroxide-num-0.1.0 (c (n "peroxide-num") (v "0.1.0") (h "0lph3rpzhhhk45s0ihzkhmz8ah34x1391y66dvhwx9pg45vlscfx")))

(define-public crate-peroxide-num-0.1.1 (c (n "peroxide-num") (v "0.1.1") (h "127acplj3rhfmmxynsj9q94mncjzbr8ap8b6p50ba1z10vh8ciya")))

(define-public crate-peroxide-num-0.1.2 (c (n "peroxide-num") (v "0.1.2") (h "10jq7jcvr6n9lmbgnnr3634lx8z97j1kvfza57fyym2ix6pz4346")))

(define-public crate-peroxide-num-0.1.3 (c (n "peroxide-num") (v "0.1.3") (h "1396r28b7ssppwhwy0yi7ykrnx0s6s7z1fh596c5kzap01nyzw8h")))

(define-public crate-peroxide-num-0.1.4 (c (n "peroxide-num") (v "0.1.4") (h "0qvqlwrwkxqq2788dw86mb5pzv5q78m0wpcbni3j32a1f7gpvcg6")))

