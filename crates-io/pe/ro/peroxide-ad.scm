(define-module (crates-io pe ro peroxide-ad) #:use-module (crates-io))

(define-public crate-peroxide-ad-0.1.0 (c (n "peroxide-ad") (v "0.1.0") (h "1ninkbgbam7ypmk4fjzjkbw6qvhh0gdz59mx8kv4wdggj0z96f54")))

(define-public crate-peroxide-ad-0.1.1 (c (n "peroxide-ad") (v "0.1.1") (h "1k02v8yy0m0pdmwayspdpacfjkv6p98h1dy9f7agp585618ahv7z")))

(define-public crate-peroxide-ad-0.1.2 (c (n "peroxide-ad") (v "0.1.2") (h "0b06zsmpg4mzzlpdi2q0cfhalmqq7nr8lfiv091i9dsz2cf1s4la")))

(define-public crate-peroxide-ad-0.1.3 (c (n "peroxide-ad") (v "0.1.3") (h "0yfcn7fpl6m4lj830zcb2v1rb6cnz0lc8mdzsnp5db22a0v45yrz")))

(define-public crate-peroxide-ad-0.1.4 (c (n "peroxide-ad") (v "0.1.4") (h "1hc30qf312amh585kgfy61zh9n95i4ghlvzg1s2kamzn3apph47v")))

(define-public crate-peroxide-ad-0.2.0 (c (n "peroxide-ad") (v "0.2.0") (d (list (d (n "watt") (r "^0.4") (d #t) (k 0)))) (h "1chcy6bdl7768yc9yacyxrkhqpz0qmhlkp29mqvq17an2m8py31h")))

(define-public crate-peroxide-ad-0.2.1 (c (n "peroxide-ad") (v "0.2.1") (d (list (d (n "watt") (r "^0.4") (d #t) (k 0)))) (h "0cwqwja94ksdi8nxk7s49nj0x2j17b87gh2yi7j5qr1ss58v3slx")))

(define-public crate-peroxide-ad-0.3.0 (c (n "peroxide-ad") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "136wa7ffxxysk74qw5y8ymzfb8xspaczcif7yyb7kdj07zzsiyzn")))

