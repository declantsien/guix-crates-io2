(define-module (crates-io pe bb pebble_syscall_common) #:use-module (crates-io))

(define-public crate-pebble_syscall_common-0.1.0 (c (n "pebble_syscall_common") (v "0.1.0") (h "0q1l12hrl8vk8idxcay62nayvb7279p2ii0agcc6k38n7wcsi81q")))

(define-public crate-pebble_syscall_common-0.1.1 (c (n "pebble_syscall_common") (v "0.1.1") (h "0sy9f4gn5shh75acavs79s3kl0mw4v2aqf5zfysl9ar2y9c95nng")))

(define-public crate-pebble_syscall_common-0.1.2 (c (n "pebble_syscall_common") (v "0.1.2") (h "0118aw7nhf7n0a3i26mjh55mmprcwnjjyd1rn5v0fqczviralwr4")))

(define-public crate-pebble_syscall_common-0.1.3 (c (n "pebble_syscall_common") (v "0.1.3") (h "1cks8dxxjjcqr99pj8rwqyayfd623ckvym00d37lkkx68h99pr59")))

(define-public crate-pebble_syscall_common-0.1.4 (c (n "pebble_syscall_common") (v "0.1.4") (h "0ckczrspan47rvngm3wvwgcbp0c7vynnh46rf98hg8nrhwdg1q3w")))

