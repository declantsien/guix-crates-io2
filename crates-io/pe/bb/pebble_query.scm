(define-module (crates-io pe bb pebble_query) #:use-module (crates-io))

(define-public crate-pebble_query-0.1.0 (c (n "pebble_query") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "sea-orm") (r "^0.12.3") (f (quote ("sqlx-sqlite" "runtime-tokio-rustls" "mock" "macros" "debug-print" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "09lgfdca3wbfz027iwrk9i2gih0bw4viwqcfh1yqzznbjbkrbayl")))

