(define-module (crates-io pe bb pebble) #:use-module (crates-io))

(define-public crate-pebble-0.8.4 (c (n "pebble") (v "0.8.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "recipe-reader") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "0zwr68vvi93ncvnqi4491zmgwlf66g7km6czfri51pzhm00g0pga")))

(define-public crate-pebble-0.8.5 (c (n "pebble") (v "0.8.5") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)) (d (n "recipe-reader") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.4") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "0wkn28kvxq89z4c1v0zyx0zdj86gj3ss2i9xb4c7cc7vbffziqrh")))

