(define-module (crates-io pe bb pebble-skip) #:use-module (crates-io))

(define-public crate-pebble-skip-0.0.1 (c (n "pebble-skip") (v "0.0.1") (d (list (d (n "debugless-unwrap") (r "^0.0.4") (d #t) (k 0)) (d (n "pebble-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1.0") (d #t) (k 0)))) (h "0z4svbcdgqgb5zi3y4qqmahhy297pqcdldw728928rnp2cka3i83")))

