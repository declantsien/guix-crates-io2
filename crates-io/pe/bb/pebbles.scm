(define-module (crates-io pe bb pebbles) #:use-module (crates-io))

(define-public crate-pebbles-0.0.1 (c (n "pebbles") (v "0.0.1") (d (list (d (n "quickjs-rs") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17jpps5x4kx8hyw64241ng5x2p0nfrin4706j1lznsf10hrxvjlj")))

(define-public crate-pebbles-0.0.101 (c (n "pebbles") (v "0.0.101") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1b3bm19qbsq10rv3bgjz4jnir8nx17p8q7ym0qbb32bc4hn6ca3l")))

(define-public crate-pebbles-0.0.102 (c (n "pebbles") (v "0.0.102") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "11d65yfdqz677f29ni87z1iy5dkiavp6caic60pbqijfk2ihf5lx")))

(define-public crate-pebbles-0.0.103 (c (n "pebbles") (v "0.0.103") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1brmglmr3ngzkgqiiknpd8k7l25zpc64275msbg2acv0dabgp8m4")))

