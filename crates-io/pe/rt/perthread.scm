(define-module (crates-io pe rt perthread) #:use-module (crates-io))

(define-public crate-perthread-0.1.0 (c (n "perthread") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "1kf1r8kxkdhlbb3kcycnz9h07mj68vxm28cpk9vsniwsr318psra")))

(define-public crate-perthread-0.1.1 (c (n "perthread") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "03zjvaxk11zsz7c4rsxyzrvwr5frxi6gpr388ly5bvbwfpajibqy")))

(define-public crate-perthread-0.1.2 (c (n "perthread") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "03mw7lkzwn6nl1azh16ils02jjap4ym307i9f8gi3hdiywwwrc0k")))

(define-public crate-perthread-0.1.3 (c (n "perthread") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.12") (d #t) (k 2)))) (h "1g6jbn0r0yz31qa3q47lkal818jm0p02vp7hacgpvfiycrp0cpq6")))

