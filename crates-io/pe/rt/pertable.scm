(define-module (crates-io pe rt pertable) #:use-module (crates-io))

(define-public crate-pertable-0.1.0 (c (n "pertable") (v "0.1.0") (h "1dxg2d0rh8wjkqpb47gqzr6zz1kpdzgikm6320w80b4andq4vdl0") (y #t)))

(define-public crate-pertable-0.1.2 (c (n "pertable") (v "0.1.2") (h "0fs4m7fpfp0irpix82372kpncf80dx81wxc8bmmgnzj8qwxffn2p")))

(define-public crate-pertable-0.1.3 (c (n "pertable") (v "0.1.3") (h "12hm2gc40dyw6h1q7l1xrv8la81jmp1dzj0kr55rw54ak5mdd518")))

(define-public crate-pertable-0.1.4 (c (n "pertable") (v "0.1.4") (h "18865d9f6zlaswarylck5flb9rl7fscs4scbp62m48kbn8xfqkbv")))

