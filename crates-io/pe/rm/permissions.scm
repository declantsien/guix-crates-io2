(define-module (crates-io pe rm permissions) #:use-module (crates-io))

(define-public crate-permissions-0.0.1 (c (n "permissions") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "069wycgj8fl2fnwfix3civzw6c04c4px5ff0x7y4h5y61iadppzf")))

(define-public crate-permissions-0.1.0 (c (n "permissions") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zvnc7an03b00rhs7lg93s5w9qh5h8i0w69dbrp70xaz02xhsdcx")))

(define-public crate-permissions-0.1.1 (c (n "permissions") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18nqfxfqqwyaxssn1fcwyr1jfgmlxy2yik5hz733b8yr08vbgfbp")))

(define-public crate-permissions-0.2.0 (c (n "permissions") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lk79c931jz2nczrqbr6pv0nycdsk1fahfz39sp14hw8pljimss0")))

(define-public crate-permissions-0.3.0 (c (n "permissions") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w22jn7vp6g0aryrmdycm7p7pkljsfs90yzvb0z0ipin3rhbzjln") (y #t)))

(define-public crate-permissions-0.2.1 (c (n "permissions") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10w96w9iqsv947g0m0zr4ksdm4mvqav4b2g44hd5z02q4drh9x8k") (y #t)))

(define-public crate-permissions-0.2.2 (c (n "permissions") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zp2qw3fnkg7ldlrz0vfr33kkl8alyvba3f18q0yqcha04d6czb9")))

(define-public crate-permissions-0.4.0 (c (n "permissions") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vl56sgd2r2f1yrmv8dqy06yhkb95vw127fp6bwj9l904nkfin61") (y #t)))

(define-public crate-permissions-0.4.1 (c (n "permissions") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p5jadvzf4gwyp5l5zhpnhrsvfzqrr94hvmrhf7p60bpmdbg0gfc")))

(define-public crate-permissions-0.5.0 (c (n "permissions") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jrqwabm2w6672kjvk7g5nc0z218r7dyz2r5bajyi7xil2ldqa21")))

(define-public crate-permissions-0.5.1 (c (n "permissions") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jbla4ydy6sm2pirn6q7n72khrxjdbkhipmpxbsldnxg85ga2lpw")))

