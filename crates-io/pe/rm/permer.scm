(define-module (crates-io pe rm permer) #:use-module (crates-io))

(define-public crate-permer-0.1.1 (c (n "permer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0l9al2gi0krwfkiai6d87ii1jvaljb8bcjmwq43pg0gc6dfbri8q")))

