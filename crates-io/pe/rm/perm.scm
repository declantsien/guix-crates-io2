(define-module (crates-io pe rm perm) #:use-module (crates-io))

(define-public crate-perm-0.1.0 (c (n "perm") (v "0.1.0") (h "1p770vy6q2c63avwzr4ibjklaq4pbm6iibr593mbpf2qq8wwg138")))

(define-public crate-perm-0.2.0 (c (n "perm") (v "0.2.0") (h "019j7qgzmk08fwp068dnnf90lbmfzj6x6vh5c6cf0sbnjbknqhy7") (f (quote (("std"))))))

(define-public crate-perm-0.3.0 (c (n "perm") (v "0.3.0") (h "1h7zc8pnajz3sszqlbg1yn676q4m824mbddnni27l5smk8p3wpkf") (f (quote (("std"))))))

(define-public crate-perm-0.4.0 (c (n "perm") (v "0.4.0") (h "0hsg1c01pgv54bdw5sri48225dfn4lsc1k8s5jak82p3hcnxjgzj") (f (quote (("std"))))))

(define-public crate-perm-0.5.0 (c (n "perm") (v "0.5.0") (h "1k6n5h4wng0fxlwbq2msrdh4s23xb1hg7bw9fdb5l7d6arnbjq90") (f (quote (("std"))))))

(define-public crate-perm-0.6.0 (c (n "perm") (v "0.6.0") (h "1805ip4nkbyf5kbv6cr1y3rgy7qryv0p8hx6zvy6h9lwpff4596f") (f (quote (("std"))))))

(define-public crate-perm-0.7.0 (c (n "perm") (v "0.7.0") (h "1393la7wg6bj5y4z077sg98zf5lq2lwkh12zl8y4iwyw20p9fj25") (f (quote (("std"))))))

(define-public crate-perm-0.8.0 (c (n "perm") (v "0.8.0") (h "0pccacsiw3ph49j6m8hq5w1472blwh6q0dwynbmpinifq1yngv53") (f (quote (("std"))))))

(define-public crate-perm-0.8.1 (c (n "perm") (v "0.8.1") (h "165ib2wqz698g1jb2ndvsp5s7rsc29rw2ygwd2rgi2sjz5rfy8sf") (f (quote (("std"))))))

(define-public crate-perm-0.9.0 (c (n "perm") (v "0.9.0") (h "1dmjzl2zc1kfqhb5yybp3z7c4xnvjj9nvd33f8n3sbdlkybgcr4g") (f (quote (("std"))))))

(define-public crate-perm-0.9.1 (c (n "perm") (v "0.9.1") (h "0ixw4a8nv9n53w0234dls2n4xad0ikh7givf85zcnm7zy7csag73") (f (quote (("std"))))))

(define-public crate-perm-0.9.2 (c (n "perm") (v "0.9.2") (h "061ynnfl282gw04z1g77amwqq6bp8mhcbxnx5pvl48280k388n1s") (f (quote (("std"))))))

(define-public crate-perm-0.9.3 (c (n "perm") (v "0.9.3") (h "1z76wfpnm5cpmak2h69agm2x5sr0s0wzz1vl474fjwp5dvav0071") (f (quote (("std"))))))

