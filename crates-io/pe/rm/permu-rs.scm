(define-module (crates-io pe rm permu-rs) #:use-module (crates-io))

(define-public crate-permu-rs-0.1.0 (c (n "permu-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "118l5pzb85c74lf0n7ar2f11ndklmhfg3haf14gl6s5jdi3xqqlf")))

(define-public crate-permu-rs-0.1.1 (c (n "permu-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "09lpzs7phwgg5g9mf67b9f05nsl22f4i5i2rh6ddkl2x35jljr47")))

(define-public crate-permu-rs-0.1.2 (c (n "permu-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "193vlyhxy923dazcfmafrdydq3wxw808hxl1mr6b88fhqiiyd7pq")))

(define-public crate-permu-rs-0.1.3 (c (n "permu-rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "00nfqnfwckc1bz61ngl23v23s2mgyc3fwlbb7d68xaryvhrdcmcm")))

(define-public crate-permu-rs-0.1.3-1 (c (n "permu-rs") (v "0.1.3-1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1zy4405na9wa8b5pafanvzbj3sh0bbl2p6pnavq3p5qmyxc7l9cz")))

(define-public crate-permu-rs-0.1.4 (c (n "permu-rs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "03g4zhibjaj24ibwizq9gg12k0rfwvpb58j81a3iycaa61v7mnw0")))

(define-public crate-permu-rs-0.2.0 (c (n "permu-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "160gip00qqqg8av6xygqwbf8vsnj8qy02azlv63lx2bwc658fac7")))

