(define-module (crates-io pe rm permutation_iterator) #:use-module (crates-io))

(define-public crate-permutation_iterator-0.1.0 (c (n "permutation_iterator") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0379gmbn70kldcj3h5s4pksibzgs5f753kiybcr73ibb8nh4lgv7")))

(define-public crate-permutation_iterator-0.1.1 (c (n "permutation_iterator") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1ca15spgb2ksi87wah7dsrrn2j5bssfr21bfhidv6kzrgg3zmmwc")))

(define-public crate-permutation_iterator-0.1.2 (c (n "permutation_iterator") (v "0.1.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1jx9djl2269bvfn595dgjrs9xzyda6drzbyx40w4wvp0kwbham5m")))

