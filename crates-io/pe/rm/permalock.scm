(define-module (crates-io pe rm permalock) #:use-module (crates-io))

(define-public crate-permalock-0.1.4 (c (n "permalock") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "09n6mpsvxk6gi4l07q1rdqx7cxs2zawz206iwdsg6kzxl0hkk7l5") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-permalock-0.2.0 (c (n "permalock") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "1zcjjww5khdqzjjb5k9p94rhj64pfzi0wsbcrlqmy0ajjld2zmjl") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

