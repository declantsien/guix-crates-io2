(define-module (crates-io pe rm permoot) #:use-module (crates-io))

(define-public crate-permoot-0.1.0 (c (n "permoot") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1vd63vq18g54g5r554lj80p6kd3cicf9aqym8vbcwcdcb39j69rn") (f (quote (("std"))))))

(define-public crate-permoot-0.2.0 (c (n "permoot") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0p2i95b0pxhs3bdjl6d4z818m32glf8xxg7hd5x908kcc1z8qj9d") (f (quote (("std"))))))

(define-public crate-permoot-0.2.1 (c (n "permoot") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "16y7xilhwyz4csng1ss0bcrha6hvkvagkann0x348xhhv19ajpwc") (f (quote (("std"))))))

