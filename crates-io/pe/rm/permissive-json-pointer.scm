(define-module (crates-io pe rm permissive-json-pointer) #:use-module (crates-io))

(define-public crate-permissive-json-pointer-0.1.0 (c (n "permissive-json-pointer") (v "0.1.0") (d (list (d (n "big_s") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15n97icqgz9ymn353m6k3c3jd2m2rjzzsji86qn6idz49b1mkwr2")))

(define-public crate-permissive-json-pointer-0.1.1 (c (n "permissive-json-pointer") (v "0.1.1") (d (list (d (n "big_s") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w498g4hns5chwm2n8nqa0djn3dni2gi9km6hq03dlb9v9iphniq")))

(define-public crate-permissive-json-pointer-0.1.2 (c (n "permissive-json-pointer") (v "0.1.2") (d (list (d (n "big_s") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18dbrsdb9brvwvp0cxrkj3vbv7ngv0as9a0mnzjplsny134pyli7")))

(define-public crate-permissive-json-pointer-0.2.0 (c (n "permissive-json-pointer") (v "0.2.0") (d (list (d (n "big_s") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19hvixxdza736kr27ybks5lz87gp88w39az6bhkgspx48kyga991")))

