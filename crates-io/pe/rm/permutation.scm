(define-module (crates-io pe rm permutation) #:use-module (crates-io))

(define-public crate-permutation-0.1.0 (c (n "permutation") (v "0.1.0") (h "16j6jl0v6i7v0a3kj8m9rqwkf5f5h0qgk42znqpcasc5660xkqhz")))

(define-public crate-permutation-0.2.0 (c (n "permutation") (v "0.2.0") (h "1zz05nhbwbx3x4b01kjh0qvi28ga2vnc7p3lg4z7avj3sb47k3b9")))

(define-public crate-permutation-0.2.1 (c (n "permutation") (v "0.2.1") (h "1c7lh55j43jh561bm29rrisrvrlwskw3v5jn99l4s8av1rqa248g")))

(define-public crate-permutation-0.2.2 (c (n "permutation") (v "0.2.2") (h "16wqqlrcvbsq7i96gzikvshmj3cmz2wrx1drm503xbsihr57rrh6")))

(define-public crate-permutation-0.2.3 (c (n "permutation") (v "0.2.3") (h "1wzxrg1ydvigvby6yim54fmv39i5dsyn0mpyrg1wf4ancy8dpykr")))

(define-public crate-permutation-0.2.4 (c (n "permutation") (v "0.2.4") (h "0wpn95m1sg1pmqi2rkgirk52a5lx0rnmr14jna0mmhkkcsnd6c0a")))

(define-public crate-permutation-0.2.5 (c (n "permutation") (v "0.2.5") (h "0hkin9ig66a0i5n0bzsfk790dqkm5nfx19j7fklmicd4z1i8k5yr")))

(define-public crate-permutation-0.3.0 (c (n "permutation") (v "0.3.0") (h "1p7kylcjxgb0vlpyni5l3pwwv07f5f5sqmnknp7lw1f4sfwpkp9x")))

(define-public crate-permutation-0.4.0 (c (n "permutation") (v "0.4.0") (h "0x17z1y2pvyh3s4j367v3n9f5n27y1z84808b6xnaqddm63s7x7k")))

(define-public crate-permutation-0.4.1 (c (n "permutation") (v "0.4.1") (h "1msp65z2q3f7p4fkz5b2h6agn03v01xz5mdgancki3jv1w5jn86z")))

