(define-module (crates-io pe rm permit) #:use-module (crates-io))

(define-public crate-permit-0.1.0 (c (n "permit") (v "0.1.0") (h "1v7ask3p4y634a98p21m9qb9aj8qg897whkxabv5cbkzsq6lx8v6")))

(define-public crate-permit-0.1.1 (c (n "permit") (v "0.1.1") (h "0nyx8l3fmzbakl7r7g8wxgwmc7vyjmk0wnbjc8ix2d6lq5i8kvml")))

(define-public crate-permit-0.1.2 (c (n "permit") (v "0.1.2") (d (list (d (n "safina") (r "^0.1.9") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1.9") (d #t) (k 2)))) (h "14q1nz1af1wxxx8jiwij6d22a2h21sndwijpbyf5ripn7qjddwv4") (y #t)))

(define-public crate-permit-0.1.3 (c (n "permit") (v "0.1.3") (d (list (d (n "safina") (r "^0.1.9") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1.9") (d #t) (k 2)))) (h "12ravl5q5di9d470x3sha91dr7qpgq52rw25x0fivaaiidw924mr") (y #t)))

(define-public crate-permit-0.1.4 (c (n "permit") (v "0.1.4") (d (list (d (n "safina") (r "^0.1.9") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1.9") (d #t) (k 2)))) (h "0wm2ffyl6axd23zr32ya7x6hyd98jjk1pvm6drxqv2xjaxmg1vn6")))

(define-public crate-permit-0.1.5 (c (n "permit") (v "0.1.5") (d (list (d (n "safina") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)))) (h "1imb10b1rrqna43fa5d7w76hcdwxbj7bp6f53m3rkwayvpxysccq")))

(define-public crate-permit-0.2.0 (c (n "permit") (v "0.2.0") (d (list (d (n "safina") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)))) (h "0qlh3yx18w1n1jsyqqajp9pgjsx71y01c6l3s1h2rn7i5x9qis35") (y #t)))

(define-public crate-permit-0.2.1 (c (n "permit") (v "0.2.1") (d (list (d (n "safina") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)))) (h "13n8g00022nih5j766nl271l3bah2xndva1cyippnbifbmagx9fq")))

