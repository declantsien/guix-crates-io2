(define-module (crates-io pe rm permutation-generator-rs) #:use-module (crates-io))

(define-public crate-permutation-generator-rs-0.0.1 (c (n "permutation-generator-rs") (v "0.0.1") (h "04im8zri7hk4bfbad6pp1cwk7qv7w9v322zksah3dqifjwmc3ahy") (y #t)))

(define-public crate-permutation-generator-rs-0.1.0 (c (n "permutation-generator-rs") (v "0.1.0") (d (list (d (n "bit-index") (r ">=0.2") (d #t) (k 0)))) (h "0jgv2alfxnyxmiwlmfaxc4fhsq5vmhncizibgacv543mls0aayyn") (y #t)))

(define-public crate-permutation-generator-rs-0.1.1 (c (n "permutation-generator-rs") (v "0.1.1") (d (list (d (n "bit-index") (r ">=0.2") (d #t) (k 0)))) (h "0d4i41n3cw87ghxfhr446qqsjfsjsg19mka4a9v12x95wa631z0m") (y #t)))

