(define-module (crates-io pe rm permutation-rs) #:use-module (crates-io))

(define-public crate-permutation-rs-1.0.0 (c (n "permutation-rs") (v "1.0.0") (h "1rmf5kcxrz06hp0hjigj4dk3h6fhca43ymr8d84sky7vnkfw4xik")))

(define-public crate-permutation-rs-1.1.0 (c (n "permutation-rs") (v "1.1.0") (h "1yjc6hlbzh76iwa1n37jzwwl95qnc5wspcaybxnarr13m926nv6n")))

(define-public crate-permutation-rs-1.1.2 (c (n "permutation-rs") (v "1.1.2") (h "0nlgsl1rrzb0qb13mk353fxfcgr3yccy3ja7fj6mdpxaa4rz9sq2")))

(define-public crate-permutation-rs-2.0.0 (c (n "permutation-rs") (v "2.0.0") (h "1nf7ipwfmmvmq4brifgh56f5f21pja2chhcwicak1aqf68jhmb1l")))

(define-public crate-permutation-rs-3.0.0 (c (n "permutation-rs") (v "3.0.0") (h "1m3r21fnb45yc9d6ybhcnhgpmsckf0bginzjygza4cfi7cxyfbvj")))

