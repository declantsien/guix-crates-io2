(define-module (crates-io pe rm permut_lib) #:use-module (crates-io))

(define-public crate-permut_lib-0.1.0 (c (n "permut_lib") (v "0.1.0") (h "0m745v9rxcfxhq2lfwjvsvqd8hbdwr62w14m4wkkc8n9qckw9zh5")))

(define-public crate-permut_lib-0.1.1 (c (n "permut_lib") (v "0.1.1") (h "0ikwf2q5b9zdvhva32nid0nkkg32qc7xms94ficcfvw22wrap8kn")))

(define-public crate-permut_lib-0.1.2 (c (n "permut_lib") (v "0.1.2") (h "0m189qng7x6j76am0siylajp65rbpirvzzbhlm6kzjgj8z02vfdz")))

