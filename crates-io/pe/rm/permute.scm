(define-module (crates-io pe rm permute) #:use-module (crates-io))

(define-public crate-permute-0.1.0 (c (n "permute") (v "0.1.0") (h "02cj9rd04drpxqqcfv64cvmvc62w6003bry2iwwjsmwyd9mczmdd")))

(define-public crate-permute-0.2.1 (c (n "permute") (v "0.2.1") (h "1jl8grl533vir5y667sb8vip1j0q5r3s4dz3pl141iv8rzrbipag")))

