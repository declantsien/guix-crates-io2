(define-module (crates-io pe rm permutations) #:use-module (crates-io))

(define-public crate-permutations-0.1.0 (c (n "permutations") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0hsnxgi3rxd9vpkzx0k2pf3gs2h2pb6gl8sncr61gss136xfliaa") (f (quote (("random" "rand") ("default"))))))

(define-public crate-permutations-0.1.1 (c (n "permutations") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1wwxghc7f5asdjxk865wn551bzz7rr4hc0wsygl0v4ilh6xjmkkd") (f (quote (("random" "rand") ("default"))))))

