(define-module (crates-io pe rm permutations_iter) #:use-module (crates-io))

(define-public crate-permutations_iter-0.1.0 (c (n "permutations_iter") (v "0.1.0") (h "04wzp0v7c26ldgvgzz057r8nf5vli6aglvah0lpx1adsqsn36lyp")))

(define-public crate-permutations_iter-0.1.1 (c (n "permutations_iter") (v "0.1.1") (h "0fqxfxnyfj1j9c3ibi4z4ni77303fsnhxcid8xzkz5549mqv44yx")))

