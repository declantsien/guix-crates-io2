(define-module (crates-io pe rm permutator) #:use-module (crates-io))

(define-public crate-permutator-0.1.0 (c (n "permutator") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1ff2z4aifdcx1mkrs09qf729s5fzdsmqdpw1y4ihwas21ykhf4bw")))

(define-public crate-permutator-0.1.1 (c (n "permutator") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1y8kbr9ibb2sjx3i4w56kzpnhg03d95nnx8i4b40vb33kd1x67y9")))

(define-public crate-permutator-0.1.2 (c (n "permutator") (v "0.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "19fask1vbg6aan5d1lz6lbwc2kf261zr4frnlja69lywr5zj1cfs")))

(define-public crate-permutator-0.1.3 (c (n "permutator") (v "0.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0ycxcb1d8853l14mz5csskbrmsgbqkpwm6d8gvg3mzp0ji7p8g6m")))

(define-public crate-permutator-0.1.4 (c (n "permutator") (v "0.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0arn97i72zi9xcw5f6jmpp8zj9mvr5kb1i0kbp85kb9d9a9rbnim") (y #t)))

(define-public crate-permutator-0.1.5 (c (n "permutator") (v "0.1.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0jqvj2ylxl1m0jvjkhzjnypaans29nzvm2qfwnl7ysif3rshbgiq") (y #t)))

(define-public crate-permutator-0.1.6 (c (n "permutator") (v "0.1.6") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1bmv41mvd63zxfqrbbf6a8gcddw8fiw0nbq3mx8r5a4i9prjln7b")))

(define-public crate-permutator-0.2.0 (c (n "permutator") (v "0.2.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1n49iz4v4m5dfhjgafhfcdnp3cy2lal50fch94xwnrjqzk52f3rm")))

(define-public crate-permutator-0.2.1 (c (n "permutator") (v "0.2.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "153jsw8hfdls3b8276k4hhj1r7b59zl4w8kj7b8647apfxnm588z")))

(define-public crate-permutator-0.2.2 (c (n "permutator") (v "0.2.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1j1gb5ah938m972zkf0ncmmmchm4p3i601jgs6nn5pwaq0sa71bm")))

(define-public crate-permutator-0.2.3 (c (n "permutator") (v "0.2.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1k7g22ndbw3qlld125wa8ildrxcszz94h7nh3fg1ynrg2icl78hf") (y #t)))

(define-public crate-permutator-0.3.0 (c (n "permutator") (v "0.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0yxhh3yp0dwa91sz70zfg5v91fjj9s3v5ysb77c30k2pd7ymzq3j")))

(define-public crate-permutator-0.3.1 (c (n "permutator") (v "0.3.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0fdmlk0kacm249y4607ds4kglpk2bdwy3ynyyfkv707m78m1hcp5")))

(define-public crate-permutator-0.3.2 (c (n "permutator") (v "0.3.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0ycfzl905p9wny1f3wm6472x6biv2vchvh2kszan8fbzk5pjwjr0")))

(define-public crate-permutator-0.3.3 (c (n "permutator") (v "0.3.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0cyw5ix05zbf945yaigg5d3ba9j9cq858xpi7yhqvkfcmjjfrnsf")))

(define-public crate-permutator-0.4.0 (c (n "permutator") (v "0.4.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "02vahw9n4rj0j596z7vjr08hwbm9gp7yf2ikxgd3qr4h721k4lda") (f (quote (("default"))))))

(define-public crate-permutator-0.4.1 (c (n "permutator") (v "0.4.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0p2rqg954nafkl0invmp152hkkdb2dzv3rvliw6bqjm3ilr93zl2") (f (quote (("default"))))))

(define-public crate-permutator-0.4.2 (c (n "permutator") (v "0.4.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "04x2vx2qhrjxcnk47pq6dazqbpxyx1h71kyxvw7mzm2w17p0hz9c") (f (quote (("default"))))))

(define-public crate-permutator-0.4.3 (c (n "permutator") (v "0.4.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1mhh4mpmmlh45by6bavfk241g2qriw7lgaj4wd5w6d98qf7z7im9") (f (quote (("default"))))))

