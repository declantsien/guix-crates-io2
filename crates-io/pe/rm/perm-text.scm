(define-module (crates-io pe rm perm-text) #:use-module (crates-io))

(define-public crate-perm-text-1.0.2 (c (n "perm-text") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16crwmblah2kf578dyaa2aa7ywwacwprqfg49rmqa4174yh5h5z5")))

(define-public crate-perm-text-1.0.3 (c (n "perm-text") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1lc92i2drzbwb5zfkkr21269k0rrv25ag4nqvbw5w2497i36yxzp")))

(define-public crate-perm-text-1.0.4 (c (n "perm-text") (v "1.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1iyzpw3j67gknx263dysg8wp06b9d08mcl3j98wjp47nj7cfhgbm")))

