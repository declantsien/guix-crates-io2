(define-module (crates-io pe rm permutation-xoodoo) #:use-module (crates-io))

(define-public crate-permutation-xoodoo-0.1.0 (c (n "permutation-xoodoo") (v "0.1.0") (d (list (d (n "crypto-permutation") (r "^0.1") (f (quote ("io_le_uint_slice" "io_uint_u32"))) (d #t) (k 0)) (d (n "xoodoo-p") (r "^0.1") (d #t) (k 0)))) (h "093yiiwx8dvjd0vsvq6s63b8kdg8yvrb0hz4iv5qmn51pz151bjw") (f (quote (("default") ("debug")))) (r "1.65")))

