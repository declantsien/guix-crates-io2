(define-module (crates-io pe rm permutohedron) #:use-module (crates-io))

(define-public crate-permutohedron-0.1.0 (c (n "permutohedron") (v "0.1.0") (h "1zd88mps5sk7aa8z0x80i3cmz4s8fdmgqdpni83w3b5b0g7m03hn")))

(define-public crate-permutohedron-0.1.1 (c (n "permutohedron") (v "0.1.1") (h "0wq0rf7q9lz2ivx90bsphnkqis18hrsqyhj91imjpfirfpflw77j")))

(define-public crate-permutohedron-0.1.2 (c (n "permutohedron") (v "0.1.2") (h "0r32kmzxlbspkk6an3iq0x1nc1hi7l4gjb7mc4mbljh6ziy0b703")))

(define-public crate-permutohedron-0.1.3 (c (n "permutohedron") (v "0.1.3") (h "152hb30vi6g7f9x1wb5gqzks425kbf30n2nb57p6kcmmpn51ii4q")))

(define-public crate-permutohedron-0.1.4 (c (n "permutohedron") (v "0.1.4") (h "1n0gidihlfhrwvcwcm094fa542q8hjq9a6d05bypn184rkc8ibb4")))

(define-public crate-permutohedron-0.1.5 (c (n "permutohedron") (v "0.1.5") (h "11xd0i81iyvw4rc75bxwdiki9w7qkal053arp4p2qkr80hwsrf6y")))

(define-public crate-permutohedron-0.1.6 (c (n "permutohedron") (v "0.1.6") (h "0yr70q7rp7kkwbsjy8ha11cl1713llcckl83qfszk8pd5396hqps")))

(define-public crate-permutohedron-0.1.7 (c (n "permutohedron") (v "0.1.7") (h "0ai6q7yrn7900zlwm5g6z1k9dkxgs6i7baq49fjxjw05l20viksp")))

(define-public crate-permutohedron-0.2.0 (c (n "permutohedron") (v "0.2.0") (h "1nscp4js0f3q7cig7d0jwq343g5mhz1r84qbgg5d411lxl20i557")))

(define-public crate-permutohedron-0.2.1 (c (n "permutohedron") (v "0.2.1") (h "10qbfj2i8f6b90ml7d8qazyzbg3258v63wy45zljsy2jicg8mxxb") (f (quote (("std") ("default" "std"))))))

(define-public crate-permutohedron-0.2.2 (c (n "permutohedron") (v "0.2.2") (h "0krhf3m5ha62d2wppr8kl8i062mp6jyihkg433fcya3xk5lm280m") (f (quote (("std") ("default" "std"))))))

(define-public crate-permutohedron-0.2.3 (c (n "permutohedron") (v "0.2.3") (h "1aiznwnv85naf23x2d1pqavfnsj8vn7hn44c9738vw01qrmjb8sw") (f (quote (("std") ("default" "std"))))))

(define-public crate-permutohedron-0.2.4 (c (n "permutohedron") (v "0.2.4") (h "0b1pzh48j86v46wxngch6k1kx9cdw3jr3lwa86gd6jd4bmxzz1xn") (f (quote (("std") ("default" "std"))))))

