(define-module (crates-io pe rm permutate) #:use-module (crates-io))

(define-public crate-permutate-0.1.0 (c (n "permutate") (v "0.1.0") (h "1n8zvx6mmvjs0afwk0mgkqxgswcsaz3sslzwybp606r5mk79lns3")))

(define-public crate-permutate-0.1.1 (c (n "permutate") (v "0.1.1") (h "007a0naa2ccrmz4b3ln9ygyg8ajz9v4ydv6wd2wn5w4bfzlc2nyk")))

(define-public crate-permutate-0.1.2 (c (n "permutate") (v "0.1.2") (h "1b76hmfjvmdlacd090wgngpjbqmha5znyqd5cpvn3b2dgi38c1vb")))

(define-public crate-permutate-0.1.3 (c (n "permutate") (v "0.1.3") (h "1fg6y1p121zxqrjf8xprz0jjhairl661p4g41gp59b824m55hsm9")))

(define-public crate-permutate-0.2.0 (c (n "permutate") (v "0.2.0") (h "1a271ciim2cji4lwxip29jy7c0wjszh3zx8dripnv2i3my0ajjqv")))

(define-public crate-permutate-0.3.0 (c (n "permutate") (v "0.3.0") (h "1sgj1774cypmbn0g29iwpxgxl23ddcabrlk0034iflfjfdd24jkj")))

(define-public crate-permutate-0.3.1 (c (n "permutate") (v "0.3.1") (h "03ik3gycbm9rh99ly2by3rsxj9s0bbxgy80nj2qab3d51252lmhf")))

(define-public crate-permutate-0.3.2 (c (n "permutate") (v "0.3.2") (h "1big083ybpv26pcmpxpf30mzmzb7n12dvackhsrzlpvikaqxbdsk")))

