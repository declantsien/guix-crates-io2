(define-module (crates-io pe rm permutation-keccak) #:use-module (crates-io))

(define-public crate-permutation-keccak-0.1.0 (c (n "permutation-keccak") (v "0.1.0") (d (list (d (n "crypto-permutation") (r "^0.1") (f (quote ("io_le_uint_slice" "io_uint_u64"))) (d #t) (k 0)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "166n7mdax1ayqvbb5zgm11cf2fxkpb4wrsdgg3mx3v90vnjv2w6n") (f (quote (("default") ("debug")))) (r "1.65")))

