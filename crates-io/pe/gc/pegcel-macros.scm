(define-module (crates-io pe gc pegcel-macros) #:use-module (crates-io))

(define-public crate-pegcel-macros-0.1.0 (c (n "pegcel-macros") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p246r49m66rq10gkplndf5p4l4j52ygippp0vfhpnyn65ya8pqc")))

