(define-module (crates-io pe tp petpet-gui) #:use-module (crates-io))

(define-public crate-petpet-gui-1.0.0 (c (n "petpet-gui") (v "1.0.0") (d (list (d (n "nfd2") (r "^0.3.0") (d #t) (k 0)) (d (n "petpet") (r "^2.1.1") (d #t) (k 0)))) (h "1rfwx9g2ryfxp5zg6bwp0v7fcr806pg7vbjcgwr8049sh6zppz7m")))

(define-public crate-petpet-gui-1.0.1 (c (n "petpet-gui") (v "1.0.1") (d (list (d (n "petpet") (r "^2.3.2") (f (quote ("more_format" "image"))) (d #t) (k 0)) (d (n "rfd") (r "^0.13") (d #t) (k 0)))) (h "1jd87bw5chv6pk0bn0q7q2cl228rvg4f9zyy39q4bzpp309pjspf")))

(define-public crate-petpet-gui-1.1.0 (c (n "petpet-gui") (v "1.1.0") (d (list (d (n "petpet") (r "^2.4.1") (f (quote ("more_format" "image"))) (d #t) (k 0)) (d (n "rfd") (r "^0.13") (d #t) (k 0)))) (h "0935yz3pc1wbw85v4gsglavsapk4ycy6alvbymh1gv7zpimhmlzm")))

(define-public crate-petpet-gui-1.1.1 (c (n "petpet-gui") (v "1.1.1") (d (list (d (n "petpet") (r "^2.4.1") (f (quote ("more_format" "image"))) (d #t) (k 0)) (d (n "rfd") (r "^0.13") (d #t) (k 0)))) (h "1ikdf8bxa10ip2kqppilac7zc7zi8diqw4j58qavbr434gv48c5c")))

