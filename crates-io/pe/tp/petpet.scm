(define-module (crates-io pe tp petpet) #:use-module (crates-io))

(define-public crate-petpet-0.1.0 (c (n "petpet") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "10hr9f3aypf8cbv1wlagr2phvs0cy1yqpjh5khk620xn99zkdk55") (y #t)))

(define-public crate-petpet-0.2.0 (c (n "petpet") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "1dvzmm2klq29p7l7xrhksqxnszp7wfi7zr0il698glcgaldlhb7s") (y #t)))

(define-public crate-petpet-0.2.1 (c (n "petpet") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "0g3x4ylcr4ih1mnib39cxj7s0wy94a2ld6frqkwqvh4hx9ck35xf") (y #t)))

(define-public crate-petpet-0.2.2 (c (n "petpet") (v "0.2.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "1rahbm6qpr44x00yijh049jv0546fvymdrhq6x3kwl1rc2sqmr53") (y #t)))

(define-public crate-petpet-0.2.3 (c (n "petpet") (v "0.2.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)))) (h "1arw3dvsc43ksk1m2zpjyk3615d9fc0n3m60p2c7ydfrwdz2l05k") (y #t)))

(define-public crate-petpet-0.3.0 (c (n "petpet") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1q3gz93vli12avhsfi7ivximzakhq3i0c5qvgjmip0njj0xgk25c")))

(define-public crate-petpet-0.5.0 (c (n "petpet") (v "0.5.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "14c18fkss08qcy0r54xzbhkvn6q9v3jmfr3xqkq31afp5i7xwssf")))

(define-public crate-petpet-0.6.0 (c (n "petpet") (v "0.6.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1b0yqhdyzm9na5r8zpjr5ri9wz5jxvcjrbpa9c5bm2vnk2nabjsr")))

(define-public crate-petpet-0.7.1 (c (n "petpet") (v "0.7.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "14688wh7szhky3l4kv03bdvjzra71sk22nxmwplpif15z7y5h8nw")))

(define-public crate-petpet-2.1.1 (c (n "petpet") (v "2.1.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "0imhzh8pzy2dgg7l7kg5g5z88950krd58ahgy2f2n3ncwd7h1am8")))

(define-public crate-petpet-2.2.0 (c (n "petpet") (v "2.2.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "1ng0wh14855jqxbm6rqlr8rwwqam8y2pwp7zqha9brnyf3h5dgvg")))

(define-public crate-petpet-2.2.1 (c (n "petpet") (v "2.2.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "004iv2s59bkwivfalwk9nbf1jszngnhq0r35p9yrkp0fyxqm677k")))

(define-public crate-petpet-2.3.1 (c (n "petpet") (v "2.3.1") (d (list (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)))) (h "0i6f251gfkw00lbxpx8k9ajjqi4dl2pzhlxjxbyidp826pjkbv2m") (f (quote (("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands")))) (s 2) (e (quote (("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

(define-public crate-petpet-2.3.2 (c (n "petpet") (v "2.3.2") (d (list (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)))) (h "09p610pr28knsz36rv0h4q4623prl7k4jdl7aasycbsg4j3d9s6g") (f (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands")))) (s 2) (e (quote (("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

(define-public crate-petpet-2.4.0 (c (n "petpet") (v "2.4.0") (d (list (d (n "apng") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)) (d (n "png") (r "^0.17.7") (o #t) (d #t) (k 0)))) (h "1p944ndmnlx5ln6kr3rq649b1n04ski5zxaflq6k7d1phxawkjsa") (f (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (s 2) (e (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

(define-public crate-petpet-2.4.1 (c (n "petpet") (v "2.4.1") (d (list (d (n "apng") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)) (d (n "png") (r "^0.17.7") (o #t) (d #t) (k 0)))) (h "10smhscvka57jp2wrln8bcshjp9n3h315bkmldy6qp9rw918nsbw") (f (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (s 2) (e (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

(define-public crate-petpet-2.4.2 (c (n "petpet") (v "2.4.2") (d (list (d (n "apng") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)) (d (n "png") (r "^0.17.7") (o #t) (d #t) (k 0)))) (h "01gsf7m5anpkmy6qcaq7fvw46aj1zs489v0mmr9a12jh7ws0dmbc") (f (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (s 2) (e (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

(define-public crate-petpet-2.4.3 (c (n "petpet") (v "2.4.3") (d (list (d (n "apng") (r "^0.3.4") (f (quote ("png"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (k 0)) (d (n "image") (r "^0.24.0") (o #t) (k 1)))) (h "097r8j4ivdaa9g7hl746331k0avn4dpa75hfg4m8m02h3871f8wp") (f (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (s 2) (e (quote (("encode_to_apng" "dep:apng") ("bundle_raw_hands" "dep:image" "image/webp")))) (r "1.70")))

