(define-module (crates-io pe n- pen-ffi-macro) #:use-module (crates-io))

(define-public crate-pen-ffi-macro-0.1.0 (c (n "pen-ffi-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v219gq4s5lmf3d7pgvpfiqkbfwnvwzz6k9v79xjqfmkvd3kcgq6")))

(define-public crate-pen-ffi-macro-0.1.1 (c (n "pen-ffi-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8mfv51wxxcayq89npfq571dldl4jx7qc1dy7qg8yp81g3s8nxp")))

(define-public crate-pen-ffi-macro-0.1.2 (c (n "pen-ffi-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fv01spp9k1ljl3328z44glnmn9x0hk29kanx2859q27d4nvnv3m")))

(define-public crate-pen-ffi-macro-0.1.3 (c (n "pen-ffi-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12rrcp7216lzzcsgccxr74hlqqlwvfr12awkfvjyqs2gxg9vz2qc")))

(define-public crate-pen-ffi-macro-0.1.4 (c (n "pen-ffi-macro") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrgpc9bd9ryhy3czyndm5igra3clb057qq0iy9ycs2zmwy6p9fn")))

(define-public crate-pen-ffi-macro-0.1.5 (c (n "pen-ffi-macro") (v "0.1.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14dl7jli0d2xpbz2b2v0vd51l7xw7mk89ylsvzyvpr7zl89vjp50")))

(define-public crate-pen-ffi-macro-0.1.6 (c (n "pen-ffi-macro") (v "0.1.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06mrs163nl0cj8qq82irlr2qc1sab334g9w1ihhl8s26xi68qg21")))

(define-public crate-pen-ffi-macro-0.1.7 (c (n "pen-ffi-macro") (v "0.1.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wym9wvysjrlvm6gx6vbxxbvnbjjdla90q9nkqv8mldfhsdzz5fy")))

(define-public crate-pen-ffi-macro-0.1.8 (c (n "pen-ffi-macro") (v "0.1.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lzcfw5wlqhn31wl22r0849vfzc79rfn4vl9hkblnrx9vdhhk1im")))

(define-public crate-pen-ffi-macro-0.1.9 (c (n "pen-ffi-macro") (v "0.1.9") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rx4bbh2pg1295kh2pwzm9mdgn11zjkiqjz47k8997wxbcl03cds")))

(define-public crate-pen-ffi-macro-0.1.10 (c (n "pen-ffi-macro") (v "0.1.10") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z25fdg9r4b1mxj2rjsznxa7knvky5w6nrk9q673q6vr9jbhx6dw")))

(define-public crate-pen-ffi-macro-0.1.11 (c (n "pen-ffi-macro") (v "0.1.11") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l88gaa1g2kv8kgvx5m8cy28r10hfpafq8c34qk6wqka1dczg0bx")))

(define-public crate-pen-ffi-macro-0.1.12 (c (n "pen-ffi-macro") (v "0.1.12") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgz6q1fmywgc6fqbnhpwaw9x7b677pf0j1dkas23bg0sk2hc7sb")))

(define-public crate-pen-ffi-macro-0.1.13 (c (n "pen-ffi-macro") (v "0.1.13") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1af7a00hcq86cklw0ind3sdh0znqc4si8vk160zqvqn8cnffcnf5")))

(define-public crate-pen-ffi-macro-0.1.14 (c (n "pen-ffi-macro") (v "0.1.14") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vyw78ala0kf5cxfrjqxmrydnsfvcsz3mhmb4m9hmpcvxqk6s2iz")))

(define-public crate-pen-ffi-macro-0.1.15 (c (n "pen-ffi-macro") (v "0.1.15") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "181c23gmxlav7bpyas593pr3zsqh7ln3lbg528s4ghva6afg11q2")))

(define-public crate-pen-ffi-macro-0.1.16 (c (n "pen-ffi-macro") (v "0.1.16") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08dac5qd4q0fgv2mxvrc7cbn6xw06cqv22hns726j4nc1dszlfiz")))

(define-public crate-pen-ffi-macro-0.1.17 (c (n "pen-ffi-macro") (v "0.1.17") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pklk0igbfkvdg1ykchi6yixa3kxy3yv8lsr86xnggjq8jbbw019")))

(define-public crate-pen-ffi-macro-0.1.18 (c (n "pen-ffi-macro") (v "0.1.18") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w9rx98yx635kqrblnlp61l4nds68ck3ka5m318lvsjxkzdi2sn1")))

(define-public crate-pen-ffi-macro-0.1.19 (c (n "pen-ffi-macro") (v "0.1.19") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a66kqcah82c67wjsdpawz3kbnkbx7ribyvg0g74c1nl91ln4lhr")))

(define-public crate-pen-ffi-macro-0.1.20 (c (n "pen-ffi-macro") (v "0.1.20") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fn7cf3ch7p1vvk86bdkvi7jn8glz251dxjk6lyx3jhdrv2rza5s")))

(define-public crate-pen-ffi-macro-0.1.21 (c (n "pen-ffi-macro") (v "0.1.21") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i2xs6r1fbsh8ym2793z2ddmxhmsbciv715j2k1qa8zi11hkwy11")))

(define-public crate-pen-ffi-macro-0.1.22 (c (n "pen-ffi-macro") (v "0.1.22") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h14f9zdwzg9ci8ahzsfbpilh328bfqal6xz7kpp4qlajz33v066")))

(define-public crate-pen-ffi-macro-0.1.23 (c (n "pen-ffi-macro") (v "0.1.23") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d87ks3dm0v2zlvhlghjp3cg42v2cgn7h3qdjzfbk2smrq9d0mg5")))

(define-public crate-pen-ffi-macro-0.1.24 (c (n "pen-ffi-macro") (v "0.1.24") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvxmgg4yz6nxnw9wis59ma845drghn319apc5li9c18xskyvd2f")))

(define-public crate-pen-ffi-macro-0.1.25 (c (n "pen-ffi-macro") (v "0.1.25") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00nw7i1abcaqgz179748vhlfh22kyd4vbypf1hqn1lhahf7fmnii")))

(define-public crate-pen-ffi-macro-0.1.26 (c (n "pen-ffi-macro") (v "0.1.26") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16v6ar07criffhfkla7yxynmdngydad220pkq6k0ab20jabkq9y6")))

(define-public crate-pen-ffi-macro-0.1.27 (c (n "pen-ffi-macro") (v "0.1.27") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8jvxiv4zpim60cl9aq6b4y1hycna33f5q6i9q3gydmaybn17jp")))

(define-public crate-pen-ffi-macro-0.1.28 (c (n "pen-ffi-macro") (v "0.1.28") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvpk76ac16i96a34zdrpqz17n324hjvm62ylgggv1mj1f7wbw4p")))

(define-public crate-pen-ffi-macro-0.1.29 (c (n "pen-ffi-macro") (v "0.1.29") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nncp5xjkdpr3kbw2frfkjif4gjgg3hlnfd3s2izqhcvbgkkzd9g")))

(define-public crate-pen-ffi-macro-0.1.30 (c (n "pen-ffi-macro") (v "0.1.30") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ga3m6rhym2ymahri8wdx8r1v2shg2g53wnwgqrsadwas7hyf4iy")))

(define-public crate-pen-ffi-macro-0.2.0 (c (n "pen-ffi-macro") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lkjrkidyrh8s1fphsb7kvc1jh0fz6686dzb7q4lwhinri2x1aby")))

(define-public crate-pen-ffi-macro-0.2.1 (c (n "pen-ffi-macro") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q5kfgmi2v0jidi1jvhzyi4ifdjadh40h4hipgvf1d37mbq7d8hk")))

(define-public crate-pen-ffi-macro-0.2.2 (c (n "pen-ffi-macro") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "069w0587idqaby7iih7cqjsp2b5vgchlgsdag8py4569rijs2izv")))

(define-public crate-pen-ffi-macro-0.2.3 (c (n "pen-ffi-macro") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dfzaq23rk9hc77y8hagyv1cglkkqlqkrvcp1y2wwh63089iglzg")))

(define-public crate-pen-ffi-macro-0.3.0 (c (n "pen-ffi-macro") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0khi6950p0qk00559yn6blzg5ijwsax5h8ig3njpi13bwaal2sd2")))

(define-public crate-pen-ffi-macro-0.4.0 (c (n "pen-ffi-macro") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jda9wbzjnzyvj94c70nbvi4mlnj1yq7jx7v9j23b9fc3qbpm1rx")))

(define-public crate-pen-ffi-macro-0.4.1 (c (n "pen-ffi-macro") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "058wrnaqii3vf045g1nvg5g2m94ncxh3qarnlmr9mfz5p1n34c3c")))

(define-public crate-pen-ffi-macro-0.5.0 (c (n "pen-ffi-macro") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w29rb04xf2yhr97qhfynhs0qxg90kcylinpmgks849wyc4i1aq7")))

