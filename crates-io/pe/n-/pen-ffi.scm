(define-module (crates-io pe n- pen-ffi) #:use-module (crates-io))

(define-public crate-pen-ffi-0.1.0 (c (n "pen-ffi") (v "0.1.0") (h "01xf1kpjpn6nfhn8pfinnbf0nha04q6gckqrrrmbgkgwwgg2d9b0")))

(define-public crate-pen-ffi-0.1.1 (c (n "pen-ffi") (v "0.1.1") (h "0cyn3y6r8i6sx5w8z5apm8gd73i500kild9vbbzz19rhs7q0p0jq")))

(define-public crate-pen-ffi-0.1.2 (c (n "pen-ffi") (v "0.1.2") (h "07b3ly2r7zllkz67dk3hzssdasn6pd25zlc9686jazv8i2fj7c98")))

(define-public crate-pen-ffi-0.1.3 (c (n "pen-ffi") (v "0.1.3") (h "1iy9vigvfj7klkqn4kgih54cw9dzrpbdhsnivq18hbr7v8vgsnsq")))

(define-public crate-pen-ffi-0.1.4 (c (n "pen-ffi") (v "0.1.4") (h "14k6hfyzcn5gsiigji7shbrk25kayg0y9yn5bxia4011qcxqm67n")))

(define-public crate-pen-ffi-0.1.5 (c (n "pen-ffi") (v "0.1.5") (h "1rkk4cd5z53k42yiclmmqcqzl0j0smi0azdarr94qxrkkcr75xvd")))

(define-public crate-pen-ffi-0.1.6 (c (n "pen-ffi") (v "0.1.6") (h "1yn6zd5vyhgq0q5fh3x58i70jcd4szmw7115ccj6qmk85a8bxxk4")))

(define-public crate-pen-ffi-0.1.7 (c (n "pen-ffi") (v "0.1.7") (h "13m5i09x2sq09p18i2zhhmsvc29dsrkbkh68s7aj9sfyc732289m")))

(define-public crate-pen-ffi-0.1.8 (c (n "pen-ffi") (v "0.1.8") (h "0h9kkvax5xj90a9rp9labfq5150bq04i65ycrsfjc8bdhr9cjwis")))

(define-public crate-pen-ffi-0.1.9 (c (n "pen-ffi") (v "0.1.9") (h "0s7kggfb3aarjcznsirqm3ikpkvja7bdh40f43dxklmpvs4qj2g9")))

(define-public crate-pen-ffi-0.1.10 (c (n "pen-ffi") (v "0.1.10") (h "0yrqbl2npl4kl2hj83z9s65kjxmbkpj2dy2pyc9l1vaf8c6196fq")))

(define-public crate-pen-ffi-0.2.0 (c (n "pen-ffi") (v "0.2.0") (h "0qbww5axj6v5wc80ghwahzkn7y44xa44mq9109xapqjda6n9z7j3")))

(define-public crate-pen-ffi-0.2.1 (c (n "pen-ffi") (v "0.2.1") (h "18pqf6jaabccmwskqkcbjvgha6vbql4pxdry3ik005hpjsid18za")))

(define-public crate-pen-ffi-0.2.2 (c (n "pen-ffi") (v "0.2.2") (h "0kwhh83iv4rmbhkb1dk9abm2iva7rxh4449w1r6jlbnakm9ps7mj")))

(define-public crate-pen-ffi-0.2.3 (c (n "pen-ffi") (v "0.2.3") (h "1zmi5dzmfa5jx9hf9dkwwbqivizfbhdh76f6ic4lxx4k7a2b7dg8")))

(define-public crate-pen-ffi-0.2.4 (c (n "pen-ffi") (v "0.2.4") (h "0qrgki3h4ri39ybc34rjl3dqwmghp17lxxw53pzrhci2vrgjf3hm")))

(define-public crate-pen-ffi-0.2.6 (c (n "pen-ffi") (v "0.2.6") (h "0wjgm6szb9ahbwjvwy3rxzslyx1h1i2z5vv2cipdw4izbf4fi58j")))

(define-public crate-pen-ffi-0.2.7 (c (n "pen-ffi") (v "0.2.7") (h "1a9509s91zg6xg3dw0df31q6w0y6y7cz316shxr77byhq2qbgs0k")))

(define-public crate-pen-ffi-0.2.8 (c (n "pen-ffi") (v "0.2.8") (h "0r23v6rvlrrkyfgbha3lcc3h3jvd63n6wl4gd804rzaldw3pk9nm")))

(define-public crate-pen-ffi-0.2.9 (c (n "pen-ffi") (v "0.2.9") (h "12xhjkqiznq6z3x0b242622m55xswmwkgqaz1lvicx75m87xjalb")))

(define-public crate-pen-ffi-0.2.10 (c (n "pen-ffi") (v "0.2.10") (h "19j699h8n6ig9ha9mlyy5j5ck6v6c00h9hf2k7k8ggpx8sv4yjbg")))

(define-public crate-pen-ffi-0.3.0-beta.0 (c (n "pen-ffi") (v "0.3.0-beta.0") (h "1qxrvvgvl40glh2z4vw0k4q6lfhyakrcw3v9qi3s6mm7jwcpm9ws")))

(define-public crate-pen-ffi-0.3.0 (c (n "pen-ffi") (v "0.3.0") (h "1pg0ii3zaqb9s5fmm6lmh1lvf2k3as5wp8818vqbkg3cmvckb0q5")))

(define-public crate-pen-ffi-0.3.1 (c (n "pen-ffi") (v "0.3.1") (h "06m2yxl66diyr4p6qzxdli767nas330vixgyixb11x185f9b1r48")))

(define-public crate-pen-ffi-0.3.2 (c (n "pen-ffi") (v "0.3.2") (h "167cpbpa20zj61jkqj7z7fjy8qsmilbf0wbahgr18i3rzx081sij")))

(define-public crate-pen-ffi-0.3.3 (c (n "pen-ffi") (v "0.3.3") (h "11qfz766gzdrzvn8x0r98qy58f643dy12f4136r3rsxx8xps5cp7")))

(define-public crate-pen-ffi-0.3.4 (c (n "pen-ffi") (v "0.3.4") (h "08aja9pw5w2h3x1dnjy6wwkxs7277cmncym9ywc2xgamgml0ng9x")))

(define-public crate-pen-ffi-0.3.5 (c (n "pen-ffi") (v "0.3.5") (h "1f3hl7iib54mpwph31cwsm7ixzf8kq2d55207cifx8ik2a02hzl6")))

(define-public crate-pen-ffi-0.3.6 (c (n "pen-ffi") (v "0.3.6") (h "1n29v963dirf7y1i83ixg3yvwjqg69dy2zzb2yiwjkchn5yiwypn")))

(define-public crate-pen-ffi-0.3.7 (c (n "pen-ffi") (v "0.3.7") (h "1s3fk37j0fabyrg4kdms2zqyy0cvh8gps9h2h46vhqhfw54a5cc1")))

(define-public crate-pen-ffi-0.3.8 (c (n "pen-ffi") (v "0.3.8") (h "1w6j9ykayfvz0w2w3cn5bfl68zgzg34xy3mavyg4pnlaidxsngm8")))

(define-public crate-pen-ffi-0.3.9 (c (n "pen-ffi") (v "0.3.9") (h "15fyxal2r4wrk0q4jjd58cic3q4mima8mq7dpa60zcljvhnhjdyp")))

(define-public crate-pen-ffi-0.3.10 (c (n "pen-ffi") (v "0.3.10") (h "0d1cwwprzzi73bvshdqxndlg8g8lnbbsfrnsmy9qsd5dyyzqyba2")))

(define-public crate-pen-ffi-0.3.11 (c (n "pen-ffi") (v "0.3.11") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0s6mr58z5w5pqd3c6kpji2ih46pzc84l5rilafddlnrdzylvrkjs")))

(define-public crate-pen-ffi-0.3.12 (c (n "pen-ffi") (v "0.3.12") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1gfi9gqp04kk8cys2mv84f5xn7bb2q1q6c2djg1ypqixlmawgjzq")))

(define-public crate-pen-ffi-0.3.13 (c (n "pen-ffi") (v "0.3.13") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0iq7qjzgdf7njb11qjq4gsphw23wy6rzq93ycdbxks56njisccpq")))

(define-public crate-pen-ffi-0.3.14 (c (n "pen-ffi") (v "0.3.14") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cv34xnj2gdv0sqqs2vb3dgp7wkzpf5swpwgfq9ma9l0yj20jj6h")))

(define-public crate-pen-ffi-0.3.15 (c (n "pen-ffi") (v "0.3.15") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "06xhlvqcbmkl09avjvxin7w46b3fv3h8wpa6d40wdpaax898sb1q")))

(define-public crate-pen-ffi-0.3.16 (c (n "pen-ffi") (v "0.3.16") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kdwc474w2jg7k95qq7bi7nxib0m220krd97rx9xml0bw5pxyw87")))

(define-public crate-pen-ffi-0.3.17 (c (n "pen-ffi") (v "0.3.17") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "13jnk3dl19fwvikpc0qgk6ngby9qc4xlsx0i7fr31f9qizc93w0b")))

(define-public crate-pen-ffi-0.3.18 (c (n "pen-ffi") (v "0.3.18") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "093zxf9gkfi4ddx7azrql5jd1r9y937a08zqf859hdizl642jcsw")))

(define-public crate-pen-ffi-0.3.19 (c (n "pen-ffi") (v "0.3.19") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xjplmz6p9r228i1a3xqnqr65kl820kmppc09aisdlca9b3lfzxy")))

(define-public crate-pen-ffi-0.3.20 (c (n "pen-ffi") (v "0.3.20") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1naa86iihv9977nzkqiq4cs3i8yid6j1w2jaa4yvrrx6q136sybm")))

(define-public crate-pen-ffi-0.3.21 (c (n "pen-ffi") (v "0.3.21") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hh8mz4jdsf36hmkm4r6b5y8wjz6sdvijkagakzg54zhg5ksmr8l")))

(define-public crate-pen-ffi-0.3.22 (c (n "pen-ffi") (v "0.3.22") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1w93dc58lch0ans3rfkzvasldmcnh0mlyadj7843rc7j3mivwdvy")))

(define-public crate-pen-ffi-0.3.23 (c (n "pen-ffi") (v "0.3.23") (d (list (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0qgvngc5lqgd25sizr4cx1pjrsvibkv0rr1c67zvy6sma1prx3ih")))

(define-public crate-pen-ffi-0.3.24 (c (n "pen-ffi") (v "0.3.24") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "03plml53hvjg9qpyqdwmxvp5zc7yllnca1jygpxz61nnrsxplfi6")))

(define-public crate-pen-ffi-0.3.25 (c (n "pen-ffi") (v "0.3.25") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "04xxcxs6gvcg2sk2jp9n8fdrnzllwxr0xb3ir09y6jpqv2id92l7")))

(define-public crate-pen-ffi-0.3.26 (c (n "pen-ffi") (v "0.3.26") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1rcq6yxskv534avgzbw7r2jggfv2i41w6rqkpzhv59hkmy0crcyl")))

(define-public crate-pen-ffi-0.3.27 (c (n "pen-ffi") (v "0.3.27") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "12pw61rnb33zndpgnhja58n54wm2br9y2rm5dl57qizcsy8r1lv1")))

(define-public crate-pen-ffi-0.3.28 (c (n "pen-ffi") (v "0.3.28") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1bpg3jymmjv9zb2rsksbpy9kxqjpny7fb27nb5rgdr61ng7ri8vq")))

(define-public crate-pen-ffi-0.3.29 (c (n "pen-ffi") (v "0.3.29") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1kcniy8y2dfnxw9mv07c74ppqn4a2j0f8lp9zdakss0g3l351063")))

(define-public crate-pen-ffi-0.3.30 (c (n "pen-ffi") (v "0.3.30") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "04bf93s3qgwdxyqifyscxy15py197wlvdgf867h51dqbalil8pl0")))

(define-public crate-pen-ffi-0.3.31 (c (n "pen-ffi") (v "0.3.31") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0qz701r2ppp659iq2yb8p6xkj3vd9dj8a5abizvprac0k3b563vj")))

(define-public crate-pen-ffi-0.3.32 (c (n "pen-ffi") (v "0.3.32") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0c32vnwmnm7pzvv6q3kr1v2kw95a5gznwil32sszknzr43in15pc")))

(define-public crate-pen-ffi-0.3.33 (c (n "pen-ffi") (v "0.3.33") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0i6y5gn3qmj7dd4c6v7ji6w6vzs79ys4b70axsqki140qn01vki8")))

(define-public crate-pen-ffi-0.3.34 (c (n "pen-ffi") (v "0.3.34") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r9jyxrzk9kd73gcl92iin350gvc3f055cmw12clgjxsjwbl0q4z")))

(define-public crate-pen-ffi-0.3.35 (c (n "pen-ffi") (v "0.3.35") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0a93phfywa9yzr4fsdlpmhdxkpa5rd243b154laam3awz801crnl")))

(define-public crate-pen-ffi-0.3.36 (c (n "pen-ffi") (v "0.3.36") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jpdif34krgfb6p17g4pn2hxfhddmyzr8hpr2kf6mwcdq27v3bwf")))

(define-public crate-pen-ffi-0.3.37 (c (n "pen-ffi") (v "0.3.37") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "06w9sxjjjhm4zga8ghhaid644181lb0mki307aiwlwpmf7zdpq8c")))

(define-public crate-pen-ffi-0.3.38 (c (n "pen-ffi") (v "0.3.38") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1pxp3xns3hpblpllkmkq7d58h9jddr2c1flqhzchdrdvzplyqjzs")))

(define-public crate-pen-ffi-0.3.39 (c (n "pen-ffi") (v "0.3.39") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "174lfrz1vw6cw4vzcxmp9a1m3cwanddx0zqb2qmrk8y6sh1dhqka")))

(define-public crate-pen-ffi-0.3.40 (c (n "pen-ffi") (v "0.3.40") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1gk1pswcg78wkfp7w81c97p38vva7jbb3310xcxfjkn8p7954201")))

(define-public crate-pen-ffi-0.3.41 (c (n "pen-ffi") (v "0.3.41") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "076x7aiy0smz4x65sp2dnv9qza8f5cd39qk0f4d3h7c0084njhx9")))

(define-public crate-pen-ffi-0.3.42 (c (n "pen-ffi") (v "0.3.42") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1a70z9aycjl1pypy9sjlmfbf6sa7ad4shr6frqi6pvmdaxil4mzk")))

(define-public crate-pen-ffi-0.3.43 (c (n "pen-ffi") (v "0.3.43") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mc2r55k29jswjyifb6jfsdhqp456aksplc33rbi5ghbx8xg7mgk")))

(define-public crate-pen-ffi-0.3.44 (c (n "pen-ffi") (v "0.3.44") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1m7wmqy0916c0jfxfchgvxli3a4zn9z014y6yfar7hknc65ssn45")))

(define-public crate-pen-ffi-0.3.45 (c (n "pen-ffi") (v "0.3.45") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "14rbp9bhg6sc46fqa0ls3f17gkwg01pvafwswhi3mkamm64jl36b")))

(define-public crate-pen-ffi-0.3.46 (c (n "pen-ffi") (v "0.3.46") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "08fbi66i6wjj97s0jbqhjx0lcsa7l0lp4z85xbqbnca42rnz0j14")))

(define-public crate-pen-ffi-0.3.47 (c (n "pen-ffi") (v "0.3.47") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1z8iha75g6m123m8qryw55dr5fszaywh1qmpggr6fss6rv1vwfia")))

(define-public crate-pen-ffi-0.3.48 (c (n "pen-ffi") (v "0.3.48") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q6bv2mfdrs9nab22108l69jwb4cf6ilvf2cahah9zncaxglr8yn")))

(define-public crate-pen-ffi-0.3.49 (c (n "pen-ffi") (v "0.3.49") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "02anjjgfzsm6p52170yl8zrcxyiikgr2naszfaasc4k45ws5ksf2")))

(define-public crate-pen-ffi-0.3.50 (c (n "pen-ffi") (v "0.3.50") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cv94xbssvbawa9fz6v9lynkxsyzrff71ykryd9xy9968zbmbvdp")))

(define-public crate-pen-ffi-0.3.51 (c (n "pen-ffi") (v "0.3.51") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cy09qcskymn35l77fp4ivgc48gv031py168g9aa3d4xf7v5bypf")))

(define-public crate-pen-ffi-0.3.52 (c (n "pen-ffi") (v "0.3.52") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0j4iqgbzj61yqymm7jr1a12sjxldmjzf6901xv3qjdp4c017kfp4")))

(define-public crate-pen-ffi-0.3.53 (c (n "pen-ffi") (v "0.3.53") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ab75scfb4m5yyyp8z9qrzzpk6yfici2a8p0qc4cddqmjc0bzxqm")))

(define-public crate-pen-ffi-0.3.54 (c (n "pen-ffi") (v "0.3.54") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "17ziji0gv5mcsyhc22qfkzzmkw2x5dqz9p5sb0ss2f52as57n21c")))

(define-public crate-pen-ffi-0.3.55 (c (n "pen-ffi") (v "0.3.55") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "00in6q748hqfyabpk3pk1blw5h4ffxk7rs0ny4ja70qnzsd0c27i")))

(define-public crate-pen-ffi-0.3.56 (c (n "pen-ffi") (v "0.3.56") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "051hrg99m3drzbilcgqwr64kwh3r2bjqsdz1kw5gfw2v5f9hxcvz")))

(define-public crate-pen-ffi-0.3.57 (c (n "pen-ffi") (v "0.3.57") (d (list (d (n "async-stream") (r "^0.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "155z5f0zx7ddvnsmsjghcydla13qvqr1479kwci22kpbarn3y4ln")))

(define-public crate-pen-ffi-0.3.58 (c (n "pen-ffi") (v "0.3.58") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0fshqzy0nkmww1wipk1jy9w345v896j07cawx3qnd0adbxy9rpd3")))

(define-public crate-pen-ffi-0.3.59 (c (n "pen-ffi") (v "0.3.59") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0g3v1vq4mrr3ljxcxkzv58x96g7x6nb51bc6jalv7c16zzpsyqlq")))

(define-public crate-pen-ffi-0.3.60 (c (n "pen-ffi") (v "0.3.60") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1izib18nlgjp5i7gg26mkl33h8y0hjx7z4ls3k14ss8nrdpqmf2x")))

(define-public crate-pen-ffi-0.3.61 (c (n "pen-ffi") (v "0.3.61") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1skarcsv2y4hh1s38glc5m68v50y91x03rg7bmpg89gp1kaamf7l") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.62 (c (n "pen-ffi") (v "0.3.62") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0gdxnjracsgpyv11ac3yj4zilgm30h3b3in0nv5viqb4sjklxmn8") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.63 (c (n "pen-ffi") (v "0.3.63") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1q81ixrr4wkyxk1qdc046ifvbylfqnw9fmx1bmaarg6x6f8aym8v") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.64 (c (n "pen-ffi") (v "0.3.64") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0dx6jb9m2biakx9czyry4r28qan1gp52hjmda77wq7z067lipakq") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.65 (c (n "pen-ffi") (v "0.3.65") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "193zg03qx52327b7ky583sh7dmrlr4hgv96f4kcna0nyka3b7xax") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.66 (c (n "pen-ffi") (v "0.3.66") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "09g921v37s03sx336sxcm6a3nzf9n71gf4yxzdp66izgjr0d514k") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.67 (c (n "pen-ffi") (v "0.3.67") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "185knmnjlhlzhakhhqrai6nz54jx4c472sq9j3j0c0gyi2sriwgb") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.68 (c (n "pen-ffi") (v "0.3.68") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1n3hi7xl9gh5vs0rrz9qv3zcy4fxcm4yjagf7ljn29850ll0vkrn") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.69 (c (n "pen-ffi") (v "0.3.69") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1zrh8765xy2bka2agkmvmj9ffkbq8qq9dlj3pnmfnlvalcf0mszl") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.3.70 (c (n "pen-ffi") (v "0.3.70") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0h943cjkbf61bxlhw33k6bjfdip688zqbwk11dk9valr5ds6s099") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.4.0 (c (n "pen-ffi") (v "0.4.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1fv2la938pj490yjf4v9vvv064l45fgnib7qfy2f8flic60zmwh0") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.5.0 (c (n "pen-ffi") (v "0.5.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1p9yr90rnra4ci6migwbkjzwqg2zb9z26508i2jj8w6phwzfip9q") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.0 (c (n "pen-ffi") (v "0.6.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1kjrrggfr5d6nmgjvvny0fwr85hkm5cnzyrmifc3s6h87wrnvna7") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.1 (c (n "pen-ffi") (v "0.6.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0yjfmcb79v59194x3qpa13vh4psd2x6lxb2wgyqbn11d64bk1fdb") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.2 (c (n "pen-ffi") (v "0.6.2") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1lplzqrirf70s1xmahkjmx1j03022j9gp49n3p6m7qkd677iq8zj") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.3 (c (n "pen-ffi") (v "0.6.3") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1w6nvc5m4vg1a5smsnk4ixzsli64x1qkknmqqjhnik0w9fybdrb1") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.4 (c (n "pen-ffi") (v "0.6.4") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0jgjfmj6zxfnsxfzdwfbr4jnya2q8v037l2ffnycrbyzlhd50qp7") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.5 (c (n "pen-ffi") (v "0.6.5") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "11d68qvc4pqgf16xqr5ipg7y26294x1nvgpm0hbi2l275dpwgqdz") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.6 (c (n "pen-ffi") (v "0.6.6") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10h0bs9mcjn6z0ajwrnr8gnjqjags98mwkzc9qr7qzbvqdpn72ly") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.6.7 (c (n "pen-ffi") (v "0.6.7") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1nslszf8vfd8dhgvpina76w6gr4px30fvn9pvpsmihf3bpl9wbdp") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.7.0 (c (n "pen-ffi") (v "0.7.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "17azl8x386mdslcmqyxqc2ssi55jy9f1vmp72404baszimb618rf") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.7.1 (c (n "pen-ffi") (v "0.7.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0vx6s7fgwqag50f4wybprhmcx7gvvacxvz9f6r9w0fb594a76lxn") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.7.2 (c (n "pen-ffi") (v "0.7.2") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1x6ajk818ziy5cfnr8h1cpjz1h1c9rqa6jm5alwdgv51n0bvjfcn") (f (quote (("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.7.3 (c (n "pen-ffi") (v "0.7.3") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "03r53fx4h836j00px4sdhjk5a00glgmnq26a2xsq0nirqsl0wjac") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.0 (c (n "pen-ffi") (v "0.8.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0v7a27fncvfybhl6y7rwb7n2qgbmd0n5nzs2a9c6yxihk0n88hvk") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.1 (c (n "pen-ffi") (v "0.8.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ilcv0rliry0rn4lnsgisq2sbvmg9xmydr6ixskz1ngwa8lp2dc8") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.2 (c (n "pen-ffi") (v "0.8.2") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "09kinmm6kx4v6v5x1nf5kkigxq2j5r05lzcy6cc4dp5l7hqjk3ry") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.3 (c (n "pen-ffi") (v "0.8.3") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12nsi3jsy7lb1pzhvcb8lr8k75bscwjqim2jgnjs8bizjmkqydbz") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.4 (c (n "pen-ffi") (v "0.8.4") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1rrnyj4gabva1jdi2wnhifv107my7qkdhivj48xc3b7ycrw4dx58") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.5 (c (n "pen-ffi") (v "0.8.5") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ghb6nnn0yy700falbrqjfaw3yj5k2bkdl5pfa4whv1xh50dyijh") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.6 (c (n "pen-ffi") (v "0.8.6") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0nmyx6lclbyy9k9nw7krzxr6xy9m86zl4pam144zhidk1yfk8qk6") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.7 (c (n "pen-ffi") (v "0.8.7") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10v2qy2smq7k93k9pz16nplgpjcywfzd6rjfazd9hydib8p2gw75") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.8.8 (c (n "pen-ffi") (v "0.8.8") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "07856s0svgl58jh8pwhrrr1w0xxhfyargrdvcspaj90q9b60wxnr") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.9.0 (c (n "pen-ffi") (v "0.9.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0sd4krpyfsw84f7p86l0vhrfsn9hd5gqsyd33pmg7gxlf8y3il61") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.9.1 (c (n "pen-ffi") (v "0.9.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0a9rhc7xjqbc3m8zlg5m8mxh4flizgaf09mnx27v07ryp472i7d2") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.9.2 (c (n "pen-ffi") (v "0.9.2") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "00cdpzk44nnv8rk5dv1lf1jnddwvizxqws7syx2bzj6dqdnsiq4g") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.0 (c (n "pen-ffi") (v "0.10.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0cb93cxvvhywwcl3sa65m7zdrf308x36k0j2nahjyyh973c8y3hy") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.1 (c (n "pen-ffi") (v "0.10.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0v242anplawjfslr5mv7jlinsyphxk2zdhfa18znf8x36qwnyn69") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.2 (c (n "pen-ffi") (v "0.10.2") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1hcsqv9a1b764z5qc52khz3y9gabpldrg53b3g6prfavzf1n6qfa") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.3 (c (n "pen-ffi") (v "0.10.3") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1w4kjjbnzwm8n1g2f3lwar20zd2k6yfw4iwi33qipga07s777zhc") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.4 (c (n "pen-ffi") (v "0.10.4") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0fkiv0g5bxilkxnzkm807ybl2fqmnpcj5gdchj5v16kn9lcd1sn2") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.6 (c (n "pen-ffi") (v "0.10.6") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "012gkpmmm4wgjywi4kq19zf8bmcaxcgpkabm82vfzxn1kk8r0yl7") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.7 (c (n "pen-ffi") (v "0.10.7") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0gzzd2shzaxg6ca19w36ynb5ks8cripr7lmybqfvblb2986dhc30") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.8 (c (n "pen-ffi") (v "0.10.8") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0br66b7wwbrbsd5z0z5kzxn90a1i4k9m4wb70xh1ihl7d4ynnv31") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.10 (c (n "pen-ffi") (v "0.10.10") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "18ha5k77vjjl73lvxf2wsxnsxb2nkv31fw89hdfsb03cr4p80pbd") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.11 (c (n "pen-ffi") (v "0.10.11") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "02i7d15272w3j5x8abkka2cchg2hsdm5b7nvxjqm38ic4zz3mmcc") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.12 (c (n "pen-ffi") (v "0.10.12") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1n5b958ywg823fi4xfxb2969kdibjgwbm3w53p2r0sgqcc8bm6wq") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.13 (c (n "pen-ffi") (v "0.10.13") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "11mjvrhh1hk3qyzvslk3ryf49rx9ad9wn7vrg90g2xvirfrj192a") (f (quote (("std") ("runtime" "tokio" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.14 (c (n "pen-ffi") (v "0.10.14") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0hz7w52a0pzwsm226mzg15hvsj7ix0ly714zm8hvs0qvlqfbw3f7") (f (quote (("std") ("runtime" "tokio" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.15 (c (n "pen-ffi") (v "0.10.15") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0samg52fv45sigcyr53254jpjapi5rbw79dfkz4a3kwr7id0zzvy") (f (quote (("std") ("runtime" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.17 (c (n "pen-ffi") (v "0.10.17") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1h9ykalr83izkyvdw62zl7lkdh2k2vcybxg2phzl3f5fgl2favxs") (f (quote (("std") ("runtime" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.18 (c (n "pen-ffi") (v "0.10.18") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "09fapzmw9hswys0jyi3a2090biz1nx8lyxcw59xxqf25if6zkigi") (f (quote (("std") ("runtime" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.19 (c (n "pen-ffi") (v "0.10.19") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0h777wcr67ffvnrfjhvz5r26yb8waqa8vcydxa8ql81n2i2zm9xv") (f (quote (("std") ("runtime" "std" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.20 (c (n "pen-ffi") (v "0.10.20") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "06ypq2757pl4hbdmifgvzbfbh0y0n707vhw0a2y31fgk8jlv8wmw") (f (quote (("std") ("runtime" "std" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.21 (c (n "pen-ffi") (v "0.10.21") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "035hnlr63xaqi3ln9pay5yv6dixjgc3p6nl6wci6awr1644sk1vn") (f (quote (("std") ("runtime" "std" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.22 (c (n "pen-ffi") (v "0.10.22") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ig8wlgm1ah3ngys4rsxpjbqa51zv2nrns81armrbqdj0nppib08") (f (quote (("std") ("runtime" "std" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

(define-public crate-pen-ffi-0.10.23 (c (n "pen-ffi") (v "0.10.23") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pen-ffi-macro") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-macros") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "076g8cpc1zmy5184y5mzl062s4kydy4iyfmrzdnmbdap8nq126qa") (f (quote (("std") ("runtime" "std" "tokio" "tokio-macros" "tokio-stream" "tokio-util"))))))

