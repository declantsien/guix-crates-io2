(define-module (crates-io pe ac peace-constants) #:use-module (crates-io))

(define-public crate-peace-constants-0.5.3 (c (n "peace-constants") (v "0.5.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0gz95664jlkm0pq4sbiw8rssxldbwb03b269xkrz8w62b3i7wjdx") (y #t)))

