(define-module (crates-io pe ac peace_data) #:use-module (crates-io))

(define-public crate-peace_data-0.0.2 (c (n "peace_data") (v "0.0.2") (d (list (d (n "fn_graph") (r "^0.5.2") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.2") (d #t) (k 0)))) (h "0l46gp5fmd5dq11i3j6frqdxhrh2sw6fqxl2d0qibwd3p411bvs6")))

(define-public crate-peace_data-0.0.3 (c (n "peace_data") (v "0.0.3") (d (list (d (n "fn_graph") (r "^0.5.4") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.3") (d #t) (k 0)))) (h "0psi2w0cwc8iwf5sq1fz6qimsjcib58yrhy7l067h99j0xypnilw")))

(define-public crate-peace_data-0.0.4 (c (n "peace_data") (v "0.0.4") (d (list (d (n "fn_graph") (r "^0.5.4") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.4") (d #t) (k 0)))) (h "1csyixvxdkh5ny4p4gcv1kzvf7x69ljmisi1k18kyds5ash3h0yv")))

(define-public crate-peace_data-0.0.5 (c (n "peace_data") (v "0.0.5") (d (list (d (n "fn_graph") (r "^0.5.4") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.5") (d #t) (k 0)))) (h "05vnizfk2n9c3haq63kwfaiqsazqqsqwh82a1bf7bzz6ldwms4il")))

(define-public crate-peace_data-0.0.6 (c (n "peace_data") (v "0.0.6") (d (list (d (n "fn_graph") (r "^0.8.1") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.6") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.6") (d #t) (k 0)))) (h "1x7gh7p65zcadi5wlrmcq2z0x955lvfbgsx0q2030ixh5vq70lsg")))

(define-public crate-peace_data-0.0.7 (c (n "peace_data") (v "0.0.7") (d (list (d (n "fn_graph") (r "^0.8.1") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.7") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.7") (d #t) (k 0)))) (h "1a9h52p88m9xy9v63zzc2s35ww82wy35j6vz8xqf93ims4m5c677")))

(define-public crate-peace_data-0.0.8 (c (n "peace_data") (v "0.0.8") (d (list (d (n "fn_graph") (r "^0.8.1") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.8") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hz64kadn220b4jb92bpb54m636va54mkzdvnzc4s3nmqj5w4cr5")))

(define-public crate-peace_data-0.0.9 (c (n "peace_data") (v "0.0.9") (d (list (d (n "fn_graph") (r "^0.8.2") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.9") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hikldb5y59fqzckaw96c5hgxpncnljp4j8a72jl052j72ai8il9")))

(define-public crate-peace_data-0.0.10 (c (n "peace_data") (v "0.0.10") (d (list (d (n "fn_graph") (r "^0.8.3") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.10") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zb3fbnh5bqzmdv4mzn5yk2318gbkrm948njki89wa05w59n6f28")))

(define-public crate-peace_data-0.0.11 (c (n "peace_data") (v "0.0.11") (d (list (d (n "fn_graph") (r "^0.8.3") (f (quote ("resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.11") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "03y9sva4ss1633flhd2x6h7bm9b1zavxdprnpxqh2v7syqv028m4")))

(define-public crate-peace_data-0.0.12 (c (n "peace_data") (v "0.0.12") (d (list (d (n "fn_graph") (r "^0.10.0") (f (quote ("async" "interruptible" "resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.12") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ddj9bsdf9v8w4xrli4sdyfz21v7griqijf17nmxvd0g54sysbvp")))

(define-public crate-peace_data-0.0.13 (c (n "peace_data") (v "0.0.13") (d (list (d (n "fn_graph") (r "^0.12.0") (f (quote ("async" "interruptible" "resman"))) (d #t) (k 0)) (d (n "peace_core") (r "^0.0.13") (d #t) (k 0)) (d (n "peace_data_derive") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w70rsj3fsibz0k3nmvp7ipn8vlr9jka6qvna1b5ysazq5wyn3nh")))

