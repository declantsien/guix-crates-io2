(define-module (crates-io pe ac peace_diff) #:use-module (crates-io))

(define-public crate-peace_diff-0.0.2 (c (n "peace_diff") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)))) (h "086n03pifxxdcpway2w3mgvr962rrzii2562i2pd223m1yvy8d8y")))

(define-public crate-peace_diff-0.0.3 (c (n "peace_diff") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "01w8zbqfs9czv1qnvzydw17jyr9vl6n6cy7cp85mylxmkx6d0l45")))

(define-public crate-peace_diff-0.0.4 (c (n "peace_diff") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v51mk7ra6rv2hx8xybm3jhvm44wcak04vl9j9y7w6sy9s5j68g2")))

(define-public crate-peace_diff-0.0.5 (c (n "peace_diff") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z5qm49ls5mx886jmwbks76xdzf00gbps1hcrx17aprrpxz3s9zs")))

(define-public crate-peace_diff-0.0.6 (c (n "peace_diff") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "18dqif9lki0m8vbdxv8z69w97fl47x6vpvxfz6xpzv7yr363s64x")))

(define-public crate-peace_diff-0.0.7 (c (n "peace_diff") (v "0.0.7") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bkydzrz04svsxlrvmjfjp9npcqnqciy993lc95bkvlpsrdr27wv")))

(define-public crate-peace_diff-0.0.8 (c (n "peace_diff") (v "0.0.8") (d (list (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ib6y5680j0hzwi6pk6ddmq7sr2kiy360s0rhv4gqhsckq2sqgwv")))

(define-public crate-peace_diff-0.0.9 (c (n "peace_diff") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "09j5ysiyvqj7msxfx0mn4x8863vvsfwnd36p6v521r1q8w1lxh45")))

(define-public crate-peace_diff-0.0.10 (c (n "peace_diff") (v "0.0.10") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "18r2l5vnh920f6iz72b0kac38cbwlplzmmwmcb5nv4z5h9v86k0a")))

(define-public crate-peace_diff-0.0.11 (c (n "peace_diff") (v "0.0.11") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "11xg8yigf3jg9kk2b0v0bimw5mwjnmyrh8jz476jdchkxysdjky3")))

(define-public crate-peace_diff-0.0.12 (c (n "peace_diff") (v "0.0.12") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rivk0kz5yw6m653v16pqkz285wz7j35rlgp2pf1svmlyq2qybv")))

(define-public crate-peace_diff-0.0.13 (c (n "peace_diff") (v "0.0.13") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "03hczia7fr75a5b1818wx1n55nd8ryqpcxpbrgv6n7j1jxyjfxmz")))

