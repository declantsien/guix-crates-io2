(define-module (crates-io pe ac peace_params_derive) #:use-module (crates-io))

(define-public crate-peace_params_derive-0.0.10 (c (n "peace_params_derive") (v "0.0.10") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kkranww434nrzkn8dpkfnb5ji3l16ma244s17p2s8i00c52hghc")))

(define-public crate-peace_params_derive-0.0.11 (c (n "peace_params_derive") (v "0.0.11") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jha65w55f2kfynjk8112azjja71adjvbbr4i8hnz270av6cgnrr")))

(define-public crate-peace_params_derive-0.0.12 (c (n "peace_params_derive") (v "0.0.12") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wvw4kp2m0grr47hd6kmyqhrw5yyngkzscj61c2ilsgayv2l0pqi")))

(define-public crate-peace_params_derive-0.0.13 (c (n "peace_params_derive") (v "0.0.13") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09g4nvzg3af8zhys6av01n1hd3msafnn0a28wi29nvk5nmsf4qin")))

