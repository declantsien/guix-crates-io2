(define-module (crates-io pe ac peace_fmt) #:use-module (crates-io))

(define-public crate-peace_fmt-0.0.7 (c (n "peace_fmt") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "1fwmz1snipk5vn4ax923svypsqc1gxqwn0xm2mxi06rq8lr4mzbm")))

(define-public crate-peace_fmt-0.0.8 (c (n "peace_fmt") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 2)))) (h "16fvdf3h8g8jdpicyyd7cndlwpsrfyvl2f6x2z8s2gisp7knazdc")))

(define-public crate-peace_fmt-0.0.9 (c (n "peace_fmt") (v "0.0.9") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)))) (h "1vk5y70x4hvlc0h1j21gy71bnw1pa26njn26h8i7wagy0w8cg58n")))

(define-public crate-peace_fmt-0.0.10 (c (n "peace_fmt") (v "0.0.10") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)))) (h "0f17zxhfnlqbnr91y2wdyzzxnaail9s08gwp01xl59x2kby6ygxa")))

(define-public crate-peace_fmt-0.0.11 (c (n "peace_fmt") (v "0.0.11") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "04hqwr4sazx9vprdb0nkr5br63qj4lx2pn1ac8dxg9576lrp0vf7")))

(define-public crate-peace_fmt-0.0.12 (c (n "peace_fmt") (v "0.0.12") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ny9w2w3i0pymhczn1d5qkbqzl10k91kkb5rdwa7spl5sp61q3z4")))

(define-public crate-peace_fmt-0.0.13 (c (n "peace_fmt") (v "0.0.13") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 2)))) (h "1b71kp30506gwv5r4cknarj0d6zq4i9wy3jydn4jyzpkffy9yk8z")))

