(define-module (crates-io pe ac peach) #:use-module (crates-io))

(define-public crate-peach-0.1.0 (c (n "peach") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "winit") (r "^0.17.2") (d #t) (k 0)))) (h "1d1zsmq773gr8nvx9sjdm376g2yhfpqx5bkgaxpvxj488lqsl9jj") (y #t)))

(define-public crate-peach-0.1.1 (c (n "peach") (v "0.1.1") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "winit") (r "^0.17.2") (d #t) (k 0)))) (h "0lb6sxdm9qldy46xqzxvng4vz6vphn4rl16s1cglsawzb3msaaqs") (y #t)))

(define-public crate-peach-0.1.2 (c (n "peach") (v "0.1.2") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "winit") (r "^0.17.2") (d #t) (k 0)))) (h "0093dvwdd434k3vny7aks4vkayh3kccfb175b4j59ca7izgp9niv") (y #t)))

(define-public crate-peach-0.1.3 (c (n "peach") (v "0.1.3") (h "17g5d4ka44l2bbrpfji640smm8fcznvybzmxhkinm1dc0gy4xjvz") (y #t)))

(define-public crate-peach-0.2.0 (c (n "peach") (v "0.2.0") (h "12fyzqpaamrpdb1vn5lif5ihs17317sbn4a7kvw1wwl3sim1bljl")))

(define-public crate-peach-0.3.0 (c (n "peach") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.14") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "wgpu-native") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 0)))) (h "0r24pxlq9mxxc9pidc6y6p6aw5shyzrid4pl26j3gfmmh9vgfclb")))

(define-public crate-peach-0.4.0 (c (n "peach") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.16.2") (d #t) (k 0)) (d (n "pollster") (r "^0.2.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.0") (d #t) (k 0)) (d (n "wgpu-subscriber") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "1njcnp6r1zbavyj3nypbhib8iyhab1hpd9s8xlfba155l0j3vxpn") (f (quote (("force_f32") ("default"))))))

