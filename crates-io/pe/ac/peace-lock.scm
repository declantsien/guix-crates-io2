(define-module (crates-io pe ac peace-lock) #:use-module (crates-io))

(define-public crate-peace-lock-0.1.0 (c (n "peace-lock") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dq9vy3y9kar1p713a2vxmbx79i0jvi7a5sig16w2rci7w3ykzwz") (f (quote (("default" "check") ("check"))))))

(define-public crate-peace-lock-0.1.1 (c (n "peace-lock") (v "0.1.1") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17ck9hq4508zy2iaak1cvcys570f5gll8178bab4z4q0qmfyqij8") (f (quote (("default") ("check"))))))

(define-public crate-peace-lock-0.1.2 (c (n "peace-lock") (v "0.1.2") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "106h02011s31asxp67y3vpzl5v3a14j0sx0msydyxhn8wl1rv98k") (f (quote (("default") ("check"))))))

(define-public crate-peace-lock-0.1.3 (c (n "peace-lock") (v "0.1.3") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1km443yfpqi18nw9937rmdfyxvj8lmj9imh6l1x32rkssyfyq654") (f (quote (("default") ("check"))))))

