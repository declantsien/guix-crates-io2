(define-module (crates-io pe ac peachelf) #:use-module (crates-io))

(define-public crate-peachelf-0.1.0 (c (n "peachelf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wnl98h12cafzi2vzygzlzaiixkbz2kagzqp38n26ki6g3cbc6a2")))

(define-public crate-peachelf-0.1.1 (c (n "peachelf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rln2jz18j4h6cgfakdxhsfxpzhq8nwjiby0dqydcpsna4zdzg59")))

