(define-module (crates-io pe ac peace_code_gen) #:use-module (crates-io))

(define-public crate-peace_code_gen-0.0.7 (c (n "peace_code_gen") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6871wpj9n8qify3rhsv8g00w1brbdd952cnpf0jnm8fd2jc5sj")))

(define-public crate-peace_code_gen-0.0.8 (c (n "peace_code_gen") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1wl619dax54yp2321sb36q2wml9ml800nmh5h1rqsv8h7583nm3r")))

(define-public crate-peace_code_gen-0.0.9 (c (n "peace_code_gen") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0dfbvi4ivgmbqjjqnb3f5xmlhbw6056kiqch46ylym5zjxvwjwlb")))

(define-public crate-peace_code_gen-0.0.10 (c (n "peace_code_gen") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1lnmm9md6cjbpkqz90vzvh6yfj8xzkgvbhs5bl0q0k826rlz56lb")))

(define-public crate-peace_code_gen-0.0.11 (c (n "peace_code_gen") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1zkhvjhlr5sz4k3l2cyii7w1mkgyfrym6asw8fgjmxd85xdh39zd")))

(define-public crate-peace_code_gen-0.0.12 (c (n "peace_code_gen") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0y1i8xbw0plpidigg98z4d31m95m9vpz4kshpvkn77dgcq62lxsz")))

(define-public crate-peace_code_gen-0.0.13 (c (n "peace_code_gen") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "105j5pi0149k0clpr8a5m6rh3kcfw04n5l3dv9i9yjyyblivmvpd")))

