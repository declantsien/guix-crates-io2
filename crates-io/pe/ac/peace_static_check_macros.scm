(define-module (crates-io pe ac peace_static_check_macros) #:use-module (crates-io))

(define-public crate-peace_static_check_macros-0.0.3 (c (n "peace_static_check_macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.44") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "1378wy4azfmmw3yangq2hjh1lqivbbx3xnfwnfkxy17yk5bdc9gw")))

(define-public crate-peace_static_check_macros-0.0.4 (c (n "peace_static_check_macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "07cxchz0f8xv98v1ad72800kd29zzlflk0h2xgqpb922z7viwx0a")))

(define-public crate-peace_static_check_macros-0.0.5 (c (n "peace_static_check_macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0z8zbxmkg2ls84hqniriwllclmxn31vk5dkpd2irm4ij86kbfvx2")))

(define-public crate-peace_static_check_macros-0.0.6 (c (n "peace_static_check_macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "07x3f4yqb04sf16yc3091y2279382jz36wq81jar336apba9ibp5")))

(define-public crate-peace_static_check_macros-0.0.7 (c (n "peace_static_check_macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1z3nyjvsg6yd2xhn92cx83qm7r59wnmwp8v7i2fmjdqc21qx6adf")))

(define-public crate-peace_static_check_macros-0.0.8 (c (n "peace_static_check_macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1pbd5gkcljbdr8m4n18gwf4nmpa5rmrar9xknpcqsbq1gsqn5idg")))

(define-public crate-peace_static_check_macros-0.0.9 (c (n "peace_static_check_macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "02f9b0v8m7r7fv566m7xhrpz9minkrsn9j265jh4y2clhg2mypfn")))

(define-public crate-peace_static_check_macros-0.0.10 (c (n "peace_static_check_macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0gj6vhgv4p2y9qnvvl07w93qasyz44d8r27in8mhh7m31x7mzm14")))

(define-public crate-peace_static_check_macros-0.0.11 (c (n "peace_static_check_macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "07avd5xns350shxxl6kwynrs1l0b2d673cwgp64abdxn85zadj0h")))

(define-public crate-peace_static_check_macros-0.0.12 (c (n "peace_static_check_macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1vil1wa9dd70wbl6x6czp41nkbkhyxg0k51q197krhcl9sk48abc")))

(define-public crate-peace_static_check_macros-0.0.13 (c (n "peace_static_check_macros") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1d0jcnc7whiq9jp63mg6jd2wf99ipn9im0an6p4507rwp35nd41d")))

