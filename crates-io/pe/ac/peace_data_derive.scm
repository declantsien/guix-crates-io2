(define-module (crates-io pe ac peace_data_derive) #:use-module (crates-io))

(define-public crate-peace_data_derive-0.0.2 (c (n "peace_data_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0iai6cd41yxf6l2haygzx8ckzmw3zykfp7xqh1ha4nc5k23bl6a9")))

(define-public crate-peace_data_derive-0.0.3 (c (n "peace_data_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.44") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "0by0ipj2sdgvn5n7sdv52was4c7s4mvk2fcykk402swm9b98zyhb")))

(define-public crate-peace_data_derive-0.0.4 (c (n "peace_data_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "11hwi1ak7vccrzj30p1kmab8qcn8q4g10nckgmabcm36vnj4c1fj")))

(define-public crate-peace_data_derive-0.0.5 (c (n "peace_data_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1fyiggrdz6kipmcvb871rl0kkyvdmyz6489xbnnysbagsxc7bcbc")))

(define-public crate-peace_data_derive-0.0.6 (c (n "peace_data_derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0k23s7dbyfbx2yhgv2cc5n93687g1v98g9gxmiy99g2qvdmjvcxb")))

(define-public crate-peace_data_derive-0.0.7 (c (n "peace_data_derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0vwja5hq41jkmp5lilb95azs38lyc0crr8i2nyamr1szfgz9y5ym")))

(define-public crate-peace_data_derive-0.0.8 (c (n "peace_data_derive") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "08grp2n50y4y5km4ik0gsl51ja8i95jjg3vkz0896yl84d3yj18y")))

(define-public crate-peace_data_derive-0.0.9 (c (n "peace_data_derive") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "02bsnwqijw992z727gzx1a50z90pvcgdqbaqclna8vaf9rps2yrn")))

(define-public crate-peace_data_derive-0.0.10 (c (n "peace_data_derive") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1gmgkwd7r0jjid6w2mkiipx10blgqghd0vgwyqh460zgfmihqk94")))

(define-public crate-peace_data_derive-0.0.11 (c (n "peace_data_derive") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "1crpqb062v5xba66sz7q1zwkfxrixwrv45n7q0sljhrm1806rb74")))

(define-public crate-peace_data_derive-0.0.12 (c (n "peace_data_derive") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "11z85qwqs306k70hj6wfr0adq9rqbqc6l08ikdpb397sf07ycrmv")))

(define-public crate-peace_data_derive-0.0.13 (c (n "peace_data_derive") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0p45xc9g4v1nrhq3gq0rhap4jmzmxl4xplcp8kvhfy18sfq2x31l")))

