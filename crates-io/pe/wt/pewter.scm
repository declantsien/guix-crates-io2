(define-module (crates-io pe wt pewter) #:use-module (crates-io))

(define-public crate-pewter-0.0.1-prerelease (c (n "pewter") (v "0.0.1-prerelease") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1qbv0snqwzplhbz54csg2xgkhbs0kxrim43jxx5qp98sr05pwz8k") (f (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

(define-public crate-pewter-0.0.2 (c (n "pewter") (v "0.0.2") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)))) (h "07v3lfndkzs6y2xin82vkq45fb8ks2z4dyjhc9pgzz05ixj7nh9c") (f (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

(define-public crate-pewter-0.0.3 (c (n "pewter") (v "0.0.3") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)))) (h "1sjnfzrjp08gs1xic7cmmqb92ssxffcmxvhbcb9abf72lrzdsspn") (f (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

