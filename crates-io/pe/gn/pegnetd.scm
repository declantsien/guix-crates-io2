(define-module (crates-io pe gn pegnetd) #:use-module (crates-io))

(define-public crate-pegnetd-0.1.1 (c (n "pegnetd") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "1gw42fhzv23cfjj432kcrz4n5vfihqg0yqgmicnc0c1kc4wnkh5r")))

(define-public crate-pegnetd-0.1.2 (c (n "pegnetd") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "05xqaapy0y1233z6d63whg28bwmjm5ri30xllvw72023zx1rxhsc")))

