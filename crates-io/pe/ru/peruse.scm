(define-module (crates-io pe ru peruse) #:use-module (crates-io))

(define-public crate-peruse-0.2.0 (c (n "peruse") (v "0.2.0") (d (list (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "1lv0892ii7v7vnlp53zlc049j300vbsif8p5ki8wh3qfy9sfap9x")))

(define-public crate-peruse-0.3.0 (c (n "peruse") (v "0.3.0") (d (list (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "19prbq0ix24j02jfdp6w5rfvmmcm1g3q91ggw4375drnzs98nx83")))

