(define-module (crates-io pe nd pendragon) #:use-module (crates-io))

(define-public crate-pendragon-0.1.0 (c (n "pendragon") (v "0.1.0") (d (list (d (n "gee") (r "^0.3.0") (f (quote ("euclid"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.17.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "042hja27zvfd2x5q2pfxmms7jm6x2bcmxzrcfc93s6s5r50i36ir") (f (quote (("serde" "gee/serde"))))))

