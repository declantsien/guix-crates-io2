(define-module (crates-io pe nd pendulum) #:use-module (crates-io))

(define-public crate-pendulum-0.1.0 (c (n "pendulum") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0z7jx3af72zhyya17d78bq8g64gzz219ka9q0icx4ir26ij9lav9") (f (quote (("future" "crossbeam" "futures") ("default" "future"))))))

(define-public crate-pendulum-0.2.0 (c (n "pendulum") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "17mqr7bxp7w14ri109zxdhmbxnfmk4bw028xxa404pwi24n5i4z4") (f (quote (("future" "crossbeam" "futures") ("default" "future"))))))

(define-public crate-pendulum-0.2.1 (c (n "pendulum") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1zk2lc5y5iq0ng63b6a6bddx86f1gyjcgvlwfzy1r5cihjvycd87") (f (quote (("future" "crossbeam" "futures") ("default" "future"))))))

(define-public crate-pendulum-0.3.0 (c (n "pendulum") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0n4zkf8b8c901jg2dixcqdv4fkwmdw3s8gg21cdsgn7lar7p191v") (f (quote (("future" "crossbeam" "futures") ("default" "future"))))))

(define-public crate-pendulum-0.3.1 (c (n "pendulum") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0g9rh867hw2i41dja2vjqr5h7v4ygl12spd285qdj6ibzjjzqdgn") (f (quote (("future" "crossbeam" "futures") ("default" "future"))))))

