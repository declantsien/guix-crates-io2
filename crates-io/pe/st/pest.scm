(define-module (crates-io pe st pest) #:use-module (crates-io))

(define-public crate-pest-0.0.1 (c (n "pest") (v "0.0.1") (h "1i95kqrzy6hbfp2x2a1np5pd8b54vmggb368p4d850lqcv7ni71h")))

(define-public crate-pest-0.1.0 (c (n "pest") (v "0.1.0") (h "1vkirlahl4xqlbiyabwynw2kzvnjfcmg3m4r1047i4dvqh8h2qxl")))

(define-public crate-pest-0.2.0 (c (n "pest") (v "0.2.0") (h "1ks8vb4249p7ljbni8735npdvj3q1yf8w11m55whpq754x2s8m81")))

(define-public crate-pest-0.2.1 (c (n "pest") (v "0.2.1") (h "14ximhfzpwis5da937qkalhbj7lddig7lbaw97cxrf7sk3hdlgh1")))

(define-public crate-pest-0.2.2 (c (n "pest") (v "0.2.2") (h "1lkhxgrrpz7ny6hfhyhssrspfw3hw3z5fagvcbsb3rx5n3wkam2l")))

(define-public crate-pest-0.2.3 (c (n "pest") (v "0.2.3") (h "093hv1bnm0k2k9hizi57k16pspmld0jgdf5qr90i0zlsqc8mqf4j")))

(define-public crate-pest-0.2.4 (c (n "pest") (v "0.2.4") (h "0h132h4pg23xsq01jaflqiixj33lhi0v6ai8vcpx8ysrfklw45yy")))

(define-public crate-pest-0.2.5 (c (n "pest") (v "0.2.5") (h "1m3z0z8z5dbipcjjsb6v9spysidxr4xpwag82qw6l2j7g4gxvmi8")))

(define-public crate-pest-0.2.6 (c (n "pest") (v "0.2.6") (h "19qh556krl1s7rb3lkmhr290zh967i9jbj74n7armfflmrhdsbbr")))

(define-public crate-pest-0.2.7 (c (n "pest") (v "0.2.7") (h "0i6ysy98xwq5k29nb6gfyp2d3yj68is69658laawbbl41vvz4iaz")))

(define-public crate-pest-0.2.8 (c (n "pest") (v "0.2.8") (h "117kpg5x2xgvzww2bn7z9gqamzbkv7mk6q4w2yn0g9ns8zbz5r8x")))

(define-public crate-pest-0.2.9 (c (n "pest") (v "0.2.9") (h "0mhq18lvx5qyinlmki98sjhm2wq548dci4kak9lfkkdqlwcbgznm")))

(define-public crate-pest-0.3.0 (c (n "pest") (v "0.3.0") (h "1xy3dlcjl5yjai7gkm60mldfik8d8k4vypw673ipsbgxi6g12500")))

(define-public crate-pest-0.3.1 (c (n "pest") (v "0.3.1") (h "040sybn2h2h93ncz0gq7y38srpdb0qwqnsdrf03ldvc3fk31n3c5")))

(define-public crate-pest-0.3.2 (c (n "pest") (v "0.3.2") (h "0xyjjyhk5in96qflarznbpm40l3zf77krdpplsjachc2z4sgf64g")))

(define-public crate-pest-0.3.3 (c (n "pest") (v "0.3.3") (h "1j7d862fhkxakzz549bvzhcwhfbjxfr4lchdr6mg09kwsqrxlv8a")))

(define-public crate-pest-0.4.0 (c (n "pest") (v "0.4.0") (h "10phq706v0xbh62j9v7xmvdsj67kssbgaj5wkmxaynb33b46crig")))

(define-public crate-pest-0.4.1 (c (n "pest") (v "0.4.1") (h "1xn796gfai5dfhmn8fj18jpgxv7lm9xz8iiydpf4rfv7b4x84biy")))

(define-public crate-pest-1.0.0-beta.1 (c (n "pest") (v "1.0.0-beta.1") (h "01ad8q9n6cy89mmb5y57z1sjg67xhirdpd3mz0x3ygr2l186xzdc")))

(define-public crate-pest-1.0.0-beta.2 (c (n "pest") (v "1.0.0-beta.2") (d (list (d (n "pest_grammars") (r "^1.0.0-beta") (d #t) (k 2)))) (h "0fjij3ndgsykwf36hzdkv9pnlc79j7zp83ql57m3971pk00czi64")))

(define-public crate-pest-1.0.0-beta.3 (c (n "pest") (v "1.0.0-beta.3") (d (list (d (n "pest_grammars") (r "^1.0.0-beta") (d #t) (k 2)))) (h "00isz23sm91jcygbjw4slyan9vfak6pg1wvhqbvhm5idlh6jlaaz")))

(define-public crate-pest-1.0.0-beta.4 (c (n "pest") (v "1.0.0-beta.4") (h "0qrirm1mrw1db0908bb31bd6is3yjdpcy1lh1vk9j2ag6vhnkvyy")))

(define-public crate-pest-1.0.0-beta.5 (c (n "pest") (v "1.0.0-beta.5") (h "1wcvqghvrv5lqj8vi369112cwhrjqhwl9ldipi7m26hmn2w2hf2y")))

(define-public crate-pest-1.0.0-beta.6 (c (n "pest") (v "1.0.0-beta.6") (h "01k7fxhghpyi1c8iqakhmjz8gqh2kikybc6dc2rm5jax2kcjra4f")))

(define-public crate-pest-1.0.0-beta.7 (c (n "pest") (v "1.0.0-beta.7") (h "0bc7nng64gf3paz28vbzi72ic83rapcagx7g3zmiccazbxqr7qaa")))

(define-public crate-pest-1.0.0-beta.8 (c (n "pest") (v "1.0.0-beta.8") (h "0gjbar88i6x3308qxmamqb03xs1db80zn40qrcgh39mv613df2a0")))

(define-public crate-pest-1.0.0-beta.9 (c (n "pest") (v "1.0.0-beta.9") (h "1krzzvri2zc3a72fivphwf8x3g6pjaxb0clak03dlv4hjpx6wyz4")))

(define-public crate-pest-1.0.0-beta.10 (c (n "pest") (v "1.0.0-beta.10") (h "18ihk27j5gr4krwidg7n513w8zgvcgk2bhgpqcaipy4x087baqjv")))

(define-public crate-pest-1.0.0-beta.11 (c (n "pest") (v "1.0.0-beta.11") (h "1c3nwzlby132rv2bcnd74vv5lf9yl9g3fmzk8nfx4f343v61hxcj")))

(define-public crate-pest-1.0.0-beta.12 (c (n "pest") (v "1.0.0-beta.12") (h "0q60mwnmi6llgyimj94m3rzgm4s3akn0s3i0aqi3m21hh1krp4hy")))

(define-public crate-pest-1.0.0-beta.13 (c (n "pest") (v "1.0.0-beta.13") (h "0k3qns7706dp28sf0g2r775j028f1d6mqv3r2lj6z94i8lj68cdw")))

(define-public crate-pest-1.0.0-beta.14 (c (n "pest") (v "1.0.0-beta.14") (h "0cgaqyviikwrswd69vy81llq3ijbmqph2x66b17rk8yvi96qjyhs")))

(define-public crate-pest-1.0.0-beta.15 (c (n "pest") (v "1.0.0-beta.15") (h "1wmwfhb29xp666ii9da8gwphvb270rwa7pzygj408si63ndq9xvr")))

(define-public crate-pest-1.0.0-beta.16 (c (n "pest") (v "1.0.0-beta.16") (h "0rqpv8b90xq516m8xn39nlplhmz7cf3xvrhv1agj6p94xvhzkz2a")))

(define-public crate-pest-1.0.0-beta.17 (c (n "pest") (v "1.0.0-beta.17") (h "0fgcidaq53bvxv9niss1kid98pf4c9i1dsabj4sxhqcppk7vj8p5")))

(define-public crate-pest-1.0.0-rc.0 (c (n "pest") (v "1.0.0-rc.0") (h "0207flgsfcw10i8rh5pnk7z67w0gqrg9xx7q24z73asg9qw1cpqh")))

(define-public crate-pest-1.0.0-rc.1 (c (n "pest") (v "1.0.0-rc.1") (h "1yaidhxakb1b4j066q4crnqx9lbkgdgsyqab2a9xa14lp4kkczyv")))

(define-public crate-pest-1.0.0 (c (n "pest") (v "1.0.0") (h "0rn1cv10yqgylnxyrb62xd5b759jvynjg5jw99afrrnsfqarmkgk")))

(define-public crate-pest-1.0.1 (c (n "pest") (v "1.0.1") (h "0j0k37alm4xv3cc75ad14m5gbxphw5in59m63zjqkdqvwqvqqcl7")))

(define-public crate-pest-1.0.2 (c (n "pest") (v "1.0.2") (h "1szngywxcla9146a047l44l5icv6k3xa9q9brzsl6k8r5qkidnzk")))

(define-public crate-pest-1.0.3 (c (n "pest") (v "1.0.3") (h "01p9c6yfyfqdgvh0c4wp0l1x1ds4asrs376dlikdq7kmiaipq4g1")))

(define-public crate-pest-1.0.4 (c (n "pest") (v "1.0.4") (h "0d6bhrr5xkilh1pb4b1sh485v5qvl4crzscnf6ysvvzhy5a1bjs7")))

(define-public crate-pest-1.0.5 (c (n "pest") (v "1.0.5") (h "1qf24iz414pbv3abh2mw9ybkiklm1gi51fkx0i2n5y10pss83n0p") (y #t)))

(define-public crate-pest-1.0.6 (c (n "pest") (v "1.0.6") (h "1z7fbi5yndj2drh93jn488ab8ajjnm9db2ppfky86ff3bj5mvkhg")))

(define-public crate-pest-2.0.0 (c (n "pest") (v "2.0.0") (d (list (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "1zn1c3ba57zi8y7q0disk2wz8xdnb9d03x0h557p6k62i30caij8")))

(define-public crate-pest-2.0.1 (c (n "pest") (v "2.0.1") (d (list (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "0lw310462g1wlps19a2pdvzlw29rzzibxlwzd365v1nydv9v1ay3")))

(define-public crate-pest-2.0.2 (c (n "pest") (v "2.0.2") (d (list (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "1png7w61pcg3jxkgigz2wbpf75jsk3w4ab8gqysjnwr3v4d0axx6")))

(define-public crate-pest-2.1.0 (c (n "pest") (v "2.1.0") (d (list (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "1hz9cvbqswcp7dp3xs58z7hi1k2rh3gidz8basckrayqk0mcgw2l")))

(define-public crate-pest-2.1.1 (c (n "pest") (v "2.1.1") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "134686mwxm73asbiads53zfchqvvcrsrsyax2cghfcizmvg8ac4k") (f (quote (("pretty-print" "serde" "serde_json"))))))

(define-public crate-pest-2.1.2 (c (n "pest") (v "2.1.2") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "0jl9nv6jx1jy3fz2j49nw7jkh9z5igvrbvsgnbc5aan2ql0v4kvy") (f (quote (("pretty-print" "serde" "serde_json"))))))

(define-public crate-pest-2.1.3 (c (n "pest") (v "2.1.3") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (d #t) (k 0)))) (h "0lry80bm90x47nq71wxq83kjrm9ashpz4kbm92p90ysdx4m8gx0h") (f (quote (("pretty-print" "serde" "serde_json"))))))

(define-public crate-pest-2.2.0 (c (n "pest") (v "2.2.0") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (k 0)))) (h "1dxf3fm0phdfbrqx9klwk3481qakrq3vw7fnxf92xa12al2kadl2") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (y #t) (r "1.56")))

(define-public crate-pest-2.2.1 (c (n "pest") (v "2.2.1") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (k 0)))) (h "1y7f45lg0gqnyikz9ksm6lwmc49kn004wyyvcabynaidihmnwj39") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (y #t) (r "1.56")))

(define-public crate-pest-2.3.0 (c (n "pest") (v "2.3.0") (d (list (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (k 0)))) (h "1d6yr6w4pbfrxnx46d1fiwbw0mhjlxi8lff9lcjvrzni67an01ab") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.3.1 (c (n "pest") (v "2.3.1") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (k 0)))) (h "0j206skxk01jcm1f0b0i395hrnagydngz5nw1sxznl5q9g7ryxyb") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.4.0 (c (n "pest") (v "2.4.0") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.1") (k 0)))) (h "06n4g6rv96fl0wr6vgxnc79d6kr71h99pmblglrr5r32q1lvriyv") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.4.1 (c (n "pest") (v "2.4.1") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1y32ds1w4p6jqvs7hj4kgghjagp574gy10admjnaf69cqr65ca55") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.0 (c (n "pest") (v "2.5.0") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "06l666lhkpszj21c04l0kr7ca5hv29ddzhrxkwpp1gq5g47hnh2z") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.1 (c (n "pest") (v "2.5.1") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1l79whw28qajiq2brjzfhpqcv8iph80w1xwblyib1yg094syv2yc") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.2 (c (n "pest") (v "2.5.2") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1m0nm0k8by3fgy0f2hzlz071fwqxhj5k1g261mx549khkvxqcvhg") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.3 (c (n "pest") (v "2.5.3") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "02n3b2hv9ciysybs9qzxza25gp8493dd7r8b57kfkxwi9nhb8ms2") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.4 (c (n "pest") (v "2.5.4") (d (list (d (n "bytecount") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "13qdsjkahlyyb2h5iy7a7qg57plcdpwpxj8w6bkdn9iplcpjvdja") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("fast-line-col" "memchr" "bytecount") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.5 (c (n "pest") (v "2.5.5") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "0q06s01w2ym7s0gc61242h257zbwmp9bqfv6mlxm2kjc23zwr2h2") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.6 (c (n "pest") (v "2.5.6") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1xr2ny6dbapkiyh8qx0ysnsyx2n6ms41gmckqcmxg5af4fdr7gcc") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.5.7 (c (n "pest") (v "2.5.7") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "08lifi9djbaa3wv1x39ga11b6dc5fncnnqiwlzgdxm8s83l0653v") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (r "1.56")))

(define-public crate-pest-2.6.0 (c (n "pest") (v "2.6.0") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "0w3vaad35km41g2liasf0sfkgfhj190rn7mc9q9hf18zn2zq93p6") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (y #t) (r "1.56")))

(define-public crate-pest-2.6.1 (c (n "pest") (v "2.6.1") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1ql4px09gz7v6ady1mfw90n79gxr2gv4mxhrdn9cllr9n23370qn") (f (quote (("std" "ucd-trie/std" "thiserror") ("pretty-print" "serde" "serde_json") ("default" "std") ("const_prec_climber")))) (y #t) (r "1.56")))

(define-public crate-pest-2.7.0 (c (n "pest") (v "2.7.0") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1j9z793syj4jnn0269d0hv4ap97735xm71h12dzvyajyspj3afgp") (f (quote (("default" "std") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.60")))

(define-public crate-pest-2.7.1 (c (n "pest") (v "2.7.1") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1rcqfa9bii8h3fgpqklh7frj7miai0wr2f4xmmddcaaq0iaisb8d") (f (quote (("default" "std") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.60")))

(define-public crate-pest-2.7.2 (c (n "pest") (v "2.7.2") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "0npylniylqnrc3l9sfipal4zlp2yh2kr987ijfd78gx1cm1lmjqs") (f (quote (("default" "std") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.60")))

(define-public crate-pest-2.7.3 (c (n "pest") (v "2.7.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "0cydgv1ckxqcqq7sysrjfq5jdd4ifx1pn52sn3awh6lrzn2x196p") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.60")))

(define-public crate-pest-2.7.4 (c (n "pest") (v "2.7.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "194qbnc2y3qhr2hp9bp5yc0vr0b8qr2b6pxxvg028sjxnvkz28n0") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.5 (c (n "pest") (v "2.7.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1xgzh2sl4amycl4c20r68br20ypr583lhs6wi65vwi55almfx75f") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.6 (c (n "pest") (v "2.7.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "01mdyqmjm6iyrbx4dkays4ss0b3mk5i93lb42yr4ajn4hf6hs80z") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.7 (c (n "pest") (v "2.7.7") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "0ihmqicb0f1g8ssz0wfvn07bcxbznrr2j96cz59pb8mn6360v711") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.8 (c (n "pest") (v "2.7.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1a77a6v1dna7ba6hp69q65r8mrinzbrwg8afg01qx35p1wyh5y2n") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.9 (c (n "pest") (v "2.7.9") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "158ckwp6cnljwcici05ix2j17iiq05wnsc9ny01bi9z1vrcv07ri") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (y #t) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-pest-2.7.10 (c (n "pest") (v "2.7.10") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "1s4fvis7h6l872g6nk17r130kcllj4c0hjvwkzd3hi196g3320an") (f (quote (("default" "std" "memchr") ("const_prec_climber")))) (s 2) (e (quote (("std" "ucd-trie/std" "dep:thiserror") ("pretty-print" "dep:serde" "dep:serde_json")))) (r "1.61")))

