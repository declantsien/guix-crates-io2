(define-module (crates-io pe st pest_consume) #:use-module (crates-io))

(define-public crate-pest_consume-1.0.0 (c (n "pest_consume") (v "1.0.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "0h7bdr4b93jqlm0c33nq38jdca1b7rn73kdcfcm4ssbmvyvf46fv")))

(define-public crate-pest_consume-1.0.1 (c (n "pest_consume") (v "1.0.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "1ys1h2p3rphwznby8w4blaqbm2gcxchxdji65h5r24038vc99zra")))

(define-public crate-pest_consume-1.0.2 (c (n "pest_consume") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "0zlapbmndy5ym4927h903ag1xrhrmn2nc2nmzijm9463ax9flly7")))

(define-public crate-pest_consume-1.0.3 (c (n "pest_consume") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "18g3qmmfqlgq2x0r3gvz0qr820v78infnpxs09lc3vb9m36nq09z")))

(define-public crate-pest_consume-1.0.4 (c (n "pest_consume") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "1i2qi5qp9qfqqy4229djilwy99mfv2ybl11vvy57ym2wdv4734n8")))

(define-public crate-pest_consume-1.0.5 (c (n "pest_consume") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "02211bbmha16kqhn5n1d7r0jysf7ay9la1l4003bipkainwikwi5")))

(define-public crate-pest_consume-1.0.6 (c (n "pest_consume") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)))) (h "0bmq5hakajrkz8ra6r0k330510jiclmcsgdywc749gp3mc0d41iz")))

(define-public crate-pest_consume-1.1.0 (c (n "pest_consume") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1yb4nqndj97rmx88mkkp1q3csn5pfq93g4pjxs75ikjwn4h939wx")))

(define-public crate-pest_consume-1.1.1 (c (n "pest_consume") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "11ch49mw7yppg6rnr64abj47arnwcxkcj8g8p7wv28m4gjmw5dyw")))

(define-public crate-pest_consume-1.1.3 (c (n "pest_consume") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.2") (d #t) (k 0)) (d (n "pest_consume_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.2") (d #t) (k 0)))) (h "0sskbz2hlqdvrjrp0nxww5diaggsccp2ziql5qaff62xs4178i3r")))

