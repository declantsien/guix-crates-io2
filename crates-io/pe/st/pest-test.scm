(define-module (crates-io pe st pest-test) #:use-module (crates-io))

(define-public crate-pest-test-0.1.0 (c (n "pest-test") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0cz34jzf6426h8x4cpnpipzssn8fw7g27vxgxm17iham7m2bzrbz")))

(define-public crate-pest-test-0.1.1 (c (n "pest-test") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1kawaf52a6jv1pq0wi865pm1z0mabzh6iyd67qbfs8nlh244jcb5")))

(define-public crate-pest-test-0.1.2 (c (n "pest-test") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0z3b4fxr7fn3cgr1i3wyrj192gszdz2n4qna1czkvrnkrm9b7pac")))

(define-public crate-pest-test-0.1.3 (c (n "pest-test") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0zjs9zlk232pml66v8w8z2jh1r65vkk9i8wg9mdxwdp8kyi0vlm8")))

(define-public crate-pest-test-0.1.4 (c (n "pest-test") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17bizarhkpb1g4iahd5h4xlc3xknd2xf29jzixp0l04pzsz1i9k6")))

(define-public crate-pest-test-0.1.5 (c (n "pest-test") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1qbxp4wgc24qzdkgwgfvxblqij4w1lgcnbds93x93pv5rdnwaljz")))

(define-public crate-pest-test-0.1.6 (c (n "pest-test") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1zmgvv8l14xl27g2kljzcl5llp08vich009vljr9xx2ddnlzy9m4")))

