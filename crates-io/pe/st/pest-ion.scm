(define-module (crates-io pe st pest-ion) #:use-module (crates-io))

(define-public crate-pest-ion-0.0.0 (c (n "pest-ion") (v "0.0.0") (d (list (d (n "ion-rs") (r "~0.6.0") (d #t) (k 0)) (d (n "pest") (r "~2.1.3") (d #t) (k 0)) (d (n "pest_meta") (r "~2.1.3") (d #t) (k 0)) (d (n "rstest") (r "~0.10.0") (d #t) (k 2)) (d (n "smallvec") (r "~1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.25") (d #t) (k 0)))) (h "1b7c2lj220i4bklq390g7svvvnrikd8k9d50818gp69d7c9vmn3k")))

