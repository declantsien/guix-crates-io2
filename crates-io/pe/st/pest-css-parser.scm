(define-module (crates-io pe st pest-css-parser) #:use-module (crates-io))

(define-public crate-pest-css-parser-0.1.0 (c (n "pest-css-parser") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0bwk48arkhc9nsdsmjjq2zdm6v0jqa3z55mjgysnc4gdssib85j8")))

(define-public crate-pest-css-parser-0.1.5 (c (n "pest-css-parser") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hg4chicgpcff3zhny655mzqy2g9zkcdmc6440sx4l6dj6iwvc9b")))

(define-public crate-pest-css-parser-0.1.6 (c (n "pest-css-parser") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1z3h9xm6179cpvjbhnak0bw1bpkzjh80ygnlk81xlmdg19hxybni")))

(define-public crate-pest-css-parser-0.1.7 (c (n "pest-css-parser") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0xhxkf8y33bngkb288szsiy3dkhiijb16s649khrl3bmi0lml38h")))

