(define-module (crates-io pe st pest-deconstruct) #:use-module (crates-io))

(define-public crate-pest-deconstruct-0.2.0 (c (n "pest-deconstruct") (v "0.2.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest-deconstruct-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (o #t) (d #t) (k 0)))) (h "1kwrw002nf4swxp31ixmyfck639jkhykkmjmybspy1dy4671fm52") (f (quote (("examples" "pest_derive"))))))

