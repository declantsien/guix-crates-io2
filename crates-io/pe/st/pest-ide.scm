(define-module (crates-io pe st pest-ide) #:use-module (crates-io))

(define-public crate-pest-ide-0.1.0 (c (n "pest-ide") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "0xxcd43bl1c8xlzsalasdnw7ia517j2j0mkdnxf21yavni0wyxfq")))

(define-public crate-pest-ide-0.1.1 (c (n "pest-ide") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "0c0f8dzxy6ip9md8xfsd3db5b4h980nb5wni5876f53pv6xw9llg")))

(define-public crate-pest-ide-0.1.2 (c (n "pest-ide") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "06lsz33n2b9whvnawlzzh7yhzl9rvs8nvg3czx8cwr8d39r4hn93")))

(define-public crate-pest-ide-0.1.3 (c (n "pest-ide") (v "0.1.3") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "01n4djrbmyd52jnkbnww42pdhwma0hzar4j7p0zw8aarq26167pq")))

(define-public crate-pest-ide-0.1.4 (c (n "pest-ide") (v "0.1.4") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "0011f02rhkdcp0wx7naqksa26bi0dypgfsvirrclypyks3f49ns8")))

