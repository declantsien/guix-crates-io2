(define-module (crates-io pe st pest-wdl-1) #:use-module (crates-io))

(define-public crate-pest-wdl-1-0.1.0 (c (n "pest-wdl-1") (v "0.1.0") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1yr4gcqxbfcvcnd7afp476ic7swjy7xgj8nz6d1vv9cq4qgn5lgl")))

(define-public crate-pest-wdl-1-0.1.1 (c (n "pest-wdl-1") (v "0.1.1") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "0q4x3jin3bfr6rl8ahk1n0v23dpafy2yblc513jd01nr7sqj97g6")))

(define-public crate-pest-wdl-1-0.1.2 (c (n "pest-wdl-1") (v "0.1.2") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest-test") (r "^0.1.5") (d #t) (k 2)) (d (n "pest-test-gen") (r "^0.1.6") (d #t) (k 2)))) (h "11kv0dajsm02yhflg61h0ps1ziia83lz22phxgf280mjd4ws1qix")))

(define-public crate-pest-wdl-1-0.1.3 (c (n "pest-wdl-1") (v "0.1.3") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest-test") (r "^0.1.5") (d #t) (k 2)) (d (n "pest-test-gen") (r "^0.1.6") (d #t) (k 2)))) (h "0p96lamngp3a2pq18x0d0dl6n2711yv4jb0xhgf9bs0gr5rv2vrc")))

(define-public crate-pest-wdl-1-0.1.4 (c (n "pest-wdl-1") (v "0.1.4") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest-test") (r "^0.1.5") (d #t) (k 2)) (d (n "pest-test-gen") (r "^0.1.6") (d #t) (k 2)))) (h "1n6n24lk27slk3k464w3782g60wl8hm08czfzimp673cwlbaqnvh")))

(define-public crate-pest-wdl-1-0.1.5 (c (n "pest-wdl-1") (v "0.1.5") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest-test") (r "^0.1.5") (d #t) (k 2)) (d (n "pest-test-gen") (r "^0.1.6") (d #t) (k 2)))) (h "1wq8wc2dc0r82n0djnfr1fkckzypfsl97fz6zs448gdlrn1rn0mj")))

