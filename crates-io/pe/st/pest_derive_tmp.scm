(define-module (crates-io pe st pest_derive_tmp) #:use-module (crates-io))

(define-public crate-pest_derive_tmp-2.1.0 (c (n "pest_derive_tmp") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0) (p "pest_tmp")) (d (n "pest_generator") (r "^2.1.0") (d #t) (k 0) (p "pest_generator_tmp")))) (h "10xri34i4mvivsv70da5lchwzbf4s0zg62scifbkh8fhd3aql7x7")))

