(define-module (crates-io pe st pest_derive) #:use-module (crates-io))

(define-public crate-pest_derive-1.0.0-beta.1 (c (n "pest_derive") (v "1.0.0-beta.1") (d (list (d (n "pest") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "10lrflsvlx7g52dd8ijqxlh12b4zm0sswc8aw7pgl26ghdca5i2a") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.2 (c (n "pest_derive") (v "1.0.0-beta.2") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "04pm3wf5dljvxgcffws3zxnfipdhkx2fc1a3l5sdams6plc4i9bv") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.3 (c (n "pest_derive") (v "1.0.0-beta.3") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1sz1q1nl2q3zsvlq4rl440hycf4f2l784xs4y59r9r6cpli5gsfn") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.4 (c (n "pest_derive") (v "1.0.0-beta.4") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "12myf3fc7cbz5zlx2js488w02p0fvh1wqll0r35a9kjw8m1b1rlr") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.5 (c (n "pest_derive") (v "1.0.0-beta.5") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1rc8djx4ac69m1f4ziwxww2g37pcps49hfngfay8fx30zg5cyx5l") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.6 (c (n "pest_derive") (v "1.0.0-beta.6") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "176zmg6xwjxbdplsnp373b6yqaj37hgrmdp6yvn99f48i5ag7nzw") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.7 (c (n "pest_derive") (v "1.0.0-beta.7") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "08iyyfm44fa3304w1pa9l01dmgl4yjjxrgk4al39b9rbqi2c98bb") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.8 (c (n "pest_derive") (v "1.0.0-beta.8") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1n9h6m8hjpcrvaph7xzffv8w64bmhmgxapwcxn09ibpysdma4kw6") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.9 (c (n "pest_derive") (v "1.0.0-beta.9") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0l710lr2r77n0b7g7wgrh3cbq1smjnv629wfsi2sxx5jc4rv7igg") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.10 (c (n "pest_derive") (v "1.0.0-beta.10") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0kwr5789f7vsw5b39km7vjxz968dla8c80x46j77ir9kg6hadw6z") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.11 (c (n "pest_derive") (v "1.0.0-beta.11") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0sah8d48y74niv2sb2zp0p3jnmz4wxvzgwz2rg717792m5qq4wi5") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.12 (c (n "pest_derive") (v "1.0.0-beta.12") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0009mlqc201v22n46lg9ipaldy6f1jhd2q27560g9q7b5h1fpy3b") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.13 (c (n "pest_derive") (v "1.0.0-beta.13") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0b9kj698lzamaqh5ahgyvyq5ndrrp8wks3jmc83455l4f25782zx") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.14 (c (n "pest_derive") (v "1.0.0-beta.14") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0r76wkqbgdwizsgblngbmfb26jpgs79z9k4f18p1463g6jnz9hh1") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.15 (c (n "pest_derive") (v "1.0.0-beta.15") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0hv8m0rp9ylcmcf80axf1nqv8f15db0gfpg14l0fajpqn959g3c0") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.16 (c (n "pest_derive") (v "1.0.0-beta.16") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1x3lvph6cppf2gn4rfx7q4lh9ma8sbwhcsgl4lq58wjx7cxnzrka") (y #t)))

(define-public crate-pest_derive-1.0.0-beta.17 (c (n "pest_derive") (v "1.0.0-beta.17") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1dmiwxfq0risvqprmyrr27kqx66a3ldhsk9nfvcbk38k9f8p47r5") (y #t)))

(define-public crate-pest_derive-1.0.0-rc.0 (c (n "pest_derive") (v "1.0.0-rc.0") (d (list (d (n "pest") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0hxr3asgjfikp1xz8makqkv72sha1x9r20y7m39pka9jmv4ljk9m") (y #t)))

(define-public crate-pest_derive-1.0.0-rc.1 (c (n "pest_derive") (v "1.0.0-rc.1") (d (list (d (n "pest") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "04rbbkl5q4m2kbpa8p54zz86kgs1gbzn3yjmscwshfqpyvyw2p18") (y #t)))

(define-public crate-pest_derive-1.0.0 (c (n "pest_derive") (v "1.0.0") (d (list (d (n "pest") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1n70pa0hkbk63dj8n7bh284srmx8divrk6fphpm7786cfkiscgi6") (y #t)))

(define-public crate-pest_derive-1.0.1 (c (n "pest_derive") (v "1.0.1") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "06yy3c9f9wi3c8gxh2g8gpv01yr53bjwi2giff8bd5q5bkbdah2r")))

(define-public crate-pest_derive-1.0.2 (c (n "pest_derive") (v "1.0.2") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0aclpax75sgsr3fssza1ihfywgsfqhb3zs2swrn23akrhfvvwaly")))

(define-public crate-pest_derive-1.0.3 (c (n "pest_derive") (v "1.0.3") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0cvsdjdsg6zcxwn36bg2mabmgnlgi2zgn8dd5azsd3yb2xbzisqx")))

(define-public crate-pest_derive-1.0.4 (c (n "pest_derive") (v "1.0.4") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0d6zs9xqli0kkkcn4dxn6v640yrmgxpi69vzx4kcdvcrw5zw64dy")))

(define-public crate-pest_derive-1.0.5 (c (n "pest_derive") (v "1.0.5") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "05yz3gadqhp9fx879sb6yvl43nri54hp4z2fpqm4l8y3qf0zb0bd") (y #t)))

(define-public crate-pest_derive-1.0.6 (c (n "pest_derive") (v "1.0.6") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0al2vis86a2n5dpdjbsadp67jjil6ggv2a4g4wy0m669qpnzllv4")))

(define-public crate-pest_derive-1.0.7 (c (n "pest_derive") (v "1.0.7") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gsjda5znswr45nsw2axml1acacmfxd0xjl1rviwbx4kxfpzm55b")))

(define-public crate-pest_derive-1.0.8 (c (n "pest_derive") (v "1.0.8") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1r4fpzrnz6p9n4h75i9mzb7jf0pljm1615y7hjq0k4hi6zs98cna")))

(define-public crate-pest_derive-2.0.0 (c (n "pest_derive") (v "2.0.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (d #t) (k 0)))) (h "1k0xqykwdalai24nfjg703y03yk916hj6jlg54p9sm3g3wgiiiv6")))

(define-public crate-pest_derive-2.0.1 (c (n "pest_derive") (v "2.0.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_generator") (r "^2.0") (d #t) (k 0)))) (h "1rv7ql9gsdli3p3yq775nd1wnri1j07g8kizlqwwb6s18rqlfvxp")))

(define-public crate-pest_derive-2.1.0 (c (n "pest_derive") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.0") (d #t) (k 0)))) (h "1l5jfa6ril71cw5nsiw0r45br54dd8cj2r1nc2d1wq6wb3jilgc3")))

(define-public crate-pest_derive-2.2.0 (c (n "pest_derive") (v "2.2.0") (d (list (d (n "pest") (r "^2.2.0") (k 0)) (d (n "pest_generator") (r "^2.2.0") (k 0)))) (h "1dh3i73wvxcrjcagp4hsp1aj69ggnws3jd2d0v274cjrqqilnk1i") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pest_derive-2.2.1 (c (n "pest_derive") (v "2.2.1") (d (list (d (n "pest") (r "^2.2.1") (k 0)) (d (n "pest_generator") (r "^2.2.1") (k 0)))) (h "149bafz0anc905sqx7gyq8i92fqan5kdazg45k9ccczz79ip0ddi") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pest_derive-2.3.0 (c (n "pest_derive") (v "2.3.0") (d (list (d (n "pest") (r "^2.3.0") (k 0)) (d (n "pest_generator") (r "^2.3.0") (k 0)))) (h "14fs29n7rflv661sxv56kk9pdx202i449mpqq6c88lblyvvhhmwh") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.3.1 (c (n "pest_derive") (v "2.3.1") (d (list (d (n "pest") (r "^2.3.1") (k 0)) (d (n "pest_generator") (r "^2.3.1") (k 0)))) (h "1cb7gph5d6n81s5bs4z8zhali6j1yjs7y2py9yq7hlr4s2k64ash") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.4.0 (c (n "pest_derive") (v "2.4.0") (d (list (d (n "pest") (r "^2.4.0") (k 0)) (d (n "pest_generator") (r "^2.4.0") (k 0)))) (h "1ck53j665gqm5n6bin0xxfh0j5l11xswffxb9nrvqbk4p435gdv0") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.4.1 (c (n "pest_derive") (v "2.4.1") (d (list (d (n "pest") (r "^2.4.1") (k 0)) (d (n "pest_generator") (r "^2.4.1") (k 0)))) (h "13xilz89hxpsg85vx65l4x17jkd5cc0jn2xx6hnrb081a339pzfm") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.0 (c (n "pest_derive") (v "2.5.0") (d (list (d (n "pest") (r "^2.5.0") (k 0)) (d (n "pest_generator") (r "^2.5.0") (k 0)))) (h "0qvgwr99dqa4xix66960bk3aw69xfxy718w2nh17pqnn26h2ng22") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.1 (c (n "pest_derive") (v "2.5.1") (d (list (d (n "pest") (r "^2.5.1") (k 0)) (d (n "pest_generator") (r "^2.5.1") (k 0)))) (h "0i13nf9h50m86c78n9xdp8yx9daav0ch33r3xpa91zq61mh7ih6d") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.2 (c (n "pest_derive") (v "2.5.2") (d (list (d (n "pest") (r "^2.5.2") (k 0)) (d (n "pest_generator") (r "^2.5.2") (k 0)))) (h "00y6ca37ncnni4skdzm1iil5hrnwb1fbm7sgw76qs346m94l8l4n") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.3 (c (n "pest_derive") (v "2.5.3") (d (list (d (n "pest") (r "^2.5.3") (k 0)) (d (n "pest_generator") (r "^2.5.3") (k 0)))) (h "19m0wd2lcg6d2halnlfcgl0mfpgjy5a29q875vk6bp8c7cwxl714") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.4 (c (n "pest_derive") (v "2.5.4") (d (list (d (n "pest") (r "^2.5.4") (k 0)) (d (n "pest_generator") (r "^2.5.4") (k 0)))) (h "1sk5ilis2lad3z386qn7hmfm43rj899gwdyqcvdmj5aqs3i2dw4b") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.5 (c (n "pest_derive") (v "2.5.5") (d (list (d (n "pest") (r "^2.5.5") (k 0)) (d (n "pest_generator") (r "^2.5.5") (k 0)))) (h "0sabvkxwgpps47s54v2z39zgbp4igz7c2lqg10rhg939mhm95hra") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.6 (c (n "pest_derive") (v "2.5.6") (d (list (d (n "pest") (r "^2.5.6") (k 0)) (d (n "pest_generator") (r "^2.5.6") (k 0)))) (h "1mq7yzjrb9xxkzdi66fp9jqm106xg0h8zghm10s2f2ix7y38c4d8") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.5.7 (c (n "pest_derive") (v "2.5.7") (d (list (d (n "pest") (r "^2.5.7") (k 0)) (d (n "pest_generator") (r "^2.5.7") (k 0)))) (h "05cwx6p60wbh61p8pjyfxppzc20xf4fl6f82s2qnj9zwsb0w96dy") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (r "1.56")))

(define-public crate-pest_derive-2.6.0 (c (n "pest_derive") (v "2.6.0") (d (list (d (n "pest") (r "^2.6.0") (k 0)) (d (n "pest_generator") (r "^2.6.0") (k 0)))) (h "1yzv982pypxrr2zwhd08xv40plr474p155i26aj2anl63k3x8ybb") (f (quote (("std" "pest/std" "pest_generator/std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pest_derive-2.6.1 (c (n "pest_derive") (v "2.6.1") (d (list (d (n "pest") (r "^2.6.1") (k 0)) (d (n "pest_generator") (r "^2.6.1") (k 0)))) (h "1da5nxbqgvnvp39hm95zgngrcng7r7lgwyhrwncrz0q6jh7ijqvp") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-pest_derive-2.7.0 (c (n "pest_derive") (v "2.7.0") (d (list (d (n "pest") (r "^2.7.0") (k 0)) (d (n "pest_generator") (r "^2.7.0") (k 0)))) (h "0azzpyakmv4v07zk3jjkcqr9q843xsji3fpgl3sys3pspg4j7xmf") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.60")))

(define-public crate-pest_derive-2.7.1 (c (n "pest_derive") (v "2.7.1") (d (list (d (n "pest") (r "^2.7.1") (k 0)) (d (n "pest_generator") (r "^2.7.1") (k 0)))) (h "1lqmlgb102y4k4r9ryq7q31hcyby46g31yjxxafxi6d5wykvr52z") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.60")))

(define-public crate-pest_derive-2.7.2 (c (n "pest_derive") (v "2.7.2") (d (list (d (n "pest") (r "^2.7.2") (k 0)) (d (n "pest_generator") (r "^2.7.2") (k 0)))) (h "0lx88fs4hz0k8i2a9flv04h07yza032khn553l01bj2a1m4h0vb6") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.60")))

(define-public crate-pest_derive-2.7.3 (c (n "pest_derive") (v "2.7.3") (d (list (d (n "pest") (r "^2.7.3") (k 0)) (d (n "pest_generator") (r "^2.7.3") (k 0)))) (h "06h2r56g8xr4pcmlw93jcxjwg21kykihhgx387v1hyff4azfggm2") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.60")))

(define-public crate-pest_derive-2.7.4 (c (n "pest_derive") (v "2.7.4") (d (list (d (n "pest") (r "^2.7.4") (k 0)) (d (n "pest_generator") (r "^2.7.4") (k 0)))) (h "1n36kh9xd6kvs8vhrrhn8d1h3gmzw5w8zdacgjbhlh261mikyl9m") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

(define-public crate-pest_derive-2.7.5 (c (n "pest_derive") (v "2.7.5") (d (list (d (n "pest") (r "^2.7.5") (k 0)) (d (n "pest_generator") (r "^2.7.5") (k 0)))) (h "1hnzw8bcv4baf8z4dcfradi07fyakyyay75xnfiz4pjzd0j8bmw1") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

(define-public crate-pest_derive-2.7.6 (c (n "pest_derive") (v "2.7.6") (d (list (d (n "pest") (r "^2.7.6") (k 0)) (d (n "pest_generator") (r "^2.7.6") (k 0)))) (h "1pngjfj66x759llhxmpn2f4zxswj65lhx4ky094kmnxv6q9apmmw") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

(define-public crate-pest_derive-2.7.7 (c (n "pest_derive") (v "2.7.7") (d (list (d (n "pest") (r "^2.7.7") (k 0)) (d (n "pest_generator") (r "^2.7.7") (k 0)))) (h "0298vnf3lb5qvbdh6hxpx0vix344yx6zv6z6c4lld1kppn6jiq92") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

(define-public crate-pest_derive-2.7.8 (c (n "pest_derive") (v "2.7.8") (d (list (d (n "pest") (r "^2.7.8") (k 0)) (d (n "pest_generator") (r "^2.7.8") (k 0)))) (h "09hhvpbnfkng57zfh0mrz1rsmadnrlrvqf2pmjv6ml9z75r4zlmh") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

(define-public crate-pest_derive-2.7.9 (c (n "pest_derive") (v "2.7.9") (d (list (d (n "pest") (r "^2.7.9") (k 0)) (d (n "pest_generator") (r "^2.7.9") (k 0)))) (h "177n5d2bmkw7c0wdal65nfyd53w6gx6h3951rpp9f8fkasql2dgp") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (y #t) (r "1.61")))

(define-public crate-pest_derive-2.7.10 (c (n "pest_derive") (v "2.7.10") (d (list (d (n "pest") (r "^2.7.10") (k 0)) (d (n "pest_generator") (r "^2.7.10") (k 0)))) (h "0n8lsk9s21dp7958p9yarbk2gsc8wg0rvdzr7cd7pjpvjf8kqa96") (f (quote (("std" "pest/std" "pest_generator/std") ("not-bootstrap-in-src" "pest_generator/not-bootstrap-in-src") ("grammar-extras" "pest_generator/grammar-extras") ("default" "std")))) (r "1.61")))

