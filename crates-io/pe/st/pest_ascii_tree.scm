(define-module (crates-io pe st pest_ascii_tree) #:use-module (crates-io))

(define-public crate-pest_ascii_tree-0.1.0 (c (n "pest_ascii_tree") (v "0.1.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "escape_string") (r "^0.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1z38wybqg711x8y17ahm2dj0pav0zfiaxdgd7p6id64acf9v6lj1")))

