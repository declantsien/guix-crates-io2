(define-module (crates-io pe st pest_generator_tmp) #:use-module (crates-io))

(define-public crate-pest_generator_tmp-2.1.0 (c (n "pest_generator_tmp") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0) (p "pest_tmp")) (d (n "pest_meta") (r "^2.1.0") (d #t) (k 0) (p "pest_meta_tmp")) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0nhfk6pvcmrrbv9w2zyc03fiy2c28d4rlr2bldzbq82nm0fwng0l")))

