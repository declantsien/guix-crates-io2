(define-module (crates-io pe st pest-intermittent) #:use-module (crates-io))

(define-public crate-pest-intermittent-1.0.0 (c (n "pest-intermittent") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xv938179jv7zcf2cj4zw99vk3dwgsvvpmphahl9v7mg1kq9bbbs")))

(define-public crate-pest-intermittent-1.0.2 (c (n "pest-intermittent") (v "1.0.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r2gwxlfsz0v2rlh0zvrc6lanfg51zihjpm9ni9g237l1l9liwyf")))

