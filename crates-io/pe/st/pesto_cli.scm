(define-module (crates-io pe st pesto_cli) #:use-module (crates-io))

(define-public crate-pesto_cli-0.1.0 (c (n "pesto_cli") (v "0.1.0") (h "0rb79q16lgsd8k4nw7b29fz002rklsm5g2ab1y8fs1zikf07d0kq")))

(define-public crate-pesto_cli-0.2.0 (c (n "pesto_cli") (v "0.2.0") (h "13vpyk8sallp6qpw0lbi0z3mamlaff1q35vpz1nbv80qks952jl3")))

