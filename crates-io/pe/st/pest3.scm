(define-module (crates-io pe st pest3) #:use-module (crates-io))

(define-public crate-pest3-0.0.0 (c (n "pest3") (v "0.0.0") (h "047fjq4ii2y9ndvi2irbhyxj2vxg81kvpjx5k4cr7rdsdn7n9ajb")))

(define-public crate-pest3-0.0.0-prealpha0 (c (n "pest3") (v "0.0.0-prealpha0") (d (list (d (n "pest2") (r "^2.7.8") (d #t) (k 0) (p "pest")) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "11j9rzgs218cgyjb72h765k5bdn9g2yi0dr64s9ggh1c303zhmz6")))

