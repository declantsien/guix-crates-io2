(define-module (crates-io pe st pest_tree) #:use-module (crates-io))

(define-public crate-pest_tree-0.1.0 (c (n "pest_tree") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pmrv3dq49yi0zypjjwmxl275j3xfcydfzxshmvbk8b7k82vfi55")))

(define-public crate-pest_tree-0.2.0 (c (n "pest_tree") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0xvsi04ylzsykvzfvvq4shr4h35x6qbf0gghi7kcxdzag9qi1pln")))

(define-public crate-pest_tree-0.3.0 (c (n "pest_tree") (v "0.3.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1i3bmvhxm709133fv2p9npv50ii7clg6y2r0ygyi8h546g361q7b")))

(define-public crate-pest_tree-0.4.0 (c (n "pest_tree") (v "0.4.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "07xq5vkgl6gzahq5cad3hmd3ncw5rq3c7ygsz2grxb2snvnzvwwy")))

(define-public crate-pest_tree-0.4.1 (c (n "pest_tree") (v "0.4.1") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1clav9cngzz1kg22d71hwlcaxwhrmv2ih8qg57s38j6lsai8hd8j")))

(define-public crate-pest_tree-0.4.2 (c (n "pest_tree") (v "0.4.2") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1vjhwg4g8fyzbynz54sym9chxndb5cwxjbkfnry0lhjipwgvn9vc")))

(define-public crate-pest_tree-0.4.3 (c (n "pest_tree") (v "0.4.3") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_tree_derive") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08pnrrr8k0b99gf0hkg7xzshcjzmi1h50k17ql13xb7f26r7kmr7")))

