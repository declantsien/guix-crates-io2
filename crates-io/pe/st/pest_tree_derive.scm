(define-module (crates-io pe st pest_tree_derive) #:use-module (crates-io))

(define-public crate-pest_tree_derive-0.1.0 (c (n "pest_tree_derive") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1vfi731vryrhwx6w2f23dg4i1l496wmaazhm694cvrcspyxxgal3")))

(define-public crate-pest_tree_derive-0.2.0 (c (n "pest_tree_derive") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1k08fhn20kw99lxv9ydk6c1br5lwvfrja3y3mmxak88wk0jf10pd")))

(define-public crate-pest_tree_derive-0.3.0 (c (n "pest_tree_derive") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0rfy13h3sa86sh1qzm9j63vimmq8hdy2xgkcqxh2d404rlndvd5p")))

(define-public crate-pest_tree_derive-0.4.0 (c (n "pest_tree_derive") (v "0.4.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "184w1vw39n93711iibvfjfrrzq85cikzs4d0b1zx3q36dmkv78is")))

(define-public crate-pest_tree_derive-0.4.1 (c (n "pest_tree_derive") (v "0.4.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "00n8b56d23b4hlhy9r3yw76x1whihbvlja3nq1znkcivsqshhqm5")))

(define-public crate-pest_tree_derive-0.4.2 (c (n "pest_tree_derive") (v "0.4.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "03iq6khya7k99ijqgdsk19mh5kxabfx8rf76yi3vdf7r665627a3")))

(define-public crate-pest_tree_derive-0.4.3 (c (n "pest_tree_derive") (v "0.4.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0k6l7agk1rimabgfjc24yyikf59m01q2id0ips5hi8wm4ibqlmzs")))

