(define-module (crates-io pe st pest-deconstruct-derive) #:use-module (crates-io))

(define-public crate-pest-deconstruct-derive-0.2.0 (c (n "pest-deconstruct-derive") (v "0.2.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "single") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1n6avdrx5q134f32rzri6353s9cwkzsvggnqrv3k6mhp2y6cr4f7")))

