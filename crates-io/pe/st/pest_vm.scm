(define-module (crates-io pe st pest_vm) #:use-module (crates-io))

(define-public crate-pest_vm-2.0.0 (c (n "pest_vm") (v "2.0.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.0") (d #t) (k 0)))) (h "0afx53jzvwaavmd9272smaqhzvg8zl2kk306w73acfswsrlwwkdj")))

(define-public crate-pest_vm-2.1.0 (c (n "pest_vm") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1.0") (d #t) (k 0)))) (h "032b86g0hj0bqyajmxy695p8m4dyi7l8bapszx12njcgqia6fcsy")))

(define-public crate-pest_vm-2.2.0 (c (n "pest_vm") (v "2.2.0") (d (list (d (n "pest") (r "^2.2.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.2.0") (d #t) (k 0)))) (h "0c53hwy599whqpp84wpxnhmh52bsa3hjzi6fld9sc1fv25p6ia82") (y #t) (r "1.56")))

(define-public crate-pest_vm-2.2.1 (c (n "pest_vm") (v "2.2.1") (d (list (d (n "pest") (r "^2.2.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.2.1") (d #t) (k 0)))) (h "1w4amwkbil0s2slfg5jhl6n1xf3c06v0s5gfibia60mrnjsf7wgm") (y #t) (r "1.56")))

(define-public crate-pest_vm-2.3.0 (c (n "pest_vm") (v "2.3.0") (d (list (d (n "pest") (r "^2.3.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.3.0") (d #t) (k 0)))) (h "1mg54fv75gz0vrnbmv7i7fr5d04q1a7g8fvk3i3345b3hyp92vyi") (r "1.56")))

(define-public crate-pest_vm-2.3.1 (c (n "pest_vm") (v "2.3.1") (d (list (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.3.1") (d #t) (k 0)))) (h "03bmfyla8linzfqlxg0r1pagp8jq5ngwdllrqcyr9rd63nj3xi1x") (r "1.56")))

(define-public crate-pest_vm-2.4.0 (c (n "pest_vm") (v "2.4.0") (d (list (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.4.0") (d #t) (k 0)))) (h "1p56ahj9lwvqb9qrd9py72pwjlpp2j247qdh7liv4l8k7sj8jr96") (r "1.56")))

(define-public crate-pest_vm-2.4.1 (c (n "pest_vm") (v "2.4.1") (d (list (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.4.1") (d #t) (k 0)))) (h "119ax70vj6f014q3xbbrn3a74c1xp0pw8350i2lc1wfbks8ny5v0") (r "1.56")))

(define-public crate-pest_vm-2.5.0 (c (n "pest_vm") (v "2.5.0") (d (list (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.0") (d #t) (k 0)))) (h "13bnhgl49scp93690mvq6kz7mbxk283dy59k7g1ivj3i44gdws6h") (r "1.56")))

(define-public crate-pest_vm-2.5.1 (c (n "pest_vm") (v "2.5.1") (d (list (d (n "pest") (r "^2.5.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.1") (d #t) (k 0)))) (h "1vm2bdmifrlhi2p4kcgcr30w1pa76g25007scz9wz1py4ha95bid") (r "1.56")))

(define-public crate-pest_vm-2.5.2 (c (n "pest_vm") (v "2.5.2") (d (list (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.2") (d #t) (k 0)))) (h "136fnf6m64l9zjp9mc87fwvk50vgydn5qa5bpzjks7lqzhaglzgc") (r "1.56")))

(define-public crate-pest_vm-2.5.3 (c (n "pest_vm") (v "2.5.3") (d (list (d (n "pest") (r "^2.5.3") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.3") (d #t) (k 0)))) (h "0zjwpisf0s0lqb14v17jqan80cc7xa9p2bgip65ncq8wbiy7m7r4") (r "1.56")))

(define-public crate-pest_vm-2.5.4 (c (n "pest_vm") (v "2.5.4") (d (list (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.4") (d #t) (k 0)))) (h "0bf3qry21v9w4nax1d0jw5mlr1w8j1d8v3gk8hqnjza53x28fzm1") (r "1.56")))

(define-public crate-pest_vm-2.5.5 (c (n "pest_vm") (v "2.5.5") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.5") (d #t) (k 0)))) (h "18s2aiifyqf4nqp69736j0qvlqr1ijnsfi765fjqcsw6q3g1r41g") (r "1.56")))

(define-public crate-pest_vm-2.5.6 (c (n "pest_vm") (v "2.5.6") (d (list (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.6") (d #t) (k 0)))) (h "0k3byds0xl8kx03wrfbgj9aa2n2gbxarbqfkd7j6qv9clgbb803r") (r "1.56")))

(define-public crate-pest_vm-2.5.7 (c (n "pest_vm") (v "2.5.7") (d (list (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_meta") (r "^2.5.7") (d #t) (k 0)))) (h "1kmql1pv39rcj7dsmvmvkcazz8jrhd9vzv3bj3i45n0g9i7852mh") (r "1.56")))

(define-public crate-pest_vm-2.6.0 (c (n "pest_vm") (v "2.6.0") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.6.0") (d #t) (k 0)))) (h "0kv2969nr9xcxmc2fmyzigb3gmfpjlnx2mpg2wg3kwvjmq0w43wi") (y #t) (r "1.56")))

(define-public crate-pest_vm-2.6.1 (c (n "pest_vm") (v "2.6.1") (d (list (d (n "pest") (r "^2.6.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.6.1") (d #t) (k 0)))) (h "1s55h0wc8hkakg9n7wng6hz7wdiig05msgibw597ds9p0032jy9q") (y #t) (r "1.56")))

(define-public crate-pest_vm-2.7.0 (c (n "pest_vm") (v "2.7.0") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.0") (d #t) (k 0)))) (h "0m1l9b43d89bffd3pbvynf510z4zlr5p8d3xanr9kaq3p54anjk7") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.60")))

(define-public crate-pest_vm-2.7.1 (c (n "pest_vm") (v "2.7.1") (d (list (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.1") (d #t) (k 0)))) (h "0sjdb63yn5d6za6r50yf9jcgyhwslq8mmzi21cmvghyvynh0nn9w") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.60")))

(define-public crate-pest_vm-2.7.2 (c (n "pest_vm") (v "2.7.2") (d (list (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.2") (d #t) (k 0)))) (h "1rm46i4g3nni0jb3wd7d7x9cib1vqz448jrb8n3ayhbqn9l5azsa") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.60")))

(define-public crate-pest_vm-2.7.3 (c (n "pest_vm") (v "2.7.3") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.3") (d #t) (k 0)))) (h "02lb6ad78fgfrlhplv67q8axnwfh9sl312ng7mvl8l7f6549nzy6") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.60")))

(define-public crate-pest_vm-2.7.4 (c (n "pest_vm") (v "2.7.4") (d (list (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.4") (d #t) (k 0)))) (h "0ymbbgr174blwww37kwbjbn1rsjzkcj51p1bhjmp6ciqbfmq8bg4") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

(define-public crate-pest_vm-2.7.5 (c (n "pest_vm") (v "2.7.5") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.5") (d #t) (k 0)))) (h "0lslanhfl34b7296q82bw2cf629lx75g2d0kg6i7qscvnx0b9lhj") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

(define-public crate-pest_vm-2.7.6 (c (n "pest_vm") (v "2.7.6") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.6") (d #t) (k 0)))) (h "0dpiwypfp500czvkihzj3z0i2lr7p0af7spl7yzhc9za75pv7vr2") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

(define-public crate-pest_vm-2.7.7 (c (n "pest_vm") (v "2.7.7") (d (list (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.7") (d #t) (k 0)))) (h "0m7dzc1yll8kkwxk3b04zzcjj6qb7g84pzcqr0hdxbf2jphrwix6") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

(define-public crate-pest_vm-2.7.8 (c (n "pest_vm") (v "2.7.8") (d (list (d (n "pest") (r "^2.7.8") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.8") (d #t) (k 0)))) (h "0p83200zmfzj4k4inxa9kgh191666q1xb6jiijpnbkqbvqy7g1pn") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

(define-public crate-pest_vm-2.7.9 (c (n "pest_vm") (v "2.7.9") (d (list (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.9") (d #t) (k 0)))) (h "0acdmzh968v97rcsi64rb01qah2r3p7xiknb9jr0ahdynn6lhq9d") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (y #t) (r "1.61")))

(define-public crate-pest_vm-2.7.10 (c (n "pest_vm") (v "2.7.10") (d (list (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.10") (d #t) (k 0)))) (h "04cb5giyfln2nd349jdwixjsj8d2agjhh62fnyfgsc6616v5v61n") (f (quote (("grammar-extras" "pest_meta/grammar-extras")))) (r "1.61")))

