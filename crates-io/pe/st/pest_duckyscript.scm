(define-module (crates-io pe st pest_duckyscript) #:use-module (crates-io))

(define-public crate-pest_duckyscript-1.0.0 (c (n "pest_duckyscript") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "0pm55lsa008zsjzcgr6v9jss42z3zjhs8pp0mivw0lsyyyaxk91d")))

(define-public crate-pest_duckyscript-2.0.0 (c (n "pest_duckyscript") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "0kgvwbxz7rlsrwvbk37j80xlxbkriqh5nqnpsbyba2hh7nfkkmpg")))

(define-public crate-pest_duckyscript-3.0.0 (c (n "pest_duckyscript") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1plbkyyd5rjg1vy9mz7wx4xdzzs25xb28nav6ax1ma1f1ns08b7h")))

(define-public crate-pest_duckyscript-4.0.0 (c (n "pest_duckyscript") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1msgv5zqj3m781dbvw7jy9n4h8fly3p53akalqmgdjivxaczr4c2")))

(define-public crate-pest_duckyscript-5.0.0 (c (n "pest_duckyscript") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1a1gkm7p6dfznbyzb6v7hl2fsgjmix2krlx3mbl30f00051d7yl1")))

(define-public crate-pest_duckyscript-6.0.0 (c (n "pest_duckyscript") (v "6.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1mwfkwr598lqgl580m92yz1yf49sdmysb5867ljfhz62zmjw3mrp")))

(define-public crate-pest_duckyscript-8.0.0 (c (n "pest_duckyscript") (v "8.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1bvk5r13i9ik52416z9f87i4acjgk776ylc225k7zqkadkbpd8yj")))

(define-public crate-pest_duckyscript-9.0.0 (c (n "pest_duckyscript") (v "9.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1mnj7fb1b9sq7lmj6w9k7jl8wx0d22rcdrdfnw9smcb3382q9nl9")))

(define-public crate-pest_duckyscript-10.0.0 (c (n "pest_duckyscript") (v "10.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "13bbwry32icprqg33j0h31qgkrmn68frp39wvz74xv2ya5dy79g0")))

(define-public crate-pest_duckyscript-11.0.0 (c (n "pest_duckyscript") (v "11.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0l0arai9p694a6mcfssajqgwq71vvgpmmv56s45fg2il700iqgg9")))

(define-public crate-pest_duckyscript-12.0.0 (c (n "pest_duckyscript") (v "12.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0zayd4v8cd602whvq6cfxlfp7bz8zbzg220qqw4r9cr5nv4nfcsi")))

(define-public crate-pest_duckyscript-13.0.0 (c (n "pest_duckyscript") (v "13.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1l3a45rh47bqfnl97jsxdvmlv51cijvbsf0njxf69w83bxwqi5yr")))

(define-public crate-pest_duckyscript-14.0.0 (c (n "pest_duckyscript") (v "14.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0sfw35by4nl0cziljn4630fzk9ndamwjmxrzxi2sss57b0vp806r")))

