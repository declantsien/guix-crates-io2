(define-module (crates-io pe st pest_extra) #:use-module (crates-io))

(define-public crate-pest_extra-0.1.0 (c (n "pest_extra") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_fmt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "pest_meta") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_vm") (r "^2.7.5") (o #t) (d #t) (k 0)))) (h "09byk8pz91d1awb0gsw45r9sr3mdv6ahnzlmvybdgjnpv2bgbxz2") (s 2) (e (quote (("vm" "dep:pest_vm") ("formatter" "dep:pest_fmt"))))))

