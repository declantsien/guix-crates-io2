(define-module (crates-io pe st pest3_generator) #:use-module (crates-io))

(define-public crate-pest3_generator-0.0.0 (c (n "pest3_generator") (v "0.0.0") (h "1z7dvmcz5cvyhngk88lk14wp28bz5b23pz7l143xdfk32cz4zgl8")))

(define-public crate-pest3_generator-0.0.0-prealpha0 (c (n "pest3_generator") (v "0.0.0-prealpha0") (d (list (d (n "pest3") (r "=0.0.0-prealpha0") (d #t) (k 0)) (d (n "pest3_meta") (r "=0.0.0-prealpha0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0r0jh6yp2qw1sbw5yam35qjlrn0h416hmi7xxca4cn0rhr6fqllc")))

