(define-module (crates-io pe st pest3_meta) #:use-module (crates-io))

(define-public crate-pest3_meta-0.0.0 (c (n "pest3_meta") (v "0.0.0") (h "1al409ywxc07szr6qlp4h0fjx1jppp2yp6q9ksaxymgs1s24yqkk")))

(define-public crate-pest3_meta-0.0.0-prealpha0 (c (n "pest3_meta") (v "0.0.0-prealpha0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1bbr4kiii2y5h360ag5jb77vxn60fxgjrasfdfafh6q7bjl1jygn")))

