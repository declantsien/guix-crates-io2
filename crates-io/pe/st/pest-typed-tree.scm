(define-module (crates-io pe st pest-typed-tree) #:use-module (crates-io))

(define-public crate-pest-typed-tree-0.1.0 (c (n "pest-typed-tree") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 2)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 2)) (d (n "pest_meta") (r "^2.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18jk70xrqwslzxdssfr4n26cvfqd1fhmdx5fj1cgpsxdz0lf2d23")))

(define-public crate-pest-typed-tree-0.2.0 (c (n "pest-typed-tree") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 2)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 2)) (d (n "pest_meta") (r "^2.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "06c23y1v47bdgl7v78jn6w2zzax9j1q7s0gdk9qngwgckx0yk9v8")))

(define-public crate-pest-typed-tree-0.2.1 (c (n "pest-typed-tree") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 2)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 2)) (d (n "pest_meta") (r "^2.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "09hahd2svxdh204qx8zbpq19rrwhk5m43vdv4q9dwhj3idsknig3")))

