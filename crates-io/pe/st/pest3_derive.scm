(define-module (crates-io pe st pest3_derive) #:use-module (crates-io))

(define-public crate-pest3_derive-0.0.0 (c (n "pest3_derive") (v "0.0.0") (h "1af6j7x1zln1iv7hbi4mv8rf1gh7hswrk4g816pid8gfjf16a1cr")))

(define-public crate-pest3_derive-0.0.0-prealpha0 (c (n "pest3_derive") (v "0.0.0-prealpha0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "pest3") (r "=0.0.0-prealpha0") (d #t) (k 2)) (d (n "pest3_generator") (r "=0.0.0-prealpha0") (d #t) (k 0)))) (h "1mphfwrdkyj3h8qyw4i03mlqq3r1hxmdnnc8dv0p57r7883ckza4")))

