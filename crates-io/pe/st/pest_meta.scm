(define-module (crates-io pe st pest_meta) #:use-module (crates-io))

(define-public crate-pest_meta-2.0.0 (c (n "pest_meta") (v "2.0.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "00cs6rq9yy6qn6pyqykm4l80jcphznqf1s2ywwc3y6c7hn161vwv")))

(define-public crate-pest_meta-2.0.1 (c (n "pest_meta") (v "2.0.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "123vmxsqv949vs2fjscdwfk9v9mwnmlhmxxjl7788vvw8cbsvk32")))

(define-public crate-pest_meta-2.0.2 (c (n "pest_meta") (v "2.0.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 1)))) (h "122z3ccy1z3qnzhx6b4i3mxjzh4hbb4y588a3q59f7b7scl892rj") (y #t)))

(define-public crate-pest_meta-2.0.3 (c (n "pest_meta") (v "2.0.3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 1)))) (h "08c29rqnvi4vmimc1kk8yziji0y31mw7s6dghs65g0k01rpmyv8z")))

(define-public crate-pest_meta-2.1.0 (c (n "pest_meta") (v "2.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 1)))) (h "0ppv7szkkpzh086pv1sq9w8f6nx9l9xwrkdd8z1gy26j9qm4k8zm")))

(define-public crate-pest_meta-2.1.1 (c (n "pest_meta") (v "2.1.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 1)))) (h "0kaprdz3jis9bjfwhri1zncbsvack5m3gx2g5flspdy7wxnyljgj")))

(define-public crate-pest_meta-2.1.2 (c (n "pest_meta") (v "2.1.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (k 1)))) (h "0iymvrh7lcfi8iarkgq0hwgarr00np3l4xg4bx42rmvgi6czshyz")))

(define-public crate-pest_meta-2.1.3 (c (n "pest_meta") (v "2.1.3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (k 1)))) (h "07d1jbbbpxpchk0j37ljas46sdyyg599z3zw2ac0f5sk9x06xgjl")))

(define-public crate-pest_meta-2.2.0 (c (n "pest_meta") (v "2.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.2.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 1)))) (h "19p35p77f7gndf2r53rxdsylifhmm5c7imq8za38v2ay58d39j0m") (y #t) (r "1.56")))

(define-public crate-pest_meta-2.2.1 (c (n "pest_meta") (v "2.2.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.2.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 1)))) (h "1zi1x07aarwjyhvl1vnflb7ncawnin50m2x1qylazg9vxvjjpcsy") (y #t) (r "1.56")))

(define-public crate-pest_meta-2.3.0 (c (n "pest_meta") (v "2.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 1)))) (h "014c3qj1rjlh36sisc5lqm2a6m3ckh463c4ak9nmqq879xwfnf0m") (r "1.56")))

(define-public crate-pest_meta-2.3.1 (c (n "pest_meta") (v "2.3.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "19h1g36hh7bjwk61p8lk56rflzxrmrp93hizzjfjcpn4f4n1dv5w") (r "1.56")))

(define-public crate-pest_meta-2.4.0 (c (n "pest_meta") (v "2.4.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "13dc16mnawsan600di34gr1d134bqd3gwr2sarg52ycvgy91g1sc") (r "1.56")))

(define-public crate-pest_meta-2.4.1 (c (n "pest_meta") (v "2.4.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "1zj6hb54bq5qga8rl02arp67jf3ibksb69mydyrndp91gszljiw2") (r "1.56")))

(define-public crate-pest_meta-2.5.0 (c (n "pest_meta") (v "2.5.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "0qnncing3sv2syswv7kjrlcrqs7di1f4m1xyd2dghcx1y28rp5ap") (r "1.56")))

(define-public crate-pest_meta-2.5.1 (c (n "pest_meta") (v "2.5.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "082g82sagzygm7vhgkn02wh76scpg9y0rz5fq73f0apwb5fn1j7y") (r "1.56")))

(define-public crate-pest_meta-2.5.2 (c (n "pest_meta") (v "2.5.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 1)))) (h "0rg0ci2af4hajpyyc31s0yd387n3a8j9z2i77s20mqwqb2vrhhlq") (r "1.56")))

(define-public crate-pest_meta-2.5.3 (c (n "pest_meta") (v "2.5.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0lbskklh77xnyk1yr8c387l80s37s30lrfv636s7hild58rz3x0f") (r "1.56")))

(define-public crate-pest_meta-2.5.4 (c (n "pest_meta") (v "2.5.4") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0payj5z6nh04i198jdsac8c7y8x0aqp3r669kwsmd5w5q5vvc0lz") (r "1.56")))

(define-public crate-pest_meta-2.5.5 (c (n "pest_meta") (v "2.5.5") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "05lnqn0ahzs9nvg3m45p1q90i4a3s4wqyz1h1hm7wms1llmhpxp6") (r "1.56")))

(define-public crate-pest_meta-2.5.6 (c (n "pest_meta") (v "2.5.6") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "106cq75hh316lx91n4r1q8vp338vlmczy2n9xg2hv8hk3x5jhfsy") (r "1.56")))

(define-public crate-pest_meta-2.5.7 (c (n "pest_meta") (v "2.5.7") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "17imbybjh280k3z0kdfqmr1051s7l09jzr50zmcq8ggzghy0fcv7") (r "1.56")))

(define-public crate-pest_meta-2.6.0 (c (n "pest_meta") (v "2.6.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "04ala2f51zxninvajvvqk5gq80qxyp2v6cpfv3zkj7mpiqplankl") (y #t) (r "1.56")))

(define-public crate-pest_meta-2.6.1 (c (n "pest_meta") (v "2.6.1") (d (list (d (n "cargo") (r "^0.62") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.6.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "1xkdrdjqa4x6z6j35r4qc9fsq41l1mzwq0c9qlmz5wwwzh632z25") (f (quote (("not-bootstrap-in-src" "cargo") ("default")))) (y #t) (r "1.56")))

(define-public crate-pest_meta-2.7.0 (c (n "pest_meta") (v "2.7.0") (d (list (d (n "cargo") (r "^0.70") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "180cxbbq4amfa0zdn1gw6c76c54yj13bj56z691bk2xx835p27x0") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.60")))

(define-public crate-pest_meta-2.7.1 (c (n "pest_meta") (v "2.7.1") (d (list (d (n "cargo") (r "^0.70") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0hdk6sqqak83cni837wrsrq8k1z5m8vvb49004v90kabpdpccx16") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.60")))

(define-public crate-pest_meta-2.7.2 (c (n "pest_meta") (v "2.7.2") (d (list (d (n "cargo") (r "^0.70") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0j0a882n1rxf3ljhsydglnhiddbrr58x1ngn1df49l3lmwq0mbsn") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.60")))

(define-public crate-pest_meta-2.7.3 (c (n "pest_meta") (v "2.7.3") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0pxw9rikri1ic6yz5283976d4cz5ja8fiqd57hsk6ghjsfa06bxl") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.60")))

(define-public crate-pest_meta-2.7.4 (c (n "pest_meta") (v "2.7.4") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "13bhqvlfha69m6mivy8svrzrfkjrsf5hqz0fk3mkq1f4gsg4xxqx") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.5 (c (n "pest_meta") (v "2.7.5") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "1rlx167d2j2mgq5kjj4nxzkzz6m3h8g8yyf8p554m7mdsj8p2x3w") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.6 (c (n "pest_meta") (v "2.7.6") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "0zabpc2mvpjmdqx7jzfghmrsxkywvhf9967q3a6vflwy4psn5zx7") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.7 (c (n "pest_meta") (v "2.7.7") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "12ks0yvmhaz5dhgzac3s93bkl97dgq655nnb78mc4j7i0xllv4yh") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.8 (c (n "pest_meta") (v "2.7.8") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "14z2b94sqmbi2302yp1lp6zzy4b7bxfq6via6ms4c3q53iixfk4k") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.9 (c (n "pest_meta") (v "2.7.9") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "1jpdriddnqaa6z0ls3gyhmal695bh63kjdg8xk52hvvpp6dz5nra") (f (quote (("grammar-extras") ("default")))) (y #t) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

(define-public crate-pest_meta-2.7.10 (c (n "pest_meta") (v "2.7.10") (d (list (d (n "cargo") (r "^0.72.2") (o #t) (d #t) (k 1)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 1)))) (h "1kdxl164yyjsmn01lvllsll4sz3xbgy4dmkq33n63hrp5w1418np") (f (quote (("grammar-extras") ("default")))) (s 2) (e (quote (("not-bootstrap-in-src" "dep:cargo")))) (r "1.61")))

