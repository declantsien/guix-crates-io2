(define-module (crates-io pe st pesta) #:use-module (crates-io))

(define-public crate-pesta-0.1.0 (c (n "pesta") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_meta") (r "^2.1") (d #t) (k 0)) (d (n "pest_vm") (r "^2.1") (d #t) (k 0)))) (h "0pam924aiwzyyvfbw1z9k6iyfw65v74vcx3pxq1jbd3892y71qc3")))

