(define-module (crates-io pe st pest_grammars) #:use-module (crates-io))

(define-public crate-pest_grammars-1.0.0-beta.1 (c (n "pest_grammars") (v "1.0.0-beta.1") (d (list (d (n "pest") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta.1") (d #t) (k 0)))) (h "0aa6f453bs80zmrhys97z0ncpp4nr291lal9vmplbclc1i9k24m0")))

(define-public crate-pest_grammars-1.0.0-beta.2 (c (n "pest_grammars") (v "1.0.0-beta.2") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1r9yq76yr44g8mnzs8k4ch6znylisirxd6jrvpl98nji30xslzdy")))

(define-public crate-pest_grammars-1.0.0-beta.3 (c (n "pest_grammars") (v "1.0.0-beta.3") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "00rh5nbp1291dvbzrxr701qkqm4nl9l1cya0h1cyl8xfgng6pajn")))

(define-public crate-pest_grammars-1.0.0-beta.4 (c (n "pest_grammars") (v "1.0.0-beta.4") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1yy47y5ixwmxn1d74z659snnii7ifqzyg1bkkvsvqzlvcr12j6pd")))

(define-public crate-pest_grammars-1.0.0-beta.5 (c (n "pest_grammars") (v "1.0.0-beta.5") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1n2s0ala7bi1hf0apya27a6bh09inl4hc6cmixv2wblp5a7rlqnp")))

(define-public crate-pest_grammars-1.0.0-beta.6 (c (n "pest_grammars") (v "1.0.0-beta.6") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0n3qmgwi6q5zpnqd931rk13zsikh5hax796qld9jalfrq4qj9hrp")))

(define-public crate-pest_grammars-1.0.0-beta.7 (c (n "pest_grammars") (v "1.0.0-beta.7") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1lf0xlvjg22gf3x2kri4pc3fq6yyyw4q2g7zm1cf8raxj89qhnr9")))

(define-public crate-pest_grammars-1.0.0-beta.8 (c (n "pest_grammars") (v "1.0.0-beta.8") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "061ch6z1fw602mg9j2gs75djbqpnk3x1d426f4yb01v4d60k4gja")))

(define-public crate-pest_grammars-1.0.0-beta.9 (c (n "pest_grammars") (v "1.0.0-beta.9") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1y5szpvbc3vlahzmrr4525kc5qr4hm6894b5y13z8lln7vzyfhkp")))

(define-public crate-pest_grammars-1.0.0-beta.10 (c (n "pest_grammars") (v "1.0.0-beta.10") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0mi6dadwqij2cwhzk0wrgpi1vm104sllp1vvj2w7wkwi2xc8k83s")))

(define-public crate-pest_grammars-1.0.0-beta.11 (c (n "pest_grammars") (v "1.0.0-beta.11") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "14cbz9ywskldmyn0aikbjmxy5rns6lhydxwinlj6454jgza0yg5b")))

(define-public crate-pest_grammars-1.0.0-beta.12 (c (n "pest_grammars") (v "1.0.0-beta.12") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1lgavv7gz21vzxr86m3617c1z46aiyrdcz7x5biayp7bxkm7s0h7")))

(define-public crate-pest_grammars-1.0.0-beta.13 (c (n "pest_grammars") (v "1.0.0-beta.13") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0kg43w6fx9vqyph7774hwf7qpn621alkvb0b3csxg7q6bh1g9vn1")))

(define-public crate-pest_grammars-1.0.0-beta.14 (c (n "pest_grammars") (v "1.0.0-beta.14") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0cn5mzyx4dij2ib9s0kf35savl6yyvhfyxvbya3qkrwgvnc90daf")))

(define-public crate-pest_grammars-1.0.0-beta.15 (c (n "pest_grammars") (v "1.0.0-beta.15") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0qcb3dbnnc1hggpv3slk0jbz22zh219p2hvjij08zf9476kgap73")))

(define-public crate-pest_grammars-1.0.0-beta.16 (c (n "pest_grammars") (v "1.0.0-beta.16") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1z9ckj8ml86b2agb4p1mk5wwiqpmjm1q4aq9xzw86a510cvg1fvn")))

(define-public crate-pest_grammars-1.0.0-beta.17 (c (n "pest_grammars") (v "1.0.0-beta.17") (d (list (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1346gxn7xn1ihqnrip2vw3f0d1nsc8nwa663vh8vd35qyxkzmxd8")))

(define-public crate-pest_grammars-1.0.0-rc.0 (c (n "pest_grammars") (v "1.0.0-rc.0") (d (list (d (n "pest") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-rc") (d #t) (k 0)))) (h "12x0cpnmhapyn8zgjrq6p7i9qzzhvw7p8zav7nnszxv7f1gyiakl")))

(define-public crate-pest_grammars-1.0.0-rc.1 (c (n "pest_grammars") (v "1.0.0-rc.1") (d (list (d (n "pest") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-rc") (d #t) (k 0)))) (h "1lxpfzzfdb7hr4184imsswiw5bg312c8f82ivx29r0mmgnbjcpp3")))

(define-public crate-pest_grammars-1.0.0 (c (n "pest_grammars") (v "1.0.0") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1n98175483y6psk90fg4lsjd1inn0q30xl68ym0086kwz40dciq9")))

(define-public crate-pest_grammars-1.0.1 (c (n "pest_grammars") (v "1.0.1") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "0gk5jmdsakhi8amkm6f4h339lnx38mxivm918byx8kzr9k15lprg")))

(define-public crate-pest_grammars-1.0.2 (c (n "pest_grammars") (v "1.0.2") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "19b8lggxi8332l0hpyaqfbhw814z97shiyb5s2iv0haa5rng0a53")))

(define-public crate-pest_grammars-1.0.3 (c (n "pest_grammars") (v "1.0.3") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1b7fyzwiyblnmwnnmd2gdb8jl8fhnawx7f7m704xks8jzvxk0rgm")))

(define-public crate-pest_grammars-1.0.4 (c (n "pest_grammars") (v "1.0.4") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "0d1ri33m8pmqs8iprgplh0fynih1f4blssjjrdapfx68li30s40m")))

(define-public crate-pest_grammars-1.0.5 (c (n "pest_grammars") (v "1.0.5") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1d9i4rckhv96iqjl8bkcmr5nwzl7x9bdp50phyr7y8xzyqg6gh4q") (y #t)))

(define-public crate-pest_grammars-1.0.6 (c (n "pest_grammars") (v "1.0.6") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "0d51pf65bbsvli1l579i736acarv6kiz4rx4cr7wxi4156idd8bi")))

(define-public crate-pest_grammars-2.0.0 (c (n "pest_grammars") (v "2.0.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1i609fd8g0h00v68hvdcmkb0nkg3dg3rrsr67kac22vhxsks7220")))

(define-public crate-pest_grammars-2.1.0 (c (n "pest_grammars") (v "2.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1xjnv6h4c3rvhzfaqxcjdc2pls0qvrxm480sk0zn889swjf8m7gv")))

(define-public crate-pest_grammars-2.2.0 (c (n "pest_grammars") (v "2.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.2.0") (d #t) (k 0)))) (h "1lq5x6fv3nczpiparp95a040yq7gw38bzhgw752jpg0qxj2hyc17") (y #t) (r "1.56")))

(define-public crate-pest_grammars-2.2.1 (c (n "pest_grammars") (v "2.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.2.1") (d #t) (k 0)))) (h "0dibyylicpj7k6k4yna5svh13r5cdzgjiqlndq0a6rzafrpknbzh") (y #t) (r "1.56")))

(define-public crate-pest_grammars-2.3.0 (c (n "pest_grammars") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.3.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)))) (h "00rmi25dmm32jzpaagaiwz0lk79pnd1qj1rqmlq0xv5f6bm9lhjf") (r "1.56")))

(define-public crate-pest_grammars-2.3.1 (c (n "pest_grammars") (v "2.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)))) (h "1d43kfrw2i14xscshix2wyl6kakcdmc306b3v97dylr7xc3ib29w") (r "1.56")))

(define-public crate-pest_grammars-2.4.0 (c (n "pest_grammars") (v "2.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)))) (h "147m247xn71h3a0fisq23a1xc9s5hl6h2bp45a6qlknv60i1gsld") (r "1.56")))

(define-public crate-pest_grammars-2.4.1 (c (n "pest_grammars") (v "2.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)))) (h "1m6jmdinzs8ckbwqm690rxjfha0a4g97mwwqgw63w4g6v3z1gxqb") (r "1.56")))

(define-public crate-pest_grammars-2.5.0 (c (n "pest_grammars") (v "2.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 0)))) (h "0qmkgcpzh23cd100y6lks7pqgc2y0mwhc1f05gf37f8h6aw7n0cy") (r "1.56")))

(define-public crate-pest_grammars-2.5.1 (c (n "pest_grammars") (v "2.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.1") (d #t) (k 0)))) (h "0519q1f6izf3k5v9g52gb72ig0izrf8rxmblm9kqd4qfr980f8dk") (r "1.56")))

(define-public crate-pest_grammars-2.5.2 (c (n "pest_grammars") (v "2.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)))) (h "1kpzli33wv2qrd69hcspkfkg12i4zwd5h80y7bva1pbnk5rby876") (r "1.56")))

(define-public crate-pest_grammars-2.5.3 (c (n "pest_grammars") (v "2.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.3") (d #t) (k 0)))) (h "10pfx16f8951hl2r9c22hyzr6nrl0p8rciwfscxahbzr90gjn2rh") (r "1.56")))

(define-public crate-pest_grammars-2.5.4 (c (n "pest_grammars") (v "2.5.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1pypvhxgsl3ylf2g7znyj79zif6i3k7qr42dzvcqg6ilvyiib378") (r "1.56")))

(define-public crate-pest_grammars-2.5.5 (c (n "pest_grammars") (v "2.5.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1rhhy97bvp8mjf8hij8j9flg4894jjpfi256wqd21w3lni8rpfrl") (r "1.56")))

(define-public crate-pest_grammars-2.5.6 (c (n "pest_grammars") (v "2.5.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0nn564p60b7bsjz4ng96lh3yqq608ndrzfha56b5bk42qyk4x0w8") (r "1.56")))

(define-public crate-pest_grammars-2.5.7 (c (n "pest_grammars") (v "2.5.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0cz5dfi3ziq61nw8h7snk1gh1xzh9hng77shhpr75y9zmyqy64jc") (r "1.56")))

(define-public crate-pest_grammars-2.6.0 (c (n "pest_grammars") (v "2.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "06kp5wf50fqzmfkc0qwh8k3s6d97pnpx9vkf113r63cdb2nqhrh5") (y #t) (r "1.56")))

(define-public crate-pest_grammars-2.6.1 (c (n "pest_grammars") (v "2.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.6.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0bsmza5ax88zq71irs1ap42jd0xbhnply01fipdbww1jcxxb345p") (y #t) (r "1.56")))

(define-public crate-pest_grammars-2.7.0 (c (n "pest_grammars") (v "2.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1k47x25izdga6sq23i7m10mkpgkms20rh0z9gm1grzkmablv9f90") (r "1.60")))

(define-public crate-pest_grammars-2.7.1 (c (n "pest_grammars") (v "2.7.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0nz1gh922h531q61dgba32kjl69fjz7824vh4f1qpzflrisks4v4") (r "1.60")))

(define-public crate-pest_grammars-2.7.2 (c (n "pest_grammars") (v "2.7.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0r58acrr1bx08jl1vqrknmx8184ni15p348igmrxg16r2r8y12gv") (r "1.60")))

(define-public crate-pest_grammars-2.7.3 (c (n "pest_grammars") (v "2.7.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1vrja80gaf8fskkgd4b9fb3vh2mvganf6brlc9dian2c1alxxsp7") (r "1.60")))

(define-public crate-pest_grammars-2.7.4 (c (n "pest_grammars") (v "2.7.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "18qa81p1sfp4wimigbd3kiqbs9jmcfhj7ma6j82lha2bnq5b93sj") (r "1.61")))

(define-public crate-pest_grammars-2.7.5 (c (n "pest_grammars") (v "2.7.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "10jqgcnvl1jzv3b2wzk2g1vfjw8jsld4wj6hlb2hynj5x3j5ks0v") (r "1.61")))

(define-public crate-pest_grammars-2.7.6 (c (n "pest_grammars") (v "2.7.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "101885vzjffr41ai7si00ajpl62jz60js8whj5vcl888z5w649rp") (r "1.61")))

(define-public crate-pest_grammars-2.7.7 (c (n "pest_grammars") (v "2.7.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0iqmr9cv7794bqipivds6cpvksffkxmixqz2nw7cny0b75d6vbaw") (r "1.61")))

(define-public crate-pest_grammars-2.7.8 (c (n "pest_grammars") (v "2.7.8") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.7.8") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1l7am9sc06fq1bppks2yxmxywdy3yssksykfdfmxbqpsinfrk9qj") (r "1.61")))

(define-public crate-pest_grammars-2.7.9 (c (n "pest_grammars") (v "2.7.9") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0zvd07h0kr18sxl846jiy15v1zabrzxx3r720mkql9hyg8rirhvv") (y #t) (r "1.61")))

(define-public crate-pest_grammars-2.7.10 (c (n "pest_grammars") (v "2.7.10") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1yp78c3v7zdq5qyjcqyxz70i4ziz4zbv9dbd90sm5v6mbc2plcf4") (r "1.61")))

