(define-module (crates-io pe st pest_consume_macros) #:use-module (crates-io))

(define-public crate-pest_consume_macros-1.0.0 (c (n "pest_consume_macros") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0a52nrgkw0r9fdbak7nq7v2zjy0n1j5dkn249lffd8v96yk1lw8y")))

(define-public crate-pest_consume_macros-1.0.1 (c (n "pest_consume_macros") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "183dxvq2aqp90vdsqc7if723qypaljgs1wimcd239h3jyxjkmp1n")))

(define-public crate-pest_consume_macros-1.0.2 (c (n "pest_consume_macros") (v "1.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "12ckk2vcrqx77i2rjsx8w78wg9zw6xzhlga9pd8laxgh6k2mpn4q")))

(define-public crate-pest_consume_macros-1.0.3 (c (n "pest_consume_macros") (v "1.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0dp4069mnfbzz55ah4j3kq8njvmvh5fn1x4fasa2pic2fvd9a8dq")))

(define-public crate-pest_consume_macros-1.0.4 (c (n "pest_consume_macros") (v "1.0.4") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1xrc59qvlm3sbw5aiq4m1f4663mwawzi17hgnaw1y612n7rc4nk4")))

(define-public crate-pest_consume_macros-1.0.5 (c (n "pest_consume_macros") (v "1.0.5") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0kibq9vpxx4zbyy8ryir4l3yg4j3zl5namadw2jkfh4n4xich4a2")))

(define-public crate-pest_consume_macros-1.0.6 (c (n "pest_consume_macros") (v "1.0.6") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgahv4b38afqzpv4q5aaw0pn9izppf07hac62sw07krhj8ylva6")))

(define-public crate-pest_consume_macros-1.1.0 (c (n "pest_consume_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0a9zg5zishafz0hhmp2byfd04h22naka0sy1q5739jwrm2kk11lx")))

