(define-module (crates-io pe gy pegy-derive) #:use-module (crates-io))

(define-public crate-pegy-derive-0.1.0 (c (n "pegy-derive") (v "0.1.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13nk6kaszdfnyj71gd0f9d057945bg4a5jlq14xail55yp5xbjiy") (r "1.75")))

(define-public crate-pegy-derive-0.1.1 (c (n "pegy-derive") (v "0.1.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rp5y81rc19zwq27wmijbllq7ba71kymk0z3hyymq8vjr3ixqx2l") (r "1.75")))

(define-public crate-pegy-derive-0.1.2 (c (n "pegy-derive") (v "0.1.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fj2hn3nbjl8kcsa4r0nkv7iwq68lrhsi5f49c08kblqx6nd1v9f") (r "1.75")))

