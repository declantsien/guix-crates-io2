(define-module (crates-io pe gy pegy) #:use-module (crates-io))

(define-public crate-pegy-0.1.0 (c (n "pegy") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pegy-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1vk8frwq9586z9xyian6iffbvx0rbgpv6r6z8dalwyfn4p1qwqav") (f (quote (("default" "futures")))) (y #t) (s 2) (e (quote (("futures" "dep:futures")))) (r "1.75")))

(define-public crate-pegy-0.1.1 (c (n "pegy") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pegy-derive") (r "^0.1.1") (d #t) (k 0)))) (h "06ysaixnbp91mvrccwyadz8k2lw03y5acnkqpifyxs04bqbv75q1") (f (quote (("default" "futures")))) (s 2) (e (quote (("futures" "dep:futures")))) (r "1.75")))

(define-public crate-pegy-0.1.2 (c (n "pegy") (v "0.1.2") (d (list (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hexf-parse") (r "^0.1") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "lexical-core") (r "^0.7") (d #t) (k 2)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "pegy-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 2)))) (h "14bn3fnr4pbrswcn9hmb8q8va0ylxqjcs65niaj9g8lrpjmnp0as") (f (quote (("unicode") ("default" "futures" "unicode")))) (s 2) (e (quote (("futures" "dep:futures")))) (r "1.75")))

