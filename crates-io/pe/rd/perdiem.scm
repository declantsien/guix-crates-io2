(define-module (crates-io pe rd perdiem) #:use-module (crates-io))

(define-public crate-perDiem-0.1.0 (c (n "perDiem") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "cond_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "15srwkw9mzx2cz8zmxp8yj629rba62xwkkf795yzvn0jls0vpfb0")))

(define-public crate-perDiem-0.1.1 (c (n "perDiem") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "18fzg1jyvmq60bmzxzg5lc5pd4dl4h8rcxf2z8k1wc8hgaicavjq")))

(define-public crate-perDiem-0.0.2 (c (n "perDiem") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1z7bq5pz59n4qblxlcvyc3p59bmdxqj4421dlkg2j7q6viw9d8z9") (y #t)))

(define-public crate-perDiem-0.1.2 (c (n "perDiem") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "17fhivhj793bky9alw6vqzqs29lfj2cy7rxg7c3a9hw2kvz3vshc")))

(define-public crate-perDiem-0.1.3 (c (n "perDiem") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1igxgcb6anyb0r50rn98nrl2aca49pcd5d7wjg9rv0lvj15lypjg")))

(define-public crate-perDiem-0.1.4 (c (n "perDiem") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1sxm9mh58nc5hk2mldiy00fjv7nh9fk9wrd5qnx36jcalyh04hl6")))

(define-public crate-perDiem-0.1.5 (c (n "perDiem") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "0qgdcsx4366hhh8iwdmy51amf75lswb46asdsm2yg0pj0yvd73l2")))

(define-public crate-perDiem-0.1.6 (c (n "perDiem") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "0124lmq1c8xv15dxffwra9ba2pzmv543q0fk65785vpfjj05x69c")))

(define-public crate-perDiem-0.1.61 (c (n "perDiem") (v "0.1.61") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "0bc8ckrbzkksdbk9gpg7fzwbr3s8znw72xkidqaxlm0d1x2mnfcf")))

(define-public crate-perDiem-0.1.62 (c (n "perDiem") (v "0.1.62") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1yp46q0ckb5g88ib7jffg0drdv84c2afm61m01frwm05yjdrf7li")))

(define-public crate-perDiem-0.1.63 (c (n "perDiem") (v "0.1.63") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "124v8bn4ivmafnhp4cfm9gn03inwmm02gcxhv5iq4l1k3f6k5avr")))

(define-public crate-perDiem-0.1.64 (c (n "perDiem") (v "0.1.64") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1ixpj0j5zzjm264rvrlya7s9gxl9vm6s0484lbgbr49pn52xj1qq")))

(define-public crate-perDiem-0.1.65 (c (n "perDiem") (v "0.1.65") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1k3yqm3ci4m2c5n121z98ybhzjfsk81wh0bcmwwmsspfgly66xmm")))

(define-public crate-perDiem-0.1.66 (c (n "perDiem") (v "0.1.66") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "1ldzngh2lyl2c1g3ic9jvyg6mb1zdpd3v4fm1hh6x819izcd2jh2")))

(define-public crate-perDiem-0.1.67 (c (n "perDiem") (v "0.1.67") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "struct_iterable") (r "^0.1.1") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "undup") (r "^0.0.1") (d #t) (k 0)))) (h "0y0nl3bpc4kzk9q93rb5asf9pgrlbzcnragfw3lx1vpvhypmbgim")))

