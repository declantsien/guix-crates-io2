(define-module (crates-io pe rd perde-core) #:use-module (crates-io))

(define-public crate-perde-core-0.0.1 (c (n "perde-core") (v "0.0.1") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1nbgfdrn2pfg112zc9hz1ybqqa3ixl3zxbc630ydvdydamdsfzsy")))

