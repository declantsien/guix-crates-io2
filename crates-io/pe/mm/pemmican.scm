(define-module (crates-io pe mm pemmican) #:use-module (crates-io))

(define-public crate-pemmican-0.1.2 (c (n "pemmican") (v "0.1.2") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "1ihcrnlab3cnlndc4740lwf8l7kba8drxdw4xjph8xhjknjm2573")))

(define-public crate-pemmican-0.1.3 (c (n "pemmican") (v "0.1.3") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "02v3jv1w475bi019ibbqhz6cr0a8l6ms3hzzwg514hsf3l2m4id1")))

(define-public crate-pemmican-0.2.0 (c (n "pemmican") (v "0.2.0") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "1azjz37ql7z8a6nz2fm7b68dy63jv86r3sy1g0l9w5m3fylx0bah")))

(define-public crate-pemmican-0.2.1 (c (n "pemmican") (v "0.2.1") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0pmjgff7w5k7c8lgmyyw2cim95nyhc3bb255vbn4dxl0f8xhj773")))

(define-public crate-pemmican-0.3.0 (c (n "pemmican") (v "0.3.0") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "14kx10ac4lfdp18ky8jcqqvm97p4vnphrsl2g5j51zsdwgjv119r")))

(define-public crate-pemmican-0.4.0 (c (n "pemmican") (v "0.4.0") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "cookie") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "textnonce") (r "^0.6") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "0m3n30yjf5cj6lavf07vzh4dl5xaszprcj0x3q75lfix9w4d92dy")))

