(define-module (crates-io pe di pedigree) #:use-module (crates-io))

(define-public crate-pedigree-0.3.0 (c (n "pedigree") (v "0.3.0") (d (list (d (n "methylome") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("blas"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "progress_bars") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0mv04y5g5l9i4p5rlfy1pkv0falin28371714p2s1w0jf1lkmgxp")))

