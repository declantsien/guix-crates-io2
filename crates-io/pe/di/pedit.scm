(define-module (crates-io pe di pedit) #:use-module (crates-io))

(define-public crate-pedit-1.0.0 (c (n "pedit") (v "1.0.0") (d (list (d (n "cotton") (r "^0.0.9") (d #t) (k 0)) (d (n "diff") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1q7szngk5w1rsqndkppl9a3dhxjng48mj2f6c23yk2s7k8mvn82j")))

