(define-module (crates-io pe di pedis-core) #:use-module (crates-io))

(define-public crate-pedis-core-0.0.1 (c (n "pedis-core") (v "0.0.1") (h "1kcm4n4w7miyhvlxq3y5h5p51mmwwdgx8mjcq26g3az9wfz5s1fh") (r "1.78")))

(define-public crate-pedis-core-0.0.2 (c (n "pedis-core") (v "0.0.2") (h "0b1751hhdpa0mh5q40wp8v43qfhgvfdd501pghcp65fd1g9rw1xx") (r "1.78")))

(define-public crate-pedis-core-0.0.3 (c (n "pedis-core") (v "0.0.3") (h "168pazqkd7rl9cdrmq7i53x4b5wmqhz7y8i7dll8xv8h36ls32yd") (r "1.77.2")))

