(define-module (crates-io pe tg petgraph-graphml) #:use-module (crates-io))

(define-public crate-petgraph-graphml-1.0.0 (c (n "petgraph-graphml") (v "1.0.0") (d (list (d (n "petgraph") (r "^0.4.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "13f46bhkf284mfyhrp6nrx5vw32pjfp5nxi68jzblg1czsyzs18k")))

(define-public crate-petgraph-graphml-1.0.1 (c (n "petgraph-graphml") (v "1.0.1") (d (list (d (n "petgraph") (r "^0.4.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "10ihxpl0irxppdcy1vflgc7k8z8p9rzzfzj5p299qhgfa4wjbsb6")))

(define-public crate-petgraph-graphml-1.0.2 (c (n "petgraph-graphml") (v "1.0.2") (d (list (d (n "petgraph") (r "^0.4.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "13kqgw490f94584qxd381a17dcyya3rmhi3z1ry7a17gpq8yflgl")))

(define-public crate-petgraph-graphml-2.0.0 (c (n "petgraph-graphml") (v "2.0.0") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1k2vpyx8ws473i2kkc4kjfl8ac7q37jvyzkfprpqbvsn7mp30b6r")))

(define-public crate-petgraph-graphml-2.0.1 (c (n "petgraph-graphml") (v "2.0.1") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0yxla02sy2ji0dvrnicxbq9ywi8dz3vimqjqr7712vrd0wclp8pi")))

(define-public crate-petgraph-graphml-3.0.0 (c (n "petgraph-graphml") (v "3.0.0") (d (list (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0ys8x245d9al137dsr8xxm9jax41ils290rhkispaxlahmyj768z")))

