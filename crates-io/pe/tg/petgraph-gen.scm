(define-module (crates-io pe tg petgraph-gen) #:use-module (crates-io))

(define-public crate-petgraph-gen-0.1.0 (c (n "petgraph-gen") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02hrq9hpf4nvd54k5x37agylbnfl8iwfwm0zp0c8x836c7hl8baj")))

(define-public crate-petgraph-gen-0.1.1 (c (n "petgraph-gen") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kx7aklj36m0ishgh57k1z53qdpn31yfmfcyj30jdmhgnmjagipx")))

(define-public crate-petgraph-gen-0.1.2 (c (n "petgraph-gen") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0g9j2k9g99kq5nxlnimz5jq73lylsbqvgvf9i4m6pm600v9gf0xv")))

(define-public crate-petgraph-gen-0.1.3 (c (n "petgraph-gen") (v "0.1.3") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0h09d8kl3d1b7kan48h4aax7xv1lgjpf6nl6y2hnlfmkrmrcyfyq")))

