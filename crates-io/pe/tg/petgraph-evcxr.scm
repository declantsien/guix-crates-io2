(define-module (crates-io pe tg petgraph-evcxr) #:use-module (crates-io))

(define-public crate-petgraph-evcxr-0.1.0 (c (n "petgraph-evcxr") (v "0.1.0") (d (list (d (n "base64") (r ">= 0.10.1") (d #t) (k 0)) (d (n "petgraph") (r ">= 0.4.13") (d #t) (k 0)))) (h "1sm72fvf4zvjkir5dm84fyx83hy41mzcxyiacsnhz1ppnzp5m46f")))

(define-public crate-petgraph-evcxr-0.1.1 (c (n "petgraph-evcxr") (v "0.1.1") (d (list (d (n "base64") (r ">= 0.10.1") (d #t) (k 0)) (d (n "petgraph") (r ">= 0.4.13") (d #t) (k 0)))) (h "0h20vdbkrb2rkfg486bnwnb7yd2hl9fhy2vcn2ns3rya7wpij9qv")))

(define-public crate-petgraph-evcxr-0.2.0 (c (n "petgraph-evcxr") (v "0.2.0") (d (list (d (n "base64") (r ">=0.10.1") (d #t) (k 0)) (d (n "petgraph") (r ">=0.5.1") (d #t) (k 0)))) (h "15pxr4cjli1jnqlbgiqm3k99s6m3q7cq1pkjc7znjaz0nh54y5jc")))

