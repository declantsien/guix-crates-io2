(define-module (crates-io pe nn penny) #:use-module (crates-io))

(define-public crate-penny-0.1.0 (c (n "penny") (v "0.1.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "mitochondria") (r "^1.1.2") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 1)) (d (n "quick-xml") (r "^0.7.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "0dvmvjkbm9jsvlhvszbz756d6kcah7s2dqmpqbw2z3km5b3wahs4") (f (quote (("serde-serialize" "serde" "serde_derive") ("lint" "clippy") ("default" "serde-serialize"))))))

(define-public crate-penny-0.2.0 (c (n "penny") (v "0.2.0") (d (list (d (n "mitochondria") (r "^1.1.2") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 1)) (d (n "quick-xml") (r "^0.11.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "038rjys4a2qgzrib529dkhk9l9a40hwmn28jgy4p6ff4ch5dzlai") (f (quote (("std") ("serde-serialize" "serde" "serde_derive") ("default" "serde-serialize" "std"))))))

