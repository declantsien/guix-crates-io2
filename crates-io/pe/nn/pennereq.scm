(define-module (crates-io pe nn pennereq) #:use-module (crates-io))

(define-public crate-pennereq-0.1.0 (c (n "pennereq") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1v9lhp4cawkpq3brg861vv93acv8bvqka61jga9j757s4scgkhxs")))

(define-public crate-pennereq-0.2.0 (c (n "pennereq") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "18idjiv29c28shsnnabqqcng4fh650iyz81d80xj2xn0spccw4hy")))

(define-public crate-pennereq-0.3.0 (c (n "pennereq") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12iigfhv2c55i47aqjkz833wzyj0h435rz30hagphyam8ww97xml")))

(define-public crate-pennereq-0.3.1 (c (n "pennereq") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12ywa2aip0w5jgja2qh9yy03ijcgswlxq25gw7f8q3vgavsahx11")))

