(define-module (crates-io pe p- pep-508) #:use-module (crates-io))

(define-public crate-pep-508-0.1.0 (c (n "pep-508") (v "0.1.0") (d (list (d (n "chumsky") (r "^0.9.0") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "17z3rrdamhg75h6lyczj3zm8xmi7lrh70xwx1mz3zfwy223v6dq3")))

(define-public crate-pep-508-0.2.0 (c (n "pep-508") (v "0.2.0") (d (list (d (n "chumsky") (r "^1.0.0-alpha.0") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "1j1q250ncf6z3xwyxyqi70i4v3m59izf7s0kpmadd8xpicyhxls1")))

(define-public crate-pep-508-0.3.0 (c (n "pep-508") (v "0.3.0") (d (list (d (n "chumsky") (r ">=1.0.0-alpha.3") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "18qd06pm4fgkc10970fad5rcfjgardg704cszrjzvb9b0d07kv6s")))

(define-public crate-pep-508-0.4.0 (c (n "pep-508") (v "0.4.0") (d (list (d (n "chumsky") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0kqybppyq5i9yxjvwdq8ndgjjp8x719jpbi73a0cap7pbnwkcrfm")))

