(define-module (crates-io pe a- pea-pay-core) #:use-module (crates-io))

(define-public crate-pea-pay-core-0.1.0 (c (n "pea-pay-core") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pea-key") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivi1mjim5l6mwv83sjaq2ag4dvfj6brpvmphll4a6m1k9p160hw") (y #t)))

(define-public crate-pea-pay-core-0.1.1 (c (n "pea-pay-core") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)) (d (n "pea-key") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1chpkq5wr06rmv3jg8lkdcslbmk40b2s8nzwjnvax0hwiv9rdnx5") (y #t)))

