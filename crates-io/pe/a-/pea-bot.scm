(define-module (crates-io pe a- pea-bot) #:use-module (crates-io))

(define-public crate-pea-bot-0.1.0 (c (n "pea-bot") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pea-api") (r "^0.1.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.1") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxq1wl2glkp7cci4s4mm1waw67j40jrk534wzlc2fp2g4kxda9l") (y #t)))

(define-public crate-pea-bot-0.1.1 (c (n "pea-bot") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pea-api") (r "^0.2.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18jbcmgq1n3db2s8jvsmn2lm1s7fdjcq1ppc3yxzlrqaai1n3783") (y #t)))

