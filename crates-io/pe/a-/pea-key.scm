(define-module (crates-io pe a- pea-key) #:use-module (crates-io))

(define-public crate-pea-key-0.1.0 (c (n "pea-key") (v "0.1.0") (d (list (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pea-address") (r "^0.1.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1gns5q7zr8a8dksgnkqlc151qhkdn74x6zncx0ppfrkw892fb8rf") (y #t)))

(define-public crate-pea-key-0.2.0 (c (n "pea-key") (v "0.2.0") (d (list (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pea-address") (r "^0.2.0") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xwj9h7r04dmjc0mv05h60yvgciin5ab2xrsjq9mlcjwci4pwdh4") (y #t)))

(define-public crate-pea-key-0.2.1 (c (n "pea-key") (v "0.2.1") (d (list (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pea-address") (r "^0.2.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0acv5ap3ys42bqha6w1hrjw9hx29s9679crncfp9jqvza1h2lbmp") (y #t)))

(define-public crate-pea-key-0.2.2 (c (n "pea-key") (v "0.2.2") (d (list (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pea-address") (r "^0.3.0") (d #t) (k 0)) (d (n "pea-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0mdx4kw55k1c5h13cap1c4nph8ack7azb7jnmz151ncqy4wp2wd5") (y #t)))

(define-public crate-pea-key-0.3.0 (c (n "pea-key") (v "0.3.0") (d (list (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0794ck9am4fhl9nlj2sk58hyh315f3v6s8cl3h75f5h033fm7w81") (y #t)))

(define-public crate-pea-key-0.4.0 (c (n "pea-key") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "vrf") (r "^0.2.4") (d #t) (k 0)))) (h "0zd5512v813w1ma9dizfx1qd0ascnkyldkvf0ghxkfxfwsyphkrf") (y #t)))

(define-public crate-pea-key-0.5.0 (c (n "pea-key") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "vrf") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1b7b0y0p83095jz26d78x5rik4r74qyv037axm1pxfayr52l444x") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

(define-public crate-pea-key-0.6.0 (c (n "pea-key") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "vrf") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0bjhjzb2ks4402df35r5d9f5l0wmf9dqvy9hrck4hsj4yg6wi8fm") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

(define-public crate-pea-key-0.6.1 (c (n "pea-key") (v "0.6.1") (d (list (d (n "pea-core") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "vrf") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1k0sclcga2ca5xk8ivl2r1cydnc0gdah5b040llqy0ryrmdvmxmi") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

(define-public crate-pea-key-0.6.2 (c (n "pea-key") (v "0.6.2") (d (list (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "vrf") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0vclflsklwkjd706yywfc4kq90yx8vsl2zyi1c5p0ppx3vr155av") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

(define-public crate-pea-key-0.6.3 (c (n "pea-key") (v "0.6.3") (d (list (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "vrf") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pfa0b9kalgkhmi8znw3v90z1yrzwhkd19m3w6bxdr1kpy4wh0vr") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

(define-public crate-pea-key-0.6.4 (c (n "pea-key") (v "0.6.4") (d (list (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "vrf") (r "^0.2") (o #t) (d #t) (k 0)))) (h "19d3k19pbd9nr779m5rr6ybf1chwdah3labvbdvpdqi655p02qwb") (y #t) (s 2) (e (quote (("vrf" "dep:vrf"))))))

