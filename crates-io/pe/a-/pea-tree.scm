(define-module (crates-io pe a- pea-tree) #:use-module (crates-io))

(define-public crate-pea-tree-0.1.0 (c (n "pea-tree") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.0") (d #t) (k 0)))) (h "055dddda6bi5zzwy890894wabc3kch8spw0cayh72r6i1rh9wcqz") (y #t)))

(define-public crate-pea-tree-0.1.1 (c (n "pea-tree") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.1") (d #t) (k 0)))) (h "1zb263snn96cagmxpxw17n2dl4q0rkyzxp2pkpmfh3gvmyawszny") (y #t)))

(define-public crate-pea-tree-0.2.0 (c (n "pea-tree") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.0") (d #t) (k 0)))) (h "0dkb85awp3h70lqcdmwv6vpkbca2aak0gxnza51vwz5hj3kjlm7s") (y #t)))

(define-public crate-pea-tree-0.2.1 (c (n "pea-tree") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)))) (h "0sm6vkz048pxmnjm3h81f6ji358is0n8vj3n86gk4fhxs76872dk") (y #t)))

(define-public crate-pea-tree-0.2.2 (c (n "pea-tree") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.3.0") (d #t) (k 0)))) (h "0n2q5yaw8b5k7kc8r5mr9m776ipwg7vakvlq0xkmyvy0m3dg41p7") (y #t)))

(define-public crate-pea-tree-0.3.0 (c (n "pea-tree") (v "0.3.0") (h "0g63b2285ivzl027hycr0prikclfhbk76vgxnhsdwxbw0gkwfrs2") (y #t)))

(define-public crate-pea-tree-0.4.0 (c (n "pea-tree") (v "0.4.0") (h "03vcmsjb6zfj34nigpwfzxhhy7b61zby9d0k93ylsii1fhfzi1d0") (y #t)))

(define-public crate-pea-tree-0.4.1 (c (n "pea-tree") (v "0.4.1") (h "0s896yiiv63076j3c9wcvs3hvdc2603vaxg50c6h86n94330ffa5") (y #t)))

