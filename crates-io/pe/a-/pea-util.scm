(define-module (crates-io pe a- pea-util) #:use-module (crates-io))

(define-public crate-pea-util-0.1.0 (c (n "pea-util") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pea-block") (r "^0.6") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "pea-stake") (r "^0.7") (d #t) (k 0)) (d (n "pea-transaction") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "015qdskn01wqvc77j93iqghmhjy1lw0rw3ppbicy80snnaw77xww") (y #t)))

(define-public crate-pea-util-0.1.1 (c (n "pea-util") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pea-block") (r "^0.6") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "pea-stake") (r "^0.7") (d #t) (k 0)) (d (n "pea-transaction") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1avxq2ia3azgr0vh496lk8a6ly4cwxjszrh70kyypq4x6zyfpa49") (y #t)))

