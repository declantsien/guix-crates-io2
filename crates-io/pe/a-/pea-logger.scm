(define-module (crates-io pe a- pea-logger) #:use-module (crates-io))

(define-public crate-pea-logger-0.1.0 (c (n "pea-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0iwkzhmyj9fyjw4rfagdyz56b885hg0w9nyhp22s5jzdib52zdjq") (y #t)))

(define-public crate-pea-logger-0.1.1 (c (n "pea-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13nd1jbfv3y7w8nl7cg5r8vvdg7m0njbp78l9hrmq2239qjdkw8l") (y #t)))

(define-public crate-pea-logger-0.1.2 (c (n "pea-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pfvgczdzmzwiwb4b9gn3j2birx7348rzydlfcq38kch0nsj6aiy") (y #t)))

(define-public crate-pea-logger-0.1.3 (c (n "pea-logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03cki72jz5qki5cyixkj7z5wgkpvqr5d1d3f2hr9cmbmly3smzcd") (y #t)))

