(define-module (crates-io pe a- pea-vrf) #:use-module (crates-io))

(define-public crate-pea-vrf-0.1.0 (c (n "pea-vrf") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^3.1.1") (d #t) (k 0)) (d (n "pea-core") (r "^0.4.0") (d #t) (k 0)) (d (n "pea-key") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "111fndxg4syx1ymhdg8lby7y3c0r0mn7nbp7dqx67zdyizmdavad") (y #t)))

(define-public crate-pea-vrf-0.2.0 (c (n "pea-vrf") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^3.1.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)))) (h "157kwaawxbkaz3ap02rh793msq5z0407wcr321v8l8g8c1ac9vna") (y #t)))

