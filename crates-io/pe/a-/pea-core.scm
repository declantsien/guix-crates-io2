(define-module (crates-io pe a- pea-core) #:use-module (crates-io))

(define-public crate-pea-core-0.1.0 (c (n "pea-core") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "0p2p0pgf16mm8wvk57rks6xjjaw0c8zq8wc5hrdl9bh4ndpk1nr1") (y #t)))

(define-public crate-pea-core-0.1.1 (c (n "pea-core") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "ed25519") (r "^1.5.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "0fi9hidkhndc8cpyf25fvp0k9m01p3qbglscjjy7jmzvyxnf9jna") (y #t)))

(define-public crate-pea-core-0.2.0 (c (n "pea-core") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "1v62vdljinp7wsj5490agcv3cda6s9ck7p7w2zn48d6l7q298bs6") (y #t)))

(define-public crate-pea-core-0.2.1 (c (n "pea-core") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "0yw80azbbpx797p5j6wkyysnbnjmwp2w12rnlxsixbb4y7n3rq6n") (y #t)))

(define-public crate-pea-core-0.3.0 (c (n "pea-core") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "1817f6r66j3h3bng3d2gc0i8nlgplfnc26fjd0vc4ic1jnbjs0vr") (y #t)))

(define-public crate-pea-core-0.3.1 (c (n "pea-core") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "0gkry4qrwq6fsgqmc5kw2cqx7lka9pmmz4d2797wy26pydpxv43j") (y #t)))

(define-public crate-pea-core-0.4.0 (c (n "pea-core") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "17ylylckhp0pc5l1ndixx380nrslrw4jpknswhqjw0pryfcrd9dr") (y #t)))

(define-public crate-pea-core-0.5.0 (c (n "pea-core") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (d #t) (k 0)))) (h "0fxnpnfag6wfq1dlf663c3pkac90b2bfbf7vchqmf0ridnxr7x4y") (y #t)))

(define-public crate-pea-core-0.6.0 (c (n "pea-core") (v "0.6.0") (h "15gak9wr5f5jrqgk1ivz8zp86lp38qxw3dvgxnbrqqy54vk66all") (y #t)))

(define-public crate-pea-core-0.6.1 (c (n "pea-core") (v "0.6.1") (h "0b4rz5ks4jgkmqwn9y17ghl408pb835n3mns4w39dd5wc3sl8i23") (y #t)))

(define-public crate-pea-core-0.6.2 (c (n "pea-core") (v "0.6.2") (h "0spcr7rxpl1fsymx0nw5j5bv8ri2bp74kk01qd7hdlnzhnzn4zzq") (y #t)))

(define-public crate-pea-core-0.6.3 (c (n "pea-core") (v "0.6.3") (h "0c1q267wjqv6cvyqh0bjv3vhmq0ag6h66pcnsbgf55llg7i3bw42") (y #t)))

(define-public crate-pea-core-0.6.4 (c (n "pea-core") (v "0.6.4") (h "0iyrczynrfvpsbwdw3ddfb6r3cv21hy6piisi241g5j3c0v8xxl8") (y #t)))

