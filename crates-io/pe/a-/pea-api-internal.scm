(define-module (crates-io pe a- pea-api-internal) #:use-module (crates-io))

(define-public crate-pea-api-internal-0.1.0 (c (n "pea-api-internal") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "multiaddr") (r "^0.17") (d #t) (k 0)) (d (n "pea-api-internal-core") (r "^0.1") (d #t) (k 0)) (d (n "pea-block") (r "^0.6") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "pea-stake") (r "^0.7") (d #t) (k 0)) (d (n "pea-transaction") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6brgxjsndkvmmfb1482h86j4rnqis48hyv3ym24b0qnx4cbg6m") (y #t)))

