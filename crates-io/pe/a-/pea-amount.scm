(define-module (crates-io pe a- pea-amount) #:use-module (crates-io))

(define-public crate-pea-amount-0.1.0 (c (n "pea-amount") (v "0.1.0") (d (list (d (n "pea-core") (r "^0.1.0") (d #t) (k 0)))) (h "0par5k1sz7kj9mfa95inmgiwqhnjgqz1nmjd260lmnpinkhy6dp0") (y #t)))

(define-public crate-pea-amount-0.1.1 (c (n "pea-amount") (v "0.1.1") (d (list (d (n "pea-core") (r "^0.1.1") (d #t) (k 0)))) (h "05ab9gnz2wpmnfxr2fs93nczmay96dzmi9dhayb50v9lix8hyq5m") (y #t)))

(define-public crate-pea-amount-0.2.0 (c (n "pea-amount") (v "0.2.0") (d (list (d (n "pea-core") (r "^0.2.0") (d #t) (k 0)))) (h "1mzmlkl2m25bdbc03n4aynxpz5vl8rhw53f737har23ayhrqjsgk") (y #t)))

(define-public crate-pea-amount-0.2.1 (c (n "pea-amount") (v "0.2.1") (d (list (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)))) (h "1wk9b8d41ybykidabd5i44z6jpy9hipf59y117vvhwr15kdjc8fn") (y #t)))

