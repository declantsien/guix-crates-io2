(define-module (crates-io pe a- pea-p2p) #:use-module (crates-io))

(define-public crate-pea-p2p-0.1.0 (c (n "pea-p2p") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libp2p") (r "^0.50") (f (quote ("request-response" "autonat" "macros" "identify" "mdns" "gossipsub" "tokio"))) (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (d #t) (k 0)))) (h "1qnz8d46mxlpc63z3x63cxlq7c5jyxllkabsahgn9bzagyy67lx2") (y #t)))

(define-public crate-pea-p2p-0.1.1 (c (n "pea-p2p") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libp2p") (r "^0.50") (f (quote ("mplex" "noise" "tcp" "request-response" "autonat" "macros" "identify" "mdns" "gossipsub" "tokio"))) (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "pea-util") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (d #t) (k 0)))) (h "0klbxkhmp3sw7lx84k20lx1v4yr8wy1l3lazw0djlkvcllllxqhf") (y #t)))

