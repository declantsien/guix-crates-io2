(define-module (crates-io pe a- pea-address) #:use-module (crates-io))

(define-public crate-pea-address-0.1.0 (c (n "pea-address") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.0") (d #t) (k 0)))) (h "0hvgda02g569lsc9ar8ap0qmf3672a1kd3y3m217cyndbv3fcrf6") (y #t)))

(define-public crate-pea-address-0.1.1 (c (n "pea-address") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.1.1") (d #t) (k 0)))) (h "1210r7378vknp4q776p8991dx5p804brniyddmx7l29br3j82bsl") (y #t)))

(define-public crate-pea-address-0.2.0 (c (n "pea-address") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.0") (d #t) (k 0)))) (h "0b43b4f3pi8mimmqqqha6piqm653ja7dksv719yz43fv5l7ziqw2") (y #t)))

(define-public crate-pea-address-0.2.1 (c (n "pea-address") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.2.1") (d #t) (k 0)))) (h "19img2wfb2czlc8bpcdfvfwxi8iwrn3d8sv2m2a0h2bh5vys7ymr") (y #t)))

(define-public crate-pea-address-0.3.0 (c (n "pea-address") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.3.0") (d #t) (k 0)))) (h "08k3ivclvp3c0ywfncq1k38qyp2xyim9i2ylrxmkjish3iwkjzi3") (y #t)))

(define-public crate-pea-address-0.4.0 (c (n "pea-address") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.4.0") (d #t) (k 0)))) (h "0n2qlznk8ns28p11wz0yspdqvpfzy9xvajhmjx5rxc9by90w04v5") (y #t)))

(define-public crate-pea-address-0.5.0 (c (n "pea-address") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0l7jcqp8ciavcxdvqf60hw1rap9rxawb3ny6rwcvyf71zg9zp18r") (y #t)))

(define-public crate-pea-address-0.6.0 (c (n "pea-address") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1lyahpg6snlzadflm9h2zk833xgjz91b6cp795acjclpakhzkcw4") (y #t)))

(define-public crate-pea-address-0.6.1 (c (n "pea-address") (v "0.6.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "014kmlpg8yl8n7w2i78n45m3i5mnf6ga7zq64mcfad94kgrf2qvk") (y #t)))

(define-public crate-pea-address-0.6.2 (c (n "pea-address") (v "0.6.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1nn19xy48sskmxxhv7qpsmr72rswl6650qm28wl7a360afzl5bcj") (y #t)))

(define-public crate-pea-address-0.6.3 (c (n "pea-address") (v "0.6.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pea-core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "05mdmf7r998cj03da7bj4f42g2mivffll5b6rc76mlqq0df6qcdx") (y #t)))

