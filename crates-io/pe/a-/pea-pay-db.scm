(define-module (crates-io pe a- pea-pay-db) #:use-module (crates-io))

(define-public crate-pea-pay-db-0.1.0 (c (n "pea-pay-db") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pea-pay-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)))) (h "1w4shm62mqq1xh2kkri9br795bq2dbyk4z6gwa68i9hdqm7qq3a4") (y #t)))

(define-public crate-pea-pay-db-0.1.1 (c (n "pea-pay-db") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pea-pay-core") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)))) (h "1ilkawybz6b15xwjgs1x616ymsk31r8php4wxxqxl7crcpz8qmbv") (y #t)))

