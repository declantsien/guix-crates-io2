(define-module (crates-io pe a- pea-api-util) #:use-module (crates-io))

(define-public crate-pea-api-util-0.1.0 (c (n "pea-api-util") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pea-address") (r "^0.6") (d #t) (k 0)) (d (n "pea-api-core") (r "^0.1") (d #t) (k 0)) (d (n "pea-block") (r "^0.6") (d #t) (k 0)) (d (n "pea-int") (r "^0.4") (d #t) (k 0)) (d (n "pea-stake") (r "^0.7") (d #t) (k 0)) (d (n "pea-transaction") (r "^0.8") (d #t) (k 0)))) (h "1aipndiwl7xp542d3bzjl8k7nbqyhkb5q3fmj4ywgnmsc03dgqzr") (y #t)))

