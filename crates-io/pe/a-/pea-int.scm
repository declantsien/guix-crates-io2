(define-module (crates-io pe a- pea-int) #:use-module (crates-io))

(define-public crate-pea-int-0.1.0 (c (n "pea-int") (v "0.1.0") (d (list (d (n "pea-core") (r "^0.3.0") (d #t) (k 0)))) (h "03gwmyq48jihj1n6q3k4x2rnscw344mrwnxrwm3xl3k7g8h4ni30") (y #t)))

(define-public crate-pea-int-0.2.0 (c (n "pea-int") (v "0.2.0") (d (list (d (n "pea-core") (r "^0.3.1") (d #t) (k 0)))) (h "17wz4b04w62ad4w4icfand1p49yam30rffas8q27qhs2znl37g73") (y #t)))

(define-public crate-pea-int-0.2.1 (c (n "pea-int") (v "0.2.1") (d (list (d (n "pea-core") (r "^0.3.1") (d #t) (k 0)))) (h "0jly8x0pxr1w4s6pyxhgr0q0nxcffjxmc94nwv9wk93fhs4qmj9l") (y #t)))

(define-public crate-pea-int-0.3.0 (c (n "pea-int") (v "0.3.0") (h "029xjp8mq97wbd3k6chl98m9x91222qk1yfvbz8056ilbkg8g6pw") (y #t)))

(define-public crate-pea-int-0.4.0 (c (n "pea-int") (v "0.4.0") (d (list (d (n "pea-core") (r "^0.6.0") (d #t) (k 0)))) (h "1nvxydyjkh3nyp3p2zcw2qpdhs6ycvys5a4mm05kd00n9sd7lvq2") (y #t)))

(define-public crate-pea-int-0.4.2 (c (n "pea-int") (v "0.4.2") (d (list (d (n "pea-core") (r "^0.6") (d #t) (k 0)))) (h "17kgqn2sczh4fx1wh93g4y8rnlqkgad4m56bk66wdiasrp6pl990") (y #t)))

