(define-module (crates-io pe p5 pep508_parser) #:use-module (crates-io))

(define-public crate-pep508_parser-0.1.0 (c (n "pep508_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0g6v3bgfc3nk83dfij3jzlbv251055mcynr00ml00vdarv2clxv8")))

