(define-module (crates-io pe ts petstore) #:use-module (crates-io))

(define-public crate-petstore-0.1.0 (c (n "petstore") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1j2pz52bcdcj7ignm75mycybphjrq48vqji6wc6vln0dwnvb7240")))

(define-public crate-petstore-3.1.0 (c (n "petstore") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "007by87rmyjwsv1i8hm1x130hfgq656bpwf4jab4iaryblff4gh2")))

(define-public crate-petstore-1.0.0 (c (n "petstore") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1d1fyqxhxcfdjql70j2h2svpf1agx6psgw7j2faaig7ikm59k9c0")))

(define-public crate-petstore-4.0.0 (c (n "petstore") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0w3dg1qjk45lasm4w7vbwsmvb5cq9cl24my8y1ahkg6yml41iky2")))

(define-public crate-petstore-5.0.0 (c (n "petstore") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1wv1dz7jp70z8bd5m3icmphhi5pdsbz060sx5x8gc9i7jiar2pv2")))

(define-public crate-petstore-6.0.0 (c (n "petstore") (v "6.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "07rzsg5kj9r8c32062cp2kprldn5ja3b93432ryrgjiv1hmg3kvi")))

(define-public crate-petstore-7.0.0 (c (n "petstore") (v "7.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1wnwyvcwqi4jp15s6fdmnhi6hwlj3hk3h9p6d3ids7q02r52lw9w")))

(define-public crate-petstore-8.0.0 (c (n "petstore") (v "8.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1favy697kfw9dhymgyrmlqv324aix7xw42xgppplwvqv1kdz33w5")))

(define-public crate-petstore-9.0.0 (c (n "petstore") (v "9.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1vcjgy9pcbbxv4ija61nfrmgmpl1qyay3lq0dlwf1bflr6cxk49b")))

