(define-module (crates-io pe ts petscii) #:use-module (crates-io))

(define-public crate-petscii-0.1.0 (c (n "petscii") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1lvggnz48pkn5s1wwb9clmh14mjqv42qfx2palw6alfdrhdhirha") (r "1.63")))

(define-public crate-petscii-0.2.0 (c (n "petscii") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "10nfryk7psmgv9w32xvpd8g997msgp8b6zc3qmd8220a7a4ayiz3") (r "1.63")))

(define-public crate-petscii-0.2.1 (c (n "petscii") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rx6xs589ccqpdidqjcmh02rq7kp8smz6gp8587y8qyamldx03hq") (r "1.63")))

