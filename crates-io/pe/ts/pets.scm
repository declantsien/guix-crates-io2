(define-module (crates-io pe ts pets) #:use-module (crates-io))

(define-public crate-pets-0.1.2 (c (n "pets") (v "0.1.2") (d (list (d (n "no-std-compat") (r "^0.2.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.36") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1h6iracb9pgifq6bgc2cf98xyb6qbi50wpd98iynv6v7qz7wvlxg") (f (quote (("std" "no-std-compat/std" "serde/std" "num-integer/std" "alloc") ("default" "std") ("alloc" "no-std-compat/alloc" "serde/alloc"))))))

