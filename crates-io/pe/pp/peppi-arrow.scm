(define-module (crates-io pe pp peppi-arrow) #:use-module (crates-io))

(define-public crate-peppi-arrow-0.1.0 (c (n "peppi-arrow") (v "0.1.0") (d (list (d (n "arrow") (r "^4.2") (d #t) (k 0)))) (h "11hs835r2cimdxcxji750kp80rd3302p7iq64xhim200grgyc8i2")))

(define-public crate-peppi-arrow-0.2.0 (c (n "peppi-arrow") (v "0.2.0") (d (list (d (n "arrow") (r "^4.4") (d #t) (k 0)))) (h "1dcch8f2w7i1x0x7gvxpv810pa5zali991viy775fbrwdf0iwmxs")))

(define-public crate-peppi-arrow-0.2.1 (c (n "peppi-arrow") (v "0.2.1") (d (list (d (n "arrow") (r "^5.5") (d #t) (k 0)))) (h "1mbd9jvnwdpv7my5aj7q48cw5k9qbrm3qwjsg5vg5nnlrz0a4brz")))

(define-public crate-peppi-arrow-0.2.2 (c (n "peppi-arrow") (v "0.2.2") (d (list (d (n "arrow") (r "^5.5") (d #t) (k 0)))) (h "0xxidkfc14q22a5195r2w3z21x9mwg7inln4sxv53qkcw8i2xaz0")))

(define-public crate-peppi-arrow-0.2.3 (c (n "peppi-arrow") (v "0.2.3") (d (list (d (n "arrow") (r "^6.3") (d #t) (k 0)))) (h "08bpv9bk8aa48f3ri0fy81s5jb9qm68hacji1ya8qa7a61n79kgj")))

