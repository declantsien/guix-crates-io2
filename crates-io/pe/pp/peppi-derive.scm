(define-module (crates-io pe pp peppi-derive) #:use-module (crates-io))

(define-public crate-peppi-derive-0.1.0 (c (n "peppi-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "peppi-arrow") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h9gmx5n6yf052rmq9dvsq39nc7v279zbiq0bbplrz9734mpk521")))

(define-public crate-peppi-derive-0.2.0 (c (n "peppi-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "peppi-arrow") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xy1ra1f6yk07pnw67i33bgxjzydva1ybx882002zkclxrp9hjyn")))

(define-public crate-peppi-derive-0.2.1 (c (n "peppi-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "peppi-arrow") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mz5n1ndfn6rasrgsnp16xqg6cxay9bc10qbgca3gnpr7rfp8lks")))

(define-public crate-peppi-derive-0.2.2 (c (n "peppi-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "peppi-arrow") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ab1827m9nzibj62fkvmx95aq52pl6wvpav1jhc20hr1gx1785ib")))

(define-public crate-peppi-derive-0.2.3 (c (n "peppi-derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "peppi-arrow") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i302md2znh9wi435d90nh3rbb9hlqvlqn1xywcaqhfa6lgsrqjh")))

