(define-module (crates-io pe pp pepper-plugin-remedybg) #:use-module (crates-io))

(define-public crate-pepper-plugin-remedybg-0.1.0 (c (n "pepper-plugin-remedybg") (v "0.1.0") (d (list (d (n "pepper") (r "^0.28") (d #t) (k 0)))) (h "05zb8w1di65hx1g06fcy1vg07z7jr6dkw0sbxmz82w8f37vjl675")))

(define-public crate-pepper-plugin-remedybg-0.2.0 (c (n "pepper-plugin-remedybg") (v "0.2.0") (d (list (d (n "pepper") (r "^0.29") (d #t) (k 0)))) (h "11wdq4lydlgqsfssbmv1abm8rmqx1wj7zv2p1877n9hk391k5pqs")))

(define-public crate-pepper-plugin-remedybg-0.3.0 (c (n "pepper-plugin-remedybg") (v "0.3.0") (d (list (d (n "pepper") (r "^0.30") (d #t) (k 0)))) (h "0s2wjdyi12i4mjispqgxbg2cd42jgyarkb875kd5p07w276kygqq")))

(define-public crate-pepper-plugin-remedybg-0.4.0 (c (n "pepper-plugin-remedybg") (v "0.4.0") (d (list (d (n "pepper") (r "^0.31") (d #t) (k 0)))) (h "11805rwswzvmzmb0klhd6x9s26na9vww11an1k2phnzxdbfay35a")))

