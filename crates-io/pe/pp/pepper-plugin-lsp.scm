(define-module (crates-io pe pp pepper-plugin-lsp) #:use-module (crates-io))

(define-public crate-pepper-plugin-lsp-0.1.0 (c (n "pepper-plugin-lsp") (v "0.1.0") (d (list (d (n "pepper") (r "0.*") (d #t) (k 0)))) (h "10ik7r8hcx2k02i110qvkp0ndhkv571zhg31swdcvcspzzlavrl1")))

(define-public crate-pepper-plugin-lsp-0.1.1 (c (n "pepper-plugin-lsp") (v "0.1.1") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "03h22m1swaqhc165j8skzym7sc0q5pvj2mkf3nn9fxs6f1xwa75m")))

(define-public crate-pepper-plugin-lsp-0.2.0 (c (n "pepper-plugin-lsp") (v "0.2.0") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "1lwkv0l2w9k670ysrnjq488bwrg5bch9s0ln7md5z3107h7x7vby")))

(define-public crate-pepper-plugin-lsp-0.3.0 (c (n "pepper-plugin-lsp") (v "0.3.0") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "05q6gxm49fdg5mrmcyz8zyrqsr3czhy09qlbd0j8pbj7nmacq4sv")))

(define-public crate-pepper-plugin-lsp-0.3.1 (c (n "pepper-plugin-lsp") (v "0.3.1") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "17xkbnhdqb44f64jfwlfq1yv8lafr66yjgrb2zz3p0lr82mkkcpm")))

(define-public crate-pepper-plugin-lsp-0.4.0 (c (n "pepper-plugin-lsp") (v "0.4.0") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "0a2lnipnjn43b99yn8hnx2mi4siw1nd9irm2wf4vgfy0s5q4fwiv")))

(define-public crate-pepper-plugin-lsp-0.4.1 (c (n "pepper-plugin-lsp") (v "0.4.1") (d (list (d (n "pepper") (r "^0") (d #t) (k 0)))) (h "0s3b0a2bk9n5hv7wn06izvdigx3qdkhzhxxh9l9b8wmj64ayzc6x")))

(define-public crate-pepper-plugin-lsp-0.4.2 (c (n "pepper-plugin-lsp") (v "0.4.2") (d (list (d (n "pepper") (r "^0.17") (d #t) (k 0)))) (h "0nvvnlazvpacqf4j8a88jv4wswzj5b9n5nc7nrnbbqhrzf0hfbbh")))

(define-public crate-pepper-plugin-lsp-0.5.0 (c (n "pepper-plugin-lsp") (v "0.5.0") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "0ly4sz8f8lszkkr20fkvgj2dj07740000d14l91d3xcavg2il05r")))

(define-public crate-pepper-plugin-lsp-0.5.1 (c (n "pepper-plugin-lsp") (v "0.5.1") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "0n7lz7k5gqpr76xvnq78w4frsfnr65pi8n5giak8ps3gwajanxq0")))

(define-public crate-pepper-plugin-lsp-0.6.0 (c (n "pepper-plugin-lsp") (v "0.6.0") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "05j5jp6k23z66p8pbg889061nwlpdka01d7pi1jwh215d7s5xqkw")))

(define-public crate-pepper-plugin-lsp-0.6.1 (c (n "pepper-plugin-lsp") (v "0.6.1") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "1lqbpgv4xzbjkrb9ybik125r0j5yd461698ldg1pb3iq5qnz5pb0")))

(define-public crate-pepper-plugin-lsp-0.6.2 (c (n "pepper-plugin-lsp") (v "0.6.2") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "02kr0d0czm4n6ln8cmwcqni19rzb0wwm7yx8pjjlfjcaasrx102p")))

(define-public crate-pepper-plugin-lsp-0.6.3 (c (n "pepper-plugin-lsp") (v "0.6.3") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "0h6x59x6s64sdl3rsr2ap1frhlkjqq2lpnrd3il718q3r867sliw")))

(define-public crate-pepper-plugin-lsp-0.6.4 (c (n "pepper-plugin-lsp") (v "0.6.4") (d (list (d (n "pepper") (r "^0.18") (d #t) (k 0)))) (h "0rc5aig350xr2jdm0mizr7x4m4dsyapgjmqbsnn3xd0dkv2zd4md")))

(define-public crate-pepper-plugin-lsp-0.7.0 (c (n "pepper-plugin-lsp") (v "0.7.0") (d (list (d (n "pepper") (r "^0.19") (d #t) (k 0)))) (h "11c3b9y6yc56zijc58s762s0hy0z5hh02fm5zpaqjx6ajl0vd47y")))

(define-public crate-pepper-plugin-lsp-0.8.0 (c (n "pepper-plugin-lsp") (v "0.8.0") (d (list (d (n "pepper") (r "^0.20") (d #t) (k 0)))) (h "1im4akasdmhnbikbyqgr71wwb7zl7mg6nn24ax80c22mghiddlay")))

(define-public crate-pepper-plugin-lsp-0.9.0 (c (n "pepper-plugin-lsp") (v "0.9.0") (d (list (d (n "pepper") (r "^0.21") (d #t) (k 0)))) (h "14papcg36ii5l4jirbdzb38phhl2san38zmzhqp8abyx40lz9bv7")))

(define-public crate-pepper-plugin-lsp-0.10.0 (c (n "pepper-plugin-lsp") (v "0.10.0") (d (list (d (n "pepper") (r "^0.22") (d #t) (k 0)))) (h "1jmpxfm4hykd4bg5x0374c394z2kc5zcm40yq4q86piwzh8ib886")))

(define-public crate-pepper-plugin-lsp-0.12.0 (c (n "pepper-plugin-lsp") (v "0.12.0") (d (list (d (n "pepper") (r "^0.24") (d #t) (k 0)))) (h "0m033kdkmpwj2jyh683b0nswrz5xk7kla19ag891fsx5qj5p8hcy")))

(define-public crate-pepper-plugin-lsp-0.13.0 (c (n "pepper-plugin-lsp") (v "0.13.0") (d (list (d (n "pepper") (r "^0.26") (d #t) (k 0)))) (h "0v7iwvc0fwl31kxnglgm3jff0hv9ldlcvdsfhzr4ml0gjmbf3zsd")))

(define-public crate-pepper-plugin-lsp-0.14.0 (c (n "pepper-plugin-lsp") (v "0.14.0") (d (list (d (n "pepper") (r "^0.27") (d #t) (k 0)))) (h "1wafdlf0qmhrdi1xkrj3hsrydjcpb4w8nc595qzpbi4v38vl33sh")))

(define-public crate-pepper-plugin-lsp-0.15.0 (c (n "pepper-plugin-lsp") (v "0.15.0") (d (list (d (n "pepper") (r "^0.28") (d #t) (k 0)))) (h "1pq61pwfm520iwmilsnnqlxycw2gv0p2k5372k40h0nfvfbm8p5r")))

(define-public crate-pepper-plugin-lsp-0.16.0 (c (n "pepper-plugin-lsp") (v "0.16.0") (d (list (d (n "pepper") (r "^0.29") (d #t) (k 0)))) (h "0p0nsmb2lns4imk9zhrkcva2yg85gizb9lp2c4yjb0rxpayykfbx")))

(define-public crate-pepper-plugin-lsp-0.17.0 (c (n "pepper-plugin-lsp") (v "0.17.0") (d (list (d (n "pepper") (r "^0.30") (d #t) (k 0)))) (h "0k24rzyxwy37xifzbqsagndy73s8w79qxkdyx8z59dpgyqswanjd")))

(define-public crate-pepper-plugin-lsp-0.18.0 (c (n "pepper-plugin-lsp") (v "0.18.0") (d (list (d (n "pepper") (r "^0.31") (d #t) (k 0)))) (h "1x6xsrwhb2zw2v13q79r5mfj39sbzln57c7y4p36qridd6ncdarw")))

