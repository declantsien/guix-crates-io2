(define-module (crates-io pe pp pepper-win) #:use-module (crates-io))

(define-public crate-pepper-win-0.1.0 (c (n "pepper-win") (v "0.1.0") (d (list (d (n "pepper") (r "^0.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winerror" "winuser" "errhandlingapi" "stringapiset" "winnls" "consoleapi" "processenv" "handleapi" "synchapi" "ioapiset" "namedpipeapi"))) (d #t) (k 0)))) (h "1w194rrivwxr4ssnxgkcmqxyail21vs7g7xmlwbr4afv1wq1z7f7")))

(define-public crate-pepper-win-0.1.1 (c (n "pepper-win") (v "0.1.1") (h "18ici5ghdy5nff4f19dh5fy3fawrrzdjiz773p9npb2sc11nx6v0")))

