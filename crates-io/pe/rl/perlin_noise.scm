(define-module (crates-io pe rl perlin_noise) #:use-module (crates-io))

(define-public crate-perlin_noise-1.0.0 (c (n "perlin_noise") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1kjwb500w0x4xgj38pzcf38ppqhsqsklg3w68mvqvfxvhr0xr2ll")))

(define-public crate-perlin_noise-1.0.1 (c (n "perlin_noise") (v "1.0.1") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1yk1pmnyz55w6jjsw1s3x26icnl5rh8kp9bj351dqpbjapd65ji3")))

