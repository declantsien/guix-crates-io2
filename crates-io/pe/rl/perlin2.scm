(define-module (crates-io pe rl perlin2) #:use-module (crates-io))

(define-public crate-perlin2-0.1.0 (c (n "perlin2") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1a32j8cmac0bk2a3414h5dbyh79x9rv6laxhsl7wlvh3yz2j9xjr")))

(define-public crate-perlin2-0.1.1 (c (n "perlin2") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1fgrk6xwbg042wkaidvzhk4frxkmbi5v0w5xr87nvcmpfmcmlnzy") (f (quote (("serialize" "serde"))))))

