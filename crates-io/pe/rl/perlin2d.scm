(define-module (crates-io pe rl perlin2d) #:use-module (crates-io))

(define-public crate-perlin2d-0.1.0 (c (n "perlin2d") (v "0.1.0") (h "0m49bv94vjqvfmf4nszw3jwdc7i0xbb8c9qa9lxjizxq6a2vs8gs") (y #t)))

(define-public crate-perlin2d-0.2.0 (c (n "perlin2d") (v "0.2.0") (h "13rhliv7rcvmlm5zk9zhfqafcvzxxd4mpmlh896wsb3j0vjg9wnq") (y #t)))

(define-public crate-perlin2d-0.2.5 (c (n "perlin2d") (v "0.2.5") (h "14f9396fwck9fy7n7hndy0kmaknacjyvm655axyrrh68pnnj6rgw") (y #t)))

(define-public crate-perlin2d-0.2.6 (c (n "perlin2d") (v "0.2.6") (h "1q6df4828arnk283hqdb9p5xkjydhr3ljnghc8qc9690v77p2v5v")))

