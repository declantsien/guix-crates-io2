(define-module (crates-io pe rl perlin) #:use-module (crates-io))

(define-public crate-perlin-0.1.0 (c (n "perlin") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0bnfdc6w3hzl9wjckrd52yvs1ic24y093d1d38l2n4db2j7dh3hf")))

