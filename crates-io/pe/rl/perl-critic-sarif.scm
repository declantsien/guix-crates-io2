(define-module (crates-io pe rl perl-critic-sarif) #:use-module (crates-io))

(define-public crate-perl-critic-sarif-0.1.0 (c (n "perl-critic-sarif") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1mlf6fcn7a80yb818wqlnn6cq5zh1xp8nwp33dp4q11dvz172s2z")))

