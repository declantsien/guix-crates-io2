(define-module (crates-io pe pe pepe-actix) #:use-module (crates-io))

(define-public crate-pepe-actix-0.1.0 (c (n "pepe-actix") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "0f8a7ija18040xx25dy933xda7sa69w7kgb53jljdaayaxxn6f5q")))

(define-public crate-pepe-actix-0.1.1 (c (n "pepe-actix") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.2.1") (f (quote ("macros"))) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "0zjsa920gayna8jcj38bkgcfvbcnyn3dwm0j7h6kggrqv31r1m1s")))

(define-public crate-pepe-actix-0.1.2 (c (n "pepe-actix") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.2.1") (f (quote ("macros"))) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "0n75pnd3a63wgwpaa3hjl7xslxa53602fam99wnxnnak6124j63i")))

(define-public crate-pepe-actix-0.1.3 (c (n "pepe-actix") (v "0.1.3") (d (list (d (n "actix-web") (r "^4.2.1") (f (quote ("macros"))) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "014vljn5y1si3ryj623mnh4pgp2pbky1p9kwviqypg3gxw3zkpr5")))

(define-public crate-pepe-actix-0.1.4 (c (n "pepe-actix") (v "0.1.4") (d (list (d (n "actix-web") (r "^4.2.1") (f (quote ("macros"))) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "0117z4gnckvsgrp4g0ikjm8lnmb1w6lcaby18x3j7c64srynr05w")))

(define-public crate-pepe-actix-0.1.5 (c (n "pepe-actix") (v "0.1.5") (d (list (d (n "actix-web") (r "^4.2.1") (f (quote ("macros"))) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.1") (d #t) (k 0)))) (h "1hjyf49v6d16yayv8iss78nyvg0yl22v62l5yy6lr9vzhd81h1l3")))

