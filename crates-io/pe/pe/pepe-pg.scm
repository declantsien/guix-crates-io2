(define-module (crates-io pe pe pepe-pg) #:use-module (crates-io))

(define-public crate-pepe-pg-0.1.0 (c (n "pepe-pg") (v "0.1.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("r2d2" "postgres" "chrono"))) (k 0)) (d (n "diesel_migrations") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1h5kfam1gvhg89cbakfx2xc1qhymj054x3gbc3hiyfbz1q00hdri")))

(define-public crate-pepe-pg-0.1.1 (c (n "pepe-pg") (v "0.1.1") (d (list (d (n "diesel") (r "^2.1") (f (quote ("postgres" "chrono"))) (k 0)) (d (n "diesel-tracing") (r "^0.2.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0p6fm1lg65x63kig2fpjr23i32g7xpk19xr03jkypm4a123d972n") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:diesel-tracing"))))))

