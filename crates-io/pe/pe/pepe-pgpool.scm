(define-module (crates-io pe pe pepe-pgpool) #:use-module (crates-io))

(define-public crate-pepe-pgpool-0.1.0 (c (n "pepe-pgpool") (v "0.1.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("r2d2" "postgres" "chrono" "uuid" "32-column-tables"))) (k 0)) (d (n "diesel_migrations") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0wflhla0sk5202kv2p4rr5igapncl88vykf7qihzcqlcyq6r9dak")))

(define-public crate-pepe-pgpool-0.1.1 (c (n "pepe-pgpool") (v "0.1.1") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("r2d2" "postgres" "chrono" "uuid" "32-column-tables"))) (k 0)) (d (n "diesel_migrations") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "06zln1jfnqggf26639hfrnin078glpl76rg7bhrmpra665640wdy")))

(define-public crate-pepe-pgpool-0.2.0 (c (n "pepe-pgpool") (v "0.2.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("r2d2" "postgres" "chrono"))) (k 0)) (d (n "diesel_migrations") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "08rxwgrp851pc3kkhm2s79ycr0k00nanvp9z0z6gfycjfk22gzvl") (f (quote (("diesel-uuid" "diesel/uuid") ("diesel-32-column-tables" "diesel/32-column-tables"))))))

(define-public crate-pepe-pgpool-0.2.1 (c (n "pepe-pgpool") (v "0.2.1") (d (list (d (n "diesel") (r "^2.1") (f (quote ("r2d2" "postgres" "chrono"))) (k 0)) (d (n "diesel_migrations") (r "^2.1") (d #t) (k 0)) (d (n "pepe-pg") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0bnki91r1cl9z9pag244r1nmsy2zl0g59hdvpy67578ijjzqd19z") (f (quote (("diesel-uuid" "diesel/uuid") ("diesel-32-column-tables" "diesel/32-column-tables"))))))

