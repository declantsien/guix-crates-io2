(define-module (crates-io pe pe pepe-config) #:use-module (crates-io))

(define-public crate-pepe-config-0.0.1 (c (n "pepe-config") (v "0.0.1") (d (list (d (n "config") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "17rj4asylkxpch02fkg372ki7q7hlxykzdm5vbbhr4a2a5qgs8yd")))

(define-public crate-pepe-config-0.0.2 (c (n "pepe-config") (v "0.0.2") (d (list (d (n "config") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0k4kqy74smx1ahpg0m22livgn1yzqhy8nhdlc4x0qh2ywx8z85h4")))

(define-public crate-pepe-config-0.0.3 (c (n "pepe-config") (v "0.0.3") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02nr03p1bi65fhr415kqvksg1p42rvnwrb641jlq4vl56y9xjbgm")))

(define-public crate-pepe-config-0.0.4 (c (n "pepe-config") (v "0.0.4") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04l9l1mramx30zz4nkjn2q2x37ckaqwajpq722n887gafig782gy")))

(define-public crate-pepe-config-0.0.5 (c (n "pepe-config") (v "0.0.5") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0r7k2nqpvj79x7b7qcsxzni8h6blzdmps6avad32f4n4amr106cn")))

(define-public crate-pepe-config-0.0.6 (c (n "pepe-config") (v "0.0.6") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zks9d9snddrf2zk2zczjfhqcgblp3h18wllja6alvvqcqdc6lqs")))

(define-public crate-pepe-config-0.0.7 (c (n "pepe-config") (v "0.0.7") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1a5czhay8vrkjil1ypy63rqkn22zvwspn6i297x2nh57p2yagsg6")))

(define-public crate-pepe-config-0.0.8 (c (n "pepe-config") (v "0.0.8") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yawp0pjp7z73r4690rpc2y27ldsh8w62abgcn42fa0n8yr234a0")))

(define-public crate-pepe-config-0.0.9 (c (n "pepe-config") (v "0.0.9") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02a5pa97n07mykb2c7abrvqrrqzx2jq1mdbzy63rnxl90c7pidq2")))

(define-public crate-pepe-config-0.0.10 (c (n "pepe-config") (v "0.0.10") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0q385303ch1233syqdaqa8blycaqzn872l4az3wkkkanzzrdr0jy")))

(define-public crate-pepe-config-0.0.11 (c (n "pepe-config") (v "0.0.11") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0whyl4csycmrfzh23sdg39ckzila77bwghq6ignlnpa9qigx7dkr")))

(define-public crate-pepe-config-0.1.0 (c (n "pepe-config") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1wsk87jhr0d1ybany11j8ad95mbdwblfiw5k6jndhdm73hfk7ffy")))

(define-public crate-pepe-config-0.2.0 (c (n "pepe-config") (v "0.2.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1317idpfybwb4kjkh1rl8nq70p62y77ik4gl0nwcnpdqxi6w1vsl")))

(define-public crate-pepe-config-0.2.1 (c (n "pepe-config") (v "0.2.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1z86mgn1rz36cac616iicpbkxb82ii2n45xf40s9mknxa6wg1g2d")))

(define-public crate-pepe-config-0.2.2 (c (n "pepe-config") (v "0.2.2") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "duration-string") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0w364w6wdbz65prrpix02y98mg5f5qyhcxmmw93g8s9dsg2wv10v")))

(define-public crate-pepe-config-0.2.3 (c (n "pepe-config") (v "0.2.3") (d (list (d (n "config") (r "^0.14") (d #t) (k 0)) (d (n "duration-string") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yfyqsdbdjqwyrq0lnp2p6janpndda83bq747rm0p1919fc5x7li")))

