(define-module (crates-io pe m_ pem_helper) #:use-module (crates-io))

(define-public crate-pem_helper-0.1.0 (c (n "pem_helper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0r1s08bxf2lzf4z0nswm05c1fzig77nvjzyrj76ncb3f8aia2zcq") (y #t)))

