(define-module (crates-io pe nr penrose_keysyms) #:use-module (crates-io))

(define-public crate-penrose_keysyms-0.1.0 (c (n "penrose_keysyms") (v "0.1.0") (d (list (d (n "strum") (r "^0.19.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 0)))) (h "1y3pr9l2nac9h5mj87zl4lh9534npmfjrdiqj3walv4j64ijiw41")))

(define-public crate-penrose_keysyms-0.1.1 (c (n "penrose_keysyms") (v "0.1.1") (d (list (d (n "strum") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "1mn2pri410892m2n5n1llwryma5fv0idfz99znqhnzixzvq4smvs")))

(define-public crate-penrose_keysyms-0.3.3 (c (n "penrose_keysyms") (v "0.3.3") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)))) (h "059i1xx25w55zcchqyji16pmicnnhbkxschqpn1hhqncxqazrcw0")))

(define-public crate-penrose_keysyms-0.3.4 (c (n "penrose_keysyms") (v "0.3.4") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)))) (h "0wyi1j79x3b1rr0vcbcq84f5ca46cywaqhjfyhzzk97mpawj9q2i")))

