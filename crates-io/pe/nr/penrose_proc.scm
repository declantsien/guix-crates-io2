(define-module (crates-io pe nr penrose_proc) #:use-module (crates-io))

(define-public crate-penrose_proc-0.1.0 (c (n "penrose_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cg7g1hlpvyfi0nbzl3m7h6fw588vsf44wsqimnkci8lfz3xp5x0")))

(define-public crate-penrose_proc-0.1.1 (c (n "penrose_proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19j9khm5ka411kbgza25yzk7qd8f17f8av6zz8q9492fkc9r2id2")))

(define-public crate-penrose_proc-0.1.2 (c (n "penrose_proc") (v "0.1.2") (d (list (d (n "penrose_keysyms") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jfq2nlgmi4nzknls33mknqb2kqsk0243cyv0zrsxy4l3xn62wzn")))

(define-public crate-penrose_proc-0.1.3 (c (n "penrose_proc") (v "0.1.3") (d (list (d (n "penrose_keysyms") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1fgby4i5m2k04x5i81b7iwlyz52xnf5q7l4gs2xn7vfkw0w98kja")))

