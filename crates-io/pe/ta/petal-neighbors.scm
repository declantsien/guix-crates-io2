(define-module (crates-io pe ta petal-neighbors) #:use-module (crates-io))

(define-public crate-petal-neighbors-0.1.0 (c (n "petal-neighbors") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0sz286sxr48qizsdfna4fllr6avrd9n1ms4palnga2jpkzbwh0ia")))

(define-public crate-petal-neighbors-0.2.0 (c (n "petal-neighbors") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 2)))) (h "0ps5gbaajcfxcqnx12f8nmnxjrp0bkgdqk6bbamcmbzma681k4rc")))

(define-public crate-petal-neighbors-0.3.0 (c (n "petal-neighbors") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 2)))) (h "0n81hdngr92sb1rb56dgvq91fnvr1v08l7dnn4h1fsfz0k305j56")))

(define-public crate-petal-neighbors-0.4.0 (c (n "petal-neighbors") (v "0.4.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13w91y59pzh6ww85w7g7mnr7zm1blnszzqi4r5syqq20200xw5x1")))

(define-public crate-petal-neighbors-0.5.0 (c (n "petal-neighbors") (v "0.5.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jvlk309qvlklm7sq50gvci1pnl7i92k5cmkhfsbk4ipgfif9is8")))

(define-public crate-petal-neighbors-0.5.1 (c (n "petal-neighbors") (v "0.5.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l5wpyzxgprn75q04mc5fvbrczj7z6b2jfiyhxr389q1llnydw0c")))

(define-public crate-petal-neighbors-0.5.2 (c (n "petal-neighbors") (v "0.5.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jkd7c7dr1bdg53jcfzd9yr6k6w1c40n859nicbb8nd8wn7rh64l")))

(define-public crate-petal-neighbors-0.6.0 (c (n "petal-neighbors") (v "0.6.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12pbq8r36bvp7ca4fyshg38g8pqd4vi15sk2nvqsylzdclgkr185")))

(define-public crate-petal-neighbors-0.7.0 (c (n "petal-neighbors") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02sxj5xigxs587lnz72zzi8h6dcvgx51vgq3r7sblla5i2d95gmc")))

(define-public crate-petal-neighbors-0.7.1 (c (n "petal-neighbors") (v "0.7.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vm5snszjc3p5pz1i5dvdf8gl0x6ramzzs5i5pn81n67nyfj7h5q")))

(define-public crate-petal-neighbors-0.8.0 (c (n "petal-neighbors") (v "0.8.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q3l5kcw72d92yvr05hjdc19mjjr2qly3i0sz0cnk98724gczdc6")))

(define-public crate-petal-neighbors-0.9.0 (c (n "petal-neighbors") (v "0.9.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12sqkkh0f0sdaxf7dny5ab2jd0bkcsyfvszipdpsk6hkc1x11q90") (r "1.64")))

(define-public crate-petal-neighbors-0.10.0 (c (n "petal-neighbors") (v "0.10.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fh5lz82m0cgwj0sryahk15badjwvyvqvhrchg7253w0517yrs2w") (r "1.70")))

