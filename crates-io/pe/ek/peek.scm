(define-module (crates-io pe ek peek) #:use-module (crates-io))

(define-public crate-peek-0.0.0 (c (n "peek") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0zllg741d04h05qg8p30fdg3916704xblk37wrwkcj5lc76ls70q")))

