(define-module (crates-io pe ek peek-poke) #:use-module (crates-io))

(define-public crate-peek-poke-0.2.0 (c (n "peek-poke") (v "0.2.0") (d (list (d (n "euclid") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "peek-poke-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0bxgb6zgdsa1s7fvgn1z8vxjr1r2gb4l716hd0karwgbfnjxcgyr") (f (quote (("extras" "derive" "euclid") ("derive" "peek-poke-derive") ("default" "derive"))))))

