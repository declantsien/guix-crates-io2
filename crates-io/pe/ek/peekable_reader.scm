(define-module (crates-io pe ek peekable_reader) #:use-module (crates-io))

(define-public crate-peekable_reader-0.0.1 (c (n "peekable_reader") (v "0.0.1") (h "1l48pkn1g9zrpkmh331hz3krav8dkfcj7myb19n31amy5waakvmf")))

(define-public crate-peekable_reader-0.0.2 (c (n "peekable_reader") (v "0.0.2") (h "1mm5vvkff8v3q8a50h1zkxaxn3l5811mjll4iq328amqhkhgisf7")))

