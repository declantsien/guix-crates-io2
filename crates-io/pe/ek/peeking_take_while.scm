(define-module (crates-io pe ek peeking_take_while) #:use-module (crates-io))

(define-public crate-peeking_take_while-0.1.0 (c (n "peeking_take_while") (v "0.1.0") (h "13a55yvivjr4svxpryidq9g9rwyj69blzwy5p66a0415jai6yg0m")))

(define-public crate-peeking_take_while-0.1.1 (c (n "peeking_take_while") (v "0.1.1") (h "1rqcyw7pn37gnp7schmml05xzw010g8ynlz3dqs6wz8wd6f071kk")))

(define-public crate-peeking_take_while-0.1.2 (c (n "peeking_take_while") (v "0.1.2") (h "16bhqr6rdyrp12zv381cxaaqqd0pwysvm1q8h2ygihvypvfprc8r")))

(define-public crate-peeking_take_while-1.0.0 (c (n "peeking_take_while") (v "1.0.0") (h "065mgsqx76hnjp3vdqf2zam2grvmdyx5hfxqw7wgyx85icbx57ly")))

