(define-module (crates-io pe ek peekaboo) #:use-module (crates-io))

(define-public crate-peekaboo-0.1.0 (c (n "peekaboo") (v "0.1.0") (h "0pqabi18p3p2346dwbwrqvw4v7z0v551ny9a14qka7dbsnbx3sjf")))

(define-public crate-peekaboo-0.2.0 (c (n "peekaboo") (v "0.2.0") (h "1aq4iwwb2g0937zk1mngf020jmnsxfwc7wb88s7286baqldsshc5")))

(define-public crate-peekaboo-0.2.1 (c (n "peekaboo") (v "0.2.1") (h "1lv2zh5dwgynpskizaa9pjysamsj11lkyhnm6vq7jd7gl2qb4yj9")))

(define-public crate-peekaboo-0.3.0 (c (n "peekaboo") (v "0.3.0") (h "1iks9znqx17fhhryrv900hzn2k6kn3qxcjaywvvkg5nj7yq0xhg4")))

