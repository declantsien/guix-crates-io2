(define-module (crates-io pe ek peekread) #:use-module (crates-io))

(define-public crate-peekread-0.1.0 (c (n "peekread") (v "0.1.0") (h "01mfig43gcfh8a7qp14zjdji9ci3wskc1f8n6z8fn5z6azil14nq")))

(define-public crate-peekread-0.1.1 (c (n "peekread") (v "0.1.1") (h "07b8bz654syl17fl7kmad68njw05ax9p9423ccyfsf9gqz6j50cp")))

