(define-module (crates-io pe ek peek-poke-derive) #:use-module (crates-io))

(define-public crate-peek-poke-derive-0.2.1 (c (n "peek-poke-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "09m96jphg5d3n8ann35yx1f97l76ygx2snf81yz87advqljlmd3g")))

