(define-module (crates-io pe ek peekable_next) #:use-module (crates-io))

(define-public crate-peekable_next-0.1.0 (c (n "peekable_next") (v "0.1.0") (h "0iph7zr83qsai63c417d23gw7z1nqhy74gasnjzmz59yiiwcb7fr")))

(define-public crate-peekable_next-0.1.1 (c (n "peekable_next") (v "0.1.1") (h "0ayd693im35v2dd051cg77kk5544i3x6pllq2lh7bj2jzfd5y55b")))

(define-public crate-peekable_next-0.1.2 (c (n "peekable_next") (v "0.1.2") (h "1di6xhp2h8gxqxfwkr2nc20asz1i1i9j81nfkvpgch2pvgqy388r")))

(define-public crate-peekable_next-0.2.0 (c (n "peekable_next") (v "0.2.0") (h "1x1vwpnvbj12qy2jb4jphd6m3gc4wp6zpy1ff2xkm21d3djv3slj")))

