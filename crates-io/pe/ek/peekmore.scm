(define-module (crates-io pe ek peekmore) #:use-module (crates-io))

(define-public crate-peekmore-0.1.0 (c (n "peekmore") (v "0.1.0") (h "18mn9hi1mpc8ffmazpg98qzwg2qiqsrb5va145d52hj6nl3wj3hk")))

(define-public crate-peekmore-0.2.0 (c (n "peekmore") (v "0.2.0") (h "0vfbjx0av8njmjayddshphwdl8v1ynvfqvkm2wqvw7mccwci9cqn")))

(define-public crate-peekmore-0.3.0 (c (n "peekmore") (v "0.3.0") (d (list (d (n "smallvec") (r "^0.6.10") (o #t) (k 0)))) (h "1514fgvlqkvxfdrlsgczy5vx3pr55j2h1868n0r4y23rh37zqi6s") (f (quote (("default"))))))

(define-public crate-peekmore-0.3.1 (c (n "peekmore") (v "0.3.1") (d (list (d (n "smallvec") (r "^0.6.10") (o #t) (k 0)))) (h "04vmbvqp6yln5qaf7hiqc3ws4dqll75q8yvpkdj7apimjv79ha31") (f (quote (("default"))))))

(define-public crate-peekmore-0.4.0 (c (n "peekmore") (v "0.4.0") (d (list (d (n "smallvec") (r "^0.6.10") (o #t) (k 0)))) (h "0kki0vbb8vj7nxi52b9v916p97sqfifi8d00jjkd9wh3k2akdxm2") (f (quote (("default"))))))

(define-public crate-peekmore-0.5.0 (c (n "peekmore") (v "0.5.0") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "0k21gpb49fq9ldwzwka1w7qmizcdqlgm210iayg6b616q6bimhg0") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.1 (c (n "peekmore") (v "0.5.1") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "1i6fwhazch2wh019j9lj611pnvgldjzyp8h4wqs19ss7dv34apng") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.2 (c (n "peekmore") (v "0.5.2") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "1flw5c0mkz2dwbdvpsmvny9d5qafd0hnlhjzxgqhmpyr4jq66y8v") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.3 (c (n "peekmore") (v "0.5.3") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "0ghbbi0x089zjh8g1nl66rvv29dz6sqbfm83xaahjy61gx67rlwf") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.4 (c (n "peekmore") (v "0.5.4") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "17zjmsvqlbldx6wqzwgjf84b3pp6givba8085rmcz3ij4ygfhag0") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.5 (c (n "peekmore") (v "0.5.5") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "1n317vci17slz7gd0bgixz6yvplvqgc3bffwvfq1dkvav07xryxr") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-0.5.6 (c (n "peekmore") (v "0.5.6") (d (list (d (n "smallvec") (r "^1.4.0") (o #t) (k 0)))) (h "05lkl45p3xja40cx8c17958f4bbs5xqcxgqrpn0g1jqlqlqzi2a0") (f (quote (("default" "smallvec"))))))

(define-public crate-peekmore-1.0.0 (c (n "peekmore") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.5.1") (o #t) (k 0)))) (h "125ynnbpfjp2m1pb900pcb8bv0jkzrgbwdbfjjd9zq1dxl8vm16a") (f (quote (("default"))))))

(define-public crate-peekmore-1.1.0 (c (n "peekmore") (v "1.1.0") (d (list (d (n "smallvec") (r "^1.5.1") (o #t) (k 0)))) (h "11vr48invhn48bwirs75515i6a0c1b9385cxf76dd003w5cyl3xg") (f (quote (("default"))))))

(define-public crate-peekmore-1.2.0 (c (n "peekmore") (v "1.2.0") (d (list (d (n "smallvec") (r "^1.5.1") (o #t) (k 0)))) (h "13mdidp6vfjnlnvd9i8yjg9im145ia7vmmbwxzs7qblllpvcsqcl") (f (quote (("default"))))))

(define-public crate-peekmore-1.2.1 (c (n "peekmore") (v "1.2.1") (d (list (d (n "smallvec") (r "^1.5.1") (o #t) (k 0)))) (h "1s6wwghxi5fmv8w0n86fhklylsxbr97jp4zyx0qxaxkb8fqdq2id") (f (quote (("default"))))))

(define-public crate-peekmore-1.3.0 (c (n "peekmore") (v "1.3.0") (d (list (d (n "smallvec") (r "^1.5.1") (o #t) (k 0)))) (h "0n6p8wyyc9mdmzbc9qaj1kqnf5qc23jp2c8vim93vs30jwjy2qwi") (f (quote (("default"))))))

