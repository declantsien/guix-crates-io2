(define-module (crates-io pe ek peeking-iter) #:use-module (crates-io))

(define-public crate-peeking-iter-0.1.0 (c (n "peeking-iter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.13.0") (d #t) (k 2)))) (h "0gs9lpabjimdpq8idnvlx21nnan65zsjmcrcnfa0ydxdn2zncd81")))

(define-public crate-peeking-iter-0.1.1 (c (n "peeking-iter") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.13.0") (d #t) (k 2)))) (h "0iqql1b48n365l1wx3mddn6lz37bbyyq2mzwf0n63l9kbns1aywf")))

(define-public crate-peeking-iter-0.1.2 (c (n "peeking-iter") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.13.0") (d #t) (k 2)))) (h "1999c8iv47wb8bw6gnvf5kln061s05wgrpcxbbfgfmh9vfj9pasg")))

