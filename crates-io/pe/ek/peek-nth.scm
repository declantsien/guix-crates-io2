(define-module (crates-io pe ek peek-nth) #:use-module (crates-io))

(define-public crate-peek-nth-0.1.0 (c (n "peek-nth") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "1ffqs6r6sdw3qd007j3c91zwgqirsr1rmkbw5nf1j1494m0bfnr4")))

(define-public crate-peek-nth-0.2.0 (c (n "peek-nth") (v "0.2.0") (d (list (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "1v8zvrk2gh5pl17a76df196mk5swfmi0rwax43p7kabv6zb9wypv")))

