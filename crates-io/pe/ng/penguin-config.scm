(define-module (crates-io pe ng penguin-config) #:use-module (crates-io))

(define-public crate-penguin-config-0.1.0 (c (n "penguin-config") (v "0.1.0") (d (list (d (n "penguin-config-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fypbsgvpwc46jhcdpff6jnirn2vplnd2klzw9fm5k8z6796b8n3")))

(define-public crate-penguin-config-0.1.1 (c (n "penguin-config") (v "0.1.1") (d (list (d (n "penguin-config-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gs3pn9wf9kgf01dxkfyn4l0bjjziqw4akqam0li2711pzfnqcci")))

