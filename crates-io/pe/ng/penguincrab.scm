(define-module (crates-io pe ng penguincrab) #:use-module (crates-io))

(define-public crate-penguincrab-0.1.0 (c (n "penguincrab") (v "0.1.0") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)))) (h "0ggzav5cld4anbsabgh6p3z1s57q63jmpn70wrxxsh5a2z3n2zya") (y #t) (l "shared")))

(define-public crate-penguincrab-0.1.1 (c (n "penguincrab") (v "0.1.1") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)))) (h "0mr4ykmjbxdkqsrrfw6l5m6lbc0ygiijyxxdmim12h2r93km9yfq") (y #t) (l "shared")))

(define-public crate-penguincrab-0.1.2 (c (n "penguincrab") (v "0.1.2") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)))) (h "0cv8f7ikwdcl2lzak1ibnwmm5682sihvwb4laddz2kdpn7nz1jq2") (y #t) (l "shared")))

(define-public crate-penguincrab-0.1.3 (c (n "penguincrab") (v "0.1.3") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "03if9i3np6lg97qivg3k1m2cvhcpq36cpg58ikawb7hx5xs92lkl") (l "shared")))

(define-public crate-penguincrab-0.1.4 (c (n "penguincrab") (v "0.1.4") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "0wdn7cjyrpcxfg9zfi76sfs18vgmsimxdc5vvgx0fliy6cg661pl") (l "shared")))

(define-public crate-penguincrab-0.1.5 (c (n "penguincrab") (v "0.1.5") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "0wqqwr6q5hxjgmdkb6bh9sgpj953scqfhs48d17c2zb7w9bhaiyq") (l "shared")))

(define-public crate-penguincrab-0.1.6 (c (n "penguincrab") (v "0.1.6") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1syj9jd20w76rlxa1dsai9s0fdb0jfrnn473xna08yb9azrjckr8") (l "shared")))

(define-public crate-penguincrab-0.1.61 (c (n "penguincrab") (v "0.1.61") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "0cz1s6jl2nfn5nqkba94ngnz7lwf7711zs8ms9xhg1df54yimyqz") (l "shared")))

(define-public crate-penguincrab-0.1.62 (c (n "penguincrab") (v "0.1.62") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "06pl0h10qibfg3b5vjm1qjrw5rbax047r6hpdnb106cf69vz2hkh") (l "shared")))

(define-public crate-penguincrab-0.1.63 (c (n "penguincrab") (v "0.1.63") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "10bsbqfs93ypzm0kcw3czx9nv9mngp7f66sa5axa94ix4fncglfw") (l "shared")))

