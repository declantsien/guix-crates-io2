(define-module (crates-io pe ng peng-parser) #:use-module (crates-io))

(define-public crate-peng-parser-0.1.0 (c (n "peng-parser") (v "0.1.0") (h "0v0vi4pqf056rhcw7iardyc6s21glzvrhgqgj1aa5xcx5ks4s504") (y #t)))

(define-public crate-peng-parser-0.1.1 (c (n "peng-parser") (v "0.1.1") (h "1ihbakjhhkdhc8i9w16i461rnk4gmn9nvax7lzrfdgifj7gdfrps") (y #t)))

