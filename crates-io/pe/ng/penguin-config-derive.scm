(define-module (crates-io pe ng penguin-config-derive) #:use-module (crates-io))

(define-public crate-penguin-config-derive-0.1.0 (c (n "penguin-config-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "07k3d4ynd9dbnsmxjjc3v5d7pcva2l0kyvv70rndj77zkvfdg3cz")))

(define-public crate-penguin-config-derive-0.1.1 (c (n "penguin-config-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0r902pxb5g3f2b2mmbsz93c3jah1qw7s9b20zgxsx84mnawhpili")))

