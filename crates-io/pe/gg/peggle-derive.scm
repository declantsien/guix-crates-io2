(define-module (crates-io pe gg peggle-derive) #:use-module (crates-io))

(define-public crate-peggle-derive-0.1.0 (c (n "peggle-derive") (v "0.1.0") (d (list (d (n "peggle") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "13hn230j8vkj5spl957j4pi1hrrl593d2jxcgdyv8g9bpp9v7w3p")))

