(define-module (crates-io pe tc petclinic-protobuf-pet) #:use-module (crates-io))

(define-public crate-petclinic-protobuf-pet-0.0.24 (c (n "petclinic-protobuf-pet") (v "0.0.24") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "13bxk3wx2fb7qlc65yamfxzwjbwdmq232lvkphkj7r6sppfln9nk")))

(define-public crate-petclinic-protobuf-pet-0.0.25 (c (n "petclinic-protobuf-pet") (v "0.0.25") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1yn8v1scj2nnq5iixbsa0q7jmn26nj0656xm2dh223hwix55lmgh")))

(define-public crate-petclinic-protobuf-pet-0.0.26 (c (n "petclinic-protobuf-pet") (v "0.0.26") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "06c91m08hz6z3hgmqrmgsqa27nqkg62qmr73j53j1ma370a3yh7a")))

(define-public crate-petclinic-protobuf-pet-0.0.27 (c (n "petclinic-protobuf-pet") (v "0.0.27") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1fsal6yqfgrqp7f38048zz1cf4flkrszra6sg3wmfw3gb7vahv2p")))

(define-public crate-petclinic-protobuf-pet-0.0.28 (c (n "petclinic-protobuf-pet") (v "0.0.28") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0h5grdqaamv0chvfzsw8p49ah0j0w23i6yiyr351fayn9nblkbwf")))

(define-public crate-petclinic-protobuf-pet-0.0.29 (c (n "petclinic-protobuf-pet") (v "0.0.29") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1pv9024gmqb0l8ld4h7487k6j9h5jj5qhgkndpknxjfcyrm11c2h")))

(define-public crate-petclinic-protobuf-pet-0.0.30 (c (n "petclinic-protobuf-pet") (v "0.0.30") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "01l7fzq5v1s4kd91jill4n0g2ax92frpsahqznzf01kq6k4gpnn1")))

