(define-module (crates-io pe tc petclinic-protobuf-user) #:use-module (crates-io))

(define-public crate-petclinic-protobuf-user-0.0.24 (c (n "petclinic-protobuf-user") (v "0.0.24") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "08hl3ilbrghrp1iixk61mfn2jnn9imns2vdw3yisc8qsihqhr2m3")))

(define-public crate-petclinic-protobuf-user-0.0.25 (c (n "petclinic-protobuf-user") (v "0.0.25") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0dr1fdsvdg95wlhn3rc9g6gfml1ri8b6z4pygyg3937232avnlpv")))

(define-public crate-petclinic-protobuf-user-0.0.26 (c (n "petclinic-protobuf-user") (v "0.0.26") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1i0772p0qk9kim7irrxy3s3aw1rgsgbymwrd8lj4h3ikbda42sai")))

(define-public crate-petclinic-protobuf-user-0.0.27 (c (n "petclinic-protobuf-user") (v "0.0.27") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0awa1hv2c3rw199l10rhgp637hcvvblh2np3my8xy296b5s61nfh")))

(define-public crate-petclinic-protobuf-user-0.0.28 (c (n "petclinic-protobuf-user") (v "0.0.28") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "022l1wgf4i7m3ir7mjl0ksmg349p3wvy466q4g4whdfmjcmwdp03")))

(define-public crate-petclinic-protobuf-user-0.0.29 (c (n "petclinic-protobuf-user") (v "0.0.29") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0iydqy00372isidpf45x67bn3ix9kvfzsvcxbvs1pa8b14nshqqm")))

(define-public crate-petclinic-protobuf-user-0.0.30 (c (n "petclinic-protobuf-user") (v "0.0.30") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1c1bllhvk8k3132zldkjpzi85r4h2xjz4hsvqzgy3idlx1806mic")))

