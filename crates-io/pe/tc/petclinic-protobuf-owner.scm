(define-module (crates-io pe tc petclinic-protobuf-owner) #:use-module (crates-io))

(define-public crate-petclinic-protobuf-owner-0.0.24 (c (n "petclinic-protobuf-owner") (v "0.0.24") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0p5272n8fds9h8j8cwllyqq9qbysrfi7c5axbvq538invaf8zrc2")))

(define-public crate-petclinic-protobuf-owner-0.0.25 (c (n "petclinic-protobuf-owner") (v "0.0.25") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0npxm7wfl7m5j3gc327zyrshf69zawl5zhjz4kjgv8p0gv6490hh")))

(define-public crate-petclinic-protobuf-owner-0.0.26 (c (n "petclinic-protobuf-owner") (v "0.0.26") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1f1miv9slys0xfsjy31wlb2vxpq3a5jy508g8m3q6p5rv725fa51")))

(define-public crate-petclinic-protobuf-owner-0.0.27 (c (n "petclinic-protobuf-owner") (v "0.0.27") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1665d4rgwd5nw0bbffv40w01craxv4w3fkfb66md9hdpc51nc848")))

(define-public crate-petclinic-protobuf-owner-0.0.28 (c (n "petclinic-protobuf-owner") (v "0.0.28") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1j37ckp2f11629irv08j8nablidzxxkbj6abgh3ib67gxdp78vsg")))

(define-public crate-petclinic-protobuf-owner-0.0.29 (c (n "petclinic-protobuf-owner") (v "0.0.29") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "01k6imbp43b2f89knpzr0b6xbjp0bf5jjph3sb285169c1dvkpp5")))

(define-public crate-petclinic-protobuf-owner-0.0.30 (c (n "petclinic-protobuf-owner") (v "0.0.30") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1wzysi5xvyv0z3hm8qm2pzz533wyvl3dp1mck2x361b11x72pyw4")))

