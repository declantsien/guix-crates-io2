(define-module (crates-io pe ch pechakucha-gen) #:use-module (crates-io))

(define-public crate-pechakucha-gen-0.1.0 (c (n "pechakucha-gen") (v "0.1.0") (h "00zxfjxqwg7cskcwafs4s1j3c4d16q5mifp86kzdl09m2h2nmbls")))

(define-public crate-pechakucha-gen-0.1.1 (c (n "pechakucha-gen") (v "0.1.1") (h "0z5gvj136ycz8lb125a6n793ys1gkk09a76f4bcjgp8cr8xyvzk0")))

