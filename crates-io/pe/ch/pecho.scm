(define-module (crates-io pe ch pecho) #:use-module (crates-io))

(define-public crate-pecho-1.0.0 (c (n "pecho") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v22g049rgnyn0n4xr5nm73zh45kramknl3fpva5ssqdnsp6cy34")))

(define-public crate-pecho-2.0.0 (c (n "pecho") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11rpnh75pvmn93y6xknk4msk6mi5z3v9dh46h7v9yz8llv0adi7k")))

(define-public crate-pecho-2.0.1 (c (n "pecho") (v "2.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rn1fp6h61z7a95fzzh1v06w04xfkysi83m8b0sg0x2514jz0s7j")))

