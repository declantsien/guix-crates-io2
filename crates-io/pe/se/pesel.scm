(define-module (crates-io pe se pesel) #:use-module (crates-io))

(define-public crate-pesel-0.1.0 (c (n "pesel") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0aypvb911hphll8sr7v9s3kbb22dc44275lxcgb46lvpkml9w5yn")))

(define-public crate-pesel-0.1.1 (c (n "pesel") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0xkfzgfnvdjgkzgj5d9danl0ciwyn38622z435zfrn3bf8p862d1")))

(define-public crate-pesel-0.1.2 (c (n "pesel") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1gp57k2qsbna5f2kdvc3pwhnnp95ixfb26hxc55r5n3q56rhsy2j")))

(define-public crate-pesel-0.1.3 (c (n "pesel") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "15wiiiysah23mnxhbgl7lijhj0687yxwmbm8kfc1s5vcl34d92w2")))

