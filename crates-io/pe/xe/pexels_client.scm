(define-module (crates-io pe xe pexels_client) #:use-module (crates-io))

(define-public crate-pexels_client-0.1.0 (c (n "pexels_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0b8i59wswlhx1q89zh11p0gvxd0db8m5s1556bdk0vr7iaj771mc")))

(define-public crate-pexels_client-0.1.1 (c (n "pexels_client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1razdcqsfb0h5088h5pp13wpj8c1j3y7pn36ljfqgrbb78adxqqa")))

