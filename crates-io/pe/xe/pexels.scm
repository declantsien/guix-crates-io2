(define-module (crates-io pe xe pexels) #:use-module (crates-io))

(define-public crate-pexels-0.1.0 (c (n "pexels") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "06i89vsmz12vx58gxzw8hk0n8yafz8g5q4yb2zd5mflbpssfysbj")))

(define-public crate-pexels-0.1.1 (c (n "pexels") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "003g9i3drpsq4czwi6f841qbhl66p0mf86imqwrilcypbindpizm")))

(define-public crate-pexels-0.1.2 (c (n "pexels") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "04hx9q5wq5ha3idi6pr4mls9g280fgkdlh7f9dn12yap2xn7nd23")))

(define-public crate-pexels-0.2.1 (c (n "pexels") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0fqmmrqri3nb890g6bm02bzdjrv60gsfcklnbxn3l3q7npak4nwz")))

