(define-module (crates-io pe x- pex-trie) #:use-module (crates-io))

(define-public crate-pex-trie-0.0.0 (c (n "pex-trie") (v "0.0.0") (d (list (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-script") (r "^0.5.5") (d #t) (k 0)))) (h "1p8hr0d8f76cg7kpkzjxzmlq0jbpjhix0hqqzlhy5j0jvvxvs13h") (f (quote (("default"))))))

(define-public crate-pex-trie-0.0.1 (c (n "pex-trie") (v "0.0.1") (d (list (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-script") (r "^0.5.5") (d #t) (k 0)))) (h "1a92lrpph5yn5d1my152ykl07vh6ym67rsmj1kha0ffxp65pnj35") (f (quote (("default"))))))

(define-public crate-pex-trie-0.0.3 (c (n "pex-trie") (v "0.0.3") (d (list (d (n "pex") (r "^0.1.0") (f (quote ("ucd-trie" "regex-automata"))) (d #t) (k 2)) (d (n "regex-automata") (r "^0.2.0") (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-script") (r "^0.5.5") (d #t) (k 0)))) (h "0smqa83v6v51svvgqnivp72zp387ai572i49b8q80zxznw1s9jmg") (f (quote (("default"))))))

