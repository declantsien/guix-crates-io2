(define-module (crates-io pe nt pentry) #:use-module (crates-io))

(define-public crate-pentry-0.1.0 (c (n "pentry") (v "0.1.0") (d (list (d (n "getopts") (r "0.2.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "procinfo") (r "0.2.*") (d #t) (t "cfg(unix)") (k 0)))) (h "12wkr512rjhzalrgxa6p518b7c90jm3i5y4yla98pm9xvlmb5nh5")))

(define-public crate-pentry-0.1.1 (c (n "pentry") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "procinfo") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "07j70snqdz8gx58pp4gxkil2qb3s0r7xvkkjs5s0w9qqx791bz52")))

