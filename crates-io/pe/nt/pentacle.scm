(define-module (crates-io pe nt pentacle) #:use-module (crates-io))

(define-public crate-pentacle-0.1.0 (c (n "pentacle") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v4j7ij9f6fpnmdpdsqcs0kbmav85fgc42rsws3i59alj949jhym")))

(define-public crate-pentacle-0.1.1 (c (n "pentacle") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f76vcr3w6jzp21ibppdrvddvqs6jismpa3nwb5azrc7dxgwv2x6")))

(define-public crate-pentacle-0.2.0 (c (n "pentacle") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11hg4fj04737677267kqb4gfryx1l1wr09wvvx6zj2nnmld5yg5y")))

(define-public crate-pentacle-1.0.0 (c (n "pentacle") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dp77q2ddwwryxjgv69hf3rqhb8r9r1wxjk8ra83i5wawgxy8vp2")))

