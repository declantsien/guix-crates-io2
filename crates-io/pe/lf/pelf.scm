(define-module (crates-io pe lf pelf) #:use-module (crates-io))

(define-public crate-pelf-0.1.1 (c (n "pelf") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "14i0220j0cppflnvqa282njrg4j3qspnzabka8wq8iqnk704lp9j")))

(define-public crate-pelf-0.1.2 (c (n "pelf") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0zb8kk06kz1d996d7dfncddncqbxyjsp89kj9njx9wc79pdkci3s")))

(define-public crate-pelf-0.1.3 (c (n "pelf") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0cv8axz13a4v5s82fvxm5f54fabwxg0g3igdjwb4xqjw987pwib8")))

(define-public crate-pelf-0.1.4 (c (n "pelf") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pc6v292fbvzsc7n54x7qk1nj0gy5jvi9saxlnb71vqk3kjdzld1")))

(define-public crate-pelf-0.1.5 (c (n "pelf") (v "0.1.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0igw41zhjzca5b03lisbkkcyf3vhbpkh0hjdg791xbvm7xa5ip0n")))

