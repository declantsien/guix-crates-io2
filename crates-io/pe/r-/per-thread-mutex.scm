(define-module (crates-io pe r- per-thread-mutex) #:use-module (crates-io))

(define-public crate-per-thread-mutex-0.1.0 (c (n "per-thread-mutex") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1y4hj0r7fsy7hpv58h9wawbr8gn4vhxf20nc1azinm01bkch01zh")))

(define-public crate-per-thread-mutex-0.1.1 (c (n "per-thread-mutex") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0q2lx8bd26phm3l2404q3djhs93ykaq357m9d54nw08xljxvvbji")))

(define-public crate-per-thread-mutex-0.1.2 (c (n "per-thread-mutex") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1rdnsgsnxcw7dafqanlf22rp01asvvjw16mxpm722s437399w1vh")))

