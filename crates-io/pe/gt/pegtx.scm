(define-module (crates-io pe gt pegtx) #:use-module (crates-io))

(define-public crate-pegtx-0.1.1 (c (n "pegtx") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "pegnetd") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "07a02xi5d9dmkkr5m4wwy3a6di87awlnhzg2lsvlpm16wvx707zh")))

(define-public crate-pegtx-0.1.4 (c (n "pegtx") (v "0.1.4") (d (list (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "pegnetd") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "1x319nm2cgs9w6hsl2bdyyl949jdrb3bmy8xklz0k3mrmpfw4c1i")))

