(define-module (crates-io pe gt pegtastic-macros) #:use-module (crates-io))

(define-public crate-pegtastic-macros-0.1.0 (c (n "pegtastic-macros") (v "0.1.0") (d (list (d (n "pegtastic-runtime") (r "=0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "14h8x9m9gcrjgxn65a28757hhbvv5473p7rixpal2q3pmqlr4ml8") (f (quote (("trace"))))))

