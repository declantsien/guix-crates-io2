(define-module (crates-io pe dr pedrov) #:use-module (crates-io))

(define-public crate-pedrov-0.1.0 (c (n "pedrov") (v "0.1.0") (h "0c0bvx2gx6aqiiq6h059dhfdlncxag5nf4xk88fqx8fjjwr6r9r2")))

(define-public crate-pedrov-0.2.0 (c (n "pedrov") (v "0.2.0") (h "1yiy7cryixzrp0h9nykiw2z2rnkvpiamqycjsdbidv8667bnqhja")))

