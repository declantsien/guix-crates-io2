(define-module (crates-io pe gi peginator_macro) #:use-module (crates-io))

(define-public crate-peginator_macro-0.1.2 (c (n "peginator_macro") (v "0.1.2") (d (list (d (n "peginator") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "176zdsb8gjlp4fcwgnh18hwdx9wck7fn84mnh7k6mn2zs223yhif")))

(define-public crate-peginator_macro-0.1.3 (c (n "peginator_macro") (v "0.1.3") (d (list (d (n "peginator") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1v7ppp08f9w1137l5j4iy6nax36v7a1zfi8w7mwx5l8isjb4v159")))

(define-public crate-peginator_macro-0.2.0 (c (n "peginator_macro") (v "0.2.0") (d (list (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1gspp6ql2ckn982v1hwdfm2z3ziwwbr5lvqvda7pk9abgskqj8al")))

(define-public crate-peginator_macro-0.2.2 (c (n "peginator_macro") (v "0.2.2") (d (list (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0r4sf5ix8pym512n770dc5ijgwac2xg76iq0f1r86cg9srjzp2wv")))

(define-public crate-peginator_macro-0.2.3 (c (n "peginator_macro") (v "0.2.3") (d (list (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1jh3y3sgpdl0d53qbg7hkzaj8bs19d319frf37qvsqy4acyikk4v")))

(define-public crate-peginator_macro-0.2.4 (c (n "peginator_macro") (v "0.2.4") (d (list (d (n "peginator") (r "=0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1lbmq5zwqd81cnj7cf9sfrp3ymqwpa688sgvj88pxax1nrkgk3bp")))

(define-public crate-peginator_macro-0.2.5 (c (n "peginator_macro") (v "0.2.5") (d (list (d (n "peginator") (r "=0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1ga97qs7p51pd7vikbgc580x16alc6h7mlan560hgzn5w2q0hs2p") (y #t)))

(define-public crate-peginator_macro-0.2.5-a (c (n "peginator_macro") (v "0.2.5-a") (d (list (d (n "peginator") (r "=0.2.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "15xx9r6rsn6wvgi16l226hspqsm2v1hd2sz4d0z6qhknkj60ln4d")))

(define-public crate-peginator_macro-0.2.6 (c (n "peginator_macro") (v "0.2.6") (d (list (d (n "peginator") (r "=0.2.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1yv5bxirgy79isxkl7nbqrv4l8gy43k606crc0fhp59rlpwwd3s0")))

(define-public crate-peginator_macro-0.3.0 (c (n "peginator_macro") (v "0.3.0") (d (list (d (n "peginator") (r "=0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0r6h9pkriizlv1x9n4m4v7jyls4yrnqa6clbw3ka7whcgfr9i673")))

(define-public crate-peginator_macro-0.4.0 (c (n "peginator_macro") (v "0.4.0") (d (list (d (n "peginator") (r "=0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "10vgb9nailkvmyh21igbii8v13xlmfbw3539y64909i23qhbyqjp")))

(define-public crate-peginator_macro-0.5.0 (c (n "peginator_macro") (v "0.5.0") (d (list (d (n "peginator_codegen") (r "=0.5.0") (d #t) (k 0)) (d (n "peginator_runtime") (r "=0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "02gdq90mnka94a9l891spn0d0jhxmvb4gcqi47m3g9znka4kpabw") (y #t)))

(define-public crate-peginator_macro-0.5.1 (c (n "peginator_macro") (v "0.5.1") (d (list (d (n "peginator_codegen") (r "=0.5.1") (d #t) (k 0)) (d (n "peginator_runtime") (r "=0.5.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "14f0cicdmag0p8zw2kzlyzvpx3q4nbb7pangy6q7wiqdmx6yg5ai") (y #t)))

(define-public crate-peginator_macro-0.6.0 (c (n "peginator_macro") (v "0.6.0") (d (list (d (n "peginator") (r "=0.6.0") (d #t) (k 2)) (d (n "peginator_codegen") (r "=0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0fmvl4s7nzaxvh07gvmh26rwkz10mjrqzg7mvm7qxnmib5rffqk1")))

(define-public crate-peginator_macro-0.7.0 (c (n "peginator_macro") (v "0.7.0") (d (list (d (n "peginator") (r "=0.7.0") (d #t) (k 2)) (d (n "peginator_codegen") (r "=0.7.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0kqlv7ld8qldjfr9nl0rqw4nf5j45yfnfjww1c964sxdgixzmz74")))

