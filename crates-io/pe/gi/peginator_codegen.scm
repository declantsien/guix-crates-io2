(define-module (crates-io pe gi peginator_codegen) #:use-module (crates-io))

(define-public crate-peginator_codegen-0.5.0 (c (n "peginator_codegen") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "peginator_runtime") (r "=0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ybd6p05ranwbzxcqisk3b0zi21cm2cxm1q6ngs8kp898hkwm68a")))

(define-public crate-peginator_codegen-0.5.1 (c (n "peginator_codegen") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "peginator_runtime") (r "=0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1b3zaqqw17nsdkam4m989wymjy56fqzhgnb65798vsk64gr2wg0m")))

(define-public crate-peginator_codegen-0.6.0 (c (n "peginator_codegen") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "peginator") (r "=0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0cdxi4wlkbgqzxqqx4lrmx1y5lba0xbc3cpbnb5hlyq7llhb82az")))

(define-public crate-peginator_codegen-0.7.0 (c (n "peginator_codegen") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "peginator") (r "=0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0lwxcx1307g18fza2dj733r91laa7dac4rc1id6lsaws2n515hi7")))

