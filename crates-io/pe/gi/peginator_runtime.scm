(define-module (crates-io pe gi peginator_runtime) #:use-module (crates-io))

(define-public crate-peginator_runtime-0.5.0 (c (n "peginator_runtime") (v "0.5.0") (d (list (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "0jhwq4qq3i3gryxfbn34vdvy0dvmz18b6pk0jmn3gh5rcqq7qv3g") (f (quote (("default" "colored")))) (y #t)))

(define-public crate-peginator_runtime-0.5.1 (c (n "peginator_runtime") (v "0.5.1") (d (list (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1yllh89d3zll0jhi12h2p0yyig9ihwi00gvsl8al2nvhzfx1ly20") (f (quote (("default" "colored")))) (y #t)))

