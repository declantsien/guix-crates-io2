(define-module (crates-io pe gi peginator-cli) #:use-module (crates-io))

(define-public crate-peginator-cli-0.6.0 (c (n "peginator-cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "peginator") (r "=0.6.0") (d #t) (k 0)) (d (n "peginator_codegen") (r "=0.6.0") (d #t) (k 0)))) (h "0915yx2pi3lca06fdc9zx54sriqpwr0q5xq29xa7gpdb18spfixd")))

(define-public crate-peginator-cli-0.7.0 (c (n "peginator-cli") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "peginator") (r "=0.7.0") (d #t) (k 0)) (d (n "peginator_codegen") (r "=0.7.0") (d #t) (k 0)) (d (n "railroad") (r "^0.2.0") (d #t) (k 0)))) (h "127m91sd6ns9pdgcy2rwrnv4jhlmhck5vg0njfzcfdsrvy92kyrc")))

