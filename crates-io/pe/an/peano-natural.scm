(define-module (crates-io pe an peano-natural) #:use-module (crates-io))

(define-public crate-peano-natural-0.1.0 (c (n "peano-natural") (v "0.1.0") (h "0n7fd77vhb6kf7mmc98c5y8km9c8v6zw8fc9y855n8klkr7x4ddm")))

(define-public crate-peano-natural-0.2.0 (c (n "peano-natural") (v "0.2.0") (h "1rqbhv9bxs708wf5d0vq0d4c610pcvw5d9di8izr77id8khkhhyn")))

(define-public crate-peano-natural-0.2.1 (c (n "peano-natural") (v "0.2.1") (h "044v0higaw4khn9160ch3cmbbymzc2y3hn3jmvhii7k53h5x73if")))

