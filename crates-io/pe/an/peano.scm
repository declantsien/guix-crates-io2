(define-module (crates-io pe an peano) #:use-module (crates-io))

(define-public crate-peano-1.0.0 (c (n "peano") (v "1.0.0") (h "0rdy1aqkkbrnac1gf0sl2r3s1nk5xdk7y6d1n1i43ba3jbk3sbqy")))

(define-public crate-peano-1.0.1 (c (n "peano") (v "1.0.1") (h "06idswphqn4qwbgxaixaj0cajlp0vc43yyn0bmjv8w0ig3lifbfp")))

(define-public crate-peano-1.0.2 (c (n "peano") (v "1.0.2") (h "0h2jl9kddw1d2kr4lpmpr8fcniy2rlwp4mhsngjya97avj2c13ba")))

