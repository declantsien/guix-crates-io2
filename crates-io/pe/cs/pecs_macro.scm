(define-module (crates-io pe cs pecs_macro) #:use-module (crates-io))

(define-public crate-pecs_macro-0.1.0 (c (n "pecs_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1lya9f9x28g0c5czhmg8s6v81snbn9hv6hf07zk079f1n1j3hwzn")))

(define-public crate-pecs_macro-0.2.0 (c (n "pecs_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1lygf1fwyhcdpd7kapa4llyzv6xkhg0khsbpj2vwz83bjnagwaw2")))

(define-public crate-pecs_macro-0.2.1 (c (n "pecs_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08n3gb6a429b8m3fhs9g538jssqnfh308m0xq0ik29c4viff8291")))

(define-public crate-pecs_macro-0.2.3 (c (n "pecs_macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14v57kpnfmcyqb60l74sq13hc1crv2adcbz42dziv6cl34yr5w4r")))

(define-public crate-pecs_macro-0.3.0 (c (n "pecs_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0j64bhlh0gfxyc5qsdiz48885nzfn4ip0m6nn09kwkr0ivv3kyfd")))

(define-public crate-pecs_macro-0.4.0 (c (n "pecs_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ihfslzx2j6bp62wflwdcv2y3qm0bfcirjhsskdi7y0sql717xlc")))

