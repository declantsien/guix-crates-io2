(define-module (crates-io pe cs pecs_core) #:use-module (crates-io))

(define-public crate-pecs_core-0.1.0 (c (n "pecs_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0cky6s7ph6a1443s3c8jrfzxky29g9sb48dv6ds8brarcfn7q8gm")))

(define-public crate-pecs_core-0.2.0 (c (n "pecs_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.0") (d #t) (k 0)))) (h "08v6p0l7pgd30d5bmv2cgshxclbwr38a8iqm1mbz0r7l0bz7x27n")))

(define-public crate-pecs_core-0.2.1 (c (n "pecs_core") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1d7ziwsm7yj06zz1agz9bvx80j5z0a4xn35zr5lmrvnzg6jfly05")))

(define-public crate-pecs_core-0.2.3 (c (n "pecs_core") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.3") (d #t) (k 0)))) (h "01f7vkfy3pzlpripnidvxrwk4nf90ql18499qky9drkfz7y8444c")))

(define-public crate-pecs_core-0.3.0 (c (n "pecs_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.3.0") (d #t) (k 0)))) (h "11nsy918mk5xgzc46ixn1q1y2v4i9nggg9r6w33jnpf36vxg6m5x")))

(define-public crate-pecs_core-0.4.0 (c (n "pecs_core") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "0513higpa29gndiyri8zn8pv4ji7y88mlayv4r3pz2jmdcgs66gj")))

(define-public crate-pecs_core-0.5.0 (c (n "pecs_core") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "1gyqkkvilbnr6dcarymipwhqyi0cx8gcvhjs9rgk5w8f449rlzji")))

(define-public crate-pecs_core-0.6.0 (c (n "pecs_core") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "1wx3pkppmz1a007s9zdvjs7achp94xblgfcas91bz1fqkb2mbj9y")))

