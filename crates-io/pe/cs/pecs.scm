(define-module (crates-io pe cs pecs) #:use-module (crates-io))

(define-public crate-pecs-0.1.0 (c (n "pecs") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1w9gr68x5b0gqasickhv55nf794ljhlgmh2wkk0fgf55gc085fmq") (y #t)))

(define-public crate-pecs-0.1.1 (c (n "pecs") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.1.0") (d #t) (k 0)))) (h "17znjrhg6b70j5bsmh5ggql10f3lf51pb2kjf7643y2lwcxzd4wp") (y #t)))

(define-public crate-pecs-0.1.2 (c (n "pecs") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.1.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.1.0") (d #t) (k 0)))) (h "035xagd0kv7d7g6cnkmlsb9wwc75xbb6ccv78wc73jn37d4a6c8p") (y #t)))

(define-public crate-pecs-0.2.0 (c (n "pecs") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.2.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0hznljady7wc0xamy77nkjb36sb9sfp936l7sn7m2xcps0swmy0v")))

(define-public crate-pecs-0.2.1 (c (n "pecs") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.1") (d #t) (k 0)) (d (n "pecs_http") (r "^0.2.1") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0v9c3655ynq62i48rp0n82vhpyp8g25sy0nmkc2wn0zq7675id08")))

(define-public crate-pecs-0.2.3 (c (n "pecs") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.3") (d #t) (k 0)) (d (n "pecs_http") (r "^0.2.3") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.2.3") (d #t) (k 0)))) (h "1s7bhqx7n4gcq057vyhqkisrpq5pds391wqc324vrd6w6ag2fmk6")))

(define-public crate-pecs-0.3.0 (c (n "pecs") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "pecs_core") (r "^0.3.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.3.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.3.0") (d #t) (k 0)))) (h "11bahjkcxmmymvp9wnf31m31vvrff4h5hhpy5ilk6fv9f3r7ypv8")))

(define-public crate-pecs-0.4.0 (c (n "pecs") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "pecs_core") (r "^0.4.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.4.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "0zr4l2xz7k024aikc9q50wl00iyvn8yvnpfaksbifc9x5l1fcrcf")))

(define-public crate-pecs-0.5.0 (c (n "pecs") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.5.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.5.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "1080lpjyh1dvvhjd41v8qg96k69885w8mg89x1f79vhnyq5swd28")))

(define-public crate-pecs-0.6.0 (c (n "pecs") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "pecs_core") (r "^0.6.0") (d #t) (k 0)) (d (n "pecs_http") (r "^0.6.0") (d #t) (k 0)) (d (n "pecs_macro") (r "^0.4.0") (d #t) (k 0)))) (h "17sk6cl8sy5ra5bnhp8ypn0q6lbpg78ampsswgifkx2280vdhwff")))

