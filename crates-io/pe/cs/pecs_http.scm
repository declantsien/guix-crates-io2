(define-module (crates-io pe cs pecs_http) #:use-module (crates-io))

(define-public crate-pecs_http-0.1.0 (c (n "pecs_http") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.1.0") (d #t) (k 0)))) (h "0bmd5l2a2zfxhzafhnn46wv6asfh9lmjv6n9ky42929y1z2g06sj")))

(define-public crate-pecs_http-0.2.0 (c (n "pecs_http") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.0") (d #t) (k 0)))) (h "1xs7pf40ljndkm7rdap3cchwsyhqxpv5993al81q17b61dxrpvw2")))

(define-public crate-pecs_http-0.2.1 (c (n "pecs_http") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.1") (d #t) (k 0)))) (h "0a617xaxg3ksgz5bb548s8flwnr0wiwr1zj12nmjrmwd61w2lmqa")))

(define-public crate-pecs_http-0.2.3 (c (n "pecs_http") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.2.3") (d #t) (k 0)))) (h "1bqlr5p9v7ck235l6hixn9ipbx0f6s0hai6jbxn7j76daqmq7nx0")))

(define-public crate-pecs_http-0.3.0 (c (n "pecs_http") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.3.0") (d #t) (k 0)))) (h "148fjg1vz7zis81n3xfp0bzdkamdy57z5rrfrp44x2daaripmalc")))

(define-public crate-pecs_http-0.4.0 (c (n "pecs_http") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.4.0") (d #t) (k 0)))) (h "11717dz7nl5f7wy5afa3mqappxf9ks28kzdpyp4sd9iaxx10gwvv")))

(define-public crate-pecs_http-0.5.0 (c (n "pecs_http") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.5.0") (d #t) (k 0)))) (h "17zzl68fi6n9p5l1mr22l2gzds5an02riasrrjnr6nn3avjkp659")))

(define-public crate-pecs_http-0.6.0 (c (n "pecs_http") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "ehttp") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pecs_core") (r "^0.6.0") (d #t) (k 0)))) (h "1rg0zyyxvzsjjrndnqp55ki9faivvcasdmz7jcqcyp6sjzfjl84k")))

