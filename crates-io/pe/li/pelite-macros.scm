(define-module (crates-io pe li pelite-macros) #:use-module (crates-io))

(define-public crate-pelite-macros-0.1.0 (c (n "pelite-macros") (v "0.1.0") (h "183yi8c1zv0jacsr9p4j2r31q9cdxr6xdgr11x27934midj6liw0")))

(define-public crate-pelite-macros-0.1.1 (c (n "pelite-macros") (v "0.1.1") (h "0nw102zk3p39nxf76mlxix4ipj0d1azai4j8by4z9c7bxkwg6z3s")))

