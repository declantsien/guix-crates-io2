(define-module (crates-io pe kz pekzep_syllable) #:use-module (crates-io))

(define-public crate-pekzep_syllable-0.1.0 (c (n "pekzep_syllable") (v "0.1.0") (h "1845gyj1h07wddpq8yc73h59p3mxjxm513cl5wh9x0zsy9hr1fik")))

(define-public crate-pekzep_syllable-0.1.1 (c (n "pekzep_syllable") (v "0.1.1") (h "1q4bmq7mqxq4jq22hsmjnprp7d68qm82rppmlv9wjfss28czv650")))

(define-public crate-pekzep_syllable-0.1.2 (c (n "pekzep_syllable") (v "0.1.2") (h "09f6ypl7pxgr8vrbc1kxxwjby5p7xasjvpqixfxwfkssgcmfprnc")))

