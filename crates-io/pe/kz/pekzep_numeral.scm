(define-module (crates-io pe kz pekzep_numeral) #:use-module (crates-io))

(define-public crate-pekzep_numeral-0.1.0 (c (n "pekzep_numeral") (v "0.1.0") (h "0fbjws5nqhhvhf7amkifxjnia8j3q8cb63pk4zsb1b5s0fbs9i85")))

(define-public crate-pekzep_numeral-0.2.0 (c (n "pekzep_numeral") (v "0.2.0") (h "0703681aw98nm79pa7h9bchrw2p34kd47glhh2y68f3n4fjxrda3")))

