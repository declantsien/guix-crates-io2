(define-module (crates-io pe rs person_registration) #:use-module (crates-io))

(define-public crate-person_registration-0.1.0 (c (n "person_registration") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nxb5p15iablgb3sicr3zifs598lad8s9l836fyq729gaxfwhc6q")))

(define-public crate-person_registration-0.1.1 (c (n "person_registration") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wqzqkr4sr8n0fvqvx7rjklcamfhd9j35w0znz1p6n2ki9yy6w2c")))

(define-public crate-person_registration-0.1.2 (c (n "person_registration") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05yx8aqh7cavl1nlm7prvsacayvwwrc7a9hbs2wrj9ag5cw83aqb")))

