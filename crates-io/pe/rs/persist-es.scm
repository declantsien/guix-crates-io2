(define-module (crates-io pe rs persist-es) #:use-module (crates-io))

(define-public crate-persist-es-0.2.1 (c (n "persist-es") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "cqrs-es") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1dx8rxvpfazzfdy5z3awgd7cmln2rq5k02sg2vvw9iz5rxz92ndx")))

(define-public crate-persist-es-0.2.2 (c (n "persist-es") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "cqrs-es") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0lwjyrjb9v879fn2iq1ab1qxb28iv6pb9zqzim8vrlsrfxwpw8fy")))

(define-public crate-persist-es-0.2.3 (c (n "persist-es") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "cqrs-es") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "150k364c6pnsh0x0p7hi6xjf2wn8pcc3qpaz4mfg5ml7gyfh8gcc")))

(define-public crate-persist-es-0.2.4 (c (n "persist-es") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "cqrs-es") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "03lmla5nldplcn6aam6jyjqif4p5nr0ppk9wyndlcip509rdp998")))

(define-public crate-persist-es-0.2.5 (c (n "persist-es") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "cqrs-es") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0rxg2zxjl6xz2hkm535y3pqmcbcrb0ils9178kyqakrq20chz0xh")))

(define-public crate-persist-es-0.2.6 (c (n "persist-es") (v "0.2.6") (h "12glvxqgjqrg4c5ymqki3z3k8jnih321dm12cw9315nal505zljv")))

