(define-module (crates-io pe rs personify) #:use-module (crates-io))

(define-public crate-personify-0.1.0 (c (n "personify") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08cp04pbhq3ih8kvq16vpq12pmcvh80yqmzh5334i3yxmfv27pwz") (f (quote (("unstable"))))))

(define-public crate-personify-0.1.1 (c (n "personify") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "159zs83f2bifyh98n0i6gkfxk3jbp6bdii975sf1vsx2fzz50920") (f (quote (("unstable"))))))

(define-public crate-personify-0.1.2 (c (n "personify") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19w087lny31sap7bqr1j5bf0sv7f3x3fa33ahymvymin7l48077s") (f (quote (("unstable"))))))

