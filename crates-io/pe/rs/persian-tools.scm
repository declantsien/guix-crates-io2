(define-module (crates-io pe rs persian-tools) #:use-module (crates-io))

(define-public crate-persian-tools-0.1.0 (c (n "persian-tools") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "ptime") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "time") (r "^0.2.25") (d #t) (k 0)))) (h "02m1v98ssmdrm8vk55bdkj1pb4wwi247qrh831r4dwfw325bg2ij") (y #t)))

