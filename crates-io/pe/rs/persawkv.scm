(define-module (crates-io pe rs persawkv) #:use-module (crates-io))

(define-public crate-persawkv-0.0.0-alpha.0 (c (n "persawkv") (v "0.0.0-alpha.0") (d (list (d (n "persy") (r "^0.7") (d #t) (k 0)))) (h "1war0hxczacs57d0npwpldr0dchkg95v9zw1y3q19z5ynbjmhpyk") (y #t)))

(define-public crate-persawkv-0.0.0-alpha.1 (c (n "persawkv") (v "0.0.0-alpha.1") (d (list (d (n "delegate") (r "^0.3") (d #t) (k 0)) (d (n "persy") (r "^0.7") (d #t) (k 0)))) (h "04piil1dbndl22nnhaxswxrpb00jkjyfhxs0i8kkpcdq4vhbhp0r") (y #t)))

