(define-module (crates-io pe rs persia-speedy-derive) #:use-module (crates-io))

(define-public crate-persia-speedy-derive-0.7.2 (c (n "persia-speedy-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d9rxrl3pywv7x0c0232yf175wzs06w3b8l3bkz3fcs4x4akgjcz")))

(define-public crate-persia-speedy-derive-0.7.3 (c (n "persia-speedy-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k6ax6b4ci6hxa76fgybrylr7gp29z23ksr1sy55nghv9rdyn7by")))

