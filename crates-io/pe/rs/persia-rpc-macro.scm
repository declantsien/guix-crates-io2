(define-module (crates-io pe rs persia-rpc-macro) #:use-module (crates-io))

(define-public crate-persia-rpc-macro-0.1.0 (c (n "persia-rpc-macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18sf4zdd0g1g1n4rikd0wcndq9qgfrfnx6xaf614c60kzdcnvmsp")))

(define-public crate-persia-rpc-macro-0.1.1 (c (n "persia-rpc-macro") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nxin7ry1vxqx3cyf6ls95icyxzc0kfnmjjiaqfkwcp06nv0b13x")))

(define-public crate-persia-rpc-macro-0.1.2 (c (n "persia-rpc-macro") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rlj15cm4s3iwrylfgh7d2cz02rzic8cmbw7423qpbzs632xx1zc")))

(define-public crate-persia-rpc-macro-0.1.3 (c (n "persia-rpc-macro") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0by966wxvmvxg18knx8n2g4h6y4rcqwbyiygxdqnbkargv6c53bf")))

(define-public crate-persia-rpc-macro-0.1.4 (c (n "persia-rpc-macro") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x80cvc0dwbq7vs4955pbs47hs25w254gv9hi6nvdjcgr677xr6r")))

(define-public crate-persia-rpc-macro-0.1.6 (c (n "persia-rpc-macro") (v "0.1.6") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18b5qfr0c3m5fz3kmw8120i0bpgk1hcqf5xds1gnd9masr3v5bi8")))

(define-public crate-persia-rpc-macro-0.1.8 (c (n "persia-rpc-macro") (v "0.1.8") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00hb8h9wwibay1mq58qs6a208n111gnw329qq11zbrma20zh8vsq")))

(define-public crate-persia-rpc-macro-0.1.10 (c (n "persia-rpc-macro") (v "0.1.10") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14xps1rqyfqknbz1k5647zi8vswldcgzwm7mcln9kr1v0h8gawrj")))

(define-public crate-persia-rpc-macro-0.1.11 (c (n "persia-rpc-macro") (v "0.1.11") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19m3ck8ag1l3jfi98h8pvy2jfv0jdr6sgjfv5sj0647mcb54gyb2") (y #t)))

(define-public crate-persia-rpc-macro-0.1.12 (c (n "persia-rpc-macro") (v "0.1.12") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04jaxmggz7xyyaclzri5y1bjylv3dnnhrz8azi9xf24h3di96fzn")))

(define-public crate-persia-rpc-macro-0.2.0 (c (n "persia-rpc-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bgzrix1cargfnfhchw4j0qapjil89m50ljrxjnrg1br0gd2s7wc")))

(define-public crate-persia-rpc-macro-0.3.0 (c (n "persia-rpc-macro") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03phn13j8fr02ba25zzajpqjh2nqglqh9mplwc26w2i6i8q6hwxg")))

(define-public crate-persia-rpc-macro-0.4.0 (c (n "persia-rpc-macro") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sfb11q1sq1f0cgq9jbc0azj3cqcxn1hf60l6hm1r1cbs56kchqi")))

(define-public crate-persia-rpc-macro-0.5.0 (c (n "persia-rpc-macro") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jf9scxy1nwzh3wfncn0qj7hq7w4bcyfdl97pdiq1sfp8vgf9ms2")))

(define-public crate-persia-rpc-macro-0.3.1 (c (n "persia-rpc-macro") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1czrzbhqh439jx07yi70jhqz3vqz0p3shxzpskm9rgnsp97dgjjj")))

(define-public crate-persia-rpc-macro-0.4.1 (c (n "persia-rpc-macro") (v "0.4.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y2dzx832f3wfkarhkspzz4kg5jpimvf0rqv8aamcycfi0wdjwwc")))

(define-public crate-persia-rpc-macro-0.6.0 (c (n "persia-rpc-macro") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09kplwlva2cmbcgrgmyk4qcwai0imfk22i9rp79gza7g5qcvxjl3")))

