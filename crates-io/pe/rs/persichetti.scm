(define-module (crates-io pe rs persichetti) #:use-module (crates-io))

(define-public crate-persichetti-0.1.0 (c (n "persichetti") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g6xfzfb0w38b7bsmmbyci8n1qwfdc2q02jmmnbfqmnvqj3y374l")))

(define-public crate-persichetti-0.1.1 (c (n "persichetti") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hajqwasycz6hcdfq89f4qkiqvfdln2fx9vjxfjy1ggd2dw62w2q")))

(define-public crate-persichetti-0.1.2 (c (n "persichetti") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z7pyy00nlxnxfa24mkqs1h4z2cvijdym6mwcfch8b6bwycc1nxr")))

(define-public crate-persichetti-0.2.0 (c (n "persichetti") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a46b4airalnv1k3b8sp4mnqipnsfsi1dsrmnz70gs9m52qkzy8a")))

