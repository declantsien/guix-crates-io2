(define-module (crates-io pe rs persist_if_changed) #:use-module (crates-io))

(define-public crate-persist_if_changed-0.1.9 (c (n "persist_if_changed") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xbpaa91c4rscw940rwn2ynxx945x8fjch0n8fzpqkpg75ybrik7")))

(define-public crate-persist_if_changed-0.1.10 (c (n "persist_if_changed") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01rp2hxsinwm9qd9bkdib9sdmm6wyjxq4s85xp6h2wvp1i16ac9y")))

(define-public crate-persist_if_changed-0.1.11 (c (n "persist_if_changed") (v "0.1.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04y9xqmkn5701z6ali7h4g2457a4fc50w6f09qpfhh4pf9rnnfaz")))

(define-public crate-persist_if_changed-0.1.12 (c (n "persist_if_changed") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ljz5aisvwrwfv4fqk8vvpyalkvbqkh312qqxr62bf0lm2wp7a86")))

(define-public crate-persist_if_changed-0.1.13 (c (n "persist_if_changed") (v "0.1.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1arfhxfdxbminxjr8nxsf0znfjdd985fq0z6g78z6gnbs5irw4rq")))

(define-public crate-persist_if_changed-0.1.14 (c (n "persist_if_changed") (v "0.1.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0jw4wd0q46gq04y3cklpwcb0x60vvi27c6b8blcxb2ih08bzka3a")))

(define-public crate-persist_if_changed-0.1.15 (c (n "persist_if_changed") (v "0.1.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0s52aybih0p66sxy0n6w1j9pxrmbklhc8aiyz3ckwg4iaj0kq0rl")))

(define-public crate-persist_if_changed-0.1.16 (c (n "persist_if_changed") (v "0.1.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07r9kn0bam6kw49a1d9s9zrvlai9gjlbbw6jpsdw3z41pa7r4g8c")))

(define-public crate-persist_if_changed-0.1.17 (c (n "persist_if_changed") (v "0.1.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1l43qn3dr0mmc79vjqr11ql8if638x3l9qacfmm18lb767x2hy7l")))

(define-public crate-persist_if_changed-0.1.18 (c (n "persist_if_changed") (v "0.1.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gd3m1k7q3n918aq1rglxgda8m4zg379gznprsikdby1znyr0wcc")))

(define-public crate-persist_if_changed-0.1.19 (c (n "persist_if_changed") (v "0.1.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v83r9ksqpdhvczqy26m7gn31dzzn5qbl9hjhlzkwhqsp34k9vzl")))

(define-public crate-persist_if_changed-0.1.20 (c (n "persist_if_changed") (v "0.1.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1cylwqkwmngwk2j06zfgidqz53nzz34rf7fdp4x9q9wym33570n6")))

(define-public crate-persist_if_changed-0.1.21 (c (n "persist_if_changed") (v "0.1.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01g47ddprri4y8gvadl53z610iwyy2ja9dsf279qcrjmlwb4jdka")))

(define-public crate-persist_if_changed-0.1.22 (c (n "persist_if_changed") (v "0.1.22") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0phf93a8k37an2d8d8sy2lzk3fn83xi6db7156xk3qynjir54713")))

(define-public crate-persist_if_changed-0.1.23 (c (n "persist_if_changed") (v "0.1.23") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ym87sbxn2jv0w9810jnwqf3hvaznbb50nfyv2616hfwrkq02bv3")))

(define-public crate-persist_if_changed-0.1.24 (c (n "persist_if_changed") (v "0.1.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08mmir7jmawhfgb8mclzjjw5mpribnrlxnrslialz8anbrvi8mkg")))

(define-public crate-persist_if_changed-0.1.25 (c (n "persist_if_changed") (v "0.1.25") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bd2rqyrwrg5fgcxh0n0k4i2l1j8yygmhvn4lzpcgk2j4s3x7m1j")))

(define-public crate-persist_if_changed-0.1.26 (c (n "persist_if_changed") (v "0.1.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0498gqc9z50riy67fnxldzqphl0pf11vpv9792ps1bk2z6x8sclz")))

(define-public crate-persist_if_changed-0.1.27 (c (n "persist_if_changed") (v "0.1.27") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "16lssg8pv5k3x37m73mns1r8n3r3rycvim1yccq1kpqasaypkmka")))

(define-public crate-persist_if_changed-0.1.28 (c (n "persist_if_changed") (v "0.1.28") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10inq3agkxrfx7llvx4na0shviiqm7wkskh6fyl1bw46az0cjhgf")))

(define-public crate-persist_if_changed-0.1.29 (c (n "persist_if_changed") (v "0.1.29") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18w27v4am4196qiv534j85blf1723glar7wx8pn74gliv980vb28")))

(define-public crate-persist_if_changed-0.1.30 (c (n "persist_if_changed") (v "0.1.30") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ylf996xjyw8k9vvqii9nrd95xppz2i948sagcqsvxklvar3vcq3")))

(define-public crate-persist_if_changed-0.1.31 (c (n "persist_if_changed") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qbgjk6m2h9p35xrc7pcxzdlkvm08lwlj6p3ckz26qaf3s3qcj8l")))

(define-public crate-persist_if_changed-0.1.32 (c (n "persist_if_changed") (v "0.1.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fi5b6k77467iiv24v7l56vxbk4zkxyzmikrwg5mxji5apxbvcx1")))

(define-public crate-persist_if_changed-0.1.33 (c (n "persist_if_changed") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s5z4yh74dwchi18lak5484c5afcmjmhrc6xf609z1jcqd9rhxhl")))

(define-public crate-persist_if_changed-0.1.34 (c (n "persist_if_changed") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cyhfxpwygwhbkqqc91llrmrf3z8y9ijhxmhh5xlm5wiib23ra3p")))

(define-public crate-persist_if_changed-0.1.35 (c (n "persist_if_changed") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "109h2qyfbnzk4hzshmbcjf748j7p8lhnw7rpnvk89mjr4f0rbdk7")))

(define-public crate-persist_if_changed-0.1.36 (c (n "persist_if_changed") (v "0.1.36") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "150q6z9j014anqiqli0c4xs4j15svmvrr1ms6y24ahd0bd1yxjzq")))

(define-public crate-persist_if_changed-0.1.37 (c (n "persist_if_changed") (v "0.1.37") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0yj6nyv1ibg85dygg356ski74nivaphrj7vf98kcaajizpw7a8qp")))

(define-public crate-persist_if_changed-0.1.38 (c (n "persist_if_changed") (v "0.1.38") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1hvawiynbd5mw3k6354n8sgf9saigwd96k7yb8hsf5dg11hkj9pd")))

