(define-module (crates-io pe rs perseus-rocket) #:use-module (crates-io))

(define-public crate-perseus-rocket-0.4.0-rc.1 (c (n "perseus-rocket") (v "0.4.0-rc.1") (d (list (d (n "perseus") (r "^0.4.0-rc.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "04ixwfhdmzf86z3m5j90m5vxgh7cy7c9a0yzvbl1bh1s3m8i7ycq") (f (quote (("dflt-server"))))))

(define-public crate-perseus-rocket-0.4.0 (c (n "perseus-rocket") (v "0.4.0") (d (list (d (n "perseus") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "06b4wyx72yj5ijn6z0fnf267npy62qb5jchl9ng6a3shp1ly8lmj") (f (quote (("dflt-server"))))))

(define-public crate-perseus-rocket-0.4.1 (c (n "perseus-rocket") (v "0.4.1") (d (list (d (n "perseus") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1mkglafwkdcgg2sb35w8q48qw0s5rx5pa12f33zac8w7p312wn3v") (f (quote (("dflt-server"))))))

(define-public crate-perseus-rocket-0.4.2 (c (n "perseus-rocket") (v "0.4.2") (d (list (d (n "perseus") (r "^0.4.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1p8y7r5k1v3ncm0k5jzj8xv5s754xvg7zg5mjyg63zs17g9xr3nr") (f (quote (("dflt-server"))))))

