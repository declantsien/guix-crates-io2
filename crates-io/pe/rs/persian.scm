(define-module (crates-io pe rs persian) #:use-module (crates-io))

(define-public crate-persian-0.1.0 (c (n "persian") (v "0.1.0") (h "0wvns83zd81hk6h9zqgh0mjcflfdfjlv7i15mv5904vylz6zsiwb")))

(define-public crate-persian-0.1.1 (c (n "persian") (v "0.1.1") (h "1hq20q88y3irs0ayrvx4l1wmhsifll7lk4g9lamdbvr54177j69c")))

