(define-module (crates-io pe rs persian-rug_derive) #:use-module (crates-io))

(define-public crate-persian-rug_derive-0.1.0 (c (n "persian-rug_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rh00da5gi7hqwcvcl8mg684xb84mkwzda3i8cqj21m1nm1pvadh")))

(define-public crate-persian-rug_derive-0.1.1 (c (n "persian-rug_derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00rxrn8vak19fjl0kxm7wwiih1ymhmyjqiq9slnhwv3wsd1cj5vm")))

(define-public crate-persian-rug_derive-0.1.2 (c (n "persian-rug_derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1spr2mjhw51da73m4fhql57jxmlzw6z6xrfz27nj22iji48c7gqz")))

