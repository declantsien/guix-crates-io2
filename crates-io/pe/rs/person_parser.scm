(define-module (crates-io pe rs person_parser) #:use-module (crates-io))

(define-public crate-Person_Parser-0.2.0 (c (n "Person_Parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1nnrszz7fmhsik0cg9nd0lj9sir96q2gf962m2nmd1fjqbaq4g2z")))

(define-public crate-Person_Parser-0.3.0 (c (n "Person_Parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1mqwwvdlar77yc4v96qgkn4b1z2q7aaq90bgc7gv1c2fiw2ajli6")))

(define-public crate-Person_Parser-0.5.0 (c (n "Person_Parser") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1ksq342ccdxmim9mh39lfxa5fg43sa3rlyam9fxqkzppxlbn1aib")))

(define-public crate-Person_Parser-0.6.0 (c (n "Person_Parser") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0nibwn3id3d1mh7r8a19sfxhrhpfnqzdfhcqx073xq7jxrwihgfn")))

(define-public crate-Person_Parser-0.8.0 (c (n "Person_Parser") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1riz90msqj767243nzbdmlnf6pzaxh7vcb1zvazgq7lc2sc13fzv")))

