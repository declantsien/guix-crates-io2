(define-module (crates-io pe rs persistent-time) #:use-module (crates-io))

(define-public crate-persistent-time-0.1.0 (c (n "persistent-time") (v "0.1.0") (d (list (d (n "bincode") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "postgres") (r "^0.11") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0di1sj73bd3a82kdj2ifarbiii2fvlry0sv155xmmw4rgswanikz")))

