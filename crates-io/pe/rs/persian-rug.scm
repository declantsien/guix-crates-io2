(define-module (crates-io pe rs persian-rug) #:use-module (crates-io))

(define-public crate-persian-rug-0.1.0 (c (n "persian-rug") (v "0.1.0") (d (list (d (n "clone-replace") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "persian-rug_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0vk1aqifywadqwjdw4kbrmhn6qdsxws60d7xil6l69pclhr1zi4w") (f (quote (("default")))) (s 2) (e (quote (("clone-replace" "dep:clone-replace"))))))

(define-public crate-persian-rug-0.1.1 (c (n "persian-rug") (v "0.1.1") (d (list (d (n "clone-replace") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "persian-rug_derive") (r "^0.1.1") (d #t) (k 0)))) (h "17hl31zsninjcyw76rqaxl2a3cdv19cp3i1cfjl4c3xl00v0vwqy") (f (quote (("default")))) (s 2) (e (quote (("clone-replace" "dep:clone-replace"))))))

(define-public crate-persian-rug-0.1.2 (c (n "persian-rug") (v "0.1.2") (d (list (d (n "clone-replace") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "persian-rug_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0gzy88kdm3lmqzm5x5an9h97jgab7x42jcg6z87b403m0szp67z6") (f (quote (("default")))) (s 2) (e (quote (("clone-replace" "dep:clone-replace"))))))

