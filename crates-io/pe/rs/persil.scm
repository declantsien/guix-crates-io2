(define-module (crates-io pe rs persil) #:use-module (crates-io))

(define-public crate-persil-0.1.0 (c (n "persil") (v "0.1.0") (d (list (d (n "measureme") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "16j1cx1py8ph8m3n8jwvcyhh1846xp0682qm5c92zjg4mgd1bi5l") (f (quote (("profiler" "measureme" "once_cell")))) (y #t)))

(define-public crate-persil-0.1.1 (c (n "persil") (v "0.1.1") (d (list (d (n "measureme") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "19rsi0azswma8i8108d0rq9wmjpdggv1nssx7six7djn7m0sbp77") (f (quote (("profiler" "measureme" "once_cell")))) (y #t)))

(define-public crate-persil-0.1.2 (c (n "persil") (v "0.1.2") (d (list (d (n "measureme") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0vzkb2gbb2gvv66fa3s6hagxiqck1dj91qq6pymacgss3bhi3gp9") (y #t)))

(define-public crate-persil-0.1.3 (c (n "persil") (v "0.1.3") (d (list (d (n "measureme") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1kmx4zrsa35ck1zmm2f0wlydmsvkaiyaj0hvf4c699s4zpi77cbj")))

