(define-module (crates-io pe rs person_json) #:use-module (crates-io))

(define-public crate-person_json-0.1.0 (c (n "person_json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "00dkmny9wianv3lylfi6ra5jvslgxzs2pi0m6vcgb2d0xsvxslhq")))

