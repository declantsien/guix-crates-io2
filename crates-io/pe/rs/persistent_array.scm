(define-module (crates-io pe rs persistent_array) #:use-module (crates-io))

(define-public crate-persistent_array-0.1.0 (c (n "persistent_array") (v "0.1.0") (d (list (d (n "memmap") (r "^0.1.0") (d #t) (k 0)))) (h "11cs2lj63vhgq5d2f2vzxs6qbk2b113i1s4i9ggdqf648dgpn5fk")))

(define-public crate-persistent_array-0.1.1 (c (n "persistent_array") (v "0.1.1") (d (list (d (n "memmap") (r "*") (d #t) (k 0)))) (h "0h0hpqnhwz42rl56lsx63prxmhnzy30affkg9l0bpafxd1glb1b8")))

(define-public crate-persistent_array-0.2.0 (c (n "persistent_array") (v "0.2.0") (d (list (d (n "memmap") (r "*") (d #t) (k 0)))) (h "0mh44g2s18qmsc00lwy8zh71kzqx3347mn055lg1ayvfm7j6g2zm")))

(define-public crate-persistent_array-0.2.1 (c (n "persistent_array") (v "0.2.1") (d (list (d (n "memmap") (r "*") (d #t) (k 0)))) (h "1ggfq3jfnm3s4dnk880k7blmzyrfv9v5mmnfb6a0qkjlvqi2ywck")))

(define-public crate-persistent_array-0.2.2 (c (n "persistent_array") (v "0.2.2") (d (list (d (n "layout_id") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)))) (h "1cagrfff3x8vgbl46jzdxmasqp20ad2nbpcs5mpfzrjapy8x69f2")))

(define-public crate-persistent_array-0.3.0 (c (n "persistent_array") (v "0.3.0") (d (list (d (n "layout_id") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)))) (h "0fg08mckpmj7dp1mynn2asii0nj6iv15gpbanqdl46my4xxcbxb5")))

(define-public crate-persistent_array-0.3.1 (c (n "persistent_array") (v "0.3.1") (d (list (d (n "clippy") (r "*") (d #t) (k 0)) (d (n "layout_id") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0r9yypz5j4qjjqvhi2zycr02cs7jygnahd3dc1lkqbxnlcq6y8mw")))

(define-public crate-persistent_array-0.3.2 (c (n "persistent_array") (v "0.3.2") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "layout_id") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1fnk97cy1wrz9vrhwrv6gpg1f091b6p8hkq27zrcpmpbxnmrbg8h") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-persistent_array-0.3.3 (c (n "persistent_array") (v "0.3.3") (d (list (d (n "layout_id") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1knrfmn7nzag4cvlckbpd4b2cbvw7p8bi5j3xkx7admlprcf3zb5")))

(define-public crate-persistent_array-0.4.0 (c (n "persistent_array") (v "0.4.0") (d (list (d (n "layout_id") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0spyg33y2rkiasb2dlfkgpds39ywxj6c4f1jmwnp4cr19s8jgccb")))

(define-public crate-persistent_array-0.5.0 (c (n "persistent_array") (v "0.5.0") (d (list (d (n "layout_id") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1gdyglwa7p8s6k6n60dmlqpdab195vrvn68fg6x4dix23c9jxl3s")))

