(define-module (crates-io pe rs perstruct-proc-macros) #:use-module (crates-io))

(define-public crate-perstruct-proc-macros-0.1.0 (c (n "perstruct-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ck5gm5q0fbidnf87n38gh93ar1jvkzqmcdpwvpjbr5g3r75kkq9")))

