(define-module (crates-io pe rs person_struct_parser) #:use-module (crates-io))

(define-public crate-person_struct_parser-0.1.0 (c (n "person_struct_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "03parjhm9mh17ksmwd0kyks9z5j2h8hkljw934l0jf3sf6f7zm9r")))

(define-public crate-person_struct_parser-0.2.0 (c (n "person_struct_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1qfbimjw35p9xci90gvigif98vr7a8cmgs312jq5pv5sh5xl907y")))

(define-public crate-person_struct_parser-0.3.0 (c (n "person_struct_parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "12sabf3bpi5wfxccnwv72d5w7yqnhd552dbi6ajmnw4azs6ibm7s")))

(define-public crate-person_struct_parser-0.3.1 (c (n "person_struct_parser") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1nn0as6j3ail6ddq339f3n1x7y68h1vjw524aia9503bkxcvwh9i")))

(define-public crate-person_struct_parser-0.4.0 (c (n "person_struct_parser") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1m974x004c4mwlqq0gx3asjgyx23zg2n7h2rysjazyb2hyxpsvk1")))

(define-public crate-person_struct_parser-0.4.1 (c (n "person_struct_parser") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1nl3lp89l2fwpkxzsjbm9qimxjaihlqs9adgh294wpxfb7dx7x32")))

(define-public crate-person_struct_parser-0.4.2 (c (n "person_struct_parser") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "180zskc2a7lxvhjqmk8523yhn4mm4whmn4qnzfl2plpz03k4n0m6")))

(define-public crate-person_struct_parser-1.0.0 (c (n "person_struct_parser") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qzaa9rsy75v79inwm726vpgq93n9kfxbws8df7j3rdfz7q9z19a")))

(define-public crate-person_struct_parser-1.1.0 (c (n "person_struct_parser") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1901wmdrbvw9jcd8nl32agf9q2j8if1d2d0f41bhkxd58rw2f1d4")))

(define-public crate-person_struct_parser-1.1.1 (c (n "person_struct_parser") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0cr05f95zh6q6z83wgw31khkw8qjr4rlvlw7dm42kfwgnc7r898d")))

(define-public crate-person_struct_parser-1.1.2 (c (n "person_struct_parser") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1dl1x63pkh5i3li0g815y0ysvgdykdwfx86ywydd467ma18a58gk")))

(define-public crate-person_struct_parser-1.2.0 (c (n "person_struct_parser") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0mginxsdm1ixcrz2cx7j3y4n81435rcrnrgx31n9yaa0rvw72079")))

(define-public crate-person_struct_parser-1.2.1 (c (n "person_struct_parser") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "19yfzvrc660zk3igxa1v9kq5af5g21wvp5avdqflhbh5g4isv0a6")))

(define-public crate-person_struct_parser-1.2.2 (c (n "person_struct_parser") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "174jhnc0zw1xvd4x9dddcvdp76jnizlvxzmvsqldpc2b4fv5p85g")))

(define-public crate-person_struct_parser-1.2.3 (c (n "person_struct_parser") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1a4dhcqjynf5jcny52qcrp5i1lc87nnpjq0hns1nfj3j6ycc2hma")))

(define-public crate-person_struct_parser-1.2.4 (c (n "person_struct_parser") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1firs8h5k9k2jzazmfzyllvryf3lqn1psqvb54an896ildc5yp16")))

(define-public crate-person_struct_parser-1.2.5 (c (n "person_struct_parser") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1rw2979s916b0c0sa05qawggyzax2jqsssnxg1xq0d6pr3p9c13n")))

(define-public crate-person_struct_parser-1.2.6 (c (n "person_struct_parser") (v "1.2.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qlgk1ffwcgviim3r177hz1km20dijz24nlbjpkhjq1z1kh6vqcp")))

(define-public crate-person_struct_parser-1.3.0 (c (n "person_struct_parser") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1vvvfli417b6apcnz0gr72z47yr97cmsyb7aw2kv4jvhy6058gsk")))

(define-public crate-person_struct_parser-1.3.1 (c (n "person_struct_parser") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1bhy3yhdcz54cd8d17dzzaghh530pr7j3f7gms82b4qdjd4wv8ab")))

