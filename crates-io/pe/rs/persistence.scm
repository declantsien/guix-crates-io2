(define-module (crates-io pe rs persistence) #:use-module (crates-io))

(define-public crate-persistence-0.0.0 (c (n "persistence") (v "0.0.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "117m01jzkh8j3rcxs5b8cf2d4r7cakrsrqasm9hz90f2wv5k2vkj")))

(define-public crate-persistence-0.0.1 (c (n "persistence") (v "0.0.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0x5j6dldhr667c3lwkx0cdg01k0s90hm8hjvnnfppydll7q05q4y")))

(define-public crate-persistence-0.0.2 (c (n "persistence") (v "0.0.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "13if12zal2244k8mv7l1q7hdrqsv1gjj0x0wiqf1xbkx0pv9jx4a")))

(define-public crate-persistence-0.0.3 (c (n "persistence") (v "0.0.3") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0fbl5llmkp7a30yz1kv8jk3xk2mx5f7154xa0axhhqjn93hv6lzp")))

(define-public crate-persistence-0.0.4 (c (n "persistence") (v "0.0.4") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1kljgp18s98k8v97090x2j1j95inna77f345zy0qlqwy8x3ab2bc")))

(define-public crate-persistence-0.0.5 (c (n "persistence") (v "0.0.5") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "032nsrdqmj8v3py29gijbdvszh778ywwhivpi0w54il1123ics5m")))

(define-public crate-persistence-0.0.6 (c (n "persistence") (v "0.0.6") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "15pk1qa1bdryss7qc59r4ccg34sg3sigcjyjrvn8bjajrdvqwkq6")))

