(define-module (crates-io pe rs perseverance) #:use-module (crates-io))

(define-public crate-perseverance-0.1.0 (c (n "perseverance") (v "0.1.0") (d (list (d (n "json") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "150bi2a5x5w2gn4cp6nnx88h6lisk19d96gxwasl7nf7brnrjsz2") (f (quote (("use_json"))))))

(define-public crate-perseverance-0.1.1 (c (n "perseverance") (v "0.1.1") (d (list (d (n "json") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "164qhkf7j6r3h79vqd4n8907h8w054vr2xncmcwyc3yfalljzdir") (f (quote (("types"))))))

(define-public crate-perseverance-0.1.2 (c (n "perseverance") (v "0.1.2") (d (list (d (n "json") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17jn6ynppf3wmcz0smh1p9yaaqjyb0v5cn3lfy1ngnkcwm8aqhvp") (f (quote (("types" "serde" "json" "serde_json"))))))

(define-public crate-perseverance-0.1.3 (c (n "perseverance") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0d6lrrywc2l3gidszzap688qc5nlgavq62n8qh92246xgyj58zi2") (f (quote (("types" "serde" "json" "serde_json") ("async" "async-trait"))))))

