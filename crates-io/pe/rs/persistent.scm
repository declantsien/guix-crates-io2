(define-module (crates-io pe rs persistent) #:use-module (crates-io))

(define-public crate-persistent-0.0.1 (c (n "persistent") (v "0.0.1") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "049ppm3bd4az71jyj3kyp7iqgkna25czqkiivw062n0lfh9dsdwf")))

(define-public crate-persistent-0.0.2 (c (n "persistent") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "0xmgybc7zv7b2bikkj49wfvqqf486aw8jdfs9gx2zsq8affpq8pc")))

(define-public crate-persistent-0.0.3 (c (n "persistent") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "10vdlrb4kdfj5m559pggvfdi9hxvnxn1zxb3bxjz6pcpjq751pc3")))

(define-public crate-persistent-0.0.4 (c (n "persistent") (v "0.0.4") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "1ww5634qqb2f7ygb8d26fa2ybws8v89zgh250gc24bv5icxm0ckd")))

(define-public crate-persistent-0.0.5 (c (n "persistent") (v "0.0.5") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "0fpp67g4nvp72ny4wp3lwsy6qp9ab9g0n6jiq7nixsp4zqvcrqzj")))

(define-public crate-persistent-0.0.6 (c (n "persistent") (v "0.0.6") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)))) (h "0f186iq843xl8pxyh5v7rdd76paiyjm3nff7jmhlp761s9l7k5h2")))

(define-public crate-persistent-0.0.7 (c (n "persistent") (v "0.0.7") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "0mkjcfns4qjzjv3w71gbypbzqy4rs0mm86k06abxalzyphrb71mr")))

(define-public crate-persistent-0.0.8 (c (n "persistent") (v "0.0.8") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "0z4g1xir2fqga91zj9rhq6jfq15ragwmklbw1jkh3q190vvah5aw")))

(define-public crate-persistent-0.0.9 (c (n "persistent") (v "0.0.9") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "14qwy7p31maj3k4ykv4lgxmbj96r2xnmzmhpyszqvdqycrxx2pz5")))

(define-public crate-persistent-0.1.0 (c (n "persistent") (v "0.1.0") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "0xm8xv5c4bv06hs5y7p1d54258ranwb1s8nj9vqv9mn2y900r0xm")))

(define-public crate-persistent-0.1.1 (c (n "persistent") (v "0.1.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "1v20k3ss36j8dn46w8wx5pyrrdkvwdihpcya3x7vla2c3xp2yzdp") (y #t)))

(define-public crate-persistent-0.2.0 (c (n "persistent") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "1nd42hc4z1siwkfnq1ddwzc140ahsd04l5i3pa13xxw1k2gjzbzb")))

(define-public crate-persistent-0.2.1 (c (n "persistent") (v "0.2.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "1lj3bbvy3b5y0g5l01kzf4n1hvxdsjlwv9vaqn890vq2drzfl2hc")))

(define-public crate-persistent-0.3.0 (c (n "persistent") (v "0.3.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "05k95m4liz62k82nk365yhadmirbrxy1bj5wqrrc4bgp5r7wkjfl")))

(define-public crate-persistent-0.4.0 (c (n "persistent") (v "0.4.0") (d (list (d (n "iron") (r ">= 0.5, < 0.7") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)))) (h "1iblxjjzd0h784l5y573nw5z3pdb3330k69hh413agagkh0a13wf")))

