(define-module (crates-io pe rs persistent_hashmap) #:use-module (crates-io))

(define-public crate-persistent_hashmap-0.0.1 (c (n "persistent_hashmap") (v "0.0.1") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)))) (h "0q70nfwh8kmigmskpg505zq6m5h8r598hvlafw29sahmdpdjv8g9")))

(define-public crate-persistent_hashmap-0.1.0 (c (n "persistent_hashmap") (v "0.1.0") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)))) (h "1w4n0bpkqm5fymyj9md8n6hgvc0g34wv9fgdzhimqqahzan8071j")))

(define-public crate-persistent_hashmap-0.1.1 (c (n "persistent_hashmap") (v "0.1.1") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)))) (h "19f65c7xdsi9d65ccarhsijwalzd4cfnsnpbmc3qn9bsfsv7xxkw")))

(define-public crate-persistent_hashmap-0.1.2 (c (n "persistent_hashmap") (v "0.1.2") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)))) (h "13ph9aw6i24lkz5vps3h4nna9x80880if2gvnc4vzwyf6ylkih3f")))

(define-public crate-persistent_hashmap-0.1.3 (c (n "persistent_hashmap") (v "0.1.3") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "07zbcy2409xkgvg9i0vvq1krg2p0x7mkkz1c6ap1bxi4q8wqc820")))

(define-public crate-persistent_hashmap-0.1.4 (c (n "persistent_hashmap") (v "0.1.4") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "0rq6slkqm2man3rhxm1p7cyrxw5x9lyviws91fjz4f2afb6gm0xl")))

(define-public crate-persistent_hashmap-0.2.0 (c (n "persistent_hashmap") (v "0.2.0") (d (list (d (n "persistent_array") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "1jxvrz81kah8yjyhg44i1jgg8rk44wfhyq87h21f0fkckbkrka8y")))

(define-public crate-persistent_hashmap-0.2.1 (c (n "persistent_hashmap") (v "0.2.1") (d (list (d (n "clippy") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "persistent_array") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "0i0j7hpsfqj8syc1m0zid1mszwnfidnhlcsb0ysbl4fq2nnimrpm")))

(define-public crate-persistent_hashmap-0.2.2 (c (n "persistent_hashmap") (v "0.2.2") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "persistent_array") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "0nx1j434lkrarjg0ppi0w0282rpg58q56hd4s65ac294ibskf781") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-persistent_hashmap-0.2.3 (c (n "persistent_hashmap") (v "0.2.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "persistent_array") (r "^0.3.3") (d #t) (k 0)) (d (n "twox-hash") (r "^0.1") (d #t) (k 0)))) (h "03zqhf4yljpk37zhwr5cyycyb2i3mdfnwb21flralji18x8b0qr9")))

(define-public crate-persistent_hashmap-0.2.4 (c (n "persistent_hashmap") (v "0.2.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "persistent_array") (r "^0.3.3") (d #t) (k 0)))) (h "169s24z32ym1lssfsa6acmc6gaib99jpq18l9hsm5yj3fsvw83an")))

(define-public crate-persistent_hashmap-0.2.5 (c (n "persistent_hashmap") (v "0.2.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "persistent_array") (r "^0.3.3") (d #t) (k 0)) (d (n "twox-hash") (r "^0.1") (d #t) (k 0)))) (h "041n9h60pxf75bw0gayxir4zqmq3n4rm6ks37gkz4nd4ifim06jg")))

(define-public crate-persistent_hashmap-0.3.0 (c (n "persistent_hashmap") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "persistent_array") (r "^0.4.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.0") (d #t) (k 0)))) (h "1jnc83920af24svpbpvkv905dcshwpzn4gq3rbxyj4f650940l6w")))

(define-public crate-persistent_hashmap-0.4.0 (c (n "persistent_hashmap") (v "0.4.0") (d (list (d (n "persistent_array") (r "^0.5.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.0") (d #t) (k 0)))) (h "1p382bpqf39494bjpsjb7sfapgdrp2cy2yfs25l8c3ynplw9schc")))

