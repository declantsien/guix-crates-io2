(define-module (crates-io pe rs persia-futures) #:use-module (crates-io))

(define-public crate-persia-futures-0.1.0 (c (n "persia-futures") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.1") (d #t) (k 0)) (d (n "async-compat") (r "^0.1.4") (d #t) (k 0)) (d (n "async-executor") (r "^1.0") (d #t) (k 0)) (d (n "async-lock") (r "^2.1") (d #t) (k 0)) (d (n "async-oneshot") (r "^0.4") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1") (d #t) (k 0)) (d (n "flume") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smol") (r "^1.0") (d #t) (k 0)) (d (n "smol-timeout") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0lzzysc9dp2ccayih5yzkz6m9bcax6b24s08jqbw2cg149ix9f67")))

