(define-module (crates-io pe rs persistent_stack) #:use-module (crates-io))

(define-public crate-persistent_stack-0.1.0 (c (n "persistent_stack") (v "0.1.0") (h "1mw2g4j2gjr7lixr5f5lfzqabjzmz02zid2jwq2fh6q5v7500wsr")))

(define-public crate-persistent_stack-0.1.1 (c (n "persistent_stack") (v "0.1.1") (h "0l3j3fjq18alfysznw84ya2xm2aza3845mqfk5ixxz882b57fijs")))

