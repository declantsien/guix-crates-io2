(define-module (crates-io pe rs perseus-size-opt) #:use-module (crates-io))

(define-public crate-perseus-size-opt-0.1.0 (c (n "perseus-size-opt") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1q4ay55qqipsi92sqmlcaks89nn9jsz7vjnjpya3z6g7jwyf9d3h")))

(define-public crate-perseus-size-opt-0.1.1 (c (n "perseus-size-opt") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0k5nkdwpcf3ymalwa9p0h53i3yxhcy6nkpdsdkcc9n5pin8m8x3p")))

(define-public crate-perseus-size-opt-0.1.2 (c (n "perseus-size-opt") (v "0.1.2") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1wncvc4m50a71hd1jlrcz9327dky6v7f0kz7jggvn6wzswiihhd5")))

(define-public crate-perseus-size-opt-0.1.3 (c (n "perseus-size-opt") (v "0.1.3") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05rdxav2b8nw2gz89akydvh68sg91dnhcgxs09kqsf811vffs88v")))

(define-public crate-perseus-size-opt-0.1.4 (c (n "perseus-size-opt") (v "0.1.4") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15kpkdrk9m6s398cgl1hz19dkjhp0siw6vpp53ms5nslkygvsaqx")))

(define-public crate-perseus-size-opt-0.1.5 (c (n "perseus-size-opt") (v "0.1.5") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r ">=0.3.0-beta.22") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wairxf9py4sv0aa1rgh625iaarb2w77q1bb5d105gnfqg2wdb4f")))

(define-public crate-perseus-size-opt-0.1.6 (c (n "perseus-size-opt") (v "0.1.6") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0-beta.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1m2a4903rmyi2si6zs1r5nz9r4rs9av5gvkblkia79wfd9kp7s4x")))

(define-public crate-perseus-size-opt-0.1.7 (c (n "perseus-size-opt") (v "0.1.7") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vavh7m2dzgj2aw3x4vc0q97hzl9g1pi56cyfwd8wz3hlzpv6v0d")))

(define-public crate-perseus-size-opt-0.1.8 (c (n "perseus-size-opt") (v "0.1.8") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0qaij62lnajnc09bnswryb115fhw9zcsr26l29iz1vjzwds130hl")))

(define-public crate-perseus-size-opt-0.1.9 (c (n "perseus-size-opt") (v "0.1.9") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "perseus") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ajj5jss5z7phgmcf7zjhnwh32wszk8dbpr00yambwl6nr58z20d")))

