(define-module (crates-io pe rs personnel-locale) #:use-module (crates-io))

(define-public crate-personnel-locale-0.1.0 (c (n "personnel-locale") (v "0.1.0") (d (list (d (n "fluent") (r "^0.16") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "1cmrbbcqn2pyzmgvvwrzm0bk48l7i9hdq06fca84lja3p2gcvi4w")))

(define-public crate-personnel-locale-0.1.1 (c (n "personnel-locale") (v "0.1.1") (d (list (d (n "fluent") (r "^0.16") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "1pkn0mihnl7cm2akfc18qazsxcmv98mb1ls0fhp0f9k0rz549bkg")))

