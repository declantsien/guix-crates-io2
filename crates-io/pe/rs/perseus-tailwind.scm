(define-module (crates-io pe rs perseus-tailwind) #:use-module (crates-io))

(define-public crate-perseus-tailwind-0.4.0 (c (n "perseus-tailwind") (v "0.4.0") (d (list (d (n "perseus") (r "^0.4.0-beta.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "124xvn01x1vrd42h704wp3kd0amvj7bpnigj5r788sc2xpxdwy5q")))

(define-public crate-perseus-tailwind-0.4.1 (c (n "perseus-tailwind") (v "0.4.1") (d (list (d (n "perseus") (r "^0.4.0-beta.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1hymvcb90a1sp4pkzncdg9a6sk1riklg7a485gld7pnp6hp8a50j")))

(define-public crate-perseus-tailwind-0.4.2 (c (n "perseus-tailwind") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4.0-beta.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "011xbwxiaysx8ya36by0z15nw1qnpc56ph0z77nnkpp9sdwrvsal") (y #t)))

(define-public crate-perseus-tailwind-0.4.3 (c (n "perseus-tailwind") (v "0.4.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4.0-beta.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0yx9627l53cp77g7qzw74jrl9iz0b9cm0zs41r60nank4iynkxqw")))

(define-public crate-perseus-tailwind-0.4.4 (c (n "perseus-tailwind") (v "0.4.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4.0-beta.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0dkqci4v6cs7iy3gz3azq9nsgv2m803d62236qdz0zwq9f1m9jh4")))

(define-public crate-perseus-tailwind-0.4.5 (c (n "perseus-tailwind") (v "0.4.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4.0-beta.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "14hbzq0ngznnlrjdp289fa9ijgjqz4h6rkv0g311qazy1d1kb22k")))

(define-public crate-perseus-tailwind-0.4.6 (c (n "perseus-tailwind") (v "0.4.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4.0-beta.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1zckrqxspxz1ik6ksi8zs4frhqlgdy28ss9g5qahfniwrbgmlvjf")))

(define-public crate-perseus-tailwind-0.4.7 (c (n "perseus-tailwind") (v "0.4.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1fiyrlx5id7r7vvjmr0yvpzzj7zy0lid303p6y6niay158j0vda4")))

(define-public crate-perseus-tailwind-0.5.0 (c (n "perseus-tailwind") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perseus") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0a2f6z9ayz5bsi9ysfykhw0k5lmvab3wm455b35f37dipy34s773")))

