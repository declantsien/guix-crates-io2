(define-module (crates-io pe rs persist-o-vec) #:use-module (crates-io))

(define-public crate-persist-o-vec-0.1.0 (c (n "persist-o-vec") (v "0.1.0") (h "1caa3is7vwpwvna9c0rc7py8an6xhdnxxn9v7zrzblx0xfq7arps") (f (quote (("u8_index") ("u64_index") ("u32_index") ("u16_index") ("default" "u16_index"))))))

(define-public crate-persist-o-vec-0.1.1 (c (n "persist-o-vec") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0ys5g9xgxwfl92b81ck8dzb0s2i1dns4hgj8y5vz9s9aqjlsjbb4") (f (quote (("u8_index") ("u64_index") ("u32_index") ("u16_index") ("default" "u16_index"))))))

(define-public crate-persist-o-vec-0.1.2 (c (n "persist-o-vec") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0d4y6dy0r6yqqpjk0ix7mp0149h7lc5mpscn9yfhxaghkmr2r7gr") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.1.5 (c (n "persist-o-vec") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "06dxgna2iarchaf6d6g7x0kr94dipwha21swxlwr99rfvi46h369") (f (quote (("shrink_indexers_always") ("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.1.6 (c (n "persist-o-vec") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "146y6wm4jmli7nh9wgzy6j2xv5hyy2cdwq9f836p8kas197mgqf4") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.1.7 (c (n "persist-o-vec") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "16p99wlp0f27hq1fwdji93pvyx3sm0dm6rxancwg6szpnnpwc086") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.2.1 (c (n "persist-o-vec") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1vigp3qjwscg7hbxs0qkjaf464imiwyx0jaq1zg6kxvbs2wvs98r") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.2.2 (c (n "persist-o-vec") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1pa74amlhhin835vzf823aqmx6946pqi0hw1dnjkfnf8dfp8dd2m") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.2.3 (c (n "persist-o-vec") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1827csn22d1jr7iz0c6lpk4c0652ps2darv20a095cgib2vgf1mn") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.2.4 (c (n "persist-o-vec") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "159b3bd7khj50qgjzkyvmmxwva967bj8ryk62n0jrgdfx0s2wx5h") (f (quote (("default" "capacity_u32_len") ("capacity_u8_len") ("capacity_u64_len") ("capacity_u32_len") ("capacity_u16_len"))))))

(define-public crate-persist-o-vec-0.3.0 (c (n "persist-o-vec") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "11d8jr6amhfx5vilwkrfcwlxan4f3j8qsyv36n04qajyv45nlbrf")))

(define-public crate-persist-o-vec-0.3.1 (c (n "persist-o-vec") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "08gl72wl9diz065yb8hx7iqbh3jjlrf1a0mx0avbnx2j10v2zcgn")))

