(define-module (crates-io pe rs persistent-structs) #:use-module (crates-io))

(define-public crate-persistent-structs-0.1.0 (c (n "persistent-structs") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4h9xqs38lyfz270c48byaljwbx2x5n63i1abf9bd1hbq9r92f4")))

(define-public crate-persistent-structs-0.1.1 (c (n "persistent-structs") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "10biyigkyjnicghavafx9qc5lp6bbsqpz462igaw7z0ss8ccdlfn")))

