(define-module (crates-io pe rs personnummer) #:use-module (crates-io))

(define-public crate-personnummer-0.1.0 (c (n "personnummer") (v "0.1.0") (h "0zxj7xrdji9v4bgd1jkpid6h8fm6vd4zs7vi3vg5lc7myf8g64s6")))

(define-public crate-personnummer-3.0.1 (c (n "personnummer") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03vpdj8av7l3bkwi6m1zfvjxwhb0p652hl199hgcckkrwyd64wv8")))

(define-public crate-personnummer-3.0.2 (c (n "personnummer") (v "3.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11m62zrpkpfcwjw624hvcpmgfz0lzyxcd30qrlmkk5jkcgidb2rp")))

(define-public crate-personnummer-3.1.0 (c (n "personnummer") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cqwrq0yvxa0jw1id3knvwbvmwyjk2rdr351w0g23nln7y6wpmz9")))

(define-public crate-personnummer-3.1.1 (c (n "personnummer") (v "3.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m3sq7c87cligqi7bvs3wx1v5c5hs6ssbwhjgqywwb8nf2wik0da")))

