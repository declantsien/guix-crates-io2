(define-module (crates-io pe rs perspective-server) #:use-module (crates-io))

(define-public crate-perspective-server-3.0.0-alpha.1 (c (n "perspective-server") (v "3.0.0-alpha.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.115") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "tracing") (r ">=0.1.36") (d #t) (k 0)))) (h "1gj9z1lcf2l1wn7qckp2dfbwg7mlajz0r9hgshd9yp3ys42zrrf3") (f (quote (("external-cpp") ("default"))))))

