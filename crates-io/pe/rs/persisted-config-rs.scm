(define-module (crates-io pe rs persisted-config-rs) #:use-module (crates-io))

(define-public crate-persisted-config-rs-0.1.0 (c (n "persisted-config-rs") (v "0.1.0") (h "0lpydmz7ds1cn2y5z6178skbavzwgxcffabcd1d1kqbw3gdh2nja")))

(define-public crate-persisted-config-rs-0.2.0 (c (n "persisted-config-rs") (v "0.2.0") (h "121mvhl5pm0d4ghvqfy701ffvya93fn7z7m4vgv96dksysz61df3")))

(define-public crate-persisted-config-rs-0.2.1 (c (n "persisted-config-rs") (v "0.2.1") (h "0aipkh8s5dd8qssm6b2wgnk8k68yarg7pz6cg76ia08ly3s090y9")))

