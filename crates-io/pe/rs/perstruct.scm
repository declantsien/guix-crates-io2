(define-module (crates-io pe rs perstruct) #:use-module (crates-io))

(define-public crate-perstruct-0.1.0 (c (n "perstruct") (v "0.1.0") (d (list (d (n "perstruct-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "0icsq9f5ah37mbc84nkrmhkx8603xn2an1bi70bf99ms88v4bcfj")))

