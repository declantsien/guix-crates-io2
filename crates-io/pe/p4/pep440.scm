(define-module (crates-io pe p4 pep440) #:use-module (crates-io))

(define-public crate-pep440-0.1.0 (c (n "pep440") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08a90yrgjs6im3959h1nd6cx1lfc7q47jnai2173qzlqvlz3alpl")))

(define-public crate-pep440-0.1.1 (c (n "pep440") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "066r68nimyfwim7qpadwnvgkpbszi59h5vxfr47cbq0iick6mhwz")))

(define-public crate-pep440-0.2.0 (c (n "pep440") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12dx79jwmql7nnq1jgayy4ijwh6v26b6sjfdiqz91g7slq6b0hc8")))

