(define-module (crates-io pe rg perg) #:use-module (crates-io))

(define-public crate-perg-0.1.0 (c (n "perg") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1dmbc3wkkdwl6vbal8fn1padnv76k7w3glwba3by9nhhhfxhpynv")))

(define-public crate-perg-0.2.0 (c (n "perg") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "099qyvx5xkrxfr6409sscxvsx89jp5f6h171cs5cm1z6rgqv3ai0")))

(define-public crate-perg-0.2.1 (c (n "perg") (v "0.2.1") (d (list (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "108v5mn4lyf49jgjwa97ycj12zrnhqc04x4wlay1n6vx6s1zxpbb")))

(define-public crate-perg-0.3.0 (c (n "perg") (v "0.3.0") (d (list (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1yyr3w5xq85j7c7l48qnwr8w1nikadzlvss6ms9554azzxqyq56x")))

(define-public crate-perg-0.4.0 (c (n "perg") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0f77zcja0qx6h1na2gmpy8vp1h4b3cgp27mz53qa5z6qnkcanr1n")))

(define-public crate-perg-0.5.0 (c (n "perg") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "137bqaswvxjx5zr74nqy3vp70g70n0ijr4waddw7ikzm50413ry9")))

(define-public crate-perg-0.5.1 (c (n "perg") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "16qapn5bbc2cn2m09s0h6z22diwxqpbq5cwkb4jcvdsqqc459jv9")))

