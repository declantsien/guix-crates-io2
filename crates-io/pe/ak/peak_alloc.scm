(define-module (crates-io pe ak peak_alloc) #:use-module (crates-io))

(define-public crate-peak_alloc-0.1.0 (c (n "peak_alloc") (v "0.1.0") (h "164xdrmzwxvqrby60dnamid1zlg5vm816wg6jfivps9n9wmcmr8a")))

(define-public crate-peak_alloc-0.2.0 (c (n "peak_alloc") (v "0.2.0") (h "1rcdc2vf366mrxm5yqv75djrr1dccj1c51138ls6rm571a5rglw4")))

(define-public crate-peak_alloc-0.2.1 (c (n "peak_alloc") (v "0.2.1") (h "0gpawzx2rwdb52hlb9as9sx303b09qp853s68rixfbw3vpifii19")))

