(define-module (crates-io pe ak peakmem-alloc) #:use-module (crates-io))

(define-public crate-peakmem-alloc-0.1.0 (c (n "peakmem-alloc") (v "0.1.0") (h "15zy4kwwwdq6sq7n4yggfq8m1pcmgylsjp0kykn0s0fl2xpr9x9v") (f (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2.0 (c (n "peakmem-alloc") (v "0.2.0") (d (list (d (n "jemallocator") (r "^0.5.4") (d #t) (k 2)))) (h "0z73p03f443bm8ig0yaadc52jn07h1bf770lf8vsls6c2n92k39n") (f (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2.1 (c (n "peakmem-alloc") (v "0.2.1") (d (list (d (n "jemallocator") (r "^0.5.4") (d #t) (k 2)))) (h "0vbm0075njbppqyzwpw886xpmir6x3vry4prdp32a9iykq6qqwx0") (f (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2.2 (c (n "peakmem-alloc") (v "0.2.2") (d (list (d (n "jemallocator") (r "^0.5.4") (d #t) (k 2)))) (h "1m3kqd0gdyksh9fxgqp8r9l4g01wj5d7cii0p3bag7nwiplzxdan") (f (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.3.0 (c (n "peakmem-alloc") (v "0.3.0") (d (list (d (n "jemallocator") (r "^0.5.4") (d #t) (k 2)))) (h "1xj8v7cd7bkgzn1w41cwrv06fqax6g9dhmzdmijj8ivsjy545dsy") (f (quote (("nightly") ("default"))))))

