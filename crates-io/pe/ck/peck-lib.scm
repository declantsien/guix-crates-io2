(define-module (crates-io pe ck peck-lib) #:use-module (crates-io))

(define-public crate-peck-lib-1.0.0 (c (n "peck-lib") (v "1.0.0") (h "1m01a3pvp8ln4rck4azapy27dka1x1wh0fs9w4dq1390k8c1zr85") (y #t)))

(define-public crate-peck-lib-1.0.1 (c (n "peck-lib") (v "1.0.1") (h "11mjx1xfr69py5iykjya6fyp6g47wgaj6mgc86vl7klgsp5ssf9a")))

