(define-module (crates-io pe ck peck-exif) #:use-module (crates-io))

(define-public crate-peck-exif-1.0.0 (c (n "peck-exif") (v "1.0.0") (h "02sfz9rb6wvqix4hzd24k339afv6366wawrjlfqigyrfz8c366fw")))

(define-public crate-peck-exif-1.0.1 (c (n "peck-exif") (v "1.0.1") (h "1mkjrbkpnl6bdb2d704biapc1ri43iclbhgwm57ms8rq8czfadz5")))

