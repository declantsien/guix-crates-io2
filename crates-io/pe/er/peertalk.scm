(define-module (crates-io pe er peertalk) #:use-module (crates-io))

(define-public crate-peertalk-0.1.0 (c (n "peertalk") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vdjsk1i2q227p53gv3z02c44v2xsv6v3i320401bxisx2qyp2h9")))

(define-public crate-peertalk-0.2.0 (c (n "peertalk") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m4j2n4vxgyvzs4d1jplgv1xmrmz0j1ab1swf39k4iadvhg812kk")))

