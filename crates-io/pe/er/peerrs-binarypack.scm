(define-module (crates-io pe er peerrs-binarypack) #:use-module (crates-io))

(define-public crate-peerrs-binarypack-0.1.0 (c (n "peerrs-binarypack") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "01v4j6w81brgbf34ri3h0iawbbqihzm7zba40xlr6w1rwksnkriq")))

