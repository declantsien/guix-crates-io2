(define-module (crates-io pe er peer-monitoring-service-types) #:use-module (crates-io))

(define-public crate-peer-monitoring-service-types-0.1.3 (c (n "peer-monitoring-service-types") (v "0.1.3") (d (list (d (n "aptos-config") (r "^0.1.0") (d #t) (k 0)) (d (n "network") (r "^0.1.0") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0v8330dyc9j0l04z0y446ns5nbzbz7757746gb3hxayg73xif3hz") (y #t)))

(define-public crate-peer-monitoring-service-types-0.1.4 (c (n "peer-monitoring-service-types") (v "0.1.4") (d (list (d (n "aptos-config") (r "^0.1.0") (d #t) (k 0)) (d (n "network") (r "^0.1.0") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0pazqqsxdz252la226jg4lznzzzkg7fwi2mqqjpcdhn0q94042jm") (y #t)))

(define-public crate-peer-monitoring-service-types-0.1.6 (c (n "peer-monitoring-service-types") (v "0.1.6") (d (list (d (n "aptos-config") (r "^0.1.0") (d #t) (k 0)) (d (n "network") (r "^0.1.0") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0l2ldqmrwgyb307vh2dscgjz6x8lhc797cj5n4sxj0iyrviys2a5") (y #t)))

(define-public crate-peer-monitoring-service-types-0.1.7 (c (n "peer-monitoring-service-types") (v "0.1.7") (d (list (d (n "aptos-config") (r "^0.1.7") (d #t) (k 0)) (d (n "network") (r "^0.1.7") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0vkklh65pk1mjr52vrhfj6f5crp782mnfvgx7jb6p7kgxnqhima4") (y #t)))

(define-public crate-peer-monitoring-service-types-0.2.1 (c (n "peer-monitoring-service-types") (v "0.2.1") (d (list (d (n "aptos-config") (r "^0.2.1") (d #t) (k 0)) (d (n "network") (r "^0.2.1") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1kpqbdv1l0yh4ayxj2ky41dn8s12w4kc5aiqvkjjq76rfs1iv76m") (y #t)))

(define-public crate-peer-monitoring-service-types-0.2.2 (c (n "peer-monitoring-service-types") (v "0.2.2") (d (list (d (n "aptos-config") (r "^0.2.1") (d #t) (k 0)) (d (n "network") (r "^0.2.1") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "02qz68x2by61859kz5jq3z2rymqv6b7dmvwwr0gn99w0jvgh9n1h") (y #t)))

(define-public crate-peer-monitoring-service-types-0.2.6 (c (n "peer-monitoring-service-types") (v "0.2.6") (d (list (d (n "aptos-config") (r "^0.2.1") (d #t) (k 0)) (d (n "network") (r "^0.2.1") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0x95nsjm25xm81y83acv8rhgmw6n6sfb5n6zi66v3ly2yb67n78k") (y #t)))

(define-public crate-peer-monitoring-service-types-0.2.7 (c (n "peer-monitoring-service-types") (v "0.2.7") (d (list (d (n "aptos-config") (r "^0.2.1") (d #t) (k 0)) (d (n "network") (r "^0.2.1") (d #t) (k 0) (p "aptos-network")) (d (n "serde") (r "^1.0.124") (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1fhybhscqdbm1636jz8nrcsz4kzhbfl4v2yhmqcc38lfaq80g12y") (y #t)))

