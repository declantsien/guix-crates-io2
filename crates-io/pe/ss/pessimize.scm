(define-module (crates-io pe ss pessimize) #:use-module (crates-io))

(define-public crate-pessimize-1.0.0 (c (n "pessimize") (v "1.0.0") (d (list (d (n "safe_arch") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0ybqy5m1bd14q8k97rs0975kfsz2k29ckd57gsa4km06i2c2gprw") (f (quote (("std" "alloc") ("nightly") ("default_impl" "nightly") ("default" "std") ("alloc")))) (r "1.63.0")))

