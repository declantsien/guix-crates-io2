(define-module (crates-io pe re peresil) #:use-module (crates-io))

(define-public crate-peresil-0.1.0 (c (n "peresil") (v "0.1.0") (h "05qjbqfqmz0466594k17xhxhbfsfvhyf5zikvdlypf2v8lzsv2kv")))

(define-public crate-peresil-0.2.0 (c (n "peresil") (v "0.2.0") (h "0qwm6z23paffxkihlnqy16zjcwdr9dqmh6lhp9ql3fzsafy8bg37")))

(define-public crate-peresil-0.2.1 (c (n "peresil") (v "0.2.1") (h "1gzcydcwhhxnxjkrkbs14ald3z12bc2q7fzlqj4alv8fbjcyza1d")))

(define-public crate-peresil-0.3.0 (c (n "peresil") (v "0.3.0") (h "0mwyw03yqp0yqdjf4a89vn86szxaksmxvgzv1j2nw69fsmp8hn7n")))

(define-public crate-peresil-0.4.0-alpha.0 (c (n "peresil") (v "0.4.0-alpha.0") (h "19gscdxf1alxp2nr9wk81q1haf7ww54vkfjv3i8z3ds7n7vbnvsr") (f (quote (("default" "combinators") ("combinators"))))))

