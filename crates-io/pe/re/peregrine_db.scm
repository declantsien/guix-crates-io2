(define-module (crates-io pe re peregrine_db) #:use-module (crates-io))

(define-public crate-peregrine_db-0.1.0 (c (n "peregrine_db") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "11nkryzy25v3y3mng10z0jwr4x43xa7hk8zk7h8c1vipiqkkjlli")))

