(define-module (crates-io pe re perestroika) #:use-module (crates-io))

(define-public crate-perestroika-0.1.0 (c (n "perestroika") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)))) (h "1drw04nj8gb1jlyhfq56xh3dzw7a4fs166ssifgzv6fwfr0dbwrl")))

(define-public crate-perestroika-0.1.1 (c (n "perestroika") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0h0avj3ykbal05ym8aylxvawxgb4b3yr759mz1xsmrf1zz64md9j")))

(define-public crate-perestroika-0.1.2 (c (n "perestroika") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0viljgcarm3d3d0awplqs551nab79wjfbh6gkb92y739y2n9d8rp")))

(define-public crate-perestroika-0.1.3 (c (n "perestroika") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0a53bcglibxb8dlnp5r4m556d1gr2bkljw85ns2i9flr2i69y6rp")))

(define-public crate-perestroika-0.1.4 (c (n "perestroika") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1i46s7rfp770n4aqckj70xk8jpw1zvbp766ccpg1g5fhgljd69c7")))

(define-public crate-perestroika-0.1.5 (c (n "perestroika") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1q2r30zmwg8aaw6k8lwbf81h278rc21kfl4n82dl1yy3da5gzdl9")))

