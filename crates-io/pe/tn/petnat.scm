(define-module (crates-io pe tn petnat) #:use-module (crates-io))

(define-public crate-petnat-0.0.1 (c (n "petnat") (v "0.0.1") (h "0lpb64y15b5746ar443as1g5rr8p5j4h25g0qabg4f6sc8r8w58y")))

(define-public crate-petnat-0.0.2 (c (n "petnat") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)))) (h "15rc2fdcp4r6c780x6knrw95l9agkl8i9izvn2ngq8q33yr38kim")))

