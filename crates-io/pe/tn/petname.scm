(define-module (crates-io pe tn petname) #:use-module (crates-io))

(define-public crate-petname-0.1.0 (c (n "petname") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0gc0g2whs8py4v6n095yz1ari0m0nw7cankzl85c8dvb2hwb8q12")))

(define-public crate-petname-0.1.1 (c (n "petname") (v "0.1.1") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "09cyb64c9z5lb6dcg3wk1510zgmj92z9r2h7z1r4bjz0al7myjrs")))

(define-public crate-petname-0.1.2 (c (n "petname") (v "0.1.2") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0pcjqj6bbmcfw1vzhrx3xskxdfbaszzjd50v3a2pdd2kmfjnv9cg")))

(define-public crate-petname-0.2.0 (c (n "petname") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "14dw5ka822ak6c6zmqcgs48z6izg9a6xvkhjr6xjn8y2mv5ymv5m")))

(define-public crate-petname-1.0.0 (c (n "petname") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0shhg7swk1l7rixwcnhrfx2adnmx67mn4n5bl5sy4qlik0pylbwj")))

(define-public crate-petname-1.0.1 (c (n "petname") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04y3fb01029hfzsk3m0ybqh54w0nnk05i153yl3r42c6pvih2mfk")))

(define-public crate-petname-1.0.2 (c (n "petname") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06p1zd2n9gavmggzaqy6m4ch83qrfz246jd7sbnzpr298fyvam6d")))

(define-public crate-petname-1.0.3 (c (n "petname") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "112j2rvmsj96jzwqmx99h61g6iskz2wrrkf854949kwd179cy5si")))

(define-public crate-petname-1.0.4 (c (n "petname") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lkdqxrcrd4fy9lmv5xr2x8f95d8kxnnmwb8hx5q1w58lfjfnlmg")))

(define-public crate-petname-1.0.5 (c (n "petname") (v "1.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0kpcnb4qbm12wq3fx094m01zrah40gjwfzxzv21i5ghhihfh6w8s")))

(define-public crate-petname-1.0.6 (c (n "petname") (v "1.0.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lca65c3364labs9krxgh9q4z5vf65b14y6lxrjcpsgspd1wy6y4")))

(define-public crate-petname-1.0.7 (c (n "petname") (v "1.0.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0blxvp5jh56iih8zr1l3wq90w8fv33r3hmah2ack0mpjgsbz802f")))

(define-public crate-petname-1.0.8 (c (n "petname") (v "1.0.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "190qvfa1ffjhp6w7xwx0d94wsb8bywr6ydwmmcy7r5dl53m8vjyf")))

(define-public crate-petname-1.0.9 (c (n "petname") (v "1.0.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1h67w4q526fdsixr1jy4y71wiv8jbm12ra0kx6pd2ikizx7m55s3")))

(define-public crate-petname-1.0.10 (c (n "petname") (v "1.0.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zi5rvv8w6vcacy54ryym8626z1paps67q6b5nh8j4yixa6npcm8")))

(define-public crate-petname-1.0.11 (c (n "petname") (v "1.0.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0z0qn1vw100mviag1lq06lfs00y4jd0lcv016a4w2aqn8w0gqkik")))

(define-public crate-petname-1.0.12 (c (n "petname") (v "1.0.12") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "099qkw2pl1p0sryf34sp7km5m01b8b2p9jn9yp1864hnpv6idvx7")))

(define-public crate-petname-1.0.13 (c (n "petname") (v "1.0.13") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0hacj0r04pz0nqvjw29a07qvgx4yhqlqqcngwlbh6nq4wpx8zkpj")))

(define-public crate-petname-1.1.0 (c (n "petname") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "rand") (r "^0.8.0") (k 0)))) (h "1k5yj36r0v13rirc69pxiyxmjjsd3cafrfvnky0lgpbwgcf8xvfr") (f (quote (("std_rng" "rand/std" "rand/std_rng") ("default_dictionary") ("default" "clap" "std_rng" "default_dictionary"))))))

(define-public crate-petname-1.1.1 (c (n "petname") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "rand") (r "^0.8.0") (k 0)))) (h "18xmxl0faap7dciiz9p37hcnnsk14q37jyi48y73srx70r78fil2") (f (quote (("std_rng" "rand/std" "rand/std_rng") ("default_dictionary") ("default" "clap" "std_rng" "default_dictionary"))))))

(define-public crate-petname-1.1.2 (c (n "petname") (v "1.1.2") (d (list (d (n "itertools") (r "^0.10.0") (k 0)) (d (n "rand") (r "^0.8.0") (k 0)) (d (n "structopt") (r "^0.3.23") (o #t) (k 0)))) (h "1wnjbygq68l8iv5f92j05r97lir9irfb4y7lbaadhc9i006d4x4l") (f (quote (("std_rng" "rand/std" "rand/std_rng") ("default_dictionary") ("default" "clap" "std_rng" "default_dictionary") ("clap" "structopt"))))))

(define-public crate-petname-1.1.3 (c (n "petname") (v "1.1.3") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "0l3rvz6ww33v7h1cwgqcpklb00rbygyi3c2np2z50dcnc16idr7w") (f (quote (("std_rng" "rand/std" "rand/std_rng") ("default_dictionary") ("default" "clap" "std_rng" "default_dictionary"))))))

(define-public crate-petname-2.0.0-alpha.1 (c (n "petname") (v "2.0.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "0jr979d5fximsxsxdkrrcb0i9214rvig5jdmlvjvj2vizl0ky1lz") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-alpha.2 (c (n "petname") (v "2.0.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "1z4xp1l12a21557c5b220b78lfq11srshjv3v0vgn3969nkhhx94") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-alpha.3 (c (n "petname") (v "2.0.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "04x1h5j70g756b8frm3nnigjfnprlpr3770ipl10nsw48i97219r") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-alpha.4 (c (n "petname") (v "2.0.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "0wqsbwbz9302ikn2m8zsr33280lbwx734rg043pizxl654ysb42n") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-beta.1 (c (n "petname") (v "2.0.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)))) (h "0nnvqsihqp6g8k27jfk0qsfmzq2vpd9k1bmy17g7xjpg87hmvxi1") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-beta.2 (c (n "petname") (v "2.0.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0ck2nvlcv0j1k438j5k2qrig6lh1zpanw1a09j7fy5a7zx9920r7") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-beta.3 (c (n "petname") (v "2.0.0-beta.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r ">=0.11") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1nai0izljl26x5pd374kcqswg1j5l1c81xvgn7msdzjlrzn638hy") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0-beta.4 (c (n "petname") (v "2.0.0-beta.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r ">=0.11") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.8") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13bsvzbhfmk9w0qd52yid85a1azl06ikkaj5f6glf5krpnbm4pgw") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.0 (c (n "petname") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r ">=0.11") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.8") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1201jml1k1bf3irp3vnpij3a279y1ca8rwhcv2jca89zr05m94kg") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.1 (c (n "petname") (v "2.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r ">=0.11") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.8") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0xrkh49jd1p9hm2ybz3gawjphbrx0xhaac8xjassiy4ghcmgcwdd") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

(define-public crate-petname-2.0.2 (c (n "petname") (v "2.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r ">=0.11") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.8") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0s6hp8hpl8kiys65x02w9pljhcj7svnzckgg0yl32x5xvg7ivlww") (f (quote (("default-words") ("default-rng" "rand/std" "rand/std_rng") ("default" "clap" "default-rng" "default-words"))))))

