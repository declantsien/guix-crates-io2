(define-module (crates-io pe g- peg-pack) #:use-module (crates-io))

(define-public crate-peg-pack-0.1.0 (c (n "peg-pack") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "seahash") (r "^4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0hd7k9a9ns3kb407r27kykcn3jirnjzsmwh27km32v2xvvgx29hl")))

