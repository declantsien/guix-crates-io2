(define-module (crates-io pe g- peg-macros) #:use-module (crates-io))

(define-public crate-peg-macros-0.6.0 (c (n "peg-macros") (v "0.6.0") (d (list (d (n "peg-runtime") (r "= 0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0vp7w8r9p9cxxa0vcwikmwcb3sbz2zr6rlv97xml1mh7nm9li07j") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.6.1 (c (n "peg-macros") (v "0.6.1") (d (list (d (n "peg-runtime") (r "= 0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "176d203k8hkzvi0nz6dpk46agn15008a5gd1pcd7kcb2cd7ss21x") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.6.2 (c (n "peg-macros") (v "0.6.2") (d (list (d (n "peg-runtime") (r "= 0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0li8qrb8hyqr7v5mhrkym0xp7ijnbksqviqc2i3556cysdgick62") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.6.3 (c (n "peg-macros") (v "0.6.3") (d (list (d (n "peg-runtime") (r "=0.6.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0kdisa6di5gkgpw97897lg78jhsx6nliax3d4s6y8cvnz6n60vb3") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.7.0 (c (n "peg-macros") (v "0.7.0") (d (list (d (n "peg-runtime") (r "=0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0k44f3xwhkpbk0mbl0v88l0v12il24kj145gjgvbxv4dkf155amm") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.8.0 (c (n "peg-macros") (v "0.8.0") (d (list (d (n "peg-runtime") (r "=0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05nwzh94lvkalkm4cjac9gqlk3nfri7r7bfbjm4842vpgcabwdj5") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.8.1 (c (n "peg-macros") (v "0.8.1") (d (list (d (n "peg-runtime") (r "=0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "16ylkinxv7qc2ywlzxh916dx3alzyfcj7lg352245w2wq16hi42a") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.8.2 (c (n "peg-macros") (v "0.8.2") (d (list (d (n "peg-runtime") (r "=0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "141c76na4n9mfs1y22az59yanaz9kw5aabgnj28d2xlvhp71rrj6") (f (quote (("trace"))))))

(define-public crate-peg-macros-0.8.3 (c (n "peg-macros") (v "0.8.3") (d (list (d (n "peg-runtime") (r "=0.8.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10imq1mmvy9ig6i8b1c6dprih54fhchbf7ffzsjbcfpdcwhd8hgj") (f (quote (("trace"))))))

