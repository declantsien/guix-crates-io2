(define-module (crates-io pe g- peg-syntax-ext) #:use-module (crates-io))

(define-public crate-peg-syntax-ext-0.4.0 (c (n "peg-syntax-ext") (v "0.4.0") (d (list (d (n "peg") (r "^0.4") (d #t) (k 0)))) (h "0gpc8fymw0vbwdawrm6flqjd61w57qr1s79im0yzc3ha1xv570pz")))

(define-public crate-peg-syntax-ext-0.4.1 (c (n "peg-syntax-ext") (v "0.4.1") (d (list (d (n "peg") (r "^0.4") (d #t) (k 0)))) (h "03kahq74h963qxgv1sg43yx0giw33y80p7x3b0gfjx3bkkhfs0jd")))

(define-public crate-peg-syntax-ext-0.4.2 (c (n "peg-syntax-ext") (v "0.4.2") (d (list (d (n "peg") (r "^0.4") (d #t) (k 0)))) (h "1p3bvpxr84b8vfzmdasj1bs256cy5jhvindcvdbihpbd756y5ya0")))

(define-public crate-peg-syntax-ext-0.5.0 (c (n "peg-syntax-ext") (v "0.5.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 0)))) (h "0y5w35iqhrn9fwxf8lp0njnbjsq8cwv4z4csiw2n84nzx7rnikj1")))

(define-public crate-peg-syntax-ext-0.5.2 (c (n "peg-syntax-ext") (v "0.5.2") (d (list (d (n "peg") (r "^0.5") (d #t) (k 0)))) (h "1a01alv8d7hhk2hnaajgwwym919wxrk8a80xbx2p7y5w84b59ikk")))

(define-public crate-peg-syntax-ext-0.5.5 (c (n "peg-syntax-ext") (v "0.5.5") (d (list (d (n "peg") (r "^0.5") (d #t) (k 0)))) (h "0kfqvccz5ynq368yi0jficlqr2y3hr56719kr7k70yxa3b69pnvx")))

(define-public crate-peg-syntax-ext-0.5.6 (c (n "peg-syntax-ext") (v "0.5.6") (d (list (d (n "peg") (r "^0.5") (d #t) (k 0)))) (h "14x3aqq1234pq2ad2m0isb0js22g9jdl86dqlrpz7v7d0n8nq779")))

(define-public crate-peg-syntax-ext-0.5.7 (c (n "peg-syntax-ext") (v "0.5.7") (d (list (d (n "peg") (r "^0.5") (d #t) (k 0)))) (h "15jkn9d0vfvp8gvdpqnw01zmhfz5j8774h2p9r551c2kbx3mbzj2")))

