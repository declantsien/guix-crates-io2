(define-module (crates-io pe tf petfacts) #:use-module (crates-io))

(define-public crate-petfacts-0.1.0 (c (n "petfacts") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1rirg3ghh84i0j907p8ckf5jjlbrf3fgzp2k8pvax519pslhjgj1")))

(define-public crate-petfacts-0.1.1 (c (n "petfacts") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0f31cpy09s3z2wifrkz2a9zmrly2zvxrhhqrhw1qgqjhkq6pbg38") (y #t)))

(define-public crate-petfacts-0.1.2 (c (n "petfacts") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1d972w6g3w2v3dw02b5yy59ragi52qwg3pjqgcf9892gndhc6ww7") (y #t)))

(define-public crate-petfacts-0.1.3 (c (n "petfacts") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0bhai68m0qm1msinaznb7sk6g29smywnvnmv6wmkd6ckyslrcrjj") (y #t)))

(define-public crate-petfacts-0.1.4 (c (n "petfacts") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "009jw3k842jbqajlrbar6jgj3391rppim3f00g4m5ag8nj6zcqn6")))

