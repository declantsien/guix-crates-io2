(define-module (crates-io pe ni peniko) #:use-module (crates-io))

(define-public crate-peniko-0.1.0 (c (n "peniko") (v "0.1.0") (d (list (d (n "kurbo") (r "^0.11") (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1iahbny2ck1vyqzrkjbib9x1q7ms7qsbgjm4v5ahar0xc3n7zbya") (f (quote (("std" "kurbo/std") ("libm" "kurbo/libm") ("default" "std")))) (r "1.70")))

(define-public crate-peniko-0.1.1 (c (n "peniko") (v "0.1.1") (d (list (d (n "kurbo") (r "^0.11.0") (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1a8pm7q2alr8n2ivfhqblg4cwbvaqhf1kbc0pdb7i0wk80lxfa1w") (f (quote (("std" "kurbo/std") ("libm" "kurbo/libm") ("default" "std")))) (s 2) (e (quote (("serde" "smallvec/serde" "kurbo/serde" "dep:serde_bytes" "dep:serde")))) (r "1.70")))

