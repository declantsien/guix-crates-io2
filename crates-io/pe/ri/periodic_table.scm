(define-module (crates-io pe ri periodic_table) #:use-module (crates-io))

(define-public crate-periodic_table-0.1.0 (c (n "periodic_table") (v "0.1.0") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)))) (h "0wzbs8cys43cb4w5mysi24m50i5l1d5dymzyhhvn73hh4naq7708")))

(define-public crate-periodic_table-0.1.1 (c (n "periodic_table") (v "0.1.1") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)))) (h "1p0j80vc9gayk0llsjcqvcqxg42nck6w5xz7gqzhnvixdq4gk84s")))

(define-public crate-periodic_table-0.2.0 (c (n "periodic_table") (v "0.2.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "10amg9b831dvz98z3jlgn18y35n9q6slxjkd41dvn8ccpqbddihg")))

(define-public crate-periodic_table-0.3.0 (c (n "periodic_table") (v "0.3.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "1rw6fdgmqngywhvhsvr2bfsxkf5jiaxyik7cvshhzm6h0bqxvvi5")))

(define-public crate-periodic_table-0.3.1 (c (n "periodic_table") (v "0.3.1") (d (list (d (n "askama") (r "^0.8") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "1c882h4n3j9jy3q2wnzhpd029lbvx463hh4jh39im3441gd07q8v")))

(define-public crate-periodic_table-0.3.2 (c (n "periodic_table") (v "0.3.2") (d (list (d (n "askama") (r "^0.11") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "1pk3hy12nwsdfsilsjpq6ryv3wz7x7mnj9cyyasmxalp7fwxpk52")))

(define-public crate-periodic_table-0.4.0 (c (n "periodic_table") (v "0.4.0") (d (list (d (n "askama") (r "^0.12") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "1gp9nh1d4xj88z57xdrhxdzf3zfx41hgn4gagw3l9frh1g22q9a4") (r "1.62.1")))

(define-public crate-periodic_table-0.5.0 (c (n "periodic_table") (v "0.5.0") (d (list (d (n "askama") (r "^0.12") (d #t) (k 1)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "1bxh0nhis9q37b36f1cscbp03wpfgdvsg992skax4qz9hbnv32ss") (r "1.62.1")))

