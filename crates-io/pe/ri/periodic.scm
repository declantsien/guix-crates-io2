(define-module (crates-io pe ri periodic) #:use-module (crates-io))

(define-public crate-periodic-0.1.0 (c (n "periodic") (v "0.1.0") (h "06nzv8vmyw8wi698097vh22z5ysi8sdz17gi8gvvzjbv773rhhds")))

(define-public crate-periodic-0.1.1 (c (n "periodic") (v "0.1.1") (h "1ai6h249dr3bmxvvy6kfm0d83prmvxlp2lk999gdiwmvhhfqfy1h")))

