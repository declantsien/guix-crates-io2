(define-module (crates-io pe ri periodic-table-rs) #:use-module (crates-io))

(define-public crate-periodic-table-rs-0.1.0 (c (n "periodic-table-rs") (v "0.1.0") (h "06acg60q7jfk30qac6wm9qh62zjdccdifhmphw1psj8i102id5jv")))

(define-public crate-periodic-table-rs-0.1.1 (c (n "periodic-table-rs") (v "0.1.1") (d (list (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1kygj72c1r7xk0zbv69mld53g6k2g43a9pvlr4kp7lm0c3zgqfzf")))

(define-public crate-periodic-table-rs-0.1.2 (c (n "periodic-table-rs") (v "0.1.2") (d (list (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1ph4injh0cw16l04kcpcnihpgvfvbpndxv68mvbizjxqy3hf8vpq")))

