(define-module (crates-io pe ri peripherals) #:use-module (crates-io))

(define-public crate-peripherals-0.0.1 (c (n "peripherals") (v "0.0.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1jb0l1pzi6b36ghiy5i6px7h68cswfj5vkzsgm1llq45lkyf45ap")))

(define-public crate-peripherals-0.0.2 (c (n "peripherals") (v "0.0.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "006h2pbbf0jzjlzb0rx7rgzxih4v5j9ncc9mbmf87v9gl4qb6k5s")))

(define-public crate-peripherals-0.0.3 (c (n "peripherals") (v "0.0.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1bx2wp1z4l1kkl8h4pj379c4pvy9y59mipi2nikm4l9ws6nppql2")))

(define-public crate-peripherals-0.1.0 (c (n "peripherals") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0il6birxl74xfqgz55cygc27slrw60z1yzlrh4vz2dl4ra8bgdir")))

(define-public crate-peripherals-0.1.1 (c (n "peripherals") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0phh9najrrr7xq465dc2sm2wfnin2c5sv37a0872qax8mdkmg7r1")))

