(define-module (crates-io pe ri peril) #:use-module (crates-io))

(define-public crate-peril-0.0.0 (c (n "peril") (v "0.0.0") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0fpfhhyvswrxidbdhck8sany7sr5983hrpdzaaxrklp24kfz7jy7") (y #t)))

(define-public crate-peril-0.0.1 (c (n "peril") (v "0.0.1") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0l7h9llsghiprzb7mvnzl8nab8srx545x9ylgfz5ph0nlnx860r6") (y #t)))

(define-public crate-peril-0.0.2 (c (n "peril") (v "0.0.2") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0js6vvlng2pymp42rw2k3zhkwwxrdi4vrpng5sg2aidk3zl7cs0m") (y #t)))

(define-public crate-peril-0.0.3 (c (n "peril") (v "0.0.3") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "14a89yvqkdsvqxwcxlqz669c7jl4c2k78l3ky488mxasi8qms6g9") (y #t)))

(define-public crate-peril-0.0.4 (c (n "peril") (v "0.0.4") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1snwfm4vbnfzmpyc407kg78hadgpa51qv50vdh037npf6y58ah2g")))

(define-public crate-peril-0.0.5 (c (n "peril") (v "0.0.5") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0yl09ln5g0adlccbbc7vzig9mp7i50bjvnay049g5hn13i7hdpjy")))

(define-public crate-peril-0.1.0 (c (n "peril") (v "0.1.0") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1965symxr4vhz79p894hwxp8jhiysy3hahd891v5427rv0vh4619")))

(define-public crate-peril-0.1.1 (c (n "peril") (v "0.1.1") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "13rcc55rnlzvcmkqbid6qcf30hpbrc1bx4nniclgvp1za74s71p0")))

(define-public crate-peril-0.2.0 (c (n "peril") (v "0.2.0") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1jhkvac0gqsvpx37ar1kn89byivcrkcad4l7yvngr1i9hx75l57r")))

(define-public crate-peril-0.2.1 (c (n "peril") (v "0.2.1") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1i602ka0pqbd6ipya95rd74a9akw9f732p50nm026ilqb8bvy3cq")))

(define-public crate-peril-0.2.2 (c (n "peril") (v "0.2.2") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1z3gl2arzd6bpb7f1phzbzhz1ws08jaabpmlsmg8gr7y376agnvk")))

(define-public crate-peril-0.2.3 (c (n "peril") (v "0.2.3") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1q0lj4ab1k2csn16qary22as95pdlawq0z2gf3b5mhrzqa2684zr")))

(define-public crate-peril-0.2.4 (c (n "peril") (v "0.2.4") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1wjahl1ws6fff9cvb32l33jn2pvif0zlkgk79q09yi26qa4hh798")))

(define-public crate-peril-0.2.5 (c (n "peril") (v "0.2.5") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1aicm33m3964vj7k6gb09hscyc79zbws6lmxwic72s6fa4aqbpy4")))

(define-public crate-peril-0.3.0 (c (n "peril") (v "0.3.0") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0f5knvdailc2yfdqd30bk50v4s30sndhvvk2diryh3bzk60fwyaz")))

(define-public crate-peril-0.3.1 (c (n "peril") (v "0.3.1") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0vavyiqr9awv6zrk7kaanq0dihn49pp6jwg59va7l7f1wf47zlhx")))

(define-public crate-peril-0.3.2 (c (n "peril") (v "0.3.2") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "0zskdcb2mmwi0317pjxixf0qw3kbybfjxmipk2vj0zdq8hxzaalg")))

(define-public crate-peril-0.4.0 (c (n "peril") (v "0.4.0") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1nr4vz8mjsp9i038wa1r3r074ljp7rs0x7xvwys207j9hvj0167v")))

(define-public crate-peril-0.4.1 (c (n "peril") (v "0.4.1") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "1gp85gb30waghbd1qqmg5xd2pavg4flb51kj73ij4n3q7rqkcx9v")))

(define-public crate-peril-0.4.2 (c (n "peril") (v "0.4.2") (d (list (d (n "os-thread-local") (r "^0.1") (d #t) (k 0)))) (h "17q86aphwf3vzvmlkphq73940i9k1gpbx0sbf6x4zykzw1p7k0kj")))

