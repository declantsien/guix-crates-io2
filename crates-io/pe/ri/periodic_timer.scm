(define-module (crates-io pe ri periodic_timer) #:use-module (crates-io))

(define-public crate-periodic_timer-0.1.0 (c (n "periodic_timer") (v "0.1.0") (d (list (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)))) (h "0n63b817nkkgigjh9rav3h8fd1a41cl5xjanhrvdfhdm1qryd0sw")))

(define-public crate-periodic_timer-0.1.1 (c (n "periodic_timer") (v "0.1.1") (d (list (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)))) (h "19iy4pkza75vdvpbidsz4f6cxrn9lapx86hgzwi7cksg31aa0gl8")))

