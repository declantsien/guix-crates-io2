(define-module (crates-io pe ri perigee_macros) #:use-module (crates-io))

(define-public crate-perigee_macros-0.1.0 (c (n "perigee_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yg2ih7f25rbqvscc5rw664827c85p0c4q5pnnjwsflv03cvj707")))

(define-public crate-perigee_macros-0.1.1 (c (n "perigee_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13k9zdlgdad9q5ybzh3kabkd89fy1gr3hs5gyqlb4lq1c3q35ls0")))

