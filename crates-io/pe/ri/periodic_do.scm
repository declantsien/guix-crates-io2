(define-module (crates-io pe ri periodic_do) #:use-module (crates-io))

(define-public crate-periodic_do-0.1.0 (c (n "periodic_do") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)))) (h "1bxkfaprvziyyj6wkm9irn4c70n3fxzx540vl0dx4xxrr0vqggsc")))

