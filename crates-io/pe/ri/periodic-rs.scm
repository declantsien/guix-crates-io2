(define-module (crates-io pe ri periodic-rs) #:use-module (crates-io))

(define-public crate-periodic-rs-0.1.0 (c (n "periodic-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0pnmd8yf7zlpb85nkmvkxmw7wn09bifvz784gwiwsri2hsz73nqp")))

(define-public crate-periodic-rs-0.2.0 (c (n "periodic-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19alpnwb9fw1kxyvdqmzx33skgi1k0r65k3cdvkawawsnvgww1jv")))

(define-public crate-periodic-rs-0.2.1 (c (n "periodic-rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0iiqfwhzyrxyvhq703chqh5ldp6869n033n1mripx5d2cxrg57z1")))

