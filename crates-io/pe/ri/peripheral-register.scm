(define-module (crates-io pe ri peripheral-register) #:use-module (crates-io))

(define-public crate-peripheral-register-0.1.0 (c (n "peripheral-register") (v "0.1.0") (h "0xyjadqvwdca7a2r0ix272xx4m5cp5gmq9nv297syv7vm68xxs7n")))

(define-public crate-peripheral-register-0.1.1 (c (n "peripheral-register") (v "0.1.1") (h "1s6n7a6xl1wv3j473s5pn0lih0p9b1ffna764iw30zjhmvn5z39k")))

(define-public crate-peripheral-register-0.1.2 (c (n "peripheral-register") (v "0.1.2") (h "0kjx7hqas7sqw1zrbqdih4c5qp8rwndkjy91myr5fyyc0k6g02d5")))

(define-public crate-peripheral-register-0.1.3 (c (n "peripheral-register") (v "0.1.3") (h "1iry33dh80510prv5k8wiwdj4j5xdd0yf4gzh23hanrh9k5x5iac")))

(define-public crate-peripheral-register-0.1.4 (c (n "peripheral-register") (v "0.1.4") (h "0d12hd9pys2abd53ybsgk0ikwfg56fv2an24wvs0hnpfkc6b559d")))

