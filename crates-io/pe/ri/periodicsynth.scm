(define-module (crates-io pe ri periodicsynth) #:use-module (crates-io))

(define-public crate-periodicsynth-0.1.0 (c (n "periodicsynth") (v "0.1.0") (h "04j8s7pgz0xx6cnswz7sv29kyrrqzr6bl523y6f7j909c061r21n")))

(define-public crate-periodicsynth-0.1.1 (c (n "periodicsynth") (v "0.1.1") (h "1r44gmagyi3qfzkb3jffq64nhgzpghpinnf56cfmwl5ky9ghjhb5")))

(define-public crate-periodicsynth-0.1.2 (c (n "periodicsynth") (v "0.1.2") (h "1l8hzyf9mcis180w27ym3m3qgd3bdk9fqam8m2d95bb2x1c0zd56")))

(define-public crate-periodicsynth-0.1.3 (c (n "periodicsynth") (v "0.1.3") (h "1jpdg5c8zv15csii3jyskxk7c3mlzyar2q1dd069qwr66r73bqs2")))

(define-public crate-periodicsynth-0.1.4 (c (n "periodicsynth") (v "0.1.4") (h "08sjwk154ypn2c932q92q5bddssknywlv3dw1lyjj9ww9rr16yym")))

(define-public crate-periodicsynth-0.1.5 (c (n "periodicsynth") (v "0.1.5") (h "0n20swigdvlabl6k4l3cyv4pjyq275dgpml6xsdx8q5h579m974f")))

(define-public crate-periodicsynth-0.1.6 (c (n "periodicsynth") (v "0.1.6") (h "1blw1icp8cpdnp8hp9bdiwmbbz6r7a0qq612nyhlnnb86j9aid1f")))

