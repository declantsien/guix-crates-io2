(define-module (crates-io pe ri periodicelements) #:use-module (crates-io))

(define-public crate-periodicelements-0.1.0 (c (n "periodicelements") (v "0.1.0") (d (list (d (n "ascii_table") (r "^4.0.2") (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1hw5wfghyb02ywzhilb20m82c6yrb9c7fdidcba9agr00hlqshyf")))

(define-public crate-periodicelements-0.1.1 (c (n "periodicelements") (v "0.1.1") (d (list (d (n "ascii_table") (r "^4.0.2") (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0d92j0wivv0ncdqb92w7qfaky211471qjwxial4cf8dpkrclc123")))

(define-public crate-periodicelements-0.1.2 (c (n "periodicelements") (v "0.1.2") (d (list (d (n "ascii_table") (r "^4.0.2") (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1sxcyjsvvvf565wb4222zf6p05xpjg7ndxr5qjz97a39qr4ni2k4")))

