(define-module (crates-io pe rp perplex-runtime) #:use-module (crates-io))

(define-public crate-perplex-runtime-0.1.0 (c (n "perplex-runtime") (v "0.1.0") (h "0y565wk4ywjp088xvvxq5l1rblvsy1gmh588imjr5wg1nbhdpkq9")))

(define-public crate-perplex-runtime-0.1.1 (c (n "perplex-runtime") (v "0.1.1") (h "0q6fdjl564dq9jqvwcf6xb33w6532hqgg8i0kmnfsqh0022s7smw")))

