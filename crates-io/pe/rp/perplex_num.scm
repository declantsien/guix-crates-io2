(define-module (crates-io pe rp perplex_num) #:use-module (crates-io))

(define-public crate-perplex_num-0.1.0 (c (n "perplex_num") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "19589vn8ccr950q63pshz8zhi8zci841qb8wh13v9k4g7443qsb5") (f (quote (("default" "matrix")))) (s 2) (e (quote (("matrix" "dep:nalgebra")))) (r "1.76")))

