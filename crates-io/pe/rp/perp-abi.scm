(define-module (crates-io pe rp perp-abi) #:use-module (crates-io))

(define-public crate-perp-abi-0.1.0 (c (n "perp-abi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)))) (h "1yhrz694syafq7aipkwn15cb79lch5afizg6dh26gk44rii94vxy")))

