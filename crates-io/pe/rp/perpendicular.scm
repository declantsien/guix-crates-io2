(define-module (crates-io pe rp perpendicular) #:use-module (crates-io))

(define-public crate-perpendicular-0.1.0 (c (n "perpendicular") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "025l07zd9spjijgym49pjry59lg8sg3j6mzfrwpi0pjmj885n2zd") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.1 (c (n "perpendicular") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cllvc2mwzn23wz6rg8izl00bxz8fl1mavg5ajmirj8ia8x5n1cv") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.3 (c (n "perpendicular") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1909lcigp254pj4390zr5zw4hqjylwk6rfwf4dnhkrpmvwcg0878") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.4 (c (n "perpendicular") (v "0.1.4") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1560x0vp8mmy3hswqw3jgwxg9x0ycgrjzc03ph4x7xkn5bd2qyqv") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.5 (c (n "perpendicular") (v "0.1.5") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l5sf6vkwy5v08kvxnh85rgxaznnmhl3bmrml3zb7ahvbjjl08cg") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.6 (c (n "perpendicular") (v "0.1.6") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gapdh34khcxhpl22n00virx2pryxx0jmqi4paahplk18agr255r") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.7 (c (n "perpendicular") (v "0.1.7") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s6db2axz4gkvxik9g915l9h2ryjq52fy8gqx1lsknlcvnqsbfys") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.8 (c (n "perpendicular") (v "0.1.8") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jjwg9m01x7qa2swm07v5qhsjbfr0w3xy44gxpc8q0j7q0zr1wfj") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-perpendicular-0.1.9 (c (n "perpendicular") (v "0.1.9") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qlivaly17rfl6ss5bllqg3pgb1szr4a5hc0wy24vvmzp5ryj764") (f (quote (("serialize" "serde") ("default" "alloc") ("alloc"))))))

