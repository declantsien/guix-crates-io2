(define-module (crates-io pe rp perpetuals) #:use-module (crates-io))

(define-public crate-perpetuals-0.1.0 (c (n "perpetuals") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pyth-sdk-solana") (r "^0.6.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.41") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.2") (d #t) (k 0)))) (h "1905ni5ig8nngw21hpsbmpdi3aajy0rqlf83x90859dcis22h587") (f (quote (("test") ("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

