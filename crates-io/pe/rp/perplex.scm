(define-module (crates-io pe rp perplex) #:use-module (crates-io))

(define-public crate-perplex-0.4.0 (c (n "perplex") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "perplex-runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)))) (h "09zsvjxqyn8h0q3li6hmgm2lscfy2hm0mxmn49wz554lalldl1zb")))

(define-public crate-perplex-0.4.1 (c (n "perplex") (v "0.4.1") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "perplex-runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)))) (h "083zd46xs12h5gmvxy34fl9fb7x1f3yyiqaw1zzkby25f54zf4xa")))

