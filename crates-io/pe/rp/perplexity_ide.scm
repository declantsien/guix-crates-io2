(define-module (crates-io pe rp perplexity_ide) #:use-module (crates-io))

(define-public crate-perplexity_ide-0.0.1 (c (n "perplexity_ide") (v "0.0.1") (d (list (d (n "dioxus") (r "^0.2") (f (quote ("desktop"))) (d #t) (k 0)))) (h "15wj6v8yr93lvfa8m9blirsadfjvnxjbzp14fwcg34ak2zha3g65") (y #t)))

(define-public crate-perplexity_ide-0.0.2 (c (n "perplexity_ide") (v "0.0.2") (d (list (d (n "dioxus") (r "^0.2") (f (quote ("desktop"))) (d #t) (k 0)))) (h "1i0s55zd7gcjnpxj88qnqm8qi7xrlva4fzvcw5lgbmk2fzh8pn42") (y #t)))

