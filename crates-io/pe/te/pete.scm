(define-module (crates-io pe te pete) #:use-module (crates-io))

(define-public crate-pete-0.1.0 (c (n "pete") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0m6h2rnbnxhjj3l5gsbbqp004jxfccy5pgi3jzh10kyq6k3l3h0s") (y #t)))

(define-public crate-pete-0.1.1 (c (n "pete") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "05dvbs4j1cc99bb1sy04c4z9vfvqz61mwmzv7ifjm2zk54kgffiy") (y #t)))

(define-public crate-pete-0.1.2 (c (n "pete") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0gl2hbyacw7dri2pcbgmgm33wd3865rdfg8pf2zafay1rjzykj7w")))

(define-public crate-pete-0.1.3 (c (n "pete") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0can5pj6l2krv8pm4h5xilialw0ci9zr7fk60gd561a95v4q0cxn")))

(define-public crate-pete-0.1.4 (c (n "pete") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0z8k986pwf4zyf72bhr9xyrmygy4kwmxw2gdxd4jwlp8pqynim3s")))

(define-public crate-pete-0.1.5 (c (n "pete") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0yk52kmnclmnmpb97dwlp2kprzln36wm0sqc9vp561mvwk4vpp3x")))

(define-public crate-pete-0.2.0 (c (n "pete") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "14kl10fh3bpajfjpclpza4xxphp4033xjfs75jlph5r0yhqjf7a1")))

(define-public crate-pete-0.2.1 (c (n "pete") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1g98qmpn1cvqvhydfglbxl6libzmij58h9b127wsnmgjg9sf39za")))

(define-public crate-pete-0.3.0 (c (n "pete") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "11qmk8chqh6jc2l14bkxhwaip6vcfw2ywi1jijgyh4vmxhba9ibg")))

(define-public crate-pete-0.3.1 (c (n "pete") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "01rhpfpz8cnbc0c96iwkials37lcknd334i26s8aqxllh54ybl98")))

(define-public crate-pete-0.4.0 (c (n "pete") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "11wbxqprhvvrs3gh5n4ag7845w8h183zj6kx0ci1c41f7h91gm2w")))

(define-public crate-pete-0.5.0 (c (n "pete") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0ga99s2jp7rqs879llqk4zj57qv2qfrfqmks6j8qkzmsdsb5dw8s")))

(define-public crate-pete-0.6.0 (c (n "pete") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1zhnj5l3dxxl91kxq2y3yxyl88wf53r3midyi03yfmrl03vycx0l")))

(define-public crate-pete-0.7.0 (c (n "pete") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "12fxwjqfri23mv7nb6q9yap7c70x8qpcbdlrblqaz3kasib4mpsk")))

(define-public crate-pete-0.8.0 (c (n "pete") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0azsmm8p03y77czkk2dzswcfhb66hdqbb7b4qynjygv47hsbwnlf")))

(define-public crate-pete-0.9.0 (c (n "pete") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0l3q9gs4knb5zydbgajf8rjx1s8rclgqj0avx07a7sadjprb0gj9") (r "1.56.1")))

(define-public crate-pete-0.10.0 (c (n "pete") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "004xwfzdfc4bw3hh18aznz1wm143n1x6h5664xbhfg8drfrvd7i2") (r "1.63")))

(define-public crate-pete-0.11.0 (c (n "pete") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "06h0s1giw6w00gmhxh8166z8qdrg07vprdvx9aqq2xg9jds4wfgl") (r "1.64")))

(define-public crate-pete-0.12.0 (c (n "pete") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1gbc13vp3zqsyzchxg5njcrgyky67n5fhgv4z17jkps0mp0w228g") (r "1.64")))

