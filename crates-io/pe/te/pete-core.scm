(define-module (crates-io pe te pete-core) #:use-module (crates-io))

(define-public crate-pete-core-0.1.0 (c (n "pete-core") (v "0.1.0") (h "1fqsr16mrvywr529a3y3yzac87wxid0abffzy1zm83dxwpva5hpy") (y #t)))

(define-public crate-pete-core-0.2.0 (c (n "pete-core") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1wmsd15j8cirbwa6l6aykrdlblfx1w5blx71x6jqyk2ypq4j6a7d") (y #t)))

(define-public crate-pete-core-0.3.0 (c (n "pete-core") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1zsk2pixqz9f4487f8s5pf9hffhrz0rasilf49cqkc0j9abqvws8") (y #t)))

(define-public crate-pete-core-0.4.0 (c (n "pete-core") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1jv7v1k2v01rpjv69g81k3j7nvvh8hg8ckrib0jrrpzk3hr51nyb") (y #t)))

(define-public crate-pete-core-0.5.0 (c (n "pete-core") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1aar5lzcga1qi0g13b8aprflz9wyxqvip7m1s3qh936vxxcx5x5c") (y #t)))

