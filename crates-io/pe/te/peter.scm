(define-module (crates-io pe te peter) #:use-module (crates-io))

(define-public crate-peter-0.1.0 (c (n "peter") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "06pykv9g5nanlhd0xzvq001bhha4gp0cfyxz37wxn0amm2iffy6a") (y #t)))

(define-public crate-peter-0.1.1 (c (n "peter") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "0pd1n7sx6ch0w51yw8aql68jfwxw9lmzs5nbmclfnjr3xjc4xilr")))

(define-public crate-peter-0.2.0 (c (n "peter") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "15l0bhm5z6j96mf87dhbiz8cgf1vj648137w0z3bwkb09g46qzpn")))

(define-public crate-peter-0.2.1 (c (n "peter") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "08hckpkpizd4r5a49y2r5yfgc1s94pd51838n67l0xpxcxpr3n84")))

