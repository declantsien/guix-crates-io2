(define-module (crates-io pe te petersn-qdcache) #:use-module (crates-io))

(define-public crate-petersn-qdcache-0.1.0 (c (n "petersn-qdcache") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "resp") (r "^1.0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "0g279r4x46k2vj1r6d0gvbqk58jmp081210fcvafw5qk7hsxk492")))

(define-public crate-petersn-qdcache-0.1.1 (c (n "petersn-qdcache") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "resp") (r "^1.0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "0490c687sz78ri7kfkvr9lmlii844r5dr6skj59y4b9nyjvwysgp")))

