(define-module (crates-io pe lc pelcodrs) #:use-module (crates-io))

(define-public crate-pelcodrs-0.2.0 (c (n "pelcodrs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "058rhnbvam2bq7lcw2kj2p9n0f0hlnxy51irys6plhh9bj6q55v6")))

(define-public crate-pelcodrs-0.2.1 (c (n "pelcodrs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)))) (h "13hf7sih4zf1l9xjhgr8gqk9gws5cj25hcnmxyrwlmipbjkfrai8")))

