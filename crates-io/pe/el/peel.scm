(define-module (crates-io pe el peel) #:use-module (crates-io))

(define-public crate-peel-0.0.1 (c (n "peel") (v "0.0.1") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "0zlrmh0g4666wylzrqdgghzpxp2979fygb51s01ki3l2ccjkcrp1")))

(define-public crate-peel-0.0.2 (c (n "peel") (v "0.0.2") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1jp5nmskhd3kjnlqn1ipxh0zrzi8ifji3h76zjp8wkvggmnkz9xq")))

(define-public crate-peel-0.0.3 (c (n "peel") (v "0.0.3") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0daqp6x6bjqyh87hmn3g6rjbx8xv0cfxa6n7p3hr894byd49nq6g")))

(define-public crate-peel-0.1.0 (c (n "peel") (v "0.1.0") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1l3spk02f1zjs93nvi0902x4y53rfsnjn9sigd6yp3phrahfxh6q")))

(define-public crate-peel-0.1.1 (c (n "peel") (v "0.1.1") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "170njszn54ckfbc02f1nyika528hfk2z687m1l6j2kg4dgraz1pj")))

(define-public crate-peel-0.1.2 (c (n "peel") (v "0.1.2") (d (list (d (n "indextree") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "12zx5pvsi4r148svskjxyhg4a9fw8sik4ygcz5ywgbyfzag9vf2a")))

(define-public crate-peel-0.2.0 (c (n "peel") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "0rnv60qa5hywccnaq04hchv7v0q8admrnby3yaiivpmw2ls5jqi2")))

(define-public crate-peel-0.3.0 (c (n "peel") (v "0.3.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "1g9i12vdcz9s4zxvdxhpi2pbnl91fashfvi2mcxsgrxfmxcdr3hp")))

(define-public crate-peel-0.4.0 (c (n "peel") (v "0.4.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "0d7c1qw6wj4nj2a6411dzbr2j0snl0bxfwfsw9hzmyrfd40x68vk")))

(define-public crate-peel-0.5.0 (c (n "peel") (v "0.5.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "0g1n2i9brk2l63w5h2jw5wmjabj28fra163i9vijl0dkpaalp5iz")))

(define-public crate-peel-0.6.0 (c (n "peel") (v "0.6.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "0xdjgg80fnyjpzrbgp6zz9v980zmxmv6ksvbk4jxbf2almr8fkyl")))

(define-public crate-peel-0.7.0 (c (n "peel") (v "0.7.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "16xgn3rvh99n5s85kf3rjc77dm20q2zpyk107m5vwmc4lafykiwg")))

(define-public crate-peel-0.8.0 (c (n "peel") (v "0.8.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "petgraph") (r "^0") (d #t) (k 0)))) (h "1iv661smkvs8gzpxh19wg8qhgxm64mb17gps2mg13ibjpdcvy0az")))

