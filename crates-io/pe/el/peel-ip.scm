(define-module (crates-io pe el peel-ip) #:use-module (crates-io))

(define-public crate-peel-ip-0.0.1 (c (n "peel-ip") (v "0.0.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "peel") (r "^0") (d #t) (k 0)))) (h "0mxbflrmgapxzh6hqmnsrkq3w510cy4h76ildwwdxgi2iv2srlvk")))

(define-public crate-peel-ip-0.1.0 (c (n "peel-ip") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "path") (r "^0") (d #t) (k 0)) (d (n "peel") (r "^0") (d #t) (k 0)))) (h "1i4wka03r7z2qqw29sjwh50vr0zz8vv9sv3bjyj54jxrl3k1qnsf")))

(define-public crate-peel-ip-0.1.1 (c (n "peel-ip") (v "0.1.1") (d (list (d (n "log") (r "^0") (d #t) (k 2)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "peel") (r "^0") (d #t) (k 0)))) (h "0w4mf5s0sfcpv30fkvnpb926p45565qf4vff4frz0bygqp8mbbn1")))

(define-public crate-peel-ip-0.2.0 (c (n "peel-ip") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "path") (r "^0") (d #t) (k 0)) (d (n "peel") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 2)))) (h "1h1bnhafg4cy8qrfvvhrwry8dv44w38rx3abyzw918qy9h6px7w1")))

