(define-module (crates-io pe nu penum) #:use-module (crates-io))

(define-public crate-penum-0.1.0 (c (n "penum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10rj841s5b0da1ms0qxlkjlgin8rizv5bnhzb8slhfws1klhm20g")))

(define-public crate-penum-0.1.1 (c (n "penum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0y39gw0iyw8v901d0ffxnc0bbpbn2s1a2kq3dn055lsihvgbrlr1")))

(define-public crate-penum-0.1.2 (c (n "penum") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1cwxlcfnf5gy1kds0vjk2vx0wym7bw9286f16qx3nb3lf5pdkhv2")))

(define-public crate-penum-0.1.3 (c (n "penum") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "062207cdzriknyahkfkqzqlc4mf59k1145pah4ynxb460i3mnixf")))

(define-public crate-penum-0.1.4 (c (n "penum") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1fhnanhz901p5mx5js4yd5kcfgan3vg4x3jqb0jwhvs6wzq10hcx")))

(define-public crate-penum-0.1.5 (c (n "penum") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0bn0nsl0q9gmjdwd7324m10cphpklav6n1p5zv7ijp0wd8mxcxnj")))

(define-public crate-penum-0.1.6 (c (n "penum") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1q2msa96i62zgpi816bjl92hz8l3ww9xkx07va04ha0n14p2ar3x")))

(define-public crate-penum-0.1.7 (c (n "penum") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0kfgqjv7az4zwisqcilqg4d7qchx3z7xqqrl1l8hpmiipgmpz0i9")))

(define-public crate-penum-0.1.8 (c (n "penum") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1bhil8jr1jbhyfdzirvmwl9vl6k9dks6in9x6vbnrcxvy0pmv7mi")))

(define-public crate-penum-0.1.9 (c (n "penum") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0m7d1mgn7i55f7fk1n8z0xj2ibknx0vcksbmcxdxijarbmy9ld8m")))

(define-public crate-penum-0.1.10 (c (n "penum") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0nvbdmp92ggn5l7p7idrq9pwjhayvzrmga2r2kml31bi5a5cr5w4")))

(define-public crate-penum-0.1.11 (c (n "penum") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "0n8fk3ryfxbr8v17pm8dfv35dzd7bdbrmrcdb5lf2v4qhq7fjy25")))

(define-public crate-penum-0.1.12 (c (n "penum") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "0z1nmcmbqk550pvz77ziwd3cn32cn4szm14c0darwbgb9xqvwbb5")))

(define-public crate-penum-0.1.13 (c (n "penum") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1si1z9pwn81daw428p9vllhi8pbx3pv9ghqvzpy4wkhp0j3klpjs")))

(define-public crate-penum-0.1.14 (c (n "penum") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "0by1ajq06lqggz0cda1hgv6np0pws2cxn5bf1qxb3rr94r74pk3m")))

(define-public crate-penum-0.1.15 (c (n "penum") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1wy3900qj1hjl3yv3iayz70as7jhimklgzj2r8svjqszqxf1slhy")))

(define-public crate-penum-0.1.16 (c (n "penum") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "12mqswj839dz1b7lf9sphbxsazh9m5n2bl8lsrw4kjqypfx2iab2")))

(define-public crate-penum-0.1.17 (c (n "penum") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1rphzbz655v50by3ijws16rnr9xsvw4x9xp4v7b6krgwl58kwsxa")))

(define-public crate-penum-0.1.18 (c (n "penum") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "02i9dyz6vh3cxbf5asihmq62697yjfqkyvpw1r0hvbq37pcdyyjn")))

(define-public crate-penum-0.1.19 (c (n "penum") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "18d8a5swdf3cs5ffni9c15qas7jy8xl58rpzxmyrzy4v55ba3knm") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.20 (c (n "penum") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1w4xqbb6v138iqifsnssmv5rxk7ipairmx76bcb2mm2b3s3x4vfv") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.21 (c (n "penum") (v "0.1.21") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1vxvfy23zykhx7zbjbf01984hbgyq4vbmfi7dnlsfpv2k74x6a88") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.22 (c (n "penum") (v "0.1.22") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "0r6wfz4cz0jryb8a292vqndjj2b47dy7bbg3m0s9l9q3scvizba8") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.23 (c (n "penum") (v "0.1.23") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "0gfvg05i9k3cwxk5x0alghp5hbznygmarl60p9yvh0c7ggpdmcmg") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.24 (c (n "penum") (v "0.1.24") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "08wyxsk2yqyxz5sf0lk1r5yzxyxwi7bw31531dy53dh42vqxnwzi") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.25 (c (n "penum") (v "0.1.25") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1a1ilzy6hv1zkfj3wxsc5qc5ils9pk9cc6dysm2wgq3axw2m06z6") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.26 (c (n "penum") (v "0.1.26") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1i2a9ljb8bq4adddj0gzi5q7y4i24ld248v74pbjizgajzap45hw") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.27 (c (n "penum") (v "0.1.27") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1qabm5ryzcivw08chvcnff7lcgy6kczq4x4crz8hq31rkpfrhhfm") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.28 (c (n "penum") (v "0.1.28") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1yy980w385bkzrm4n33ydh6lwrf8q4n7scijpbcckzl3kk7c6hs5") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1.29 (c (n "penum") (v "0.1.29") (d (list (d (n "cargo-release") (r "^0.24.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 0)))) (h "1d5p1lfq3s9ylpgv8sv0yhyrrmp79c8bacpdm6kyxvksyxl8nd8z") (f (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

