(define-module (crates-io pe nu penumbra) #:use-module (crates-io))

(define-public crate-penumbra-0.1.0 (c (n "penumbra") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (f (quote ("suggestions"))) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kq0g0s1krv8zqrffvirgkhickiym0zzy7rmwpdshf8c7dx6qcxi")))

