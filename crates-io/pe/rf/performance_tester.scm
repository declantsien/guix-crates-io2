(define-module (crates-io pe rf performance_tester) #:use-module (crates-io))

(define-public crate-performance_tester-0.1.0 (c (n "performance_tester") (v "0.1.0") (h "16m3l9mkbk169v4cm11skvh91swhm062cq1isa56qdj40jxp5jmq")))

(define-public crate-performance_tester-0.1.1 (c (n "performance_tester") (v "0.1.1") (h "0icp7svs20kbmlnbygvnmm82ybhicq18vg9izdqnhgrr8y4xmrki")))

