(define-module (crates-io pe rf performance-mark-macro) #:use-module (crates-io))

(define-public crate-performance-mark-macro-0.1.0 (c (n "performance-mark-macro") (v "0.1.0") (d (list (d (n "performance-mark-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17qm9xifjbdbkb34cjflfb3hmyv6pjsw34fc23idgbw9rwalsimn")))

(define-public crate-performance-mark-macro-0.2.0 (c (n "performance-mark-macro") (v "0.2.0") (d (list (d (n "performance-mark-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sjw1rqi6fddrh3ssgail8l4kb9kwwf0afyqrxica660gjk982h8") (y #t)))

(define-public crate-performance-mark-macro-0.2.1 (c (n "performance-mark-macro") (v "0.2.1") (d (list (d (n "performance-mark-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zjsl354kl7c11vsb25b9qhmr4a6jr6ck3i7l80abaj1b84s1diz")))

(define-public crate-performance-mark-macro-0.2.2 (c (n "performance-mark-macro") (v "0.2.2") (d (list (d (n "performance-mark-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nkbcik8d3pis8a2sjx0p3mzwr3zfwpmyrk4mxc4s4z0pxbjpf5b")))

