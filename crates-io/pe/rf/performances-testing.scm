(define-module (crates-io pe rf performances-testing) #:use-module (crates-io))

(define-public crate-performances-testing-0.0.1 (c (n "performances-testing") (v "0.0.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "0hsl3f6cxvs0dily39cz9pk14mck7yjhbza6f1kz5nismbi0v5nb")))

(define-public crate-performances-testing-0.0.2 (c (n "performances-testing") (v "0.0.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1lng1hk6yggvk2s57y7dsxxj258l99hj738rgxqg5aqhjbvvvvq5")))

(define-public crate-performances-testing-0.0.3 (c (n "performances-testing") (v "0.0.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "0ckkv51nxr09q9a23d6y0v4r6d77vg4b2yggqr9qjgg7zz50z57x")))

(define-public crate-performances-testing-0.0.4 (c (n "performances-testing") (v "0.0.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "0fy4gwkj7d8p7i9fzm3pg988b2s3dbyqwhv8v1nj9wiq4mccz417")))

(define-public crate-performances-testing-0.0.5 (c (n "performances-testing") (v "0.0.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "18aqqgj0cpabfqhhbgkcj1vdzvzjjf91y7klbsmfy5qxzaj6nmrb")))

(define-public crate-performances-testing-0.0.6 (c (n "performances-testing") (v "0.0.6") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1whxd9lxi6ssg1l5xl3dighbqxq5awigwqm8js04m13xqkf470b0")))

(define-public crate-performances-testing-0.0.8 (c (n "performances-testing") (v "0.0.8") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "143nj8yhsidnziglybwhpah2sawyrb3m3sm4npc8p2jmmci2p99i")))

(define-public crate-performances-testing-0.0.7 (c (n "performances-testing") (v "0.0.7") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1i3s2kpvamsqc2wlvd0cqv7svlnfkkb2g0bv2rs1b1ra81hq8sck")))

(define-public crate-performances-testing-0.1.0 (c (n "performances-testing") (v "0.1.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "1xb5ydmsix8zdmr33fnw488sadkb4z7b57w3vybfa2g1md0ks1pd")))

(define-public crate-performances-testing-0.1.1 (c (n "performances-testing") (v "0.1.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0kd0b3gxjng6r6fzi7aipjf1nrnsvz3pdyfski7qk3i2fgqq075a")))

(define-public crate-performances-testing-0.1.3 (c (n "performances-testing") (v "0.1.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0489s79alk7xc6ga4pksa5vvqhw4ip5yqb0y82iwra8ax9mslcg4")))

(define-public crate-performances-testing-0.1.4 (c (n "performances-testing") (v "0.1.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0a7l9pvg7b779y3f7y89bbp0ll0zc8njbzkfvn3g3pzjd8j955hi")))

(define-public crate-performances-testing-0.1.5 (c (n "performances-testing") (v "0.1.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0vsn7c7v8mlzmahvlq7k6dmbdfynd25h98gvc3zci768yhmfiwf8")))

