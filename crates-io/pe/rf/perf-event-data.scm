(define-module (crates-io pe rf perf-event-data) #:use-module (crates-io))

(define-public crate-perf-event-data-0.1.0 (c (n "perf-event-data") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0") (d #t) (k 0)))) (h "1n5bfmx0qxffpimny17ifvxflyyg58kyywar0vkj0n6vx6vcgcgq")))

(define-public crate-perf-event-data-0.1.1 (c (n "perf-event-data") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0") (d #t) (k 0)))) (h "0jk5zhs6nhgnfj1qg992qr9vq1cqqnb85wan946di0fhwnj1k0b6")))

(define-public crate-perf-event-data-0.1.2 (c (n "perf-event-data") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0") (d #t) (k 0)))) (h "1fmrm144ynf68ybld0pribg27278icjzq6bbanpjhv7shgx5sfwn")))

(define-public crate-perf-event-data-0.1.3 (c (n "perf-event-data") (v "0.1.3") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0.3") (d #t) (k 0)))) (h "1kilx94mgnkwhgl6sch8lpqrc7kk76knk8avbk1vqgridlkn3dhq")))

(define-public crate-perf-event-data-0.1.4 (c (n "perf-event-data") (v "0.1.4") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-enum") (r "^0.2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0.3") (d #t) (k 0)))) (h "0qhw4h4gnvkk7c8in4l9w2knjf5yizrbc6jrrsack29j96f26054")))

(define-public crate-perf-event-data-0.1.5 (c (n "perf-event-data") (v "0.1.5") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-enum") (r "^0.2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0.3") (d #t) (k 0)))) (h "191kq48m6nwkcbbybv79sjihs4i6hg83q22iqlkk8ymggwn9lvxg")))

(define-public crate-perf-event-data-0.1.6 (c (n "perf-event-data") (v "0.1.6") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-enum") (r "^0.2.0") (d #t) (k 0)) (d (n "perf-event-open-sys2") (r "^5.0.3") (d #t) (k 0)))) (h "0mvkbq7i3km2pvk0yj0xmxn7dd9bvk18wdz9r4azvci0mcl4sk6q")))

