(define-module (crates-io pe rf perfect-merkle-forest) #:use-module (crates-io))

(define-public crate-perfect-merkle-forest-0.1.0 (c (n "perfect-merkle-forest") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.14.0") (d #t) (k 0)))) (h "1gr9j5bb53fq2gcgx6l5ld6y884hvz9hm1af587pqp0wdaakf1cg")))

