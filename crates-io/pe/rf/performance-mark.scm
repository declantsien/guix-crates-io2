(define-module (crates-io pe rf performance-mark) #:use-module (crates-io))

(define-public crate-performance-mark-0.1.0 (c (n "performance-mark") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s5xd0gdr8020vz4b02pl423kxjc0xsikv02m40aqzcmaidqb9b0") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-performance-mark-0.2.0 (c (n "performance-mark") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 2)))) (h "1jvzv05bh4xszpn0pk6z4bcx240dr5xdpslmnp3hwmpc8gdapdqi") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-performance-mark-0.3.0 (c (n "performance-mark") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 2)))) (h "174b6iddaqz3s0f18z09lr4s25zpyjbi19dlxhqmbwkv8a0qrs2y") (f (quote (("derive_serde" "serde/derive") ("default"))))))

