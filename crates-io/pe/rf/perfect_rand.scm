(define-module (crates-io pe rf perfect_rand) #:use-module (crates-io))

(define-public crate-perfect_rand-0.1.0 (c (n "perfect_rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1365csi2q6b1m181j94df7122d8cyc6jsw0klck81w13688py90a")))

(define-public crate-perfect_rand-0.1.1 (c (n "perfect_rand") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07b1yyg9yqjvjxh77k2zr57na4y0b4gzh3rkwc75vsl4gmqyaiz7")))

(define-public crate-perfect_rand-0.1.2 (c (n "perfect_rand") (v "0.1.2") (d (list (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03ywqvrhm48ij4gy9f5dcxnsdf90kqy8zp7vfmvwg8j6xrl0yidj")))

(define-public crate-perfect_rand-0.1.3 (c (n "perfect_rand") (v "0.1.3") (d (list (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jjkg3lfvcjzdgfj2v47zdb22466wrxjs4cbdkhip2z3slx47vxz")))

(define-public crate-perfect_rand-0.1.4 (c (n "perfect_rand") (v "0.1.4") (d (list (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wz21l1dcy44xjx0jma3kafxrr039psklswfksmn1zqn086kbga2")))

(define-public crate-perfect_rand-0.1.5 (c (n "perfect_rand") (v "0.1.5") (d (list (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11irnijir6ysyxanrz2x1czflrs4vnkycwwk68ncwifi9g9q8h0h")))

