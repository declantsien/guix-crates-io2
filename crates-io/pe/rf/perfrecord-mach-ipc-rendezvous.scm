(define-module (crates-io pe rf perfrecord-mach-ipc-rendezvous) #:use-module (crates-io))

(define-public crate-perfrecord-mach-ipc-rendezvous-0.1.0 (c (n "perfrecord-mach-ipc-rendezvous") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1fikvvxnkwbhj44cvkbz9gdf8p3j5lr2jg2q2jhwq946yjq714fn")))

(define-public crate-perfrecord-mach-ipc-rendezvous-0.2.0 (c (n "perfrecord-mach-ipc-rendezvous") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pbj87blvfzkqdjfv8lf78hapixvzld8x7314gkfpx0hvl54b4wb")))

(define-public crate-perfrecord-mach-ipc-rendezvous-0.2.1 (c (n "perfrecord-mach-ipc-rendezvous") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ngl9kq2ai766zy24qszjcb37h7hn92xk2pbn1f6bxrd4xn7w5hx")))

