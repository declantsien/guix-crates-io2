(define-module (crates-io pe rf performance-timing) #:use-module (crates-io))

(define-public crate-performance-timing-0.1.0 (c (n "performance-timing") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)))) (h "06hn3qkmf4lhl3lrqzpyl4qf11x7lyhlapap9907731n40i7md7c")))

(define-public crate-performance-timing-0.1.1 (c (n "performance-timing") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)))) (h "1avhmy1i85zz2zsilyqcxhqdyi0vn2w2s8xs7hwjls2frk44pqjc")))

