(define-module (crates-io pe rf performance-mark-impl) #:use-module (crates-io))

(define-public crate-performance-mark-impl-0.1.0 (c (n "performance-mark-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0s8sq71y87y2fgi5b0wak7ywz6bbyczny4zszfycgwy2k6qipc1n")))

(define-public crate-performance-mark-impl-0.2.0 (c (n "performance-mark-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1s3075awkam5az0sl8rrs00gxma7xn0cm59jqs9c2274qx6afkyi") (y #t)))

(define-public crate-performance-mark-impl-0.2.1 (c (n "performance-mark-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1lgmjwbf46iavz3s0ckdchwzzjyvmpv6f4lwqqmwxf47n2xfxb27")))

(define-public crate-performance-mark-impl-0.2.2 (c (n "performance-mark-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1sz7ry13b957wzy6ms7qb3iva3knvd7qp9gawd0w9vh9myysrry8")))

