(define-module (crates-io pe rf perftree) #:use-module (crates-io))

(define-public crate-perftree-0.1.0 (c (n "perftree") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1381bhrkiyg382jxlqz2hrlgb70ji6hmxln3jzisan3lnrkyww4k")))

(define-public crate-perftree-0.1.1 (c (n "perftree") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1f069jk806p431fjrds407kcwfa2i84dydm6vp1dlvq3r7n8chlf")))

(define-public crate-perftree-0.1.2 (c (n "perftree") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "015z4ipgs4pwkkss40ib0x8v96r3xkm7bmcmpgmy7z7dqjdsc3nm")))

(define-public crate-perftree-0.2.0 (c (n "perftree") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "15xq5j3qj7bfzl0970c92q5z8wdw9dymancg8a0pn8slaxsvg8q8")))

(define-public crate-perftree-0.3.0 (c (n "perftree") (v "0.3.0") (h "10l3dn2axvwd3bx4fk48fihriclj2fh7zg735z42i2p8qqcmzs43")))

