(define-module (crates-io pe rf perf_stats) #:use-module (crates-io))

(define-public crate-perf_stats-0.1.1 (c (n "perf_stats") (v "0.1.1") (d (list (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0rb4qrvas3y98xls1b76gpac6ya2xqxm0qwi8pw2cms216hdkck7")))

(define-public crate-perf_stats-0.1.2 (c (n "perf_stats") (v "0.1.2") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1z0ax72mb472rcvx88rv7wb3khq2wpm6mr6fg1w76irvqqk1ihvb")))

(define-public crate-perf_stats-0.1.3 (c (n "perf_stats") (v "0.1.3") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "02bdxzz411wh9qdq52l0h1mipshz56axa06513qms26drdbrpbdr")))

(define-public crate-perf_stats-0.1.4 (c (n "perf_stats") (v "0.1.4") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0wxlymqp2i68jj9vb4c40vvi8b33jn4cjld7zync9dzh5vbf2f9l")))

(define-public crate-perf_stats-0.1.5 (c (n "perf_stats") (v "0.1.5") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0xa58qnc1d0dy6ikg88sac544jqvgv9vlzv2q55dghr0bmzp2dmy")))

(define-public crate-perf_stats-0.1.6 (c (n "perf_stats") (v "0.1.6") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "19xbcfd8absy165mdaxzi5sk807xviqqyc3h3n18zz03xrr8naqx")))

(define-public crate-perf_stats-0.1.7 (c (n "perf_stats") (v "0.1.7") (d (list (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0hlcd8rzx2svyrcarrl6ja72kdgv7pjnqyfcpck34gkz0cyindcp")))

(define-public crate-perf_stats-0.1.8 (c (n "perf_stats") (v "0.1.8") (d (list (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "13cs7z7mrwd56hpwjrsjjy5nj30dhb2a096n4a9rf6lqf2n7y5xh")))

