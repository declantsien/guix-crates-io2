(define-module (crates-io pe rf perftacho) #:use-module (crates-io))

(define-public crate-perftacho-0.3.0 (c (n "perftacho") (v "0.3.0") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0nqkhkdm8h5rarmdzy07pv4668rj25iv0b1zzx357si5lzs0dbmm")))

(define-public crate-perftacho-0.3.1 (c (n "perftacho") (v "0.3.1") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1b2flq765m838wmn4j70d29ahzd5s2gig3findwkj93kawadzn0r")))

(define-public crate-perftacho-0.4.0 (c (n "perftacho") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1yyk86i3i4dp59icz998s1wknn6zhx90ykig5p2iqhj3j9r37869")))

(define-public crate-perftacho-0.4.1 (c (n "perftacho") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "028g2scwpsxp68scpgnpbbsnq86cfh0wqdhdjy7kr04wggb9bfls")))

(define-public crate-perftacho-0.4.2 (c (n "perftacho") (v "0.4.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1b6a77332ky262y1rx19bfl3bx3q5g5h5vpnj2ajjw63hk0qnnad")))

