(define-module (crates-io pe rf perfetto_protos) #:use-module (crates-io))

(define-public crate-perfetto_protos-0.1.0 (c (n "perfetto_protos") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3.0") (d #t) (k 1)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)))) (h "0xxffjvgj2328wvmlb0198k5xqh5gcp326zh4r7y6mzh3fn2nhrx")))

