(define-module (crates-io pe rf perfdata) #:use-module (crates-io))

(define-public crate-perfdata-0.1.0 (c (n "perfdata") (v "0.1.0") (h "1k63xm9d3087nsr9rhbir0h4i8s1x7nwg95h2wgw12z20rxn9a7v")))

(define-public crate-perfdata-0.2.0 (c (n "perfdata") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hzzkrc7fq93p622xv83fmw3glalp5cy673jnh885zpk9mx5v799")))

(define-public crate-perfdata-0.3.0 (c (n "perfdata") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04q58nvm1h5l5hwj90rz2nylrlnqz1xqlndifcq52kivj6vwjxrm")))

(define-public crate-perfdata-0.4.0 (c (n "perfdata") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m7k3ic2m3bsfnpwhl31j8fv0lsmnqh2x1hg0cwdmask3bfyr4y7")))

(define-public crate-perfdata-0.4.2 (c (n "perfdata") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "18a08z637aj2593n151qdc27n1vzn30l9rdb6jp5a80ig8krfx6f")))

(define-public crate-perfdata-0.4.3 (c (n "perfdata") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hgxg7xyyc2v7jlyv3sywc1pxadj2dnvzzgp8nr0172r847mn3n6")))

