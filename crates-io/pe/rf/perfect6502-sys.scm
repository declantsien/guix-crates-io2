(define-module (crates-io pe rf perfect6502-sys) #:use-module (crates-io))

(define-public crate-perfect6502-sys-0.1.0 (c (n "perfect6502-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jhs9qha668hk9i3w6cwrna0dlzgdh6ld1w52pqcajlcync1h4w2") (l "perfect6502")))

(define-public crate-perfect6502-sys-0.2.0 (c (n "perfect6502-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j172529a7hsclg7a2h8b9aa31b6h705wk736sridlq2vcnysc2i") (l "perfect6502")))

(define-public crate-perfect6502-sys-0.2.1 (c (n "perfect6502-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lwfcrz5xqph5vcl1mp6fifhy4i7qlbn2xbah5zf2q651aa942iw") (l "perfect6502")))

