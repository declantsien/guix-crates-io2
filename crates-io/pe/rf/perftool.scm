(define-module (crates-io pe rf perftool) #:use-module (crates-io))

(define-public crate-perftool-0.1.0 (c (n "perftool") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "047qnhcr4xr0nqs7l4wwpvpclq42n8v517i81slw4jw8014djam5")))

(define-public crate-perftool-0.2.0 (c (n "perftool") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "030da3d3acpxcgkjn4g7lcnwcfbl81srmqnpvsssswv5pj47aqxl")))

