(define-module (crates-io pe rf perfect-derive) #:use-module (crates-io))

(define-public crate-perfect-derive-0.1.0 (c (n "perfect-derive") (v "0.1.0") (d (list (d (n "cps") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "174xfw18kghvaarp8hfmh8lz6ayvwnp22wq0k2kysyfq7mbfyqnh")))

(define-public crate-perfect-derive-0.1.1 (c (n "perfect-derive") (v "0.1.1") (d (list (d (n "cps") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "035zz04zc3j64wvf9jq7gzzq28f24rl956cq1hj8ram5pqj104w6")))

(define-public crate-perfect-derive-0.1.2 (c (n "perfect-derive") (v "0.1.2") (d (list (d (n "cps") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1kc2qyq9248x7a04qid2xxhdjq98y8kwj2hx4h78w4p56nlx8a92")))

(define-public crate-perfect-derive-0.1.3 (c (n "perfect-derive") (v "0.1.3") (d (list (d (n "cps") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "14cp7q1j2lybxmrp66pw9cz7ajwh8ywdqblii549qij3ibl9w3dd")))

