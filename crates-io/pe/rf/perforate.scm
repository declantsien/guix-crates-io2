(define-module (crates-io pe rf perforate) #:use-module (crates-io))

(define-public crate-perforate-0.1.0 (c (n "perforate") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7s76yhsv1yawa60qhgpwx62aag8a2r5qq90g42har7m72w9j01")))

