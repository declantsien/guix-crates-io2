(define-module (crates-io pe rf perf-event-open-sys2) #:use-module (crates-io))

(define-public crate-perf-event-open-sys2-4.0.0 (c (n "perf-event-open-sys2") (v "4.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12g4vxxqf9m1hyb0d2nqhyqhjq7816d1jviq7y7zmcklh29bn2bz")))

(define-public crate-perf-event-open-sys2-5.0.0 (c (n "perf-event-open-sys2") (v "5.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19fqxdjvkfgrkgxyfh0fiaz2vxsvsfiyk2pnng1xr85a0016vv0x")))

(define-public crate-perf-event-open-sys2-5.0.1 (c (n "perf-event-open-sys2") (v "5.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l8qv3ylyvsqrf3jaa81gjimixkypdhs2p87lwnafy3dbj8ck6lb")))

(define-public crate-perf-event-open-sys2-5.0.2 (c (n "perf-event-open-sys2") (v "5.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n244wq67yh0bzcjg2npc4aih3f7njqzza71li79inzyw0k803lg")))

(define-public crate-perf-event-open-sys2-5.0.3 (c (n "perf-event-open-sys2") (v "5.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lxaback707c9bfny07vvzayaczafd76wza8gfwrkpvalwc98rc3")))

(define-public crate-perf-event-open-sys2-5.0.4 (c (n "perf-event-open-sys2") (v "5.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.9.1") (d #t) (k 0)))) (h "1i820npw2ziw30s02flh72abkhbfpq603g6k7zsqjl4krzh08l2r")))

