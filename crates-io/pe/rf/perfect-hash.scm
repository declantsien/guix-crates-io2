(define-module (crates-io pe rf perfect-hash) #:use-module (crates-io))

(define-public crate-perfect-hash-0.1.0 (c (n "perfect-hash") (v "0.1.0") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "0k136f8prk7fmfl30682lag2vayhk51yk88ri6s382p2vl1hy189") (y #t)))

(define-public crate-perfect-hash-0.1.1 (c (n "perfect-hash") (v "0.1.1") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "000060d7srjw86v5n97kljmflrv0zas8n04vzf77knzmk00xqa65") (y #t)))

(define-public crate-perfect-hash-0.1.2 (c (n "perfect-hash") (v "0.1.2") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "0q5jh7dikn9psgid4lf8kmmrhivxk5lk5g0w3vh8ninv5gmyxmg7") (y #t)))

(define-public crate-perfect-hash-0.1.3 (c (n "perfect-hash") (v "0.1.3") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1jlml9nbfx5p5gisyjkvngx9vqyr4kyvhn9jvcq8a3cdbg6c4apz") (y #t)))

(define-public crate-perfect-hash-0.1.4 (c (n "perfect-hash") (v "0.1.4") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1nwlll4c6wilyxj2nag3kdjgmbhb87bgj7f4wlaj3jlicf1cd5mp") (y #t)))

(define-public crate-perfect-hash-0.1.5 (c (n "perfect-hash") (v "0.1.5") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1rxzx4p1zpvn8x8hvda79s2gjw93afs2vqw4p9py1q61rm2q3hh4") (y #t)))

(define-public crate-perfect-hash-0.1.6 (c (n "perfect-hash") (v "0.1.6") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "0nq7jjr1bpfqvf2vqnp0wlya50cy1z3dyg3zq9ymcnzq5675yx6h") (y #t)))

(define-public crate-perfect-hash-0.1.7 (c (n "perfect-hash") (v "0.1.7") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "0dp7x7qmbq0m8lx548wd1qp1v89fjj55lkini4m2bpj10rh38jh8") (y #t)))

(define-public crate-perfect-hash-0.1.8 (c (n "perfect-hash") (v "0.1.8") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "0f3yiz01mghvmixgw4k7a3gn0zmch1x9h3bbhnpmikz6gpqh9k08") (y #t)))

(define-public crate-perfect-hash-0.1.9 (c (n "perfect-hash") (v "0.1.9") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1nawk09l7dn062h46wf6x8dsgnr2zgn2lbp8jxxb2vp81fj0si9i") (y #t)))

(define-public crate-perfect-hash-0.1.10 (c (n "perfect-hash") (v "0.1.10") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1082s6nlqfl3d96wyg3wzqxyb4ifiv40sxfpar9n1m1ys3xk1nyg") (y #t)))

(define-public crate-perfect-hash-0.1.11 (c (n "perfect-hash") (v "0.1.11") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "12mphnaqs9kw0hpwq8xf9q2l2ngabl5a1j5w9f31mhv4zg3j06sw") (y #t)))

(define-public crate-perfect-hash-0.1.12 (c (n "perfect-hash") (v "0.1.12") (d (list (d (n "nohash-hasher") (r "^0.1.1") (d #t) (k 0)))) (h "1d0i0w1ksdpz2sfrjsl63spa8nx8nj5l296097vrlc1mamxww8aq") (y #t)))

