(define-module (crates-io pe rf perfer) #:use-module (crates-io))

(define-public crate-perfer-0.1.1 (c (n "perfer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "wait4") (r "^0.1.3") (d #t) (k 0)))) (h "1dq6bmmlw9amc82a30x0la1bn9w53lw8wfhj3hdpc5pgbcrjhzrk")))

(define-public crate-perfer-0.1.2 (c (n "perfer") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "wait4") (r "^0.1.3") (d #t) (k 0)))) (h "1x3d21c5i0ilwrbkws7ala9crdvc05wbm9khisbj4wh8vrdpsa36")))

